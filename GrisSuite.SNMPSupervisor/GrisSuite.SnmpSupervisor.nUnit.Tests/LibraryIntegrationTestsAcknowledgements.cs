﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsAcknowledgements
    {
        [Test]
        public void DumpCompleto_PerifericaInErrore_TacitataALivelloPeriferica()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287423");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
                Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 9, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 76534\r\nDynamic Definition File: INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=0;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=0;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=0;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 2, Description: Guasto=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 2, Description: Guasto=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 0, Value: 2, Description: Guasto=0\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInAttenzione_TacitataALivelloPeriferica()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287322");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
                Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys SNMP Incompleto, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 9, ReplyICMP: True, ReplySNMP: True, Device Category: 2, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 615243\r\nDynamic Definition File: INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19364185, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Binario LED doppia faccia=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: LED-B, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 615243, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: vDSPAG96240URFI\\D9C09A[250320090940], Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 2, Description: Binario LED doppia faccia=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 1, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Dati di monitoraggio incompleti=1;Dati di monitoraggio incompleti=1;Dati di monitoraggio incompleti=1;Dati di monitoraggio incompleti=1\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.190, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 1, Value: n/d, Description: Dati di monitoraggio incompleti=1, Exception: Errore caricamento dati SNMP da periferica, StreamField: 'Gateway interfaccia di rete #1', ID: 6, Inner Exception: Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\ninternal class Evaluation {public object GetValue(Lextm.SharpSnmpLib.Variable[,] table, int row) {if (table == null) {return null;} return ((Lextm.SharpSnmpLib.IP)table[row, 5].Data).ToIPAddress();}}\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 1, Value: n/d, Description: Dati di monitoraggio incompleti=1, Exception: Errore caricamento dati SNMP da periferica, StreamField: 'DNS primario interfaccia di rete #1', ID: 7, Inner Exception: Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\ninternal class Evaluation {public object GetValue(Lextm.SharpSnmpLib.Variable[,] table, int row) {if (table == null) {return null;} return ((Lextm.SharpSnmpLib.IP)table[row, 6].Data).ToIPAddress();}}\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 1, Value: n/d, Description: Dati di monitoraggio incompleti=1, Exception: Errore caricamento dati SNMP da periferica, StreamField: 'DNS secondario interfaccia di rete #1', ID: 8, Inner Exception: Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\ninternal class Evaluation {public object GetValue(Lextm.SharpSnmpLib.Variable[,] table, int row) {if (table == null) {return null;} return ((Lextm.SharpSnmpLib.IP)table[row, 7].Data).ToIPAddress();}}\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 1, Value: n/d, Description: Dati di monitoraggio incompleti=1, Exception: Errore caricamento dati SNMP da periferica, StreamField: 'MTU interfaccia di rete #1', ID: 9, Inner Exception: Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\ninternal class Evaluation {public object GetValue(Lextm.SharpSnmpLib.Variable[,] table, int row) {if (table == null) {return null;} return ((Lextm.SharpSnmpLib.Integer32)table[row, 8].Data).ToInt32();}}\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 04/10/2009 23:33:27, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 02/10/2009 17:46:32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 11, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 8, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 1, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 35, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1 [UBICOM], Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0;A resistenza=0;Valore ricevuto=0;Riscaldatore spento=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo elemento riscaldante #1, Severity: 0, Value: 1, Description: A resistenza=0\r\nField Id: 2, Array Id: 0, Name: Potenza riscaldante applicata al riscaldatore (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato dell'elemento riscaldante #1, Severity: 0, Value: 0, Description: Riscaldatore spento=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'elemento riscaldante #1, Severity: 0, Value: Dispositivo anti-condensa #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'elemento riscaldante #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Spenta=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Spenta=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Spenta=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Spenta=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 4, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 0, Description: Spenta=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 0, Description: Spenta=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 0, Description: Spenta=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: Ventola di raffreddamento #3, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 16, Array Id: 0, Name: Tipo ventola #4, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 17, Array Id: 0, Name: Velocità della ventola (%) #4, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 18, Array Id: 0, Name: Stato della ventola #4, Severity: 0, Value: 0, Description: Spenta=0\r\nField Id: 19, Array Id: 0, Name: Descrizione della ventola #4, Severity: 0, Value: Ventola di raffreddamento #4, Description: Valore ricevuto=0\r\nField Id: 20, Array Id: 0, Name: Severità complessiva della ventola #4, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 11, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di luminosita' ambientale #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 5, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #2, Severity: 0, Value: 7, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Stato del sensore #2, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 7, Array Id: 0, Name: Descrizione del sensore #2, Severity: 0, Value: Sensore di luminosita' ambientale #2, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Severità complessiva relativa al sensore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 12, Name: Stato dei LED, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Percentuale di LED guasti, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Soglia oltre cui il messaggio può non essere comprensibile (%), Severity: 0, Value: 10, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa allo stato dei LED, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p3V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p3V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 5, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 1, Description: p3V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 3.3V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 1, Description: p3V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 3.3V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 5, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 16, Array Id: 0, Name: Tipo alimentatore #4, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 17, Array Id: 0, Name: Stato alimentatore #4, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 18, Array Id: 0, Name: Tensione in uscita dell'alimentatore #4 (V o mV), Severity: 0, Value: 12, Description: Valore ricevuto=0\r\nField Id: 19, Array Id: 0, Name: Descrizione dell'alimentatore #4, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 20, Array Id: 0, Name: Severità complessiva dell'alimentatore #4, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 21, Array Id: 0, Name: Tipo alimentatore #5, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 22, Array Id: 0, Name: Stato alimentatore #5, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 23, Array Id: 0, Name: Tensione in uscita dell'alimentatore #5 (V o mV), Severity: 0, Value: 12, Description: Valore ricevuto=0\r\nField Id: 24, Array Id: 0, Name: Descrizione dell'alimentatore #5, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 25, Array Id: 0, Name: Severità complessiva dell'alimentatore #5, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInAttenzione_Offline_TacitataALivelloPeriferica()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287430");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
                Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");
                    Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys non valido 3, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 1, Severity: 9, ReplyICMP: False, ReplySNMP: False, Device Category: -2, Device with dynamic loading rules enabled, NoMatchDeviceStatusSeverity: Warning, NoMatchDeviceStatusDescription: Dati diagnostici limitati alla sola sezione MIB-II\r\nDynamic Definition File: INFSTAZ1.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaOk_TacitataALivelloPeriferica()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174484765");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
                Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Solari 1, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 9, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 54321\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WID 3216 - Solari, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - Next Generation GT V1.0 (Huckleberry), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 8627198, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: info@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: \"Solari di Udine (default location)\", Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 123456, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 54321, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: v. 1.0.2, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.2.14-5.0, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: Atrio, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: M1T1AB01T6421M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: M1T1AB01T6421M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-18-7D-04-72-E2, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.102.109.29, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.102.109.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1496, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 14/10/2009 11:54:07, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 13/10/2009 11:56:14, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 9, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 81, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 103, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 45, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 159, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p24V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: -1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: -1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInErrore_TacitataALivelloStream()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232263810");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(4, null);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Workstation XP PDL test 02, Type: WRKPDL000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Microsoft Windows XP=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WRKPDL001, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: -255, Value: Hardware: x86 Family 6 Model 7 Stepping 6 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Microsoft Windows XP=-255\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 0, Value: 0 giorni, 00:07:40.820, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Your contact here, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Your location here, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: -255, Value: RFISERVIZI, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.110.130, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.110.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: -255, Value: 5, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Versione minore sistema operativo, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: System Uptime, Severity: -255, Value: 0 giorni, 01:25:03.590, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Unità logiche disco, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: -255, Value: 12%, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Disco D - Spazio occupato, Severity: -255, Value: 0%, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Servizi, Severity: 2, Description: In esecuzione=0;Non presente=2;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato Client DHCP, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato UltraVNC Server, Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 2, Array Id: 0, Name: Stato servizio Workstation, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato McAfee Framework, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato McAfee McShield, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato RPC (Remote Procedure Call), Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Stato servizio Ora di Windows, Severity: 0, Value: 1, Description: In esecuzione=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInAttenzione_TacitataALivelloStreamEPeriferica()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174492893");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(1, null);
                deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
                Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInAttenzione_TacitataALivelloStream()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490483");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(3, null);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Atrio Arrivi, Type: AET0000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 1400), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 1 day 18:26:01.97, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 0, Description: Stato non disponibile=0;Data e ora ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione, Severity: 0, Value: 0, Description: Stato non disponibile=0\r\nField Id: 1, Array Id: 0, Name: Data e ora, Severity: 0, Value: 18/06/2009 10:51:21, Description: Data e ora ricevuta=0\r\nStream Id: 3, Name: Parametri ambientali, Severity: 1, Description: Valore anomalo=1\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 1, Value: 56 (°C), Description: Valore anomalo=1\r\nStream Id: 4, Name: Informazioni generali, Severity: 0, Description: Produttore riconosciuto=0;Versione ricevuta=0;Monitor TFT 32 pollici=0;Locazione dispositivo ricevuta=0;Informazioni ricevute=0\r\nField Id: 0, Array Id: 0, Name: Produttore, Severity: 0, Value: Aesys SpA - Seriate (BG) - ITALY, Description: Produttore riconosciuto=0\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: 0, Value: 1.0.4 / TFT, Description: Versione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Categoria dispositivo, Severity: 0, Value: 0, Description: Monitor TFT 32 pollici=0\r\nField Id: 3, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: n/d, Description: Locazione dispositivo ricevuta=0\r\nField Id: 4, Array Id: 0, Name: Informazioni aggiuntive, Severity: 0, Value: n/d, Description: Informazioni ricevute=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaICMPInOk_TacitataALivelloStreamEPeriferica()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174471094");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
                deviceAcknowledgement.AddDeviceAcknowledgement(1, null);
                Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: STLC 1000, Type: XXIP000, Offline: 0, Severity: 9, ReplyICMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Risposta ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 0, Value: 1, Description: Risposta ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaICMPInSconosciuto_TacitataALivelloStreamEPeriferica()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174471095");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
                deviceAcknowledgement.AddDeviceAcknowledgement(1, null);
                Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: STLC 1000, Type: XXIP000, Offline: 1, Severity: 9, ReplyICMP: False\r\nStream Id: 1, Name: Stato generale, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 255, Value: 0, Description: Stato sconosciuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInAttenzione_TacitataALivelloStreamField()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511111");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(2, 0);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInErrore_TacitataALivelloStreamField()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511112");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(3, 0);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInErroreTriploStreamField_TacitataALivelloStreamField()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174502032");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(5, 3);
                deviceAcknowledgement.AddDeviceAcknowledgement(6, 4);
                deviceAcknowledgement.AddDeviceAcknowledgement(11, 2);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(3, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Sysco 144, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 608-06\r\nDynamic Definition File: INFSTAZ-Sysco.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MAD3AT08405M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 3439985, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: SYSCO S.p.A., Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: MONITOR LCD/TFT, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 608-06, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 6.1.1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 1.0, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: ATRIO - SPORTELLO DX BIGLIETTERIA, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: MAD3AT08405M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 0, Value: Monta HS2605, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: MAD3AT08405M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 03/10/2009 18:32:11, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 03/10/2009 08:59:24, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 8, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 255, Description: Valore non riconosciuto=1;Valore anomalo=1;Valore entro i limiti normali=0;Allarme lieve=1\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 1, Value: 4, Description: Valore non riconosciuto=1\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 1, Value: 2553, Description: Valore anomalo=1\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 1000, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 1, Value: 1, Description: Allarme lieve=1\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Allarme lieve=1\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 48, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Gestione Ventole, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 1, Value: 1, Description: Allarme lieve=1\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: ---, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: RADIALE, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 255, Description: Acceso=0;Nessun guasto=0;Allarme maggiore=2\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 2, Value: 3, Description: Allarme maggiore=2\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInWarningTriploStreamField_TacitataALivelloStreamField_SuUnSoloErrore()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174502033");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(5, 3);
                Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Sysco 145, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 608-06\r\nDynamic Definition File: INFSTAZ-Sysco.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MAD3AT08405M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 3439985, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: SYSCO S.p.A., Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: MONITOR LCD/TFT, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 608-06, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 6.1.1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 1.0, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: ATRIO - SPORTELLO DX BIGLIETTERIA, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: MAD3AT08405M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 0, Value: Monta HS2605, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: MAD3AT08405M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 03/10/2009 18:32:11, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 03/10/2009 08:59:24, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 8, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 255, Description: In esecuzione ma non connesso=2;Valore anomalo=1;Valore entro i limiti normali=0;Allarme maggiore=2\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 1, Value: 3553, Description: Valore anomalo=1\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 1000, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 2, Value: 3, Description: Allarme maggiore=2\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 1, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Allarme minore=1\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 48, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Gestione Ventole, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 1, Value: 2, Description: Allarme minore=1\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: ---, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: RADIALE, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 1, Description: Acceso=0;Nessun guasto=0;Allarme minore=1\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 1, Value: 2, Description: Allarme minore=1\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_PerifericaInErrorTriploStreamField_TacitataALivelloStreamFieldEStream()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174502034");

            if (deviceObject != null)
            {
                DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
                Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
                deviceAcknowledgement.AddDeviceAcknowledgement(5, null);
                deviceAcknowledgement.AddDeviceAcknowledgement(6, 4);
                deviceAcknowledgement.AddDeviceAcknowledgement(11, null);
                Assert.AreEqual(2, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
                Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

                deviceObject.DeviceAck = deviceAcknowledgement;

                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Sysco 146, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 608-06\r\nDynamic Definition File: INFSTAZ-Sysco.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MAD3AT08405M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 3439985, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: SYSCO S.p.A., Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: MONITOR LCD/TFT, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 608-06, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 6.1.1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 1.0, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: ATRIO - SPORTELLO DX BIGLIETTERIA, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: MAD3AT08405M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 0, Value: Monta HS2605, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: MAD3AT08405M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 03/10/2009 18:32:11, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 03/10/2009 08:59:24, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 8, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: Non in esecuzione=2;Valore gravemente anomalo=2;Valore entro i limiti normali=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 3, Description: Non in esecuzione=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 2, Value: 5625, Description: Valore gravemente anomalo=2\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 1000, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 2, Value: 4, Description: Allarme grave=2\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Allarme maggiore=2\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 48, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Gestione Ventole, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 2, Value: 3, Description: Allarme maggiore=2\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: ---, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: RADIALE, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 1, Description: Acceso=0;Nessun guasto=0;Allarme lieve=1\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 1, Value: 1, Description: Allarme lieve=1\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }
    }
}
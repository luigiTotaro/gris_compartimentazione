﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsUAS_NTA
    {
        #region Test UAS/NTA

        [Test]
        public void DumpCompleto_definitionUAS_NTA_1_OK()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174504186");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: UAS/NTA Verdello, Type: UASNTA100, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Forced serial number by SNMP query: 0007\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Locazione ricevuta=-255;Valore ricevuto=-255;Contatto di riferimento ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Microsoft Windows CE Version 6.0 (Build 0), Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 4 giorni, 05:31:41.860, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Locazione, Severity: -255, Value: RFI, Description: Locazione ricevuta=-255\r\nField Id: 4, Array Id: 0, Name: Codice identificativo periferica, Severity: -255, Value: .1.3.6.1.4.1.38878.1.10.1.1, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: manutenzione@sdiautomazione.it, Description: Contatto di riferimento ricevuto=-255\r\nStream Id: 2, Name: Configurazione apparato, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Seriale RS232=-255;4800 baud=-255;Delay 16=-255;Valore ricevuto=-255;Trap attive=-255;Engineering Station Serial=-255\r\nField Id: 0, Array Id: 0, Name: Nome apparato, Severity: -255, Value: NTA AT91SAM9260, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione apparato, Severity: -255, Value: Scheda UAS/NTA, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Descrizione stazione di appartenenza, Severity: -255, Value: UASNTA, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Numero di serie della UAS, Severity: -255, Value: 0007, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Posizione geografica, Severity: -255, Value: RFI, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Dettagli della revisione del sistema operativo, Severity: -255, Value: Windows CE 6.0 (1.19), Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Data della revisione del sistema operativo, Severity: -255, Value: Sep  7 2012, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Dettagli della revisione dell'applicazione (processo DAC), Severity: -255, Value: DAC 04.11.00, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Data della revisione dell'applicazione (processo DAC), Severity: -255, Value: Dec 12 2012 17:44:18, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Release della configurazione caricata dal sistema SCADA, Severity: -255, Value: [38.10][00], Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.102.184.250, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera sotto rete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.184.201, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Porta TCP per la connessione di SCADA 1, Severity: -255, Value: 2404, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Porta TCP per la connessione di SCADA 2, Severity: -255, Value: 2405, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Porta TCP per la connessione di SCADA 3, Severity: -255, Value: 2406, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo connessione seriale, Severity: -255, Value: 3, Description: Seriale RS232=-255\r\nField Id: 17, Array Id: 0, Name: Baud rate comunicazione sulla seriale, Severity: -255, Value: 6, Description: 4800 baud=-255\r\nField Id: 18, Array Id: 0, Name: Ritardo tra C105 e C106 sui modem B.B, Severity: -255, Value: 1, Description: Delay 16=-255\r\nField Id: 19, Array Id: 0, Name: Indirizzo della UAS per protocollo su seriale, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Abilitazione Trap, Severity: -255, Value: 1, Description: Trap attive=-255\r\nField Id: 21, Array Id: 0, Name: Indirizzo IP del trap receiver 1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 22, Array Id: 0, Name: Indirizzo IP del trap receiver 2, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 23, Array Id: 0, Name: Indirizzo IP del trap receiver 3, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 24, Array Id: 0, Name: Tipo stazione di ingegneria, Severity: -255, Value: 2, Description: Engineering Station Serial=-255\r\nStream Id: 3, Name: Diagnostica apparato, Severity: -255, Description: Connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255;Valore ricevuto=-255;Connesso=-255;Connesso=-255;Connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255;Non connesso=-255\r\nField Id: 0, Array Id: 0, Name: Stato connessione seriale SCADA, Severity: -255, Value: 1, Description: Connesso=-255\r\nField Id: 1, Array Id: 0, Name: Stato connessione linea TCP 104 - 1, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 2, Array Id: 0, Name: Stato connessione linea TCP 104 - 2, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 3, Array Id: 0, Name: Stato connessione linea TCP 104 - 3, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 4, Array Id: 0, Name: Numero di interfacce, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connesso=-255\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connesso=-255\r\nField Id: 7, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connesso=-255\r\nField Id: 8, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 10, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 12, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 13, Array Id: 0, Name: Stato connessione porta #09, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 15, Array Id: 0, Name: Stato connessione porta #11, Severity: -255, Value: 2, Description: Non connesso=-255\r\nField Id: 16, Array Id: 0, Name: Stato connessione porta #12, Severity: -255, Value: 2, Description: Non connesso=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
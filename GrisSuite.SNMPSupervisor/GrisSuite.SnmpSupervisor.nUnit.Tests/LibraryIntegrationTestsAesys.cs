﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsAesys
    {
        #region Test Aesys - definizioni originali

        [Test]
        public void DumpCompleto_definition_DeviceTypeAEL0000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511409");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor 2 marciapiede 5, Type: AEL0000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 1400), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 18 days, 2:55:39.670, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 0, Description: Sistema funzionante e connesso al server=0;Data e ora ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione, Severity: 0, Value: 1, Description: Sistema funzionante e connesso al server=0\r\nField Id: 1, Array Id: 0, Name: Data e ora, Severity: 0, Value: 08/02/2009 01:07:00, Description: Data e ora ricevuta=0\r\nStream Id: 3, Name: Parametri ambientali, Severity: 0, Description: Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 0, Value: 20 (°C), Description: Valore entro i limiti normali=0\r\nStream Id: 4, Name: Informazioni generali, Severity: 0, Description: Produttore riconosciuto=0;1.0.9 / LED-M10=0;Monitor LED 9 righe + 1=0;Locazione dispositivo ricevuta=0;Informazioni ricevute=0\r\nField Id: 0, Array Id: 0, Name: Produttore, Severity: 0, Value: Aesys SpA - Seriate (BG) - ITALY, Description: Produttore riconosciuto=0\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: 0, Value: 1.0.9/ LED-M10, Description: 1.0.9 / LED-M10=0\r\nField Id: 2, Array Id: 0, Name: Categoria dispositivo, Severity: 0, Value: 10, Description: Monitor LED 9 righe + 1=0\r\nField Id: 3, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: n/d, Description: Locazione dispositivo ricevuta=0\r\nField Id: 4, Array Id: 0, Name: Informazioni aggiuntive, Severity: 0, Value: n/d, Description: Informazioni ricevute=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeAET0000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490483");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Atrio Arrivi, Type: AET0000, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 1400), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 1 day 18:26:01.97, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 0, Description: Stato non disponibile=0;Data e ora ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione, Severity: 0, Value: 0, Description: Stato non disponibile=0\r\nField Id: 1, Array Id: 0, Name: Data e ora, Severity: 0, Value: 18/06/2009 10:51:21, Description: Data e ora ricevuta=0\r\nStream Id: 3, Name: Parametri ambientali, Severity: 1, Description: Valore anomalo=1\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 1, Value: 56 (°C), Description: Valore anomalo=1\r\nStream Id: 4, Name: Informazioni generali, Severity: 0, Description: Produttore riconosciuto=0;Versione ricevuta=0;Monitor TFT 32 pollici=0;Locazione dispositivo ricevuta=0;Informazioni ricevute=0\r\nField Id: 0, Array Id: 0, Name: Produttore, Severity: 0, Value: Aesys SpA - Seriate (BG) - ITALY, Description: Produttore riconosciuto=0\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: 0, Value: 1.0.4 / TFT, Description: Versione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Categoria dispositivo, Severity: 0, Value: 0, Description: Monitor TFT 32 pollici=0\r\nField Id: 3, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: n/d, Description: Locazione dispositivo ricevuta=0\r\nField Id: 4, Array Id: 0, Name: Informazioni aggiuntive, Severity: 0, Value: n/d, Description: Informazioni ricevute=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
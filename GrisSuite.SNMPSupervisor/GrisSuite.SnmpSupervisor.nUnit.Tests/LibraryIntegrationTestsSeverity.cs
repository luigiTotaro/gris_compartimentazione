﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsSeverity
    {
        #region Test severity

        [Test]
        public void Severity_definitionTemperatura76_CondizioneErroreSuTemperatura()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511118");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.IsTrue(
                        deviceDump.Contains(
                            "Field Id: 0, Array Id: 0, Name: Temperatura, Severity: 2, Value: 76 (°C), Description: Valore gravemente anomalo=2"));
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definitionTemperaturaMeno127_CondizioneNormaleSuTemperatura()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511119");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.IsTrue(
                        deviceDump.Contains(
                            "Field Id: 0, Array Id: 0, Name: Temperatura, Severity: 0, Value: -127 (°C), Description: Valore entro i limiti normali=0"));
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definitionTemperatura56_CondizioneAllertaSuTemperatura()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511121");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.IsTrue(
                        deviceDump.Contains("Field Id: 0, Array Id: 0, Name: Temperatura, Severity: 1, Value: 56 (°C), Description: Valore anomalo=1"));
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definitionTemperatura128_CondizioneAllertaSuTemperatura()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511122");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.IsTrue(
                        deviceDump.Contains(
                            "Field Id: 0, Array Id: 0, Name: Temperatura, Severity: 1, Value: 128 (°C), Description: Valore fuori scala=1"));
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definitionTemperaturaMeno128_CondizioneErroreSuTemperatura()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511123");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.IsTrue(
                        deviceDump.Contains(
                            "Field Id: 0, Array Id: 0, Name: Temperatura, Severity: 2, Value: -128 (°C), Description: Valore non valido (sensore guasto)=2"));
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
﻿using System;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
	internal class LibraryIntegrationTestsSpecializedDefinitions
	{
		[Test]
		public void DumpCompleto_definitionChiamateWorkstationSimulate_DeviceTypeWRKWIN10_Specializzata()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("2851995650");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
					{
						Assert.AreEqual(
							"Device Name: Workstation simulata 2 con definizione specializzata, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi (specializzato), Severity: 0, Description: Valore ricevuto=0;Spazio libero disco normale=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1,91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 0, Value: 9 %, Description: Spazio libero disco normale=0\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20,00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9,51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670.986.221 / Received: 237.993.236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
							deviceDump);
					}
					else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
					{
						Assert.AreEqual(
							"Device Name: Workstation simulata 2 con definizione specializzata, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi (specializzato), Severity: 0, Description: Valore ricevuto=0;Spazio libero disco normale=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1.91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 0, Value: 9 %, Description: Spazio libero disco normale=0\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20.00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9.51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670,986,221 / Received: 237,993,236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
							deviceDump);
					}
					else
					{
						Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
					}
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void
			DumpCompleto_definitionChiamateWorkstationSimulate_DeviceTypeWRKWIN10_SpecializzataMancante_DefaultADefinizioneStandard
			()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("2851995651");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
					{
						Assert.AreEqual(
							"Device Name: Workstation simulata 3 con definizione specializzata mancante, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi, Severity: 2, Description: Valore ricevuto=0;Spazio libero disco sotto il valore di attenzione=2;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1,91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 2, Value: 9 %, Description: Spazio libero disco sotto il valore di attenzione=2\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20,00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9,51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670.986.221 / Received: 237.993.236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
							deviceDump);
					}
					else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
					{
						Assert.AreEqual(
							"Device Name: Workstation simulata 3 con definizione specializzata mancante, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi, Severity: 2, Description: Valore ricevuto=0;Spazio libero disco sotto il valore di attenzione=2;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1.91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 2, Value: 9 %, Description: Spazio libero disco sotto il valore di attenzione=2\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20.00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9.51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670,986,221 / Received: 237,993,236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
							deviceDump);
					}
					else
					{
						Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
					}
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionChiamateWorkstationSimulate_DeviceTypeWRKWIN10_Specializzata_ConInvioMail()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("2851995652");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);
					Assert.AreEqual(false, device.StreamData[1].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[1].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[2].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
					{
						Assert.AreEqual(
							"Device Name: Workstation simulata 4 con definizione specializzata, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi (specializzato), Severity: 2, Description: Valore ricevuto=0;Spazio libero disco sotto il valore di attenzione=2;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1,91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 2, Value: 9 %, Description: Spazio libero disco sotto il valore di attenzione=2\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20,00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9,51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670.986.221 / Received: 237.993.236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
							deviceDump);
					}
					else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
					{
						Assert.AreEqual(
							"Device Name: Workstation simulata 4 con definizione specializzata, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi (specializzato), Severity: 2, Description: Valore ricevuto=0;Spazio libero disco sotto il valore di attenzione=2;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1.91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 2, Value: 9 %, Description: Spazio libero disco sotto il valore di attenzione=2\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20.00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9.51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670,986,221 / Received: 237,993,236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
							deviceDump);
					}
					else
					{
						Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
					}
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionPATTON3201_000_1_Ok_Specializzata()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493461");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Patton 3201 Rastignano specializzato, Type: PATTON3201, Community: N3T2010, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: 4505020003205316611_PATTON3201_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: not set, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Pt1-T-S.RUFF-RASTIG, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 22:18:59.300, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;Non attiva=0;Non attiva=0;Ok=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.143.19, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP Primario, Severity: -255, Value: 10.102.143.19, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP Secondario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Maschera di rete ethernet, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.143.201, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione BUN, Severity: -255, Value: 100, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione link DSL, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 10, Array Id: 0, Name: Perdita di segnale DSL, Severity: 0, Value: 0, Description: Non attiva=0\r\nField Id: 11, Array Id: 0, Name: Perdita sync word DSL, Severity: 0, Value: 0, Description: Non attiva=0\r\nField Id: 12, Array Id: 0, Name: Condizione linea DSL, Severity: 0, Value: 1, Description: Ok=0\r\nField Id: 13, Array Id: 0, Name: Livello di segnale linea DSL, Severity: -255, Value: 43, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Attenuazione livello di segnale all'altro capo, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: G.HS data rate, Severity: -255, Value: 65, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte switch, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;Non disponibile=-255;Attiva=-255;Sconosciuta=-255;Valore ricevuto=-255;Valore ricevuto=-255;ATM=-255;Non disponibile=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;RFC1483=-255;Non disponibile=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 21, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: CX28975 device, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 4, Description: Sconosciuta=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 00:00:01.720, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: BUN Utopia device, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 37, Description: ATM=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 0 giorni, 00:00:05.150, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: bridge, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 0 giorni, 00:00:07.150, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: BUN Ethernet device, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 0 giorni, 00:00:05.170, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: ppp channel 1, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: ppp channel 2, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Nome porta #07, Severity: -255, Value: ppp channel 3, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Tipo porta #07, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 39, Array Id: 0, Name: Velocità porta #07, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Stato attivazione porta #07, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 42, Array Id: 0, Name: Ultima modifica porta #07, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Nome porta #08, Severity: -255, Value: ppp channel 4, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Tipo porta #08, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 45, Array Id: 0, Name: Velocità porta #08, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 46, Array Id: 0, Name: Stato attivazione porta #08, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 47, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 48, Array Id: 0, Name: Ultima modifica porta #08, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome porta #09, Severity: -255, Value: ppp channel 5, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Tipo porta #09, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 51, Array Id: 0, Name: Velocità porta #09, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Stato attivazione porta #09, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 53, Array Id: 0, Name: Stato connessione porta #09, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 54, Array Id: 0, Name: Ultima modifica porta #09, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: ppp channel 6, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #10, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #11, Severity: -255, Value: ppp channel 7, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #11, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #11, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #11, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #11, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 66, Array Id: 0, Name: Ultima modifica porta #11, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 67, Array Id: 0, Name: Nome porta #12, Severity: -255, Value: ppp channel 8, Description: Valore ricevuto=-255\r\nField Id: 68, Array Id: 0, Name: Tipo porta #12, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 69, Array Id: 0, Name: Velocità porta #12, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Stato attivazione porta #12, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 71, Array Id: 0, Name: Stato connessione porta #12, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 72, Array Id: 0, Name: Ultima modifica porta #12, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 73, Array Id: 0, Name: Nome porta #13, Severity: -255, Value: ppp1, Description: Valore ricevuto=-255\r\nField Id: 74, Array Id: 0, Name: Tipo porta #13, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 75, Array Id: 0, Name: Velocità porta #13, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 76, Array Id: 0, Name: Stato attivazione porta #13, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 77, Array Id: 0, Name: Stato connessione porta #13, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 78, Array Id: 0, Name: Ultima modifica porta #13, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Nome porta #14, Severity: -255, Value: ppp2, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Tipo porta #14, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 81, Array Id: 0, Name: Velocità porta #14, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 82, Array Id: 0, Name: Stato attivazione porta #14, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 83, Array Id: 0, Name: Stato connessione porta #14, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 84, Array Id: 0, Name: Ultima modifica porta #14, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 85, Array Id: 0, Name: Nome porta #15, Severity: -255, Value: ppp3, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta #15, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta #15, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta #15, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta #15, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta #15, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 91, Array Id: 0, Name: Nome porta #16, Severity: -255, Value: ppp4, Description: Valore ricevuto=-255\r\nField Id: 92, Array Id: 0, Name: Tipo porta #16, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 93, Array Id: 0, Name: Velocità porta #16, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 94, Array Id: 0, Name: Stato attivazione porta #16, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 95, Array Id: 0, Name: Stato connessione porta #16, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 96, Array Id: 0, Name: Ultima modifica porta #16, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 97, Array Id: 0, Name: Nome porta #17, Severity: -255, Value: ppp5, Description: Valore ricevuto=-255\r\nField Id: 98, Array Id: 0, Name: Tipo porta #17, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 99, Array Id: 0, Name: Velocità porta #17, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 100, Array Id: 0, Name: Stato attivazione porta #17, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 101, Array Id: 0, Name: Stato connessione porta #17, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 102, Array Id: 0, Name: Ultima modifica porta #17, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 103, Array Id: 0, Name: Nome porta #18, Severity: -255, Value: ppp6, Description: Valore ricevuto=-255\r\nField Id: 104, Array Id: 0, Name: Tipo porta #18, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 105, Array Id: 0, Name: Velocità porta #18, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 106, Array Id: 0, Name: Stato attivazione porta #18, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 107, Array Id: 0, Name: Stato connessione porta #18, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 108, Array Id: 0, Name: Ultima modifica porta #18, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 109, Array Id: 0, Name: Nome porta #19, Severity: -255, Value: ppp7, Description: Valore ricevuto=-255\r\nField Id: 110, Array Id: 0, Name: Tipo porta #19, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 111, Array Id: 0, Name: Velocità porta #19, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 112, Array Id: 0, Name: Stato attivazione porta #19, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 113, Array Id: 0, Name: Stato connessione porta #19, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 114, Array Id: 0, Name: Ultima modifica porta #19, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 115, Array Id: 0, Name: Nome porta #20, Severity: -255, Value: ppp8, Description: Valore ricevuto=-255\r\nField Id: 116, Array Id: 0, Name: Tipo porta #20, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 117, Array Id: 0, Name: Velocità porta #20, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 118, Array Id: 0, Name: Stato attivazione porta #20, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 119, Array Id: 0, Name: Stato connessione porta #20, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 120, Array Id: 0, Name: Ultima modifica porta #20, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 127, Array Id: 0, Name: Nome porta #22, Severity: -255, Value: BUN RFC1483 device, Description: Valore ricevuto=-255\r\nField Id: 128, Array Id: 0, Name: Tipo porta #22, Severity: -255, Value: 159, Description: RFC1483=-255\r\nField Id: 129, Array Id: 0, Name: Velocità porta #22, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 130, Array Id: 0, Name: Stato attivazione porta #22, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 131, Array Id: 0, Name: Stato connessione porta #22, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 132, Array Id: 0, Name: Ultima modifica porta #22, Severity: -255, Value: 0 giorni, 00:00:07.130, Description: Valore ricevuto=-255\r\n",
						deviceDump);
					Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionPATTON3201_000_2_Ok_SpecializzataMancante_DefaultADefinizioneStandard()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493462");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Warning, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Patton 3201 Rastignano specializzato senza definizione, Type: PATTON3201, Community: N3T2010, SNMP Version: V1, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: PATTON3201_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: not set, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Pt1-T-S.RUFF-RASTIG, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 22:18:59.300, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 1, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non connesso=1;Non attiva=0;Non attiva=0;Ok=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.143.19, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP Primario, Severity: -255, Value: 10.102.143.19, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP Secondario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Maschera di rete ethernet, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.143.201, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione BUN, Severity: -255, Value: 100, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione link DSL, Severity: 1, Value: 0, Description: Non connesso=1\r\nField Id: 10, Array Id: 0, Name: Perdita di segnale DSL, Severity: 0, Value: 0, Description: Non attiva=0\r\nField Id: 11, Array Id: 0, Name: Perdita sync word DSL, Severity: 0, Value: 0, Description: Non attiva=0\r\nField Id: 12, Array Id: 0, Name: Condizione linea DSL, Severity: 0, Value: 1, Description: Ok=0\r\nField Id: 13, Array Id: 0, Name: Livello di segnale linea DSL, Severity: -255, Value: 43, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Attenuazione livello di segnale all'altro capo, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: G.HS data rate, Severity: -255, Value: 65, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte switch, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;Non disponibile=-255;Attiva=-255;Sconosciuta=-255;Valore ricevuto=-255;Valore ricevuto=-255;ATM=-255;Non disponibile=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;RFC1483=-255;Non disponibile=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 21, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: CX28975 device, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 4, Description: Sconosciuta=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 00:00:01.720, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: BUN Utopia device, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 37, Description: ATM=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 0 giorni, 00:00:05.150, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: bridge, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 0 giorni, 00:00:07.150, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: BUN Ethernet device, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 0 giorni, 00:00:05.170, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: ppp channel 1, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: ppp channel 2, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Nome porta #07, Severity: -255, Value: ppp channel 3, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Tipo porta #07, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 39, Array Id: 0, Name: Velocità porta #07, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Stato attivazione porta #07, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 42, Array Id: 0, Name: Ultima modifica porta #07, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Nome porta #08, Severity: -255, Value: ppp channel 4, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Tipo porta #08, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 45, Array Id: 0, Name: Velocità porta #08, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 46, Array Id: 0, Name: Stato attivazione porta #08, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 47, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 48, Array Id: 0, Name: Ultima modifica porta #08, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome porta #09, Severity: -255, Value: ppp channel 5, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Tipo porta #09, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 51, Array Id: 0, Name: Velocità porta #09, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Stato attivazione porta #09, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 53, Array Id: 0, Name: Stato connessione porta #09, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 54, Array Id: 0, Name: Ultima modifica porta #09, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: ppp channel 6, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #10, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #11, Severity: -255, Value: ppp channel 7, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #11, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #11, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #11, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #11, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 66, Array Id: 0, Name: Ultima modifica porta #11, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 67, Array Id: 0, Name: Nome porta #12, Severity: -255, Value: ppp channel 8, Description: Valore ricevuto=-255\r\nField Id: 68, Array Id: 0, Name: Tipo porta #12, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 69, Array Id: 0, Name: Velocità porta #12, Severity: -255, Value: 7680000, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Stato attivazione porta #12, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 71, Array Id: 0, Name: Stato connessione porta #12, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 72, Array Id: 0, Name: Ultima modifica porta #12, Severity: -255, Value: 0 giorni, 00:00:00.010, Description: Valore ricevuto=-255\r\nField Id: 73, Array Id: 0, Name: Nome porta #13, Severity: -255, Value: ppp1, Description: Valore ricevuto=-255\r\nField Id: 74, Array Id: 0, Name: Tipo porta #13, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 75, Array Id: 0, Name: Velocità porta #13, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 76, Array Id: 0, Name: Stato attivazione porta #13, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 77, Array Id: 0, Name: Stato connessione porta #13, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 78, Array Id: 0, Name: Ultima modifica porta #13, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Nome porta #14, Severity: -255, Value: ppp2, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Tipo porta #14, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 81, Array Id: 0, Name: Velocità porta #14, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 82, Array Id: 0, Name: Stato attivazione porta #14, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 83, Array Id: 0, Name: Stato connessione porta #14, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 84, Array Id: 0, Name: Ultima modifica porta #14, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 85, Array Id: 0, Name: Nome porta #15, Severity: -255, Value: ppp3, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta #15, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta #15, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta #15, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta #15, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta #15, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 91, Array Id: 0, Name: Nome porta #16, Severity: -255, Value: ppp4, Description: Valore ricevuto=-255\r\nField Id: 92, Array Id: 0, Name: Tipo porta #16, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 93, Array Id: 0, Name: Velocità porta #16, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 94, Array Id: 0, Name: Stato attivazione porta #16, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 95, Array Id: 0, Name: Stato connessione porta #16, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 96, Array Id: 0, Name: Ultima modifica porta #16, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 97, Array Id: 0, Name: Nome porta #17, Severity: -255, Value: ppp5, Description: Valore ricevuto=-255\r\nField Id: 98, Array Id: 0, Name: Tipo porta #17, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 99, Array Id: 0, Name: Velocità porta #17, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 100, Array Id: 0, Name: Stato attivazione porta #17, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 101, Array Id: 0, Name: Stato connessione porta #17, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 102, Array Id: 0, Name: Ultima modifica porta #17, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 103, Array Id: 0, Name: Nome porta #18, Severity: -255, Value: ppp6, Description: Valore ricevuto=-255\r\nField Id: 104, Array Id: 0, Name: Tipo porta #18, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 105, Array Id: 0, Name: Velocità porta #18, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 106, Array Id: 0, Name: Stato attivazione porta #18, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 107, Array Id: 0, Name: Stato connessione porta #18, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 108, Array Id: 0, Name: Ultima modifica porta #18, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 109, Array Id: 0, Name: Nome porta #19, Severity: -255, Value: ppp7, Description: Valore ricevuto=-255\r\nField Id: 110, Array Id: 0, Name: Tipo porta #19, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 111, Array Id: 0, Name: Velocità porta #19, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 112, Array Id: 0, Name: Stato attivazione porta #19, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 113, Array Id: 0, Name: Stato connessione porta #19, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 114, Array Id: 0, Name: Ultima modifica porta #19, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 115, Array Id: 0, Name: Nome porta #20, Severity: -255, Value: ppp8, Description: Valore ricevuto=-255\r\nField Id: 116, Array Id: 0, Name: Tipo porta #20, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 117, Array Id: 0, Name: Velocità porta #20, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 118, Array Id: 0, Name: Stato attivazione porta #20, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 119, Array Id: 0, Name: Stato connessione porta #20, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 120, Array Id: 0, Name: Ultima modifica porta #20, Severity: -255, Value: 0 giorni, 00:00:05.300, Description: Valore ricevuto=-255\r\nField Id: 127, Array Id: 0, Name: Nome porta #22, Severity: -255, Value: BUN RFC1483 device, Description: Valore ricevuto=-255\r\nField Id: 128, Array Id: 0, Name: Tipo porta #22, Severity: -255, Value: 159, Description: RFC1483=-255\r\nField Id: 129, Array Id: 0, Name: Velocità porta #22, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 130, Array Id: 0, Name: Stato attivazione porta #22, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 131, Array Id: 0, Name: Stato connessione porta #22, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 132, Array Id: 0, Name: Ultima modifica porta #22, Severity: -255, Value: 0 giorni, 00:00:07.130, Description: Valore ricevuto=-255\r\n",
						deviceDump);
					Assert.AreEqual("Anomalia lieve", device.ValueDescriptionComplete);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_Invio_Email()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);
					Assert.AreEqual(true, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloPeriferica()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
				Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
					Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);
					Assert.AreEqual(false, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 9, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloStream_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, null);
				Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(true, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloStream_2()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(5, null);
				Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(false, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloStreamField_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(5, 0);
				deviceAcknowledgement.AddDeviceAcknowledgement(5, 1);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(2, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(false, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloStreamField_2()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(5, 1);
				deviceAcknowledgement.AddDeviceAcknowledgement(5, 2);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(2, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(true, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloStreamField_3()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 10);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 11);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(2, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Error, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(true, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloStreamField_4()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(5, 0);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 5);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 10);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 15);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(4, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(false, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloStream_e_StreamField_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(5, null);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 5);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 10);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 15);
				Assert.AreEqual(1, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(3, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(false, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_Periferica_INFSTAZ_Aesys_InErrore_TacitataALivelloDevice_e_StreamField_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287679");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 10);
				deviceAcknowledgement.AddDeviceAcknowledgement(14, 11);
				Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(2, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(false, device.StreamData[4].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[6].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[11].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[10].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[12].FieldData[15].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: Monitor Infostazioni Aesys 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 9, ReplyICMP: True, ReplySNMP: True, Device Category: 0, Device with dynamic loading rules enabled\r\nDynamic Definition File: 197326959625437187_INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19440832, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Monitor LCD/TFT 32 pollici=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: TFT32, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 76534, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.14.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: (no firmware), Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria (specializzata), Severity: 0, Value: 0, Description: Monitor LCD/TFT 32 pollici=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F1-0E, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.191, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.254.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 127.0.0.1, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/04/2005 16:11:52, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 11/04/2005 10:18:39, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 26, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 2, Description: In esecuzione ma non connesso=2;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 69, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 33, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 34, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Sconosciuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 5, Description: Sconosciuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Lampada di retroilluminazione TFT, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 2, Description: Valore ricevuto=0;p5V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p12V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2;p24V=0;Guasto=2;Valore ricevuto=0;Valore ricevuto=0;Allarme grave=2\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 2, Value: 4, Description: Allarme grave=2\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 4, Description: p24V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 2, Value: 2, Description: Guasto=2\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 24V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 2, Value: 4, Description: Allarme grave=2\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP66000_InvioMailSuNonRispostaSNMP_NoDataAvailableForcedState_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034937");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Warning, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(true, device.StreamData[1].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[1].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[1].FieldData[2].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[3].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[4].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: cns-4-10-ata-1 ATA FXS 2 linee (Telefin - telefonia VoIP) specializzata, Type: VP66000, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-10-ata-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-10-ata-1 2.6.33 #1 Mon Feb 4 11:32:46 CET 2013 armv5tejl, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 04:09:25.370, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf), Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Unknown (configure /etc/snmp/snmpd.local.conf), Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 1, Description: Non in esecuzione=1;Non in esecuzione=1;Non in esecuzione=-255;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato processo 'sshd', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 1, Array Id: 0, Name: Stato processo 'ntp', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 2, Array Id: 0, Name: Stato processo 'crond', Severity: -255, Value: 0, Description: Non in esecuzione=-255\r\nField Id: 3, Array Id: 0, Name: Stato processo 'cherokee', Severity: 0, Value: cherokee, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato processo 'asterisk', Severity: 0, Value: asterisk, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato processo 'monit', Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP66000_InvioMailSuNonRispostaSNMP_NoDataAvailableForcedState_Tacitata_1
			()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034937");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(2, 0);
				deviceAcknowledgement.AddDeviceAcknowledgement(2, 1);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(2, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(false, device.StreamData[1].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.StreamData[1].FieldData[2].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[3].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[4].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: cns-4-10-ata-1 ATA FXS 2 linee (Telefin - telefonia VoIP) specializzata, Type: VP66000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-10-ata-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-10-ata-1 2.6.33 #1 Mon Feb 4 11:32:46 CET 2013 armv5tejl, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 04:09:25.370, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf), Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Unknown (configure /etc/snmp/snmpd.local.conf), Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 0, Description: Non in esecuzione=1;Non in esecuzione=1;Non in esecuzione=-255;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato processo 'sshd', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 1, Array Id: 0, Name: Stato processo 'ntp', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 2, Array Id: 0, Name: Stato processo 'crond', Severity: -255, Value: 0, Description: Non in esecuzione=-255\r\nField Id: 3, Array Id: 0, Name: Stato processo 'cherokee', Severity: 0, Value: cherokee, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato processo 'asterisk', Severity: 0, Value: asterisk, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato processo 'monit', Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP66000_InvioMailSuNonRispostaSNMP_NoDataAvailableForcedState_Tacitata_2
			()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034937");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(2, 1);
				deviceAcknowledgement.AddDeviceAcknowledgement(2, 2);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(2, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Warning, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(true, device.StreamData[1].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[2].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[3].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[4].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(true, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: cns-4-10-ata-1 ATA FXS 2 linee (Telefin - telefonia VoIP) specializzata, Type: VP66000, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-10-ata-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-10-ata-1 2.6.33 #1 Mon Feb 4 11:32:46 CET 2013 armv5tejl, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 04:09:25.370, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf), Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Unknown (configure /etc/snmp/snmpd.local.conf), Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 1, Description: Non in esecuzione=1;Non in esecuzione=1;Non in esecuzione=-255;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato processo 'sshd', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 1, Array Id: 0, Name: Stato processo 'ntp', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 2, Array Id: 0, Name: Stato processo 'crond', Severity: -255, Value: 0, Description: Non in esecuzione=-255\r\nField Id: 3, Array Id: 0, Name: Stato processo 'cherokee', Severity: 0, Value: cherokee, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato processo 'asterisk', Severity: 0, Value: asterisk, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato processo 'monit', Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP66000_InvioMailSuNonRispostaSNMP_NoDataAvailableForcedState_Tacitata_3
			()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034937");

			if (deviceObject != null)
			{
				DeviceAcknowledgement deviceAcknowledgement = new DeviceAcknowledgement(deviceObject.DeviceId);
				Assert.AreEqual(false, deviceAcknowledgement.IsDeviceAcknowledged);
				deviceAcknowledgement.AddDeviceAcknowledgement(null, null);
				Assert.AreEqual(true, deviceAcknowledgement.IsDeviceAcknowledged);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamAcknowledgements.Count);
				Assert.AreEqual(0, deviceAcknowledgement.DeviceStreamFieldAcknowledgements.Count);

				deviceObject.DeviceAck = deviceAcknowledgement;

				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.MaintenanceMode, device.SeverityLevel);
					Assert.AreEqual(true, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00SPEC", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(false, device.StreamData[1].FieldData[0].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[1].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[2].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[3].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[4].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.StreamData[1].FieldData[5].ShouldSendNotificationByEmail);
					Assert.AreEqual(false, device.ShouldSendNotificationByEmail);

					Assert.AreEqual(
						"Device Name: cns-4-10-ata-1 ATA FXS 2 linee (Telefin - telefonia VoIP) specializzata, Type: VP66000, Community: public, SNMP Version: V2, Offline: 0, Severity: 9, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-10-ata-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-10-ata-1 2.6.33 #1 Mon Feb 4 11:32:46 CET 2013 armv5tejl, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 04:09:25.370, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf), Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Unknown (configure /etc/snmp/snmpd.local.conf), Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 1, Description: Non in esecuzione=1;Non in esecuzione=1;Non in esecuzione=-255;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato processo 'sshd', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 1, Array Id: 0, Name: Stato processo 'ntp', Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 2, Array Id: 0, Name: Stato processo 'crond', Severity: -255, Value: 0, Description: Non in esecuzione=-255\r\nField Id: 3, Array Id: 0, Name: Stato processo 'cherokee', Severity: 0, Value: cherokee, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato processo 'asterisk', Severity: 0, Value: asterisk, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato processo 'monit', Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}
	}
}
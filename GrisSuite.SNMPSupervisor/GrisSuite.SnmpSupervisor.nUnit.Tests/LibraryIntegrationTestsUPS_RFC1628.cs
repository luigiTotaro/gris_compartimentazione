﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsUPS_RFC1628
    {
        [Test]
        public void DumpCompleto_definitionRFC1628_1()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174477291");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: UPS RFC1628 (1), Type: UPSRFC1628, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=-255;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: NetMan 100 plus, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 20 giorni, 00:21:27.760, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Informazioni generali, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Produttore apparato, Severity: -255, Value: RPS SpA, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Modello apparato, Severity: -255, Value: UOD1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione software, Severity: -255, Value: SWM035-01-06, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione agente software, Severity: -255, Value: NetMan 100 plus, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Identificativo apparato, Severity: -255, Value: NetMan plus, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Identificativo dispositivi connessi, Severity: -255, Value: ???, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni batteria, Severity: 0, Description: Batteria normale=0;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Stato batteria, Severity: 0, Value: 2, Description: Batteria normale=0\r\nField Id: 1, Array Id: 0, Name: Minuti autonomia residua, Severity: -255, Value: 118, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Parametri linea ingresso, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Tensione in ingresso #01, Severity: -255, Value: 232 V, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Corrente in ingresso #01, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in ingresso #02, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in ingresso #02, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Tensione in ingresso #03, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Corrente in ingresso #03, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nStream Id: 5, Name: Parametri linea uscita, Severity: 0, Description: Normale=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Sorgente output, Severity: 0, Value: 3, Description: Normale=0\r\nField Id: 1, Array Id: 0, Name: Frequenza in uscita, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita #01, Severity: -255, Value: 231 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in uscita #01, Severity: -255, Value: 0.6 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Percentuale carico in uscita #01, Severity: -255, Value: 7 %, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Tensione in uscita #02, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Corrente in uscita #02, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Percentuale carico in uscita #02, Severity: -255, Value: 0 %, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita #03, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita #03, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Percentuale carico in uscita #03, Severity: -255, Value: 0 %, Description: Valore ricevuto=-255\r\nStream Id: 6, Name: Parametri bypass, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Frequenza di bypass, Severity: -255, Value: 49.9 Hz, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tensione di bypass #01, Severity: -255, Value: 232 V, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione di bypass #02, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Tensione di bypass #03, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nStream Id: 7, Name: Informazioni allarmi, Severity: -255, Description: Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero di allarmi attivi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionRFC1628_2()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174473684");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: UPS RFC1628 (2), Type: UPSRFC1628, Community: LiebertEM, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: UPS Agent, Description: Nome ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: NET Agent II, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 67 giorni, 18:30:22.000, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Administrator, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: My Office, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni generali, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Produttore apparato, Severity: -255, Value: Emerson Network Power, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Modello apparato, Severity: -255, Value: LiebertPSI, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione software, Severity: -255, Value: EX00127P, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione agente software, Severity: -255, Value: 2.40.CP504E.ABE.22, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Identificativo apparato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Identificativo dispositivi connessi, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni batteria, Severity: 2, Description: Batteria esaurita=2;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Stato batteria, Severity: 2, Value: 4, Description: Batteria esaurita=2\r\nField Id: 1, Array Id: 0, Name: Minuti autonomia residua, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Parametri linea ingresso, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Tensione in ingresso #01, Severity: -255, Value: 227 V, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Corrente in ingresso #01, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nStream Id: 5, Name: Parametri linea uscita, Severity: 0, Description: Normale=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Sorgente output, Severity: 0, Value: 3, Description: Normale=0\r\nField Id: 1, Array Id: 0, Name: Frequenza in uscita, Severity: -255, Value: 49.9 Hz, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita #01, Severity: -255, Value: 230 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in uscita #01, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Percentuale carico in uscita #01, Severity: -255, Value: 3 %, Description: Valore ricevuto=-255\r\nStream Id: 6, Name: Parametri bypass, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Frequenza di bypass, Severity: -255, Value: 0 Hz, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tensione di bypass #01, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nStream Id: 7, Name: Informazioni allarmi, Severity: -255, Description: Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero di allarmi attivi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionRFC1628_3()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174479170");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: UPS RFC1628 (3), Type: UPSRFC1628, Community: LiebertEM, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Uninitialized, Description: Nome ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Uninitialized, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 7 giorni, 05:08:13.590, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Uninitialized, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Uninitialized, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni generali, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Produttore apparato, Severity: -255, Value: Emerson Network Power, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Modello apparato, Severity: -255, Value: NXC, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione software, Severity: -255, Value: Not Supported, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione agente software, Severity: -255, Value: 5.280.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Identificativo apparato, Severity: -255, Value: Uninitialized, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Identificativo dispositivi connessi, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni batteria, Severity: 0, Description: Batteria normale=0;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Stato batteria, Severity: 0, Value: 2, Description: Batteria normale=0\r\nField Id: 1, Array Id: 0, Name: Minuti autonomia residua, Severity: -255, Value: 207, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Parametri linea ingresso, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Tensione in ingresso #01, Severity: -255, Value: 237 V, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Corrente in ingresso #01, Severity: -255, Value: 2 A, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in ingresso #02, Severity: -255, Value: 236 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in ingresso #02, Severity: -255, Value: 3 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Tensione in ingresso #03, Severity: -255, Value: 239 V, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Corrente in ingresso #03, Severity: -255, Value: 1 A, Description: Valore ricevuto=-255\r\nStream Id: 5, Name: Parametri linea uscita, Severity: 0, Description: Normale=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Sorgente output, Severity: 0, Value: 3, Description: Normale=0\r\nField Id: 1, Array Id: 0, Name: Frequenza in uscita, Severity: -255, Value: 49.9 Hz, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita #01, Severity: -255, Value: 229 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in uscita #01, Severity: -255, Value: 4 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Percentuale carico in uscita #01, Severity: -255, Value: 15 %, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Tensione in uscita #02, Severity: -255, Value: 229 V, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Corrente in uscita #02, Severity: -255, Value: 4 A, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Percentuale carico in uscita #02, Severity: -255, Value: 15 %, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita #03, Severity: -255, Value: 232 V, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita #03, Severity: -255, Value: 1 A, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Percentuale carico in uscita #03, Severity: -255, Value: 3 %, Description: Valore ricevuto=-255\r\nStream Id: 6, Name: Parametri bypass, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Frequenza di bypass, Severity: -255, Value: 49.9 Hz, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tensione di bypass #01, Severity: -255, Value: 236 V, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione di bypass #02, Severity: -255, Value: 236 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Tensione di bypass #03, Severity: -255, Value: 238 V, Description: Valore ricevuto=-255\r\nStream Id: 7, Name: Informazioni allarmi, Severity: -255, Description: Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero di allarmi attivi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionRFC1628_4()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511194");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: UPS RFC1628 (4), Type: UPSRFC1628, Community: LiebertEM, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: UPS Agent, Description: Nome ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: NET Agent II, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 338 giorni, 21:47:03.000, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Administrator, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: My Office, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni generali, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Produttore apparato, Severity: -255, Value: Emerson Network Power, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Modello apparato, Severity: -255, Value: LiebertPSI, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione software, Severity: -255, Value: EX00117P, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione agente software, Severity: -255, Value: 2.40.CP504E.ABE.22, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Identificativo apparato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Identificativo dispositivi connessi, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni batteria, Severity: 0, Description: Batteria normale=0;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Stato batteria, Severity: 0, Value: 2, Description: Batteria normale=0\r\nField Id: 1, Array Id: 0, Name: Minuti autonomia residua, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Parametri linea ingresso, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Tensione in ingresso #01, Severity: -255, Value: 225 V, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Corrente in ingresso #01, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nStream Id: 5, Name: Parametri linea uscita, Severity: 0, Description: Normale=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Sorgente output, Severity: 0, Value: 3, Description: Normale=0\r\nField Id: 1, Array Id: 0, Name: Frequenza in uscita, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita #01, Severity: -255, Value: 228 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in uscita #01, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Percentuale carico in uscita #01, Severity: -255, Value: 5 %, Description: Valore ricevuto=-255\r\nStream Id: 6, Name: Parametri bypass, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Frequenza di bypass, Severity: -255, Value: 0 Hz, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tensione di bypass #01, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nStream Id: 7, Name: Informazioni allarmi, Severity: -255, Description: Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero di allarmi attivi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionRFC1628_5()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174512567");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: UPS RFC1628 (5), Type: UPSRFC1628, Community: LiebertEM, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Uninitialized, Description: Nome ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Uninitialized, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 5 giorni, 21:14:36.990, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Uninitialized, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Uninitialized, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni generali, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Produttore apparato, Severity: -255, Value: Emerson Network Power, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Modello apparato, Severity: -255, Value: NX, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione software, Severity: -255, Value: Not Supported, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione agente software, Severity: -255, Value: 5.280.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Identificativo apparato, Severity: -255, Value: Uninitialized, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Identificativo dispositivi connessi, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni batteria, Severity: 0, Description: Batteria normale=0;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Stato batteria, Severity: 0, Value: 2, Description: Batteria normale=0\r\nField Id: 1, Array Id: 0, Name: Minuti autonomia residua, Severity: -255, Value: 167, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Parametri linea ingresso, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Tensione in ingresso #01, Severity: -255, Value: 231 V, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Corrente in ingresso #01, Severity: -255, Value: 5 A, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in ingresso #02, Severity: -255, Value: 231 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in ingresso #02, Severity: -255, Value: 5 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Tensione in ingresso #03, Severity: -255, Value: 233 V, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Corrente in ingresso #03, Severity: -255, Value: 3 A, Description: Valore ricevuto=-255\r\nStream Id: 5, Name: Parametri linea uscita, Severity: 0, Description: Normale=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Sorgente output, Severity: 0, Value: 3, Description: Normale=0\r\nField Id: 1, Array Id: 0, Name: Frequenza in uscita, Severity: -255, Value: 49.9 Hz, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita #01, Severity: -255, Value: 231 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in uscita #01, Severity: -255, Value: 6 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Percentuale carico in uscita #01, Severity: -255, Value: 15 %, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Tensione in uscita #02, Severity: -255, Value: 231 V, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Corrente in uscita #02, Severity: -255, Value: 6 A, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Percentuale carico in uscita #02, Severity: -255, Value: 15 %, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita #03, Severity: -255, Value: 230 V, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita #03, Severity: -255, Value: 5 A, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Percentuale carico in uscita #03, Severity: -255, Value: 12 %, Description: Valore ricevuto=-255\r\nStream Id: 6, Name: Parametri bypass, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Frequenza di bypass, Severity: -255, Value: 49.9 Hz, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tensione di bypass #01, Severity: -255, Value: 232 V, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione di bypass #02, Severity: -255, Value: 232 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Tensione di bypass #03, Severity: -255, Value: 233 V, Description: Valore ricevuto=-255\r\nStream Id: 7, Name: Informazioni allarmi, Severity: -255, Description: Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero di allarmi attivi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionRFC1628_6()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174488812");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: UPS RFC1628 (6), Type: UPSRFC1628, Community: LiebertEM, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: UPS Agent, Description: Nome ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: NET Agent II, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 116 giorni, 00:26:15.000, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Administrator, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: My Office, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni generali, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Produttore apparato, Severity: -255, Value: Emerson Network Power, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Modello apparato, Severity: -255, Value: LiebertPSI, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione software, Severity: -255, Value: EX00127P, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione agente software, Severity: -255, Value: 2.40.CP504E.ABE.22, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Identificativo apparato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Identificativo dispositivi connessi, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni batteria, Severity: 0, Description: Batteria normale=0;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Stato batteria, Severity: 0, Value: 2, Description: Batteria normale=0\r\nField Id: 1, Array Id: 0, Name: Minuti autonomia residua, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Parametri linea ingresso, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Tensione in ingresso #01, Severity: -255, Value: 233 V, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Corrente in ingresso #01, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nStream Id: 5, Name: Parametri linea uscita, Severity: 0, Description: Normale=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Sorgente output, Severity: 0, Value: 3, Description: Normale=0\r\nField Id: 1, Array Id: 0, Name: Frequenza in uscita, Severity: -255, Value: 49.9 Hz, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita #01, Severity: -255, Value: 235 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Corrente in uscita #01, Severity: -255, Value: 0 A, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Percentuale carico in uscita #01, Severity: -255, Value: 3 %, Description: Valore ricevuto=-255\r\nStream Id: 6, Name: Parametri bypass, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Frequenza di bypass, Severity: -255, Value: 0 Hz, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tensione di bypass #01, Severity: -255, Value: 0 V, Description: Valore ricevuto=-255\r\nStream Id: 7, Name: Informazioni allarmi, Severity: -255, Description: Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero di allarmi attivi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }
    }
}
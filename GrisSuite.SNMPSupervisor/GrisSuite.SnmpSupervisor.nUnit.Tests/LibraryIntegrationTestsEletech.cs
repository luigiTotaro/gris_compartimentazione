﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsEletech
    {
        #region Test Web Radio

        [Test]
        public void DumpCompleto_definitionEletech_WebRadio_1_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493266");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Web Radio Casalecchio Garibaldi, Type: ETRADIO100, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: B100114\r\nDynamic Definition File: ETRADIO100.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: SnmpV2 Eletech S.r.l., Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 00:03:20.000, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione MIB, Severity: -255, Value: v1.00, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome del Costruttore del dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Modello del dispositivo, Severity: -255, Value: Radio Insertion, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Numero Seriale del dispositivo, Severity: -255, Value: B100114, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Versione del Software del dispositivo, Severity: -255, Value: 02.10 13/08/2009, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione del firmware del dispositivo, Severity: -255, Value: v1.1, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Categoria di appartenenza del dispositivo, Severity: -255, Value: Internet Radio, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome della locazione del dispositivo o identificativo, Severity: -255, Value: ELETECH_CASALEC, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni rete, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.105.76.1, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Gateway, Severity: -255, Value: 10.105.76.201, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: DNS Primario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: DNS Secondario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Porta Web Server, Severity: -255, Value: 80, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni periferica, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: URL Primario, Severity: -255, Value: rtp://@224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: URL Secondario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: URL Terziario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Periodo per il check dello stream, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Limite al periodo per il check dello stream, Severity: -255, Value: 180, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: RTP Buffer in corso, Severity: -255, Value: 8000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Buffer in corso, Severity: -255, Value: 5436, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Priorità dello streaming in esecuzione, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: URL in esecuzione, Severity: -255, Value: rtp://@224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: BitRate dello stream in corso, Severity: -255, Value: 56, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Byte Played, Severity: -255, Value: 3584317134, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Contatore di Frame persi, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Contatore di errori software, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Riconnessioni, Severity: -255, Value: 117946, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Volume, Severity: -255, Value: 65, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Contatore messaggi bufferizzati, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Contatore messaggi riprodotti, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionEletech_WebRadio_Offline()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493267");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");
                    Assert.IsFalse(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    // Visto che la device è offline, la discovery non può funzionare. Il tipo resta quello base e la device
                    // va in stato di errore e messaggio forzato, perché non risponde (in base al parametro ForcedSeverityAndDescriptionWhenOffline).
                    // Tutte le definizioni Eletech (Web Radio) hanno la forzatura in caso di offline, ma non subentrerà mai,
                    // perché la discovery non può avvenire, quindi non esistono test specifici.
                    Assert.AreEqual(
                        "Device Name: Web Radio Offline, Type: ELETECH000, Community: public, SNMP Version: V2, Offline: 1, Severity: 2, ReplyICMP: False, ReplySNMP: False, Device Category: -2, Device with dynamic loading rules enabled\r\nDynamic Definition File: ELETECH000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\n",
                        deviceDump);
                    Assert.AreEqual("Dispositivo non raggiungibile", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionEletech_WebRadio_2_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493274");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Web Radio Pioppe Di Salvano, Type: ETRADIO100, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: ETRADIO100.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: SnmpV2 Eletech S.r.l., Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 11:11:48.480, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione MIB, Severity: -255, Value: v1.00, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome del Costruttore del dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Modello del dispositivo, Severity: -255, Value: Radio Insertion, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Numero Seriale del dispositivo, Severity: -255, Value: B100138, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Versione del Software del dispositivo, Severity: -255, Value: 02.10 13/08/2009, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione del firmware del dispositivo, Severity: -255, Value: v1.1, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Categoria di appartenenza del dispositivo, Severity: -255, Value: Internet Radio, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome della locazione del dispositivo o identificativo, Severity: -255, Value: ELETECH_PIOPPE, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni rete, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.105.76.9, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Gateway, Severity: -255, Value: 10.105.76.201, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: DNS Primario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: DNS Secondario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Porta Web Server, Severity: -255, Value: 80, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni periferica, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: URL Primario, Severity: -255, Value: rtp://@224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: URL Secondario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: URL Terziario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Periodo per il check dello stream, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Limite al periodo per il check dello stream, Severity: -255, Value: 180, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: RTP Buffer in corso, Severity: -255, Value: 8000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Buffer in corso, Severity: -255, Value: 5664, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Priorità dello streaming in esecuzione, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: URL in esecuzione, Severity: -255, Value: rtp://@224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: BitRate dello stream in corso, Severity: -255, Value: 56, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Byte Played, Severity: -255, Value: 1122691999, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Contatore di Frame persi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Contatore di errori software, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Riconnessioni, Severity: -255, Value: 72933, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Volume, Severity: -255, Value: 70, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Contatore messaggi bufferizzati, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Contatore messaggi riprodotti, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionEletech_WebRadio_3_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174492766");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Web Radio Imola, Type: ETRADIO100, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: ETRADIO100.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: SnmpV2 Eletech S.r.l., Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 08:28:48.290, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione MIB, Severity: -255, Value: v2.00, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome del Costruttore del dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Modello del dispositivo, Severity: -255, Value: Radio Insertion, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Numero Seriale del dispositivo, Severity: -255, Value: K100028, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Versione del Software del dispositivo, Severity: -255, Value: 02.10 13/08/2009, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione del firmware del dispositivo, Severity: -255, Value: v2.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Categoria di appartenenza del dispositivo, Severity: -255, Value: Internet Radio, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome della locazione del dispositivo o identificativo, Severity: -255, Value: ELETECH, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni rete, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.105.73.6, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Gateway, Severity: -255, Value: 10.105.73.201, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: DNS Primario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: DNS Secondario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Porta Web Server, Severity: -255, Value: 80, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni periferica, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: URL Primario, Severity: -255, Value: rtp://224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: URL Secondario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: URL Terziario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Periodo per il check dello stream, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Limite al periodo per il check dello stream, Severity: -255, Value: 180, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: RTP Buffer in corso, Severity: -255, Value: 8000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Buffer in corso, Severity: -255, Value: 4648, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Priorità dello streaming in esecuzione, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: URL in esecuzione, Severity: -255, Value: rtp://224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: BitRate dello stream in corso, Severity: -255, Value: 56, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Byte Played, Severity: -255, Value: 160857816, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Contatore di Frame persi, Severity: -255, Value: 57, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Contatore di errori software, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Riconnessioni, Severity: -255, Value: 97, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Volume, Severity: -255, Value: 70, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Contatore messaggi bufferizzati, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Contatore messaggi riprodotti, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionEletech_WebRadio_4_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174492788");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Web Radio Varignana, Type: ETRADIO100, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: ETRADIO100.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: SnmpV2 Eletech S.r.l., Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 1 giorno, 01:03:20.760, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione MIB, Severity: -255, Value: v1.00, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome del Costruttore del dispositivo, Severity: -255, Value: Eletech S.r.l., Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Modello del dispositivo, Severity: -255, Value: Radio Insertion, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Numero Seriale del dispositivo, Severity: -255, Value: A100086, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Versione del Software del dispositivo, Severity: -255, Value: 02.10 13/08/2009, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione del firmware del dispositivo, Severity: -255, Value: v1.1, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Categoria di appartenenza del dispositivo, Severity: -255, Value: Internet Radio, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome della locazione del dispositivo o identificativo, Severity: -255, Value: ELETECH_VARIGNA, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni rete, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.105.73.4, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Gateway, Severity: -255, Value: 10.105.73.201, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: DNS Primario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: DNS Secondario, Severity: -255, Value: 0.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Porta Web Server, Severity: -255, Value: 80, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni periferica, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: URL Primario, Severity: -255, Value: rtp://@224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: URL Secondario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: URL Terziario, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Periodo per il check dello stream, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Limite al periodo per il check dello stream, Severity: -255, Value: 180, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: RTP Buffer in corso, Severity: -255, Value: 8000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Buffer in corso, Severity: -255, Value: 5336, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Priorità dello streaming in esecuzione, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: URL in esecuzione, Severity: -255, Value: rtp://@224.105.72.1:3030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: BitRate dello stream in corso, Severity: -255, Value: 56, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Byte Played, Severity: -255, Value: 2977135926, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Contatore di Frame persi, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Contatore di errori software, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Riconnessioni, Severity: -255, Value: 5642, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Volume, Severity: -255, Value: 70, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Contatore messaggi bufferizzati, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Contatore messaggi riprodotti, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
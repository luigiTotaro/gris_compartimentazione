﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsDeviceStatus
    {
        #region Test device status

        [Test]
        public void IsAlive_definition_IcmpNotSnmp()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511107");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void IsAlive_definition_SnmpNotIcmp()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511108");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void IsAlive_definition_NotSnmpNotIcmp()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511109");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp);
                    Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_OK()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511110");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_Warning()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511111");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_Error()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511112");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definitionIcmpNoSnmp_Warning()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511114");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_Unknown()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511115");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definitionIcmpSnmpOnlyMib2_Warning()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511117");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                    Assert.AreEqual("Dati SNMP incongruenti", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test severità

        [Test]
        public void Severity_definition_VarieCombinazioniSeveritaInfo_Valutazione_su_StreamField_Stream()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("167837953");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Test severità 1, Type: XXSNMP1, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: 1, Severity: -255, Description: Nome macchina ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\nStream Id: 2, Name: 2, Severity: -255, Description: Versione riconosciuta=-255\r\nField Id: 0, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Microsoft Windows CE Version 5.0 (Build 1400), Description: Versione riconosciuta=-255\r\nStream Id: 3, Name: 3, Severity: 0, Description: Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Up time, Severity: 0, Value: 29 days, 18:45:0.650, Description: Up Time ricevuto=0\r\nStream Id: 4, Name: 4, Severity: 0, Description: Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Up time, Severity: 0, Value: 29 days, 18:45:0.650, Description: Up Time ricevuto=0\r\nStream Id: 5, Name: 5, Severity: 1, Description: Up Time ricevuto=1\r\nField Id: 0, Array Id: 0, Name: Up time, Severity: 1, Value: 29 days, 18:45:0.650, Description: Up Time ricevuto=1\r\nStream Id: 6, Name: 6, Severity: 2, Description: Up Time ricevuto=2\r\nField Id: 0, Array Id: 0, Name: Up time, Severity: 2, Value: 29 days, 18:45:0.650, Description: Up Time ricevuto=2\r\nStream Id: 7, Name: 7, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Up time, Severity: 255, Value: 29 days, 18:45:0.650, Description: Up Time ricevuto\r\nStream Id: 8, Name: 8, Severity: -255, Description: Nome macchina ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\nStream Id: 9, Name: 9, Severity: 0, Description: Nome macchina ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nStream Id: 10, Name: 10, Severity: 1, Description: Nome macchina ricevuto=1\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 1, Value: WindowsCE, Description: Nome macchina ricevuto=1\r\nStream Id: 11, Name: 11, Severity: 2, Description: Nome macchina ricevuto=2\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 2, Value: WindowsCE, Description: Nome macchina ricevuto=2\r\nStream Id: 12, Name: 12, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: WindowsCE, Description: Nome macchina ricevuto\r\nStream Id: 13, Name: 13, Severity: -255, Description: Nome macchina ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\nStream Id: 14, Name: 14, Severity: 0, Description: Nome macchina ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nStream Id: 15, Name: 15, Severity: 1, Description: Nome macchina ricevuto=1\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 1, Value: WindowsCE, Description: Nome macchina ricevuto=1\r\nStream Id: 16, Name: 16, Severity: 2, Description: Nome macchina ricevuto=2\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 2, Value: WindowsCE, Description: Nome macchina ricevuto=2\r\nStream Id: 17, Name: 17, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: WindowsCE, Description: Nome macchina ricevuto\r\nStream Id: 18, Name: 18, Severity: -255, Description: Valore non riconosciuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Valore non riconosciuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_SeveritaInfo_DeviceCompletamenteInformazione_StatoPerifericaSconosciuto()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("167837954");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Test severità 2, Type: XXSNMP2, Community: public, SNMP Version: V2, Offline: 0, Severity: 255, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: -255, Description: Nome macchina ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\nStream Id: 2, Name: Informazioni standard, Severity: -255, Description: Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 1, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Locazione, Severity: -255, Value: Your Location Here, Description: Locazione ricevuta=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_SeveritaInfo_DeviceCompletamenteInformazione_SoloMIB2_StatoPerifericaSconosciuto()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("167837955");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Test severità 3, Type: XXSNMP3, Community: public, SNMP Version: V2, Offline: 0, Severity: 255, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: -255, Description: Nome macchina ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_SeveritaInfo_PrevalenzaWarning_Su_MIB2_Info()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("167837956");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Test severità 4, Type: XXSNMP4, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 1, Description: Nome macchina ricevuto=1\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 1, Value: WindowsCE, Description: Nome macchina ricevuto=1\r\nStream Id: 2, Name: Informazioni standard, Severity: -255, Description: Nome macchina ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Severity_definition_SeveritaInfo_PrevalenzaUnknown_Su_MIB2_Info()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("167837957");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp);
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Test severità 5, Type: XXSNMP5, Community: public, SNMP Version: V2, Offline: 0, Severity: 255, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: WindowsCE, Description: Nome macchina ricevuto\r\nStream Id: 2, Name: Informazioni standard, Severity: -255, Description: Nome macchina ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WindowsCE, Description: Nome macchina ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void Periferica_ICMP_Stream_Stato_Info_Periferica_Sconosciuta()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("167837958");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Test severità 6, Type: XXIP001, Offline: 0, Severity: 255, ReplyICMP: True\r\nStream Id: 1, Name: Stato generale, Severity: -255, Description: Risposta ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: -255, Value: 1, Description: Risposta ricevuta=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
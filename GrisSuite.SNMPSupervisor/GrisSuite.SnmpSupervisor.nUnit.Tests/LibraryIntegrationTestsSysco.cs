﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsSysco
    {
        #region Tets Sysco - definizioni originali

        [Test]
        public void DumpCompleto_definition_DeviceTypeSYLM000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174515595");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Binario 1, Type: SYLM000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: M01T00547L, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 7 days, 4:07:12.77, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 2, Description: Valore temperatura non disponibile=2;Termostato non funzionante=2;Valore tensione ricevuto=0;Funzionamento regolare=0;Scheda IF-Video non rilevata=0\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 2, Value: 999, Description: Valore temperatura non disponibile=2\r\nField Id: 1, Array Id: 0, Name: Stato temperatura, Severity: 2, Value: 3, Description: Termostato non funzionante=2\r\nField Id: 2, Array Id: 0, Name: Tensione alimentatore, Severity: 0, Value: 480, Description: Valore tensione ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato tensione alimentatore, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 4, Array Id: 0, Name: Presenza scheda di interfaccia video LED, Severity: 0, Value: 2, Description: Scheda IF-Video non rilevata=0\r\nStream Id: 3, Name: Informazioni generali, Severity: 0, Description: Monitor LED=0\r\nField Id: 0, Array Id: 0, Name: Categoria dispositivo, Severity: 0, Value: MON_LED_1, Description: Monitor LED=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSYLF000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174463822");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Partenze Atrio, Type: SYLF000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MATR1T02666M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 14 days, 16:33:12.89, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 2, Description: Valore temperatura ricevuto=0;Funzionamento regolare=0;Valore temperatura ricevuto=0;Termostato non funzionante=2;Valore tensione ricevuto=0;Funzionamento regolare=0;Scheda IF-Video non rilevata=0\r\nField Id: 0, Array Id: 0, Name: Temperatura ventole, Severity: 0, Value: 35, Description: Valore temperatura ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato temperatura ventole, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 2, Array Id: 0, Name: Temperatura scaldiglie, Severity: 0, Value: 998, Description: Valore temperatura ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato temperatura scaldiglie, Severity: 2, Value: 3, Description: Termostato non funzionante=2\r\nField Id: 4, Array Id: 0, Name: Tensione alimentatore, Severity: 0, Value: 0, Description: Valore tensione ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Stato tensione alimentatore, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 6, Array Id: 0, Name: Presenza scheda di interfaccia video LED, Severity: 0, Value: 2, Description: Scheda IF-Video non rilevata=0\r\nStream Id: 3, Name: Informazioni generali, Severity: 2, Description: Categoria errata: Monitor CRT tecn. Hantarex=2\r\nField Id: 0, Array Id: 0, Name: Categoria dispositivo, Severity: 2, Value: CRT_HANTAREX, Description: Categoria errata: Monitor CRT tecn. Hantarex=2\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSYTM200()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174512014");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Partenze 3, Type: SYTM200, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: M03T11766M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 19:27:59.17, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 2, Description: Valore temperatura non disponibile=2;Termostato non funzionante=2\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 2, Value: 999, Description: Valore temperatura non disponibile=2\r\nField Id: 1, Array Id: 0, Name: Stato temperatura, Severity: 2, Value: 3, Description: Termostato non funzionante=2\r\nField Id: 2, Array Id: 0, Name: Stato lampade retro TFT, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni generali, Severity: 0, Description: Monitor TFT r.2=0\r\nField Id: 0, Array Id: 0, Name: Categoria dispositivo, Severity: 0, Value: TFT_2_AIRPORT, Description: Monitor TFT r.2=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSYCMH00()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("774471350");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Bar Test Hantarex, Type: SYCMH00, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MATR1T02666M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 18 days, 16:33:12.89, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 0, Description: Valore temperatura ricevuto=0;Funzionamento regolare=0;Valore tensione ricevuto=0;Funzionamento regolare=0;Scheda IF-Video non rilevata=0\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 0, Value: 35, Description: Valore temperatura ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato temperatura, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 2, Array Id: 0, Name: Tensione alimentatore, Severity: 0, Value: 0, Description: Valore tensione ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato tensione alimentatore, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 4, Array Id: 0, Name: Presenza scheda di interfaccia video LED, Severity: 0, Value: 2, Description: Scheda IF-Video non rilevata=0\r\nStream Id: 3, Name: Informazioni generali, Severity: 0, Description: Monitor CRT tecn. Hantarex=0\r\nField Id: 0, Array Id: 0, Name: Categoria dispositivo, Severity: 0, Value: CRT_HANTAREX, Description: Monitor CRT tecn. Hantarex=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSYLB000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174463209");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Binario 1, Type: SYLB000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: BIN1T01321L, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 26 days, 19:12:10.72, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 0, Description: Valore temperatura ricevuto=0;Funzionamento regolare=0;Valore temperatura ricevuto=0;Funzionamento regolare=0;Valore tensione ricevuto=0;Funzionamento regolare=0;Scheda IF-Video non rilevata=0\r\nField Id: 0, Array Id: 0, Name: Temperatura ventole, Severity: 0, Value: 38, Description: Valore temperatura ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato temperatura ventole, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 2, Array Id: 0, Name: Temperatura scaldiglie, Severity: 0, Value: 40, Description: Valore temperatura ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato temperatura scaldiglie, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 4, Array Id: 0, Name: Tensione alimentatore, Severity: 0, Value: 501, Description: Valore tensione ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Stato tensione alimentatore, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nField Id: 6, Array Id: 0, Name: Presenza scheda di interfaccia video LED, Severity: 0, Value: 2, Description: Scheda IF-Video non rilevata=0\r\nStream Id: 3, Name: Informazioni generali, Severity: 0, Description: Indicatore di binario LED=0\r\nField Id: 0, Array Id: 0, Name: Categoria dispositivo, Severity: 0, Value: BIN_LED_3R, Description: Indicatore di binario LED=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSYTM100()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174471350");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Bar, Type: SYTM100, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: SESTOM2, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 4.20 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 1 day 20:36:11.52, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 0, Description: Valore temperatura ricevuto=0;Funzionamento regolare=0\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 0, Value: 37, Description: Valore temperatura ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato temperatura, Severity: 0, Value: 0, Description: Funzionamento regolare=0\r\nStream Id: 3, Name: Informazioni generali, Severity: 2, Description: Categoria errata: Monitor CRT tecn. Hantarex=2\r\nField Id: 0, Array Id: 0, Name: Categoria dispositivo, Severity: 2, Value: CRT_HANTAREX, Description: Categoria errata: Monitor CRT tecn. Hantarex=2\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
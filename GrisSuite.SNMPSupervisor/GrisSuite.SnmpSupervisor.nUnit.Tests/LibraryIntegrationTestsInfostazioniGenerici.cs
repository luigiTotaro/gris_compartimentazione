﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsInfostazioniGenerici
    {
        #region Test Infostazioni generici

        [Test]
        public void DumpCompleto_definitionINFSTAZ1_Aesys_PerifericaNonConformeSpecificaInfostazioniStatoForzato()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287428");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                    Assert.AreEqual("Dati diagnostici limitati alla sola sezione MIB-II", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys non valido, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True, Device Category: -2, Device with dynamic loading rules enabled, NoMatchDeviceStatusSeverity: Warning, NoMatchDeviceStatusDescription: Dati diagnostici limitati alla sola sezione MIB-II\r\nDynamic Definition File: INFSTAZ1.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 3294696, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionINFSTAZ1_Aesys_PerifericaNonConformeSpecificaInfostazioniStatoForzatoNoPing()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287429");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                    Assert.AreEqual("Dati diagnostici limitati alla sola sezione MIB-II", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys non valido 2, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: False, ReplySNMP: True, Device Category: -2, Device with dynamic loading rules enabled, NoMatchDeviceStatusSeverity: Warning, NoMatchDeviceStatusDescription: Dati diagnostici limitati alla sola sezione MIB-II\r\nDynamic Definition File: INFSTAZ1.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 3294696, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionINFSTAZ1_Aesys_PerifericaNonConformeSpecificaInfostazioniNoSnmp()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287430");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");
                    Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);
                    Assert.AreEqual("Dispositivo non raggiungibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys non valido 3, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 1, Severity: 255, ReplyICMP: False, ReplySNMP: False, Device Category: -2, Device with dynamic loading rules enabled, NoMatchDeviceStatusSeverity: Warning, NoMatchDeviceStatusDescription: Dati diagnostici limitati alla sola sezione MIB-II\r\nDynamic Definition File: INFSTAZ1.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionINFSTAZ1_Aesys_PerifericaNonConformeSpecificaInfostazioniCategoriaSconosciuta()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287431");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                    Assert.AreEqual("Dati diagnostici limitati alla sola sezione MIB-II", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys non valido 4, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True, Device Category: -2, Device with dynamic loading rules enabled, NoMatchDeviceStatusSeverity: Warning, NoMatchDeviceStatusDescription: Dati diagnostici limitati alla sola sezione MIB-II\r\nDynamic Definition File: INFSTAZ1.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 3294696, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionINFSTAZ1_Aesys_PerifericaNonConformeSpecificaInfostazioniProduttoreSconosciuto()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287432");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);
                    Assert.AreEqual("Dati diagnostici limitati alla sola sezione MIB-II", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys non valido 5, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True, Device Category: -2, Device with dynamic loading rules enabled, NoMatchDeviceStatusSeverity: Warning, NoMatchDeviceStatusDescription: Dati diagnostici limitati alla sola sezione MIB-II\r\nDynamic Definition File: INFSTAZ1.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 3294696, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionINFSTAZ1_Aesys_SogliaTempiRispostaWebService()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232287433");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Infostazioni Aesys test soglie 1, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 11, Device with dynamic loading rules enabled, Forced serial number by SNMP query: (not-defined)\r\nDynamic Definition File: INFSTAZ-Aesys.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WindowsCE, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Microsoft Windows CE Version 5.0 (Build 0), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 09:09:06.960, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Your System Contact Here, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Your Location Here, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Tabellone LED arrivi e partenze 10 righe + 2=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Aesys S.p.A., Seriate (Bergamo), ITALY, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: LED-T10+2, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: (not-defined), Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 2.0.16.0, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: vDSPAG144656URFI\\D8K04B[041120081730, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 11, Description: Tabellone LED arrivi e partenze 10 righe + 2=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: ID-NOT-SET, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: diagnosticaInfostazioni-MIB Version 0.60, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: WindowsCE, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: PCI\\RTL81391, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-50-B7-F2-F2-78, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 192.168.202.195, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.252.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 192.168.200.254, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 192.168.201.100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 192.168.201.101, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 06/10/2009 17:34:23, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 06/10/2009 08:25:29, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 4, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione ma non connesso=2;Valore anomalo=1;Valore gravemente anomalo=2;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 2, Value: 2, Description: In esecuzione ma non connesso=2\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 1, Value: 0 giorni, 00:00:25.400, Description: Valore anomalo=1\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 2, Value: 0 giorni, 00:00:42.100, Description: Valore gravemente anomalo=2\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 27, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di temperatura #1 [UBICOM], Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Spenta=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Spenta=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;Spenta=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 0, Description: Spenta=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: Ventola di raffreddamento #1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 0, Description: Spenta=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: Ventola di raffreddamento #2, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Ventola, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 0, Description: Spenta=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: Ventola di raffreddamento #3, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Sensore di luminosita' ambientale #1, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 12, Name: Stato dei LED, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Percentuale di LED guasti, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Soglia oltre cui il messaggio può non essere comprensibile (%), Severity: 0, Value: 10, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa allo stato dei LED, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p3V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 4, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 1, Description: p3V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Alimentatore 3.3V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 5, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Alimentatore 5V, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo alimentatore #3, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 12, Array Id: 0, Name: Stato alimentatore #3, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 13, Array Id: 0, Name: Tensione in uscita dell'alimentatore #3 (V o mV), Severity: 0, Value: 12, Description: Valore ricevuto=0\r\nField Id: 14, Array Id: 0, Name: Descrizione dell'alimentatore #3, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva dell'alimentatore #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 16, Array Id: 0, Name: Tipo alimentatore #4, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 17, Array Id: 0, Name: Stato alimentatore #4, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 18, Array Id: 0, Name: Tensione in uscita dell'alimentatore #4 (V o mV), Severity: 0, Value: 12, Description: Valore ricevuto=0\r\nField Id: 19, Array Id: 0, Name: Descrizione dell'alimentatore #4, Severity: 0, Value: Alimentatore 12V, Description: Valore ricevuto=0\r\nField Id: 20, Array Id: 0, Name: Severità complessiva dell'alimentatore #4, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
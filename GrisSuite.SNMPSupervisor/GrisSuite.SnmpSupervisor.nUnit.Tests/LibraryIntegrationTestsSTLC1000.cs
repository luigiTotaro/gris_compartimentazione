﻿using System;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsSTLC1000
    {
        #region Test STLC 1000

        [Test]
        public void DumpCompleto_definition_DeviceTypeSTLC1000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511288");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: STLC remoto test Bologna, Type: STLC1000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: TELEFIN0ER8Q5I0, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 9 Stepping 5 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 6621 hours 24 minutes 7 seconds, Description: Up Time ricevuto=0\r\nStream Id: 2, Name: Stato dischi, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 400,00 MB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 0, Value: 20 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 1998,25 MB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato IIS, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato SQL Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Stato SNMP Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato SNMP Supervisor Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato STLC Manager, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato Telnet Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Stato SCAgent Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 7, Array Id: 0, Name: Stato Supervisor Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address scheda di rete Intel, Severity: 0, Value: 00-E0-4B-0D-10-91, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche scheda di rete Intel, Severity: 0, Value: Sent: 1.213.672.604 / Received: 3.018.270.392, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP scheda di rete Intel, Severity: 0, Value: 10.102.212.184, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address scheda di rete DAVICOM, Severity: 0, Value: 00-15-7A-00-00-5A, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Statistiche scheda di rete DAVICOM, Severity: 0, Value: Sent: 2.086.506 / Received: 893.622, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP scheda di rete DAVICOM, Severity: 0, Value: 192.168.0.1, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: STLC remoto test Bologna, Type: STLC1000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: TELEFIN0ER8Q5I0, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 9 Stepping 5 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 6621 hours 24 minutes 7 seconds, Description: Up Time ricevuto=0\r\nStream Id: 2, Name: Stato dischi, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 400.00 MB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 0, Value: 20 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 1998.25 MB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato IIS, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato SQL Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Stato SNMP Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato SNMP Supervisor Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato STLC Manager, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato Telnet Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Stato SCAgent Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 7, Array Id: 0, Name: Stato Supervisor Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address scheda di rete Intel, Severity: 0, Value: 00-E0-4B-0D-10-91, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche scheda di rete Intel, Severity: 0, Value: Sent: 1,213,672,604 / Received: 3,018,270,392, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP scheda di rete Intel, Severity: 0, Value: 10.102.212.184, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address scheda di rete DAVICOM, Severity: 0, Value: 00-15-7A-00-00-5A, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Statistiche scheda di rete DAVICOM, Severity: 0, Value: Sent: 2,086,506 / Received: 893,622, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP scheda di rete DAVICOM, Severity: 0, Value: 192.168.0.1, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else
                    {
                        Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
                    }
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSTLC1000_ForzaturaDatiNonDisponibili()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511289");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: STLC remoto 2 test Bologna, Type: STLC1000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: TELEFIN0ER8Q5I0, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 9 Stepping 5 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 6621 hours 24 minutes 7 seconds, Description: Up Time ricevuto=0\r\nStream Id: 2, Name: Stato dischi, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 400,00 MB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 0, Value: 20 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 1998,25 MB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 2, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;Non in esecuzione=2;Non in esecuzione=2;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato IIS, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato SQL Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Stato SNMP Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato SNMP Supervisor Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato STLC Manager, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 5, Array Id: 0, Name: Stato Telnet Server, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 6, Array Id: 0, Name: Stato SCAgent Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 7, Array Id: 0, Name: Stato Supervisor Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address scheda di rete Intel, Severity: 0, Value: 00-E0-4B-0D-10-91, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche scheda di rete Intel, Severity: 0, Value: Sent: 1.213.672.604 / Received: 3.018.270.392, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP scheda di rete Intel, Severity: 0, Value: 10.102.212.184, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address scheda di rete DAVICOM, Severity: 0, Value: 00-15-7A-00-00-5A, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Statistiche scheda di rete DAVICOM, Severity: 0, Value: Sent: 2.086.506 / Received: 893.622, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP scheda di rete DAVICOM, Severity: 0, Value: 192.168.0.1, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: STLC remoto 2 test Bologna, Type: STLC1000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: TELEFIN0ER8Q5I0, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 9 Stepping 5 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 6621 hours 24 minutes 7 seconds, Description: Up Time ricevuto=0\r\nStream Id: 2, Name: Stato dischi, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 400.00 MB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 0, Value: 20 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 1998.25 MB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 2, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;Non in esecuzione=2;Non in esecuzione=2;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato IIS, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato SQL Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Stato SNMP Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato SNMP Supervisor Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato STLC Manager, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 5, Array Id: 0, Name: Stato Telnet Server, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 6, Array Id: 0, Name: Stato SCAgent Service, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 7, Array Id: 0, Name: Stato Supervisor Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address scheda di rete Intel, Severity: 0, Value: 00-E0-4B-0D-10-91, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche scheda di rete Intel, Severity: 0, Value: Sent: 1,213,672,604 / Received: 3,018,270,392, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP scheda di rete Intel, Severity: 0, Value: 10.102.212.184, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address scheda di rete DAVICOM, Severity: 0, Value: 00-15-7A-00-00-5A, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Statistiche scheda di rete DAVICOM, Severity: 0, Value: Sent: 2,086,506 / Received: 893,622, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP scheda di rete DAVICOM, Severity: 0, Value: 192.168.0.1, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else
                    {
                        Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
                    }
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
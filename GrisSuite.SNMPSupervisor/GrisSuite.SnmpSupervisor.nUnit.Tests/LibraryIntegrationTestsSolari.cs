﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsSolari
    {
        #region Test Solari - definizioni originali

        [Test]
        public void DumpCompleto_definition_DeviceTypeSOCM000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174466391");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor Sala Attesa 2, Type: SOCM000, Community: solari_public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WID 3216 - Solari, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - Next Generation GT V1.0 (Huckleberry), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 2 days, 1:20:40.89, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: atrevisan@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: \"Solari di Udine (default location)\", Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 2, Description: Temperatura entro i limiti normali=0;Tensione sotto i limiti normali=2;Tensione entro i limiti normali=0;Tensione entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 0, Value: 46, Description: Temperatura entro i limiti normali=0\r\nField Id: 1, Array Id: 0, Name: Stato tensione +27V, Severity: 2, Value: 30, Description: Tensione sotto i limiti normali=2\r\nField Id: 2, Array Id: 0, Name: Stato tensione +12V, Severity: 0, Value: 76, Description: Tensione entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Stato tensione +16V, Severity: 0, Value: 76, Description: Tensione entro i limiti normali=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSOTM000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174513846");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Totem 34 Piano Terra Ingresso 1 (2), Type: SOTM000, Community: solari_public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: WID 3216 - Solari, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - Next Generation GT V1.0 (Huckleberry), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up Time, Severity: 0, Value: 1 day, 22:47:11.650, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: atrevisan@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: \"Solari di Udine (default location)\", Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 2, Description: Lampade accese=0;Valore corrente anomalo=2;Valore di tensione di alimentazione anomalo=2;Corrente entro i limiti normali=0;Tensione di alimentazione entro i limiti normali=0;Temperatura entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Stato lampade, Severity: 0, Value: 1, Description: Lampade accese=0\r\nField Id: 1, Array Id: 0, Name: Corrente pannello, Severity: 2, Value: 37, Description: Valore corrente anomalo=2\r\nField Id: 2, Array Id: 0, Name: Tensione di alimentazione pannello, Severity: 2, Value: 1202, Description: Valore di tensione di alimentazione anomalo=2\r\nField Id: 3, Array Id: 0, Name: Corrente inverter, Severity: 0, Value: 605, Description: Corrente entro i limiti normali=0\r\nField Id: 4, Array Id: 0, Name: Tensione di alimentazione inverter, Severity: 0, Value: 2385, Description: Tensione di alimentazione entro i limiti normali=0\r\nField Id: 5, Array Id: 0, Name: Temperatura, Severity: 0, Value: 34, Description: Temperatura entro i limiti normali=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeSOLM000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174460253");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor1 Biglietteria Arrivi, Type: SOLM000, Community: solari_public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MON01T08668L, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Linux MON01T08668L 2.4.7-rmk3-np1-devfs , Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 1 day 18:51:00.14, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: info@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Unknown, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Stato generale, Severity: 2, Description: Temperatura entro i limiti normali=0;Valore di luminosità corretto=0;Messaggio non comprensibile=2\r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 0, Value: 35, Description: Temperatura entro i limiti normali=0\r\nField Id: 1, Array Id: 0, Name: Luminosità display, Severity: 0, Value: 31, Description: Valore di luminosità corretto=0\r\nField Id: 2, Array Id: 0, Name: Intellegibilità del messaggio esposto, Severity: 2, Value: 0, Description: Messaggio non comprensibile=2\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionConCommunitySbagliata_DeviceTypeSOLM000NonAccessibileSNMP()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174460254");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Monitor1 Biglietteria Arrivi, Type: SOLM000, Community: solari_public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: False\r\nStream Id: 1, Name: Informazioni standard, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato generale, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Temperatura, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Luminosità display, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Intellegibilità del messaggio esposto, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
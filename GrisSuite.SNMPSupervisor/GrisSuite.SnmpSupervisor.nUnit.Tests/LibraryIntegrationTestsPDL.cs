﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsPDL
    {
        #region Test Workstation

        [Test]
        public void DumpCompleto_definitionPDLSimulata01_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232263809");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Workstation XP PDL test 01, Type: WRKPDL000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Microsoft Windows XP=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WRKPDL001, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: -255, Value: Hardware: x86 Family 6 Model 7 Stepping 6 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Microsoft Windows XP=-255\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 0, Value: 0 giorni, 00:50:33.290, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Your contact here, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Your location here, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: -255, Value: RFISERVIZI, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.110.129, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.110.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: -255, Value: 5, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Versione minore sistema operativo, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: System Uptime, Severity: -255, Value: 0 giorni, 08:31:05.930, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Unità logiche disco, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: -255, Value: 9%, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Disco D - Spazio occupato, Severity: -255, Value: 42%, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Servizi, Severity: 0, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato Client DHCP, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato UltraVNC Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Stato servizio Workstation, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato McAfee Framework, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato McAfee McShield, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato RPC (Remote Procedure Call), Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Stato servizio Ora di Windows, Severity: 0, Value: 1, Description: In esecuzione=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionPDLSimulata02_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232263810");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Workstation XP PDL test 02, Type: WRKPDL000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Microsoft Windows XP=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WRKPDL001, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: -255, Value: Hardware: x86 Family 6 Model 7 Stepping 6 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Microsoft Windows XP=-255\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 0, Value: 0 giorni, 00:07:40.820, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Your contact here, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Your location here, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: -255, Value: RFISERVIZI, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.110.130, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.110.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: -255, Value: 5, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Versione minore sistema operativo, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: System Uptime, Severity: -255, Value: 0 giorni, 01:25:03.590, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Unità logiche disco, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: -255, Value: 12%, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Disco D - Spazio occupato, Severity: -255, Value: 0%, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Servizi, Severity: 2, Description: In esecuzione=0;Non presente=2;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato Client DHCP, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato UltraVNC Server, Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 2, Array Id: 0, Name: Stato servizio Workstation, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato McAfee Framework, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato McAfee McShield, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Stato RPC (Remote Procedure Call), Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Stato servizio Ora di Windows, Severity: 0, Value: 1, Description: In esecuzione=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionPDLSimulata03_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232263811");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Workstation XP PDL test 03, Type: WRKPDL000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Microsoft Windows XP=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WRKPDL001, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: -255, Value: Hardware: x86 Family 6 Model 23 Stepping 6 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Microsoft Windows XP=-255\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 0, Value: 0 giorni, 00:07:26.730, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Your contact here, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Your location here, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: -255, Value: RFISERVIZI, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.110.131, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.110.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: -255, Value: 5, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Versione minore sistema operativo, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: System Uptime, Severity: -255, Value: 0 giorni, 01:22:41.090, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Unità logiche disco, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: -255, Value: 14%, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Disco D - Spazio occupato, Severity: -255, Value: 0%, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Disco E - Spazio occupato, Severity: -255, Value: 100%, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Servizi, Severity: 2, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;Arrestato=2;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato Client DHCP, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato UltraVNC Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Stato servizio Workstation, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato McAfee Framework, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Stato McAfee McShield, Severity: 2, Value: 4, Description: Arrestato=2\r\nField Id: 5, Array Id: 0, Name: Stato RPC (Remote Procedure Call), Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Stato servizio Ora di Windows, Severity: 0, Value: 1, Description: In esecuzione=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionPDLSimulata04_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232263812");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Workstation XP PDL test 04, Type: WRKPDL000, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Microsoft Windows XP=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WRKPDL001, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: -255, Value: Hardware: x86 Family 6 Model 23 Stepping 6 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Microsoft Windows XP=-255\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 0, Value: 0 giorni, 00:09:44.560, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Your contact here, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Your location here, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: -255, Value: RFISERVIZI, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.110.132, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.110.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: -255, Value: 5, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Versione minore sistema operativo, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: System Uptime, Severity: -255, Value: 0 giorni, 01:45:27.030, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Unità logiche disco, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: -255, Value: 17%, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Disco D - Spazio occupato, Severity: -255, Value: 0%, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Servizi, Severity: 1, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;Arresto in corso=1;Ripresa in corso=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Stato Client DHCP, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Stato UltraVNC Server, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Stato servizio Workstation, Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Stato McAfee Framework, Severity: 1, Value: 3, Description: Arresto in corso=1\r\nField Id: 4, Array Id: 0, Name: Stato McAfee McShield, Severity: 0, Value: 2, Description: Ripresa in corso=0\r\nField Id: 5, Array Id: 0, Name: Stato RPC (Remote Procedure Call), Severity: 0, Value: 1, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Stato servizio Ora di Windows, Severity: 0, Value: 1, Description: In esecuzione=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionPDLSimulata05_DiagnosticaNonDisponibile_SNMPOffline_ForzaturaDisattivata()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232263813");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Workstation XP PDL test 05, Type: WRKPDL000, Community: public, SNMP Version: V2, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: False\r\nStream Id: 1, Name: Informazioni standard, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Informazioni sistema, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP scheda di rete #3, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 6, Array Id: 0, Name: Maschera di sottorete scheda di rete #3, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 8, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione minore sistema operativo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 10, Array Id: 0, Name: System Uptime, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Unità logiche disco, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Disco A - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Disco B - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Disco D - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Disco E - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Disco F - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 6, Array Id: 0, Name: Disco G - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 7, Array Id: 0, Name: Disco H - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 8, Array Id: 0, Name: Disco I - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Disco J - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 10, Array Id: 0, Name: Disco K - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 11, Array Id: 0, Name: Disco L - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 12, Array Id: 0, Name: Disco M - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 13, Array Id: 0, Name: Disco N - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 14, Array Id: 0, Name: Disco O - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 15, Array Id: 0, Name: Disco P - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 16, Array Id: 0, Name: Disco Q - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 17, Array Id: 0, Name: Disco R - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 18, Array Id: 0, Name: Disco S - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 19, Array Id: 0, Name: Disco T - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 20, Array Id: 0, Name: Disco U - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 21, Array Id: 0, Name: Disco V - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 22, Array Id: 0, Name: Disco W - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 23, Array Id: 0, Name: Disco X - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 24, Array Id: 0, Name: Disco Y - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 25, Array Id: 0, Name: Disco Z - Spazio occupato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 4, Name: Servizi, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Stato Client DHCP, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Stato UltraVNC Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Stato servizio Workstation, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 3, Array Id: 0, Name: Stato McAfee Framework, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Stato McAfee McShield, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 5, Array Id: 0, Name: Stato RPC (Remote Procedure Call), Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 6, Array Id: 0, Name: Stato servizio Ora di Windows, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionPDLSimulata06_Errore_SNMPOnline_ForzaturaAttiva()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("3232263814");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Workstation XP PDL test 06, Type: WRKPDL000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Microsoft Windows XP=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WRKPDL001, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: -255, Value: Hardware: x86 Family 6 Model 23 Stepping 6 AT/AT COMPATIBLE - Software: Windows 2000 Version 5.1 (Build 2600 Uniprocessor Free), Description: Microsoft Windows XP=-255\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 0, Value: 0 giorni, 00:09:44.560, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Your contact here, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Your location here, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: -255, Value: RFISERVIZI, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 10.102.110.132, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #2, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #2, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.110.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: -255, Value: 5, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Versione minore sistema operativo, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: System Uptime, Severity: -255, Value: 0 giorni, 01:45:27.030, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Unità logiche disco, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: -255, Value: 17%, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Disco D - Spazio occupato, Severity: -255, Value: 0%, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Servizi, Severity: 2, Description: Non presente=2;Non presente=2;Non presente=2;Non presente=2;Non presente=2;Non presente=2;Non presente=2\r\nField Id: 0, Array Id: 0, Name: Stato Client DHCP, Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 1, Array Id: 0, Name: Stato UltraVNC Server, Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 2, Array Id: 0, Name: Stato servizio Workstation, Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 3, Array Id: 0, Name: Stato McAfee Framework, Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 4, Array Id: 0, Name: Stato McAfee McShield, Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 5, Array Id: 0, Name: Stato RPC (Remote Procedure Call), Severity: 2, Value: 0, Description: Non presente=2\r\nField Id: 6, Array Id: 0, Name: Stato servizio Ora di Windows, Severity: 2, Value: 0, Description: Non presente=2\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
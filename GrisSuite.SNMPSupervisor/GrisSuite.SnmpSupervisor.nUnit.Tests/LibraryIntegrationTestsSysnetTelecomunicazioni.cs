﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsSysnetTelecomunicazioni
    {
        #region Test Sysnet Telecomunicazioni - Zeus M384 v4

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M384_V4_000_Ok_LoopAB()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493207");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: ZEUS Marzabotto, Type: SYSNETM384V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 111BB00100AB\r\nDynamic Definition File: SYSNETZEUS_M384_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-Bologna-Marzabotto, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs1-T-Bologna-Marzabotto, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 196 giorni, 19:19:32.160, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 2, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore gravemente anomalo=2\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M384, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 111BB00100AB, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-M384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.04, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 25.01.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0604734, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 1.01.1.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 03.07.06, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.142.23, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.142.201, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 2, Value: 61 °C, Description: Valore gravemente anomalo=2\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop AB abilitati=0;Master=0;B=0;Non attiva=0;192 Kb/s=0;2304 Kb/s=0;1024 Kb/s=0;2 fili=0;l0=0;Slave=0;B=0;Attiva=0;192 Kb/s=0;2304 Kb/s=0;192 Kb/s=0;2 fili=0;l0=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 3, Description: Loop AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità modem #1, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 2, Array Id: 0, Name: Annex modem #1, Severity: 0, Value: 2, Description: B=0\r\nField Id: 3, Array Id: 0, Name: Velocità adattativa modem #1, Severity: 0, Value: 3, Description: Non attiva=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima loop modem #1, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima loop modem #1, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità loop modem #1, Severity: 0, Value: 16, Description: 1024 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore modem #1, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: Livello Tx modem #1, Severity: 0, Value: 48, Description: l0=0\r\nField Id: 9, Array Id: 0, Name: Modalità modem #2, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex modem #2, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità adattativa modem #2, Severity: 0, Value: 1, Description: Attiva=0\r\nField Id: 12, Array Id: 0, Name: Velocità minima loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità massima loop modem #2, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 14, Array Id: 0, Name: Velocità loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 15, Array Id: 0, Name: Connettore modem #2, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 16, Array Id: 0, Name: Livello Tx modem #2, Severity: 0, Value: 48, Description: l0=0\r\nField Id: 33, Array Id: 0, Name: Velocità modem #1, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione modem #1, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 35, Array Id: 0, Name: Guadagno Rx modem #1, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Rapporto segnale/rumore modem #1, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Attenuazione di linea modem #1, Severity: -255, Value: 8, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Stato segment anomaly modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 39, Array Id: 0, Name: Stato segment defect modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 40, Array Id: 0, Name: Velocità modem #2, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione modem #2, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 42, Array Id: 0, Name: Guadagno Rx modem #2, Severity: -255, Value: 25, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Rapporto segnale/rumore modem #2, Severity: -255, Value: 7, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Attenuazione di linea modem #2, Severity: -255, Value: 21, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 46, Array Id: 0, Name: Stato segment defect modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                    Assert.AreEqual("Anomalia grave", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M384_V4_001_Warning_LoopAB()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493197");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: ZEUS Pioppe di Salvaro, Type: SYSNETM384V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M384_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-Bologna-PioppeDiSalvaro, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs1-T-Bologna-PioppeDiSalvaro, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 199 giorni, 23:14:42.160, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M384, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB00100C7, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-M384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.03, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 19.01.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0277181, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 2.01.1.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 05.08.06, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.142.19, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.142.201, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 36 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 2, Description: Loop AB abilitati=0;Master=0;B=0;Non attiva=0;384 Kb/s=0;2304 Kb/s=0;256 Kb/s=2;2 fili=0;l0=0;Slave=0;B=0;Attiva=0;192 Kb/s=0;2304 Kb/s=0;192 Kb/s=0;2 fili=0;l0=0;Valore ricevuto=-255;Allineamento in corso=1;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Attivo=0;Attivo=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 3, Description: Loop AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità modem #1, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 2, Array Id: 0, Name: Annex modem #1, Severity: 0, Value: 2, Description: B=0\r\nField Id: 3, Array Id: 0, Name: Velocità adattativa modem #1, Severity: 0, Value: 3, Description: Non attiva=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima loop modem #1, Severity: 0, Value: 6, Description: 384 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima loop modem #1, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità loop modem #1, Severity: 2, Value: 4, Description: 256 Kb/s=2\r\nField Id: 7, Array Id: 0, Name: Connettore modem #1, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: Livello Tx modem #1, Severity: 0, Value: 48, Description: l0=0\r\nField Id: 9, Array Id: 0, Name: Modalità modem #2, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex modem #2, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità adattativa modem #2, Severity: 0, Value: 1, Description: Attiva=0\r\nField Id: 12, Array Id: 0, Name: Velocità minima loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità massima loop modem #2, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 14, Array Id: 0, Name: Velocità loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 15, Array Id: 0, Name: Connettore modem #2, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 16, Array Id: 0, Name: Livello Tx modem #2, Severity: 0, Value: 48, Description: l0=0\r\nField Id: 33, Array Id: 0, Name: Velocità modem #1, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione modem #1, Severity: 1, Value: 1, Description: Allineamento in corso=1\r\nField Id: 35, Array Id: 0, Name: Guadagno Rx modem #1, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Rapporto segnale/rumore modem #1, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Attenuazione di linea modem #1, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Stato segment anomaly modem #1, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 39, Array Id: 0, Name: Stato segment defect modem #1, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 40, Array Id: 0, Name: Velocità modem #2, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione modem #2, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 42, Array Id: 0, Name: Guadagno Rx modem #2, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Rapporto segnale/rumore modem #2, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Attenuazione di linea modem #2, Severity: -255, Value: 8, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 46, Array Id: 0, Name: Stato segment defect modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                    Assert.AreEqual("Anomalia grave", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M384_V4_002_Ok_LoopA()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493256");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: ZEUS Bologna locale PCS-SIA, Type: SYSNETM384V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M384_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-P-Bologna-Ottagono, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs1-P-Bologna-Ottagono, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 344 giorni, 00:18:58.930, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M384, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB00100CA, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-M384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.03, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 19.01.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0604782, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 1.01.1.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.3, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 11.07.06, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.142.72, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.142.201, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 31 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop A abilitato=0;Master=0;B=0;Non attiva=0;192 Kb/s=0;2304 Kb/s=0;2048 Kb/s=0;2 fili=0;l0=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 1, Description: Loop A abilitato=0\r\nField Id: 1, Array Id: 0, Name: Modalità modem #1, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 2, Array Id: 0, Name: Annex modem #1, Severity: 0, Value: 2, Description: B=0\r\nField Id: 3, Array Id: 0, Name: Velocità adattativa modem #1, Severity: 0, Value: 3, Description: Non attiva=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima loop modem #1, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima loop modem #1, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità loop modem #1, Severity: 0, Value: 32, Description: 2048 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore modem #1, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: Livello Tx modem #1, Severity: 0, Value: 48, Description: l0=0\r\nField Id: 33, Array Id: 0, Name: Velocità modem #1, Severity: -255, Value: 2048, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione modem #1, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 35, Array Id: 0, Name: Guadagno Rx modem #1, Severity: -255, Value: 26, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Rapporto segnale/rumore modem #1, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Attenuazione di linea modem #1, Severity: -255, Value: 18, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Stato segment anomaly modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 39, Array Id: 0, Name: Stato segment defect modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M684S v4

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M684_V4_000_Ok_LoopAC()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493843");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: ZEUS DD/P di Modena, Type: SYSNETM684V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M684S_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: zeus, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: zeus, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 204 giorni, 02:07:58.820, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M684S, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB001013D, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-M684S, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.04, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 25.01.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0607674, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 1.02.1.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 18.09.06, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Indirizzo IP scheda di rete #13, Severity: -255, Value: 10.102.144.147, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Maschera di sottorete scheda di rete #13, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Gateway scheda di rete #13, Severity: -255, Value: 10.102.145.201, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 50 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop AC abilitati=0;Master=0;B=0;Non attiva=0;192 Kb/s=0;2304 Kb/s=0;512 Kb/s=0;2 fili=0;Auto=0;Master=0;B=0;Non attiva=0;192 Kb/s=0;2304 Kb/s=0;640 Kb/s=0;2 fili=0;l0=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 5, Description: Loop AC abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità modem #1, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 2, Array Id: 0, Name: Annex modem #1, Severity: 0, Value: 2, Description: B=0\r\nField Id: 3, Array Id: 0, Name: Velocità adattativa modem #1, Severity: 0, Value: 3, Description: Non attiva=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima loop modem #1, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima loop modem #1, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità loop modem #1, Severity: 0, Value: 8, Description: 512 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore modem #1, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: Livello Tx modem #1, Severity: 0, Value: 71, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità modem #2, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 10, Array Id: 0, Name: Annex modem #2, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità adattativa modem #2, Severity: 0, Value: 3, Description: Non attiva=0\r\nField Id: 12, Array Id: 0, Name: Velocità minima loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità massima loop modem #2, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 14, Array Id: 0, Name: Velocità loop modem #2, Severity: 0, Value: 10, Description: 640 Kb/s=0\r\nField Id: 15, Array Id: 0, Name: Connettore modem #2, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 16, Array Id: 0, Name: Livello Tx modem #2, Severity: 0, Value: 48, Description: l0=0\r\nField Id: 33, Array Id: 0, Name: Velocità modem #1, Severity: -255, Value: 512, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione modem #1, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 35, Array Id: 0, Name: Guadagno Rx modem #1, Severity: -255, Value: 33, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Rapporto segnale/rumore modem #1, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Attenuazione di linea modem #1, Severity: -255, Value: 23, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Stato segment anomaly modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 39, Array Id: 0, Name: Stato segment defect modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 40, Array Id: 0, Name: Velocità modem #2, Severity: -255, Value: 640, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione modem #2, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 42, Array Id: 0, Name: Guadagno Rx modem #2, Severity: -255, Value: 28, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Rapporto segnale/rumore modem #2, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Attenuazione di linea modem #2, Severity: -255, Value: 21, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 46, Array Id: 0, Name: Stato segment defect modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M684_V4_001_Ok_LoopBD()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493844");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual("Device Name: ZEUS DD/U di Rubiera, Type: SYSNETM684V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M684S_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: zeus, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: zeus, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 196 giorni, 19:57:58.820, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M684S, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB001012D, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-M684S, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.04, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 25.01.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0607671, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 1.02.1.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 18.09.06, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Indirizzo IP scheda di rete #13, Severity: -255, Value: 10.102.144.148, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Maschera di sottorete scheda di rete #13, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Gateway scheda di rete #13, Severity: -255, Value: 10.102.145.201, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 53 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop BD abilitati=0;Slave=0;B=0;Attiva=0;192 Kb/s=0;2304 Kb/s=0;448 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;Attiva=0;192 Kb/s=0;2304 Kb/s=0;256 Kb/s=0;2 fili=0;Auto=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 10, Description: Loop BD abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità modem #1, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 2, Array Id: 0, Name: Annex modem #1, Severity: 0, Value: 2, Description: B=0\r\nField Id: 3, Array Id: 0, Name: Velocità adattativa modem #1, Severity: 0, Value: 1, Description: Attiva=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima loop modem #1, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima loop modem #1, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità loop modem #1, Severity: 0, Value: 7, Description: 448 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore modem #1, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: Livello Tx modem #1, Severity: 0, Value: 71, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità modem #2, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex modem #2, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità adattativa modem #2, Severity: 0, Value: 1, Description: Attiva=0\r\nField Id: 12, Array Id: 0, Name: Velocità minima loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità massima loop modem #2, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 14, Array Id: 0, Name: Velocità loop modem #2, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 15, Array Id: 0, Name: Connettore modem #2, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 16, Array Id: 0, Name: Livello Tx modem #2, Severity: 0, Value: 71, Description: Auto=0\r\nField Id: 33, Array Id: 0, Name: Velocità modem #1, Severity: -255, Value: 512, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione modem #1, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 35, Array Id: 0, Name: Guadagno Rx modem #1, Severity: -255, Value: 31, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Rapporto segnale/rumore modem #1, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Attenuazione di linea modem #1, Severity: -255, Value: 23, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Stato segment anomaly modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 39, Array Id: 0, Name: Stato segment defect modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 40, Array Id: 0, Name: Velocità modem #2, Severity: -255, Value: 640, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione modem #2, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 42, Array Id: 0, Name: Guadagno Rx modem #2, Severity: -255, Value: 30, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Rapporto segnale/rumore modem #2, Severity: -255, Value: 9, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Attenuazione di linea modem #2, Severity: -255, Value: 22, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 46, Array Id: 0, Name: Stato segment defect modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M684_V4_002_Ok_LoopABCD()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493852");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual("Device Name: ZEUS DD/I di Cadeo, Type: SYSNETM684V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M684S_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-Bologna-Cadeo, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs1-T-Bologna-Cadeo, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 308 giorni, 06:07:58.820, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M684S, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB0010137, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-M684S, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.04, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 25.01.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0607679, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 1.02.1.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 18.09.06, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Indirizzo IP scheda di rete #13, Severity: -255, Value: 10.102.144.156, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Maschera di sottorete scheda di rete #13, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Gateway scheda di rete #13, Severity: -255, Value: 10.102.145.201, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 54 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop ABCD abilitati=0;Master=0;B=0;Non attiva=0;192 Kb/s=0;2304 Kb/s=0;512 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;Attiva=0;192 Kb/s=0;2304 Kb/s=0;192 Kb/s=0;2 fili=0;Auto=0;Master=0;B=0;Non attiva=0;192 Kb/s=0;2304 Kb/s=0;768 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;Attiva=0;192 Kb/s=0;2304 Kb/s=0;192 Kb/s=0;2 fili=0;Auto=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 15, Description: Loop ABCD abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità modem #1, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 2, Array Id: 0, Name: Annex modem #1, Severity: 0, Value: 2, Description: B=0\r\nField Id: 3, Array Id: 0, Name: Velocità adattativa modem #1, Severity: 0, Value: 3, Description: Non attiva=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima loop modem #1, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima loop modem #1, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità loop modem #1, Severity: 0, Value: 8, Description: 512 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore modem #1, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: Livello Tx modem #1, Severity: 0, Value: 71, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità modem #2, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex modem #2, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità adattativa modem #2, Severity: 0, Value: 1, Description: Attiva=0\r\nField Id: 12, Array Id: 0, Name: Velocità minima loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità massima loop modem #2, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 14, Array Id: 0, Name: Velocità loop modem #2, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 15, Array Id: 0, Name: Connettore modem #2, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 16, Array Id: 0, Name: Livello Tx modem #2, Severity: 0, Value: 71, Description: Auto=0\r\nField Id: 17, Array Id: 0, Name: Modalità modem #3, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 18, Array Id: 0, Name: Annex modem #3, Severity: 0, Value: 2, Description: B=0\r\nField Id: 19, Array Id: 0, Name: Velocità adattativa modem #3, Severity: 0, Value: 3, Description: Non attiva=0\r\nField Id: 20, Array Id: 0, Name: Velocità minima loop modem #3, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 21, Array Id: 0, Name: Velocità massima loop modem #3, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 22, Array Id: 0, Name: Velocità loop modem #3, Severity: 0, Value: 12, Description: 768 Kb/s=0\r\nField Id: 23, Array Id: 0, Name: Connettore modem #3, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 24, Array Id: 0, Name: Livello Tx modem #3, Severity: 0, Value: 71, Description: Auto=0\r\nField Id: 25, Array Id: 0, Name: Modalità modem #4, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 26, Array Id: 0, Name: Annex modem #4, Severity: 0, Value: 2, Description: B=0\r\nField Id: 27, Array Id: 0, Name: Velocità adattativa modem #4, Severity: 0, Value: 1, Description: Attiva=0\r\nField Id: 28, Array Id: 0, Name: Velocità minima loop modem #4, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 29, Array Id: 0, Name: Velocità massima loop modem #4, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 30, Array Id: 0, Name: Velocità loop modem #4, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 31, Array Id: 0, Name: Connettore modem #4, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 32, Array Id: 0, Name: Livello Tx modem #4, Severity: 0, Value: 71, Description: Auto=0\r\nField Id: 33, Array Id: 0, Name: Velocità modem #1, Severity: -255, Value: 512, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione modem #1, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 35, Array Id: 0, Name: Guadagno Rx modem #1, Severity: -255, Value: 89, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Rapporto segnale/rumore modem #1, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Attenuazione di linea modem #1, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Stato segment anomaly modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 39, Array Id: 0, Name: Stato segment defect modem #1, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 40, Array Id: 0, Name: Velocità modem #2, Severity: -255, Value: 768, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione modem #2, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 42, Array Id: 0, Name: Guadagno Rx modem #2, Severity: -255, Value: 18, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Rapporto segnale/rumore modem #2, Severity: -255, Value: 7, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Attenuazione di linea modem #2, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 46, Array Id: 0, Name: Stato segment defect modem #2, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 47, Array Id: 0, Name: Velocità modem #3, Severity: -255, Value: 768, Description: Valore ricevuto=-255\r\nField Id: 48, Array Id: 0, Name: Stato connessione modem #3, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 49, Array Id: 0, Name: Guadagno Rx modem #3, Severity: -255, Value: 19, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Rapporto segnale/rumore modem #3, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 51, Array Id: 0, Name: Attenuazione di linea modem #3, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Stato segment anomaly modem #3, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 53, Array Id: 0, Name: Stato segment defect modem #3, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 54, Array Id: 0, Name: Velocità modem #4, Severity: -255, Value: 768, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Stato connessione modem #4, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 56, Array Id: 0, Name: Guadagno Rx modem #4, Severity: -255, Value: 19, Description: Valore ricevuto=-255\r\nField Id: 57, Array Id: 0, Name: Rapporto segnale/rumore modem #4, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Attenuazione di linea modem #4, Severity: -255, Value: 13, Description: Valore ricevuto=-255\r\nField Id: 59, Array Id: 0, Name: Stato segment anomaly modem #4, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 60, Array Id: 0, Name: Stato segment defect modem #4, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Non connesso=0;Auto=0;Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus Vari, senza MIB

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_GTW400R_V5_000_Warning_SenzaMIB()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174492893");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual("Dati diagnostici limitati alla sola sezione MIB-II", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G384_V2_000_Warning_SenzaMIB()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174492898");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Warning, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zs1-T-Bologna-PoggioRusco, Type: SYSNET000, Community: public, SNMP Version: V1, Offline: 0, Severity: 1, ReplyICMP: True, ReplySNMP: True, Device Category: -2, Device with dynamic loading rules enabled, NoMatchDeviceStatusSeverity: Warning, NoMatchDeviceStatusDescription: Dati diagnostici limitati alla sola sezione MIB-II\r\nDynamic Definition File: SYSNET000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 1, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Locazione ricevuta=-255;Zeus G384 V2=1\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: Zs1-T-Bologna-PoggioRusco, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Zs1-T-Bologna-PoggioRusco, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 196 giorni, 20:05:11.710, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Locazione, Severity: -255, Value: Milano, Description: Locazione ricevuta=-255\r\nField Id: 4, Array Id: 0, Name: Modello apparato Sysnet (privo di MIB specifica), Severity: 1, Value: .1.3.6.1.4.1.15078.41.7.384, Description: Zeus G384 V2=1\r\n",
                        deviceDump);
                    Assert.AreEqual("Dati diagnostici limitati alla sola sezione MIB-II", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus G384 v4

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G384_V4_000_Error()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174492899");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zs2-T-Bologna-PoggioRusco, Type: SYSNETG384V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_G384_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs2-T-Bologna-PoggioRusco, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs2-T-Bologna-PoggioRusco, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 00:07:55.800, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-G384, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB00104E3, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-G384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.08, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 25.01.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0805413, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 2.01.1.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 10.07.08, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.140.227, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.140.201, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 45 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN, Severity: 2, Description: Link E1-AB abilitati=0;Unframed=0;Esterna=0;Unframed=0;Esterna=0;Allarme=2;Normale=0;Normale=0;Normale=0;Normale=0;Allarme=2;Normale=0;Normale=0;Normale=0;Normale=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia G703, Severity: 0, Value: 3, Description: Link E1-AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità frame #1, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 2, Array Id: 0, Name: Sorgente clock modem #1, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 3, Array Id: 0, Name: Modalità frame #2, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 4, Array Id: 0, Name: Sorgente clock modem #2, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 9, Array Id: 0, Name: Perdita di segnale modem #1, Severity: 2, Value: 1, Description: Allarme=2\r\nField Id: 10, Array Id: 0, Name: Perdita di allineamento frame modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 11, Array Id: 0, Name: Rapporto bit/errore modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 12, Array Id: 0, Name: Segnale indicazione allarme modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 13, Array Id: 0, Name: Loop1 modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 14, Array Id: 0, Name: Perdita di segnale modem #2, Severity: 2, Value: 1, Description: Allarme=2\r\nField Id: 15, Array Id: 0, Name: Perdita di allineamento frame modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 16, Array Id: 0, Name: Rapporto bit/errore modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 17, Array Id: 0, Name: Segnale indicazione allarme modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 18, Array Id: 0, Name: Loop1 modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                    Assert.AreEqual("Anomalia grave", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus G684S v4

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G684_V4_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("167874708");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus G684S simulato, Type: SYSNETG684V4, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_G684_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: zeus, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: zeus, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 134 giorni, 13:31:18.820, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-G684S, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000AABB1012D, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-G684S, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.00, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 18.04.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: AABB1012, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 1.01.1.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 16.04.09, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Indirizzo IP scheda di rete #13, Severity: -255, Value: 10.1.144.148, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Maschera di sottorete scheda di rete #13, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Gateway scheda di rete #13, Severity: -255, Value: 10.1.145.201, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 45 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Link E1-BD abilitati=0;Unframed=0;Esterna=0;Unframed=0;Esterna=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia G703, Severity: 0, Value: 10, Description: Link E1-BD abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità frame #1, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 2, Array Id: 0, Name: Sorgente clock modem #1, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 3, Array Id: 0, Name: Modalità frame #2, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 4, Array Id: 0, Name: Sorgente clock modem #2, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 9, Array Id: 0, Name: Perdita di segnale modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 10, Array Id: 0, Name: Perdita di allineamento frame modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 11, Array Id: 0, Name: Rapporto bit/errore modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 12, Array Id: 0, Name: Segnale indicazione allarme modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 13, Array Id: 0, Name: Loop1 modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 14, Array Id: 0, Name: Perdita di segnale modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 15, Array Id: 0, Name: Perdita di allineamento frame modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 16, Array Id: 0, Name: Rapporto bit/errore modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 17, Array Id: 0, Name: Segnale indicazione allarme modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 18, Array Id: 0, Name: Loop1 modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M684S v3

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M684S_V3_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174482201");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual("Device Name: Zeus ZEUS-M684S v3 Abbadia Lariana, Type: SYSNETM684V3, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M684S_V3_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-Colico-Abbadia, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs1-T-Colico-Abbadia, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 18 giorni, 20:14:17.190, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M684S v3, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB0010711, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY3STZ-M684S, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 01.00, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 09.07.09, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0902445, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0033, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 29.05.09, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Indirizzo IP scheda di rete #13, Severity: -255, Value: 10.102.99.25, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Maschera di sottorete scheda di rete #13, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Gateway scheda di rete #13, Severity: -255, Value: 10.102.99.126, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 56 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus G384S v3

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G384_V3_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511634");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zs1-T-MilanoLambrate-Albate, Type: SYSNETG384V3, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_G384_V3_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-MilanoLambrate-Albate, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs1-T-MilanoLambrate-Albate, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 151 giorni, 02:24:20.210, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-G384 v1, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB0010780, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY1STZ-G384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 01.01, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 15.10.09, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0903851, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0033, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 09.10.09, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.214.18, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.214.111, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 52 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Link E1-AB abilitati=0;Unframed=0;Esterna=0;Unframed=0;Esterna=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia G703, Severity: 0, Value: 3, Description: Link E1-AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità frame #1, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 2, Array Id: 0, Name: Sorgente clock modem #1, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 3, Array Id: 0, Name: Modalità frame #2, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 4, Array Id: 0, Name: Sorgente clock modem #2, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 9, Array Id: 0, Name: Perdita di segnale modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 10, Array Id: 0, Name: Perdita di allineamento frame modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 11, Array Id: 0, Name: Rapporto bit/errore modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 12, Array Id: 0, Name: Segnale indicazione allarme modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 13, Array Id: 0, Name: Loop1 modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 14, Array Id: 0, Name: Perdita di segnale modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 15, Array Id: 0, Name: Perdita di allineamento frame modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 16, Array Id: 0, Name: Rapporto bit/errore modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 17, Array Id: 0, Name: Segnale indicazione allarme modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 18, Array Id: 0, Name: Loop1 modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus G384 V3

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G384_V3_001_Error()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493284");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: zs2-p-bologna-ottagono, Type: SYSNETG384V3, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_G384_V3_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: zs2-p-bologna-ottagono, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: zs2-p-bologna-ottagono, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 197 giorni, 14:26:52.160, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-G384 v1, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB001079E, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY1STZ-G384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 01.01, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 22.10.09, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0903893, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0033, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 20.10.09, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.142.100, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.142.201, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni WAN, Severity: 2, Description: Link E1-AB abilitati=0;Unframed=0;Esterna=0;Unframed=0;Esterna=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Allarme=2;Normale=0;Normale=0;Normale=0;Normale=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia G703, Severity: 0, Value: 3, Description: Link E1-AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità frame #1, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 2, Array Id: 0, Name: Sorgente clock modem #1, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 3, Array Id: 0, Name: Modalità frame #2, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 4, Array Id: 0, Name: Sorgente clock modem #2, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 9, Array Id: 0, Name: Perdita di segnale modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 10, Array Id: 0, Name: Perdita di allineamento frame modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 11, Array Id: 0, Name: Rapporto bit/errore modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 12, Array Id: 0, Name: Segnale indicazione allarme modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 13, Array Id: 0, Name: Loop1 modem #1, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 14, Array Id: 0, Name: Perdita di segnale modem #2, Severity: 2, Value: 1, Description: Allarme=2\r\nField Id: 15, Array Id: 0, Name: Perdita di allineamento frame modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 16, Array Id: 0, Name: Rapporto bit/errore modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 17, Array Id: 0, Name: Segnale indicazione allarme modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 18, Array Id: 0, Name: Loop1 modem #2, Severity: 0, Value: 0, Description: Normale=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M384 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M384_V5_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174517684");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus M384 V5 Quattro Ville, Type: SYSNETM384V5, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB000FF78\r\nDynamic Definition File: SYSNETZEUS_M384_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus M384 V5=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: ZS1-T-VR01-Quattroville, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus M384, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 209 giorni, 15:55:47.790, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.13.384, Description: Zeus M384 V5=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M384, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB000FF78, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000027, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 13.10.11, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1103417, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.0, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 05.07.11, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 24, Array Id: 0, Name: Indirizzo IP scheda di rete #8, Severity: -255, Value: 192.168.10.10, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Maschera di sottorete scheda di rete #8, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.237.180, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.237.13, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 43 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop AB abilitati=0;Auto=0;Master=0;B=0;256 Kb/s=0;2304 Kb/s=0;1024 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;256 Kb/s=0;2304 Kb/s=0;n/d=-255;2 fili=0;Attivo=0;Nessuna=0;Valore ricevuto=-255;Disconnesso=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;n/d=-255;n/d=-255;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 3, Description: Loop AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Modalità loop A, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 3, Array Id: 0, Name: Annex loop A, Severity: 0, Value: 2, Description: B=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima loop A, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima loop A, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità loop A, Severity: 0, Value: 16, Description: 1024 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore loop A, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità loop B, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex loop B, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità minima loop B, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 12, Array Id: 0, Name: Velocità massima loop B, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità loop B, Severity: -255, Value: 1, Description: n/d=-255\r\nField Id: 14, Array Id: 0, Name: Connettore loop B, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 15, Array Id: 0, Name: Stato abilitazione loop A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 16, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: -1, Description: Nessuna=0\r\nField Id: 17, Array Id: 0, Name: Velocità loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 18, Array Id: 0, Name: Stato connessione loop A, Severity: -255, Value: 3, Description: Disconnesso=-255\r\nField Id: 19, Array Id: 0, Name: Potenza Tx loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Margine rumore modem loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Stato attenuazione modem loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Stato segment anomaly loop A, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 23, Array Id: 0, Name: Stato segment defect loop A, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 24, Array Id: 0, Name: Stato abilitazione loop B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 25, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 26, Array Id: 0, Name: Velocità loop B, Severity: -255, Value: 512, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Stato connessione loop B, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 28, Array Id: 0, Name: Potenza Tx loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 29, Array Id: 0, Name: Margine rumore modem loop B, Severity: -255, Value: 13, Description: Valore ricevuto=-255\r\nField Id: 30, Array Id: 0, Name: Stato attenuazione modem loop B, Severity: -255, Value: 19, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Stato segment anomaly loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 32, Array Id: 0, Name: Stato segment defect loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M684 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M684_V5_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174512170");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus M684 V5 Settimo, Type: SYSNETM684V5, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB000FF66\r\nDynamic Definition File: SYSNETZEUS_M684_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus M684 V5=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs-T-To01-Bivio-Settimo, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus M684, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 119 giorni, 11:38:17.960, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.13.684, Description: Zeus M684 V5=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M684, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB000FF66, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000030, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 14.09.11, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1103385, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.0, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 30.05.11, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Indirizzo IP scheda di rete #12, Severity: -255, Value: 192.168.10.10, Description: Valore ricevuto=-255\r\nField Id: 33, Array Id: 0, Name: Maschera di sottorete scheda di rete #12, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Indirizzo IP scheda di rete #13, Severity: -255, Value: 10.102.216.42, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Maschera di sottorete scheda di rete #13, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Gateway scheda di rete #13, Severity: -255, Value: 10.102.217.201, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 49 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop ABCD abilitati=0;Auto=0;Master=0;B=0;256 Kb/s=0;2304 Kb/s=0;1536 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;256 Kb/s=0;2304 Kb/s=0;n/d=-255;2 fili=0;Auto=0;Master=0;B=0;256 Kb/s=0;2304 Kb/s=0;1536 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;256 Kb/s=0;2304 Kb/s=0;n/d=-255;2 fili=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 15, Description: Loop ABCD abilitati=0\r\nField Id: 1, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Modalità loop A, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 3, Array Id: 0, Name: Annex loop A, Severity: 0, Value: 2, Description: B=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima modem loop A, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima modem loop A, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità modem loop A, Severity: 0, Value: 24, Description: 1536 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore loop A, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità loop B, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex loop B, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità minima modem loop B, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 12, Array Id: 0, Name: Velocità massima modem loop B, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità modem loop B, Severity: -255, Value: 1, Description: n/d=-255\r\nField Id: 14, Array Id: 0, Name: Connettore loop B, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 15, Array Id: 0, Name: PAM modulation loop C, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 16, Array Id: 0, Name: Modalità loop C, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 17, Array Id: 0, Name: Annex loop C, Severity: 0, Value: 2, Description: B=0\r\nField Id: 18, Array Id: 0, Name: Velocità minima modem loop C, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 19, Array Id: 0, Name: Velocità massima modem loop C, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 20, Array Id: 0, Name: Velocità modem loop C, Severity: 0, Value: 24, Description: 1536 Kb/s=0\r\nField Id: 21, Array Id: 0, Name: Connettore loop C, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 22, Array Id: 0, Name: PAM modulation loop D, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 23, Array Id: 0, Name: Modalità loop D, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 24, Array Id: 0, Name: Annex loop D, Severity: 0, Value: 2, Description: B=0\r\nField Id: 25, Array Id: 0, Name: Velocità minima modem loop D, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 26, Array Id: 0, Name: Velocità massima modem loop D, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 27, Array Id: 0, Name: Velocità modem loop D, Severity: -255, Value: 1, Description: n/d=-255\r\nField Id: 28, Array Id: 0, Name: Connettore loop D, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 29, Array Id: 0, Name: Stato abilitazione loop A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 30, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 31, Array Id: 0, Name: Velocità loop A, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Stato connessione loop A, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 33, Array Id: 0, Name: Potenza Tx loop A, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Margine rumore modem loop A, Severity: -255, Value: 26, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Stato attenuazione modem loop A, Severity: -255, Value: 5, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Stato segment anomaly loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 37, Array Id: 0, Name: Stato segment defect loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 38, Array Id: 0, Name: Stato abilitazione loop B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 39, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 40, Array Id: 0, Name: Velocità loop B, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione loop B, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 42, Array Id: 0, Name: Potenza Tx loop B, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Margine rumore modem loop B, Severity: -255, Value: 25, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Stato attenuazione modem loop B, Severity: -255, Value: 8, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 46, Array Id: 0, Name: Stato segment defect loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 47, Array Id: 0, Name: Stato abilitazione loop C, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 48, Array Id: 0, Name: PAM modulation loop C, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 49, Array Id: 0, Name: Velocità loop C, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Stato connessione loop C, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 51, Array Id: 0, Name: Potenza Tx loop C, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Margine rumore modem loop C, Severity: -255, Value: 26, Description: Valore ricevuto=-255\r\nField Id: 53, Array Id: 0, Name: Stato attenuazione modem loop C, Severity: -255, Value: 6, Description: Valore ricevuto=-255\r\nField Id: 54, Array Id: 0, Name: Stato segment anomaly loop C, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 55, Array Id: 0, Name: Stato segment defect loop C, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 56, Array Id: 0, Name: Stato abilitazione loop D, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 57, Array Id: 0, Name: PAM modulation loop D, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 58, Array Id: 0, Name: Velocità loop D, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione loop D, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 60, Array Id: 0, Name: Potenza Tx loop D, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Margine rumore modem loop D, Severity: -255, Value: 29, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Stato attenuazione modem loop D, Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 63, Array Id: 0, Name: Stato segment anomaly loop D, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 64, Array Id: 0, Name: Stato segment defect loop D, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M184 V4

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M184_V4_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174469187");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus M184 V4 Bivio d'Aurisina, Type: SYSNETM184V4, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB001091B\r\nDynamic Definition File: SYSNETZEUS_M184_V4_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus M184 V4=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: zeus-Bivio-Aurisina, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: zeus-Bivio-Aurisina, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 455 giorni, 03:35:07.730, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.43.13.184, Description: Zeus M184 V4=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M184 v1, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB001091B, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY1STZ-M184, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 01.03, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 10.09.10, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1000961, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.5.4, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0033, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 21.05.10, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.48.67, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.48.50, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 42 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop A abilitato=0;Loop A connesso=0;Loop B disconnesso=-255;pam16=0;Master=0;B=0;Non attivo=0;192 Kb/s=0;2304 Kb/s=0;1536 Kb/s=0;2 fili=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 1, Description: Loop A abilitato=0\r\nField Id: 1, Array Id: 0, Name: Stato loop DSL downstream, Severity: 0, Value: 1, Description: Loop A connesso=0\r\nField Id: 2, Array Id: 0, Name: Stato loop DSL upstream, Severity: -255, Value: 0, Description: Loop B disconnesso=-255\r\nField Id: 3, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 4, Array Id: 0, Name: Modalità loop A, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 5, Array Id: 0, Name: Annex loop A, Severity: 0, Value: 2, Description: B=0\r\nField Id: 6, Array Id: 0, Name: Modem adaptive rate loop A, Severity: 0, Value: 3, Description: Non attivo=0\r\nField Id: 7, Array Id: 0, Name: Velocità minima modem loop A, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 8, Array Id: 0, Name: Velocità massima modem loop A, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 9, Array Id: 0, Name: Velocità modem loop A, Severity: 0, Value: 24, Description: 1536 Kb/s=0\r\nField Id: 10, Array Id: 0, Name: Connettore loop A, Severity: 0, Value: 2, Description: 2 fili=0\r\nField Id: 19, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 20, Array Id: 0, Name: Velocità loop A, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Stato connessione loop A, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 22, Array Id: 0, Name: Potenza Tx loop A, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 23, Array Id: 0, Name: Margine rumore modem loop A, Severity: -255, Value: 26, Description: Valore ricevuto=-255\r\nField Id: 24, Array Id: 0, Name: Stato attenuazione modem loop A, Severity: -255, Value: 9, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Stato segment anomaly loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 26, Array Id: 0, Name: Stato segment defect loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M184 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M184_V5_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174469175");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus M184 V5 Muzzana del T., Type: SYSNETM184V5, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB000FF6E\r\nDynamic Definition File: SYSNETZEUS_M184_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus M184 V5=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zeus-Muzzana, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus M184, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 41 giorni, 04:22:45.450, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.13.184, Description: Zeus M184 V5=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M184, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB000FF6E, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000025, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 29.09.11, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1103360, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.0, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 31.05.11, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 24, Array Id: 0, Name: Indirizzo IP scheda di rete #8, Severity: -255, Value: 10.102.48.55, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Maschera di sottorete scheda di rete #8, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 57, Array Id: 0, Name: Gateway scheda di rete #8, Severity: -255, Value: 10.102.48.50, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 21 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop AB abilitati=0;Auto=0;Master=0;B=0;256 Kb/s=0;2304 Kb/s=0;1536 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;256 Kb/s=0;2304 Kb/s=0;n/d=-255;2 fili=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 3, Description: Loop AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Modalità loop A, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 3, Array Id: 0, Name: Annex loop A, Severity: 0, Value: 2, Description: B=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima modem loop A, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima modem loop A, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità modem loop A, Severity: 0, Value: 24, Description: 1536 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore loop A, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità loop B, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex loop B, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità minima modem loop B, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 12, Array Id: 0, Name: Velocità massima modem loop B, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità modem loop B, Severity: -255, Value: 1, Description: n/d=-255\r\nField Id: 14, Array Id: 0, Name: Connettore loop B, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 15, Array Id: 0, Name: Stato abilitazione loop A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 16, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 17, Array Id: 0, Name: Velocità loop A, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 18, Array Id: 0, Name: Stato connessione loop A, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 19, Array Id: 0, Name: Potenza Tx loop A, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Margine rumore modem loop A, Severity: -255, Value: 26, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Stato attenuazione modem loop A, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Stato segment anomaly loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 23, Array Id: 0, Name: Stato segment defect loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 24, Array Id: 0, Name: Stato abilitazione loop B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 25, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 26, Array Id: 0, Name: Velocità loop B, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Stato connessione loop B, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 28, Array Id: 0, Name: Potenza Tx loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 29, Array Id: 0, Name: Margine rumore modem loop B, Severity: -255, Value: 24, Description: Valore ricevuto=-255\r\nField Id: 30, Array Id: 0, Name: Stato attenuazione modem loop B, Severity: -255, Value: 18, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Stato segment anomaly loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 32, Array Id: 0, Name: Stato segment defect loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Non connesso=0;Auto=0;Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M384 V1

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M384_V1_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174481939");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus M384 V1 S.Giovanni al Natisone, Type: SYSNETM384V1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB00106DB\r\nDynamic Definition File: SYSNETZEUS_M384_V1_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus M384 V1=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: zeus, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: zeus, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 142 giorni, 09:12:44.800, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.40.13.384, Description: Zeus M384 V1=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M384 v1, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB00106DB, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY1STZ-M384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 01.01, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 27.05.09, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0900264, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 2.01.1.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.1.1, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 09.02.09, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.98.19, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.98.110, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 41 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M684S V1

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M684S_V1_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174508040");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus M684S V1 Montjovet, Type: SYSNETM684V1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB001049F\r\nDynamic Definition File: SYSNETZEUS_M684S_V1_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus M684S V1=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: zeus, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: zeus, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 177 giorni, 13:09:21.780, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.40.13.6842, Description: Zeus M684S V1=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M684S v2, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB001049F, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY2STZ-M684S, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 01.01, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 16.07.08, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0805356, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 2.01.1.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.3.1, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 30.06.08, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Indirizzo IP scheda di rete #13, Severity: -255, Value: 10.102.200.8, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Maschera di sottorete scheda di rete #13, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Gateway scheda di rete #13, Severity: -255, Value: 10.102.200.108, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 47 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop ABCD abilitati=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 15, Description: Loop ABCD abilitati=0\r\nField Id: 1, Array Id: 0, Name: Velocità modem #1, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Velocità modem #2, Severity: -255, Value: 1088, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Velocità modem #3, Severity: -255, Value: 2048, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Velocità modem #4, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus G384 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G384_V5_001_Error()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174614785");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus G384S V5 Firenze Campo Marte, Type: SYSNETG384V5, Community: public, SNMP Version: V1, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB000FF98\r\nDynamic Definition File: SYSNETZEUS_G384_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus G384 V5=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs2-P-FirenzeCM-LTINFO, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus G384, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 70 giorni, 02:22:14.370, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.7.384, Description: Zeus G384 V5=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-G384, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB000FF98, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000022, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 15.11.11, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1104823, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.0, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 05.07.11, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.104.105.1, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.104.105.201, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 37 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN, Severity: 2, Description: Link E1-AB abilitati=0;Unframed=0;Esterna=0;Unframed=0;Esterna=0;Attivo=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Attivo=0;Allarme=2;Normale=0;Normale=0;Normale=0;Normale=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia G703, Severity: 0, Value: 3, Description: Link E1-AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità frame E1-A, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 2, Array Id: 0, Name: Sorgente clock modem E1-A, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 3, Array Id: 0, Name: Modalità frame E1-B, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 4, Array Id: 0, Name: Sorgente clock modem E1-B, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 5, Array Id: 0, Name: Stato abilitazione link modem E1-A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 6, Array Id: 0, Name: Perdita di segnale modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 7, Array Id: 0, Name: Perdita di allineamento frame modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 8, Array Id: 0, Name: Rapporto bit/errore modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 9, Array Id: 0, Name: Segnale indicazione allarme modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 10, Array Id: 0, Name: Loop1 modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 11, Array Id: 0, Name: Stato abilitazione link modem E1-B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 12, Array Id: 0, Name: Perdita di segnale modem E1-B, Severity: 2, Value: 1, Description: Allarme=2\r\nField Id: 13, Array Id: 0, Name: Perdita di allineamento frame modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 14, Array Id: 0, Name: Rapporto bit/errore modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 15, Array Id: 0, Name: Segnale indicazione allarme modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 16, Array Id: 0, Name: Loop1 modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nStream Id: 4, Name: Informazioni porte switch, Severity: 0, Description: Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M690 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M690_V5_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174612225");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: Zeus M690 V5 Gallarate, Type: SYSNETM690V5, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB001209D\r\nDynamic Definition File: SYSNETZEUS_M690_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus M690 V5=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs2-T-MI01-Gallarate-LT, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus M690, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 38 giorni, 03:08:54.020, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.13.690, Description: Zeus M690 V5=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M690, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB001209D, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000040, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 08.10.12, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1203028, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.0.7, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 08.10.12, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 30, Array Id: 0, Name: Indirizzo IP scheda di rete #11, Severity: -255, Value: 10.104.95.1, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Maschera di sottorete scheda di rete #11, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 60, Array Id: 0, Name: Gateway scheda di rete #11, Severity: -255, Value: 10.104.95.201, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 44 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop ABCD abilitati=0;Auto=0;Master=0;B=0;256 Kb/s=0;5696 Kb/s=0;1536 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;192 Kb/s=0;5696 Kb/s=0;adattativa=-255;2 fili=0;Auto=0;Master=0;B=0;256 Kb/s=0;5696 Kb/s=0;1024 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;256 Kb/s=0;5696 Kb/s=0;adattativa=-255;2 fili=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Attivo=0;Nessuna=0;Valore ricevuto=-255;Disconnesso=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;n/d=-255;n/d=-255;Attivo=0;Nessuna=0;Valore ricevuto=-255;Disconnesso=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;n/d=-255;n/d=-255;Attivo=0;Nessuna=0;Valore ricevuto=-255;Disconnesso=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;n/d=-255;n/d=-255\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 15, Description: Loop ABCD abilitati=0\r\nField Id: 1, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Modalità loop A, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 3, Array Id: 0, Name: Annex loop A, Severity: 0, Value: 2, Description: B=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima modem loop A, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima modem loop A, Severity: 0, Value: 89, Description: 5696 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità modem loop A, Severity: 0, Value: 24, Description: 1536 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore loop A, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità loop B, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex loop B, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità minima modem loop B, Severity: 0, Value: 3, Description: 192 Kb/s=0\r\nField Id: 12, Array Id: 0, Name: Velocità massima modem loop B, Severity: 0, Value: 89, Description: 5696 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità modem loop B, Severity: -255, Value: 0, Description: adattativa=-255\r\nField Id: 14, Array Id: 0, Name: Connettore loop B, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 15, Array Id: 0, Name: PAM modulation loop C, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 16, Array Id: 0, Name: Modalità loop C, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 17, Array Id: 0, Name: Annex loop C, Severity: 0, Value: 2, Description: B=0\r\nField Id: 18, Array Id: 0, Name: Velocità minima modem loop C, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 19, Array Id: 0, Name: Velocità massima modem loop C, Severity: 0, Value: 89, Description: 5696 Kb/s=0\r\nField Id: 20, Array Id: 0, Name: Velocità modem loop C, Severity: 0, Value: 16, Description: 1024 Kb/s=0\r\nField Id: 21, Array Id: 0, Name: Connettore loop C, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 22, Array Id: 0, Name: PAM modulation loop D, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 23, Array Id: 0, Name: Modalità loop D, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 24, Array Id: 0, Name: Annex loop D, Severity: 0, Value: 2, Description: B=0\r\nField Id: 25, Array Id: 0, Name: Velocità minima modem loop D, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 26, Array Id: 0, Name: Velocità massima modem loop D, Severity: 0, Value: 89, Description: 5696 Kb/s=0\r\nField Id: 27, Array Id: 0, Name: Velocità modem loop D, Severity: -255, Value: 0, Description: adattativa=-255\r\nField Id: 28, Array Id: 0, Name: Connettore loop D, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 29, Array Id: 0, Name: Stato abilitazione loop A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 30, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 31, Array Id: 0, Name: Velocità loop A, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Stato connessione loop A, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 33, Array Id: 0, Name: Potenza Tx loop A, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Margine rumore modem loop A, Severity: -255, Value: 21, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Stato attenuazione modem loop A, Severity: -255, Value: 11, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Stato segment anomaly loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 37, Array Id: 0, Name: Stato segment defect loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 38, Array Id: 0, Name: Stato abilitazione loop B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 39, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: -1, Description: Nessuna=0\r\nField Id: 40, Array Id: 0, Name: Velocità loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione loop B, Severity: -255, Value: 3, Description: Disconnesso=-255\r\nField Id: 42, Array Id: 0, Name: Potenza Tx loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Margine rumore modem loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Stato attenuazione modem loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly loop B, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 46, Array Id: 0, Name: Stato segment defect loop B, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 47, Array Id: 0, Name: Stato abilitazione loop C, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 48, Array Id: 0, Name: PAM modulation loop C, Severity: 0, Value: -1, Description: Nessuna=0\r\nField Id: 49, Array Id: 0, Name: Velocità loop C, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Stato connessione loop C, Severity: -255, Value: 3, Description: Disconnesso=-255\r\nField Id: 51, Array Id: 0, Name: Potenza Tx loop C, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Margine rumore modem loop C, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 53, Array Id: 0, Name: Stato attenuazione modem loop C, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 54, Array Id: 0, Name: Stato segment anomaly loop C, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 55, Array Id: 0, Name: Stato segment defect loop C, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 56, Array Id: 0, Name: Stato abilitazione loop D, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 57, Array Id: 0, Name: PAM modulation loop D, Severity: 0, Value: -1, Description: Nessuna=0\r\nField Id: 58, Array Id: 0, Name: Velocità loop D, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione loop D, Severity: -255, Value: 3, Description: Disconnesso=-255\r\nField Id: 60, Array Id: 0, Name: Potenza Tx loop D, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Margine rumore modem loop D, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Stato attenuazione modem loop D, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 63, Array Id: 0, Name: Stato segment anomaly loop D, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 64, Array Id: 0, Name: Stato segment defect loop D, Severity: -255, Value: 0, Description: n/d=-255\r\n", deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus G384 V1

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G384_V1_001_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174475278");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zeus G384 V1 Pontremoli, Type: SYSNETG384V1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB00104FA\r\nDynamic Definition File: SYSNETZEUS_G384_V1_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-Pontremoli-Fornovo, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zs1-T-Pontremoli-Fornovo, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 8 giorni, 01:41:33.410, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-G384, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB00104FA, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: SY-STZ-G384, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 02.05, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 17.09.08, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A0805452, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 2.01.1.0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 2.2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: SY-SLP-0030, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 30.06.08, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.102.72.14, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.102.72.111, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 42 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni porte switch, Severity: 0, Description: Connesso=0;Auto=0;Connesso=0;Auto=0;Non connesso=0;Auto=0;Non connesso=0;Auto=0\r\nField Id: 0, Array Id: 0, Name: Stato connessione porta #1, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 1, Array Id: 0, Name: Configurazione porta #1, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Stato connessione porta #2, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 3, Array Id: 0, Name: Configurazione porta #2, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #3, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 5, Array Id: 0, Name: Configurazione porta #3, Severity: 0, Value: 1, Description: Auto=0\r\nField Id: 6, Array Id: 0, Name: Stato connessione porta #4, Severity: 0, Value: 0, Description: Non connesso=0\r\nField Id: 7, Array Id: 0, Name: Configurazione porta #4, Severity: 0, Value: 1, Description: Auto=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M190 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M190_V5_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174616345");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zeus M190 V5 Rapolano, Type: SYSNETM190V5, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB00122CF\r\nDynamic Definition File: SYSNETZEUS_M190_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Modello non riconosciuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-Fi01-Rapolano, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus M190, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 16 giorni, 03:44:41.520, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.13.190, Description: Modello non riconosciuto=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore entro i limiti normali=0\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-M190, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB00122CF, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000036, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 22.11.13, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1304721, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.0.7, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 24.09.13, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.104.111.25, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.104.111.113, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Temperatura scheda processore, Severity: 0, Value: 47 °C, Description: Valore entro i limiti normali=0\r\nStream Id: 3, Name: Informazioni WAN DSL, Severity: 0, Description: Loop AB abilitati=0;Auto=0;Master=0;B=0;256 Kb/s=0;2304 Kb/s=0;1024 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;256 Kb/s=0;2304 Kb/s=0;n/d=-255;2 fili=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0;Attivo=0;pam16=0;Valore ricevuto=-255;Connesso=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Non attivo=0;Non attivo=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 3, Description: Loop AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Modalità loop A, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 3, Array Id: 0, Name: Annex loop A, Severity: 0, Value: 2, Description: B=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima modem loop A, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima modem loop A, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità modem loop A, Severity: 0, Value: 16, Description: 1024 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore loop A, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità loop B, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex loop B, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità minima modem loop B, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 12, Array Id: 0, Name: Velocità massima modem loop B, Severity: 0, Value: 36, Description: 2304 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità modem loop B, Severity: -255, Value: 1, Description: n/d=-255\r\nField Id: 14, Array Id: 0, Name: Connettore loop B, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 15, Array Id: 0, Name: Stato abilitazione loop A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 16, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 17, Array Id: 0, Name: Velocità loop A, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 18, Array Id: 0, Name: Stato connessione loop A, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 19, Array Id: 0, Name: Potenza Tx loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Margine rumore modem loop A, Severity: -255, Value: 6, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Stato attenuazione modem loop A, Severity: -255, Value: 26, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Stato segment anomaly loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 23, Array Id: 0, Name: Stato segment defect loop A, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 24, Array Id: 0, Name: Stato abilitazione loop B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 25, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 1, Description: pam16=0\r\nField Id: 26, Array Id: 0, Name: Velocità loop B, Severity: -255, Value: 1536, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Stato connessione loop B, Severity: 0, Value: 2, Description: Connesso=0\r\nField Id: 28, Array Id: 0, Name: Potenza Tx loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 29, Array Id: 0, Name: Margine rumore modem loop B, Severity: -255, Value: 25, Description: Valore ricevuto=-255\r\nField Id: 30, Array Id: 0, Name: Stato attenuazione modem loop B, Severity: -255, Value: 17, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Stato segment anomaly loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\nField Id: 32, Array Id: 0, Name: Stato segment defect loop B, Severity: 0, Value: 2, Description: Non attivo=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus M6904 V1

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M6904_V1_001_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174467306");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zeus M6904 V1 Palermo Notarbartolo, Type: SYSNETM6904, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M6904_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus-M6904=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-PALERMO-NOTARBARTOLO, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ZEUS-M6904 (HW: 3.5, SW: 1.5.6), Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 77 giorni, 20:49:07.460, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.1.6904, Description: Zeus-M6904=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Standalone=0;Valore ricevuto=-255;Nessun errore=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome modello, Severity: -255, Value: ZEUS-M6904, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Device ID, Severity: -255, Value: 192_168_0_235, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione hardware, Severity: -255, Value: 3.5, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: -255, Value: 1.5.6, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data compilazione software, Severity: -255, Value: 15.5.2014, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Tipo modulo, Severity: 0, Value: 2, Description: Standalone=0\r\nField Id: 6, Array Id: 0, Name: Indirizzo subrack, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Codice errore, Severity: -255, Value: 0, Description: Nessun errore=-255\r\nField Id: 18, Array Id: 0, Name: Indirizzo IP scheda di rete #6, Severity: -255, Value: 10.102.40.234, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Maschera di sottorete scheda di rete #6, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 48, Array Id: 0, Name: Numero sensori temperatura, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome sensore #01, Severity: -255, Value: Board temperature, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Valore sensore #01, Severity: -255, Value: 49 °C, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Attiva=-255;Disattivata a basso livello=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Disattivata a basso livello=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Disattivata a basso livello=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #001, Severity: -255, Value: LAN1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #001, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #001, Severity: -255, Value: 100000000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #001, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #001, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #001, Severity: -255, Value: 52 giorni, 18:46:05.600, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #002, Severity: -255, Value: LAN2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #002, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #002, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #002, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #002, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #002, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #003, Severity: -255, Value: LAN3, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #003, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #003, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #003, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #003, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #003, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #004, Severity: -255, Value: LAN4, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #004, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #004, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #004, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #004, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #004, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #005, Severity: -255, Value: INT, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #005, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #005, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #005, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #005, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #005, Severity: -255, Value: 0 giorni, 00:00:12.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #006, Severity: -255, Value: WAN1, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #006, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #006, Severity: -255, Value: 1024000, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #006, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #006, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #006, Severity: -255, Value: 52 giorni, 19:19:53.770, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Nome porta #007, Severity: -255, Value: WAN2, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Tipo porta #007, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 39, Array Id: 0, Name: Velocità porta #007, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 40, Array Id: 0, Name: Stato attivazione porta #007, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione porta #007, Severity: -255, Value: 7, Description: Disattivata a basso livello=-255\r\nField Id: 42, Array Id: 0, Name: Ultima modifica porta #007, Severity: -255, Value: 0 giorni, 00:00:13.510, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Nome porta #008, Severity: -255, Value: WAN3, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Tipo porta #008, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 45, Array Id: 0, Name: Velocità porta #008, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 46, Array Id: 0, Name: Stato attivazione porta #008, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 47, Array Id: 0, Name: Stato connessione porta #008, Severity: -255, Value: 7, Description: Disattivata a basso livello=-255\r\nField Id: 48, Array Id: 0, Name: Ultima modifica porta #008, Severity: -255, Value: 0 giorni, 00:00:13.510, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome porta #009, Severity: -255, Value: WAN4, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Tipo porta #009, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 51, Array Id: 0, Name: Velocità porta #009, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Stato attivazione porta #009, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 53, Array Id: 0, Name: Stato connessione porta #009, Severity: -255, Value: 7, Description: Disattivata a basso livello=-255\r\nField Id: 54, Array Id: 0, Name: Ultima modifica porta #009, Severity: -255, Value: 0 giorni, 00:00:13.510, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #010, Severity: -255, Value: MWAN1, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #010, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #010, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #010, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #010, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #010, Severity: -255, Value: 0 giorni, 00:00:11.960, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #011, Severity: -255, Value: MWAN2, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #011, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #011, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #011, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #011, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 66, Array Id: 0, Name: Ultima modifica porta #011, Severity: -255, Value: 0 giorni, 00:00:11.960, Description: Valore ricevuto=-255\r\nField Id: 67, Array Id: 0, Name: Nome porta #012, Severity: -255, Value: DSL1, Description: Valore ricevuto=-255\r\nField Id: 68, Array Id: 0, Name: Tipo porta #012, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 69, Array Id: 0, Name: Velocità porta #012, Severity: -255, Value: 1032000, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Stato attivazione porta #012, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 71, Array Id: 0, Name: Stato connessione porta #012, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 72, Array Id: 0, Name: Ultima modifica porta #012, Severity: -255, Value: 52 giorni, 18:30:48.230, Description: Valore ricevuto=-255\r\nField Id: 73, Array Id: 0, Name: Nome porta #013, Severity: -255, Value: DSL2, Description: Valore ricevuto=-255\r\nField Id: 74, Array Id: 0, Name: Tipo porta #013, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 75, Array Id: 0, Name: Velocità porta #013, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 76, Array Id: 0, Name: Stato attivazione porta #013, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 77, Array Id: 0, Name: Stato connessione porta #013, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 78, Array Id: 0, Name: Ultima modifica porta #013, Severity: -255, Value: 0 giorni, 00:00:07.590, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Nome porta #014, Severity: -255, Value: DSL3, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Tipo porta #014, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 81, Array Id: 0, Name: Velocità porta #014, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 82, Array Id: 0, Name: Stato attivazione porta #014, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 83, Array Id: 0, Name: Stato connessione porta #014, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 84, Array Id: 0, Name: Ultima modifica porta #014, Severity: -255, Value: 0 giorni, 00:00:07.590, Description: Valore ricevuto=-255\r\nField Id: 85, Array Id: 0, Name: Nome porta #015, Severity: -255, Value: DSL4, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta #015, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta #015, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta #015, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta #015, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta #015, Severity: -255, Value: 0 giorni, 00:00:07.590, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni WAN DSL, Severity: -255, Description: Valore ricevuto=-255;modem=-255;Valore ricevuto=-255;master=-255;gshdsl_bis=-255;clockMode1=-255;pam16=-255;annexB=-255;Valore ricevuto=-255;up=-255;pam16=-255;annexB=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;slave=-255;gshdsl_bis=-255;clockMode1=-255;auto=-255;annexAB=-255;Valore ricevuto=-255;preactivation=-255;non disponibile=-255;non disponibile=-255;link non attivo=-255;link non attivo=-255;link non attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;master=-255;gshdsl_bis=-255;clockMode1=-255;pam16=-255;annexB=-255;Valore ricevuto=-255;preactivation=-255;non disponibile=-255;non disponibile=-255;link non attivo=-255;link non attivo=-255;link non attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;slave=-255;gshdsl_bis=-255;clockMode1=-255;auto=-255;annexAB=-255;Valore ricevuto=-255;preactivation=-255;non disponibile=-255;non disponibile=-255;link non attivo=-255;link non attivo=-255;link non attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero canali DSL, Severity: -255, Value: 4, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo unità DSL, Severity: -255, Value: 1, Description: modem=-255\r\nField Id: 2, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #01, Severity: -255, Value: 13, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Tipo di attivazione canale #01, Severity: -255, Value: 1, Description: master=-255\r\nField Id: 4, Array Id: 0, Name: Modalità G.SHDSL canale #01, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 5, Array Id: 0, Name: Modalità clock G.SHDSL canale #01, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 6, Array Id: 0, Name: Codice linea configurato canale #01, Severity: -255, Value: 4, Description: pam16=-255\r\nField Id: 7, Array Id: 0, Name: Maschera PSD (annex) configurata canale #01, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 8, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #01, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Stato attivazione canale #01, Severity: -255, Value: 4, Description: up=-255\r\nField Id: 10, Array Id: 0, Name: Codice linea corrente canale #01, Severity: -255, Value: 4, Description: pam16=-255\r\nField Id: 11, Array Id: 0, Name: Maschera PSD (annex) corrente canale #01, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 12, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #01, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #01, Severity: -255, Value: 1032, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #01, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Noise margin (dB) corrente canale #01, Severity: -255, Value: 24, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Power backoff (dB) corrente canale #01, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 17, Array Id: 0, Name: Far end power backoff (dB) corrente canale #01, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 18, Array Id: 0, Name: Loop attenuation canale #01, Severity: -255, Value: 9, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: EOC number canale #01, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Numero di rigeneratori sul link canale #01, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 23, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #02, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 24, Array Id: 0, Name: Tipo di attivazione canale #02, Severity: -255, Value: 2, Description: slave=-255\r\nField Id: 25, Array Id: 0, Name: Modalità G.SHDSL canale #02, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 26, Array Id: 0, Name: Modalità clock G.SHDSL canale #02, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 27, Array Id: 0, Name: Codice linea configurato canale #02, Severity: -255, Value: 1, Description: auto=-255\r\nField Id: 28, Array Id: 0, Name: Maschera PSD (annex) configurata canale #02, Severity: -255, Value: 3, Description: annexAB=-255\r\nField Id: 29, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 30, Array Id: 0, Name: Stato attivazione canale #02, Severity: -255, Value: 2, Description: preactivation=-255\r\nField Id: 31, Array Id: 0, Name: Codice linea corrente canale #02, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 32, Array Id: 0, Name: Maschera PSD (annex) corrente canale #02, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 33, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #02, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 34, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #02, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 35, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #02, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 36, Array Id: 0, Name: Noise margin (dB) corrente canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Power backoff (dB) corrente canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Far end power backoff (dB) corrente canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 39, Array Id: 0, Name: Loop attenuation canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: EOC number canale #02, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Numero di rigeneratori sul link canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #03, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Tipo di attivazione canale #03, Severity: -255, Value: 1, Description: master=-255\r\nField Id: 46, Array Id: 0, Name: Modalità G.SHDSL canale #03, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 47, Array Id: 0, Name: Modalità clock G.SHDSL canale #03, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 48, Array Id: 0, Name: Codice linea configurato canale #03, Severity: -255, Value: 4, Description: pam16=-255\r\nField Id: 49, Array Id: 0, Name: Maschera PSD (annex) configurata canale #03, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 50, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #03, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 51, Array Id: 0, Name: Stato attivazione canale #03, Severity: -255, Value: 2, Description: preactivation=-255\r\nField Id: 52, Array Id: 0, Name: Codice linea corrente canale #03, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 53, Array Id: 0, Name: Maschera PSD (annex) corrente canale #03, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 54, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 55, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 56, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 57, Array Id: 0, Name: Noise margin (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Power backoff (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 59, Array Id: 0, Name: Far end power backoff (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 60, Array Id: 0, Name: Loop attenuation canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: EOC number canale #03, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Numero di rigeneratori sul link canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 65, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #04, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 66, Array Id: 0, Name: Tipo di attivazione canale #04, Severity: -255, Value: 2, Description: slave=-255\r\nField Id: 67, Array Id: 0, Name: Modalità G.SHDSL canale #04, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 68, Array Id: 0, Name: Modalità clock G.SHDSL canale #04, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 69, Array Id: 0, Name: Codice linea configurato canale #04, Severity: -255, Value: 1, Description: auto=-255\r\nField Id: 70, Array Id: 0, Name: Maschera PSD (annex) configurata canale #04, Severity: -255, Value: 3, Description: annexAB=-255\r\nField Id: 71, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 72, Array Id: 0, Name: Stato attivazione canale #04, Severity: -255, Value: 2, Description: preactivation=-255\r\nField Id: 73, Array Id: 0, Name: Codice linea corrente canale #04, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 74, Array Id: 0, Name: Maschera PSD (annex) corrente canale #04, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 75, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 76, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 77, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 78, Array Id: 0, Name: Noise margin (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Power backoff (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Far end power backoff (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 81, Array Id: 0, Name: Loop attenuation canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 82, Array Id: 0, Name: EOC number canale #04, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 83, Array Id: 0, Name: Numero di rigeneratori sul link canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M6904_V1_002_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174467307");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zeus M6904 V1 Francia, Type: SYSNETM6904, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M6904_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus-M6904=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-PA01-FRANCIA, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ZEUS-M6904 (HW: 3.5, SW: 1.5.6), Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 25 giorni, 22:23:51.460, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.1.6904, Description: Zeus-M6904=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Standalone=0;Valore ricevuto=-255;Nessun errore=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome modello, Severity: -255, Value: ZEUS-M6904, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Device ID, Severity: -255, Value: 192_168_0_235, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione hardware, Severity: -255, Value: 3.5, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: -255, Value: 1.5.6, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data compilazione software, Severity: -255, Value: 15.5.2014, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Tipo modulo, Severity: 0, Value: 2, Description: Standalone=0\r\nField Id: 6, Array Id: 0, Name: Indirizzo subrack, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Codice errore, Severity: -255, Value: 0, Description: Nessun errore=-255\r\nField Id: 18, Array Id: 0, Name: Indirizzo IP scheda di rete #6, Severity: -255, Value: 10.102.40.235, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Maschera di sottorete scheda di rete #6, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 48, Array Id: 0, Name: Numero sensori temperatura, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome sensore #01, Severity: -255, Value: Board temperature, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Valore sensore #01, Severity: -255, Value: 42 °C, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Disattivata a basso livello=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Disattivata a basso livello=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #001, Severity: -255, Value: LAN1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #001, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #001, Severity: -255, Value: 100000000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #001, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #001, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #001, Severity: -255, Value: 0 giorni, 00:34:02.900, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #002, Severity: -255, Value: LAN2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #002, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #002, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #002, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #002, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #002, Severity: -255, Value: 0 giorni, 00:26:22.100, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #003, Severity: -255, Value: LAN3, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #003, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #003, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #003, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #003, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #003, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #004, Severity: -255, Value: LAN4, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #004, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #004, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #004, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #004, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #004, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #005, Severity: -255, Value: INT, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #005, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #005, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #005, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #005, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #005, Severity: -255, Value: 0 giorni, 00:34:00.990, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #006, Severity: -255, Value: WAN1, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #006, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #006, Severity: -255, Value: 1024000, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #006, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #006, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #006, Severity: -255, Value: 0 giorni, 00:34:02.090, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Nome porta #007, Severity: -255, Value: WAN2, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Tipo porta #007, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 39, Array Id: 0, Name: Velocità porta #007, Severity: -255, Value: 1024000, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Stato attivazione porta #007, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione porta #007, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 42, Array Id: 0, Name: Ultima modifica porta #007, Severity: -255, Value: 0 giorni, 20:03:07.950, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Nome porta #008, Severity: -255, Value: WAN3, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Tipo porta #008, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 45, Array Id: 0, Name: Velocità porta #008, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 46, Array Id: 0, Name: Stato attivazione porta #008, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 47, Array Id: 0, Name: Stato connessione porta #008, Severity: -255, Value: 7, Description: Disattivata a basso livello=-255\r\nField Id: 48, Array Id: 0, Name: Ultima modifica porta #008, Severity: -255, Value: 0 giorni, 00:34:02.090, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome porta #009, Severity: -255, Value: WAN4, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Tipo porta #009, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 51, Array Id: 0, Name: Velocità porta #009, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Stato attivazione porta #009, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 53, Array Id: 0, Name: Stato connessione porta #009, Severity: -255, Value: 7, Description: Disattivata a basso livello=-255\r\nField Id: 54, Array Id: 0, Name: Ultima modifica porta #009, Severity: -255, Value: 0 giorni, 00:34:02.090, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #010, Severity: -255, Value: MWAN1, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #010, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #010, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #010, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #010, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #010, Severity: -255, Value: 0 giorni, 00:00:11.960, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #011, Severity: -255, Value: MWAN2, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #011, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #011, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #011, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #011, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 66, Array Id: 0, Name: Ultima modifica porta #011, Severity: -255, Value: 0 giorni, 00:00:11.960, Description: Valore ricevuto=-255\r\nField Id: 67, Array Id: 0, Name: Nome porta #012, Severity: -255, Value: DSL1, Description: Valore ricevuto=-255\r\nField Id: 68, Array Id: 0, Name: Tipo porta #012, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 69, Array Id: 0, Name: Velocità porta #012, Severity: -255, Value: 1032000, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Stato attivazione porta #012, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 71, Array Id: 0, Name: Stato connessione porta #012, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 72, Array Id: 0, Name: Ultima modifica porta #012, Severity: -255, Value: 0 giorni, 00:01:12.680, Description: Valore ricevuto=-255\r\nField Id: 73, Array Id: 0, Name: Nome porta #013, Severity: -255, Value: DSL2, Description: Valore ricevuto=-255\r\nField Id: 74, Array Id: 0, Name: Tipo porta #013, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 75, Array Id: 0, Name: Velocità porta #013, Severity: -255, Value: 1032000, Description: Valore ricevuto=-255\r\nField Id: 76, Array Id: 0, Name: Stato attivazione porta #013, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 77, Array Id: 0, Name: Stato connessione porta #013, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 78, Array Id: 0, Name: Ultima modifica porta #013, Severity: -255, Value: 0 giorni, 20:03:03.990, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Nome porta #014, Severity: -255, Value: DSL3, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Tipo porta #014, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 81, Array Id: 0, Name: Velocità porta #014, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 82, Array Id: 0, Name: Stato attivazione porta #014, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 83, Array Id: 0, Name: Stato connessione porta #014, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 84, Array Id: 0, Name: Ultima modifica porta #014, Severity: -255, Value: 0 giorni, 00:00:07.590, Description: Valore ricevuto=-255\r\nField Id: 85, Array Id: 0, Name: Nome porta #015, Severity: -255, Value: DSL4, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta #015, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta #015, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta #015, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta #015, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta #015, Severity: -255, Value: 0 giorni, 00:00:07.590, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni WAN DSL, Severity: -255, Description: Valore ricevuto=-255;modem=-255;Valore ricevuto=-255;master=-255;gshdsl_bis=-255;clockMode1=-255;pam32=-255;annexB=-255;Valore ricevuto=-255;up=-255;pam32=-255;annexB=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;slave=-255;gshdsl_bis=-255;clockMode1=-255;auto=-255;annexAB=-255;Valore ricevuto=-255;up=-255;pam16=-255;annexB=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;master=-255;gshdsl_bis=-255;clockMode1=-255;pam32=-255;annexB=-255;Valore ricevuto=-255;preactivation=-255;non disponibile=-255;non disponibile=-255;link non attivo=-255;link non attivo=-255;link non attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;slave=-255;gshdsl_bis=-255;clockMode1=-255;auto=-255;annexAB=-255;Valore ricevuto=-255;preactivation=-255;non disponibile=-255;non disponibile=-255;link non attivo=-255;link non attivo=-255;link non attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero canali DSL, Severity: -255, Value: 4, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo unità DSL, Severity: -255, Value: 1, Description: modem=-255\r\nField Id: 2, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #01, Severity: -255, Value: 13, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Tipo di attivazione canale #01, Severity: -255, Value: 1, Description: master=-255\r\nField Id: 4, Array Id: 0, Name: Modalità G.SHDSL canale #01, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 5, Array Id: 0, Name: Modalità clock G.SHDSL canale #01, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 6, Array Id: 0, Name: Codice linea configurato canale #01, Severity: -255, Value: 5, Description: pam32=-255\r\nField Id: 7, Array Id: 0, Name: Maschera PSD (annex) configurata canale #01, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 8, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #01, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Stato attivazione canale #01, Severity: -255, Value: 4, Description: up=-255\r\nField Id: 10, Array Id: 0, Name: Codice linea corrente canale #01, Severity: -255, Value: 5, Description: pam32=-255\r\nField Id: 11, Array Id: 0, Name: Maschera PSD (annex) corrente canale #01, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 12, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #01, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #01, Severity: -255, Value: 1032, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #01, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Noise margin (dB) corrente canale #01, Severity: -255, Value: 20, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Power backoff (dB) corrente canale #01, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 17, Array Id: 0, Name: Far end power backoff (dB) corrente canale #01, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 18, Array Id: 0, Name: Loop attenuation canale #01, Severity: -255, Value: 4, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: EOC number canale #01, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Numero di rigeneratori sul link canale #01, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 23, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #02, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 24, Array Id: 0, Name: Tipo di attivazione canale #02, Severity: -255, Value: 2, Description: slave=-255\r\nField Id: 25, Array Id: 0, Name: Modalità G.SHDSL canale #02, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 26, Array Id: 0, Name: Modalità clock G.SHDSL canale #02, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 27, Array Id: 0, Name: Codice linea configurato canale #02, Severity: -255, Value: 1, Description: auto=-255\r\nField Id: 28, Array Id: 0, Name: Maschera PSD (annex) configurata canale #02, Severity: -255, Value: 3, Description: annexAB=-255\r\nField Id: 29, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 30, Array Id: 0, Name: Stato attivazione canale #02, Severity: -255, Value: 4, Description: up=-255\r\nField Id: 31, Array Id: 0, Name: Codice linea corrente canale #02, Severity: -255, Value: 4, Description: pam16=-255\r\nField Id: 32, Array Id: 0, Name: Maschera PSD (annex) corrente canale #02, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 33, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #02, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #02, Severity: -255, Value: 1032, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #02, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Noise margin (dB) corrente canale #02, Severity: -255, Value: 25, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Power backoff (dB) corrente canale #02, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Far end power backoff (dB) corrente canale #02, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 39, Array Id: 0, Name: Loop attenuation canale #02, Severity: -255, Value: 9, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: EOC number canale #02, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Numero di rigeneratori sul link canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #03, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Tipo di attivazione canale #03, Severity: -255, Value: 1, Description: master=-255\r\nField Id: 46, Array Id: 0, Name: Modalità G.SHDSL canale #03, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 47, Array Id: 0, Name: Modalità clock G.SHDSL canale #03, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 48, Array Id: 0, Name: Codice linea configurato canale #03, Severity: -255, Value: 5, Description: pam32=-255\r\nField Id: 49, Array Id: 0, Name: Maschera PSD (annex) configurata canale #03, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 50, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #03, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 51, Array Id: 0, Name: Stato attivazione canale #03, Severity: -255, Value: 2, Description: preactivation=-255\r\nField Id: 52, Array Id: 0, Name: Codice linea corrente canale #03, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 53, Array Id: 0, Name: Maschera PSD (annex) corrente canale #03, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 54, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 55, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 56, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 57, Array Id: 0, Name: Noise margin (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Power backoff (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 59, Array Id: 0, Name: Far end power backoff (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 60, Array Id: 0, Name: Loop attenuation canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: EOC number canale #03, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Numero di rigeneratori sul link canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 65, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #04, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 66, Array Id: 0, Name: Tipo di attivazione canale #04, Severity: -255, Value: 2, Description: slave=-255\r\nField Id: 67, Array Id: 0, Name: Modalità G.SHDSL canale #04, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 68, Array Id: 0, Name: Modalità clock G.SHDSL canale #04, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 69, Array Id: 0, Name: Codice linea configurato canale #04, Severity: -255, Value: 1, Description: auto=-255\r\nField Id: 70, Array Id: 0, Name: Maschera PSD (annex) configurata canale #04, Severity: -255, Value: 3, Description: annexAB=-255\r\nField Id: 71, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 72, Array Id: 0, Name: Stato attivazione canale #04, Severity: -255, Value: 2, Description: preactivation=-255\r\nField Id: 73, Array Id: 0, Name: Codice linea corrente canale #04, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 74, Array Id: 0, Name: Maschera PSD (annex) corrente canale #04, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 75, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 76, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 77, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 78, Array Id: 0, Name: Noise margin (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Power backoff (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Far end power backoff (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 81, Array Id: 0, Name: Loop attenuation canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 82, Array Id: 0, Name: EOC number canale #04, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 83, Array Id: 0, Name: Numero di rigeneratori sul link canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_M6904_V1_003_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174467308");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zeus M6904 V1 S. Lorenzo Colli, Type: SYSNETM6904, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: SYSNETZEUS_M6904_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus-M6904=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs1-T-PA01-SLORENZO, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ZEUS-M6904 (HW: 3.5, SW: 1.5.6), Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 76 giorni, 23:28:39.030, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.1.6904, Description: Zeus-M6904=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Standalone=0;Valore ricevuto=-255;Nessun errore=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome modello, Severity: -255, Value: ZEUS-M6904, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Device ID, Severity: -255, Value: 192_168_0_235, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Versione hardware, Severity: -255, Value: 3.5, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: -255, Value: 1.5.6, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data compilazione software, Severity: -255, Value: 15.5.2014, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Tipo modulo, Severity: 0, Value: 2, Description: Standalone=0\r\nField Id: 6, Array Id: 0, Name: Indirizzo subrack, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Codice errore, Severity: -255, Value: 0, Description: Nessun errore=-255\r\nField Id: 18, Array Id: 0, Name: Indirizzo IP scheda di rete #6, Severity: -255, Value: 10.102.40.236, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Maschera di sottorete scheda di rete #6, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 48, Array Id: 0, Name: Numero sensori temperatura, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome sensore #01, Severity: -255, Value: Board temperature, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Valore sensore #01, Severity: -255, Value: 47 °C, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Disattivata a basso livello=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Attiva=-255;Disattivata a basso livello=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Valore ricevuto=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;Valore ricevuto=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #001, Severity: -255, Value: LAN1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #001, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #001, Severity: -255, Value: 100000000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #001, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #001, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #001, Severity: -255, Value: 50 giorni, 22:58:57.600, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #002, Severity: -255, Value: LAN2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #002, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #002, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #002, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #002, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #002, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #003, Severity: -255, Value: LAN3, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #003, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #003, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #003, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #003, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #003, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #004, Severity: -255, Value: LAN4, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #004, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #004, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #004, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #004, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #004, Severity: -255, Value: 0 giorni, 00:00:12.040, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #005, Severity: -255, Value: INT, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #005, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #005, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #005, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #005, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #005, Severity: -255, Value: 0 giorni, 00:00:12.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #006, Severity: -255, Value: WAN1, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #006, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #006, Severity: -255, Value: 1024000, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #006, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #006, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #006, Severity: -255, Value: 50 giorni, 22:50:42.270, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Nome porta #007, Severity: -255, Value: WAN2, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Tipo porta #007, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 39, Array Id: 0, Name: Velocità porta #007, Severity: -255, Value: 1024000, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Stato attivazione porta #007, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione porta #007, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 42, Array Id: 0, Name: Ultima modifica porta #007, Severity: -255, Value: 51 giorni, 01:38:22.340, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Nome porta #008, Severity: -255, Value: WAN3, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Tipo porta #008, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 45, Array Id: 0, Name: Velocità porta #008, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 46, Array Id: 0, Name: Stato attivazione porta #008, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 47, Array Id: 0, Name: Stato connessione porta #008, Severity: -255, Value: 7, Description: Disattivata a basso livello=-255\r\nField Id: 48, Array Id: 0, Name: Ultima modifica porta #008, Severity: -255, Value: 0 giorni, 00:00:13.510, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome porta #009, Severity: -255, Value: WAN4, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Tipo porta #009, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 51, Array Id: 0, Name: Velocità porta #009, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Stato attivazione porta #009, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 53, Array Id: 0, Name: Stato connessione porta #009, Severity: -255, Value: 7, Description: Disattivata a basso livello=-255\r\nField Id: 54, Array Id: 0, Name: Ultima modifica porta #009, Severity: -255, Value: 0 giorni, 00:00:13.510, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #010, Severity: -255, Value: MWAN1, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #010, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #010, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #010, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #010, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #010, Severity: -255, Value: 0 giorni, 00:00:11.960, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #011, Severity: -255, Value: MWAN2, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #011, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #011, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #011, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #011, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 66, Array Id: 0, Name: Ultima modifica porta #011, Severity: -255, Value: 0 giorni, 00:00:11.960, Description: Valore ricevuto=-255\r\nField Id: 67, Array Id: 0, Name: Nome porta #012, Severity: -255, Value: DSL1, Description: Valore ricevuto=-255\r\nField Id: 68, Array Id: 0, Name: Tipo porta #012, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 69, Array Id: 0, Name: Velocità porta #012, Severity: -255, Value: 1032000, Description: Valore ricevuto=-255\r\nField Id: 70, Array Id: 0, Name: Stato attivazione porta #012, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 71, Array Id: 0, Name: Stato connessione porta #012, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 72, Array Id: 0, Name: Ultima modifica porta #012, Severity: -255, Value: 50 giorni, 22:50:40.990, Description: Valore ricevuto=-255\r\nField Id: 73, Array Id: 0, Name: Nome porta #013, Severity: -255, Value: DSL2, Description: Valore ricevuto=-255\r\nField Id: 74, Array Id: 0, Name: Tipo porta #013, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 75, Array Id: 0, Name: Velocità porta #013, Severity: -255, Value: 1032000, Description: Valore ricevuto=-255\r\nField Id: 76, Array Id: 0, Name: Stato attivazione porta #013, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 77, Array Id: 0, Name: Stato connessione porta #013, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 78, Array Id: 0, Name: Ultima modifica porta #013, Severity: -255, Value: 51 giorni, 01:05:29.750, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Nome porta #014, Severity: -255, Value: DSL3, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Tipo porta #014, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 81, Array Id: 0, Name: Velocità porta #014, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 82, Array Id: 0, Name: Stato attivazione porta #014, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 83, Array Id: 0, Name: Stato connessione porta #014, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 84, Array Id: 0, Name: Ultima modifica porta #014, Severity: -255, Value: 0 giorni, 00:00:07.590, Description: Valore ricevuto=-255\r\nField Id: 85, Array Id: 0, Name: Nome porta #015, Severity: -255, Value: DSL4, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta #015, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta #015, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta #015, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta #015, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta #015, Severity: -255, Value: 0 giorni, 00:00:07.590, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni WAN DSL, Severity: -255, Description: Valore ricevuto=-255;modem=-255;Valore ricevuto=-255;master=-255;gshdsl_bis=-255;clockMode1=-255;pam16=-255;annexB=-255;Valore ricevuto=-255;up=-255;pam16=-255;annexB=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;slave=-255;gshdsl_bis=-255;clockMode1=-255;auto=-255;annexAB=-255;Valore ricevuto=-255;up=-255;pam32=-255;annexB=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;master=-255;gshdsl_bis=-255;clockMode1=-255;pam16=-255;annexB=-255;Valore ricevuto=-255;preactivation=-255;non disponibile=-255;non disponibile=-255;link non attivo=-255;link non attivo=-255;link non attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;slave=-255;gshdsl_bis=-255;clockMode1=-255;auto=-255;annexAB=-255;Valore ricevuto=-255;preactivation=-255;non disponibile=-255;non disponibile=-255;link non attivo=-255;link non attivo=-255;link non attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero canali DSL, Severity: -255, Value: 4, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo unità DSL, Severity: -255, Value: 1, Description: modem=-255\r\nField Id: 2, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #01, Severity: -255, Value: 13, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Tipo di attivazione canale #01, Severity: -255, Value: 1, Description: master=-255\r\nField Id: 4, Array Id: 0, Name: Modalità G.SHDSL canale #01, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 5, Array Id: 0, Name: Modalità clock G.SHDSL canale #01, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 6, Array Id: 0, Name: Codice linea configurato canale #01, Severity: -255, Value: 4, Description: pam16=-255\r\nField Id: 7, Array Id: 0, Name: Maschera PSD (annex) configurata canale #01, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 8, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #01, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Stato attivazione canale #01, Severity: -255, Value: 4, Description: up=-255\r\nField Id: 10, Array Id: 0, Name: Codice linea corrente canale #01, Severity: -255, Value: 4, Description: pam16=-255\r\nField Id: 11, Array Id: 0, Name: Maschera PSD (annex) corrente canale #01, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 12, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #01, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #01, Severity: -255, Value: 1032, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #01, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Noise margin (dB) corrente canale #01, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Power backoff (dB) corrente canale #01, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 17, Array Id: 0, Name: Far end power backoff (dB) corrente canale #01, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 18, Array Id: 0, Name: Loop attenuation canale #01, Severity: -255, Value: 11, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: EOC number canale #01, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Numero di rigeneratori sul link canale #01, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 23, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #02, Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 24, Array Id: 0, Name: Tipo di attivazione canale #02, Severity: -255, Value: 2, Description: slave=-255\r\nField Id: 25, Array Id: 0, Name: Modalità G.SHDSL canale #02, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 26, Array Id: 0, Name: Modalità clock G.SHDSL canale #02, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 27, Array Id: 0, Name: Codice linea configurato canale #02, Severity: -255, Value: 1, Description: auto=-255\r\nField Id: 28, Array Id: 0, Name: Maschera PSD (annex) configurata canale #02, Severity: -255, Value: 3, Description: annexAB=-255\r\nField Id: 29, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 30, Array Id: 0, Name: Stato attivazione canale #02, Severity: -255, Value: 4, Description: up=-255\r\nField Id: 31, Array Id: 0, Name: Codice linea corrente canale #02, Severity: -255, Value: 5, Description: pam32=-255\r\nField Id: 32, Array Id: 0, Name: Maschera PSD (annex) corrente canale #02, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 33, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #02, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #02, Severity: -255, Value: 1032, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #02, Severity: -255, Value: 1024, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Noise margin (dB) corrente canale #02, Severity: -255, Value: 19, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Power backoff (dB) corrente canale #02, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Far end power backoff (dB) corrente canale #02, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 39, Array Id: 0, Name: Loop attenuation canale #02, Severity: -255, Value: 4, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: EOC number canale #02, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Numero di rigeneratori sul link canale #02, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #03, Severity: -255, Value: 15, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Tipo di attivazione canale #03, Severity: -255, Value: 1, Description: master=-255\r\nField Id: 46, Array Id: 0, Name: Modalità G.SHDSL canale #03, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 47, Array Id: 0, Name: Modalità clock G.SHDSL canale #03, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 48, Array Id: 0, Name: Codice linea configurato canale #03, Severity: -255, Value: 4, Description: pam16=-255\r\nField Id: 49, Array Id: 0, Name: Maschera PSD (annex) configurata canale #03, Severity: -255, Value: 2, Description: annexB=-255\r\nField Id: 50, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #03, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 51, Array Id: 0, Name: Stato attivazione canale #03, Severity: -255, Value: 2, Description: preactivation=-255\r\nField Id: 52, Array Id: 0, Name: Codice linea corrente canale #03, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 53, Array Id: 0, Name: Maschera PSD (annex) corrente canale #03, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 54, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 55, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 56, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #03, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 57, Array Id: 0, Name: Noise margin (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 58, Array Id: 0, Name: Power backoff (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 59, Array Id: 0, Name: Far end power backoff (dB) corrente canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 60, Array Id: 0, Name: Loop attenuation canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: EOC number canale #03, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Numero di rigeneratori sul link canale #03, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 65, Array Id: 0, Name: Indice del canale DSL nelle interfacce di rete #04, Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 66, Array Id: 0, Name: Tipo di attivazione canale #04, Severity: -255, Value: 2, Description: slave=-255\r\nField Id: 67, Array Id: 0, Name: Modalità G.SHDSL canale #04, Severity: -255, Value: 2, Description: gshdsl_bis=-255\r\nField Id: 68, Array Id: 0, Name: Modalità clock G.SHDSL canale #04, Severity: -255, Value: 1, Description: clockMode1=-255\r\nField Id: 69, Array Id: 0, Name: Codice linea configurato canale #04, Severity: -255, Value: 1, Description: auto=-255\r\nField Id: 70, Array Id: 0, Name: Maschera PSD (annex) configurata canale #04, Severity: -255, Value: 3, Description: annexAB=-255\r\nField Id: 71, Array Id: 0, Name: Base data rate (64kbit/s) configurata canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 72, Array Id: 0, Name: Stato attivazione canale #04, Severity: -255, Value: 2, Description: preactivation=-255\r\nField Id: 73, Array Id: 0, Name: Codice linea corrente canale #04, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 74, Array Id: 0, Name: Maschera PSD (annex) corrente canale #04, Severity: -255, Value: 0, Description: non disponibile=-255\r\nField Id: 75, Array Id: 0, Name: Base data rate (64 kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 76, Array Id: 0, Name: Working data rate (kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 77, Array Id: 0, Name: Payload rate (kbit/s) corrente canale #04, Severity: -255, Value: 0, Description: link non attivo=-255\r\nField Id: 78, Array Id: 0, Name: Noise margin (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Power backoff (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Far end power backoff (dB) corrente canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 81, Array Id: 0, Name: Loop attenuation canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 82, Array Id: 0, Name: EOC number canale #04, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 83, Array Id: 0, Name: Numero di rigeneratori sul link canale #04, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus G180 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_G180_V5_001_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174616344");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zeus G180 V5 Ponte a Elsa, Type: SYSNETG180V5, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB00122B3\r\nDynamic Definition File: SYSNETZEUS_G180_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus G180 V5=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: ZS1-T-FI01-PONTEAELSA, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus G180, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 93 giorni, 02:00:20.600, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.7.180, Description: Zeus G180 V5=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-G180, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB00122B3, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000041, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 07.11.13, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1304693, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.1, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 24.09.13, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Indirizzo IP scheda di rete #9, Severity: -255, Value: 10.104.111.24, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Maschera di sottorete scheda di rete #9, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Gateway scheda di rete #9, Severity: -255, Value: 10.104.111.112, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Link E1-AB abilitati=0;Unframed=0;Esterna=0;Unframed=0;Esterna=0;Attivo=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Attivo=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia G703, Severity: 0, Value: 3, Description: Link E1-AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità frame E1-A, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 2, Array Id: 0, Name: Sorgente clock modem E1-A, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 3, Array Id: 0, Name: Modalità frame E1-B, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 4, Array Id: 0, Name: Sorgente clock modem E1-B, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 5, Array Id: 0, Name: Stato abilitazione link modem E1-A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 6, Array Id: 0, Name: Perdita di segnale modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 7, Array Id: 0, Name: Perdita di allineamento frame modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 8, Array Id: 0, Name: Rapporto bit/errore modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 9, Array Id: 0, Name: Segnale indicazione allarme modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 10, Array Id: 0, Name: Loop1 modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 11, Array Id: 0, Name: Stato abilitazione link modem E1-B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 12, Array Id: 0, Name: Perdita di segnale modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 13, Array Id: 0, Name: Perdita di allineamento frame modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 14, Array Id: 0, Name: Rapporto bit/errore modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 15, Array Id: 0, Name: Segnale indicazione allarme modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 16, Array Id: 0, Name: Loop1 modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Sysnet Telecomunicazioni - Zeus S190 V5

        [Test]
        public void DumpCompleto_definitionSYTLCZEUS_S190_V5_000_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174614273");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Zeus S190 V5 Voghera, Type: SYSNETS190V5, Community: public, SNMP Version: V1, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 000BB001223B\r\nDynamic Definition File: SYSNETZEUS_S190_V5_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Zeus S190 V5=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Zs2-P-Voghera-Lt, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Zeus S190, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 29 giorni, 02:47:41.900, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato Sysnet, Severity: -255, Value: .1.3.6.1.4.1.15078.44.19.190, Description: Zeus S190 V5=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Nome sistema, Severity: -255, Value: ZEUS-S190, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Numero seriale, Severity: -255, Value: 000BB001223B, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Codice prodotto, Severity: -255, Value: STZ000032, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Codice assemblaggio, Severity: -255, Value: 10000, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Data costruzione, Severity: -255, Value: 02.07.13, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Numero di serie scheda processore, Severity: -255, Value: A1302926, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Release hardware scheda processore, Severity: -255, Value: 3.00.3.1, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Release software scheda processore, Severity: -255, Value: 3.0.7, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Codice prodotto scheda processore, Severity: -255, Value: 3MC000004, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Data costruzione scheda processore, Severity: -255, Value: 24.06.13, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Indirizzo IP scheda di rete #1, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Maschera di sottorete scheda di rete #1, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 40, Array Id: 0, Name: Temperatura scheda processore, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni WAN G703, Severity: 2, Description: Link E1-AB abilitati=0;Unframed=0;Esterna=0;Unframed=0;Esterna=0;Attivo=0;Normale=0;Normale=0;Normale=0;Normale=0;Normale=0;Attivo=0;Allarme=2;Normale=0;Normale=0;Normale=0;Normale=0\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia G703, Severity: 0, Value: 3, Description: Link E1-AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: Modalità frame E1-A, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 2, Array Id: 0, Name: Sorgente clock modem E1-A, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 3, Array Id: 0, Name: Modalità frame E1-B, Severity: 0, Value: 0, Description: Unframed=0\r\nField Id: 4, Array Id: 0, Name: Sorgente clock modem E1-B, Severity: 0, Value: 1, Description: Esterna=0\r\nField Id: 5, Array Id: 0, Name: Stato abilitazione link modem E1-A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 6, Array Id: 0, Name: Perdita di segnale modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 7, Array Id: 0, Name: Perdita di allineamento frame modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 8, Array Id: 0, Name: Rapporto bit/errore modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 9, Array Id: 0, Name: Segnale indicazione allarme modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 10, Array Id: 0, Name: Loop1 modem E1-A, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 11, Array Id: 0, Name: Stato abilitazione link modem E1-B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 12, Array Id: 0, Name: Perdita di segnale modem E1-B, Severity: 2, Value: 1, Description: Allarme=2\r\nField Id: 13, Array Id: 0, Name: Perdita di allineamento frame modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 14, Array Id: 0, Name: Rapporto bit/errore modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 15, Array Id: 0, Name: Segnale indicazione allarme modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nField Id: 16, Array Id: 0, Name: Loop1 modem E1-B, Severity: 0, Value: 0, Description: Normale=0\r\nStream Id: 4, Name: Informazioni WAN DSL, Severity: 0, Description: Loop AB abilitati=0;Auto=0;Master=0;B=0;256 Kb/s=0;5696 Kb/s=0;1536 Kb/s=0;2 fili=0;Auto=0;Slave=0;B=0;256 Kb/s=0;5696 Kb/s=0;adattativa=-255;2 fili=0;Attivo=0;Nessuna=0;Valore ricevuto=-255;Disconnesso=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;n/d=-255;n/d=-255;Attivo=0;Nessuna=0;Valore ricevuto=-255;Disconnesso=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;n/d=-255;n/d=-255\r\nField Id: 0, Array Id: 0, Name: Abilitazione interfaccia DSL, Severity: 0, Value: 3, Description: Loop AB abilitati=0\r\nField Id: 1, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 2, Array Id: 0, Name: Modalità loop A, Severity: 0, Value: 1, Description: Master=0\r\nField Id: 3, Array Id: 0, Name: Annex loop A, Severity: 0, Value: 2, Description: B=0\r\nField Id: 4, Array Id: 0, Name: Velocità minima modem loop A, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 5, Array Id: 0, Name: Velocità massima modem loop A, Severity: 0, Value: 89, Description: 5696 Kb/s=0\r\nField Id: 6, Array Id: 0, Name: Velocità modem loop A, Severity: 0, Value: 24, Description: 1536 Kb/s=0\r\nField Id: 7, Array Id: 0, Name: Connettore loop A, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 8, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: 0, Description: Auto=0\r\nField Id: 9, Array Id: 0, Name: Modalità loop B, Severity: 0, Value: 2, Description: Slave=0\r\nField Id: 10, Array Id: 0, Name: Annex loop B, Severity: 0, Value: 2, Description: B=0\r\nField Id: 11, Array Id: 0, Name: Velocità minima modem loop B, Severity: 0, Value: 4, Description: 256 Kb/s=0\r\nField Id: 12, Array Id: 0, Name: Velocità massima modem loop B, Severity: 0, Value: 89, Description: 5696 Kb/s=0\r\nField Id: 13, Array Id: 0, Name: Velocità modem loop B, Severity: -255, Value: 0, Description: adattativa=-255\r\nField Id: 14, Array Id: 0, Name: Connettore loop B, Severity: 0, Value: 3, Description: 2 fili=0\r\nField Id: 29, Array Id: 0, Name: Stato abilitazione loop A, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 30, Array Id: 0, Name: PAM modulation loop A, Severity: 0, Value: -1, Description: Nessuna=0\r\nField Id: 31, Array Id: 0, Name: Velocità loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Stato connessione loop A, Severity: -255, Value: 3, Description: Disconnesso=-255\r\nField Id: 33, Array Id: 0, Name: Potenza Tx loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 34, Array Id: 0, Name: Margine rumore modem loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 35, Array Id: 0, Name: Stato attenuazione modem loop A, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Stato segment anomaly loop A, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 37, Array Id: 0, Name: Stato segment defect loop A, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 38, Array Id: 0, Name: Stato abilitazione loop B, Severity: 0, Value: 1, Description: Attivo=0\r\nField Id: 39, Array Id: 0, Name: PAM modulation loop B, Severity: 0, Value: -1, Description: Nessuna=0\r\nField Id: 40, Array Id: 0, Name: Velocità loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione loop B, Severity: -255, Value: 3, Description: Disconnesso=-255\r\nField Id: 42, Array Id: 0, Name: Potenza Tx loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Margine rumore modem loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Stato attenuazione modem loop B, Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 45, Array Id: 0, Name: Stato segment anomaly loop B, Severity: -255, Value: 0, Description: n/d=-255\r\nField Id: 46, Array Id: 0, Name: Stato segment defect loop B, Severity: -255, Value: 0, Description: n/d=-255\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
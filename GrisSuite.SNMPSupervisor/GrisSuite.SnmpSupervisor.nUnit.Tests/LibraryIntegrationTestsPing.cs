﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsPing
    {
        #region Test periferica ping

        [Test]
        public void DumpCompleto_definition_DeviceTypeXXIP000_Ping_OK()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174471094");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: STLC 1000, Type: XXIP000, Offline: 0, Severity: 0, ReplyICMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Risposta ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 0, Value: 1, Description: Risposta ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeXXIP000_Ping_NessunaRisposta()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174471095");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: STLC 1000, Type: XXIP000, Offline: 1, Severity: 255, ReplyICMP: False\r\nStream Id: 1, Name: Stato generale, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 255, Value: 0, Description: Stato sconosciuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
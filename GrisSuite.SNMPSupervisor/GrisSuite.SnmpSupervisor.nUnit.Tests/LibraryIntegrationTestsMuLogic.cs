﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsMuLogic
    {
        #region Test MuLogic CDTS-2048

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_1_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174489868");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 01, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: utd_testa2..vr_m42-sct, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 277 giorni, 01:52:20.880, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Intermedio Singola Dorsale=-255;Primario=-255;Collegamento downstream primario attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.5.0 (12:06:59 Mar 15 2004), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 2, Description: Intermedio Singola Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: -255, Value: 1211, Description: Collegamento downstream primario attivo=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.129.12, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.129.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 2, Description: Valore ricevuto=-255;Disconnesso=2;1024 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;1536 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione collegamento upstream primario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Stato collegamento upstream primario, Severity: 2, Value: 0, Description: Disconnesso=2\r\nField Id: 2, Array Id: 0, Name: Velocità di trasmissione upstream primario, Severity: -255, Value: 1024, Description: 1024 Kbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Attenuazione linea upstream primario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Livello di rumore upstream primario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione collegamento downstream primario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Stato collegamento downstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Velocità di trasmissione downstream primario, Severity: -255, Value: 1536, Description: 1536 Kbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Attenuazione linea downstream primario (dB), Severity: -255, Value: 14, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Livello di rumore downstream primario (dB), Severity: -255, Value: 40, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("Anomalia grave", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_2_Ok()
        {
            // dorsale singola V1.4
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490372");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 02 Singola Dorsale V 1.4, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: utd_novaraarona-ccl.., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 11 giorni, 02:43:41.110, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Intermedio Singola Dorsale=-255;Primario=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.4.1 (11:56:14 Jul 26 2002), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 2, Description: Intermedio Singola Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.131.4, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.131.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 2, Description: Valore ricevuto=-255;Disconnesso=2;2048 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;2048 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione collegamento upstream primario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Stato collegamento upstream primario, Severity: 2, Value: 0, Description: Disconnesso=2\r\nField Id: 2, Array Id: 0, Name: Velocità di trasmissione upstream primario, Severity: -255, Value: 2048, Description: 2048 Kbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Attenuazione linea upstream primario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Livello di rumore upstream primario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione collegamento downstream primario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Stato collegamento downstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Velocità di trasmissione downstream primario, Severity: -255, Value: 2048, Description: 2048 Kbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Attenuazione linea downstream primario (dB), Severity: -255, Value: 9, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Livello di rumore downstream primario (dB), Severity: -255, Value: 36, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Connessa=-255;Connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("Anomalia grave", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_3_Ok()
        {
            // dorsale doppia
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490373");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 03 Doppia Dorsale, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: utd-vignale-ccl.., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 50 giorni, 19:14:23.430, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Intermedio Doppia Dorsale=-255;Primario=-255;Collegamento upstream primario attivo e collegamento downstream primario attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.5.0 (12:06:59 Mar 15 2004), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 5, Description: Intermedio Doppia Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: -255, Value: 2211, Description: Collegamento upstream primario attivo e collegamento downstream primario attivo=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.131.5, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.131.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 2, Description: Valore ricevuto=-255;Connesso=0;2048 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;1024 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Disconnesso=2;2048 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Disconnesso=2;128 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione collegamento upstream primario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Stato collegamento upstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 2, Array Id: 0, Name: Velocità di trasmissione upstream primario, Severity: -255, Value: 2048, Description: 2048 Kbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Attenuazione linea upstream primario (dB), Severity: -255, Value: 10, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Livello di rumore upstream primario (dB), Severity: -255, Value: 38, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione collegamento downstream primario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Stato collegamento downstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Velocità di trasmissione downstream primario, Severity: -255, Value: 1024, Description: 1024 Kbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Attenuazione linea downstream primario (dB), Severity: -255, Value: 28, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Livello di rumore downstream primario (dB), Severity: -255, Value: 30, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Versione collegamento upstream secondario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Stato collegamento upstream secondario, Severity: 2, Value: 0, Description: Disconnesso=2\r\nField Id: 12, Array Id: 0, Name: Velocità di trasmissione upstream secondario, Severity: -255, Value: 2048, Description: 2048 Kbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Attenuazione linea upstream secondario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Livello di rumore upstream secondario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Versione collegamento downstream secondario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Stato collegamento downstream secondario, Severity: 2, Value: 0, Description: Disconnesso=2\r\nField Id: 17, Array Id: 0, Name: Velocità di trasmissione downstream secondario, Severity: -255, Value: 128, Description: 128 Kbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Attenuazione linea downstream secondario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Livello di rumore downstream secondario (dB), Severity: -255, Value: 0, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 4, Array Id: 0, Name: Connessione porta switch 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Connessione porta switch 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 6, Array Id: 0, Name: Connessione porta switch 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 7, Array Id: 0, Name: Connessione porta switch 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 8, Array Id: 0, Name: Connessione porta secondaria 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 9, Array Id: 0, Name: Connessione porta secondaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 10, Array Id: 0, Name: Connessione porta secondaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 11, Array Id: 0, Name: Connessione porta secondaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("Anomalia grave", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_4_Ok()
        {
            // dorsale doppia, testa
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490438");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 04 Doppia Dorsale testa, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: novara-biella-ccl.., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 11 giorni, 03:18:54.160, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Testa Doppia Dorsale=-255;Primario=-255;Collegamento downstream primario attivo e collegamento downstream secondario attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.5.0 (12:06:59 Mar 15 2004), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 4, Description: Testa Doppia Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: -255, Value: 1212, Description: Collegamento downstream primario attivo e collegamento downstream secondario attivo=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.131.70, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.131.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Valore ricevuto=-255;Connesso=0;1536 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;1536 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione collegamento downstream primario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Stato collegamento downstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Velocità di trasmissione downstream primario, Severity: -255, Value: 1536, Description: 1536 Kbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Attenuazione linea downstream primario (dB), Severity: -255, Value: 18, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Livello di rumore downstream primario (dB), Severity: -255, Value: 32, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Versione collegamento downstream secondario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Stato collegamento downstream secondario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 17, Array Id: 0, Name: Velocità di trasmissione downstream secondario, Severity: -255, Value: 1536, Description: 1536 Kbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Attenuazione linea downstream secondario (dB), Severity: -255, Value: 18, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Livello di rumore downstream secondario (dB), Severity: -255, Value: 29, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Connessa=-255;Connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 4, Array Id: 0, Name: Connessione porta switch 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Connessione porta switch 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Connessione porta switch 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 7, Array Id: 0, Name: Connessione porta switch 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 8, Array Id: 0, Name: Connessione porta secondaria 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 9, Array Id: 0, Name: Connessione porta secondaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 10, Array Id: 0, Name: Connessione porta secondaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 11, Array Id: 0, Name: Connessione porta secondaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_5_Ok()
        {
            // dorsale doppia, coda
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490452");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 05 Doppia Dorsale coda, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: utd-coda-salussola-ccl.., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 245 giorni, 09:45:33.860, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Coda Doppia Dorsale=-255;Primario=-255;Collegamento upstream primario attivo e collegamento upstream secondario attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.5.0 (12:06:59 Mar 15 2004), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 6, Description: Coda Doppia Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: -255, Value: 2121, Description: Collegamento upstream primario attivo e collegamento upstream secondario attivo=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.131.84, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.131.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Valore ricevuto=-255;Connesso=0;1536 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;1536 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione collegamento upstream primario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Stato collegamento upstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 2, Array Id: 0, Name: Velocità di trasmissione upstream primario, Severity: -255, Value: 1536, Description: 1536 Kbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Attenuazione linea upstream primario (dB), Severity: -255, Value: 19, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Livello di rumore upstream primario (dB), Severity: -255, Value: 34, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Versione collegamento upstream secondario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Stato collegamento upstream secondario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 12, Array Id: 0, Name: Velocità di trasmissione upstream secondario, Severity: -255, Value: 1536, Description: 1536 Kbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Attenuazione linea upstream secondario (dB), Severity: -255, Value: 16, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Livello di rumore upstream secondario (dB), Severity: -255, Value: 36, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 4, Array Id: 0, Name: Connessione porta switch 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Connessione porta switch 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 6, Array Id: 0, Name: Connessione porta switch 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 7, Array Id: 0, Name: Connessione porta switch 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 8, Array Id: 0, Name: Connessione porta secondaria 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 9, Array Id: 0, Name: Connessione porta secondaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 10, Array Id: 0, Name: Connessione porta secondaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 11, Array Id: 0, Name: Connessione porta secondaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_6_Ok()
        {
            // dorsale doppia, testa
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490628");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 06 Doppia Dorsale testa, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: cd2-t-torino-xxx.., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 285 giorni, 21:33:31.610, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Testa Doppia Dorsale=-255;Primario=-255;Collegamento downstream primario attivo e collegamento downstream secondario attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.5.0 (12:06:59 Mar 15 2004), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 4, Description: Testa Doppia Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: -255, Value: 1212, Description: Collegamento downstream primario attivo e collegamento downstream secondario attivo=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.132.4, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.133.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Valore ricevuto=-255;Connesso=0;2048 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;1536 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione collegamento downstream primario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Stato collegamento downstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Velocità di trasmissione downstream primario, Severity: -255, Value: 2048, Description: 2048 Kbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Attenuazione linea downstream primario (dB), Severity: -255, Value: 8, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Livello di rumore downstream primario (dB), Severity: -255, Value: 39, Description: Valore ricevuto=-255\r\nField Id: 15, Array Id: 0, Name: Versione collegamento downstream secondario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Stato collegamento downstream secondario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 17, Array Id: 0, Name: Velocità di trasmissione downstream secondario, Severity: -255, Value: 1536, Description: 1536 Kbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Attenuazione linea downstream secondario (dB), Severity: -255, Value: 8, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Livello di rumore downstream secondario (dB), Severity: -255, Value: 39, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255;Connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 4, Array Id: 0, Name: Connessione porta switch 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 5, Array Id: 0, Name: Connessione porta switch 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 6, Array Id: 0, Name: Connessione porta switch 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 7, Array Id: 0, Name: Connessione porta switch 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 8, Array Id: 0, Name: Connessione porta secondaria 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 9, Array Id: 0, Name: Connessione porta secondaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 10, Array Id: 0, Name: Connessione porta secondaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 11, Array Id: 0, Name: Connessione porta secondaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_7_Ok()
        {
            // dorsale singola V1.4
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490124");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 07 Singola Dorsale V 1.4, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: utd_isoladasti.., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 179 giorni, 01:35:13.470, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Intermedio Singola Dorsale=-255;Primario=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.4.1 (11:56:14 Jul 26 2002), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 2, Description: Intermedio Singola Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.130.12, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.130.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Valore ricevuto=-255;Connesso=0;1536 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Connesso=0;1024 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione collegamento upstream primario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Stato collegamento upstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 2, Array Id: 0, Name: Velocità di trasmissione upstream primario, Severity: -255, Value: 1536, Description: 1536 Kbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Attenuazione linea upstream primario (dB), Severity: -255, Value: 22, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Livello di rumore upstream primario (dB), Severity: -255, Value: 33, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione collegamento downstream primario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Stato collegamento downstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Velocità di trasmissione downstream primario, Severity: -255, Value: 1024, Description: 1024 Kbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Attenuazione linea downstream primario (dB), Severity: -255, Value: 27, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Livello di rumore downstream primario (dB), Severity: -255, Value: 30, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Non connessa=-255;Non connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_8_Ok()
        {
            // dorsale singola, testa
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174489867");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 08 Singola Dorsale testa, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: utd_testa1., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 277 giorni, 18:51:36.420, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Testa Singola Dorsale=-255;Primario=-255;Collegamento downstream primario attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.5.0 (12:06:59 Mar 15 2004), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 1, Description: Testa Singola Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: -255, Value: 1211, Description: Collegamento downstream primario attivo=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.129.11, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.129.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Valore ricevuto=-255;Connesso=0;1024 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Versione collegamento downstream primario, Severity: -255, Value: DSL MTU-C [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Stato collegamento downstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 7, Array Id: 0, Name: Velocità di trasmissione downstream primario, Severity: -255, Value: 1024, Description: 1024 Kbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Attenuazione linea downstream primario (dB), Severity: -255, Value: 6, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Livello di rumore downstream primario (dB), Severity: -255, Value: 38, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Connessa=-255;Connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionMULCDTS2048_000_9_Ok()
        {
            // dorsale singola, coda
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174490137");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual("Device Name: MuLogic CDTS-2048 09 Singola Dorsale coda, Type: MULCDTS2048, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: MULCDTS2048_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: utd-alba.., Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: CDTS SNMP Agent, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 2 giorni, 19:27:07.480, Description: Up time ricevuto=0\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Coda Singola Dorsale=-255;Primario=-255;Collegamento upstream primario attivo=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;10 Mbit/s=-255\r\nField Id: 0, Array Id: 0, Name: Versione apparato, Severity: -255, Value: CDTS v1.5.0 (12:06:59 Mar 15 2004), Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo di apparato, Severity: -255, Value: 3, Description: Coda Singola Dorsale=-255\r\nField Id: 2, Array Id: 0, Name: Collegamento Attivo, Severity: -255, Value: 0, Description: Primario=-255\r\nField Id: 3, Array Id: 0, Name: Stato Linea, Severity: -255, Value: 2111, Description: Collegamento upstream primario attivo=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP, Severity: -255, Value: 10.102.130.25, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.130.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Velocità scheda di rete, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nStream Id: 3, Name: Informazioni WAN, Severity: 0, Description: Valore ricevuto=-255;Connesso=0;1024 Kbit/s=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione collegamento upstream primario, Severity: -255, Value: DSL MTU-R [1] V2.25   , Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Stato collegamento upstream primario, Severity: 0, Value: 1, Description: Connesso=0\r\nField Id: 2, Array Id: 0, Name: Velocità di trasmissione upstream primario, Severity: -255, Value: 1024, Description: 1024 Kbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Attenuazione linea upstream primario (dB), Severity: -255, Value: 27, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Livello di rumore upstream primario (dB), Severity: -255, Value: 32, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Informazioni porte switch, Severity: -255, Description: Connessa=-255;Connessa=-255;Non connessa=-255;Non connessa=-255\r\nField Id: 0, Array Id: 0, Name: Connessione porta primaria 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 1, Array Id: 0, Name: Connessione porta primaria 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 2, Array Id: 0, Name: Connessione porta primaria 3, Severity: -255, Value: 0, Description: Non connessa=-255\r\nField Id: 3, Array Id: 0, Name: Connessione porta primaria 4, Severity: -255, Value: 0, Description: Non connessa=-255\r\n", deviceDump);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
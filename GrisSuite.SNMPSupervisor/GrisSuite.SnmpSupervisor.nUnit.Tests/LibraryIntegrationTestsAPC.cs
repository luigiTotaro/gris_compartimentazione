﻿using System;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsAPC
    {
        #region Test APC

        [Test]
        public void DumpCompleto_definition_DeviceTypeAPDP3120()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511254");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Gruppo Continuita` Sala Server IaP, Type: APDP3120, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: UPS Locale tecnologico IAP, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: APC Web/SNMP Management Card (MB:v3.6.8 PF:v2.6.4 PN:apc_hw02_aos_264.bin AF1:v2.6.1 AN1:apc_hw02_dp3e_261.bin MN:AP9617 HR:A10 SN: ZA0526002067 MD:06/20/2005) (Embedded PowerNet SNMP Agent SW v2.2 compatible), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 456 giorni, 21:36:43.940, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Unknown, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Bologna C.le, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Modello riconosciuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Modello, Severity: 0, Value: Silcon DP3120E, Description: Modello riconosciuto=0\r\nField Id: 1, Array Id: 0, Name: ID Apparato, Severity: 0, Value: UPS_IDEN, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Revisione firmware, Severity: 0, Value: 329.16.I, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Numero di serie, Severity: 0, Value: CE0750150247, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato generale, Severity: 0, Description: Alimentazione da linea=0;Tre fasi presenti=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Stato UPS (Codice), Severity: 0, Value: 2, Description: Alimentazione da linea=0\r\nField Id: 1, Array Id: 0, Name: Fasi in uscita, Severity: 0, Value: 3, Description: Tre fasi presenti=0\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita fase 1, Severity: 0, Value: 397 V, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita fase 2, Severity: 0, Value: 399 V, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Tensione in uscita fase 3, Severity: 0, Value: 396 V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Carico uscita fase 1, Severity: 0, Value: 27 %, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Carico uscita fase 2, Severity: 0, Value: 20 %, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Carico uscita fase 3, Severity: 0, Value: 31 %, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Corrente in uscita fase 1, Severity: 0, Value: 47 A, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita fase 2, Severity: 0, Value: 36 A, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Corrente in uscita fase 3, Severity: 0, Value: 55 A, Description: Valore ricevuto=0\r\nField Id: 11, Array Id: 0, Name: Frequenza in uscita, Severity: 0, Value: 50 Hz, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato batterie, Severity: 0, Description: Batterie in funzionamento normale=0;Valore ricevuto=0;Valore ricevuto=0;Batterie da non sostituire=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Nessun pacco batterie in avaria=0\r\nField Id: 0, Array Id: 0, Name: Stato batterie (Codice), Severity: 0, Value: 2, Description: Batterie in funzionamento normale=0\r\nField Id: 1, Array Id: 0, Name: Capacità residua batterie, Severity: 0, Value: 100 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Autonomia residua batterie, Severity: 0, Value: 0 giorni, 05:32:00.000, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Indicatore sostituzione batterie (Codice), Severity: 0, Value: 1, Description: Batterie da non sostituire=0\r\nField Id: 4, Array Id: 0, Name: Temperatura batterie, Severity: 0, Value: 21 °C, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Tensione batterie, Severity: 0, Value: 436 V, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Corrente batterie, Severity: 0, Value: 0 A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Numero di pacchi batterie, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Numero di pacchi batterie guasti, Severity: 0, Value: 0, Description: Nessun pacco batterie in avaria=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeAP00000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174519522");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Gruppo Continuità di Acquafredda, Type: AP00000, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: Unknown, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: APC Web/SNMP Management Card (MB:v3.8.6 PF:v3.5.5 PN:apc_hw02_aos_355.bin AF1:v3.5.5 AN1:apc_hw02_sumx_355.bin MN:AP9617 HR:A10 SN: BA0828001220 MD:07/07/2008) (Embedded PowerNet SNMP Agent SW v2.2 compatible), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 43 giorni, 07:20:49.900, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Unknown, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Unknown, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Modello, Severity: 0, Value: Smart-UPS 1500 RM, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: ID Apparato, Severity: 0, Value: UPS_IDEN, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Revisione firmware, Severity: 0, Value: 667.18.I, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Numero di serie, Severity: 0, Value: AS0826233588, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato generale, Severity: 0, Description: Alimentazione da linea=0;Una sola fase in uscita=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Stato UPS (Codice), Severity: 0, Value: 2, Description: Alimentazione da linea=0\r\nField Id: 1, Array Id: 0, Name: Fasi in uscita, Severity: 0, Value: 1, Description: Una sola fase in uscita=0\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita, Severity: 0, Value: 244 V, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Carico uscita, Severity: 0, Value: 11 %, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Corrente in uscita, Severity: 0, Value: 0 A, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Frequenza in uscita, Severity: 0, Value: 50 Hz, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato batterie, Severity: 0, Description: Batterie in funzionamento normale=0;Valore ricevuto=0;Valore ricevuto=0;Batterie da non sostituire=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Nessun pacco batterie in avaria=0\r\nField Id: 0, Array Id: 0, Name: Stato batterie (Codice), Severity: 0, Value: 2, Description: Batterie in funzionamento normale=0\r\nField Id: 1, Array Id: 0, Name: Capacità residua batterie, Severity: 0, Value: 100 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Autonomia residua batterie, Severity: 0, Value: 0 giorni, 01:59:00.000, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Indicatore sostituzione batterie (Codice), Severity: 0, Value: 1, Description: Batterie da non sostituire=0\r\nField Id: 4, Array Id: 0, Name: Temperatura batterie, Severity: 0, Value: 28 °C, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Tensione batterie, Severity: 0, Value: 27 V, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Corrente batterie, Severity: 0, Value: 0 A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Numero di pacchi batterie, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Numero di pacchi batterie guasti, Severity: 0, Value: 0, Description: Nessun pacco batterie in avaria=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definition_DeviceTypeMGGA5000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174485654");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: Gruppo Continuità MGGA5000 di Foggia, Type: MGGA5000, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Forced serial number by SNMP query: 1P3K10001\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GALAXY 5000 40 kVA, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 160 giorni, 00:25:45.350, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Computer Room Manager, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Computer Room, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Modello riconosciuto (MGE Galaxy 5000 40 kVA)=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Modello, Severity: 0, Value: 5000_40, Description: Modello riconosciuto (MGE Galaxy 5000 40 kVA)=0\r\nField Id: 1, Array Id: 0, Name: ID Apparato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Revisione firmware, Severity: 0, Value: V1.GDENA7S300IC303, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Numero di serie, Severity: 0, Value: 1P3K10001, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato generale, Severity: 0, Description: Alimentazione da linea=0;Tre fasi presenti=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Bypass non inserito=0;Servizio erogato=0;Inverter attivo=0;Carico uscita normale=0;Temperatura normale=0\r\nField Id: 0, Array Id: 0, Name: Stato UPS (Codice), Severity: 0, Value: 2, Description: Alimentazione da linea=0\r\nField Id: 1, Array Id: 0, Name: Fasi in uscita, Severity: 0, Value: 3, Description: Tre fasi presenti=0\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita fase 1, Severity: 0, Value: 398 V, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita fase 2, Severity: 0, Value: 400 V, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Tensione in uscita fase 3, Severity: 0, Value: 399 V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Carico uscita fase 1, Severity: 0, Value: 25 %, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Carico uscita fase 2, Severity: 0, Value: 28 %, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Carico uscita fase 3, Severity: 0, Value: 23 %, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Corrente in uscita fase 1, Severity: 0, Value: 16,2 A, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita fase 2, Severity: 0, Value: 14,9 A, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Corrente in uscita fase 3, Severity: 0, Value: 12,7 A, Description: Valore ricevuto=0\r\nField Id: 11, Array Id: 0, Name: Frequenza in uscita, Severity: 0, Value: 50 Hz, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Stato bypass (Codice), Severity: 0, Value: 2, Description: Bypass non inserito=0\r\nField Id: 13, Array Id: 0, Name: Stato servizio (Codice), Severity: 0, Value: 2, Description: Servizio erogato=0\r\nField Id: 14, Array Id: 0, Name: Stato inverter (Codice), Severity: 0, Value: 2, Description: Inverter attivo=0\r\nField Id: 15, Array Id: 0, Name: Stato uscita (Codice), Severity: 0, Value: 2, Description: Carico uscita normale=0\r\nField Id: 16, Array Id: 0, Name: Stato temperatura (Codice), Severity: 0, Value: 2, Description: Temperatura normale=0\r\nStream Id: 4, Name: Stato batterie, Severity: 0, Description: Batterie in servizio=0;Valore ricevuto=0;Valore ricevuto=0;Batterie da non sostituire=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Stato batterie (Codice), Severity: 0, Value: 2, Description: Batterie in servizio=0\r\nField Id: 1, Array Id: 0, Name: Capacità residua batterie, Severity: 0, Value: 100 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Autonomia residua batterie, Severity: 0, Value: 54 min, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Indicatore sostituzione batterie (Codice), Severity: 0, Value: 2, Description: Batterie da non sostituire=0\r\nField Id: 4, Array Id: 0, Name: Temperatura batterie, Severity: 0, Value: 21 °C, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Tensione batterie, Severity: 0, Value: 489 V, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Corrente batterie, Severity: 0, Value: 0 A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Tempo ricarica batterie, Severity: 0, Value: 0 s, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: Gruppo Continuità MGGA5000 di Foggia, Type: MGGA5000, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Forced serial number by SNMP query: 1P3K10001\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GALAXY 5000 40 kVA, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 160 giorni, 00:25:45.350, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Computer Room Manager, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Computer Room, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Modello riconosciuto (MGE Galaxy 5000 40 kVA)=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Modello, Severity: 0, Value: 5000_40, Description: Modello riconosciuto (MGE Galaxy 5000 40 kVA)=0\r\nField Id: 1, Array Id: 0, Name: ID Apparato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Revisione firmware, Severity: 0, Value: V1.GDENA7S300IC303, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Numero di serie, Severity: 0, Value: 1P3K10001, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato generale, Severity: 0, Description: Alimentazione da linea=0;Tre fasi presenti=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Bypass non inserito=0;Servizio erogato=0;Inverter attivo=0;Carico uscita normale=0;Temperatura normale=0\r\nField Id: 0, Array Id: 0, Name: Stato UPS (Codice), Severity: 0, Value: 2, Description: Alimentazione da linea=0\r\nField Id: 1, Array Id: 0, Name: Fasi in uscita, Severity: 0, Value: 3, Description: Tre fasi presenti=0\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita fase 1, Severity: 0, Value: 398 V, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita fase 2, Severity: 0, Value: 400 V, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Tensione in uscita fase 3, Severity: 0, Value: 399 V, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Carico uscita fase 1, Severity: 0, Value: 25 %, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Carico uscita fase 2, Severity: 0, Value: 28 %, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Carico uscita fase 3, Severity: 0, Value: 23 %, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Corrente in uscita fase 1, Severity: 0, Value: 16.2 A, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita fase 2, Severity: 0, Value: 14.9 A, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Corrente in uscita fase 3, Severity: 0, Value: 12.7 A, Description: Valore ricevuto=0\r\nField Id: 11, Array Id: 0, Name: Frequenza in uscita, Severity: 0, Value: 50 Hz, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Stato bypass (Codice), Severity: 0, Value: 2, Description: Bypass non inserito=0\r\nField Id: 13, Array Id: 0, Name: Stato servizio (Codice), Severity: 0, Value: 2, Description: Servizio erogato=0\r\nField Id: 14, Array Id: 0, Name: Stato inverter (Codice), Severity: 0, Value: 2, Description: Inverter attivo=0\r\nField Id: 15, Array Id: 0, Name: Stato uscita (Codice), Severity: 0, Value: 2, Description: Carico uscita normale=0\r\nField Id: 16, Array Id: 0, Name: Stato temperatura (Codice), Severity: 0, Value: 2, Description: Temperatura normale=0\r\nStream Id: 4, Name: Stato batterie, Severity: 0, Description: Batterie in servizio=0;Valore ricevuto=0;Valore ricevuto=0;Batterie da non sostituire=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Stato batterie (Codice), Severity: 0, Value: 2, Description: Batterie in servizio=0\r\nField Id: 1, Array Id: 0, Name: Capacità residua batterie, Severity: 0, Value: 100 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Autonomia residua batterie, Severity: 0, Value: 54 min, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Indicatore sostituzione batterie (Codice), Severity: 0, Value: 2, Description: Batterie da non sostituire=0\r\nField Id: 4, Array Id: 0, Name: Temperatura batterie, Severity: 0, Value: 21 °C, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Tensione batterie, Severity: 0, Value: 489 V, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Corrente batterie, Severity: 0, Value: 0 A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Tempo ricarica batterie, Severity: 0, Value: 0 s, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else
                    {
                        Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
                    }
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test MGE UPS

        [Test]
        public void DumpCompleto_definition_DeviceTypeMGUPS000()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174519190");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: Gruppo Continuità MGE UPS di Savona, Type: MGUPS000, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Forced serial number by SNMP query: XXXXXXXXXX\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MGE UPS Galaxy 3000 30KVA, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Galaxy 3000 30 kVA, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 316 giorni, 05:22:55.410, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Sig. Chiastra, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Sala CED - SAVONA, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Modello, Severity: 0, Value: 3000_30, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: ID Apparato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Revisione firmware, Severity: 0, Value: V1.GDERE2S300IC303, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Numero di serie, Severity: 0, Value: XXXXXXXXXX, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato generale, Severity: 0, Description: Alimentazione da linea=0;Tre fasi presenti=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Bypass non inserito=0;Servizio erogato=0;Inverter attivo=0;Carico uscita normale=0;Temperatura normale=0\r\nField Id: 0, Array Id: 0, Name: Stato UPS (Codice), Severity: 0, Value: 2, Description: Alimentazione da linea=0\r\nField Id: 1, Array Id: 0, Name: Fasi in uscita, Severity: 0, Value: 3, Description: Tre fasi presenti=0\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita fase 1, Severity: -255, Value: 396 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Frequenza in uscita fase 1, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Carico uscita fase 1, Severity: -255, Value: 49 %, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Corrente in uscita fase 1, Severity: -255, Value: 22,9 A, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tensione in uscita fase 2, Severity: -255, Value: 398 V, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Frequenza in uscita fase 2, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Carico uscita fase 2, Severity: -255, Value: 49 %, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita fase 2, Severity: -255, Value: 22,4 A, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Tensione in uscita fase 3, Severity: -255, Value: 395 V, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Frequenza in uscita fase 3, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Carico uscita fase 3, Severity: -255, Value: 49 %, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Corrente in uscita fase 3, Severity: -255, Value: 19,2 A, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Stato bypass (Codice), Severity: 0, Value: 2, Description: Bypass non inserito=0\r\nField Id: 15, Array Id: 0, Name: Stato servizio (Codice), Severity: 0, Value: 2, Description: Servizio erogato=0\r\nField Id: 16, Array Id: 0, Name: Stato inverter (Codice), Severity: 0, Value: 2, Description: Inverter attivo=0\r\nField Id: 17, Array Id: 0, Name: Stato uscita (Codice), Severity: 0, Value: 2, Description: Carico uscita normale=0\r\nField Id: 18, Array Id: 0, Name: Stato temperatura (Codice), Severity: 0, Value: 2, Description: Temperatura normale=0\r\nStream Id: 4, Name: Stato batterie, Severity: 0, Description: Batterie in servizio=0;Valore ricevuto=0;Valore ricevuto=0;Batterie da non sostituire=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Stato batterie (Codice), Severity: 0, Value: 2, Description: Batterie in servizio=0\r\nField Id: 1, Array Id: 0, Name: Capacità residua batterie, Severity: 0, Value: 100 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Autonomia residua batterie, Severity: 0, Value: 38 min, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Indicatore sostituzione batterie (Codice), Severity: 0, Value: 2, Description: Batterie da non sostituire=0\r\nField Id: 4, Array Id: 0, Name: Temperatura batterie, Severity: 0, Value: 26 °C, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Tensione batterie, Severity: 0, Value: 395 V, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Corrente batterie, Severity: 0, Value: 0 A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Tempo ricarica batterie, Severity: 0, Value: 0 s, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: Gruppo Continuità MGE UPS di Savona, Type: MGUPS000, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Forced serial number by SNMP query: XXXXXXXXXX\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: MGE UPS Galaxy 3000 30KVA, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Galaxy 3000 30 kVA, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 316 giorni, 05:22:55.410, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: Sig. Chiastra, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Sala CED - SAVONA, Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Modello, Severity: 0, Value: 3000_30, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: ID Apparato, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 2, Array Id: 0, Name: Revisione firmware, Severity: 0, Value: V1.GDERE2S300IC303, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Numero di serie, Severity: 0, Value: XXXXXXXXXX, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato generale, Severity: 0, Description: Alimentazione da linea=0;Tre fasi presenti=0;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Bypass non inserito=0;Servizio erogato=0;Inverter attivo=0;Carico uscita normale=0;Temperatura normale=0\r\nField Id: 0, Array Id: 0, Name: Stato UPS (Codice), Severity: 0, Value: 2, Description: Alimentazione da linea=0\r\nField Id: 1, Array Id: 0, Name: Fasi in uscita, Severity: 0, Value: 3, Description: Tre fasi presenti=0\r\nField Id: 2, Array Id: 0, Name: Tensione in uscita fase 1, Severity: -255, Value: 396 V, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Frequenza in uscita fase 1, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Carico uscita fase 1, Severity: -255, Value: 49 %, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Corrente in uscita fase 1, Severity: -255, Value: 22.9 A, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tensione in uscita fase 2, Severity: -255, Value: 398 V, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Frequenza in uscita fase 2, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Carico uscita fase 2, Severity: -255, Value: 49 %, Description: Valore ricevuto=-255\r\nField Id: 9, Array Id: 0, Name: Corrente in uscita fase 2, Severity: -255, Value: 22.4 A, Description: Valore ricevuto=-255\r\nField Id: 10, Array Id: 0, Name: Tensione in uscita fase 3, Severity: -255, Value: 395 V, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Frequenza in uscita fase 3, Severity: -255, Value: 50 Hz, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Carico uscita fase 3, Severity: -255, Value: 49 %, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Corrente in uscita fase 3, Severity: -255, Value: 19.2 A, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Stato bypass (Codice), Severity: 0, Value: 2, Description: Bypass non inserito=0\r\nField Id: 15, Array Id: 0, Name: Stato servizio (Codice), Severity: 0, Value: 2, Description: Servizio erogato=0\r\nField Id: 16, Array Id: 0, Name: Stato inverter (Codice), Severity: 0, Value: 2, Description: Inverter attivo=0\r\nField Id: 17, Array Id: 0, Name: Stato uscita (Codice), Severity: 0, Value: 2, Description: Carico uscita normale=0\r\nField Id: 18, Array Id: 0, Name: Stato temperatura (Codice), Severity: 0, Value: 2, Description: Temperatura normale=0\r\nStream Id: 4, Name: Stato batterie, Severity: 0, Description: Batterie in servizio=0;Valore ricevuto=0;Valore ricevuto=0;Batterie da non sostituire=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Stato batterie (Codice), Severity: 0, Value: 2, Description: Batterie in servizio=0\r\nField Id: 1, Array Id: 0, Name: Capacità residua batterie, Severity: 0, Value: 100 %, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Autonomia residua batterie, Severity: 0, Value: 38 min, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Indicatore sostituzione batterie (Codice), Severity: 0, Value: 2, Description: Batterie da non sostituire=0\r\nField Id: 4, Array Id: 0, Name: Temperatura batterie, Severity: 0, Value: 26 °C, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Tensione batterie, Severity: 0, Value: 395 V, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Corrente batterie, Severity: 0, Value: 0 A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Tempo ricarica batterie, Severity: 0, Value: 0 s, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else
                    {
                        Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
                    }
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
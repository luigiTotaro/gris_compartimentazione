﻿using System.Text;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
	internal class TestUtilities
	{
		private static readonly string dumpFilename;
		private const string DEVICES_DUMP_FILENAME = "D:\\DevicesObjectDump.txt";

		static TestUtilities()
		{
			dumpFilename = DEVICES_DUMP_FILENAME;
		}

		private static string EncodeStringForTest(string inputValue)
		{
			return string.Format("\"{0}\"", inputValue.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("\r", "\\r").Replace("\n", "\\n"));
		}

		public static void WriteDeviceDumpToFile(IDevice device)
		{
			StringBuilder dump = new StringBuilder();

			dump = dump.AppendFormat("Device IP: {0}, Device Address: {1}\r\n\r\n", device.DeviceIPEndPoint.Address, DataUtility.EncodeIPAddress(device.DeviceIPEndPoint.Address));

			dump = dump.AppendFormat("Assert.AreEqual(Severity.{0}, device.SeverityLevel);\r\n\r\n", device.SeverityLevel);
			dump = dump.AppendFormat("Assert.AreEqual(\r\n");
			dump = dump.AppendFormat("\t{0},\r\n", EncodeStringForTest(device.Dump()));
			dump = dump.AppendFormat("\tdeviceDump);\r\n");
			dump = dump.AppendFormat("----------------------------------------------------------------------\r\n");

			FileUtility.AppendStringToFile(dumpFilename, dump.ToString());
		}
	}
}
﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsIndicatoriDiCarrozza
    {
        #region Test Indicatori di Carrozza Fida - Semplice Ping

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Fida_Ping_NoRisposta()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174589185");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Napoli Fida IC BIN. 12 - POS. 1A, Type: XXIP00FI, Offline: 1, Severity: 255, ReplyICMP: False\r\nStream Id: 1, Name: Stato generale, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 255, Value: 0, Description: Stato sconosciuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Fida_Ping_Online()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174589186");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Napoli Fida IC BIN. 12 - POS. 1B, Type: XXIP00FI, Offline: 0, Severity: 0, ReplyICMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Risposta ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 0, Value: 1, Description: Risposta ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Indicatori di Carrozza Solari - Semplice Ping

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_Ping_NoRisposta()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174587905");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");

                    Assert.AreEqual(1, device.Offline);
                    Assert.AreEqual(Severity.Unknown, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Solari 1 (non conforme Infostazioni), Type: XXIP00SO, Offline: 1, Severity: 255, ReplyICMP: False\r\nStream Id: 1, Name: Stato generale, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 255, Value: 0, Description: Stato sconosciuto\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_Ping_Online()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174587906");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Solari 2 (non conforme Infostazioni), Type: XXIP00SO, Offline: 0, Severity: 0, ReplyICMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Risposta ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 0, Value: 1, Description: Risposta ricevuta=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

        #region Test Indicatori di Carrozza Solari - Infostazioni

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_1_Infostazioni_Categoria21_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174589339");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Napoli Solari IC BIN. 19 - POS. 13A, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 21, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 8250697-01\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: IC13NA1B19T9218M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - GTx-ADP, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 5 giorni, 08:24:45.460, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: mlucchini@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Solari di Udine (default location), Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Indicatore di carrozza=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 8250697, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 8250697-01, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 090925, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.6.18-92.el5, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 21, Description: Indicatore di carrozza=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: BIN19-13A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: IC13NA1B19T9218M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: IC13NA1B19T9218M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-19-99-5C-37-DB, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.104.5.155, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.255.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.104.5.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/06/2011 19:44:16, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 08/06/2011 11:19:27, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 117, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.050, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.060, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 42, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 235, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 511, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 1192, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_2_Infostazioni_Categoria21_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174590721");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Torino P.N. Solari IC BIN. 16 - POS. 1A, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 21, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 8250697-254\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: IC01TO1B16T219M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - GTx-ADP, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 21:34:08.510, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: mlucchini@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Solari di Udine (default location), Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Indicatore di carrozza=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 8250697, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 8250697-254, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 090925, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.6.18-92.el5, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 21, Description: Indicatore di carrozza=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: bin16-1a, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: IC01TO1B16T219M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: IC01TO1B16T219M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-19-99-68-29-14, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.104.11.1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.255.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.104.11.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/06/2011 17:25:41, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 26/05/2011 18:43:04, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 40, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.030, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.040, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 46, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 221, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 510, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 1198, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_3_Infostazioni_Categoria21_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174590772");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Torino P.N. Solari IC BIN. 17 - POS. 13B, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 21, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 8250697-89\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: IC13TO2B17T219M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - GTx-ADP, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 22:13:23.970, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: mlucchini@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Solari di Udine (default location), Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Indicatore di carrozza=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 8250697, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 8250697-89, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 090925, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.6.18-92.el5, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 21, Description: Indicatore di carrozza=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: bin17-13b, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: IC13TO2B17T219M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: IC13TO2B17T219M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-19-99-68-29-72, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.104.11.52, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.255.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.104.11.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/06/2011 17:17:46, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 26/05/2011 18:16:18, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 42, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.030, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.030, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 48, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 251, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 510, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 1198, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_4_Infostazioni_Categoria21_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174590992");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Torino P.S. Solari IC BIN. 3 - POS. 8B, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 21, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 8250697-95\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: IC08TO2B03T35M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - GTx-ADP, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 75 giorni, 05:47:46.340, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: mlucchini@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Solari di Udine (default location), Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Indicatore di carrozza=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 8250697, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 8250697-95, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 090925, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.6.18-92.el5, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 21, Description: Indicatore di carrozza=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: bin3-8b, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: IC08TO2B03T35M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: IC08TO2B03T35M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-19-99-68-29-46, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.104.12.16, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.255.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.104.12.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/06/2011 17:13:50, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 30/03/2011 10:57:18, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 4052, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.160, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.190, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 46, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 117, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 510, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 1198, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_5_Infostazioni_Categoria21_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174591024");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Torino P.S. Solari IC BIN. 4 - POS. 12B, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 21, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 8250697-353\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: IC12TO2B04T35M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - GTx-ADP, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 35 giorni, 19:46:40.040, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: mlucchini@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Solari di Udine (default location), Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Indicatore di carrozza=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 8250697, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 8250697-353, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 090925, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.6.18-92.el5, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 21, Description: Indicatore di carrozza=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: Atrio, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: IC12TO2B04T35M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: IC12TO2B04T35M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-19-99-68-29-8F, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.104.12.48, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.255.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.104.12.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/06/2011 19:38:42, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 08/05/2011 23:36:30, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 24, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.160, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.190, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 45, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 103, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 510, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 1198, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_6_Infostazioni_Categoria21_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174591233");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Venezia Solari IC BIN. 3 - POS. 1A, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 21, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 8250697-76\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: IC01VE1B03T2593M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - GTx-ADP, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 1 giorno, 00:41:06.230, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: mlucchini@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Solari di Udine (default location), Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Indicatore di carrozza=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 8250697, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 8250697-76, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 090925, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.6.18-92.el5, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 21, Description: Indicatore di carrozza=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: BIN3-1A, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: IC01VE1B03T2593M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: IC01VE1B03T2593M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-19-99-68-28-BE, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.104.13.1, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.255.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.104.13.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/06/2011 17:51:19, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 12/06/2011 17:10:25, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 368, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 0 giorni, 00:00:03.340, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 0 giorni, 00:00:01.150, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 46, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 235, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 511, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 1192, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionIndicatoreDiCarrozza_Solari_7_Infostazioni_Categoria21_Ok()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174591244");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);
                    Assert.AreEqual("In servizio", device.ValueDescriptionComplete);

                    Assert.AreEqual(
                        "Device Name: Indicatore di carrozza Venezia Solari IC BIN. 3 - POS. 6B, Type: INFSTAZ1, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: 21, Device with dynamic loading rules enabled, Forced serial number by SNMP query: 8250697-69\r\nDynamic Definition File: INFSTAZ-Solari.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0;Contatto di riferimento ricevuto=0;Locazione ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: IC06VE2B03T2593M, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: GTx - GTx-ADP, Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 1 giorno, 00:58:54.190, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 0, Value: mlucchini@solari.it, Description: Contatto di riferimento ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 0, Value: Solari di Udine (default location), Description: Locazione ricevuta=0\r\nStream Id: 2, Name: Informazioni generali, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Indicatore di carrozza=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome del costruttore, Severity: 0, Value: Solari di Udine, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Sigla del modello, Severity: 0, Value: 8250697, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero seriale, Severity: 0, Value: 8250697-69, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Versione software, Severity: 0, Value: 090925, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Versione firmware, Severity: 0, Value: 2.6.18-92.el5, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Categoria, Severity: 0, Value: 21, Description: Indicatore di carrozza=0\r\nField Id: 6, Array Id: 0, Name: Locazione dispositivo, Severity: 0, Value: BIN3-6B, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Id dispositivo, Severity: 0, Value: IC06VE2B03T2593M, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Informazioni aggiuntive, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 9, Array Id: 0, Name: Versione MIB, Severity: 0, Value: Version: 1.0, Build 09:34, 23/09/2009, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Informazioni di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome host dell'apparato, Severity: 0, Value: IC06VE2B03T2593M, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Numero di interfacce di rete, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Descrizione interfaccia di rete #1, Severity: 0, Value: Main Network Interface, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: MAC Address interfaccia di rete #1, Severity: 0, Value: 00-19-99-68-28-B5, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP interfaccia di rete #1, Severity: 0, Value: 10.104.13.12, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Maschera di rete interfaccia di rete #1, Severity: 0, Value: 255.255.255.0, Description: Valore ricevuto=0\r\nField Id: 6, Array Id: 0, Name: Gateway interfaccia di rete #1, Severity: 0, Value: 10.104.13.201, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: DNS primario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: DNS secondario interfaccia di rete #1, Severity: 0, Value: 0.0.0.0, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: MTU interfaccia di rete #1, Severity: 0, Value: 1500, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Informazioni sistema, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Data e ora del sistema, Severity: 0, Value: 13/06/2011 18:02:59, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Data e ora ultimo riavvio del sistema, Severity: 0, Value: 12/06/2011 17:04:18, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Numero di riavvii del sistema, Severity: 0, Value: 364, Description: Valore ricevuto=0\r\nStream Id: 5, Name: Informazioni sulla connessione, Severity: 0, Description: In esecuzione e connesso=0;Valore entro i limiti normali=0;Valore entro i limiti normali=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato della connessione, Severity: 0, Value: 1, Description: In esecuzione e connesso=0\r\nField Id: 1, Array Id: 0, Name: Tempo medio di risposta del web service (msec), Severity: 0, Value: 0 giorni, 00:00:01.430, Description: Valore entro i limiti normali=0\r\nField Id: 2, Array Id: 0, Name: Tempo di risposta all'ultima chiamata del web service (msec), Severity: 0, Value: 0 giorni, 00:00:00.190, Description: Valore entro i limiti normali=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva relativa alla connessione, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 6, Name: Informazioni ambientali - Temperatura, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di temperatura installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Temperatura del sensore #1 (°C), Severity: 0, Value: 47, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Main Temperature Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 7, Name: Informazioni ambientali - Umidità, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di umidità installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 8, Name: Informazioni ambientali - Riscaldatori, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Numero di riscaldatori installati, Severity: 0, Value: 0, Description: Valore ricevuto=0\r\nStream Id: 9, Name: Informazioni ambientali - Ventole, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0;Valore ricevuto=0;Valore ricevuto=0;In funzione=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di ventole installate, Severity: 0, Value: 3, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo ventola #1, Severity: 0, Value: Top, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Velocità della ventola (%) #1, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Stato della ventola #1, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 4, Array Id: 0, Name: Descrizione della ventola #1, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva della ventola #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo ventola #2, Severity: 0, Value: Middle, Description: Valore ricevuto=0\r\nField Id: 7, Array Id: 0, Name: Velocità della ventola (%) #2, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 8, Array Id: 0, Name: Stato della ventola #2, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 9, Array Id: 0, Name: Descrizione della ventola #2, Severity: 0, Value: FAN , Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva della ventola #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 11, Array Id: 0, Name: Tipo ventola #3, Severity: 0, Value: Bottom, Description: Valore ricevuto=0\r\nField Id: 12, Array Id: 0, Name: Velocità della ventola (%) #3, Severity: 0, Value: 100, Description: Valore ricevuto=0\r\nField Id: 13, Array Id: 0, Name: Stato della ventola #3, Severity: 0, Value: 1, Description: In funzione=0\r\nField Id: 14, Array Id: 0, Name: Descrizione della ventola #3, Severity: 0, Value: FAN, Description: Valore ricevuto=0\r\nField Id: 15, Array Id: 0, Name: Severità complessiva della ventola #3, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 10, Name: Informazioni ambientali - Sensori di luminosità, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Installato=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di sensori di luminosità installati, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Luminosità rilevata dal sensore (%) #1, Severity: 0, Value: 232, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Stato del sensore #1, Severity: 0, Value: 2, Description: Installato=0\r\nField Id: 3, Array Id: 0, Name: Descrizione del sensore #1, Severity: 0, Value: Environment Light Sensor, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Severità complessiva relativa al sensore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 11, Name: Stato del display, Severity: 0, Description: Acceso=0;Nessun guasto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Stato di accensione del display (pilotato dalla CPU), Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 1, Array Id: 0, Name: Causa del guasto, Severity: 0, Value: 1, Description: Nessun guasto=0\r\nField Id: 2, Array Id: 0, Name: Severità complessiva relativa al display, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 13, Name: Stato lampade per retroilluminazione, Severity: 0, Description: Valore ricevuto=0;Accesa=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di lampade per retroilluminazione installate, Severity: 0, Value: 1, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Stato lampada per retroilluminazione #1, Severity: 0, Value: 4, Description: Accesa=0\r\nField Id: 2, Array Id: 0, Name: Descrizione della lampada per retroilluminazione #1, Severity: 0, Value: Backlight Lamps, Description: Valore ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Severità complessiva della lampada per retroilluminazione #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nStream Id: 14, Name: Stato alimentatori, Severity: 0, Description: Valore ricevuto=0;p5V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0;p12V=0;Acceso=0;Valore ricevuto=0;Valore ricevuto=0;Nessun allarme=0\r\nField Id: 0, Array Id: 0, Name: Numero di alimentatori installati, Severity: 0, Value: 2, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Tipo alimentatore #1, Severity: 0, Value: 2, Description: p5V=0\r\nField Id: 2, Array Id: 0, Name: Stato alimentatore #1, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 3, Array Id: 0, Name: Tensione in uscita dell'alimentatore #1 (V o mV), Severity: 0, Value: 511, Description: Valore ricevuto=0\r\nField Id: 4, Array Id: 0, Name: Descrizione dell'alimentatore #1, Severity: 0, Value: Panel PS, Description: Valore ricevuto=0\r\nField Id: 5, Array Id: 0, Name: Severità complessiva dell'alimentatore #1, Severity: 0, Value: 0, Description: Nessun allarme=0\r\nField Id: 6, Array Id: 0, Name: Tipo alimentatore #2, Severity: 0, Value: 3, Description: p12V=0\r\nField Id: 7, Array Id: 0, Name: Stato alimentatore #2, Severity: 0, Value: 1, Description: Acceso=0\r\nField Id: 8, Array Id: 0, Name: Tensione in uscita dell'alimentatore #2 (V o mV), Severity: 0, Value: 1192, Description: Valore ricevuto=0\r\nField Id: 9, Array Id: 0, Name: Descrizione dell'alimentatore #2, Severity: 0, Value: Inverter PS, Description: Valore ricevuto=0\r\nField Id: 10, Array Id: 0, Name: Severità complessiva dell'alimentatore #2, Severity: 0, Value: 0, Description: Nessun allarme=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
	[TestFixture]
	public class LibraryIntegrationTestsCNS_VoIP
	{
		[Test]
		public void DumpCompleto_definition_DeviceTypeVP69000()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034826");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: cns-4-10-ds-1 Interfaccia di Diffusione Sonora (Telefin - DS), Type: VP69000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-10-ds-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-10-ds-1 2.6.33 #1 Mon Feb 4 14:46:52 CET 2013 armv5tejl, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 04:08:16.680, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf), Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Unknown (configure /etc/snmp/snmpd.local.conf), Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 0, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Web server, Severity: 0, Value: cherokee, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Server SSH, Severity: 0, Value: dropbear, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Agente di diagnostica SNMP, Severity: 0, Value: snmpsubagent, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Sincronizzazione oraria, Severity: 0, Value: ntpd, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Client VoIP, Severity: 0, Value: linphonec, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Diffusione sonora, Severity: 0, Value: dsgd, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Amplificazione, Severity: 0, Value: amplid, Description: In esecuzione=0\r\nField Id: 7, Array Id: 0, Name: Monitoraggio processi, Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP00010()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034305");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: cns-4-pbx-1 IP-PBX Centralino VoIP (Telefin - telefonia VoIP), Type: VP00010, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-pbx-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-pbx-1 2.6.32-5-686 #1 SMP Mon Feb 25 01:04:36 UTC 2013 i686, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 06:13:57.730, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Me <me@example.org>, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Sitting on the Dock of the Bay, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 0, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Agente di diagnostica Zabbix, Severity: 0, Value: zabbix_agentd, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Watchdog, Severity: 0, Value: watchdog, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Server SSH, Severity: 0, Value: sshd, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Provisioning, Severity: 0, Value: rsync, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Sincronizzazione oraria, Severity: 0, Value: ntpd, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Database, Severity: 0, Value: mysqld, Description: In esecuzione=0\r\nField Id: 6, Array Id: 0, Name: Web server, Severity: 0, Value: httpd, Description: In esecuzione=0\r\nField Id: 7, Array Id: 0, Name: Ridondanza, Severity: 0, Value: heartbeat, Description: In esecuzione=0\r\nField Id: 8, Array Id: 0, Name: Server DHCP, Severity: 0, Value: dhcpd, Description: In esecuzione=0\r\nField Id: 9, Array Id: 0, Name: Centralino VoIP, Severity: 0, Value: asterisk, Description: In esecuzione=0\r\nField Id: 10, Array Id: 0, Name: Monitoraggio processi, Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP66000_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034935");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: cns-4-10-ata-1 ATA FXS 2 linee (Telefin - telefonia VoIP), Type: VP66000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-10-ata-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-10-ata-1 2.6.33 #1 Mon Feb 4 11:32:46 CET 2013 armv5tejl, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 04:09:25.370, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf), Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Unknown (configure /etc/snmp/snmpd.local.conf), Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 0, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Server SSH, Severity: 0, Value: dropbear, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Sincronizzazione oraria, Severity: 0, Value: ntpd, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Schedulazione, Severity: 0, Value: cron, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Web server, Severity: 0, Value: cherokee, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Client VoIP, Severity: 0, Value: asterisk, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Monitoraggio processi, Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP66000_2()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034936");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: cns-4-10-ata-2 ATA FXS 2 linee (Telefin - telefonia VoIP), Type: VP66000, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: cns-4-10-ata-2, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: Linux cns-4-10-ata-2 2.6.33 #1 Mon Feb 4 11:32:46 CET 2013 armv5tejl, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 04:09:54.410, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Root <root@localhost> (configure /etc/snmp/snmpd.local.conf), Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Unknown (configure /etc/snmp/snmpd.local.conf), Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Stato processi di sistema, Severity: 0, Description: In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0;In esecuzione=0\r\nField Id: 0, Array Id: 0, Name: Server SSH, Severity: 0, Value: dropbear, Description: In esecuzione=0\r\nField Id: 1, Array Id: 0, Name: Sincronizzazione oraria, Severity: 0, Value: ntpd, Description: In esecuzione=0\r\nField Id: 2, Array Id: 0, Name: Schedulazione, Severity: 0, Value: cron, Description: In esecuzione=0\r\nField Id: 3, Array Id: 0, Name: Web server, Severity: 0, Value: cherokee, Description: In esecuzione=0\r\nField Id: 4, Array Id: 0, Name: Client VoIP, Severity: 0, Value: asterisk, Description: In esecuzione=0\r\nField Id: 5, Array Id: 0, Name: Monitoraggio processi, Severity: 0, Value: monit, Description: In esecuzione=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP30006()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168034570");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: IGS-801M Switch 8 porte (Switch Planet IGS-801M), Type: VP30006, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: IGS-801M, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: PLANET IGS-801M Managed Industrial Switch, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 17 giorni, 02:12:48.000, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: SYSTEM CONTACT, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: SYSTEM LOCATION, Description: Locazione ricevuta=-255\r\nStream Id: 3, Name: Informazioni porte switch, Severity: -255, Description: Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;1 Gbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 9, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 2, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 00:00:03.000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 7, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 10, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 0 giorni, 00:01:10.000, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 12, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 15, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 0 giorni, 00:01:10.000, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 17, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 19, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 20, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 0 giorni, 00:02:45.000, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 22, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 23, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 24, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 25, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 0 giorni, 00:00:05.000, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 0 giorni, 00:00:03.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Tipo porta #07, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 32, Array Id: 0, Name: Velocità porta #07, Severity: -255, Value: 1000000000, Description: 1 Gbit/s=-255\r\nField Id: 33, Array Id: 0, Name: Stato attivazione porta #07, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 35, Array Id: 0, Name: Ultima modifica porta #07, Severity: -255, Value: 0 giorni, 01:36:26.000, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Tipo porta #08, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 37, Array Id: 0, Name: Velocità porta #08, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 38, Array Id: 0, Name: Stato attivazione porta #08, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 39, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 40, Array Id: 0, Name: Ultima modifica porta #08, Severity: -255, Value: 0 giorni, 00:00:03.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP50000_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168035073");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: Modem Keymile LineRunner SCADA NG Master, Type: VP50000, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Forced serial number by SNMP query: 115204\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: LR SCADA BMD NG, V6.00.04 Nov 13 2009 10:00, Description: Descrizione ricevuta=-255\r\nField Id: 1, Array Id: 0, Name: Up time, Severity: 0, Value: 39 giorni, 01:52:48.580, Description: Up Time ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: KEYMILE, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Master 63, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;manKeymile=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Descrizione modulo, Severity: -255, Value: LR SCADA BMD NG, V6.00.04 Nov 13 2009 10:00, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Produttore, Severity: -255, Value: 11, Description: manKeymile=-255\r\nField Id: 3, Array Id: 0, Name: Codice identificativo modulo, Severity: -255, Value: 8960, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Numero di serie, Severity: -255, Value: 115204, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Anno di produzione, Severity: -255, Value: 2012, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Settimana di produzione, Severity: -255, Value: 38, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Numero di versione hardware, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Numero di revisione hardware, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni interfacce, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;1984 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;RS-232=-255;19.2 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: Line1:LR SCADA LI NG 2M, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 1984000, Description: 1984 Kbit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: MAC Address porta #01, Severity: -255, Value: --, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: ETH2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 100000, Description: 100 Kbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: MAC Address porta #02, Severity: -255, Value: 00-E0-DF-61-5D-FB, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: User RS 232D, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 33, Description: RS-232=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 19200, Description: 19.2 Kbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: MAC Address porta #03, Severity: -255, Value: --, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceTypeVP50000_2()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168035074");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Snmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: Modem Keymile LineRunner SCADA NG Slave, Type: VP50000, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Forced serial number by SNMP query: 113940\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Descrizione sistema, Severity: -255, Value: LR SCADA BMD NG, V6.00.04 Nov 13 2009 10:00, Description: Descrizione ricevuta=-255\r\nField Id: 1, Array Id: 0, Name: Up time, Severity: 0, Value: 21 giorni, 09:39:08.580, Description: Up Time ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: KEYMILE, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Nome macchina, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Slave 01, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;manKeymile=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Descrizione modulo, Severity: -255, Value: LR SCADA BMD NG, V6.00.04 Nov 13 2009 10:00, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Produttore, Severity: -255, Value: 11, Description: manKeymile=-255\r\nField Id: 3, Array Id: 0, Name: Codice identificativo modulo, Severity: -255, Value: 8960, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Numero di serie, Severity: -255, Value: 113940, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Anno di produzione, Severity: -255, Value: 2012, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Settimana di produzione, Severity: -255, Value: 20, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Numero di versione hardware, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Numero di revisione hardware, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni interfacce, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;1984 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: Line2:LR SCADA LI NG 2M, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 1984000, Description: 1984 Kbit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: MAC Address porta #01, Severity: -255, Value: --, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: ETH2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 100000, Description: 100 Kbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: MAC Address porta #02, Severity: -255, Value: 00-E0-DF-6D-DB-FF, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceType_VP18007_1()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168035338");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Icmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: Telefono Grandstream GXP2110 (Grandstream - telefonia VoIP), Type: VP18007, Offline: 0, Severity: 0, ReplyICMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Risposta ricevuta=0\r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 0, Value: 1, Description: Risposta ricevuta=0\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definition_DeviceType_VP18007_2()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168035339");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (IcmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsFalse(device.IsAliveIcmp, "IsAliveIcmp is true");

					Assert.AreEqual(1, device.Offline);
					Assert.AreEqual(Severity.Unknown, device.SeverityLevel);
					Assert.AreEqual(false, device.DeviceConfigurationParameters.DefinitionFileIsByDevice);
					Assert.AreEqual("1.00", device.DeviceConfigurationParameters.DefinitionVersion);
					Assert.AreEqual(DeviceKind.Icmp, device.DeviceConfigurationParameters.DefinitionType);

					Assert.AreEqual(
						"Device Name: Telefono Grandstream GXP2110 (Grandstream - telefonia VoIP), Type: VP18007, Offline: 1, Severity: 255, ReplyICMP: False\r\nStream Id: 1, Name: Stato generale, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Esito ping, Severity: 255, Value: 0, Description: Stato sconosciuto\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}
	}
}
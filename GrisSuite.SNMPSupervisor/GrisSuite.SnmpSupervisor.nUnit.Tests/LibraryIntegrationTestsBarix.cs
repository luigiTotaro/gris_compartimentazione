﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsBarix
    {
        #region Test Barix

        [Test]
        public void DumpCompleto_definitionPEAUBC105_Barionet_Dispositivo1_OK()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByOriginalDevId(5600);

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Unità Centrale Sat1, Type: PEAUBC105, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Sistema in funzione=0;Link Cobranet 1 connesso=0;Link Cobranet 2 connesso=0;Link Cobranet 3 connesso=0;Amplificatori normali attivi=0;Amplificatori in servizio=0\r\nField Id: 0, Array Id: 0, Name: Stato sistema, Severity: 0, Value: 1, Description: Sistema in funzione=0\r\nField Id: 1, Array Id: 0, Name: Stato link Cobranet 1, Severity: 0, Value: 1, Description: Link Cobranet 1 connesso=0\r\nField Id: 2, Array Id: 0, Name: Stato link Cobranet 2, Severity: 0, Value: 1, Description: Link Cobranet 2 connesso=0\r\nField Id: 3, Array Id: 0, Name: Stato link Cobranet 3, Severity: 0, Value: 1, Description: Link Cobranet 3 connesso=0\r\nField Id: 4, Array Id: 0, Name: Stato applicativo amplificatori, Severity: 0, Value: 1, Description: Amplificatori normali attivi=0\r\nField Id: 5, Array Id: 0, Name: Stato servizio amplificatori, Severity: 0, Value: 1, Description: Amplificatori in servizio=0\r\nStream Id: 2, Name: Informazioni hardware diagnostica, Severity: 0, Description: Descrizione ricevuta=0;UpTime ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Descrizione, Severity: 0, Value: BARIX Barionet snr  02.13 20080204, Description: Descrizione ricevuta=0\r\nField Id: 1, Array Id: 0, Name: UpTime, Severity: 0, Value: 196 giorni, 19:19:32.160, Description: UpTime ricevuto=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionPEAUBA100_Barionet_Dispositivo1_OK()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByOriginalDevId(5601);

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Amplificatore Sat1 1, Type: PEAUBA100, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Amplificatore in funzione=0\r\nField Id: 0, Array Id: 0, Name: Stato amplificatore, Severity: 0, Value: 1, Description: Amplificatore in funzione=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void DumpCompleto_definitionPEAUBA200_Barionet_Dispositivo1_OK()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByOriginalDevId(5602);

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Ok, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Amplificatore Sat1 2, Type: PEAUBA200, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Stato generale, Severity: 0, Description: Amplificatore in funzione=0\r\nField Id: 0, Array Id: 0, Name: Stato amplificatore, Severity: 0, Value: 1, Description: Amplificatore in funzione=0\r\n",
                        deviceDump);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
	[TestFixture]
	public class LibraryIntegrationTestsRAD
	{
		#region Test RAD Data senza definizione

		[Test]
		public void DumpCompleto_definitionRAD_Generico_Offline()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174479970");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsFalse(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Warning, device.SeverityLevel);

					Assert.AreEqual("Diagnostica non disponibile", device.ValueDescriptionComplete);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data Optimux 34

		[Test]
		public void DumpCompleto_definitionRADOPT34_000_1_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174479971");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: RAD Data Comm. Optimux 34 01, Type: RADOPT34, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADOPT34_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Optimux-34=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Optimux _Priverno_CCL_DM, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Optimux-34 HW Version: 1.00/B, SW Version: 2.87, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 320 giorni, 22:25:12.390, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.123, Description: Optimux-34=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Attiva=-255;Attiva=-255;Non disponibile=-255;op34E1=-255;Valore ricevuto=-255;Abilitata=-255;Abilitata=-255\r\nField Id: 0, Array Id: 0, Name: Versione hardware, Severity: -255, Value: Hw Version: 1.00/B, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: -255, Value: SW Version : 2.87, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Stato alimentazione primaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 3, Array Id: 0, Name: Stato alimentazione secondaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Ridondanza linea, Severity: -255, Value: 3, Description: Non disponibile=-255\r\nField Id: 5, Array Id: 0, Name: Tipo di prodotto Far-End, Severity: -255, Value: 22, Description: op34E1=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.91.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 8, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Fast Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Fast Ethernet=-255;Non disponibile=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;PropMultiLink=-255;34 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Tipo porta ethernet di gestione, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 2, Array Id: 0, Name: Velocità porta ethernet di gestione, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Stato attivazione porta ethernet di gestione, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta ethernet di gestione, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Ultima modifica porta ethernet di gestione, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tipo porta ethernet utente, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 7, Array Id: 0, Name: Velocità porta ethernet utente, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 8, Array Id: 0, Name: Stato attivazione porta ethernet utente, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta ethernet utente, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 10, Array Id: 0, Name: Ultima modifica porta ethernet utente, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Tipo porta E1 nr. 1, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 12, Array Id: 0, Name: Velocità porta E1 nr. 1, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Stato attivazione porta E1 nr. 1, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta E1 nr. 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 15, Array Id: 0, Name: Ultima modifica porta E1 nr. 1, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo porta E1 nr. 2, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 17, Array Id: 0, Name: Velocità porta E1 nr. 2, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Stato attivazione porta E1 nr. 2, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 19, Array Id: 0, Name: Stato connessione porta E1 nr. 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 20, Array Id: 0, Name: Ultima modifica porta E1 nr. 2, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Tipo porta E1 nr. 3, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 22, Array Id: 0, Name: Velocità porta E1 nr. 3, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 23, Array Id: 0, Name: Stato attivazione porta E1 nr. 3, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 24, Array Id: 0, Name: Stato connessione porta E1 nr. 3, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 25, Array Id: 0, Name: Ultima modifica porta E1 nr. 3, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta E1 nr. 4, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta E1 nr. 4, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta E1 nr. 4, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta E1 nr. 4, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta E1 nr. 4, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Tipo porta E1 nr. 5, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 32, Array Id: 0, Name: Velocità porta E1 nr. 5, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 33, Array Id: 0, Name: Stato attivazione porta E1 nr. 5, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione porta E1 nr. 5, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 35, Array Id: 0, Name: Ultima modifica porta E1 nr. 5, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Tipo porta E1 nr. 6, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 37, Array Id: 0, Name: Velocità porta E1 nr. 6, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 38, Array Id: 0, Name: Stato attivazione porta E1 nr. 6, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 39, Array Id: 0, Name: Stato connessione porta E1 nr. 6, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 40, Array Id: 0, Name: Ultima modifica porta E1 nr. 6, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Tipo porta E1 nr. 7, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 42, Array Id: 0, Name: Velocità porta E1 nr. 7, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 43, Array Id: 0, Name: Stato attivazione porta E1 nr. 7, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 44, Array Id: 0, Name: Stato connessione porta E1 nr. 7, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 45, Array Id: 0, Name: Ultima modifica porta E1 nr. 7, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 46, Array Id: 0, Name: Tipo porta E1 nr. 8, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 47, Array Id: 0, Name: Velocità porta E1 nr. 8, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 48, Array Id: 0, Name: Stato attivazione porta E1 nr. 8, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 49, Array Id: 0, Name: Stato connessione porta E1 nr. 8, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 50, Array Id: 0, Name: Ultima modifica porta E1 nr. 8, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 51, Array Id: 0, Name: Tipo porta E1 nr. 9, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 52, Array Id: 0, Name: Velocità porta E1 nr. 9, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 53, Array Id: 0, Name: Stato attivazione porta E1 nr. 9, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 54, Array Id: 0, Name: Stato connessione porta E1 nr. 9, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 55, Array Id: 0, Name: Ultima modifica porta E1 nr. 9, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta E1 nr. 10, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta E1 nr. 10, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta E1 nr. 10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta E1 nr. 10, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta E1 nr. 10, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Tipo porta E1 nr. 11, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 62, Array Id: 0, Name: Velocità porta E1 nr. 11, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 63, Array Id: 0, Name: Stato attivazione porta E1 nr. 11, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 64, Array Id: 0, Name: Stato connessione porta E1 nr. 11, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 65, Array Id: 0, Name: Ultima modifica porta E1 nr. 11, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 66, Array Id: 0, Name: Tipo porta E1 nr. 12, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 67, Array Id: 0, Name: Velocità porta E1 nr. 12, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 68, Array Id: 0, Name: Stato attivazione porta E1 nr. 12, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 69, Array Id: 0, Name: Stato connessione porta E1 nr. 12, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 70, Array Id: 0, Name: Ultima modifica porta E1 nr. 12, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 71, Array Id: 0, Name: Tipo porta E1 nr. 13, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 72, Array Id: 0, Name: Velocità porta E1 nr. 13, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 73, Array Id: 0, Name: Stato attivazione porta E1 nr. 13, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 74, Array Id: 0, Name: Stato connessione porta E1 nr. 13, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 75, Array Id: 0, Name: Ultima modifica porta E1 nr. 13, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 76, Array Id: 0, Name: Tipo porta E1 nr. 14, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 77, Array Id: 0, Name: Velocità porta E1 nr. 14, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 78, Array Id: 0, Name: Stato attivazione porta E1 nr. 14, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 79, Array Id: 0, Name: Stato connessione porta E1 nr. 14, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 80, Array Id: 0, Name: Ultima modifica porta E1 nr. 14, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 81, Array Id: 0, Name: Tipo porta E1 nr. 15, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 82, Array Id: 0, Name: Velocità porta E1 nr. 15, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 83, Array Id: 0, Name: Stato attivazione porta E1 nr. 15, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 84, Array Id: 0, Name: Stato connessione porta E1 nr. 15, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 85, Array Id: 0, Name: Ultima modifica porta E1 nr. 15, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta E1 nr. 16, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta E1 nr. 16, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta E1 nr. 16, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta E1 nr. 16, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta E1 nr. 16, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 91, Array Id: 0, Name: Tipo porta E3 link A, Severity: -255, Value: 54, Description: PropMultiLink=-255\r\nField Id: 92, Array Id: 0, Name: Velocità porta E3 link A, Severity: -255, Value: 34368000, Description: 34 Mbit/s=-255\r\nField Id: 93, Array Id: 0, Name: Stato attivazione porta E3 link A, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 94, Array Id: 0, Name: Stato connessione porta E3 link A, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 95, Array Id: 0, Name: Ultima modifica porta E3 link A, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Moduli montati, Severity: -255, Description: E1 quattro Two BNC=-255;E1 quattro Two BNC=-255;E1 quattro Two BNC=-255;E1 quattro Two BNC=-255;E3 link Fibra Ottica 1=-255;E3 link Fibra Ottica 1=-255\r\nField Id: 0, Array Id: 0, Name: Modulo E1 nr. 1, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 1, Array Id: 0, Name: Modulo E1 nr. 2, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 2, Array Id: 0, Name: Modulo E1 nr. 3, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 3, Array Id: 0, Name: Modulo E1 nr. 4, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 4, Array Id: 0, Name: Modulo E3 nr. 1, Severity: -255, Value: 4, Description: E3 link Fibra Ottica 1=-255\r\nField Id: 5, Array Id: 0, Name: Modulo E3 nr. 2, Severity: -255, Value: 4, Description: E3 link Fibra Ottica 1=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADOPT34_000_2_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174479972");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: RAD Data Comm. Optimux 34 02, Type: RADOPT34, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADOPT34_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Optimux-34=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Optimux, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Optimux-34 HW Version: 1.00/B, SW Version: 2.87, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 40 giorni, 00:08:43.410, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.123, Description: Optimux-34=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Attiva=-255;Attiva=-255;Non disponibile=-255;op34E1=-255;Valore ricevuto=-255;Abilitata=-255;Abilitata=-255\r\nField Id: 0, Array Id: 0, Name: Versione hardware, Severity: -255, Value: Hw Version: 1.00/B, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: -255, Value: SW Version : 2.87, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Stato alimentazione primaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 3, Array Id: 0, Name: Stato alimentazione secondaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Ridondanza linea, Severity: -255, Value: 3, Description: Non disponibile=-255\r\nField Id: 5, Array Id: 0, Name: Tipo di prodotto Far-End, Severity: -255, Value: 22, Description: op34E1=-255\r\nField Id: 6, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.91.201, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 8, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Fast Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Fast Ethernet=-255;Non disponibile=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;PropMultiLink=-255;34 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Tipo porta ethernet di gestione, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 2, Array Id: 0, Name: Velocità porta ethernet di gestione, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Stato attivazione porta ethernet di gestione, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta ethernet di gestione, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Ultima modifica porta ethernet di gestione, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tipo porta ethernet utente, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 7, Array Id: 0, Name: Velocità porta ethernet utente, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 8, Array Id: 0, Name: Stato attivazione porta ethernet utente, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta ethernet utente, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 10, Array Id: 0, Name: Ultima modifica porta ethernet utente, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Tipo porta E1 nr. 1, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 12, Array Id: 0, Name: Velocità porta E1 nr. 1, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Stato attivazione porta E1 nr. 1, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta E1 nr. 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 15, Array Id: 0, Name: Ultima modifica porta E1 nr. 1, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo porta E1 nr. 2, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 17, Array Id: 0, Name: Velocità porta E1 nr. 2, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Stato attivazione porta E1 nr. 2, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 19, Array Id: 0, Name: Stato connessione porta E1 nr. 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 20, Array Id: 0, Name: Ultima modifica porta E1 nr. 2, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Tipo porta E1 nr. 3, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 22, Array Id: 0, Name: Velocità porta E1 nr. 3, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 23, Array Id: 0, Name: Stato attivazione porta E1 nr. 3, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 24, Array Id: 0, Name: Stato connessione porta E1 nr. 3, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 25, Array Id: 0, Name: Ultima modifica porta E1 nr. 3, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta E1 nr. 4, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta E1 nr. 4, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta E1 nr. 4, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta E1 nr. 4, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta E1 nr. 4, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Tipo porta E1 nr. 5, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 32, Array Id: 0, Name: Velocità porta E1 nr. 5, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 33, Array Id: 0, Name: Stato attivazione porta E1 nr. 5, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione porta E1 nr. 5, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 35, Array Id: 0, Name: Ultima modifica porta E1 nr. 5, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Tipo porta E1 nr. 6, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 37, Array Id: 0, Name: Velocità porta E1 nr. 6, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 38, Array Id: 0, Name: Stato attivazione porta E1 nr. 6, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 39, Array Id: 0, Name: Stato connessione porta E1 nr. 6, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 40, Array Id: 0, Name: Ultima modifica porta E1 nr. 6, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Tipo porta E1 nr. 7, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 42, Array Id: 0, Name: Velocità porta E1 nr. 7, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 43, Array Id: 0, Name: Stato attivazione porta E1 nr. 7, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 44, Array Id: 0, Name: Stato connessione porta E1 nr. 7, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 45, Array Id: 0, Name: Ultima modifica porta E1 nr. 7, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 46, Array Id: 0, Name: Tipo porta E1 nr. 8, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 47, Array Id: 0, Name: Velocità porta E1 nr. 8, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 48, Array Id: 0, Name: Stato attivazione porta E1 nr. 8, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 49, Array Id: 0, Name: Stato connessione porta E1 nr. 8, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 50, Array Id: 0, Name: Ultima modifica porta E1 nr. 8, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 51, Array Id: 0, Name: Tipo porta E1 nr. 9, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 52, Array Id: 0, Name: Velocità porta E1 nr. 9, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 53, Array Id: 0, Name: Stato attivazione porta E1 nr. 9, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 54, Array Id: 0, Name: Stato connessione porta E1 nr. 9, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 55, Array Id: 0, Name: Ultima modifica porta E1 nr. 9, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta E1 nr. 10, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta E1 nr. 10, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta E1 nr. 10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta E1 nr. 10, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta E1 nr. 10, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Tipo porta E1 nr. 11, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 62, Array Id: 0, Name: Velocità porta E1 nr. 11, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 63, Array Id: 0, Name: Stato attivazione porta E1 nr. 11, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 64, Array Id: 0, Name: Stato connessione porta E1 nr. 11, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 65, Array Id: 0, Name: Ultima modifica porta E1 nr. 11, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 66, Array Id: 0, Name: Tipo porta E1 nr. 12, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 67, Array Id: 0, Name: Velocità porta E1 nr. 12, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 68, Array Id: 0, Name: Stato attivazione porta E1 nr. 12, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 69, Array Id: 0, Name: Stato connessione porta E1 nr. 12, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 70, Array Id: 0, Name: Ultima modifica porta E1 nr. 12, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 71, Array Id: 0, Name: Tipo porta E1 nr. 13, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 72, Array Id: 0, Name: Velocità porta E1 nr. 13, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 73, Array Id: 0, Name: Stato attivazione porta E1 nr. 13, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 74, Array Id: 0, Name: Stato connessione porta E1 nr. 13, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 75, Array Id: 0, Name: Ultima modifica porta E1 nr. 13, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 76, Array Id: 0, Name: Tipo porta E1 nr. 14, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 77, Array Id: 0, Name: Velocità porta E1 nr. 14, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 78, Array Id: 0, Name: Stato attivazione porta E1 nr. 14, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 79, Array Id: 0, Name: Stato connessione porta E1 nr. 14, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 80, Array Id: 0, Name: Ultima modifica porta E1 nr. 14, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 81, Array Id: 0, Name: Tipo porta E1 nr. 15, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 82, Array Id: 0, Name: Velocità porta E1 nr. 15, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 83, Array Id: 0, Name: Stato attivazione porta E1 nr. 15, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 84, Array Id: 0, Name: Stato connessione porta E1 nr. 15, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 85, Array Id: 0, Name: Ultima modifica porta E1 nr. 15, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta E1 nr. 16, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta E1 nr. 16, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta E1 nr. 16, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta E1 nr. 16, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta E1 nr. 16, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 91, Array Id: 0, Name: Tipo porta E3 link A, Severity: -255, Value: 54, Description: PropMultiLink=-255\r\nField Id: 92, Array Id: 0, Name: Velocità porta E3 link A, Severity: -255, Value: 34368000, Description: 34 Mbit/s=-255\r\nField Id: 93, Array Id: 0, Name: Stato attivazione porta E3 link A, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 94, Array Id: 0, Name: Stato connessione porta E3 link A, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 95, Array Id: 0, Name: Ultima modifica porta E3 link A, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nStream Id: 4, Name: Moduli montati, Severity: -255, Description: E1 quattro Two BNC=-255;E1 quattro Two BNC=-255;E1 quattro Two BNC=-255;E1 quattro Two BNC=-255;E3 link Fibra Ottica 1=-255;E3 link Fibra Ottica 1=-255\r\nField Id: 0, Array Id: 0, Name: Modulo E1 nr. 1, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 1, Array Id: 0, Name: Modulo E1 nr. 2, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 2, Array Id: 0, Name: Modulo E1 nr. 3, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 3, Array Id: 0, Name: Modulo E1 nr. 4, Severity: -255, Value: 44, Description: E1 quattro Two BNC=-255\r\nField Id: 4, Array Id: 0, Name: Modulo E3 nr. 1, Severity: -255, Value: 4, Description: E3 link Fibra Ottica 1=-255\r\nField Id: 5, Array Id: 0, Name: Modulo E3 nr. 2, Severity: -255, Value: 4, Description: E3 link Fibra Ottica 1=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data Optimux 108

		[Test]
		public void DumpCompleto_definitionRADOPT108_1_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174492912");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: RAD Data Comm. Optimux 108 01, Type: RADOPT108, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADOPT108_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Optimux-108=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Op2-T-Bologna-SFeliceSP, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Optimux-108 HW Version: 0.00/C SW version: 6.23, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 357 giorni, 00:08:51.830, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.137, Description: Optimux-108=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Attiva=-255;Nessuno=-255;Non disponibile=-255;Valore ricevuto=-255;Valore non classificato=-255;Abilitata=-255\r\nField Id: 0, Array Id: 0, Name: Versione hardware, Severity: -255, Value: 0.00/C  Firmware: 4AE Ver. 1.04, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: -255, Value: 6.23, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Stato alimentazione primaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 3, Array Id: 0, Name: Stato alimentazione secondaria, Severity: -255, Value: 6, Description: Nessuno=-255\r\nField Id: 4, Array Id: 0, Name: Ridondanza linea, Severity: -255, Value: 3, Description: Non disponibile=-255\r\nField Id: 5, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.140.201, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 0, Description: Valore non classificato=-255\r\nField Id: 7, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;PropMultiLink=-255;135 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;PPP=-255;115,2 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo porta E1 nr. 1, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 2, Array Id: 0, Name: Velocità porta E1 nr. 1, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Stato attivazione porta E1 nr. 1, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta E1 nr. 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Ultima modifica porta E1 nr. 1, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tipo porta E1 nr. 2, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 7, Array Id: 0, Name: Velocità porta E1 nr. 2, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Stato attivazione porta E1 nr. 2, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta E1 nr. 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 10, Array Id: 0, Name: Ultima modifica porta E1 nr. 2, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Tipo porta E1 nr. 3, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 12, Array Id: 0, Name: Velocità porta E1 nr. 3, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Stato attivazione porta E1 nr. 3, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta E1 nr. 3, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 15, Array Id: 0, Name: Ultima modifica porta E1 nr. 3, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo porta E1 nr. 4, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 17, Array Id: 0, Name: Velocità porta E1 nr. 4, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Stato attivazione porta E1 nr. 4, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 19, Array Id: 0, Name: Stato connessione porta E1 nr. 4, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 20, Array Id: 0, Name: Ultima modifica porta E1 nr. 4, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Tipo porta link A, Severity: -255, Value: 54, Description: PropMultiLink=-255\r\nField Id: 22, Array Id: 0, Name: Velocità porta link A, Severity: -255, Value: 135168000, Description: 135 Mbit/s=-255\r\nField Id: 23, Array Id: 0, Name: Stato attivazione porta link A, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 24, Array Id: 0, Name: Stato connessione porta link A, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 25, Array Id: 0, Name: Ultima modifica porta link A, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Tipo porta seriale di controllo, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 32, Array Id: 0, Name: Velocità porta seriale di controllo, Severity: -255, Value: 115200, Description: 115,2 Kbit/s=-255\r\nField Id: 33, Array Id: 0, Name: Stato attivazione porta seriale di controllo, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione porta seriale di controllo, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 35, Array Id: 0, Name: Ultima modifica porta seriale di controllo, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Tipo porta ethernet di gestione, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 37, Array Id: 0, Name: Velocità porta ethernet di gestione, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 38, Array Id: 0, Name: Stato attivazione porta ethernet di gestione, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 39, Array Id: 0, Name: Stato connessione porta ethernet di gestione, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 40, Array Id: 0, Name: Ultima modifica porta ethernet di gestione, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Tipo porta ethernet utente, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 42, Array Id: 0, Name: Velocità porta ethernet utente, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 43, Array Id: 0, Name: Stato attivazione porta ethernet utente, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 44, Array Id: 0, Name: Stato connessione porta ethernet utente, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 45, Array Id: 0, Name: Ultima modifica porta ethernet utente, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADOPT108_2_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511643");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: RAD Data Comm. Optimux 108 03, Type: RADOPT108, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADOPT108_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Optimux-108=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Optimux-108, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Optimux-108 HW Version: 0.00/C SW version: 6.22, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 148 giorni, 08:41:08.730, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.137, Description: Optimux-108=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Attiva=-255;Nessuno=-255;Non disponibile=-255;Valore ricevuto=-255;Valore non classificato=-255;Abilitata=-255\r\nField Id: 0, Array Id: 0, Name: Versione hardware, Severity: -255, Value: 0.00/C  Firmware: 4AE Ver. 1.04, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: -255, Value: 6.22, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Stato alimentazione primaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 3, Array Id: 0, Name: Stato alimentazione secondaria, Severity: -255, Value: 6, Description: Nessuno=-255\r\nField Id: 4, Array Id: 0, Name: Ridondanza linea, Severity: -255, Value: 3, Description: Non disponibile=-255\r\nField Id: 5, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.214.201, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 0, Description: Valore non classificato=-255\r\nField Id: 7, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;PropMultiLink=-255;135 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;PPP=-255;115,2 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo porta E1 nr. 1, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 2, Array Id: 0, Name: Velocità porta E1 nr. 1, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Stato attivazione porta E1 nr. 1, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta E1 nr. 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Ultima modifica porta E1 nr. 1, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tipo porta E1 nr. 2, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 7, Array Id: 0, Name: Velocità porta E1 nr. 2, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Stato attivazione porta E1 nr. 2, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta E1 nr. 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 10, Array Id: 0, Name: Ultima modifica porta E1 nr. 2, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Tipo porta E1 nr. 3, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 12, Array Id: 0, Name: Velocità porta E1 nr. 3, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Stato attivazione porta E1 nr. 3, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta E1 nr. 3, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 15, Array Id: 0, Name: Ultima modifica porta E1 nr. 3, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo porta E1 nr. 4, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 17, Array Id: 0, Name: Velocità porta E1 nr. 4, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Stato attivazione porta E1 nr. 4, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 19, Array Id: 0, Name: Stato connessione porta E1 nr. 4, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 20, Array Id: 0, Name: Ultima modifica porta E1 nr. 4, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Tipo porta link A, Severity: -255, Value: 54, Description: PropMultiLink=-255\r\nField Id: 22, Array Id: 0, Name: Velocità porta link A, Severity: -255, Value: 135168000, Description: 135 Mbit/s=-255\r\nField Id: 23, Array Id: 0, Name: Stato attivazione porta link A, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 24, Array Id: 0, Name: Stato connessione porta link A, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 25, Array Id: 0, Name: Ultima modifica porta link A, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Tipo porta seriale di controllo, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 32, Array Id: 0, Name: Velocità porta seriale di controllo, Severity: -255, Value: 115200, Description: 115,2 Kbit/s=-255\r\nField Id: 33, Array Id: 0, Name: Stato attivazione porta seriale di controllo, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione porta seriale di controllo, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 35, Array Id: 0, Name: Ultima modifica porta seriale di controllo, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Tipo porta ethernet di gestione, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 37, Array Id: 0, Name: Velocità porta ethernet di gestione, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 38, Array Id: 0, Name: Stato attivazione porta ethernet di gestione, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 39, Array Id: 0, Name: Stato connessione porta ethernet di gestione, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 40, Array Id: 0, Name: Ultima modifica porta ethernet di gestione, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Tipo porta ethernet utente, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 42, Array Id: 0, Name: Velocità porta ethernet utente, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 43, Array Id: 0, Name: Stato attivazione porta ethernet utente, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 44, Array Id: 0, Name: Stato connessione porta ethernet utente, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 45, Array Id: 0, Name: Ultima modifica porta ethernet utente, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADOPT108_3_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174493902");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: RAD Data Comm. Optimux 108 04, Type: RADOPT108, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADOPT108_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Optimux-108=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Optimux-108, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Optimux-108 HW Version: 0.01/B SW version: 6.21, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 68 giorni, 20:35:38.180, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.137, Description: Optimux-108=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Attiva=-255;Nessuno=-255;Non disponibile=-255;Valore ricevuto=-255;Valore non classificato=-255;Abilitata=-255\r\nField Id: 0, Array Id: 0, Name: Versione hardware, Severity: -255, Value: 0.01/B  Firmware: 4AB Ver. 1.00, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: -255, Value: 6.21, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Stato alimentazione primaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 3, Array Id: 0, Name: Stato alimentazione secondaria, Severity: -255, Value: 6, Description: Nessuno=-255\r\nField Id: 4, Array Id: 0, Name: Ridondanza linea, Severity: -255, Value: 3, Description: Non disponibile=-255\r\nField Id: 5, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.145.201, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 0, Description: Valore non classificato=-255\r\nField Id: 7, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;DS1=-255;8,5 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;PPP=-255;115,2 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo porta E1 nr. 1, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 2, Array Id: 0, Name: Velocità porta E1 nr. 1, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Stato attivazione porta E1 nr. 1, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta E1 nr. 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Ultima modifica porta E1 nr. 1, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tipo porta E1 nr. 2, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 7, Array Id: 0, Name: Velocità porta E1 nr. 2, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Stato attivazione porta E1 nr. 2, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta E1 nr. 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 10, Array Id: 0, Name: Ultima modifica porta E1 nr. 2, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Tipo porta E1 nr. 3, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 12, Array Id: 0, Name: Velocità porta E1 nr. 3, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Stato attivazione porta E1 nr. 3, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta E1 nr. 3, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 15, Array Id: 0, Name: Ultima modifica porta E1 nr. 3, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo porta E1 nr. 4, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 17, Array Id: 0, Name: Velocità porta E1 nr. 4, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Stato attivazione porta E1 nr. 4, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 19, Array Id: 0, Name: Stato connessione porta E1 nr. 4, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 20, Array Id: 0, Name: Ultima modifica porta E1 nr. 4, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Tipo porta link A, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 22, Array Id: 0, Name: Velocità porta link A, Severity: -255, Value: 8448000, Description: 8,5 Mbit/s=-255\r\nField Id: 23, Array Id: 0, Name: Stato attivazione porta link A, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 24, Array Id: 0, Name: Stato connessione porta link A, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 25, Array Id: 0, Name: Ultima modifica porta link A, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Tipo porta seriale di controllo, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 32, Array Id: 0, Name: Velocità porta seriale di controllo, Severity: -255, Value: 115200, Description: 115,2 Kbit/s=-255\r\nField Id: 33, Array Id: 0, Name: Stato attivazione porta seriale di controllo, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione porta seriale di controllo, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 35, Array Id: 0, Name: Ultima modifica porta seriale di controllo, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Tipo porta ethernet di gestione, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 37, Array Id: 0, Name: Velocità porta ethernet di gestione, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 38, Array Id: 0, Name: Stato attivazione porta ethernet di gestione, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 39, Array Id: 0, Name: Stato connessione porta ethernet di gestione, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 40, Array Id: 0, Name: Ultima modifica porta ethernet di gestione, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADOPT108_4_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174512134");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: RAD Data Comm. Optimux 108 02, Type: RADOPT108, Community: public, SNMP Version: V2, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADOPT108_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;Optimux-108=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Op2-P-To02-CED-1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Optimux-108 HW Version: 0.00/C SW version: 6.24, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 13 giorni, 06:21:25.910, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.137, Description: Optimux-108=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Attiva=-255;Nessuno=-255;Non disponibile=-255;Valore ricevuto=-255;Valore non classificato=-255;Abilitata=-255\r\nField Id: 0, Array Id: 0, Name: Versione hardware, Severity: -255, Value: 0.00/C  Firmware: 4AE Ver. 1.04, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Versione software, Severity: -255, Value: 6.24, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Stato alimentazione primaria, Severity: -255, Value: 4, Description: Attiva=-255\r\nField Id: 3, Array Id: 0, Name: Stato alimentazione secondaria, Severity: -255, Value: 6, Description: Nessuno=-255\r\nField Id: 4, Array Id: 0, Name: Ridondanza linea, Severity: -255, Value: 3, Description: Non disponibile=-255\r\nField Id: 5, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.217.201, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 0, Description: Valore non classificato=-255\r\nField Id: 7, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;PropMultiLink=-255;135 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;PPP=-255;115,2 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Tipo porta E1 nr. 1, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 2, Array Id: 0, Name: Velocità porta E1 nr. 1, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 3, Array Id: 0, Name: Stato attivazione porta E1 nr. 1, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 4, Array Id: 0, Name: Stato connessione porta E1 nr. 1, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 5, Array Id: 0, Name: Ultima modifica porta E1 nr. 1, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Tipo porta E1 nr. 2, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 7, Array Id: 0, Name: Velocità porta E1 nr. 2, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 8, Array Id: 0, Name: Stato attivazione porta E1 nr. 2, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 9, Array Id: 0, Name: Stato connessione porta E1 nr. 2, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 10, Array Id: 0, Name: Ultima modifica porta E1 nr. 2, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 11, Array Id: 0, Name: Tipo porta E1 nr. 3, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 12, Array Id: 0, Name: Velocità porta E1 nr. 3, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 13, Array Id: 0, Name: Stato attivazione porta E1 nr. 3, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 14, Array Id: 0, Name: Stato connessione porta E1 nr. 3, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 15, Array Id: 0, Name: Ultima modifica porta E1 nr. 3, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 16, Array Id: 0, Name: Tipo porta E1 nr. 4, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 17, Array Id: 0, Name: Velocità porta E1 nr. 4, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 18, Array Id: 0, Name: Stato attivazione porta E1 nr. 4, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 19, Array Id: 0, Name: Stato connessione porta E1 nr. 4, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 20, Array Id: 0, Name: Ultima modifica porta E1 nr. 4, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Tipo porta link A, Severity: -255, Value: 54, Description: PropMultiLink=-255\r\nField Id: 22, Array Id: 0, Name: Velocità porta link A, Severity: -255, Value: 135168000, Description: 135 Mbit/s=-255\r\nField Id: 23, Array Id: 0, Name: Stato attivazione porta link A, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 24, Array Id: 0, Name: Stato connessione porta link A, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 25, Array Id: 0, Name: Ultima modifica porta link A, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Tipo porta seriale di controllo, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 32, Array Id: 0, Name: Velocità porta seriale di controllo, Severity: -255, Value: 115200, Description: 115,2 Kbit/s=-255\r\nField Id: 33, Array Id: 0, Name: Stato attivazione porta seriale di controllo, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 34, Array Id: 0, Name: Stato connessione porta seriale di controllo, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 35, Array Id: 0, Name: Ultima modifica porta seriale di controllo, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 36, Array Id: 0, Name: Tipo porta ethernet di gestione, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 37, Array Id: 0, Name: Velocità porta ethernet di gestione, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 38, Array Id: 0, Name: Stato attivazione porta ethernet di gestione, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 39, Array Id: 0, Name: Stato connessione porta ethernet di gestione, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 40, Array Id: 0, Name: Ultima modifica porta ethernet di gestione, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 41, Array Id: 0, Name: Tipo porta ethernet utente, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 42, Array Id: 0, Name: Velocità porta ethernet utente, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 43, Array Id: 0, Name: Stato attivazione porta ethernet utente, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 44, Array Id: 0, Name: Stato connessione porta ethernet utente, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 45, Array Id: 0, Name: Ultima modifica porta ethernet utente, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data RICi E1

		[Test]
		public void DumpCompleto_definitionRADRICIE1_0_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174598383");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: Fast Ethernet over E1 NTU Pescara C.Le, Type: RADRICIE1, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADRICIE1_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;RICi-E1 Fast Ethernet over E1 NTU=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: RC1-T-ANCONA-PESCARACLE, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Fast Ethernet over E1 NTU, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 29 giorni, 18:56:12.310, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.5.37, Description: RICi-E1 Fast Ethernet over E1 NTU=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Abilitata=-255;Abilitata=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione agente hardware, Severity: -255, Value: Put your string here, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.104.40.201, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 3, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.104.40.239, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;E1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: E1 PORT UNBL, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 19, Description: E1=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: ETHERNET PORT, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #10, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADRICIE1_1_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174470814");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: Fast Ethernet over E1 NTU Voghera, Type: RADRICIE1, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADRICIE1_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;RICi-E1 Fast Ethernet over E1 NTU=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: RICi-E1, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Fast Ethernet over E1 NTU, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 21 giorni, 04:17:26.200, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.5.37, Description: RICi-E1 Fast Ethernet over E1 NTU=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Abilitata=-255;Abilitata=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione agente hardware, Severity: -255, Value: Put your string here, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.54.201, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 3, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.102.54.158, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;E1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 2, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: E1 PORT UNBL, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 19, Description: E1=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: ETHERNET PORT, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #10, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data MiRICi-E1

		[Test]
		public void DumpCompleto_definitionRADMIRICIE1_0_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174597360");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: MiRICi-E1 Gallarate, Type: RADMIRICIE1, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADMIRICIE1_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;MiRICi-E1 - Miniature Ethernet to E1 Remote Bridge=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: MiRICi, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: E1 Intelligent Converter, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 45 giorni, 08:11:50.510, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .0.0.105.82.73.67.105.0.108, Description: MiRICi-E1 - Miniature Ethernet to E1 Remote Bridge=-255\r\nStream Id: 2, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data RICi-4E1

		[Test]
		public void DumpCompleto_definitionRADRICI4E1_0_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174481921");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: Fast Ethernet over four E1 NTU Trieste C.le, Type: RADRICI4E1, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADRICI4E1_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;RICi-4E1 Mid-band Ethernet and Fast Ethernet over Four E1 NTU=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Ri1-P-Trieste-S.Server, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: Fast Ethernet over four E1 NTU, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 11 giorni, 14:45:12.470, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.6.6, Description: RICi-4E1 Mid-band Ethernet and Fast Ethernet over Four E1 NTU=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Abilitata=-255;Abilitata=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.98.201, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 3, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.102.98.1, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;Non disponibile=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;DS1=-255;Non disponibile=-255;Non attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP Multilink Bundle=-255;4 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;PPP=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;teLink=-255;4 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;teLink=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;teLink=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 19, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: ETH 1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: ETH 2, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: ETH 3, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: ETH 4, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: E1 PORT 1 BL, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: E1 PORT 2 BL, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Nome porta #07, Severity: -255, Value: E1 PORT 3 BL, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Tipo porta #07, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 39, Array Id: 0, Name: Velocità porta #07, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 40, Array Id: 0, Name: Stato attivazione porta #07, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 42, Array Id: 0, Name: Ultima modifica porta #07, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Nome porta #08, Severity: -255, Value: E1 PORT 4 BL, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Tipo porta #08, Severity: -255, Value: 18, Description: DS1=-255\r\nField Id: 45, Array Id: 0, Name: Velocità porta #08, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 46, Array Id: 0, Name: Stato attivazione porta #08, Severity: -255, Value: 2, Description: Non attiva=-255\r\nField Id: 47, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 48, Array Id: 0, Name: Ultima modifica porta #08, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome porta #09, Severity: -255, Value: LOGICAL PORT MLPPP, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Tipo porta #09, Severity: -255, Value: 108, Description: PPP Multilink Bundle=-255\r\nField Id: 51, Array Id: 0, Name: Velocità porta #09, Severity: -255, Value: 4096000, Description: 4 Mbit/s=-255\r\nField Id: 52, Array Id: 0, Name: Stato attivazione porta #09, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 53, Array Id: 0, Name: Stato connessione porta #09, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 54, Array Id: 0, Name: Ultima modifica porta #09, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: LOGICAL PORT 1, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #10, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #11, Severity: -255, Value: LOGICAL PORT 2, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #11, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #11, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #11, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #11, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 66, Array Id: 0, Name: Ultima modifica porta #11, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 67, Array Id: 0, Name: Nome porta #12, Severity: -255, Value: LOGICAL PORT 3, Description: Valore ricevuto=-255\r\nField Id: 68, Array Id: 0, Name: Tipo porta #12, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 69, Array Id: 0, Name: Velocità porta #12, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 70, Array Id: 0, Name: Stato attivazione porta #12, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 71, Array Id: 0, Name: Stato connessione porta #12, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 72, Array Id: 0, Name: Ultima modifica porta #12, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 73, Array Id: 0, Name: Nome porta #13, Severity: -255, Value: LOGICAL PORT 4, Description: Valore ricevuto=-255\r\nField Id: 74, Array Id: 0, Name: Tipo porta #13, Severity: -255, Value: 23, Description: PPP=-255\r\nField Id: 75, Array Id: 0, Name: Velocità porta #13, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 76, Array Id: 0, Name: Stato attivazione porta #13, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 77, Array Id: 0, Name: Stato connessione porta #13, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 78, Array Id: 0, Name: Ultima modifica porta #13, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 79, Array Id: 0, Name: Nome porta #14, Severity: -255, Value: Bridge Port 1, Description: Valore ricevuto=-255\r\nField Id: 80, Array Id: 0, Name: Tipo porta #14, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 81, Array Id: 0, Name: Velocità porta #14, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 82, Array Id: 0, Name: Stato attivazione porta #14, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 83, Array Id: 0, Name: Stato connessione porta #14, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 84, Array Id: 0, Name: Ultima modifica porta #14, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 85, Array Id: 0, Name: Nome porta #15, Severity: -255, Value: Bridge Port 2, Description: Valore ricevuto=-255\r\nField Id: 86, Array Id: 0, Name: Tipo porta #15, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 87, Array Id: 0, Name: Velocità porta #15, Severity: -255, Value: 4096000, Description: 4 Mbit/s=-255\r\nField Id: 88, Array Id: 0, Name: Stato attivazione porta #15, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 89, Array Id: 0, Name: Stato connessione porta #15, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 90, Array Id: 0, Name: Ultima modifica porta #15, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 91, Array Id: 0, Name: Nome porta #16, Severity: -255, Value: Bridge Port 3, Description: Valore ricevuto=-255\r\nField Id: 92, Array Id: 0, Name: Tipo porta #16, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 93, Array Id: 0, Name: Velocità porta #16, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 94, Array Id: 0, Name: Stato attivazione porta #16, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 95, Array Id: 0, Name: Stato connessione porta #16, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 96, Array Id: 0, Name: Ultima modifica porta #16, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 97, Array Id: 0, Name: Nome porta #17, Severity: -255, Value: Bridge Port 4, Description: Valore ricevuto=-255\r\nField Id: 98, Array Id: 0, Name: Tipo porta #17, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 99, Array Id: 0, Name: Velocità porta #17, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 100, Array Id: 0, Name: Stato attivazione porta #17, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 101, Array Id: 0, Name: Stato connessione porta #17, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 102, Array Id: 0, Name: Ultima modifica porta #17, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 103, Array Id: 0, Name: Nome porta #18, Severity: -255, Value: Bridge Port 5, Description: Valore ricevuto=-255\r\nField Id: 104, Array Id: 0, Name: Tipo porta #18, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 105, Array Id: 0, Name: Velocità porta #18, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 106, Array Id: 0, Name: Stato attivazione porta #18, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 107, Array Id: 0, Name: Stato connessione porta #18, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 108, Array Id: 0, Name: Ultima modifica porta #18, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\nField Id: 109, Array Id: 0, Name: Nome porta #19, Severity: -255, Value: Bridge Port 6, Description: Valore ricevuto=-255\r\nField Id: 110, Array Id: 0, Name: Tipo porta #19, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 111, Array Id: 0, Name: Velocità porta #19, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 112, Array Id: 0, Name: Stato attivazione porta #19, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 113, Array Id: 0, Name: Stato connessione porta #19, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 114, Array Id: 0, Name: Ultima modifica porta #19, Severity: -255, Value: 0 giorni, 00:00:00.000, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data ASMi 52L

		[Test]
		public void DumpCompleto_definitionRADASMi52L_0_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174465848");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMI52L SA HW Version: 0.00, SW Version: 1.00b01 Milano Porta Garibaldi, Type: RADASMI52L, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI52L_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-52L SHDSL modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMI52L SA HW Version: 0.00, SW Version: 1.00b01, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 44 giorni, 02:22:08.090, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.121, Description: ASMi-52L SHDSL modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.102.35.56, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;9600 bit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;2 Mbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 6, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 9600, Description: 9600 bit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 44 giorni, 02:22:08.280, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 44 giorni, 02:22:08.380, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: Ethernet Port 1, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 44 giorni, 02:22:08.490, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: Ethernet Port 2, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 44 giorni, 02:22:08.590, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: Ethernet Port 3, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 44 giorni, 02:22:08.690, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: Ethernet Port 4, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 44 giorni, 02:22:08.780, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Nome porta #07, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 38, Array Id: 0, Name: Tipo porta #07, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 39, Array Id: 0, Name: Velocità porta #07, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 40, Array Id: 0, Name: Stato attivazione porta #07, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 41, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 42, Array Id: 0, Name: Ultima modifica porta #07, Severity: -255, Value: 44 giorni, 02:22:08.880, Description: Valore ricevuto=-255\r\nField Id: 43, Array Id: 0, Name: Nome porta #08, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 44, Array Id: 0, Name: Tipo porta #08, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 45, Array Id: 0, Name: Velocità porta #08, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 46, Array Id: 0, Name: Stato attivazione porta #08, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 47, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 48, Array Id: 0, Name: Ultima modifica porta #08, Severity: -255, Value: 44 giorni, 02:22:08.970, Description: Valore ricevuto=-255\r\nField Id: 49, Array Id: 0, Name: Nome porta #09, Severity: -255, Value: Ethernet Port 1, Description: Valore ricevuto=-255\r\nField Id: 50, Array Id: 0, Name: Tipo porta #09, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 51, Array Id: 0, Name: Velocità porta #09, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 52, Array Id: 0, Name: Stato attivazione porta #09, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 53, Array Id: 0, Name: Stato connessione porta #09, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 54, Array Id: 0, Name: Ultima modifica porta #09, Severity: -255, Value: 44 giorni, 02:22:09.060, Description: Valore ricevuto=-255\r\nField Id: 55, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: Ethernet Port 2, Description: Valore ricevuto=-255\r\nField Id: 56, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 57, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 58, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 59, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 60, Array Id: 0, Name: Ultima modifica porta #10, Severity: -255, Value: 44 giorni, 02:22:09.160, Description: Valore ricevuto=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #11, Severity: -255, Value: Ethernet Port 3, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #11, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #11, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #11, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #11, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 66, Array Id: 0, Name: Ultima modifica porta #11, Severity: -255, Value: 44 giorni, 02:22:09.250, Description: Valore ricevuto=-255\r\nField Id: 67, Array Id: 0, Name: Nome porta #12, Severity: -255, Value: Ethernet Port 4, Description: Valore ricevuto=-255\r\nField Id: 68, Array Id: 0, Name: Tipo porta #12, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 69, Array Id: 0, Name: Velocità porta #12, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 70, Array Id: 0, Name: Stato attivazione porta #12, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 71, Array Id: 0, Name: Stato connessione porta #12, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 72, Array Id: 0, Name: Ultima modifica porta #12, Severity: -255, Value: 44 giorni, 02:22:09.350, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADASMi52L_1_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174494844");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMI52L SA HW Version: 0.00, SW Version: 1.04 Torino Porta Nuova, Type: RADASMI52L, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI52L_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-52L SHDSL modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMI52L SA HW Version: 0.00, SW Version: 1.04, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 40 giorni, 01:15:31.010, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.121, Description: ASMi-52L SHDSL modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.102.148.124, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;9600 bit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;768 Kbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;768 Kbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;768 Kbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;768 Kbit/s=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 6, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 9600, Description: 9600 bit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 40 giorni, 01:15:31.390, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 768000, Description: 768 Kbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 40 giorni, 01:15:31.600, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: Ethernet Port 1, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 40 giorni, 01:15:31.730, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: Ethernet Port 2, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 768000, Description: 768 Kbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 40 giorni, 01:15:31.880, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: Ethernet Port 3, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 768000, Description: 768 Kbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 40 giorni, 01:15:32.020, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: Ethernet Port 4, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 768000, Description: 768 Kbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 40 giorni, 01:15:32.230, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data ASMi 54

		[Test]
		public void DumpCompleto_definitionRADASMi54_0_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174465106");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMi-54 HW Version: 0.00, SW Version: 1.00 Ancona, Type: RADASMI54, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI54_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-54 Multiport SHDSL.bis Modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: ASMi-54, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMi-54 HW Version: 0.00, SW Version: 1.00, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 25 giorni, 12:07:51.850, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.140, Description: ASMi-54 Multiport SHDSL.bis Modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Abilitata=-255;Abilitata=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione agente hardware, Severity: -255, Value: Put your string here, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.32.201, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 3, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.102.32.82, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;SHDSL=-255;5696 Kb/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;SHDSL=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Ethernet=-255;5696 Kb/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;teLink=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;teLink=-255;5696 Kb/s=-255;Attiva=-255;Connessa=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 13, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: ETH Port 1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: ETH Port 2, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 8, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 9, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 10, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 11, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: ETH Port 3, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 13, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 14, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 15, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 16, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: ETH Port 4, Description: Valore ricevuto=-255\r\nField Id: 17, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 18, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 19, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 20, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 21, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: PME Port 1, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 23, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 5696000, Description: 5696 Kb/s=-255\r\nField Id: 24, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 25, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 26, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: PME Port 2, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 28, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 29, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 30, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #07, Severity: -255, Value: PCS Port 1, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #07, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #07, Severity: -255, Value: 5696000, Description: 5696 Kb/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #07, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Nome porta #08, Severity: -255, Value: Bridge Port 1, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Tipo porta #08, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 38, Array Id: 0, Name: Velocità porta #08, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 39, Array Id: 0, Name: Stato attivazione porta #08, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 40, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 41, Array Id: 0, Name: Nome porta #09, Severity: -255, Value: Bridge Port 2, Description: Valore ricevuto=-255\r\nField Id: 42, Array Id: 0, Name: Tipo porta #09, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 43, Array Id: 0, Name: Velocità porta #09, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 44, Array Id: 0, Name: Stato attivazione porta #09, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 45, Array Id: 0, Name: Stato connessione porta #09, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 46, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: Bridge Port 3, Description: Valore ricevuto=-255\r\nField Id: 47, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 48, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 49, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 50, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 51, Array Id: 0, Name: Nome porta #11, Severity: -255, Value: Bridge Port 4, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Tipo porta #11, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 53, Array Id: 0, Name: Velocità porta #11, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 54, Array Id: 0, Name: Stato attivazione porta #11, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 55, Array Id: 0, Name: Stato connessione porta #11, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 56, Array Id: 0, Name: Nome porta #12, Severity: -255, Value: Bridge Port 5, Description: Valore ricevuto=-255\r\nField Id: 57, Array Id: 0, Name: Tipo porta #12, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 58, Array Id: 0, Name: Velocità porta #12, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 59, Array Id: 0, Name: Stato attivazione porta #12, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 60, Array Id: 0, Name: Stato connessione porta #12, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 61, Array Id: 0, Name: Nome porta #13, Severity: -255, Value: Bridge Port 6, Description: Valore ricevuto=-255\r\nField Id: 62, Array Id: 0, Name: Tipo porta #13, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 63, Array Id: 0, Name: Velocità porta #13, Severity: -255, Value: 5696000, Description: 5696 Kb/s=-255\r\nField Id: 64, Array Id: 0, Name: Stato attivazione porta #13, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 65, Array Id: 0, Name: Stato connessione porta #13, Severity: -255, Value: 1, Description: Connessa=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data ASMi 54L

		[Test]
		public void DumpCompleto_definitionRADASMi54L_1_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174519466");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMi-54L HW Version: 0.00, SW Version: 2.50 Omignano Salento, Type: RADASMI54L, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI54L_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-54L SHDSL.bis Modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Rack IaP Omignano-> Badge Omignano, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMi-54L HW Version: 0.00, SW Version: 2.50, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 40 giorni, 05:05:11.520, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.160, Description: ASMi-54L SHDSL.bis Modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Abilitata=-255;Abilitata=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Versione agente hardware, Severity: -255, Value: Put your string here, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Gateway scheda di rete, Severity: -255, Value: 10.102.245.201, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Interfaccia Web, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 3, Array Id: 0, Name: Interfaccia Telnet, Severity: -255, Value: 3, Description: Abilitata=-255\r\nField Id: 4, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.102.244.170, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.254.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Ethernet=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;Ethernet=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;SHDSL=-255;5696 Kb/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Ethernet=-255;5696 Kb/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;teLink=-255;100 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;teLink=-255;Non disponibile=-255;Attiva=-255;Non connessa=-255;Valore ricevuto=-255;teLink=-255;5696 Kb/s=-255;Attiva=-255;Connessa=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 12, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: ETH Port 1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: ETH Port 2, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 8, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 9, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 10, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 11, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: ETH Port 3, Description: Valore ricevuto=-255\r\nField Id: 12, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 13, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 14, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 15, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 16, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: ETH Port 4, Description: Valore ricevuto=-255\r\nField Id: 17, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 18, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 19, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 20, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 21, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: SHDSL Port 1, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 23, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 5696000, Description: 5696 Kb/s=-255\r\nField Id: 24, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 25, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 26, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: PCS Port 1, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 6, Description: Ethernet=-255\r\nField Id: 28, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 5696000, Description: 5696 Kb/s=-255\r\nField Id: 29, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 30, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #07, Severity: -255, Value: Bridge Port 1, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #07, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #07, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #07, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #07, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Nome porta #08, Severity: -255, Value: Bridge Port 2, Description: Valore ricevuto=-255\r\nField Id: 37, Array Id: 0, Name: Tipo porta #08, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 38, Array Id: 0, Name: Velocità porta #08, Severity: -255, Value: 100000000, Description: 100 Mbit/s=-255\r\nField Id: 39, Array Id: 0, Name: Stato attivazione porta #08, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 40, Array Id: 0, Name: Stato connessione porta #08, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 41, Array Id: 0, Name: Nome porta #09, Severity: -255, Value: Bridge Port 3, Description: Valore ricevuto=-255\r\nField Id: 42, Array Id: 0, Name: Tipo porta #09, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 43, Array Id: 0, Name: Velocità porta #09, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 44, Array Id: 0, Name: Stato attivazione porta #09, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 45, Array Id: 0, Name: Stato connessione porta #09, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 46, Array Id: 0, Name: Nome porta #10, Severity: -255, Value: Bridge Port 4, Description: Valore ricevuto=-255\r\nField Id: 47, Array Id: 0, Name: Tipo porta #10, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 48, Array Id: 0, Name: Velocità porta #10, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 49, Array Id: 0, Name: Stato attivazione porta #10, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 50, Array Id: 0, Name: Stato connessione porta #10, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 51, Array Id: 0, Name: Nome porta #11, Severity: -255, Value: Bridge Port 5, Description: Valore ricevuto=-255\r\nField Id: 52, Array Id: 0, Name: Tipo porta #11, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 53, Array Id: 0, Name: Velocità porta #11, Severity: -255, Value: 0, Description: Non disponibile=-255\r\nField Id: 54, Array Id: 0, Name: Stato attivazione porta #11, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 55, Array Id: 0, Name: Stato connessione porta #11, Severity: -255, Value: 2, Description: Non connessa=-255\r\nField Id: 56, Array Id: 0, Name: Nome porta #12, Severity: -255, Value: Bridge Port 6, Description: Valore ricevuto=-255\r\nField Id: 57, Array Id: 0, Name: Tipo porta #12, Severity: -255, Value: 200, Description: teLink=-255\r\nField Id: 58, Array Id: 0, Name: Velocità porta #12, Severity: -255, Value: 5696000, Description: 5696 Kb/s=-255\r\nField Id: 59, Array Id: 0, Name: Stato attivazione porta #12, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 60, Array Id: 0, Name: Stato connessione porta #12, Severity: -255, Value: 1, Description: Connessa=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion

		#region Test RAD Data ASMi 52

		[Test]
		public void DumpCompleto_definitionRADASMi52SA_0_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("179155150");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMI 52 SA HW Version: 1.00, SW Version: 2.84, Type: RADASMI52SA, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI52SA_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-52 SA SHDSL modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Master VS La, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMI 52 SA HW Version: 1.00, SW Version: 2.84, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 18:20:41.020, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.99, Description: ASMi-52 SA SHDSL modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.173.176.206, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;9600 bit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;4 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 9600, Description: 9600 bit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 18:20:41.150, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 0 giorni, 18:20:41.220, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: Fast Ethernet Port, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 0 giorni, 18:20:41.290, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 4096000, Description: 4 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 0 giorni, 18:20:41.360, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 0 giorni, 18:20:41.420, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: Fast Ethernet Port, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 0 giorni, 18:20:41.490, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADASMi52SA_1_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("179155169");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMI 52 SA  HW Version: 1.00, SW Version: 2.52E01, Type: RADASMI52SA, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI52SA_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-52 SA SHDSL modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Link vs Pala, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMI 52 SA  HW Version: 1.00, SW Version: 2.52E01, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 41 giorni, 15:47:41.590, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.99, Description: ASMi-52 SA SHDSL modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.173.176.225, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;9600 bit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;4 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 9600, Description: 9600 bit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 41 giorni, 15:47:41.920, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 41 giorni, 15:47:42.060, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: Fast Ethernet Port, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 41 giorni, 15:47:42.220, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 4096000, Description: 4 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 41 giorni, 15:47:42.360, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 41 giorni, 15:47:42.530, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: Fast Ethernet Port, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 41 giorni, 15:47:42.660, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADASMi52SA_2_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("179155197");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMI 52 SA HW Version: 1.00, SW Version: 2.04, Type: RADASMI52SA, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI52SA_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-52 SA SHDSL modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: -255, Value: Master Vs TLC3 Bz, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMI 52 SA HW Version: 1.00, SW Version: 2.04, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 45 giorni, 20:57:01.760, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.99, Description: ASMi-52 SA SHDSL modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;9600 bit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;4 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: Slip interface, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 9600, Description: 9600 bit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 45 giorni, 20:57:01.860, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: SHDSL interface, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 45 giorni, 20:57:01.930, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: Ethernet IEEE802.3, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 45 giorni, 20:57:02.000, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: Slip interface, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 4096000, Description: 4 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 45 giorni, 20:57:02.060, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: SHDSL interface, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 2048000, Description: 2 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 45 giorni, 20:57:02.130, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: Ethernet IEEE802.3, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 45 giorni, 20:57:02.190, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADASMi52SA_3_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("179154976");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMI 52 SA  HW Version: 1.00, SW Version: 2.03000, Type: RADASMI52SA, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI52SA_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-52 SA SHDSL modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMI 52 SA  HW Version: 1.00, SW Version: 2.03000, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 0 giorni, 10:17:56.180, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.99, Description: ASMi-52 SA SHDSL modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: 255, Description: \r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;9600 bit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;1,98 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;1,98 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;1,98 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: Slip interface, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 9600, Description: 9600 bit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 0 giorni, 10:17:56.340, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: SHDSL interface, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 1984000, Description: 1,98 Mbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 0 giorni, 10:17:56.450, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: Ethernet IEEE802.3, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 0 giorni, 10:17:56.580, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: Slip interface, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 1984000, Description: 1,98 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 0 giorni, 10:17:56.750, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: SHDSL interface, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 1984000, Description: 1,98 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 0 giorni, 10:17:56.830, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: Ethernet IEEE802.3, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 0 giorni, 10:17:56.920, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		[Test]
		public void DumpCompleto_definitionRADASMi52SA_4_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("179155170");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is false");

					Assert.AreEqual(0, device.Offline);
					Assert.AreEqual(Severity.Ok, device.SeverityLevel);

					Assert.AreEqual(
						"Device Name: ASMI 52 SA HW Version: 1.00, SW Version: 2.54, Type: RADASMI52SA, Community: public, SNMP Version: V1, Offline: 0, Severity: 0, ReplyICMP: True, ReplySNMP: True, Device Category: -3, Device with dynamic loading rules enabled\r\nDynamic Definition File: RADASMI52SA_000.xml\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Descrizione ricevuta=-255;Up time ricevuto=0;ASMi-52 SA SHDSL modem=-255\r\nField Id: 0, Array Id: 0, Name: Nome dispositivo, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Descrizione, Severity: -255, Value: ASMI 52 SA HW Version: 1.00, SW Version: 2.54, Description: Descrizione ricevuta=-255\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 21 giorni, 07:53:35.520, Description: Up time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Modello apparato RAD, Severity: -255, Value: .1.3.6.1.4.1.164.6.1.3.99, Description: ASMi-52 SA SHDSL modem=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Indirizzo IP scheda di rete, Severity: -255, Value: 10.173.176.226, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Maschera di sottorete scheda di rete, Severity: -255, Value: 255.255.255.0, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Informazioni porte, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;9600 bit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2,25 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Altro=-255;2,25 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;SHDSL=-255;2,25 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255;Valore ricevuto=-255;Fast Ethernet=-255;10 Mbit/s=-255;Attiva=-255;Connessa=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Numero interfacce di rete, Severity: -255, Value: 3, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Nome porta #01, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Tipo porta #01, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 3, Array Id: 0, Name: Velocità porta #01, Severity: -255, Value: 9600, Description: 9600 bit/s=-255\r\nField Id: 4, Array Id: 0, Name: Stato attivazione porta #01, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 5, Array Id: 0, Name: Stato connessione porta #01, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 6, Array Id: 0, Name: Ultima modifica porta #01, Severity: -255, Value: 21 giorni, 07:53:35.840, Description: Valore ricevuto=-255\r\nField Id: 7, Array Id: 0, Name: Nome porta #02, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 8, Array Id: 0, Name: Tipo porta #02, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 9, Array Id: 0, Name: Velocità porta #02, Severity: -255, Value: 2304000, Description: 2,25 Mbit/s=-255\r\nField Id: 10, Array Id: 0, Name: Stato attivazione porta #02, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 11, Array Id: 0, Name: Stato connessione porta #02, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 12, Array Id: 0, Name: Ultima modifica porta #02, Severity: -255, Value: 21 giorni, 07:53:36.000, Description: Valore ricevuto=-255\r\nField Id: 13, Array Id: 0, Name: Nome porta #03, Severity: -255, Value: Fast Ethernet Port, Description: Valore ricevuto=-255\r\nField Id: 14, Array Id: 0, Name: Tipo porta #03, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 15, Array Id: 0, Name: Velocità porta #03, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 16, Array Id: 0, Name: Stato attivazione porta #03, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 17, Array Id: 0, Name: Stato connessione porta #03, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 18, Array Id: 0, Name: Ultima modifica porta #03, Severity: -255, Value: 21 giorni, 07:53:36.130, Description: Valore ricevuto=-255\r\nField Id: 19, Array Id: 0, Name: Nome porta #04, Severity: -255, Value: RS-232, Description: Valore ricevuto=-255\r\nField Id: 20, Array Id: 0, Name: Tipo porta #04, Severity: -255, Value: 1, Description: Altro=-255\r\nField Id: 21, Array Id: 0, Name: Velocità porta #04, Severity: -255, Value: 2304000, Description: 2,25 Mbit/s=-255\r\nField Id: 22, Array Id: 0, Name: Stato attivazione porta #04, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 23, Array Id: 0, Name: Stato connessione porta #04, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 24, Array Id: 0, Name: Ultima modifica porta #04, Severity: -255, Value: 21 giorni, 07:53:36.240, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Nome porta #05, Severity: -255, Value: SHDSL link, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Tipo porta #05, Severity: -255, Value: 169, Description: SHDSL=-255\r\nField Id: 27, Array Id: 0, Name: Velocità porta #05, Severity: -255, Value: 2304000, Description: 2,25 Mbit/s=-255\r\nField Id: 28, Array Id: 0, Name: Stato attivazione porta #05, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 29, Array Id: 0, Name: Stato connessione porta #05, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 30, Array Id: 0, Name: Ultima modifica porta #05, Severity: -255, Value: 21 giorni, 07:53:36.350, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Nome porta #06, Severity: -255, Value: Fast Ethernet Port, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Tipo porta #06, Severity: -255, Value: 62, Description: Fast Ethernet=-255\r\nField Id: 33, Array Id: 0, Name: Velocità porta #06, Severity: -255, Value: 10000000, Description: 10 Mbit/s=-255\r\nField Id: 34, Array Id: 0, Name: Stato attivazione porta #06, Severity: -255, Value: 1, Description: Attiva=-255\r\nField Id: 35, Array Id: 0, Name: Stato connessione porta #06, Severity: -255, Value: 1, Description: Connessa=-255\r\nField Id: 36, Array Id: 0, Name: Ultima modifica porta #06, Severity: -255, Value: 21 giorni, 07:53:36.480, Description: Valore ricevuto=-255\r\n",
						deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion
	}
}
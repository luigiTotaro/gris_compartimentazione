﻿using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsGeneral
    {
        #region Test device type e inizializzazione

        [Test]
        public void LoadDefinition_validMonitor_NoError()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511107");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject del monitor AEL0000");
            }
        }

        [Test]
        public void LoadDefinition_invalidMonitorType_Error()
        {
            DeviceObject deviceObject = new DeviceObject(true, 1, "Monitor", "ZZZ000", DataUtility.DecodeIPAddress("174511412"), 1, "N/D", 1, "N/D", 1,
														 "N/D", 1, "N/D", "N/D", "00000", new SystemPort(1, "SNMP Manager Locale", "TCP Client", true), 1, 1, 1, 1, 1,
                                                         new TopographyLocation(1, "N/D", 1, "N/D", "N/D", 1, "N/D", "N/D", "N/D"), "public", 161, null);

            DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();

	        IDevice device = null;

	        try
	        {
		        device = deviceTypeFactory.Create(deviceObject);
			}
	        catch (DeviceConfigurationHelperException ex)
	        {
				Assert.IsNotNull(ex);
	        }

			Assert.IsNull(device);
		}

        [Test]
        public void Populate_valid_NoError()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511107");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    Assert.IsNotNull(device.StreamData);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Test]
        public void PopulateStream_valid_NoError()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("174511107");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    Assert.IsNotNull(device.StreamData);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion
    }
}
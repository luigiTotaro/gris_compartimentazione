﻿using System;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using NUnit.Framework;

namespace GrisSuite.SnmpSupervisor.nUnit.Tests
{
    [TestFixture]
    public class LibraryIntegrationTestsWorkstation
    {
        #region Test Workstation

        [Test]
        public void DumpCompleto_definitionChiamateWorkstationSimulate_DeviceTypeWRKWIN10()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("2851995649");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    if (Thread.CurrentThread.CurrentCulture.Name.Equals("it-IT", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: Workstation simulata 1, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi, Severity: 2, Description: Valore ricevuto=0;Spazio libero disco sotto il valore di attenzione=2;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1,91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 2, Value: 9 %, Description: Spazio libero disco sotto il valore di attenzione=2\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20,00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9,51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670.986.221 / Received: 237.993.236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else if (Thread.CurrentThread.CurrentCulture.Name.Equals("en-US", StringComparison.OrdinalIgnoreCase))
                    {
                        Assert.AreEqual(
                            "Device Name: Workstation simulata 1, Type: WRKWIN10, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=0;Descrizione ricevuta=0;Up Time ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: 0, Value: RFITRMGRIW01PRO, Description: Nome macchina ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema, Severity: 0, Value: Hardware: x86 Family 6 Model 15 Stepping 11 AT/AT COMPATIBLE - Software: Windows Version 5.2 (Build 3790 Multiprocessor Free), Description: Descrizione ricevuta=0\r\nField Id: 2, Array Id: 0, Name: Up time, Severity: 0, Value: 81 giorni, 18:29:38.110, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nStream Id: 2, Name: Stato dischi, Severity: 2, Description: Valore ricevuto=0;Spazio libero disco sotto il valore di attenzione=2;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Spazio libero disco rigido C, Severity: 0, Value: 1.91 GB, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Percentuale spazio libero disco rigido C, Severity: 2, Value: 9 %, Description: Spazio libero disco sotto il valore di attenzione=2\r\nField Id: 2, Array Id: 0, Name: Spazio totale disco rigido C, Severity: 0, Value: 20.00 GB, Description: Valore ricevuto=0\r\nStream Id: 3, Name: Stato processi di sistema, Severity: 0, Description: Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: Memoria allocata da SQL Server, Severity: 255, Value: n/d, Description: Valore non ricevuto\r\nField Id: 1, Array Id: 0, Name: Memoria allocata da Windows Explorer, Severity: 0, Value: 9.51 MB, Description: Valore ricevuto=0\r\nStream Id: 4, Name: Stato schede di rete, Severity: 0, Description: Valore ricevuto=0;Valore ricevuto=0;Valore ricevuto=0\r\nField Id: 0, Array Id: 0, Name: MAC Address prima scheda di rete 100 Megabit, Severity: 0, Value: 00-1E-0B-71-43-BC, Description: Valore ricevuto=0\r\nField Id: 1, Array Id: 0, Name: Statistiche prima scheda di rete 100 Megabit, Severity: 0, Value: Sent: 670,986,221 / Received: 237,993,236, Description: Valore ricevuto=0\r\nField Id: 2, Array Id: 0, Name: Indirizzo IP prima scheda di rete 100 Megabit, Severity: 0, Value: 192.168.0.80, Description: Valore ricevuto=0\r\n",
                            deviceDump);
                    }
                    else
                    {
                        Assert.Fail("Cultura corrente del server di test non supportata. Modificare i test");
                    }
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        [Ignore("Test da usare per chiamate SNMP reali verso agente locale - punta a 127.0.0.1")]
        [Test]
        public void DumpCompleto_definitionChiamateWorkstationRealeLocale_DeviceTypeWRKWIN10()
        {
            DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("2130706433");

            if (deviceObject != null)
            {
                DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
                IDevice device = deviceTypeFactory.Create(deviceObject);
                Assert.IsInstanceOf(typeof (SnmpDeviceBase), device);

                if (device.LastError != null)
                {
                    if (device.LastError is DeviceConfigurationHelperException)
                    {
                        Assert.Fail(device.LastError.Message);
                    }
                }
                else
                {
                    device.Populate();

                    if (device.LastError != null)
                    {
                        if (device.LastError is DeviceConfigurationHelperException)
                        {
                            Assert.Fail(device.LastError.Message);
                        }
                    }
                }

                if (device.LastError == null)
                {
                    string deviceDump = device.Dump();

                    Assert.IsNotNull(deviceDump);

                    Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
                    Assert.IsTrue(((SnmpDeviceBase) device).IsAliveSnmp, "IsAliveSnmp is true");

                    Assert.AreEqual(0, device.Offline);
                }
            }
            else
            {
                Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
            }
        }

        #endregion

		#region Test Posto Periferico Ansaldo

		[Test]
		public void DumpCompleto_PostoPeriferico_00_Ok()
		{
			DeviceObject deviceObject = FakeDatabaseUtility.GetMonitorDeviceByAddress("168430185");

			if (deviceObject != null)
			{
				DeviceTypeFactory deviceTypeFactory = new DeviceTypeFactory();
				IDevice device = deviceTypeFactory.Create(deviceObject);
				Assert.IsInstanceOf(typeof(SnmpDeviceBase), device);

				if (device.LastError != null)
				{
					if (device.LastError is DeviceConfigurationHelperException)
					{
						Assert.Fail(device.LastError.Message);
					}
				}
				else
				{
					device.Populate();

					if (device.LastError != null)
					{
						if (device.LastError is DeviceConfigurationHelperException)
						{
							Assert.Fail(device.LastError.Message);
						}
					}
				}

				if (device.LastError == null)
				{
					string deviceDump = device.Dump();

					Assert.IsNotNull(deviceDump);

					Assert.IsTrue(device.IsAliveIcmp, "IsAliveIcmp is false");
					Assert.IsTrue(((SnmpDeviceBase)device).IsAliveSnmp, "IsAliveSnmp is true");

					Assert.AreEqual(0, device.Offline);
                    Assert.AreEqual(Severity.Error, device.SeverityLevel);

                    Assert.AreEqual(
                        "Device Name: Ansaldo Posto Periferico 01, Type: ANPP000, Community: public, SNMP Version: V2, Offline: 0, Severity: 2, ReplyICMP: True, ReplySNMP: True\r\nStream Id: 1, Name: Informazioni standard, Severity: 0, Description: Nome macchina ricevuto=-255;Microsoft Windows 7=-255;Up Time ricevuto=0;Contatto di riferimento ricevuto=-255;Locazione ricevuta=-255\r\nField Id: 0, Array Id: 0, Name: Nome macchina, Severity: -255, Value: WIN7PROX64, Description: Nome macchina ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Descrizione sistema / sistema operativo, Severity: -255, Value: Hardware: Intel64 Family 6 Model 30 Stepping 5 AT/AT COMPATIBLE - Software: Windows Version 6.1 (Build 7601 Multiprocessor Free), Description: Microsoft Windows 7=-255\r\nField Id: 2, Array Id: 0, Name: SNMP Up time, Severity: 0, Value: 0 giorni, 04:56:00.390, Description: Up Time ricevuto=0\r\nField Id: 3, Array Id: 0, Name: Contatto di riferimento, Severity: -255, Value: Ansaldo Posto Periferico, Description: Contatto di riferimento ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Locazione, Severity: -255, Value: Ansaldo, Description: Locazione ricevuta=-255\r\nStream Id: 2, Name: Informazioni sistema, Severity: -255, Description: Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255;Valore ricevuto=-255\r\nField Id: 0, Array Id: 0, Name: Dominio / Workgroup, Severity: -255, Value: WORKGROUP, Description: Valore ricevuto=-255\r\nField Id: 1, Array Id: 0, Name: Indirizzo IP scheda di rete #01, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 2, Array Id: 0, Name: Maschera di sottorete scheda di rete #01, Severity: -255, Value: 255.0.0.0, Description: Valore ricevuto=-255\r\nField Id: 3, Array Id: 0, Name: Indirizzo IP scheda di rete #02, Severity: -255, Value: 169.254.12.129, Description: Valore ricevuto=-255\r\nField Id: 4, Array Id: 0, Name: Maschera di sottorete scheda di rete #02, Severity: -255, Value: 255.255.0.0, Description: Valore ricevuto=-255\r\nField Id: 5, Array Id: 0, Name: Indirizzo IP scheda di rete #03, Severity: -255, Value: 169.254.56.121, Description: Valore ricevuto=-255\r\nField Id: 6, Array Id: 0, Name: Maschera di sottorete scheda di rete #03, Severity: -255, Value: 255.255.0.0, Description: Valore ricevuto=-255\r\nField Id: 21, Array Id: 0, Name: Gateway scheda di rete #01, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 22, Array Id: 0, Name: Gateway scheda di rete #02, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 23, Array Id: 0, Name: Gateway scheda di rete #03, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 24, Array Id: 0, Name: Gateway scheda di rete #04, Severity: -255, Value: 169.254.12.129, Description: Valore ricevuto=-255\r\nField Id: 25, Array Id: 0, Name: Gateway scheda di rete #05, Severity: -255, Value: 169.254.12.129, Description: Valore ricevuto=-255\r\nField Id: 26, Array Id: 0, Name: Gateway scheda di rete #06, Severity: -255, Value: 169.254.56.121, Description: Valore ricevuto=-255\r\nField Id: 27, Array Id: 0, Name: Gateway scheda di rete #07, Severity: -255, Value: 169.254.12.129, Description: Valore ricevuto=-255\r\nField Id: 28, Array Id: 0, Name: Gateway scheda di rete #08, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 29, Array Id: 0, Name: Gateway scheda di rete #09, Severity: -255, Value: 127.0.0.1, Description: Valore ricevuto=-255\r\nField Id: 31, Array Id: 0, Name: Versione maggiore sistema operativo, Severity: -255, Value: 6, Description: Valore ricevuto=-255\r\nField Id: 32, Array Id: 0, Name: Versione minore sistema operativo, Severity: -255, Value: 1, Description: Valore ricevuto=-255\r\nField Id: 33, Array Id: 0, Name: System Uptime, Severity: -255, Value: 0 giorni, 05:22:25.840, Description: Valore ricevuto=-255\r\nStream Id: 3, Name: Unità logiche disco, Severity: 1, Description: Spazio disco occupato oltre valore di attenzione=1;Spazio disco normale=0\r\nField Id: 2, Array Id: 0, Name: Disco C - Spazio occupato, Severity: 1, Value: 87%, Description: Spazio disco occupato oltre valore di attenzione=1\r\nField Id: 5, Array Id: 0, Name: Disco F - Spazio occupato, Severity: 0, Value: 17%, Description: Spazio disco normale=0\r\nStream Id: 4, Name: Servizi, Severity: 2, Description: Non in esecuzione=1;Non in esecuzione=1;Non in esecuzione=1;Non in esecuzione=2;Non in esecuzione=1;Non in esecuzione=2;Non in esecuzione=2;Non in esecuzione=2;Non in esecuzione=2;Non in esecuzione=1;Non in esecuzione=1\r\nField Id: 0, Array Id: 0, Name: Stato processo STLCStatoApparatiAgentService, Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 1, Array Id: 0, Name: Stato processo cygrunsrv, Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 2, Array Id: 0, Name: Stato processo rsync, Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 3, Array Id: 0, Name: Stato processo SnmpSupervisorService, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 4, Array Id: 0, Name: Stato processo SPVServerD, Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 5, Array Id: 0, Name: Stato processo ComandoDispositiviAudio, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 6, Array Id: 0, Name: Stato processo Supvis_GesMS_Iec, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 7, Array Id: 0, Name: Stato processo tomcat7, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 8, Array Id: 0, Name: Stato processo STLCManagerService, Severity: 2, Value: 0, Description: Non in esecuzione=2\r\nField Id: 9, Array Id: 0, Name: Stato processo WpfSaraNotifServiceHost, Severity: 1, Value: 0, Description: Non in esecuzione=1\r\nField Id: 10, Array Id: 0, Name: Stato processo FileZilla Server, Severity: 1, Value: 0, Description: Non in esecuzione=1\r\n",
                        deviceDump);
				}
			}
			else
			{
				Assert.Fail("Impossibile recuperare il DeviceObject della periferica");
			}
		}

		#endregion
	}
}
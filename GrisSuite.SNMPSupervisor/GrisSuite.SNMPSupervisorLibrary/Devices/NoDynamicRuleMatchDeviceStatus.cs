﻿using GrisSuite.SnmpSupervisorLibrary.Database;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Incapsula i dati da usare nel caso una periferica non risponda alle regole di caricamento dinamico
    /// </summary>
    public class NoDynamicRuleMatchDeviceStatus
    {
        /// <summary>
        ///   Indica la severità forzata della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico
        /// </summary>
        public Severity NoMatchDeviceStatusSeverity { get; private set; }

        /// <summary>
        ///   Indica la descrizione dello stato forzata della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico
        /// </summary>
        public string NoMatchDeviceStatusDescription { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "noMatchDeviceStatusSeverity">Indica la severità forzata della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico</param>
        /// <param name = "noMatchDeviceStatusDescription">Indica la descrizione dello stato forzata della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico</param>
        public NoDynamicRuleMatchDeviceStatus(Severity noMatchDeviceStatusSeverity, string noMatchDeviceStatusDescription)
        {
            this.NoMatchDeviceStatusSeverity = noMatchDeviceStatusSeverity;
            this.NoMatchDeviceStatusDescription = noMatchDeviceStatusDescription;
        }
    }
}
﻿namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
	public class DeviceTypeFactory
	{
		/// <summary>
		///     Ritorna una istanza di periferica, in base al tipo indicato
		/// </summary>
		/// <param name="deviceObject">Oggetto di stato con le informazioni relative al device</param>
		/// <returns></returns>
		public IDevice Create(DeviceObject deviceObject)
		{
			IDevice device = null;

			DeviceConfigurationHelperParameters deviceConfigurationHelperParameters =
				DeviceConfigurationHelperParameters.PreloadConfigurationParameters(deviceObject.DeviceType, deviceObject.DeviceId);

			switch (deviceConfigurationHelperParameters.DefinitionType)
			{
				case DeviceKind.Icmp:
					device = new IcmpDeviceBase(deviceObject.FakeLocalData, deviceObject.Name, deviceObject.DeviceType,
					                            deviceObject.EndPoint, deviceObject.LastEvent, deviceConfigurationHelperParameters,
					                            deviceObject);
					break;
				case DeviceKind.Snmp:
					device = new SnmpDeviceBase(deviceObject.FakeLocalData, deviceObject.Name, deviceObject.DeviceType,
					                            deviceObject.EndPoint, deviceObject.LastEvent, deviceObject.SnmpCommunity,
					                            deviceConfigurationHelperParameters, deviceObject);
					break;
				//case DeviceKind.Telnet:
				//    break;
			}

			return device;
		}
	}
}
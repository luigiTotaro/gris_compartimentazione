﻿namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
	/// <summary>
	///     Rappresenta il genere della periferica
	/// </summary>
	public enum DeviceKind
	{
		/// <summary>
		///     Periferica ICMP (Ping)
		/// </summary>
		Icmp,

		/// <summary>
		///     Periferica SNMP
		/// </summary>
		Snmp//,

		/// <summary>
		///     Periferica Telnet
		/// </summary>
//		Telnet
	}
}
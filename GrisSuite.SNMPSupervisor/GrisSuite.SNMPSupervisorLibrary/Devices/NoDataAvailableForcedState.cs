﻿using GrisSuite.SnmpSupervisorLibrary.Database;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
	/// <summary>
	///     Indica le informazioni per la valorizzazione dello StreamField quando non sono restituiti dati nelle query
	/// </summary>
	public class NoDataAvailableForcedState
	{
		/// <summary>
		///     Severità forzata per lo StreamField
		/// </summary>
		public Severity ForcedSeverity { get; private set; }

		/// <summary>
		///     Descrizione del valore forzato per lo StreamField
		/// </summary>
		public string ForcedValueDescription { get; private set; }

		/// <summary>
		///     Valore forzato per lo StreamField
		/// </summary>
		public string ForcedValue { get; private set; }

		/// <summary>
		///     Indica se inviare notifica per email nel caso che non ci sia risposta per questo Streamfield
		/// </summary>
		public bool ShouldSendNotificationByEmailIfMatch { get; private set; }

		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="forcedSeverity">Severità forzata per lo StreamField</param>
		/// <param name="valueDescription">Descrizione del valore forzato per lo StreamField</param>
		/// <param name="value">Valore forzato per lo StreamField</param>
		/// <param name="shouldSendNotificationByEmail">Indica se inviare notifica per email nel caso che non ci sia risposta per questo Streamfield</param>
		public NoDataAvailableForcedState(Severity forcedSeverity,
		                                  string valueDescription,
		                                  string value,
		                                  bool shouldSendNotificationByEmail)
		{
			this.ForcedSeverity = forcedSeverity;
			this.ForcedValueDescription = valueDescription;
			this.ForcedValue = value;
			this.ShouldSendNotificationByEmailIfMatch = shouldSendNotificationByEmail;
		}
	}
}
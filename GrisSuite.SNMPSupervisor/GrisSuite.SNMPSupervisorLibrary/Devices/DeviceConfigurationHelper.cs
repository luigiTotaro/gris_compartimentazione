﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security;
using System.Text.RegularExpressions;
using System.Xml;
using GrisSuite.SnmpSupervisorLibrary.Database;
using Lextm.SharpSnmpLib;
using System.Diagnostics;
namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///     Classe helper per il caricamento delle definizioni
    /// </summary>
    public class DeviceConfigurationHelper
    {
        #region Costanti
        // indica quale è il padre dello streamfield corrette
        /// <summary>
        ///  automaticamente lo streamfield corrente diventa figlio
        /// </summary>
        private const String ATTR_STREAM_FIELD_FATHER = "Father";
        // indica se lo stream field è padre di altri stream field
        // IL PADRE(NELLA DEFINIZIONE) DEVE SEMPRE VERIRE PRIMA DEI FIGLI
        private const String ATTR_STREAM_FIELD_IS_FATHER = "isFather";
        // indica che quello stream field è un arrat
        private const String ATTR_STREAM_FIELD_IS_ARRAY = "isArray";
        // specifica una dimensione massima dell'array
        private const String ATTR_STREAM_FIELD_MAX_SIZE = "MaxSize";
        // da usare solo con streamfield figli che hanno come padre un arrray: indica che lo stream field corrente non viene inglobato nella nella description del padre
        // quindi avraà una posizione assestante nell'array del db
        private const String ATTR_STREAM_FIELD_AGGREGATION = "Aggregation";
        // indica che nel campo Value verrà mostrato "NomeStreamField: valore"
        private const String ATTR_STREAM_FIELD_SHOW_NAME = "ShowNameAndValue";
        // indica che al posto del valore verrà mostrato il ValueDescription
        private const String ATTR_STREAM_FIELD_SHOW_VALUE_DESCRIPTION = "ShowValueDescription";
        // indica che lo stream field definisce un array di streamfield
        private const String ATTR_STREAM_FIELD_IS_STREAM_FIELD_ARRAY = "isStreamFieldArray";
        // indica che il dato prelevado è di tipo TIME TICKS con codifica in ms(e non in centosecondi)
        private const String ATTR_STREAM_FIELD_IS_TIME_TICKS_MS = "IsTimeTicksMs";
        // indica che il padre dello streamfieldarray deve essere usato come testa di un gruppo di elementi
        private const String ATTR_STREAM_FIELD_USE_TO_HEAD_OF_STREAM_FIELD_ARRAY = "UseToHeadOfStreamFieldArray";
        // indica che verrà usata un espressione per visualizzare il nome array + id row
        private const String ATTR_STREAM_FIELD_SHOW_ROW_ID = "ShowExpressionRowId";
        // indica che il conteggio legato a ShowExpressionRowId parte da 0
        private const String ATTR_STREAM_FIELD_SHOW_ROW_ID_ZERO_BASE = "StreamFieldArrayIdZeroBased";
        // codice c# per la valutazione di un valore scalare
        private const String ATTR_STREAM_FIELD_EVALUATION_FORMULA = "EvaluationFormula";

        // indica che non bisogna verificare se possiede la mib2
        private const String ATTR_DEVICE_CHECK_IF_HAS_MIB2 = "CheckIfHasMib2";
        // indica quale oid bisogna usare per verificare il corretto funzionamento dell'snmp
        private const String ATTR_DEVICE_SNMP_ALIVE_OID = "SnmpAliveOid";
        #endregion
        
        #region Proprietà

        /// <summary>
        ///     Definizione della periferica
        /// </summary>
        public string DefinitionFileName { get; private set; }

        /// <summary>
        ///     Versione del protocollo Snmp per le richieste
        /// </summary>
        public VersionCode SnmpVersionCode { get; private set; }

        /// <summary>
        ///     Dati di severità e descrizione periferica, nel caso la periferica sia offline
        /// </summary>
        public ForcedSeverityAndDescriptionWhenOffline ForcedSeverityAndDescriptionWhenDeviceOffline { get; private set; }

        /// <summary>
        ///     Indica se la definizione è configurata per effettuare il caricamento dinamico di altre definizioni
        /// </summary>
        public bool HasDynamicLoadingRules { get; private set; }

        /// <summary>
        ///     Indica se bisogna verificare se la periferica possiede MIB2
        /// </summary>
        public bool CheckIfHasMib2 { get; private set; }

        /// <summary>
        ///     Intero che indica la categoria della periferica (dipende da un enumerato indicato nella MIB, se presente). Il
        ///     valore -1 indica nessuna category specificata
        /// </summary>
        public int DeviceCategory { get; private set; }

        /// <summary>
        ///     Indica se caricare i dati da file di risposte locali per simulazione
        /// </summary>
        public bool FakeLocalData { get; private set; }

        /// <summary>
        ///     Community string per l'accesso Snmp in lettura
        /// </summary>
        public string SnmpCommunity { get; private set; }

        /// <summary>
        ///     Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di
        ///     caricamento dinamico
        /// </summary>
        public NoDynamicRuleMatchDeviceStatus NoMatchDeviceStatus { get; private set; }

        /// <summary>
        ///     Nome di periferica forzato, che può subentrare, se configurato, nel caso di definizioni a caricamento dinamico
        /// </summary>
        public string ForcedDeviceType { get; private set; }

        /// <summary>
        ///     Oid opzionale che indica come recuperare via SNMP il numero di serie della periferica
        /// </summary>
        public ObjectIdentifier SerialNumberOid { get; private set; }

        /// <summary>
        ///     Oid opzionale che indica come verificare le il servizio SNMP è vivo 
        /// </summary>
        public ObjectIdentifier SnmpAliveOid { get; private set; }

        /// <summary>
        ///     Classe helper che contiene i parametri base delle definizioni
        /// </summary>
        public DeviceConfigurationHelperParameters DeviceConfigurationParameters { get; private set; }

        /// <summary>
        ///     Codice C# per il calcolo dinamico della severità
        /// </summary>
        public string DynamicCodeCalculateSeverity { get; private set; }

        /// <summary>
        ///     Codice C# per il calcolo dinamico del messaggio di stato
        /// </summary>
        public string DynamicCodeCalculateStatusMessage { get; private set; }

        #endregion

        #region Variabili private

        private readonly IPEndPoint ipEndPoint;
        private readonly MessengerFactory messengerFactory;
        private readonly long deviceId;

        #endregion

        #region Costruttore

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name="deviceId">Device Id della periferica</param>
        /// <param name="messengerFactory">Classe messenger per l'esecuzione delle query SNMP</param>
        /// <param name="ipEndPoint">Indirizzo IP e porta della periferica</param>
        /// <param name="snmpCommunity">Community string per l'accesso Snmp in lettura</param>
        /// <param name="deviceConfigurationHelperParameters">Classe helper che contiene i parametri base delle definizioni</param>
        public DeviceConfigurationHelper(bool fakeLocalData,
            long deviceId,
            MessengerFactory messengerFactory,
            IPEndPoint ipEndPoint,
            string snmpCommunity,
            DeviceConfigurationHelperParameters deviceConfigurationHelperParameters)
        {
            if (deviceConfigurationHelperParameters == null)
            {
                throw new ArgumentException("La classe DeviceConfigurationHelperParameters è obbligatoria.", "deviceConfigurationHelperParameters");
            }

            if (deviceId <= 0)
            {
                throw new ArgumentException("Il DeviceId è obbligatorio.", "deviceId");
            }

            if (messengerFactory == null)
            {
                throw new ArgumentException("La classe Messenger è obbligatoria.", "messengerFactory");
            }

            if ((ipEndPoint == null) || (ipEndPoint.Address == null))
            {
                throw new ArgumentException("L'indirizzo IP e porta della periferica sono obbligatori.", "ipEndPoint");
            }

            this.DefinitionFileName = deviceConfigurationHelperParameters.DefinitionFileName;
            this.messengerFactory = messengerFactory;
            this.ipEndPoint = ipEndPoint;
            this.DeviceCategory = (int) SpecialDeviceCategory.Undefined; // non inizializzato
            this.FakeLocalData = fakeLocalData;
            this.deviceId = deviceId;
            this.SnmpCommunity = snmpCommunity;
            this.NoMatchDeviceStatus = null;
            this.ForcedDeviceType = null;
            this.SerialNumberOid = null;
            this.DeviceConfigurationParameters = deviceConfigurationHelperParameters;
            this.ForcedSeverityAndDescriptionWhenDeviceOffline = null;
            this.DynamicCodeCalculateSeverity = null;
            this.DynamicCodeCalculateStatusMessage = null;
            this.CheckIfHasMib2 = true;
            this.SnmpAliveOid = null;
        }

        #endregion

        #region Precaricamento definizione, per poter configurare il caricamento dinamico delle sotto-definizioni, se presente

        /// <summary>
        ///     Esegue un parsing preliminare della definizione, per poter configurare il caricamento dinamico delle
        ///     sotto-definizioni, se presente
        /// </summary>
        public void PreloadConfiguration()
        {
            XmlNode root = this.GetDefinitionXmlRootElement();

            if (root != null)
            {
                #region SNMPVersionCode

                object snmpVersionCode = null;

                if (root.Attributes != null)
                {
                    if (root.Attributes["SNMPVersionCode"] != null)
                    {
                        try
                        {
                            string versionCodeString = root.Attributes["SNMPVersionCode"].Value.Trim();
                            snmpVersionCode = Enum.Parse(typeof (VersionCode), versionCodeString, true);
                        }
                        catch (ArgumentException)
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                "L'attributo 'SNMPVersionCode' del nodo 'Device' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
                                this.DefinitionFileName, root.Attributes["SNMPVersionCode"].Value.Trim()));
                        }
                    }
                }

                if (snmpVersionCode == null)
                {
                    // Per default le query Snmp usano la versione 2
                    this.SnmpVersionCode = VersionCode.V2;
                }
                else
                {
                    this.SnmpVersionCode = (VersionCode) snmpVersionCode;
                }

                #endregion

                #region ForcedSeverityAndDescriptionWhenOffline

                if (root.Attributes != null)
                {
                    if (root.Attributes["ForcedSeverityAndDescriptionWhenOffline"] != null)
                    {
                        this.ForcedSeverityAndDescriptionWhenDeviceOffline = new ForcedSeverityAndDescriptionWhenOffline(this.DefinitionFileName,
                            root.Attributes["ForcedSeverityAndDescriptionWhenOffline"].Value);
                    }
                }

                #endregion

                #region CheckHasMib2

                bool tmpCheckIfHasMib2 = true;
                if (root.Attributes != null)
                {
                    if (root.Attributes[ATTR_DEVICE_CHECK_IF_HAS_MIB2] != null)
                    {
                        string checkIfHasMib2_string =
                            root.Attributes[ATTR_DEVICE_CHECK_IF_HAS_MIB2].Value.Trim().ToLowerInvariant();
                        if (!bool.TryParse(checkIfHasMib2_string, out tmpCheckIfHasMib2))
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                "L'attributo '{0}' del nodo 'Device' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                ATTR_DEVICE_CHECK_IF_HAS_MIB2, this.DefinitionFileName, root.Attributes[ATTR_DEVICE_CHECK_IF_HAS_MIB2].Value.Trim()));
                        }
                    }
                }

                this.CheckIfHasMib2 = tmpCheckIfHasMib2;
                #endregion

                #region SnmpAliveOid

                ObjectIdentifier SnmpAliveOidLocal = null;

                if (root.Attributes != null)
                {
                    if (root.Attributes[ATTR_DEVICE_SNMP_ALIVE_OID] != null)
                    {
                        string SnmpAliveOidString = root.Attributes[ATTR_DEVICE_SNMP_ALIVE_OID].Value.Trim();

                        if (!string.IsNullOrEmpty(SnmpAliveOidString))
                        {
                            SnmpAliveOidLocal = SnmpUtility.TryParseOid(SnmpAliveOidString);

                            if (SnmpAliveOidLocal == null)
                            {
                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                    "L'attributo '{0}' del nodo 'Device' nel file {1}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{2}'",
                                    ATTR_DEVICE_SNMP_ALIVE_OID, this.DefinitionFileName, root.Attributes[ATTR_DEVICE_SNMP_ALIVE_OID].Value));
                            }
                        }
                        else
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                "L'attributo '{0}' del nodo 'Device' nel file {1}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{2}'",
                                ATTR_DEVICE_SNMP_ALIVE_OID, this.DefinitionFileName, root.Attributes[ATTR_DEVICE_SNMP_ALIVE_OID].Value));
                        }
                    }
                }

                this.SnmpAliveOid = SnmpAliveOidLocal;

                #endregion

                #region HasDynamicLoadingRules

                bool hasDynamicLoadingRules = false;

                if (root.Attributes != null)
                {
                    if (root.Attributes["HasDynamicLoadingRules"] != null)
                    {
                        string hasDynamicLoadingRulesString = root.Attributes["HasDynamicLoadingRules"].Value.Trim().ToLowerInvariant();
                        if (!Boolean.TryParse(hasDynamicLoadingRulesString, out hasDynamicLoadingRules))
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                "L'attributo 'HasDynamicLoadingRules' del nodo 'Device' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
                                this.DefinitionFileName, root.Attributes["HasDynamicLoadingRules"].Value.Trim()));
                        }
                    }
                }

                this.HasDynamicLoadingRules = hasDynamicLoadingRules;

                #endregion

                if (this.HasDynamicLoadingRules)
                {
                    bool rulesFound = false;
                    List<LoadingRule> loadingRules = new List<LoadingRule>();
                    LoadingRuleExactMatch loadingRuleExactMatch = null;
                    LoadingRuleOjectId loadingRuleOjectId = null;
                    LoadingRuleQueryEvaluate loadingRuleQueryEvaluate = null;

                    #region Caricamento del nodo base LoadingRules

                    foreach (XmlNode rootElement in root.ChildNodes)
                    {
                        if (rootElement.Name.Equals("loadingrules", StringComparison.OrdinalIgnoreCase))
                        {
                            foreach (XmlNode rule in rootElement.ChildNodes)
                            {
                                if (rule.Name.Equals("rule", StringComparison.OrdinalIgnoreCase))
                                {
                                    #region Regola di caricamento di tipo Rule

                                    if (rule.Attributes != null)
                                    {
                                        if ((rule.Attributes["ProbingCategoryOid"] == null) ||
                                            (rule.Attributes["ProbingCategoryOid"].Value.Trim().Length == 0) ||
                                            (rule.Attributes["Manufacturer"] == null) || (rule.Attributes["Manufacturer"].Value.Trim().Length == 0) ||
                                            (rule.Attributes["DefinitionFile"] == null) ||
                                            (rule.Attributes["DefinitionFile"].Value.Trim().Length == 0))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "Impossibile trovare l'attributo 'ProbingCategoryOid', 'Manufacturer' o 'DefinitionFile' sul nodo 'Rule', oppure Oid per il recupero della categoria, nome del produttore o nome del file della definizione nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                this.DefinitionFileName, rule.OuterXml));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Impossibile trovare l'attributo 'ProbingCategoryOid', 'Manufacturer' o 'DefinitionFile' sul nodo 'Rule', oppure Oid per il recupero della categoria, nome del produttore o nome del file della definizione nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                            this.DefinitionFileName, rule.OuterXml));
                                    }

                                    #region ProbingCategoryOid

                                    ObjectIdentifier probingCategoryOid;

                                    if (rule.Attributes["ProbingCategoryOid"] != null)
                                    {
                                        string probingCategoryOidString = rule.Attributes["ProbingCategoryOid"].Value.Trim();

                                        if (!string.IsNullOrEmpty(probingCategoryOidString))
                                        {
                                            probingCategoryOid = SnmpUtility.TryParseOid(probingCategoryOidString);

                                            if (probingCategoryOid == null)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'ProbingCategoryOid' del nodo 'Rule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, rule.Attributes["ProbingCategoryOid"].Value));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "L'attributo 'ProbingCategoryOid' del nodo 'Rule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                this.DefinitionFileName, rule.Attributes["ProbingCategoryOid"].Value));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Il nodo 'Rule' nel file {0}, non contiene l'attributo obbligatorio 'ProbingManufacturerOid'",
                                            this.DefinitionFileName));
                                    }

                                    #endregion

                                    loadingRules.Add(new LoadingRule(probingCategoryOid, rule.Attributes["Manufacturer"].Value.Trim(),
                                        rule.Attributes["DefinitionFile"].Value.Trim()));

                                    rulesFound = true;

                                    #endregion
                                }
                                else if (rule.Name.Equals("exactmatchrule", StringComparison.OrdinalIgnoreCase))
                                {
                                    #region Regola di caricamento di tipo ExactMatchRule

                                    if (rule.Attributes != null)
                                    {
                                        if ((rule.Attributes["ProbingCategoryOid"] == null) ||
                                            (rule.Attributes["ProbingCategoryOid"].Value.Trim().Length == 0) ||
                                            (rule.Attributes["ProbingManufacturerOid"] == null) ||
                                            (rule.Attributes["ProbingManufacturerOid"].Value.Trim().Length == 0))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "Impossibile trovare l'attributo 'ProbingCategoryOid' o 'ProbingManufacturerOid' sul nodo 'ExactMatchRule', oppure Oid per il recupero della categoria od Oid per il recupero del produttore della periferica nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                this.DefinitionFileName, rule.OuterXml));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Impossibile trovare l'attributo 'ProbingCategoryOid' o 'ProbingManufacturerOid' sul nodo 'ExactMatchRule', oppure Oid per il recupero della categoria od Oid per il recupero del produttore della periferica nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                            this.DefinitionFileName, rule.OuterXml));
                                    }

                                    bool manufacturerDefinitionFound = false;

                                    List<ManufacturerDefinition> manufacturerDefinitionList = new List<ManufacturerDefinition>();

                                    #region Caricamento sotto nodi ManufacturerDefinition

                                    foreach (XmlNode manufacturerDefinition in rule.ChildNodes)
                                    {
                                        if (manufacturerDefinition.Name.Equals("manufacturerdefinition", StringComparison.OrdinalIgnoreCase))
                                        {
                                            if (manufacturerDefinition.Attributes != null)
                                            {
                                                if ((manufacturerDefinition.Attributes["Description"] == null) ||
                                                    (manufacturerDefinition.Attributes["Description"].Value.Trim().Length == 0) ||
                                                    (manufacturerDefinition.Attributes["DefinitionFile"] == null) ||
                                                    (manufacturerDefinition.Attributes["DefinitionFile"].Value.Trim().Length == 0))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile trovare l'attributo 'Description' o 'DefinitionFile' sul nodo 'ManufacturerDefinition', oppure descrizione identificativa del produttore o nome del file della definizione nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                        this.DefinitionFileName, manufacturerDefinition.OuterXml));
                                                }
                                            }

                                            #region DescriptionRegex

                                            Regex descriptionRegex = null;

                                            if (manufacturerDefinition.Attributes != null)
                                            {
                                                if (manufacturerDefinition.Attributes["Description"] != null)
                                                {
                                                    try
                                                    {
                                                        descriptionRegex = new Regex(manufacturerDefinition.Attributes["Description"].Value,
                                                            RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                                                    }
                                                    catch (ArgumentNullException)
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'Description' del nodo 'ManufacturerDefinition' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, manufacturerDefinition.Attributes["Description"].Value));
                                                    }
                                                    catch (ArgumentException)
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'Description' del nodo 'ManufacturerDefinition' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, manufacturerDefinition.Attributes["Description"].Value));
                                                    }
                                                }

                                                #endregion

                                                manufacturerDefinitionList.Add(new ManufacturerDefinition(descriptionRegex,
                                                    manufacturerDefinition.Attributes["DefinitionFile"].Value.Trim()));
                                            }

                                            manufacturerDefinitionFound = true;
                                        }
                                    }

                                    #endregion

                                    if (!manufacturerDefinitionFound)
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno un nodo 'ManufacturerDefinition' all'interno del nodo 'ExactMatchRule', necessario per il caricamento dinamico delle definizioni in base ai produttori",
                                            this.DefinitionFileName));
                                    }

                                    #region ProbingCategoryOid

                                    ObjectIdentifier probingCategoryOid;

                                    if (rule.Attributes["ProbingCategoryOid"] != null)
                                    {
                                        string probingCategoryOidString = rule.Attributes["ProbingCategoryOid"].Value.Trim();

                                        if (!string.IsNullOrEmpty(probingCategoryOidString))
                                        {
                                            probingCategoryOid = SnmpUtility.TryParseOid(probingCategoryOidString);

                                            if (probingCategoryOid == null)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'ProbingCategoryOid' del nodo 'ExactMatchRule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, rule.Attributes["ProbingCategoryOid"].Value));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "L'attributo 'ProbingCategoryOid' del nodo 'ExactMatchRule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                this.DefinitionFileName, rule.Attributes["ProbingCategoryOid"].Value));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Il nodo 'ExactMatchRule' nel file {0}, non contiene l'attributo obbligatorio 'ProbingCategoryOid'",
                                            this.DefinitionFileName));
                                    }

                                    #endregion

                                    #region ProbingManufacturerOid

                                    ObjectIdentifier probingManufacturerOid;

                                    if (rule.Attributes["ProbingManufacturerOid"] != null)
                                    {
                                        string probingManufacturerOidString = rule.Attributes["ProbingManufacturerOid"].Value.Trim();

                                        if (!string.IsNullOrEmpty(probingManufacturerOidString))
                                        {
                                            probingManufacturerOid = SnmpUtility.TryParseOid(probingManufacturerOidString);

                                            if (probingManufacturerOid == null)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'ProbingManufacturerOid' del nodo 'ExactMatchRule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, rule.Attributes["ProbingManufacturerOid"].Value));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "L'attributo 'ProbingManufacturerOid' del nodo 'ExactMatchRule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                this.DefinitionFileName, rule.Attributes["ProbingManufacturerOid"].Value));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Il nodo 'ExactMatchRule' nel file {0}, non contiene l'attributo obbligatorio 'ProbingManufacturerOid'",
                                            this.DefinitionFileName));
                                    }

                                    #endregion

                                    #region NoMatchDeviceStatusSeverityAndDescription

                                    NoDynamicRuleMatchDeviceStatus noRuleMatchDeviceStatus = null;

                                    if (rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim()))
                                        {
                                            string noMatchDeviceStatusSeverityAndDescription =
                                                rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim();

                                            string[] values = noMatchDeviceStatusSeverityAndDescription.Split(';');

                                            if (values.Length == 2)
                                            {
                                                object severity;

                                                try
                                                {
                                                    severity = Enum.Parse(typeof (Severity), values[0].Trim(), true);
                                                }
                                                catch (ArgumentException)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown). Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, values[0].Trim()));
                                                }

                                                string severityDescription = values[1].Trim();

                                                if (string.IsNullOrEmpty(severityDescription))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene una Descrizione per lo stato periferica valida. Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, severityDescription));
                                                }

                                                if (severity is Severity)
                                                {
                                                    noRuleMatchDeviceStatus = new NoDynamicRuleMatchDeviceStatus((Severity) severity,
                                                        severityDescription);
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown)",
                                                        this.DefinitionFileName));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non è nel formato 'SeveritàPeriferica;Descrizione stato periferica' (dove 'SeveritàPeriferica' può assumere un valore tra: Ok, Warning, Error e Unknown e 'Descrizione stato periferica' non deve contenere punti e virgole ';')",
                                                    this.DefinitionFileName));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' è nullo e non valido",
                                                this.DefinitionFileName));
                                        }
                                    }

                                    #endregion

                                    loadingRuleExactMatch = new LoadingRuleExactMatch(probingCategoryOid, probingManufacturerOid,
                                        manufacturerDefinitionList.AsReadOnly(), noRuleMatchDeviceStatus);

                                    rulesFound = true;

                                    #endregion
                                }
                                else if (rule.Name.Equals("objectidrule", StringComparison.OrdinalIgnoreCase))
                                {
                                    #region Regola di caricamento di tipo ObjectIDRule

                                    if (rule.Attributes != null)
                                    {
                                        if ((rule.Attributes["ProbingObjectIDOid"] == null) ||
                                            (rule.Attributes["ProbingObjectIDOid"].Value.Trim().Length == 0))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "Impossibile trovare l'attributo 'ProbingObjectIDOid' sul nodo 'ObjectIDRule', oppure Oid per il recupero dell'ID Oggetto della periferica nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                this.DefinitionFileName, rule.OuterXml));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Impossibile trovare l'attributo 'ProbingObjectIDOid' sul nodo 'ObjectIDRule', oppure Oid per il recupero dell'ID Oggetto della periferica nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                            this.DefinitionFileName, rule.OuterXml));
                                    }

                                    bool objectIdDefinitionFound = false;

                                    List<ObjectIdDefinition> objectIDDefinitionList = new List<ObjectIdDefinition>();

                                    #region Caricamento sotto nodi ObjectIDDefinition

                                    foreach (XmlNode objectIDDefinition in rule.ChildNodes)
                                    {
                                        if (objectIDDefinition.Name.Equals("objectiddefinition", StringComparison.OrdinalIgnoreCase))
                                        {
                                            if (objectIDDefinition.Attributes != null)
                                            {
                                                if ((objectIDDefinition.Attributes["Description"] == null) ||
                                                    (objectIDDefinition.Attributes["Description"].Value.Trim().Length == 0) ||
                                                    (objectIDDefinition.Attributes["DefinitionFile"] == null) ||
                                                    (objectIDDefinition.Attributes["DefinitionFile"].Value.Trim().Length == 0))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile trovare l'attributo 'Description' o 'DefinitionFile' sul nodo 'ObjectIDDefinition', oppure descrizione dell'ID Oggetto della periferica o nome del file della definizione nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                        this.DefinitionFileName, objectIDDefinition.OuterXml));
                                                }
                                            }

                                            if (objectIDDefinition.Attributes != null)
                                            {
                                                #region DescriptionRegex

                                                Regex descriptionRegex = null;

                                                if (objectIDDefinition.Attributes["Description"] != null)
                                                {
                                                    try
                                                    {
                                                        descriptionRegex = new Regex(objectIDDefinition.Attributes["Description"].Value,
                                                            RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                                                    }
                                                    catch (ArgumentNullException)
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'Description' del nodo 'ObjectIDDefinition' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, objectIDDefinition.Attributes["Description"].Value));
                                                    }
                                                    catch (ArgumentException)
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'Description' del nodo 'ObjectIDDefinition' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, objectIDDefinition.Attributes["Description"].Value));
                                                    }
                                                }

                                                #endregion

                                                #region ForcedDeviceType

                                                string forcedDeviceType = null;

                                                if (objectIDDefinition.Attributes["ForcedDeviceType"] != null)
                                                {
                                                    string forcedDeviceTypeString = objectIDDefinition.Attributes["ForcedDeviceType"].Value.Trim();

                                                    if (!string.IsNullOrEmpty(forcedDeviceTypeString))
                                                    {
                                                        forcedDeviceType = forcedDeviceTypeString;
                                                    }
                                                }

                                                #endregion

                                                objectIDDefinitionList.Add(new ObjectIdDefinition(descriptionRegex,
                                                    objectIDDefinition.Attributes["DefinitionFile"].Value.Trim(), forcedDeviceType));
                                            }

                                            objectIdDefinitionFound = true;
                                        }
                                    }

                                    #endregion

                                    if (!objectIdDefinitionFound)
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno un nodo 'ObjectIDDefinition' all'interno del nodo 'ObjectIDRule', necessario per il caricamento dinamico delle definizioni in base agli ID Oggetti",
                                            this.DefinitionFileName));
                                    }

                                    #region ProbingObjectIdOid

                                    ObjectIdentifier probingObjectIdOid;

                                    if (rule.Attributes["ProbingObjectIDOid"] != null)
                                    {
                                        string probingObjectIdOidString = rule.Attributes["ProbingObjectIDOid"].Value.Trim();

                                        if (!string.IsNullOrEmpty(probingObjectIdOidString))
                                        {
                                            probingObjectIdOid = SnmpUtility.TryParseOid(probingObjectIdOidString);

                                            if (probingObjectIdOid == null)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'ProbingObjectIdOid' del nodo 'ObjectIDRule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, rule.Attributes["ProbingObjectIdOid"].Value));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "L'attributo 'ProbingObjectIdOid' del nodo 'ObjectIDRule' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                this.DefinitionFileName, rule.Attributes["ProbingObjectIdOid"].Value));
                                        }
                                    }
                                    else
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Il nodo 'ObjectIDRule' nel file {0}, non contiene l'attributo obbligatorio 'ProbingObjectIDOid'",
                                            this.DefinitionFileName));
                                    }

                                    #endregion

                                    #region NoMatchDeviceStatusSeverityAndDescription

                                    NoDynamicRuleMatchDeviceStatus noRuleMatchDeviceStatus = null;

                                    if (rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim()))
                                        {
                                            string noMatchDeviceStatusSeverityAndDescription =
                                                rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim();

                                            string[] values = noMatchDeviceStatusSeverityAndDescription.Split(';');

                                            if (values.Length == 2)
                                            {
                                                object severity;

                                                try
                                                {
                                                    severity = Enum.Parse(typeof (Severity), values[0].Trim(), true);
                                                }
                                                catch (ArgumentException)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown). Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, values[0].Trim()));
                                                }

                                                string severityDescription = values[1].Trim();

                                                if (string.IsNullOrEmpty(severityDescription))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene una Descrizione per lo stato periferica valida. Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, severityDescription));
                                                }

                                                if (severity is Severity)
                                                {
                                                    noRuleMatchDeviceStatus = new NoDynamicRuleMatchDeviceStatus((Severity) severity,
                                                        severityDescription);
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown)",
                                                        this.DefinitionFileName));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non è nel formato 'SeveritàPeriferica;Descrizione stato periferica' (dove 'SeveritàPeriferica' può assumere un valore tra: Ok, Warning, Error e Unknown e 'Descrizione stato periferica' non deve contenere punti e virgole ';')",
                                                    this.DefinitionFileName));
                                            }
                                        }
                                        else
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' è nullo e non valido",
                                                this.DefinitionFileName));
                                        }
                                    }

                                    #endregion

                                    loadingRuleOjectId = new LoadingRuleOjectId(probingObjectIdOid, objectIDDefinitionList.AsReadOnly(),
                                        noRuleMatchDeviceStatus);

                                    rulesFound = true;

                                    #endregion
                                }
                                else if (rule.Name.Equals("queryevaluaterule", StringComparison.OrdinalIgnoreCase))
                                {
                                    #region Regola di caricamento di tipo QueryEvaluateRule

                                    bool queryEvaluateRuleDefinitionFound = false;

                                    List<QueryEvaluateRuleDefinition> queryEvaluateRuleDefinitionList = new List<QueryEvaluateRuleDefinition>();

                                    #region Caricamento sotto nodi QueryEvaluateRuleDefinition

                                    foreach (XmlNode queryEvaluateRuleDefinition in rule.ChildNodes)
                                    {
                                        if (queryEvaluateRuleDefinition.Name.Equals("queryevaluateruledefinition", StringComparison.OrdinalIgnoreCase))
                                        {
                                            if (queryEvaluateRuleDefinition.Attributes != null)
                                            {
                                                if ((queryEvaluateRuleDefinition.Attributes["QueryOidAndOperator"] == null) ||
                                                    (queryEvaluateRuleDefinition.Attributes["QueryOidAndOperator"].Value.Trim().Length == 0) ||
                                                    (queryEvaluateRuleDefinition.Attributes["DefinitionFile"] == null) ||
                                                    (queryEvaluateRuleDefinition.Attributes["DefinitionFile"].Value.Trim().Length == 0))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile trovare l'attributo 'QueryOidAndOperator' o 'DefinitionFile' sul nodo 'QueryEvaluateRuleDefinition', oppure query SNMP / operatore nulli o nome del file della definizione nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                        this.DefinitionFileName, queryEvaluateRuleDefinition.OuterXml));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Impossibile trovare l'attributo 'QueryOidAndOperator' o 'DefinitionFile' sul nodo 'QueryEvaluateRuleDefinition', oppure query SNMP / operatore nulli o nome del file della definizione nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                    this.DefinitionFileName, queryEvaluateRuleDefinition.OuterXml));
                                            }

                                            #region QueryOidAndOperator

                                            string queryOidAndOperator = null;

                                            if (queryEvaluateRuleDefinition.Attributes["QueryOidAndOperator"] != null)
                                            {
                                                string queryOidAndOperatorString =
                                                    queryEvaluateRuleDefinition.Attributes["QueryOidAndOperator"].Value.Trim();

                                                if (!string.IsNullOrEmpty(queryOidAndOperatorString))
                                                {
                                                    queryOidAndOperator = queryOidAndOperatorString;
                                                }
                                            }

                                            #endregion

                                            #region ForcedDeviceType

                                            string forcedDeviceType = null;

                                            if (queryEvaluateRuleDefinition.Attributes["ForcedDeviceType"] != null)
                                            {
                                                string forcedDeviceTypeString =
                                                    queryEvaluateRuleDefinition.Attributes["ForcedDeviceType"].Value.Trim();

                                                if (!string.IsNullOrEmpty(forcedDeviceTypeString))
                                                {
                                                    forcedDeviceType = forcedDeviceTypeString;
                                                }
                                            }

                                            #endregion

                                            QueryEvaluateRuleDefinition newQueryEvaluateRuleDefinition;

                                            try
                                            {
                                                newQueryEvaluateRuleDefinition = new QueryEvaluateRuleDefinition(queryOidAndOperator,
                                                    queryEvaluateRuleDefinition.Attributes["DefinitionFile"].Value.Trim(), forcedDeviceType);

                                                if (!newQueryEvaluateRuleDefinition.IsValid)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Errore durante la lettura della regola 'QueryEvaluateRuleDefinition' del nodo 'QueryEvaluateRule' nel file {0}. Regola non valida.",
                                                        this.DefinitionFileName));
                                                }
                                            }
                                            catch (ArgumentException ex)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Errore durante la lettura della regola 'QueryEvaluateRuleDefinition' del nodo 'QueryEvaluateRule' nel file {0}. {1}",
                                                    this.DefinitionFileName, ex.Message));
                                            }

                                            queryEvaluateRuleDefinitionList.Add(newQueryEvaluateRuleDefinition);

                                            queryEvaluateRuleDefinitionFound = true;
                                        }
                                    }

                                    #endregion

                                    if (!queryEvaluateRuleDefinitionFound)
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno un nodo 'QueryEvaluateRuleDefinition' all'interno del nodo 'QueryEvaluateRule', necessario per il caricamento dinamico delle definizioni in base alla valutazione delle risposte a singole query SNMP",
                                            this.DefinitionFileName));
                                    }

                                    #region NoMatchDeviceStatusSeverityAndDescription

                                    NoDynamicRuleMatchDeviceStatus noRuleMatchDeviceStatus = null;

                                    if (rule.Attributes != null)
                                    {
                                        if (rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"] != null)
                                        {
                                            if (!string.IsNullOrEmpty(rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim()))
                                            {
                                                string noMatchDeviceStatusSeverityAndDescription =
                                                    rule.Attributes["NoMatchDeviceStatusSeverityAndDescription"].Value.Trim();

                                                string[] values = noMatchDeviceStatusSeverityAndDescription.Split(';');

                                                if (values.Length == 2)
                                                {
                                                    object severity;

                                                    try
                                                    {
                                                        severity = Enum.Parse(typeof (Severity), values[0].Trim(), true);
                                                    }
                                                    catch (ArgumentException)
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown). Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, values[0].Trim()));
                                                    }

                                                    string severityDescription = values[1].Trim();

                                                    if (string.IsNullOrEmpty(severityDescription))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene una Descrizione per lo stato periferica valida. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, severityDescription));
                                                    }

                                                    if (severity is Severity)
                                                    {
                                                        noRuleMatchDeviceStatus = new NoDynamicRuleMatchDeviceStatus((Severity) severity,
                                                            severityDescription);
                                                    }
                                                    else
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown)",
                                                            this.DefinitionFileName));
                                                    }
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' non è nel formato 'SeveritàPeriferica;Descrizione stato periferica' (dove 'SeveritàPeriferica' può assumere un valore tra: Ok, Warning, Error e Unknown e 'Descrizione stato periferica' non deve contenere punti e virgole ';')",
                                                        this.DefinitionFileName));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'NoMatchDeviceStatusSeverityAndDescription' all'interno del nodo 'ExactMatchRule' è nullo e non valido",
                                                    this.DefinitionFileName));
                                            }
                                        }
                                    }

                                    #endregion

                                    loadingRuleQueryEvaluate = new LoadingRuleQueryEvaluate(queryEvaluateRuleDefinitionList.AsReadOnly(),
                                        noRuleMatchDeviceStatus);

                                    rulesFound = true;

                                    #endregion
                                }
                            }

                            break;
                        }
                    }

                    #endregion

                    if (!rulesFound)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare il nodo 'LoadingRules' al primo livello, in una definizione che è configurata per il caricamento dinamico",
                            this.DefinitionFileName));
                    }

                    int deviceCategory;

                    if (loadingRules.Count > 0)
                    {
                        #region Gestione regola per produttore, con Enterprise unica (per Infostazioni)

                        // Nel caso siano indicate sia regole per produttore che generica con Enterprise unica, prevale quella per produttore
                        foreach (LoadingRule rule in loadingRules)
                        {
                            // Nel caso siano indicate le regole di caricamento per produttore, si esegue ogni query per recuperare la categoria
                            // Alla prima query che ottiene una risposta valida (categoria con valore maggiore o uguale a zero),
                            // sappiamo di avere trovato una periferica che appartiene a quel produttore, perché siamo riusciti ad entrare in una
                            // Enterprise privata, quindi possiamo usare la definizione dinamica configurata, impostare la categoria e uscire

                            deviceCategory = this.GetCategoryFromDevice(rule.ProbingCategoryOid, false);

                            if (deviceCategory >= 0)
                            {
                                this.DeviceCategory = deviceCategory;
                                this.DeviceConfigurationParameters =
                                    DeviceConfigurationHelperParameters.PreloadConfigurationParameters(rule.DefinitionFile, this.deviceId);
                                this.DefinitionFileName = this.DeviceConfigurationParameters.DefinitionFileName;
                                break;
                            }
                        }

                        #endregion
                    }
                    else if ((loadingRuleExactMatch != null) && (loadingRuleExactMatch.ManufacturerDefinitions.Count > 0))
                    {
                        #region Gestione regole di match via nome prodottore e categoria dispositivo (per Infostazioni)

                        // Nel caso sia indicata la regola unica, che si basa su una Enterprise comune a tutti i produttori
                        // recupera dalla periferica la categoria ed il nome del produttore
                        // Dato il nome del produttore cerca tra quelli configurati, per poter estrarre la
                        // relativa definizione. Nel caso che trovi il produttore e la relativa definizione, reimposta
                        // la definizione corrente e la categoria della periferica ed esce

                        deviceCategory = this.GetCategoryFromDevice(loadingRuleExactMatch.ProbingCategoryOid, true);
                        this.DeviceCategory = deviceCategory;

                        string deviceManufaturer = this.GetManufacturerFromDevice(loadingRuleExactMatch.ProbingManufacturerOid);

                        if (string.IsNullOrEmpty(deviceManufaturer))
                        {
                            // Nel caso che il produttore non sia recuperabile, resettiamo anche la categoria
                            // I due valori sono strettamente legati per la parte dinamica
                            deviceCategory = (int) SpecialDeviceCategory.NotAvailable;
                            this.DeviceCategory = (int) SpecialDeviceCategory.NotAvailable;
                        }

                        if (deviceCategory >= 0)
                        {
                            if (!string.IsNullOrEmpty(deviceManufaturer))
                            {
                                foreach (ManufacturerDefinition manufacturerDefinition in
                                    loadingRuleExactMatch.ManufacturerDefinitions)
                                {
                                    if (manufacturerDefinition.Description.IsMatch(deviceManufaturer))
                                    {
                                        this.DeviceConfigurationParameters =
                                            DeviceConfigurationHelperParameters.PreloadConfigurationParameters(manufacturerDefinition.DefinitionFile,
                                                this.deviceId);
                                        this.DefinitionFileName = this.DeviceConfigurationParameters.DefinitionFileName;
                                        break;
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else if ((loadingRuleOjectId != null) && (loadingRuleOjectId.ObjectIdDefinitions.Count > 0))
                    {
                        #region Gestione regole di match via tipo oggetto (Object ID dalla MIB-II)

                        // Carica una definizione specifica partendo dall'ObjectID presente nella MIB-II.
                        // In questo modo si può indicare un tipo unico di device, ma usare definizioni specifiche per modello/versione

                        this.DeviceCategory = (int) SpecialDeviceCategory.NotAvailable;

                        string objectIdValue = this.GetSnmpScalarDataValue(loadingRuleOjectId.ProbingObjectIdOid);

                        if (!string.IsNullOrEmpty(objectIdValue))
                        {
                            foreach (ObjectIdDefinition objectIdDefinition in loadingRuleOjectId.ObjectIdDefinitions)
                            {
                                if (objectIdDefinition.Description.IsMatch(objectIdValue))
                                {
                                    this.DeviceConfigurationParameters =
                                        DeviceConfigurationHelperParameters.PreloadConfigurationParameters(objectIdDefinition.DefinitionFile,
                                            this.deviceId);
                                    this.DefinitionFileName = this.DeviceConfigurationParameters.DefinitionFileName;

                                    // Impostiamo una categoria statica per questo tipo di definizione dinamica
                                    this.DeviceCategory = (int) SpecialDeviceCategory.NotDefinedForCurrentDevice;

                                    if (!string.IsNullOrEmpty(objectIdDefinition.ForcedDeviceType))
                                    {
                                        // Nel caso che la regola abbia un tipo periferca forzato, lo riassegnamo
                                        this.ForcedDeviceType = objectIdDefinition.ForcedDeviceType;
                                    }

                                    break;
                                }
                            }
                        }

                        #endregion
                    }
                    else if ((loadingRuleQueryEvaluate != null) && (loadingRuleQueryEvaluate.QueryEvaluateRuleDefinitions.Count > 0))
                    {
                        #region Gestione regole di caricamento dinamico definizioni, in base a valutazione singole chiamate SNMP

                        // Carica una definizione specifica partendo da una chiamata SNMP, che è poi valutata in base ad un operatore
                        // In questo modo si può indicare un tipo unico di device e caricare le definizioni specifiche in base al fatto che
                        // certe query SNMP rispondano con valori specifici o non rispondano affatto

                        this.DeviceCategory = (int) SpecialDeviceCategory.NotAvailable;

                        foreach (QueryEvaluateRuleDefinition queryEvaluateRuleDefinition in loadingRuleQueryEvaluate.QueryEvaluateRuleDefinitions)
                        {
                            Variable queryValue = this.GetSnmpScalarDataValueVariable(queryEvaluateRuleDefinition.QueryOid);

                            if (QueryEvaluateRuleDefinition.IsMatchEvaluateQueryWithOperator(queryValue, queryEvaluateRuleDefinition.QueryOperator))
                            {
                                this.DeviceConfigurationParameters =
                                    DeviceConfigurationHelperParameters.PreloadConfigurationParameters(queryEvaluateRuleDefinition.DefinitionFile,
                                        this.deviceId);
                                this.DefinitionFileName = this.DeviceConfigurationParameters.DefinitionFileName;

                                // Impostiamo una categoria statica per questo tipo di definizione dinamica
                                this.DeviceCategory = (int) SpecialDeviceCategory.NotDefinedForCurrentDevice;

                                if (!string.IsNullOrEmpty(queryEvaluateRuleDefinition.ForcedDeviceType))
                                {
                                    // Nel caso che la regola abbia un tipo periferca forzato, lo riassegnamo
                                    this.ForcedDeviceType = queryEvaluateRuleDefinition.ForcedDeviceType;
                                }

                                break;
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare delle regole valide in una definizione che è configurata per il caricamento dinamico",
                            this.DefinitionFileName));
                    }

                    // Se arriviamo qui senza avere cambiato definizione corrente e categoria della periferica, rimarranno impostate
                    // la definizione originale e la categoria a "-2". La definizione originale indicherà una MIB-II standard
                    // e saremo in una condizione di query SNMP non specifiche per produttore/periferica
                    if (this.DeviceCategory == (int) SpecialDeviceCategory.NotAvailable)
                    {
                        if ((loadingRuleExactMatch != null) && (loadingRuleExactMatch.NoMatchDeviceStatus != null))
                        {
                            this.NoMatchDeviceStatus = loadingRuleExactMatch.NoMatchDeviceStatus;
                        }

                        if ((loadingRuleOjectId != null) && (loadingRuleOjectId.NoMatchDeviceStatus != null))
                        {
                            this.NoMatchDeviceStatus = loadingRuleOjectId.NoMatchDeviceStatus;
                        }
                    }
                }
            }
        }

        #endregion

        #region LoadConfiguration - Carica la definizione, la valida e popola la struttura di Stream e StreamField

        /// <summary>
        ///     Carica la definizione, la valida e popola la struttura di Stream e StreamField
        /// </summary>
        /// <param name="streamData">Collezione di DbStream che sarà popolata nel parsing della definizione</param>
        public void LoadConfiguration(Collection<DBStream> streamData)
        {
            if (streamData == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Collezione degli Stream da caricare nulla. Impossibile caricare la configurazione della periferica."));
            }

            XmlNode root = this.GetDefinitionXmlRootElement();

            if (root != null)
            {
                #region VersionCode

                object snmpVersionCode = null;

                if (root.Attributes != null)
                {
                    if (root.Attributes["SNMPVersionCode"] != null)
                    {
                        try
                        {
                            string versionCodeString = root.Attributes["SNMPVersionCode"].Value.Trim();
                            snmpVersionCode = Enum.Parse(typeof (VersionCode), versionCodeString, true);
                        }
                        catch (ArgumentException)
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                "L'attributo 'SNMPVersionCode' del nodo 'Device' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
                                this.DefinitionFileName, root.Attributes["SNMPVersionCode"].Value.Trim()));
                        }
                    }
                }

                if (snmpVersionCode == null)
                {
                    // Per default le query Snmp usano la versione 2
                    this.SnmpVersionCode = VersionCode.V2;
                }
                else
                {
                    this.SnmpVersionCode = (VersionCode) snmpVersionCode;
                }

                #endregion

                #region SNOid

                ObjectIdentifier snOid = null;

                if (root.Attributes != null)
                {
                    if (root.Attributes["SNOid"] != null)
                    {
                        string snOidString = root.Attributes["SNOid"].Value.Trim();

                        if (!string.IsNullOrEmpty(snOidString))
                        {
                            snOid = SnmpUtility.TryParseOid(snOidString);

                            if (snOid == null)
                            {
                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                    "L'attributo 'SNOid' del nodo 'Device' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                    this.DefinitionFileName, root.Attributes["SNOid"].Value));
                            }
                        }
                        else
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                "L'attributo 'SNOid' del nodo 'Device' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                this.DefinitionFileName, root.Attributes["SNOid"].Value));
                        }
                    }
                }

                this.SerialNumberOid = snOid;

                #endregion

                #region DynamicCodeCalculateSeverity

                string dynamicCodeCalculateSeverity = null;

                foreach (XmlNode childNode in root.ChildNodes)
                {
                    if (childNode.Name.Equals("DynamicCodeCalculateSeverity", StringComparison.OrdinalIgnoreCase))
                    {
                        if ((childNode.ChildNodes.Count > 0) && (childNode.ChildNodes[0] is XmlCDataSection))
                        {
                            XmlCDataSection cdataSection = childNode.ChildNodes[0] as XmlCDataSection;

                            if (!string.IsNullOrEmpty(cdataSection.Value))
                            {
                                dynamicCodeCalculateSeverity = cdataSection.Value;
                            }

                            break;
                        }
                    }
                }

                this.DynamicCodeCalculateSeverity = dynamicCodeCalculateSeverity;

                #endregion

                #region DynamicCodeCalculateStatusMessage

                string dynamicCodeCalculateStatusMessage = null;

                foreach (XmlNode childNode in root.ChildNodes)
                {
                    if (childNode.Name.Equals("DynamicCodeCalculateStatusMessage", StringComparison.OrdinalIgnoreCase))
                    {
                        if ((childNode.ChildNodes.Count > 0) && (childNode.ChildNodes[0] is XmlCDataSection))
                        {
                            XmlCDataSection cdataSection = childNode.ChildNodes[0] as XmlCDataSection;

                            if (!string.IsNullOrEmpty(cdataSection.Value))
                            {
                                dynamicCodeCalculateStatusMessage = cdataSection.Value;
                            }

                            break;
                        }
                    }
                }

                this.DynamicCodeCalculateStatusMessage = dynamicCodeCalculateStatusMessage;

                #endregion

                bool streamsFound = false;

                #region Streams

                foreach (XmlNode childNode in root.ChildNodes)
                {
                    if (childNode.Name.Equals("streams", StringComparison.OrdinalIgnoreCase))
                    {
                        #region Stream                        

                        bool streamFound = false;

                        foreach (XmlNode stream in childNode.ChildNodes)
                        {
                            // dizionario di streamFields padri
                            // la chiave è l'id dello stream field
                            Dictionary<int, DBStreamField> streamFieldsFathers = new Dictionary<int, DBStreamField>();

                            if (stream.Name.Equals("stream", StringComparison.OrdinalIgnoreCase))
                            {
                                if ((stream.Attributes["ID"] == null) || (stream.Attributes["ID"].Value.Trim().Length == 0) ||
                                    (stream.Attributes["Name"] == null) || (stream.Attributes["Name"].Value.Trim().Length == 0))
                                {
                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                        "Impossibile trovare l'attributo 'ID' o 'Name' sul nodo 'Stream', oppure il nome dello Stream è nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                        this.DefinitionFileName, stream.OuterXml));
                                }

                                #region ID

                                int streamId;

                                if (!int.TryParse(stream.Attributes["ID"].Value, out streamId))
                                {
                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                        "L'attributo 'ID' del nodo 'Stream' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                        this.DefinitionFileName, stream.Attributes["ID"].Value));
                                }

                                #endregion

                                #region IsMib2

                                bool isMib2 = false;

                                if (stream.Attributes["IsMIB2"] != null)
                                {
                                    string isMib2String = stream.Attributes["IsMIB2"].Value.Trim().ToLowerInvariant();
                                    if (!Boolean.TryParse(isMib2String, out isMib2))
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "L'attributo 'IsMIB2' del nodo 'Stream' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
                                            this.DefinitionFileName, stream.Attributes["IsMIB2"].Value.Trim()));
                                    }
                                }

                                #endregion

                                #region ExcludedFromSeverityComputation

                                bool streamExcludedFromSeverityComputation = false;

                                if (stream.Attributes["ExcludedFromSeverityComputation"] != null)
                                {
                                    string excludedFromSeverityComputationString =
                                        stream.Attributes["ExcludedFromSeverityComputation"].Value.Trim().ToLowerInvariant();
                                    if (!bool.TryParse(excludedFromSeverityComputationString, out streamExcludedFromSeverityComputation))
                                    {
                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                            "L'attributo 'ExcludedFromSeverityComputation' del nodo 'Stream' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
                                            this.DefinitionFileName, stream.Attributes["ExcludedFromSeverityComputation"].Value.Trim()));
                                    }
                                }

                                #endregion

                                DBStream str = new DBStream(streamId, stream.Attributes["Name"].Value.Trim(), isMib2,
                                    streamExcludedFromSeverityComputation);

                                #region StreamField

                                bool streamFieldFound = false;

                                // indica se ci troviamo in presenza di un raccoglimento 
                                bool is_compress_into_desc = false;
                                // è il nodo
                                XmlNode original = null;
                                

                                foreach (XmlNode streamField in stream.ChildNodes)
                                {
                                    if (streamField.Name.Equals("StreamField", StringComparison.OrdinalIgnoreCase))
                                    {
                                        #region Verifica presenza attributi ID e Name

                                        if ((streamField.Attributes["ID"] == null) || (streamField.Attributes["ID"].Value.Trim().Length == 0) ||
                                            (streamField.Attributes["Name"] == null) || (streamField.Attributes["Name"].Value.Trim().Length == 0))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "Impossibile trovare l'attributo 'ID' o 'Name' sul nodo 'StreamField' nel file {0}, oppure nome dello StreamField nullo. Il contenuto del nodo è:\r\n{1}",
                                                this.DefinitionFileName, streamField.OuterXml));
                                        }

                                        #endregion

                                        // Verifica che sia presente almeno uno tra gli attributi principali: Oid, TableOid o FixedValueString
                                        if ((this.DeviceConfigurationParameters.DefinitionType == DeviceKind.Snmp) &&
                                            ((streamField.Attributes["Oid"] == null) || (streamField.Attributes["Oid"].Value.Trim().Length == 0)) &&
                                            ((streamField.Attributes["TableOid"] == null) ||
                                             (streamField.Attributes["TableOid"].Value.Trim().Length == 0)) &&
                                            ((streamField.Attributes["FixedValueString"] == null) ||
                                             (streamField.Attributes["FixedValueString"].Value.Trim().Length == 0)))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "Almeno un attributo tra 'Oid', 'TableOid' e 'FixedValueString' è obbligatorio sul nodo 'StreamField' nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                this.DefinitionFileName, streamField.OuterXml));
                                        }

                                        #region Validazione attributi nel caso non sia indicato l'Oid principale (quindi se si usa la gestione tabellare) e la periferica è di tipo SNMP

                                        if ((this.DeviceConfigurationParameters.DefinitionType == DeviceKind.Snmp) &&
                                            ((streamField.Attributes["Oid"] == null) || (streamField.Attributes["Oid"].Value.Trim().Length == 0)) &&
                                            ((streamField.Attributes["FixedValueString"] == null) ||
                                             (streamField.Attributes["FixedValueString"].Value.Trim().Length == 0)))
                                        {
                                            #region Verifica presenza TableOid

                                            if ((streamField.Attributes["TableOid"] == null) ||
                                                (streamField.Attributes["TableOid"].Value.Trim().Length == 0))
                                            {
                                                // Nel caso che non sia presente l'Oid per la richiesta scalare, è obbligatorio quello per la richiesta tabellare
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Almeno un attributo tra 'Oid' e 'TableOid' è obbligatorio sul nodo 'StreamField' nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                    this.DefinitionFileName, streamField.OuterXml));
                                            }

                                            #endregion

                                            // In questo punto, siamo nella gestione tabellare, quindi occorre verificare gli opportuni attributi

                                            #region Validazioni attributi per query SNMP tabellari

                                            if ((streamField.Attributes["TableRowIndex"] != null) &&
                                                (streamField.Attributes["TableRowIndex"].Value.Trim().Length > 0))
                                            {
                                                // E' presente la TableRowIndex, è obbligatorio il TableEvaluationFormula

                                                #region Verifica presenza TableEvaluationFormula

                                                if ((streamField.Attributes["TableEvaluationFormula"] == null) ||
                                                    (streamField.Attributes["TableEvaluationFormula"].Value.Trim().Length == 0))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile trovare l'attributo 'TableEvaluationFormula' sul nodo 'StreamField' nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                        this.DefinitionFileName, streamField.OuterXml));
                                                }

                                                #endregion
                                            }
                                            else
                                            {
                                                #region Verifica presenza TableFilterColumnIndex e TableFilterColumnRegex

                                                // Non è presente la TableRowIndex, quindi sono obbligatori TableFilterColumnIndex e TableFilterColumnRegex
                                                if ((streamField.Attributes["TableFilterColumnIndex"] == null) ||
                                                    (streamField.Attributes["TableFilterColumnIndex"].Value.Trim().Length == 0) ||
                                                    (streamField.Attributes["TableFilterColumnRegex"] == null) ||
                                                    (streamField.Attributes["TableFilterColumnRegex"].Value.Trim().Length == 0))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "Impossibile trovare l'attributo 'TableFilterColumnIndex', 'TableFilterColumnRegex' o 'TableCorrelationEvaluationFormula' sul nodo 'StreamField' nel file {0}, oppure indice di colonna per tabella SNMP, espressione regolare per ricerca o codice di valutazione del valore tabellare correlato SNMP nulli. Il contenuto del nodo è:\r\n{1}",
                                                        this.DefinitionFileName, streamField.OuterXml));
                                                }

                                                #endregion

                                                #region Presente CorrelationTableOid

                                                if ((streamField.Attributes["CorrelationTableOid"] != null) &&
                                                    (streamField.Attributes["CorrelationTableOid"].Value.Trim().Length > 0))
                                                {
                                                    // E' indicato l'Oid per il recupero di dati da tabella correlata, è obbligatorio TableCorrelationEvaluationFormula
                                                    if ((streamField.Attributes["TableCorrelationEvaluationFormula"] == null) ||
                                                        (streamField.Attributes["TableCorrelationEvaluationFormula"].Value.Trim().Length == 0))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "Impossibile trovare l'attributo 'TableCorrelationEvaluationFormula' sul nodo 'StreamField' nel file {0}, oppure codice di valutazione del valore tabellare correlato SNMP nullo. Il contenuto del nodo è:\r\n{1}",
                                                            this.DefinitionFileName, streamField.OuterXml));
                                                    }

                                                    #region Presente TableCorrelationFilterColumnIndex

                                                    if ((streamField.Attributes["TableCorrelationFilterColumnIndex"] != null) &&
                                                        (streamField.Attributes["TableCorrelationFilterColumnIndex"].Value.Trim().Length > 0))
                                                    {
                                                        // E' indicato l'attributo TableCorrelationFilterColumnIndex, è obbligatorio TableEvaluationFormula
                                                        if ((streamField.Attributes["TableEvaluationFormula"] == null) ||
                                                            (streamField.Attributes["TableEvaluationFormula"].Value.Trim().Length == 0))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "Impossibile trovare l'attributo 'TableEvaluationFormula' sul nodo 'StreamField' nel file {0}, oppure codice di valutazione del valore tabellare SNMP nullo. Il contenuto del nodo è:\r\n{1}",
                                                                this.DefinitionFileName, streamField.OuterXml));
                                                        }
                                                    }

                                                    #endregion
                                                }

                                                #endregion

                                                #region CorrelationTableOid nullo

                                                if ((streamField.Attributes["CorrelationTableOid"] == null) ||
                                                    (streamField.Attributes["CorrelationTableOid"].Value.Trim().Length == 0))
                                                {
                                                    // Non è indicato l'Oid per il recupero di dati da tabella correlata, quindi è obbligatoria la TableEvaluationFormula
                                                    if ((streamField.Attributes["TableEvaluationFormula"] == null) ||
                                                        (streamField.Attributes["TableEvaluationFormula"].Value.Trim().Length == 0))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "Impossibile trovare l'attributo 'TableEvaluationFormula' sul nodo 'StreamField' nel file {0}, oppure codice di valutazione del valore tabellare SNMP nullo. Il contenuto del nodo è:\r\n{1}",
                                                            this.DefinitionFileName, streamField.OuterXml));
                                                    }
                                                }

                                                #endregion
                                            }

                                            #endregion
                                        }

                                        #endregion

                                        #region ID

                                        int fieldId;

                                        if (!int.TryParse(streamField.Attributes["ID"].Value, out fieldId))
                                        {
                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                "L'attributo 'ID' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                this.DefinitionFileName, streamField.Attributes["ID"].Value));
                                        }

                                        #endregion

                                        #region ArrayID

                                        int arrayId = 0;

                                        if (streamField.Attributes["ArrayID"] != null)
                                        {
                                            if (!int.TryParse(streamField.Attributes["ArrayID"].Value, out arrayId))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'ArrayID' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["ArrayID"].Value));
                                            }
                                        }

                                        #endregion

                                        #region Oid

                                        ObjectIdentifier oid = null;

                                        if (streamField.Attributes["Oid"] != null)
                                        {
                                            string oidString = streamField.Attributes["Oid"].Value.Trim();

                                            if (!string.IsNullOrEmpty(oidString))
                                            {
                                                oid = SnmpUtility.TryParseOid(oidString);

                                                if (oid == null)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'Oid' del nodo 'StreamField' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, streamField.Attributes["Oid"].Value));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'Oid' del nodo 'StreamField' nel file {0}, non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["Oid"].Value));
                                            }
                                        }

                                        #endregion

                                        #region FixedValueString

                                        string fixedValueString = null;

                                        if (streamField.Attributes["FixedValueString"] != null)
                                        {
                                            fixedValueString = streamField.Attributes["FixedValueString"].Value.Trim();

                                            if (string.IsNullOrEmpty(fixedValueString))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'FixedValueString' del nodo 'StreamField' nel file {0}, non contiene un valore valido. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["FixedValueString"].Value));
                                            }
                                        }

                                        #endregion

                                        #region TableOid

                                        TableOidAndSupportData tableOidAndSupportData = null;

                                        if (streamField.Attributes["TableOid"] != null)
                                        {
                                            TableOidAndSupportData tableOidAndSupportDataLocal;

                                            try
                                            {
                                                tableOidAndSupportDataLocal = new TableOidAndSupportData("TableOid",
                                                    streamField.Attributes["TableOid"].Value.Trim());

                                                if (!tableOidAndSupportDataLocal.IsValid)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'TableOid' del nodo 'StreamField' nel file {0}, non è nel formato atteso 'Oid tabella[;table entry Id;numero colonne;colonna indice1[|colonna indice2][|colonna indice 3][...]]', ad esempio '1.3.6.1.2.1.4.22;1;4;1|3' (tabella sparsa, con 4 colonne e indice composito su colonna 1 e 3), '1.3.6.1.2.1.2.2;1;22;1' (tabella sparsa con 22 colonne, indice su colonna 1), '1.3.6.1.2.1.4.20' (tabella standard). Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, streamField.Attributes["TableOid"].Value.Trim()));
                                                }
                                            }
                                            catch (ArgumentException ex)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Errore durante la lettura dell'attributo 'TableOid' del nodo 'StreamField' nel file {0}. {1}",
                                                    this.DefinitionFileName, ex.Message));
                                            }

                                            tableOidAndSupportData = tableOidAndSupportDataLocal;
                                        }

                                        #endregion

                                        #region CorrelationTableOid

                                        TableOidAndSupportData correlationTableOidAndSupportData = null;

                                        if (streamField.Attributes["CorrelationTableOid"] != null)
                                        {
                                            TableOidAndSupportData correlationTableOidAndSupportDataLocal;

                                            try
                                            {
                                                correlationTableOidAndSupportDataLocal = new TableOidAndSupportData("CorrelationTableOid",
                                                    streamField.Attributes["CorrelationTableOid"].Value.Trim());

                                                if (!correlationTableOidAndSupportDataLocal.IsValid)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'CorrelationTableOid' del nodo 'StreamField' nel file {0}, non è nel formato atteso 'Oid tabella[;table entry Id;numero colonne;colonna indice1[|colonna indice2][|colonna indice 3][...]]', ad esempio '1.3.6.1.2.1.4.22;1;4;1|3' (tabella sparsa, con 4 colonne e indice composito su colonna 1 e 3), '1.3.6.1.2.1.2.2;1;22;1' (tabella sparsa con 22 colonne, indice su colonna 1), '1.3.6.1.2.1.4.20' (tabella standard). Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, streamField.Attributes["CorrelationTableOid"].Value.Trim()));
                                                }
                                            }
                                            catch (ArgumentException ex)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Errore durante la lettura dell'attributo 'CorrelationTableOid' del nodo 'StreamField' nel file {0}. {1}",
                                                    this.DefinitionFileName, ex.Message));
                                            }

                                            correlationTableOidAndSupportData = correlationTableOidAndSupportDataLocal;
                                        }

                                        #endregion

                                        #region TableRowIndex

                                        int? tableRowIndex = null;

                                        if (streamField.Attributes["TableRowIndex"] != null)
                                        {
                                            int tableRowIndexValue;

                                            if (!int.TryParse(streamField.Attributes["TableRowIndex"].Value, out tableRowIndexValue))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'TableRowIndex' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["TableRowIndex"].Value));
                                            }

                                            tableRowIndex = tableRowIndexValue;
                                        }

                                        #endregion

                                        
                                        #region ATTR_STREAM_FIELD_FATHER
                                        int stream_field_father_id = -1;
                                        if (streamField.Attributes[ATTR_STREAM_FIELD_FATHER] != null)
                                        {
                                            if (!int.TryParse(streamField.Attributes[ATTR_STREAM_FIELD_FATHER].Value, out stream_field_father_id))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore numerico intero. Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_FATHER, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_FATHER].Value));
                                            }

                                            if (stream_field_father_id < 0)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore numerico maggiore o uguale di 0. Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_FATHER, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_FATHER].Value));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_MAX_SIZE
                                        int stream_field_max_size = -1;
                                        if (streamField.Attributes[ATTR_STREAM_FIELD_MAX_SIZE] != null)
                                        {
                                            if (!int.TryParse(streamField.Attributes[ATTR_STREAM_FIELD_MAX_SIZE].Value, out stream_field_max_size))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore numerico intero. Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_MAX_SIZE, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_MAX_SIZE].Value));
                                            }

                                            if (stream_field_max_size <= 0)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore numerico maggiore di 0. Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_MAX_SIZE, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_MAX_SIZE].Value));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_IS_FATHER
                                        bool stream_field_is_father = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_IS_FATHER] != null)
                                        {
                                            string stream_field_is_father_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_IS_FATHER].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_father_string, out stream_field_is_father))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_IS_FATHER, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_IS_FATHER].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_AGGREGATION
                                        bool stream_field_is_aggregation = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_AGGREGATION] != null)
                                        {
                                            string stream_field_is_aggregation_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_AGGREGATION].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_aggregation_string, out stream_field_is_aggregation))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_AGGREGATION, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_AGGREGATION].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_SHOW_NAME
                                        bool stream_field_is_show_name = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_SHOW_NAME] != null)
                                        {
                                            string stream_field_is_show_name_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_SHOW_NAME].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_show_name_string, out stream_field_is_show_name))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_SHOW_NAME, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_SHOW_NAME].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_SHOW_VALUE_DESCRIPTION
                                        bool stream_field_is_show_value_description = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_SHOW_VALUE_DESCRIPTION] != null)
                                        {
                                            string stream_field_is_show_value_description_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_SHOW_VALUE_DESCRIPTION].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_show_value_description_string, out stream_field_is_show_value_description))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_SHOW_VALUE_DESCRIPTION, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_SHOW_VALUE_DESCRIPTION].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_SHOW_ROW_ID
                                        String show_row_id_expression = null;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_SHOW_ROW_ID] != null && streamField.Attributes[ATTR_STREAM_FIELD_SHOW_ROW_ID].Value.Trim().Length > 0 )
                                        {
                                            show_row_id_expression = streamField.Attributes[ATTR_STREAM_FIELD_SHOW_ROW_ID].Value.Trim();
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_EVALUATION_FORMULA
                                        String scalar_evaluation_formula = null;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_EVALUATION_FORMULA] != null && streamField.Attributes[ATTR_STREAM_FIELD_EVALUATION_FORMULA].Value.Trim().Length > 0)
                                        {
                                            scalar_evaluation_formula = streamField.Attributes[ATTR_STREAM_FIELD_EVALUATION_FORMULA].Value.Trim();
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_IS_ARRAY
                                        bool stream_field_is_array = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_IS_ARRAY] != null)
                                        {
                                            string stream_field_is_array_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_IS_ARRAY].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_array_string, out stream_field_is_array))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_IS_ARRAY, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_IS_ARRAY].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_IS_STREAM_FIELD_ARRAY
                                        bool stream_field_is_stream_field_array = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_IS_STREAM_FIELD_ARRAY] != null)
                                        {
                                            string stream_field_is_stream_field_array_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_IS_STREAM_FIELD_ARRAY].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_stream_field_array_string, out stream_field_is_stream_field_array))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_IS_STREAM_FIELD_ARRAY, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_IS_STREAM_FIELD_ARRAY].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_IS_TIME_TICKS_MS
                                        bool stream_field_is_time_ticks_ms = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_IS_TIME_TICKS_MS] != null)
                                        {
                                            string stream_field_is_time_ticks_ms_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_IS_TIME_TICKS_MS].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_time_ticks_ms_string, out stream_field_is_time_ticks_ms))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_IS_TIME_TICKS_MS, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_IS_TIME_TICKS_MS].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_SHOW_ROW_ID_ZERO_BASE
                                        bool stream_field_is_stream_field_row_id_zero_based = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_SHOW_ROW_ID_ZERO_BASE] != null)
                                        {
                                            string stream_field_is_stream_field_row_id_zero_based_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_SHOW_ROW_ID_ZERO_BASE].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_is_stream_field_row_id_zero_based_string, out stream_field_is_stream_field_row_id_zero_based))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_SHOW_ROW_ID_ZERO_BASE, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_SHOW_ROW_ID_ZERO_BASE].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region ATTR_STREAM_FIELD_USE_TO_HEAD_OF_STREAM_FIELD_ARRAY
                                        bool stream_field_use_to_head_of_stream_field_array = false;

                                        if (streamField.Attributes[ATTR_STREAM_FIELD_USE_TO_HEAD_OF_STREAM_FIELD_ARRAY] != null)
                                        {
                                            string stream_field_use_to_head_of_stream_field_array_string =
                                                streamField.Attributes[ATTR_STREAM_FIELD_USE_TO_HEAD_OF_STREAM_FIELD_ARRAY].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(stream_field_use_to_head_of_stream_field_array_string, out stream_field_use_to_head_of_stream_field_array))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo '{0}' del nodo 'StreamField' nel file {1}, non contiene un valore booleano (true/false). Valore contenuto: '{2}'",
                                                    ATTR_STREAM_FIELD_USE_TO_HEAD_OF_STREAM_FIELD_ARRAY, this.DefinitionFileName, streamField.Attributes[ATTR_STREAM_FIELD_USE_TO_HEAD_OF_STREAM_FIELD_ARRAY].Value.Trim()));
                                            }
                                        }
                                        #endregion

                                        #region TableEvaluationFormula

                                        string evaluationFormula = null;

                                        if (streamField.Attributes["TableEvaluationFormula"] != null)
                                        {
                                            evaluationFormula = streamField.Attributes["TableEvaluationFormula"].Value;
                                        }

                                        #endregion

                                        #region TableCorrelationEvaluationFormula

                                        string tableCorrelationEvaluationFormula = null;

                                        if (streamField.Attributes["TableCorrelationEvaluationFormula"] != null)
                                        {
                                            tableCorrelationEvaluationFormula = streamField.Attributes["TableCorrelationEvaluationFormula"].Value;
                                        }

                                        #endregion

                                        #region TableFilterColumnIndex

                                        int? tableFilterColumnIndex = null;

                                        if (streamField.Attributes["TableFilterColumnIndex"] != null)
                                        {
                                            int tableFilterColumnIndexValue;

                                            if (!int.TryParse(streamField.Attributes["TableFilterColumnIndex"].Value, out tableFilterColumnIndexValue))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'TableFilterColumnIndex' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["TableFilterColumnIndex"].Value));
                                            }

                                            tableFilterColumnIndex = tableFilterColumnIndexValue;
                                        }

                                        #endregion

                                        #region TableCorrelationFilterColumnIndex

                                        int? tableCorrelationFilterColumnIndex = null;

                                        if (streamField.Attributes["TableCorrelationFilterColumnIndex"] != null)
                                        {
                                            int tableCorrelationFilterColumnIndexValue;

                                            if (
                                                !int.TryParse(streamField.Attributes["TableCorrelationFilterColumnIndex"].Value,
                                                    out tableCorrelationFilterColumnIndexValue))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'TableCorrelationFilterColumnIndex' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["TableCorrelationFilterColumnIndex"].Value));
                                            }

                                            tableCorrelationFilterColumnIndex = tableCorrelationFilterColumnIndexValue;
                                        }

                                        #endregion

                                        #region TableFilterColumnRegex

                                        Regex tableFilterColumnRegex = null;

                                        if (streamField.Attributes["TableFilterColumnRegex"] != null)
                                        {
                                            try
                                            {
                                                tableFilterColumnRegex = new Regex(streamField.Attributes["TableFilterColumnRegex"].Value,
                                                    RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                                            }
                                            catch (ArgumentNullException)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'TableFilterColumnRegex' del nodo 'StreamField' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["TableFilterColumnRegex"].Value));
                                            }
                                            catch (ArgumentException)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'TableFilterColumnRegex' del nodo 'StreamField' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["TableFilterColumnRegex"].Value));
                                            }
                                        }

                                        #endregion

                                        #region RelevantToDeviceCategory

                                        List<int> relevantToDeviceCategoryList = null;

                                        if (streamField.Attributes["RelevantToDeviceCategory"] != null)
                                        {
                                            string relevantToDeviceCategoryString = streamField.Attributes["RelevantToDeviceCategory"].Value.Trim();

                                            if (!string.IsNullOrEmpty(relevantToDeviceCategoryString))
                                            {
                                                if (relevantToDeviceCategoryString.Equals("*", StringComparison.OrdinalIgnoreCase))
                                                {
                                                    relevantToDeviceCategoryList = new List<int>();
                                                }
                                                else
                                                {
                                                    relevantToDeviceCategoryList = new List<int>();

                                                    foreach (string relevantToDeviceCategoryValueString in
                                                        relevantToDeviceCategoryString.Split(','))
                                                    {
                                                        int relevantToDeviceCategoryValue;

                                                        if (
                                                            !int.TryParse(relevantToDeviceCategoryValueString.Trim(),
                                                                out relevantToDeviceCategoryValue))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'RelevantToDeviceCategory' del nodo 'StreamField' nel file {0}, non contiene una lista di valori numerici interi separati da virgole. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, streamField.Attributes["RelevantToDeviceCategory"].Value));
                                                        }

                                                        relevantToDeviceCategoryList.Add(relevantToDeviceCategoryValue);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (this.HasDynamicLoadingRules)
                                            {
                                                if ((this.DeviceCategory == (int) SpecialDeviceCategory.NotDefinedForCurrentDevice) ||
                                                    (this.DeviceCategory == (int) SpecialDeviceCategory.NotAvailable))
                                                {
                                                    // Se la periferica è a definizione dinamica e quindi non ha una categoria applicabile,
                                                    // oppure non è riuscita a caricare una definizione via regole dinamiche,
                                                    // rendiamo tutti gli stream generici, come se fosse indicato "*"
                                                    relevantToDeviceCategoryList = new List<int>();
                                                }
                                            }
                                        }

                                        #endregion

                                        #region ValueFormatExpression

                                        string valueFormatExpression = null;

                                        if (streamField.Attributes["ValueFormatExpression"] != null)
                                        {
                                            valueFormatExpression = streamField.Attributes["ValueFormatExpression"].Value;
                                        }

                                        #endregion

                                        #region TableIndexAndScalarCounterOid

                                        int? tableIndex = -1;
                                        ObjectIdentifier scalarCounterOid = null;

                                        if (streamField.Attributes["TableIndexAndScalarCounterOid"] != null)
                                        {
                                            string tableIndexAndScalarCounterOidString =
                                                streamField.Attributes["TableIndexAndScalarCounterOid"].Value.Trim();

                                            if (!string.IsNullOrEmpty(tableIndexAndScalarCounterOidString))
                                            {
                                                string[] values = tableIndexAndScalarCounterOidString.Split(',');

                                                if (values.Length == 2)
                                                {
                                                    #region Parsing indice di riga per tabella dinamica

                                                    string tableIndexValueString = values[0].Trim();

                                                    int tableIndexValue;

                                                    if (!int.TryParse(tableIndexValueString, out tableIndexValue))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'TableIndexAndScalarCounterOid' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero prima della virgola. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, streamField.Attributes["TableIndexAndScalarCounterOid"].Value));
                                                    }

                                                    tableIndex = tableIndexValue;

                                                    #endregion

                                                    #region Parsing Oid a valore scalare con il contatore relativo alla tabella

                                                    string scalarCounterOidString = values[1].Trim();
                                                    if (!string.IsNullOrEmpty(scalarCounterOidString))
                                                    {
                                                        scalarCounterOid = SnmpUtility.TryParseOid(scalarCounterOidString);

                                                        if (scalarCounterOid == null)
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'TableIndexAndScalarCounterOid' del nodo 'StreamField' nel file {0}, non contiene una lista di valori numerici interi separati da punti dopo la virgola. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, streamField.Attributes["TableIndexAndScalarCounterOid"].Value));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'TableIndexAndScalarCounterOid' del nodo 'StreamField' nel file {0}, non contiene una lista di valori numerici interi separati da punti dopo la virgola. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, streamField.Attributes["TableIndexAndScalarCounterOid"].Value));
                                                    }

                                                    #endregion
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'TableIndexAndScalarCounterOid' del nodo 'StreamField' nel file {0}, non contiene un valore numerico intero seguito da virgola e da una serie di valori numerici interi separati da punti. Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, streamField.Attributes["TableIndexAndScalarCounterOid"].Value));
                                                }
                                            }
                                        }

                                        #endregion

                                        #region ExcludedFromSeverityComputation

                                        bool excludedFromSeverityComputation = false;

                                        if (streamField.Attributes["ExcludedFromSeverityComputation"] != null)
                                        {
                                            string excludedFromSeverityComputationString =
                                                streamField.Attributes["ExcludedFromSeverityComputation"].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(excludedFromSeverityComputationString, out excludedFromSeverityComputation))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'ExcludedFromSeverityComputation' del nodo 'StreamField' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["ExcludedFromSeverityComputation"].Value.Trim()));
                                            }
                                        }

                                        #endregion

                                        #region NoDataAvailableForcedState

                                        NoDataAvailableForcedState noDataAvailableForcedState = null;

                                        if (streamField.Attributes["NoDataAvailableForcedState"] != null)
                                        {
                                            string noDataAvailableForcedStateString =
                                                streamField.Attributes["NoDataAvailableForcedState"].Value.Trim();

                                            string[] values = noDataAvailableForcedStateString.Split(';');

                                            if ((values.Length == 3) || (values.Length == 4))
                                            {
                                                object severity;

                                                try
                                                {
                                                    severity = Enum.Parse(typeof (Severity), values[0].Trim(), true);
                                                }
                                                catch (ArgumentException)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error, Unknown e Info). Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, values[0].Trim()));
                                                }

                                                string value = values[1].Trim();

                                                if (string.IsNullOrEmpty(value))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene una stringa per il valore valida, oppure la stringa è vuota. Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, values[1].Trim()));
                                                }

                                                string valueDescription = values[2].Trim();

                                                if (string.IsNullOrEmpty(valueDescription))
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene una stringa per la descrizione del valore valida, oppure la stringa è vuota. Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, values[2].Trim()));
                                                }

                                                bool shouldSendNotificationByEmail = false;

                                                if (values.Length == 4)
                                                {
                                                    string shouldSendNotificationByEmailString = values[3].Trim();

                                                    if (shouldSendNotificationByEmailString.Equals("ShouldSendNotificationByEmail",
                                                        StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        // La presenza stessa della stringa permette di identificare l'invio delle mail
                                                        shouldSendNotificationByEmail = true;
                                                    }
                                                    else
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene il valore previsto 'ShouldSendNotificationByEmail' in quarta posizione. L'invio delle mail sarà disattivato. Valore contenuto: '{1}'",
                                                            this.DefinitionFileName, values[3].Trim()));
                                                    }
                                                }

                                                if (severity is Severity)
                                                {
                                                    noDataAvailableForcedState = new NoDataAvailableForcedState((Severity) severity, valueDescription,
                                                        value, shouldSendNotificationByEmail);
                                                }
                                                else
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error, Unknown e Info). Valore contenuto: '{1}'",
                                                        this.DefinitionFileName, values[0].Trim()));
                                                }
                                            }
                                            else
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'NoDataAvailableForcedState' del nodo 'StreamField' nel file {0}, non contiene tre valori separati da punti e virgola, ad esempio 'Error;0;Non in esecuzione'. Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["NoDataAvailableForcedState"].Value.Trim()));
                                            }
                                        }

                                        #endregion

                                        #region TableHideIfNotAvailableRowAndColumnIndex

                                        TableHideIfNotAvailableRowAndColumnIndex tableHideIfNotAvailableRowAndColumnIndex = null;

                                        if (streamField.Attributes["TableHideIfNotAvailableRowAndColumnIndex"] != null)
                                        {
                                            TableHideIfNotAvailableRowAndColumnIndex tableHideIfNotAvailableRowAndColumnIndexLocal;

                                            try
                                            {
                                                tableHideIfNotAvailableRowAndColumnIndexLocal =
                                                    new TableHideIfNotAvailableRowAndColumnIndex(
                                                        streamField.Attributes["TableHideIfNotAvailableRowAndColumnIndex"].Value.Trim());

                                                if (!tableHideIfNotAvailableRowAndColumnIndexLocal.IsValid)
                                                {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                        "L'attributo 'TableHideIfNotAvailableRowAndColumnIndex' del nodo 'StreamField' nel file {0}, non è nel formato atteso 'Valore da ricercare su righe, colonna tabella SNMP, Oid tabella SNMP su cui effettuare la ricerca', ad esempio '1,0,1.3.6.1.4.1.12.43.13.384.3.1.4'. Valore contenuto: '{1}'",
                                                        this.DefinitionFileName,
                                                        streamField.Attributes["TableHideIfNotAvailableRowAndColumnIndex"].Value.Trim()));
                                                }
                                            }
                                            catch (ArgumentException ex)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Errore durante la lettura dell'attributo 'TableHideIfNotAvailableRowAndColumnIndex' del nodo 'StreamField' nel file {0}. {1}",
                                                    this.DefinitionFileName, ex.Message));
                                            }

                                            tableHideIfNotAvailableRowAndColumnIndex = tableHideIfNotAvailableRowAndColumnIndexLocal;
                                        }

                                        #endregion

                                        #region HideIfNoDataAvailable

                                        bool hideIfNoDataAvailable = false;

                                        if (streamField.Attributes["HideIfNoDataAvailable"] != null)
                                        {
                                            string hideIfNoDataAvailableString =
                                                streamField.Attributes["HideIfNoDataAvailable"].Value.Trim().ToLowerInvariant();
                                            if (!bool.TryParse(hideIfNoDataAvailableString, out hideIfNoDataAvailable))
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "L'attributo 'HideIfNoDataAvailable' del nodo 'StreamField' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
                                                    this.DefinitionFileName, streamField.Attributes["HideIfNoDataAvailable"].Value.Trim()));
                                            }
                                        }

                                        #endregion

                                        DBStreamField strField = null;

                                        if ((this.DeviceCategory == (int) SpecialDeviceCategory.Undefined) ||
                                            (this.DeviceCategory == (int) SpecialDeviceCategory.NotDefinedForCurrentDevice) ||
                                            ((this.DeviceCategory == (int) SpecialDeviceCategory.NotAvailable) &&
                                             (relevantToDeviceCategoryList.Count == 0)) ||
                                            ((this.DeviceCategory >= 0) && (relevantToDeviceCategoryList != null) &&
                                             ((relevantToDeviceCategoryList.Count == 0) ||
                                              ((relevantToDeviceCategoryList.Count > 0) &&
                                               (relevantToDeviceCategoryList.Contains(this.DeviceCategory))))))
                                        {
                                            // Lo Stream Field deve essere aggiunto se:
                                            // La periferica corrente non ha una categoria indicata (-1)
                                            // La periferica corrente è di tipo dinamico, ma non è stato possibile sapere la categoria (-2) e
                                            // quindi non si carica una definizione custom per produttore, di cui si usano solo gli stream per categorie "*"
                                            // La periferica corrente ha una categoria e:
                                            // o ha configurato la lista delle categorie relative come "*", quindi valido per qualsiasi categoria
                                            // oppure esiste una lista di categorie relative e quella della periferica corrente è contenuta nella lista
                                            // Nel caso che sia disponibile la categoria della periferica, l'attributo RelevantToDeviceCategory
                                            // deve essere presente e contenere o "*" oppure una lista di categorie separate da virgole
                                            // La periferica corrente è di tipo dinamico e non ha categoria valida (cioè è di tipo per ObjectID)

                                            strField = new DBStreamField(fieldId, arrayId, streamField.Attributes["Name"].Value.Trim(), oid,
                                                tableOidAndSupportData, correlationTableOidAndSupportData, tableRowIndex, evaluationFormula,
                                                tableCorrelationEvaluationFormula, tableFilterColumnIndex, tableCorrelationFilterColumnIndex,
                                                tableFilterColumnRegex, new FormatExpression(valueFormatExpression), tableIndex, scalarCounterOid,
                                                excludedFromSeverityComputation, noDataAvailableForcedState, tableHideIfNotAvailableRowAndColumnIndex,
                                                fixedValueString, hideIfNoDataAvailable);
                                        }

                                        if (strField != null)
                                        {
                                            #region FieldValueDescription

                                            bool streamFieldValueDescriptionFound = false;

                                            foreach (XmlNode fieldValueDescription in streamField.ChildNodes)
                                            {
                                                if (fieldValueDescription.Name.Equals("FieldValueDescription", StringComparison.OrdinalIgnoreCase))
                                                {
                                                    if ((fieldValueDescription.Attributes["ValueDescription"] == null) ||
                                                        (fieldValueDescription.Attributes["ValueDescription"].Value.Trim().Length == 0))
                                                    {
                                                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                            "Impossibile trovare l'attributo 'ValueDescription' sul nodo 'FieldValueDescription', oppure descrizione del valore nulla nel file {0}. Il contenuto del nodo è:\r\n{1}",
                                                            this.DefinitionFileName, fieldValueDescription.OuterXml));
                                                    }

                                                    #region Severity

                                                    object severity = null;

                                                    if (fieldValueDescription.Attributes["Severity"] != null)
                                                    {
                                                        try
                                                        {
                                                            string severityString = fieldValueDescription.Attributes["Severity"].Value.Trim();
                                                            severity = Enum.Parse(typeof (Severity), severityString, true);
                                                            if (severity == null)
                                                            {
                                                                throw new DeviceConfigurationHelperException(
                                                                    string.Format(CultureInfo.InvariantCulture,
                                                                        "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
                                                                        this.DefinitionFileName,
                                                                        fieldValueDescription.Attributes["Severity"].Value.Trim()));
                                                            }
                                                        }
                                                        catch (ArgumentException)
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'Severity' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore tra quelli ammessi. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, fieldValueDescription.Attributes["Severity"].Value.Trim()));
                                                        }
                                                    }

                                                    if (severity == null)
                                                    {
                                                        severity = Severity.Ok;
                                                    }

                                                    #endregion

                                                    #region ExactValueString

                                                    string exactValueString = null;

                                                    if (fieldValueDescription.Attributes["ExactValueString"] != null)
                                                    {
                                                        exactValueString = fieldValueDescription.Attributes["ExactValueString"].Value;
                                                    }

                                                    #endregion

                                                    #region ExactValueInt

                                                    int? exactValueIntNull = null;

                                                    if (fieldValueDescription.Attributes["ExactValueInt"] != null)
                                                    {
                                                        string exactValueIntString = fieldValueDescription.Attributes["ExactValueInt"].Value.Trim();
                                                        int exactValueInt;

                                                        if (!int.TryParse(exactValueIntString, out exactValueInt))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'ExactValueInt' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName,
                                                                fieldValueDescription.Attributes["ExactValueInt"].Value.Trim()));
                                                        }

                                                        exactValueIntNull = exactValueInt;
                                                    }

                                                    #endregion

                                                    #region DBStreamFieldValueRange

                                                    #region RangeValueMin

                                                    string rangeValueMinNull = null;

                                                    if (fieldValueDescription.Attributes["RangeValueMin"] != null)
                                                    {
                                                        rangeValueMinNull = fieldValueDescription.Attributes["RangeValueMin"].Value.Trim();
                                                    }

                                                    #endregion

                                                    #region RangeValueMax

                                                    string rangeValueMaxNull = null;

                                                    if (fieldValueDescription.Attributes["RangeValueMax"] != null)
                                                    {
                                                        rangeValueMaxNull = fieldValueDescription.Attributes["RangeValueMax"].Value.Trim();
                                                    }

                                                    #endregion

                                                    DBStreamFieldValueRange dbStreamFieldValueRange = null;

                                                    if (!string.IsNullOrEmpty(rangeValueMinNull) && string.IsNullOrEmpty(rangeValueMaxNull))
                                                    {
                                                        try
                                                        {
                                                            dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMinNull);
                                                        }
                                                        catch (DeviceConfigurationHelperException ex)
                                                        {
                                                            throw new DeviceConfigurationHelperException(
                                                                string.Format(CultureInfo.InvariantCulture,
                                                                    "Il nodo 'FieldValueDescription' nel file {0}, non è valido.",
                                                                    this.DefinitionFileName), ex);
                                                        }
                                                    }
                                                    else if (string.IsNullOrEmpty(rangeValueMinNull) && !string.IsNullOrEmpty(rangeValueMaxNull))
                                                    {
                                                        try
                                                        {
                                                            dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMaxNull);
                                                        }
                                                        catch (DeviceConfigurationHelperException ex)
                                                        {
                                                            throw new DeviceConfigurationHelperException(
                                                                string.Format(CultureInfo.InvariantCulture,
                                                                    "Il nodo 'FieldValueDescription' nel file {0}, non è valido.",
                                                                    this.DefinitionFileName), ex);
                                                        }
                                                    }
                                                    else if (!string.IsNullOrEmpty(rangeValueMinNull) && !string.IsNullOrEmpty(rangeValueMaxNull))
                                                    {
                                                        try
                                                        {
                                                            dbStreamFieldValueRange = new DBStreamFieldValueRange(rangeValueMinNull, rangeValueMaxNull);
                                                        }
                                                        catch (DeviceConfigurationHelperException ex)
                                                        {
                                                            throw new DeviceConfigurationHelperException(
                                                                string.Format(CultureInfo.InvariantCulture,
                                                                    "Il nodo 'FieldValueDescription' nel file {0}, non è valido.",
                                                                    this.DefinitionFileName), ex);
                                                        }
                                                    }

                                                    #endregion

                                                    #region RegexValueString

                                                    Regex regexValueString = null;

                                                    if (fieldValueDescription.Attributes["RegexValueString"] != null)
                                                    {
                                                        try
                                                        {
                                                            regexValueString = new Regex(fieldValueDescription.Attributes["RegexValueString"].Value,
                                                                RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                                                        }
                                                        catch (ArgumentNullException)
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'RegexValueString' del nodo 'FieldValueDescription' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName,
                                                                fieldValueDescription.Attributes["RegexValueString"].Value.Trim()));
                                                        }
                                                        catch (ArgumentException)
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'RegexValueString' del nodo 'FieldValueDescription' nel file {0}, non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                                this.DefinitionFileName,
                                                                fieldValueDescription.Attributes["RegexValueString"].Value.Trim()));
                                                        }
                                                    }

                                                    #endregion

                                                    #region IsDefault

                                                    bool isDefault = false;

                                                    if (fieldValueDescription.Attributes["IsDefault"] != null)
                                                    {
                                                        string isDefaultString =
                                                            fieldValueDescription.Attributes["IsDefault"].Value.Trim().ToLowerInvariant();
                                                        if (!Boolean.TryParse(isDefaultString, out isDefault))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'IsDefault' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
                                                                this.DefinitionFileName, fieldValueDescription.Attributes["IsDefault"].Value.Trim()));
                                                        }
                                                    }

                                                    #endregion

                                                    #region ShouldSendNotificationByEmail

                                                    bool shouldSendNotificationByEmail = false;

                                                    if (fieldValueDescription.Attributes["ShouldSendNotificationByEmail"] != null)
                                                    {
                                                        string shouldSendNotificationByEmailString =
                                                            fieldValueDescription.Attributes["ShouldSendNotificationByEmail"].Value.Trim()
                                                                .ToLowerInvariant();
                                                        if (!Boolean.TryParse(shouldSendNotificationByEmailString, out shouldSendNotificationByEmail))
                                                        {
                                                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                "L'attributo 'ShouldSendNotificationByEmail' del nodo 'FieldValueDescription' nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
                                                                this.DefinitionFileName,
                                                                fieldValueDescription.Attributes["ShouldSendNotificationByEmail"].Value.Trim()));
                                                        }
                                                    }

                                                    #endregion
                                                    strField.DbStreamFieldValueDescriptions.Add(new DBStreamFieldValueDescription(strField, isDefault,
                                                        fieldValueDescription.Attributes["ValueDescription"].Value.Trim(), (Severity) severity,
                                                        exactValueString, exactValueIntNull, dbStreamFieldValueRange, regexValueString,
                                                        shouldSendNotificationByEmail));

                                                    streamFieldValueDescriptionFound = true;
                                                }
                                            }

                                            if (!streamFieldValueDescriptionFound)
                                            {
                                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno una descrizione di decodifica dei valori dello stream field '{1}'.",
                                                    this.DefinitionFileName, streamField.Attributes["Name"].Value.Trim()));
                                            }

                                            #endregion

                                            // aggiunge il figlio ad un padre
                                            if (stream_field_father_id != -1)
                                            {
                                                if (streamFieldsFathers.ContainsKey(stream_field_father_id))
                                                {
                                                    streamFieldsFathers[stream_field_father_id].DbStreamFieldValueChildren.Add(strField);
                                                    strField.StreamFieldFather = streamFieldsFathers[stream_field_father_id];
                                                }
                                                else {
                                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                    "Impossibile caricare il file {0}, con la definizione XML della periferica. Il padre, indicato dallo 'streamField' con id {1}, non esiste.",
                                                    this.DefinitionFileName, streamField.Attributes["ID"].Value.Trim()));
                                                }
                                            }

                                            strField.RowIdZeroBased = stream_field_is_stream_field_row_id_zero_based;
                                            strField.UseToHeadOfStreamFieldArray = stream_field_use_to_head_of_stream_field_array;
                                            // indica il numero massimo di elementi dell'array
                                            strField.MaxSizeArray = stream_field_max_size;
                                            strField.ExpressionFormatShowRowId = show_row_id_expression;
                                            strField.EvaluationFormula = scalar_evaluation_formula;

                                            str.FieldData.Add(strField);

                                            if (stream_field_is_father)
                                            {
                                                strField.setIsFather(true);
                                                streamFieldsFathers.Add(fieldId, strField);

                                            }

                                            strField.IsShowName = stream_field_is_show_name;
                                            strField.IsAggregation = stream_field_is_aggregation;
                                            if (strField.IsAggregation && (strField.StreamFieldFather.IsArray || strField.StreamFieldFather.IsStreamFieldArray))
                                            {
                                                strField.StreamFieldFather.PositionArray++;
                                            }

                                            strField.IsArray = stream_field_is_array;
                                            strField.IsStreamFieldArray = stream_field_is_stream_field_array;
                                            strField.IsShowValueDescription = stream_field_is_show_value_description;
                                            strField.IsTimeTicksMs = stream_field_is_time_ticks_ms;

                                            if (strField.IsStreamFieldArray == true && strField.MaxSizeArray <= 0) {
                                                strField.MaxSizeArray = (int)GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.StreamFieldArraySizeDefault;
                                            }
                                        }

                                        streamFieldFound = true;
                                    }
                                }

                                if (!streamFieldFound)
                                {
                                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno uno stream field definito per lo stream '{1}' che sia relativo al tipo della periferica corrente.",
                                        this.DefinitionFileName, stream.Attributes["Name"].Value.Trim()));
                                }

                                #endregion

                                if ((str != null) && (str.FieldData != null) && (str.FieldData.Count > 0))
                                {
                                    // Lo Stream è aggiunto solo se contiene almeno uno Stream Field
                                    // Può succedere che uno Stream sia vuoto, pur se configurato nella definizione
                                    // se stiamo filtrando per categoria della periferica e tutti gli Stream Field riguardano
                                    // una categoria diversa da quella della periferica corrente
                                    streamData.Add(str);
                                }
                            }

                            streamFound = true;
                        }

                        if (!streamFound)
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare almeno uno Stream definito.",
                                this.DefinitionFileName));
                        }

                        #endregion

                        streamsFound = true;
                        break;
                    }
                }

                if (!streamsFound)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Impossibile trovare il nodo 'Streams' al primo livello.",
                        this.DefinitionFileName));
                }

                #endregion
            }
        }

        #endregion

        #region Metodi privati

        private XmlNode GetDefinitionXmlRootElement()
        {
            if (string.IsNullOrEmpty(this.DefinitionFileName))
            {
                throw new DeviceConfigurationHelperException("Nome del file con la definizione XML della periferica non definito");
            }

            if (!FileUtility.CheckFileCanRead(this.DefinitionFileName))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile leggere la definizione XML della periferica: {0}", this.DefinitionFileName));
            }

            XmlDocument doc = new XmlDocument();
            XmlNode root;

            try
            {
                doc.Load(this.DefinitionFileName);
                root = doc.DocumentElement;
            }
            catch (XmlException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                        this.DefinitionFileName), ex);
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                        this.DefinitionFileName), ex);
            }
            catch (IOException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                        this.DefinitionFileName), ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                        this.DefinitionFileName), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                        this.DefinitionFileName), ex);
            }
            catch (SecurityException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Configurazione non valida nel file {0}, con la definizione XML della periferica",
                        this.DefinitionFileName), ex);
            }

            if (root == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Configurazione non valida nel file {0}, con la definizione XML della periferica", this.DefinitionFileName));
            }

            if (!root.Name.Equals("Device", StringComparison.OrdinalIgnoreCase))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile trovare l'elemento 'Device' come nodo radice del file di configurazione {0}.", this.DefinitionFileName));
            }

            return root;
        }

        /// <summary>
        ///     Ritorna il produttore di una periferica dato l'ObjectIdentifier per recuperarlo via SNMP
        /// </summary>
        /// <param name="oid">ObjectIdentifier per recuperare il produttore della periferica</param>
        /// <returns>La stringa contenente la descrizione del produttore</returns>
        private string GetManufacturerFromDevice(ObjectIdentifier oid)
        {
            return this.GetSnmpScalarDataValue(oid);
        }

        /// <summary>
        ///     Ritorna la categoria di una periferica dato l'ObjectIdentifier per recuperarla via SNMP
        /// </summary>
        /// <param name="oid">ObjectIdentifier per recuperare la categoria della periferica</param>
        /// <param name="returnNotAvailable">Ritorna il valore -2 (non disponibile) se true, -1 (non definito) se false</param>
        /// <returns>Un intero che indica la categoria della periferica, dato un enumerato specificato nella MIB.</returns>
        private int GetCategoryFromDevice(ObjectIdentifier oid, bool returnNotAvailable)
        {
            string categoryString = this.GetSnmpScalarDataValue(oid);
            int category;

            if (string.IsNullOrEmpty(categoryString))
            {
                if (returnNotAvailable)
                {
                    return (int) SpecialDeviceCategory.NotAvailable;
                }

                return (int) SpecialDeviceCategory.Undefined;
            }

            if (int.TryParse(categoryString, out category))
            {
                return category;
            }

            if (returnNotAvailable)
            {
                return (int) SpecialDeviceCategory.NotAvailable;
            }

            return (int) SpecialDeviceCategory.Undefined;
        }

        /// <summary>
        ///     Esegue una query SNMP verso la periferica corrente, ritornando il valore di ritorno nel formato stringa
        /// </summary>
        /// <param name="oid">ObjectIdentifier per recuperare il valore</param>
        /// <returns>
        ///     La stringa contenente il valore scalare recuperato. null in caso non ci siano dati o la periferica non abbia
        ///     risposto
        /// </returns>
        private string GetSnmpScalarDataValue(ObjectIdentifier oid)
        {
            string returnValue = null;

            IList<Variable> values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.ipEndPoint,
                new OctetString(this.SnmpCommunity), new List<Variable> {new Variable(oid)});

            if ((values != null) && (values.Count > 0))
            {
                returnValue = SnmpUtility.DecodeDataByType(values[0]);
            }

            return returnValue;
        }

        /// <summary>
        ///     Esegue una query SNMP verso la periferica corrente, ritornando il valore di ritorno di tipo SNMP
        /// </summary>
        /// <param name="oid">ObjectIdentifier per recuperare il valore</param>
        /// <returns>
        ///     Il dato SNMP contenente il valore scalare recuperato. null in caso non ci siano dati o la periferica non abbia
        ///     risposto
        /// </returns>
        private Variable GetSnmpScalarDataValueVariable(ObjectIdentifier oid)
        {
            Variable returnValue = null;

            IList<Variable> values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.ipEndPoint,
                new OctetString(this.SnmpCommunity), new List<Variable> {new Variable(oid)});

            if ((values != null) && (values.Count > 0))
            {
                returnValue = values[0];
            }

            return returnValue;
        }

        #endregion
    }
}
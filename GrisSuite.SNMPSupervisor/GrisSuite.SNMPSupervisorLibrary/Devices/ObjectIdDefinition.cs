﻿using System.Text.RegularExpressions;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Indica l'associazione tra un Object ID di periferica ed un file di definizione
    /// </summary>
    public class ObjectIdDefinition
    {
        /// <summary>
        ///   Espressione regolare per identificare l'Object ID della periferica
        /// </summary>
        public Regex Description { get; private set; }

        /// <summary>
        ///   Nome del file con la definizione relativa
        /// </summary>
        public string DefinitionFile { get; private set; }

        /// <summary>
        /// Nome di periferica da forzare, nel caso si usi questa regola
        /// </summary>
        public string ForcedDeviceType { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "description">Espressione regolare per identificare l'Object ID della periferica</param>
        /// <param name = "definitionFile">Nome del file con la definizione relativa</param>
        /// <param name="forcedDeviceType">Nome di periferica da forzare, nel caso si usi questa regola</param>
        public ObjectIdDefinition(Regex description, string definitionFile, string forcedDeviceType)
        {
            this.Description = description;
            this.DefinitionFile = definitionFile;
            this.ForcedDeviceType = forcedDeviceType;
        }
    }
}
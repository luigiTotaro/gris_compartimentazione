using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    ///<summary>
    ///  Interfaccia per una periferica SNMP
    ///</summary>
    public interface ISnmpDevice : IDevice
    {
        /// <summary>
        ///   Community string per l'accesso in lettura
        /// </summary>
        string SnmpCommunity { get; }

        /// <summary>
        ///   Versione del protocollo SNMP per le richieste
        /// </summary>
        VersionCode SnmpVersionCode { get; }

        /// <summary>
        ///   Indica se la periferica � raggiungibile via SNMP
        /// </summary>
        bool IsAliveSnmp { get; }
    }
}
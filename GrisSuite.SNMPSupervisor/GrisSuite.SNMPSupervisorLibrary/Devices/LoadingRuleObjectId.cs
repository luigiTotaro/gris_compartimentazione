﻿using System.Collections.ObjectModel;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Indica le regole di caricamento dinamiche per Object ID della periferica
    /// </summary>
    public class LoadingRuleOjectId
    {
        /// <summary>
        ///   Oid da testare per valutare l'Object ID della periferica
        /// </summary>
        public ObjectIdentifier ProbingObjectIdOid { get; private set; }

        /// <summary>
        ///   Lista di associazioni tra Object IDs e file di definizione
        /// </summary>
        public ReadOnlyCollection<ObjectIdDefinition> ObjectIdDefinitions { get; private set; }

        /// <summary>
        ///   Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico
        /// </summary>
        public NoDynamicRuleMatchDeviceStatus NoMatchDeviceStatus { get; private set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="probingObjectIdOid">Oid da testare per valutare l'Object ID della periferica</param>
        /// <param name="objectIdDefinitions">Lista di associazioni tra Object IDs e file di definizione</param>
        /// <param name="noDynamicRuleMatchDeviceStatus">Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico</param>
        public LoadingRuleOjectId(ObjectIdentifier probingObjectIdOid, ReadOnlyCollection<ObjectIdDefinition> objectIdDefinitions,
                                  NoDynamicRuleMatchDeviceStatus noDynamicRuleMatchDeviceStatus)
        {
            this.ProbingObjectIdOid = probingObjectIdOid;
            this.ObjectIdDefinitions = objectIdDefinitions;
            this.NoMatchDeviceStatus = noDynamicRuleMatchDeviceStatus;
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    public class MessengerFactory
    {
        public MessengerFactory() {
            this.IsDataRetrieved = true;
        }

        public bool IsDataRetrieved { set; get; }

        /// <summary>
        ///   Ritorna una serie di risultati data una query SNMP
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "variables">Lista variabili da recuperare</param>
        /// <returns>Lista di variabili SNMP contenenti i valori di ritorno</returns>
        public IList<Variable> GetSnmpData(bool fakeLocalData, VersionCode version, IPEndPoint endpoint, OctetString community,
                                           IList<Variable> variables)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return SnmpDataRetriever.Get(version, endpoint, community, variables);
                    }

                    return FakeDataRetriever.Get(version, endpoint, community, variables);
                }

                IList<Variable> value = SnmpDataRetriever.Get(version, endpoint, community, variables);

                if (!(value == null || value.Count == 0 || value[0].Data is NoSuchObject))
                {
                    this.IsDataRetrieved = true;
                }

                return value;
            }

            return null;
        }

        /// <summary>
        ///   Ritorna un risultato tabellare data una query SNMP
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "oid">ObjectIdentifier per la query SNMP (nel caso non si trattasse di un Oid che restituisce una tabella, non ci sono risultati)</param>
        /// <returns>Vettore bidimensionale con lista di variabili SNMP contenenti i valori di ritorno</returns>
        public Variable[,] GetSnmpTable(bool fakeLocalData, VersionCode version, IPEndPoint endpoint, OctetString community, ObjectIdentifier oid)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return SnmpDataRetriever.GetTable(version, endpoint, community, oid);
                    }

                    return FakeDataRetriever.GetTable(version, endpoint, community, oid);
                }

                Variable[,] value = SnmpDataRetriever.GetTable(version, endpoint, community, oid);

                if (!(value == null || (value.Length == 0 && value.GetLength(0) == 0)))
                {
                    this.IsDataRetrieved = true;
                }

                return value;
            }

            return null;
        }

        /// <summary>
        ///   Ritorna un risultato tabellare data una query SNMP (su tabella sparsa)
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "oid">ObjectIdentifier per la query SNMP (nel caso non si trattasse di un Oid che restituisce una tabella, non ci sono risultati)</param>
        /// <param name="tableColumnsCount">Numero di colonne della tabella</param>
        /// <param name="tableEntryOid">Oid dell'entry della tabella (normalmente 1)</param>
        /// <param name="tableIndexColumns">Lista riferimenti delle colonne indice della tabella, ad esempio 1 e 3 indicano che le colonne 1 e 3 della tabella contengono l'indice composito</param>
        /// <returns>Vettore bidimensionale con lista di variabili SNMP contenenti i valori di ritorno. Se la tabella è sparsa, ogni cella priva di valore conterrà NoSuchInstance</returns>
        public Variable[,] GetTableUsingMIBData(bool fakeLocalData, VersionCode version, IPEndPoint endpoint, OctetString community,
                                                ObjectIdentifier oid, int tableColumnsCount, uint? tableEntryOid,
                                                ReadOnlyCollection<uint> tableIndexColumns)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return SnmpDataRetriever.GetTableUsingMIBData(version, endpoint, community, oid, tableColumnsCount, tableEntryOid,
                                                                      tableIndexColumns);
                    }

                    // A livello di fake file, non ci sono differenze tra una tabella sparsa o no (il generato di una tabella sparsa conterrà i dati NoSuchInstance)
                    // quindi usiamo il GetTable normale
                    return FakeDataRetriever.GetTable(version, endpoint, community, oid);
                }

                Variable[,] value = SnmpDataRetriever.GetTableUsingMIBData(version, endpoint, community, oid, tableColumnsCount, tableEntryOid, tableIndexColumns);

                if (!(value == null || (value.Length == 0 && value.GetLength(0) == 0)))
                {
                    this.IsDataRetrieved = true;
                }

                return value;
            }

            return null;
        }

        /// <summary>
        ///   Ottiene una risposta alla richiesta Ping ICMP
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "endpoint">Endpoint IP</param>
        /// <returns>True se l'endpoint risponde a ICMP, false altrimenti</returns>
        public bool GetPingResponse(bool fakeLocalData, IPEndPoint endpoint)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return IcmpDataRetriever.GetPingResponse(endpoint);
                    }

                    return FakeDataRetriever.GetPingResponse(endpoint);
                }

                return IcmpDataRetriever.GetPingResponse(endpoint);
            }

            return false;
        }

        /// <summary>
        ///   Ottiene lo stato SNMP della periferica
        /// </summary>
        /// <param name = "fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name = "version">Versione del protocollo SNMP</param>
        /// <param name = "endpoint">Endpoint</param>
        /// <param name = "community">Community string</param>
        /// <param name = "variables">Lista variabili da recuperare</param>
        /// <returns>True se l'endpoint risponde a SNMP, false altrimenti</returns>
        public IList<Variable> GetAliveSnmp(bool fakeLocalData, VersionCode version, IPEndPoint endpoint, OctetString community,
                                            IList<Variable> variables)
        {
            if (endpoint != null)
            {
                if (fakeLocalData)
                {
                    if (endpoint.Address.Equals(IPAddress.Loopback))
                    {
                        return SnmpDataRetriever.Get(version, endpoint, community, variables);
                    }

                    return FakeDataRetriever.GetAliveSnmp(version, endpoint, community, variables);
                }

                return SnmpDataRetriever.Get(version, endpoint, community, variables);
            }

            return null;
        }
    }
}
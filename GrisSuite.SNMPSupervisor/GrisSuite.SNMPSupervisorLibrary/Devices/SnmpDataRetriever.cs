﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;
using Lextm.SharpSnmpLib.Messaging;
using Lextm.SharpSnmpLib.Mib;
using TimeoutException = Lextm.SharpSnmpLib.Messaging.TimeoutException;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
	public static class SnmpDataRetriever
	{
		/// <summary>
		///     Ritorna una serie di risultati data una query SNMP
		/// </summary>
		/// <param name="version">Versione del protocollo SNMP</param>
		/// <param name="endpoint">Endpoint</param>
		/// <param name="community">Community string</param>
		/// <param name="variables">Lista variabili da recuperare</param>
		/// <returns>Lista di variabili SNMP contenenti i valori di ritorno</returns>
		public static IList<Variable> Get(VersionCode version, IPEndPoint endpoint, OctetString community, IList<Variable> variables)
		{
			if ((endpoint != null) && (variables != null))
			{
				DateTime startRequest = DateTime.Now;

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					foreach (Variable variable in variables)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
							string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Inizio richiesta SNMP scalare, IP: {1}, community: {2}, Oid: {3}",
								Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id));
					}
				}

				IList<Variable> data = null;

				ushort retryCounter = 1;

				while (retryCounter <= Settings.Default.SnmpMaxRetriesCount)
				{
					try
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							foreach (Variable variable in variables)
							{
								FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
									string.Format(CultureInfo.InvariantCulture,
										"Thread: {0} - Richiesta SNMP scalare, IP: {1}, community: {2}, Oid: {3}, Tentativo: {4}, Timeout impostato: {5}",
										Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id, retryCounter,
										retryCounter*Settings.Default.SnmpTimeoutMilliseconds));
							}
						}

						data = Messenger.Get(version, endpoint, community, variables, retryCounter*Settings.Default.SnmpTimeoutMilliseconds);
						break;
					}
					catch (TimeoutException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							foreach (Variable variable in variables)
							{
								FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
									string.Format(CultureInfo.InvariantCulture,
										"Thread: {0} - Eccezione timeout richiesta SNMP scalare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Timeout: {5}, Dettagli: {6}",
										Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id, ex.Message, ex.Timeout, ex.Details));
							}
						}

						Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenSnmpRetries);
						retryCounter++;
					}
					catch (SnmpException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							foreach (Variable variable in variables)
							{
								FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
									string.Format(CultureInfo.InvariantCulture,
										"Thread: {0} - Eccezione richiesta SNMP scalare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Dettagli: {5}",
										Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id, ex.Message, ex.Details));
							}
						}

						break;
					}
					catch (SocketException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							foreach (Variable variable in variables)
							{
								FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
									string.Format(CultureInfo.InvariantCulture,
										"Thread: {0} - Eccezione socket richiesta SNMP scalare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Codice errore: {5}",
										Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id, ex.Message, ex.ErrorCode));
							}
						}

						break;
					}
					catch (ArgumentException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							foreach (Variable variable in variables)
							{
								FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
									string.Format(CultureInfo.InvariantCulture,
										"Thread: {0} - Eccezione socket richiesta SNMP scalare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Nome parametro: {5}",
										Thread.CurrentThread.GetHashCode(), endpoint.Address, community, variable.Id, ex.Message, ex.ParamName));
							}
						}

						break;
					}
				}

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
					foreach (Variable variable in variables)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
							string.Format(CultureInfo.InvariantCulture,
								"Thread: {0} - Fine richiesta SNMP scalare, IP: {1}, community: {2}, Oid: {3}, Durata richiesta: {4} (ms)", Thread.CurrentThread.GetHashCode(),
								endpoint.Address, community, variable.Id, requestDuration.TotalMilliseconds));
					}
				}

				return data;
			}

			return null;
		}

		/// <summary>
		///     Ritorna un risultato tabellare data una query SNMP
		/// </summary>
		/// <param name="version">Versione del protocollo SNMP</param>
		/// <param name="endpoint">Endpoint</param>
		/// <param name="community">Community string</param>
		/// <param name="oid">
		///     ObjectIdentifier per la query SNMP (nel caso non si trattasse di un Oid che restituisce una tabella,
		///     non ci sono risultati)
		/// </param>
		/// <returns>Vettore bidimensionale con lista di variabili SNMP contenenti i valori di ritorno</returns>
		public static Variable[,] GetTable(VersionCode version, IPEndPoint endpoint, OctetString community, ObjectIdentifier oid)
		{
			if (endpoint != null)
			{
				DateTime startRequest = DateTime.Now;

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Inizio richiesta SNMP tabellare, IP: {1}, community: {2}, Oid: {3}",
							Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid));
				}

				Variable[,] data = null;

				ushort retryCounter = 1;

				while (retryCounter <= Settings.Default.SnmpMaxRetriesCount)
				{
					try
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Richiesta SNMP tabellare, IP: {1}, community: {2}, Oid: {3}, Tentativo: {4}, Timeout impostato: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, retryCounter,
									retryCounter*Settings.Default.SnmpTableTimeoutMilliseconds));
						}

						data = Messenger.GetTable(version, endpoint, community, oid, retryCounter*Settings.Default.SnmpTableTimeoutMilliseconds,
							Settings.Default.SnmpTableMaxRepetitions, null);
						break;
					}
					catch (TimeoutException ex)
					{
						Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenSnmpRetries);
						retryCounter++;

						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione timeout richiesta SNMP tabellare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Timeout: {5}, Dettagli: {6}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.Timeout, ex.Details));
						}
					}
					catch (SnmpException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione richiesta SNMP tabellare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Dettagli: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.Details));
						}

						break;
					}
					catch (SocketException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione socket richiesta SNMP tabellare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Codice errore: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.ErrorCode));
						}

						break;
					}
					catch (ArgumentException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione socket richiesta SNMP tabellare, IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Nome parametro: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.ParamName));
						}

						break;
					}
				}

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						string.Format(CultureInfo.InvariantCulture,
							"Thread: {0} - Fine richiesta SNMP tabellare, IP: {1}, community: {2}, Oid: {3}, Durata richiesta: {4} (ms)",
							Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, requestDuration.TotalMilliseconds));
				}

				return data;
			}

			return null;
		}

		/// <summary>
		///     Ritorna un risultato tabellare data una query SNMP (su tabella sparsa)
		/// </summary>
		/// <param name="version">Versione del protocollo SNMP</param>
		/// <param name="endpoint">Endpoint</param>
		/// <param name="community">Community string</param>
		/// <param name="oid">
		///     ObjectIdentifier per la query SNMP (nel caso non si trattasse di un Oid che restituisce una tabella,
		///     non ci sono risultati)
		/// </param>
		/// <param name="tableColumnsCount">Numero di colonne della tabella</param>
		/// <param name="tableEntryOid">Oid dell'entry della tabella (normalmente 1)</param>
		/// <param name="tableIndexColumns">
		///     Lista riferimenti delle colonne indice della tabella, ad esempio 1 e 3 indicano che le
		///     colonne 1 e 3 della tabella contengono l'indice composito
		/// </param>
		/// <returns>
		///     Vettore bidimensionale con lista di variabili SNMP contenenti i valori di ritorno. Se la tabella è sparsa,
		///     ogni cella priva di valore conterrà NoSuchInstance
		/// </returns>
		public static Variable[,] GetTableUsingMIBData(VersionCode version,
			IPEndPoint endpoint,
			OctetString community,
			ObjectIdentifier oid,
			int tableColumnsCount,
			uint? tableEntryOid,
			ReadOnlyCollection<uint> tableIndexColumns)
		{
			if (endpoint != null)
			{
				DateTime startRequest = DateTime.Now;

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Inizio richiesta SNMP tabellare (sparsa), IP: {1}, community: {2}, Oid: {3}",
							Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid));
				}

				Variable[,] data = null;

				ushort retryCounter = 1;

				while (retryCounter <= Settings.Default.SnmpMaxRetriesCount)
				{
					try
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Richiesta SNMP tabellare (sparsa), IP: {1}, community: {2}, Oid: {3}, Tentativo: {4}, Timeout impostato: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, retryCounter,
									retryCounter*Settings.Default.SnmpTableTimeoutMilliseconds));
						}

						data = GetTableUsingMIBDataInternal(version, endpoint, community, oid, retryCounter*Settings.Default.SnmpTableTimeoutMilliseconds,
							Settings.Default.SnmpTableMaxRepetitions, null, tableColumnsCount, tableEntryOid, tableIndexColumns);
						break;
					}
					catch (TimeoutException ex)
					{
						Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenSnmpRetries);
						retryCounter++;

						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione timeout richiesta SNMP tabellare (sparsa), IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Timeout: {5}, Dettagli: {6}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.Timeout, ex.Details));
						}
					}
					catch (SnmpException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione richiesta SNMP tabellare (sparsa), IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Dettagli: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.Details));
						}

						break;
					}
					catch (SocketException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione socket richiesta SNMP tabellare (sparsa), IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Codice errore: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.ErrorCode));
						}

						break;
					}
					catch (ArgumentException ex)
					{
						if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
						{
							FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
								string.Format(CultureInfo.InvariantCulture,
									"Thread: {0} - Eccezione socket richiesta SNMP tabellare (sparsa), IP: {1}, community: {2}, Oid: {3}, Messaggio: {4}, Nome parametro: {5}",
									Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, ex.Message, ex.ParamName));
						}

						break;
					}
				}

				if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
				{
					TimeSpan requestDuration = DateTime.Now.Subtract(startRequest);
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						string.Format(CultureInfo.InvariantCulture,
							"Thread: {0} - Fine richiesta SNMP tabellare (sparsa), IP: {1}, community: {2}, Oid: {3}, Durata richiesta: {4} (ms)",
							Thread.CurrentThread.GetHashCode(), endpoint.Address, community, oid, requestDuration.TotalMilliseconds));
				}

				return data;
			}

			return null;
		}

		#region Recupero dati tabelle sparse

		/// <summary>
		///     Implementazione custom per recuperare una tabella SNMP basandosi su informazioni aggiuntive, recuperate dalla MIB.
		///     Utile per tabelle sparse.
		/// </summary>
		/// <param name="version">Versione SNMP</param>
		/// <param name="endpoint">EndPoint SNMP</param>
		/// <param name="community">Community string</param>
		/// <param name="table">Oid della tabella</param>
		/// <param name="timeout">Timeout delle richieste SNMP</param>
		/// <param name="maxRepetitions">Max repetitions da passare all'agente</param>
		/// <param name="registry">Registro con la MIB caricata</param>
		/// <param name="tableColumnsCount">Numero di colonne della tabella</param>
		/// <param name="tableEntryOid">Oid dell'entry della tabella (normalmente 1)</param>
		/// <param name="tableIndexColumns">
		///     Lista riferimenti delle colonne indice della tabella, ad esempio 1 e 3 indicano che le
		///     colonne 1 e 3 della tabella contengono l'indice composito
		/// </param>
		/// <returns>La tabella popolata. Se la tabella è sparsa, ogni cella priva di valore conterrà NoSuchInstance</returns>
		private static Variable[,] GetTableUsingMIBDataInternal(VersionCode version,
			IPEndPoint endpoint,
			OctetString community,
			ObjectIdentifier table,
			int timeout,
			int maxRepetitions,
			IObjectRegistry registry,
			int tableColumnsCount,
			uint? tableEntryOid,
			ReadOnlyCollection<uint> tableIndexColumns)
		{
			if (version == VersionCode.V3)
			{
				throw new NotSupportedException("SNMP v3 is not supported");
			}

			bool canContinue = registry == null || registry.ValidateTable(table);
			if (!canContinue)
			{
				throw new ArgumentException("not a table OID: " + table);
			}

			if (tableColumnsCount <= 0)
			{
				throw new ArgumentException("Table columns count not valid");
			}

			if ((tableIndexColumns == null) || (tableIndexColumns.Count == 0))
			{
				throw new ArgumentException("List of table index columns not valid");
			}

			if (tableEntryOid <= 0)
			{
				throw new ArgumentException("Table entry Oid not valid");
			}

			ObjectIdentifier tableEntry =
				new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", table, (tableEntryOid.HasValue ? tableEntryOid : 1)));

			int tableIndexRowCounter = -1;
			IList<IList<Variable>> listIndexes = new List<IList<Variable>>();
			IList<IList<Variable>> listColumns = new List<IList<Variable>>();
			IList<Variable> columnResult;

			#region Recupera direttamente le colonne con gli indici usando Walk/BulkWalk

			// La richiesta di OID indica come colonna indice la 0, che è valore non valido, quindi, applichiamo algoritmo particolare
			if ((tableIndexColumns.Count == 1) && (tableIndexColumns[0] == 0))
			{
				// Algoritmo per tabelle particolari, senza indice di colonna (o marcato come non-accessibile come OID SNMP)

				// Usiamo una colonna arbitraria SNMP, che deve essere almeno in sola-lettura e ritornare una lista di righe. Serve solo per il conteggio delle righe.
				const int MUST_EXIST_COLUMN_INDEX = 2;
				columnResult = new List<Variable>();

				if (version == VersionCode.V1)
				{
					Messenger.Walk(version, endpoint, community,
						new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableEntry, MUST_EXIST_COLUMN_INDEX)), columnResult, timeout,
						WalkMode.WithinSubtree);
				}
				else
				{
					Messenger.BulkWalk(version, endpoint, community,
						new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableEntry, MUST_EXIST_COLUMN_INDEX)), columnResult, timeout,
						maxRepetitions, WalkMode.WithinSubtree);
				}

				int indexRowCounter = columnResult.Count;

				if (indexRowCounter == 0)
				{
					// Non ci sono righe ritornate usando la colonna MUST_EXIST_COLUMN_INDEX. Implementare un algoritmo differente (prova colonna successiva...), se necessario.
					// Al momento, ritorniamo una tabella vuota.
					return new Variable[0, 0];
				}

				tableIndexRowCounter = indexRowCounter;

				// Il risultato è popolato, ma dobbiamo costruire dati di indice nuovi, quindi scartiamo i dati già esistenti.
				columnResult.Clear();

				// Crea una lista di risultati per l'indice, con il numero corretto di righe e una serie di valori interi positivi, che iniziano da 1 (indice standard)
				for (int rowCounter = 1; rowCounter <= tableIndexRowCounter; rowCounter++)
				{
					columnResult.Add(new Variable(new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}", tableEntry, 1, rowCounter)),
						new Integer32(rowCounter)));
				}

				listIndexes.Add(columnResult);
			}
			else
			{
				// Algoritmo per le tabelle classiche, con indice
				foreach (uint indexColumn in tableIndexColumns)
				{
					columnResult = new List<Variable>();

					if (version == VersionCode.V1)
					{
						Messenger.Walk(version, endpoint, community,
							new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableEntry, indexColumn)), columnResult, timeout,
							WalkMode.WithinSubtree);
					}
					else
					{
						Messenger.BulkWalk(version, endpoint, community,
							new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableEntry, indexColumn)), columnResult, timeout, maxRepetitions,
							WalkMode.WithinSubtree);
					}

					int indexRowCounter = columnResult.Count;

					if (tableIndexRowCounter < 0)
					{
						tableIndexRowCounter = indexRowCounter;
					}

					if (indexRowCounter != tableIndexRowCounter)
					{
						// Il numero di righe delle colonne indice è diverso, quindi ci sono celle mancanti sugli indici
						// Situazione non supportata, restituiamo una tabella vuota
						return new Variable[0, 0];
					}

					tableIndexRowCounter = indexRowCounter;

					listIndexes.Add(columnResult);
				}
			}

			if (tableIndexRowCounter <= 0)
			{
				return new Variable[0, 0];
			}

			#endregion

			#region Recupera direttamente le altre colonne della tabella, usando Walk/BulkWalk

			for (uint indexColumn = 1; indexColumn <= tableColumnsCount; indexColumn++)
			{
				if (tableIndexColumns.Contains(indexColumn))
				{
					// La colonna è già stata recuperata, perché è un indice, la saltiamo
					continue;
				}

				columnResult = new List<Variable>();

				if (version == VersionCode.V1)
				{
					Messenger.Walk(version, endpoint, community,
						new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableEntry, indexColumn)), columnResult, timeout,
						WalkMode.WithinSubtree);
				}
				else
				{
					Messenger.BulkWalk(version, endpoint, community,
						new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}", tableEntry, indexColumn)), columnResult, timeout, maxRepetitions,
						WalkMode.WithinSubtree);
				}

				listColumns.Add(columnResult);
			}

			#endregion

			// A questo punto abbiamo:
			// tableIndexRowCounter = numero di righe nelle colonne indice (omogeneo) - saranno il numero di righe della tabella restituita
			// tableColumnsCount = numero di colonne della tabella restituita (passato al metodo, dalla MIB)

			Variable[,] result = new Variable[tableIndexRowCounter, tableColumnsCount];

			// Costruisce l'indice della tabella, aggregando le varie colonne indice (obbligatorio per le tabelle con indice composito come la ipNetToMediaTable)
			string[] indexes = new string[tableIndexRowCounter];

			foreach (IList<Variable> index in listIndexes)
			{
				for (int rowCounter = 0; rowCounter < tableIndexRowCounter; rowCounter++)
				{
					indexes[rowCounter] += string.Format(CultureInfo.InvariantCulture, ".{0}", GetIndexRepresentationFromVariable(index[rowCounter]));
				}
			}

			// Popola la tabella dei risultati con dei valori vuoti
			for (int rowCounter = 0; rowCounter < tableIndexRowCounter; rowCounter++)
			{
				for (int columnCounter = 0; columnCounter < tableColumnsCount; columnCounter++)
				{
					result[rowCounter, columnCounter] =
						new Variable(
							new ObjectIdentifier(string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}", tableEntry, columnCounter + 1, indexes[rowCounter])),
							new NoSuchInstance());
				}
			}

			// Riempie la tabella dei risultati usando i valori letti nelle colonne indice
			foreach (IList<Variable> index in listIndexes)
			{
				foreach (Variable indexVariable in index)
				{
					SetResultTableDataSingleCell(result, indexVariable);
				}
			}

			// Riempie la tabella dei risultati usando i valori letti nelle altre colonne
			foreach (IList<Variable> column in listColumns)
			{
				foreach (Variable variable in column)
				{
					SetResultTableDataSingleCell(result, variable);
				}
			}

			return result;
		}

		/// <summary>
		///     Ottiene una stringa da usare per l'indice di tabella
		/// </summary>
		/// <param name="variable">Variabile SNMP</param>
		/// <returns>Rappresentazione stringa dei dati contenuti</returns>
		private static string GetIndexRepresentationFromVariable(Variable variable)
		{
			// La rappresentazione della cella serve per costruire la chiave univoca della tabella.
			// Il meccanismo si basa sul tipo dati SNMP restituito. Per i tipi standard usati negli indici (INTEGER, INTEGER32, IPADDRESS)
			// l'implementazione sotto è sufficiente. Da valutare nel caso di tipi per indici complessi.
			// Il valore da usare nell'indice di tabella è lo stesso generato dal MIB Browser iReasoning, nella colonna blu
			switch (variable.Data.TypeCode)
			{
				default:
					return variable.Data.ToString();
			}
		}

		// Set empty result table cell using data retrieved from Walk

		/// <summary>
		///     Imposta i dati sulla tabella vuota, in base agli indici di cella forniti, recuperati dal Walk
		/// </summary>
		/// <param name="result">Tabella SNMP vuota</param>
		/// <param name="cell">Cella che contiene i dati da copiare nella tabella</param>
		private static void SetResultTableDataSingleCell(Variable[,] result, Variable cell)
		{
			for (int resultRow = 0; resultRow <= result.GetUpperBound(0); resultRow++)
			{
				for (int resultColumn = 0; resultColumn <= result.GetUpperBound(1); resultColumn++)
				{
					if (result[resultRow, resultColumn].Id == cell.Id)
					{
						result[resultRow, resultColumn] = cell;
						return;
					}
				}
			}
		}

		#endregion
	}
}
﻿using System.Collections.ObjectModel;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Indica le regole di caricamento dinamiche per singola query e relativa valutazione della risposta
    /// </summary>
    public class LoadingRuleQueryEvaluate
    {
        /// <summary>
        ///   Lista di associazioni tra valutazione di risposta SNMP e file di definizione
        /// </summary>
        public ReadOnlyCollection<QueryEvaluateRuleDefinition> QueryEvaluateRuleDefinitions { get; private set; }

        /// <summary>
        ///   Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico
        /// </summary>
        public NoDynamicRuleMatchDeviceStatus NoMatchDeviceStatus { get; private set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="queryEvaluateRuleDefinitions">Lista di associazioni tra valutazione di risposta SNMP e file di definizione</param>
        /// <param name="noDynamicRuleMatchDeviceStatus">Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di caricamento dinamico</param>
        public LoadingRuleQueryEvaluate(ReadOnlyCollection<QueryEvaluateRuleDefinition> queryEvaluateRuleDefinitions,
                                        NoDynamicRuleMatchDeviceStatus noDynamicRuleMatchDeviceStatus)
        {
            this.QueryEvaluateRuleDefinitions = queryEvaluateRuleDefinitions;
            this.NoMatchDeviceStatus = noDynamicRuleMatchDeviceStatus;
        }
    }
}
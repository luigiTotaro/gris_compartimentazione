﻿using System;
using System.CodeDom.Compiler;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using Microsoft.CSharp;

// ReSharper disable RedundantNameQualifier

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    internal static class SeverityCalculators
    {
        private const string DYNAMIC_ASSEMBLY_FILENAME_PREFIX = "DynamicSeverityEvaluation";
        private const string DYNAMIC_ASSEMBLY_FOLDER = "DynamicAssemblies";
        private static readonly object locker = new object();

        internal static GrisSuite.SnmpSupervisorLibrary.Database.Severity CalculateSnmpDeviceSeverity(SnmpDeviceBase device, string evaluationFormula)
        {
            const string objectSubject = "della severità";

            if (!string.IsNullOrEmpty(evaluationFormula))
            {
                // CODICE DA COMMENTARE PER TESTARE DEL CODICE DINAMICO C#, CHE IN SEGUITO SARA' MESSO NELLA DEFINIZIONE
                //if (device.Device.DeviceId != 1409818792165377)
                //{
                    object returnValue = CalculateDeviceObjectDynamic(device, GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default,
                        evaluationFormula, objectSubject);

                    GrisSuite.SnmpSupervisorLibrary.Database.Severity severity;

                    try
                    {
                        severity =
                            (GrisSuite.SnmpSupervisorLibrary.Database.Severity)
                                Enum.Parse(typeof(GrisSuite.SnmpSupervisorLibrary.Database.Severity), returnValue.ToString(), true);
                    }
                    catch (ArgumentException)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile ottenere il tipo dato di ritorno previsto, nel codice con la formula di calcolo dinamica {0}", objectSubject));
                    }

                    return severity;
                //}
                //else return testDynamicCode(device);
            }

            return CalculateSnmpDeviceDefaultSeverity(device);
        }

        internal static string CalculateSnmpDeviceStatusMessage(SnmpDeviceBase device, string evaluationFormula)
        {
            const string objectSubject = "del messaggio";

            if (!string.IsNullOrEmpty(evaluationFormula))
            {
                object returnValue = CalculateDeviceObjectDynamic(device, GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default,
                    evaluationFormula, objectSubject);

                string message;

                if (returnValue is String)
                {
                    message = (String) returnValue;
                }
                else
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile ottenere il tipo dato di ritorno previsto, nel codice con la formula di calcolo dinamica {0}", objectSubject));
                }

                return message;
            }

            return CalculateSnmpDeviceDefaultStatusMessage(device);
        }

        internal static GrisSuite.SnmpSupervisorLibrary.Database.Severity CalculateIcmpDeviceSeverity(IcmpDeviceBase device, string evaluationFormula)
        {
            const string objectSubject = "della severità";

            if (!string.IsNullOrEmpty(evaluationFormula))
            {
                object returnValue = CalculateDeviceObjectDynamic(device, GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default,
                    evaluationFormula, objectSubject);

                GrisSuite.SnmpSupervisorLibrary.Database.Severity severity;

                try
                {
                    severity =
                        (GrisSuite.SnmpSupervisorLibrary.Database.Severity)
                            Enum.Parse(typeof (GrisSuite.SnmpSupervisorLibrary.Database.Severity), returnValue.ToString(), true);
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile ottenere il tipo dato di ritorno previsto, nel codice con la formula di calcolo dinamica {0}", objectSubject));
                }

                return severity;
            }

            return CalculateIcmpDeviceDefaultSeverity(device);
        }

        internal static string CalculateIcmpDeviceStatusMessage(IcmpDeviceBase device, string evaluationFormula)
        {
            const string objectSubject = "del messaggio";

            if (!string.IsNullOrEmpty(evaluationFormula))
            {
                object returnValue = CalculateDeviceObjectDynamic(device, GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default,
                    evaluationFormula, objectSubject);

                string message;

                if (returnValue is String)
                {
                    message = (String) returnValue;
                }
                else
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile ottenere il tipo dato di ritorno previsto, nel codice con la formula di calcolo dinamica {0}", objectSubject));
                }

                return message;
            }

            return CalculateIcmpDeviceDefaultStatusMessage(device);
        }

        #region Calcolo della severità di default per la periferica SNMP

        private static GrisSuite.SnmpSupervisorLibrary.Database.Severity CalculateSnmpDeviceDefaultSeverity(SnmpDeviceBase device)
        {
            if (device.ExcludedFromSeverityComputation)
            {
                // La periferica è stata tacitata da configurazione, quindi è in uno stato forzato statico
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.MaintenanceMode;
            }

            if (device.LastError != null)
            {
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
            }

            if (device.IsAliveIcmp && !device.IsAliveSnmp)
            {
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning;
            }

            if (!device.IsAliveIcmp && !device.IsAliveSnmp)
            {
                if (device.ForcedSeverityAndDescriptionWhenDeviceOffline != null)
                {
                    // Se è impostata una severità forzata nella singola definizione, usiamo quella, in override a quella configurata nel supervisore
                    return device.ForcedSeverityAndDescriptionWhenDeviceOffline.DeviceStatusSeverity;
                }

                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusSeverityUnreachable;
            }

            // Se sono impostate la severità e la descrizione forzate per la periferica, restituisce quelle
            // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
            // con regole dinamiche che è ricaduta nella MIB-II base
            if (device.NoMatchDeviceStatus != null)
            {
                // la periferica può forzare uno stato SNMP, solo nel caso che SNMP funzioni
                if (device.IsAliveSnmp)
                {
                    return device.NoMatchDeviceStatus.NoMatchDeviceStatusSeverity;
                }

                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning;
            }

            GrisSuite.SnmpSupervisorLibrary.Database.Severity streamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;

            if (device.StreamData.Count > 0)
            {
                int streamDataMib2BaseIndex = -1;

                for (int streamCounter = 0; streamCounter < device.StreamData.Count; streamCounter++)
                {
                    // in caso siano definiti più stream come base MIB-II, sarà considerato il primo
                    if (device.StreamData[streamCounter].IsMib2)
                    {
                        streamDataMib2BaseIndex = streamCounter;
                        break;
                    }
                }

                if (device.StreamData.Count == 1)
                {
                    #region Esiste un solo stream

                    if (streamDataMib2BaseIndex >= 0)
                    {
                        // esiste solo la MIB-II base
                        if ((device.StreamData[streamDataMib2BaseIndex].SeverityLevel != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) &&
                            ((streamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) ||
                             ((int) device.StreamData[streamDataMib2BaseIndex].SeverityLevel > (int) streamSeverity)))
                        {
                            streamSeverity = device.StreamData[streamDataMib2BaseIndex].SeverityLevel;
                        }
                    }
                    else
                    {
                        // restituiamo lo stato dell'unico stream esistente
                        streamDataMib2BaseIndex = 0;

                        // Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
                        if ((device.StreamData[streamDataMib2BaseIndex].SeverityLevel != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) &&
                            ((streamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) ||
                             ((int) device.StreamData[streamDataMib2BaseIndex].SeverityLevel > (int) streamSeverity)) &&
                            (!device.StreamData[streamDataMib2BaseIndex].ExcludedFromSeverityComputation))
                        {
                            streamSeverity = device.StreamData[streamDataMib2BaseIndex].SeverityLevel;
                        }
                    }

                    if (streamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Info)
                    {
                        // Se la severità complessiva peggiore degli stream è rimasta ad info, la periferica va in stato sconosciuto,
                        // perché non è possibile indicarne un vero stato
                        streamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
                    }

                    #endregion
                }
                else
                {
                    #region Esiste più di uno stream

                    // Livello di severità della MIB-II parziale
                    GrisSuite.SnmpSupervisorLibrary.Database.Severity mib2BaseStreamSeverity =
                        GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;

                    if (streamDataMib2BaseIndex >= 0)
                    {
                        if ((device.StreamData[streamDataMib2BaseIndex].SeverityLevel != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) &&
                            ((mib2BaseStreamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) ||
                             ((int) device.StreamData[streamDataMib2BaseIndex].SeverityLevel > (int) mib2BaseStreamSeverity)))
                        {
                            mib2BaseStreamSeverity = device.StreamData[streamDataMib2BaseIndex].SeverityLevel;
                        }
                    }
                    else
                    {
                        // nel caso che non sia definita nessuna MIB-II base, lo stato rimane a sconosciuto
                        mib2BaseStreamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
                    }

                    // Livello di severità delle MIB proprietarie
                    GrisSuite.SnmpSupervisorLibrary.Database.Severity customMibStreamSeverity =
                        GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;

                    for (int streamCounter = 0; streamCounter < device.StreamData.Count; streamCounter++)
                    {
                        if (device.StreamData[streamCounter].IsMib2)
                        {
                            // saltiamo tutti gli stream marcati come di tipo MIB-II base
                            continue;
                        }

                        // Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
                        if ((device.StreamData[streamCounter].SeverityLevel != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) &&
                            ((customMibStreamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) ||
                             ((int) device.StreamData[streamCounter].SeverityLevel > (int) customMibStreamSeverity)) &&
                            (!device.StreamData[streamCounter].ExcludedFromSeverityComputation))
                        {
                            customMibStreamSeverity = device.StreamData[streamCounter].SeverityLevel;
                        }

                        if ((!device.DeviceContainsStreamWithError) && (device.StreamData[streamCounter].ContainsStreamFieldWithError))
                        {
                            device.DeviceContainsStreamWithError = true;
                        }
                    }

                    if ((customMibStreamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) &&
                        (mib2BaseStreamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Ok))
                    {
                        // Nel caso che non risponda la MIB proprietaria, ma solo quella base, siamo in stato di allarme lieve
                        // (nel caso ordinario, saremmo in OK) - la periferica è probabilmente del tipo sbagliato nel System.xml
                        streamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning;
                        device.ReplyOnlyToMIBBase = true;
                    }
                    else
                    {
                        foreach (GrisSuite.SnmpSupervisorLibrary.Database.DBStream stream in device.StreamData)
                        {
                            // Escludiamo dal calcolo gli stream marcati come esclusi. Non è possibile escludere lo stream della MIB-II base.
                            if ((stream.SeverityLevel != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) &&
                                ((streamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) ||
                                 ((int) stream.SeverityLevel > (int) streamSeverity)) && (!stream.ExcludedFromSeverityComputation))
                            {
                                streamSeverity = stream.SeverityLevel;
                            }
                        }
                    }

                    if (streamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Info)
                    {
                        // Se la severità complessiva peggiore degli stream è rimasta ad info, la periferica va in stato sconosciuto,
                        // perché non è possibile indicarne un vero stato
                        streamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
                        device.ContainsOnlyInfoStreams = true;
                    }

                    // Valutiamo, come ultima cosa, la condizione di eccezione negli stream field.
                    // Essendo la finale, prevale su qualsiasi altra, ma solo se la periferica non è già in errore critico
                    // In caso di modifiche alla severità della condizione di eccezione, occorre adattare il codice
                    // in modo che la severità forzata non nasconda severità più gravi calcolate
                    if ((streamSeverity != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Error) && (device.DeviceContainsStreamWithError))
                    {
                        streamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning;
                    }

                    #endregion
                }
            }

            return streamSeverity;
        }

        #endregion

        #region Calcolo del messaggio di default per la periferica SNMP

        private static string CalculateSnmpDeviceDefaultStatusMessage(SnmpDeviceBase device)
        {
            if (device.ExcludedFromSeverityComputation)
            {
                // La periferica è stata tacitata da configurazione, quindi è in uno stato forzato statico
                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageMaintenanceMode;
            }

            if (device.LastError != null)
            {
                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnknown;
            }

            if (device.IsAliveIcmp && !device.IsAliveSnmp)
            {
                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageMaintenanceMode;
            }

            if ((device.IsAliveIcmp && device.IsAliveSnmp) || (!device.IsAliveIcmp && device.IsAliveSnmp))
            {
                // Se sono impostate la severità e la descrizione forzate per la periferica, restiuisce quelle
                // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
                // con regole dinamiche che è ricaduta nella MIB-II base
                if (device.NoMatchDeviceStatus != null)
                {
                    return device.NoMatchDeviceStatus.NoMatchDeviceStatusDescription;
                }

                switch (device.SeverityLevel)
                {
                    case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Ok:
                        return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageOK;
                    case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning:
                        if (device.DeviceContainsStreamWithError)
                        {
                            return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageIncompleteData;
                        }

                        if (device.ReplyOnlyToMIBBase)
                        {
                            return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageMaintenanceModeMIB2Only;
                        }

                        return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageWarning;
                    case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Error:
                        return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageError;
                    case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown:
                        return device.ContainsOnlyInfoStreams
                            ? GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageNotApplicable
                            : GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnknown;
                }
            }

            if (!device.IsAliveIcmp && !device.IsAliveSnmp)
            {
                if (device.ForcedSeverityAndDescriptionWhenDeviceOffline != null)
                {
                    // Se è impostata una severità forzata nella singola definizione, usiamo quella, in override a quella configurata nel supervisore
                    return device.ForcedSeverityAndDescriptionWhenDeviceOffline.DeviceStatusDescription;
                }

                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnreachable;
            }

            return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnknown;
        }

        #endregion

        #region Calcolo della severità di default per la periferica ICMP

        private static GrisSuite.SnmpSupervisorLibrary.Database.Severity CalculateIcmpDeviceDefaultSeverity(IcmpDeviceBase device)
        {
            if (device.ExcludedFromSeverityComputation)
            {
                // La periferica è stata tacitata da configurazione, quindi è in uno stato forzato statico
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.MaintenanceMode;
            }

            if (device.LastError != null)
            {
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
            }

            if (!device.IsAliveIcmp)
            {
                if (device.ForcedSeverityAndDescriptionWhenDeviceOffline != null)
                {
                    // Se è impostata una severità forzata nella singola definizione, usiamo quella, in override a quella configurata nel supervisore
                    return device.ForcedSeverityAndDescriptionWhenDeviceOffline.DeviceStatusSeverity;
                }

                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusSeverityUnreachable;
            }

            GrisSuite.SnmpSupervisorLibrary.Database.Severity streamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;

            if (device.StreamData.Count > 0)
            {
                // restituiamo lo stato dell'unico stream esistente
                if ((device.StreamData[0].SeverityLevel != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) &&
                    ((streamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) ||
                     ((int) device.StreamData[0].SeverityLevel > (int) streamSeverity)))
                {
                    streamSeverity = device.StreamData[0].SeverityLevel;
                }
            }

            if (streamSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Info)
            {
                // Se la severità complessiva peggiore degli stream è rimasta ad info, la periferica va in stato sconosciuto,
                // perché non è possibile indicarne un vero stato
                streamSeverity = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
                device.ContainsOnlyInfoStreams = true;
            }

            return streamSeverity;
        }

        #endregion

        #region Calcolo del messaggio di default per la periferica ICMP

        private static string CalculateIcmpDeviceDefaultStatusMessage(IcmpDeviceBase device)
        {
            if (device.ExcludedFromSeverityComputation)
            {
                // La periferica è stata tacitata da configurazione, quindi è in uno stato forzato statico
                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageMaintenanceMode;
            }

            if (device.LastError != null)
            {
                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnknown;
            }

            if (!device.IsAliveIcmp)
            {
                if (device.ForcedSeverityAndDescriptionWhenDeviceOffline != null)
                {
                    // Se è impostata una severità forzata nella singola definizione, usiamo quella, in override a quella configurata nel supervisore
                    return device.ForcedSeverityAndDescriptionWhenDeviceOffline.DeviceStatusDescription;
                }

                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnreachable;
            }

            switch (device.SeverityLevel)
            {
                case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Ok:
                    return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageOK;
                case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning:
                    return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageWarning;
                case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Error:
                    return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageError;
                case GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown:
                    return device.ContainsOnlyInfoStreams
                        ? GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageNotApplicable
                        : GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnknown;
            }

            return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusMessageUnknown;
        }

        #endregion

        #region Metodo di calcolo dinamico oggetto

        private static object CalculateDeviceObjectDynamic(IDevice device,
            GrisSuite.SnmpSupervisorLibrary.Properties.Settings defaultSettings,
            string evaluationFormula,
            string objectSubject)
        {
            if (string.IsNullOrEmpty(evaluationFormula))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Formula per il calcolo dinamico {0} della periferica nulla", objectSubject));
            }

            if (device == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Oggetto periferica fornito al calcolo dinamico {0} nullo", objectSubject));
            }

            if (defaultSettings == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Oggetto per parametri di configurazione fornito al calcolo dinamico {0} nullo", objectSubject));
            }

            const string className = "Evaluation";
            const string methodName = "GetValue";
            const string returnType = "object";
            string inputObjectType1;
            const string inputObjectType2 = "GrisSuite.SnmpSupervisorLibrary.Properties.Settings";

            if (device is SnmpDeviceBase)
            {
                inputObjectType1 = "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpDeviceBase";
            }
            else if (device is IcmpDeviceBase)
            {
                inputObjectType1 = "GrisSuite.SnmpSupervisorLibrary.Devices.IcmpDeviceBase";
            }
            else
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Tipo oggetto periferica fornito al calcolo dinamico {0} non previsto. Tipo corrente: '{1}'", objectSubject, device.GetType().Name));
            }

            string code;
            try
            {
                code = string.Format(CultureInfo.InvariantCulture,
                    "internal class {0} {{\r\npublic {1} {2}({3} device, {4} defaultSettings){{\r\n{5}\r\n}}\r\n}}", className, returnType, methodName,
                    inputObjectType1, inputObjectType2, evaluationFormula);
            }
            catch (ArgumentNullException ex)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Errore nella formattazione del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, ex));
            }
            catch (FormatException ex)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Errore nella formattazione del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, ex));
            }

            if (string.IsNullOrEmpty(code))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Errore nel codice con la formula di calcolo dinamico {0}, codice C# nullo o vuoto", objectSubject));
            }

            string codeHash;

            using (SHA256 sha = new SHA256Managed())
            {
                try
                {
                    codeHash = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(code))).Replace("-", string.Empty);
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella generazione del nome file con il codice dinamico di calcolo {0}", objectSubject));
                }
                catch (ObjectDisposedException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella generazione del nome file con il codice dinamico di calcolo {0}", objectSubject));
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella generazione del nome file con il codice dinamico di calcolo {0}", objectSubject));
                }
            }

            string dynamicAssemblyFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DYNAMIC_ASSEMBLY_FOLDER);

            if (!Directory.Exists(dynamicAssemblyFolder))
            {
                try
                {
                    Directory.CreateDirectory(dynamicAssemblyFolder);
                }
                catch (IOException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice di calcolo dinamico {0} ({1})", objectSubject,
                        dynamicAssemblyFolder));
                }
                catch (UnauthorizedAccessException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice di calcolo dinamico {0} ({1})", objectSubject,
                        dynamicAssemblyFolder));
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice di calcolo dinamico {0} ({1})", objectSubject,
                        dynamicAssemblyFolder));
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice di calcolo dinamico {0} ({1})", objectSubject,
                        dynamicAssemblyFolder));
                }
            }

            if (string.IsNullOrEmpty(codeHash))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Errore nella generazione del nome dell'assembly dinamico per il calcolo {0}", objectSubject));
            }

            string dynamicAssembly = Path.Combine(dynamicAssemblyFolder,
                string.Format(CultureInfo.InvariantCulture, "{0}-{1}.dll", DYNAMIC_ASSEMBLY_FILENAME_PREFIX, codeHash));

            object loObject = null;
            // ReSharper disable TooWideLocalVariableScope
            Assembly loAssembly;
            // ReSharper restore TooWideLocalVariableScope

            lock (locker)
            {
                if (FileUtility.CheckFileCanRead(dynamicAssembly))
                {
                    #region Caricamento dinamico Assembly già esistente

                    try
                    {
                        loAssembly = Assembly.LoadFile(dynamicAssembly);
                        loObject = loAssembly.CreateInstance(className);
                    }
                    catch (ArgumentException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, code), ex);
                    }
                    catch (MissingMethodException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, code), ex);
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, code), ex);
                    }
                    catch (FileLoadException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, code), ex);
                    }
                    catch (BadImageFormatException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, code), ex);
                    }

                    #endregion
                }
                else
                {
                    #region L'Assembly dinamico non esiste, deve essere generato e compilato

                    #region Preparazione parametri per il compilatore dinamico

                    CompilerParameters parameters = new CompilerParameters();
                    parameters.GenerateExecutable = false;
                    parameters.OutputAssembly = dynamicAssembly;
                    parameters.GenerateInMemory = true;
                    parameters.TreatWarningsAsErrors = true;
                    parameters.TempFiles.KeepFiles = false;
                    parameters.CompilerOptions = "/target:library /optimize+";
                    parameters.IncludeDebugInformation = false;
                    parameters.WarningLevel = 3;

                    #endregion

                    #region Caricamento ed assegnazione References

                    // ad una DLL da referenziare che sia preceduta da "(local)", sarà aggiunto il prefisso con il path completo dell'applicazione
                    // per effettuare la ricerca della DLL nella cartella corrente (in caso opposto, si presuppone che le DLL siano in GAC)
                    const string referencesAssemblies = "System.dll,(local)SharpSnmpLib.dll,(local)GrisSuite.SnmpSupervisorLibrary.dll";

                    foreach (string refAssembly in referencesAssemblies.Split(','))
                    {
                        if (!string.IsNullOrEmpty(refAssembly))
                        {
                            if (refAssembly.Trim().StartsWith("(local)", StringComparison.OrdinalIgnoreCase))
                            {
                                parameters.ReferencedAssemblies.Add(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                    refAssembly.Trim().Replace("(local)", string.Empty)));
                            }
                            else
                            {
                                parameters.ReferencedAssemblies.Add(refAssembly.Trim());
                            }
                        }
                    }

                    #endregion

                    using (CSharpCodeProvider provider = new CSharpCodeProvider())
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            #region Compilazione dinamica codice

                            CompilerResults compilerResults;

                            try
                            {
                                compilerResults = provider.CompileAssemblyFromSource(parameters, code);
                            }
                            catch (NotImplementedException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella compilazione del codice con la formula di calcolo dinamico {0}", objectSubject), ex);
                            }
                            catch (IOException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella compilazione del codice con la formula di calcolo dinamico {0}", objectSubject), ex);
                            }

                            if ((compilerResults.Errors != null) && (compilerResults.Errors.Count > 0))
                            {
                                StringBuilder compileErrors = new StringBuilder();

                                foreach (CompilerError compileError in compilerResults.Errors)
                                {
                                    compileErrors.AppendLine(compileError.ErrorText);
                                }

                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                    "Errore nella compilazione del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject, compileErrors));
                            }

                            #endregion

                            #region Caricamento dinamico Assembly appena generato

                            try
                            {
                                loAssembly = compilerResults.CompiledAssembly;
                                loObject = loAssembly.CreateInstance(className);
                            }
                            catch (ArgumentException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject,
                                        code), ex);
                            }
                            catch (MissingMethodException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject,
                                        code), ex);
                            }
                            catch (FileNotFoundException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject,
                                        code), ex);
                            }
                            catch (FileLoadException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject,
                                        code), ex);
                            }
                            catch (BadImageFormatException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}\r\n{1}", objectSubject,
                                        code), ex);
                            }

                            #endregion
                        }
                    }

                    #endregion
                }
            }

            if (loObject == null)
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Errore nella creazione di istanza del codice con la formula di calcolo dinamico {0}", objectSubject));
            }

            object[] loCodeParms = new object[2];
            loCodeParms[0] = device;
            loCodeParms[1] = defaultSettings;

            try
            {
                object returnValue = loObject.GetType()
                    .InvokeMember(methodName, BindingFlags.InvokeMethod, null, loObject, loCodeParms, CultureInfo.InvariantCulture);

                if (returnValue == null)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "calcolo dinamico {0} fallito, oggetto calcolato nullo", objectSubject));
                }

                return returnValue;
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (MethodAccessException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (MissingFieldException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (MissingMethodException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (TargetException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (TargetInvocationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (AmbiguousMatchException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
            catch (OverflowException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di calcolo dinamico {0}\r\n{1}",
                        objectSubject, code), ex);
            }
        }

        #endregion

        // funzione per testare del codice c# per testare del codice dinamico per il calcolo della severità
        private static GrisSuite.SnmpSupervisorLibrary.Database.Severity testDynamicCode(SnmpDeviceBase device) {
            System.String streamName = "Linee RS485";
            System.String statusFieldName = "Stato";
            System.String typeFieldName = "Tipo";

            if (device.ExcludedFromSeverityComputation)
            {
                // La periferica è stata tacitata da configurazione, quindi è in uno stato forzato statico
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.MaintenanceMode;
            }

            if (device.LastError != null)
            {
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
            }

            if (device.IsAliveIcmp && !device.IsAliveSnmp)
            {
                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning;
            }

            if (!device.IsAliveIcmp && !device.IsAliveSnmp)
            {
                if (device.ForcedSeverityAndDescriptionWhenDeviceOffline != null)
                {
                    // Se è impostata una severità forzata nella singola definizione, usiamo quella, in override a quella configurata nel supervisore
                    return device.ForcedSeverityAndDescriptionWhenDeviceOffline.DeviceStatusSeverity;
                }

                return GrisSuite.SnmpSupervisorLibrary.Properties.Settings.Default.DeviceStatusSeverityUnreachable;
            }

            // Se sono impostate la severità e la descrizione forzate per la periferica, restituisce quelle
            // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
            // con regole dinamiche che è ricaduta nella MIB-II base
            if (device.NoMatchDeviceStatus != null)
            {
                // la periferica può forzare uno stato SNMP, solo nel caso che SNMP funzioni
                if (device.IsAliveSnmp)
                {
                    return device.NoMatchDeviceStatus.NoMatchDeviceStatusSeverity;
                }

                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning;
            }

            // Se sono impostate la severità e la descrizione forzate per la periferica, restituisce quelle
            // La forzatura si applica sempre qui, perché sarà valorizzata solo quando si tratta di una periferica
            // con regole dinamiche che è ricaduta nella MIB-II base
            if (device.NoMatchDeviceStatus != null)
            {
                // la periferica può forzare uno stato SNMP, solo nel caso che SNMP funzioni
                if (device.IsAliveSnmp)
                {
                    return device.NoMatchDeviceStatus.NoMatchDeviceStatusSeverity;
                }

                return GrisSuite.SnmpSupervisorLibrary.Database.Severity.Warning;
            }

            GrisSuite.SnmpSupervisorLibrary.Database.Severity deviceSeverity = device.SeverityLevel;

            foreach (GrisSuite.SnmpSupervisorLibrary.Database.DBStream stream in device.StreamData) {
                if (stream.Name.Equals(streamName)) {
                    foreach (GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField field in stream.FieldData) {
                        if (field.IsStreamFieldArray) {
                            GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField statusField = null;
                            GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField typeField = null;

                            field.SeverityLevel = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown;
                            foreach (GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField child in field.DbStreamFieldValueChildren)
                            {
                                if (child.Name.Equals(statusFieldName))
                                {
                                    statusField = child;
                                }
                                else if (child.Name.Equals(typeFieldName))
                                {
                                    typeField = child;
                                }
                            }

                            if (statusField != null && typeField != null) {
                                if (statusField.Value.Equals("0") && typeField.Value.Equals("0")) {
                                    statusField.SeverityLevel = GrisSuite.SnmpSupervisorLibrary.Database.Severity.Info;
                                }

                                if ((statusField.SeverityLevel != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) && (statusField.PersistOnDatabase) && (!statusField.ExcludedFromSeverityComputation) &&
                                                ((field.SeverityLevel == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) || ((int)statusField.SeverityLevel > (int)field.SeverityLevel)))
                                {
                                    field.SeverityLevel = statusField.SeverityLevel;
                                }
                            }
                        }
                    }
                }

                // calcolo severità dell'intero device
                GrisSuite.SnmpSupervisorLibrary.Database.Severity streamSeverity = stream.SeverityLevel;
                if ((streamSeverity != GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) && (!stream.ExcludedFromSeverityComputation) &&
                                            ((deviceSeverity == GrisSuite.SnmpSupervisorLibrary.Database.Severity.Unknown) || ((int)streamSeverity > (int)deviceSeverity)))
                {
                    deviceSeverity = streamSeverity;
                }
            }

            return deviceSeverity;
        } 
    }
}

// ReSharper restore RedundantNameQualifier
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///     <para>Rappresenta la classe base per i dispositivi SNMP.</para>
    ///     <para>Comprende la definizione parziale di MIB II standard, comune a tutti i dispositivi.</para>
    /// </summary>
    public class SnmpDeviceBase : ISnmpDevice
    {
        #region Variabili private

        private readonly int deviceCategory;
        private readonly bool hasDynamicLoadingRules;
        private bool isPopulated;
        private readonly Collection<DBStream> streamData;
        private readonly Collection<EventObject> eventsData;
        private readonly MessengerFactory messengerFactory;
        private readonly string deviceType;
        // Severità calcolata della periferica
        private Severity severity;

        #endregion

        #region Variabili fisse usate per il test di funzionalità SNMP - query sysName

        /// <summary>
        ///     Indice posizionale dei valori retituiti in base al sysName
        /// </summary>
        /// <see cref="oidSysName" />
        private const int OID_SYS_NAME_INDEX = 0;

        /// <summary>
        ///     Indice posizionale dei valori retituiti in base all'Oid del numero di serie
        /// </summary>
        private const int OID_SERIAL_NUMBER_INDEX = 0;

        /// <summary>
        ///     Oid standard del nome (sysName) per la MIB II parziale - usata per testare il funzionamento SNMP
        /// </summary>
        private readonly uint[] oidSysName = {1, 3, 6, 1, 2, 1, 1, 1, 0};

        #endregion

        #region Costruttore

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name="deviceName">Nome della periferica</param>
        /// <param name="deviceType">Codice univoco della periferica</param>
        /// <param name="deviceIP">Indirizzo IP e porta della periferica</param>
        /// <param name="lastEvent">Informazioni dell'ultimo evento in ordine temporale associato alla periferica</param>
        /// <param name="snmpCommunity">Community string per l'accesso Snmp in lettura</param>
        /// <param name="deviceConfigurationHelperParameters">Classe helper che contiene i parametri base delle definizioni</param>
        /// <param name="device">Contenitore oggetto periferica, come letto da System.xml</param>
        public SnmpDeviceBase(bool fakeLocalData,
            string deviceName,
            string deviceType,
            IPEndPoint deviceIP,
            EventObject lastEvent,
            string snmpCommunity,
            DeviceConfigurationHelperParameters deviceConfigurationHelperParameters,
            DeviceObject device)
        {
            if (String.IsNullOrEmpty(deviceName))
            {
                throw new ArgumentException("Il nome della periferica è obbligatorio.", "deviceName");
            }

            if (String.IsNullOrEmpty(deviceType))
            {
                throw new ArgumentException("Il tipo della periferica è obbligatorio.", "deviceType");
            }

            if ((deviceIP == null) || (deviceIP.Address == null))
            {
                throw new ArgumentException("L'indirizzo IP della periferica è obbligatorio.", "deviceIP");
            }

            if (device == null)
            {
                throw new ArgumentException("L'oggetto della periferica è obbligatorio.", "device");
            }

            this.DeviceName = deviceName;
            this.deviceType = deviceType;
            this.streamData = new Collection<DBStream>();
            this.eventsData = new Collection<EventObject>();
            this.DeviceIPEndPoint = deviceIP;
            this.LastEvent = lastEvent;
            this.LastError = null;
            this.messengerFactory = new MessengerFactory();
            this.DefinitionFileName = deviceConfigurationHelperParameters.DefinitionFileName;
            this.deviceCategory = (int) SpecialDeviceCategory.Undefined;
            this.FakeLocalData = fakeLocalData;
            this.SnmpCommunity = snmpCommunity;
            this.NoMatchDeviceStatus = null;
            this.DeviceConfigurationParameters = deviceConfigurationHelperParameters;
            this.Device = device;
            this.ContainsOnlyInfoStreams = false;
            this.severity = Severity.Unknown;
            this.ShouldSendNotificationByEmail = false;
            this.DynamicCodeCalculateSeverity = null;
            this.DynamicCodeCalculateStatusMessage = null;
            this.CheckIfHasMib2 = true;
            this.SnmpAliveOid = null;
            this.IsDataRetrieved = true;

            try
            {
                DeviceConfigurationHelper deviceConfigurationHelper = new DeviceConfigurationHelper(this.FakeLocalData, device.DeviceId,
                    this.messengerFactory, this.DeviceIPEndPoint, this.SnmpCommunity, this.DeviceConfigurationParameters);

                // Elaboriamo la definizione per sapere se contiene una configurazione di caricamento dinamico
                // Nel caso positivo, si riconfigurerà su un file di definizione corretto e troveremo la categoria della periferica impostata
                // Il precaricamento imposta anche la versione Snmp per come è indicata nella definizione originale,
                // in modo da poter caricare i dati della categoria e del produttore via SNMP
                deviceConfigurationHelper.PreloadConfiguration();

                this.SnmpVersionCode = deviceConfigurationHelper.SnmpVersionCode;
                this.deviceCategory = deviceConfigurationHelper.DeviceCategory;
                this.hasDynamicLoadingRules = deviceConfigurationHelper.HasDynamicLoadingRules;
                this.DefinitionFileName = deviceConfigurationHelper.DefinitionFileName;
                this.NoMatchDeviceStatus = deviceConfigurationHelper.NoMatchDeviceStatus;
                this.ForcedDeviceType = deviceConfigurationHelper.ForcedDeviceType;
                this.ExcludedFromSeverityComputation = false;
                this.DeviceConfigurationParameters = deviceConfigurationHelper.DeviceConfigurationParameters;
                this.CheckIfHasMib2 = deviceConfigurationHelper.CheckIfHasMib2;
                this.SnmpAliveOid = deviceConfigurationHelper.SnmpAliveOid;

                deviceConfigurationHelper.LoadConfiguration(this.streamData);

                // rileggiamo la versione Snmp nel caso sia cambiata a seguito di caricamento successivo di definizione dinamica
                this.SnmpVersionCode = deviceConfigurationHelper.SnmpVersionCode;

                this.SerialNumberOid = deviceConfigurationHelper.SerialNumberOid;
                this.ForcedSeverityAndDescriptionWhenDeviceOffline = deviceConfigurationHelper.ForcedSeverityAndDescriptionWhenDeviceOffline;
                this.DynamicCodeCalculateSeverity = deviceConfigurationHelper.DynamicCodeCalculateSeverity;
                this.DynamicCodeCalculateStatusMessage = deviceConfigurationHelper.DynamicCodeCalculateStatusMessage;
            }
            catch (DeviceConfigurationHelperException ex)
            {
                this.LastError = ex;
            }
            catch (ArgumentException ex)
            {
                this.LastError = ex;
            }

            if (this.LastError == null)
            {
                this.CheckStatus();
            }
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///     Nome della periferica
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        ///     Indica se è stato ritrovato almeno uno streamfield
        /// </summary>
        public bool IsDataRetrieved { get; private set; }

        /// <summary>
        ///     Codice univoco della periferica (forzato nel caso di definizioni dinamiche)
        /// </summary>
        public string DeviceType
        {
            get
            {
                // Se esiste un tipo periferica forzato (letto dalle regole dinamiche di caricamento su definizioni dinamiche)
                // restituiamo quello
                if (!string.IsNullOrEmpty(this.ForcedDeviceType))
                {
                    return this.ForcedDeviceType;
                }

                return this.deviceType;
            }
        }

        /// <summary>
        ///     Indica se bisgna verificare se la mib2 esiste
        /// </summary>
        public bool CheckIfHasMib2 { get; private set; }

        /// <summary>
        ///     Community string per l'accesso Snmp in lettura
        /// </summary>
        public string SnmpCommunity { get; private set; }

        /// <summary>
        ///     Versione del protocollo Snmp per le richieste
        /// </summary>
        public VersionCode SnmpVersionCode { get; private set; }

        /// <summary>
        ///     Dati di severità e descrizione periferica, nel caso la periferica sia offline
        /// </summary>
        public ForcedSeverityAndDescriptionWhenOffline ForcedSeverityAndDescriptionWhenDeviceOffline { get; private set; }

        /// <summary>
        ///     Nome completo della definizione XML della periferica
        /// </summary>
        public string DefinitionFileName { get; private set; }

        /// <summary>
        ///     EndPoint IP della periferica
        /// </summary>
        public IPEndPoint DeviceIPEndPoint { get; private set; }

        /// <summary>
        ///     Lista degli Stream relativi alla periferica
        /// </summary>
        public Collection<DBStream> StreamData
        {
            get
            {
                if (this.LastError != null)
                {
                    return new Collection<DBStream>();
                }

                return this.streamData;
            }
        }

        /// <summary>
        ///     Classe helper che contiene i parametri base delle definizioni
        /// </summary>
        public DeviceConfigurationHelperParameters DeviceConfigurationParameters { get; private set; }

        /// <summary>
        ///     Contenitore oggetto periferica, come letto da System.xml
        /// </summary>
        public DeviceObject Device { get; private set; }

        /// <summary>
        ///     Indica se caricare i dati da file di risposte locali per simulazione
        /// </summary>
        public bool FakeLocalData { get; private set; }

        /// <summary>
        ///     <para>Severità relativa al Device.</para>
        ///     <para>Nel caso il Device sia visibile come ICMP, ma non come SNMP, la severità è sconosciuta.</para>
        /// </summary>
        public Severity SeverityLevel
        {
            get { return this.severity; }
        }

        /// <summary>
        ///     Stato del Device, inteso come Online (raggiungibile almeno via SNMP) oppure Offline (non raggiungibile e non
        ///     monitorabile)
        /// </summary>
        public byte Offline
        {
            get
            {
                if (this.IsAliveIcmp && this.IsAliveSnmp)
                {
                    return 0;
                }

                if (!this.IsAliveIcmp && this.IsAliveSnmp)
                {
                    return 0;
                }

                if (this.IsAliveIcmp && !this.IsAliveSnmp)
                {
                    return 0;
                }

                return 1;
            }
        }

        /// <summary>
        ///     Indica se gli stream e stream fields della periferica devono essere persistiti
        /// </summary>
        public bool ShouldPersistStreamAndStreamFields
        {
            get
            {
                if (this.IsAliveIcmp && this.IsAliveSnmp)
                {
                    return true;
                }

                if (!this.IsAliveIcmp && this.IsAliveSnmp)
                {
                    return true;
                }

                if (this.IsAliveIcmp && !this.IsAliveSnmp)
                {
                    return false;
                }

                return false;
            }
        }

        /// <summary>
        ///     Codice C# per il calcolo dinamico della severità
        /// </summary>
        public string DynamicCodeCalculateSeverity { get; private set; }

        /// <summary>
        ///     Codice C# per il calcolo dinamico del messaggio di stato
        /// </summary>
        public string DynamicCodeCalculateStatusMessage { get; private set; }

        /// <summary>
        ///     Decodifica descrittiva dello stato della periferica
        /// </summary>
        public string ValueDescriptionComplete
        {
            get { return SeverityCalculators.CalculateSnmpDeviceStatusMessage(this, this.DynamicCodeCalculateStatusMessage); }
        }

        /// <summary>
        ///     Indica se la periferica è raggiungibile via ICMP (ping)
        /// </summary>
        public bool IsAliveIcmp { get; private set; }

        /// <summary>
        ///     Indica se la periferica è raggiungibile via query SNMP
        /// </summary>
        public bool IsAliveSnmp { get; private set; }

        /// <summary>
        ///     Indica se c'è stata una eccezione durante il caricamento della periferica
        /// </summary>
        public Exception LastError { get; set; }

        /// <summary>
        ///     Informazioni dell'ultimo evento in ordine temporale associato alla periferica
        /// </summary>
        public EventObject LastEvent { get; private set; }

        /// <summary>
        ///     Lista degli Eventi relativi alla periferica
        /// </summary>
        public Collection<EventObject> EventsData
        {
            get { return this.eventsData; }
        }

        /// <summary>
        ///     Indica le informazioni forzate della periferica, nel caso che non sia possibile determinare una regola di
        ///     caricamento dinamico
        /// </summary>
        public NoDynamicRuleMatchDeviceStatus NoMatchDeviceStatus { get; private set; }

        /// <summary>
        ///     Oid opzionale che indica come recuperare via SNMP il numero di serie della periferica
        /// </summary>
        public ObjectIdentifier SerialNumberOid { get; private set; }

        /// <summary>
        ///     Oid opzionale che indica come verificare le il servizio SNMP è vivo 
        /// </summary>
        public ObjectIdentifier SnmpAliveOid { get; private set; }

        /// <summary>
        ///     Indica se la periferica è stata tacitata e quindi sarà in stato forzato
        /// </summary>
        public bool ExcludedFromSeverityComputation { get; private set; }

        /// <summary>
        ///     Indica se occorre inviare una notifica per mail. È un aggregato denormalizzato dei flag di tutti gli Stream Field
        ///     contenuti
        /// </summary>
        public bool ShouldSendNotificationByEmail { get; private set; }

        /// <summary>
        ///     Indica se esiste almeno uno stream field che ha generato eccezioni durante il caricamento dei dati
        /// </summary>
        public bool DeviceContainsStreamWithError { get; set; }

        /// <summary>
        ///     Indica se la periferica ha risposto solo a stream marcati come MIB-II base
        /// </summary>
        public bool ReplyOnlyToMIBBase { get; set; }

        /// <summary>
        ///     Indica se la periferica contiene solamente stream in stato Info
        /// </summary>
        public bool ContainsOnlyInfoStreams { get; set; }

        #endregion

        #region Proprietà private

        /// <summary>
        ///     Nome di periferica forzato, che può subentrare, se configurato, nel caso di definizioni a caricamento dinamico
        /// </summary>
        internal string ForcedDeviceType { get; private set; }

        #endregion

        #region Metodi privati

        private void RetrieveData()
        {
            this.IsDataRetrieved = false;
            this.messengerFactory.IsDataRetrieved = false;

            Dictionary<ObjectIdentifier, Variable[,]> snmpTableCache = new Dictionary<ObjectIdentifier, Variable[,]>();

            Dictionary<ObjectIdentifier, IList<Variable>> snmpScalarCache = new Dictionary<ObjectIdentifier, IList<Variable>>();

            foreach (DBStream stream in this.StreamData)
            {
                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                        string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati Stream ID: {1} - Stream Name: {2}",
                            Thread.CurrentThread.GetHashCode(), stream.StreamId, stream.Name));
                }

                for (int i = 0; i < stream.FieldData.Count; i++)
                {
                    DBStreamField field = stream.FieldData[i];
                    String strVal = null;

                    if (field.isDiscardData) {
                        continue;
                    }

                        // gestisco il caso di iterazioni su valori di espressioni regolari di tipo Regex
                        // gestisco il caso solo se lo stream field è un "nipote" e il nonno è un array di streamfield 
                        if (field.StreamFieldFather != null && field.StreamFieldFather.StreamFieldFather != null && field.StreamFieldFather.StreamFieldFather.IsStreamFieldArray && field.StreamFieldFather.StreamFieldFather.RawData != null && field.StreamFieldFather.StreamFieldFather.RawData.Count == 1)
                        {
                            strVal = "" + field.StreamFieldFather.StreamFieldFather.RawData[0].Data;
                        }
                        else if (field.StreamFieldFather != null && field.StreamFieldFather.IsStreamFieldArray && field.StreamFieldFather.RawData != null && field.StreamFieldFather.RawData.Count == 1)
                        {
                            strVal = "" + field.StreamFieldFather.RawData[0].Data;
                        }

                        if (strVal != null)
                        {
                            if (field.TableFilterColumnRegex != null)
                            {
                                field.TableFilterColumnRegex = new System.Text.RegularExpressions.Regex("^" + strVal + "$", System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                            }
                            else if (field.TableRowIndex.HasValue)
                            {
                                // se non è tipo regex ipotizzo ci sia in corso una iterazione su indice di riga
                                int numValue = -1;
                                try
                                {
                                    numValue = Int32.Parse(strVal);
                                }
                                catch (Exception e)
                                {
                                    System.Diagnostics.Debug.WriteLine(
                                    string.Format(CultureInfo.InvariantCulture, "Errore nel recupero del valore di un elemento  di tipo StremFieldArray: valore non intero; Stream Field ID: {0} - Stream Field Name: {1} - Valore={2}",
                                    field.FieldId, field.Name, strVal == null ? "null" : strVal));
                                }

                                if (numValue >= 0)
                                    field.TableRowIndex = numValue;
                            }
                        }
                    
                        if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                        {
                            FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati Stream Field ID: {1} - Stream Field Name: {2}",
                                    Thread.CurrentThread.GetHashCode(), field.FieldId, field.Name));
                        }

                        if (field.FieldTableOid != null)
                        {
                            #region Recupero dati SNMP tabellari

                            // La presenza dell'Oid per la query tabellare forza l'uso della gestione tabellare

                            #region Recupero tabella principale query SNMP tabellare

                            Variable[,] table = null;

                            if (snmpTableCache.ContainsKey(field.FieldTableOid.TableOid))
                            {
                                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                {
                                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                        string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP tabellari da cache, Oid: {1}",
                                            Thread.CurrentThread.GetHashCode(), field.FieldTableOid.TableOid));
                                }

                                table = snmpTableCache[field.FieldTableOid.TableOid];
                            }
                            else
                            {
                                if (field.FieldTableOid.TableQueryMode == TableMode.Standard)
                                {
                                    table = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                        new OctetString(this.SnmpCommunity), field.FieldTableOid.TableOid);
                                }
                                else if (field.FieldTableOid.TableQueryMode == TableMode.Sparse)
                                {
                                    table = this.messengerFactory.GetTableUsingMIBData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                        new OctetString(this.SnmpCommunity), field.FieldTableOid.TableOid,
                                        (field.FieldTableOid.TableColumnCount.HasValue ? field.FieldTableOid.TableColumnCount.Value : 1),
                                        field.FieldTableOid.TableEntry, field.FieldTableOid.TableIndexColumns);
                                }

                                snmpTableCache.Add(field.FieldTableOid.TableOid, table);
                            }

                            #endregion

                            #region Recupero tabella correlata query SNMP tabellare

                            Variable[,] correlationTable = null;

                            if (field.FieldCorrelationTableOid != null)
                            {
                                // Estraiamo la tabella correlata prima possibile rispetto a quella principale, perché potrebbe essere necessario usare un indice di riga
                                // della tabella principale per accedere a quella correlata, che nel frattempo potrebbe essere cambiata (ad esempio le liste processi)

                                if (snmpTableCache.ContainsKey(field.FieldCorrelationTableOid.TableOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP tabellari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(), field.FieldCorrelationTableOid.TableOid));
                                    }

                                    correlationTable = snmpTableCache[field.FieldCorrelationTableOid.TableOid];
                                }
                                else
                                {
                                    if (field.FieldCorrelationTableOid.TableQueryMode == TableMode.Standard)
                                    {
                                        correlationTable = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode,
                                            this.DeviceIPEndPoint, new OctetString(this.SnmpCommunity), field.FieldCorrelationTableOid.TableOid);
                                    }
                                    else if (field.FieldCorrelationTableOid.TableQueryMode == TableMode.Sparse)
                                    {
                                        correlationTable = this.messengerFactory.GetTableUsingMIBData(this.FakeLocalData, this.SnmpVersionCode,
                                            this.DeviceIPEndPoint, new OctetString(this.SnmpCommunity), field.FieldCorrelationTableOid.TableOid,
                                            (field.FieldCorrelationTableOid.TableColumnCount.HasValue
                                                ? field.FieldCorrelationTableOid.TableColumnCount.Value
                                                : 1), field.FieldCorrelationTableOid.TableEntry, field.FieldCorrelationTableOid.TableIndexColumns);
                                    }

                                    snmpTableCache.Add(field.FieldCorrelationTableOid.TableOid, correlationTable);
                                }
                            }

                            #endregion

                            #region Recupero eventuale tabella aggiuntiva per filtro persistenza DB per query SNMP tabellare

                            Variable[,] hideIfNotAvailableTable = null;

                            if ((field.HideIfNotAvailableRowAndColumnIndex != null) && (field.HideIfNotAvailableRowAndColumnIndex.IsValid) &&
                                (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid != null))
                            {
                                if (snmpTableCache.ContainsKey(field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP tabellari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(),
                                                field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid));
                                    }

                                    hideIfNotAvailableTable =
                                        snmpTableCache[field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid];
                                }
                                else
                                {
                                    if (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableQueryMode ==
                                        TableMode.Standard)
                                    {
                                        hideIfNotAvailableTable = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode,
                                            this.DeviceIPEndPoint, new OctetString(this.SnmpCommunity),
                                            field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid);
                                    }
                                    else if (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableQueryMode ==
                                             TableMode.Sparse)
                                    {
                                        hideIfNotAvailableTable = this.messengerFactory.GetTableUsingMIBData(this.FakeLocalData, this.SnmpVersionCode,
                                            this.DeviceIPEndPoint, new OctetString(this.SnmpCommunity),
                                            field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid,
                                            (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableColumnCount.HasValue
                                                ? field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableColumnCount
                                                    .Value
                                                : 1), field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableEntry,
                                            field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableIndexColumns);
                                    }

                                    snmpTableCache.Add(field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid,
                                        hideIfNotAvailableTable);
                                }
                            }

                            #endregion

                            #region Valutazione di contatore di righe di tabella dinamica e relativo indice di riga da definizione

                            if ((field.TableIndex.HasValue) && (field.TableIndex.Value >= 0) && (field.ScalarTableCounterOid != null))
                            {
                                // E' indicato un indice di riga dinamica per la tabella e il relativo Oid da cui recuperare il valore
                                // Nel caso che il contatore delle righe della tabella dinamica sia maggiore o uguale all'indice corrente di riga,
                                // riportato nella definizione, lo stream field dovrà essere salvato su database, altrimenti sarà saltato
                                IList<Variable> tableCounters;

                                if (snmpScalarCache.ContainsKey(field.ScalarTableCounterOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP scalari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(), field.ScalarTableCounterOid));
                                    }

                                    tableCounters = snmpScalarCache[field.ScalarTableCounterOid];
                                }
                                else
                                {
                                    tableCounters = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                        new OctetString(this.SnmpCommunity), new List<Variable> { new Variable(field.ScalarTableCounterOid) });

                                    snmpScalarCache.Add(field.ScalarTableCounterOid, tableCounters);
                                }

                                // Inizializziamo lo stream field in modo che sia saltato durante la persistenza su base dati
                                field.PersistOnDatabase = false;

                                if ((tableCounters != null) && (tableCounters.Count > 0) && (tableCounters[0].Data != null))
                                {
                                    string dynamicTableRowCounterString = SnmpUtility.DecodeDataByType(tableCounters[0]);

                                    if (!string.IsNullOrEmpty(dynamicTableRowCounterString))
                                    {
                                        int dynamicTableRowCounter;

                                        if (int.TryParse(dynamicTableRowCounterString.Trim(), out dynamicTableRowCounter))
                                        {
                                            // Il contatore di righe per la tabella dinamica ha un valore valido
                                            if (field.TableIndex.Value <= dynamicTableRowCounter)
                                            {
                                                // Lo stream field appartiene ad una riga dinamica di tabella con
                                                // indice inferiore o uguale al contatore totale, quindi può essere
                                                // salvato
                                                field.PersistOnDatabase = true;
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region Valutazione di ricerca valore su righe di tabella, su specifica colonna di tabella SNMP, per decidere se persistere o meno lo stream field su DB

                            if ((field.HideIfNotAvailableRowAndColumnIndex != null) && (field.HideIfNotAvailableRowAndColumnIndex.IsValid))
                            {
                                // Sono indicati i parametri per filtrare su una colonna specifica della tabella SNMP indicata:
                                // Oid della tabella su cui eseguire la ricerca, colonna della tabella e espressione regolare da usare per la ricerca
                                // Se si trova una riga rispondente alla ricerca, lo stream field è da persistere, altrimenti no
                                // Serve per creare n stream field fissi in una definizione e scriverli su base dati solamente se si trova la riga corrispondente
                                // L'uso base è quello di indicare sullo stream field la riga corrispondente allo stream field e cercare per l'indice di tabella (normalmente
                                // sulla colonna 0). Se non si trova l'indice, lo stream field non sarà da persistere, perché la tabella SNMP non contiene i dati dichiarati sulla definizione.
                                // Questo caso è più generico rispetto a quello analizzato nel codice del blocco precedente (attributo TableIndexAndScalarCounterOid della definizione),
                                // che si basa sulla presenza di un contatore di righe di tabella esterno alla tabella stessa e indicato da un valore scalare della MIB

                                // Inizializziamo lo stream field in modo che sia saltato durante la persistenza su base dati
                                field.PersistOnDatabase = false;

                                if ((hideIfNotAvailableTable != null) &&
                                    (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableColumnIndex.HasValue) &&
                                    (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableRowRegex != null))
                                {
                                    field.PersistOnDatabase = SnmpUtility.ExistsValueInTableWithColumnFilter(hideIfNotAvailableTable,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableColumnIndex.Value,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableRowRegex,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableSecondColumnIndex,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableRowSecondRegex);
                                }
                            }

                            #endregion

                            if (field.PersistOnDatabase)
                            {
                                // Il recupero dei dati ha senso solamente se lo stream field è da persistere in base dati

                                string snmpValue = null;

                                if (field.TableRowIndex.HasValue)
                                {
                                    // E' stata indicata una riga per l'accesso alla tabella
                                    try
                                    {
                                        snmpValue = SnmpUtility.GetValueFromTableAtRow(table, field.TableRowIndex.Value, field.TableEvaluationFormula);
                                    }
                                    catch (DeviceConfigurationHelperException ex)
                                    {
                                        field.LastError =
                                            new DeviceConfigurationHelperException(
                                                string.Format(CultureInfo.InvariantCulture,
                                                    "Errore caricamento dati SNMP da periferica, StreamField: '{0}', ID: {1}", field.Name, field.FieldId),
                                                ex);
                                    }
                                }
                                else
                                {
                                    if ((field.TableFilterColumnIndex.HasValue) && (field.TableFilterColumnRegex != null))
                                    {
                                        if (field.FieldCorrelationTableOid == null)
                                        {
                                            // Recupero di dati da una tabella, usando un filtro specifico su colonna
                                            // per individuare la prima riga possibile che corrisponde alla regex
                                            try
                                            {
                                                snmpValue = SnmpUtility.GetValueFromTableWithColumnFilter(table, field.TableFilterColumnIndex.Value,
                                                    field.TableFilterColumnRegex, field.TableEvaluationFormula);
                                            }
                                            catch (DeviceConfigurationHelperException ex)
                                            {
                                                field.LastError =
                                                    new DeviceConfigurationHelperException(
                                                        string.Format(CultureInfo.InvariantCulture,
                                                            "Errore caricamento dati SNMP da periferica, StreamField: '{0}', ID: {1}", field.Name,
                                                            field.FieldId), ex);
                                            }
                                        }
                                        else
                                        {
                                            if (field.TableCorrelationFilterColumnIndex.HasValue)
                                            {
                                                // Recupero di dati da una tabella correlata, usando un filtro su colonna indicata ed un valore estratto
                                                // dalla tabella principale, filtrando su una colonna specifica ed applicando una regex
                                                try
                                                {
                                                    bool findExactlyString = field.StreamFieldFather != null && (field.StreamFieldFather.IsStreamFieldArray || (field.StreamFieldFather.StreamFieldFather != null && field.StreamFieldFather.StreamFieldFather.IsStreamFieldArray));
                                                    int lastCorrelationTableRow;
                                                    snmpValue = SnmpUtility.GetValueFromCorrelationTableWithCorrelationValue(table, correlationTable,
                                                        field.TableFilterColumnIndex.Value, field.TableCorrelationFilterColumnIndex.Value,
                                                        field.TableFilterColumnRegex, field.TableEvaluationFormula,
                                                        field.TableCorrelationEvaluationFormula, field.LastTableCorrelationRow, out lastCorrelationTableRow, findExactlyString);
                                                    if ((field.isChild() && field.StreamFieldFather != null && field.StreamFieldFather.IsArray) || (field.isFather() && field.IsArray))
                                                    {
                                                        field.LastTableCorrelationRow = lastCorrelationTableRow + 1;
                                                    }
                                                }
                                                catch (DeviceConfigurationHelperException ex)
                                                {
                                                    field.LastError =
                                                        new DeviceConfigurationHelperException(
                                                            string.Format(CultureInfo.InvariantCulture,
                                                                "Errore caricamento dati SNMP da periferica, StreamField: '{0}', ID: {1}", field.Name,
                                                                field.FieldId), ex);
                                                }
                                            }
                                            else
                                            {
                                                // Recupero di dati da una tabella correlata, usando un indice di riga trovato con una ricerca
                                                // sulla tabella principale, su colonna specifica, usando una regex
                                                try
                                                {
                                                    snmpValue = SnmpUtility.GetValueFromCorrelationTableWithColumnFilter(table, correlationTable,
                                                        field.TableFilterColumnIndex.Value, field.TableFilterColumnRegex,
                                                        field.TableCorrelationEvaluationFormula);
                                                }
                                                catch (DeviceConfigurationHelperException ex)
                                                {
                                                    field.LastError =
                                                        new DeviceConfigurationHelperException(
                                                            string.Format(CultureInfo.InvariantCulture,
                                                                "Errore caricamento dati SNMP da periferica, StreamField: '{0}', ID: {1}", field.Name,
                                                                field.FieldId), ex);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (snmpValue != null)
                                {
                                    IList<Variable> valuesString = new List<Variable>();

                                    // Tutte le query tabellari ritornano un valore stringa già elaborato, che ha perso il tipo originale,
                                    // quindi sono promossi a OctetString, tranne nel caso di NoSuchInstance, che può accadere con le tabelle sparse
                                    valuesString.Add(snmpValue.Equals("NoSuchInstance")
                                        ? new Variable(field.FieldTableOid.TableOid, new NoSuchInstance())
                                        : new Variable(field.FieldTableOid.TableOid, SnmpUtility.FillOctetStringFromRawString(snmpValue)));

                                    field.RawData = valuesString;
                                }
                                else
                                {
                                    if (field.LastError != null)
                                    {
                                        // La severità sarà calcolata internamente allo StreamField,
                                        // basandosi sul fatto che il valore è nullo e l'errore è impostato
                                        field.SeverityLevel = Severity.Warning;
                                        field.ValueDescription = Settings.Default.ErrorOnDataRetrievalMessage;
                                        field.Value = Settings.Default.ErrorOnDataRetrievalValue;
                                    }
                                    else if (field.NoSnmpDataAvailableForcedState != null)
                                    {
                                        field.SeverityLevel = field.NoSnmpDataAvailableForcedState.ForcedSeverity;
                                        field.ValueDescription = field.NoSnmpDataAvailableForcedState.ForcedValueDescription;
                                        field.Value = field.NoSnmpDataAvailableForcedState.ForcedValue;
                                    }
                                    else
                                    {
                                        field.SeverityLevel = Severity.Unknown;
                                        field.ValueDescription = Settings.Default.NoDataAvailableMessage;
                                        field.Value = Settings.Default.NoDataAvailableValue;

                                        if (field.HideIfNoDataAvailable)
                                        {
                                            // Se non ci sono dati validi restituiti dalla query e il campo non deve essere persistito in questo caso, lo forziamo
                                            field.PersistOnDatabase = false;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                field.SeverityLevel = Severity.Unknown;
                                field.ValueDescription = Settings.Default.NoDataAvailableMessage;
                                field.Value = Settings.Default.NoDataAvailableValue;
                            }

                            #endregion
                            #region Gestisco la possibilita di avvere array di dati da una struttura tabellare
                            if (field.Value != Settings.Default.NoDataAvailableValue)
                            {
                                // se il field è padre(ci deve essere un solo padre per array) ed è un array
                                if (field.isFather() && field.IsArray && (field.MaxSizeArray == -1 || field.MaxSizeArray > field.PositionArray + 1))
                                {
                                   field.Clone(stream);
                                } else if(field.isFather() && field.IsStreamFieldArray && (field.MaxSizeArray == -1 || field.MaxSizeArray > field.Offset + 1))
                                {
                                   field.Clone(stream);
                                }
                            }
                            else if (field.Value == Settings.Default.NoDataAvailableValue)
                            {
                                // se il field è padre(ci deve essere un solo padre per array) ed è uno streamfield di array
                                if (field.isFather() && field.IsStreamFieldArray)
                                {
                                    // rimuovo il padre e figli dalla lista
                                    foreach (DBStreamField child in field.DbStreamFieldValueChildren)
                                    {
                                        // rimuovo il padre e figli dalla lista
                                        if (child.DbStreamFieldValueChildren != null)
                                        {
                                            foreach (DBStreamField child2 in child.DbStreamFieldValueChildren)
                                            {
                                                child2.isDiscardData = true;
                                            }
                                        }
                                        child.isDiscardData = true;
                                    }
                                    field.isDiscardData = true;
                                }
                                    // se il field è padre ed è un array semplice
                                else if (field.isFather() && field.IsArray) {
                                    bool existAggregators = false;
                                    // rimuovo il padre e figli dalla lista
                                    foreach (DBStreamField child in field.DbStreamFieldValueChildren)
                                    {
                                        if (child.IsAggregation)
                                            existAggregators = true;
                                        else
                                            child.isDiscardData = true;
                                    }
                                    if(!existAggregators)
                                        field.isDiscardData = true;
                                }
                            }
                            #endregion
                        }
                        else if (field.FieldOid != null)
                        {
                            #region Recupero dati SNMP a valore scalare

                            IList<Variable> values;

                            if (snmpScalarCache.ContainsKey(field.FieldOid))
                            {
                                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                {
                                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                        string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP scalari da cache, Oid: {1}",
                                            Thread.CurrentThread.GetHashCode(), field.FieldOid));
                                }

                                values = snmpScalarCache[field.FieldOid];
                            }
                            else
                            {
                                values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                    new OctetString(this.SnmpCommunity), new List<Variable> { new Variable(field.FieldOid) });

                                snmpScalarCache.Add(field.FieldOid, values);
                            }

                            if ((values == null) || (values.Count == 0) || ((values.Count == 1) && (values[0].Data is NoSuchObject)) ||
                                ((values.Count == 1) && (values[0].Data is NoSuchInstance)))
                            {
                                if (field.NoSnmpDataAvailableForcedState != null)
                                {
                                    field.SeverityLevel = field.NoSnmpDataAvailableForcedState.ForcedSeverity;
                                    field.ValueDescription = field.NoSnmpDataAvailableForcedState.ForcedValueDescription;
                                    field.Value = field.NoSnmpDataAvailableForcedState.ForcedValue;
                                }
                                else
                                {
                                    field.SeverityLevel = Severity.Unknown;
                                    field.ValueDescription = Settings.Default.NoDataAvailableMessage;
                                    field.Value = Settings.Default.NoDataAvailableValue;

                                    if (field.HideIfNoDataAvailable)
                                    {
                                        // Se non ci sono dati validi restituiti dalla query e il campo non deve essere persistito in questo caso, lo forziamo
                                        field.PersistOnDatabase = false;
                                    }
                                }
                            }
                            else
                            {
                                IList<Variable> valuesString = new List<Variable>();
                                String value_str;

                                foreach (Variable snmpValue in values)
                                {
                                   // elabaorazione valore in modo dinamico
                                    if (field.EvaluationFormula != null)
                                    {
                                        value_str = SnmpUtility.GetValueFromSNMPValue(snmpValue, field.EvaluationFormula);
                                        if (value_str != null)
                                        {
                                            valuesString.Add(value_str.Equals("NoSuchInstance")
                                             ? new Variable(field.FieldOid, new NoSuchInstance())
                                             : new Variable(field.FieldOid, SnmpUtility.FillOctetStringFromRawString(value_str)));
                                        }
                                    }
                                    else
                                    {
                                        valuesString.Add(snmpValue);
                                    }
                                }

                                field.RawData = valuesString;
                            }



                            #endregion
                        }
                        else if (field.FixedValueString != null)
                        {
                            #region Recupero eventuale tabella aggiuntiva per filtro persistenza DB per query SNMP tabellare

                            Variable[,] hideIfNotAvailableTable = null;

                            if ((field.HideIfNotAvailableRowAndColumnIndex != null) && (field.HideIfNotAvailableRowAndColumnIndex.IsValid) &&
                                (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid != null))
                            {
                                if (snmpTableCache.ContainsKey(field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP tabellari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(),
                                                field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid));
                                    }

                                    hideIfNotAvailableTable =
                                        snmpTableCache[field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid];
                                }
                                else
                                {
                                    if (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableQueryMode ==
                                        TableMode.Standard)
                                    {
                                        hideIfNotAvailableTable = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode,
                                            this.DeviceIPEndPoint, new OctetString(this.SnmpCommunity),
                                            field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid);
                                    }
                                    else if (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableQueryMode ==
                                             TableMode.Sparse)
                                    {
                                        hideIfNotAvailableTable = this.messengerFactory.GetTableUsingMIBData(this.FakeLocalData, this.SnmpVersionCode,
                                            this.DeviceIPEndPoint, new OctetString(this.SnmpCommunity),
                                            field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid,
                                            (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableColumnCount.HasValue
                                                ? field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableColumnCount
                                                    .Value
                                                : 1), field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableEntry,
                                            field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableIndexColumns);
                                    }

                                    snmpTableCache.Add(field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableFieldTableOid.TableOid,
                                        hideIfNotAvailableTable);
                                }
                            }

                            #endregion

                            #region Valutazione di ricerca valore su righe di tabella, su specifica colonna di tabella SNMP, per decidere se persistere o meno lo stream field su DB

                            if ((field.HideIfNotAvailableRowAndColumnIndex != null) && (field.HideIfNotAvailableRowAndColumnIndex.IsValid))
                            {
                                // Sono indicati i parametri per filtrare su una colonna specifica della tabella SNMP indicata:
                                // Oid della tabella su cui eseguire la ricerca, colonna della tabella e espressione regolare da usare per la ricerca
                                // Se si trova una riga rispondente alla ricerca, lo stream field è da persistere, altrimenti no
                                // Serve per creare n stream field fissi in una definizione e scriverli su base dati solamente se si trova la riga corrispondente
                                // L'uso base è quello di indicare sullo stream field la riga corrispondente allo stream field e cercare per l'indice di tabella (normalmente
                                // sulla colonna 0). Se non si trova l'indice, lo stream field non sarà da persistere, perché la tabella SNMP non contiene i dati dichiarati sulla definizione.
                                // Questo caso è più generico rispetto a quello analizzato nel codice del blocco precedente (attributo TableIndexAndScalarCounterOid della definizione),
                                // che si basa sulla presenza di un contatore di righe di tabella esterno alla tabella stessa e indicato da un valore scalare della MIB

                                // Inizializziamo lo stream field in modo che sia saltato durante la persistenza su base dati
                                field.PersistOnDatabase = false;

                                if ((hideIfNotAvailableTable != null) &&
                                    (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableColumnIndex.HasValue) &&
                                    (field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableRowRegex != null))
                                {
                                    field.PersistOnDatabase = SnmpUtility.ExistsValueInTableWithColumnFilter(hideIfNotAvailableTable,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableColumnIndex.Value,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableRowRegex,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableSecondColumnIndex,
                                        field.HideIfNotAvailableRowAndColumnIndex.TableHideIfNotAvailableRowSecondRegex);
                                }
                            }

                            #endregion

                            if (field.PersistOnDatabase)
                            {
                                // Nel caso di valore fisso diretto da definizione, simuliamo la creazione di una variabile SNMP con Oid fittizio
                                IList<Variable> valuesString = new List<Variable>();
                                valuesString.Add(new Variable(new ObjectIdentifier(new uint[] { 1, 3 }), new OctetString(field.FixedValueString)));

                                field.RawData = valuesString;
                            }
                        }

                        #region Elaborazione recupero dati SNMP scalari per eventuali range di valutazione basati su Oid

                        foreach (DBStreamFieldValueDescription valueDescription in field.DbStreamFieldValueDescriptions)
                        {
                            if (valueDescription.RangeValueToCompare.MinValueOid != null)
                            {
                                #region Richieste SNMP scalari per popolare il valore minimo del range basato su Oid

                                IList<Variable> values;

                                if (snmpScalarCache.ContainsKey(valueDescription.RangeValueToCompare.MinValueOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP scalari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(), valueDescription.RangeValueToCompare.MinValueOid));
                                    }

                                    values = snmpScalarCache[valueDescription.RangeValueToCompare.MinValueOid];
                                }
                                else
                                {
                                    values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                        new OctetString(this.SnmpCommunity),
                                        new List<Variable> { new Variable(valueDescription.RangeValueToCompare.MinValueOid) });

                                    snmpScalarCache.Add(valueDescription.RangeValueToCompare.MinValueOid, values);
                                }

                                if ((values == null) || (values.Count == 0))
                                {
                                    valueDescription.RangeValueToCompare.SetMinValue(null);
                                }
                                else
                                {
                                    IList<Variable> valuesString = new List<Variable>();

                                    foreach (Variable snmpValue in values)
                                    {
                                        valuesString.Add(snmpValue);
                                    }

                                    valueDescription.RangeValueToCompare.SetMinValue(valuesString);
                                }

                                #endregion
                            }
                            else if (valueDescription.RangeValueToCompare.MinTableValueOid != null)
                            {
                                #region Recupero dati SNMP tabellari per popolare il valore minimo del range basato su Oid

                                #region Recupero tabella principale query SNMP tabellare

                                Variable[,] table;

                                if (snmpTableCache.ContainsKey(valueDescription.RangeValueToCompare.MinTableValueOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP tabellari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(), valueDescription.RangeValueToCompare.MinTableValueOid));
                                    }

                                    table = snmpTableCache[valueDescription.RangeValueToCompare.MinTableValueOid];
                                }
                                else
                                {
                                    table = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                        new OctetString(this.SnmpCommunity), valueDescription.RangeValueToCompare.MinTableValueOid);

                                    snmpTableCache.Add(valueDescription.RangeValueToCompare.MinTableValueOid, table);
                                }

                                #endregion

                                string snmpValue = null;

                                if ((valueDescription.RangeValueToCompare.MinTableFilterColumnIndex.HasValue) &&
                                    (valueDescription.RangeValueToCompare.MinTableFilterColumnRegex != null))
                                {
                                    // Recupero di dati da una tabella, usando un filtro specifico su colonna
                                    // per individuare la prima riga possibile che corrisponde alla regex
                                    try
                                    {
                                        snmpValue = SnmpUtility.GetValueFromTableWithColumnFilter(table,
                                            valueDescription.RangeValueToCompare.MinTableFilterColumnIndex.Value,
                                            valueDescription.RangeValueToCompare.MinTableFilterColumnRegex,
                                            valueDescription.RangeValueToCompare.MinTableEvaluationFormula);
                                    }
                                    catch (DeviceConfigurationHelperException ex)
                                    {
                                        field.LastError =
                                            new DeviceConfigurationHelperException(
                                                string.Format(CultureInfo.InvariantCulture,
                                                    "Errore caricamento dati SNMP da periferica, StreamField: '{0}', ID: {1}, FieldValueDescription Oid Tabellare Minimo: {2}",
                                                    field.Name, field.FieldId, valueDescription.RangeValueToCompare.MinTableValueOid), ex);
                                    }
                                }

                                if (snmpValue != null)
                                {
                                    IList<Variable> valuesString = new List<Variable>();

                                    // Tutte le query tabellari ritornano un valore stringa già elaborato, che ha perso il tipo originale,
                                    // quindi sono promossi a OctetString, tranne nel caso di NoSuchInstance, che può accadere con le tabelle sparse
                                    valuesString.Add(snmpValue.Equals("NoSuchInstance")
                                        ? new Variable(valueDescription.RangeValueToCompare.MinTableValueOid, new NoSuchInstance())
                                        : new Variable(valueDescription.RangeValueToCompare.MinTableValueOid,
                                            SnmpUtility.FillOctetStringFromRawString(snmpValue)));

                                    valueDescription.RangeValueToCompare.SetMinValue(valuesString);
                                }
                                else
                                {
                                    valueDescription.RangeValueToCompare.SetMinValue(null);
                                }

                                #endregion
                            }

                            if (valueDescription.RangeValueToCompare.MaxValueOid != null)
                            {
                                #region Richieste SNMP scalari per popolare il valore massimo del range basato su Oid

                                IList<Variable> values;

                                if (snmpScalarCache.ContainsKey(valueDescription.RangeValueToCompare.MaxValueOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP scalari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(), valueDescription.RangeValueToCompare.MaxValueOid));
                                    }

                                    values = snmpScalarCache[valueDescription.RangeValueToCompare.MaxValueOid];
                                }
                                else
                                {
                                    values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                        new OctetString(this.SnmpCommunity),
                                        new List<Variable> { new Variable(valueDescription.RangeValueToCompare.MaxValueOid) });

                                    snmpScalarCache.Add(valueDescription.RangeValueToCompare.MaxValueOid, values);
                                }

                                if ((values == null) || (values.Count == 0))
                                {
                                    valueDescription.RangeValueToCompare.SetMaxValue(null);
                                }
                                else
                                {
                                    IList<Variable> valuesString = new List<Variable>();

                                    foreach (Variable snmpValue in values)
                                    {
                                        valuesString.Add(snmpValue);
                                    }

                                    valueDescription.RangeValueToCompare.SetMaxValue(valuesString);
                                }

                                #endregion
                            }
                            else if (valueDescription.RangeValueToCompare.MaxTableValueOid != null)
                            {
                                #region Recupero dati SNMP tabellari per popolare il valore massimo del range basato su Oid

                                #region Recupero tabella principale query SNMP tabellare

                                Variable[,] table;

                                if (snmpTableCache.ContainsKey(valueDescription.RangeValueToCompare.MaxTableValueOid))
                                {
                                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                                    {
                                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP tabellari da cache, Oid: {1}",
                                                Thread.CurrentThread.GetHashCode(), valueDescription.RangeValueToCompare.MaxTableValueOid));
                                    }

                                    table = snmpTableCache[valueDescription.RangeValueToCompare.MaxTableValueOid];
                                }
                                else
                                {
                                    table = this.messengerFactory.GetSnmpTable(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                                        new OctetString(this.SnmpCommunity), valueDescription.RangeValueToCompare.MaxTableValueOid);

                                    snmpTableCache.Add(valueDescription.RangeValueToCompare.MaxTableValueOid, table);
                                }

                                #endregion

                                string snmpValue = null;

                                if ((valueDescription.RangeValueToCompare.MaxTableFilterColumnIndex.HasValue) &&
                                    (valueDescription.RangeValueToCompare.MaxTableFilterColumnRegex != null))
                                {
                                    // Recupero di dati da una tabella, usando un filtro specifico su colonna
                                    // per individuare la prima riga possibile che corrisponde alla regex
                                    try
                                    {
                                        snmpValue = SnmpUtility.GetValueFromTableWithColumnFilter(table,
                                            valueDescription.RangeValueToCompare.MaxTableFilterColumnIndex.Value,
                                            valueDescription.RangeValueToCompare.MaxTableFilterColumnRegex,
                                            valueDescription.RangeValueToCompare.MaxTableEvaluationFormula);
                                    }
                                    catch (DeviceConfigurationHelperException ex)
                                    {
                                        field.LastError =
                                            new DeviceConfigurationHelperException(
                                                string.Format(CultureInfo.InvariantCulture,
                                                    "Errore caricamento dati SNMP da periferica, StreamField: '{0}', ID: {1}, FieldValueDescription Oid Tabellare Massimo: {2}",
                                                    field.Name, field.FieldId, valueDescription.RangeValueToCompare.MaxTableValueOid), ex);
                                    }
                                }

                                if (snmpValue != null)
                                {
                                    IList<Variable> valuesString = new List<Variable>();

                                    // Tutte le query tabellari ritornano un valore stringa già elaborato, che ha perso il tipo originale,
                                    // quindi sono promossi a OctetString, tranne nel caso di NoSuchInstance, che può accadere con le tabelle sparse
                                    valuesString.Add(snmpValue.Equals("NoSuchInstance")
                                        ? new Variable(valueDescription.RangeValueToCompare.MaxTableValueOid, new NoSuchInstance())
                                        : new Variable(valueDescription.RangeValueToCompare.MaxTableValueOid,
                                            SnmpUtility.FillOctetStringFromRawString(snmpValue)));

                                    valueDescription.RangeValueToCompare.SetMaxValue(valuesString);
                                }
                                else
                                {
                                    valueDescription.RangeValueToCompare.SetMaxValue(null);
                                }

                                #endregion
                            }
                        }

                        #endregion
                    }
            }

            if (this.SerialNumberOid != null)
            {
                #region Recupero dati SNMP a valore scalare, relativo al numero di serie della periferica

                IList<Variable> values;

                if (snmpScalarCache.ContainsKey(this.SerialNumberOid))
                {
                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati SNMP scalari da cache, Oid: {1}",
                                Thread.CurrentThread.GetHashCode(), this.SerialNumberOid));
                    }

                    values = snmpScalarCache[this.SerialNumberOid];
                }
                else
                {
                    values = this.messengerFactory.GetSnmpData(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                        new OctetString(this.SnmpCommunity), new List<Variable> {new Variable(this.SerialNumberOid)});

                    snmpScalarCache.Add(this.SerialNumberOid, values);
                }

                if ((values != null) && (values.Count > 0) && (values[OID_SERIAL_NUMBER_INDEX].Data != null))
                {
                    string value = SnmpUtility.DecodeDataByType(values[OID_SERIAL_NUMBER_INDEX]);

                    if (!string.IsNullOrEmpty(value))
                    {
                        if ((value.Trim().Length > 0) && (!this.Device.SerialNumber.Equals(value.Trim(), StringComparison.OrdinalIgnoreCase)))
                        {
                            this.Device.SerialNumber = value.Trim();
                            this.Device.IsSerialNumberForced = true;
                        }
                    }
                }

                #endregion
            }

            if ( this.messengerFactory.IsDataRetrieved )
            {
                this.IsDataRetrieved = true;
            }
        }

        private void CheckStatus()
        {
            this.GetIsAliveIcmp();

            if (this.CheckIfHasMib2)
            {
                this.GetIsAliveSnmp();
            }
            else {
                this.IsAliveSnmp = true;
            }
        }

        private void GetIsAliveIcmp()
        {
            this.IsAliveIcmp = this.messengerFactory.GetPingResponse(this.FakeLocalData, this.DeviceIPEndPoint);
        }

        private void GetIsAliveSnmp()
        {
            bool returnValue = false;

            IList<Variable> snmpData = this.messengerFactory.GetAliveSnmp(this.FakeLocalData, this.SnmpVersionCode, this.DeviceIPEndPoint,
                new OctetString(this.SnmpCommunity), new List<Variable> { this.SnmpAliveOid == null ? new Variable(this.oidSysName) : new Variable(this.SnmpAliveOid) });

            if ((snmpData != null) && (snmpData.Count > 0) && (snmpData[OID_SYS_NAME_INDEX].Data != null))
            {
                string value = SnmpUtility.DecodeDataByType(snmpData[OID_SYS_NAME_INDEX]);

                if (!string.IsNullOrEmpty(value))
                {
                    if (value.Trim().Length > 0)
                    {
                        returnValue = true;
                    }
                }
            }

            this.IsAliveSnmp = returnValue;
        }

        /// <summary>
        ///     Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        private void PopulateInternal()
        {
            this.LastError = null;

            if (this.IsAliveSnmp)
            {
                this.RetrieveData();
            }

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate(this.IsAliveSnmp);
                    }
                }
            }
            else
            {
                this.isPopulated = false;
            }
        }

        /// <summary>
        ///     Calcola la severità della periferica e ne valorizza gli stati possibili
        /// </summary>
        /// <returns></returns>
        private Severity CalculateSeverity()
        {
            return SeverityCalculators.CalculateSnmpDeviceSeverity(this, this.DynamicCodeCalculateSeverity);
        }

        /// <summary>
        ///     Imposta le tacitazioni su periferica, stream e stream field, in base alla configurazione ricevuta da base dati
        /// </summary>
        private void SetDeviceAcknowledgements()
        {
            if (this.Device.DeviceAck != null)
            {
                #region Tacitazione di periferica

                if (this.Device.DeviceAck.IsDeviceAcknowledged)
                {
                    this.ExcludedFromSeverityComputation = true;
                }

                #endregion

                #region Tacitazione stream

                foreach (DeviceStreamAcknowledgement streamAcknowledgement in this.Device.DeviceAck.DeviceStreamAcknowledgements)
                {
                    foreach (DBStream stream in this.StreamData)
                    {
                        if (stream.StreamId == streamAcknowledgement.StreamId)
                        {
                            stream.ExcludedFromSeverityComputation = true;
                            break;
                        }
                    }
                }

                #endregion

                #region Tacitazione stream field

                foreach (DeviceStreamFieldAcknowledgement streamFieldAcknowledgement in
                    this.Device.DeviceAck.DeviceStreamFieldAcknowledgements)
                {
                    foreach (DBStream stream in this.StreamData)
                    {
                        if (stream.StreamId == streamFieldAcknowledgement.StreamId)
                        {
                            foreach (DBStreamField streamField in stream.FieldData)
                            {
                                if (streamField.FieldId == streamFieldAcknowledgement.StreamFieldId)
                                {
                                    streamField.ExcludedFromSeverityComputation = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                #endregion
            }
        }

        /// <summary>
        ///     Con le tacitazioni già impostate nella periferica, stream e stream field, esclude tutti gli eventuali invii di mail
        ///     possibili
        /// </summary>
        private void CalculateDeviceShouldSendNotificationByEmailUsingAck()
        {
            if (this.ExcludedFromSeverityComputation)
            {
                // Se è tacitata l'intera periferica, tutti gli Stream Field contenuti saranno resettati per non inviare mail
                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField streamField in stream.FieldData)
                    {
                        streamField.ShouldSendNotificationByEmail = false;
                    }
                }
            }
            else
            {
                // Non è tacitata la periferica, quindi elabora gli Stream singolarmente
                foreach (DBStream stream in this.StreamData)
                {
                    if (stream.ExcludedFromSeverityComputation)
                    {
                        // Se è tacitato lo Stream, resettiamo tutti gli Stream Field interni
                        foreach (DBStreamField streamField in stream.FieldData)
                        {
                            streamField.ShouldSendNotificationByEmail = false;
                        }
                    }
                    else
                    {
                        // Se non è tacitato lo Stream, cerchiamo gli Stream Field esclusi e resettiamo solo quelli
                        foreach (DBStreamField streamField in stream.FieldData)
                        {
                            if (streamField.ExcludedFromSeverityComputation)
                            {
                                streamField.ShouldSendNotificationByEmail = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Verifica su tutti gli Stream Field esistenti, se ne esiste almeno uno che indica di inviare la mail
        /// </summary>
        private void AggregateShouldSendNotificationByEmailForDevice()
        {
            bool foundShouldSendNotificationByEmail = false;

            foreach (DBStream stream in this.StreamData)
            {
                foreach (DBStreamField streamField in stream.FieldData)
                {
                    if (streamField.ShouldSendNotificationByEmail)
                    {
                        this.ShouldSendNotificationByEmail = true;
                        foundShouldSendNotificationByEmail = true;
                        break;
                    }
                }

                if (foundShouldSendNotificationByEmail)
                {
                    break;
                }
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        ///     Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        public void Populate()
        {
            this.LastError = null;

            this.SetDeviceAcknowledgements();

            if (this.IsAliveSnmp)
            {
                this.RetrieveData();
            }

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate(this.IsAliveSnmp);
                    }
                }

                if (Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem > 0)
                {
                    // E' attivo il tentativo di recupero in caso di severità non normale della periferica
                    // Tenta di recuperare una periferica in condizione di errore, ma non permanente (fluttuazione di stato)

                    // C'è già stato un tentativo, quello principale, quindi il prossimo sarà il secondo
                    ushort retryCounter = 2;

                    while (retryCounter <= Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem)
                    {
                        if (this.SeverityLevel != Severity.Ok)
                        {
                            Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenRetriesToRecoverFromDeviceSeverityProblem);
                            this.PopulateInternal();
                            retryCounter++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                // Resettiamo gli eventuali flag di invio mail per gli elementi tacitati, dopo che sono state calcolate le severità
                this.CalculateDeviceShouldSendNotificationByEmailUsingAck();
            }
            else
            {
                this.isPopulated = false;
            }

            // Il calcolo della severità permette di avere tutti gli stati possibili impostati e gli stream valutati
            this.severity = this.CalculateSeverity();

            this.AggregateShouldSendNotificationByEmailForDevice();
        }

        /// <summary>
        ///     Produce una rappresentazione completa e descrittiva della periferica a fini diagnostici
        /// </summary>
        /// <returns>Rappresentazione completa e descrittiva della periferica a fini diagnostici</returns>
        public string Dump()
        {
            if ((!this.isPopulated) && (this.LastError == null))
            {
                // Se ci sono errori impostati, è già stato tentato un popolamento, quindi è inutile ritentare
                this.Populate();
            }

            StringBuilder dump = new StringBuilder();

            if (this.hasDynamicLoadingRules)
            {
                dump.AppendFormat(CultureInfo.InvariantCulture,
                    "Device Name: {0}, Type: {1}, Community: {2}, SNMP Version: {3}, Offline: {4}, Severity: {5}, ReplyICMP: {6}, ReplySNMP: {7}, Device Category: {8}, Device with dynamic loading rules enabled{9}{10}" +
                    Environment.NewLine, this.DeviceName, this.DeviceType, this.SnmpCommunity, this.SnmpVersionCode, this.Offline,
                    (int) this.SeverityLevel, this.IsAliveIcmp, this.IsAliveSnmp, this.deviceCategory,
                    ((this.NoMatchDeviceStatus != null)
                        ? string.Format(CultureInfo.InvariantCulture, ", NoMatchDeviceStatusSeverity: {0}, NoMatchDeviceStatusDescription: {1}",
                            this.NoMatchDeviceStatus.NoMatchDeviceStatusSeverity, this.NoMatchDeviceStatus.NoMatchDeviceStatusDescription)
                        : string.Empty),
                    ((this.Device.IsSerialNumberForced)
                        ? string.Format(", Forced serial number by SNMP query: {0}", this.Device.SerialNumber)
                        : string.Empty));
                dump.AppendFormat("Dynamic Definition File: {0}" + Environment.NewLine, Path.GetFileName(this.DefinitionFileName));
            }
            else
            {
                dump.AppendFormat(CultureInfo.InvariantCulture,
                    "Device Name: {0}, Type: {1}, Community: {2}, SNMP Version: {3}, Offline: {4}, Severity: {5}, ReplyICMP: {6}, ReplySNMP: {7}{8}" +
                    Environment.NewLine, this.DeviceName, this.DeviceType, this.SnmpCommunity, this.SnmpVersionCode, this.Offline,
                    (int) this.SeverityLevel, this.IsAliveIcmp, this.IsAliveSnmp,
                    ((this.Device.IsSerialNumberForced)
                        ? string.Format(", Forced serial number by SNMP query: {0}", this.Device.SerialNumber)
                        : string.Empty));
            }

            foreach (DBStream stream in this.StreamData)
            {
                dump.AppendFormat(CultureInfo.InvariantCulture, "Stream Id: {0}, Name: {1}, Severity: {2}, Description: {3}" + Environment.NewLine,
                    stream.StreamId, stream.Name, (int) stream.SeverityLevel, stream.ValueDescriptionComplete);

                foreach (DBStreamField field in stream.FieldData)
                {
                    if (field.PersistOnDatabase)
                    {
                        dump.AppendFormat(CultureInfo.InvariantCulture,
                            "Field Id: {0}, Array Id: {1}, Name: {2}, Severity: {3}, Value: {4}, Description: {5}{6}" + Environment.NewLine,
                            field.FieldId, field.ArrayId, field.Name, (int) field.SeverityLevel, field.Value, field.ValueDescriptionComplete,
                            (field.LastError != null
                                ? string.Format(CultureInfo.InvariantCulture, ", Exception: {0}, Inner Exception: {1}", field.LastError.Message,
                                    (field.LastError.InnerException != null) ? field.LastError.InnerException.Message : "N/A")
                                : string.Empty));
                    }
                }
            }

            return dump.ToString();
        }

        #endregion
    }
}
﻿namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Rappresenta gli operatori supportati nelle regole di valutazione singole per caricamento dinamico di definizioni
    /// </summary>
    public enum QueryEvaluateRuleDefinitionOperator
    {
        /// <summary>
        /// Non definito
        /// </summary>
        Unknown,
        /// <summary>
        ///   Risposta di qualsiasi tipo alla query (diversa da NoSuchObject)
        /// </summary>
        Exists
    }
}
﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///   Indica l'associazione tra un Object ID di periferica ed un file di definizione
    /// </summary>
    public class QueryEvaluateRuleDefinition
    {
        /// <summary>
        /// OID della query SNMP su cui effettuare la valutazione della regola
        /// </summary>
        public ObjectIdentifier QueryOid { get; private set; }

        /// <summary>
        /// Operatore che indica come valutare le risposte alla query SNMP
        /// </summary>
        public QueryEvaluateRuleDefinitionOperator QueryOperator { get; private set; }

        /// <summary>
        ///   Nome del file con la definizione relativa
        /// </summary>
        public string DefinitionFile { get; private set; }

        /// <summary>
        /// Nome di periferica da forzare, nel caso si usi questa regola
        /// </summary>
        public string ForcedDeviceType { get; private set; }

        /// <summary>
        /// Indica se l'oggetto è stato correttamente inizializzato e se contiene dati validi
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "queryOidAndOperator">Stringa che contiene l'Oid della query SNMP su cui effettuare la valutazione, l'operatore di valutazione ed eventuali valori di confronto</param>
        /// <param name = "definitionFile">Nome del file con la definizione relativa</param>
        /// <param name="forcedDeviceType">Nome di periferica da forzare, nel caso si usi questa regola</param>
        public QueryEvaluateRuleDefinition(string queryOidAndOperator, string definitionFile, string forcedDeviceType)
        {
            this.IsValid = false;

            if (string.IsNullOrEmpty(queryOidAndOperator))
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                          "L'attributo 'QueryOidAndOperator' è vuoto o privo di valore. Valore obbligatorio. Valore contenuto: '{0}'",
                                                          queryOidAndOperator));
            }

            if (string.IsNullOrEmpty(definitionFile))
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                          "L'attributo 'DefinitionFile' è vuoto o privo di valore. Valore obbligatorio. Valore contenuto: '{0}'",
                                                          queryOidAndOperator));
            }

            this.DefinitionFile = definitionFile;
            this.ForcedDeviceType = forcedDeviceType;
            this.ParseQueryOidAndOperator(queryOidAndOperator);
        }

        private void ParseQueryOidAndOperator(string queryOidAndOperator)
        {
            ObjectIdentifier queryOid;
            QueryEvaluateRuleDefinitionOperator queryOperator;

            string queryOidAndOperatorString = queryOidAndOperator.Trim();

            if (!string.IsNullOrEmpty(queryOidAndOperatorString))
            {
                string[] values = queryOidAndOperatorString.Split(',');

                if (values.Length >= 2)
                {
                    #region Parsing Oid relativo alla query su cui fare la valutazione

                    string oidString = values[0].Trim();

                    if (!string.IsNullOrEmpty(oidString))
                    {
                        queryOid = SnmpUtility.TryParseOid(oidString);

                        if (queryOid == null)
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                      "La prima parte dell'attributo 'QueryOidAndOperator', non contiene un Oid SNMP valido. Valore contenuto: '{0}'",
                                                                      values[0].Trim()));
                        }
                    }
                    else
                    {
                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                  "La prima parte dell'attributo 'QueryOidAndOperator', non contiene un Oid SNMP valido. Valore contenuto: '{0}'",
                                                                  values[0].Trim()));
                    }

                    #endregion

                    #region Parsing enumerato che indica il tipo di valutazione da fare sul valore

                    object queryOperatorRule;

                    try
                    {
                        queryOperatorRule = Enum.Parse(typeof (QueryEvaluateRuleDefinitionOperator), values[1].Trim(), true);
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                  "La seconda parte dell'attributo 'QueryOidAndOperator', non contiene un nome di operatore rinosciuto. Valore contenuto: '{0}'",
                                                                  values[1].Trim()));
                    }

                    if (queryOperatorRule is QueryEvaluateRuleDefinitionOperator)
                    {
                        queryOperator = (QueryEvaluateRuleDefinitionOperator) queryOperatorRule;
                    }
                    else
                    {
                        throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                                  "La seconda parte dell'attributo 'QueryOidAndOperator', non contiene un nome di operatore rinosciuto. Valore contenuto: '{0}'",
                                                                  values[1].Trim()));
                    }

                    #endregion
                }
                else
                {
                    throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                              "L'attributo 'QueryOidAndOperator' non è nel formato atteso 'Oid query SNMP su cui effettuare la valutazione, operatore di valutazione', ad esempio '.1.3.6.1.4.1.23273.4.1.1.0,Exists'. Valore contenuto: '{0}'",
                                                              queryOidAndOperator));
                }
            }
            else
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture,
                                                          "L'attributo 'QueryOidAndOperator' è vuoto o privo di valore. Valore obbligatorio. Valore contenuto: '{0}'",
                                                          queryOidAndOperator));
            }

            if (queryOperator != QueryEvaluateRuleDefinitionOperator.Unknown)
            {
                this.QueryOid = queryOid;
                this.QueryOperator = queryOperator;

                this.IsValid = true;
            }
        }

        /// <summary>
        /// Valuta il dato SNMP in base ad un operatore fornito
        /// </summary>
        /// <param name="queryValue">Variabile SNMP che contiene il dato grezzo da valutare</param>
        /// <param name="queryOperator">Operatore che indica il tipo di valutazione da eseguire</param>
        /// <returns></returns>
        internal static bool IsMatchEvaluateQueryWithOperator(Variable queryValue, QueryEvaluateRuleDefinitionOperator queryOperator)
        {
            return IsMatchEvaluateQueryWithOperator(queryValue, queryOperator, null);
        }

        /// <summary>
        /// Valuta il dato SNMP in base ad un operatore fornito
        /// </summary>
        /// <param name="queryValue">Variabile SNMP che contiene il dato grezzo da valutare</param>
        /// <param name="queryOperator">Operatore che indica il tipo di valutazione da eseguire</param>
        /// <param name="comparisonValues">Lista di valori stringa da usare nelle valutazioni</param>
        /// <returns></returns>
        internal static bool IsMatchEvaluateQueryWithOperator(Variable queryValue, QueryEvaluateRuleDefinitionOperator queryOperator,
                                                       ReadOnlyCollection<string> comparisonValues)
        {
            bool isMatch = false;

            switch (queryOperator)
            {
                case QueryEvaluateRuleDefinitionOperator.Exists:
                    // Verifica che il dato SNMP contenga un valore qualsiasi - discrimina le chiamate SNMP valide da quelle su Oid non esistenti
                    if ((queryValue != null) && (queryValue.Data != null) && (queryValue.Data.TypeCode != SnmpType.NoSuchObject))
                    {
                        isMatch = true;
                    }
                    break;
                case QueryEvaluateRuleDefinitionOperator.Unknown:
                    isMatch = false;
                    break;
            }

            return isMatch;
        }
    }
}
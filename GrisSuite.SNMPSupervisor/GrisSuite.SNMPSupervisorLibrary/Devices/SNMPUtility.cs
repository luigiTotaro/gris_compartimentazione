﻿using System;
using System.CodeDom.Compiler;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;
using Microsoft.CSharp;
using System.Threading;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    internal static class SnmpUtility
    {
        private const string DYNAMIC_ASSEMBLY_FILENAME_PREFIX = "DynamicSnmpTableEvaluation";
        private const string DYNAMIC_ASSEMBLY_FOLDER = "DynamicAssemblies";
        private static readonly object locker = new object();

        #region DecodeDataByType - Produce una rappresentazione stringa di un tipo dati SNMP

        /// <summary>
        ///     Produce una rappresentazione stringa di un tipo dati SNMP
        /// </summary>
        /// <param name="inputData">Variabile SNMP</param>
        /// <returns>Rappresentazione interpretata della variabile</returns>
        internal static string DecodeDataByType(Variable inputData, bool timeTicksMs = false)
        {
            string data;
            switch (inputData.Data.TypeCode)
            {
                case SnmpType.TimeTicks:
                    data = DecodeTimeTicksData(inputData, TimeTicksFormat.Simple, timeTicksMs);
                    break;
                case SnmpType.NoSuchObject:
                    data = null;
                    break;
                case SnmpType.NoSuchInstance:
                    data = null;
                    break;
                case SnmpType.Null:
                    data = null;
                    break;
                case SnmpType.OctetString:
                    data = Encoding.GetEncoding("iso-8859-1").GetString(((OctetString)inputData.Data).GetRaw());
                    break;
                default:
                    data = inputData.Data.ToString();
                    break;
            }

            return data;
        }

        #endregion

        #region GetRawStringData - Produce una rappresentazione stringa di un tipo dati SNMP, senza nessuna decodifica in base al tipo dati

        /// <summary>
        ///     Produce una rappresentazione stringa di un tipo dati SNMP, senza nessuna decodifica in base al tipo dati
        /// </summary>
        /// <param name="inputData">Variabile SNMP</param>
        /// <returns>Rappresentazione della variabile</returns>
        internal static string GetRawStringData(Variable inputData)
        {
            string data;
            switch (inputData.Data.TypeCode)
            {
                case SnmpType.NoSuchObject:
                    data = null;
                    break;
                case SnmpType.NoSuchInstance:
                    data = null;
                    break;
                case SnmpType.Null:
                    data = null;
                    break;
                case SnmpType.OctetString:
                    data = Encoding.GetEncoding("iso-8859-1").GetString(((OctetString)inputData.Data).GetRaw());
                    break;
                default:
                    data = inputData.Data.ToString();
                    break;
            }

            return data;
        }

        #endregion

        #region GetDateAndTimeFromData - Produce una rappresentazione stringa di un tipo dati SNMP OctetString, codificato come DateAndTime SNMP

        /// <summary>
        ///     Produce una rappresentazione stringa di un tipo dati SNMP OctetString, codificato come DateAndTime SNMP
        /// </summary>
        /// <param name="inputData">Variabile SNMP</param>
        /// <returns>Rappresentazione della variabile</returns>
        internal static DateTime? GetDateAndTimeFromData(Variable inputData)
        {
            DateTime? data;
            switch (inputData.Data.TypeCode)
            {
                case SnmpType.NoSuchObject:
                    data = null;
                    break;
                case SnmpType.NoSuchInstance:
                    data = null;
                    break;
                case SnmpType.Null:
                    data = null;
                    break;
                case SnmpType.OctetString:
                    // 2008-11-01T19:35:00.0000000-07:00
                    // 07DE 05 16 13 01 11 00 2B 0000
                    /*
                      1      1-2   year*                     0..65536
                      2       3    month                     1..12
                      3       4    day                       1..31
                      4       5    hour                      0..23
                      5       6    minutes                   0..59
                      6       7    seconds                   0..60
                                   (use 60 for leap-second)
                      7       8    deci-seconds              0..9
                      8       9    direction from UTC        '+' / '-'
                      9      10    hours from UTC*           0..13
                     10      11    minutes from UTC          0..59
                    */

                    byte[] bytes = ((OctetString)inputData.Data).GetRaw();

                    if (bytes.Length == 8 || bytes.Length == 11)
                    {
                        ushort year = (ushort)((bytes[0] << 8) + bytes[1]);
                        byte month = bytes[2];
                        byte day = bytes[3];
                        byte hour = bytes[4];
                        byte minutes = bytes[5];
                        byte seconds = bytes[6];
                        byte deciseconds = bytes[7];
                        char direction = ' ';
                        byte hoursFromUTC = 0;
                        byte minutesFromUTC = 0;

                        if (bytes.Length == 11) {
                            direction = (char)bytes[8];
                            hoursFromUTC = bytes[9];
                            minutesFromUTC = bytes[10];
                        }

                        DateTime dateTime;
                        if (
                            bytes.Length == 11 && DateTime.TryParse(
                                String.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}.{6:0000000}{7}{8:00}{9:00}", year, month, day, hour,
                                    minutes, seconds, deciseconds, direction, hoursFromUTC, minutesFromUTC), out dateTime))
                        {
                            data = dateTime;
                        }
                        else if(bytes.Length == 8 && DateTime.TryParse(
                                String.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}.{6:0000000}", year, month, day, hour,
                                    minutes, seconds, deciseconds), out dateTime))
                        {
                           data = dateTime;
                        }
                        else
                        {
                            data = null;
                        }
                    }
                    else
                    {
                        data = null;
                    }

                    break;
                default:
                    data = null;
                    break;
            }

            return data;
        }

        #endregion

        #region Funzioni per la gestione OctetString codificati come bytes

        /// <summary>
        ///     Ritorna un array di byte relativi ad una stringa codificata in esadecimale
        /// </summary>
        /// <param name="hexString">Stringa </param>
        /// <returns>Array di byte</returns>
        internal static byte[] ConvertHexStringToByteArray(string hexString)
        {
            if (hexString == null)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale nulla"));
            }

            if (hexString.Length == 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale vuota"));
            }

            if (hexString.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
            {
                hexString = hexString.Substring(2);
            }

            hexString = hexString.ToUpper();

            if (hexString.Length < 2)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale non valida: {0}", hexString));
            }

            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale di lunghezza dispari: {0}",
                    hexString));
            }

            byte[] hexAsBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < hexAsBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                hexAsBytes[index] = Byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return hexAsBytes;
        }

        /// <summary>
        ///     Converte una stringa in un array di byte
        /// </summary>
        /// <param name="data">Dati stringa</param>
        /// <returns>Array di byte</returns>
        internal static byte[] ConvertStringToByteArray(string data)
        {
            if (data == null)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa nulla"));
            }

            if (data.Length == 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa vuota"));
            }

            char[] chars = data.ToCharArray();

            byte[] bytes = new byte[chars.Length];
            for (int index = 0; index < bytes.Length; index++)
            {
                bytes[index] = (byte)chars[index];
            }

            return bytes;
        }

        /// <summary>
        ///     Converte i byte di una stringa, nella rappresentazione esadecimale
        /// </summary>
        /// <param name="data">Dati stringa</param>
        /// <returns>Rappresentazione esadecimale della stringa</returns>
        internal static string ConvertStringCharsToHexString(string data)
        {
            if (data == null)
            {
                return String.Empty;
            }

            if (data.Length == 0)
            {
                return String.Empty;
            }

            StringBuilder hexString = new StringBuilder();
            hexString.Append("0x");

            foreach (char c in data)
            {
                hexString.AppendFormat("{0:X2}", (int)c);
            }

            return hexString.ToString();
        }

        /// <summary>
        ///     Converte una stringa in formato byte, nel valore a 64 bit senza segno. Usata per convertire i Bits in OctetString,
        ///     nel formato dal bit meno significativo al più significativo.
        /// </summary>
        /// <param name="s">Stringa in ingresso</param>
        /// <param name="result">Valore ULong di ritorno</param>
        /// <returns>True se la conversione è riuscita, false altrimenti</returns>
        internal static bool TryParseULongString(string s, out ulong result)
        {
            bool canParse = false;
            result = 0;

            try
            {
                byte[] data = ConvertStringToByteArray(s);
                byte[] data64 = new byte[8];

                for (int counter = 0; counter <= 7; counter++)
                {
                    if (counter < data.Length)
                    {
                        data64[counter] = data[counter];
                    }
                    else
                    {
                        data64[counter] = 0x00;
                    }
                }

                Array.Reverse(data64);

                result = ConvertBitStringToULong(GetBitRepresentationOfSnmpBitsOctetString(BitConverter.ToUInt64(data64, 0)));
                canParse = true;
            }
            catch (ArgumentException)
            {
            }

            return canParse;
        }

        /// <summary>
        ///     Converte una stringa in formato esadecimale a byte, nel valore a 64 bit senza segno. Usata per le maschere di bit
        ///     codificate nelle definizioni XML. Formato di codifica esadecimale invertito, little-endian.
        /// </summary>
        /// <param name="s">Stringa in ingresso</param>
        /// <param name="result">Valore ULong di ritorno</param>
        /// <returns>True se la conversione è riuscita, false altrimenti</returns>
        internal static bool TryParseULongHexStringLTR(string s, out ulong result)
        {
            bool canParse = false;
            result = 0;

            try
            {
                byte[] data = ConvertHexStringToByteArray(s);
                byte[] data64 = new byte[8];

                for (int counter = 0; counter <= 7; counter++)
                {
                    if (counter < data.Length)
                    {
                        data64[counter] = data[counter];
                    }
                    else
                    {
                        data64[counter] = 0x00;
                    }
                }

                result = BitConverter.ToUInt64(data64, 0);
                canParse = true;
            }
            catch (ArgumentException)
            {
            }

            return canParse;
        }

        /// <summary>
        ///     Converte una stringa in formato esadecimale a byte, nel valore a 64 bit senza segno. Usata per le maschere di bit
        ///     codificate nelle definizioni XML. Formato standard di codifica esadecimale, big-endian.
        /// </summary>
        /// <param name="s">Stringa in ingresso</param>
        /// <param name="result">Valore ULong di ritorno</param>
        /// <returns>True se la conversione è riuscita, false altrimenti</returns>
        internal static bool TryParseULongHexStringRTL(string s, out ulong result)
        {
            bool canParse = false;
            result = 0;

            if (s == null)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale nulla"));
            }

            if (s.Length == 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale vuota"));
            }

            if (s.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
            {
                s = s.Substring(2);
            }

            s = s.ToUpper();

            if (s.Length < 2)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale non valida: {0}", s));
            }

            if (s.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "Stringa con valore esadecimale di lunghezza dispari: {0}", s));
            }

            try
            {
                canParse = ulong.TryParse(s, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out result);
            }
            catch (ArgumentException)
            {
            }

            return canParse;
        }

        /// <summary>
        ///     Ritorna una rappresentazione stringa di un valore numerico intero, corrispondente alla codifica Bits di un
        ///     OctetString SNMP. Il formato presuppone il bit meno significativo a sinistra, quindi primo bit posizione 0, secondo
        ///     posizione 1...
        /// </summary>
        /// <param name="value">Intero a 64 bit senza segno</param>
        /// <returns>Rappresentazione stringa del formato SNMP BITS</returns>
        internal static string GetBitRepresentationOfSnmpBitsOctetString(ulong value)
        {
            if (value == 0x00)
            {
                return "00000000";
            }

            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);

            StringBuilder str = new StringBuilder();

            /*
            bool gotBitSet = false;
            foreach (byte b in bytes)
            {
                if ((gotBitSet) || (b != 0x00))
                {
                    str.AppendFormat("{0} ", ConvertByteToBitString(b)); 
                    gotBitSet = true;
                }
            }
            */

            foreach (byte b in bytes)
            {
                str.AppendFormat("{0} ", ConvertByteToBitString(b));
            }

            return str.ToString().Trim();
        }

        /// <summary>
        ///     Ritorna una rappresentazione stringa di un valore numerico intero.
        ///     Il formato presuppone il bit meno significativo a destra.
        /// </summary>
        /// <param name="value">Intero a 64 bit senza segno</param>
        /// <returns>Rappresentazione stringa del numero</returns>
        internal static string GetBitRepresentationULong(ulong value)
        {
            if (value == 0x00)
            {
                return "00000000";
            }

            byte[] bytes = BitConverter.GetBytes(value);

            StringBuilder str = new StringBuilder();

            bool gotBitSet = false;
            for (int byteCounter = bytes.Length - 1; byteCounter >= 0; byteCounter--)
            {
                if ((gotBitSet) || (bytes[byteCounter] != 0x00))
                {
                    str.AppendFormat("{0} ", ConvertByteToBitString(bytes[byteCounter]));
                    gotBitSet = true;
                }
            }

            return str.ToString().Trim();
        }

        /// <summary>
        ///     Formatta un byte nei suoi singoli bit
        /// </summary>
        /// <param name="b">Byte da stampare</param>
        /// <returns>Valore del byte formattato nei singoli bit</returns>
        private static string ConvertByteToBitString(byte b)
        {
            StringBuilder str = new StringBuilder(8);
            int[] bl = new int[8];

            for (int i = 0; i < bl.Length; i++)
            {
                bl[bl.Length - 1 - i] = ((b & (1 << i)) != 0) ? 1 : 0;
            }

            foreach (int num in bl)
            {
                str.Append(num);
            }

            return str.ToString();
        }

        /// <summary>
        ///     Converte una stringa nel formato binario, con bit meno significativo all'inizio, in intero a 64 bit senza segno
        /// </summary>
        /// <param name="s">Stringa in formato binario</param>
        /// <returns>Intero a 64 bit senza segno</returns>
        internal static ulong ConvertBitStringToULong(string s)
        {
            ulong val = 0;

            // Le stringhe di bit sono formattate a pacchetti di 8 bits, occorre rimuovere gli spazi
            s = s.Replace(" ", String.Empty);

            for (ushort counter = 0; counter < s.Length; counter++)
            {
                if (s[counter] == '1')
                {
                    val |= ((ulong)1 << counter);
                }
            }

            return val;
        }

        /// <summary>
        ///     Ritorna un OctetString costruito da una stringa con byte
        /// </summary>
        /// <param name="s">Stringa in ingresso</param>
        /// <returns>OctetString costruito dai byte grezzi della stringa</returns>
        internal static OctetString FillOctetStringFromRawString(string s)
        {
            return new OctetString(s, Encoding.GetEncoding("iso-8859-1"));
        }

        #endregion

        #region DecodeTimeTicksData - Decodifica formato TimeTicks in giorni, ore, minuti, secondi

        private static string DecodeTimeTicksData(Variable inputData, TimeTicksFormat format = TimeTicksFormat.Simple, bool timeTicksMs = false)
        {
            string data = string.Empty;

            TimeSpan ts = ((TimeTicks)inputData.Data).ToTimeSpan();
            if (timeTicksMs)
            {
                ts = new TimeSpan(ts.Ticks / 10);
            }

            switch (format)
            {
                case TimeTicksFormat.Undefined:
                case TimeTicksFormat.Simple:
                    if (ts.Days != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} giorni, ", ts.Days);
                    }
                    else if (ts.Days == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} giorno, ", ts.Days);
                    }
                    data = data +
                           string.Format(CultureInfo.InvariantCulture, "{0:00}:{1:00}:{2:00}.{3:000}", ts.Hours, ts.Minutes, ts.Seconds,
                               ts.Milliseconds);
                    break;
                case TimeTicksFormat.Extended:
                    if (ts.Days != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} giorni ", ts.Days);
                    }
                    else if (ts.Days == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} giorno ", ts.Days);
                    }
                    if (ts.Hours != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} ore ", ts.Hours);
                    }
                    else if (ts.Hours == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} ora ", ts.Hours);
                    }
                    if (ts.Minutes != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} minuti ", ts.Minutes);
                    }
                    else if (ts.Minutes == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} minuto ", ts.Minutes);
                    }
                    if (ts.Seconds != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} secondi ", ts.Seconds);
                    }
                    else if (ts.Seconds == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} secondo ", ts.Seconds);
                    }
                    if (ts.Milliseconds != 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} millisecondi", ts.Milliseconds);
                    }
                    else if (ts.Milliseconds == 1)
                    {
                        data = data + string.Format(CultureInfo.InvariantCulture, "{0} millisecondo", ts.Milliseconds);
                    }
                    break;
            }

            return data;
        }

        #endregion

        #region TimeTicksFormat - Specifica del formato di formattazione date

        /// <summary>
        ///     Specifica del formato di formattazione date
        /// </summary>
        private enum TimeTicksFormat
        {
            Undefined,
            Extended,
            Simple
        }

        #endregion

        #region Metodi pubblici per l'elaborazione delle tabelle SNMP

        /// <summary>
        ///     Elabora un valore di ritorno, cercando su una specifica colonna della tabella correlata il valore estratto dalla
        ///     riga della tabella principale, trovata con una espressione regolare di ricerca applicata ad una colonna specifica
        /// </summary>
        /// <param name="table">Tabella SNMP</param>
        /// <param name="correlationTable">Tabella SNMP correlata</param>
        /// <param name="filterColumnIndex">Indice della colonna della tabella sui cui eseguire le ricerche</param>
        /// <param name="correlationFilterColumnIndex">Indice della colonna della tabella correlata sui cui eseguire le ricerche</param>
        /// <param name="filterColumnRegex">
        ///     Espressione regolare da applicare alla colonna della tabella, per ottenere la riga
        ///     cercata
        /// </param>
        /// <param name="evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <param name="correlationEvaluationFormula">Codice C# per l'elaborazione della riga correlata</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromCorrelationTableWithCorrelationValue(Variable[,] table,
            Variable[,] correlationTable,
            int filterColumnIndex,
            int correlationFilterColumnIndex,
            Regex filterColumnRegex,
            string evaluationFormula,
            string correlationEvaluationFormula,
            int startCorrelationTableOid,
            out int endCorrelationTableOid,
            bool findExactlyString = false)
        {
            string seekValue = null;
            string returnValue = null;

            endCorrelationTableOid = startCorrelationTableOid;

            if (table != null)
            {
                for (int row = 0; row <= table.GetUpperBound(0); row++)
                {
                    if ((filterColumnIndex <= table.GetUpperBound(1)) && (filterColumnRegex.IsMatch(table[row, filterColumnIndex].Data.ToString())))
                    {
                        seekValue = EvaluateValueExpressionFromTable(table, row, evaluationFormula, Settings.Default.ReferencedAssemblies);
                        break;
                    }
                }
            }

            if (!string.IsNullOrEmpty(seekValue))
            {
                if (correlationTable != null)
                {
                    for (int row = startCorrelationTableOid; row <= correlationTable.GetUpperBound(0); row++)
                    {
                        if ((correlationFilterColumnIndex <= correlationTable.GetUpperBound(1)) &&
                            (Regex.IsMatch(correlationTable[row, correlationFilterColumnIndex].Data.ToString(), (findExactlyString == false ? seekValue : "^" + seekValue + "$"),
                                RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)))
                        {
                            returnValue = EvaluateValueExpressionFromTable(correlationTable, row, correlationEvaluationFormula,
                                Settings.Default.ReferencedAssemblies);
                            endCorrelationTableOid = row;
                            break;
                        }
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        ///     Elabora un valore di ritorno, data una riga della tabella correlata, trovata con una espressione regolare di
        ///     ricerca applicata ad una colonna specifica
        /// </summary>
        /// <param name="table">Tabella SNMP</param>
        /// <param name="correlationTable">Tabella SNMP correlata</param>
        /// <param name="filterColumnIndex">Indice della colonna della tabella sui cui eseguire le ricerche</param>
        /// <param name="filterColumnRegex">
        ///     Espressione regolare da applicare alla colonna della tabella, per ottenere la riga
        ///     cercata
        /// </param>
        /// <param name="correlationEvaluationFormula">Codice C# per l'elaborazione della riga correlata</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromCorrelationTableWithColumnFilter(Variable[,] table,
            Variable[,] correlationTable,
            int filterColumnIndex,
            Regex filterColumnRegex,
            string correlationEvaluationFormula)
        {
            int seekValueRow = -1;
            string returnValue = null;

            if (table != null)
            {
                for (int row = 0; row <= table.GetUpperBound(0); row++)
                {
                    if ((filterColumnIndex <= table.GetUpperBound(1)) && (filterColumnRegex.IsMatch(table[row, filterColumnIndex].Data.ToString())))
                    {
                        seekValueRow = row;
                        break;
                    }
                }
            }

            if (seekValueRow >= 0)
            {
                if ((correlationTable != null) && (seekValueRow <= correlationTable.GetUpperBound(0)))
                {
                    returnValue = EvaluateValueExpressionFromTable(correlationTable, seekValueRow, correlationEvaluationFormula,
                        Settings.Default.ReferencedAssemblies);
                }
            }

            return returnValue;
        }

        /// <summary>
        ///     Elabora un valore di ritorno, data una riga della tabella, trovata con una espressione regolare di ricerca
        ///     applicata ad una colonna specifica
        /// </summary>
        /// <param name="table">Tabella SNMP</param>
        /// <param name="filterColumnIndex">Indice della colonna della tabella sui cui eseguire le ricerche</param>
        /// <param name="filterColumnRegex">
        ///     Espressione regolare da applicare alla colonna della tabella, per ottenere la riga
        ///     cercata
        /// </param>
        /// <param name="evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromTableWithColumnFilter(Variable[,] table,
            int filterColumnIndex,
            Regex filterColumnRegex,
            string evaluationFormula)
        {
            string returnValue = null;

            if (table != null)
            {
                for (int row = 0; row <= table.GetUpperBound(0); row++)
                {
                    if ((filterColumnIndex <= table.GetUpperBound(1)) && (filterColumnRegex.IsMatch(table[row, filterColumnIndex].Data.ToString())))
                    {
                        returnValue = EvaluateValueExpressionFromTable(table, row, evaluationFormula, Settings.Default.ReferencedAssemblies);
                        break;
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        ///     Elabora un valore di ritorno, basato su una specifica riga di una tabella SNMP
        /// </summary>
        /// <param name="table">Tabella SNMP</param>
        /// <param name="rowIndex">Indice della riga della tabella</param>
        /// <param name="evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromTableAtRow(Variable[,] table, int rowIndex, string evaluationFormula)
        {
            string returnValue = null;

            if (table != null)
            {
                if ((rowIndex <= table.GetUpperBound(0)))
                {
                    returnValue = EvaluateValueExpressionFromTable(table, rowIndex, evaluationFormula, Settings.Default.ReferencedAssemblies);
                }
            }

            return returnValue;
        }

        /// <summary>
        ///     Elabora un valore di ritorno, basato su un valore tornato da un Oid(NON TABELLARE)
        /// </summary>
        /// <param name="table">Valore SNMP</param>
        /// <param name="evaluationFormula">Codice C# per l'elaborazione del valore</param>
        /// <returns>Valore stringa elaborato</returns>
        internal static string GetValueFromSNMPValue(Variable value, string evaluationFormula)
        {
            string returnValue = null;

            if (value != null)
            {
                try
                {
                    returnValue = EvaluateValueExpressionFromSNMPValue(value, evaluationFormula, Settings.Default.ReferencedAssemblies);
                }
                catch (Exception e) {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
						                                               string.Format(CultureInfo.InvariantCulture,
                                                                                     "Thread: {0} - Errore in EvaluateValueExpressionFromSNMPValue eseguendo il codice C#: {1};\nCon errore: {2}", Thread.CurrentThread.GetHashCode(), evaluationFormula, e.ToString()));
                    returnValue = null;
                }
            }

            return returnValue;
        }

        /// <summary>
        ///     Converte un Oid in formato stringa nel relativo oggetto ObjectIdentifier
        /// </summary>
        /// <param name="oidString">Stringa che contiene l'Oid, nel formato "1.3.6.1.2.1.1.5.0"</param>
        /// <returns>Oggetto ObjectIdentifier. Null nel caso non sia una stringa valida.</returns>
        internal static ObjectIdentifier TryParseOid(string oidString)
        {
            ObjectIdentifier oid = null;

            try
            {
                oid = new ObjectIdentifier(oidString);
            }
            catch (ArgumentException)
            {
            }
            catch (FormatException)
            {
            }
            catch (OverflowException)
            {
            }

            return oid;
        }

        /// <summary>
        ///     Ritorna un booleano che indica se nella tabella SNMP fornita, su specifica colonna, esiste almeno una riga che
        ///     corrisponde all'espressione regolare di ricerca fornita
        /// </summary>
        /// <param name="table">Tabella SNMP</param>
        /// <param name="filterColumnIndex">Indice della colonna della tabella sui cui eseguire le ricerche</param>
        /// <param name="filterColumnRegex">
        ///     Espressione regolare da applicare alla colonna della tabella, per ottenere la riga
        ///     cercata
        /// </param>
        /// <param name="secondFilterColumnIndex">
        ///     Secondo indice (opzionale) della colonna della tabella sui cui eseguire le
        ///     ricerche
        /// </param>
        /// <param name="secondFilterColumnRegex">
        ///     Seconda espressione regolare (opzionale) da applicare alla colonna della tabella,
        ///     per ottenere la riga cercata
        /// </param>
        /// <returns>Valore booleano che indica se esiste almeno una riga nella tabella corrispondente alla ricerca effettuata</returns>
        internal static bool ExistsValueInTableWithColumnFilter(Variable[,] table,
            int filterColumnIndex,
            Regex filterColumnRegex,
            int? secondFilterColumnIndex,
            Regex secondFilterColumnRegex)
        {
            bool returnValue = false;

            if (table != null)
            {
                for (int row = 0; row <= table.GetUpperBound(0); row++)
                {
                    if ((filterColumnIndex <= table.GetUpperBound(1)) && (filterColumnRegex.IsMatch(table[row, filterColumnIndex].Data.ToString())))
                    {
                        if ((secondFilterColumnIndex.HasValue) && (secondFilterColumnRegex != null))
                        {
                            // E' indicata la ricerca in AND con secondo parametro
                            if ((secondFilterColumnIndex.Value <= table.GetUpperBound(1)) &&
                                (secondFilterColumnRegex.IsMatch(table[row, secondFilterColumnIndex.Value].Data.ToString())))
                            {
                                returnValue = true;
                                break;
                            }
                        }
                        else
                        {
                            returnValue = true;
                            break;
                        }
                    }
                }
            }

            return returnValue;
        }

        #endregion

        #region EvaluateValueExpressionFromTable - Elabora i dati di una riga di tabella, eseguendo codice C# dinamico

        /// <summary>
        ///     Elabora i dati di una riga di tabella, eseguendo codice C# dinamico
        /// </summary>
        /// <param name="table">Tabella SNMP</param>
        /// <param name="tableRow">Indice della riga della tabella</param>
        /// <param name="evaluationFormula">Codice C# per l'elaborazione della riga</param>
        /// <param name="referencesAssemblies">Assembly referenziati per la compilazione del codice C#</param>
        /// <returns>Valore stringa elaborato</returns>
        private static string EvaluateValueExpressionFromTable(Variable[,] table,
            int tableRow,
            string evaluationFormula,
            string referencesAssemblies)
        {
            try
            {
                evaluationFormula = Regex.Replace(evaluationFormula, "#(?<col>\\d{1,10})#", "table[row, ${col}]",
                    RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException("Errore nell'espansione della formula di elaborazione tabella SNMP", ex);
            }

            const string className = "Evaluation";
            const string methodName = "GetValue";

            const string evaluationCode =
                   "using System; internal class {0} {{public object {1}(Lextm.SharpSnmpLib.Variable[,] table, int row) {{if (table == null) {{return null;}} return {2};}}}}";

            string code;
            try
            {
                code = string.Format(CultureInfo.InvariantCulture, evaluationCode, className, methodName, evaluationFormula);
            }
            catch (ArgumentNullException ex)
            {
                throw new DeviceConfigurationHelperException("Errore nella formattazione del codice con la formula di elaborazione tabella SNMP", ex);
            }
            catch (FormatException ex)
            {
                throw new DeviceConfigurationHelperException("Errore nella formattazione del codice con la formula di elaborazione tabella SNMP", ex);
            }

            if (string.IsNullOrEmpty(code))
            {
                throw new DeviceConfigurationHelperException("Errore nel codice con la formula di elaborazione tabella SNMP, codice C# nullo o vuoto");
            }

            string codeHash;

            using (SHA256 sha = new SHA256Managed())
            {
                try
                {
                    codeHash = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(code))).Replace("-", string.Empty);
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(
                        "Errore nella generazione del nome file con il codice dinamico di elaborazione tabella SNMP");
                }
                catch (ObjectDisposedException)
                {
                    throw new DeviceConfigurationHelperException(
                        "Errore nella generazione del nome file con il codice dinamico di elaborazione tabella SNMP");
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException(
                        "Errore nella generazione del nome file con il codice dinamico di elaborazione tabella SNMP");
                }
            }

            string dynamicAssemblyFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DYNAMIC_ASSEMBLY_FOLDER);

            if (!Directory.Exists(dynamicAssemblyFolder))
            {
                try
                {
                    Directory.CreateDirectory(dynamicAssemblyFolder);
                }
                catch (IOException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione tabelle SNMP ({0})",
                        dynamicAssemblyFolder));
                }
                catch (UnauthorizedAccessException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione tabelle SNMP ({0})",
                        dynamicAssemblyFolder));
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione tabelle SNMP ({0})",
                        dynamicAssemblyFolder));
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione tabelle SNMP ({0})",
                        dynamicAssemblyFolder));
                }
            }

            if (string.IsNullOrEmpty(codeHash))
            {
                throw new DeviceConfigurationHelperException(
                    "Errore nella generazione del nome dell'assembly dinamico per l'elaborazione tabelle SNMP");
            }

            string dynamicAssembly = Path.Combine(dynamicAssemblyFolder,
                string.Format(CultureInfo.InvariantCulture, "{0}-{1}.dll", DYNAMIC_ASSEMBLY_FILENAME_PREFIX, codeHash));

            object loObject = null;
            // ReSharper disable TooWideLocalVariableScope
            Assembly loAssembly;
            // ReSharper restore TooWideLocalVariableScope

            lock (locker)
            {
                if (FileUtility.CheckFileCanRead(dynamicAssembly))
                {
                    #region Caricamento dinamico Assembly già esistente

                    try
                    {
                        loAssembly = Assembly.LoadFile(dynamicAssembly);
                        loObject = loAssembly.CreateInstance(className);
                    }
                    catch (ArgumentException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                    }
                    catch (MissingMethodException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                    }
                    catch (FileLoadException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                    }
                    catch (BadImageFormatException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                    }

                    #endregion
                }
                else
                {
                    #region L'Assembly dinamico non esiste, deve essere generato e compilato

                    #region Preparazione parametri per il compilatore dinamico

                    CompilerParameters parameters = new CompilerParameters();
                    parameters.GenerateExecutable = false;
                    parameters.OutputAssembly = dynamicAssembly;
                    parameters.GenerateInMemory = true;
                    parameters.TreatWarningsAsErrors = true;
                    parameters.TempFiles.KeepFiles = false;
                    parameters.CompilerOptions = "/target:library /optimize+";
                    parameters.IncludeDebugInformation = false;
                    parameters.WarningLevel = 3;

                    #endregion

                    #region Caricamento ed assegnazione References

                    if (string.IsNullOrEmpty(referencesAssemblies))
                    {
                        // ad una DLL da referenziare che sia preceduta da "(local)", sarà aggiunto il prefisso con il path completo dell'applicazione
                        // per effettuare la ricerca della DLL nella cartella corrente (in caso opposto, si presuppone che le DLL siano in GAC)
                        referencesAssemblies = "System.dll,(local)SharpSnmpLib.dll";
                    }

                    foreach (string refAssembly in referencesAssemblies.Split(','))
                    {
                        if (!string.IsNullOrEmpty(refAssembly))
                        {
                            if (refAssembly.Trim().StartsWith("(local)", StringComparison.OrdinalIgnoreCase))
                            {
                                parameters.ReferencedAssemblies.Add(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                    refAssembly.Trim().Replace("(local)", string.Empty)));
                            }
                            else
                            {
                                parameters.ReferencedAssemblies.Add(refAssembly.Trim());
                            }
                        }
                    }

                    #endregion

                    using (CSharpCodeProvider provider = new CSharpCodeProvider())
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            #region Compilazione dinamica codice

                            CompilerResults compilerResults;

                            try
                            {
                                compilerResults = provider.CompileAssemblyFromSource(parameters, code);
                            }
                            catch (NotImplementedException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    "Errore nella compilazione del codice con la formula di elaborazione tabella SNMP", ex);
                            }
                            catch (IOException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    "Errore nella compilazione del codice con la formula di elaborazione tabella SNMP", ex);
                            }

                            if ((compilerResults.Errors != null) && (compilerResults.Errors.Count > 0))
                            {
                                StringBuilder compileErrors = new StringBuilder();

                                foreach (CompilerError compileError in compilerResults.Errors)
                                {
                                    compileErrors.AppendLine(compileError.ErrorText);
                                }

                                throw new DeviceConfigurationHelperException(
                                    "Errore nella compilazione del codice con la formula di elaborazione tabella SNMP\r\n" + compileErrors);
                            }

                            #endregion

                            #region Caricamento dinamico Assembly appena generato

                            try
                            {
                                loAssembly = compilerResults.CompiledAssembly;
                                loObject = loAssembly.CreateInstance(className);
                            }
                            catch (ArgumentException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                            }
                            catch (MissingMethodException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                            }
                            catch (FileNotFoundException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                            }
                            catch (FileLoadException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                            }
                            catch (BadImageFormatException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP\r\n{0}", code), ex);
                            }

                            #endregion
                        }
                    }

                    #endregion
                }
            }

            if (loObject == null)
            {
                throw new DeviceConfigurationHelperException(
                    "Errore nella creazione di istanza del codice con la formula di elaborazione tabella SNMP");
            }

            object[] loCodeParms = new object[2];
            loCodeParms[0] = table;
            loCodeParms[1] = tableRow;

            try
            {
                object returnValue = loObject.GetType()
                    .InvokeMember(methodName, BindingFlags.InvokeMethod, null, loObject, loCodeParms, CultureInfo.InvariantCulture);

                if (returnValue != null)
                {
                    if (returnValue is ISnmpData)
                    {
                        // L'oggetto non è convertito nella formula
                        ISnmpData returnValueSnmp = returnValue as ISnmpData;

                        switch (returnValueSnmp.TypeCode)
                        {
                            case SnmpType.OctetString:
                                return Encoding.GetEncoding("iso-8859-1").GetString(((OctetString) returnValueSnmp).GetRaw());
                            default:
                                return returnValueSnmp.ToString();
                        }
                    }

                    // L'oggetto è già stato convertito (ad esempio in intero), quindi si ritorna come stringa diretta
                    return returnValue.ToString();
                }

                return null;
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (MethodAccessException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (MissingFieldException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (MissingMethodException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (TargetException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (TargetInvocationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (AmbiguousMatchException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione tabella SNMP\r\n{0}",
                        code), ex);
            }
        }

        #endregion

        #region EvaluateValueExpressionFromSNMPValue - Elabora il valore di una GET SNMP

        /// <summary>
        ///     Elabora i dati di una riga di tabella, eseguendo codice C# dinamico
        /// </summary>
        /// <param name="table">Valore SNMP</param>
        /// <param name="evaluationFormula">Codice C# per l'elaborazione del valore</param>
        /// <param name="referencesAssemblies">Assembly referenziati per la compilazione del codice C#</param>
        /// <returns>Valore stringa elaborato</returns>
        private static string EvaluateValueExpressionFromSNMPValue(Variable value,
            string evaluationFormula,
            string referencesAssemblies)
        {
            const string className = "Evaluation";
            const string methodName = "GetValue";

            const string evaluationCode =
                   "using System; internal class {0} {{public object {1}(Lextm.SharpSnmpLib.Variable value) {{if (value == null) {{return null;}} return {2};}}}}";

            string code;
            try
            {
                code = string.Format(CultureInfo.InvariantCulture, evaluationCode, className, methodName, evaluationFormula);
            }
            catch (ArgumentNullException ex)
            {
                throw new DeviceConfigurationHelperException("Errore nella formattazione del codice con la formula di elaborazione del valore SNMP", ex);
            }
            catch (FormatException ex)
            {
                throw new DeviceConfigurationHelperException("Errore nella formattazione del codice con la formula di elaborazione del valore SNMP", ex);
            }

            if (string.IsNullOrEmpty(code))
            {
                throw new DeviceConfigurationHelperException("Errore nel codice con la formula di elaborazione del valore SNMP, codice C# nullo o vuoto");
            }

            string codeHash;

            using (SHA256 sha = new SHA256Managed())
            {
                try
                {
                    codeHash = BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(code))).Replace("-", string.Empty);
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(
                        "Errore nella generazione del nome file con il codice dinamico di elaborazione del valore SNMP");
                }
                catch (ObjectDisposedException)
                {
                    throw new DeviceConfigurationHelperException(
                        "Errore nella generazione del nome file con il codice dinamico di elaborazione del valore SNMP");
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException(
                        "Errore nella generazione del nome file con il codice dinamico di elaborazione del valore SNMP");
                }
            }

            string dynamicAssemblyFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DYNAMIC_ASSEMBLY_FOLDER);

            if (!Directory.Exists(dynamicAssemblyFolder))
            {
                try
                {
                    Directory.CreateDirectory(dynamicAssemblyFolder);
                }
                catch (IOException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione del valore SNMP ({0})",
                        dynamicAssemblyFolder));
                }
                catch (UnauthorizedAccessException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione del valore SNMP ({0})",
                        dynamicAssemblyFolder));
                }
                catch (ArgumentException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione del valore SNMP ({0})",
                        dynamicAssemblyFolder));
                }
                catch (NotSupportedException)
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Errore nella creazione della cartella dove salvare i file con il codice dinamico di elaborazione del valore SNMP ({0})",
                        dynamicAssemblyFolder));
                }
            }

            if (string.IsNullOrEmpty(codeHash))
            {
                throw new DeviceConfigurationHelperException(
                    "Errore nella generazione del nome dell'assembly dinamico per l'elaborazione del valore SNMP");
            }

            string dynamicAssembly = Path.Combine(dynamicAssemblyFolder,
                string.Format(CultureInfo.InvariantCulture, "{0}-{1}.dll", DYNAMIC_ASSEMBLY_FILENAME_PREFIX, codeHash));

            object loObject = null;
            // ReSharper disable TooWideLocalVariableScope
            Assembly loAssembly;
            // ReSharper restore TooWideLocalVariableScope

            lock (locker)
            {
                if (FileUtility.CheckFileCanRead(dynamicAssembly))
                {
                    #region Caricamento dinamico Assembly già esistente

                    try
                    {
                        loAssembly = Assembly.LoadFile(dynamicAssembly);
                        loObject = loAssembly.CreateInstance(className);
                    }
                    catch (ArgumentException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                    }
                    catch (MissingMethodException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                    }
                    catch (FileNotFoundException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                    }
                    catch (FileLoadException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                    }
                    catch (BadImageFormatException ex)
                    {
                        throw new DeviceConfigurationHelperException(
                            string.Format(CultureInfo.InvariantCulture,
                                "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                    }

                    #endregion
                }
                else
                {
                    #region L'Assembly dinamico non esiste, deve essere generato e compilato

                    #region Preparazione parametri per il compilatore dinamico

                    CompilerParameters parameters = new CompilerParameters();
                    parameters.GenerateExecutable = false;
                    parameters.OutputAssembly = dynamicAssembly;
                    parameters.GenerateInMemory = true;
                    parameters.TreatWarningsAsErrors = true;
                    parameters.TempFiles.KeepFiles = false;
                    parameters.CompilerOptions = "/target:library /optimize+";
                    parameters.IncludeDebugInformation = false;
                    parameters.WarningLevel = 3;

                    #endregion

                    #region Caricamento ed assegnazione References

                    if (string.IsNullOrEmpty(referencesAssemblies))
                    {
                        // ad una DLL da referenziare che sia preceduta da "(local)", sarà aggiunto il prefisso con il path completo dell'applicazione
                        // per effettuare la ricerca della DLL nella cartella corrente (in caso opposto, si presuppone che le DLL siano in GAC)
                        referencesAssemblies = "System.dll,(local)SharpSnmpLib.dll";
                    }

                    foreach (string refAssembly in referencesAssemblies.Split(','))
                    {
                        if (!string.IsNullOrEmpty(refAssembly))
                        {
                            if (refAssembly.Trim().StartsWith("(local)", StringComparison.OrdinalIgnoreCase))
                            {
                                parameters.ReferencedAssemblies.Add(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                    refAssembly.Trim().Replace("(local)", string.Empty)));
                            }
                            else
                            {
                                parameters.ReferencedAssemblies.Add(refAssembly.Trim());
                            }
                        }
                    }

                    #endregion

                    using (CSharpCodeProvider provider = new CSharpCodeProvider())
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            #region Compilazione dinamica codice

                            CompilerResults compilerResults;

                            try
                            {
                                compilerResults = provider.CompileAssemblyFromSource(parameters, code);
                            }
                            catch (NotImplementedException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    "Errore nella compilazione del codice con la formula di elaborazione del valore SNMP", ex);
                            }
                            catch (IOException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    "Errore nella compilazione del codice con la formula di elaborazione del valore SNMP", ex);
                            }

                            if ((compilerResults.Errors != null) && (compilerResults.Errors.Count > 0))
                            {
                                StringBuilder compileErrors = new StringBuilder();

                                foreach (CompilerError compileError in compilerResults.Errors)
                                {
                                    compileErrors.AppendLine(compileError.ErrorText);
                                }

                                throw new DeviceConfigurationHelperException(
                                    "Errore nella compilazione del codice con la formula di elaborazione del valore SNMP\r\n" + compileErrors);
                            }

                            #endregion

                            #region Caricamento dinamico Assembly appena generato

                            try
                            {
                                loAssembly = compilerResults.CompiledAssembly;
                                loObject = loAssembly.CreateInstance(className);
                            }
                            catch (ArgumentException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                            }
                            catch (MissingMethodException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                            }
                            catch (FileNotFoundException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                            }
                            catch (FileLoadException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                            }
                            catch (BadImageFormatException ex)
                            {
                                throw new DeviceConfigurationHelperException(
                                    string.Format(CultureInfo.InvariantCulture,
                                        "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP\r\n{0}", code), ex);
                            }

                            #endregion
                        }
                    }

                    #endregion
                }
            }

            if (loObject == null)
            {
                throw new DeviceConfigurationHelperException(
                    "Errore nella creazione di istanza del codice con la formula di elaborazione del valore SNMP");
            }

            object[] loCodeParms = new object[1];
            loCodeParms[0] = value;

            try
            {
                object returnValue = loObject.GetType()
                    .InvokeMember(methodName, BindingFlags.InvokeMethod, null, loObject, loCodeParms, CultureInfo.InvariantCulture);

                if (returnValue != null)
                {
                    if (returnValue is ISnmpData)
                    {
                        // L'oggetto non è convertito nella formula
                        ISnmpData returnValueSnmp = returnValue as ISnmpData;

                        switch (returnValueSnmp.TypeCode)
                        {
                            case SnmpType.OctetString:
                                return Encoding.GetEncoding("iso-8859-1").GetString(((OctetString)returnValueSnmp).GetRaw());
                            default:
                                return returnValueSnmp.ToString();
                        }
                    }

                    // L'oggetto è già stato convertito (ad esempio in intero), quindi si ritorna come stringa diretta
                    return returnValue.ToString();
                }

                return null;
            }
            catch (ArgumentException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (MethodAccessException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (MissingFieldException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (MissingMethodException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (TargetException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (TargetInvocationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (AmbiguousMatchException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (NotSupportedException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new DeviceConfigurationHelperException(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'esecuzione del codice con la formula di elaborazione del valore SNMP\r\n{0}",
                        code), ex);
            }
        }

        #endregion
    }
}
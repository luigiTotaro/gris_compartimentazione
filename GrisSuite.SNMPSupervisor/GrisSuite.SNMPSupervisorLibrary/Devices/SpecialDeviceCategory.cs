﻿namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    /// Indica delle categorie particolari per le periferiche
    /// </summary>
    internal enum SpecialDeviceCategory
    {
        /// <summary>
        /// Periferica che non ha categoria valorizzata o per cui la categoria non è applicabile
        /// </summary>
        Undefined = -1,
        /// <summary>
        /// Periferica che non è in grado di recuperare la categoria, in base alla MIB caricata, dai dati SNMP letti
        /// </summary>
        NotAvailable = -2,
        /// <summary>
        /// Periferica che usa definizione dinamica, ma per cui la categoria non è valorizzabile
        /// </summary>
        NotDefinedForCurrentDevice = -3
    }
}
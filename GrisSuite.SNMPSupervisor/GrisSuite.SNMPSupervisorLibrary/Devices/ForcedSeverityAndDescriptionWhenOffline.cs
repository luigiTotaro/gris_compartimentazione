﻿using System;
using System.Globalization;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///     Incapsula i dati di severità e descrizione periferica, nel caso la periferica sia offline
    ///     (offline)
    /// </summary>
    public class ForcedSeverityAndDescriptionWhenOffline
    {
        /// <summary>
        ///     Indica la severità forzata della periferica, nel caso la periferica sia offline
        /// </summary>
        public Severity DeviceStatusSeverity { get; private set; }

        /// <summary>
        ///     Indica la descrizione dello stato forzata della periferica, nel caso la periferica sia offline
        /// </summary>
        public string DeviceStatusDescription { get; private set; }

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="definitionFileName">Nome del file della definizione XML corrente</param>
        /// <param name="data">Valore stringa dell'attributo, letto dalla definizione</param>
        public ForcedSeverityAndDescriptionWhenOffline(string definitionFileName, string data)
        {
            this.DeviceStatusSeverity = Settings.Default.DeviceStatusSeverityUnreachable;
            this.DeviceStatusDescription = Settings.Default.DeviceStatusMessageUnreachable;

            if (!string.IsNullOrEmpty(data.Trim()))
            {
                string[] values = data.Trim().Split(';');

                if (values.Length == 2)
                {
                    object severity;

                    try
                    {
                        severity = Enum.Parse(typeof (Severity), values[0].Trim(), true);
                    }
                    catch (ArgumentException)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'ForcedSeverityAndDescriptionWhenNoPingAndNoSnmp' del nodo 'Device' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown). Valore contenuto: '{1}'",
                            definitionFileName, values[0].Trim()));
                    }

                    string severityDescription = values[1].Trim();

                    if (string.IsNullOrEmpty(severityDescription))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'ForcedSeverityAndDescriptionWhenNoPingAndNoSnmp' del nodo 'Device' non contiene una Descrizione per lo stato periferica valida. Valore contenuto: '{1}'",
                            definitionFileName, severityDescription));
                    }

                    if (severity is Severity)
                    {
                        this.DeviceStatusSeverity = (Severity) severity;
                    }
                    else
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                            "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'ForcedSeverityAndDescriptionWhenNoPingAndNoSnmp' del nodo 'Device' non contiene un valore per la Severità valido (valori ammessi: Ok, Warning, Error e Unknown)",
                            definitionFileName));
                    }

                    this.DeviceStatusDescription = severityDescription;
                }
                else
                {
                    throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                        "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'ForcedSeverityAndDescriptionWhenNoPingAndNoSnmp' del nodo 'Device' non è nel formato 'SeveritàPeriferica;Descrizione stato periferica' (dove 'SeveritàPeriferica' può assumere un valore tra: Ok, Warning, Error e Unknown e 'Descrizione stato periferica' non deve contenere punti e virgole ';')",
                        definitionFileName));
                }
            }
            else
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                    "Impossibile caricare il file {0}, con la definizione XML della periferica. Il contenuto dell'attributo 'ForcedSeverityAndDescriptionWhenNoPingAndNoSnmp' del nodo 'Device' è nullo e non valido",
                    definitionFileName));
            }
        }
    }
}
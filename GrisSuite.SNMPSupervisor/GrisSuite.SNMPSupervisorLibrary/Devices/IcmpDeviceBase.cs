﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Database;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Devices
{
    /// <summary>
    ///     Rappresenta la classe base per i dispositivi ICMP
    /// </summary>
    public class IcmpDeviceBase : IDevice
    {
        #region Variabili private

        private bool isPopulated;
        private readonly Collection<DBStream> streamData;
        private readonly Collection<EventObject> eventsData;
        private readonly MessengerFactory messengerFactory;
        // Severità calcolata della periferica
        private Severity severity;

        #endregion

        #region Costruttore

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="fakeLocalData">Indica se caricare i dati da file di risposte locali per simulazione</param>
        /// <param name="deviceName">Nome della periferica</param>
        /// <param name="deviceType">Codice univoco della periferica</param>
        /// <param name="deviceIP">Indirizzo IP e porta della periferica</param>
        /// <param name="lastEvent">Informazioni dell'ultimo evento in ordine temporale associato alla periferica</param>
        /// <param name="deviceConfigurationHelperParameters">Classe helper che contiene i parametri base delle definizioni</param>
        /// <param name="device">Contenitore oggetto periferica, come letto da System.xml</param>
        public IcmpDeviceBase(bool fakeLocalData,
            string deviceName,
            string deviceType,
            IPEndPoint deviceIP,
            EventObject lastEvent,
            DeviceConfigurationHelperParameters deviceConfigurationHelperParameters,
            DeviceObject device)
        {
            if (String.IsNullOrEmpty(deviceName))
            {
                throw new ArgumentException("Il nome della periferica è obbligatorio.", "deviceName");
            }

            if (String.IsNullOrEmpty(deviceType))
            {
                throw new ArgumentException("Il tipo della periferica è obbligatorio.", "deviceType");
            }

            if ((deviceIP == null) || (deviceIP.Address == null))
            {
                throw new ArgumentException("L'indirizzo IP della periferica è obbligatorio.", "deviceIP");
            }

            if (device == null)
            {
                throw new ArgumentException("L'oggetto della periferica è obbligatorio.", "device");
            }

            this.DeviceName = deviceName;
            this.DeviceType = deviceType;
            this.streamData = new Collection<DBStream>();
            this.eventsData = new Collection<EventObject>();
            this.DeviceIPEndPoint = deviceIP;
            this.LastEvent = lastEvent;
            this.LastError = null;
            this.messengerFactory = new MessengerFactory();
            this.DefinitionFileName = deviceConfigurationHelperParameters.DefinitionFileName;
            this.FakeLocalData = fakeLocalData;
            this.DeviceConfigurationParameters = deviceConfigurationHelperParameters;
            this.Device = device;
            this.ContainsOnlyInfoStreams = false;
            this.severity = Severity.Unknown;
            this.ExcludedFromSeverityComputation = false;
            this.ShouldSendNotificationByEmail = false;
            this.DynamicCodeCalculateSeverity = null;
            this.DynamicCodeCalculateStatusMessage = null;
            this.IsDataRetrieved = true;

            try
            {
                DeviceConfigurationHelper deviceConfigurationHelper = new DeviceConfigurationHelper(this.FakeLocalData, device.DeviceId,
                    this.messengerFactory, this.DeviceIPEndPoint, string.Empty, this.DeviceConfigurationParameters);

                deviceConfigurationHelper.PreloadConfiguration();

                this.DeviceConfigurationParameters = deviceConfigurationHelper.DeviceConfigurationParameters;

                deviceConfigurationHelper.LoadConfiguration(this.streamData);

                this.ForcedSeverityAndDescriptionWhenDeviceOffline = deviceConfigurationHelper.ForcedSeverityAndDescriptionWhenDeviceOffline;
                this.DynamicCodeCalculateSeverity = deviceConfigurationHelper.DynamicCodeCalculateSeverity;
                this.DynamicCodeCalculateStatusMessage = deviceConfigurationHelper.DynamicCodeCalculateStatusMessage;
            }
            catch (DeviceConfigurationHelperException ex)
            {
                this.LastError = ex;
            }
            catch (ArgumentException ex)
            {
                this.LastError = ex;
            }

            if (this.LastError == null)
            {
                this.CheckStatus();
            }
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///     Nome della periferica
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        ///     Codice univoco della periferica
        /// </summary>
        public string DeviceType { get; private set; }

        /// <summary>
        ///     Dati di severità e descrizione periferica, nel caso la periferica sia offline
        /// </summary>
        public ForcedSeverityAndDescriptionWhenOffline ForcedSeverityAndDescriptionWhenDeviceOffline { get; private set; }

        /// <summary>
        ///     Nome completo della definizione XML della periferica
        /// </summary>
        public string DefinitionFileName { get; private set; }

        /// <summary>
        ///     EndPoint IP della periferica
        /// </summary>
        public IPEndPoint DeviceIPEndPoint { get; private set; }

        /// <summary>
        ///     Indica se è stato ritrovato almeno uno streamfield
        /// </summary>
        public bool IsDataRetrieved { get; private set; }

        /// <summary>
        ///     Lista degli Stream relativi alla periferica
        /// </summary>
        public Collection<DBStream> StreamData
        {
            get
            {
                if (this.LastError != null)
                {
                    return new Collection<DBStream>();
                }

                return this.streamData;
            }
        }

        /// <summary>
        ///     Indica se caricare i dati da file di risposte locali per simulazione
        /// </summary>
        public bool FakeLocalData { get; private set; }

        /// <summary>
        ///     Severità relativa al Device
        /// </summary>
        public Severity SeverityLevel
        {
            get { return this.severity; }
        }

        /// <summary>
        ///     Stato del Device, inteso come Online (raggiungibile via ICMP) oppure Offline (non raggiungibile)
        /// </summary>
        public byte Offline
        {
            get
            {
                if (this.IsAliveIcmp)
                {
                    return 0;
                }

                return 1;
            }
        }

        /// <summary>
        ///     Indica se gli stream e stream fields della periferica devono essere persistiti
        /// </summary>
        public bool ShouldPersistStreamAndStreamFields
        {
            get
            {
                if (this.IsAliveIcmp)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        ///     Codice C# per il calcolo dinamico della severità
        /// </summary>
        public string DynamicCodeCalculateSeverity { get; private set; }

        /// <summary>
        ///     Codice C# per il calcolo dinamico del messaggio di stato
        /// </summary>
        public string DynamicCodeCalculateStatusMessage { get; private set; }

        /// <summary>
        ///     Decodifica descrittiva dello stato della periferica
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                return SeverityCalculators.CalculateIcmpDeviceStatusMessage(this, this.DynamicCodeCalculateStatusMessage);
            }
        }

        /// <summary>
        ///     Indica se la periferica è raggiungibile via ICMP (ping)
        /// </summary>
        public bool IsAliveIcmp { get; private set; }

        /// <summary>
        ///     Indica se c'è stata una eccezione durante il caricamento della periferica
        /// </summary>
        public Exception LastError { get; set; }

        /// <summary>
        ///     Informazioni dell'ultimo evento in ordine temporale associato alla periferica
        /// </summary>
        public EventObject LastEvent { get; private set; }

        /// <summary>
        ///     Lista degli Eventi relativi alla periferica
        /// </summary>
        public Collection<EventObject> EventsData
        {
            get { return this.eventsData; }
        }

        /// <summary>
        ///     Classe helper che contiene i parametri base delle definizioni
        /// </summary>
        public DeviceConfigurationHelperParameters DeviceConfigurationParameters { get; private set; }

        /// <summary>
        ///     Contenitore oggetto periferica, come letto da System.xml
        /// </summary>
        public DeviceObject Device { get; private set; }

        /// <summary>
        ///     Indica se la periferica è stata tacitata e quindi sarà in stato forzato
        /// </summary>
        public bool ExcludedFromSeverityComputation { get; private set; }

        /// <summary>
        ///     Indica se occorre inviare una notifica per mail. È un aggregato denormalizzato dei flag di tutti gli Stream Field
        ///     contenuti
        /// </summary>
        public bool ShouldSendNotificationByEmail { get; private set; }

        /// <summary>
        ///     Indica se la periferica contiene solamente stream in stato Info
        /// </summary>
        public bool ContainsOnlyInfoStreams { get; set; }

        #endregion

        #region Metodi privati

        private void RetrieveData()
        {
            foreach (DBStream stream in this.StreamData)
            {
                if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                        string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati Stream ID: {1} - Stream Name: {2}",
                            Thread.CurrentThread.GetHashCode(), stream.StreamId, stream.Name));
                }

                foreach (DBStreamField field in stream.FieldData)
                {
                    if (FileUtility.LogLevel >= LoggingLevel.Diagnostic)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Diagnostic,
                            string.Format(CultureInfo.InvariantCulture, "Thread: {0} - Recupero dati Stream Field ID: {1} - Stream Field Name: {2}",
                                Thread.CurrentThread.GetHashCode(), field.FieldId, field.Name));
                    }

                    // Generiamo un Oid fittizio
                    IList<Variable> valuesString = new List<Variable>
                    {
                        new Variable(new ObjectIdentifier(new uint[] {1, 1}), new OctetString(this.IsAliveIcmp ? "1" : "0"))
                    };

                    field.RawData = valuesString;

                    break;
                }

                break;
            }
        }

        private void CheckStatus()
        {
            this.GetIsAliveIcmp();
        }

        private void GetIsAliveIcmp()
        {
            this.IsAliveIcmp = this.messengerFactory.GetPingResponse(this.FakeLocalData, this.DeviceIPEndPoint);
        }

        /// <summary>
        ///     Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        private void PopulateInternal()
        {
            this.LastError = null;

            this.RetrieveData();

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate(false);
                    }
                }
            }
            else
            {
                this.isPopulated = false;
            }
        }

        /// <summary>
        ///     Calcola la severità della periferica e ne valorizza gli stati possibili
        /// </summary>
        /// <returns></returns>
        private Severity CalculateSeverity()
        {
            return SeverityCalculators.CalculateIcmpDeviceSeverity(this, this.DynamicCodeCalculateSeverity);
        }

        /// <summary>
        ///     Imposta le tacitazioni su periferica, stream e stream field, in base alla configurazione ricevuta da base dati
        /// </summary>
        private void SetDeviceAcknowledgements()
        {
            if (this.Device.DeviceAck != null)
            {
                #region Tacitazione di periferica

                if (this.Device.DeviceAck.IsDeviceAcknowledged)
                {
                    this.ExcludedFromSeverityComputation = true;
                }

                #endregion

                #region Tacitazione stream

                foreach (DeviceStreamAcknowledgement streamAcknowledgement in this.Device.DeviceAck.DeviceStreamAcknowledgements)
                {
                    foreach (DBStream stream in this.StreamData)
                    {
                        if (stream.StreamId == streamAcknowledgement.StreamId)
                        {
                            stream.ExcludedFromSeverityComputation = true;
                            break;
                        }
                    }
                }

                #endregion

                #region Tacitazione stream field

                foreach (DeviceStreamFieldAcknowledgement streamFieldAcknowledgement in
                    this.Device.DeviceAck.DeviceStreamFieldAcknowledgements)
                {
                    foreach (DBStream stream in this.StreamData)
                    {
                        if (stream.StreamId == streamFieldAcknowledgement.StreamId)
                        {
                            foreach (DBStreamField streamField in stream.FieldData)
                            {
                                if (streamField.FieldId == streamFieldAcknowledgement.StreamFieldId)
                                {
                                    streamField.ExcludedFromSeverityComputation = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                #endregion
            }
        }

        /// <summary>
        ///     Con le tacitazioni già impostate nella periferica, stream e stream field, esclude tutti gli eventuali invii di mail
        ///     possibili
        /// </summary>
        private void CalculateDeviceShouldSendNotificationByEmailUsingAck()
        {
            if (this.ExcludedFromSeverityComputation)
            {
                // Se è tacitata l'intera periferica, tutti gli Stream Field contenuti saranno resettati per non inviare mail
                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField streamField in stream.FieldData)
                    {
                        streamField.ShouldSendNotificationByEmail = false;
                    }
                }
            }
            else
            {
                // Non è tacitata la periferica, quindi elabora gli Stream singolarmente
                foreach (DBStream stream in this.StreamData)
                {
                    if (stream.ExcludedFromSeverityComputation)
                    {
                        // Se è tacitato lo Stream, resettiamo tutti gli Stream Field interni
                        foreach (DBStreamField streamField in stream.FieldData)
                        {
                            streamField.ShouldSendNotificationByEmail = false;
                        }
                    }
                    else
                    {
                        // Se non è tacitato lo Stream, cerchiamo gli Stream Field esclusi e resettiamo solo quelli
                        foreach (DBStreamField streamField in stream.FieldData)
                        {
                            if (streamField.ExcludedFromSeverityComputation)
                            {
                                streamField.ShouldSendNotificationByEmail = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Verifica su tutti gli Stream Field esistenti, se ne esiste almeno uno che indica di inviare la mail
        /// </summary>
        private void AggregateShouldSendNotificationByEmailForDevice()
        {
            bool foundShouldSendNotificationByEmail = false;

            foreach (DBStream stream in this.StreamData)
            {
                foreach (DBStreamField streamField in stream.FieldData)
                {
                    if (streamField.ShouldSendNotificationByEmail)
                    {
                        this.ShouldSendNotificationByEmail = true;
                        foundShouldSendNotificationByEmail = true;
                        break;
                    }
                }

                if (foundShouldSendNotificationByEmail)
                {
                    break;
                }
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        ///     Popola i dati della periferica, in base agli Stream configurati
        /// </summary>
        public void Populate()
        {
            this.LastError = null;

            this.SetDeviceAcknowledgements();

            this.RetrieveData();

            if (this.LastError == null)
            {
                this.isPopulated = true;

                foreach (DBStream stream in this.StreamData)
                {
                    foreach (DBStreamField field in stream.FieldData)
                    {
                        field.Evaluate(false);
                    }
                }

                if (Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem > 0)
                {
                    // E' attivo il tentativo di recupero in caso di severità non normale della periferica
                    // Tenta di recuperare una periferica in condizione di errore, ma non permanente (fluttuazione di stato)

                    // C'è già stato un tentativo, quello principale, quindi il prossimo sarà il secondo
                    ushort retryCounter = 2;

                    while (retryCounter <= Settings.Default.MaxRetriesToRecoverFromDeviceSeverityProblem)
                    {
                        if (this.SeverityLevel != Severity.Ok)
                        {
                            Thread.Sleep(Settings.Default.SleepTimeMillisecondsBetweenRetriesToRecoverFromDeviceSeverityProblem);
                            this.PopulateInternal();
                            retryCounter++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                // Resettiamo gli eventuali flag di invio mail per gli elementi tacitati, dopo che sono state calcolate le severità
                this.CalculateDeviceShouldSendNotificationByEmailUsingAck();
            }
            else
            {
                this.isPopulated = false;
            }

            // Il calcolo della severità permette di avere tutti gli stati possibili impostati e gli stream valutati
            this.severity = this.CalculateSeverity();

            this.AggregateShouldSendNotificationByEmailForDevice();
        }

        /// <summary>
        ///     Produce una rappresentazione completa e descrittiva della periferica a fini diagnostici
        /// </summary>
        /// <returns>Rappresentazione completa e descrittiva della periferica a fini diagnostici</returns>
        public string Dump()
        {
            if ((!this.isPopulated) && (this.LastError == null))
            {
                // Se ci sono errori impostati, è già stato tentato un popolamento, quindi è inutile ritentare
                this.Populate();
            }

            StringBuilder dump = new StringBuilder();

            dump.AppendFormat(CultureInfo.InvariantCulture,
                "Device Name: {0}, Type: {1}, Offline: {2}, Severity: {3}, ReplyICMP: {4}" + Environment.NewLine, this.DeviceName, this.DeviceType,
                this.Offline, (int) this.SeverityLevel, this.IsAliveIcmp);

            foreach (DBStream stream in this.StreamData)
            {
                dump.AppendFormat(CultureInfo.InvariantCulture, "Stream Id: {0}, Name: {1}, Severity: {2}, Description: {3}" + Environment.NewLine,
                    stream.StreamId, stream.Name, (int) stream.SeverityLevel, stream.ValueDescriptionComplete);

                foreach (DBStreamField field in stream.FieldData)
                {
                    if (field.PersistOnDatabase)
                    {
                        dump.AppendFormat(CultureInfo.InvariantCulture,
                            "Field Id: {0}, Array Id: {1}, Name: {2}, Severity: {3}, Value: {4}, Description: {5}" + Environment.NewLine,
                            field.FieldId, field.ArrayId, field.Name, (int) field.SeverityLevel, field.Value, field.ValueDescriptionComplete);
                    }
                }
            }

            return dump.ToString();
        }

        #endregion
    }
}
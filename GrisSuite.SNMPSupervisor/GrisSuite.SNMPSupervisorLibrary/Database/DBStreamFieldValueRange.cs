﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Rappresenta il tipo di valori ad intervallo Minimo-Massimo
    /// </summary>
    public class DBStreamFieldValueRange
    {
        #region Costruttore

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "minValue">Valore minimo</param>
        /// <param name = "maxValue">Valore massimo</param>
        public DBStreamFieldValueRange(string minValue, string maxValue)
        {
            if (minValue != null)
            {
                this.ParseValue(minValue, ValueType.Min);
            }

            if (maxValue != null)
            {
                this.ParseValue(maxValue, ValueType.Max);
            }
        }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "value">Valore singolo (inteso come minimo e massimo uguali)</param>
        public DBStreamFieldValueRange(string value)
        {
            if (value != null)
            {
                this.ParseValue(value, ValueType.Min);
                this.ParseValue(value, ValueType.Max);
            }
        }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Elabora i dati SNMP per impostare il valore minimo del range
        /// </summary>
        /// <param name="values"></param>
        public void SetMinValue(IList<Variable> values)
        {
            if ((values == null) || (values.Count == 0))
            {
                this.MinValue = null;
            }
            else
            {
                string valueString = SnmpUtility.GetRawStringData(values[0]);

                if (valueString == null)
                {
                    this.MinValue = null;
                }
                else
                {
                    long value;
                    if (long.TryParse(valueString, out value))
                    {
                        this.MinValue = value;
                    }
                    else
                    {
                        this.MinValue = null;
                    }
                }
            }
        }

        /// <summary>
        /// Elabora i dati SNMP per impostare il valore massimo del range
        /// </summary>
        /// <param name="values"></param>
        public void SetMaxValue(IList<Variable> values)
        {
            if ((values == null) || (values.Count == 0))
            {
                this.MaxValue = null;
            }
            else
            {
                string valueString = SnmpUtility.GetRawStringData(values[0]);

                if (valueString == null)
                {
                    this.MaxValue = null;
                }
                else
                {
                    long value;
                    if (long.TryParse(valueString, out value))
                    {
                        this.MaxValue = value;
                    }
                    else
                    {
                        this.MaxValue = null;
                    }
                }
            }
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        ///   Valore minimo
        /// </summary>
        public long? MinValue { get; private set; }

        /// <summary>
        ///   Valore massimo
        /// </summary>
        public long? MaxValue { get; private set; }

        /// <summary>
        ///   ObjectIdentifier valore minimo
        /// </summary>
        public ObjectIdentifier MinValueOid { get; private set; }

        /// <summary>
        ///   ObjectIdentifier valore massimo
        /// </summary>
        public ObjectIdentifier MaxValueOid { get; private set; }

        /// <summary>
        ///   ObjectIdentifier tabellare valore minimo
        /// </summary>
        public ObjectIdentifier MinTableValueOid { get; private set; }

        /// <summary>
        ///   ObjectIdentifier tabellare valore massimo
        /// </summary>
        public ObjectIdentifier MaxTableValueOid { get; private set; }

        /// <summary>
        ///   Indice della colonna della tabella SNMP sui cui eseguire le ricerche, per il valore minimo
        /// </summary>
        public int? MinTableFilterColumnIndex { get; private set; }

        /// <summary>
        ///   Espressione regolare da applicare alla colonna della tabella SNMP, per ottenere la riga cercata, per il valore minimo
        /// </summary>
        public Regex MinTableFilterColumnRegex { get; private set; }

        /// <summary>
        ///   Codice C# per l'elaborazione della riga della tabella restituita dalla query SNMP, per il valore minimo
        /// </summary>
        public string MinTableEvaluationFormula { get; private set; }

        /// <summary>
        ///   Indice della colonna della tabella SNMP sui cui eseguire le ricerche, per il valore massimo
        /// </summary>
        public int? MaxTableFilterColumnIndex { get; private set; }

        /// <summary>
        ///   Espressione regolare da applicare alla colonna della tabella SNMP, per ottenere la riga cercata, per il valore massimo
        /// </summary>
        public Regex MaxTableFilterColumnRegex { get; private set; }

        /// <summary>
        ///   Codice C# per l'elaborazione della riga della tabella restituita dalla query SNMP, per il valore massimo
        /// </summary>
        public string MaxTableEvaluationFormula { get; private set; }

        #endregion

        #region Metodi privati

        /// <summary>
        /// Tipo del valore del range
        /// </summary>
        private enum ValueType
        {
            /// <summary>
            /// Valore minimo
            /// </summary>
            Min,

            /// <summary>
            /// Valore massimo
            /// </summary>
            Max
        };

        /// <summary>
        /// Stabilisce il tipo di dato in ingresso, in base ad una codifica e imposta i campi appropriati in base al tipo di valore
        /// </summary>
        /// <param name="value">Valore stringa in ingressp</param>
        /// <param name="type">Tipo del valore del range</param>
        private void ParseValue(string value, ValueType type)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                           "Il valore indicato per l'intervallo di valutazione è vuoto. Valore contenuto: '{0}'",
                                                                           value));
            }

            if (value.StartsWith("1.3.6."))
            {
                #region Gestione range dati da richieste SNMP, con Oid

                if (value.IndexOf("§") > 0)
                {
                    #region Gestione Oid tabellare e relativi sotto componenti di configutazione

                    string[] values = value.Split('§');

                    ObjectIdentifier tableOid = SnmpUtility.TryParseOid(values[0]);

                    if (tableOid == null)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "Il valore indicato per l'intervallo di valutazione non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{0}'",
                                                                                   value));
                    }

                    string tableFilterColumnIndexString = this.GetParamData("TableFilterColumnIndex", value);
                    string tableFilterColumnRegexString = this.GetParamData("TableFilterColumnRegex", value);
                    string tableEvaluationFormulaString = this.GetParamData("TableEvaluationFormula", value);

                    #region Verifica presenza TableFilterColumnIndex e TableFilterColumnRegex

                    // Non è presente la TableRowIndex, quindi sono obbligatori TableFilterColumnIndex e TableFilterColumnRegex
                    if ((tableFilterColumnIndexString == null) || (tableFilterColumnIndexString.Trim().Length == 0) ||
                        (tableFilterColumnRegexString == null) || (tableFilterColumnRegexString.Trim().Length == 0))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "Impossibile trovare le componenti 'TableFilterColumnIndex' o 'TableFilterColumnRegex' sull'attributo '{0}' del nodo 'FieldValueDescription', oppure indice di colonna per tabella SNMP o espressione regolare per ricerca nulli. Il contenuto dell'attributo è:\r\n{1}",
                                                                                   this.GetComponentName(type), value));
                    }

                    #endregion

                    #region TableFilterColumnIndex

                    int? tableFilterColumnIndex = null;

                    int tableFilterColumnIndexValue;

                    if (!int.TryParse(tableFilterColumnIndexString, out tableFilterColumnIndexValue))
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "La componente 'TableFilterColumnIndex' sull'attributo '{0}' del nodo 'FieldValueDescription', non contiene un valore numerico intero. Valore contenuto: '{1}'",
                                                                                   this.GetComponentName(type), tableFilterColumnIndexString));
                    }

                    tableFilterColumnIndex = tableFilterColumnIndexValue;

                    #endregion

                    #region TableFilterColumnRegex

                    Regex tableFilterColumnRegex = null;

                    try
                    {
                        tableFilterColumnRegex = new Regex(tableFilterColumnRegexString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                    }
                    catch (ArgumentNullException)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "La componente 'TableFilterColumnRegex' sull'attributo '{0}' del nodo 'FieldValueDescription', non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                                                   this.GetComponentName(type), tableFilterColumnRegexString));
                    }
                    catch (ArgumentException)
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "La componente 'TableFilterColumnRegex' sull'attributo '{0}' del nodo 'FieldValueDescription', non contiene un'espressione regolare valida. Valore contenuto: '{1}'",
                                                                                   this.GetComponentName(type), tableFilterColumnRegexString));
                    }

                    #endregion

                    #region TableEvaluationFormula

                    string evaluationFormula = null;

                    if ((tableEvaluationFormulaString != null) && (tableEvaluationFormulaString.Trim().Length > 0))
                    {
                        evaluationFormula = tableEvaluationFormulaString;
                    }
                    else
                    {
                        throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                   "La componente 'TableEvaluationFormula' sull'attributo '{0}' del nodo 'FieldValueDescription', è nulla o vuota. Valore contenuto: '{1}'",
                                                                                   this.GetComponentName(type), tableEvaluationFormulaString));
                    }

                    #endregion

                    switch (type)
                    {
                        case ValueType.Min:
                            this.MinTableValueOid = tableOid;
                            this.MinValueOid = null;
                            this.MinTableFilterColumnIndex = tableFilterColumnIndex;
                            this.MinTableFilterColumnRegex = tableFilterColumnRegex;
                            this.MinTableEvaluationFormula = evaluationFormula;
                            break;
                        case ValueType.Max:
                            this.MaxTableValueOid = tableOid;
                            this.MaxValueOid = null;
                            this.MaxTableFilterColumnIndex = tableFilterColumnIndex;
                            this.MaxTableFilterColumnRegex = tableFilterColumnRegex;
                            this.MaxTableEvaluationFormula = evaluationFormula;
                            break;
                    }

                    #endregion
                }
                else
                {
                    #region Gestione Oid scalare

                    switch (type)
                    {
                        case ValueType.Min:

                            this.MinValueOid = SnmpUtility.TryParseOid(value);

                            if (this.MinValueOid == null)
                            {
                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                           "Il valore minimo indicato per l'intervallo di valutazione non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{0}'",
                                                                                           value));
                            }

                            break;
                        case ValueType.Max:
                            this.MaxValueOid = SnmpUtility.TryParseOid(value);

                            if (this.MaxValueOid == null)
                            {
                                throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                           "Il valore massimo indicato per l'intervallo di valutazione non contiene una lista di valori numerici interi separati da punti. Valore contenuto: '{0}'",
                                                                                           value));
                            }

                            break;
                    }

                    #endregion
                }

                #endregion
            }
            else
            {
                #region Gestione valore scalare long

                switch (type)
                {
                    case ValueType.Min:
                        long minValue;

                        if (long.TryParse(value, out minValue))
                        {
                            this.MinValue = minValue;
                        }
                        else
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                       "Il valore minimo indicato per l'intervallo di valutazione non contiene un valore numerico intero. Valore contenuto: '{0}'",
                                                                                       value));
                        }
                        break;
                    case ValueType.Max:
                        long maxValue;

                        if (long.TryParse(value, out maxValue))
                        {
                            this.MaxValue = maxValue;
                        }
                        else
                        {
                            throw new DeviceConfigurationHelperException(string.Format(CultureInfo.InvariantCulture,
                                                                                       "Il valore massimo indicato per l'intervallo di valutazione non contiene un valore numerico intero. Valore contenuto: '{0}'",
                                                                                       value));
                        }
                        break;
                }

                #endregion
            }
        }

        /// <summary>
        /// Estrae i dati dal valore dell'attributo, in base al nome della componente. Le varie componenti sono separate da §
        /// </summary>
        /// <param name="paramName">Nome della componente</param>
        /// <param name="paramData">Stringa che contiene tutti i dati dell'attributo</param>
        /// <returns>Valore indicato sulla singola componente</returns>
        private string GetParamData(string paramName, string paramData)
        {
            Regex regex = new Regex(string.Format("{0}=(.*?)(?:§|$)", paramName), RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

            Match data = regex.Match(paramData);

            return data.Groups[1].Value;
        }

        /// <summary>
        /// In base al tipo di valore del range, restituisce un nome leggibile
        /// </summary>
        /// <param name="type">Tipo di valore del range (minimo / massimo)</param>
        /// <returns>Stringa con il nome leggibile dell'attributo del range</returns>
        private string GetComponentName(ValueType type)
        {
            switch (type)
            {
                case ValueType.Min:
                    return "RangeValueMin";
                case ValueType.Max:
                    return "RangeValueMax";
                default:
                    return string.Empty;
            }
        }

        #endregion
    }
}
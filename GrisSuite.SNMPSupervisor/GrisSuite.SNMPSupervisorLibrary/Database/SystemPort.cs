﻿using System.Globalization;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
	/// <summary>
	///     Porta di comunicazione per la periferica
	/// </summary>
	public class SystemPort
	{
		private const string NO_PARAMETERS = "n/d";
		private const string PORT_ENABLED_STATUS = "Porta attiva";
		private const string PORT_NOT_ENABLED_STATUS = "Porta non attiva";

		/// <summary>
		///     ID della Porta
		/// </summary>
		public int Id { get; private set; }

		/// <summary>
		///     Nome della Porta
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		///     Tipo della Porta
		/// </summary>
		public string Type { get; private set; }

		/// <summary>
		///     Parametri della Porta
		/// </summary>
		public string Parameters { get; private set; }

		/// <summary>
		///     Stato della Porta
		/// </summary>
		public string Status { get; private set; }

		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="id">ID della Porta</param>
		/// <param name="name">Nome della Porta</param>
		/// <param name="type">Tipo della Porta</param>
		/// <param name="parameters">Parametri della Porta</param>
		/// <param name="status">Stato della Porta</param>
		public SystemPort(int id, string name, string type, string parameters, string status)
		{
			this.Id = id;
			this.Name = name;
			this.Type = type;
			this.Parameters = parameters;
			this.Status = status;
		}

		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="id">ID della Porta</param>
		/// <param name="name">Nome della Porta</param>
		/// <param name="type">Tipo della Porta</param>
		/// <param name="enabled">Indica se la porta è attiva o meno</param>
		public SystemPort(int id, string name, string type, bool enabled)
		{
			this.Id = id;
			this.Name = name;
			this.Type = type;
			this.Parameters = NO_PARAMETERS;
			this.Status = (enabled ? PORT_ENABLED_STATUS : PORT_NOT_ENABLED_STATUS);
		}

		/// <summary>
		///     Produce una rappresentazione completa della porta
		/// </summary>
		/// <returns>Rappresentazione testuale della porta</returns>
		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture,
				"Port Id: {0}, Port Name: {1}, Port Type: {2}, Port Parameters: {3}, Port Status: {4}", this.Id, this.Name,
				this.Type, this.Parameters, this.Status);
		}
	}
}
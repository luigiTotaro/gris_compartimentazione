﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
	/// <summary>
	///     Rappresenta gli oggetti indicati per la decodifica dei valori SNMP
	/// </summary>
	public class DBStreamFieldValueDescription : System.ICloneable
	{
		/// <summary>
		///     Costruttore privato
		/// </summary>
		/// <param name="field">DBStreamField da valutare</param>
		/// <param name="isDefault">Indica se è il valore predefinito</param>
		/// <param name="valueDescription">Descrizione da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
		/// <param name="severityLevelIfMatch">Severità da assegnare al Field nel caso il valore sia uguale a quello indicato nei parametri successivi</param>
		/// <param name="exactStringValueToCompare">Valore di tipo stringa da confrontare con quello recuperato dalla query SNMP</param>
		/// <param name="exactIntValueToCompare">Valore di tipo intero da confrontare con quello recuperato dalla query SNMP</param>
		/// <param name="rangeValueToCompare">Valore di tipo intervallo da confrontare con quello recuperato dalla query SNMP</param>
		/// <param name="regexValueString">Espressione regolare per il match con il valore stringa recuperato dalla query SNMP</param>
		/// <param name="shouldSendNotificationByEmail">Indica se inviare una notifica per mail nel caso che questa sia la valutazione subentrata</param>
		public DBStreamFieldValueDescription(DBStreamField field,
		                                     bool isDefault,
		                                     string valueDescription,
		                                     Severity severityLevelIfMatch,
		                                     string exactStringValueToCompare,
		                                     long? exactIntValueToCompare,
		                                     DBStreamFieldValueRange rangeValueToCompare,
		                                     Regex regexValueString,
		                                     bool shouldSendNotificationByEmail)
		{
			this.Field = field;
			this.IsDefault = isDefault;
			this.ValueDescription = valueDescription;
			this.SeverityLevelIfMatch = severityLevelIfMatch;
			this.ExactStringValueToCompare = exactStringValueToCompare;
			this.ExactIntValueToCompare = exactIntValueToCompare;
			this.RegexValueStringToMatch = regexValueString;
			this.ShouldSendNotificationByEmailIfMatch = shouldSendNotificationByEmail;
			this.ParseDescriptions(valueDescription);

			this.RangeValueToCompare = rangeValueToCompare ?? new DBStreamFieldValueRange(null, null);
		}

		/// <summary>
		///     Field relativo alla decodifica dei valori
		/// </summary>
		public DBStreamField Field { get; set; }

		/// <summary>
		///     Indica se il valore è quello predefinito
		/// </summary>
		public bool IsDefault { get; private set; }

		/// <summary>
		///     Indica se inviare una notifica per mail nel caso che questa sia la valutazione subentrata
		/// </summary>
		public bool ShouldSendNotificationByEmailIfMatch { get; private set; }

		/// <summary>
		///     Severità assegnata al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		public Severity SeverityLevelIfMatch { get; private set; }

		/// <summary>
		///     Valore statico di tipo stringa da confrontare con il valore recuperato
		/// </summary>
		public string ExactStringValueToCompare { get; private set; }

		/// <summary>
		///     Valore statico di tipo intero da confrontare con il valore recuperato
		/// </summary>
		public long? ExactIntValueToCompare { get; private set; }

		/// <summary>
		///     Valore statico di tipo intervallo da confrontare con il valore recuperato
		/// </summary>
		public DBStreamFieldValueRange RangeValueToCompare { get; private set; }

		/// <summary>
		///     Espressione regolare per il match con il valore stringa recuperato
		/// </summary>
		public Regex RegexValueStringToMatch { get; private set; }

		/// <summary>
		///     Ritorna la descrizione assegnata al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		/// <param name="descriptionKey">Chiave per eseguire la lookup nell'elenco descrizioni</param>
		/// <returns></returns>
		public string DescriptionIfMatch(string descriptionKey)
		{
			if (this.DescriptionsIfMatch != null)
			{
				if (this.DescriptionsIfMatch.ContainsKey(descriptionKey))
				{
					return this.DescriptionsIfMatch[descriptionKey];
				}
			}
			else
			{
				return this.ValueDescription;
			}

			return string.Empty;
		}

		/// <summary>
		///     Descrizione assegnata al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		private string ValueDescription { get; set; }

		/// <summary>
		///     Dizionario di descrizioni assegnate al valore del Field nel caso di corrispondenza con le regole indicate
		/// </summary>
		private Dictionary<string, string> DescriptionsIfMatch { get; set; }

		/// <summary>
		///     Elabora la stringa descrittiva, nel caso contenga un array di possibili valori, per decodifica enumerato
		/// </summary>
		/// <param name="descriptionIfMatch"></param>
		private void ParseDescriptions(string descriptionIfMatch)
		{
			if (!string.IsNullOrEmpty(descriptionIfMatch))
			{
				string[] descriptions = descriptionIfMatch.Split('§');

				if (descriptions.Length > 1)
				{
					this.DescriptionsIfMatch = new Dictionary<string, string>();

					Regex regex = new Regex("(?<key>.*?)=(?<description>.*)", RegexOptions.CultureInvariant | RegexOptions.Compiled);

					foreach (string valueDescription in descriptions)
					{
						Match data = regex.Match(valueDescription);
						if ((data.Groups.Count == 3) && (data.Groups["key"] != null) && (data.Groups["description"] != null) &&
						    (!string.IsNullOrEmpty(data.Groups["key"].Value)) && (!string.IsNullOrEmpty(data.Groups["description"].Value)))
						{
							this.DescriptionsIfMatch.Add(data.Groups["key"].Value, data.Groups["description"].Value);
						}
					}
				}
			}
		}

        /// <summary>
        ///     Metodo per la clonazione dell'oggetto
        /// </summary>
        public object Clone()
        {
            DBStreamFieldValueDescription cloneObj = new DBStreamFieldValueDescription(
                this.Field,
                this.IsDefault,
                this.ValueDescription,
                this.SeverityLevelIfMatch,
                this.ExactStringValueToCompare,
                this.ExactIntValueToCompare,
                this.RangeValueToCompare,
                this.RegexValueStringToMatch,
                this.ShouldSendNotificationByEmailIfMatch
            );

            if(this.RangeValueToCompare != null) {
                if (this.RangeValueToCompare.MinValue != null && this.RangeValueToCompare.MaxValue != null)
                {
                    cloneObj.RangeValueToCompare = new DBStreamFieldValueRange("" + this.RangeValueToCompare.MinValue, "" + this.RangeValueToCompare.MaxValue);
                }
                else {
                    cloneObj.RangeValueToCompare = new DBStreamFieldValueRange(null, null);
                }
            }


            return cloneObj;
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
	public static class FakeDatabaseUtility
	{
		/// <summary>
		///     Aggiorna lo stato della periferica
		/// </summary>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="severityLevel">Livello di severità del Device</param>
		/// <param name="descriptionStatus">Stato del Device</param>
		/// <param name="offline">Indica se il Device è fuori linea o non raggiungibile</param>
		/// <param name="shouldSendNotificationByEmail">Indica se inviare mail di notifica</param>
		public static void UpdateDeviceStatus(long deviceId,
			int severityLevel,
			string descriptionStatus,
			Byte offline,
			Byte shouldSendNotificationByEmail)
		{
			string dumpFile = CreateDumpFolderAndFile(deviceId);

			if (!string.IsNullOrEmpty(dumpFile))
			{
				string text = string.Format(CultureInfo.InvariantCulture,
					"Device Id: {0}, Severity Level: {1}, Description: {2}, Offline: {3}, ShouldSendNotificationByEmail: {4}\r\n",
					deviceId, severityLevel, descriptionStatus, offline, shouldSendNotificationByEmail);
				FileUtility.AppendStringToFile(dumpFile,
					"<div style=\"font-family:Consolas,Courier;font-size:10pt;\">" + FileUtility.HtmlNewLine(text) + "</div>");
			}
		}

        /*
         * Torna la versione della definizione
         */
        public static String GetDefinitionVersion() {
            return "1.00";
        }

		/// <summary>
		///     Aggiorna il tipo della periferica
		/// </summary>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDeviceType">Nuovo tipo del Device</param>
		public static void UpdateDeviceType(long deviceId, string newDeviceType)
		{
			string dumpFile = CreateDumpFolderAndFile(deviceId);

			if (!string.IsNullOrEmpty(dumpFile))
			{
				string text = string.Format(CultureInfo.InvariantCulture, "Device Id: {0}, New Device Type: {1}\r\n", deviceId,
					newDeviceType);
				FileUtility.AppendStringToFile(dumpFile,
					"<div style=\"font-family:Consolas,Courier;font-size:10pt;\">" + FileUtility.HtmlNewLine(text) + "</div>");
			}
		}

		/// <summary>
		///     Aggiorna il numero di serie della periferica
		/// </summary>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDeviceSerialNumber">Nuovo numero di serie del Device</param>
		public static void UpdateDeviceSerialNumber(long deviceId, string newDeviceSerialNumber)
		{
			string dumpFile = CreateDumpFolderAndFile(deviceId);

			if (!string.IsNullOrEmpty(dumpFile))
			{
				string text = string.Format(CultureInfo.InvariantCulture, "Device Id: {0}, New Device Serial Number: {1}\r\n",
					deviceId, newDeviceSerialNumber);
				FileUtility.AppendStringToFile(dumpFile,
					"<div style=\"font-family:Consolas,Courier;font-size:10pt;\">" + FileUtility.HtmlNewLine(text) + "</div>");
			}
		}

		/// <summary>
		///     Aggiorna la versione della definizione della periferica
		/// </summary>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDefinitionVersion">Nuova versione della definizione</param>
		public static void UpdateDeviceDefinitionVersion(long deviceId, string newDefinitionVersion)
		{
			string dumpFile = CreateDumpFolderAndFile(deviceId);

			if (!string.IsNullOrEmpty(dumpFile))
			{
				string text = string.Format(CultureInfo.InvariantCulture, "Device Id: {0}, New Device Definition Version: {1}\r\n",
					deviceId, newDefinitionVersion);
				FileUtility.AppendStringToFile(dumpFile,
					"<div style=\"font-family:Consolas,Courier;font-size:10pt;\">" + FileUtility.HtmlNewLine(text) + "</div>");
			}
		}

		/// <summary>
		///     Aggiorna lo Stream
		/// </summary>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="streamId">Id dello Stream</param>
		/// <param name="name">Nome dello Stream</param>
		/// <param name="severityLevel">Livello di severità dello Stream</param>
		/// <param name="description">Descrizione del valore dello Stream</param>
		public static void UpdateStream(long deviceId, int streamId, string name, int severityLevel, string description)
		{
			string dumpFile = CreateDumpFolderAndFile(deviceId);

			if (!string.IsNullOrEmpty(dumpFile))
			{
				string text = string.Format(CultureInfo.InvariantCulture,
					"Stream Id: {0}, Stream Name: {1}, Severity Level: {2}, Description: {3}\r\n", streamId, name, severityLevel,
					description);
				FileUtility.AppendStringToFile(dumpFile,
					"<div style=\"font-family:Consolas,Courier;font-size:10pt;\">" + FileUtility.HtmlNewLine(text) + "</div>");
			}
		}

		/// <summary>
		///     Aggiorna il Field
		/// </summary>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="fieldId">Id del Field</param>
		/// <param name="arrayId">Id del vettore dei valori del Field</param>
		/// <param name="name">Nome del Field</param>
		/// <param name="severityLevel">Livello di severità del Field</param>
		/// <param name="value">Valore del Field</param>
		/// <param name="description">Descrizione del valore del Field</param>
		/// <param name="shouldSendNotificationByEmail">Indica se inviare mail di notifica</param>
		public static void UpdateStreamField(long deviceId,
			int fieldId,
			int arrayId,
			string name,
			int severityLevel,
			string value,
			string description,
			Byte shouldSendNotificationByEmail)
		{
			string dumpFile = CreateDumpFolderAndFile(deviceId);

			if (!string.IsNullOrEmpty(dumpFile))
			{
				string text = string.Format(CultureInfo.InvariantCulture,
					"Stream Field Id: {0}, Array Id: {1}, Stream Field Name: {2}, Severity Level: {3}, Value: {4}, Description: {5}, ShouldSendNotificationByEmail: {6}\r\n",
					fieldId, arrayId, name, severityLevel, value, description, shouldSendNotificationByEmail);
				FileUtility.AppendStringToFile(dumpFile,
					"<div style=\"font-family:Consolas,Courier;font-size:10pt;\">" + FileUtility.HtmlNewLine(text) + "</div>");
			}
		}

		/// <summary>
		///     Recupera una singola periferica dalla lista dei dispositivi configurati nel System.xml, dato il suo indirizzo IP
		///     numerico
		/// </summary>
		/// <param name="address">Indirizzo IP in forma numerica</param>
		/// <returns>Device trovato. Null nel caso non esista.</returns>
		public static DeviceObject GetMonitorDeviceByAddress(string address)
		{
			DeviceObject device = null;

			ReadOnlyCollection<DeviceObject> devices =
				SystemXmlHelper.GetMonitorDeviceList(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "System.xml"));

			if (devices != null)
			{
				foreach (DeviceObject deviceObject in devices)
				{
					if (deviceObject.Active == 1)
					{
						if (deviceObject.Address.Equals(DataUtility.DecodeIPAddress(address)))
						{
							device = deviceObject;
							break;
						}
					}
				}
			}

			return device;
		}

		/// <summary>
		///     Recupera una singola periferica dalla lista dei dispositivi configurati nel System.xml, dato il suo Id di
		///     periferica
		/// </summary>
		/// <param name="deviceId">Device Id della periferica</param>
		/// <returns>Device trovato. Null nel caso non esista.</returns>
		public static DeviceObject GetMonitorDeviceByOriginalDevId(long deviceId)
		{
			DeviceObject device = null;

			ReadOnlyCollection<DeviceObject> devices =
				SystemXmlHelper.GetMonitorDeviceList(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "System.xml"));

			if (devices != null)
			{
				foreach (DeviceObject deviceObject in devices)
				{
					if (deviceObject.Active == 1)
					{
						if (new DeviceIdentifier(deviceObject.DeviceId).OriginalDeviceId == deviceId)
						{
							device = deviceObject;
							break;
						}
					}
				}
			}

			return device;
		}

		/// <summary>
		///     Verifica l'esistenza di una periferica in base dati
		/// </summary>
		public static bool? CheckDeviceExistence()
		{
			return true;
		}

		/// <summary>
		///     Recupera i dati di lookup per poter creare una periferica su database
		/// </summary>
		/// <returns>Dati di lookup con l'Id di Stazione, Fabbricato, Armadio e Interfaccia</returns>
		public static DeviceDatabaseSupport GetStationBuildingRackPortData()
		{
			return new DeviceDatabaseSupport(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());
		}

		/// <summary>
		///     Aggiorna o inserisce i dati della periferica su database
		/// </summary>
		/// <param name="deviceId">Id della periferica</param>
		/// <param name="nodeId">Id del Nodo</param>
		/// <param name="serverId">Id del Server</param>
		/// <param name="deviceName">Nome della periferica</param>
		/// <param name="deviceType">Tipo della periferica</param>
		/// <param name="serialNumber">Numero di serie della periferica</param>
		/// <param name="deviceAddress">Indirizzo IP della periferica</param>
		/// <param name="portId">Id dell'interfaccia della periferica</param>
		/// <param name="profileId">Id del profilo della periferica</param>
		/// <param name="active">Stato della periferica</param>
		/// <param name="scheduled">Stato di schedulazione della periferica</param>
		/// <param name="removed">Stato di rimozione della periferica</param>
		/// <param name="rackId">Id dell'Armadio</param>
		/// <param name="rackPositionRow">Indice della posizione di riga nell'Armadio</param>
		/// <param name="rackPositionCol">Indice della posizione di colonna nell'Armadio</param>
		/// <param name="monitoringDeviceId">Identifica l'apparato che esegue il monitoraggio della periferica</param>
		/// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
		public static bool UpdateDevice(long deviceId,
			long nodeId,
			int serverId,
			string deviceName,
			string deviceType,
			string serialNumber,
			IPAddress deviceAddress,
			Guid portId,
			int profileId,
			Byte active,
			Byte scheduled,
			Byte removed,
			Guid rackId,
			int rackPositionRow,
			int rackPositionCol,
			string monitoringDeviceId)
		{
			string dumpFile = CreateDumpFolderAndFile(deviceId);

			if (!string.IsNullOrEmpty(dumpFile))
			{
				string text = string.Format(CultureInfo.InvariantCulture,
					"Device Id: {0}, Nome Periferica: {1}, Indirizzo IP: {2}, Type: {3}, Node Id: {4}, Server Id: {5}, Serial Number: {6}, Port Id: {7}, Profile Id: {8}, Active: {9}, Scheduled: {10}, RackPositionColumn: {11}, RackPositionRow: {12}, Removed: {13}, Rack Id: {14}, Monitoring Device Id: {15}",
					deviceId, deviceName, DataUtility.EncodeIPAddress(deviceAddress), deviceType, nodeId, serverId, serialNumber,
					portId, profileId, active, scheduled, rackPositionCol, rackPositionRow, removed, rackId, monitoringDeviceId);

				FileUtility.AppendStringToFile(dumpFile,
					"<hr/><div style=\"font-family:Consolas,Courier;font-size:10pt;font-weight:bold;\">[" +
					DateTime.Now.ToString(@"yyyy/MM/dd HH\:mm\:ss.fff", CultureInfo.InvariantCulture) + "]<br/>" +
					FileUtility.HtmlNewLine(text) + "</div>");
			}

			return true;
		}

		/// <summary>
		///     Elimina i dati della periferica su database
		/// </summary>
		/// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
		public static bool DeleteDevice()
		{
			return true;
		}

		/// <summary>
		///     Recupera i dati delle tacitazioni relative al supervisore corrente
		/// </summary>
		/// <returns>Lista di tacitazioni</returns>
		public static ReadOnlyCollection<DbDeviceAcknowledgement> GetDeviceAcknowledgementsList()
		{
			return new List<DbDeviceAcknowledgement>().AsReadOnly();
		}

		/// <summary>
		///     Elimina le tacitazioni scadute (rispetto al momento corrente) e al supervisore in esecuzione
		/// </summary>
		/// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
		public static bool DeleteExpiredAcknowledgements()
		{
			return true;
		}

        /// <summary>
        ///     Elimina tutti gli streamfield legati ad uno stream ed ad un dato device
        /// </summary>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool DeleteStreamField() {
            return true;
        }

		/// <summary>
		///     Crea un nome di file completo per il dump dei dati della periferica su disco
		/// </summary>
		/// <param name="deviceId"></param>
		/// <returns></returns>
		private static string CreateDumpFolderAndFile(long deviceId)
		{
			string dumpFolderAndFile = null;

			try
			{
				string dumpFolderTemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Dump\\");
				Directory.CreateDirectory(dumpFolderTemp);

				if (!string.IsNullOrEmpty(dumpFolderTemp))
				{
					dumpFolderAndFile = Path.Combine(dumpFolderTemp, string.Format(CultureInfo.InvariantCulture, "{0}.htm", deviceId));
				}
			}
			catch (ArgumentException)
			{
			}
			catch (UnauthorizedAccessException)
			{
			}
			catch (IOException)
			{
			}
			catch (ObjectDisposedException)
			{
			}
			catch (NotSupportedException)
			{
			}

			return dumpFolderAndFile;
		}
	}
}
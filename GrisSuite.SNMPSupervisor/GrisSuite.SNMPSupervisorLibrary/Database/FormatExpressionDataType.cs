﻿namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    // Tipo di dati supportato dalla stringa di formattazione
    public enum FormatExpressionDataType
    {
        /// <summary>
        ///   Non definito
        /// </summary>
        Unknown,
        /// <summary>
        ///   Stringa
        /// </summary>
        String,
        /// <summary>
        ///   Intero a 64 bit
        /// </summary>
        Long,
        /// <summary>
        /// Formato data/ora SNMP
        /// </summary>
        /// <example>
        /// This class represents the DateAndTime data type as defined in SNMPV2-TC
        /// DateAndTime ::= TEXTUAL-CONVENTION
        /// DISPLAY-HINT "2d-1d-1d,1d:1d:1d.1d,1a1d:1d"
        /// STATUS       current
        /// DESCRIPTION
        /// "A date-time specification.
        /// field  octets  contents                  range
        /// -----  ------  --------                  -----
        /// 1      1-2   year*                     0..65536
        /// 2       3    month                     1..12
        /// 3       4    day                       1..31
        /// 4       5    hour                      0..23
        /// 5       6    minutes                   0..59
        /// 6       7    seconds                   0..60
        ///              (use 60 for leap-second)
        /// 7       8    deci-seconds              0..9
        /// 8       9    direction from UTC        '+' / '-'
        /// 9      10    hours from UTC*           0..13
        /// 10     11    minutes from UTC          0..59
        /// * Notes:
        /// - the value of year is in network-byte order
        /// - daylight saving time in New Zealand is +13
        /// 
        /// For example, Tuesday May 26, 1992 at 1:30:15 PM EDT would be
        /// displayed as:
        /// 
        /// 1992-5-26,13:30:15.0,-4:0
        /// 
        /// Note that if only local time is known, then timezone
        /// information (fields 8-10) is not present."
        /// SYNTAX       OCTET STRING (SIZE (8 | 11))
        /// </example>
        DateAndTime
    } ;
}
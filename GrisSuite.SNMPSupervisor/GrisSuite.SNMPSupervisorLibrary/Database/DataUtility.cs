﻿using System.Globalization;
using System.Net;
using System.Net.Sockets;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    public static class DataUtility
    {
        /// <summary>
        ///   Decodifica una stringa che contiene un valore a 64 bit in un indirizzo IP
        /// </summary>
        /// <param name = "address"></param>
        /// <returns></returns>
        public static IPAddress DecodeIPAddress(string address)
        {
            long ip;
            if (long.TryParse(address, out ip))
            {
                if (ip == 0)
                {
                    return null;
                }

                return
                    new IPAddress(new[]
                                  {(byte) ((uint) ip >> 24), (byte) (((uint) ip & 0x00FF0000) >> 16), (byte) (((uint) ip & 0x0000FF00) >> 8), (byte) (((uint) ip & 0x000000FF))});
            }

            return null;
        }

        /// <summary>
        ///   Ritorna una stringa che contiene un valore a 64 bit dato un indirizzo IP
        /// </summary>
        /// <param name = "address"></param>
        /// <returns></returns>
        public static string EncodeIPAddress(IPAddress address)
        {
            long ip = 0;

            if (address != null)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    byte[] ipBytes = address.GetAddressBytes();
                    if (ipBytes.Length == 4)
                    {
                        ip = (((uint) ipBytes[0] << 24) | ((uint) ipBytes[1] << 16) | ((uint) ipBytes[2] << 8) | ipBytes[3]);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }

            return ip.ToString(CultureInfo.InvariantCulture);
        }
    }
}
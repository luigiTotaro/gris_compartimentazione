﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    /// Oggetto che contiene i dati di tacitazione per la periferica
    /// </summary>
    public class DeviceAcknowledgement
    {
        /// <summary>
        /// Device Id
        /// </summary>
        public long DeviceId { get; private set; }

        /// <summary>
        /// Indica se l'intera periferica è stata tacitata
        /// </summary>
        public bool IsDeviceAcknowledged { get; private set; }

        /// <summary>
        /// Collezione di Stream su cui è stata impostata la tacitazione
        /// </summary>
        public ReadOnlyCollection<DeviceStreamAcknowledgement> DeviceStreamAcknowledgements
        {
            get { return this.DeviceStreamAcks.AsReadOnly(); }
        }

        /// <summary>
        /// Collezione di Stream su cui è stata impostata la tacitazione
        /// </summary>
        private List<DeviceStreamAcknowledgement> DeviceStreamAcks { get; set; }

        /// <summary>
        /// Collezione di Stream Field su cui è stata impostata la tacitazione
        /// </summary>
        public ReadOnlyCollection<DeviceStreamFieldAcknowledgement> DeviceStreamFieldAcknowledgements
        {
            get { return this.DeviceStreamFieldAcks.AsReadOnly(); }
        }

        /// <summary>
        /// Collezione di Stream Field su cui è stata impostata la tacitazione
        /// </summary>
        private List<DeviceStreamFieldAcknowledgement> DeviceStreamFieldAcks { get; set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="deviceId">Device Id</param>
        public DeviceAcknowledgement(long deviceId)
        {
            this.DeviceId = deviceId;
            this.IsDeviceAcknowledged = false;
            this.DeviceStreamAcks = new List<DeviceStreamAcknowledgement>();
            this.DeviceStreamFieldAcks = new List<DeviceStreamFieldAcknowledgement>();
        }

        /// <summary>
        /// Aggiunge una tacitazione per periferica, decidendo il tipo in base ai prametri
        /// </summary>
        /// <param name="streamId">StreamID della tacitazione (opzionale)</param>
        /// <param name="fieldId">FieldID della tacitazione (opzionale)</param>
        public void AddDeviceAcknowledgement(int? streamId, int? fieldId)
        {
            if ((!streamId.HasValue) && (!fieldId.HasValue))
            {
                // Se è indicato solo il DevID, allora è una tacitazione per intera periferica
                this.IsDeviceAcknowledged = true;
            }

            if ((streamId.HasValue) && (!fieldId.HasValue))
            {
                // Se è indicato solo lo StreamID e non il FieldID, è una tacitazione per Stream
                this.DeviceStreamAcks.Add(new DeviceStreamAcknowledgement(streamId.Value));
            }

            if ((streamId.HasValue) && (fieldId.HasValue))
            {
                // Se è indicato sia lo StreamID che il FieldID, è una tacitazione per StreamField
                this.DeviceStreamFieldAcks.Add(new DeviceStreamFieldAcknowledgement(streamId.Value, fieldId.Value));
            }
        }

        /// <summary>
        /// Rappresentazione stringa dell'oggetto
        /// </summary>
        /// <returns>Stringa che contiene l'oggetto</returns>
        public override string ToString()
        {
            StringBuilder streamAcks = new StringBuilder();
            bool appendSeparator = false;
            foreach (DeviceStreamAcknowledgement streamAcknowledgement in this.DeviceStreamAcks)
            {
                streamAcks.AppendFormat("{0}{1}", (appendSeparator ? ", " : string.Empty), streamAcknowledgement.StreamId);
                appendSeparator = true;
            }

            if (streamAcks.Length == 0)
            {
                streamAcks.Append("Nessuno");
            }

            StringBuilder streamFieldAcks = new StringBuilder();
            appendSeparator = false;
            foreach (DeviceStreamFieldAcknowledgement streamFieldAcknowledgement in this.DeviceStreamFieldAcks)
            {
                streamFieldAcks.AppendFormat("{0}{1}-{2}", (appendSeparator ? ", " : string.Empty), streamFieldAcknowledgement.StreamId,
                                             streamFieldAcknowledgement.StreamFieldId);
                appendSeparator = true;
            }

            if (streamFieldAcks.Length == 0)
            {
                streamFieldAcks.Append("Nessuno");
            }

            return string.Format(CultureInfo.InvariantCulture,
                                 "Device Id: {0}, Tacitazione a livello di periferica: {1}, Stream tacitati: {2}, Stream Field tacitati: {3}",
                                 this.DeviceId, (this.IsDeviceAcknowledged ? "Sì" : "No"), streamAcks, streamFieldAcks);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text.RegularExpressions;
using GrisSuite.SnmpSupervisorLibrary.Devices;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Lextm.SharpSnmpLib;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///     Rappresenta il singolo Field da persistere su database
    /// </summary>
    public class DBStreamField
    {
        #region Costanti

        // Costanti per maschere di bit nel formato SNMP (bit meno significativo a sinistra), espresse in OctetString
        private const int SNMP_BITS_MASK_PATTERN_LENGTH = 24;
        private const string SNMP_BITS_MASK_STRING_PREFIX = "BITS(0x";
        private const string SNMP_BITS_MASK_STRING_SUFFIX = ")";
        // Lunghezza stringa "BITS(" - senza prefisso 0x che indica valore esadecimale
        private const int SNMP_BITS_MASK_PREFIX_LENGTH = 5;
        // 16 caratteri per la maschera più due caratteri per prefisso 0x - esempio 0x0004000000000000 - maschera a 8 byte, per intero a 64 bit
        private const int SNMP_BITS_MASK_LENGTH = 18;

        // Costanti per maschere di bit nel formato classico, espresse in ulong
        private const int BIT_MASK_PATTERN_LENGTH = 24;
        private const string BIT_MASK_STRING_PREFIX = "MASK(0x";
        private const string BIT_MASK_STRING_SUFFIX = ")";
        // Lunghezza stringa "MASK(" - senza prefisso 0x che indica valore esadecimale
        private const int BIT_MASK_PREFIX_LENGTH = 5;
        // 16 caratteri per la maschera più due caratteri per prefisso 0x - esempio 0x0004000000000000 - maschera a 8 byte, per intero a 64 bit
        private const int BIT_MASK_LENGTH = 18;

        private const string SNMP_RAW_OCTETSTRING_BYTES_PREFIX = "(0x";
        private const string SNMP_RAW_OCTETSTRING_BYTES_SUFFIX = ")";
        // Lunghezza stringa "(" - senza prefisso 0x che indica valore esadecimale
        private const int SNMP_RAW_OCTETSTRING_BYTES_PREFIX_LENGTH = 1;

        #endregion

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="fieldId">Id del Field</param>
        /// <param name="arrayId">Posizione del valore nel vettore, nel caso la query SNMP restituisca più valori</param>
        /// <param name="fieldName">Nome del Field</param>
        /// <param name="fieldOid">OID della query SNMP</param>
        /// <param name="fieldTableOid">OID della query SNMP tabellare</param>
        /// <param name="fieldCorrelationTableOid">OID della query SNMP tabellare correlata</param>
        /// <param name="tableRowIndex">Indice della riga della tabella restituita dalla query SNMP</param>
        /// <param name="evaluationFormula">Codice C# per l'elaborazione della riga della tabella restituita dalla query SNMP</param>
        /// <param name="correlationEvaluationFormula">
        ///     Codice C# per l'elaborazione della riga della tabella correlata restituita
        ///     dalla query SNMP
        /// </param>
        /// <param name="filterColumnIndex">Indice della colonna della tabella SNMP sui cui eseguire le ricerche</param>
        /// <param name="correlationFilterColumnIndex">
        ///     Indice della colonna della tabella SNMP correlata sui cui eseguire le
        ///     ricerche
        /// </param>
        /// <param name="filterColumnRegex">
        ///     Espressione regolare da applicare alla colonna della tabella SNMP, per ottenere la riga
        ///     cercata
        /// </param>
        /// <param name="formatExpression">Espressione di formattazione da applicare al valore</param>
        /// <param name="tableIndex">Indice della riga di tabella dinamica a cui questo stream field si riferisce</param>
        /// <param name="scalarTableCounterOid">OID della query SNMP scalare che indica il numero di righe per una tabella dinamica</param>
        /// <param name="excludedFromSeverityComputation">
        ///     Indica se lo stream field e la sua severity devono essere tenuti in
        ///     considerazione nel calcolo della severità dello stream padre
        /// </param>
        /// <param name="noDataAvailableForcedState">
        ///     Informazioni per la valorizzazione dello Stream nel caso che non siano
        ///     restituiti dati nelle query SNMP
        /// </param>
        /// <param name="tableHideIfNotAvailableRowAndColumnIndex">
        ///     Informazioni per gestire la non persistenza di uno stream field
        ///     in base dati, se non presente la riga cercata nella tabella SNMP
        /// </param>
        /// <param name="fixedValueString">
        ///     Valore stringa da scrivere direttamente in base dati per lo stream field, senza
        ///     recuperarlo dalla periferica
        /// </param>
        /// <param name="hideIfNoDataAvailable">
        ///     Indica se lo stream sarà persistito su database quando la query SNMP non ritorna un
        ///     valore
        /// </param>
        public DBStreamField(int fieldId,
            int arrayId,
            string fieldName,
            ObjectIdentifier fieldOid,
            TableOidAndSupportData fieldTableOid,
            TableOidAndSupportData fieldCorrelationTableOid,
            int? tableRowIndex,
            string evaluationFormula,
            string correlationEvaluationFormula,
            int? filterColumnIndex,
            int? correlationFilterColumnIndex,
            Regex filterColumnRegex,
            FormatExpression formatExpression,
            int? tableIndex,
            ObjectIdentifier scalarTableCounterOid,
            bool excludedFromSeverityComputation,
            NoDataAvailableForcedState noDataAvailableForcedState,
            TableHideIfNotAvailableRowAndColumnIndex tableHideIfNotAvailableRowAndColumnIndex,
            string fixedValueString,
            bool hideIfNoDataAvailable)
        {
            this.FieldId = fieldId;
            this.ArrayId = arrayId;
            this.Name = fieldName;
            this.FieldOid = fieldOid;
            this.FieldTableOid = fieldTableOid;
            this.FieldCorrelationTableOid = fieldCorrelationTableOid;
            this.TableRowIndex = tableRowIndex;
            this.TableEvaluationFormula = evaluationFormula;
            this.TableCorrelationEvaluationFormula = correlationEvaluationFormula;
            this.TableFilterColumnIndex = filterColumnIndex;
            this.TableCorrelationFilterColumnIndex = correlationFilterColumnIndex;
            this.TableFilterColumnRegex = filterColumnRegex;
            this.DbStreamFieldValueDescriptions = new Collection<DBStreamFieldValueDescription>();
            this.ValueFormatExpression = formatExpression;
            this.PersistOnDatabase = true;
            this.TableIndex = tableIndex;
            this.ScalarTableCounterOid = scalarTableCounterOid;
            this.ExcludedFromSeverityComputation = excludedFromSeverityComputation;
            this.NoSnmpDataAvailableForcedState = noDataAvailableForcedState;
            this.HideIfNotAvailableRowAndColumnIndex = tableHideIfNotAvailableRowAndColumnIndex;
            this.FixedValueString = fixedValueString;
            this.HideIfNoDataAvailable = hideIfNoDataAvailable;
            this.DbStreamFieldValueChildren = null;
            this.StreamFieldFather = null;
            this.IsArray = false;
            this.PositionArray = ArrayId;
            this.MaxSizeArray = -1;
            this.LastTableCorrelationRow = 0;
            this.IsAggregation = false;
            this.IsShowName = false;
            this.IsShowValueDescription = false;
            this.IsStreamFieldArray = false;
            this.isFirstChildOfStreamFieldArray = false;
            this.isDiscardData = false;
            this.IsTimeTicksMs = false;
            this.UseToHeadOfStreamFieldArray = false;
            this.StreamFieldArrayid = 0;
            this.ExpressionFormatShowRowId = null;
            this.RowIdZeroBased = false;
            this.EvaluationFormula = null;
            this.MemoryBase = new FieldMemoryBase();
            this.Offset = 0;
        }

        #region Proprietà pubbliche

        /// <summary>
        ///     codice c# per la valutazione di un valore scalare
        /// </summary>
        public String EvaluationFormula { get; set; }

        /// <summary>
        ///     indica che l'id di stramfieldArray parte da 0, altrimenti da 1
        /// </summary>
        public bool RowIdZeroBased { get; set; }

        /// <summary>
        ///     Espressione per la formatazione del nome dello streamfielarray + row id
        /// </summary>
        public String ExpressionFormatShowRowId { get; set; }

        /// <summary>
        ///     offest dalla base nel db
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        ///     Id in un StreamFiedArray
        /// </summary>
        public int StreamFieldArrayid { get; set; }

        /// <summary>
        ///     Indica se è di tipo TimeTicks con codifica in ms
        /// </summary>
        public bool IsTimeTicksMs { get; set; }

        /// <summary>
        ///     Se è uno stream field array padre, e questo valore è true, allora l'elemteno sarà elaborato come la testa dello streamfieldarray
        /// </summary>
        public bool UseToHeadOfStreamFieldArray { get; set; }

        /// <summary>
        ///     Indica se il dato deve essere ignorato
        /// </summary>
        public bool isDiscardData { get; set; }

        /// <summary>
        ///     Indica che l'elemento rappresenta il primo gruppo di elementi creato nello streamFieldArray
        /// </summary>
        public bool isFirstChildOfStreamFieldArray { get; set; }

        public FieldMemoryBase MemoryBase;

        /// <summary>
        ///     Indica che l'elemento rappresenta uno array di stream field
        /// </summary>
        public bool IsStreamFieldArray { get; set; }

        /// <summary>
        ///     Indica se l'elemento deve motrare il nome o la il valore nel campo descrizione del db
        /// </summary>
        public bool IsShowName { get; set; }

        /// <summary>
        ///     Indica che lo streamfield deve mostrare il valueDesciption al posto del valore
        /// </summary>
        public bool IsShowValueDescription { get; set; }

        /// <summary>
        ///     Indica se l'elemento è un deve essere aggregato ad un array
        /// </summary>
        public bool IsAggregation { get; set; }

        /// <summary>
        ///     Ultima riga trovata nella tabella di correlazione
        /// </summary>
        public int LastTableCorrelationRow { get; set; }

        /// <summary>
        ///     Indica la grandezza massima di elementi nell'array( deve essere diverso da -1 ); ha significato solo in presenza di array e se lo streamfield è padre
        /// </summary>
        public int MaxSizeArray { get; set; }

        /// <summary>
        ///     Indica se è un array
        /// </summary>
        public bool IsArray { get; set; }

        /// <summary>
        ///     Indice all'interno dell'array
        /// </summary>
        public int PositionArray { get; set; }

        /// <summary>
        ///     Lista di StreamField figli
        /// </summary>
        public Collection<DBStreamField> DbStreamFieldValueChildren { get; set; }

        /// <summary>
        ///     DBStreamField padre
        /// </summary>
        public DBStreamField StreamFieldFather { get; set; }

        /// <summary>
        ///     Severità relativa al Field
        /// </summary>
        public Severity SeverityLevel { get; set; }

        /// <summary>
        ///     Decodifica descrittiva del valore del Field
        /// </summary>
        public string ValueDescription { get; set; }

        /// <summary>
        ///     Decodifica descrittiva del valore del Field, concatenata alla severità (solo nel caso non sia sconosciuta)
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                if (this.SeverityLevel == Severity.Unknown)
                {
                    return this.ValueDescription;
                }

                return this.ValueDescription + "=" + (int) this.SeverityLevel;
            }
        }

        /// <summary>
        ///     Id del Field
        /// </summary>
        public int FieldId { get; set; }

        /// <summary>
        ///     OID della query SNMP
        /// </summary>
        public ObjectIdentifier FieldOid { get; private set; }

        /// <summary>
        ///     OID della query SNMP tabellare e relativi dati di supporto per tabelle sparse
        /// </summary>
        public TableOidAndSupportData FieldTableOid { get; private set; }

        /// <summary>
        ///     OID della query SNMP tabellare correlata e relativi dati di supporto per tabelle sparse
        /// </summary>
        public TableOidAndSupportData FieldCorrelationTableOid { get; private set; }

        /// <summary>
        ///     Valore del Field, come letto dalla query SNMP
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        ///     Lista di valori
        /// </summary>
        public IList<Variable> RawData { get; set; }

        /// <summary>
        ///     Posizione del valore nel vettore
        /// </summary>
        public int ArrayId { get; set; }

        /// <summary>
        ///     Nome del Field
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///     Lista di descrizioni per la decodifica del valore e della severità
        /// </summary>
        public Collection<DBStreamFieldValueDescription> DbStreamFieldValueDescriptions { get; private set; }

        /// <summary>
        ///     Indice della riga della tabella restituita dalla query SNMP
        /// </summary>
        public int? TableRowIndex { get; set; }

        /// <summary>
        ///     Codice C# per l'elaborazione della riga della tabella restituita dalla query SNMP
        /// </summary>
        public string TableEvaluationFormula { get; private set; }

        /// <summary>
        ///     Codice C# per l'elaborazione della riga della tabella correlata restituita dalla query SNMP
        /// </summary>
        public string TableCorrelationEvaluationFormula { get; private set; }

        /// <summary>
        ///     Indice della colonna della tabella SNMP sui cui eseguire le ricerche
        /// </summary>
        public int? TableFilterColumnIndex { get; private set; }

        /// <summary>
        ///     Indice della colonna della tabella SNMP correlata sui cui eseguire le ricerche
        /// </summary>
        public int? TableCorrelationFilterColumnIndex { get; private set; }

        /// <summary>
        ///     Espressione regolare da applicare alla colonna della tabella SNMP, per ottenere la riga cercata
        /// </summary>
        public Regex TableFilterColumnRegex { get; set; }

        /// <summary>
        ///     Espressione di formattazione da applicare al valore
        /// </summary>
        public FormatExpression ValueFormatExpression { get; private set; }

        /// <summary>
        ///     Indica se lo stream field deve essere salvato sul database
        /// </summary>
        /// <remarks>
        ///     E' possibile che uno stream field di una tabella dinamica non abbia senso,
        ///     a fronte di una valutazione del contatore che riguarda le righe della tabella
        /// </remarks>
        public bool PersistOnDatabase { get; set; }

        /// <summary>
        ///     Indice della riga di tabella dinamica a cui questo stream field si riferisce
        /// </summary>
        public int? TableIndex { get; private set; }

        /// <summary>
        ///     OID della query SNMP scalare che indica il numero di righe per una tabella dinamica
        /// </summary>
        public ObjectIdentifier ScalarTableCounterOid { get; private set; }

        /// <summary>
        ///     Indica se lo stream field e la sua severity devono essere tenuti in considerazione nel calcolo della severità dello
        ///     stream padre
        /// </summary>
        public bool ExcludedFromSeverityComputation { get; set; }

        /// <summary>
        ///     Informazioni per la valorizzazione dello Stream nel caso che non siano restituiti dati nelle query SNMP
        /// </summary>
        public NoDataAvailableForcedState NoSnmpDataAvailableForcedState { get; private set; }

        /// <summary>
        ///     Contiene l'eventuale eccezione generata durante il popolamento con dati del field
        /// </summary>
        public Exception LastError { get; set; }

        /// <summary>
        ///     Informazioni per gestire la non persistenza di uno stream field in base dati, se non presente la riga cercata nella
        ///     tabella SNMP
        /// </summary>
        public TableHideIfNotAvailableRowAndColumnIndex HideIfNotAvailableRowAndColumnIndex { get; private set; }

        /// <summary>
        ///     Valore stringa da scrivere direttamente in base dati per lo stream field, senza recuperarlo dalla periferica
        /// </summary>
        public string FixedValueString { get; private set; }

        /// <summary>
        ///     Indica se lo stream sarà persistito su database quando la query SNMP non ritorna un valore
        /// </summary>
        public bool HideIfNoDataAvailable { get; private set; }

        /// <summary>
        ///     Indica se, a seguito della valutazione dei casi, occorre inviare una notifica per mail, può essere resettato
        ///     dall'esterno, durante la valutazione delle tacitazioni
        /// </summary>
        public bool ShouldSendNotificationByEmail { get; set; }

        #endregion

        private enum EvaluationNeeded
        {
            Unknown,
            StringComparison,
            IntComparison,
            RangeComparison,
            RegexMatch
        }

        private EvaluationNeeded ChooseEvaluationMethod()
        {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

            if ((this.DbStreamFieldValueDescriptions != null) && (this.DbStreamFieldValueDescriptions.Count > 0))
            {
                if (this.DbStreamFieldValueDescriptions.Count == 1)
                {
                    evaluationNeeded = GetEvaluationNeeded(this.DbStreamFieldValueDescriptions[0]);
                }
                else
                {
                    foreach (DBStreamFieldValueDescription dbStreamFieldValueDescription in
                        this.DbStreamFieldValueDescriptions)
                    {
                        if (!dbStreamFieldValueDescription.IsDefault)
                        {
                            // Consideriamo solo il primo tipo - casi di tipi di valori etereogenei non sono supportati
                            evaluationNeeded = GetEvaluationNeeded(dbStreamFieldValueDescription);
                            break;
                        }
                    }
                }
            }

            return evaluationNeeded;
        }

        private static EvaluationNeeded GetEvaluationNeeded(DBStreamFieldValueDescription dbStreamFieldValueDescription)
        {
            EvaluationNeeded evaluationNeeded = EvaluationNeeded.Unknown;

            if ((dbStreamFieldValueDescription.RangeValueToCompare != null) &&
                ((dbStreamFieldValueDescription.RangeValueToCompare.MinValue.HasValue) ||
                 (dbStreamFieldValueDescription.RangeValueToCompare.MaxValue.HasValue)))
            {
                evaluationNeeded = EvaluationNeeded.RangeComparison;
            }
            else if (dbStreamFieldValueDescription.ExactIntValueToCompare.HasValue)
            {
                evaluationNeeded = EvaluationNeeded.IntComparison;
            }
            else if (!string.IsNullOrEmpty(dbStreamFieldValueDescription.ExactStringValueToCompare))
            {
                evaluationNeeded = EvaluationNeeded.StringComparison;
            }
            else if (dbStreamFieldValueDescription.RegexValueStringToMatch != null)
            {
                evaluationNeeded = EvaluationNeeded.RegexMatch;
            }

            return evaluationNeeded;
        }

        /// <summary>
        ///     Esegue la valutazione del valore contenuto nel campo, in base alle regole configurate
        /// </summary>
        public void Evaluate(bool isAliveSnmp)
        {
            if ((this.DbStreamFieldValueDescriptions != null) && (this.DbStreamFieldValueDescriptions.Count > 0))
            {
                if (this.DbStreamFieldValueDescriptions.Count == 1)
                {
                    this.ParseDefaultSingleValue(this.DbStreamFieldValueDescriptions[0].DescriptionIfMatch(string.Empty),
                        this.DbStreamFieldValueDescriptions[0].SeverityLevelIfMatch, isAliveSnmp,
                        this.DbStreamFieldValueDescriptions[0].ShouldSendNotificationByEmailIfMatch);
                }
                else
                {
                    switch (this.ChooseEvaluationMethod())
                    {
                        case EvaluationNeeded.Unknown:
                            this.ParseStringValue(isAliveSnmp);
                            break;
                        case EvaluationNeeded.StringComparison:
                            this.ParseStringValue(isAliveSnmp);
                            break;
                        case EvaluationNeeded.IntComparison:
                            this.ParseLongValue(isAliveSnmp);
                            break;
                        case EvaluationNeeded.RangeComparison:
                            this.ParseRangeValue(isAliveSnmp);
                            break;
                        case EvaluationNeeded.RegexMatch:
                            this.ParseRegexMatchValue(isAliveSnmp);
                            break;
                    }
                }
            }
        }

        #region Metodi privati per l'elaborazione dei dati relativi ai vari Fields degli Stream, con decodifica di descrizione e severità

        /// <summary>
        ///     Elabora i dati di tipo stringa relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isAliveSnmp">Indica se la periferica è raggiungibile via query SNMP</param>
        private void ParseStringValue(bool isAliveSnmp)
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string valueString = SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]);

                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown(isAliveSnmp);
                    }
                    else
                    {
                        DBStreamFieldValueDescription defaultDescription = null;
                        bool fieldValueSet = false;

                        foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                        {
                            if ((description.ExactStringValueToCompare != null) &&
                                (description.ExactStringValueToCompare.Length == SNMP_BITS_MASK_PATTERN_LENGTH) &&
                                (description.ExactStringValueToCompare.StartsWith(SNMP_BITS_MASK_STRING_PREFIX)) &&
                                (description.ExactStringValueToCompare.EndsWith(SNMP_BITS_MASK_STRING_SUFFIX)))
                            {
                                #region Gestione maschera di bit su valori stringa OctetString

                                ulong value;
                                if (SnmpUtility.TryParseULongString(valueString, out value))
                                {
                                    ulong mask;
                                    // Codifica per la gestione del tipo dati SNMP BITS, di tipo OctetString, che contiene n byte da interpretare come maschere di bit
                                    // Il formato previsto è BITS(0x0004000000000000) dove sono possibili maschere fino a 64 bit (long), espresse in formato esadecimale
                                    if (
                                        SnmpUtility.TryParseULongHexStringLTR(
                                            description.ExactStringValueToCompare.Substring(SNMP_BITS_MASK_PREFIX_LENGTH, SNMP_BITS_MASK_LENGTH),
                                            out mask))
                                    {
                                        if ((value & mask) == mask)
                                        {
                                            string bitValueString = SnmpUtility.GetBitRepresentationOfSnmpBitsOctetString(value);

                                            // SOLO A FINE DI VISUALIZZAZIONE SU INTERFACCIA GRIS
                                            // Estrae fino a un massimo di 4 maschere significative di bit da salvare nel campo 'Value'
                                            // della tabella 'stream_fields' del database
                                            string bitTruncatedValueString = RightTruncateBitValueString(bitValueString, 4);

                                            // La valutazione di tutti i casi delle maschere, concatenerà la serie di descrizioni
                                            this.SetFieldDataString(description.DescriptionIfMatch(bitValueString), bitTruncatedValueString,
                                                description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch, true);

                                            fieldValueSet = true;
                                        }
                                    }
                                }

                                #endregion
                            }
                            else if ((description.ExactStringValueToCompare != null) &&
                                     (description.ExactStringValueToCompare.Length == BIT_MASK_PATTERN_LENGTH) &&
                                     (description.ExactStringValueToCompare.StartsWith(BIT_MASK_STRING_PREFIX)) &&
                                     (description.ExactStringValueToCompare.EndsWith(BIT_MASK_STRING_SUFFIX)))
                            {
                                #region Gestione maschera di bit classica su valori ulong

                                ulong value;
                                if (ulong.TryParse(valueString, out value))
                                {
                                    ulong mask;
                                    // Codifica per la gestione del tipo dati MASK, di tipo ulong, che contiene n byte da interpretare come maschere di bit
                                    // Il formato previsto è MASK(0x0000000000000020) dove sono possibili maschere fino a 64 bit (long), espresse in formato esadecimale
                                    if (
                                        SnmpUtility.TryParseULongHexStringRTL(
                                            description.ExactStringValueToCompare.Substring(BIT_MASK_PREFIX_LENGTH, BIT_MASK_LENGTH), out mask))
                                    {
                                        if ((value & mask) == mask)
                                        {
                                            string bitValueString = SnmpUtility.GetBitRepresentationULong(value);
                                            // La valutazione di tutti i casi delle maschere, concatenerà la serie di descrizioni
                                            this.SetFieldDataString(description.DescriptionIfMatch(bitValueString), bitValueString,
                                                description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch, true);
                                            fieldValueSet = true;
                                        }
                                    }
                                }

                                #endregion
                            }
                            else if ((description.ExactStringValueToCompare != null) && (description.ExactStringValueToCompare.Length >= 5) &&
                                     (description.ExactStringValueToCompare.StartsWith(SNMP_RAW_OCTETSTRING_BYTES_PREFIX)) &&
                                     (description.ExactStringValueToCompare.EndsWith(SNMP_RAW_OCTETSTRING_BYTES_SUFFIX)))
                            {
                                #region Gestione tipo dati array di byte per definizione stringa

                                ulong value;
                                if (SnmpUtility.TryParseULongString(valueString, out value))
                                {
                                    ulong encodedValue;
                                    // Codifica per la gestione del tipo dati SNMP di tipo OctetString, che contiene n byte
                                    // Il formato previsto è (0x0004000000000000) dove sono codificate stringhe, espresse in formato esadecimale, da convertire in interi a 64 bit
                                    if (
                                        SnmpUtility.TryParseULongHexStringLTR(
                                            description.ExactStringValueToCompare.Substring(SNMP_RAW_OCTETSTRING_BYTES_PREFIX_LENGTH,
                                                description.ExactStringValueToCompare.Length - 2), out encodedValue))
                                    {
                                        if (value == encodedValue)
                                        {
                                            string bitValueString = SnmpUtility.GetBitRepresentationOfSnmpBitsOctetString(value);
                                            this.SetFieldDataString(description.DescriptionIfMatch(bitValueString), bitValueString,
                                                description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch, true);
                                            fieldValueSet = true;
                                            break;
                                        }
                                    }
                                }

                                #endregion
                            }
                            else if (String.Equals(valueString, description.ExactStringValueToCompare, StringComparison.OrdinalIgnoreCase))
                            {
                                this.SetFieldDataString(description.DescriptionIfMatch(valueString), valueString,
                                    description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch);
                                fieldValueSet = true;
                                break;
                            }

                            if (description.IsDefault)
                            {
                                // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                defaultDescription = description;
                            }
                        }

                        if (!fieldValueSet)
                        {
                            if (defaultDescription != null)
                            {
                                this.SetFieldDataString(defaultDescription.DescriptionIfMatch(valueString), valueString,
                                    defaultDescription.SeverityLevelIfMatch, defaultDescription.ShouldSendNotificationByEmailIfMatch);
                            }
                            else
                            {
                                this.SetFieldStateToUnknown(isAliveSnmp);
                            }
                        }
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown(isAliveSnmp);
            }
        }

        /// <summary>
        ///     Elabora i dati di tipo espressione regolare relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isAliveSnmp">Indica se la periferica è raggiungibile via query SNMP</param>
        private void ParseRegexMatchValue(bool isAliveSnmp)
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string valueString = SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]);

                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown(isAliveSnmp);
                    }
                    else
                    {
                        DBStreamFieldValueDescription defaultDescription = null;
                        bool fieldValueSet = false;

                        foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                        {
                            if ((description.RegexValueStringToMatch != null) && (description.RegexValueStringToMatch.IsMatch(valueString)))
                            {
                                this.SetFieldDataString(description.DescriptionIfMatch(valueString), valueString, description.SeverityLevelIfMatch,
                                    description.ShouldSendNotificationByEmailIfMatch);
                                fieldValueSet = true;
                                break;
                            }

                            if (description.IsDefault)
                            {
                                // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                defaultDescription = description;
                            }
                        }

                        if (!fieldValueSet)
                        {
                            if (defaultDescription != null)
                            {
                                this.SetFieldDataString(defaultDescription.DescriptionIfMatch(valueString), valueString,
                                    defaultDescription.SeverityLevelIfMatch, defaultDescription.ShouldSendNotificationByEmailIfMatch);
                            }
                            else
                            {
                                this.SetFieldStateToUnknown(isAliveSnmp);
                            }
                        }
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown(isAliveSnmp);
            }
        }

        /// <summary>
        ///     Elabora i dati di tipo intero relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isAliveSnmp">Indica se la periferica è raggiungibile via query SNMP</param>
        private void ParseLongValue(bool isAliveSnmp)
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string valueString = SnmpUtility.GetRawStringData(this.RawData[this.ArrayId]);

                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown(isAliveSnmp);
                    }
                    else
                    {
                        bool isTimeTicksData = false;

                        if (this.RawData[this.ArrayId].Data.TypeCode == SnmpType.TimeTicks)
                        {
                            isTimeTicksData = true;
                            valueString = ((TimeTicks) this.RawData[this.ArrayId].Data).ToUInt32().ToString(CultureInfo.InvariantCulture);
                        }

                        long value;
                        if (long.TryParse(valueString, out value))
                        {
                            DBStreamFieldValueDescription defaultDescription = null;
                            bool fieldValueSet = false;

                            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                            {
                                if (value == description.ExactIntValueToCompare)
                                {
                                    if (isTimeTicksData)
                                    {
                                        this.SetFieldDataString(description.DescriptionIfMatch(value.ToString()),
                                            SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]), description.SeverityLevelIfMatch,
                                            description.ShouldSendNotificationByEmailIfMatch);
                                    }
                                    else
                                    {
                                        this.SetFieldDataLong(description.DescriptionIfMatch(value.ToString()), value,
                                            description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch);
                                    }
                                    fieldValueSet = true;
                                    break;
                                }

                                if (description.IsDefault)
                                {
                                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                    defaultDescription = description;
                                }
                            }

                            if (!fieldValueSet)
                            {
                                if (defaultDescription != null)
                                {
                                    if (isTimeTicksData)
                                    {
                                        this.SetFieldDataString(defaultDescription.DescriptionIfMatch(value.ToString()),
                                            SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]), defaultDescription.SeverityLevelIfMatch,
                                            defaultDescription.ShouldSendNotificationByEmailIfMatch);
                                    }
                                    else
                                    {
                                        this.SetFieldDataLong(defaultDescription.DescriptionIfMatch(value.ToString()), value,
                                            defaultDescription.SeverityLevelIfMatch, defaultDescription.ShouldSendNotificationByEmailIfMatch);
                                    }
                                }
                                else
                                {
                                    this.SetFieldStateToUnknown(isAliveSnmp);
                                }
                            }
                        }
                        else
                        {
                            this.SetFieldStateToUnknown(isAliveSnmp);
                        }
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown(isAliveSnmp);
            }
        }

        /// <summary>
        ///     Elabora i dati di tipo intervallo relativi ai vari Fields degli Streams
        /// </summary>
        /// <param name="isAliveSnmp">Indica se la periferica è raggiungibile via query SNMP</param>
        private void ParseRangeValue(bool isAliveSnmp)
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string valueString = SnmpUtility.GetRawStringData(this.RawData[this.ArrayId]);

                    if (valueString == null)
                    {
                        this.SetFieldStateToUnknown(isAliveSnmp);
                    }
                    else
                    {
                        bool isTimeTicksData = false;

                        if (this.RawData[this.ArrayId].Data.TypeCode == SnmpType.TimeTicks)
                        {
                            isTimeTicksData = true;
                            valueString = ((TimeTicks) this.RawData[this.ArrayId].Data).ToUInt32().ToString(CultureInfo.InvariantCulture);
                        }

                        long value;
                        if (long.TryParse(valueString, out value))
                        {
                            DBStreamFieldValueDescription defaultDescription = null;
                            bool fieldValueSet = false;

                            foreach (DBStreamFieldValueDescription description in this.DbStreamFieldValueDescriptions)
                            {
                                if ((description.RangeValueToCompare.MinValue.HasValue) && (description.RangeValueToCompare.MaxValue.HasValue))
                                {
                                    if ((value >= description.RangeValueToCompare.MinValue.Value) &&
                                        (value <= description.RangeValueToCompare.MaxValue.Value))
                                    {
                                        if (isTimeTicksData)
                                        {
                                            this.SetFieldDataString(description.DescriptionIfMatch(value.ToString()),
                                                SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]), description.SeverityLevelIfMatch,
                                                description.ShouldSendNotificationByEmailIfMatch);
                                        }
                                        else
                                        {
                                            this.SetFieldDataLong(description.DescriptionIfMatch(value.ToString()), value,
                                                description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch);
                                        }
                                        fieldValueSet = true;
                                        break;
                                    }
                                }
                                else if ((description.RangeValueToCompare.MinValue.HasValue) && (!description.RangeValueToCompare.MaxValue.HasValue))
                                {
                                    if (value >= description.RangeValueToCompare.MinValue.Value)
                                    {
                                        if (isTimeTicksData)
                                        {
                                            this.SetFieldDataString(description.DescriptionIfMatch(value.ToString()),
                                                SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]), description.SeverityLevelIfMatch,
                                                description.ShouldSendNotificationByEmailIfMatch);
                                        }
                                        else
                                        {
                                            this.SetFieldDataLong(description.DescriptionIfMatch(value.ToString()), value,
                                                description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch);
                                        }
                                        fieldValueSet = true;
                                        break;
                                    }
                                }
                                else if ((!description.RangeValueToCompare.MinValue.HasValue) && (description.RangeValueToCompare.MaxValue.HasValue))
                                {
                                    if (value <= description.RangeValueToCompare.MaxValue.Value)
                                    {
                                        if (isTimeTicksData)
                                        {
                                            this.SetFieldDataString(description.DescriptionIfMatch(value.ToString()),
                                                SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]), description.SeverityLevelIfMatch,
                                                description.ShouldSendNotificationByEmailIfMatch);
                                        }
                                        else
                                        {
                                            this.SetFieldDataLong(description.DescriptionIfMatch(value.ToString()), value,
                                                description.SeverityLevelIfMatch, description.ShouldSendNotificationByEmailIfMatch);
                                        }
                                        fieldValueSet = true;
                                        break;
                                    }
                                }

                                if (description.IsDefault)
                                {
                                    // In questo modo, se ci fossero più valori di default definiti, sarebbe considerato l'ultimo
                                    defaultDescription = description;
                                }
                            }

                            if (!fieldValueSet)
                            {
                                if (defaultDescription != null)
                                {
                                    if (isTimeTicksData)
                                    {
                                        this.SetFieldDataString(defaultDescription.DescriptionIfMatch(value.ToString()),
                                            SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId]), defaultDescription.SeverityLevelIfMatch,
                                            defaultDescription.ShouldSendNotificationByEmailIfMatch);
                                    }
                                    else
                                    {
                                        this.SetFieldDataLong(defaultDescription.DescriptionIfMatch(value.ToString()), value,
                                            defaultDescription.SeverityLevelIfMatch, defaultDescription.ShouldSendNotificationByEmailIfMatch);
                                    }
                                }
                                else
                                {
                                    this.SetFieldStateToUnknown(isAliveSnmp);
                                }
                            }
                        }
                        else
                        {
                            this.SetFieldStateToUnknown(isAliveSnmp);
                        }
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown(isAliveSnmp);
            }
        }

        /// <summary>
        ///     Elabora un dato unico relativo al Field degli Streams
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore passato</param>
        /// <param name="severity">Severità da attribuire allo stream field</param>
        /// <param name="isAliveSnmp">Indica se la periferica è raggiungibile via query SNMP</param>
        /// <param name="shouldSendNotificationByEmailIfMatch">
        ///     Indica se inviare mail di notifica, perché è subentrata questa
        ///     condizione
        /// </param>
        private void ParseDefaultSingleValue(string valueDescription, Severity severity, bool isAliveSnmp, bool shouldSendNotificationByEmailIfMatch)
        {
            if ((this.RawData != null) && (this.RawData.Count > 0))
            {
                if (this.ArrayId >= this.RawData.Count)
                {
                    this.SetFieldStateToInvalidArrayIndex();
                }
                else
                {
                    string value = SnmpUtility.DecodeDataByType(this.RawData[this.ArrayId], this.IsTimeTicksMs);

                    if ((value != null) && (value.Trim().Length > 0))
                    {
                        if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
                        {
                            if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.Long)
                            {
                                string valueString = SnmpUtility.GetRawStringData(this.RawData[this.ArrayId]);

                                if (valueString == null)
                                {
                                    this.SetFieldStateToUnknown(isAliveSnmp);
                                }
                                else
                                {
                                    long valueLong;
                                    if (long.TryParse(valueString, out valueLong))
                                    {
                                        this.SetFieldDataInternal(valueDescription,
                                            (valueLong*this.ValueFormatExpression.Factor + this.ValueFormatExpression.Addend).ToString(
                                                this.ValueFormatExpression.Format), severity, shouldSendNotificationByEmailIfMatch);
                                    }
                                    else
                                    {
                                        this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity,
                                            shouldSendNotificationByEmailIfMatch);
                                    }
                                }
                            }
                            else if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.DateAndTime)
                            {
                                DateTime? valueData = SnmpUtility.GetDateAndTimeFromData(this.RawData[this.ArrayId]);

                                if (!valueData.HasValue)
                                {
                                    this.SetFieldStateToUnknown(isAliveSnmp);
                                }
                                else
                                {
                                    this.SetFieldDataInternal(valueDescription, valueData.Value.ToString(this.ValueFormatExpression.Format), severity,
                                        shouldSendNotificationByEmailIfMatch);
                                }
                            }
                            else if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                            {
                                this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity,
                                    shouldSendNotificationByEmailIfMatch);
                            }
                        }
                        else
                        {
                            this.SetFieldDataString(valueDescription, value, severity, shouldSendNotificationByEmailIfMatch);
                        }
                    }
                    else
                    {
                        this.SetFieldStateToUnknown(isAliveSnmp);
                    }
                }
            }
            else
            {
                this.SetFieldStateToUnknown(isAliveSnmp);
            }
        }

        /// <summary>
        ///     Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore da impostare</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="severity">Severità da impostare</param>
        /// <param name="shouldSendNotificationByEmailIfMatch">
        ///     Indica se inviare mail di notifica, perché è subentrata questa
        ///     condizione
        /// </param>
        private void SetFieldDataLong(string valueDescription, long value, Severity severity, bool shouldSendNotificationByEmailIfMatch)
        {
            if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
            {
                if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.Long)
                {
                    this.SetFieldDataInternal(valueDescription,
                        (value*this.ValueFormatExpression.Factor + this.ValueFormatExpression.Addend).ToString(this.ValueFormatExpression.Format),
                        severity, shouldSendNotificationByEmailIfMatch);
                }
                else if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                {
                    this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity,
                        shouldSendNotificationByEmailIfMatch);
                }
            }
            else
            {
                this.SetFieldDataInternal(valueDescription, value.ToString(), severity, shouldSendNotificationByEmailIfMatch);
            }
        }

        /// <summary>
        ///     Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore da impostare</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="severity">Severità da impostare</param>
        /// <param name="shouldSendNotificationByEmailIfMatch">
        ///     Indica se inviare mail di notifica, perché è subentrata questa
        ///     condizione
        /// </param>
        /// <param name="appendDescription">Se true, il valore per la descrizione è accodato a quello già esistente</param>
        private void SetFieldDataString(string valueDescription,
            string value,
            Severity severity,
            bool shouldSendNotificationByEmailIfMatch,
            bool appendDescription = false)
        {
            if ((this.ValueFormatExpression != null) && (this.ValueFormatExpression.IsValid))
            {
                if (this.ValueFormatExpression.FormatDataType == FormatExpressionDataType.String)
                {
                    this.SetFieldDataInternal(valueDescription, value + this.ValueFormatExpression.Format, severity,
                        shouldSendNotificationByEmailIfMatch, appendDescription);
                }
            }
            else
            {
                this.SetFieldDataInternal(valueDescription, value, severity, shouldSendNotificationByEmailIfMatch, appendDescription);
            }
        }

        /// <summary>
        /// Da una stringa costituita dalla concatenazione di n maschere di bit estrae un sottoinsime
        /// delle stesse a partire da quella più a destra
        /// '00000000 00000000 00000000 00000000 10000000 01000010 00000000 00000000' -> '10000000 01000010 00000000 00000000'
        /// </summary>
        /// <param name="bitValueString">stringa originale</param>
        /// <param name="NumMaskBit">numero di mascehre di 8 bit da considerare</param>
        /// <returns>stringa elaborata</returns>
        private string RightTruncateBitValueString(string bitValueString, int NumMaskBit)
        {
            string   sRet   = bitValueString;

            try
            {
                string[] masks = bitValueString.Split(' ');

                if (masks.Length > NumMaskBit)
                {
                    string sTmp = "";
                    bool gotBitSet = false;

                    for (int j = 0; j < NumMaskBit; j++)
                    {
                        string mask = masks[j + (masks.Length - NumMaskBit)];

                        if ((gotBitSet) || (mask != "00000000"))
                        {
                            sTmp += string.Format("{0} ", mask);
                            gotBitSet = true;
                        }
                    }

                    sRet = sTmp;
                }
            }
            catch { } // In caso di eccezione ritorna la stringa passata in ingresso

            return sRet;
        }

        /// <summary>
        ///     Imposta il valore, la descrizione e la severità del Field
        /// </summary>
        /// <param name="valueDescription">Descrizione del valore da impostare</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="severity">Severità da impostare</param>
        /// <param name="shouldSendNotificationByEmailIfMatch">
        ///     Indica se inviare mail di notifica, perché è subentrata questa
        ///     condizione
        /// </param>
        /// <param name="appendDescription">Se true, il valore per la descrizione è accodato a quello già esistente</param>
        private void SetFieldDataInternal(string valueDescription,
            string value,
            Severity severity,
            bool shouldSendNotificationByEmailIfMatch,
            bool appendDescription = false)
        {
            if (appendDescription)
            {
                if (String.IsNullOrEmpty(this.ValueDescription))
                {
                    this.ValueDescription = valueDescription;
                }
                else
                {
                    this.ValueDescription += String.Format(", {0}", valueDescription);
                }
            }
            else
            {
                this.ValueDescription = valueDescription;
            }

            this.SeverityLevel = severity;
            this.Value = value;
            this.ShouldSendNotificationByEmail = shouldSendNotificationByEmailIfMatch;

            // eseguo la riduzione di valori verso i livelli superiori della gerarchia
            // se non sono un aggregato e ho un padre
            if (this.StreamFieldFather != null && !this.IsAggregation) {
                // aggregazione verso il padre
                if ((this.SeverityLevel != Severity.Unknown) && (this.PersistOnDatabase) && (!this.ExcludedFromSeverityComputation) &&
                            ((this.StreamFieldFather.SeverityLevel == Severity.Unknown) || ((int)this.SeverityLevel > (int)this.StreamFieldFather.SeverityLevel)))
                {
                    this.StreamFieldFather.SeverityLevel = this.SeverityLevel;

                    // verifico se ho un nonno e in tal caso riduco il padre verso esso
                    if (this.StreamFieldFather.StreamFieldFather != null && (this.StreamFieldFather.SeverityLevel != Severity.Unknown) && (this.StreamFieldFather.PersistOnDatabase) && (!this.StreamFieldFather.ExcludedFromSeverityComputation) &&
                            ((this.StreamFieldFather.StreamFieldFather.SeverityLevel == Severity.Unknown) || ((int)this.StreamFieldFather.SeverityLevel > (int)this.StreamFieldFather.StreamFieldFather.SeverityLevel)))
                    {
                        this.StreamFieldFather.StreamFieldFather.SeverityLevel = this.StreamFieldFather.SeverityLevel;
                    }
                }
            }
            else if (this.IsAggregation && this.StreamFieldFather != null && this.StreamFieldFather.StreamFieldFather != null)
            {
                if ((this.SeverityLevel != Severity.Unknown) && (this.PersistOnDatabase) && (!this.ExcludedFromSeverityComputation) &&
                            ((this.StreamFieldFather.StreamFieldFather.SeverityLevel == Severity.Unknown) || ((int)this.SeverityLevel > (int)this.StreamFieldFather.StreamFieldFather.SeverityLevel)))
                {
                    this.StreamFieldFather.StreamFieldFather.SeverityLevel = this.SeverityLevel;
                }
            }
        }

        /// <summary>
        ///     Imposta il valore del Field nel caso il valore sia nullo, usando i dati di forzatura se presenti
        /// </summary>
        /// <param name="isAliveSnmp">Indica se la periferica è raggiungibile via query SNMP</param>
        private void SetFieldStateToUnknown(bool isAliveSnmp)
        {
            if (this.LastError != null)
            {
                this.SetFieldDataInternal(Settings.Default.ErrorOnDataRetrievalMessage, Settings.Default.ErrorOnDataRetrievalValue, Severity.Warning,
                    false);
            }
            else if (this.NoSnmpDataAvailableForcedState != null)
            {
                if (isAliveSnmp)
                {
                    // La forzatura di stato influisce solo se la periferica risponde su SNMP
                    this.SetFieldDataInternal(this.NoSnmpDataAvailableForcedState.ForcedValueDescription,
                        this.NoSnmpDataAvailableForcedState.ForcedValue, this.NoSnmpDataAvailableForcedState.ForcedSeverity,
                        this.NoSnmpDataAvailableForcedState.ShouldSendNotificationByEmailIfMatch);
                }
                else
                {
                    this.SetFieldDataInternal(Settings.Default.NoDataAvailableMessage, Settings.Default.NoDataAvailableValue, Severity.Unknown, false);
                }
            }
            else
            {
                this.SetFieldDataInternal(Settings.Default.NoDataAvailableMessage, Settings.Default.NoDataAvailableValue, Severity.Unknown, false);
            }
        }

        /// <summary>
        ///     Imposta il valore del Field nel caso si sia richiesto un valore con indice non disponibile
        /// </summary>
        private void SetFieldStateToInvalidArrayIndex()
        {
            this.SetFieldDataInternal(
                string.Format(CultureInfo.InvariantCulture, "L'indice dell'array {0} non è valido per la dimensione dei dati disponibili {1}",
                    this.ArrayId, this.RawData.Count), Settings.Default.NoDataAvailableValue, Severity.Unknown, false);
        }

        #endregion

        /// <summary>
        ///     Indica se è padre di StreamField figli
        /// </summary>
        public bool isFather()
        {
            return this.DbStreamFieldValueChildren != null;
        }

        /// <summary>
        ///     Indica se è padre di StreamField figli
        /// </summary>
        public void setIsFather(bool isFather)
        {
            if (isFather && this.DbStreamFieldValueChildren == null)
            {
                this.DbStreamFieldValueChildren = new Collection<DBStreamField>();
            }
            else if (!isFather)
            {
                this.DbStreamFieldValueChildren = null;
            }
        }

        /// <summary>
        ///     Indica se è un figlio
        /// </summary>
        public bool isChild()
        {
            return this.StreamFieldFather != null;
        }

        /// <summary>
        ///     Genera un clone dell'oggetto corrente( clona anche i figli e i relativi streamFieldDescription )
        /// </summary>
        public object Clone(DBStream stream, bool cloneAll = false)
        {
            DBStreamField cloneObj = new DBStreamField(
                this.FieldId,
                this.ArrayId,
                this.Name,
                this.FieldOid,
                this.FieldTableOid,
                this.FieldCorrelationTableOid,
                this.TableRowIndex ,
                this.TableEvaluationFormula,
                this.TableCorrelationEvaluationFormula,
                this.TableFilterColumnIndex,
                this.TableCorrelationFilterColumnIndex,
                this.TableFilterColumnRegex,
                this.ValueFormatExpression,
                this.TableIndex,
                this.ScalarTableCounterOid,
                this.ExcludedFromSeverityComputation,
                this.NoSnmpDataAvailableForcedState,
                this.HideIfNotAvailableRowAndColumnIndex,
                this.FixedValueString,
                this.HideIfNoDataAvailable
            );

            cloneObj.StreamFieldFather = this.StreamFieldFather;
            cloneObj.IsArray = this.IsArray;
            cloneObj.SeverityLevel = Severity.Unknown;
            cloneObj.ValueDescription = this.ValueDescription;
            cloneObj.Value = this.Value;
            cloneObj.PersistOnDatabase = this.PersistOnDatabase;
            cloneObj.PositionArray = this.PositionArray;
            cloneObj.MaxSizeArray = this.MaxSizeArray;
            cloneObj.LastTableCorrelationRow = this.LastTableCorrelationRow;
            cloneObj.IsShowName = this.IsShowName;
            cloneObj.IsShowValueDescription = this.IsShowValueDescription;
            cloneObj.IsStreamFieldArray = this.IsStreamFieldArray;
            cloneObj.IsAggregation = this.IsAggregation;
            cloneObj.IsTimeTicksMs = this.IsTimeTicksMs;
            cloneObj.UseToHeadOfStreamFieldArray = this.UseToHeadOfStreamFieldArray;
            cloneObj.StreamFieldArrayid = this.StreamFieldArrayid + 1;
            cloneObj.ExpressionFormatShowRowId = this.ExpressionFormatShowRowId;
            cloneObj.RowIdZeroBased = this.RowIdZeroBased;
            cloneObj.EvaluationFormula = this.EvaluationFormula;
            cloneObj.MemoryBase = this.MemoryBase;

            if(cloneAll)
                cloneObj.isFirstChildOfStreamFieldArray = true;

            if (this.isFather() && (!this.IsStreamFieldArray || this.UseToHeadOfStreamFieldArray))
            {
                cloneObj.setIsFather( true );
                if (!cloneAll)
                {
                    if (!cloneObj.UseToHeadOfStreamFieldArray)
                        cloneObj.PositionArray++;
                    else
                        cloneObj.FieldId++;
                    cloneObj.TableRowIndex++;
                }
                stream.FieldData.Add(cloneObj);

                if (this.IsStreamFieldArray == true) {
                    cloneObj.Offset = this.Offset + 1;
                }

                foreach (DBStreamField child in this.DbStreamFieldValueChildren)
                {
                    // se l'elemento è d'aggregazione, allora non deve essere clonato 
                    if (child.IsAggregation && !cloneAll)
                    {
                        continue;
                    }
                    DBStreamField cloneChild = (DBStreamField)child.Clone(stream);
                    cloneObj.DbStreamFieldValueChildren.Add(cloneChild);
                    cloneChild.StreamFieldFather = cloneObj;
                    if (cloneAll)
                    {
                        cloneChild.isFirstChildOfStreamFieldArray = true;
                        cloneChild.LastTableCorrelationRow = 0;
                    }
                    if (!cloneAll)
                    {
                        cloneChild.PositionArray++;
                        cloneChild.TableRowIndex++;
                        if (cloneChild.FieldCorrelationTableOid != null)
                            cloneChild.LastTableCorrelationRow = cloneObj.LastTableCorrelationRow;
                    }
                    stream.FieldData.Add(cloneChild);
                }
            }
            else if (this.IsStreamFieldArray) {

                cloneObj.Offset = this.Offset + 1;

                cloneObj.setIsFather(true);
                stream.FieldData.Add(cloneObj);
                cloneObj.TableRowIndex++;
                cloneObj.FieldId++;
                // clono solo il primo elemento figlio che in questo caso è anche padre
                if (this.DbStreamFieldValueChildren != null && this.DbStreamFieldValueChildren.Count > 0) {
                    DBStreamField firstChild = (DBStreamField)this.DbStreamFieldValueChildren[0].Clone(stream, true);
                    firstChild.StreamFieldFather = cloneObj;
                    cloneObj.DbStreamFieldValueChildren.Add(firstChild);
                    firstChild.FieldId++;
                }
            }


            foreach (DBStreamFieldValueDescription valueDesc in this.DbStreamFieldValueDescriptions)
            {
                DBStreamFieldValueDescription cloneValueDesc = (DBStreamFieldValueDescription)valueDesc.Clone();
                cloneValueDesc.Field = cloneObj;
                cloneObj.DbStreamFieldValueDescriptions.Add(cloneValueDesc);
            }

            return cloneObj;
        }
    }
}
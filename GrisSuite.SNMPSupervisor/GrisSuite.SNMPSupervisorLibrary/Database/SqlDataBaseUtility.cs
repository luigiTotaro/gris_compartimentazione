﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Threading;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
	/// <summary>
	///     Rappresenta la classe di utility per l'accesso al database
	/// </summary>
	public static class SqlDatabaseUtility
	{
		private const string DEFINITION_VERSION = "1.00";
		private const string PROTOCOL_DEFINITION_VERSION = "1.00";

		#region Metodi pubblici

        #region DeleteStreamField - Elimina tutti gli streamfield
        /// <summary>
        ///     Elimina tutti gli streamfield legati ad uno stream ed ad un dato device con un dato id
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id della periferica</param>
        /// <param name="deviceId">Id dello stream</param>
        /// /// <param name="deviceId">Id dello streamField</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool DeleteStreamField(string dbConnectionString, long deviceId, int streamId, int streamFieldId = -1)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            bool deleteOperationSuccessful = false;

            try
            {
                try
                    {
                        dbConnection = new SqlConnection(dbConnectionString);
                    }
                    catch (ArgumentException ex)
                    {
                        FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                            "La stringa di connessione al database non è valida. " + ex.Message);
                        return false;
                    }

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                    cmd.CommandText = "DELETE FROM [device_ack] WHERE (DevID = @DevID AND StrID = @StrID";
                    if (streamFieldId >= 0)
                    {
                        cmd.CommandText += " AND FieldID = @FieldID";
                    }
                    cmd.CommandText += ");";

                    cmd.CommandText += "DELETE FROM [stream_fields] WHERE (DevID = @DevID AND StrID = @StrID";
                    if (streamFieldId >= 0)
                    {
                        cmd.CommandText += " AND FieldID = @FieldID";
                    }
                    cmd.CommandText += ");";

                    cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                        DataRowVersion.Current, deviceId));

                    cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                        DataRowVersion.Current, streamId));

                    if (streamFieldId >= 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@FieldID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                            DataRowVersion.Current, streamFieldId));
                    }

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();

                    deleteOperationSuccessful = true;
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                    "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deleteOperationSuccessful;
        }

        #endregion

		#region UpdateDeviceStatus - Aggiorna lo stato della periferica

		/// <summary>
		///     Aggiorna lo stato della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="severityLevel">Livello di severità del Device</param>
		/// <param name="descriptionStatus">Stato del Device</param>
		/// <param name="offline">Indica se il Device è fuori linea o non raggiungibile</param>
		/// <param name="shouldSendNotificationByEmail">Indica se inviare mail di notifica</param>
		public static void UpdateDeviceStatus(string dbConnectionString,
			long deviceId,
			int severityLevel,
			string descriptionStatus,
			Byte offline,
			Byte shouldSendNotificationByEmail)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"IF EXISTS (SELECT [DevID] FROM [device_status] WHERE [DevID] = @DevID) BEGIN UPDATE [device_status] SET [SevLevel] = @SevLevel, [Description] = @Description, [Offline] = @Offline, [ShouldSendNotificationByEmail] = @ShouldSendNotificationByEmail WHERE [DevID] = @DevID; END ELSE BEGIN INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline], [ShouldSendNotificationByEmail]) VALUES (@DevID, @SevLevel, @Description, @Offline, @ShouldSendNotificationByEmail); END";
				cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, deviceId));
				cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, severityLevel));
				cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, descriptionStatus));
				cmd.Parameters.Add(new SqlParameter("@Offline", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, offline));
				cmd.Parameters.Add(new SqlParameter("@ShouldSendNotificationByEmail", SqlDbType.TinyInt, 1, ParameterDirection.Input,
					false, 0, 3, null, DataRowVersion.Current, shouldSendNotificationByEmail));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				cmd.ExecuteNonQuery();
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			finally
			{
				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}
		}

		#endregion

		#region UpdateDeviceType - Aggiorna il tipo della periferica

		/// <summary>
		///     Aggiorna il tipo della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDeviceType">Nuovo tipo del Device</param>
		public static void UpdateDeviceType(string dbConnectionString, long deviceId, string newDeviceType)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText = Settings.Default.IsOnClientDatabase
					? "UPDATE devices SET DiscoveredType = @DiscoveredType WHERE (DevID = @DevID)"
					: "UPDATE devices SET Type = @DiscoveredType WHERE (DevID = @DevID)";
				cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, deviceId));
				cmd.Parameters.Add(new SqlParameter("@DiscoveredType", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, newDeviceType));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				cmd.ExecuteNonQuery();
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			finally
			{
				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}
		}

		#endregion

		#region UpdateDeviceSerialNumber - Aggiorna il numero di serie della periferica

		/// <summary>
		///     Aggiorna il numero di serie della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDeviceSerialNumber">Nuovo numero di serie del Device</param>
		public static void UpdateDeviceSerialNumber(string dbConnectionString, long deviceId, string newDeviceSerialNumber)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText = "UPDATE devices SET SN = @SN WHERE (DevID = @DevID)";
				cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, deviceId));
				cmd.Parameters.Add(new SqlParameter("@SN", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, newDeviceSerialNumber));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				cmd.ExecuteNonQuery();
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			finally
			{
				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}
		}

		#endregion

		#region UpdateDeviceDefinitionVersion - Aggiorna la versione della definizione della periferica

		/// <summary>
		///     Aggiorna la versione della definizione della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDefinitionVersion">Nuova versione della definizione</param>
		public static void UpdateDeviceDefinitionVersion(string dbConnectionString, long deviceId, string newDefinitionVersion)
		{
			if (!DEFINITION_VERSION.Equals(newDefinitionVersion))
			{
				// La versione della definizione è da aggiornare solamente se differente rispetto a quella di default,
				// inserita in fase di creazione delle periferiche
				SqlConnection dbConnection = null;
				SqlCommand cmd = new SqlCommand();

				try
				{
					try
					{
						dbConnection = new SqlConnection(dbConnectionString);
					}
					catch (ArgumentException ex)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
							"La stringa di connessione al database non è valida. " + ex.Message);
						return;
					}

					cmd.CommandType = CommandType.Text;
					cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
					cmd.CommandText = "UPDATE devices SET DefinitionVersion = @DefinitionVersion WHERE (DevID = @DevID)";
					cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
						DataRowVersion.Current, deviceId));
					cmd.Parameters.Add(new SqlParameter("@DefinitionVersion", SqlDbType.VarChar, 8, ParameterDirection.Input, false, 0,
						0, null, DataRowVersion.Current, newDefinitionVersion));

					cmd.Connection = dbConnection;

					dbConnection.Open();

					cmd.ExecuteNonQuery();
				}
				catch (SqlException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"Errore durante l'accesso alla base dati. " + ex.Message);
				}
				finally
				{
					if (dbConnection != null)
					{
						dbConnection.Close();
					}

					cmd.Dispose();
				}
			}
		}

		#endregion

		#region UpdateStream - Aggiorna lo Stream

		/// <summary>
		///     Aggiorna lo Stream
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="streamId">Id dello Stream</param>
		/// <param name="name">Nome dello Stream</param>
		/// <param name="severityLevel">Livello di severità dello Stream</param>
		/// <param name="description">Descrizione del valore dello Stream</param>
		public static void UpdateStream(string dbConnectionString,
			long deviceId,
			int streamId,
			string name,
			int severityLevel,
			string description)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"IF EXISTS (SELECT DevID FROM streams WHERE (DevID = @DevID) AND (StrID = @StrID)) BEGIN UPDATE [streams] SET [Name] = @Name, [Data] = NULL, [DateTime] = GetDate(), [SevLevel] = @SevLevel, [Description] = @Description WHERE (([DevID] = @DevID) AND ([StrID] = @StrID)); END ELSE BEGIN INSERT INTO [streams] ([DevID], [StrID], [Name], [Data], [DateTime], [SevLevel], [Description], [Visible]) VALUES (@DevID, @StrID, @Name, NULL, GetDate(), @SevLevel, @Description, 1) END";
				cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, deviceId));
				cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, streamId));
				cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, name));
				cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, severityLevel));
				cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.Text, 2147483647, ParameterDirection.Input, false, 0,
					0, null, DataRowVersion.Current, description));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				cmd.ExecuteNonQuery();
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			finally
			{
				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}
		}

		#endregion

		#region UpdateStreamField - Aggiorna il Field

        /// <summary>
        ///     Aggiorna il Field
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="streamId">Id dello Stream</param>
        /// <param name="fieldId">Id del Field</param>
        /// <param name="arrayId">Id del vettore dei valori del Field</param>
        /// <param name="name">Nome del Field</param>
        /// <param name="severityLevel">Livello di severità del Field</param>
        /// <param name="value">Valore del Field</param>
        /// <param name="description">Descrizione del valore del Field</param>
        /// <param name="shouldSendNotificationByEmail">Indica se inviare mail di notifica</param>
        public static void UpdateStreamField(string dbConnectionString,
            long deviceId,
            int streamId,
            int fieldId,
            int arrayId,
            string name,
            int severityLevel,
            string value,
            string description,
            Byte shouldSendNotificationByEmail, bool isVisible = true, bool isTable = false)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                        "La stringa di connessione al database non è valida. " + ex.Message);
                    return;
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF EXISTS (SELECT FieldID FROM stream_fields WHERE (DevID = @DevID) AND (StrID = @StrID) AND (FieldID = @FieldID) AND (ArrayID = @ArrayID)) BEGIN UPDATE [stream_fields] SET [Name] = @Name, [SevLevel] = @SevLevel, [Value] = @Value, [Description] = @Description, [ShouldSendNotificationByEmail] = @ShouldSendNotificationByEmail, [Visible] = @Visible, [IsTable] = @IsTable WHERE (([DevID] = @DevID) AND ([StrID] = @StrID) AND ([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID)); END ELSE BEGIN INSERT INTO [stream_fields] ([FieldID], [ArrayID], [StrID], [DevID], [Name], [SevLevel], [Value], [Description], [Visible], ReferenceID, ShouldSendNotificationByEmail, [IsTable]) VALUES (@FieldID, @ArrayID, @StrID, @DevID, @Name, @SevLevel, @Value, @Description, @Visible, NULL, @ShouldSendNotificationByEmail, @IsTable); END";
                cmd.Parameters.Add(new SqlParameter("@FieldID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, fieldId));
                cmd.Parameters.Add(new SqlParameter("@ArrayID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, arrayId));
                cmd.Parameters.Add(new SqlParameter("@StrID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, streamId));
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, name));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, severityLevel));
                cmd.Parameters.Add(new SqlParameter("@Value", SqlDbType.VarChar, 1024, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, value));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.Text, 2147483647, ParameterDirection.Input, false, 0,
                    0, null, DataRowVersion.Current, description));
                cmd.Parameters.Add(new SqlParameter("@Visible", SqlDbType.TinyInt, 1, ParameterDirection.Input,
                    false, 0, 3, null, DataRowVersion.Current, (isVisible == true ? 1 : 0)));
                cmd.Parameters.Add(new SqlParameter("@ShouldSendNotificationByEmail", SqlDbType.TinyInt, 1, ParameterDirection.Input,
                    false, 0, 3, null, DataRowVersion.Current, shouldSendNotificationByEmail));
                cmd.Parameters.Add(new SqlParameter("@IsTable", SqlDbType.TinyInt, 1, ParameterDirection.Input,
                    false, 0, 3, null, DataRowVersion.Current, (isTable == true ? 1 : 0)));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                    "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

		#endregion

		#region CheckDeviceExistence - Verifica l'esistenza di una periferica in base dati

		/// <summary>
		///     Verifica l'esistenza di una periferica in base dati
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <returns>
		///     Ritorna null nel caso ci sia un errore nell'accesso alla base dati o nell'esecuzione del comando, true se la
		///     periferica esiste, false altrimenti
		/// </returns>
		public static bool? CheckDeviceExistence(string dbConnectionString, long deviceId)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDevices = null;
			bool? deviceExists;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return null;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText = "SELECT DevID FROM [devices] WHERE (DevID = @DevID)";
				cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, deviceId));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDevices = cmd.ExecuteReader();

				if (drDevices.Read())
				{
					int colDevID = drDevices.GetOrdinal("DevID");

					do
					{
						if (drDevices.IsDBNull(colDevID))
						{
							deviceExists = false;
						}
						else
						{
							long deviceID = drDevices.GetSqlInt64(colDevID).Value;

							deviceExists = deviceID > 0;
						}
					} while (drDevices.Read());
				}
				else
				{
					deviceExists = false;
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
				deviceExists = null;
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando CheckDeviceExistence. " +
					ex.Message);
				deviceExists = null;
			}
			catch (ThreadAbortException)
			{
				deviceExists = null;
			}
			finally
			{
				if (drDevices != null && !drDevices.IsClosed)
				{
					drDevices.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return deviceExists;
		}

		#endregion

		#region GetStationBuildingRackPortData - Recupera i dati di lookup per poter creare una periferica su database

		/// <summary>
		///     Recupera i dati di lookup per poter creare una periferica su database
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="location">Dati della topografia della periferica</param>
		/// <param name="regionId">ID del Compartimento</param>
		/// <param name="regionName">Nome del Compartimento</param>
		/// <param name="zoneId">ID della Zona</param>
		/// <param name="zoneName">Nome della Zona</param>
		/// <param name="nodeId">ID del Nodo</param>
		/// <param name="nodeName">Nome del Nodo</param>
		/// <param name="serverId">ID del Server da System.xml</param>
		/// <param name="serverHost">Host che monitora la periferica</param>
		/// <param name="serverName">Nome del Server che monitora la periferica</param>
		/// <param name="port">Porta da System.xml</param>
		/// <param name="createTopography">Indica se creare la topografia, se non esistente su database</param>
		/// <returns>Dati di lookup con l'Id di Stazione, Fabbricato, Armadio e Interfaccia</returns>
		public static DeviceDatabaseSupport GetStationBuildingRackPortData(string dbConnectionString,
			TopographyLocation location,
			long regionId,
			string regionName,
			long zoneId,
			string zoneName,
			long nodeId,
			string nodeName,
			int serverId,
			string serverHost,
			string serverName,
			SystemPort port,
			bool createTopography)
		{
			DeviceDatabaseSupport deviceDatabaseSupport = null;

			if (location != null)
			{
				SqlConnection dbConnectionExists = null;
				SqlCommand cmdExists = new SqlCommand();
				SqlDataReader drDeviceDatabaseSupportExists = null;
				SqlConnection dbConnectionCreate = null;
				SqlCommand cmdCreate = new SqlCommand();
				SqlDataReader drDeviceDatabaseSupportCreate = null;

				try
				{
					try
					{
						dbConnectionExists = new SqlConnection(dbConnectionString);
						dbConnectionCreate = new SqlConnection(dbConnectionString);
					}
					catch (ArgumentException ex)
					{
						FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
							"La stringa di connessione al database non è valida. " + ex.Message);
						return null;
					}

					cmdExists.CommandType = CommandType.Text;
					cmdExists.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
					cmdExists.CommandText =
						"DECLARE @StationID uniqueidentifier; DECLARE @BuildingID uniqueidentifier; DECLARE @RackID uniqueidentifier; DECLARE @PortID uniqueidentifier; SELECT TOP (1) @StationID = station.StationID, @BuildingID = building.BuildingID, @RackID = rack.RackID FROM station INNER JOIN building ON station.StationID = building.StationID INNER JOIN rack ON building.BuildingID = rack.BuildingID WHERE (station.StationName = @StationName) AND (building.BuildingName = @BuildingName) AND (building.BuildingXMLID = @BuildingXMLID) AND (building.BuildingDescription = @BuildingDescription) AND (rack.RackName = @RackName) AND (rack.RackXMLID = @RackXMLID) AND (rack.RackType = @RackType) AND (rack.RackDescription = @RackDescription) AND (building.Removed = 0) AND (rack.Removed = 0) AND (station.Removed = 0); SELECT TOP (1) @PortID = port.PortID FROM port WHERE (SrvID = @SrvID) AND (PortXMLID = @PortXMLID) SELECT @StationID AS StationID, @BuildingID AS BuildingID, @RackID AS RackID, @PortID AS PortID";
					cmdExists.Parameters.Add(new SqlParameter("@StationName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0,
						0, null, DataRowVersion.Current, location.StationName));
					cmdExists.Parameters.Add(new SqlParameter("@BuildingName", SqlDbType.VarChar, 64, ParameterDirection.Input, false,
						0, 0, null, DataRowVersion.Current, location.BuildingName));
					cmdExists.Parameters.Add(new SqlParameter("@BuildingXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0,
						10, null, DataRowVersion.Current, location.BuildingId));
					cmdExists.Parameters.Add(new SqlParameter("@RackName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
						null, DataRowVersion.Current, location.LocationName));
					cmdExists.Parameters.Add(new SqlParameter("@RackXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10,
						null, DataRowVersion.Current, location.LocationId));
					cmdExists.Parameters.Add(new SqlParameter("@RackType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
						null, DataRowVersion.Current, location.LocationType));
					cmdExists.Parameters.Add(new SqlParameter("@RackDescription", SqlDbType.VarChar, 256, ParameterDirection.Input,
						false, 0, 0, null, DataRowVersion.Current, location.LocationNotes));
					cmdExists.Parameters.Add(new SqlParameter("@BuildingDescription", SqlDbType.VarChar, 256, ParameterDirection.Input,
						false, 0, 0, null, DataRowVersion.Current, location.BuildingNotes));
					cmdExists.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
						DataRowVersion.Current, serverId));
					cmdExists.Parameters.Add(new SqlParameter("@PortXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10,
						null, DataRowVersion.Current, port.Id));

					cmdExists.Connection = dbConnectionExists;

					dbConnectionExists.Open();

					drDeviceDatabaseSupportExists = cmdExists.ExecuteReader();

					if (drDeviceDatabaseSupportExists.Read())
					{
						int colStationId = drDeviceDatabaseSupportExists.GetOrdinal("StationID");
						int colBuildingId = drDeviceDatabaseSupportExists.GetOrdinal("BuildingID");
						int colRackId = drDeviceDatabaseSupportExists.GetOrdinal("RackID");
						int colPortId = drDeviceDatabaseSupportExists.GetOrdinal("PortID");

						do
						{
							if ((!drDeviceDatabaseSupportExists.IsDBNull(colStationId)) &&
							    (!drDeviceDatabaseSupportExists.IsDBNull(colBuildingId)) &&
							    (!drDeviceDatabaseSupportExists.IsDBNull(colRackId)) && (!drDeviceDatabaseSupportExists.IsDBNull(colPortId)))
							{
								deviceDatabaseSupport = new DeviceDatabaseSupport(drDeviceDatabaseSupportExists.GetSqlGuid(colStationId).Value,
									drDeviceDatabaseSupportExists.GetSqlGuid(colBuildingId).Value,
									drDeviceDatabaseSupportExists.GetSqlGuid(colRackId).Value,
									drDeviceDatabaseSupportExists.GetSqlGuid(colPortId).Value);
							}
						} while (drDeviceDatabaseSupportExists.Read());
					}

					if ((deviceDatabaseSupport == null) && (createTopography) && (Settings.Default.IsOnClientDatabase))
					{
						// La topografia non esiste, eventualmente la creiamo
						Guid stationId = InsertStation(dbConnectionString, location.StationId, location.StationName, 0);
						if (stationId != Guid.Empty)
						{
							Guid buildingId = InsertBuilding(dbConnectionString, location.BuildingId, location.BuildingName,
								location.BuildingNotes, stationId, 0);

							if (buildingId != Guid.Empty)
							{
								Guid rackId = InsertRack(dbConnectionString, location.LocationId, location.LocationName, location.LocationType,
									location.LocationNotes, buildingId, 0);

								if (rackId != Guid.Empty)
								{
									serverId = InsertServer(dbConnectionString, serverId, regionId, regionName, 0, zoneId, zoneName, 0, nodeId,
										nodeName, 0, serverHost, serverName, 0);

									if (serverId > 0)
									{
										Guid portId = InsertPort(dbConnectionString, port.Id, port.Name, port.Type, port.Parameters, port.Status,
											serverId, 0);

										if (portId != Guid.Empty)
										{
											cmdCreate.CommandType = CommandType.Text;
											cmdCreate.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
											cmdCreate.CommandText =
												"DECLARE @StationID uniqueidentifier; DECLARE @BuildingID uniqueidentifier; DECLARE @RackID uniqueidentifier; DECLARE @PortID uniqueidentifier; SELECT TOP (1) @StationID = station.StationID, @BuildingID = building.BuildingID, @RackID = rack.RackID FROM station INNER JOIN building ON station.StationID = building.StationID INNER JOIN rack ON building.BuildingID = rack.BuildingID WHERE (station.StationName = @StationName) AND (building.BuildingName = @BuildingName) AND (building.BuildingXMLID = @BuildingXMLID) AND (building.BuildingDescription = @BuildingDescription) AND (rack.RackName = @RackName) AND (rack.RackXMLID = @RackXMLID) AND (rack.RackType = @RackType) AND (rack.RackDescription = @RackDescription) AND (building.Removed = 0) AND (rack.Removed = 0) AND (station.Removed = 0); SELECT TOP (1) @PortID = port.PortID FROM port WHERE (SrvID = @SrvID) AND (PortXMLID = @PortXMLID) SELECT @StationID AS StationID, @BuildingID AS BuildingID, @RackID AS RackID, @PortID AS PortID";
											cmdCreate.Parameters.Add(new SqlParameter("@StationName", SqlDbType.VarChar, 64, ParameterDirection.Input,
												false, 0, 0, null, DataRowVersion.Current, location.StationName));
											cmdCreate.Parameters.Add(new SqlParameter("@BuildingName", SqlDbType.VarChar, 64, ParameterDirection.Input,
												false, 0, 0, null, DataRowVersion.Current, location.BuildingName));
											cmdCreate.Parameters.Add(new SqlParameter("@BuildingXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false,
												0, 10, null, DataRowVersion.Current, location.BuildingId));
											cmdCreate.Parameters.Add(new SqlParameter("@RackName", SqlDbType.VarChar, 64, ParameterDirection.Input, false,
												0, 0, null, DataRowVersion.Current, location.LocationName));
											cmdCreate.Parameters.Add(new SqlParameter("@RackXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0,
												10, null, DataRowVersion.Current, location.LocationId));
											cmdCreate.Parameters.Add(new SqlParameter("@RackType", SqlDbType.VarChar, 64, ParameterDirection.Input, false,
												0, 0, null, DataRowVersion.Current, location.LocationType));
											cmdCreate.Parameters.Add(new SqlParameter("@RackDescription", SqlDbType.VarChar, 256,
												ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.LocationNotes));
											cmdCreate.Parameters.Add(new SqlParameter("@BuildingDescription", SqlDbType.VarChar, 256,
												ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.BuildingNotes));
											cmdCreate.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10,
												null, DataRowVersion.Current, serverId));
											cmdCreate.Parameters.Add(new SqlParameter("@PortXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0,
												10, null, DataRowVersion.Current, port.Id));

											cmdCreate.Connection = dbConnectionCreate;

											dbConnectionCreate.Open();

											drDeviceDatabaseSupportCreate = cmdCreate.ExecuteReader();

											if (drDeviceDatabaseSupportCreate.Read())
											{
												int colStationId = drDeviceDatabaseSupportCreate.GetOrdinal("StationID");
												int colBuildingId = drDeviceDatabaseSupportCreate.GetOrdinal("BuildingID");
												int colRackId = drDeviceDatabaseSupportCreate.GetOrdinal("RackID");
												int colPortId = drDeviceDatabaseSupportCreate.GetOrdinal("PortID");

												do
												{
													if ((!drDeviceDatabaseSupportCreate.IsDBNull(colStationId)) &&
													    (!drDeviceDatabaseSupportCreate.IsDBNull(colBuildingId)) &&
													    (!drDeviceDatabaseSupportCreate.IsDBNull(colRackId)) &&
													    (!drDeviceDatabaseSupportCreate.IsDBNull(colPortId)))
													{
														deviceDatabaseSupport =
															new DeviceDatabaseSupport(drDeviceDatabaseSupportCreate.GetSqlGuid(colStationId).Value,
																drDeviceDatabaseSupportCreate.GetSqlGuid(colBuildingId).Value,
																drDeviceDatabaseSupportCreate.GetSqlGuid(colRackId).Value,
																drDeviceDatabaseSupportCreate.GetSqlGuid(colPortId).Value);
													}
												} while (drDeviceDatabaseSupportCreate.Read());
											}
										}
									}
								}
							}
						}
					}
				}
				catch (SqlException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"Errore durante l'accesso alla base dati. " + ex.Message);
				}
				catch (IndexOutOfRangeException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando GetStationBuildingRackPortData. " +
						ex.Message);
				}
				finally
				{
					if (drDeviceDatabaseSupportExists != null && !drDeviceDatabaseSupportExists.IsClosed)
					{
						drDeviceDatabaseSupportExists.Close();
					}

					if (drDeviceDatabaseSupportCreate != null && !drDeviceDatabaseSupportCreate.IsClosed)
					{
						drDeviceDatabaseSupportCreate.Close();
					}

					if (dbConnectionExists != null)
					{
						dbConnectionExists.Close();
					}

					if (dbConnectionCreate != null)
					{
						dbConnectionCreate.Close();
					}

					cmdExists.Dispose();
					cmdCreate.Dispose();
				}
			}

			return deviceDatabaseSupport;
		}

		#endregion


        #region GetDefinitionVersion - Recupera la versione della definizione

        /// <summary>
        ///     Recupera la versione della definizione dato l'id dell'oggetto
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">id device</param>
        /// <returns>Torna la versione della definizione</returns>
        public static String GetDefinitionVersion(string dbConnectionString,
            long deviceId)
        {
            String version = null;

            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDeviceDatabaseSupport = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                        "La stringa di connessione al database non è valida. " + ex.Message);
                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "SELECT DefinitionVersion FROM devices WHERE (DevID = @DevID)";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, deviceId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDeviceDatabaseSupport = cmd.ExecuteReader();

                if (drDeviceDatabaseSupport.Read())
                {
                    int versionCol = drDeviceDatabaseSupport.GetOrdinal("DefinitionVersion");
                    version = ( drDeviceDatabaseSupport.IsDBNull(versionCol) ? null : drDeviceDatabaseSupport.GetSqlString(versionCol).Value );
                }
            }
            catch (SqlException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                    "Errore durante l'accesso alla base dati. " + ex.Message);
            }
            catch (IndexOutOfRangeException ex)
            {
                FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
                    "Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando GetDeviceAcknowledgementsList. " +
                    ex.Message);
            }
            finally
            {
                if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                {
                    drDeviceDatabaseSupport.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return version;
        }

        #endregion




		#region UpdateDevice - Aggiorna o inserisce i dati della periferica su database

		/// <summary>
		///     Aggiorna o inserisce i dati della periferica su database
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id della periferica</param>
		/// <param name="nodeId">Id del Nodo</param>
		/// <param name="serverId">Id del Server</param>
		/// <param name="deviceName">Nome della periferica</param>
		/// <param name="deviceType">Tipo della periferica</param>
		/// <param name="serialNumber">Numero di serie della periferica</param>
		/// <param name="deviceAddress">Indirizzo IP della periferica</param>
		/// <param name="portId">Id dell'interfaccia della periferica</param>
		/// <param name="profileId">Id del profilo della periferica</param>
		/// <param name="active">Stato della periferica</param>
		/// <param name="scheduled">Stato di schedulazione della periferica</param>
		/// <param name="removed">Stato di rimozione della periferica</param>
		/// <param name="rackId">Id dell'Armadio</param>
		/// <param name="rackPositionRow">Indice della posizione di riga nell'Armadio</param>
		/// <param name="rackPositionCol">Indice della posizione di colonna nell'Armadio</param>
		/// <param name="monitoringDeviceId">Identifica l'apparato che esegue il monitoraggio della periferica</param>
		/// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
		public static bool UpdateDevice(string dbConnectionString,
			long deviceId,
			long nodeId,
			int serverId,
			string deviceName,
			string deviceType,
			string serialNumber,
			IPAddress deviceAddress,
			Guid portId,
			int profileId,
			Byte active,
			Byte scheduled,
			Byte removed,
			Guid rackId,
			int rackPositionRow,
			int rackPositionCol,
			string monitoringDeviceId)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDevice = null;
			bool writeOperationSuccessful = false;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return false;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

				cmd.CommandText = Settings.Default.IsOnClientDatabase
					? "IF NOT EXISTS (SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID)) BEGIN INSERT INTO [devices] ([DevID], [NodID], [SrvID], [Name], [Type], [SN], [Addr], [PortId], [ProfileID], [Active], [Scheduled], [Removed], [RackID], [RackPositionRow], [RackPositionCol], [DefinitionVersion], [ProtocolDefinitionVersion], [DiscoveredType], [MonitoringDeviceID]) VALUES (@DevID, @NodID, @SrvID, @Name, @Type, @SN, @Addr, @PortId, @ProfileID, @Active, @Scheduled, @Removed, @RackID, @RackPositionRow, @RackPositionCol, @DefinitionVersion, @ProtocolDefinitionVersion, NULL, @MonitoringDeviceID) END ELSE BEGIN UPDATE [devices] SET [NodID] = @NodID, [SrvID] = @SrvID, [Name] = @Name, [Type] = @Type, [SN] = @SN, [Addr] = @Addr, [PortId] = @PortId, [ProfileID] = @ProfileID, [Active] = @Active, [Scheduled] = @Scheduled, [Removed] = @Removed, [RackID] = @RackID, [RackPositionRow] = @RackPositionRow, [RackPositionCol] = @RackPositionCol, [DiscoveredType] = NULL, [MonitoringDeviceID] = @MonitoringDeviceID WHERE ([DevID] = @DevID) END; SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID);"
					: "IF NOT EXISTS (SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID)) BEGIN INSERT INTO [devices] ([DevID], [NodID], [SrvID], [Name], [Type], [SN], [Addr], [PortId], [ProfileID], [Active], [Scheduled], [RackID], [RackPositionRow], [RackPositionCol], [DefinitionVersion], [ProtocolDefinitionVersion]) VALUES (@DevID, @NodID, @SrvID, @Name, @Type, @SN, @Addr, @PortId, @ProfileID, @Active, @Scheduled, @RackID, @RackPositionRow, @RackPositionCol, @DefinitionVersion, @ProtocolDefinitionVersion) END ELSE BEGIN UPDATE [devices] SET [NodID] = @NodID, [SrvID] = @SrvID, [Name] = @Name, [Type] = @Type, [SN] = @SN, [Addr] = @Addr, [PortId] = @PortId, [ProfileID] = @ProfileID, [Active] = @Active, [Scheduled] = @Scheduled, [RackID] = @RackID, [RackPositionRow] = @RackPositionRow, [RackPositionCol] = @RackPositionCol WHERE ([DevID] = @DevID) END; SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID);";

				cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, deviceId));
				cmd.Parameters.Add(new SqlParameter("@NodID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, nodeId));
				cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, serverId));
				cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, deviceName));
				cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, deviceType));
				cmd.Parameters.Add(new SqlParameter("@SN", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, serialNumber));
				cmd.Parameters.Add(new SqlParameter("@Addr", SqlDbType.VarChar, 32, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, DataUtility.EncodeIPAddress(deviceAddress)));
				cmd.Parameters.Add(new SqlParameter("@PortId", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, portId));
				cmd.Parameters.Add(new SqlParameter("@ProfileID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, profileId));
				cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, active));
				cmd.Parameters.Add(new SqlParameter("@Scheduled", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, scheduled));
				cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, removed));
				cmd.Parameters.Add(new SqlParameter("@RackID", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, rackId));
				cmd.Parameters.Add(new SqlParameter("@RackPositionRow", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10,
					null, DataRowVersion.Current, rackPositionRow));
				cmd.Parameters.Add(new SqlParameter("@RackPositionCol", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10,
					null, DataRowVersion.Current, rackPositionCol));
				cmd.Parameters.Add(new SqlParameter("@DefinitionVersion", SqlDbType.VarChar, 8, ParameterDirection.Input, false, 0,
					0, null, DataRowVersion.Current, DEFINITION_VERSION));
				cmd.Parameters.Add(new SqlParameter("@ProtocolDefinitionVersion", SqlDbType.VarChar, 8, ParameterDirection.Input,
					false, 0, 0, null, DataRowVersion.Current, PROTOCOL_DEFINITION_VERSION));
				cmd.Parameters.Add(new SqlParameter("@MonitoringDeviceID", SqlDbType.VarChar, 128, ParameterDirection.Input, false,
					0, 0, null, DataRowVersion.Current,
					(string.IsNullOrEmpty(monitoringDeviceId) ? (object) DBNull.Value : monitoringDeviceId)));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDevice = cmd.ExecuteReader();

				if (drDevice.Read())
				{
					int colDevID = drDevice.GetOrdinal("DevID");

					do
					{
						if ((!drDevice.IsDBNull(colDevID)) && (drDevice.GetSqlInt64(colDevID).Value == deviceId))
						{
							writeOperationSuccessful = true;
						}
					} while (drDevice.Read());
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando UpdateDevice. " +
					ex.Message);
			}
			finally
			{
				if (drDevice != null && !drDevice.IsClosed)
				{
					drDevice.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return writeOperationSuccessful;
		}

		#endregion

		#region DeleteDevice - Elimina i dati della periferica su database

		/// <summary>
		///     Elimina i dati della periferica su database
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id della periferica</param>
		/// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
		public static bool DeleteDevice(string dbConnectionString, long deviceId)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			bool deleteOperationSuccessful = false;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return false;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

				cmd.CommandText = Settings.Default.IsOnClientDatabase
					? "DELETE FROM [stream_fields] WHERE (DevID = @DevID); DELETE FROM [streams] WHERE (DevID = @DevID); DELETE FROM [events] WHERE (DevID = @DevID); DELETE FROM [procedures] WHERE (DevID = @DevID); DELETE FROM [device_status] WHERE (DevID = @DevID); DELETE FROM [devices] WHERE (DevID = @DevID);"
					: "DELETE FROM [stream_fields] WHERE (DevID = @DevID); DELETE FROM [streams] WHERE (DevID = @DevID); DELETE FROM [procedures] WHERE (DevID = @DevID); DELETE FROM [device_status] WHERE (DevID = @DevID); DELETE FROM [devices] WHERE (DevID = @DevID);";

				cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, deviceId));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				cmd.ExecuteNonQuery();

				deleteOperationSuccessful = true;
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			finally
			{
				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return deleteOperationSuccessful;
		}

		#endregion

		#region GetDeviceAcknowledgementsList - Recupera i dati delle tacitazioni relative al supervisore corrente

		/// <summary>
		///     Recupera i dati delle tacitazioni relative al supervisore corrente
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="snmpSupervisorID">Id del supervisore corrente</param>
		/// <returns>Lista di tacitazioni</returns>
		public static ReadOnlyCollection<DbDeviceAcknowledgement> GetDeviceAcknowledgementsList(string dbConnectionString,
			int snmpSupervisorID)
		{
			List<DbDeviceAcknowledgement> deviceAcknowledgementsList = new List<DbDeviceAcknowledgement>();

			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDeviceDatabaseSupport = null;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return null;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"SELECT DeviceAckID, DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username FROM device_ack WHERE (SupervisorID = @SupervisorID)";
				cmd.Parameters.Add(new SqlParameter("@SupervisorID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, snmpSupervisorID));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDeviceDatabaseSupport = cmd.ExecuteReader();

				if (drDeviceDatabaseSupport.Read())
				{
					int colDeviceAckID = drDeviceDatabaseSupport.GetOrdinal("DeviceAckID");
					int colDevID = drDeviceDatabaseSupport.GetOrdinal("DevID");
					int colStrID = drDeviceDatabaseSupport.GetOrdinal("StrID");
					int colFieldID = drDeviceDatabaseSupport.GetOrdinal("FieldID");
					int colAckDate = drDeviceDatabaseSupport.GetOrdinal("AckDate");
					int colAckDurationMinutes = drDeviceDatabaseSupport.GetOrdinal("AckDurationMinutes");
					int colSupervisorID = drDeviceDatabaseSupport.GetOrdinal("SupervisorID");
					int colUsername = drDeviceDatabaseSupport.GetOrdinal("Username");

					do
					{
						if (!drDeviceDatabaseSupport.IsDBNull(colDeviceAckID))
						{
							deviceAcknowledgementsList.Add(
								new DbDeviceAcknowledgement(drDeviceDatabaseSupport.GetSqlGuid(colDeviceAckID).Value,
									drDeviceDatabaseSupport.GetSqlInt64(colDevID).Value,
									drDeviceDatabaseSupport.IsDBNull(colStrID) ? (int?) null : drDeviceDatabaseSupport.GetSqlInt32(colStrID).Value,
									drDeviceDatabaseSupport.IsDBNull(colFieldID)
										? (int?) null
										: drDeviceDatabaseSupport.GetSqlInt32(colFieldID).Value,
									drDeviceDatabaseSupport.GetSqlDateTime(colAckDate).Value,
									drDeviceDatabaseSupport.GetSqlInt32(colAckDurationMinutes).Value,
									drDeviceDatabaseSupport.GetSqlByte(colSupervisorID).Value,
									drDeviceDatabaseSupport.GetSqlString(colUsername).Value));
						}
					} while (drDeviceDatabaseSupport.Read());
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando GetDeviceAcknowledgementsList. " +
					ex.Message);
			}
			finally
			{
				if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
				{
					drDeviceDatabaseSupport.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return deviceAcknowledgementsList.AsReadOnly();
		}

		#endregion

		#region DeleteExpiredAcknowledgements - Elimina le tacitazioni scadute (rispetto al momento corrente) e al supervisore in esecuzione

		/// <summary>
		///     Elimina le tacitazioni scadute (rispetto al momento corrente) e al supervisore in esecuzione
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="snmpSupervisorID">Id del supervisore corrente</param>
		/// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
		public static bool DeleteExpiredAcknowledgements(string dbConnectionString, int snmpSupervisorID)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			bool deleteOperationSuccessful = false;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return false;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"DELETE FROM device_ack WHERE (DATEADD(minute, AckDurationMinutes, AckDate) <= GetDate()) AND (SupervisorID = @SupervisorID);";
				cmd.Parameters.Add(new SqlParameter("@SupervisorID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, snmpSupervisorID));
				cmd.Connection = dbConnection;

				dbConnection.Open();

				cmd.ExecuteNonQuery();

				deleteOperationSuccessful = true;
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			finally
			{
				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return deleteOperationSuccessful;
		}

		#endregion

		#endregion

		#region Metodi privati

		#region InsertStation - Inserisce una stazione

		/// <summary>
		///     Inserisce una stazione
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="stationXmlId">Id della stazione da XML</param>
		/// <param name="stationName">Nome della stazione</param>
		/// <param name="removed">Intero che indica se la stazione è stata eliminata</param>
		/// <returns>Unique Identifier della stazione inserita o trovata</returns>
		private static Guid InsertStation(string dbConnectionString, int stationXmlId, string stationName, Byte removed)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDeviceDatabaseSupport = null;
			Guid stationId = Guid.Empty;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return Guid.Empty;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"DECLARE @StationID uniqueidentifier; SELECT TOP (1) @StationID = StationID FROM station WHERE (StationXMLID = @StationXMLID) AND (StationName LIKE @StationName); IF (@StationID IS NULL) BEGIN INSERT INTO station (StationID, StationXMLID, StationName, Removed) VALUES (NEWID(), @StationXMLID, @StationName, @Removed); SELECT @StationID = StationID FROM station WHERE (StationXMLID = @StationXMLID) AND (StationName LIKE @StationName); END ELSE BEGIN UPDATE station SET Removed = @Removed WHERE (StationXMLID = @StationXMLID) AND (StationName LIKE @StationName); END SELECT @StationID AS StationID;";
				cmd.Parameters.Add(new SqlParameter("@StationXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, stationXmlId));
				cmd.Parameters.Add(new SqlParameter("@StationName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, stationName));
				cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, removed));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDeviceDatabaseSupport = cmd.ExecuteReader();

				if (drDeviceDatabaseSupport.Read())
				{
					int colStationID = drDeviceDatabaseSupport.GetOrdinal("StationID");

					do
					{
						if (!drDeviceDatabaseSupport.IsDBNull(colStationID))
						{
							stationId = drDeviceDatabaseSupport.GetSqlGuid(colStationID).Value;
						}
					} while (drDeviceDatabaseSupport.Read());
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando InsertStation. " +
					ex.Message);
			}
			finally
			{
				if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
				{
					drDeviceDatabaseSupport.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return stationId;
		}

		#endregion

		#region InsertBuilding - Inserisce un fabbricato

		/// <summary>
		///     Inserisce un fabbricato
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="buildingXmlId">Id del fabbricato da XML</param>
		/// <param name="buildingName">Nome del fabbricato</param>
		/// <param name="buildingDescription">Descrizione del fabbricato</param>
		/// <param name="stationId">Identificativo della stazione che contiene il fabbricato</param>
		/// <param name="removed">Intero che indica se il fabbricato è stato eliminato</param>
		/// <returns>Unique Identifier del fabbricato inserito o trovato</returns>
		private static Guid InsertBuilding(string dbConnectionString,
			int buildingXmlId,
			string buildingName,
			string buildingDescription,
			Guid stationId,
			Byte removed)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDeviceDatabaseSupport = null;
			Guid buildingId = Guid.Empty;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return Guid.Empty;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"DECLARE @BuildingID uniqueidentifier; SELECT TOP (1) @BuildingID = BuildingID FROM building WHERE (BuildingXMLID = @BuildingXMLID) AND (StationID = @StationID) AND (BuildingName LIKE @BuildingName); IF (@BuildingID IS NULL) BEGIN INSERT INTO building (BuildingID, BuildingXMLID, StationID, BuildingName, BuildingDescription, Removed) VALUES (NEWID(), @BuildingXMLID, @StationID, @BuildingName, @BuildingDescription, @Removed); SELECT @BuildingID = BuildingID FROM building WHERE (BuildingXMLID = @BuildingXMLID) AND (StationID = @StationID) AND (BuildingName LIKE @BuildingName); END ELSE BEGIN UPDATE building SET Removed = @Removed, BuildingDescription = @BuildingDescription WHERE (BuildingXMLID = @BuildingXMLID) AND (StationID = @StationID) AND (BuildingName LIKE @BuildingName); END SELECT @BuildingID AS BuildingID;";
				cmd.Parameters.Add(new SqlParameter("@BuildingXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, buildingXmlId));
				cmd.Parameters.Add(new SqlParameter("@BuildingName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, buildingName));
				cmd.Parameters.Add(new SqlParameter("@BuildingDescription", SqlDbType.VarChar, 256, ParameterDirection.Input, false,
					0, 0, null, DataRowVersion.Current, buildingDescription));
				cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, removed));
				cmd.Parameters.Add(new SqlParameter("@StationID", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0,
					0, null, DataRowVersion.Current, stationId));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDeviceDatabaseSupport = cmd.ExecuteReader();

				if (drDeviceDatabaseSupport.Read())
				{
					int colBuildingID = drDeviceDatabaseSupport.GetOrdinal("BuildingID");

					do
					{
						if (!drDeviceDatabaseSupport.IsDBNull(colBuildingID))
						{
							buildingId = drDeviceDatabaseSupport.GetSqlGuid(colBuildingID).Value;
						}
					} while (drDeviceDatabaseSupport.Read());
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando InsertBuilding. " +
					ex.Message);
			}
			finally
			{
				if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
				{
					drDeviceDatabaseSupport.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return buildingId;
		}

		#endregion

		#region InsertRack - Inserisce un supporto

		/// <summary>
		///     Inserisce un supporto
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="rackXmlId">Id del supporto da XML</param>
		/// <param name="rackName">Nome del supporto</param>
		/// <param name="rackType">Tipo del supporto</param>
		/// <param name="rackDescription">Descrizione del supporto</param>
		/// <param name="buildingId">Identificativo del fabbricato che contiene il supporto</param>
		/// <param name="removed">Intero che indica se il fabbricato è stato eliminato</param>
		/// <returns>Unique Identifier del supporto inserito o trovato</returns>
		private static Guid InsertRack(string dbConnectionString,
			int rackXmlId,
			string rackName,
			string rackType,
			string rackDescription,
			Guid buildingId,
			Byte removed)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDeviceDatabaseSupport = null;
			Guid rackId = Guid.Empty;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return Guid.Empty;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"DECLARE @RackID uniqueidentifier; SELECT TOP (1) @RackID = RackID FROM rack WHERE (RackXMLID = @RackXMLID) AND (BuildingID = @BuildingID) AND (RackName LIKE @RackName); IF (@RackID IS NULL) BEGIN INSERT INTO rack (RackID, RackXMLID, BuildingID, RackName, RackType, RackDescription, Removed) VALUES (NEWID(), @RackXMLID, @BuildingID, @RackName, @RackType, @RackDescription, @Removed); SELECT @RackID = RackID FROM rack WHERE (RackXMLID = @RackXMLID) AND (BuildingID = @BuildingID) AND (RackName LIKE @RackName); END ELSE BEGIN UPDATE rack SET Removed = @Removed, RackDescription = @RackDescription, RackType = @RackType WHERE (RackXMLID = @RackXMLID) AND (BuildingID = @BuildingID) AND (RackName LIKE @RackName); END SELECT @RackID AS RackID;";
				cmd.Parameters.Add(new SqlParameter("@RackXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, rackXmlId));
				cmd.Parameters.Add(new SqlParameter("@RackName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, rackName));
				cmd.Parameters.Add(new SqlParameter("@RackType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, rackType));
				cmd.Parameters.Add(new SqlParameter("@RackDescription", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0,
					0, null, DataRowVersion.Current, rackDescription));
				cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, removed));
				cmd.Parameters.Add(new SqlParameter("@BuildingID", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false,
					0, 0, null, DataRowVersion.Current, buildingId));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDeviceDatabaseSupport = cmd.ExecuteReader();

				if (drDeviceDatabaseSupport.Read())
				{
					int colRackID = drDeviceDatabaseSupport.GetOrdinal("RackID");

					do
					{
						if (!drDeviceDatabaseSupport.IsDBNull(colRackID))
						{
							rackId = drDeviceDatabaseSupport.GetSqlGuid(colRackID).Value;
						}
					} while (drDeviceDatabaseSupport.Read());
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando InsertRack. " +
					ex.Message);
			}
			finally
			{
				if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
				{
					drDeviceDatabaseSupport.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return rackId;
		}

		#endregion

		#region InsertPort - Inserisce una interfaccia

		/// <summary>
		///     Inserisce una interfaccia
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="portXmlId">Id della porta da XML</param>
		/// <param name="portName">Nome della porta</param>
		/// <param name="portType">Tipo della porta</param>
		/// <param name="parameters">Parametri della porta</param>
		/// <param name="status">Parametri della porta</param>
		/// <param name="serverId">Identificativo del server a cui la porta è riferita</param>
		/// <param name="removed">Intero che indica se la porta è stata eliminato</param>
		/// <returns>Unique Identifier della porta inserita o trovata</returns>
		private static Guid InsertPort(string dbConnectionString,
			int portXmlId,
			string portName,
			string portType,
			string parameters,
			string status,
			int serverId,
			Byte removed)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDeviceDatabaseSupport = null;
			Guid portId = Guid.Empty;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return Guid.Empty;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"DECLARE @PortID uniqueidentifier; SELECT TOP (1) @PortID = PortID FROM port WHERE (PortXMLID = @PortXMLID) AND (SrvID = @SrvID) AND (PortName LIKE @PortName); IF (@PortID IS NULL) BEGIN INSERT INTO port (PortID, PortXMLID, PortName, PortType, Parameters, Status, Removed, SrvID) VALUES (NEWID(), @PortXMLID, @PortName, @PortType, @Parameters, @Status, @Removed, @SrvID); SELECT @PortID = PortID FROM port WHERE (PortXMLID = @PortXMLID) AND (SrvID = @SrvID) AND (PortName LIKE @PortName); END ELSE BEGIN UPDATE port SET Removed = @Removed, PortType = @PortType, Parameters = @Parameters, Status = @Status WHERE (PortXMLID = @PortXMLID) AND (SrvID = @SrvID) AND (PortName LIKE @PortName); END SELECT @PortID AS PortID;";
				cmd.Parameters.Add(new SqlParameter("@PortXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, portXmlId));
				cmd.Parameters.Add(new SqlParameter("@PortName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, portName));
				cmd.Parameters.Add(new SqlParameter("@PortType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, portType));
				cmd.Parameters.Add(new SqlParameter("@Parameters", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, parameters));
				cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, status));
				cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
					DataRowVersion.Current, removed));
				cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, serverId));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDeviceDatabaseSupport = cmd.ExecuteReader();

				if (drDeviceDatabaseSupport.Read())
				{
					int colPortID = drDeviceDatabaseSupport.GetOrdinal("PortID");

					do
					{
						if (!drDeviceDatabaseSupport.IsDBNull(colPortID))
						{
							portId = drDeviceDatabaseSupport.GetSqlGuid(colPortID).Value;
						}
					} while (drDeviceDatabaseSupport.Read());
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando InsertPort. " +
					ex.Message);
			}
			finally
			{
				if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
				{
					drDeviceDatabaseSupport.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return portId;
		}

		#endregion

		#region InsertServer - Inserisce un server e relativa gerarchia

		/// <summary>
		///     Inserisce un server e relativa gerarchia
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="serverId">ID del Server da System.xml</param>
		/// <param name="regionId">ID del Compartimento</param>
		/// <param name="regionName">Nome del Compartimento</param>
		/// <param name="regionRemoved">Intero che indica se il Compartimento è rimosso</param>
		/// <param name="zoneId">ID della Zona</param>
		/// <param name="zoneName">Nome della Zona</param>
		/// <param name="zoneRemoved">Intero che indica se la Zona è rimossa</param>
		/// <param name="nodeId">ID del Nodo</param>
		/// <param name="nodeName">Nome del Nodo</param>
		/// <param name="nodeRemoved">Intero che indica se il Nodo è rimosso</param>
		/// <param name="serverHost">Host che monitora la periferica</param>
		/// <param name="serverName">Nome del Server che monitora la periferica</param>
		/// <param name="serverRemoved">Intero che indica se il Server è rimosso</param>
		/// <returns>Id del server inserito o trovato</returns>
		private static int InsertServer(string dbConnectionString,
			int serverId,
			long regionId,
			string regionName,
			byte regionRemoved,
			long zoneId,
			string zoneName,
			byte zoneRemoved,
			long nodeId,
			string nodeName,
			byte nodeRemoved,
			string serverHost,
			string serverName,
			byte serverRemoved)
		{
			SqlConnection dbConnection = null;
			SqlCommand cmd = new SqlCommand();
			SqlDataReader drDeviceDatabaseSupport = null;
			int currentServerId = 0;

			try
			{
				try
				{
					dbConnection = new SqlConnection(dbConnectionString);
				}
				catch (ArgumentException ex)
				{
					FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
						"La stringa di connessione al database non è valida. " + ex.Message);
					return 0;
				}

				cmd.CommandType = CommandType.Text;
				cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
				cmd.CommandText =
					"DECLARE @ServerID INT; SELECT @ServerID = SrvID FROM servers WHERE (SrvID = @SrvID) AND (NodID = @NodID); IF NOT EXISTS (SELECT RegId FROM regions WHERE RegId = @RegID) BEGIN INSERT INTO regions (RegID, Name, Removed) VALUES (@RegID, @RegionName, @RegionRemoved); END ELSE BEGIN UPDATE regions SET Name = @RegionName, Removed = @RegionRemoved WHERE RegID = @RegID; END IF NOT EXISTS (SELECT ZonID FROM zones WHERE ZonId = @ZonID) BEGIN INSERT INTO zones (ZonID, RegID, Name, Removed) VALUES (@ZonID, @RegID, @ZoneName, @ZoneRemoved); END ELSE BEGIN UPDATE zones SET Name = @ZoneName, Removed = @ZoneRemoved, RegID = @RegID WHERE ZonID = @ZonID; END IF NOT EXISTS (SELECT NodId FROM nodes WHERE NodId = @NodID) BEGIN INSERT INTO nodes (NodID, ZonID, Name, Removed, Meters) VALUES (@NodID, @ZonID, @NodeName, @NodeRemoved, @Meters); END ELSE BEGIN UPDATE nodes SET Name = @NodeName, Removed = @NodeRemoved, ZonID = @ZonID, Meters = @Meters WHERE NodID = @NodID; END IF (@ServerID IS NULL) BEGIN INSERT INTO servers (SrvID, Name, Host, FullHostName, IP, LastUpdate, LastMessageType, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC, Removed) VALUES (@SrvID, @ServerName, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey, @NodID, @ServerVersion, @MAC, @ServerRemoved); SELECT @ServerID = SrvID FROM servers WHERE (SrvID = @SrvID) AND (NodID = @NodID); END ELSE BEGIN UPDATE servers SET Name = @ServerName, Host = @Host, FullHostName = @FullHostName, IP = @IP, ClientValidationSign = @ClientValidationSign, ClientDateValidationRequested = @ClientDateValidationRequested, ClientDateValidationObtained = @ClientDateValidationObtained, ClientKey = @ClientKey, ServerVersion = @ServerVersion, MAC = @MAC, Removed = @ServerRemoved WHERE (SrvID = @SrvID) AND (NodID = @NodID); END SELECT @SrvID AS SrvID;";
				cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, serverId));
				cmd.Parameters.Add(new SqlParameter("@ServerName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, serverName));
				cmd.Parameters.Add(new SqlParameter("@Host", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, serverHost));
				cmd.Parameters.Add(new SqlParameter("@ServerRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3,
					null, DataRowVersion.Current, serverRemoved));
				cmd.Parameters.Add(new SqlParameter("@NodID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, nodeId));
				cmd.Parameters.Add(new SqlParameter("@RegID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, regionId));
				cmd.Parameters.Add(new SqlParameter("@RegionName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, regionName));
				cmd.Parameters.Add(new SqlParameter("@RegionRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3,
					null, DataRowVersion.Current, regionRemoved));
				cmd.Parameters.Add(new SqlParameter("@ZonID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
					DataRowVersion.Current, zoneId));
				cmd.Parameters.Add(new SqlParameter("@ZoneName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, zoneName));
				cmd.Parameters.Add(new SqlParameter("@ZoneRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3,
					null, DataRowVersion.Current, zoneRemoved));
				cmd.Parameters.Add(new SqlParameter("@NodeName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, nodeName));
				cmd.Parameters.Add(new SqlParameter("@NodeRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3,
					null, DataRowVersion.Current, nodeRemoved));
				cmd.Parameters.Add(new SqlParameter("@Meters", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
					DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@FullHostName", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, string.Empty));
				cmd.Parameters.Add(new SqlParameter("@IP", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, string.Empty));
				cmd.Parameters.Add(new SqlParameter("@LastUpdate", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@LastMessageType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, string.Empty));
				cmd.Parameters.Add(new SqlParameter("@SupervisorSystemXML", SqlDbType.Xml, -1, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@ClientSupervisorSystemXMLValidated", SqlDbType.Xml, -1,
					ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@ClientValidationSign", SqlDbType.VarBinary, 128, ParameterDirection.Input,
					false, 0, 0, null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@ClientDateValidationRequested", SqlDbType.DateTime, 8,
					ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@ClientDateValidationObtained", SqlDbType.DateTime, 8, ParameterDirection.Input,
					false, 0, 0, null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@ClientKey", SqlDbType.VarBinary, 148, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@ServerVersion", SqlDbType.VarChar, 32, ParameterDirection.Input, false, 0, 0,
					null, DataRowVersion.Current, DBNull.Value));
				cmd.Parameters.Add(new SqlParameter("@MAC", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null,
					DataRowVersion.Current, string.Empty));

				cmd.Connection = dbConnection;

				dbConnection.Open();

				drDeviceDatabaseSupport = cmd.ExecuteReader();

				if (drDeviceDatabaseSupport.Read())
				{
					int colSrvID = drDeviceDatabaseSupport.GetOrdinal("SrvID");

					do
					{
						if (!drDeviceDatabaseSupport.IsDBNull(colSrvID))
						{
							currentServerId = drDeviceDatabaseSupport.GetSqlInt32(colSrvID).Value;
						}
					} while (drDeviceDatabaseSupport.Read());
				}
			}
			catch (SqlException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. " + ex.Message);
			}
			catch (IndexOutOfRangeException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi, durante l'esecuzione del comando InsertServer. " +
					ex.Message);
			}
			finally
			{
				if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
				{
					drDeviceDatabaseSupport.Close();
				}

				if (dbConnection != null)
				{
					dbConnection.Close();
				}

				cmd.Dispose();
			}

			return currentServerId;
		}

		#endregion

		#endregion
	}
}
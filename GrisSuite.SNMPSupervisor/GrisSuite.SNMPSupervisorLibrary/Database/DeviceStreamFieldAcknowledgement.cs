﻿namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    public class DeviceStreamFieldAcknowledgement
    {
        /// <summary>
        /// Stream Id
        /// </summary>
        public int StreamId { get; private set; }

        /// <summary>
        ///  Stream Field Id
        /// </summary>
        public int StreamFieldId { get; private set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="streamId">Stream Id</param>
        /// <param name="streamFieldId">Stream Field Id</param>
        public DeviceStreamFieldAcknowledgement(int streamId, int streamFieldId)
        {
            this.StreamId = streamId;
            this.StreamFieldId = streamFieldId;
        }
    }
}
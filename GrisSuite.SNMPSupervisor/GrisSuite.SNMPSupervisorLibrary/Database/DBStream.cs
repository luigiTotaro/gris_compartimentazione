﻿using System.Collections.ObjectModel;
using System.Text;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    /// <summary>
    ///   Rappresenta il singolo Stream da persistere su database
    /// </summary>
    public class DBStream
    {
        #region Variabili private

        private readonly Collection<DBStreamField> streamData;

        #endregion

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "streamId">Id dello Stream</param>
        /// <param name = "name">Nome dello Stream</param>
        /// <param name = "isMib2">Indica se lo stream è relativo al MIB II standard</param>
        /// <param name = "excludedFromSeverityComputation">Indica se lo stream e la sua severity devono essere tenuti in considerazione nel calcolo della severità del device</param>
        public DBStream(int streamId, string name, bool isMib2, bool excludedFromSeverityComputation)
        {
            this.StreamId = streamId;
            this.Name = name;
            this.IsMib2 = isMib2;
            this.ContainsStreamFieldWithError = false;
            this.ExcludedFromSeverityComputation = excludedFromSeverityComputation;
            this.streamData = new Collection<DBStreamField>();
        }

        #region Proprietà pubbliche

        /// <summary>
        ///   Nome dello Stream
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///   Lista dei Field contenuti nello Stream
        /// </summary>
        public Collection<DBStreamField> FieldData
        {
            get { return this.streamData; }
        }

        /// <summary>
        ///   Id dello Stream
        /// </summary>
        public int StreamId { get; private set; }

        /// <summary>
        ///   Indica se lo stream è relativo al MIB II standard
        /// </summary>
        public bool IsMib2 { get; private set; }

        /// <summary>
        ///   Indica se lo stream contiene almeno uno stream field che ha generato eccezione nel caricamento dei dati
        /// </summary>
        public bool ContainsStreamFieldWithError { get; private set; }

        /// <summary>
        ///   Severità relativa allo Stream (come aggregata dei Field contenuti)
        /// </summary>
        public Severity SeverityLevel
        {
            get
            {
                Severity streamSeverity = Severity.Unknown;

                foreach (DBStreamField field in this.streamData)
                {
                    bool processSeverity = false;

                    if (!field.isDiscardData)
                    {
                        if (!field.isChild() && !field.isFather())
                        {
                            processSeverity = true;
                        }
                        // se è un aggregato che non fa parte di uno stream field array
                        else if (field.IsAggregation && ((field.StreamFieldFather != null && field.StreamFieldFather.StreamFieldFather == null) || field.StreamFieldFather == null))
                        {
                            processSeverity = true;
                        }
                        // se è un padre e non fa parte di uno stream field array
                        else if (field.isFather() && field.IsArray && field.StreamFieldFather == null)
                        {
                            processSeverity = true;
                        }
                        // se è un padre e uno stream field array
                        else if (field.isFather() && field.IsStreamFieldArray)
                        {
                            processSeverity = true;
                        }
                    }


                    if (processSeverity)
                    {
                        // occorre escludere i campi che non saranno poi salvati sul database, perché la loro severità
                        // sebbene effettiva, non deve essere contemplata
                        if ((field.SeverityLevel != Severity.Unknown) && (field.PersistOnDatabase) && (!field.ExcludedFromSeverityComputation) &&
                            ((streamSeverity == Severity.Unknown) || ((int)field.SeverityLevel > (int)streamSeverity)))
                        {
                            streamSeverity = field.SeverityLevel;
                        }
                    }

                    if ((!this.ContainsStreamFieldWithError) && (field.LastError != null))
                    {
                        this.ContainsStreamFieldWithError = true;
                    }
                }

                return streamSeverity;
            }
        }

        /// <summary>
        ///   Decodifica descrittiva del valore dello Stream, come concatenazione delle descrizioni dei singoli Field, se la loro severità è non sconosciuta
        /// </summary>
        public string ValueDescriptionComplete
        {
            get
            {
                StringBuilder deviceDescription = new StringBuilder();

                int fieldCounter = 0;

                foreach (DBStreamField field in this.streamData)
                {
                    // escludiamo dall'aggregazione i campi che non saranno salvati sul database
                    if ((field.SeverityLevel != Severity.Unknown) && (field.PersistOnDatabase))
                    {
                        if (fieldCounter > 0)
                        {
                            deviceDescription.Append(";" + field.ValueDescriptionComplete);
                        }
                        else
                        {
                            deviceDescription.Append(field.ValueDescriptionComplete);
                        }
                        fieldCounter++;
                    }
                }

                return deviceDescription.ToString();
            }
        }

        /// <summary>
        ///   Indica se lo stream  e la sua severity devono essere tenuti in considerazione nel calcolo della severità della device
        /// </summary>
        public bool ExcludedFromSeverityComputation { get; set; }

        #endregion
    }
}
﻿using System;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
    public class FieldMemoryBase : ICloneable
    {
        public FieldMemoryBase() {
            this.MemoryBase = -1;
        }

        public int MemoryBase { set; get; }

        public object Clone()
        {
            FieldMemoryBase obj = new FieldMemoryBase();
            obj.MemoryBase = this.MemoryBase;
            return obj;
        }
    }
}

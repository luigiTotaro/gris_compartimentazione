using System;
using System.Collections.ObjectModel;
using System.Net;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
	/// <summary>
	///     Factory che si occupa della persistenza e recupero dei dati
	/// </summary>
	public class DatabaseFactory
	{
		private readonly bool persistOnSqlDatabase;

		/// <summary>
		///     Costruttore
		/// </summary>
		/// <param name="persistOnDatabase">
		///     Indica se persistere i dati su database o meno (dump su file). Se non indicato, usa
		///     l'impostazione da configurazione (parametro 'PersistOnDatabase')
		/// </param>
		public DatabaseFactory(bool? persistOnDatabase)
		{
			this.persistOnSqlDatabase = persistOnDatabase.HasValue ? persistOnDatabase.Value : Settings.Default.PersistOnDatabase;
		}

        /// <summary>
        ///     Elimina tutti gli streamfield legati ad uno stream ed ad un dato device
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id della periferica</param>
        /// <param name="deviceId">Id dello stream</param>
        /// <returns>Booleano che indica se l'operazione � avvenuta correttamente</returns>
        public bool DeleteStreamField(string dbConnectionString, long deviceId, int streamId) {
            if (this.persistOnSqlDatabase)
            {
                return SqlDatabaseUtility.DeleteStreamField(dbConnectionString, deviceId, streamId);
            }

            return FakeDatabaseUtility.DeleteStreamField();
        }

        public bool DeleteStreamField(string dbConnectionString, long deviceId, int streamId, int streamFiledId)
        {
            if (this.persistOnSqlDatabase)
            {
                return SqlDatabaseUtility.DeleteStreamField(dbConnectionString, deviceId, streamId, streamFiledId);
            }

            return FakeDatabaseUtility.DeleteStreamField();
        }


		/// <summary>
		///     Aggiorna lo stato della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="severityLevel">Livello di severit� del Device</param>
		/// <param name="descriptionStatus">Stato del Device</param>
		/// <param name="offline">Indica se il Device � fuori linea o non raggiungibile</param>
		/// <param name="shouldSendNotificationByEmail">Indica se inviare mail di notifica</param>
		public void UpdateDeviceStatus(string dbConnectionString,
			long deviceId,
			int severityLevel,
			string descriptionStatus,
			Byte offline,
			Byte shouldSendNotificationByEmail)
		{
			if (this.persistOnSqlDatabase)
			{
				SqlDatabaseUtility.UpdateDeviceStatus(dbConnectionString, deviceId, severityLevel, descriptionStatus, offline,
					shouldSendNotificationByEmail);
			}
			else
			{
				FakeDatabaseUtility.UpdateDeviceStatus(deviceId, severityLevel, descriptionStatus, offline,
					shouldSendNotificationByEmail);
			}
		}

		/// <summary>
		///     Aggiorna il tipo della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDeviceType">Nuovo tipo del Device</param>
		public void UpdateDeviceType(string dbConnectionString, long deviceId, string newDeviceType)
		{
			if (this.persistOnSqlDatabase)
			{
				SqlDatabaseUtility.UpdateDeviceType(dbConnectionString, deviceId, newDeviceType);
			}
			else
			{
				FakeDatabaseUtility.UpdateDeviceType(deviceId, newDeviceType);
			}
		}

		/// <summary>
		///     Aggiorna il numero di serie della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDeviceSerialNumber">Nuovo numero di serie del Device</param>
		public void UpdateDeviceSerialNumber(string dbConnectionString, long deviceId, string newDeviceSerialNumber)
		{
			if (this.persistOnSqlDatabase)
			{
				SqlDatabaseUtility.UpdateDeviceSerialNumber(dbConnectionString, deviceId, newDeviceSerialNumber);
			}
			else
			{
				FakeDatabaseUtility.UpdateDeviceSerialNumber(deviceId, newDeviceSerialNumber);
			}
		}

		/// <summary>
		///     Aggiorna la versione della definizione della periferica
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="newDefinitionVersion">Nuova versione della definizione</param>
		public void UpdateDeviceDefinitionVersion(string dbConnectionString, long deviceId, string newDefinitionVersion)
		{
			if (this.persistOnSqlDatabase)
			{
				SqlDatabaseUtility.UpdateDeviceDefinitionVersion(dbConnectionString, deviceId, newDefinitionVersion);
			}
			else
			{
				FakeDatabaseUtility.UpdateDeviceDefinitionVersion(deviceId, newDefinitionVersion);
			}
		}

		/// <summary>
		///     Aggiorna lo Stream
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		/// <param name="streamId">Id dello Stream</param>
		/// <param name="name">Nome dello Stream</param>
		/// <param name="severityLevel">Livello di severit� dello Stream</param>
		/// <param name="description">Descrizione del valore dello Stream</param>
		public void UpdateStream(string dbConnectionString,
			long deviceId,
			int streamId,
			string name,
			int severityLevel,
			string description)
		{
			if (this.persistOnSqlDatabase)
			{
				SqlDatabaseUtility.UpdateStream(dbConnectionString, deviceId, streamId, name, severityLevel, description);
			}
			else
			{
				FakeDatabaseUtility.UpdateStream(deviceId, streamId, name, severityLevel, description);
			}
		}

        /// <summary>
        ///     Aggiorna il Field
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="streamId">Id dello Stream</param>
        /// <param name="fieldId">Id del Field</param>
        /// <param name="arrayId">Id del vettore dei valori del Field</param>
        /// <param name="name">Nome del Field</param>
        /// <param name="severityLevel">Livello di severit� del Field</param>
        /// <param name="value">Valore del Field</param>
        /// <param name="description">Descrizione del valore del Field</param>
        /// <param name="shouldSendNotificationByEmail">Indica se inviare mail di notifica</param>
        public void UpdateStreamField(string dbConnectionString,
            long deviceId,
            int streamId,
            int fieldId,
            int arrayId,
            string name,
            int severityLevel,
            string value,
            string description,
            Byte shouldSendNotificationByEmail, bool isVisible = true, bool isTable = false)
        {
            if (this.persistOnSqlDatabase)
            {
                SqlDatabaseUtility.UpdateStreamField(dbConnectionString, deviceId, streamId, fieldId, arrayId, name, severityLevel,
                    value, description, shouldSendNotificationByEmail, isVisible, isTable);
            }
            else
            {
                FakeDatabaseUtility.UpdateStreamField(deviceId, fieldId, arrayId, name, severityLevel, value, description,
                    shouldSendNotificationByEmail);
            }
        }


		/// <summary>
		///     Verifica l'esistenza di una periferica in base dati
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id del Device</param>
		public bool? CheckDeviceExistence(string dbConnectionString, long deviceId)
		{
			if (this.persistOnSqlDatabase)
			{
				return SqlDatabaseUtility.CheckDeviceExistence(dbConnectionString, deviceId);
			}

			return FakeDatabaseUtility.CheckDeviceExistence();
		}

		/// <summary>
		///     Recupera i dati di lookup per poter creare una periferica su database
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="location">Dati della topografia della periferica</param>
		/// <param name="regionId">ID del Compartimento</param>
		/// <param name="regionName">Nome del Compartimento</param>
		/// <param name="zoneId">ID della Zona</param>
		/// <param name="zoneName">Nome della Zona</param>
		/// <param name="nodeId">ID del Nodo</param>
		/// <param name="nodeName">Nome del Nodo</param>
		/// <param name="serverId">ID del Server da System.xml</param>
		/// <param name="serverHost">Host che monitora la periferica</param>
		/// <param name="serverName">Nome del Server che monitora la periferica</param>
		/// <param name="port">Porta da System.xml</param>
		/// <param name="createTopography">Indica se creare la topografia, se non esistente su database</param>
		/// <returns>Dati di lookup con l'Id di Stazione, Fabbricato, Armadio e Interfaccia</returns>
		public DeviceDatabaseSupport GetStationBuildingRackPortData(string dbConnectionString,
			TopographyLocation location,
			long regionId,
			string regionName,
			long zoneId,
			string zoneName,
			long nodeId,
			string nodeName,
			int serverId,
			string serverHost,
			string serverName,
			SystemPort port,
			bool createTopography)
		{
			if (this.persistOnSqlDatabase)
			{
				return SqlDatabaseUtility.GetStationBuildingRackPortData(dbConnectionString, location, regionId, regionName, zoneId,
					zoneName, nodeId, nodeName, serverId, serverHost, serverName, port, createTopography);
			}

			return FakeDatabaseUtility.GetStationBuildingRackPortData();
		}

        /// <summary>
        ///     Recupera la versione della  definizione
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id della periferica</param>
        /// <returns>Versione del device</returns>
        public String GetDefinitionVersion(string dbConnectionString,
            long deviceId)
        {
            if (this.persistOnSqlDatabase)
            {
                return SqlDatabaseUtility.GetDefinitionVersion(dbConnectionString, deviceId);
            }

            return FakeDatabaseUtility.GetDefinitionVersion();
        }

		/// <summary>
		///     Aggiorna o inserisce i dati della periferica su database
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id della periferica</param>
		/// <param name="nodeId">Id del Nodo</param>
		/// <param name="serverId">Id del Server</param>
		/// <param name="deviceName">Nome della periferica</param>
		/// <param name="deviceType">Tipo della periferica</param>
		/// <param name="serialNumber">Numero di serie della periferica</param>
		/// <param name="deviceAddress">Indirizzo IP della periferica</param>
		/// <param name="portId">Id dell'interfaccia della periferica</param>
		/// <param name="profileId">Id del profilo della periferica</param>
		/// <param name="active">Stato della periferica</param>
		/// <param name="scheduled">Stato di schedulazione della periferica</param>
		/// <param name="removed">Stato di rimozione della periferica</param>
		/// <param name="rackId">Id dell'Armadio</param>
		/// <param name="rackPositionRow">Indice della posizione di riga nell'Armadio</param>
		/// <param name="rackPositionCol">Indice della posizione di colonna nell'Armadio</param>
		/// <param name="monitoringDeviceId">Identifica l'apparato che esegue il monitoraggio della periferica</param>
		/// <returns>Booleano che indica se l'operazione � avvenuta correttamente</returns>
		public bool UpdateDevice(string dbConnectionString,
			long deviceId,
			long nodeId,
			int serverId,
			string deviceName,
			string deviceType,
			string serialNumber,
			IPAddress deviceAddress,
			Guid portId,
			int profileId,
			Byte active,
			Byte scheduled,
			Byte removed,
			Guid rackId,
			int rackPositionRow,
			int rackPositionCol,
			string monitoringDeviceId)
		{
			if (this.persistOnSqlDatabase)
			{
				return SqlDatabaseUtility.UpdateDevice(dbConnectionString, deviceId, nodeId, serverId, deviceName, deviceType,
					serialNumber, deviceAddress, portId, profileId, active, scheduled, removed, rackId, rackPositionRow,
					rackPositionCol, monitoringDeviceId);
			}

			return FakeDatabaseUtility.UpdateDevice(deviceId, nodeId, serverId, deviceName, deviceType, serialNumber,
				deviceAddress, portId, profileId, active, scheduled, removed, rackId, rackPositionRow, rackPositionCol,
				monitoringDeviceId);
		}

		/// <summary>
		///     Elimina i dati della periferica su database
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <param name="deviceId">Id della periferica</param>
		/// <returns>Booleano che indica se l'operazione � avvenuta correttamente</returns>
		public bool DeleteDevice(string dbConnectionString, long deviceId)
		{
			if (this.persistOnSqlDatabase)
			{
				return SqlDatabaseUtility.DeleteDevice(dbConnectionString, deviceId);
			}

			return FakeDatabaseUtility.DeleteDevice();
		}

		/// <summary>
		///     Recupera i dati delle tacitazioni relative al supervisore corrente
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <returns>Lista di tacitazioni</returns>
		public ReadOnlyCollection<DbDeviceAcknowledgement> GetDeviceAcknowledgementsList(string dbConnectionString)
		{
			if (this.persistOnSqlDatabase)
			{
				return SqlDatabaseUtility.GetDeviceAcknowledgementsList(dbConnectionString, Settings.Default.SnmpSupervisorID);
			}

			return FakeDatabaseUtility.GetDeviceAcknowledgementsList();
		}

		/// <summary>
		///     Elimina le tacitazioni scadute (rispetto al momento corrente) e al supervisore in esecuzione
		/// </summary>
		/// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
		/// <returns>Booleano che indica se l'operazione � avvenuta correttamente</returns>
		public bool DeleteExpiredAcknowledgements(string dbConnectionString)
		{
			if (this.persistOnSqlDatabase)
			{
				return SqlDatabaseUtility.DeleteExpiredAcknowledgements(dbConnectionString, Settings.Default.SnmpSupervisorID);
			}

			return FakeDatabaseUtility.DeleteExpiredAcknowledgements();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security;
using System.Xml;
using GrisSuite.SnmpSupervisorLibrary.Properties;

namespace GrisSuite.SnmpSupervisorLibrary.Database
{
	/// <summary>
	///     Classe di supporto per il parsing del file di configurazione (System.xml)
	/// </summary>
	public static class SystemXmlHelper
	{
		/// <summary>
		///     File di configurazione (System.xml) da cui caricare le periferiche da monitorare
		/// </summary>
		public static string ConfigurationSystemFile { get; private set; }

		/// <summary>
		///     Recupera la lista dei dispositivi da monitorare da file di configurazione System.xml
		/// </summary>
		/// <param name="configurationSystemFile">File di configurazione (System.xml) da cui caricare le periferiche da monitorare</param>
		/// <returns>Lista di DeviceObject da monitorare</returns>
		public static ReadOnlyCollection<DeviceObject> GetMonitorDeviceList(string configurationSystemFile)
		{
			ConfigurationSystemFile = configurationSystemFile;

			ReadOnlyCollection<DeviceObject> devices;

			try
			{
				devices = LoadDevicesFromSystemXml();
			}
			catch (ConfigurationSystemException ex)
			{
				FileUtility.AppendStringToFileWithLoggingLevel(LoggingLevel.Error,
					"Errore nel caricamento della lista dei dispositivi. " + ex.Message);
				return null;
			}

			return devices;
		}

		/// <summary>
		///     Carica il file di configurazione System.xml con la relativa topologia e dispositivi
		/// </summary>
		/// <returns>La lista dei dispositivi caricati</returns>
		private static ReadOnlyCollection<DeviceObject> LoadDevicesFromSystemXml()
		{
			XmlNode root = GetDefinitionXmlRootElement();

			List<TopographyLocation> locations = LoadTopographyLocations(root);
			Dictionary<int, SystemPort> ports = LoadPorts(root);

			bool existsAtLeastOneDevice = false;

			List<DeviceObject> devices = new List<DeviceObject>();

			foreach (XmlNode rootElement in root.ChildNodes)
			{
				if (rootElement.Name.Equals("system", StringComparison.OrdinalIgnoreCase))
				{
					foreach (XmlNode server in rootElement.ChildNodes)
					{
						if (server.Name.Equals("server", StringComparison.OrdinalIgnoreCase))
						{
							foreach (XmlNode region in server.ChildNodes)
							{
								if (region.Name.Equals("region", StringComparison.OrdinalIgnoreCase))
								{
									foreach (XmlNode zone in region.ChildNodes)
									{
										if (zone.Name.Equals("zone", StringComparison.OrdinalIgnoreCase))
										{
											foreach (XmlNode node in zone.ChildNodes)
											{
												if (node.Name.Equals("node", StringComparison.OrdinalIgnoreCase))
												{
													foreach (XmlNode device in node.ChildNodes)
													{
														if (device.Name.Equals("device", StringComparison.OrdinalIgnoreCase))
														{
															#region supervisorId

															// Il default è 0, ossia il Supervisor originale
															int supervisorId;

															if (device.Attributes["supervisor_id"] == null)
															{
																// In mancanza dell'attributo, il default è 0, ossia il Supervisor originale
																supervisorId = 0;
															}
															else
															{
																if (!int.TryParse(device.Attributes["supervisor_id"].Value, out supervisorId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'supervisor_id' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["supervisor_id"].Value));
																}
															}

															#endregion

															// Processiamo solo i device che riguardano la gestione di questo Supervisor Snmp
															if (supervisorId == Settings.Default.SnmpSupervisorID)
															{
																#region Verifiche obbligatorietà su nodo Device

																if ((device.Attributes["DevID"] == null) || (device.Attributes["DevID"].Value.Trim().Length == 0) ||
																    (device.Attributes["name"] == null) || (device.Attributes["name"].Value.Trim().Length == 0) ||
																    (device.Attributes["station"] == null) || (device.Attributes["station"].Value.Trim().Length == 0) ||
																    (device.Attributes["building"] == null) || (device.Attributes["building"].Value.Trim().Length == 0) ||
																    (device.Attributes["location"] == null) || (device.Attributes["location"].Value.Trim().Length == 0) ||
																    (device.Attributes["type"] == null) || (device.Attributes["type"].Value.Trim().Length == 0) ||
																    (device.Attributes["addr"] == null) || (device.Attributes["addr"].Value.Trim().Length == 0) ||
																    (device.Attributes["SN"] == null) || (device.Attributes["SN"].Value.Trim().Length == 0) ||
																    (device.Attributes["profile"] == null) || (device.Attributes["profile"].Value.Trim().Length == 0) ||
																    (device.Attributes["active"] == null) || (device.Attributes["active"].Value.Trim().Length == 0) ||
																    (device.Attributes["scheduled"] == null) ||
																    (device.Attributes["scheduled"].Value.Trim().Length == 0) || (device.Attributes["position"] == null) ||
																    (device.Attributes["position"].Value.Trim().Length == 0) || (device.Attributes["port"] == null) ||
																    (device.Attributes["port"].Value.Trim().Length == 0))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"Impossibile trovare l'attributo 'DevID', 'name', 'station', 'building', 'location', 'type', 'addr', 'SN', 'port', 'profile', 'active', 'scheduled' o 'position' sul nodo 'device', oppure almeno un attributo nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
																		ConfigurationSystemFile, device.OuterXml));
																}

																#endregion

																#region Verifiche obbligatorietà su nodo Node

																if ((node.Attributes["NodID"] == null) || (node.Attributes["NodID"].Value.Trim().Length == 0) ||
																    (node.Attributes["name"] == null) || (node.Attributes["name"].Value.Trim().Length == 0))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"Impossibile trovare l'attributo 'NodID' o 'name' sul nodo 'node', oppure almeno un attributo nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
																		ConfigurationSystemFile, node.OuterXml));
																}

																#endregion

																#region Verifiche obbligatorietà su nodo Zone

																if ((zone.Attributes["ZonID"] == null) || (zone.Attributes["ZonID"].Value.Trim().Length == 0) ||
																    (zone.Attributes["name"] == null) || (zone.Attributes["name"].Value.Trim().Length == 0))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"Impossibile trovare l'attributo 'ZonID' o 'name' sul nodo 'zone', oppure almeno un attributo nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
																		ConfigurationSystemFile, zone.OuterXml));
																}

																#endregion

																#region Verifiche obbligatorietà su nodo Region

																if ((region.Attributes["RegID"] == null) || (region.Attributes["RegID"].Value.Trim().Length == 0) ||
																    (region.Attributes["name"] == null) || (region.Attributes["name"].Value.Trim().Length == 0))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"Impossibile trovare l'attributo 'RegID' o 'name' sul nodo 'region', oppure almeno un attributo nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
																		ConfigurationSystemFile, region.OuterXml));
																}

																#endregion

																#region Verifiche obbligatorietà su nodo Server

																if ((server.Attributes["SrvID"] == null) || (server.Attributes["SrvID"].Value.Trim().Length == 0) ||
																    (server.Attributes["host"] == null) || (server.Attributes["host"].Value.Trim().Length == 0) ||
																    (server.Attributes["name"] == null) || (server.Attributes["name"].Value.Trim().Length == 0))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"Impossibile trovare l'attributo 'SrvID', 'host' o 'name' sul nodo 'server', oppure almeno un attributo nullo nel file {0}. Il contenuto del nodo è:\r\n{1}",
																		ConfigurationSystemFile, server.OuterXml));
																}

																#endregion

																#region originalDeviceId

																ushort originalDeviceId;

																if (!ushort.TryParse(device.Attributes["DevID"].Value, out originalDeviceId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'DevID' del nodo 'device' nel file {0}, non contiene un valore numerico intero positivo. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["DevID"].Value));
																}

																#endregion

																string deviceName = device.Attributes["name"].Value;

																#region stationId

																int stationId;

																if (!int.TryParse(device.Attributes["station"].Value, out stationId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'station' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["station"].Value));
																}

																#endregion

																#region buildingId

																int buildingId;

																if (!int.TryParse(device.Attributes["building"].Value, out buildingId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'building' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["building"].Value));
																}

																#endregion

																#region locationId

																int locationId;

																if (!int.TryParse(device.Attributes["location"].Value, out locationId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'location' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["location"].Value));
																}

																#endregion

																string deviceType = device.Attributes["type"].Value;

																#region deviceAddr

																IPAddress deviceAddr;

																if (!IPAddress.TryParse(device.Attributes["addr"].Value, out deviceAddr))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'addr' del nodo 'device' nel file {0}, non contiene un indirizzo IP valido. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["addr"].Value));
																}

																#endregion

																string serialNumber = device.Attributes["SN"].Value;

																#region portId

																int portId;

																if (!int.TryParse(device.Attributes["port"].Value, out portId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'port' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["port"].Value));
																}

																#endregion

																#region profileId

																int profileId;

																if (!int.TryParse(device.Attributes["profile"].Value, out profileId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'profile' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["profile"].Value));
																}

																#endregion

																#region active

																bool activeValue;

																if (!bool.TryParse(device.Attributes["active"].Value.ToLowerInvariant(), out activeValue))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'active' del nodo 'device' nel file {0}, non contiene un valore booleano valido (true/false). Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["active"].Value));
																}

																byte active = (byte) (activeValue ? 1 : 0);

																#endregion

																#region scheduled

																bool scheduledValue;

																if (!bool.TryParse(device.Attributes["scheduled"].Value.ToLowerInvariant(), out scheduledValue))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'scheduled' del nodo 'device' nel file {0}, non contiene un valore booleano valido (true/false). Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["scheduled"].Value));
																}

																byte scheduled = (byte) (scheduledValue ? 1 : 0);

																#endregion

																#region snmp_community

																string snmpCommunity;

																if (device.Attributes["snmp_community"] == null)
																{
																	// Nel caso non sia presente l'attributo nel file System.xml, ripieghiamo sul
																	// default da configurazione
																	snmpCommunity = Settings.Default.SnmpDefaultCommunity;
																}
																else
																{
																	snmpCommunity = device.Attributes["snmp_community"].Value.Trim();
																}

																#endregion

																#region snmp_port

																int snmpPort;

																if (device.Attributes["snmp_port"] == null)
																{
																	// Nel caso non sia presente l'attributo nel file System.xml, ripieghiamo sul
																	// default da configurazione
																	snmpPort = Settings.Default.SnmpDefaultPort;
																}
																else
																{
																	if (!int.TryParse(device.Attributes["snmp_port"].Value, out snmpPort))
																	{
																		throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																			"L'attributo 'snmp_port' del nodo 'device' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																			ConfigurationSystemFile, device.Attributes["snmp_port"].Value));
																	}
																}

																#endregion

																#region rackPosition (column / row)

																int rackPositionColumn;
																int rackPositionRow;

																string rackPositionString = device.Attributes["position"].Value.Trim();
																string[] rackPositions = rackPositionString.Split(',');

																if (rackPositions.Length == 2)
																{
																	if (!int.TryParse(rackPositions[0], out rackPositionColumn))
																	{
																		throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																			"L'attributo 'position' del nodo 'device' nel file {0}, non contiene due valori numerici interi separati da virgola. Valore contenuto: {1}",
																			ConfigurationSystemFile, device.Attributes["position"].Value));
																	}

																	if (!int.TryParse(rackPositions[1], out rackPositionRow))
																	{
																		throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																			"L'attributo 'position' del nodo 'device' nel file {0}, non contiene due valori numerici interi separati da virgola. Valore contenuto: {1}",
																			ConfigurationSystemFile, device.Attributes["position"].Value));
																	}
																}
																else
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'position' del nodo 'device' nel file {0}, non contiene due valori numerici interi separati da virgola. Valore contenuto: {1}",
																		ConfigurationSystemFile, device.Attributes["position"].Value));
																}

																#endregion

																#region originalRegionId

																ushort originalRegionId;

																if (!ushort.TryParse(region.Attributes["RegID"].Value, out originalRegionId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'RegID' del nodo 'region' nel file {0}, non contiene un valore numerico intero positivo. Valore contenuto: {1}",
																		ConfigurationSystemFile, region.Attributes["RegID"].Value));
																}

																#endregion

																string regionName = region.Attributes["name"].Value;

																#region originalZoneId

																ushort originalZoneId;

																if (!ushort.TryParse(zone.Attributes["ZonID"].Value, out originalZoneId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'ZonID' del nodo 'region' nel file {0}, non contiene un valore numerico intero positivo. Valore contenuto: {1}",
																		ConfigurationSystemFile, zone.Attributes["ZonID"].Value));
																}

																#endregion

																string zoneName = zone.Attributes["name"].Value;

																#region originalNodeId

																ushort originalNodeId;

																if (!ushort.TryParse(node.Attributes["NodID"].Value, out originalNodeId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'NodID' del nodo 'node' nel file {0}, non contiene un valore numerico intero positivo. Valore contenuto: {1}",
																		ConfigurationSystemFile, node.Attributes["NodID"].Value));
																}

																#endregion

																string nodeName = node.Attributes["name"].Value;

																#region serverId

																int serverId;

																if (!int.TryParse(server.Attributes["SrvID"].Value, out serverId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"L'attributo 'SrvID' del nodo 'server' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
																		ConfigurationSystemFile, server.Attributes["SrvID"].Value));
																}

																#endregion

																string serverHost = server.Attributes["host"].Value;
																string serverName = server.Attributes["name"].Value;

																#region MonitoringDeviceId

																// Il default è null, ossia non indicato
																string monitoringDeviceId;

																if (device.Attributes["monitoring_device_id"] == null)
																{
																	// In mancanza dell'attributo, il default è null, non indicato, gestione classica da STLC
																	monitoringDeviceId = null;
																}
																else
																{
																	monitoringDeviceId = device.Attributes["monitoring_device_id"].Value.Trim();

																	if (string.IsNullOrEmpty(monitoringDeviceId))
																	{
																		// Se l'attributo è vuoto o contiene solo spazi, usiamo il default a null, non indicato, gestione classica da STLC
																		monitoringDeviceId = null;
																	}

																	if ((monitoringDeviceId ?? string.Empty).Length > 128)
																	{
																		throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																			"L'attributo 'monitoring_device_id' del nodo 'device' nel file {0}, contiene un valore stringa di lunghezza superiore al massimo consentito. Valore contenuto: {1}",
																			ConfigurationSystemFile, monitoringDeviceId));
																	}
																}

																#endregion

																#region FakeLocalData

																bool fakeLocalData;

																if (device.Attributes["fake_local_data"] != null)
																{
																	if (!bool.TryParse(device.Attributes["fake_local_data"].Value, out fakeLocalData))
																	{
																		throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																			"L'attributo 'fake_local_data' del nodo 'device' nel file {0}, non contiene un valore booleano valido (true/false). Valore contenuto: {1}",
																			ConfigurationSystemFile, device.Attributes["fake_local_data"].Value));
																	}
																}
																else
																{
																	fakeLocalData = false;
																}

																#endregion

																TopographyLocation location = null;

																foreach (TopographyLocation topography in locations)
																{
																	location = topography.GetTopography(stationId, buildingId, locationId);
																	if (location != null)
																	{
																		break;
																	}
																}

																if (!ports.ContainsKey(portId))
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"Impossibile trovare una porta con l'Id associato alla periferica corrente nella lista porte del file {0}. Dispositivo corrente: {1}",
																		ConfigurationSystemFile, device.OuterXml));
																}

																SystemPort devicePort = ports[portId];

																if (location != null)
																{
																	devices.Add(new DeviceObject(fakeLocalData,
																		new DeviceIdentifier(originalDeviceId, originalNodeId, originalZoneId, originalRegionId).DeviceId,
																		deviceName, deviceType, deviceAddr, new RegionIdentifier(originalRegionId).RegionId, regionName,
																		new ZoneIdentifier(originalZoneId, originalRegionId).ZoneId, zoneName,
																		new NodeIdentifier(originalNodeId, originalZoneId, originalRegionId).NodeId, nodeName, serverId,
																		serverHost, serverName, serialNumber, devicePort, profileId, active, scheduled, rackPositionColumn,
																		rackPositionRow, location, snmpCommunity, snmpPort, monitoringDeviceId));

																	existsAtLeastOneDevice = true;
																}
																else
																{
																	throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
																		"Impossibile trovare una stazione/fabbricato/armadio che siano associate alla periferica corrente nella topologia del file {0}. Dispositivo corrente: {1}",
																		ConfigurationSystemFile, device.OuterXml));
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					break;
				}
			}

			if (!existsAtLeastOneDevice)
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Impossibile trovare almeno una periferica configurata nel file {0}", ConfigurationSystemFile));
			}

			return devices.AsReadOnly();
		}

		#region Metodi privati

		private static List<TopographyLocation> LoadTopographyLocations(XmlNode root)
		{
			List<TopographyLocation> locations = new List<TopographyLocation>();

			bool existsAtLeastOneLocation = false;

			// Normalmente il nodo della topografia è presente prima di quello con i sistemi
			// Per sicurezza lo cerchiamo singolarmente, in modo che la topografia sia popolata quando si elaborano i sistemi
			if (root != null)
			{
				foreach (XmlNode rootElement in root.ChildNodes)
				{
					#region Elaborazione nodo Topography

					if (rootElement.Name.Equals("topography", StringComparison.OrdinalIgnoreCase))
					{
						foreach (XmlNode station in rootElement.ChildNodes)
						{
							#region Elaborazione nodo Station

							if (station.Name.Equals("station", StringComparison.OrdinalIgnoreCase))
							{
								if ((station.Attributes == null) || (station.Attributes["id"] == null) ||
								    (station.Attributes["id"].Value.Trim().Length == 0) || (station.Attributes["name"] == null) ||
								    (station.Attributes["name"].Value.Trim().Length == 0))
								{
									throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
										"Impossibile trovare l'attributo 'id' o 'name' sul nodo 'station', oppure il nome della stazione o l'id sono nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
										ConfigurationSystemFile, station.OuterXml));
								}

								#region StationID

								int stationID;

								if (!int.TryParse(station.Attributes["id"].Value, out stationID))
								{
									throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
										"L'attributo 'id' del nodo 'station' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
										ConfigurationSystemFile, station.Attributes["id"].Value));
								}

								#endregion

								string stationName = station.Attributes["name"].Value;

								foreach (XmlNode building in station.ChildNodes)
								{
									#region Elaborazione nodo Building

									if (building.Name.Equals("building", StringComparison.OrdinalIgnoreCase))
									{
										if ((building.Attributes == null) || (building.Attributes["id"] == null) ||
										    (building.Attributes["id"].Value.Trim().Length == 0) || (building.Attributes["name"] == null) ||
										    (building.Attributes["name"].Value.Trim().Length == 0))
										{
											throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
												"Impossibile trovare l'attributo 'id' o 'name' sul nodo 'building', oppure il nome del fabbricato o l'id sono nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
												ConfigurationSystemFile, building.OuterXml));
										}

										#region BuildingID

										int buildingID;

										if (!int.TryParse(building.Attributes["id"].Value, out buildingID))
										{
											throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
												"L'attributo 'id' del nodo 'building' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
												ConfigurationSystemFile, building.Attributes["id"].Value));
										}

										#endregion

										string buildingName = building.Attributes["name"].Value;

										#region BuildingNotes

										string buildingNotes = string.Empty;
										if (building.Attributes["note"] != null)
										{
											buildingNotes = building.Attributes["note"].Value;
										}

										#endregion

										foreach (XmlNode location in building.ChildNodes)
										{
											#region Elaborazione nodo location

											if (location.Name.Equals("location", StringComparison.OrdinalIgnoreCase))
											{
												if ((location.Attributes == null) || (location.Attributes["id"] == null) ||
												    (location.Attributes["id"].Value.Trim().Length == 0) || (location.Attributes["name"] == null) ||
												    (location.Attributes["name"].Value.Trim().Length == 0) || (location.Attributes["type"] == null) ||
												    (location.Attributes["type"].Value.Trim().Length == 0))
												{
													throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
														"Impossibile trovare l'attributo 'id', 'name' o 'type' sul nodo 'location', oppure il nome della locazione, l'id o il tipo sono nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
														ConfigurationSystemFile, location.OuterXml));
												}

												#region LocationID

												int locationID;

												if (!int.TryParse(location.Attributes["id"].Value, out locationID))
												{
													throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
														"L'attributo 'id' del nodo 'location' nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
														ConfigurationSystemFile, location.Attributes["id"].Value));
												}

												#endregion

												string locationName = location.Attributes["name"].Value;
												string locationType = location.Attributes["type"].Value;

												#region LocationNotes

												string locationNotes = string.Empty;
												if (location.Attributes["note"] != null)
												{
													locationNotes = location.Attributes["note"].Value;
												}

												#endregion

												locations.Add(new TopographyLocation(stationID, stationName, buildingID, buildingName, buildingNotes,
													locationID, locationName, locationType, locationNotes));

												existsAtLeastOneLocation = true;
											}

											#endregion
										}
									}

									#endregion
								}
							}

							#endregion
						}
						break;
					}

					#endregion
				}
			}
			else
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile));
			}

			if (!existsAtLeastOneLocation)
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Impossibile trovare almeno una stazione/fabbricato/supporto nel file {0}.", ConfigurationSystemFile));
			}

			return locations;
		}

		private static Dictionary<int, SystemPort> LoadPorts(XmlNode root)
		{
			Dictionary<int, SystemPort> ports = new Dictionary<int, SystemPort>();

			bool existsAtLeastOnePort = false;

			if (root != null)
			{
				foreach (XmlNode rootElement in root.ChildNodes)
				{
					#region Elaborazione nodo Topography

					if (rootElement.Name.Equals("port", StringComparison.OrdinalIgnoreCase))
					{
						foreach (XmlNode item in rootElement.ChildNodes)
						{
							#region Elaborazione nodo Item

							if (item.Name.Equals("item", StringComparison.OrdinalIgnoreCase))
							{
								if ((item.Attributes == null) || (item.Attributes["id"] == null) ||
								    (item.Attributes["id"].Value.Trim().Length == 0) || (item.Attributes["name"] == null) ||
								    (item.Attributes["name"].Value.Trim().Length == 0) || (item.Attributes["type"] == null) ||
								    (item.Attributes["type"].Value.Trim().Length == 0))
								{
									throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
										"Impossibile trovare l'attributo 'id' o 'name' sul nodo 'item' del ramo 'ports', oppure il nome della porta o l'id sono nulli nel file {0}. Il contenuto del nodo è:\r\n{1}",
										ConfigurationSystemFile, item.OuterXml));
								}

								#region ID

								int portId;

								if (!int.TryParse(item.Attributes["id"].Value, out portId))
								{
									throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
										"L'attributo 'id' del nodo 'item' del ramo 'ports', nel file {0}, non contiene un valore numerico intero. Valore contenuto: {1}",
										ConfigurationSystemFile, item.Attributes["id"].Value));
								}

								#endregion

								string portName = item.Attributes["name"].Value;
								string portType = item.Attributes["type"].Value;

								#region com

								string comPort = string.Empty;
								if (item.Attributes["com"] != null)
								{
									comPort = item.Attributes["com"].Value;
								}

								#endregion

								#region baud

								string baud = string.Empty;
								if (item.Attributes["baud"] != null)
								{
									baud = item.Attributes["baud"].Value;
								}

								#endregion

								#region echo

								string echo = string.Empty;
								if (item.Attributes["echo"] != null)
								{
									echo = item.Attributes["echo"].Value;
								}

								#endregion

								#region data

								string data = string.Empty;
								if (item.Attributes["data"] != null)
								{
									data = item.Attributes["data"].Value;
								}

								#endregion

								#region stop

								string stop = string.Empty;
								if (item.Attributes["stop"] != null)
								{
									stop = item.Attributes["stop"].Value;
								}

								#endregion

								#region parity

								string parity = string.Empty;
								if (item.Attributes["parity"] != null)
								{
									parity = item.Attributes["parity"].Value;
								}

								#endregion

								#region timeout

								string timeout = string.Empty;
								if (item.Attributes["timeout"] != null)
								{
									timeout = item.Attributes["timeout"].Value;
								}

								#endregion

								#region persistent

								string persistent = string.Empty;
								if (item.Attributes["persistent"] != null)
								{
									persistent = item.Attributes["persistent"].Value;
								}

								#endregion

								#region ip

								string ip = string.Empty;
								if (item.Attributes["ip"] != null)
								{
									ip = item.Attributes["ip"].Value;
								}

								#endregion

								#region port

								string tcpPort = string.Empty;
								if (item.Attributes["port"] != null)
								{
									tcpPort = item.Attributes["port"].Value;
								}

								#endregion

								#region enabled

								bool enabled = true;
								if (item.Attributes["enabled"] != null)
								{
									string enableString = item.Attributes["enabled"].Value.Trim().ToLowerInvariant();
									if (!bool.TryParse(enableString, out enabled))
									{
										throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
											"L'attributo 'enabled' del nodo 'item', nel ramo 'ports', nel file {0}, non contiene un valore booleano (true/false). Valore contenuto: '{1}'",
											ConfigurationSystemFile, item.Attributes["enabled"].Value.Trim()));
									}
								}

								#endregion

								// ReSharper disable NotAccessedVariable
								string parameters = string.Empty;
								// ReSharper restore NotAccessedVariable

								if (string.IsNullOrEmpty(ip))
								{
									// Porta seriale
									// ReSharper disable RedundantAssignment
									parameters = string.Format(CultureInfo.InvariantCulture,
										"Persistent: {0}, COM: {1}, Baud: {2}, Echo: {3}, Data: {4}, Stop: {5}, Parity: {6}, Timeout: {7}", persistent,
										comPort, baud, echo, data, stop, parity, timeout);
									// ReSharper restore RedundantAssignment
								}
								else
								{
									// Porta TCP/IP
									// ReSharper disable RedundantAssignment
									parameters = string.Format(CultureInfo.InvariantCulture, "Persistent: {0}, IP {1}, Port: {2}, Timeout: {3}",
										persistent, ip, tcpPort, timeout);
									// ReSharper restore RedundantAssignment
								}

								if (!ports.ContainsKey(portId))
								{
									ports.Add(portId, new SystemPort(portId, portName, portType, enabled));
								}

								existsAtLeastOnePort = true;
							}

							#endregion
						}
						break;
					}

					#endregion
				}
			}
			else
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile));
			}

			if (!existsAtLeastOnePort)
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Impossibile trovare almeno una porta di comunicazione nel file {0}.", ConfigurationSystemFile));
			}

			return ports;
		}

		private static XmlNode GetDefinitionXmlRootElement()
		{
			if (String.IsNullOrEmpty(ConfigurationSystemFile))
			{
				throw new ConfigurationSystemException(
					"Nome del file di configurazione con la lista dei dispositivi da monitorare non definito");
			}

			if (!FileUtility.CheckFileCanRead(ConfigurationSystemFile))
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Impossibile leggere il file di configurazione con la lista dei dispositivi: {0}", ConfigurationSystemFile));
			}

			XmlDocument doc = new XmlDocument();
			XmlNode root;

			try
			{
				doc.Load(ConfigurationSystemFile);
				root = doc.DocumentElement;
			}
			catch (XmlException ex)
			{
				throw new ConfigurationSystemException(
					string.Format(CultureInfo.InvariantCulture,
						"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
			}
			catch (ArgumentException ex)
			{
				throw new ConfigurationSystemException(
					string.Format(CultureInfo.InvariantCulture,
						"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
			}
			catch (IOException ex)
			{
				throw new ConfigurationSystemException(
					string.Format(CultureInfo.InvariantCulture,
						"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
			}
			catch (UnauthorizedAccessException ex)
			{
				throw new ConfigurationSystemException(
					string.Format(CultureInfo.InvariantCulture,
						"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
			}
			catch (NotSupportedException ex)
			{
				throw new ConfigurationSystemException(
					string.Format(CultureInfo.InvariantCulture,
						"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
			}
			catch (SecurityException ex)
			{
				throw new ConfigurationSystemException(
					string.Format(CultureInfo.InvariantCulture,
						"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile), ex);
			}

			if (root == null)
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Configurazione non valida nel file {0}, con la definizione XML della periferica", ConfigurationSystemFile));
			}

			if (!root.Name.Equals("telefin", StringComparison.OrdinalIgnoreCase))
			{
				throw new ConfigurationSystemException(string.Format(CultureInfo.InvariantCulture,
					"Impossibile trovare l'elemento 'telefin' come nodo radice del file di configurazione {0}.",
					ConfigurationSystemFile));
			}

			return root;
		}

		#endregion
	}
}
﻿using System;
using System.Security;
using GrisSuite.SnmpSupervisorLibrary.Properties;
using Microsoft.Win32;

namespace GrisSuite.SnmpSupervisorLibrary
{
	/// <summary>
	///     Classe helper per il caricamento delle configurazioni da Registry o file xml dei settings
	/// </summary>
	public class ConfigHelper
	{
		private static readonly ConfigHelper defaultInstance = new ConfigHelper();
        private String defaultDebugLogFolder = ".";

        private ConfigHelper() {
            defaultDebugLogFolder = AppDomain.CurrentDomain.BaseDirectory;
        }

		/// <summary>
		///     Istanza della classe
		/// </summary>
		public static ConfigHelper Default
		{
			get { return defaultInstance; }
		}

        public string GlobalRegisterKeyPath
        {
            get {
                return Settings.Default.GlobalRegisterKeysPath;
            }
        }

		public string DebugLogFolder
		{
			get
			{
                // verifico se esiste il parametro nel file di configurazione
                string debugLogFolder = Settings.Default.DebugLogFolder;

                if (string.IsNullOrEmpty(debugLogFolder))
                {
                    // provo a recuperare il dato da chiave di registro di windows
                    debugLogFolder = this.GetRegistryKey(GlobalRegisterKeyPath, Settings.Default.DebugLogFolderRegisterKeyName);
                    if (string.IsNullOrEmpty(debugLogFolder))
                    {
                        // dato che riesco ad ottenere il valore
                        debugLogFolder = defaultDebugLogFolder;
                    }
                }

                return debugLogFolder;
			}
		}

		public string DefinitionFolder
		{
			get
			{
				string definitionFolder = this.GetRegistryKey(@"SYSTEM\CurrentControlSet\Services\STLCManagerService\Paths",
					"PathSNMPDefinitionsFolder");
				if (string.IsNullOrEmpty(definitionFolder))
				{
					return Settings.Default.DefinitionFolder;
				}

				return definitionFolder;
			}
		}

		public string SpecializedDefinitionFolder
		{
			get
			{
				string specializedDefinitionFolder = this.GetRegistryKey(@"SYSTEM\CurrentControlSet\Services\STLCManagerService\Paths",
					"PathSNMPSpecializedDefinitionsFolder");
				if (string.IsNullOrEmpty(specializedDefinitionFolder))
				{
					return Settings.Default.SpecializedDefinitionFolder;
				}

				return specializedDefinitionFolder;
			}
		}

		private string GetRegistryKey(string KeyName, string ValueName)
		{
			string keyValue = null;
			RegistryKey key = null;

			try
			{
				key = Registry.LocalMachine.OpenSubKey(KeyName);
				if (key != null)
				{
					keyValue = key.GetValue(ValueName) as String;
				}
			}
			catch (SecurityException)
			{
			}
			finally
			{
				if (key != null)
				{
					key.Close();
				}
			}

			return keyValue;
		}
	}
}
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project. 
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc. 
//
// To add a suppression to this file, right-click the message in the 
// Error List, point to "Suppress Message(s)", and click 
// "In Project Suppression File". 
// You do not need to add suppressions to this file manually. 

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "GrisSuite.SnmpSupervisorLibrary")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "EndPoint", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.DeviceObject.#EndPoint")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Scope = "member", Target = "GrisSuite.SnmpSupervisorLibrary.FileUtility.#.cctor()")
]
[assembly: SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix", Scope = "type", Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStream")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Mib", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStream.#.ctor(System.Int32,System.String,System.Boolean)")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Db", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#DbStreamFieldValueDescriptions")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#FieldCorrelationTableOid")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#FieldOid")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Mib", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStream.#IsMib2")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#.ctor(System.Int32,System.Int32,System.String,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,System.Nullable`1<System.Int32>,System.String,System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.Text.RegularExpressions.Regex)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#GetValueFromTableWithColumnFilter(Lextm.SharpSnmpLib.Variable[,],System.Int32,System.Text.RegularExpressions.Regex,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#GetValueFromTableAtRow(Lextm.SharpSnmpLib.Variable[,],System.Int32,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#GetValueFromCorrelationTableWithCorrelationValue(Lextm.SharpSnmpLib.Variable[,],Lextm.SharpSnmpLib.Variable[,],System.Int32,System.Int32,System.Text.RegularExpressions.Regex,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#GetValueFromCorrelationTableWithCorrelationValue(Lextm.SharpSnmpLib.Variable[,],Lextm.SharpSnmpLib.Variable[,],System.Int32,System.Int32,System.Text.RegularExpressions.Regex,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#GetValueFromCorrelationTableWithColumnFilter(Lextm.SharpSnmpLib.Variable[,],Lextm.SharpSnmpLib.Variable[,],System.Int32,System.Text.RegularExpressions.Regex,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#GetValueFromCorrelationTableWithColumnFilter(Lextm.SharpSnmpLib.Variable[,],Lextm.SharpSnmpLib.Variable[,],System.Int32,System.Text.RegularExpressions.Regex,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#EvaluateValueExpressionFromTable(Lextm.SharpSnmpLib.Variable[,],System.Int32,System.String,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpDeviceBase.#SeverityLevel")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#FieldTableOid")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.FakeDatabaseUtility.#GetStationBuildingRackPortData()")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DeviceIdentifier.#.ctor(System.Int64)")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamFieldValueDescription.#.ctor(GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField,System.Boolean,System.String,GrisSuite.SnmpSupervisorLibrary.Database.Severity,System.String,System.Nullable`1<System.Int64>,GrisSuite.SnmpSupervisorLibrary.Database.DBStreamFieldValueRange)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly", Scope = "member", Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#RawData")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#ValueDescription")]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#RawData")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.NodeIdentifier.#.ctor(System.Int64)")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.RegionIdentifier.#.ctor(System.Int64)")]
[assembly: SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Scope = "type", Target = "GrisSuite.SnmpSupervisorLibrary.Database.Severity")]
[assembly:
    SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.FakeDataRetriever.#GetTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.FakeDataRetriever.#GetTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.FakeDataRetriever.#GetTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.FakeDataRetriever.#GetTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.ZoneIdentifier.#.ctor(System.Int64)")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.SystemXmlHelper.#GetMonitorDeviceList()")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.SystemXmlHelper.#LoadDevicesFromSystemXml()")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1809:AvoidExcessiveLocals", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.SystemXmlHelper.#LoadDevicesFromSystemXml()")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.SystemXmlHelper.#LoadDevicesFromSystemXml()")]
[assembly:
    SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.SystemXmlHelper.#LoadDevicesFromSystemXml()")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Icmp", Scope = "type",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.IcmpDataRetriever")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "EndPoint", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.IcmpDataRetriever.#GetPingResponse(System.Net.IPEndPoint)")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "EndPoint", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.IDevice.#DeviceIPEndPoint")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Icmp", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.IDevice.#IsAliveIcmp")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.LoadingRule.#ProbingCategoryOid")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.LoadingRuleExactMatch.#ProbingManufacturerOid")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.LoadingRuleExactMatch.#ProbingCategoryOid")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.LoadingRuleExactMatch.#.ctor(Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,System.Collections.ObjectModel.ReadOnlyCollection`1<GrisSuite.SnmpSupervisorLibrary.Devices.ManufacturerDefinition>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetAliveSnmp(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,System.Collections.Generic.IList`1<Lextm.SharpSnmpLib.Variable>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetPingResponse(System.Net.IPEndPoint)")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpData(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,System.Collections.Generic.IList`1<Lextm.SharpSnmpLib.Variable>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "EndPoint", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#.ctor(System.String,GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory,System.Net.IPEndPoint)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CATEGORY", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_UNDEFINED")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "DEVICE", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_UNDEFINED")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_UNDEFINED")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNDEFINED", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_UNDEFINED")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#LoadConfiguration(System.Collections.Generic.List`1<GrisSuite.SnmpSupervisorLibrary.Database.DBStream>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#LoadConfiguration(System.Collections.Generic.List`1<GrisSuite.SnmpSupervisorLibrary.Database.DBStream>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#PreLoadConfiguration()")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpDataRetriever.#GetTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpDataRetriever.#GetTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#EvaluateValueExpressionFromTable(Lextm.SharpSnmpLib.Variable[,],System.Int32,System.String,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.LoadingRule.#.ctor(Lextm.SharpSnmpLib.ObjectIdentifier,System.String,System.String)")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#LoadConfiguration(System.Collections.ObjectModel.Collection`1<GrisSuite.SnmpSupervisorLibrary.Database.DBStream>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#PreloadConfiguration()")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#LoadConfiguration(System.Collections.ObjectModel.Collection`1<GrisSuite.SnmpSupervisorLibrary.Database.DBStream>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#LoadConfiguration(System.Collections.ObjectModel.Collection`1<GrisSuite.SnmpSupervisorLibrary.Database.DBStream>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#PreloadConfiguration()")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#.ctor(System.Int32,System.Int32,System.String,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,System.Nullable`1<System.Int32>,System.String,System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.Text.RegularExpressions.Regex,GrisSuite.SnmpSupervisorLibrary.Database.FormatExpression)"
        )]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.FakeDataRetriever.#GetTable(Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1809:AvoidExcessiveLocals", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#LoadConfiguration(System.Collections.ObjectModel.Collection`1<GrisSuite.SnmpSupervisorLibrary.Database.DBStream>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetAliveSnmp(System.Boolean,Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,System.Collections.Generic.IList`1<Lextm.SharpSnmpLib.Variable>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetPingResponse(System.Boolean,System.Net.IPEndPoint)")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpData(System.Boolean,Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,System.Collections.Generic.IList`1<Lextm.SharpSnmpLib.Variable>)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpTable(System.Boolean,Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpTable(System.Boolean,Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory.#GetSnmpTable(System.Boolean,Lextm.SharpSnmpLib.VersionCode,System.Net.IPEndPoint,Lextm.SharpSnmpLib.OctetString,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "EndPoint", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#.ctor(System.Boolean,System.String,GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory,System.Net.IPEndPoint)"
        )]
[assembly:
    SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.Reflection.Assembly.LoadFile", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpUtility.#EvaluateValueExpressionFromTable(Lextm.SharpSnmpLib.Variable[,],System.Int32,System.String,System.String,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Ttl", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.IPStatus.#TtlReassemblyTimeExceeded")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Ttl", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.IPStatus.#TtlExpired")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Icmp", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.IPStatus.#IcmpError")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_NOT_AVAILABLE")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "NOT", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_NOT_AVAILABLE")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "DEVICE", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_NOT_AVAILABLE")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CATEGORY", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_NOT_AVAILABLE")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AVAILABLE", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#DEVICE_CATEGORY_NOT_AVAILABLE")]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.PingReply.#Buffer")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Ttl", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.PingOptions.#Ttl")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Dont", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.PingOptions.#DontFragment")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "ttl", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.PingOptions.#.ctor(System.Int32,System.Boolean)")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "dont", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.MonoPing.PingOptions.#.ctor(System.Int32,System.Boolean)")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#.ctor(System.Int32,System.Int32,System.String,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,System.Nullable`1<System.Int32>,System.String,System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.Text.RegularExpressions.Regex,GrisSuite.SnmpSupervisorLibrary.Database.FormatExpression,System.Nullable`1<System.Int32>,Lextm.SharpSnmpLib.ObjectIdentifier)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#ScalarTableCounterOid")]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.SnmpDeviceBase.#RetrieveData()")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#.ctor(System.Int32,System.Int32,System.String,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,System.Nullable`1<System.Int32>,System.String,System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.Text.RegularExpressions.Regex,GrisSuite.SnmpSupervisorLibrary.Database.FormatExpression,System.Nullable`1<System.Int32>,Lextm.SharpSnmpLib.ObjectIdentifier,System.Boolean)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#ParseRangeValue()")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.LoadingRuleExactMatch.#.ctor(Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,System.Collections.ObjectModel.ReadOnlyCollection`1<GrisSuite.SnmpSupervisorLibrary.Devices.ManufacturerDefinition>,GrisSuite.SnmpSupervisorLibrary.Devices.NoDynamicRuleMatchDeviceStatus)"
        )]
[assembly:
    SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#PreloadConfiguration()")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "EndPoint", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#.ctor(System.Boolean,System.String,GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory,System.Net.IPEndPoint,System.String)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oid", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Database.DBStreamField.#.ctor(System.Int32,System.Int32,System.String,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,Lextm.SharpSnmpLib.ObjectIdentifier,System.Nullable`1<System.Int32>,System.String,System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.Text.RegularExpressions.Regex,GrisSuite.SnmpSupervisorLibrary.Database.FormatExpression,System.Nullable`1<System.Int32>,Lextm.SharpSnmpLib.ObjectIdentifier,System.Boolean,GrisSuite.SnmpSupervisorLibrary.Devices.NoDataAvailableForcedState)"
        )]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Icmp", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceKind.#Icmp")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Icmp", Scope = "type",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.IcmpDeviceBase")]
[assembly:
    SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "EndPoint", Scope = "member",
        Target =
            "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceConfigurationHelper.#.ctor(System.Boolean,System.String,GrisSuite.SnmpSupervisorLibrary.Devices.MessengerFactory,System.Net.IPEndPoint,System.String,GrisSuite.SnmpSupervisorLibrary.Devices.DeviceKind)"
        )]
[assembly:
    SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member",
        Target = "GrisSuite.SnmpSupervisorLibrary.Devices.DeviceTypeFactory.#Create(GrisSuite.SnmpSupervisorLibrary.DeviceObject)")]
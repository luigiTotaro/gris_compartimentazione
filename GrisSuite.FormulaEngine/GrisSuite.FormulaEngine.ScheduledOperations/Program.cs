﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using GrisSuite.FormulaEngine.Common;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;
using NLog;

namespace GrisSuite.FormulaEngine.ScheduledOperations
{
    internal class Program
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        // Da implementare per logiche di nuovi tentativi di esecuzione
        private static readonly int retryNumber = 1;

        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("it-IT");

            _logger.Info("Avvio gestione code schedulate");

            try
            {
                var schedules = OperationSchedule.GetNotExecutedSchedules();

                foreach (var schedule in schedules)
                {
                    OperationScheduleExecResult executed = new OperationScheduleExecResult();

                    try
                    {
                        var executor = GetExecutor(schedule.Type);
                        _logger.Info("Avvio gestore coda tipo: {0}, Id: {1}, Creata il: {2}", schedule.Type.AssemblyName, schedule.Id,
                            schedule.Created);
                        executed = executor.Exec(schedule, _logger);
                        _logger.Info("Fine gestore coda tipo: {0}, Id: {1}", schedule.Type.AssemblyName, schedule.Id);
                    }
                    catch (Exception exc)
                    {
                        _logger.ErrorException(
                            string.Format("Errore nell'esecuzione asincrona dell'operazione schedulata (id {0}): {1}<br />ST: {2}", schedule.Id,
                                exc.Message, exc.StackTrace), exc);
                        continue;
                    }

                    // Al momento è gestita solo la logica Success = Ok, altrimenti fallito
                    switch (executed.Status)
                    {
                        case Common.OperationScheduleResultStatus.Unknown:
                            OperationSchedule.SetAborted(schedule.Id, retryNumber,
                                Enum.GetName(typeof(OperationScheduleResultStatus), executed.Status), executed.Code, executed.Message,
                                executed.SerializedParameters);
                            break;
                        case Common.OperationScheduleResultStatus.Success:
                            OperationSchedule.SetExecuted(schedule.Id, retryNumber,
                                Enum.GetName(typeof(OperationScheduleResultStatus), executed.Status), executed.Code, executed.Message,
                                executed.SerializedParameters);
                            break;
                        case Common.OperationScheduleResultStatus.Error:
                            OperationSchedule.SetAborted(schedule.Id, retryNumber,
                                Enum.GetName(typeof(OperationScheduleResultStatus), executed.Status), executed.Code, executed.Message,
                                executed.SerializedParameters);
                            break;
                        case Common.OperationScheduleResultStatus.ShouldRetry:
                            OperationSchedule.SetAborted(schedule.Id, retryNumber,
                                Enum.GetName(typeof(OperationScheduleResultStatus), executed.Status), executed.Code, executed.Message,
                                executed.SerializedParameters);
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                _logger.ErrorException(
                    string.Format("Errore nell'esecuzione asincrona delle operazioni schedulate: {0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
            }

            _logger.Info("Fine gestione code schedulate");
        }

        public static IOperationExecutor GetExecutor(IOperationScheduleType scheduleType)
        {
            var executor = new OperationExecutorFactory().GetExecutor((OperationScheduleType)scheduleType);
            if (executor == null)
            {
                throw new SchedulerAssemblyNotLoadedException(string.Format("Impossibile caricare l'assembly per lo scheduler di tipo '{0}'.",
                    scheduleType.Code));
            }

            return executor;
        }
    }
}
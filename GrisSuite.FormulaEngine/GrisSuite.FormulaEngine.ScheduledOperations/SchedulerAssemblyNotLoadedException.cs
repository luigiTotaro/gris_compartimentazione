﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.ScheduledOperations
{
	public class SchedulerAssemblyNotLoadedException : Exception
	{
		private readonly string _assemblyName;

		public SchedulerAssemblyNotLoadedException ()
        {
        }

		public SchedulerAssemblyNotLoadedException ( string message )
			: base(message)
        {
        }

		public SchedulerAssemblyNotLoadedException ( string message, Exception inner )
			: base(message, inner)
		{
		}

		public SchedulerAssemblyNotLoadedException ( string assemblyName, string message, Exception inner )
			: base(message, inner)
		{
			_assemblyName = assemblyName;
		}

		protected SchedulerAssemblyNotLoadedException ( SerializationInfo info, StreamingContext context )
			: base(info, context)
        {
        }

		public string AssemblyName
		{
			get { return _assemblyName; }
		}
	}
}

﻿
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.ScheduledOperations
{
	public interface IOperationExecutorFactory
	{
		IOperationExecutor GetExecutor ( OperationScheduleType schedulerType );
	}

	public class OperationExecutorFactory : IOperationExecutorFactory
	{
		public IOperationExecutor GetExecutor ( OperationScheduleType scheduleType )
		{
			if ( scheduleType != null )
			{
				try
				{
					string currentAssemblyDirectoryName = Path.GetFullPath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase.Substring("file://".Length + 1)));
					// il caricamento non viene eseguito se l'assembly è già stato caricato
					Assembly executorAssbly = Assembly.LoadFrom(string.Format(@"{0}\Operations\{1}.dll", currentAssemblyDirectoryName, scheduleType.AssemblyName));
					var exportedTypes = (from exportedType in executorAssbly.GetExportedTypes()
					                    where exportedType.Name.Contains(string.Format("{0}Executor", scheduleType.Code))
					                    select exportedType).ToList();

					if ( exportedTypes.Any() )
					{
						// si da per scontato che ci sia solo un match
						IOperationExecutor executor = Activator.CreateInstance(exportedTypes.First()) as IOperationExecutor;

						if ( executor != null )
						{
							return executor;
						}						
					}
				}
				catch ( Exception exc )
				{
					throw new SchedulerAssemblyNotLoadedException(scheduleType.AssemblyName, string.Format("Impossibile caricare l'assembly '{0}' per l'executor di tipo '{1}'.", scheduleType.AssemblyName, scheduleType.Code), exc);
				}
			}

			return null;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.ScheduledOperations
{
    public class OperationScheduler : IOperationScheduler
    {
        #region Implementation of IOperationScheduler

        /// <summary>
        ///     Schedula un operazione passando le informazioni di stato di cui l'operazione ha bisogno ed indicando se va tentata
        ///     anche un esecuzione sincrona.
        /// </summary>
        /// <param name="scheduleType">Tipo di schedulazione, serve per invocare l'executor corretto</param>
        /// <param name="state">Stato per l'operazione.</param>
        /// <param name="directExecution">Indica se lo scheduler deve tentare un'esecuzione sincrona prima di schedulare.</param>
        /// <param name="lastOperationOlderThanMinutes">Minuti che devono trascorrere dall'ultima operazione schedulata dello stesso tipo.</param>
        /// <param name="logger">Istanza logger</param>
        /// <param name="operationKeys">Chiavi da usare per poter identificare in modo univoco l'operazione</param>
        /// <param name="operationScheduleCheckType">Tipo di controllo per l'esistenza di una schedulazione analoga, all'interno della finestra temporale</param>
        /// <returns></returns>
        public ScheduleResult Schedule(string scheduleType,
            Dictionary<string, string> state,
            bool directExecution,
            int lastOperationOlderThanMinutes,
            NLog.Logger logger,
            string operationKeys,
            OperationScheduleCreateIfCheckType operationScheduleCheckType)
        {
            var scheduleTypeInstance = OperationScheduleType.GetByCode(scheduleType);

            if (scheduleTypeInstance == null)
            {
                throw new SchedulerAssemblyNotLoadedException(string.Format("Impossibile reperire la schedulazione di tipo '{0}' dal database.",
                    scheduleType));
            }

            return this.Schedule(state, directExecution, new OperationExecutorFactory(), scheduleTypeInstance, lastOperationOlderThanMinutes, logger, operationKeys, operationScheduleCheckType);
        }

        /// <summary>
        ///     Schedula un operazione passando le informazioni di stato di cui l'operazione ha bisogno ed indicando se va tentata
        ///     anche un esecuzione sincrona.
        /// </summary>
        /// <param name="state">Stato per l'operazione.</param>
        /// <param name="directExecution">Indica se lo scheduler deve tentare un'esecuzione sincrona prima di schedulare.</param>
        /// <param name="opExecutorFactory">Factory che fornisce l'executor dell' operazione</param>
        /// <param name="scheduleType">Istanza di tipo schedulazione da creare</param>
        /// <param name="lastOperationOlderThanMinutes">Minuti che devono trascorrere dall'ultima operazione schedulata dello stesso tipo.</param>
        /// <param name="logger">Istanza logger</param>
        /// <param name="operationKeys">Chiavi da usare per poter identificare in modo univoco l'operazione</param>
        /// <param name="operationScheduleCheckType">Tipo di controllo per l'esistenza di una schedulazione analoga, all'interno della finestra temporale</param>
        /// <returns></returns>
        public ScheduleResult Schedule(Dictionary<string, string> state,
            bool directExecution,
            IOperationExecutorFactory opExecutorFactory,
            OperationScheduleType scheduleType,
            int lastOperationOlderThanMinutes,
            NLog.Logger logger,
            string operationKeys,
            OperationScheduleCreateIfCheckType operationScheduleCheckType)
        {
            // la prima operazione effettuata è il salvataggio del record di schedulazione, di modo che se anche il metodo dovesse andare successivamente in errore,
            // la logica verrebbe rieseguita dallo scheduler esterno
            var schedule = new OperationSchedule {TypeId = scheduleType.Id, Parameters = state, OperationKeys = operationKeys };
            schedule.CreateIf(lastOperationOlderThanMinutes, operationScheduleCheckType);

            if (schedule.Id != Guid.Empty)
            {
                bool success = false;
                if (directExecution)
                {
                    var task = Task<OperationScheduleExecResult>.Factory.StartNew(() =>
                    {
                        var executor = opExecutorFactory.GetExecutor(scheduleType);
                        if (executor == null)
                        {
                            throw new SchedulerAssemblyNotLoadedException(
                                string.Format("Impossibile caricare l'assembly per lo scheduler di tipo '{0}'.", scheduleType.Code));
                        }
                        return executor.Exec(schedule, logger);
                    });
                    success = task.Result.Status == Common.OperationScheduleResultStatus.Success;
                }

                if (success)
                {
                    OperationSchedule.SetExecuted(schedule.Id, 0, String.Empty, String.Empty, String.Empty, String.Empty);
                }

                return new ScheduleResult(schedule.Id, success);
            }

            return new ScheduleResult(Guid.Empty, false);
        }

        #endregion
    }
}
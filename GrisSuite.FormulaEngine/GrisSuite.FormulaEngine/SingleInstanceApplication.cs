using System;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;

namespace GrisSuite.FormulaEngine
{
    /// <summary>
    /// Questa classe definisce il modello a singola istanza secondo l'implementazione Microsoft nel namespace Visual Basic.
    /// Questo tipo di implementazione � ben pi� robusto dell'uso del Mutex.
    /// Per riferimento: http://msdn.microsoft.com/msdnmag/issues/05/09/NETMatters/ e http://blogs.msdn.com/pedrosilva/archive/2005/03/09/391381.aspx
    /// </summary>
    public class SingleInstanceApplication : WindowsFormsApplicationBase
    {
        private SingleInstanceApplication()
        {
            base.IsSingleInstance = false;
        }

        public static void Run(Form f, StartupNextInstanceEventHandler startupHandler)
        {
            SingleInstanceApplication app = new SingleInstanceApplication();
            app.MainForm = f;
            app.StartupNextInstance += startupHandler;
            app.Run(Environment.GetCommandLineArgs());
        }
    }
}
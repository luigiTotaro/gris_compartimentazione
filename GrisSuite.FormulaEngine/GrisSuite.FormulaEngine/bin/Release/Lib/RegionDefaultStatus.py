# -*- coding: latin-1 -*-
# script di calcolo dello stato di default per gli oggetti Region
import clr
import sys

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage, SeverityWeightsAndRanges
from Infrastructure.MetaClasses import *

if isinstance(obj, Region): 
    obj = GrisObjectPxy(obj, state)
        
    if obj.AreAllObjectsByParentUnknown():
        obj.Severity = Unknown.Severity
        # Il flag ForcedSeverityLevel era valorizzato nella function object_status_calc, ma mai portato in update sulla tabella object_status
        # obj.ForcedSeverityLevel = ForcedSeverityLevel.AllChildrenAreNotReachable            
    else:
        # obj.ForcedSeverityLevel = ForcedSeverityLevel.NotForced
        weightsAndRanges = SeverityWeightsAndRanges(sevRanges)
        obj.Severity = GetWeightedAverage(
            obj.Zones.GetInSeverity(Ok.Severity).Count, 
            obj.Zones.GetInSeverity(Warning.Severity).Count, 
            obj.Zones.GetInSeverity(Error.Severity).Count + obj.Zones.GetInSeverity(Offline.Severity).Count + obj.Zones.GetInSeverity(Unknown.Severity).Count, 
            obj.Zones.GetInSeverity(NotActive.Severity, True).Count,
            weightsAndRanges)
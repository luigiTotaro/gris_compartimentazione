# -*- coding: latin-1 -*-
import sys
import clr

sys.path.append('..')

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import *

if isinstance(obj, GrisObject):
	if obj.Children.AreAllInSeverity(ServerOffline.SeverityDetail):
		obj.Severity = Unknown.Severity
		obj.ForcedSeverityLevel = ForcedSeverityLevel.STLC1000ServerNotReachable
	else:
		if obj.Children.AnyAtLeastInSeverity(Warning.Severity, NotActive.Severity, Unknown.Severity):
			obj.SeverityDetail = Warning.SeverityDetail
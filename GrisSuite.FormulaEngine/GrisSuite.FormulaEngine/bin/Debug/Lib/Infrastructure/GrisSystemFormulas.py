# -*- coding: latin-1 -*-
# script che raccoglie tutte le funzioni di calcolo di sistema di Gris utilizzabili da altri scripts
import sys
from GrisSuite.FormulaEngine.Model import *
from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

def GetWeightedAverage (numOk, numWarning, numError, numMaintenance, weightsAndRanges):
    tot = numOk + numWarning + numError
    
    if tot > 0: # ci sono periferiche in ok o warning o in error
        avgValue = ((numOk + .0) / (tot + .0) * weightsAndRanges.OkWeight()) + ((numWarning + .0) / (tot + .0) * weightsAndRanges.WarningWeight()) + ((numError + .0) / (tot + .0) * weightsAndRanges.ErrorWeight())

        iteration = 0
        for range, ret in weightsAndRanges.Ranges():
            if iteration == 0 and avgValue >= range[0] and avgValue <= range[1]:
                return ret
            if iteration > 0 and avgValue > range[0] and avgValue <= range[1]:
                return ret
            iteration += 1

    if numMaintenance > 0: # ce ne sono solo in Maintenance
        return Severity.NotActive
            
    return Severity.NotClassified

class SeverityWeightsAndRanges (object):

	def __init__ (self, sevRanges):
		if sevRanges is None:
			raise TypeError("Il parametro 'sevRanges' passato al costruttore non � inizializzato. E' possibile che la variabile globale 'sevRanges' non sia inizializzata correttamente.")

		self.__ranges = []
		counter = 0
		for range in sevRanges:
			if range.Id == 1:
				self.__okWeight = range.SeverityWeight
			if range.Id == 2:
				self.__warningWeight = range.SeverityWeight
			if range.Id == 3:
				self.__errorWeight = range.SeverityWeight

			self.__ranges.append(((range.MinValue, range.MaxValue), counter))
			counter += 1

	def OkWeight(self):
		return self.__okWeight
	def WarningWeight(self):
		return self.__warningWeight
	def ErrorWeight(self):
		return self.__errorWeight
	def Ranges(self):
		return self.__ranges

def IsNotNullOrEmpty(value):
    return value is not None and len(value) > 0
        
def IsNullOrEmpty(value):
    return not IsNotNullOrEmpty(value)

def GetDividedName (name):
    caps = [cap for cap in name if cap.isupper() or cap.isdigit()]
    scaps = set(caps)
    for cap in scaps:
        name = name.replace(cap, ' ' + cap)
    return name.strip()

def GetSeverityIndexByWeights (totals, ranges):

	avgValue = 0
	tot = totals.GetTotal()
	
	if tot > 0:
		if totals.GetNotActiveTotal() == tot:
			return Severity.NotActive
		avgValue = (totals.GetOkTotal() * 0.0 + 
					totals.GetWarningTrascurabileWeightedTotal() + totals.GetWarningLieveWeightedTotal() + totals.GetWarningStandardWeightedTotal() + totals.GetWarningGraveWeightedTotal() + totals.GetWarningBloccanteWeightedTotal() +
					totals.GetErrorTrascurabileWeightedTotal() + totals.GetErrorLieveWeightedTotal() + totals.GetErrorStandardWeightedTotal() + totals.GetErrorGraveWeightedTotal() + totals.GetErrorBloccanteWeightedTotal() +
					totals.GetOfflineTrascurabileWeightedTotal() + totals.GetOfflineLieveWeightedTotal() + totals.GetOfflineStandardWeightedTotal() + totals.GetOfflineGraveWeightedTotal() + totals.GetOfflineBloccanteWeightedTotal()
					) / (tot + .0)
	
		index = (1 - avgValue) * 100

		if index < 0:
			index = 0

		for range, ret in ranges.Get():
			if index >= range[0] and index <= range[1]:
				return ret

	return Severity.NotClassified

class SubObjectTotals (object):
	__parent = None
	__weight = None
	OkTotal = None
	WarningTrascurabileTotal = 0
	WarningLieveTotal = 0
	WarningStandard = None
	WarningGraveTotal = 0
	WarningBloccanteTotal = 0
	ErrorTrascurabileTotal = 0
	ErrorLieveTotal = 0
	ErrorStandard = None
	ErrorGraveTotal = 0
	ErrorBloccanteTotal = 0
	OfflineTrascurabileTotal = 0
	OfflineLieveTotal = 0
	OfflineStandard = None
	OfflineGraveTotal = 0
	OfflineBloccanteTotal = 0
	NotActiveTotal = None

	def __init__ (self, parent, weights):
		if not isinstance(parent, GrisObject):
			raise TypeError("Il parametro 'parent' del costruttore della classe 'SubObjectTotals' non � del tipo corretto.")
		if parent is None:
			raise TypeError("Il parametro 'parent' del costruttore della classe 'SubObjectTotals' non � inizializzato correttamente.")
		if weights is None:
			raise TypeError("Il parametro 'weights' del costruttore della classe 'SubObjectTotals' non � inizializzato correttamente.")
		self.__parent = parent
		self.__weights = weights

	def GetOkTotal (self):
		if self.OkTotal == None:
			self.OkTotal = self.__parent.Children.GetInSeverity(Ok.Severity).Count
		return self.OkTotal

	def GetWarningTrascurabileTotal (self):
		return self.WarningTrascurabileTotal
	def GetWarningTrascurabileWeightedTotal (self):
		return self.GetWarningTrascurabileTotal() * self.__weights.WarningTrascurabileWeight

	def GetWarningLieveTotal (self):
		return self.WarningLieveTotal
	def GetWarningLieveWeightedTotal (self):
		return self.GetWarningLieveTotal() * self.__weights.WarningLieveWeight

	def GetWarningStandardTotal (self):
		if self.WarningStandard == None:
			self.WarningStandard = self.__parent.Children.GetInSeverity(Warning.Severity).Count - (self.WarningTrascurabileTotal + self.WarningLieveTotal + self.WarningGraveTotal + self.WarningBloccanteTotal)
		return self.WarningStandard
	def GetWarningStandardWeightedTotal (self):
		return self.GetWarningStandardTotal() * self.__weights.WarningStandardWeight

	def GetWarningGraveTotal (self):
		return self.WarningGraveTotal
	def GetWarningGraveWeightedTotal (self):
		return self.GetWarningGraveTotal() * self.__weights.WarningGraveWeight

	def GetWarningBloccanteTotal (self):
		return self.WarningBloccanteTotal
	def GetWarningBloccanteWeightedTotal (self):
		return self.GetWarningBloccanteTotal() * self.GetTotal()

	def GetErrorTrascurabileTotal (self):
		return self.ErrorTrascurabileTotal
	def GetErrorTrascurabileWeightedTotal (self):
		return self.GetErrorTrascurabileTotal() * self.__weights.ErrorTrascurabileWeight

	def GetErrorLieveTotal (self):
		return self.ErrorLieveTotal
	def GetErrorLieveWeightedTotal (self):
		return self.GetErrorLieveTotal() * self.__weights.ErrorLieveWeight

	def GetErrorStandardTotal (self):
		if self.ErrorStandard == None:
			self.ErrorStandard = self.__parent.Children.GetInSeverity(Error.Severity).Count - (self.ErrorTrascurabileTotal + self.ErrorLieveTotal + self.ErrorGraveTotal + self.ErrorBloccanteTotal)
		return self.ErrorStandard
	def GetErrorStandardWeightedTotal (self):
		return self.GetErrorStandardTotal() * self.__weights.ErrorStandardWeight

	def GetErrorGraveTotal (self):
		return self.ErrorGraveTotal
	def GetErrorGraveWeightedTotal (self):
		return self.GetErrorGraveTotal() * self.__weights.ErrorGraveWeight

	def GetErrorBloccanteTotal (self):
		return self.ErrorBloccanteTotal
	def GetErrorBloccanteWeightedTotal (self):
		return self.GetErrorBloccanteTotal() * self.GetTotal()

	def GetOfflineTrascurabileTotal (self):
		return self.OfflineTrascurabileTotal
	def GetOfflineTrascurabileWeightedTotal (self):
		return self.GetOfflineTrascurabileTotal() * self.__weights.OfflineTrascurabileWeight

	def GetOfflineLieveTotal (self):
		return self.OfflineLieveTotal
	def GetOfflineLieveWeightedTotal (self):
		return self.GetOfflineLieveTotal() * self.__weights.OfflineLieveWeight

	def GetOfflineStandardTotal (self):
		if self.OfflineStandard == None:
			self.OfflineStandard = self.__parent.Children.GetInSeverity(Offline.Severity).Count - (self.OfflineTrascurabileTotal + self.OfflineLieveTotal + self.OfflineGraveTotal + self.OfflineBloccanteTotal)
		return self.OfflineStandard
	def GetOfflineStandardWeightedTotal (self):
		return self.GetOfflineStandardTotal() * self.__weights.OfflineStandardWeight

	def GetOfflineGraveTotal (self):
		return self.OfflineGraveTotal
	def GetOfflineGraveWeightedTotal (self):
		return self.GetOfflineGraveTotal() * self.__weights.OfflineGraveWeight

	def GetOfflineBloccanteTotal (self):
		return self.OfflineBloccanteTotal
	def GetOfflineBloccanteWeightedTotal (self):
		return self.GetOfflineBloccanteTotal() * self.GetTotal()

	def GetNotActiveTotal (self):
		if self.NotActiveTotal == None:
			self.NotActiveTotal = self.__parent.Children.GetInSeverity(NotActive.Severity).Count
		return self.NotActiveTotal

	def GetTotal (self):
		return self.GetOkTotal() + \
			self.GetWarningTrascurabileTotal() + self.GetWarningLieveTotal() + self.GetWarningStandardTotal() + self.GetWarningGraveTotal() + self.GetWarningBloccanteTotal() + \
			self.GetErrorTrascurabileTotal() + self.GetErrorLieveTotal() + self.GetErrorStandardTotal() + self.GetErrorGraveTotal() + self.GetErrorBloccanteTotal() + \
			self.GetOfflineTrascurabileTotal() + self.GetOfflineLieveTotal() + self.GetOfflineStandardTotal() + self.GetOfflineGraveTotal() + self.GetOfflineBloccanteTotal()
	#def GetTotal (self):
	#	return self.GetOkTotal() + \
	#		self.GetWarningLieveTotal() + self.GetWarningStandardTotal() + self.GetWarningGraveTotal() + self.GetWarningBloccanteTotal() + \
	#		self.GetErrorLieveTotal() + self.GetErrorStandardTotal() + self.GetErrorGraveTotal() + self.GetErrorBloccanteTotal() + \
	#		self.GetOfflineLieveTotal() + self.GetOfflineStandardTotal() + self.GetOfflineGraveTotal() + self.GetOfflineBloccanteTotal()

class SeverityWeights (object):
	WarningTrascurabileWeight = 0.0
	WarningLieveWeight = 0.37
	WarningStandardWeight = 0.40
	WarningGraveWeight = 0.67
	ErrorTrascurabileWeight = 0.0
	ErrorLieveWeight = 0.40
	ErrorStandardWeight = 0.67
	ErrorGraveWeight = 0.75
	OfflineTrascurabileWeight = 0.0
	OfflineLieveWeight = 0.67
	OfflineStandardWeight = 0.75
	OfflineGraveWeight = 0.80

class SevByObjectRanges (object):
	__rngs = None
	def __init__ (self):
		self.__rngs = (((0, 35), 2), ((36, 80), 1), ((81, 100), 0))
	def Get (self):
		return self.__rngs
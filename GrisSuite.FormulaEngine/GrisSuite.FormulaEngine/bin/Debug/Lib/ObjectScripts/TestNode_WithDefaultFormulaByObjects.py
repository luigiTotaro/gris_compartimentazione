# -*- coding: latin-1 -*-
import sys
import clr
import System

sys.path.append('..')

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy
from Infrastructure.GrisSystemFormulas import *

if isinstance(obj, Node):
	clr.Convert(obj, Node)

	gobj = obj
	obj = GrisObjectPxy(obj, state)
	totals = SubObjectTotals(gobj, SeverityWeights())

	#if (obj.PannelloZone.Severity == Error.Severity):
	#	totals.ErrorBloccanteTotal += 1

	#if obj.PannelloZone.Severity == Warning.Severity:
	#	totals.WarningTrascurabileTotal += 1

	obj.Severity = GetSeverityIndexByWeights(totals, SevByObjectRanges())
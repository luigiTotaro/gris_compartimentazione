
import sys
import clr
import System

sys.path.append('..')

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, Node):
    clr.Convert(obj, Node)

    obj = GrisObjectPxy(obj, state)
        
    if obj.S0.SeverityDetail == Error.SeverityDetail:
        obj.Severity = Error.Severity
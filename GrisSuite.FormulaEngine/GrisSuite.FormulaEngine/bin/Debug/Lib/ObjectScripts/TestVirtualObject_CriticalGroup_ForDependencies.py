# test non realistico, solo allo scopo di testare il ricalcolo delle dipendenze
import sys
import clr

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import *

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.Children.AnyAtLeastInSeverity(Offline.Severity):
        obj.SeverityDetail = Offline.SeverityDetail
        obj.Parent.ForceSeverity(obj.Severity) # server
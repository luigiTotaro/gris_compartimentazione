#[07/07/2011 15:20:53] Cristian Storti: se io ho 2 PZ ridontati nella stazione x
#[07/07/2011 15:20:59] Cristian Storti: uno e' giallo
#[07/07/2011 15:21:02] Cristian Storti: e l'altro e' rosso
#[07/07/2011 15:21:09] Cristian Storti: di che colore e' la stazione?
#
#[07/07/2011 15:26:28] dam107it: in base all'importanza delle zone il pz e' + o - importante
#[07/07/2011 15:26:31] dam107it: quindi
#[07/07/2011 15:26:45] dam107it: se quello rosso e' il meno importante > stazione gialla
#[07/07/2011 15:26:59] dam107it: se quello rosso e' il + importante > stazione rossa

import sys
import clr

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import *

try:
    if isinstance(obj, NodeSystem):
        clr.Convert(obj, NodeSystem)

        obj = GrisObjectPxy(obj, state)
        
        pz_important = obj.GetDeviceByName("PZ_guz")
        pz_less_important = obj.GetDeviceByName("PZ_ovetz")

        if pz_important and pz_less_important:
            if pz_important.Severity >= Warning.Severity:
                obj.Severity = pz_important.Severity
            else:
                if pz_less_important.Severity >= Warning.Severity:
                    obj.Severity = pz_less_important.Severity

            obj.Parent.ForceSeverity(obj.Severity) # stazione
except:
    type, value = sys.exc_info()[:2]
    print type
    print value
    print 1
# -*- coding: latin-1 -*-
import sys
import clr
import System

sys.path.append('..')

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy
from Infrastructure.GrisSystemFormulas import *

if isinstance(obj, NodeSystem):
	clr.Convert(obj, NodeSystem)

	gobj = obj
	obj = GrisObjectPxy(obj, state)
	totals = SubObjectTotals(gobj, SeverityWeights())

	######
	#if (obj.PannelloZone.Severity == Warning.Severity):
	#	totals.WarningGraveTotal += 1

	if (obj.PannelloZone.Severity == Error.Severity):
		totals.ErrorBloccanteTotal += 1

	if (obj.PannelloZone.Severity == Offline.Severity):
		totals.OfflineBloccanteTotal += 1

	if (obj.Amplificatore.Severity == Warning.Severity):
		totals.WarningTrascurabileTotal += 1

	#	
	#if (obj.Amplificatore.Severity == Error.Severity):
	#	totals.ErrorTrascurabileTotal += 1

	#if (obj.Amplificatore.Severity == Offline.Severity):
	#	totals.OfflineTrascurabileTotal += 1

	if (obj.SistemaAlimentazione.Severity == Warning.Severity):
		totals.WarningTrascurabileTotal += 1

	#if (obj.SistemaAlimentazione.Severity == Error.Severity):
	#	totals.ErrorBloccanteTotal += 1

	#if (obj.SistemaAlimentazione.Severity == Offline.Severity):
	#	totals.OfflineBloccanteTotal += 1
	######

	obj.Severity = GetSeverityIndexByWeights(totals, SevByObjectRanges())
	if obj.Severity == NotAvailable.Severity:
		logger.Info("Esito invio SMS: inviato -> {0}", operations.SendSms("+39045556677", "Stazione in stato indefinito.").Success)
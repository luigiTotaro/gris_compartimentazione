# -*- coding: latin-1 -*-
import sys
import clr

sys.path.append('..')

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import *
from Infrastructure.Notifications import SetupEnvNotifications, SendEnvNotifications

if isinstance(obj, GrisObject): 
	SetupEnvNotifications(obj, operations)

	if obj.Children.AreXPercInSeverity(50, Error.Severity):
		obj.SeverityDetail = Error.SeverityDetail

	SendEnvNotifications(obj, operations)
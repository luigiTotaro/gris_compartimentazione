# -*- coding: latin-1 -*-
# script di calcolo dello stato che assegna lo stato fisso di 'Stato Sconosciuto'
import clr
import sys

from GrisSuite.FormulaEngine.Model import *
from GrisSuite.FormulaEngine.Common.Severities import *
from Infrastructure.Notifications import SetupEnvNotifications, SendEnvNotifications

if isinstance(obj, GrisObject):
	SetupEnvNotifications(obj, operations)
	obj.SeverityDetail = Unknown.SeverityDetail
	SendEnvNotifications(obj, operations)
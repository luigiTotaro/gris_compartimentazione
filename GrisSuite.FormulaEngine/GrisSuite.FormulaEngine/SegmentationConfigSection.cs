﻿
using System.Configuration;

namespace GrisSuite.FormulaEngine
{
	public class SegmentationConfigSection : ConfigurationSection
	{
		[ConfigurationProperty("Segments")]
		public SegmentCollection Segments
		{
			get { return ( (SegmentCollection) ( base["Segments"] ) ); }
		}
	}

	[ConfigurationCollection(typeof(Segment))]
	public class SegmentCollection : ConfigurationElementCollection
	{
		#region Overrides of ConfigurationElementCollection

		protected override ConfigurationElement CreateNewElement ()
		{
			return new Segment();
		}

		protected override object GetElementKey ( ConfigurationElement element )
		{
			return ( (Segment) ( element ) ).Name;
		}

		#endregion

		public Segment this[int idx]
		{
			get
			{
				return (Segment) BaseGet(idx);
			}
		}
	}

	public class Segment : ConfigurationElement
	{
		[ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
		public string Name
		{
			get
			{
				return ( (string) ( base["name"] ) );
			}
			set
			{
				base["name"] = value;
			}
		}

		[ConfigurationProperty("objectType", DefaultValue = "", IsKey = false, IsRequired = true)]
		public string ObjectType
		{
			get
			{
				return ( (string) ( base["objectType"] ) );
			}
			set
			{
				base["objectType"] = value;
			}
		}

		[ConfigurationProperty("objectIds", DefaultValue = "", IsKey = false, IsRequired = true)]
		public string ObjectIds
		{
			get
			{
				return ( (string) ( base["objectIds"] ) );
			}
			set
			{
				base["objectIds"] = value;
			}
		}

		[ConfigurationProperty("remoteAddress", DefaultValue = "", IsKey = false, IsRequired = false)]
		public string RemoteAddress
		{
			get
			{
				return ( (string) ( base["remoteAddress"] ) );
			}
			set
			{
				base["remoteAddress"] = value;
			}
		}
	}
}

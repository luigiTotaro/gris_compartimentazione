﻿
using System;
using System.Windows.Forms;

namespace GrisSuite.FormulaEngine
{
	public partial class MainForm : Form
	{
		public MainForm ()
		{
			InitializeComponent();
		}

		private void MainForm_Load ( object sender, EventArgs e )
		{
			Launcher launcher = new Launcher();
			launcher.LaunchEvaluation();
			Environment.Exit(0);
		}
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Configuration;
using System.ServiceModel;
using System.Threading.Tasks;
using GrisSuite.FormulaEngine.FormulaService;
using GrisSuite.FormulaEngine.Library;
using System.DirectoryServices;
using System.IO;

namespace GrisSuite.FormulaEngine
{
    class Launcher
    {
        public EventHandler EvaluationStarted;
        public EventHandler EvaluationStopped;
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
		private readonly Stopwatch _timer = new Stopwatch();

        public void LaunchEvaluation ()
        {
			_logger.Info("Inizio elaborazione...");

            String TestConnectionActiveDirectoryGroup = ConfigurationManager.AppSettings["TestConnectionActiveDirectoryGroup"];
            if (TestConnectionActiveDirectoryGroup != "")
            {
                _logger.Info("Test Connessione Active Directory");

                String ActiveDirectoryConnectionString_group = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString_group"];
                String ActiveDirectoryUsername_group = ConfigurationManager.AppSettings["ActiveDirectoryUsername_group"];
                String ActiveDirectoryPassword_group = ConfigurationManager.AppSettings["ActiveDirectoryPassword_group"];
                String ActiveDirectoryConnectionString_user = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString_user"];
                String ActiveDirectoryUsername_user = ConfigurationManager.AppSettings["ActiveDirectoryUsername_user"];
                String ActiveDirectoryPassword_user = ConfigurationManager.AppSettings["ActiveDirectoryPassword_user"];
                String ActiveDirectoryTelephoneKey = ConfigurationManager.AppSettings["ActiveDirectoryTelephoneKey"];
                String ActiveDirectoryMailKey = ConfigurationManager.AppSettings["ActiveDirectoryMailKey"];


                _logger.Info("ActiveDirectoryConnectionString (Gruppi): " + ActiveDirectoryConnectionString_group);
                _logger.Info("ActiveDirectoryUsername (Gruppi): " + ActiveDirectoryUsername_group);
                _logger.Info("ActiveDirectoryPassword (Gruppi): " + ActiveDirectoryPassword_group);
                _logger.Info("ActiveDirectoryConnectionString (Utenti): " + ActiveDirectoryConnectionString_user);
                _logger.Info("ActiveDirectoryUsername (Utenti): " + ActiveDirectoryUsername_user);
                _logger.Info("ActiveDirectoryPassword (Utenti): " + ActiveDirectoryPassword_user);
                _logger.Info("Gruppo da controllare (Utenti): " + TestConnectionActiveDirectoryGroup);
                _logger.Info("Chiave di ricerca Telefono: " + ActiveDirectoryTelephoneKey);
                _logger.Info("Chiave di ricerca Mail: " + ActiveDirectoryMailKey);


                //per ogni gruppo devo vedere i componenti
                DirectoryEntry de = null;
                try
                {
                    de = new DirectoryEntry(ActiveDirectoryConnectionString_group, ActiveDirectoryUsername_group, ActiveDirectoryPassword_group);
                    DirectorySearcher ds = new DirectorySearcher(de);
                    _logger.Info("GUID sel server (Gruppi): " + de.Guid);

                    ds.Filter = "(&(objectCategory=group)(name=" + TestConnectionActiveDirectoryGroup + "))";

                    ds.SearchScope = SearchScope.Subtree;
                    SearchResult rs = ds.FindOne();

                    List<string> nomi2Find = new List<string>();

                    if (rs != null)
                    {
                        var numUtenti = rs.GetDirectoryEntry().Properties["member"].Count;


                        for (var i = 0; i < numUtenti; i++)
                        {
                            object utente = rs.GetDirectoryEntry().Properties["member"][i];
                            String[] properties = utente.ToString().Split(',');
                            String displayName = "";
                            for (int t = 0; t < properties.Count(); t++)
                            {
                                if (properties[t].StartsWith("CN="))
                                {
                                    displayName = properties[t].Substring(3);
                                    nomi2Find.Add(displayName);
                                    break;
                                }
                            }
                            
                            /*
                            if (displayName != "")
                            {
                                _logger.Info("Trovato utente: " + displayName);
                                //recupero l'utente
                                ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(displayName=" + displayName + "))";
                                SearchResult rs2 = ds.FindOne();
                                if (rs2 != null)
                                {
                                    String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                    String email = rs2.GetDirectoryEntry().Properties["mail"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["mail"].Value.ToString();
                                    String tel = rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value.ToString();
                                    _logger.Info("Dettagli utente - nome: " + name + " - mail: " + email + " - telefono: " + tel);
                                }
                            }*/
                        }
                    }

                    if (de != null) de.Dispose();
                    _logger.Info("Trovati " + nomi2Find.Count + " utenti da esaminare");

                    //seconda parte, recupero i nomi
                    de = new DirectoryEntry(ActiveDirectoryConnectionString_user, ActiveDirectoryUsername_user, ActiveDirectoryPassword_user);
                    ds = new DirectorySearcher(de);
                    _logger.Info("GUID sel server (Utenti): " + de.Guid);

                    foreach (var displayName in nomi2Find)
                    {
                        if (displayName != "")
                        {
                            _logger.Info("Trovato utente: " + displayName);
                            //recupero l'utente
                            ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(CN=" + displayName + "))";
                            SearchResult rs2 = ds.FindOne();
                            if (rs2 != null)
                            {
                                String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                String email = rs2.GetDirectoryEntry().Properties[ActiveDirectoryMailKey].Value == null ? "" : rs2.GetDirectoryEntry().Properties[ActiveDirectoryMailKey].Value.ToString();
                                String tel = rs2.GetDirectoryEntry().Properties[ActiveDirectoryTelephoneKey].Value == null ? "" : rs2.GetDirectoryEntry().Properties[ActiveDirectoryTelephoneKey].Value.ToString();
                                _logger.Info("Dettagli utente - nome: " + name + " - mail: " + email + " - telefono: " + tel);
                            }
                        }
                    }

                    if (de != null) de.Dispose();
                
 
                }
                catch (Exception exc)
                {
                    _logger.Info("Errore: " + exc.Message);
                }

                if (de != null) de.Dispose();


                //test messaggi da file
                DateTime currentOperationTimestamp = DateTime.Now;
                String smsMessageTemplate = "";
                string devicesListString = "device1, device2, device 3";

                _logger.Info("Test Messaggi da file");

                try
                {
                    smsMessageTemplate = File.ReadAllText(string.Format("{0}\\Operations\\node_sms_template.txt", Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)));
                }
                catch (Exception exc)
                {
                    _logger.Info("Errore recuperando il contenuto del file - " + exc.Message);
                    //smsMessageTemplate = @"Avviso generato il {0} alle ore {1}, Stazione in stato di errore: {2}, Perif. problematiche: {3}.";
                    smsMessageTemplate = @"Alert GRIS {0}-{1}, {2} {3}.";
                }

                // Usiamo le stringhe di formattazione statiche per l'italiano, corrispondono a ShortDate / ShortTime
                String Pippo = string.Format(smsMessageTemplate, currentOperationTimestamp.ToString("d/M/yyyy"), currentOperationTimestamp.ToString("HH:mm"),
                    "Nome", devicesListString);

                _logger.Info(Pippo);



                _logger.Info("Fine elaborazione...");
                return;
            }
			
			if ( this.EvaluationStarted != null ) this.EvaluationStarted(this, null);

			_timer.Start();

			try
			{
				Guid marker = Guid.NewGuid();
				var libLauncher = new Library.Launcher();
				var segments = this.GetSegmentation();

				// esegue le operazioni preparatorie per tutti gli oggetti
				//libLauncher.PrepareObjects(marker);

				if ( segments != null && segments.Count > 0 )
				{
                    libLauncher.PrepareObjects(marker);

                    // l'elaborazione delle formule vanno eseguite in più segmenti paralleli
					int taskIndex = 0;
					Task[] tasks = new Task[segments.Count]; 
					
					foreach ( var segment in segments )
					{
						var currSegment = segment;
						Guid[] objIds = null;
						Type objType = Type.GetType(currSegment.ObjectType);

						try
						{
							objIds = ( from idString in currSegment.ObjectIds.Split(',')
									   select Guid.Parse(idString.Trim()) ).ToArray();
						}
						catch ( FormatException exc )
						{
							_logger.ErrorException(string.Format("Il formato dei Guids per la segmentazione del calcolo non è corretto: {0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
						}

						if ( objType != null && objIds != null )
						{
							if ( string.IsNullOrEmpty(segment.RemoteAddress) )
							{
								// esecuzione in più Tasks paralleli
								tasks[taskIndex] = Task<int>.Factory.StartNew(() =>
								{
								    var objectPcks = libLauncher.LoadObjectsFromDb(objType, objIds);
								    libLauncher.EvaluateObjects(objectPcks);
								    libLauncher.SaveToDb(objType, objectPcks);
								    return 0;
								});
							}
							else
							{
								// esecuzione in più Tasks (out of process) paralleli, potenzialmente su più macchine
								tasks[taskIndex] = Task<int>.Factory.StartNew(() =>
								{
									Stopwatch remoteRequestTimer = new Stopwatch(); 
									remoteRequestTimer.Start();
									_logger.Info("Invio richiesta di calcolo out of process al server '{0}'.", currSegment.RemoteAddress);

									try
									{
										FormulaServiceClient pxy = new FormulaServiceClient();
										if ( pxy.ClientCredentials != null ) pxy.ClientCredentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;
										pxy.Endpoint.Address = new EndpointAddress(currSegment.RemoteAddress);
										pxy.EvaluateAndSaveObjects(objType.AssemblyQualifiedName, objIds);
									}
									catch ( Exception exc )
									{
										_logger.ErrorException(string.Format("Eccezione '{0}' restituita dal server '{1}'.", exc.Message, currSegment.RemoteAddress), exc);
									}

									remoteRequestTimer.Stop();
									_logger.Info("Fine invio richiesta di calcolo out of process al server '{0}', oggetti valutati in {1} millisecondi.", currSegment.RemoteAddress, remoteRequestTimer.ElapsedMilliseconds);
									return 0;
								});
							}
						}

						taskIndex++;
					}
					Task.WaitAll(tasks);
				}
				else
				{
                    //elaborazione totale o solo di un/più compartimenti?

                    String Compartiment2Evaluate = ConfigurationManager.AppSettings["Compartiment2Evaluate"];
                    if (Compartiment2Evaluate == "")
                    {
                        // tutta l'elaborazione delle formule va eseguita in process
                        libLauncher.PrepareObjects(marker);
                        
                        var objectPcks = libLauncher.LoadObjectsFromDb(null);
                        libLauncher.EvaluateObjects(objectPcks);
                        libLauncher.SaveToDb(objectPcks);                    
                    }
                    else
                    {
                        libLauncher.PrepareObjectsExt(marker, Compartiment2Evaluate);
                        
                        //da webservice, per valutare cambiamenti in un compartimento
                        // questo metodo presuppone che gli oggetti da valutare siano tutti precaricati e compresi tra i discendenti del compartimento
                        // se non si può soddisfare il prerequisito è meglio utilizzare il metodo EvaluateAndSaveObjectAndAncestors
                        Guid[] objIds = null;
                        try
                        {
                            objIds = (from idString in Compartiment2Evaluate.Split(',')
                                      select Guid.Parse(idString.Trim())).ToArray();
                                     
                        }
                        catch (FormatException exc)
                        {
                            _logger.ErrorException(string.Format("Il formato dei Guids per la compartimentazione del calcolo non è corretto: {0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
                        }
                        
                        //Guid[] objIds = new Guid[1];
                        //objIds[0] = new Guid(Compartiment2Evaluate); 
                        var objs = libLauncher.GetObjectsAndDescendantsByIds(objIds);
                        //var objs = GrisObject.GetObjectsAndDescendantsByIds(typeof(IRegion), new ObjectInitializationValues { ToBeEvaluated = true, LookupInCacheOnly = true }, objectId);

                        if (objs != null && objs.Count > 0)
                        {
                            var objsPckg = DataAccessHelper.InitializeObjects(objs);
                            libLauncher.EvaluateObjects(objsPckg);

                            List<EvaluablePackage> regPckg = objsPckg.Where(pckg => pckg.GrisObject is GrisSuite.FormulaEngine.Common.Contracts.Entities.IRegion).ToList();

                            if (regPckg.Count > 0)
                            {
                                libLauncher.SaveToDb(typeof(GrisSuite.FormulaEngine.Common.Contracts.Entities.IRegion), regPckg);
                            }
                        }                    
                    }
				}

				// esegue le operazioni di finalizzazione per tutti gli oggetti
				libLauncher.FinalizeObjects(marker);
			}
			catch ( Exception exc )
			{
				_logger.ErrorException(string.Format("{0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
			}

			_timer.Stop();
			_logger.Info("Elaborazione totale eseguita in {0} millisecondi.", this._timer.ElapsedMilliseconds);

            if (this.EvaluationStopped != null) this.EvaluationStopped(this, null);
        }

		private IList<Segment> GetSegmentation ()
		{
			SegmentationConfigSection section = (SegmentationConfigSection) ConfigurationManager.GetSection("Segmentation");

			List<Segment> segs = new List<Segment>();
			if ( section != null && section.Segments != null && section.Segments.Count > 0 )
			{
				for ( int index = 0; index < section.Segments.Count; index++ )
				{
					segs.Add(section.Segments[index]);
				}
			}

			return segs;
		}
    }
}

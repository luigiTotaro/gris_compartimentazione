﻿
using System;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;

namespace GrisSuite.FormulaEngine
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main ()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			SingleInstanceApplication.Run(new MainForm(), StartupNextInstanceHandler);
		}

		private static void StartupNextInstanceHandler ( object sender, StartupNextInstanceEventArgs e )
		{
		}
	}
}

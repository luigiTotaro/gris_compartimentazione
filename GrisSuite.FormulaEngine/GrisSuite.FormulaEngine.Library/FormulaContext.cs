﻿
using System;
using System.Collections.Generic;
using System.IO;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.Entities;
using Microsoft.Scripting.Hosting;

namespace GrisSuite.FormulaEngine.Library
{
	public class FormulaContext : IEvaluationContext
	{
		private readonly ScriptEngine _scriptEngine;
		private readonly IList<ISeverityRange> _severityRanges;
		private readonly IOperations _operations;
		private static readonly Dictionary<string, CompiledCode> _scriptSources = new Dictionary<string, CompiledCode>(10000);
		private static readonly object _locker = new object();
		private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

		public FormulaContext ( ScriptEngine scriptEngine, IList<ISeverityRange> severityRanges, IOperations operations = null )
		{
			if ( scriptEngine == null ) throw new ArgumentException("L' oggetto FormulaContext non può essere inizializzato con uno ScriptEngine nullo.", "scriptEngine");
			if ( severityRanges == null ) throw new ArgumentException("L' oggetto FormulaContext non può essere inizializzato con una lista di soglie di severità nulla.", "severityRanges");	

			this._scriptEngine = scriptEngine;
			this._severityRanges = severityRanges;
			_operations = operations ?? new Operations();
		}

		public void Evaluate ( IEvaluableObject evalObject )
		{
			this.Evaluate(evalObject, null);
		}

		public void Evaluate ( IEvaluableObject evalObject, Dictionary<string, object> state )
		{
			foreach ( var customFormula in evalObject.CustomEvaluationFormulas )
			{
				this.ExecuteFormula(customFormula, state);
			}

			if ( !evalObject.IsSeverityEstabilished ) this.ExecuteFormula(evalObject.DefaultEvaluationFormula, state);
		}

		private void ExecuteFormula ( IEvaluationFormula formula, Dictionary<string, object> state )
		{
			if ( formula.CanEvaluate )
			{
				CompiledCode source = this.GetCompiledSource(formula);

				if ( source != null )
				{
					ScriptScope scope = this._scriptEngine.CreateScope();
					scope.SetVariable("obj", formula.Owner);
					scope.SetVariable("state", state);
					scope.SetVariable("logger", _logger);
					scope.SetVariable("sevRanges", this._severityRanges);
					scope.SetVariable("operations", this._operations);

                    //var obj = (IGrisObject) formula.Owner;
                    //if (obj.Id == Guid.Parse("794DC37D-D964-E411-8982-00505680063A") )
                    //{
                    //    System.Diagnostics.Debugger.Break();
                    //}

					formula.Owner.EvaluatingFormula = formula;

					try
					{
						source.Execute(scope);
					}
					catch ( Exception e )
					{
						ExceptionOperations eo = this._scriptEngine.GetService<ExceptionOperations>();

					    string evaluationError = String.Empty;

					    if (e.InnerException != null)
					    {
                            // Questa eccezione wrappa eccezioni dei task interni, di cui servono i dettagli
					        evaluationError = String.Format("Inner Exception: {0}, StackTrace: {1}", e.InnerException.Message, e.InnerException.StackTrace);
					    }

                        evaluationError += eo.FormatException(e);

                        formula.EvaluationError = evaluationError;
					}
				}
			}
		}

		private CompiledCode GetCompiledSource ( IEvaluationFormula evalFormula )
		{
			if ( evalFormula != null && evalFormula.CanEvaluate )
			{
				if ( _scriptSources.ContainsKey(evalFormula.ScriptPath) )
				{
					return _scriptSources[evalFormula.ScriptPath];
				}

				string scriptPath = Path.GetFullPath(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase.Substring("file://".Length + 1)) + evalFormula.ScriptPath);
				CompiledCode cc = this._scriptEngine.CreateScriptSourceFromFile(scriptPath).Compile();

				lock ( _locker )
				{
					if ( !_scriptSources.ContainsKey(evalFormula.ScriptPath) )
					{
						_scriptSources.Add(evalFormula.ScriptPath, cc);
					}
				}

				return cc;
			}

			return null;
		}
	}
}

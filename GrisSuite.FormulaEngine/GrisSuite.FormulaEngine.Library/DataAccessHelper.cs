﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.Library
{
	public class DataAccessHelper
	{
		public static IList<IGrisObject> GetAllObjects ()
		{
			return GrisObject.GetAll( new ObjectInitializationValues { ToBeEvaluated = true, LookupInCacheOnly = true } );
		}

		public static IList<IGrisObject> GetObjectsAndDescendants ( Type type, params Guid[] objectIds )
		{
			return GrisObject.GetObjectsAndDescendantsByIds(type, null, objectIds);
		}

        public static IList<IGrisObject> GetObjectAndAncestors(Guid objectId)
        {
            return GrisObject.GetObjectAndAncestors(objectId);
        }

        public static IList<Tuple<Guid, Guid>> GetRelatedObjectIds(params Guid[] objectIds)
        {
            return GrisObject.GetRelatedObjectIds(objectIds);
        }

		public static IList<EvaluablePackage> InitializeObjects ( List<IGrisObject> objects, List<Tuple<Guid, Guid>> relatedKeys = null )
		{
			var factory = new ObjectFactory();

			int serverMinuteTimeout;
			IParameter param = factory.GetParameter().GetByName("ServerMinuteTimeout");
			if ( param == null || !int.TryParse(param.Value, out serverMinuteTimeout) )
			{
				serverMinuteTimeout = 60;
			}

			GrisObject.Cache.Reset();
			GrisObject.Cache.AddObjects(objects);
            if ( relatedKeys != null ) GrisObject.Cache.AddRelatedObjects(relatedKeys);

			return objects.ConvertAll(go =>
			{
				if ( go is Server )
				{
					return new EvaluablePackage(go, new Dictionary<string, object>
			                                                                {
			                                                                    {"serverMinuteTimeout", serverMinuteTimeout}
			                                                                });
				}

				return new EvaluablePackage(go, null);
			});
		}

        public static void SyncObjects(Guid syncGuid, String Compartiment2Evaluate)
        {
            GrisObject.SyncObjects(syncGuid, Compartiment2Evaluate);
        }

        public static void UpdateObjectStatusPostComputation(Guid syncGuid)
        {
            GrisObject.UpdateObjectStatusPostComputation(syncGuid);
        }

		public static void SaveObject ( IStorable objectToSave )
		{
			ObjectContext.SaveObject(objectToSave);
		}

		public static void SaveBranch ( IStorable branchRoot )
        {

            ObjectContext.SaveBranch(branchRoot);
		}
	}
}
﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model;

namespace GrisSuite.FormulaEngine.Library
{
    public class Launcher
    {
        public EventHandler EvaluationStarted;
        public EventHandler EvaluationStopped;
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
		private readonly Stopwatch _timer = new Stopwatch();
		private readonly Stopwatch _commandTimer = new Stopwatch();

        #region Preparazione oggetti

        public void PrepareObjects ( Guid marker )
		{
			_logger.Info("Preparazione degli oggetti da valutare...");

			_timer.Reset();
			_timer.Start();

			DataAccessHelper.SyncObjects(marker, null);

			_timer.Stop();
			_logger.Info("Fine preparazione.");
		}

        public void PrepareObjectsExt(Guid marker, String Compartiment2Evaluate)
		{
			_logger.Info("Preparazione degli oggetti da valutare... - compartimentati");

			_timer.Reset();
			_timer.Start();

            DataAccessHelper.SyncObjects(marker, Compartiment2Evaluate);

			_timer.Stop();
			_logger.Info("Fine preparazione.");
		}


		#endregion

		#region Caricamento da Db

		public IList<EvaluablePackage> LoadObjectsFromDb ( Type entityType, params Guid[] objectIds )
		{
			_logger.Info("Caricamento degli oggetti da DB...");

			_timer.Reset();
			_timer.Start();

            Model.Entities.DeviceFilter.UpdateAllNotificationsFilters();

            IList<IGrisObject> objects = new List<IGrisObject>();
            IList<Tuple<Guid, Guid>> relatedKeys = new List<Tuple<Guid, Guid>>();

			try
			{
				objects = entityType == null ? DataAccessHelper.GetAllObjects() : DataAccessHelper.GetObjectsAndDescendants(entityType, objectIds);
			}
			catch ( SqlException exc )
			{
				if ( exc.Number == 1205 ) // deadlock
				{
					_logger.Warn("Riesecuzione caricamento a seguito di deadlock.");
					objects = entityType == null ? DataAccessHelper.GetAllObjects() : DataAccessHelper.GetObjectsAndDescendants(entityType, objectIds);					
				}
			}

            relatedKeys = entityType == null ? DataAccessHelper.GetRelatedObjectIds(null) : DataAccessHelper.GetRelatedObjectIds(objectIds);

			_timer.Stop();
			_logger.Info("{0} Oggetti caricati in {1} millisecondi.", objects.Count, _timer.ElapsedMilliseconds);
			_logger.Info("Fine caricamento.");

			return DataAccessHelper.InitializeObjects(objects.ToList(), relatedKeys.ToList());
		}

		#endregion

		#region Calcolo Severità

		public void EvaluateObjects ( IList<EvaluablePackage> objectPcks )
		{
			this.EvaluateObjects(new FormulaEngine(), objectPcks);
		}


        public List<IGrisObject> GetObjectsAndDescendantsByIds(params Guid[] ids)
        {
            return GrisObject.GetObjectsAndDescendantsByIds(typeof(GrisSuite.FormulaEngine.Common.Contracts.Entities.IRegion), new ObjectInitializationValues { ToBeEvaluated = true, LookupInCacheOnly = true }, ids);
        }
        
        
        
        public void EvaluateObjects ( FormulaEngine engine, IList<EvaluablePackage> objectPcks )
		{
			if ( objectPcks.Count > 0 )
			{
				_logger.Info("Inizio valutazione oggetti...");

				_timer.Reset();
				_timer.Start();

				try
				{
					engine.Eval(objectPcks);
				}
				catch ( AggregateException aExc )
				{
					foreach ( var innerException in aExc.InnerExceptions )
					{
						_logger.ErrorException(string.Format("{0}<br />ST: {1}", innerException.Message, innerException.StackTrace), innerException);
					}
				}

				_timer.Stop();
				_logger.Info("{0} Oggetti valutati in {1} millisecondi.", objectPcks.Count, _timer.ElapsedMilliseconds);
			}
		}

		#endregion

		#region Salvataggio in Db

		public void SaveToDb ( IList<EvaluablePackage> objectPcks )
		{
			this.SaveToDb(typeof(IRegion), objectPcks);
		}

		public void SaveToDb ( Type entityType, IList<EvaluablePackage> objectPcks )
		{
			_logger.Info("Inizio salvataggio oggetti in DB...");
			_timer.Reset();
			_timer.Start();

			var ancestors = this.FilterObjectsByType(entityType, objectPcks);

			foreach ( IGrisObject ancestor in ancestors )
			{
                _commandTimer.Start();
                DataAccessHelper.SaveBranch(ancestor);
				_logger.Info("Ramo {0} salvato in {1} millisecondi.", ancestor.Name, _commandTimer.ElapsedMilliseconds);
				_commandTimer.Reset();
            }

			_timer.Stop();
			_logger.Info("{0} Oggetti salvati in db in {1} millisecondi.", objectPcks.Count, _timer.ElapsedMilliseconds);
		}

		private IEnumerable<IGrisObject> FilterObjectsByType ( Type type, IEnumerable<EvaluablePackage> objectPcks )
		{
			if ( type == typeof(IZone) )
			{
				return from evaluablePackage in objectPcks
					   where evaluablePackage.GrisObject is IZone
					   select evaluablePackage.GrisObject;
			}

			if ( type == typeof(INode) )
			{
				return from evaluablePackage in objectPcks
					   where evaluablePackage.GrisObject is INode
					   select evaluablePackage.GrisObject;
			}

			return from evaluablePackage in objectPcks
				   where evaluablePackage.GrisObject is IRegion
				   select evaluablePackage.GrisObject;
		}

		#endregion

		#region Finalizzazione oggetti

		public void FinalizeObjects ( Guid marker )
		{
			_logger.Info("Finalizzazione degli oggetti...");

			_timer.Reset();
			_timer.Start();

			DataAccessHelper.UpdateObjectStatusPostComputation(marker);

			_timer.Stop();
			_logger.Info("Fine finalizzazione.");
		}

		#endregion
	}
}

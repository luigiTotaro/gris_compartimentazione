﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;

namespace GrisSuite.FormulaEngine.Library
{
	[Serializable]
	public class EvaluablePackage
	{
		private readonly IGrisObject _grisObject;
		private readonly Dictionary<string, object> _state;

		public EvaluablePackage ( IGrisObject grisObject )
		{
			_grisObject = grisObject;
			_state = null;
		}

		public EvaluablePackage ( IGrisObject grisObject, Dictionary<string, object> state )
		{
			_grisObject = grisObject;
			_state = state;
		}

		public IGrisObject GrisObject
		{
			get { return _grisObject; }
		}

		public Dictionary<string, object> State
		{
			get { return _state; }
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.ScheduledOperations;

namespace GrisSuite.FormulaEngine.Library
{
    public interface IOperations
    {
        ScheduleResult SendSms(string phoneNumber, string message, string operationKeys);
        ScheduleResult SendMail(string phoneNumber, string message, string operationKeys);
        ScheduleResult SendStreamFieldMail(long nodeSystemId);
        ScheduleResult Schedule(string scheduleType, Dictionary<string, string> state, bool directExecution, string operationKeys);
    }

    public class Operations : IOperations
    {
        public int DontScheduleIfOlderThanMinutesStreamFieldMail
        {
            get
            {
                int mins;
                return ConfigurationManager.AppSettings["DontScheduleIfOlderThanMinutesStreamFieldMail"] != null &&
                       int.TryParse(ConfigurationManager.AppSettings["DontScheduleIfOlderThanMinutesStreamFieldMail"], out mins)
                    ? mins
                    : 30;
            }
        }

        public int DontScheduleIfOlderThanMinutesSms
        {
            get
            {
                int mins;
                return ConfigurationManager.AppSettings["DontScheduleIfOlderThanMinutesSms"] != null &&
                       int.TryParse(ConfigurationManager.AppSettings["DontScheduleIfOlderThanMinutesSms"], out mins)
                    ? mins
                    : 10;
            }
        }

        public ScheduleResult SendSms(string phoneNumber, string message, string operationKeys = "")
        {
            IOperationScheduler scheduler = new OperationScheduler();
            return scheduler.Schedule("Sms"
                , new Dictionary<string, string> {{"phoneNum", phoneNumber}, {"message", message}}
                , false
                , this.DontScheduleIfOlderThanMinutesSms
                , null
                , operationKeys, OperationScheduleCreateIfCheckType.ByKeys);
        }

        public ScheduleResult SendMail(string phoneNumber, string message, string operationKeys = "")
        {
            throw new NotImplementedException();
        }

        public ScheduleResult SendStreamFieldMail(long nodeSystemId)
        {
            IOperationScheduler scheduler = new OperationScheduler();
            return scheduler.Schedule("StreamFieldMail"
                , new Dictionary<string, string> {{"nodeSysId", nodeSystemId.ToString()}}
                , false
                , this.DontScheduleIfOlderThanMinutesStreamFieldMail
                , null
                , String.Format("NodeSystemId:{0}", nodeSystemId.ToString())
                , OperationScheduleCreateIfCheckType.ByParameters);
        }


        public List<ScheduleResult> SendNodeSystemNotification(Guid objectStatusId, String customError)
        {
            var contacts = Model.Entities.ContactEntry.GetContactEntriesByObjectStatusId(objectStatusId);

            
            var contatti2 = Model.Entities.ContactGroupEntry.GetContactGroupEntriesByObjectStatusId4Mail(objectStatusId);
            foreach (var contact in contatti2)
            {
                //lo aggiungo solo se esiste la mail
                //if (contact.Email != "") contact.SendEMail = true;
                contacts.Add(contact);
            }
            
            var os = Model.GrisObject.GetById(objectStatusId);
            List<ScheduleResult> results = new List<ScheduleResult>();
            
            //if (os.TypeId != 6) return null;

            var node = GetNode(os);
            var nodeName = node != null ? node.Name : "?" ;
            var mailTo = new StringBuilder();
            var postfix = String.IsNullOrWhiteSpace(customError) ? "" : "_" + customError;

            foreach (var contact in contacts)
            {
                if (!string.IsNullOrWhiteSpace(contact.Email)) {
                    mailTo.Append(contact.Email).Append(",");
                }
            }
            if (mailTo.Length > 0)
            {
                var message = "il sistema {0} della stazione {1} è in errore " + postfix + "";

                message = string.Format(message, os.Name, nodeName);

                IOperationScheduler scheduler = new OperationScheduler();
                results.Add( scheduler.Schedule("Mail"
                    , new Dictionary<string, string> { { "to", mailTo.ToString(0, mailTo.Length-1) }, { "message", message }, {"objectStatusId", objectStatusId.ToString()} }
                    , false
                    , this.DontScheduleIfOlderThanMinutesStreamFieldMail
                    , null
                    , String.Format("Email:{0},NodeSystemIds:{1}", mailTo.ToString(0, mailTo.Length - 1), os.SpecificId), OperationScheduleCreateIfCheckType.ByKeys));
            }

            return results;

        }


        public List<ScheduleResult> SendDeviceNotification(Guid objectStatusId, String customError)
        {
            var contacts = Model.Entities.ContactEntry.GetContactEntriesByObjectStatusId(objectStatusId);
            var os = Model.GrisObject.GetById(objectStatusId);
            List<ScheduleResult> results = new List<ScheduleResult>();
            
            if (os.TypeId != 6) return null;

            var node = GetNode(os);
            var nodeName = node != null ? node.Name : "?" ;
            var system = GetSystem(os);
            var systemName = system != null ? system.Name : "?";
            var mailTo = new StringBuilder();
            var postfix = String.IsNullOrWhiteSpace(customError) ? "" : "_" + customError;

            foreach (var contact in contacts)
            {
                if (contact.SendSms && !string.IsNullOrWhiteSpace(contact.PhoneNumber))
                {
                    var message = "La periferica {0} della stazione {1} sistema {2} è in errore " + postfix + "";
                    try
                    {
                        message = File.ReadAllText(@".\Operations\device_sms_template" + postfix + ".txt");
                    }
                    catch (Exception ingnorException)
                    {
                    }

                    message = string.Format(message, os.Name, nodeName, systemName);

                    IOperationScheduler scheduler = new OperationScheduler();
                    results.Add(scheduler.Schedule("Sms"
                        , new Dictionary<string, string> { { "phoneNum", contact.PhoneNumber }, { "message", message } }
                        , false
                        , this.DontScheduleIfOlderThanMinutesSms
                        , null
                        ,  String.Format("PhoneNumber:{0},DeviceIds:{1}", contact.PhoneNumber, os.SpecificId), OperationScheduleCreateIfCheckType.ByKeys));
                } 
                
                if (contact.SendEMail && !string.IsNullOrWhiteSpace(contact.Email)) {
                    mailTo.Append(contact.Email).Append(",");
                }
            }
            if (mailTo.Length > 0)
            {
                var message = "La periferica {0} della stazione {1} sistema {2} è in errore " + postfix + "";
                try
                {
                    message = File.ReadAllText(@".\Operations\device_mail_template" + postfix + ".txt");
                }
                catch (Exception ingnorException)
                {
                }
                message = string.Format(message, os.Name, nodeName, systemName);

                IOperationScheduler scheduler = new OperationScheduler();
                results.Add( scheduler.Schedule("Mail"
                    , new Dictionary<string, string> { { "to", mailTo.ToString(0, mailTo.Length-1) }, { "message", message }, {"objectStatusId", objectStatusId.ToString()} }
                    , false
                    , this.DontScheduleIfOlderThanMinutesStreamFieldMail
                    , null
                    , String.Format("Email:{0},DeviceIds:{1}", mailTo.ToString(0, mailTo.Length - 1), os.SpecificId), OperationScheduleCreateIfCheckType.ByKeys));
            }

            return results;

        }

        private IGrisObject GetNode(IGrisObject node)
        {
            if (node == null) return null;
            var parent = node.Parent;
            while (parent != null && parent.TypeId !=  3 /*nodes = stazione*/ )
            {
                parent = parent.Parent;
            }
            return parent;
        }

        private IGrisObject GetSystem(IGrisObject node)
        {
            if (node == null) return null;
            var parent = node.Parent;
            while (parent != null && parent.TypeId != 4 /*nodes_systems = sistema*/ )
            {
                parent = parent.Parent;
            }
            return parent;
        }

        public ScheduleResult Schedule(string scheduleType, Dictionary<string, string> state, bool directExecution, string operationKeys = "")
        {
            IOperationScheduler scheduler = new OperationScheduler();
            return scheduler.Schedule(scheduleType, state, directExecution, operationKeys: operationKeys);
        }
    }
}
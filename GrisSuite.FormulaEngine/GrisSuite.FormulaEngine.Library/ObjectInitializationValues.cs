﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Library
{
	public class ObjectInitializationValues : Dictionary<string, object>
	{
		public ObjectInitializationValues ()
		{
			this.Add("ToBeEvaluated", true);
			this.Add("LookupInCacheOnly", false);
		}

		public bool ToBeEvaluated
		{
			get { return (bool) this["ToBeEvaluated"]; }
			set { this["ToBeEvaluated"] = value; }
		}

		public bool LookupInCacheOnly
		{
			get { return (bool) this["LookupInCacheOnly"]; }
			set { this["LookupInCacheOnly"] = value; }
		}
	}
}

﻿using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Library
{
    [Serializable]
	public class FormulaEngineException : Exception
    {
		public FormulaEngineException ()
        {
        }

		public FormulaEngineException ( string message )
			: base(message)
        {
        }

		public FormulaEngineException ( string message, Exception inner )
			: base(message, inner)
        {
        }

		protected FormulaEngineException ( SerializationInfo info, StreamingContext context )
			: base(info, context)
        {
        }
    }
}
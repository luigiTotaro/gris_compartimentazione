﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace GrisSuite.FormulaEngine.Library
{
    public class FormulaEngine : IFormulaEngine
    {
        private readonly ICollection<string> _searchPaths;
        private ScriptEngine _scriptEng;
        private IList<IObjectAttributeType> _availableAttributes;
        private IList<ISeverityRange> _severityRanges; 
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly Stopwatch _timer = new Stopwatch();
        private readonly Stopwatch _timerProgress = new Stopwatch();

        public int FormulaEvaluationWarningTimeout
        {
            get
            {
                int formulaEvaluationWarningTimeout;
                if ( ConfigurationManager.AppSettings["FormulaEvaluationWarningTimeout"] != null && 
                    int.TryParse(ConfigurationManager.AppSettings["FormulaEvaluationWarningTimeout"], out formulaEvaluationWarningTimeout) )
                {
                    return formulaEvaluationWarningTimeout;
                }

                return 500; // default applicativo stabilito in mezzo secondo
            }
        }

        public FormulaEngine () { }

        public FormulaEngine ( ICollection<string> searchPaths )
        {
            _searchPaths = searchPaths;
        }

        private void InitializeEngine ()
        {
            Dictionary<string, object> options = new Dictionary<string, object>();
#if DEBUG
			options["Debug"] = true;
#endif
			options["LightweightScopes"] = true;
            this._scriptEng = Python.CreateEngine(options);
            if ( this._searchPaths != null && this._searchPaths.Count > 0 ) this._scriptEng.SetSearchPaths(this._searchPaths);
        }

		public void Eval ( IList<EvaluablePackage> objectsToEval )
        {
            this.InitializeEngine();

            this._scriptEng.Runtime.LoadAssembly(typeof(IGrisObject).Assembly);
            this._scriptEng.Runtime.LoadAssembly(typeof(GrisObject).Assembly);

            var factory = new ObjectFactory();

            this._availableAttributes = factory.GetAttributeType().GetAll();
            this._severityRanges = factory.GetSeverityRange().GetAll();

            bool isFirstObjectByType = true;
            int lastType = 0;
            int count = objectsToEval.Count;
            double progress = 0;
            _timerProgress.Start();
            foreach ( var objectToEval in objectsToEval )
            {
                progress++;
                if (_timerProgress.ElapsedMilliseconds > 600000) // ogni 10 minuti fai vedere la % di completamenteno
                {
                    _logger.Info("Completamento {0:##.00}%", progress / count * 100);
                    _timerProgress.Restart();
                }
            	try
            	{
					if ( lastType != objectToEval.GrisObject.TypeId )
					{
						isFirstObjectByType = true;
						lastType = objectToEval.GrisObject.TypeId;
					}

                    // t odo: debug code, comment
                    //var o = objectToEval.GrisObject as IDevice;
                    //if ((o != null) && (o.SpecificId == 423911309427212295))
                    //{
                    //    Debugger.Break();
                    //}

					_timer.Restart();
					objectToEval.GrisObject.InitializeForEvaluation(
						new FormulaContext(this._scriptEng, this._severityRanges),
						this._availableAttributes,
						objectToEval.State
						).Evaluate();
					_timer.Stop();

					if ( _timer.ElapsedMilliseconds >= this.FormulaEvaluationWarningTimeout && !isFirstObjectByType )
						_logger.Warn("L'esecuzione della formula per l'oggetto con id {0}, di tipo {1}, ha impiegato {2} millisecondi.", objectToEval.GrisObject.Id, objectToEval.GrisObject.GetType(), _timer.ElapsedMilliseconds);

					this.LogObjectErrors(objectToEval.GrisObject);

					isFirstObjectByType = false;
				}
            	catch ( Exception exc )
            	{
					_logger.Error("Object type: {0}, Object id: {1}, error: {2}<br />ST: {3}", objectToEval.GrisObject.GetType(), objectToEval.GrisObject.SpecificId, exc.Message, exc.StackTrace);
				}
            }

            this._scriptEng.Runtime.Shutdown();
        }

        private void LogObjectErrors ( IGrisObject grisObject )
        {
            dynamic sd = grisObject.Attributes.Contains("SeverityDetail") ? grisObject.Attributes["SeverityDetail"] : "";
            dynamic s = grisObject.Attributes.Contains("Severity") ? grisObject.Attributes["Severity"] : "";
            
            List<string> errors = new List<string>();
            if ( grisObject.CustomEvaluationFormulas != null )
                foreach ( var customEvaluationFormula in grisObject.CustomEvaluationFormulas )
                    if (customEvaluationFormula.HasErrorInEvaluation) errors.Add(customEvaluationFormula.EvaluationError);

            if ( grisObject.DefaultEvaluationFormula.CanEvaluate && grisObject.DefaultEvaluationFormula.HasErrorInEvaluation ) errors.Add(grisObject.DefaultEvaluationFormula.EvaluationError);

            if ( errors.Count > 0 ) _logger.Error("Object type: {0}, Object id: {1}, severity: {2}, severitydetail: {3}, error: {4}", grisObject.GetType(), grisObject.SpecificId, s, sd, string.Join(", ", errors));
        }
    }
}

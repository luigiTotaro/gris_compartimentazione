﻿
using System;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using Attribute = GrisSuite.FormulaEngine.Model.Entities.ObjectAttributeType;

namespace GrisSuite.FormulaEngine.Library
{
	public class FakeFormulaEngine : IFormulaEngine
	{
		private ScriptEngine _scriptEng;
		private IList<EvaluablePackage> _objectsToEvalCache;

		private void InitializeEngine ()
		{
			Dictionary<string, object> options = new Dictionary<string, object>();
			options["Debug"] = true;
			options["LightweightScopes"] = true;
			this._scriptEng = Python.CreateEngine(options);
		}

		public void Eval ( IList<EvaluablePackage> objectsToEval )
		{
			this.InitializeEngine();

			this._objectsToEvalCache = objectsToEval;
			this._scriptEng.Runtime.LoadAssembly(typeof(IGrisObject).Assembly);
			this._scriptEng.Runtime.LoadAssembly(typeof(GrisObject).Assembly);
			this._scriptEng.SetSearchPaths(new List<string> { ".\\Lib" });

			foreach ( var objectToEval in objectsToEval ) objectToEval.GrisObject.InitializeForEvaluation(
																								new FormulaContext(this._scriptEng, this.BuildSeverityRanges()),
																								this.BuildTypeAttributes(),
																								objectToEval.State
																								).Evaluate();

			this._scriptEng.Runtime.Shutdown();
			//AppDomain.Unload(this._scriptAppDomain);
		}

		public void SetContext ( object sender, EventArgs e )
		{
			if ( sender != null && sender is IGrisObject )
			{
				var go = (IGrisObject) sender;
				var cachedGo = this._objectsToEvalCache.SingleOrDefault(gp => gp.GrisObject.Id == go.Id);

				go.InitializeForEvaluation(new FormulaContext(this._scriptEng, this.BuildSeverityRanges()), this.BuildTypeAttributes(),
										   ( ( cachedGo != null ) ? cachedGo.State : null ));
			}
		}

		public IList<IObjectAttributeType> BuildTypeAttributes ()
		{
			return new List<IObjectAttributeType>
			       	{
			       		new Attribute
			       			{
			       				Id = 1,
			       				Name = "Severity"
			       			},			       		
						new Attribute
			       			{
			       				Id = 2,
			       				Name = "SeverityDetail"
			       			},
						new Attribute
			       			{
			       				Id = 3,
			       				Name = "RealSeverity"
			       			},
						new Attribute
			       			{
			       				Id = 4,
			       				Name = "RealSeverityDetail"
			       			},
						new Attribute
			       			{
			       				Id = 5,
			       				Name = "LastSeverity"
			       			},
						new Attribute
			       			{
			       				Id = 6,
			       				Name = "LastSeverityDetail"
			       			},
                        new Attribute
			       			{
			       				Id = 7,
			       				Name = "ExcludeFromParentStatus"
			       			},
                        new Attribute
			       			{
			       				Id = 8,
			       				Name = "Color"
			       			},
                        new Attribute
			       			{
			       				Id = 9,
			       				Name = "ForcedSevLevel"
			       			}
			       	};
		}

		public IList<ISeverityRange> BuildSeverityRanges ()
		{
			return new List<ISeverityRange>
			       	{
			       		new SeverityRange
			       			{
			       				Id = 1,
			       				Name = "In Servizio",
								SeverityWeight = 0m,
								MinValue = 0,
								MaxValue = 3
			       			},			       		
						new SeverityRange
			       			{
			       				Id = 2,
			       				Name = "Anomalia Lieve",
								SeverityWeight = 4.4m,
								MinValue = 3,
								MaxValue = 7
			       			},
						new SeverityRange
			       			{
			       				Id = 3,
			       				Name = "Anomalia Grave",
								SeverityWeight = 10m,
								MinValue = 7,
								MaxValue = 10
			       			}
			       	};
		}
	}
}
# -*- coding: latin-1 -*-
import clr
import sys

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Contracts.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import *
from Infrastructure.Notifications import SetupEnvNotifications, SendEnvNotifications

if isinstance(obj, GrisObject):
	SetupEnvNotifications(obj, operations)

	if obj.Children.Count == 0:
		obj.ExcludedFromParentStatus = True
		obj.SeverityDetail = NotAvailable.SeverityDetail
	else:
		obj.ExcludedFromParentStatus = False
		if obj.Children.AreAllInSeverity(NotActive.Severity, True, NotAvailable.Severity):
			obj.ExcludedFromParentStatus = True
			obj.SeverityDetail = NotActive.SeverityDetail
		elif obj.Children.AreAllInSeverity(Ok.Severity):
			obj.SeverityDetail = Ok.SeverityDetail
		else:
			obj.SeverityDetail = NotCompletelyOk.SeverityDetail

	SendEnvNotifications(obj, operations)
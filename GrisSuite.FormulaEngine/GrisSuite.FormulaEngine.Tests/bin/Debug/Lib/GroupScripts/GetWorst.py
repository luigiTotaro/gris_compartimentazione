# -*- coding: latin-1 -*-
# script di calcolo dello stato che assegna lo stato peggiore tra quelli dei figli
import clr
import sys

from GrisSuite.FormulaEngine.Model import *
from GrisSuite.FormulaEngine.Common.Severities import *
from Infrastructure.Notifications import SetupEnvNotifications, SendEnvNotifications

if isinstance(obj, GrisObject):
	SetupEnvNotifications(obj, operations)

	if obj.Children.Count == 0:
		obj.ExcludedFromParentStatus = True
		obj.SeverityDetail = NotAvailable.SeverityDetail
	else:
		obj.ExcludedFromParentStatus = False
		if obj.Children.AreAllInSeverity(NotActive.Severity, True, NotAvailable.Severity):
			obj.ExcludedFromParentStatus = True
			obj.SeverityDetail = NotActive.SeverityDetail
		else:
			obj.SeverityDetail = obj.Children.GetWorstSeverityDetail(NotActive.SeverityDetail, Unknown.SeverityDetail)

	SendEnvNotifications(obj, operations)
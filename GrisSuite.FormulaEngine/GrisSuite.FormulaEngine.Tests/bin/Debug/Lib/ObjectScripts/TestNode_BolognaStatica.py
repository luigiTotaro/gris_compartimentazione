
import sys
import clr
import System

sys.path.append('..')
clr.AddReference('System.Drawing')

from System.Drawing import Color, ColorTranslator

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, Node):
    clr.Convert(obj, Node)

    obj = GrisObjectPxy(obj, state)
        
    obj.Color = ColorTranslator.FromHtml('#408ACA')
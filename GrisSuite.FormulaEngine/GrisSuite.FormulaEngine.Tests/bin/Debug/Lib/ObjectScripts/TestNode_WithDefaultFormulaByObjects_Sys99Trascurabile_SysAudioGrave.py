# -*- coding: latin-1 -*-
import sys
import clr
import System

sys.path.append('..')

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *
from GrisSuite.FormulaEngine.Common.Contracts.Entities import Systems

from Infrastructure.MetaClasses import GrisObjectPxy
from Infrastructure.GrisSystemFormulas import *

if isinstance(obj, Node):
	clr.Convert(obj, Node)

	gobj = obj
	obj = GrisObjectPxy(obj, state)
	totals = SubObjectTotals(gobj, SeverityWeights())

	if (obj.GetSystemBySystemId(Systems.AltrePeriferiche) and obj.GetSystemBySystemId(Systems.AltrePeriferiche).Severity == Warning.Severity):
		totals.WarningTrascurabileTotal += 1
	if (obj.GetSystemBySystemId(Systems.AltrePeriferiche) and obj.GetSystemBySystemId(Systems.AltrePeriferiche).Severity == Error.Severity):
		totals.ErrorTrascurabileTotal += 1
	if (obj.GetSystemBySystemId(Systems.AltrePeriferiche) and obj.GetSystemBySystemId(Systems.AltrePeriferiche).Severity == Offline.Severity):
		totals.OfflineTrascurabileTotal += 1

	if (obj.GetSystemBySystemId(Systems.DiffusioneSonora) and obj.GetSystemBySystemId(Systems.DiffusioneSonora).Severity == Warning.Severity):
		totals.WarningGraveTotal += 1
	if (obj.GetSystemBySystemId(Systems.DiffusioneSonora) and obj.GetSystemBySystemId(Systems.DiffusioneSonora).Severity == Error.Severity):
		totals.ErrorGraveTotal += 1
	if (obj.GetSystemBySystemId(Systems.DiffusioneSonora) and obj.GetSystemBySystemId(Systems.DiffusioneSonora).Severity == Offline.Severity):
		totals.OfflineGraveTotal += 1

	obj.Severity = GetSeverityIndexByWeights(totals, SevByObjectRanges())
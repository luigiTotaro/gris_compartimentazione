
import sys
import clr
import System

sys.path.append('..')

from datetime import date, datetime, timedelta

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from System import DateTime, Nullable

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, Device):
    clr.Convert(obj, Device)

    obj = GrisObjectPxy(obj, state)
        
    if obj.Type == "TT10220":
        dateFrom = DateTime(2011, 12, 1, 20, 0, 0)
        dateTo = DateTime(2011, 12, 31, 23, 59, 59)
        eventDescVals = reduce(lambda x, y: x + y, map(lambda x: x.EventDescriptionValue.split(","), filter(lambda x: x.EventDescription == "ESITO TEST PERIODICO", obj.GetRaisedEvents(dateFrom, dateTo))), [])
        allOk = reduce(lambda x, y: x & y, map(lambda x: x.strip().find(":OK") > -1, eventDescVals), True)
        if len(eventDescVals) > 0 and allOk:
            obj.SeverityDetail = Ok.SeverityDetail
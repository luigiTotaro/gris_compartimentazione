
import sys
import clr

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import *

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.Children.AreAllAtLeastInSeverity(Warning.Severity):
        obj.Severity = obj.Children.GetBestSeverity()
        obj.Parent.ForceSeverity(obj.Severity) # sistema
        obj.Parent.Parent.ForceSeverity(obj.Severity) # stazione
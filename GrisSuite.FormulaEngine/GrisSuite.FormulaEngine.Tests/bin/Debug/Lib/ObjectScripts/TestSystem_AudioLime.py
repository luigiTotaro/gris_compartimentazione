
import sys
import clr
import System

sys.path.append('..')
clr.AddReference('System.Drawing')

from System.Drawing import Color, ColorTranslator

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, NodeSystem):
    clr.Convert(obj, NodeSystem)

    obj = GrisObjectPxy(obj, state)
        
    if obj.PZ1.SeverityDetail >= Error.SeverityDetail:
        obj.SeverityDetail = obj.PZ1.SeverityDetail
        obj.Color = ColorTranslator.FromHtml('#BFFF00')
        node = obj.Node
        node.ForceSeverity(obj.Severity)

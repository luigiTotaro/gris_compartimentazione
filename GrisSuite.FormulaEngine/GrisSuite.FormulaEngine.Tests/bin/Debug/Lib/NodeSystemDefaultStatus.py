# -*- coding: latin-1 -*-
# script di calcolo dello stato di default per gli oggetti NodeSystem
import clr
import sys

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Contracts.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage, SeverityWeightsAndRanges
from Infrastructure.Notifications import SetupEnvNotifications, SendEnvNotifications
from Infrastructure.MetaClasses import *

if isinstance(obj, NodeSystem):
	SetupEnvNotifications(obj, operations)
	obj = GrisObjectPxy(obj, state)
			
	if obj.GetMonitoringServersInSeverity(IncompleteServerData.SeverityDetail).Count > 0:
		obj.Severity = Error.Severity
		obj.ForcedSeverityLevel = ForcedSeverityLevel.IncompleteServerData
	else:
		if obj.Node.AreAllNodeServersOffline():
			obj.Severity = Unknown.Severity
			obj.ForcedSeverityLevel = ForcedSeverityLevel.STLC1000ServerNotReachable
		else:
			worstStatus = obj.GetWorstSeverityForDevicesOfType(["TT10210", "TT10210V3", "TT10220", "TT10230IP", "TT10230_000"])
			if obj.SystemId == Systems.DiffusioneSonora and worstStatus != None and worstStatus in [Warning.Severity, Error.Severity, Unknown.Severity, Offline.Severity]:
				obj.ForcedSeverityLevel = ForcedSeverityLevel.AtLeastOnePZInError
				if worstStatus > Error.Severity:
					obj.Severity = Error.Severity
				else:
					obj.Severity = worstStatus
			else:
				obj.ForcedSeverityLevel = ForcedSeverityLevel.NotForced
				if obj.Devices.HasNotExcluded():
					weightsAndRanges = SeverityWeightsAndRanges(sevRanges)
					obj.Severity = GetWeightedAverage(
						obj.Devices.GetInSeverity(Ok.Severity).Count, 
						obj.Devices.GetInSeverity(Warning.Severity).Count, 
						obj.Devices.GetInSeverity(Error.Severity).Count + obj.Devices.GetInSeverity(Offline.Severity).Count + obj.Devices.GetInSeverity(Unknown.Severity).Count, 
						obj.Devices.GetInSeverity(NotActive.Severity, True).Count,
						weightsAndRanges)
				else:
					obj.SeverityDetail = NotActive.SeverityDetail
					obj.ExcludedFromParentStatus = True
		
	if obj.SystemId == Systems.AltrePeriferiche or obj.SystemId == Systems.SistemaIlluminazione or obj.SystemId == Systems.SistemaTelefonia or obj.SystemId == Systems.SistemaFDS or obj.SystemId == Systems.MonitoraggioVPNVerde or obj.SystemId == Systems.WebRadio or obj.SystemId == Systems.Orologi or obj.SystemId == Systems.Ascensori:
		obj.ExcludedFromParentStatus = True

	SendEnvNotifications(obj, operations)
﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.Tests
{
	public static class EntityFactory
	{
		public static Region GetRegion ( bool toBeEvaluated = true )
		{
			var initVals = new ObjectInitializationValues { ToBeEvaluated = toBeEvaluated, LookupInCacheOnly = true };
			var entity = new Region(initVals);
			SetupGrisObject(entity);

			return entity;
		}

		public static Zone GetZone ( bool toBeEvaluated = true )
		{
			var initVals = new ObjectInitializationValues { ToBeEvaluated = toBeEvaluated, LookupInCacheOnly = true };
			var entity = new Zone(initVals);
			SetupGrisObject(entity);

			return entity;
		}

		public static Node GetNode ( bool toBeEvaluated = true )
		{
			var initVals = new ObjectInitializationValues { ToBeEvaluated = toBeEvaluated, LookupInCacheOnly = true };
			var entity = new Node(initVals);
			SetupGrisObject(entity);

			return entity;
		}

		public static NodeSystem GetNodeSystem ( bool toBeEvaluated = true )
		{
			var initVals = new ObjectInitializationValues { ToBeEvaluated = toBeEvaluated, LookupInCacheOnly = true };
			var entity = new NodeSystem(initVals);
			SetupGrisObject(entity);

			return entity;
		}

		public static VirtualObject GetVirtualObject ( bool toBeEvaluated = true )
		{
			var initVals = new ObjectInitializationValues { ToBeEvaluated = toBeEvaluated, LookupInCacheOnly = true };
			var entity = new VirtualObject(initVals);
			SetupGrisObject(entity);

			return entity;
		}

		public static Server GetServer ( bool toBeEvaluated = true )
		{
			var initVals = new ObjectInitializationValues { ToBeEvaluated = toBeEvaluated, LookupInCacheOnly = true };
			var entity = new Server(initVals);
			SetupGrisObject(entity);

			return entity;
		}

		public static Device GetDevice ( bool toBeEvaluated = true )
		{
			var initVals = new ObjectInitializationValues { ToBeEvaluated = toBeEvaluated, LookupInCacheOnly = true };
			var entity = new Device(initVals);
			SetupGrisObject(entity);

			entity.Streams = new List<IStream>();
			return entity;
		}

		public static Stream GetStream ()
		{
			var entity = new Stream();
			entity.StreamFields = new List<IStreamField>();

			return entity;
		}

		public static StreamHistory GetStreamHistory ()
		{
			return new StreamHistory();
		}

		public static StreamField GetStreamField ()
		{
			var entity = new StreamField();

			return entity;
		}

		public static StreamFieldHistory GetStreamFieldHistory ()
		{
			return new StreamFieldHistory();
		}

		public static void SetupGrisObject ( GrisObject grisObject )
		{
			grisObject.Id = Guid.NewGuid();

			InitializeGrisObjectAttributes(grisObject);

			grisObject.ObjectFormulas = new List<IObjectFormula>();

			grisObject.IsNavigationActive = false;
		}

		public static void InitializeGrisObjectAttributes ( GrisObject grisObject )
		{
			grisObject.Attributes = new GrisAttributeCollection(grisObject, new FakeFormulaEngine().BuildTypeAttributes(), true);
			grisObject.Attributes.Initialize();
		}
	}
}

﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.ObjectCache
{
	[TestFixture]
	public class ObjectCacheTests
	{
		[Test]
		public void QuandoPeriferica_E_Server_AggiuntiAObjectCache_MiAspettoServerNonNullo ()
		{
			// initialization
			var objs = new List<GrisObject>();
			var dev = new Model.Entities.Device();
			var srv = new Model.Entities.Server();

			// setup
			Utility.SetupEntities(dev, srv);

			// relations
			dev.ServerId = srv.Id;
			objs.Add(srv);
			objs.Add(dev);

			// act
			dev.IsNavigationActive = true;
			GrisObject.Cache.AddObjects(objs);

			// assert
			Assert.IsNotNull(dev.Server);
		}

		[Test]
		public void QuandoPeriferica_AggiuntaComeFigliaASistema_MiAspettoCollezioneFigliNonNullaEUgualeA1 ()
		{
			// initialization
			var sys = new Model.Entities.NodeSystem();
			var dev = new Model.Entities.Device();

			// setup
			Utility.SetupEntities(dev, sys);

			// act
			dev.System = sys;
			sys.IsNavigationActive = true;

			// assert
			Assert.IsNotNull(sys.Children);
			Assert.IsTrue(sys.Children.Count == 1);
		}

		[Test]
		public void QuandoPeriferica_AggiuntaComeFigliaASistema_MiAspettoParentNonNull ()
		{
			// initialization
			var sys = new Model.Entities.NodeSystem();
			var dev = new Model.Entities.Device();

			// setup
			Utility.SetupEntities(dev, sys);

			// act
			sys.Devices.AddEntity(dev);
			dev.IsNavigationActive = true;

			// assert
			Assert.IsNotNull(dev.Parent);
		}

		[Test]
		public void QuandoPeriferica_èRaggruppataInVirtualObjectSottoSistema_MiAspettoParentNonNulleSystemNonNull ()
		{
			// initialization
			var sys = EntityFactory.GetNodeSystem(false);
			var vo = EntityFactory.GetVirtualObject(false);
			var dev = EntityFactory.GetDevice(false);

			// setup
			dev.ParentId = vo.Id;
			dev.NodeSystemId = sys.Id;

			// act
			var objects = new List<IGrisObject>(3);
			objects.Add(dev);
			objects.Add(vo);
			objects.Add(sys);

			GrisObject.Cache.Reset();
			GrisObject.Cache.AddObjects(objects);

			dev.IsNavigationActive = true;

			// assert
			Assert.IsNotNull(dev.Parent);
			Assert.IsNotNull(dev.System);
		}
	}
}

﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common;
using GrisSuite.FormulaEngine.Model.Entities;
using GrisSuite.FormulaEngine.ScheduledOperations;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.ScheduledOperations
{
	[TestFixture]
	public class OperationsTests
	{
		[Test]
		public void QuandoSmsExecutor_CaricaCorrettamenteAssembly_MiAspettoSuccessFalse ()
		{
			const string PHONE_NUMBER = "+39045556677";
			const string MESSAGE = "Ciao com'ela?";
		    const string FAKE_WEB_SERVICE_URL = "http://localhost/mgwc/ComSmsService/";
            const string FAKE_WEB_SERVICE_URL_BACKUP = "http://localhost:8081/mgwc/ComSmsService/";

			IOperationExecutorFactory executorFatory = new OperationExecutorFactory(); // factory di default

            var schedType = new OperationScheduleType();
            schedType.Id = 1;
            schedType.Code = "Sms";
            schedType.AssemblyName = "GrisSuite.FormulaEngine.Sms"; //dev'esserci l'assembly 

            var schedule = new OperationSchedule();
            schedule.Id = Guid.NewGuid();
            schedule.Parameters = new Dictionary<string, string> { { "phoneNum", PHONE_NUMBER }, { "message", MESSAGE }, { "url", FAKE_WEB_SERVICE_URL }, { "urlBackup", FAKE_WEB_SERVICE_URL_BACKUP } };
		    schedule.Created = DateTime.Now;
		    schedule.Type = schedType;

            // La chiamata usa un web service locale inesistente, che quindi andrà in errore, loggato e riportato come
            // invio fallito, nel result. Non permette di testare l'effettivo invio e la lettura della configurazione da produzione,
            // ma è ritenuto sufficiente per la coverage. L'invio di SMS dal Gateway RFI è accessibile solo da alcune macchine di produzione.
			var executor = executorFatory.GetExecutor(schedType);
            var result = executor.Exec(schedule, null);

			Assert.IsNotNull(result);
			Assert.AreEqual(OperationScheduleResultStatus.ShouldRetry, result.Status);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void QuandoSmsExecutor_CaricaCorrettamenteAssembly_AcuiNonVienePassatoParamState_MiAspettoArgumentException ()
		{
			IOperationExecutorFactory executorFatory = new OperationExecutorFactory(); // factory di default

			var schedType = new OperationScheduleType();
			schedType.Id = 1;
			schedType.Code = "Sms";
			schedType.AssemblyName = "GrisSuite.FormulaEngine.Sms"; //dev'esserci l'assembly 

            var schedule = new OperationSchedule();
            schedule.Id = Guid.NewGuid();
            schedule.Created = DateTime.Now;
            schedule.Type = schedType;

			var executor = executorFatory.GetExecutor(schedType);
			var result = executor.Exec(schedule, null);

            Assert.AreEqual(OperationScheduleResultStatus.Error, result.Status);
		}

        [Test]
        public void QuandoStreamFieldMailExecutor_CaricaCorrettamenteAssembly_MiAspettoSuccessTrue()
        {
            const long nodeSysId = 1;

           IOperationExecutorFactory executorFatory = new OperationExecutorFactory(); // factory di default

            var schedType = new OperationScheduleType();
            schedType.Id = 3;
            schedType.Code = "StreamFieldMail";
            schedType.AssemblyName = "GrisSuite.FormulaEngine.StreamFieldMail"; //dev'esserci l'assembly 

            var schedule = new OperationSchedule();
            schedule.Id = Guid.NewGuid();
            schedule.Parameters = new Dictionary<string, string> { { "nodeSysId", nodeSysId.ToString() } };
            schedule.Created = DateTime.Now;
            schedule.Type = schedType;

            var executor = executorFatory.GetExecutor(schedType);
            var result = executor.Exec(schedule, null);

            Assert.IsNotNull(result);
            // La mail non parte, perché il sistema non ha contatti associati, test poco veritiero, ma si verifica solo il caricamento ed esecuzione DLL
            // Assert.IsTrue(result);
        }
    }
}

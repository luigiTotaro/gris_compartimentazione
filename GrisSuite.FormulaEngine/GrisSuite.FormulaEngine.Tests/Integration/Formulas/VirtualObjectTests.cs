﻿
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
    [TestFixture]
    public class VirtualObjectTests
    {
		[TestFixtureSetUp]
		public void SetUp ()
		{
			GrisObject.Cache.Reset();
		}

		#region Cluster di PZ

        [Test]
        public void QuandoVirtualObjectèClusterdi_PerifInAttenz_E_PerifInError_MiAspettoVirtualObjectInAttenzESistemaInAttenzEStazioneInAttenz()
        {
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var cluster = EntityFactory.GetVirtualObject();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();

            // severities
            dev1.SeverityDetail = Warning.SeverityDetail;
            dev2.SeverityDetail = Error.SeverityDetail;

            // relations
            cluster.Children.AddEntity(dev1);
            cluster.Children.AddEntity(dev2);
            cluster.Parent = system;
            system.Parent = node;

            // formulas
			Utility.SetFormula(cluster, @".\Lib\ObjectScripts\TestVirtualObject_Cluster.py");

            // act
			Utility.Eval(cluster);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(cluster));

            Assert.AreEqual(Severity.Warning, cluster.Severity);
            Assert.AreEqual(Severity.Warning, system.Severity);
            Assert.AreEqual(Severity.Warning, node.Severity);
        }

        #endregion

        #region Gruppo critico

        [Test]
        public void
            QuandoVirtualObjectèRaggruppamentoDi_AlmenoUnaPerifInErroreOSuperioreDiCuiLaPeggioreInOffline_MiAspettoVirtualObjectInOfflineEStazioneInOffline
            ()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var gruppoCritico = EntityFactory.GetVirtualObject();
            var node = EntityFactory.GetNode();

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Offline.SeverityDetail;
            system.SeverityDetail = Error.SeverityDetail;

            // relations
            gruppoCritico.Children.AddEntity(dev1);
            gruppoCritico.Children.AddEntity(dev2);
            gruppoCritico.Children.AddEntity(dev3);
            gruppoCritico.Children.AddEntity(system);
            gruppoCritico.Parent = node;

            // formulas
			Utility.SetFormula(gruppoCritico, @".\Lib\ObjectScripts\TestVirtualObject_CriticalGroup.py");

			// act
			Utility.Eval(gruppoCritico);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(gruppoCritico));

            Assert.AreEqual(Offline.SeverityDetail, gruppoCritico.SeverityDetail);
            Assert.AreEqual(Severity.Offline, node.Severity);
        }

        #endregion

		#region Raggruppamenti come da specifiche funzionali (GrisGestioneRaggruppamentiEFormule_V3.pdf)

		#region Raggruppamento dispositivi generici: se un virtual object con formula dei "dispositivi generici" associata, ha 1 periferica in ok, 1 in warning, 4 in error e nessuna non attiva -> il virtual object dev'essere giallo (con media pesata standard sarebbe stato rosso)

		[Test]
		public void QuandoVirtualObject_ha6Periferiche_SeverityOk_SeverityWarning_SeverityError_SeverityError_SeverityError_SeverityError_MiAspettoWarning ()
		{
			// initialization
			var devOk = EntityFactory.GetDevice(false);
			var devWar = EntityFactory.GetDevice(false);
			var devErr1 = EntityFactory.GetDevice(false);
			var devErr2 = EntityFactory.GetDevice(false);
			var devErr3 = EntityFactory.GetDevice(false);
			var devErr4 = EntityFactory.GetDevice(false);
			var vo = EntityFactory.GetVirtualObject();

			// severities
			devOk.SeverityDetail = Ok.SeverityDetail;
			devWar.SeverityDetail = Warning.SeverityDetail;
			devErr1.SeverityDetail = Error.SeverityDetail;
			devErr2.SeverityDetail = Error.SeverityDetail;
			devErr3.SeverityDetail = Error.SeverityDetail;
			devErr4.SeverityDetail = Error.SeverityDetail;

			// relations
			devOk.Parent = vo;
			devWar.Parent = vo;
			devErr1.Parent = vo;
			devErr2.Parent = vo;
			devErr3.Parent = vo;
			devErr4.Parent = vo;
			vo.Children.AddEntity(devOk);
			vo.Children.AddEntity(devWar);
			vo.Children.AddEntity(devErr1);
			vo.Children.AddEntity(devErr2);
			vo.Children.AddEntity(devErr3);
			vo.Children.AddEntity(devErr4);

			// formulas
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GetWeightedAverageMaxWarning.py");

			// act
			Utility.Eval(vo);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo));

			Assert.AreEqual(Warning.Severity, vo.Severity);
		}

		#endregion

		#region Formula "1 o più periferiche in errore / offline > Contenitore in Errore"

		[Test]
		public void QuandoVirtualObject_haFormula_xxxGroupWith_AnyElemIn_E_O_Get_Exxx_ha3Periferiche_SeverityOk_SeverityOk_SeverityError_MiAspettoError ()
		{
			// initialization
			var devOk1 = EntityFactory.GetDevice(false);
			var devOk2 = EntityFactory.GetDevice(false);
			var devErr1 = EntityFactory.GetDevice(false);
			var vo = EntityFactory.GetVirtualObject();

			// severities
			devOk1.SeverityDetail = Ok.SeverityDetail;
			devOk2.SeverityDetail = Ok.SeverityDetail;
			devErr1.SeverityDetail = Error.SeverityDetail;

			// relations
			Utility.SetBaseRelation(vo, devOk1);
			Utility.SetBaseRelation(vo, devOk2);
			Utility.SetBaseRelation(vo, devErr1);

			// formulas
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AnyElemIn_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");

			// act
			Utility.Eval(vo);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo));

			Assert.AreEqual(Error.Severity, vo.Severity);
		}

		[Test]
		public void QuandoVirtualObject_haFormula_xxxGroupWith_AnyElemIn_E_O_Get_Exxx_ha3Periferiche_SeverityOk_SeverityOk_SeverityOk_MiAspettoOk ()
		{
			// initialization
			var devOk1 = EntityFactory.GetDevice(false);
			var devOk2 = EntityFactory.GetDevice(false);
			var devOk3 = EntityFactory.GetDevice(false);
			var vo = EntityFactory.GetVirtualObject();

			// severities
			devOk1.SeverityDetail = Ok.SeverityDetail;
			devOk2.SeverityDetail = Ok.SeverityDetail;
			devOk3.SeverityDetail = Ok.SeverityDetail;

			// relations
			Utility.SetBaseRelation(vo, devOk1);
			Utility.SetBaseRelation(vo, devOk2);
			Utility.SetBaseRelation(vo, devOk3);

			// formulas
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AnyElemIn_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");

			// act
			Utility.Eval(vo);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo));

			Assert.AreEqual(Ok.Severity, vo.Severity);
		}

		[Test]
		public void QuandoVirtualObject_haFormula_xxxGroupWith_AnyElemIn_E_O_Get_Exxx_ha3Periferiche_SeverityNotActive_SeverityNotActive_SeverityNotActive_MiAspettoNotActive ()
		{
			// initialization
			var devNa1 = EntityFactory.GetDevice(false);
			var devNa2 = EntityFactory.GetDevice(false);
			var devNa3 = EntityFactory.GetDevice(false);
			var vo = EntityFactory.GetVirtualObject();

			// severities
			devNa1.SeverityDetail = NotActive.SeverityDetail;
			devNa2.SeverityDetail = NotActive.SeverityDetail;
			devNa3.SeverityDetail = NotActive.SeverityDetail;

			// relations
			Utility.SetBaseRelation(vo, devNa1);
			Utility.SetBaseRelation(vo, devNa2);
			Utility.SetBaseRelation(vo, devNa3);

			// formulas
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AnyElemIn_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");

			// act
			Utility.Eval(vo);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo));

			Assert.AreEqual(NotActive.Severity, vo.Severity);
		}

		[Test]
		public void QuandoVirtualObject_haFormula_xxxGroupWith_AnyElemIn_E_O_Get_Exxx_ha3Periferiche_SeverityOk_SeverityOk_SeverityWarning_MiAspettoNotCompletelyOk ()
		{
			// initialization
			var devOk1 = EntityFactory.GetDevice(false);
			var devOk2 = EntityFactory.GetDevice(false);
			var devWar1 = EntityFactory.GetDevice(false);
			var vo = EntityFactory.GetVirtualObject();

			// severities
			devOk1.SeverityDetail = Ok.SeverityDetail;
			devOk2.SeverityDetail = Ok.SeverityDetail;
			devWar1.SeverityDetail = Warning.SeverityDetail;

			// relations
			Utility.SetBaseRelation(vo, devOk1);
			Utility.SetBaseRelation(vo, devOk2);
			Utility.SetBaseRelation(vo, devWar1);

			// formulas
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AnyElemIn_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");

			// act
			Utility.Eval(vo);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo));

			Assert.AreEqual(NotCompletelyOk.SeverityDetail, vo.SeverityDetail);
		}

		[Test]
		public void QuandoNode_haServerInMaint_MiAspettoRaggruppamentiESistemiNotActive ()
		{
			// initialization
			var server = EntityFactory.GetServer();
			var devAlim = EntityFactory.GetDevice();
			var devPz = EntityFactory.GetDevice();
			var devAmp1 = EntityFactory.GetDevice();
			var devAmp2 = EntityFactory.GetDevice();
			var devTds = EntityFactory.GetDevice();
			var devMonitor = EntityFactory.GetDevice();
			var devStlc = EntityFactory.GetDevice();
			var voAudioGenerici = EntityFactory.GetVirtualObject();
			var voAudioFondamentali = EntityFactory.GetVirtualObject();
			var voAudioImportanti = EntityFactory.GetVirtualObject();
			var voAudioComplementari = EntityFactory.GetVirtualObject();
			var voVideoGenerici = EntityFactory.GetVirtualObject();
			var voVideoFondamentali = EntityFactory.GetVirtualObject();
			var sysAudio = EntityFactory.GetNodeSystem();
			var sysVideo = EntityFactory.GetNodeSystem();
			var sysDiagn = EntityFactory.GetNodeSystem();
			var node = EntityFactory.GetNode();

			// entity data
			sysAudio.Name = "Diffusione sonora";
			sysVideo.Name = "Informazione visiva";
			sysDiagn.Name = "Diagnostica";
			server.InMaintenance = true;
			server.ForcedByUser = true;

			voAudioGenerici.Name = "Generici";
			voAudioFondamentali.Name = "Fondamentali";
			voAudioImportanti.Name = "Importanti";
			voAudioComplementari.Name = "Complementari";
			voVideoGenerici.Name = "Generici";
			voVideoFondamentali.Name = "Fondamentali";

			devAlim.Name = "Sistema Alimentazione";
			devPz.Name = "Pannello Zone";
			devAmp1.Name = "Amplificatore (1)";
			devAmp2.Name = "Amplificatore (2)";
			devTds.Name = "Pannello TDS";
			devMonitor.Name = "Monitor Sala Attesa";
			devStlc.Name = "STLC1000";

            devAlim.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            devPz.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            devAmp1.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            devAmp2.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            devTds.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            devMonitor.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            devStlc.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

			// relations
			Utility.SetBaseRelation(voAudioFondamentali, devAlim);
			Utility.SetBaseRelation(voAudioFondamentali, devPz);
			Utility.SetBaseRelation(voAudioImportanti, devAmp1);
			Utility.SetBaseRelation(voAudioImportanti, devAmp2);
			Utility.SetBaseRelation(voAudioComplementari, devTds);
			Utility.SetBaseRelation(voVideoFondamentali, devMonitor);
			Utility.SetBaseRelation(sysAudio, voAudioGenerici);
			Utility.SetBaseRelation(sysAudio, voAudioFondamentali);
			Utility.SetBaseRelation(sysAudio, voAudioImportanti);
			Utility.SetBaseRelation(sysAudio, voAudioComplementari);
			Utility.SetBaseRelation(sysVideo, voVideoGenerici);
			Utility.SetBaseRelation(sysVideo, voVideoFondamentali);
			Utility.SetBaseRelation(sysDiagn, devStlc);
			Utility.SetBaseRelation(node, sysAudio);
			Utility.SetBaseRelation(node, sysVideo);
			Utility.SetBaseRelation(node, sysDiagn);

			devAlim.SetSystem(sysAudio, false);
			devPz.SetSystem(sysAudio, false);
			devAmp1.SetSystem(sysAudio, false);
			devAmp2.SetSystem(sysAudio, false);
			devTds.SetSystem(sysAudio, false);
			devMonitor.SetSystem(sysVideo, false);

			devStlc.System = sysDiagn;
			sysDiagn.Devices.AddEntity(devStlc);

			devAlim.Server = server;
			devPz.Server = server;
			devAmp1.Server = server;
			devAmp2.Server = server;
			devTds.Server = server;
			devMonitor.Server = server;
			devStlc.Server = server;

			sysDiagn.Node = node;

			// formulas
			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");
			Utility.SetFormula(devAlim, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(devPz, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(devAmp1, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(devAmp2, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(devTds, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(devMonitor, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(devStlc, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(voAudioGenerici, @".\Lib\GroupScripts\GetWeightedAverageMaxWarning.py");
			Utility.SetFormula(voAudioFondamentali, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(voAudioFondamentali, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(voAudioImportanti, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(voAudioImportanti, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_W.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(voAudioComplementari, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(voVideoGenerici, @".\Lib\GroupScripts\GetWeightedAverageMaxWarning.py");
			Utility.SetFormula(voVideoFondamentali, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(voVideoFondamentali, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(sysAudio, @".\Lib\GroupScripts\GetWorst.py");
			Utility.SetFormula(sysVideo, @".\Lib\GroupScripts\GetWorst.py");
			Utility.SetFormula(sysDiagn, @".\Lib\NodeSystemDefaultStatus.py");
			Utility.SetFormula(node, @".\Lib\GroupScripts\GetWorst.py");

			// navigation
			devAlim.IsNavigationActive = true;
			devPz.IsNavigationActive = true;
			devAmp1.IsNavigationActive = true;
			devAmp2.IsNavigationActive = true;
			devTds.IsNavigationActive = true;
			devMonitor.IsNavigationActive = true;
			devStlc.IsNavigationActive = true;

			voAudioGenerici.IsNavigationActive = true;
			voAudioFondamentali.IsNavigationActive = true;
			voAudioImportanti.IsNavigationActive = true;
			voAudioComplementari.IsNavigationActive = true;
			voVideoGenerici.IsNavigationActive = true;
			voVideoFondamentali.IsNavigationActive = true;

			sysAudio.IsNavigationActive = true;
			sysVideo.IsNavigationActive = true;
			sysDiagn.IsNavigationActive = true;
			node.IsNavigationActive = true;

			// act
			//Utility.Eval(devStlc);
			Utility.Eval(node);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(server));
			Assert.IsFalse(Utility.ObjectHasErrors(devAlim));
			Assert.IsFalse(Utility.ObjectHasErrors(devPz));
			Assert.IsFalse(Utility.ObjectHasErrors(devAmp1));
			Assert.IsFalse(Utility.ObjectHasErrors(devAmp2));
			Assert.IsFalse(Utility.ObjectHasErrors(devTds));
			Assert.IsFalse(Utility.ObjectHasErrors(devMonitor));
			Assert.IsFalse(Utility.ObjectHasErrors(devStlc));

			Assert.IsFalse(Utility.ObjectHasErrors(voAudioGenerici));
			Assert.IsFalse(Utility.ObjectHasErrors(voAudioFondamentali));
			Assert.IsFalse(Utility.ObjectHasErrors(voAudioImportanti));
			Assert.IsFalse(Utility.ObjectHasErrors(voAudioComplementari));
			Assert.IsFalse(Utility.ObjectHasErrors(voVideoGenerici));
			Assert.IsFalse(Utility.ObjectHasErrors(voVideoFondamentali));

			Assert.IsFalse(Utility.ObjectHasErrors(sysAudio));
			Assert.IsFalse(Utility.ObjectHasErrors(sysVideo));
			Assert.IsFalse(Utility.ObjectHasErrors(sysDiagn));

			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, server.SeverityDetail);
			Assert.AreEqual(ServerNotActive.SeverityDetail, devAlim.SeverityDetail);
			Assert.AreEqual(ServerNotActive.SeverityDetail, devPz.SeverityDetail);
			Assert.AreEqual(ServerNotActive.SeverityDetail, devAmp1.SeverityDetail);
			Assert.AreEqual(ServerNotActive.SeverityDetail, devAmp2.SeverityDetail);
			Assert.AreEqual(ServerNotActive.SeverityDetail, devTds.SeverityDetail);
			Assert.AreEqual(ServerNotActive.SeverityDetail, devMonitor.SeverityDetail);
			Assert.AreEqual(ServerNotActive.SeverityDetail, devStlc.SeverityDetail);			

			Assert.AreEqual(NotAvailable.SeverityDetail, voAudioGenerici.SeverityDetail);
			Assert.AreEqual(NotActive.SeverityDetail, voAudioFondamentali.SeverityDetail);
			Assert.AreEqual(NotActive.SeverityDetail, voAudioImportanti.SeverityDetail);
			Assert.AreEqual(NotActive.SeverityDetail, voAudioComplementari.SeverityDetail);
			Assert.AreEqual(NotAvailable.SeverityDetail, voVideoGenerici.SeverityDetail);
			Assert.AreEqual(NotActive.SeverityDetail, voVideoFondamentali.SeverityDetail);

			Assert.AreEqual(NotActive.Severity, sysAudio.Severity);
			Assert.AreEqual(NotActive.Severity, sysVideo.Severity);
			Assert.AreEqual(NotActive.Severity, sysDiagn.Severity);

			Assert.AreEqual(NotActive.Severity, node.Severity);
		}

		#endregion

		#endregion

		#region Virtual object contenenti solo device offline (server offline, nella realtà, ma non rilevabile in VO)

		[Test]
		public void QuandoVirtualObject_ha3PerifericheOffline_MiAspettoUnknown()
		{
			// initialization
			var dev1 = EntityFactory.GetDevice(false);
			var dev2 = EntityFactory.GetDevice(false);
			var dev3 = EntityFactory.GetDevice(false);
			var vo = EntityFactory.GetVirtualObject();

			// severities
			dev1.SeverityDetail = ServerOffline.SeverityDetail;
			dev2.SeverityDetail = ServerOffline.SeverityDetail;
			dev3.SeverityDetail = ServerOffline.SeverityDetail;

			// relations
			dev1.Parent = vo;
			dev2.Parent = vo;
			dev3.Parent = vo;
			vo.Children.AddEntity(dev1);
			vo.Children.AddEntity(dev2);
			vo.Children.AddEntity(dev3);

			// formulas
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_W.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");

			// act
			Utility.Eval(vo);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo));

			Assert.AreEqual(Unknown.Severity, vo.Severity);
		}

		[Test]
		public void QuandoNodo_ha2Sistemi_Con1VirtualObjectAnyElemInWEOGetW_E1Device_DeviceOffline_MiAspettoError()
		{
			// initialization
			var dev1 = EntityFactory.GetDevice(false);
			var dev2 = EntityFactory.GetDevice(false);
			var dev3 = EntityFactory.GetDevice(false);
			var dev4 = EntityFactory.GetDevice(false);
			var vo1 = EntityFactory.GetVirtualObject();
			var system1 = EntityFactory.GetNodeSystem();
			var system2 = EntityFactory.GetNodeSystem();
			var node = EntityFactory.GetNode();
			var server = EntityFactory.GetServer();

			// severities
			dev1.SeverityDetail = ServerOffline.SeverityDetail;
			dev2.SeverityDetail = ServerOffline.SeverityDetail;
			dev3.SeverityDetail = ServerOffline.SeverityDetail;
			dev4.SeverityDetail = ServerOffline.SeverityDetail;
			node.Severity = Ok.Severity;
			server.SeverityDetail = Offline.SeverityDetail;

			// relations
			Utility.SetBaseRelation(node, system1);
			Utility.SetBaseRelation(node, system2);
			Utility.SetBaseRelation(vo1, dev1);
			Utility.SetBaseRelation(vo1, dev2);
			Utility.SetBaseRelation(vo1, dev3);
			Utility.SetBaseRelation(system1, vo1);
			Utility.SetBaseRelation(system2, dev4);
			dev1.Server = server;
			dev1.Node = node;
			dev2.Server = server;
			dev2.Node = node;
			dev3.Server = server;
			dev3.Node = node;
			dev4.Server = server;
			dev4.Node = node;
			system1.Node = node;
			system2.Node = node;
			server.Node = node;
			node.Devices.Add(dev1);
			node.Devices.Add(dev2);
			node.Devices.Add(dev3);
			node.Devices.Add(dev4);

			// formulas
			Utility.SetFormula(vo1, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_W.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo1, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(system1, @".\Lib\GroupScripts\GetWorst.py");
			Utility.SetFormula(system2, @".\Lib\NodeSystemDefaultStatus.py");
			Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

			// navigation
			system1.IsNavigationActive = true;
			system2.IsNavigationActive = true;
			vo1.IsNavigationActive = true;
			node.IsNavigationActive = true;

			// act
			Utility.Eval(system1);
			Utility.Eval(system2);
			Utility.Eval(node);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo1));
			Assert.IsFalse(Utility.ObjectHasErrors(system1));
			Assert.IsFalse(Utility.ObjectHasErrors(system2));
			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(Unknown.Severity, vo1.Severity);
			Assert.AreEqual(Unknown.Severity, system1.Severity);
			Assert.AreEqual(Unknown.Severity, system2.Severity);
			Assert.AreEqual(Error.Severity, node.Severity);
		}

		[Test]
		public void QuandoNodo_ha2Sistemi_Con1VirtualObjectAnyElemInWEOGetE_E1Device_DeviceOffline_MiAspettoError()
		{
			// initialization
			var dev1 = EntityFactory.GetDevice(false);
			var dev2 = EntityFactory.GetDevice(false);
			var dev3 = EntityFactory.GetDevice(false);
			var dev4 = EntityFactory.GetDevice(false);
			var vo1 = EntityFactory.GetVirtualObject();
			var system1 = EntityFactory.GetNodeSystem();
			var system2 = EntityFactory.GetNodeSystem();
			var node = EntityFactory.GetNode();
			var server = EntityFactory.GetServer();

			// severities
			dev1.SeverityDetail = ServerOffline.SeverityDetail;
			dev2.SeverityDetail = ServerOffline.SeverityDetail;
			dev3.SeverityDetail = ServerOffline.SeverityDetail;
			dev4.SeverityDetail = ServerOffline.SeverityDetail;
			node.Severity = Ok.Severity;
			server.SeverityDetail = Offline.SeverityDetail;

			// relations
			Utility.SetBaseRelation(node, system1);
			Utility.SetBaseRelation(node, system2);
			Utility.SetBaseRelation(vo1, dev1);
			Utility.SetBaseRelation(vo1, dev2);
			Utility.SetBaseRelation(vo1, dev3);
			Utility.SetBaseRelation(system1, vo1);
			Utility.SetBaseRelation(system2, dev4);
			dev1.Server = server;
			dev1.Node = node;
			dev2.Server = server;
			dev2.Node = node;
			dev3.Server = server;
			dev3.Node = node;
			dev4.Server = server;
			dev4.Node = node;
			system1.Node = node;
			system2.Node = node;
			server.Node = node;
			node.Devices.Add(dev1);
			node.Devices.Add(dev2);
			node.Devices.Add(dev3);
			node.Devices.Add(dev4);

			// formulas
			Utility.SetFormula(vo1, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo1, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(system1, @".\Lib\GroupScripts\GetWorst.py");
			Utility.SetFormula(system2, @".\Lib\NodeSystemDefaultStatus.py");
			Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

			// navigation
			system1.IsNavigationActive = true;
			system2.IsNavigationActive = true;
			vo1.IsNavigationActive = true;
			node.IsNavigationActive = true;

			// act
			Utility.Eval(system1);
			Utility.Eval(system2);
			Utility.Eval(node);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo1));
			Assert.IsFalse(Utility.ObjectHasErrors(system1));
			Assert.IsFalse(Utility.ObjectHasErrors(system2));
			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(Unknown.Severity, vo1.Severity);
			Assert.AreEqual(Unknown.Severity, system1.Severity);
			Assert.AreEqual(Unknown.Severity, system2.Severity);
			Assert.AreEqual(Error.Severity, node.Severity);
		}

		[Test]
		public void QuandoNodo_ha2Sistemi_Con1VirtualObjectAnyElemInWEOGetE_Con1VirtualObjectAnyElemInWEOGetW_DeviceOffline_MiAspettoError()
		{
			// initialization
			var dev1 = EntityFactory.GetDevice(false);
			var dev2 = EntityFactory.GetDevice(false);
			var dev3 = EntityFactory.GetDevice(false);
			var dev4 = EntityFactory.GetDevice(false);
			var vo1 = EntityFactory.GetVirtualObject();
			var vo2 = EntityFactory.GetVirtualObject();
			var system1 = EntityFactory.GetNodeSystem();
			var system2 = EntityFactory.GetNodeSystem();
			var node = EntityFactory.GetNode();
			var server = EntityFactory.GetServer();

			// severities
			dev1.SeverityDetail = ServerOffline.SeverityDetail;
			dev2.SeverityDetail = ServerOffline.SeverityDetail;
			dev3.SeverityDetail = ServerOffline.SeverityDetail;
			dev4.SeverityDetail = ServerOffline.SeverityDetail;
			node.Severity = Ok.Severity;
			server.SeverityDetail = Offline.SeverityDetail;

			// relations
			Utility.SetBaseRelation(node, system1);
			Utility.SetBaseRelation(node, system2);
			Utility.SetBaseRelation(vo1, dev1);
			Utility.SetBaseRelation(vo1, dev2);
			Utility.SetBaseRelation(vo1, dev3);
			Utility.SetBaseRelation(vo2, dev4);
			Utility.SetBaseRelation(system1, vo1);
			Utility.SetBaseRelation(system2, vo2);
			dev1.Server = server;
			dev1.Node = node;
			dev2.Server = server;
			dev2.Node = node;
			dev3.Server = server;
			dev3.Node = node;
			dev4.Server = server;
			dev4.Node = node;
			system1.Node = node;
			system2.Node = node;
			server.Node = node;
			node.Devices.Add(dev1);
			node.Devices.Add(dev2);
			node.Devices.Add(dev3);
			node.Devices.Add(dev4);

			// formulas
			Utility.SetFormula(vo1, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo1, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(vo2, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_W.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(vo2, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(system1, @".\Lib\GroupScripts\GetWorst.py");
			Utility.SetFormula(system2, @".\Lib\GroupScripts\GetWorst.py");
			Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

			// navigation
			system1.IsNavigationActive = true;
			system2.IsNavigationActive = true;
			vo1.IsNavigationActive = true;
			vo2.IsNavigationActive = true;
			node.IsNavigationActive = true;

			// act
			Utility.Eval(system1);
			Utility.Eval(system2);
			Utility.Eval(node);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vo1));
			Assert.IsFalse(Utility.ObjectHasErrors(vo2));
			Assert.IsFalse(Utility.ObjectHasErrors(system1));
			Assert.IsFalse(Utility.ObjectHasErrors(system2));
			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(Unknown.Severity, vo1.Severity);
			Assert.AreEqual(Unknown.Severity, vo2.Severity);
			Assert.AreEqual(Unknown.Severity, system1.Severity);
			Assert.AreEqual(Unknown.Severity, system2.Severity);
			Assert.AreEqual(Error.Severity, node.Severity);
		}

		#endregion
	}
}
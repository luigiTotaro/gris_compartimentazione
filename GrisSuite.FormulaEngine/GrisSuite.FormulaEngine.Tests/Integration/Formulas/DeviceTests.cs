﻿using System;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
    [TestFixture]
    public class DeviceTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            GrisObject.Cache.Reset();
        }

        #region Test formule default

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_IPNonInizializzato_MiAspettoEsclusaDalCalcoloDelParent()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Address = "0";

            server.SeverityDetail = Ok.SeverityDetail;
            server.RealSeverityDetail = Offline.SeverityDetail;
            server.LastSeverityDetail = Warning.SeverityDetail;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_IPNonInizializzato_LastStatusServerNullo_MiAspettoEsclusaDalCalcoloDelParent()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Address = "0";
            dev.ForcedByUser = false;

            server.SeverityDetail = Ok.SeverityDetail;
            server.RealSeverityDetail = Offline.SeverityDetail;
            server.LastSeverityDetail = Warning.SeverityDetail;
            server.SevLevelDetailLast = null;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_IPNonInizializzato_LastStatusServerNullo_NonForzatoDallUtente_MiAspettoEsclusaDalCalcoloDelParent()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Address = "0";
            dev.ForcedByUser = false;

            server.SeverityDetail = Ok.SeverityDetail;
            server.RealSeverityDetail = Offline.SeverityDetail;
            server.LastSeverityDetail = Warning.SeverityDetail;
            server.SevLevelDetailLast = null;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void
            QuandoDevice_DiTipoSTLC1000_IPNonInizializzato_LastStatusServerNullo_ForzatoDallUtente_MiAspettoNotAvailableEsclusaDalCalcoloDelParent()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Address = "0";
            dev.ForcedByUser = true;

            server.SeverityDetail = Ok.SeverityDetail;
            server.RealSeverityDetail = Offline.SeverityDetail;
            server.LastSeverityDetail = Warning.SeverityDetail;
            server.SevLevelDetailLast = null;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_MiAspettoEsclusaDalCalcoloDelParent()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Port = null;

            server.SeverityDetail = Offline.SeverityDetail;
            server.RealSeverityDetail = Warning.SeverityDetail;
            server.LastSeverityDetail = Error.SeverityDetail;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_SistemaDiagnostica_MiAspettoNonEsclusaDalCalcoloDelParent()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Port = null;

            server.SeverityDetail = Offline.SeverityDetail;
            server.RealSeverityDetail = Warning.SeverityDetail;
            server.LastSeverityDetail = Error.SeverityDetail;

            system.SystemId = Systems.Diagnostica;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_ServerAnomaliaGrave_MiAspettoEsclusaDalCalcoloDelParent_AnomaliaGrave()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Port = null;

            server.SeverityDetail = Error.SeverityDetail;
            server.RealSeverityDetail = Warning.SeverityDetail;
            server.LastSeverityDetail = Error.SeverityDetail;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Error.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_ServerNonAttivo_MiAspettoEsclusaDalCalcoloDelParent_NonAttivo()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Port = null;

            server.SeverityDetail = NotActive.SeverityDetail;
            server.RealSeverityDetail = Warning.SeverityDetail;
            server.LastSeverityDetail = Error.SeverityDetail;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(NotActive.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_ServerSconosciuto_MiAspettoEsclusaDalCalcoloDelParent_Sconosciuto()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Port = null;

            server.SeverityDetail = Unknown.SeverityDetail;
            server.RealSeverityDetail = Warning.SeverityDetail;
            server.LastSeverityDetail = Error.SeverityDetail;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_ServerAnomaliaLieve_MiAspettoEsclusaDalCalcoloDelParent_AnomaliaLieve()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Port = null;

            server.SeverityDetail = Warning.SeverityDetail;
            server.RealSeverityDetail = Warning.SeverityDetail;
            server.LastSeverityDetail = Error.SeverityDetail;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Warning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_DiTipoSTLC1000_PortaNonInizializzata_ServerNonClassificato_MiAspettoEsclusaDalCalcoloDelParent_ServerOffline()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "STLC1000";
            dev.Port = null;

            server.SeverityDetail = Old.SeverityDetail;
            server.RealSeverityDetail = Warning.SeverityDetail;
            server.LastSeverityDetail = Error.SeverityDetail;

            system.SystemId = Systems.AltrePeriferiche;

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InMaintenance_MiAspettoDiagnosticaDisattivataDallOperatore()
        {
            // initialization
            var dev = EntityFactory.GetDevice();
            var server = EntityFactory.GetServer();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Offline;

            // relations
            dev.Server = server;

            // entity data
            dev.Type = "TT10210V3";
            dev.InMaintenance = true;
            dev.System = system;

            system.SystemId = Systems.Diagnostica;

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotActive, dev.Severity);
            Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_NonAttivo_MiAspettoDiagnosticaDisattivataDallOperatore()
        {
            // initialization
            var dev = EntityFactory.GetDevice();
            var server = EntityFactory.GetServer();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.Type = "TT10210V3";
            dev.IsActive = 0;
            dev.System = system;

            server.Severity = Severity.Ok;

            // relations
            dev.Server = server;

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotActive, dev.Severity);
            Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_ConServerDiMonitoraggioInConfigurazione_MiAspettoCollettoreSTLC1000NonAttivo()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.NotActive;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            dev.System = system;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotActive, dev.Severity);
            Assert.AreEqual(ServerNotActive.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_ConServerDiMonitoraggioInConfigurazioneEDeviceOk_MiAspettoCollettoreSTLC1000NonAttivo()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.NotActive;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotActive, dev.Severity);
            Assert.AreEqual(ServerNotActive.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_ConServerDiMonitoraggioInConfigurazioneEDeviceAttenzione_MiAspettoCollettoreSTLC1000NonAttivo()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.NotActive;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotActive, dev.Severity);
            Assert.AreEqual(ServerNotActive.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_ConServerDiMonitoraggioInConfigurazioneStatoRealeOfflineEDeviceAttenzione_MiAspettoCollettoreSTLC1000NonAttivo()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.NotActive;
            server.RealSeverity = Offline.Severity;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotActive, dev.Severity);
            Assert.AreEqual(ServerNotActive.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_ConServerDiMonitoraggioNonRaggiungibile_MiAspettoCollettoreSTLC1000NonRaggiungibile()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Offline;
            server.RealSeverity = Offline.Severity;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(ServerOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Offline_E_DescrizioneDiversaDaDiagnosticaNonRaggiungibile_MiAspettoSconosciuto()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 1;
            dev.Description = "In funzione";
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Offline_E_DescrizioneUgualeADiagnosticaNonRaggiungibile_MiAspettoDiagnosticaNonRaggiungibile()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 1;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            dev.Description = "diaGNOstica non raggiungibile"; // Verifichiamo controllo stringa case insensitive

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(DiagnosticOffline.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(DiagnosticOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Offline_E_DescrizioneUgualeADispositivoNonRaggiungibile_MiAspettoDiagnosticaNonRaggiungibile()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 1;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            dev.Description = "dispoSITIVO non raggiungibile"; // Verifichiamo controllo stringa case insensitive

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(DiagnosticOffline.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(DiagnosticOffline.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Offline_E_DescrizioneUgualeADispositivoNonRaggiungibile_SeveritaErrore_MiAspettoDiagnosticaNonRaggiungibileInErrore()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 1;
            dev.Type = "NETMIB200";
            dev.Description = "Dispositivo non raggiungibile";

            // relations
            dev.Server = server;
            dev.System = system;
            dev.ActualSevLevel = Severity.Error;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Error, dev.Severity);
            Assert.AreEqual(DiagnosticOfflineInError.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(DiagnosticOfflineInError.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Ok_MiAspettoDiagnosticaOk()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "In servizio";
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Ok, dev.Severity);
            Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaLieve_E_DatiSNMPIncongruenti_MiAspettoDatiSNMPIncongruenti()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Dati SNMP incongruenti";
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaLieve_E_DiagnosticaNonDisponibile_MiAspettoDiagnosticaNonDisponibile()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Diagnostica non disponibile";
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(DiagnosticNotAvailableWarning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(DiagnosticNotAvailableWarning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(DiagnosticNotAvailableWarning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaLieve_E_DatiDiagnosticiLimitatiAllaSolaSezioneMIBII_MiAspettoDatiDiagnosticiLimitatiAllaSolaSezioneMIBII()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Dati diagnostici limitati alla sola sezione MIB-II";
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaLieve_E_DatiDiMonitoraggioIncompleti_MiAspettoDatiDiMonitoraggioIncompleti()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Dati di monitoraggio incompleti";
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(IncompleteDiagnosticalData.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(IncompleteDiagnosticalData.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(IncompleteDiagnosticalData.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaLieve_MiAspettoAnomaliaLieve()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Anomalia lieve";
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(Warning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaLieve_E_MessaggioImprevisto_MiAspettoAnomaliaLieve()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Attenzione";
            dev.System = system;
            dev.ActualSevLevel = Severity.Warning;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(Warning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Warning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaGrave_MiAspettoAnomaliaGrave()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Anomalia grave";
            dev.System = system;
            dev.ActualSevLevel = Severity.Error;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Error, dev.Severity);
            Assert.AreEqual(Error.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_AnomaliaGrave_E_MessaggioImprevisto_MiAspettoAnomaliaGrave()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Errore";
            dev.System = system;
            dev.ActualSevLevel = Severity.Error;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Error, dev.Severity);
            Assert.AreEqual(Error.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Error.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void
            QuandoDevice_InSeveritaNonAttiva_E_DatiDiagnosticiLimitatiAllaSolaSezioneMIBII_MiAspettoDatiDiagnosticiLimitatiAllaSolaSezioneMIBII()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Dati diagnostici limitati alla sola sezione MIB-II";
            dev.System = system;
            dev.ActualSevLevel = Severity.NotActive;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InSeveritaNonAttiva_E_DatiSNMPIncongruenti_MiAspettoDatiSNMPIncongruenti()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Dati SNMP incongruenti";
            dev.System = system;
            dev.ActualSevLevel = Severity.NotActive;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(InconsistentSNMPDataWarning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InSeveritaNonAttiva_E_InfoStazioni_MiAspettoDatiDiagnosticiLimitatiAllaSolaSezioneMIBII()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "InfoStazioni";
            dev.System = system;
            dev.ActualSevLevel = Severity.NotActive;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Warning, dev.Severity);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(MIB2OnlyWarning.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InSeveritaNonAttiva_MiAspettoDiagnosticaNonDisponibile()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Non attiva";
            dev.System = system;
            dev.ActualSevLevel = Severity.NotActive;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotActive, dev.Severity);
            Assert.AreEqual(DiagnosticNotAvailable.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(DiagnosticNotAvailable.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(DiagnosticNotAvailable.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InSeveritaSconosciuta_E_DescrizioneNonApplicabile_SoloConStreamInformativi_MiAspettoNonApplicabile()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Non applicabile";
            dev.System = system;
            dev.ActualSevLevel = Severity.Unknown;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(NA.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(NA.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(NA.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InSeveritaSconosciuta_MiAspettoSconosciuto()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "";
            dev.System = system;
            dev.ActualSevLevel = Severity.Unknown;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InSeveritaSconosciuta_E_DescrizioneImprevista_MiAspettoSconosciuto()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = "Sconosciuto";
            dev.System = system;
            dev.ActualSevLevel = Severity.Unknown;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_InSeveritaSconosciuta_E_DescrizioneNulla_MiAspettoSconosciuto()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 0;
            dev.Description = null;
            dev.System = system;
            dev.ActualSevLevel = Severity.Unknown;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_NonClassificato_MiAspettoNonClassificato()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.System = system;
            dev.ActualSevLevel = Severity.NotClassified;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotClassified, dev.Severity);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Offline_MiAspettoNonClassificato()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.System = system;
            dev.ActualSevLevel = Severity.Offline;
            dev.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.NotClassified, dev.Severity);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Ok_E_Sistema99_MiAspettoDiagnosticaOk_E_EsclusoDalCalcoloDeiPadri()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // entity data
            server.Severity = Severity.Ok;
            system.Severity = Severity.Ok;
            dev.System = system;
            dev.Parent = system;
            dev.IsOffline = 0;
            dev.Type = "XXIP000";
            dev.Description = "In servizio";
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            dev.System.SystemId = 99;

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsTrue(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Ok, dev.Severity);
            Assert.AreEqual(Ok.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        [Test]
        public void QuandoDevice_Ok_TipoSTLC1000_E_Offline_MiAspettoDiagnosticaUnknown()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            server.Severity = Severity.Ok;

            dev.IsOffline = 1;
            dev.Description = "In servizio";
            dev.System = system;
            dev.ActualSevLevel = Severity.Ok;
            dev.Type = "STLC1000";
            dev.Address = "174474336";
            dev.PortId = new Guid("68B8A7F4-EE16-4197-8973-A716D0332639");

            // relations
            dev.Server = server;
            server.Devices.AddEntity(dev);
            system.Devices.AddEntity(dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(dev.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, dev.ForcedSeverityLevel);
            Assert.AreEqual(Severity.Unknown, dev.Severity);
            Assert.AreEqual(Unknown.SeverityDetail, dev.SeverityDetail);
            Assert.AreEqual(Unknown.SeverityDetail, dev.RealSeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, dev.LastSeverityDetail);
        }

        #endregion

        #region Connessione porta primaria 2 non connessa negli ultimi 5 minuti

        [Test]
        public void
            QuandoPeriferica_ContieneStreamFieldsConnessionePortaPrimaria2_ConsecutivamenteNonConnessaDaAlmeno5Minuti_MiAspettoTutteLePerifericheInCascataInOffline
            ()
        {
            // se la lo stream filed che mi indica lo stato di una connessione risulta in errore per + di 5 minuti, indico come offline le 2 periferiche che so essere connesse alla porta in cascata.

            var node = EntityFactory.GetNode();
            var devMain = EntityFactory.GetDevice();
            var dev1ConnectedToPrimaryConn2 = EntityFactory.GetDevice(false);
            var dev2ConnectedToPrimaryConn2 = EntityFactory.GetDevice(false);
            var system = EntityFactory.GetNodeSystem();
            var streamInformazioniPorteSwitch = EntityFactory.GetStream();
            var streamFieldConnessionePortaPrimaria2 = EntityFactory.GetStreamField();

            // entity data
            devMain.Name = "Periferica principale";
            dev1ConnectedToPrimaryConn2.Name = "Dev 1 connessa a porta 2";
            dev2ConnectedToPrimaryConn2.Name = "Dev 2 connessa a porta 2";
            dev1ConnectedToPrimaryConn2.SeverityDetail = Ok.SeverityDetail;
            dev2ConnectedToPrimaryConn2.SeverityDetail = Ok.SeverityDetail;

            streamInformazioniPorteSwitch.Name = "Informazioni porte switch";
            streamFieldConnessionePortaPrimaria2.Name = "Connessione porta primaria 2";

            // relations
            system.Node = node;
            node.Systems.AddEntity(system);

            devMain.Node = node;
            node.Devices.AddEntity(devMain, false);
            dev1ConnectedToPrimaryConn2.Node = node;
            node.Devices.AddEntity(dev1ConnectedToPrimaryConn2, false);
            dev2ConnectedToPrimaryConn2.Node = node;
            node.Devices.AddEntity(dev2ConnectedToPrimaryConn2, false);

            devMain.System = system;
            system.Devices.AddEntity(devMain);
            dev1ConnectedToPrimaryConn2.System = system;
            system.Devices.AddEntity(dev1ConnectedToPrimaryConn2);
            dev2ConnectedToPrimaryConn2.System = system;
            system.Devices.AddEntity(dev2ConnectedToPrimaryConn2);

            streamInformazioniPorteSwitch.Device = devMain;
            devMain.Streams.Add(streamInformazioniPorteSwitch);

            streamFieldConnessionePortaPrimaria2.Stream = streamInformazioniPorteSwitch;
            streamInformazioniPorteSwitch.StreamFields.Add(streamFieldConnessionePortaPrimaria2);

            // history
            var initialDateTime = DateTime.Now.AddMinutes(-10);
            for (int index = 0; index < (DateTime.Now - initialDateTime).Seconds/30; index++)
            {
                // initialization
                var streamHistory = EntityFactory.GetStreamHistory();
                streamHistory.DateReceived = initialDateTime.AddSeconds(30);

                streamHistory.Stream = streamInformazioniPorteSwitch;
                streamInformazioniPorteSwitch.StreamsHistory.Add(streamHistory);

                var streamFieldHistory = EntityFactory.GetStreamFieldHistory();
                streamFieldHistory.Description = "VelocitàConnessione=0;StabilitàConnessione=2;StatoConnessione=2";

                streamFieldHistory.StreamField = streamFieldConnessionePortaPrimaria2;
                streamFieldConnessionePortaPrimaria2.StreamFieldsHistory.Add(streamFieldHistory);
            }

            // formulas
            Utility.SetFormula(devMain, @".\Lib\ObjectScripts\TestDevice_Connessione2_NonConnessaPer5Min.py");

            node.IsNavigationActive = system.IsNavigationActive = devMain.IsNavigationActive = true;

            // act
            Utility.Eval(devMain);

            foreach (var customEvaluationFormula in devMain.CustomEvaluationFormulas)
            {
                Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
            }
            Assert.IsFalse(devMain.DefaultEvaluationFormula.HasErrorInEvaluation);

            Assert.AreEqual(ForcedSeverityLevel.NotForced, devMain.ForcedSeverityLevel);
            Assert.AreEqual(Offline.SeverityDetail, devMain.SeverityDetail);
            Assert.AreEqual(Offline.SeverityDetail, dev1ConnectedToPrimaryConn2.SeverityDetail);
            Assert.AreEqual(Offline.SeverityDetail, dev2ConnectedToPrimaryConn2.SeverityDetail);
        }

        #endregion

        #region Amplificatore in Errore e Periferica PZ in stato di OK con Stream Fields relativi all' Amplificatore tutti OK, mi aspetto Amplificatore OK

        [Test]
        public void AmplificatoreInErrore_ePerifericaPZinOK_conTuttigliStreamFieldsRelativiAllAmpliInOK_MiAspettoAmplificatoreOK()
        {
            // initialization
            var system = EntityFactory.GetNodeSystem(false);
            var devAmpli = EntityFactory.GetDevice(false);
            var devPz = EntityFactory.GetDevice();
            var strmSg = EntityFactory.GetStream();
            var sfA1 = EntityFactory.GetStreamField();

            // entity data
            devAmpli.Name = "Amplificatore";
            devAmpli.SeverityDetail = Error.SeverityDetail;
            devPz.Name = "Pannello Zone";

            strmSg.Name = "Stato Generale";
            sfA1.Name = "Stato amplificatore 1";
            sfA1.Description = "Uscita installata=0;Uscita in servizio=0;Uscita attiva=0;Uscita testata=0";

            // relations
            sfA1.Stream = strmSg;
            strmSg.StreamFields.Add(sfA1);

            strmSg.Device = devPz;
            devPz.Streams.Add(strmSg);

            system.Devices.AddEntity(devPz);
            system.Devices.AddEntity(devAmpli);

            // formulas
            Utility.SetFormula(devPz, @".\Lib\ObjectScripts\TestDevice_SeverityBasedToOtherDevCases.py");

            // ents that require navigation
            devPz.IsNavigationActive = true;
            system.IsNavigationActive = true;

            // act
            Utility.Eval(devPz);

            Assert.IsFalse(Utility.ObjectHasErrors(devPz));

            Assert.AreEqual(ForcedSeverityLevel.NotForced, devPz.ForcedSeverityLevel);
            Assert.AreEqual(Ok.SeverityDetail, devPz.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, devAmpli.SeverityDetail);
        }

        #endregion

        #region Una device con il flag di invio email alzato dovrebbe impostarlo anche sull'oggetto padre

        [Test]
        public void QuandoDevice_HaShouldSendNotificationByEmail_true_ed_èFigliaDirettaDiSistema_MiAspettoSistemaConHasChildrenThatShouldSendEmail_true()
        {
            // initialization
            var dev = EntityFactory.GetDevice();
            var server = EntityFactory.GetServer();
            var system = EntityFactory.GetNodeSystem();

            // entity data
            dev.ShouldSendNotificationByEmail = true;

            // relations
            dev.Server = server;
            dev.System = system;

            Utility.SetBaseRelation(system, dev);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dev);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));

            Assert.IsTrue(system.HasChildrenThatShouldSendEmail.HasValue && system.HasChildrenThatShouldSendEmail.Value);
        }

        [Test]
        public void QuandoDevice_HaShouldSendNotificationByEmail_true_ed_èFigliaIndirettaDiSistema_MiAspettoSistemaConHasChildrenThatShouldSendEmail_true()
        {
            // initialization
            var dev = EntityFactory.GetDevice();
            var server = EntityFactory.GetServer();
            var system = EntityFactory.GetNodeSystem();
            var voParent = EntityFactory.GetVirtualObject();
            var voGrandparent = EntityFactory.GetVirtualObject();

            // entity data
            dev.ShouldSendNotificationByEmail = true;

            // relations
            dev.Server = server;
            dev.System = system;

            Utility.SetBaseRelation(voParent, dev);
            Utility.SetBaseRelation(voGrandparent, voParent);
            Utility.SetBaseRelation(system, voGrandparent);

            // formulas
            Utility.SetFormula(dev, @".\Lib\DeviceDefaultStatus.py");
            Utility.SetFormula(voParent, @".\Lib\GroupScripts\GetWorst.py");
            Utility.SetFormula(voGrandparent, @".\Lib\GroupScripts\GetWorst.py");

            // act
            Utility.Eval(dev);
            Utility.Eval(voParent);
            Utility.Eval(voGrandparent);

            Assert.IsFalse(Utility.ObjectHasErrors(dev));
            Assert.IsFalse(Utility.ObjectHasErrors(voParent));
            Assert.IsFalse(Utility.ObjectHasErrors(voGrandparent));

            Assert.IsTrue(system.HasChildrenThatShouldSendEmail.HasValue && system.HasChildrenThatShouldSendEmail.Value);
        }

        [Test]
        public void QuandoPiùDevice_HannoShouldSendNotificationByEmail_false_edUnaSola_true_ed_èFigliaIndirettaDiSistema_MiAspettoSistemaConHasChildrenThatShouldSendEmail_true()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var server = EntityFactory.GetServer();
            var system = EntityFactory.GetNodeSystem();
            var voParent = EntityFactory.GetVirtualObject();
            var voGrandparent = EntityFactory.GetVirtualObject();

            // entity data
            dev1.ShouldSendNotificationByEmail = false;
            dev2.ShouldSendNotificationByEmail = true;
            dev3.ShouldSendNotificationByEmail = false;
            dev4.ShouldSendNotificationByEmail = false;

            // relations
            dev1.Server = server;
            dev1.System = system;
            dev2.Server = server;
            dev2.System = system;
            dev3.Server = server;
            dev3.System = system;
            dev4.Server = server;
            dev4.System = system;

            Utility.SetBaseRelation(voParent, dev1);
            Utility.SetBaseRelation(voParent, dev2);
            Utility.SetBaseRelation(voParent, dev3);
            Utility.SetBaseRelation(voParent, dev4);
            Utility.SetBaseRelation(voGrandparent, voParent);
            Utility.SetBaseRelation(system, voGrandparent);

            // formulas
            Utility.SetFormula(dev1, @".\Lib\DeviceDefaultStatus.py");
            Utility.SetFormula(dev2, @".\Lib\DeviceDefaultStatus.py");
            Utility.SetFormula(dev3, @".\Lib\DeviceDefaultStatus.py");
            Utility.SetFormula(dev4, @".\Lib\DeviceDefaultStatus.py");
            Utility.SetFormula(voParent, @".\Lib\GroupScripts\GetWorst.py");
            Utility.SetFormula(voGrandparent, @".\Lib\GroupScripts\GetWorst.py");

            // act
            Utility.Eval(dev1);
            Utility.Eval(dev2);
            Utility.Eval(dev3);
            Utility.Eval(dev4);
            Utility.Eval(voParent);
            Utility.Eval(voGrandparent);

            Assert.IsFalse(Utility.ObjectHasErrors(dev1));
            Assert.IsFalse(Utility.ObjectHasErrors(dev2));
            Assert.IsFalse(Utility.ObjectHasErrors(dev3));
            Assert.IsFalse(Utility.ObjectHasErrors(dev4));
            Assert.IsFalse(Utility.ObjectHasErrors(voParent));
            Assert.IsFalse(Utility.ObjectHasErrors(voGrandparent));

            Assert.IsTrue(system.HasChildrenThatShouldSendEmail.HasValue && system.HasChildrenThatShouldSendEmail.Value);
        }

        #endregion

        #region Una device DT04000 è esclusa dal calcolo di stato del parent se è presente un pannello a zone, ad essa relazionato e non offline, nella stazione

        [Test]
        public void QuandoUnaDevice_DT04000_HaUnPZAssociato_nonOffline_MiAspettoExcludedFromParentStatus_true()
        {
            // initialization
            var dt = EntityFactory.GetDevice();
            var pz = EntityFactory.GetDevice(false);
            var server = EntityFactory.GetServer(false);
            var system = EntityFactory.GetNodeSystem(false);
            var node = EntityFactory.GetNode(false);

            // entity data
            dt.Type = "DT04000";
            pz.Type = "TT10210";
            dt.ExcludedFromParentStatus = true;

            // relations
            dt.Server = server;
            dt.System = system;
            pz.Server = server;
            pz.System = system;
            system.Node = node;

            dt.Related.Add(pz);

            // formulas
            Utility.SetFormula(dt, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(dt);

            Assert.IsFalse(Utility.ObjectHasErrors(dt));

            Assert.IsTrue(dt.ExcludedFromParentStatus);
        }

        #endregion

        #region Una device Centralino VoIP (VP00010) è esclusa dal calcolo di stato del parent se è in Errore ma la sua correlata (altro nodo del cluster) è in Ok

        [Test]
        public void QuandoUnaDevice_VP00010_InErrore_HaUnaVP00010Associata_InOk_MiAspettoExcludedFromParentStatus_true()
        {
            // initialization
            var voip1 = EntityFactory.GetDevice();
            var voip2 = EntityFactory.GetDevice();
            var server = EntityFactory.GetServer(false);
            var system = EntityFactory.GetNodeSystem(false);
            var node = EntityFactory.GetNode(false);

            // entity data
            voip1.Type = "VP00010";
            voip2.Type = "VP00010";
            voip1.ExcludedFromParentStatus = false;
            voip2.ExcludedFromParentStatus = false;

            var stream1 = new Stream
                              {
                                  Name = "Stato processi di sistema",
                              };
            var streamField1 = new StreamField
                                   {
                                       Name = "Centralino VoIP",
                                       ActualSevLevel = 2 // Errore
                                   };
            stream1.StreamFields.Add(streamField1);
            voip1.Streams.Add(stream1);

            var stream2 = new Stream
            {
                Name = "Stato processi di sistema",
            };
            var streamField2 = new StreamField
            {
                Name = "Centralino VoIP",
                ActualSevLevel = 0 // Ok
            };
            stream2.StreamFields.Add(streamField2);
            voip2.Streams.Add(stream2);

            // relations
            voip1.Server = server;
            voip1.System = system;
            voip2.Server = server;
            voip2.System = system;
            system.Node = node;

            voip1.Related.Add(voip2);
            voip2.Related.Add(voip1);

            // formulas
            Utility.SetFormula(voip1, @".\Lib\DeviceDefaultStatus.py");
            Utility.SetFormula(voip2, @".\Lib\DeviceDefaultStatus.py");

            // act
            Utility.Eval(voip1);
            Utility.Eval(voip2);

            Assert.IsFalse(Utility.ObjectHasErrors(voip1));
            Assert.IsFalse(Utility.ObjectHasErrors(voip2));

            Assert.IsTrue(voip1.ExcludedFromParentStatus);
            Assert.IsFalse(voip2.ExcludedFromParentStatus);
        }

        #endregion
    }
}
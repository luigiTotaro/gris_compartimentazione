﻿
using System;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
    [TestFixture]
    public class NodeSystemTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            GrisObject.Cache.Reset();
        }

        #region Test formule default sistemi

        [Test]
        public void QuandoNodeSystem_haIlServerConDatiIncompleti_MiAspettoErrore()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;

            // severities
            dev1.SeverityDetail = Warning.SeverityDetail;
            dev2.SeverityDetail = Error.SeverityDetail;
            node.Severity = Ok.SeverityDetail;
            server.SeverityDetail = IncompleteServerData.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
        	Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.IncompleteServerData, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha1Periferica_Ok_MiAspettoOk()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "DT05000";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Parent = node;
            dev1.Server = server;
            dev1.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
			Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Ok.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha2Periferiche_DT05000Ok_DT05000Warning_MiAspettoOk()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.SistemaTelefonia;
            dev1.Type = "DT05000";
            dev2.Type = "DT05000";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Warning.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
			Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Ok.Severity, system.Severity);
            Assert.IsTrue(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha4Periferiche_DT13000Ok_TT10220Error_DT04000Unknown_DT04000Ok_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT13000";
            dev2.Type = "TT10220";
            dev3.Type = "DT04000";
            dev4.Type = "DT04000";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Error.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
			Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha1Periferica_STLC1000Ok_MiAspettoOk()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "STLC1000";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Parent = node;
            dev1.Server = server;
            dev1.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
			Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Ok.Severity, system.Severity);
            Assert.IsTrue(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha4Periferiche_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Unknown_INFSTAZ1Unknown_MiAspettoWarning()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";
            dev4.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev4.SeverityDetail = DiagnosticOffline.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
			Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Warning.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha5Periferiche_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Error_MiAspettoOk()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Error.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Devices.Add(dev5);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
			Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Ok.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha1Periferica_INFSTAZ1Unknown_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = DiagnosticOffline.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Parent = node;
            dev1.Server = server;
            dev1.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
			Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha5Periferiche_DT13000Ok_DT04000Ok_TT10210V3Error_DT05B10Ok_DT05B10Ok_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT13000";
            dev2.Type = "DT04000";
            dev3.Type = "TT10210V3";
            dev4.Type = "DT05B10";
            dev5.Type = "DT05B10";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Error.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Devices.Add(dev5);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha4Periferiche_DT04000Unknown_DT13000Ok_TT10220Warning_DT05BC0Ok_MiAspettoWarning()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT04000";
            dev2.Type = "DT13000";
            dev3.Type = "TT10220";
            dev4.Type = "DT05BC0";

            // severities
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Warning.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Warning.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha3Periferiche_DT13000Error_TT10210V3Error_DT04000Ok_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT13000";
            dev2.Type = "TT10210V3";
            dev3.Type = "DT04000";

            // severities
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Error.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha4Periferiche_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Unknown_MiAspettoOk()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";
            dev4.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = DiagnosticOffline.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Ok.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha3Periferiche_DT13000Unknown_TT10210V3Unknown_DT04000Unknown_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT13000";
            dev2.Type = "TT10210V3";
            dev3.Type = "DT04000";

            // severities
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Unknown.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha3Periferiche_DT04000Unknown_DT13000Unknown_TT10210Error_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT04000";
            dev2.Type = "DT13000";
            dev3.Type = "TT10210";

            // severities
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Unknown.SeverityDetail;
            dev3.SeverityDetail = Error.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha3Periferiche_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Error_MiAspettoWarning()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Error.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Warning.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha3Periferiche_INFSTAZ1Warning_INFSTAZ1Unknown_INFSTAZ1Warning_MiAspettoWarning()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = DiagnosticNotAvailableWarning.SeverityDetail;
            dev2.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev3.SeverityDetail = DiagnosticNotAvailableWarning.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Warning.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha1Periferica_HA01000Error_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.SistemaTelefonia;
            dev1.Type = "HA01000";

            // severities
            dev1.SeverityDetail = Error.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Parent = node;
            dev1.Server = server;
            dev1.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsTrue(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoNodeSystem_ha29Periferiche_INFSTAZ1Warning_INFSTAZ1Warning_INFSTAZ1Warning_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Unknown_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Unknown_INFSTAZ1Ok_INFSTAZ1Unknown_INFSTAZ1Unknown_INFSTAZ1Warning_INFSTAZ1Unknown_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Ok_INFSTAZ1Unknown_MiAspettoOk
            ()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();
            var dev11 = EntityFactory.GetDevice();
            var dev12 = EntityFactory.GetDevice();
            var dev13 = EntityFactory.GetDevice();
            var dev14 = EntityFactory.GetDevice();
            var dev15 = EntityFactory.GetDevice();
            var dev16 = EntityFactory.GetDevice();
            var dev17 = EntityFactory.GetDevice();
            var dev18 = EntityFactory.GetDevice();
            var dev19 = EntityFactory.GetDevice();
            var dev20 = EntityFactory.GetDevice();
            var dev21 = EntityFactory.GetDevice();
            var dev22 = EntityFactory.GetDevice();
            var dev23 = EntityFactory.GetDevice();
            var dev24 = EntityFactory.GetDevice();
            var dev25 = EntityFactory.GetDevice();
            var dev26 = EntityFactory.GetDevice();
            var dev27 = EntityFactory.GetDevice();
            var dev28 = EntityFactory.GetDevice();
            var dev29 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "INFSTAZ1";
            dev9.Type = "INFSTAZ1";
            dev10.Type = "INFSTAZ1";
            dev11.Type = "INFSTAZ1";
            dev12.Type = "INFSTAZ1";
            dev13.Type = "INFSTAZ1";
            dev14.Type = "INFSTAZ1";
            dev15.Type = "INFSTAZ1";
            dev16.Type = "INFSTAZ1";
            dev17.Type = "INFSTAZ1";
            dev18.Type = "INFSTAZ1";
            dev19.Type = "INFSTAZ1";
            dev20.Type = "INFSTAZ1";
            dev21.Type = "INFSTAZ1";
            dev22.Type = "INFSTAZ1";
            dev23.Type = "INFSTAZ1";
            dev24.Type = "INFSTAZ1";
            dev25.Type = "INFSTAZ1";
            dev26.Type = "INFSTAZ1";
            dev27.Type = "INFSTAZ1";
            dev28.Type = "INFSTAZ1";
            dev29.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev2.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev3.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;
            dev9.SeverityDetail = Ok.SeverityDetail;
            dev10.SeverityDetail = Ok.SeverityDetail;
            dev11.SeverityDetail = Ok.SeverityDetail;
            dev12.SeverityDetail = Ok.SeverityDetail;
            dev13.SeverityDetail = Ok.SeverityDetail;
            dev14.SeverityDetail = Ok.SeverityDetail;
            dev15.SeverityDetail = Ok.SeverityDetail;
            dev16.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev17.SeverityDetail = Ok.SeverityDetail;
            dev18.SeverityDetail = Ok.SeverityDetail;
            dev19.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev20.SeverityDetail = Ok.SeverityDetail;
            dev21.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev22.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev23.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev24.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev25.SeverityDetail = Ok.SeverityDetail;
            dev26.SeverityDetail = Ok.SeverityDetail;
            dev27.SeverityDetail = Ok.SeverityDetail;
            dev28.SeverityDetail = Ok.SeverityDetail;
            dev29.SeverityDetail = DiagnosticOffline.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Devices.Add(dev5);
            system.Devices.Add(dev6);
            system.Devices.Add(dev7);
            system.Devices.Add(dev8);
            system.Devices.Add(dev9);
            system.Devices.Add(dev10);
            system.Devices.Add(dev11);
            system.Devices.Add(dev12);
            system.Devices.Add(dev13);
            system.Devices.Add(dev14);
            system.Devices.Add(dev15);
            system.Devices.Add(dev16);
            system.Devices.Add(dev17);
            system.Devices.Add(dev18);
            system.Devices.Add(dev19);
            system.Devices.Add(dev20);
            system.Devices.Add(dev21);
            system.Devices.Add(dev22);
            system.Devices.Add(dev23);
            system.Devices.Add(dev24);
            system.Devices.Add(dev25);
            system.Devices.Add(dev26);
            system.Devices.Add(dev27);
            system.Devices.Add(dev28);
            system.Devices.Add(dev29);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev11.Server = server;
            dev12.Server = server;
            dev13.Server = server;
            dev14.Server = server;
            dev15.Server = server;
            dev16.Server = server;
            dev17.Server = server;
            dev18.Server = server;
            dev19.Server = server;
            dev20.Server = server;
            dev21.Server = server;
            dev22.Server = server;
            dev23.Server = server;
            dev24.Server = server;
            dev25.Server = server;
            dev26.Server = server;
            dev27.Server = server;
            dev28.Server = server;
            dev29.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            dev11.Node = node;
            dev12.Node = node;
            dev13.Node = node;
            dev14.Node = node;
            dev15.Node = node;
            dev16.Node = node;
            dev17.Node = node;
            dev18.Node = node;
            dev19.Node = node;
            dev20.Node = node;
            dev21.Node = node;
            dev22.Node = node;
            dev23.Node = node;
            dev24.Node = node;
            dev25.Node = node;
            dev26.Node = node;
            dev27.Node = node;
            dev28.Node = node;
            dev29.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Ok.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha1Periferica_MGGA5000Unknown_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.SistemaErogazioneEnergia;
            dev1.Type = "MGGA5000";

            // severities
            dev1.SeverityDetail = DiagnosticOffline.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Parent = node;
            dev1.Server = server;
            dev1.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha2Periferiche_INFSTAZ1Unknown_INFSTAZ1Warning_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";

            // severities
            dev1.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev2.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha6Periferiche_DT13000Warning_DT04000Ok_TT10210Unknown_DT05L00Ok_DT04000Unknown_DT04000Ok_MiAspettoError()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT13000";
            dev2.Type = "DT04000";
            dev3.Type = "TT10210";
            dev4.Type = "DT05L00";
            dev5.Type = "DT04000";
            dev6.Type = "DT04000";

            // severities
            dev1.SeverityDetail = Warning.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Unknown.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Error.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Devices.Add(dev5);
            system.Devices.Add(dev6);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha4Periferiche_DT05L00Ok_DT13000Unknown_DT04000Unknown_TT10210V3Ok_MiAspettoWarning()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.DiffusioneSonora;
            dev1.Type = "DT05L00";
            dev2.Type = "DT13000";
            dev3.Type = "DT04000";
            dev4.Type = "TT10210V3";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Unknown.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Devices.Add(dev3);
            system.Devices.Add(dev4);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Warning.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha2Periferiche_AEL0000Unknown_AEL0000Ok_MiAspettoWarning()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "AEL0000";
            dev2.Type = "AEL0000";

            // severities
            dev1.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Warning.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Warning.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_nonHaPeriferiche_MiAspettoNotActive()
        {
            // initialization
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var system = EntityFactory.GetNodeSystem();

            // entity data

            // severities
            server.SeverityDetail = Ok.SeverityDetail;
            node.Severity = Ok.Severity;

            // relations
            system.Parent = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
			Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(NotActive.Severity, system.Severity);
            Assert.IsTrue(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha1PerifericaInMaintenance_MiAspettoNotActive()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.SistemaErogazioneEnergia;
            dev1.Type = "MGGA5000";
            dev1.InMaintenance = true;

            // severities
            dev1.SeverityDetail = NotActive.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Parent = node;
            dev1.Server = server;
            dev1.Node = node;
            system.Node = node;

            // formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Utility.ObjectHasErrors(system);

            Assert.AreEqual(NotActive.Severity, system.Severity);
            Assert.IsFalse(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

		[Test]
		public void QuandoNodeSystem_ha1Periferica_E_ServerOffline_MiAspettoUnknown ()
		{
			// initialization
			var dev1 = EntityFactory.GetDevice();
			var system = EntityFactory.GetNodeSystem();
			var node = EntityFactory.GetNode();
			var server = EntityFactory.GetServer();

			// entity data
			system.SystemId = Systems.SistemaErogazioneEnergia;
			dev1.Type = "MGGA5000";
			dev1.InMaintenance = true;

			// severities
			dev1.SeverityDetail = NotActive.SeverityDetail;
			node.Severity = Ok.Severity;
			server.SeverityDetail = Offline.SeverityDetail;

			// relations
			system.Devices.Add(dev1);
			system.Parent = node;
			dev1.Server = server;
			dev1.Node = node;
			system.Node = node;
			server.Node = node;
			node.Devices.Add(dev1);

			// formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

			// act
			Utility.Eval(system);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(system));

			Assert.AreEqual(Unknown.Severity, system.Severity);
			Assert.IsFalse(system.ExcludedFromParentStatus);
			Assert.AreEqual(ForcedSeverityLevel.STLC1000ServerNotReachable, system.ForcedSeverityLevel);
		}

		[Test]
		public void QuandoNodeSystem_TuttePerifericheInDiagnosticaDisattivataDaOperatore_E_Offline_E_ServerOnline_MiAspettoNotActive ()
		{
			// initialization
			var dev1 = EntityFactory.GetDevice();
			var dev2 = EntityFactory.GetDevice();
			var dev3 = EntityFactory.GetDevice();
			var dev4 = EntityFactory.GetDevice();
			var system = EntityFactory.GetNodeSystem();
			var node = EntityFactory.GetNode();
			var server = EntityFactory.GetServer();

			// entity data
			system.SystemId = Systems.DiffusioneSonora;
			dev1.Type = "DT13000";
			dev1.InMaintenance = false;
			dev1.IsOffline = Convert.ToByte(true);
			dev1.IsActive = Convert.ToByte(false);

			dev2.Type = "DT04000";
			dev2.InMaintenance = false;
			dev2.IsOffline = Convert.ToByte(true);
			dev2.IsActive = Convert.ToByte(false);

			dev3.Type = "TT10210V3";
			dev3.InMaintenance = false;
			dev3.IsOffline = Convert.ToByte(true);
			dev3.IsActive = Convert.ToByte(false);

			dev4.Type = "DT13000";
			dev4.InMaintenance = false;
			dev4.IsOffline = Convert.ToByte(true);
			dev4.IsActive = Convert.ToByte(false);

			// severities
			dev1.SeverityDetail = Unknown.SeverityDetail;
			dev2.SeverityDetail = Unknown.SeverityDetail;
			dev3.SeverityDetail = Unknown.SeverityDetail;
			dev4.SeverityDetail = Unknown.SeverityDetail;
			server.SeverityDetail = Ok.SeverityDetail;

			// relations
			system.Node = node;
			system.Devices.AddEntity(dev1);
			system.Devices.AddEntity(dev2);
			system.Devices.AddEntity(dev3);
			system.Devices.AddEntity(dev4);
			dev1.Server = server;
			dev2.Server = server;
			dev3.Server = server;
			dev4.Server = server;
			dev1.Node = node;
			dev2.Node = node;
			dev3.Node = node;
			dev4.Node = node;
			server.Node = node;

			// formulas
			Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

			// act
			Utility.Eval(system);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(system));

			Assert.AreEqual(NotActive.Severity, system.Severity);
			Assert.IsFalse(system.ExcludedFromParentStatus);
		}

        [Test]
        public void QuandoNodeSystem_ha2Periferiche_SistemaVPNVerde_MiAspettoEsclusoDalCalcoloDeiPadri()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.MonitoraggioVPNVerde;
            dev1.Type = "NETMIB200";
            dev2.Type = "NETMIB200";

            // severities
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Warning.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            system.Node = node;

            // formulas
            Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Ok.Severity, system.Severity);
            Assert.IsTrue(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoNodeSystem_ha2Periferiche_SistemaWebRadio_MiAspettoEsclusoDalCalcoloDeiPadri()
        {
            // initialization
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var system = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            system.SystemId = Systems.WebRadio;
            dev1.Type = "ETAUDIO2IP100";
            dev2.Type = "ETIP2AUDIO100";

            // severities
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Error.SeverityDetail;
            node.Severity = Ok.Severity;
            server.SeverityDetail = Ok.SeverityDetail;

            // relations
            system.Devices.Add(dev1);
            system.Devices.Add(dev2);
            system.Parent = node;
            dev1.Server = server;
            dev2.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            system.Node = node;

            // formulas
            Utility.SetFormula(system, @".\Lib\NodeSystemDefaultStatus.py");

            // act
            Utility.Eval(system);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(system));

            Assert.AreEqual(Error.Severity, system.Severity);
            Assert.IsTrue(system.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, system.ForcedSeverityLevel);
        }

        #endregion

        #region Test formule custom sistemi

		[Test]
		public void QuandoNodeSystemHa_PZPiuImportanteInAttenz_E_PZMenoImportanteInError_MiAspettoNodeSystemInAttenz ()
		{
			// initialization
			var dev1 = EntityFactory.GetDevice();
			var dev2 = EntityFactory.GetDevice();
			var system = EntityFactory.GetNodeSystem();
			var node = EntityFactory.GetNode();

			// entity data
			dev1.Name = "PZ_guz";
			dev2.Name = "PZ_ovetz";

			// severities
			dev1.SeverityDetail = Warning.SeverityDetail;
			dev2.SeverityDetail = Error.SeverityDetail;
			node.Severity = Severity.NotClassified;

			// relations
			system.Devices.Add(dev1);
			system.Devices.Add(dev2);
			system.Parent = node;

			// formulas
			Utility.SetFormula(system, @".\Lib\ObjectScripts\TestSystem_PZWithDifferentImportance.py");

			// act
			Utility.Eval(system);

			// assert
			foreach ( var customEvaluationFormula in system.CustomEvaluationFormulas )
			{
				Assert.IsFalse(customEvaluationFormula.HasErrorInEvaluation, customEvaluationFormula.EvaluationError);
			}
			Assert.IsFalse(system.DefaultEvaluationFormula.HasErrorInEvaluation, system.DefaultEvaluationFormula.EvaluationError);

			Assert.AreEqual(Severity.Warning, system.Severity);
			Assert.AreEqual(Severity.Warning, node.Severity);
		}

		[Test]
		public void QuandoNodeSystemHa_ServerOffline_1Periferica_1RaggruppamentoFondamentali_MiAspettoNodeSystemERaggruppamentoFondamentaliNonAttivi ()
		{
			// initialization
			var server = EntityFactory.GetServer(false);
			var devStlc = EntityFactory.GetDevice();
			var vobj = EntityFactory.GetVirtualObject();
			var system = EntityFactory.GetNodeSystem();

			// entity data

			// severities
			server.Severity = NotActive.Severity;

			// relations
			devStlc.System = system;
			devStlc.Server = server;
            devStlc.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

			Utility.SetBaseRelation(vobj, devStlc);
			Utility.SetBaseRelation(system, vobj);

			// formulas
			Utility.SetFormula(devStlc, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(vobj, @".\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py");
			Utility.SetFormula(vobj, @".\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_E.py", EvaluationFormulaType.Custom);
			Utility.SetFormula(system, @".\Lib\GroupScripts\GetWorst.py");

			devStlc.IsNavigationActive = true;
			vobj.IsNavigationActive = true;
			system.IsNavigationActive = true;

			// act
			Utility.Eval(system);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(vobj));
			Assert.IsFalse(Utility.ObjectHasErrors(system));

			Assert.AreEqual(NotActive.Severity, vobj.Severity);
			Assert.AreEqual(NotActive.Severity, system.Severity);
		}

        #endregion
    }
}
﻿
using System.Drawing;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
    [TestFixture]
    public class NodeTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            GrisObject.Cache.Reset();
        }

        #region Test formule default nodi

        [Test]
        public void QuandoStazione1700938186762_ha2Sistemi_DiffusioneSonoraError_AltrePerifericheNotActive_MiAspettoError ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 1700938186762;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "TT10210V3";
            dev2.Type = "DT04000";
            dev3.Type = "DT13000";
            dev4.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Error.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            altrePerifericheSystem.Devices.Add(dev4);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione7108302012426_ha3Sistemi_DiffusioneSonoraError_InformazioneVisivaOk_AltrePerifericheNotActive_MiAspettoError()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 7108302012426;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "DT13000";
            dev3.Type = "TT10210V3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Error.Severity;
            informazioneVisivaSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            altrePerifericheSystem.Devices.Add(dev5);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");


            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione3788292816906_ha2Sistemi_DiffusioneSonoraErrorConPZErrore_AltrePerifericheNotActive_MiAspettoError()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 3788292816906;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "TT10210";
            dev3.Type = "DT04000";
            dev4.Type = "DT05L00";
            dev5.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Error.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Error.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Unknown.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            altrePerifericheSystem.Devices.Add(dev5);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione6575726133258_ha3Sistemi_DiffusioneSonoraErrorConPZV3Error_InformazioneVisivaOk_AltrePerifericheNotActive_MiAspettoError()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 6575726133258;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "TT10210V3";
            dev3.Type = "DT04000";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Error.Severity;
            informazioneVisivaSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Error.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            altrePerifericheSystem.Devices.Add(dev7);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione_ha3Sistemi_DiffusioneSonoraError_SistemaTelefoniaOk_AltrePerifericheNotActive_MiAspettoError()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();

            // entity data
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT05B10";
            dev3.Type = "TT10220";
            dev4.Type = "DT04000";
            dev5.Type = "DT00000";
            dev6.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Error.Severity;
            sistemaTelefoniaSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Error.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            sistemaTelefoniaSystem.Devices.Add(dev5);
            altrePerifericheSystem.Devices.Add(dev6);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione9457649319946_ha3Sistemi_DiffusioneSonoraErrorConPZV3Unknown_InformazioneVisivaOk_AltrePerifericheNotActive_MiAspettoError()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();
            var dev11 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 9457649319946;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "TT10210V3";
            dev3.Type = "DT04000";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "INFSTAZ1";
            dev9.Type = "INFSTAZ1";
            dev10.Type = "INFSTAZ1";
            dev11.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Error.Severity;
            informazioneVisivaSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Unknown.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Error.SeverityDetail;
            dev9.SeverityDetail = Ok.SeverityDetail;
            dev10.SeverityDetail = Ok.SeverityDetail;
            dev11.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            informazioneVisivaSystem.Devices.Add(dev7);
            informazioneVisivaSystem.Devices.Add(dev8);
            informazioneVisivaSystem.Devices.Add(dev9);
            informazioneVisivaSystem.Devices.Add(dev10);
            altrePerifericheSystem.Devices.Add(dev11);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev11.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            dev11.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);
            node.Devices.Add(dev10);
            node.Devices.Add(dev11);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione13439084134410_ha4Sistemi_DiffusioneSonoraError_InformazioneVisivaOk_SistemaTelefoniaOk_AltrePerifericheNotActive_MiAspettoError
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 13439084134410;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT04000";
            dev3.Type = "TT10220";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "DT05000";
            dev8.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Error.Severity;
            informazioneVisivaSystem.Severity = Ok.Severity;
            sistemaTelefoniaSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Error.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            sistemaTelefoniaSystem.Devices.Add(dev7);
            altrePerifericheSystem.Devices.Add(dev8);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione1838377467914_ha3Sistemi_DiffusioneSonoraNotActive_InformazioneVisivaNotActive_AltrePerifericheNotActive_MiAspettoNotActive()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 1838377467914;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "DT13000";
            dev3.Type = "TT10210";
            dev4.Type = "SOLM000";
            dev5.Type = "SYTM100";
            dev6.Type = "SYTM100";
            dev7.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = NotActive.Severity;
            informazioneVisivaSystem.Severity = NotActive.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = NoMACAddress.SeverityDetail;
            dev1.SeverityDetail = ServerNotActive.SeverityDetail;
            dev2.SeverityDetail = ServerNotActive.SeverityDetail;
            dev3.SeverityDetail = ServerNotActive.SeverityDetail;
            dev4.SeverityDetail = ServerNotActive.SeverityDetail;
            dev5.SeverityDetail = ServerNotActive.SeverityDetail;
            dev6.SeverityDetail = ServerNotActive.SeverityDetail;
            dev7.SeverityDetail = NoMACAddress.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            altrePerifericheSystem.Devices.Add(dev7);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(NotActive.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione1426008440834_ha1Sistema_AltrePerifericheNotClassified_MiAspettoNotActive()
        {
            // initialization
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 1426008440834;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "STLC1000";
            dev1.ExcludedFromParentStatus = true;
            dev1.InMaintenance = true;
            altrePerifericheSystem.InMaintenance = true;

            // severities
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail;
            dev1.SeverityDetail = ServerNotActive.SeverityDetail;

            // relations
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(altrePerifericheSystem);
            altrePerifericheSystem.Devices.Add(dev1);
            dev1.Server = server;
            dev1.Node = node;
            node.Devices.Add(dev1);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
			Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(NotActive.Severity, node.Severity);
            Assert.IsTrue(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione12253673357322_ha2Sistemi_DiffusioneSonoraOk_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();
            var dev11 = EntityFactory.GetDevice();
            var dev12 = EntityFactory.GetDevice();
            var dev13 = EntityFactory.GetDevice();
            var dev14 = EntityFactory.GetDevice();
            var dev15 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 12253673357322;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "PEAVGD102";
            dev2.Type = "PEAVGD200";
            dev3.Type = "PEAVGA110";
            dev4.Type = "PEAVGA120";
            dev5.Type = "PEAVGA130";
            dev6.Type = "PEAVGA140";
            dev7.Type = "PEAVGA210";
            dev8.Type = "PEAVGA220";
            dev9.Type = "PEAVGA230";
            dev10.Type = "PEAVGA240";
            dev11.Type = "PEAVGL101";
            dev12.Type = "PEAVGL201";
            dev13.Type = "PEAVGM101";
            dev14.Type = "PEAVGM201";
            dev15.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;
            dev9.SeverityDetail = Ok.SeverityDetail;
            dev10.SeverityDetail = Ok.SeverityDetail;
            dev11.SeverityDetail = Ok.SeverityDetail;
            dev12.SeverityDetail = Ok.SeverityDetail;
            dev13.SeverityDetail = Ok.SeverityDetail;
            dev14.SeverityDetail = Ok.SeverityDetail;
            dev15.SeverityDetail = Unknown.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            diffusioneSonoraSystem.Devices.Add(dev5);
            diffusioneSonoraSystem.Devices.Add(dev6);
            diffusioneSonoraSystem.Devices.Add(dev7);
            diffusioneSonoraSystem.Devices.Add(dev8);
            diffusioneSonoraSystem.Devices.Add(dev9);
            diffusioneSonoraSystem.Devices.Add(dev10);
            diffusioneSonoraSystem.Devices.Add(dev11);
            diffusioneSonoraSystem.Devices.Add(dev12);
            diffusioneSonoraSystem.Devices.Add(dev13);
            diffusioneSonoraSystem.Devices.Add(dev14);
            altrePerifericheSystem.Devices.Add(dev15);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev11.Server = server;
            dev12.Server = server;
            dev13.Server = server;
            dev14.Server = server;
            dev15.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            dev11.Node = node;
            dev12.Node = node;
            dev13.Node = node;
            dev14.Node = node;
            dev15.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);
            node.Devices.Add(dev10);
            node.Devices.Add(dev11);
            node.Devices.Add(dev12);
            node.Devices.Add(dev13);
            node.Devices.Add(dev14);
            node.Devices.Add(dev15);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione12425471721482_ha2Sistemi_DiffusioneSonoraOk_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 12425471721482;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            altrePerifericheSystem.Devices.Add(dev2);
            dev1.Server = server;
            dev2.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione9337390170122_ha4Sistemi_DiffusioneSonoraWarning_InformazioneVisivaOk_SistemaTelefoniaOk_AltrePerifericheNotActive_MiAspettoOk
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 9337390170122;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "TT10220";
            dev3.Type = "DT13000";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "DT05000";
            dev6.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Warning.Severity;
            informazioneVisivaSystem.Severity = Ok.Severity;
            sistemaTelefoniaSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            sistemaTelefoniaSystem.Devices.Add(dev5);
            altrePerifericheSystem.Devices.Add(dev6);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione7417546276875_ha3Sistemi_DiffusioneSonoraWarning_InformazioneVisivaOk_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 7417546276875;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT05L00";
            dev3.Type = "TT10210V3";
            dev4.Type = "DT04000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Warning.Severity;
            informazioneVisivaSystem.Severity = Ok.Severity;
            altrePerifericheSystem.Severity = NotActive.Severity;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Unknown.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Unknown.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            altrePerifericheSystem.Devices.Add(dev6);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione6807596564481_ha2Sistemi_DiffusioneSonoraOk_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 6807596564481;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT05L00";
            dev3.Type = "TT10210V3";
            dev4.Type = "DT04000";
            dev5.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Warning.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Unknown.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            altrePerifericheSystem.Devices.Add(dev5);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione7224208392193_ha3Sistemi_DiffusioneSonoraOk_InformazioneVisivaError_AltrePerifericheNotActive_MiAspettoWarning()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 7224208392193;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT05L00";
            dev3.Type = "TT10210V3";
            dev4.Type = "DT04000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Error;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev6.SeverityDetail = Unknown.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            altrePerifericheSystem.Devices.Add(dev6);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione12408233197569_ha4Sistemi_DiffusioneSonoraOk_InformazioneVisivaWarning_SistemaTelefoniaError_AltrePerifericheNotActive_MiAspettoWarning
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 12408233197569;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "TT10210V3";
            dev3.Type = "DT05B10";
            dev4.Type = "DT13000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "DT01000";
            dev7.Type = "DT05000";
            dev8.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Warning;
            sistemaTelefoniaSystem.Severity = Severity.Error;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev6.SeverityDetail = Warning.SeverityDetail;
            dev7.SeverityDetail = Unknown.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            sistemaTelefoniaSystem.Devices.Add(dev6);
            sistemaTelefoniaSystem.Devices.Add(dev7);
            altrePerifericheSystem.Devices.Add(dev8);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione13112615370754_ha4Sistemi_DiffusioneSonoraWarning_InformazioneVisivaWarning_SistemaTelefoniaError_AltrePerifericheNotActive_MiAspettoWarning
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 13112615370754;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "TT10220";
            dev2.Type = "DT04000";
            dev3.Type = "DT13000";
            dev4.Type = "DT13000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "INFSTAZ1";
            dev9.Type = "HA01000";
            dev10.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Warning;
            informazioneVisivaSystem.Severity = Severity.Warning;
            sistemaTelefoniaSystem.Severity = Severity.Error;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Unknown.SeverityDetail;
            dev5.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev6.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev7.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev8.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev9.SeverityDetail = Error.SeverityDetail;
            dev10.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            informazioneVisivaSystem.Devices.Add(dev7);
            informazioneVisivaSystem.Devices.Add(dev8);
            sistemaTelefoniaSystem.Devices.Add(dev9);
            altrePerifericheSystem.Devices.Add(dev10);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);
            node.Devices.Add(dev10);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione43022767620098_ha4Sistemi_DiffusioneSonoraOk_InformazioneVisivaOk_SistemaTelefoniaError_AltrePerifericheNotActive_MiAspettoWarning
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();
            var dev11 = EntityFactory.GetDevice();
            var dev12 = EntityFactory.GetDevice();
            var dev13 = EntityFactory.GetDevice();
            var dev14 = EntityFactory.GetDevice();
            var dev15 = EntityFactory.GetDevice();
            var dev16 = EntityFactory.GetDevice();
            var dev17 = EntityFactory.GetDevice();
            var dev18 = EntityFactory.GetDevice();
            var dev19 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 43022767620098;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "TT10210V3";
            dev2.Type = "DT04000";
            dev3.Type = "DT04000";
            dev4.Type = "DT13000";
            dev5.Type = "DT13000";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "INFSTAZ1";
            dev9.Type = "INFSTAZ1";
            dev10.Type = "INFSTAZ1";
            dev11.Type = "INFSTAZ1";
            dev12.Type = "INFSTAZ1";
            dev13.Type = "INFSTAZ1";
            dev14.Type = "INFSTAZ1";
            dev15.Type = "INFSTAZ1";
            dev16.Type = "INFSTAZ1";
            dev17.Type = "INFSTAZ1";
            dev18.Type = "HA01000";
            dev19.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Ok;
            sistemaTelefoniaSystem.Severity = Severity.Error;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;
            dev9.SeverityDetail = Ok.SeverityDetail;
            dev10.SeverityDetail = Ok.SeverityDetail;
            dev11.SeverityDetail = Ok.SeverityDetail;
            dev12.SeverityDetail = Ok.SeverityDetail;
            dev13.SeverityDetail = Ok.SeverityDetail;
            dev14.SeverityDetail = Ok.SeverityDetail;
            dev15.SeverityDetail = Ok.SeverityDetail;
            dev16.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev17.SeverityDetail = Ok.SeverityDetail;
            dev18.SeverityDetail = Error.SeverityDetail;
            dev19.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            diffusioneSonoraSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            informazioneVisivaSystem.Devices.Add(dev7);
            informazioneVisivaSystem.Devices.Add(dev8);
            informazioneVisivaSystem.Devices.Add(dev9);
            informazioneVisivaSystem.Devices.Add(dev10);
            informazioneVisivaSystem.Devices.Add(dev11);
            informazioneVisivaSystem.Devices.Add(dev12);
            informazioneVisivaSystem.Devices.Add(dev13);
            informazioneVisivaSystem.Devices.Add(dev14);
            informazioneVisivaSystem.Devices.Add(dev15);
            informazioneVisivaSystem.Devices.Add(dev16);
            informazioneVisivaSystem.Devices.Add(dev17);
            sistemaTelefoniaSystem.Devices.Add(dev18);
            altrePerifericheSystem.Devices.Add(dev19);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev11.Server = server;
            dev12.Server = server;
            dev13.Server = server;
            dev14.Server = server;
            dev15.Server = server;
            dev16.Server = server;
            dev17.Server = server;
            dev18.Server = server;
            dev19.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            dev11.Node = node;
            dev12.Node = node;
            dev13.Node = node;
            dev14.Node = node;
            dev15.Node = node;
            dev16.Node = node;
            dev17.Node = node;
            dev18.Node = node;
            dev19.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);
            node.Devices.Add(dev10);
            node.Devices.Add(dev11);
            node.Devices.Add(dev12);
            node.Devices.Add(dev13);
            node.Devices.Add(dev14);
            node.Devices.Add(dev15);
            node.Devices.Add(dev16);
            node.Devices.Add(dev17);
            node.Devices.Add(dev18);
            node.Devices.Add(dev19);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione1576333213698_ha4Sistemi_DiffusioneSonoraOk_InformazioneVisivaWarning_SistemaTelefoniaError_AltrePerifericheNotActive_MiAspettoWarning
            ()
        {
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 1576333213698;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "TT10220";
            dev2.Type = "DT04000";
            dev3.Type = "DT13000";
            dev4.Type = "DT13000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "HA01000";
            dev8.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Warning;
            sistemaTelefoniaSystem.Severity = Severity.Error;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Warning.SeverityDetail;
            dev5.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev6.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev7.SeverityDetail = Error.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            sistemaTelefoniaSystem.Devices.Add(dev7);
            altrePerifericheSystem.Devices.Add(dev8);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione4084592869378_ha2Sistemi_InformazioneVisivaWarning_AltrePerifericheNotActive_MiAspettoWarning()
        {
            // initialization
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 4084592869378;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "STLC1000";

            // severities
            informazioneVisivaSystem.Severity = Severity.Warning;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev2.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev3.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev4.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev5.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev6.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev7.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;

            // relations
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            informazioneVisivaSystem.Devices.Add(dev1);
            informazioneVisivaSystem.Devices.Add(dev2);
            informazioneVisivaSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            informazioneVisivaSystem.Devices.Add(dev7);
            altrePerifericheSystem.Devices.Add(dev8);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione12537088507906_ha2Sistemi_InformazioneVisivaWarning_AltrePerifericheNotActive_MiAspettoWarning()
        {
            // initialization
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();
            var dev11 = EntityFactory.GetDevice();
            var dev12 = EntityFactory.GetDevice();
            var dev13 = EntityFactory.GetDevice();
            var dev14 = EntityFactory.GetDevice();
            var dev15 = EntityFactory.GetDevice();
            var dev16 = EntityFactory.GetDevice();
            var dev17 = EntityFactory.GetDevice();
            var dev18 = EntityFactory.GetDevice();
            var dev19 = EntityFactory.GetDevice();
            var dev20 = EntityFactory.GetDevice();
            var dev21 = EntityFactory.GetDevice();
            var dev22 = EntityFactory.GetDevice();
            var dev23 = EntityFactory.GetDevice();
            var dev24 = EntityFactory.GetDevice();
            var dev25 = EntityFactory.GetDevice();
            var dev26 = EntityFactory.GetDevice();
            var dev27 = EntityFactory.GetDevice();
            var dev28 = EntityFactory.GetDevice();
            var dev29 = EntityFactory.GetDevice();
            var dev30 = EntityFactory.GetDevice();
            var dev31 = EntityFactory.GetDevice();
            var dev32 = EntityFactory.GetDevice();
            var dev33 = EntityFactory.GetDevice();
            var dev34 = EntityFactory.GetDevice();
            var dev35 = EntityFactory.GetDevice();
            var dev36 = EntityFactory.GetDevice();
            var dev37 = EntityFactory.GetDevice();
            var dev38 = EntityFactory.GetDevice();
            var dev39 = EntityFactory.GetDevice();
            var dev40 = EntityFactory.GetDevice();
            var dev41 = EntityFactory.GetDevice();
            var dev42 = EntityFactory.GetDevice();
            var dev43 = EntityFactory.GetDevice();
            var dev44 = EntityFactory.GetDevice();
            var dev45 = EntityFactory.GetDevice();
            var dev46 = EntityFactory.GetDevice();
            var dev47 = EntityFactory.GetDevice();
            var dev48 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 12537088507906;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "INFSTAZ1";
            dev2.Type = "INFSTAZ1";
            dev3.Type = "INFSTAZ1";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "INFSTAZ1";
            dev9.Type = "INFSTAZ1";
            dev10.Type = "INFSTAZ1";
            dev11.Type = "INFSTAZ1";
            dev12.Type = "INFSTAZ1";
            dev13.Type = "INFSTAZ1";
            dev14.Type = "INFSTAZ1";
            dev15.Type = "INFSTAZ1";
            dev16.Type = "INFSTAZ1";
            dev17.Type = "INFSTAZ1";
            dev18.Type = "INFSTAZ1";
            dev19.Type = "INFSTAZ1";
            dev20.Type = "INFSTAZ1";
            dev21.Type = "INFSTAZ1";
            dev22.Type = "INFSTAZ1";
            dev23.Type = "INFSTAZ1";
            dev24.Type = "INFSTAZ1";
            dev25.Type = "INFSTAZ1";
            dev26.Type = "INFSTAZ1";
            dev27.Type = "INFSTAZ1";
            dev28.Type = "INFSTAZ1";
            dev29.Type = "INFSTAZ1";
            dev30.Type = "INFSTAZ1";
            dev31.Type = "INFSTAZ1";
            dev32.Type = "INFSTAZ1";
            dev33.Type = "INFSTAZ1";
            dev34.Type = "INFSTAZ1";
            dev35.Type = "INFSTAZ1";
            dev36.Type = "INFSTAZ1";
            dev37.Type = "INFSTAZ1";
            dev38.Type = "INFSTAZ1";
            dev39.Type = "INFSTAZ1";
            dev40.Type = "INFSTAZ1";
            dev41.Type = "INFSTAZ1";
            dev42.Type = "INFSTAZ1";
            dev43.Type = "INFSTAZ1";
            dev44.Type = "INFSTAZ1";
            dev45.Type = "INFSTAZ1";
            dev46.Type = "INFSTAZ1";
            dev47.Type = "INFSTAZ1";
            dev48.Type = "STLC1000";

            // severities
            informazioneVisivaSystem.Severity = Severity.Warning;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev7.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev8.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev9.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev10.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev11.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev12.SeverityDetail = Ok.SeverityDetail;
            dev13.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev14.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev15.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev16.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev17.SeverityDetail = Ok.SeverityDetail;
            dev18.SeverityDetail = Ok.SeverityDetail;
            dev19.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev20.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev21.SeverityDetail = Ok.SeverityDetail;
            dev22.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev23.SeverityDetail = Ok.SeverityDetail;
            dev24.SeverityDetail = Ok.SeverityDetail;
            dev25.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev26.SeverityDetail = Ok.SeverityDetail;
            dev27.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev28.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev29.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev30.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev31.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev32.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev33.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev34.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev35.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev36.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev37.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev38.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev39.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev40.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev41.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev42.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev43.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev44.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev45.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev46.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev47.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev48.SeverityDetail = Ok.SeverityDetail;

            // relations
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            informazioneVisivaSystem.Devices.Add(dev1);
            informazioneVisivaSystem.Devices.Add(dev2);
            informazioneVisivaSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            informazioneVisivaSystem.Devices.Add(dev7);
            informazioneVisivaSystem.Devices.Add(dev8);
            informazioneVisivaSystem.Devices.Add(dev9);
            informazioneVisivaSystem.Devices.Add(dev10);
            informazioneVisivaSystem.Devices.Add(dev11);
            informazioneVisivaSystem.Devices.Add(dev12);
            informazioneVisivaSystem.Devices.Add(dev13);
            informazioneVisivaSystem.Devices.Add(dev14);
            informazioneVisivaSystem.Devices.Add(dev15);
            informazioneVisivaSystem.Devices.Add(dev16);
            informazioneVisivaSystem.Devices.Add(dev17);
            informazioneVisivaSystem.Devices.Add(dev18);
            informazioneVisivaSystem.Devices.Add(dev19);
            informazioneVisivaSystem.Devices.Add(dev20);
            informazioneVisivaSystem.Devices.Add(dev21);
            informazioneVisivaSystem.Devices.Add(dev22);
            informazioneVisivaSystem.Devices.Add(dev23);
            informazioneVisivaSystem.Devices.Add(dev24);
            informazioneVisivaSystem.Devices.Add(dev25);
            informazioneVisivaSystem.Devices.Add(dev26);
            informazioneVisivaSystem.Devices.Add(dev27);
            informazioneVisivaSystem.Devices.Add(dev28);
            informazioneVisivaSystem.Devices.Add(dev29);
            informazioneVisivaSystem.Devices.Add(dev30);
            informazioneVisivaSystem.Devices.Add(dev31);
            informazioneVisivaSystem.Devices.Add(dev32);
            informazioneVisivaSystem.Devices.Add(dev33);
            informazioneVisivaSystem.Devices.Add(dev34);
            informazioneVisivaSystem.Devices.Add(dev35);
            informazioneVisivaSystem.Devices.Add(dev36);
            informazioneVisivaSystem.Devices.Add(dev37);
            informazioneVisivaSystem.Devices.Add(dev38);
            informazioneVisivaSystem.Devices.Add(dev39);
            informazioneVisivaSystem.Devices.Add(dev40);
            informazioneVisivaSystem.Devices.Add(dev41);
            informazioneVisivaSystem.Devices.Add(dev42);
            informazioneVisivaSystem.Devices.Add(dev43);
            informazioneVisivaSystem.Devices.Add(dev44);
            informazioneVisivaSystem.Devices.Add(dev45);
            informazioneVisivaSystem.Devices.Add(dev46);
            informazioneVisivaSystem.Devices.Add(dev47);
            altrePerifericheSystem.Devices.Add(dev48);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev11.Server = server;
            dev12.Server = server;
            dev13.Server = server;
            dev14.Server = server;
            dev15.Server = server;
            dev16.Server = server;
            dev17.Server = server;
            dev18.Server = server;
            dev19.Server = server;
            dev20.Server = server;
            dev21.Server = server;
            dev22.Server = server;
            dev23.Server = server;
            dev24.Server = server;
            dev25.Server = server;
            dev26.Server = server;
            dev27.Server = server;
            dev28.Server = server;
            dev29.Server = server;
            dev30.Server = server;
            dev31.Server = server;
            dev32.Server = server;
            dev33.Server = server;
            dev34.Server = server;
            dev35.Server = server;
            dev36.Server = server;
            dev37.Server = server;
            dev38.Server = server;
            dev39.Server = server;
            dev40.Server = server;
            dev41.Server = server;
            dev42.Server = server;
            dev43.Server = server;
            dev44.Server = server;
            dev45.Server = server;
            dev46.Server = server;
            dev47.Server = server;
            dev48.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            dev11.Node = node;
            dev12.Node = node;
            dev13.Node = node;
            dev14.Node = node;
            dev15.Node = node;
            dev16.Node = node;
            dev17.Node = node;
            dev18.Node = node;
            dev19.Node = node;
            dev20.Node = node;
            dev21.Node = node;
            dev22.Node = node;
            dev23.Node = node;
            dev24.Node = node;
            dev25.Node = node;
            dev26.Node = node;
            dev27.Node = node;
            dev28.Node = node;
            dev29.Node = node;
            dev30.Node = node;
            dev31.Node = node;
            dev32.Node = node;
            dev33.Node = node;
            dev34.Node = node;
            dev35.Node = node;
            dev36.Node = node;
            dev37.Node = node;
            dev38.Node = node;
            dev39.Node = node;
            dev40.Node = node;
            dev41.Node = node;
            dev42.Node = node;
            dev43.Node = node;
            dev44.Node = node;
            dev45.Node = node;
            dev46.Node = node;
            dev47.Node = node;
            dev48.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);
            node.Devices.Add(dev10);
            node.Devices.Add(dev11);
            node.Devices.Add(dev12);
            node.Devices.Add(dev13);
            node.Devices.Add(dev14);
            node.Devices.Add(dev15);
            node.Devices.Add(dev16);
            node.Devices.Add(dev17);
            node.Devices.Add(dev18);
            node.Devices.Add(dev19);
            node.Devices.Add(dev20);
            node.Devices.Add(dev21);
            node.Devices.Add(dev22);
            node.Devices.Add(dev23);
            node.Devices.Add(dev24);
            node.Devices.Add(dev25);
            node.Devices.Add(dev26);
            node.Devices.Add(dev27);
            node.Devices.Add(dev28);
            node.Devices.Add(dev29);
            node.Devices.Add(dev30);
            node.Devices.Add(dev31);
            node.Devices.Add(dev32);
            node.Devices.Add(dev33);
            node.Devices.Add(dev34);
            node.Devices.Add(dev35);
            node.Devices.Add(dev36);
            node.Devices.Add(dev37);
            node.Devices.Add(dev38);
            node.Devices.Add(dev39);
            node.Devices.Add(dev40);
            node.Devices.Add(dev41);
            node.Devices.Add(dev42);
            node.Devices.Add(dev43);
            node.Devices.Add(dev44);
            node.Devices.Add(dev45);
            node.Devices.Add(dev46);
            node.Devices.Add(dev47);
            node.Devices.Add(dev48);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione2714499678210_ha3Sistemi_DiffusioneSonoraNotActive_InformazioneVisivaWarning_AltrePerifericheNotActive_MiAspettoWarning()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();
            var dev11 = EntityFactory.GetDevice();
            var dev12 = EntityFactory.GetDevice();
            var dev13 = EntityFactory.GetDevice();
            var dev14 = EntityFactory.GetDevice();
            var dev15 = EntityFactory.GetDevice();
            var dev16 = EntityFactory.GetDevice();
            var dev17 = EntityFactory.GetDevice();
            var dev18 = EntityFactory.GetDevice();
            var dev19 = EntityFactory.GetDevice();
            var dev20 = EntityFactory.GetDevice();
            var dev21 = EntityFactory.GetDevice();
            var dev22 = EntityFactory.GetDevice();
            var dev23 = EntityFactory.GetDevice();
            var dev24 = EntityFactory.GetDevice();
            var dev25 = EntityFactory.GetDevice();
            var dev26 = EntityFactory.GetDevice();
            var dev27 = EntityFactory.GetDevice();
            var dev28 = EntityFactory.GetDevice();
            var dev29 = EntityFactory.GetDevice();
            var dev30 = EntityFactory.GetDevice();
            var dev31 = EntityFactory.GetDevice();
            var dev32 = EntityFactory.GetDevice();
            var dev33 = EntityFactory.GetDevice();
            var dev34 = EntityFactory.GetDevice();
            var dev35 = EntityFactory.GetDevice();
            var dev36 = EntityFactory.GetDevice();
            var dev37 = EntityFactory.GetDevice();
            var dev38 = EntityFactory.GetDevice();
            var dev39 = EntityFactory.GetDevice();
            var dev40 = EntityFactory.GetDevice();
            var dev41 = EntityFactory.GetDevice();
            var dev42 = EntityFactory.GetDevice();
            var dev43 = EntityFactory.GetDevice();
            var dev44 = EntityFactory.GetDevice();
            var dev45 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 2714499678210;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT13000";
            dev3.Type = "INFSTAZ1";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "INFSTAZ1";
            dev9.Type = "INFSTAZ1";
            dev10.Type = "INFSTAZ1";
            dev11.Type = "INFSTAZ1";
            dev12.Type = "INFSTAZ1";
            dev13.Type = "INFSTAZ1";
            dev14.Type = "INFSTAZ1";
            dev15.Type = "INFSTAZ1";
            dev16.Type = "INFSTAZ1";
            dev17.Type = "INFSTAZ1";
            dev18.Type = "INFSTAZ1";
            dev19.Type = "INFSTAZ1";
            dev20.Type = "INFSTAZ1";
            dev21.Type = "INFSTAZ1";
            dev22.Type = "INFSTAZ1";
            dev23.Type = "INFSTAZ1";
            dev24.Type = "INFSTAZ1";
            dev25.Type = "INFSTAZ1";
            dev26.Type = "INFSTAZ1";
            dev27.Type = "INFSTAZ1";
            dev28.Type = "INFSTAZ1";
            dev29.Type = "INFSTAZ1";
            dev30.Type = "INFSTAZ1";
            dev31.Type = "INFSTAZ1";
            dev32.Type = "INFSTAZ1";
            dev33.Type = "INFSTAZ1";
            dev34.Type = "INFSTAZ1";
            dev35.Type = "INFSTAZ1";
            dev36.Type = "INFSTAZ1";
            dev37.Type = "INFSTAZ1";
            dev38.Type = "INFSTAZ1";
            dev39.Type = "INFSTAZ1";
            dev40.Type = "INFSTAZ1";
            dev41.Type = "INFSTAZ1";
            dev42.Type = "INFSTAZ1";
            dev43.Type = "INFSTAZ1";
            dev44.Type = "INFSTAZ1";
            dev45.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.NotActive;
            informazioneVisivaSystem.Severity = Severity.Warning;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail;
            dev2.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail;
            dev3.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev4.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev5.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev6.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev7.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev8.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev9.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev10.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev11.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev12.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev13.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev14.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev15.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev16.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev17.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev18.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev19.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev20.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev21.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev22.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev23.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev24.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev25.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev26.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev27.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev28.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev29.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev30.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev31.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev32.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev33.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev34.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev35.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev36.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev37.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev38.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev39.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev40.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev41.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev42.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev43.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev44.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev45.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            informazioneVisivaSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            informazioneVisivaSystem.Devices.Add(dev7);
            informazioneVisivaSystem.Devices.Add(dev8);
            informazioneVisivaSystem.Devices.Add(dev9);
            informazioneVisivaSystem.Devices.Add(dev10);
            informazioneVisivaSystem.Devices.Add(dev11);
            informazioneVisivaSystem.Devices.Add(dev12);
            informazioneVisivaSystem.Devices.Add(dev13);
            informazioneVisivaSystem.Devices.Add(dev14);
            informazioneVisivaSystem.Devices.Add(dev15);
            informazioneVisivaSystem.Devices.Add(dev16);
            informazioneVisivaSystem.Devices.Add(dev17);
            informazioneVisivaSystem.Devices.Add(dev18);
            informazioneVisivaSystem.Devices.Add(dev19);
            informazioneVisivaSystem.Devices.Add(dev20);
            informazioneVisivaSystem.Devices.Add(dev21);
            informazioneVisivaSystem.Devices.Add(dev22);
            informazioneVisivaSystem.Devices.Add(dev23);
            informazioneVisivaSystem.Devices.Add(dev24);
            informazioneVisivaSystem.Devices.Add(dev25);
            informazioneVisivaSystem.Devices.Add(dev26);
            informazioneVisivaSystem.Devices.Add(dev27);
            informazioneVisivaSystem.Devices.Add(dev28);
            informazioneVisivaSystem.Devices.Add(dev29);
            informazioneVisivaSystem.Devices.Add(dev30);
            informazioneVisivaSystem.Devices.Add(dev31);
            informazioneVisivaSystem.Devices.Add(dev32);
            informazioneVisivaSystem.Devices.Add(dev33);
            informazioneVisivaSystem.Devices.Add(dev34);
            informazioneVisivaSystem.Devices.Add(dev35);
            informazioneVisivaSystem.Devices.Add(dev36);
            informazioneVisivaSystem.Devices.Add(dev37);
            informazioneVisivaSystem.Devices.Add(dev38);
            informazioneVisivaSystem.Devices.Add(dev39);
            informazioneVisivaSystem.Devices.Add(dev40);
            informazioneVisivaSystem.Devices.Add(dev41);
            informazioneVisivaSystem.Devices.Add(dev42);
            informazioneVisivaSystem.Devices.Add(dev43);
            informazioneVisivaSystem.Devices.Add(dev44);
            altrePerifericheSystem.Devices.Add(dev45);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev11.Server = server;
            dev12.Server = server;
            dev13.Server = server;
            dev14.Server = server;
            dev15.Server = server;
            dev16.Server = server;
            dev17.Server = server;
            dev18.Server = server;
            dev19.Server = server;
            dev20.Server = server;
            dev21.Server = server;
            dev22.Server = server;
            dev23.Server = server;
            dev24.Server = server;
            dev25.Server = server;
            dev26.Server = server;
            dev27.Server = server;
            dev28.Server = server;
            dev29.Server = server;
            dev30.Server = server;
            dev31.Server = server;
            dev32.Server = server;
            dev33.Server = server;
            dev34.Server = server;
            dev35.Server = server;
            dev36.Server = server;
            dev37.Server = server;
            dev38.Server = server;
            dev39.Server = server;
            dev40.Server = server;
            dev41.Server = server;
            dev42.Server = server;
            dev43.Server = server;
            dev44.Server = server;
            dev45.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            dev11.Node = node;
            dev12.Node = node;
            dev13.Node = node;
            dev14.Node = node;
            dev15.Node = node;
            dev16.Node = node;
            dev17.Node = node;
            dev18.Node = node;
            dev19.Node = node;
            dev20.Node = node;
            dev21.Node = node;
            dev22.Node = node;
            dev23.Node = node;
            dev24.Node = node;
            dev25.Node = node;
            dev26.Node = node;
            dev27.Node = node;
            dev28.Node = node;
            dev29.Node = node;
            dev30.Node = node;
            dev31.Node = node;
            dev32.Node = node;
            dev33.Node = node;
            dev34.Node = node;
            dev35.Node = node;
            dev36.Node = node;
            dev37.Node = node;
            dev38.Node = node;
            dev39.Node = node;
            dev40.Node = node;
            dev41.Node = node;
            dev42.Node = node;
            dev43.Node = node;
            dev44.Node = node;
            dev45.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);
            node.Devices.Add(dev10);
            node.Devices.Add(dev11);
            node.Devices.Add(dev12);
            node.Devices.Add(dev13);
            node.Devices.Add(dev14);
            node.Devices.Add(dev15);
            node.Devices.Add(dev16);
            node.Devices.Add(dev17);
            node.Devices.Add(dev18);
            node.Devices.Add(dev19);
            node.Devices.Add(dev20);
            node.Devices.Add(dev21);
            node.Devices.Add(dev22);
            node.Devices.Add(dev23);
            node.Devices.Add(dev24);
            node.Devices.Add(dev25);
            node.Devices.Add(dev26);
            node.Devices.Add(dev27);
            node.Devices.Add(dev28);
            node.Devices.Add(dev29);
            node.Devices.Add(dev30);
            node.Devices.Add(dev31);
            node.Devices.Add(dev32);
            node.Devices.Add(dev33);
            node.Devices.Add(dev34);
            node.Devices.Add(dev35);
            node.Devices.Add(dev36);
            node.Devices.Add(dev37);
            node.Devices.Add(dev38);
            node.Devices.Add(dev39);
            node.Devices.Add(dev40);
            node.Devices.Add(dev41);
            node.Devices.Add(dev42);
            node.Devices.Add(dev43);
            node.Devices.Add(dev44);
            node.Devices.Add(dev45);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione7765380562946_ha4Sistemi_DiffusioneSonoraOk_InformazioneVisivaWarning_SistemaTelefoniaError_AltrePerifericheNotActive_MiAspettoWarning
            ()
        {
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 7765380562946;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "TT10210V3";
            dev2.Type = "DT13000";
            dev3.Type = "DT04000";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "DT00000";
            dev7.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Warning;
            sistemaTelefoniaSystem.Severity = Severity.Error;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev5.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail;
            dev6.SeverityDetail = Unknown.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            sistemaTelefoniaSystem.Devices.Add(dev6);
            altrePerifericheSystem.Devices.Add(dev7);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione10625834352643_ha4Sistemi_DiffusioneSonoraWarning_InformazioneVisivaOk_SistemaTelefoniaOk_AltrePerifericheNotActive_MiAspettoWarning
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();
            var dev10 = EntityFactory.GetDevice();
            var dev11 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 10625834352643;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "TT10220";
            dev2.Type = "DT13000";
            dev3.Type = "DT04000";
            dev4.Type = "DT04000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "INFSTAZ1";
            dev8.Type = "INFSTAZ1";
            dev9.Type = "DT00000";
            dev10.Type = "DT00000";
            dev11.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Warning;
            informazioneVisivaSystem.Severity = Severity.Ok;
            sistemaTelefoniaSystem.Severity = Severity.Ok;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Warning.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;
            dev9.SeverityDetail = Warning.SeverityDetail;
            dev10.SeverityDetail = Ok.SeverityDetail;
            dev11.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            informazioneVisivaSystem.Devices.Add(dev7);
            informazioneVisivaSystem.Devices.Add(dev8);
            sistemaTelefoniaSystem.Devices.Add(dev9);
            sistemaTelefoniaSystem.Devices.Add(dev10);
            altrePerifericheSystem.Devices.Add(dev11);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev10.Server = server;
            dev11.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            dev10.Node = node;
            dev11.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);
            node.Devices.Add(dev10);
            node.Devices.Add(dev11);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione13000951267331_ha4Sistemi_DiffusioneSonoraOk_InformazioneVisivaOk_SistemaTelefoniaOk_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();
            var dev9 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 13000951267331;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "DT04000";
            dev3.Type = "DT13000";
            dev4.Type = "TT10220";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "DT00000";
            dev8.Type = "DT01000";
            dev9.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Ok;
            sistemaTelefoniaSystem.Severity = Severity.Ok;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;
            dev9.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            sistemaTelefoniaSystem.Devices.Add(dev7);
            sistemaTelefoniaSystem.Devices.Add(dev8);
            altrePerifericheSystem.Devices.Add(dev9);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev9.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            dev9.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);
            node.Devices.Add(dev9);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione10943661932547_ha3Sistemi_DiffusioneSonoraOk_InformazioneVisivaOk_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 10943661932547;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "DT13000";
            dev3.Type = "TT10210V3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Ok;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Warning.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            altrePerifericheSystem.Devices.Add(dev6);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione7692371689475_ha3Sistemi_DiffusioneSonoraOk_InformazioneVisivaWarning_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 7692371689475;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT04000";
            dev2.Type = "DT13000";
            dev3.Type = "TT10210V3";
            dev4.Type = "DT05L00";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Warning;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            altrePerifericheSystem.Devices.Add(dev7);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione10844884238340_ha3Sistemi_DiffusioneSonoraOk_InformazioneVisivaNotClassified_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 10844884238340;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT04000";
            dev3.Type = "TT10210V3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.NotClassified;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = NotAvailable.SeverityDetail;
            dev5.SeverityDetail = Unknown.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            altrePerifericheSystem.Devices.Add(dev5);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoStazione6081831239694_ha4Sistemi_DiffusioneSonoraOk_InformazioneVisivaWarning_SistemaTelefoniaOk_AltrePerifericheNotActive_MiAspettoOk
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();
            var dev8 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 6081831239694;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "TT10210V3";
            dev2.Type = "DT04000";
            dev3.Type = "DT04000";
            dev4.Type = "DT13000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "DT05000";
            dev8.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Warning;
            sistemaTelefoniaSystem.Severity = Severity.Ok;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev6.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;
            dev8.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            sistemaTelefoniaSystem.Devices.Add(dev7);
            altrePerifericheSystem.Devices.Add(dev8);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev8.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            dev8.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);
            node.Devices.Add(dev8);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione592856350733_ha3Sistemi_DiffusioneSonoraWarning_InformazioneVisivaOk_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 592856350733;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "TT10210V3";
            dev3.Type = "DT04000";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Warning;
            informazioneVisivaSystem.Severity = Severity.Ok;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Error.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            altrePerifericheSystem.Devices.Add(dev5);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione3126867722250_ha3Sistemi_DiffusioneSonoraOk_InformazioneVisivaWarning_AltrePerifericheNotActive_MiAspettoOk()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();
            var dev7 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 3126867722250;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT05L00";
            dev2.Type = "DT04000";
            dev3.Type = "DT13000";
            dev4.Type = "TT10210V3";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "INFSTAZ1";
            dev7.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Warning;
            altrePerifericheSystem.Severity = Severity.NotActive;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev6.SeverityDetail = MIB2OnlyWarning.SeverityDetail;
            dev7.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            informazioneVisivaSystem.Devices.Add(dev6);
            altrePerifericheSystem.Devices.Add(dev7);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev7.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            dev7.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);
            node.Devices.Add(dev7);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Ok.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione13301625454599_ha2Sistemi_DiffusioneSonoraOk_AltrePerifericheOk_ServerDiMonitoraggioConDatiIncompleti_MiAspettoError
            ()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 13301625454599;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT13000";
            dev2.Type = "DT04000";
            dev3.Type = "TT10210V3";
            dev4.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            altrePerifericheSystem.Severity = Severity.Ok;
            server.SeverityDetail = IncompleteServerData.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            altrePerifericheSystem.Devices.Add(dev4);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.IncompleteServerData, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione2933574139911_ha2Sistemi_E_2Server_DiffusioneSonoraOk_InformazioneVisivaOk_EntrambiServerOffline_MiAspettoError()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 2933574139911;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";
            dev3.Type = "PEAVDOM3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Ok;
            informazioneVisivaSystem.Severity = Severity.Ok;
            server1.SeverityDetail = Offline.SeverityDetail;
            server2.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            informazioneVisivaSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            dev1.Server = server1;
            dev2.Server = server1;
            dev3.Server = server1;
            dev4.Server = server2;
            dev5.Server = server2;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.STLC1000ServerNotReachable, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione4118959226883_nonHaSistemi_MiAspettoNotActive()
        {
            // initialization
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();

            // entity data
            node.NodeId = 4118959226883;

            // severities
            server.SeverityDetail = Ok.SeverityDetail;

            // relations

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
			Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(NotActive.Severity, node.Severity);
            Assert.IsTrue(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione1700938186763_ha2Sistemi_SistemaTelefoniaError_AltrePerifericheNotActive_MiAspettoNotActive()
        {
            // initialization
            var sistemaTelefoniaSystem = EntityFactory.GetNodeSystem(false);
			var altrePerifericheSystem = EntityFactory.GetNodeSystem(false);
            var node = EntityFactory.GetNode();
			var server = EntityFactory.GetServer(false);
			var dev1 = EntityFactory.GetDevice(false);
			var dev4 = EntityFactory.GetDevice(false);

            // entity data
            node.NodeId = 1700938186763;
            sistemaTelefoniaSystem.SystemId = Systems.SistemaTelefonia;
            altrePerifericheSystem.SystemId = Systems.AltrePeriferiche;
            dev1.Type = "DT00000";
            dev4.Type = "STLC1000";

            // severities
            sistemaTelefoniaSystem.Severity = Error.Severity;
			sistemaTelefoniaSystem.ExcludedFromParentStatus = true;
			altrePerifericheSystem.Severity = NotActive.Severity;
			altrePerifericheSystem.ExcludedFromParentStatus = true;

            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;

            // relations
            sistemaTelefoniaSystem.Node = node;
            sistemaTelefoniaSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(sistemaTelefoniaSystem);
            node.Systems.Add(altrePerifericheSystem);
            sistemaTelefoniaSystem.Devices.Add(dev1);
            altrePerifericheSystem.Devices.Add(dev4);
            dev1.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev4.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev4);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
			Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(NotActive.Severity, node.Severity);
            Assert.IsTrue(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione_ha2Sistemi_DiffusioneSonoraWarning_DiagnosticaError_MiAspettoWarning_PrevaleSistemaConPZ_SuErroreAltroSistema()
        {
            // initialization
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var altrePerifericheSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();

            // entity data
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            altrePerifericheSystem.SystemId = Systems.Diagnostica;
            dev1.Type = "DT13000";
            dev2.Type = "DT04000";
            dev3.Type = "TT10210V3";
            dev4.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Warning;
            altrePerifericheSystem.Severity = Severity.Error;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Ok.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Warning.SeverityDetail;
            dev4.SeverityDetail = Unknown.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            altrePerifericheSystem.Node = node;
            altrePerifericheSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(altrePerifericheSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            altrePerifericheSystem.Devices.Add(dev4);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione2783211618305_ha3Sistemi_DiffusioneSonoraError_InformazioneVisivaError_DiagnosticaOk_MiAspettoError_Forzato()
        {
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var diagnosticaSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 2783211618305;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            diagnosticaSystem.SystemId = Systems.Diagnostica;
            dev1.Type = "TT10210";
            dev2.Type = "DT13000";
            dev3.Type = "DT00L00";
            dev4.Type = "DT04000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Error;
            informazioneVisivaSystem.Severity = Severity.Error;
            diagnosticaSystem.Severity = Severity.Ok;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev2.SeverityDetail = Unknown.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Unknown.SeverityDetail;
            dev5.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            diagnosticaSystem.Node = node;
            diagnosticaSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(diagnosticaSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            diagnosticaSystem.Devices.Add(dev6);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.AtLeastOnePZInError, node.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoStazione2783211618305_ha3Sistemi_DiffusioneSonoraError_InformazioneVisivaError_DiagnosticaOk_ConPZNonAttivo_MiAspettoWarning()
        {
            var diffusioneSonoraSystem = EntityFactory.GetNodeSystem();
            var informazioneVisivaSystem = EntityFactory.GetNodeSystem();
            var diagnosticaSystem = EntityFactory.GetNodeSystem();
            var node = EntityFactory.GetNode();
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();
            var dev6 = EntityFactory.GetDevice();

            // entity data
            node.NodeId = 2783211618305;
            diffusioneSonoraSystem.SystemId = Systems.DiffusioneSonora;
            informazioneVisivaSystem.SystemId = Systems.InformazioneVisiva;
            diagnosticaSystem.SystemId = Systems.Diagnostica;
            dev1.Type = "TT10210";
            dev2.Type = "DT13000";
            dev3.Type = "DT00L00";
            dev4.Type = "DT04000";
            dev5.Type = "INFSTAZ1";
            dev6.Type = "STLC1000";

            // severities
            diffusioneSonoraSystem.Severity = Severity.Error;
            informazioneVisivaSystem.Severity = Severity.Error;
            diagnosticaSystem.Severity = Severity.Ok;
            server.SeverityDetail = Ok.SeverityDetail;
            dev1.SeverityDetail = Unknown.SeverityDetail;
            dev1.IsActive = 0;
            dev2.SeverityDetail = Unknown.SeverityDetail;
            dev3.SeverityDetail = Unknown.SeverityDetail;
            dev4.SeverityDetail = Unknown.SeverityDetail;
            dev5.SeverityDetail = DiagnosticOffline.SeverityDetail;
            dev6.SeverityDetail = Ok.SeverityDetail;

            // relations
            diffusioneSonoraSystem.Node = node;
            diffusioneSonoraSystem.Parent = node;
            informazioneVisivaSystem.Node = node;
            informazioneVisivaSystem.Parent = node;
            diagnosticaSystem.Node = node;
            diagnosticaSystem.Parent = node;
            node.Systems.Add(diffusioneSonoraSystem);
            node.Systems.Add(informazioneVisivaSystem);
            node.Systems.Add(diagnosticaSystem);
            diffusioneSonoraSystem.Devices.Add(dev1);
            diffusioneSonoraSystem.Devices.Add(dev2);
            diffusioneSonoraSystem.Devices.Add(dev3);
            diffusioneSonoraSystem.Devices.Add(dev4);
            informazioneVisivaSystem.Devices.Add(dev5);
            diagnosticaSystem.Devices.Add(dev6);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;
            dev6.Server = server;
            dev1.Node = node;
            dev2.Node = node;
            dev3.Node = node;
            dev4.Node = node;
            dev5.Node = node;
            dev6.Node = node;
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            node.Devices.Add(dev6);

            // formulas
            Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

            // act
            Utility.Eval(node);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Warning.Severity, node.Severity);
            Assert.IsFalse(node.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, node.ForcedSeverityLevel);
        }

        #endregion

        #region Test formule custom nodi

        [Test]
        public void QuandoStazioneHa_SistemaAudioInErrore_E_SistemaVideoInErrore_MiAspettoSistemaAudioDiColoreLimeESistemaVideoDiColoreViola()
        {
            // initialization
            var devVideo1 = EntityFactory.GetDevice(false);
            var devVideo2 = EntityFactory.GetDevice(false);
            var devPZ = EntityFactory.GetDevice(false);
            var virtObj = EntityFactory.GetVirtualObject();
            var audioSystem = EntityFactory.GetNodeSystem();
            var videoSystem = EntityFactory.GetNodeSystem(false);
            var node = EntityFactory.GetNode(false);

            Color lime = ColorTranslator.FromHtml("#BFFF00");
            Color violet = ColorTranslator.FromHtml("#7F00FF");

            // entity data
            devPZ.Name = "PZ1";

            // severities
            devVideo1.SeverityDetail = Ok.SeverityDetail;
            devVideo2.SeverityDetail = Offline.SeverityDetail;
            devPZ.SeverityDetail = Error.SeverityDetail;

            // relations
            virtObj.Children.AddEntity(devVideo1);
            virtObj.Children.AddEntity(devVideo2);
            virtObj.Parent = videoSystem;
            videoSystem.Node = node;
            devPZ.System = audioSystem;
            audioSystem.Node = node;

			// formulas
			Utility.SetFormula(audioSystem, @".\Lib\ObjectScripts\TestSystem_AudioLime.py");
			Utility.SetFormula(virtObj, @".\Lib\ObjectScripts\TestVirtualObject_VideoViolet.py");

            // ents that require navigation
            audioSystem.IsNavigationActive = true;
            virtObj.IsNavigationActive = true;
            // la navigazione va attivata anche quando nella formula si usa 
            // una risoluzione di oggetto per nome (es: .PZ1), questo per rendere possibile l'accesso alla cache

            // act
			Utility.Eval(audioSystem);
			Utility.Eval(virtObj);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(audioSystem));

            Assert.IsFalse(Utility.ObjectHasErrors(virtObj));

            Assert.AreEqual(lime, audioSystem.Color);
            Assert.AreEqual(violet, videoSystem.Color);
        }

        #endregion

		#region 

		[Test]
		public void QuandoStazioneHa_SistemaFDSInOk_E_SistemaDiagnosticaInOk_ConFormulaCustomStatoPeggioreDeiFigli_MiAspettoNodoOk()
		{
			// initialization
			var fdsSystem = EntityFactory.GetNodeSystem(false);
			var diagnosticaSystem = EntityFactory.GetNodeSystem(false);
			var node = EntityFactory.GetNode();

			fdsSystem.SystemId = Systems.SistemaFDS;
			diagnosticaSystem.SystemId = Systems.Diagnostica;

			// severities
			fdsSystem.Severity = Severity.Ok;
			diagnosticaSystem.Severity = Severity.Ok;

			// relations
			Utility.SetBaseRelation(node, fdsSystem);
			Utility.SetBaseRelation(node, diagnosticaSystem);
			fdsSystem.Parent = node;
			diagnosticaSystem.Parent = node;
			fdsSystem.Node = node;
			diagnosticaSystem.Node = node;
			node.Children.AddEntity(fdsSystem);
			node.Children.AddEntity(diagnosticaSystem);

			// formulas
			Utility.SetFormula(fdsSystem, @".\Lib\NodeSystemDefaultStatus.py");
			Utility.SetFormula(diagnosticaSystem, @".\Lib\NodeSystemDefaultStatus.py");
			Utility.SetFormula(node, @".\Lib\GroupScripts\GetWorst.py");

			fdsSystem.IsNavigationActive = true;
			diagnosticaSystem.IsNavigationActive = true;

			// act
			Utility.Eval(fdsSystem);
			Utility.Eval(diagnosticaSystem);
			Utility.Eval(node);

			// assert
			Assert.IsFalse(Utility.ObjectHasErrors(fdsSystem));
			Assert.IsFalse(Utility.ObjectHasErrors(diagnosticaSystem));
			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(Severity.Ok, node.Severity);
		}

		#endregion
	}
}
﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
	[TestFixture]
	public class TemplateStatisticByObject
	{
		[TestFixtureSetUp]
		public void SetUp ()
		{
			GrisObject.Cache.Reset();
		}

		#region Quando una stazione ha 2 periferiche in stato di ok e 1 in stato di errore, con la formula dei 'pesi per oggetto' con tutti pesi standard -> la stazione risulta gialla
		[Test]
		public void QuandoStazione_con2PerifVerdi_e1PerifRossa_conFormulaPesiPerOggetto_MiAspettoStazioneInWarning ()
		{
			// initialization
			var node = EntityFactory.GetNode();
			var dev1 = EntityFactory.GetDevice(false);
			var dev2 = EntityFactory.GetDevice(false);
			var dev3 = EntityFactory.GetDevice(false);

			// entity data
			dev3.Name = "Pannello Zone";
			dev1.SeverityDetail = Ok.SeverityDetail;
			dev2.SeverityDetail = Ok.SeverityDetail;
			dev3.SeverityDetail = Error.SeverityDetail;

			// relations
			node.Devices.AddEntity(dev1);
			node.Devices.AddEntity(dev2);
			node.Devices.AddEntity(dev3);

			Utility.SetFormula(node, @".\Lib\ObjectScripts\TestNode_WithDefaultFormulaByObjects.py");

			// ents that require navigation
			dev1.IsNavigationActive = true;
			dev2.IsNavigationActive = true;
			dev3.IsNavigationActive = true;
			node.IsNavigationActive = true;

			Utility.Eval(node);

			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(Warning.Severity, node.Severity);
		}
		#endregion

		#region Quando un sistema ha 5 periferiche in stato di ok e 1 in stato di errore, con la formula dei 'pesi per oggetto' con Pannello a Zone impostato come Errore Bloccante -> il sistema risulta rosso
		[Test]
		public void QuandoSistema_con5PerifVerdi_e1PerifRossa_ConPZInErrore_eErroreBloccante_conFormulaPesiPerOggetto_MiAspettoSistemaInErrore ()
		{
			// initialization
			var sys = EntityFactory.GetNodeSystem();
			var dev1 = EntityFactory.GetDevice(false);
			var dev2 = EntityFactory.GetDevice(false);
			var dev3 = EntityFactory.GetDevice(false);
			var dev4 = EntityFactory.GetDevice(false);
			var dev5 = EntityFactory.GetDevice(false);

			// entity data
			dev5.Name = "Pannello Zone";
			dev1.SeverityDetail = Ok.SeverityDetail;
			dev2.SeverityDetail = Ok.SeverityDetail;
			dev3.SeverityDetail = Ok.SeverityDetail;
			dev4.SeverityDetail = Ok.SeverityDetail;
			dev5.SeverityDetail = Error.SeverityDetail;

			// relations
			sys.Devices.AddEntity(dev1);
			sys.Devices.AddEntity(dev2);
			sys.Devices.AddEntity(dev3);
			sys.Devices.AddEntity(dev4);
			sys.Devices.AddEntity(dev5);

			Utility.SetFormula(sys, @".\Lib\ObjectScripts\TestSystem_WithDefaultFormulaByObjects_PannelloZoneBloccante.py");

			// ents that require navigation
			sys.IsNavigationActive = true;

			Utility.Eval(sys);

			Assert.IsFalse(Utility.ObjectHasErrors(sys));

			Assert.AreEqual(Error.Severity, sys.Severity);
		}
		#endregion

		#region Quando una stazione ha 3 sistemi di cui 2 in stato di Ok e uno ha 5 periferiche in stato di ok e 1 in stato di errore, con la formula dei 'pesi per oggetto' con Pannello a Zone impostato come Errore Bloccante -> la stazione risulta gialla
		[Test]
		public void QuandoStazione_con2SistemiOk_e1Sistema_con5PerifVerdi_e1PerifRossa_ConPZInErrore_eErroreBloccante_conFormulaPesiPerOggetto_MiAspettoStazioneInWarning ()
		{
			// initialization
			var sys = EntityFactory.GetNodeSystem();
			var dev1 = EntityFactory.GetDevice(false);
			var dev2 = EntityFactory.GetDevice(false);
			var dev3 = EntityFactory.GetDevice(false);
			var dev4 = EntityFactory.GetDevice(false);
			var dev5 = EntityFactory.GetDevice(false);
			var sys2 = EntityFactory.GetNodeSystem(false);
			var sys3 = EntityFactory.GetNodeSystem(false);
			var node = EntityFactory.GetNode();
			var server = EntityFactory.GetServer(false);

			// entity data
			dev5.Name = "Pannello Zone";
			dev1.SeverityDetail = Ok.SeverityDetail;
			dev2.SeverityDetail = Ok.SeverityDetail;
			dev3.SeverityDetail = Ok.SeverityDetail;
			dev4.SeverityDetail = Ok.SeverityDetail;
			dev5.SeverityDetail = Error.SeverityDetail;
            server.SeverityDetail = Ok.SeverityDetail;
			sys2.Severity = Ok.Severity;
			sys3.Severity = Ok.Severity;

			// relations
			sys.Devices.AddEntity(dev1);
			sys.Devices.AddEntity(dev2);
			sys.Devices.AddEntity(dev3);
			sys.Devices.AddEntity(dev4);
			sys.Devices.AddEntity(dev5);
			node.Systems.AddEntity(sys);
			node.Systems.AddEntity(sys2);
			node.Systems.AddEntity(sys3);
            node.Devices.Add(dev1);
            node.Devices.Add(dev2);
            node.Devices.Add(dev3);
            node.Devices.Add(dev4);
            node.Devices.Add(dev5);
            dev1.Server = server;
            dev2.Server = server;
            dev3.Server = server;
            dev4.Server = server;
            dev5.Server = server;

			// caching
            Utility.CacheEntities(dev1, dev2, dev3, dev4, dev5, sys, sys2, sys3, node);

			// formulas
			Utility.SetFormula(sys, @".\Lib\ObjectScripts\TestSystem_WithDefaultFormulaByObjects_PannelloZoneBloccante.py");
			Utility.SetFormula(node, @".\Lib\NodeDefaultStatus.py");

			// ents that require navigation
			sys.IsNavigationActive = true;
			node.IsNavigationActive = true;

			Utility.Eval(sys);
			Utility.Eval(node);

			// node assertions
			Assert.IsFalse(Utility.ObjectHasErrors(node));

            Assert.AreEqual(Error.Severity, sys.Severity);
            Assert.AreEqual(Warning.Severity, node.Severity);
		}
		#endregion

		#region Quando una stazione ha 5 sistemi di cui quello 99 con tutte le severità trascurabili ed il sistema audio con tutte le severità gravi, il sistema 99 è in errore, tutti gli altri in ok, il sistema audio è in warning -> la stazione è gialla
		[Test]
		public void QuandoStazione_con3SistemiOK_eIlSistemaAudioInWarningETutteleSeverityGravi_eIlSistema99InErroreETutteleSeverityTrascurabili_MiAspettoStazioneInWarning ()
		{
			// initialization
			var node = EntityFactory.GetNode();
			var sysInformazioneVisiva = EntityFactory.GetNodeSystem(false);
			var sysDiffSonora = EntityFactory.GetNodeSystem(false);
			var sysAltrePeriferiche = EntityFactory.GetNodeSystem(false);
			var sysIlluninazione = EntityFactory.GetNodeSystem(false);
			var sysTelefonia = EntityFactory.GetNodeSystem(false);

			// entity data
			sysAltrePeriferiche.TypeId = 99;
			sysAltrePeriferiche.Severity = Error.Severity;
			sysDiffSonora.TypeId = 1;
			sysDiffSonora.Severity = Warning.Severity;
			sysInformazioneVisiva.TypeId = 2;
			sysInformazioneVisiva.Severity = Ok.Severity;
			sysIlluninazione.TypeId = 3;
			sysIlluninazione.Severity = Ok.Severity;
			sysTelefonia.TypeId = 4;
			sysTelefonia.Severity = Ok.Severity;

			// relations
			node.Systems.AddEntity(sysAltrePeriferiche);
			node.Systems.AddEntity(sysInformazioneVisiva);
			node.Systems.AddEntity(sysDiffSonora);
			node.Systems.AddEntity(sysIlluninazione);
			node.Systems.AddEntity(sysTelefonia);

			// formulas
			Utility.SetFormula(node, @".\Lib\ObjectScripts\TestNode_WithDefaultFormulaByObjects_Sys99Trascurabile_SysAudioGrave.py");

			// ents that require navigation
			node.IsNavigationActive = true;

			Utility.Eval(node);

			// node assertions
			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(Warning.Severity, node.Severity);
		}
		#endregion
	}
}

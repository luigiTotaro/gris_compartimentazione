﻿
using GrisSuite.FormulaEngine.Common.Severities;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
	[TestFixture]
	public class GrisObjectTests
	{
		#region Quando assegno ad un oggetto la peggiore delle severità di dettaglio dei suoi figli, anche se non hanno severità di dettaglio inizializzata ma solo severità, ottengo severità inizializzata correttamente

		[Test]
		public void QuandoSistema_ha3Gruppi_InError_InWarning_InOk_TuttiConSeverityDetailNonInizializzata_AssegnoAlSistemaLaSeverityDetailDelPeggiore_MiAspettoSistemaInError ()
		{
			// initialization
			var system = EntityFactory.GetNodeSystem(false);
			var virtObj1 = EntityFactory.GetVirtualObject(false);
			var virtObj2 = EntityFactory.GetVirtualObject(false);
			var virtObj3 = EntityFactory.GetVirtualObject(false);

			// severities
			virtObj1.Severity = 2;
			virtObj2.Severity = 1;
			virtObj3.Severity = 0;

			// relations
			Utility.SetBaseRelation(system, virtObj1);
			Utility.SetBaseRelation(system, virtObj2);
			Utility.SetBaseRelation(system, virtObj3);

			// act
			system.SeverityDetail = system.Children.GetWorstSeverityDetail();

			// assert
			Assert.AreEqual(Error.Severity, system.Severity);
		}

		#endregion
	}
}

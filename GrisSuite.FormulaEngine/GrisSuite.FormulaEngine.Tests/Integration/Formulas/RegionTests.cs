﻿using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
    [TestFixture]
    public class RegionTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            GrisObject.Cache.Reset();
        }

        #region Test formule default compartimenti

        [Test]
        public void QuandoCompartimento1_ha2Zone_Linea1Ok_Linea2Ok_MiAspettoOk()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var zone2 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            zone2.ZoneId = 151191566;
            node1.NodeId = 528432168973;
            node2.NodeId = 541317070861;
            node3.NodeId = 511252299789;
            server1.ServerId = 168820747;
            server2.ServerId = 120782870;
            server3.ServerId = 87294134;

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = Ok.Severity;
            zone2.Severity = Ok.Severity;
            node1.Severity = Ok.Severity;
            node2.Severity = Ok.Severity;
            node3.Severity = Ok.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;
            server2.SeverityDetail = Ok.SeverityDetail;
            server3.SeverityDetail = Ok.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Zones.Add(zone2);
            zone1.Region = region;
            zone2.Region = region;
            zone1.Nodes.Add(node1);
            zone1.Nodes.Add(node2);
            zone2.Nodes.Add(node3);
            node1.Zone = zone1;
            node2.Zone = zone1;
            node3.Zone = zone2;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(Ok.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento2_ha2Zone_Linea1Warning_Linea2Warning_MiAspettoWarning()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var zone2 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            zone2.ZoneId = 151191566;
            node1.NodeId = 528432168973;
            node2.NodeId = 541317070861;
            node3.NodeId = 511252299789;
            server1.ServerId = 168820747;
            server2.ServerId = 120782870;
            server3.ServerId = 87294134;

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = Warning.Severity;
            zone2.Severity = Warning.Severity;
            node1.Severity = Warning.Severity;
            node2.Severity = Warning.Severity;
            node3.Severity = Warning.Severity;
            server1.SeverityDetail = Warning.SeverityDetail;
            server2.SeverityDetail = Warning.SeverityDetail;
            server3.SeverityDetail = Warning.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Zones.Add(zone2);
            zone1.Region = region;
            zone2.Region = region;
            zone1.Nodes.Add(node1);
            zone1.Nodes.Add(node2);
            zone2.Nodes.Add(node3);
            node1.Zone = zone1;
            node2.Zone = zone1;
            node3.Zone = zone2;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(Warning.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento3_ha2Zone_Linea1Warning_Linea2Error_MiAspettoError()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var zone2 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            zone2.ZoneId = 151191566;
            node1.NodeId = 528432168973;
            node2.NodeId = 541317070861;
            node3.NodeId = 511252299789;
            server1.ServerId = 168820747;
            server2.ServerId = 120782870;
            server3.ServerId = 87294134;

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = Warning.Severity;
            zone2.Severity = Error.Severity;
            node1.Severity = Warning.Severity;
            node2.Severity = Error.Severity;
            node3.Severity = Error.Severity;
            server1.SeverityDetail = Warning.SeverityDetail;
            server2.SeverityDetail = Error.SeverityDetail;
            server3.SeverityDetail = Error.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Zones.Add(zone2);
            zone1.Region = region;
            zone2.Region = region;
            zone1.Nodes.Add(node1);
            zone1.Nodes.Add(node2);
            zone2.Nodes.Add(node3);
            node1.Zone = zone1;
            node2.Zone = zone1;
            node3.Zone = zone2;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(Error.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento4_ha2Zone_Linea1NotActive_Linea2NotActive_MiAspettoNotActive()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var zone2 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            zone2.ZoneId = 151191566;
            node1.NodeId = 528432168973;
            node2.NodeId = 541317070861;
            node3.NodeId = 511252299789;
            server1.ServerId = 168820747;
            server2.ServerId = 120782870;
            server3.ServerId = 87294134;

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = NotActive.Severity;
            zone2.Severity = NotActive.Severity;
            node1.Severity = NotActive.Severity;
            node2.Severity = NotActive.Severity;
            node3.Severity = NotActive.Severity;
            server1.SeverityDetail = NotActive.SeverityDetail;
            server2.SeverityDetail = NotActive.SeverityDetail;
            server3.SeverityDetail = NotActive.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Zones.Add(zone2);
            zone1.Region = region;
            zone2.Region = region;
            zone1.Nodes.Add(node1);
            zone1.Nodes.Add(node2);
            zone2.Nodes.Add(node3);
            node1.Zone = zone1;
            node2.Zone = zone1;
            node3.Zone = zone2;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(NotActive.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento5_nonHaZone_MiAspettoNotAvailable()
        {
            // initialization
            var region = EntityFactory.GetRegion();

            // entity data
            region.RegionId = 1;

            // severities
            region.Severity = Ok.Severity;

            // relations

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(NotAvailable.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento5_ha1Zona_Linea1NotActive_ServerOffline_MiAspettoNotActive()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            node1.NodeId = 528432168973;
            server1.ServerId = 168820747;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = NotActive.Severity;
            node1.Severity = NotActive.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Children.Add(zone1);
            zone1.Region = region;
            zone1.Nodes.Add(node1);
            node1.Zone = zone1;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(NotActive.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento5_ha1ZonaUnknown_Linea1NotActive_ServerOffline_MiAspettoUnknown()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            node1.NodeId = 528432168973;
            server1.ServerId = 168820747;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = Unknown.Severity;
            node1.Severity = NotActive.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Children.Add(zone1);
            zone1.Region = region;
            zone1.Nodes.Add(node1);
            node1.Zone = zone1;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(Unknown.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento_ha2Zone_Zona1Unknown_Zona2Error_MiAspettoError()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var zone2 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            zone2.ZoneId = 151191566;
            node1.NodeId = 528432168973;
            server1.ServerId = 168820747;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = Unknown.Severity;
            zone2.Severity = Error.Severity;
            node1.Severity = NotActive.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Zones.Add(zone2);
            region.Children.Add(zone1);
            region.Children.Add(zone2);
            zone1.Region = region;
            zone2.Region = region;
            zone1.Nodes.Add(node1);
            zone2.Region = region;
            node1.Zone = zone1;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(Error.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento_ha2Zone_Zona1Unknown_Zona2NotActive_MiAspettoUnknown()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var zone2 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            zone2.ZoneId = 151191566;
            node1.NodeId = 528432168973;
            server1.ServerId = 168820747;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = Unknown.Severity;
            zone2.Severity = NotActive.Severity;
            node1.Severity = NotActive.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Zones.Add(zone2);
            region.Children.Add(zone1);
            region.Children.Add(zone2);
            zone1.Region = region;
            zone2.Region = region;
            zone1.Nodes.Add(node1);
            zone2.Region = region;
            node1.Zone = zone1;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(Unknown.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoCompartimento_ha2Zone_Zona1Unknown_Zona2Unknown_MiAspettoUnknown()
        {
            // initialization
            var region = EntityFactory.GetRegion();
            var zone1 = EntityFactory.GetZone();
            var zone2 = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();

            // entity data
            region.RegionId = 1;
            zone1.ZoneId = 151191565;
            zone2.ZoneId = 151191566;
            node1.NodeId = 528432168973;
            server1.ServerId = 168820747;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";

            // severities
            region.Severity = Ok.Severity;
            zone1.Severity = Unknown.Severity;
            zone2.Severity = Unknown.Severity;
            node1.Severity = NotActive.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;

            // relations
            region.Zones.Add(zone1);
            region.Zones.Add(zone2);
            region.Children.Add(zone1);
            region.Children.Add(zone2);
            zone1.Region = region;
            zone2.Region = region;
            zone1.Nodes.Add(node1);
            zone2.Region = region;
            node1.Zone = zone1;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);

            // formulas
            Utility.SetFormula(region, @".\Lib\RegionDefaultStatus.py");

            // act
            Utility.Eval(region);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(region));

            Assert.AreEqual(Unknown.Severity, region.Severity);
            Assert.IsFalse(region.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, region.ForcedSeverityLevel);
        }

        #endregion
    }
}
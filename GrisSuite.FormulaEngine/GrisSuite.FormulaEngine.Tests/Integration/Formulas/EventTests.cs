﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Model.Entities;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
	[TestFixture]
	public class EventTests
	{
		[TestFixtureSetUp]
		public void SetUp ()
		{
			GrisObject.Cache.Reset();
		}

		#region Quando un Pzi ha tutti gli eventi di tipo 'ESITO TEST PERIODICO' con valore OK -> mi aspetto la periferica in OK

		[Test]
		public void QuandoDevicePzi_HaTuttiGliEventi_diTipoEsitoTestPeriodicoInOk_MiAspettoSeverityDetailInOk ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var devPzi = EntityFactory.GetDevice();

			// entity data
			//devPzi.SpecificId = 284537400000519;
			devPzi.Type = "TT10220";

			// formulas
			Utility.SetFormula(devPzi, @".\Lib\ObjectScripts\TestPzi_AllEsitoTestPeriodicoEventsOk.py");

			// methods mocking
			Func<DateTime, DateTime, List<IEvent>> mock = ( dFrom, dTo ) => new List<IEvent>
			                                                              	{
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ESITO TEST PERIODICO",
																						EventDescriptionValue = "Amplificatore 1:OK, Zona 1:OK"
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ESITO TEST PERIODICO",
																						EventDescriptionValue = "Amplificatore 1:OK, Zona 1:OK"
			                                                              			}
			                                                              	};

			formulaEng.Eval(new List<EvaluablePackage> { Utility.PackageIt(devPzi, "GetRaisedEvents", mock) });

			Assert.IsFalse(Utility.ObjectHasErrors(devPzi));

			Assert.AreEqual(Ok.SeverityDetail, devPzi.SeverityDetail);
		}

		#endregion

		#region Quando un Pzi non ha nessun evento di tipo 'ESITO TEST PERIODICO' -> mi aspetto la periferica in 'Non Dispobibile' (-1)

		[Test]
		public void QuandoDevicePzi_NonHaNessunEvento_diTipoEsitoTestPeriodico_MiAspettoSeverityDetailInNotAvailable ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var devPzi = EntityFactory.GetDevice();

			// entity data
			//devPzi.SpecificId = 284537400000519;
			devPzi.Type = "TT10220";

			// formulas
			Utility.SetFormula(devPzi, @".\Lib\ObjectScripts\TestPzi_AllEsitoTestPeriodicoEventsOk.py");

			// methods mocking
			Func<DateTime, DateTime, List<IEvent>> mock = ( dFrom, dTo ) => new List<IEvent>
			                                                              	{
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			}
			                                                              	};

			formulaEng.Eval(new List<EvaluablePackage> { Utility.PackageIt(devPzi, "GetRaisedEvents", mock) });

			Assert.IsFalse(Utility.ObjectHasErrors(devPzi));

			Assert.AreEqual(NotAvailable.SeverityDetail, devPzi.SeverityDetail);
		}

		#endregion

		#region Quando un Pzi ha almeno un evento di tipo 'ESITO TEST PERIODICO' con valore non OK -> mi aspetto la periferica in 'Non Disponibile'

		[Test]
		public void QuandoDevicePzi_HaAlmenoUnEvento_diTipoEsitoTestPeriodicoNonInOk_MiAspettoSeverityDetailInNotAvailable ()
		{
			// initialization
			var formulaEng = new FakeFormulaEngine();

			var devPzi = EntityFactory.GetDevice();

			// entity data
			//devPzi.SpecificId = 284537400000519;
			devPzi.Type = "TT10220";

			// formulas
			Utility.SetFormula(devPzi, @".\Lib\ObjectScripts\TestPzi_AllEsitoTestPeriodicoEventsOk.py");

			// methods mocking
			Func<DateTime, DateTime, List<IEvent>> mock = ( dFrom, dTo ) => new List<IEvent>
			                                                              	{
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ESITO TEST PERIODICO",
																						EventDescriptionValue = "Amplificatore 1:OK, Zona 1:OK, Amplificatore 2:OK, Zona 2:OK"
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "FINE ATTIVAZIONE SONORA",
																						EventDescriptionValue = null
			                                                              			},
			                                                              		new Event
			                                                              			{
			                                                              				EventDescription = "ESITO TEST PERIODICO",
																						EventDescriptionValue = "Amplificatore 1:OK, Zona 1:KO, Amplificatore 2:OK, Zona 2:OK"
			                                                              			}
			                                                              	};

			formulaEng.Eval(new List<EvaluablePackage> { Utility.PackageIt(devPzi, "GetRaisedEvents", mock) });

			Assert.IsFalse(Utility.ObjectHasErrors(devPzi));

			Assert.AreEqual(NotAvailable.SeverityDetail, devPzi.SeverityDetail);
		}

		#endregion
	}
}

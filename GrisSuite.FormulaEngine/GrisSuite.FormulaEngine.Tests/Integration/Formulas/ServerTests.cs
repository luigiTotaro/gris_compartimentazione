﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
    [TestFixture]
    public class ServerTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            GrisObject.Cache.Reset();
        }

        #region Storico, Dato archiviato

        [Test]
        public void QuandoServer_SenzaDeviceECancellato_MiAspettoServerStoricoDatoArchiviato()
        {
			// initialization
			var server = EntityFactory.GetServer();

            // entity data
            server.ServerId = 87162882;
            server.Name = "Server STLC1000";
            server.Host = "STLC0C8AF9";
            server.FullHostName = "STLC0C8AF9";
            server.IpAddress = "10.102.143.73";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "IsAlive";
            server.NodeId = new Guid("91EB6EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B0C8AF9";
            server.IsDeleted = true;

            // formulas
			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            // act
            Utility.Eval(server);

            // assert
			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.NotClassified, server.Severity);
            Assert.AreEqual(Old.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Old.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Dati del collettore incompleti

        [Test]
        public void QuandoServer_SenzaDeviceENonCancellato_MiAspettoServerDatiCollettoreIncompleti()
        {
			// initialization
			var server = EntityFactory.GetServer();

            // entity data
            server.ServerId = 87162882;
            server.Name = "Server STLC1000";
            server.Host = "STLC0C8AF9";
            server.FullHostName = "STLC0C8AF9";
            server.IpAddress = "10.102.143.73";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "IsAlive";
            server.NodeId = new Guid("91EB6EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B0C8AF9";
            server.IsDeleted = false;

            // formulas
			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.Error, server.Severity);
            Assert.AreEqual(IncompleteServerData.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(IncompleteServerData.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Non raggiungibile (Offline)

        [Test]
        public void QuandoServer_UltimoAggiornamentoOltreUnOraFa_ESenzaMAC_MiAspettoServerNonRaggiungibile()
        {
			// initialization
			var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 120783119;
            server.Name = "Server STLC1000 Candia Canavese";
            server.Host = "TELEFIN-6E8130X";
            server.FullHostName = "TELEFIN-6E8130X";
            server.IpAddress = "10.102.200.119";
            server.LastUpdate = DateTime.Now.AddMinutes(-70);
            server.LastMessageType = null;
            server.NodeId = new Guid("85E86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = null;
            server.IsDeleted = false;

            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.Offline, server.Severity);
            Assert.AreEqual(Offline.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Offline.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Raggiungibile (ultimo aggiornamento 59 minuti fa)

        [Test]
        public void QuandoServer_UltimoAggiornamento59MinutiFa_EConMAC_e_IP_Non_Duplicato_MiAspettoServerOk()
        {
            // initialization
            var formulaEng = new FakeFormulaEngine();

            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 120783119;
            server.Name = "Server STLC1000 Candia Canavese";
            server.Host = "TELEFIN-6E8130X";
            server.FullHostName = "TELEFIN-6E8130X";
            server.IpAddress = "10.102.200.119";
            server.LastUpdate = DateTime.Now.AddMinutes(-59);
            server.LastMessageType = null;
            server.NodeId = new Guid("85E86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B0C8AF9";
            server.IsDeleted = false;

            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            // methods mocking
            Func<bool> mock = () => false;

            formulaEng.Eval(new List<EvaluablePackage> {Utility.PackageIt(server, "HasDuplicateIpAddress", mock)});

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.Ok, server.Severity);
            Assert.AreEqual(Ok.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Un STLC per essere considerato valido deve avere un ip

        [Test]
        public void QuandoServer_SenzaIP_E_AlmenoUnDevice_MiAspettoServerSenzaIP()
        {
			// initialization
			var dev1 = EntityFactory.GetDevice();
            var server = EntityFactory.GetServer();

            // entity data
            server.ServerId = 87162882;
            server.Name = "Server STLC1000";
            server.Host = "STLC0C8AF9";
            server.FullHostName = "STLC0C8AF9";
            server.IpAddress = null;
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "IsAlive";
            server.NodeId = new Guid("91EB6EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B0C8AF9";
            server.IsDeleted = false;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.NotActive, server.Severity);
            Assert.AreEqual(NoIPAddress.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(NoIPAddress.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Diagnostica disattivata dall'operatore (Server side)

        [Test]
        public void QuandoServer_InManutenzione_MiAspettoDiagnosticaDisattivataDallOperatore_E_RealeOK()
        {
			// initialization
			var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 103219278;
            server.Name = "Server STLC1000";
            server.Host = "STLC072833";
            server.FullHostName = "STLC072833";
            server.IpAddress = "10.102.151.21";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "UpdateStatus";
            server.NodeId = new Guid("DBE76EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B072833";
            server.IsDeleted = false;
            server.InMaintenance = true;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.NotActive, server.Severity);
            Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Un STLC per essere considerato valido deve avere un MAC

        [Test]
        public void QuandoServer_SenzaMAC_MiAspettoServerSenzaMAC()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 117899289;
            server.Name = "Server STLC1000";
            server.Host = "STLC0283";
            server.FullHostName = "STLC0283.rfiservizi.corp";
            server.IpAddress = "10.102.180.6";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "UpdateStatus";
            server.NodeId = new Guid("DCE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = null;
            server.IsDeleted = false;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            // act
            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.NotActive, server.Severity);
            Assert.AreEqual(NoMACAddress.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(NoMACAddress.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Un STLC per essere considerato valido deve avere un FullHostName

        [Test]
        public void QuandoServer_SenzaFullHostName_MiAspettoServerSenzaFullHostName()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 117899289;
            server.Name = "Server STLC1000";
            server.Host = "STLC0283";
            server.FullHostName = null;
            server.IpAddress = "10.102.180.6";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "UpdateStatus";
            server.NodeId = new Guid("DCE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = null;
            server.IsDeleted = false;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

            // formulas
			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            // act
            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.NotActive, server.Severity);
            Assert.AreEqual(NoHostName.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(NoHostName.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Ip duplicato

        [Test]
        public void QuandoServer_ConAltroServerUgualeIP_MiAspettoServerIPDuplicato_E_RealeOK()
        {
            // initialization
            var formulaEng = new FakeFormulaEngine();

            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 117899289;
            server.Name = "Server STLC1000";
            server.Host = "STLC0283";
            server.FullHostName = "STLC0283";
            server.IpAddress = "10.102.180.6";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "UpdateStatus";
            server.NodeId = new Guid("DCE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B0C8AF9";
            server.IsDeleted = false;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

            // formulas
			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            // methods mocking
            Func<bool> mock = () => true;

            formulaEng.Eval(new List<EvaluablePackage> {Utility.PackageIt(server, "HasDuplicateIpAddress", mock)});

			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.NotActive, server.Severity);
            Assert.AreEqual(DuplicateIPAddress.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Server Ok

        [Test]
        public void QuandoServer_Normale_MiAspettoServerOk()
        {
            // initialization
            var formulaEng = new FakeFormulaEngine();

            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 117899290;
            server.Name = "Server STLC1000";
            server.Host = "STLC1520B9";
            server.FullHostName = "STLC1520B9";
            server.IpAddress = "10.182.71.18";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "ReplaceStatus";
            server.NodeId = new Guid("BAE86EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B1520B9";
            server.IsDeleted = false;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

            // formulas
			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            // methods mocking
            Func<bool> mock = () => false;

            // act
            formulaEng.Eval(new List<EvaluablePackage> {Utility.PackageIt(server, "HasDuplicateIpAddress", mock)});

            // assert
			Assert.IsFalse(Utility.ObjectHasErrors(server));

			Assert.AreEqual(Severity.Ok, server.Severity);
            Assert.AreEqual(Ok.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Server forzato dall utente in maintenance

        [Test]
        public void QuandoServer_ForzatoDallUtente_InMaintenance_MiAspettoDiagnosticaDisattivataDallOperatore_E_Reale_Ok()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 103219278;
            server.Name = "Server STLC1000";
            server.Host = "STLC072833";
            server.FullHostName = "STLC072833";
            server.IpAddress = "10.102.151.21";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "UpdateStatus";
            server.NodeId = new Guid("DBE76EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B072833";
            server.IsDeleted = false;
            server.InMaintenance = true;
            server.ForcedByUser = true;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));
			
			Assert.AreEqual(Severity.NotActive, server.Severity);
            Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Ok.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Server forzato dall utente in maintenance, non raggiungibile (offline)

        [Test]
        public void QuandoServer_ForzatoDallUtente_Offline_InMaintenance_MiAspettoDiagnosticaDisattivataDallOperatore_E_Reale_Offline()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 103219278;
            server.Name = "Server STLC1000";
            server.Host = "STLC072833";
            server.FullHostName = "STLC072833";
            server.IpAddress = "10.102.151.21";
            server.LastUpdate = DateTime.Now.AddMinutes(-200); // oltre l'ora
            server.LastMessageType = "UpdateStatus";
            server.NodeId = new Guid("DBE76EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B072833";
            server.IsDeleted = false;
            server.InMaintenance = true;
            server.ForcedByUser = true;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

            Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            Utility.Eval(server);

            Assert.IsFalse(Utility.ObjectHasErrors(server));

            Assert.AreEqual(Severity.NotActive, server.Severity);
            Assert.AreEqual(DiagnosticDisabledByOperator.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(Offline.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion

        #region Server forzato dall utente non in maintenance

        [Test]
        public void QuandoServer_ForzatoDallUtente_NonInMaintenance_MiAspettoSconosciuto_E_Reale_NonDisponibile()
        {
            // initialization
            var server = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();

            // entity data
            server.ServerId = 103219278;
            server.Name = "Server STLC1000";
            server.Host = "STLC072833";
            server.FullHostName = "STLC072833";
            server.IpAddress = "10.102.151.21";
            server.LastUpdate = DateTime.Now.AddMinutes(-20);
            server.LastMessageType = "UpdateStatus";
            server.NodeId = new Guid("DBE76EFE-E5D8-DE11-93A6-001E0B6CA3E0");
            server.ServerVersion = null;
            server.MacAddress = "00E04B072833";
            server.IsDeleted = false;
            server.InMaintenance = false;
            server.ForcedByUser = true;

            // relations
            dev1.ServerId = server.Id;
            server.Devices.Add(dev1);

			Utility.SetFormula(server, @".\Lib\ServerDefaultStatus.py");

            Utility.Eval(server);

			Assert.IsFalse(Utility.ObjectHasErrors(server));
			
			Assert.AreEqual(Severity.Unknown, server.Severity);
            Assert.AreEqual(Unknown.SeverityDetail, server.SeverityDetail);
            Assert.AreEqual(NotAvailable.SeverityDetail, server.RealSeverityDetail);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, server.ForcedSeverityLevel);
        }

        #endregion
    }
}
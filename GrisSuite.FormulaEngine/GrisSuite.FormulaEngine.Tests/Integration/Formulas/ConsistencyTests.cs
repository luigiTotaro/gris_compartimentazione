﻿
using System;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
	[TestFixture]
	public class ConsistencyTests
	{
		[TestFixtureSetUp]
		public void SetUp ()
		{
			GrisObject.Cache.Reset();
		}

		[Test]
		public void QuandoPriferica_NonAttivaPercheServerNonAttivo_QuandoFormulaCustomImpostaServerOffline_MiAspettoPerifericaOffline ()
		{
			// situazione non realistica, ipotizzata solo per riprodurre il ricalcolo oggetti dipendenti in cascata

			// initialization
			var sys = EntityFactory.GetNodeSystem();
			var dev1 = EntityFactory.GetDevice();
			var srv = EntityFactory.GetServer(false);
			var virtualObj = EntityFactory.GetVirtualObject();
			var dev2 = EntityFactory.GetDevice(false);

			// entity data
			srv.SeverityDetail = NotActive.SeverityDetail;
			dev2.IsOffline = Convert.ToByte(true);
			dev2.SeverityDetail = Offline.SeverityDetail;
            dev1.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule
            dev2.Type = "XXIP000"; // Device fittizia, ma tipo obbligatorio su DB e formule

			// relations
			dev1.System = sys;
			dev1.Server = srv;
			virtualObj.Parent = srv;
			virtualObj.Children.Add(dev2);
			dev2.System = sys;


			// caching
			Utility.CacheEntities(sys, dev1, srv, virtualObj, dev2);

			// formulas
			Utility.SetFormula(dev1, @".\Lib\DeviceDefaultStatus.py");
			Utility.SetFormula(srv, @".\Lib\ServerDefaultStatus.py");
			Utility.SetFormula(virtualObj, @".\Lib\ObjectScripts\TestVirtualObject_CriticalGroup_ForDependencies.py", EvaluationFormulaType.Custom);

			// ents that require navigation
			dev1.IsNavigationActive = true;
			srv.IsNavigationActive = true;
			virtualObj.IsNavigationActive = true;

			// il server deve risultare calcolato, forzo lo stato desiderato perchè non impostabile via formula
			Utility.Eval(srv);
			srv.ForceSeverity(NotActive.SeverityDetail);

			Utility.Eval(dev1);
			Assert.IsFalse(Utility.ObjectHasErrors(dev1));

			Assert.AreEqual(Severity.NotActive, srv.Severity);
			Assert.AreEqual(ServerNotActive.SeverityDetail, dev1.SeverityDetail);

			// fase 2 //
			Utility.Eval(virtualObj);

			Assert.IsFalse(Utility.ObjectHasErrors(virtualObj));

			Assert.AreEqual(Severity.Offline, srv.Severity);
			Assert.AreEqual(ServerOffline.SeverityDetail, dev1.SeverityDetail);
			Assert.AreEqual(Offline.SeverityDetail, virtualObj.SeverityDetail);
			Assert.AreEqual(Offline.SeverityDetail, dev2.SeverityDetail);
		}

		[Test]
		public void QuandoStazione_AccedeASistemaComeSottoOggetto_EAssegnaStatoErrore_MiAspettoStazioneInErrore ()
		{
			// initialization
			var system = EntityFactory.GetNodeSystem(false);
			var node = EntityFactory.GetNode();

			// entity data
			system.Name = "S0";
			system.SeverityDetail = Error.SeverityDetail;

			// relations
			node.Systems.AddEntity(system);

			// formulas
			Utility.SetFormula(node, @".\Lib\ObjectScripts\TestNode_ImplicitGetObjectByName.py", EvaluationFormulaType.Custom);

			// ents that require navigation
			node.IsNavigationActive = true;
			// la navigazione va attivata anche quando nella formula si usa 
			// una risoluzione di oggetto per nome (es: .PZ1), questo per rendere possibile l'accesso alla cache

			// act
			Utility.Eval(node);

			Assert.IsFalse(Utility.ObjectHasErrors(node));

			Assert.AreEqual(Severity.Error, node.Severity);
		}
	}
}

﻿using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model;
using NUnit.Framework;

namespace GrisSuite.FormulaEngine.Tests.Integration.Formulas
{
    [TestFixture]
    public class ZoneTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            GrisObject.Cache.Reset();
        }

        #region Test formule default linee

        [Test]
        public void
            QuandoLinea151191565_ha12Nodi_Stazione1Ok_Stazione2Ok_Stazione3Ok_Stazione4Ok_Stazione5Error_Stazione6Ok_Stazione7Ok_Stazione8Ok_Stazione9Ok_Stazione10Ok_Stazione11Ok_Stazione12Ok_MiAspettoOk
            ()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var node4 = EntityFactory.GetNode();
            var node5 = EntityFactory.GetNode();
            var node6 = EntityFactory.GetNode();
            var node7 = EntityFactory.GetNode();
            var node8 = EntityFactory.GetNode();
            var node9 = EntityFactory.GetNode();
            var node10 = EntityFactory.GetNode();
            var node11 = EntityFactory.GetNode();
            var node12 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();
            var server4 = EntityFactory.GetServer();
            var server5 = EntityFactory.GetServer();
            var server6 = EntityFactory.GetServer();
            var server7 = EntityFactory.GetServer();
            var server8 = EntityFactory.GetServer();
            var server9 = EntityFactory.GetServer();
            var server10 = EntityFactory.GetServer();
            var server11 = EntityFactory.GetServer();
            var server12 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 151191565;
            node1.NodeId = 528432168973;
            node2.NodeId = 541317070861;
            node3.NodeId = 511252299789;
            node4.NodeId = 524137201677;
            node5.NodeId = 519842234381;
            node6.NodeId = 786130206733;
            node7.NodeId = 781835239437;
            node8.NodeId = 670166089741;
            node9.NodeId = 257849229325;
            node10.NodeId = 55985766413;
            node11.NodeId = 455417724941;
            node12.NodeId = 287914000397;
            server1.ServerId = 168820747;
            server2.ServerId = 120782870;
            server3.ServerId = 87294134;
            server4.ServerId = 134938635;
            server5.ServerId = 120783065;
            server6.ServerId = 103219231;
            server7.ServerId = 134938627;
            server8.ServerId = 134938630;
            server9.ServerId = 118358033;
            server10.ServerId = 151846919;
            server11.ServerId = 87228439;
            server12.ServerId = 120783063;

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = Ok.Severity;
            node2.Severity = Ok.Severity;
            node3.Severity = Ok.Severity;
            node4.Severity = Ok.Severity;
            node5.Severity = Error.Severity;
            node6.Severity = Ok.Severity;
            node7.Severity = Ok.Severity;
            node8.Severity = Ok.Severity;
            node9.Severity = Ok.Severity;
            node10.Severity = Ok.Severity;
            node11.Severity = Ok.Severity;
            node12.Severity = Ok.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;
            server2.SeverityDetail = Ok.SeverityDetail;
            server3.SeverityDetail = Ok.SeverityDetail;
            server4.SeverityDetail = Ok.SeverityDetail;
            server5.SeverityDetail = Ok.SeverityDetail;
            server6.SeverityDetail = Ok.SeverityDetail;
            server7.SeverityDetail = Ok.SeverityDetail;
            server8.SeverityDetail = Ok.SeverityDetail;
            server9.SeverityDetail = Ok.SeverityDetail;
            server10.SeverityDetail = Ok.SeverityDetail;
            server11.SeverityDetail = Ok.SeverityDetail;
            server12.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Nodes.Add(node2);
            zone.Nodes.Add(node3);
            zone.Nodes.Add(node4);
            zone.Nodes.Add(node5);
            zone.Nodes.Add(node6);
            zone.Nodes.Add(node7);
            zone.Nodes.Add(node8);
            zone.Nodes.Add(node9);
            zone.Nodes.Add(node10);
            zone.Nodes.Add(node11);
            zone.Nodes.Add(node12);
            node1.Zone = zone;
            node2.Zone = zone;
            node3.Zone = zone;
            node4.Zone = zone;
            node5.Zone = zone;
            node6.Zone = zone;
            node7.Zone = zone;
            node8.Zone = zone;
            node9.Zone = zone;
            node10.Zone = zone;
            node11.Zone = zone;
            node12.Zone = zone;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            node4.Servers.Add(server4);
            node5.Servers.Add(server5);
            node6.Servers.Add(server6);
            node7.Servers.Add(server7);
            node8.Servers.Add(server8);
            node9.Servers.Add(server9);
            node10.Servers.Add(server10);
            node11.Servers.Add(server11);
            node12.Servers.Add(server12);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;
            server4.Node = node4;
            server5.Node = node5;
            server6.Node = node6;
            server7.Node = node7;
            server8.Node = node8;
            server9.Node = node9;
            server10.Node = node10;
            server11.Node = node11;
            server12.Node = node12;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Ok.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea92012548_ha3Nodi_Stazione1Ok_Stazione2Ok_Stazione3Ok_MiAspettoOk()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 92012548;
            node1.NodeId = 12545691484164;
            node2.NodeId = 9728192937988;
            node3.NodeId = 8615796408324;
            server1.ServerId = 134807587;
            server2.ServerId = 118358025;
            server3.ServerId = 135004183;

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = Ok.Severity;
            node2.Severity = Ok.Severity;
            node3.Severity = Ok.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;
            server2.SeverityDetail = Ok.SeverityDetail;
            server3.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Nodes.Add(node2);
            zone.Nodes.Add(node3);
            node1.Zone = zone;
            node2.Zone = zone;
            node3.Zone = zone;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Ok.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoLinea151126029_ha7Nodi_Stazione1Warning_Stazione2Warning_Stazione3Warning_Stazione4Warning_Stazione5Warning_Stazione6Warning_Stazione7Warning_MiAspettoWarning
            ()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var node4 = EntityFactory.GetNode();
            var node5 = EntityFactory.GetNode();
            var node6 = EntityFactory.GetNode();
            var node7 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();
            var server4 = EntityFactory.GetServer();
            var server5 = EntityFactory.GetServer();
            var server6 = EntityFactory.GetServer();
            var server7 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 151126029;
            node1.NodeId = 167654850573;
            node2.NodeId = 678755958797;
            node3.NodeId = 356633411597;
            node4.NodeId = 262144131085;
            node5.NodeId = 326568640525;
            node6.NodeId = 330863607821;
            node7.NodeId = 322273673229;
            server1.ServerId = 151846920;
            server2.ServerId = 87228456;
            server3.ServerId = 151846941;
            server4.ServerId = 118358043;
            server5.ServerId = 118358030;
            server6.ServerId = 169672705;
            server7.ServerId = 101318686;

            // severities
            zone.Severity = Warning.Severity;
            node1.Severity = Warning.Severity;
            node2.Severity = Warning.Severity;
            node3.Severity = Warning.Severity;
            node4.Severity = Warning.Severity;
            node5.Severity = Warning.Severity;
            node6.Severity = Warning.Severity;
            node7.Severity = Warning.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;
            server2.SeverityDetail = Ok.SeverityDetail;
            server3.SeverityDetail = Ok.SeverityDetail;
            server4.SeverityDetail = Ok.SeverityDetail;
            server5.SeverityDetail = Ok.SeverityDetail;
            server6.SeverityDetail = Ok.SeverityDetail;
            server7.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Nodes.Add(node2);
            zone.Nodes.Add(node3);
            zone.Nodes.Add(node4);
            zone.Nodes.Add(node5);
            zone.Nodes.Add(node6);
            zone.Nodes.Add(node7);
            node1.Zone = zone;
            node2.Zone = zone;
            node3.Zone = zone;
            node4.Zone = zone;
            node5.Zone = zone;
            node6.Zone = zone;
            node7.Zone = zone;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            node4.Servers.Add(server4);
            node5.Servers.Add(server5);
            node6.Servers.Add(server6);
            node7.Servers.Add(server7);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;
            server4.Node = node4;
            server5.Node = node5;
            server6.Node = node6;
            server7.Node = node7;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Warning.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoLinea151257101_ha10Nodi_Stazione1Error_Stazione2Warning_Stazione3Ok_Stazione4Ok_Stazione5Warning_Stazione6Warning_Stazione7Error_Stazione8Ok_Stazione9Ok_Stazione10Warning_MiAspettoWarning
            ()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var node4 = EntityFactory.GetNode();
            var node5 = EntityFactory.GetNode();
            var node6 = EntityFactory.GetNode();
            var node7 = EntityFactory.GetNode();
            var node8 = EntityFactory.GetNode();
            var node9 = EntityFactory.GetNode();
            var node10 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();
            var server4 = EntityFactory.GetServer();
            var server5 = EntityFactory.GetServer();
            var server6 = EntityFactory.GetServer();
            var server7 = EntityFactory.GetServer();
            var server8 = EntityFactory.GetServer();
            var server9 = EntityFactory.GetServer();
            var server10 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 151257101;
            node1.NodeId = 610036613133;
            node2.NodeId = 743180599309;
            node3.NodeId = 270734196749;
            node4.NodeId = 339453673485;
            node5.NodeId = 64575766541;
            node6.NodeId = 34510995469;
            node7.NodeId = 116115374093;
            node8.NodeId = 421058052109;
            node9.NodeId = 253554327565;
            node10.NodeId = 296504000525;
            server1.ServerId = 87293993;
            server2.ServerId = 135004184;
            server3.ServerId = 151846952;
            server4.ServerId = 168820737;
            server5.ServerId = 103219269;
            server6.ServerId = 136642571;
            server7.ServerId = 151846938;
            server8.ServerId = 120783023;
            server9.ServerId = 135004211;
            server10.ServerId = 101318730;

            // severities
            zone.Severity = Warning.Severity;
            node1.Severity = Error.Severity;
            node2.Severity = Warning.Severity;
            node3.Severity = Ok.Severity;
            node4.Severity = Ok.Severity;
            node5.Severity = Warning.Severity;
            node6.Severity = Warning.Severity;
            node7.Severity = Error.Severity;
            node8.Severity = Ok.Severity;
            node9.Severity = Ok.Severity;
            node10.Severity = Warning.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;
            server2.SeverityDetail = Ok.SeverityDetail;
            server3.SeverityDetail = Ok.SeverityDetail;
            server4.SeverityDetail = Ok.SeverityDetail;
            server5.SeverityDetail = Ok.SeverityDetail;
            server6.SeverityDetail = Ok.SeverityDetail;
            server7.SeverityDetail = Ok.SeverityDetail;
            server8.SeverityDetail = Ok.SeverityDetail;
            server9.SeverityDetail = Ok.SeverityDetail;
            server10.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Nodes.Add(node2);
            zone.Nodes.Add(node3);
            zone.Nodes.Add(node4);
            zone.Nodes.Add(node5);
            zone.Nodes.Add(node6);
            zone.Nodes.Add(node7);
            zone.Nodes.Add(node8);
            zone.Nodes.Add(node9);
            zone.Nodes.Add(node10);
            node1.Zone = zone;
            node2.Zone = zone;
            node3.Zone = zone;
            node4.Zone = zone;
            node5.Zone = zone;
            node6.Zone = zone;
            node7.Zone = zone;
            node8.Zone = zone;
            node9.Zone = zone;
            node10.Zone = zone;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            node4.Servers.Add(server4);
            node5.Servers.Add(server5);
            node6.Servers.Add(server6);
            node7.Servers.Add(server7);
            node8.Servers.Add(server8);
            node9.Servers.Add(server9);
            node10.Servers.Add(server10);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;
            server4.Node = node4;
            server5.Node = node5;
            server6.Node = node6;
            server7.Node = node7;
            server8.Node = node8;
            server9.Node = node9;
            server10.Node = node10;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Warning.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea137887755_ha2Nodi_Stazione1Error_Stazione2Error_MiAspettoError()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 137887755;
            node1.NodeId = 8207820390411;
            node2.NodeId = 8688856727563;
            server1.ServerId = 169672709;
            server2.ServerId = 118161423;

            // severities
            zone.Severity = Error.Severity;
            node1.Severity = Error.Severity;
            node2.Severity = Error.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;
            server2.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Nodes.Add(node2);
            node1.Zone = zone;
            node2.Zone = zone;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            server1.Node = node1;
            server2.Node = node2;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Error.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void
            QuandoLinea137691147_ha10Nodi_Stazione1Error_Stazione2Error_Stazione3Warning_Stazione4Warning_Stazione5Error_Stazione6Error_Stazione7Error_Stazione8Error_Stazione9Warning_Stazione10Ok_MiAspettoError
            ()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var node3 = EntityFactory.GetNode();
            var node4 = EntityFactory.GetNode();
            var node5 = EntityFactory.GetNode();
            var node6 = EntityFactory.GetNode();
            var node7 = EntityFactory.GetNode();
            var node8 = EntityFactory.GetNode();
            var node9 = EntityFactory.GetNode();
            var node10 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var server2 = EntityFactory.GetServer();
            var server3 = EntityFactory.GetServer();
            var server4 = EntityFactory.GetServer();
            var server5 = EntityFactory.GetServer();
            var server6 = EntityFactory.GetServer();
            var server7 = EntityFactory.GetServer();
            var server8 = EntityFactory.GetServer();
            var server9 = EntityFactory.GetServer();
            var server10 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 137691147;
            node1.NodeId = 4076061655051;
            node2.NodeId = 5897127788555;
            node3.NodeId = 10574347173899;
            node4.NodeId = 10887879786507;
            node5.NodeId = 1709534674955;
            node6.NodeId = 13194277224459;
            node7.NodeId = 3646564925451;
            node8.NodeId = 5532055568395;
            node9.NodeId = 11639499063307;
            node10.NodeId = 7456200917003;
            server1.ServerId = 103219281;
            server2.ServerId = 120783042;
            server3.ServerId = 103219230;
            server4.ServerId = 103219252;
            server5.ServerId = 87228424;
            server6.ServerId = 103219289;
            server7.ServerId = 87228455;
            server8.ServerId = 151519241;
            server9.ServerId = 103219250;
            server10.ServerId = 103219266;

            // severities
            zone.Severity = Error.Severity;
            node1.Severity = Error.Severity;
            node2.Severity = Error.Severity;
            node3.Severity = Warning.Severity;
            node4.Severity = Warning.Severity;
            node5.Severity = Error.Severity;
            node6.Severity = Error.Severity;
            node7.Severity = Error.Severity;
            node8.Severity = Error.Severity;
            node9.Severity = Warning.Severity;
            node10.Severity = Ok.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;
            server2.SeverityDetail = Ok.SeverityDetail;
            server3.SeverityDetail = Ok.SeverityDetail;
            server4.SeverityDetail = Ok.SeverityDetail;
            server5.SeverityDetail = Ok.SeverityDetail;
            server6.SeverityDetail = Ok.SeverityDetail;
            server7.SeverityDetail = Ok.SeverityDetail;
            server8.SeverityDetail = Ok.SeverityDetail;
            server9.SeverityDetail = Ok.SeverityDetail;
            server10.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Nodes.Add(node2);
            zone.Nodes.Add(node3);
            zone.Nodes.Add(node4);
            zone.Nodes.Add(node5);
            zone.Nodes.Add(node6);
            zone.Nodes.Add(node7);
            zone.Nodes.Add(node8);
            zone.Nodes.Add(node9);
            zone.Nodes.Add(node10);
            node1.Zone = zone;
            node2.Zone = zone;
            node3.Zone = zone;
            node4.Zone = zone;
            node5.Zone = zone;
            node6.Zone = zone;
            node7.Zone = zone;
            node8.Zone = zone;
            node9.Zone = zone;
            node10.Zone = zone;
            node1.Servers.Add(server1);
            node2.Servers.Add(server2);
            node3.Servers.Add(server3);
            node4.Servers.Add(server4);
            node5.Servers.Add(server5);
            node6.Servers.Add(server6);
            node7.Servers.Add(server7);
            node8.Servers.Add(server8);
            node9.Servers.Add(server9);
            node10.Servers.Add(server10);
            server1.Node = node1;
            server2.Node = node2;
            server3.Node = node3;
            server4.Node = node4;
            server5.Node = node5;
            server6.Node = node6;
            server7.Node = node7;
            server8.Node = node8;
            server9.Node = node9;
            server10.Node = node10;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Error.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea131727370_ha1Nodo_Stazione1Error_ServerOffline_MiAspettoError()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            zone.ZoneId = 131727370;
            node1.NodeId = 43014229196810;
            server1.ServerId = 152109083;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";
            dev3.Type = "PEAVDOM3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = Error.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Children.Add(node1);
            node1.Zone = zone;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev3.Server = server1;
            dev4.Server = server1;
            dev5.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            dev3.Node = node1;
            dev4.Node = node1;
            dev5.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);
            node1.Devices.Add(dev3);
            node1.Devices.Add(dev4);
            node1.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Error.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea131727370_ha1Nodo_Stazione1Unknown_ServerOffline_MiAspettoUnknown()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            zone.ZoneId = 131727370;
            node1.NodeId = 43014229196810;
            server1.ServerId = 152109083;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";
            dev3.Type = "PEAVDOM3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = Unknown.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Children.Add(node1);
            node1.Zone = zone;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev3.Server = server1;
            dev4.Server = server1;
            dev5.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            dev3.Node = node1;
            dev4.Node = node1;
            dev5.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);
            node1.Devices.Add(dev3);
            node1.Devices.Add(dev4);
            node1.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Unknown.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea131727370_ha2Nodi_Stazione1Unknown_ServerOffline_Stazione2NonAttiva_MiAspettoUnknown()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            zone.ZoneId = 131727370;
            node1.NodeId = 43014229196810;
            node2.NodeId = 43014229196811;
            server1.ServerId = 152109083;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";
            dev3.Type = "PEAVDOM3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = Unknown.Severity;
            node2.Severity = NotActive.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Children.Add(node1);
            zone.Children.Add(node2);
            node1.Zone = zone;
            node2.Zone = zone;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev3.Server = server1;
            dev4.Server = server1;
            dev5.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            dev3.Node = node1;
            dev4.Node = node1;
            dev5.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);
            node1.Devices.Add(dev3);
            node1.Devices.Add(dev4);
            node1.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Unknown.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea131727370_ha2Nodi_Stazione1Error_ServerOffline_Stazione2Unknown_MiAspettoError()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var node2 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();
            var dev1 = EntityFactory.GetDevice();
            var dev2 = EntityFactory.GetDevice();
            var dev3 = EntityFactory.GetDevice();
            var dev4 = EntityFactory.GetDevice();
            var dev5 = EntityFactory.GetDevice();

            // entity data
            zone.ZoneId = 131727370;
            node1.NodeId = 43014229196810;
            node2.NodeId = 43014229196811;
            server1.ServerId = 152109083;
            dev1.Type = "PEAVDOM1";
            dev2.Type = "PEAVDOM2";
            dev3.Type = "PEAVDOM3";
            dev4.Type = "INFSTAZ1";
            dev5.Type = "INFSTAZ1";

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = Error.Severity;
            node2.Severity = Unknown.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;
            dev1.SeverityDetail = Error.SeverityDetail;
            dev2.SeverityDetail = Ok.SeverityDetail;
            dev3.SeverityDetail = Ok.SeverityDetail;
            dev4.SeverityDetail = Ok.SeverityDetail;
            dev5.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            zone.Children.Add(node1);
            zone.Children.Add(node2);
            node1.Zone = zone;
            node2.Zone = zone;
            node1.Servers.Add(server1);
            server1.Node = node1;
            dev1.Server = server1;
            dev2.Server = server1;
            dev3.Server = server1;
            dev4.Server = server1;
            dev5.Server = server1;
            dev1.Node = node1;
            dev2.Node = node1;
            dev3.Node = node1;
            dev4.Node = node1;
            dev5.Node = node1;
            node1.Devices.Add(dev1);
            node1.Devices.Add(dev2);
            node1.Devices.Add(dev3);
            node1.Devices.Add(dev4);
            node1.Devices.Add(dev5);

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Error.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea131727370_nonHaNodi_MiAspettoNotAvailable()
        {
            // initialization
            var zone = EntityFactory.GetZone();

            // entity data
            zone.ZoneId = 131727370;

            // severities
            zone.Severity = Ok.Severity;

            // relations

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(NotAvailable.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea131727370_ha1Nodo_Stazione1InMaintenance_MiAspettoNotActive()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 131727370;
            node1.NodeId = 43014229196810;
            server1.ServerId = 152109083;
            node1.InMaintenance = true;
            server1.InMaintenance = true;

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = NotActive.Severity;
            server1.SeverityDetail = Offline.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            node1.Zone = zone;
            node1.Servers.Add(server1);
            server1.Node = node1;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(NotActive.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        [Test]
        public void QuandoLinea131727370_ha1Nodo_Stazione1Ok_MiAspettoOk()
        {
            // initialization
            var zone = EntityFactory.GetZone();
            var node1 = EntityFactory.GetNode();
            var server1 = EntityFactory.GetServer();

            // entity data
            zone.ZoneId = 131727370;
            node1.NodeId = 43014229196810;
            server1.ServerId = 152109083;

            // severities
            zone.Severity = Ok.Severity;
            node1.Severity = Ok.Severity;
            server1.SeverityDetail = Ok.SeverityDetail;

            // relations
            zone.Nodes.Add(node1);
            node1.Zone = zone;
            node1.Servers.Add(server1);
            server1.Node = node1;

            // formulas
            Utility.SetFormula(zone, @".\Lib\ZoneDefaultStatus.py");

            // act
            Utility.Eval(zone);

            // assert
            Assert.IsFalse(Utility.ObjectHasErrors(zone));

            Assert.AreEqual(Ok.Severity, zone.Severity);
            Assert.IsFalse(zone.ExcludedFromParentStatus);
            Assert.AreEqual(ForcedSeverityLevel.NotForced, zone.ForcedSeverityLevel);
        }

        #endregion
    }
}
﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Model;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.Tests.Integration
{
	public static class Utility
	{
		public static void SetupEntities ( params GrisObject[] grisObjects )
		{
			foreach ( var grisObject in grisObjects )
			{
				EntityFactory.SetupGrisObject(grisObject);
			}
		}

		public static void CacheEntities ( params GrisObject[] grisObjects )
		{
			GrisObject.Cache.Reset();
			GrisObject.Cache.AddObjects(grisObjects);
		}

		public static void InitializeAttributes ( params GrisObject[] grisObjects )
		{
			foreach ( var grisObject in grisObjects )
			{
				EntityFactory.InitializeGrisObjectAttributes(grisObject);
			}
		}

		public static EvaluablePackage PackageIt ( GrisObject grisObject )
		{
			return PackageIt(grisObject, null, null);
		}

		public static EvaluablePackage PackageIt ( GrisObject grisObject, string mockingFunctionName, object mockingFunction )
		{
			Dictionary<string, object> state = new Dictionary<string, object>();

			if ( !string.IsNullOrEmpty(mockingFunctionName) && mockingFunction != null ) state.Add(mockingFunctionName, mockingFunction);
			if ( grisObject is Server ) state.Add("serverMinuteTimeout", 60);

			if ( state.Count > 0 )
			{
				return new EvaluablePackage(grisObject, state);
			}

			return new EvaluablePackage(grisObject);
		}

		public static void SetFormula ( GrisObject grisObject, string formulaPath, EvaluationFormulaType formulaType = EvaluationFormulaType.Default )
		{
			var formula = new ObjectFormula();
			formula.Index = (byte) formulaType;
			formula.ScriptPath = formulaPath;
			formula.GrisObject = grisObject;
			grisObject.ObjectFormulas.Add(formula);
		}

		public static void SetBaseRelation ( GrisObject parent, GrisObject child )
		{
			child.Parent = parent;
			parent.Children.AddEntity(child);
		}

		public static void Eval ( GrisObject grisObject )
		{
			new FakeFormulaEngine().Eval(new List<EvaluablePackage> { PackageIt(grisObject) });
		}

		public static bool ObjectHasErrors ( GrisObject grisObject )
		{
			if ( grisObject != null && grisObject.DefaultEvaluationFormula != null )
			{
				if ( grisObject.CustomEvaluationFormulas != null )
				{
					foreach ( var customEvaluationFormula in grisObject.CustomEvaluationFormulas )
					{
						if ( customEvaluationFormula.HasErrorInEvaluation ) return true;
					}					
				}

				return grisObject.DefaultEvaluationFormula.HasErrorInEvaluation;				
			}

			return true;
		}
	}
}

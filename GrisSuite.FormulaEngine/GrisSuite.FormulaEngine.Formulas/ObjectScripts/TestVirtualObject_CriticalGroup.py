
import sys
import clr
import System

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import *

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.Children.AnyAtLeastInSeverity(Error.Severity):
        obj.SeverityDetail = obj.Children.GetWorstSeverityDetail()
        obj.Parent.ForceSeverity(obj.Severity) # Node
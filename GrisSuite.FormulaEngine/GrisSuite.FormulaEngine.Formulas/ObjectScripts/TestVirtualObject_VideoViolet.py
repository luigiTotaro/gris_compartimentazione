
import sys
import clr
import System

sys.path.append('..')
clr.AddReference('System.Drawing')

from System.Drawing import Color, ColorTranslator

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, VirtualObject):
    clr.Convert(obj, VirtualObject)

    obj = GrisObjectPxy(obj, state)
        
    if obj.Children.AnyAtLeastInSeverity(Error.Severity):
        obj.SeverityDetail = obj.Children.GetWorstSeverityDetail()
        system = GrisObjectPxy(obj.Parent, state)
        system.ForceSeverity(obj.Severity)
        system.Color = ColorTranslator.FromHtml('#7F00FF');
        node = system.Node
        node.ForceSeverity(obj.Severity)

# -*- coding: latin-1 -*-
# script che raccoglie tutte le funzioni relative alle notifiche dal motore
import sys
from Infrastructure.MetaClasses import *
from GrisSuite.FormulaEngine.Model import *
from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

def SetupEnvNotifications (obj, operations):
    if obj.HasChildrenThatShouldSendEmail: obj.Parent.HasChildrenThatShouldSendEmail = True
    def SeverityChanged (obj, sevChangedEvntArgs): SendForErrorOnNodeOrDevice(obj, sevChangedEvntArgs, operations)
    InitializeForSeverityChanged(obj, SeverityChanged)

def InitializeForSeverityChanged (obj, localChangeHandler):
    if (IsObjectA(obj, Node) and obj.IsSmsNotificationEnabled and obj.HasNotificationContacts) or (IsObjectA(obj, Device) and obj.HasNotificationContacts) or (IsObjectA(obj, NodeSystem) and obj.HasNotificationContacts):
        obj.SeverityChanged += localChangeHandler

def SendForErrorOnNodeOrDevice (obj, sevChangedEvntArgs, operations):
	if sevChangedEvntArgs.NewSeverity == Error.Severity:
		if IsObjectA(obj, Node):
			message = obj.GetSmsNotificationMessage()
			for number in obj.GetRecipientNumbers():
				operationKeys = obj.GetSmsNotificationKeys(number)
				operations.SendSms(number, message, operationKeys)
		elif IsObjectA(obj, Device):
			operations.SendDeviceNotification(obj.Id,"")
		elif IsObjectA(obj, NodeSystem):
			operations.SendNodeSystemNotification(obj.Id,"")
	if sevChangedEvntArgs.NewSeverity == Unknown.Severity:
		if IsObjectA(obj, Device) and obj.Type.startswith("D2404LIFT") and obj.Server.Severity == Ok.Severity:
			operations.SendDeviceNotification(obj.Id,"PLC_Offline")


def SendEnvNotifications (obj, operations):
    if IsObjectA(obj, NodeSystem) and obj.HasChildrenThatShouldSendEmail:
        operations.SendStreamFieldMail(obj.SpecificId)
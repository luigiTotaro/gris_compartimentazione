# -*- coding: latin-1 -*-
# script di calcolo dello stato di default per gli oggetti Device
import clr
import sys
#clr.AddReferenceToFile('GrisSuite.Data.Model.dll')
from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Contracts.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.Notifications import SetupEnvNotifications, SendEnvNotifications
from Infrastructure.MetaClasses import *

if isinstance(obj, Device) and (obj.System is not None) and (obj.System != None) and (obj.Type is not None) and (obj.Type != None) and (obj.Server is not None) and (obj.Server != None):
    SetupEnvNotifications(obj, operations)
    obj = GrisObjectPxy(obj, state)
    if obj.ShouldSendNotificationByEmail and not obj.IsAcknowledged: obj.Parent.HasChildrenThatShouldSendEmail = True

    ## Test stato periferiche STLC1000 virtuali (non fisiche, monitorate via SNMP, prive di IP o Porta) # prendono lo stato del relativo server
    if obj.Type == "STLC1000" and (obj.Address == "0" or obj.PortId == None):
        if obj.Server.Severity == Offline.Severity or obj.Server.Severity == Old.Severity:
            obj.SeverityDetail = ServerOffline.SeverityDetail
        else:
            obj.SeverityDetail = obj.Server.SeverityDetail

    elif obj.InMaintenance:
        obj.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail
        
    elif obj.IsActive == 0:
        obj.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail

    elif obj.Server.Severity == NotActive.Severity:
        obj.SeverityDetail = ServerNotActive.SeverityDetail

    elif obj.Server.Severity == Offline.Severity:
        obj.SeverityDetail = ServerOffline.SeverityDetail

    elif obj.IsOffline and "dispositivo non raggiungibile" in (obj.Description or "").lower():
        if obj.ActualSevLevel == Error.Severity:
            ## Caso di dispositivo in offline, ma con severit� forzata ad errore
            obj.SeverityDetail = DiagnosticOfflineInError.SeverityDetail
        else:
            ## Caso standard di dispositivo offline, con severit� sconosciuta
            obj.SeverityDetail = DiagnosticOffline.SeverityDetail

    elif obj.IsOffline and "diagnostica non raggiungibile" in (obj.Description or "").lower():
        if obj.ActualSevLevel == Error.Severity:
            ## Caso di dispositivo in offline, ma con severit� forzata ad errore
            obj.SeverityDetail = DiagnosticOfflineInError.SeverityDetail
        else:
            ## Caso standard di dispositivo offline, con severit� sconosciuta
            obj.SeverityDetail = DiagnosticOffline.SeverityDetail

    elif obj.IsOffline and not ("diagnostica non raggiungibile" in (obj.Description or "").lower()):
        obj.SeverityDetail = Unknown.SeverityDetail

    elif obj.ActualSevLevel == Ok.Severity:
        obj.SeverityDetail = Ok.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
        obj.SeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "diagnostica non disponibile" in (obj.Description or "").lower():
        obj.SeverityDetail = DiagnosticNotAvailableWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
        obj.SeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati di monitoraggio incompleti" in (obj.Description or "").lower():
        obj.SeverityDetail = IncompleteDiagnosticalData.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity:
        obj.SeverityDetail = Warning.SeverityDetail

    elif obj.ActualSevLevel == Error.Severity:
        obj.SeverityDetail = Error.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
        obj.SeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
        obj.SeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "infostazioni" in (obj.Description or "").lower():
        obj.SeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity:
        obj.SeverityDetail = DiagnosticNotAvailable.SeverityDetail

    elif obj.ActualSevLevel == Unknown.Severity and "non applicabile" in (obj.Description or "").lower():
        obj.SeverityDetail = NA.SeverityDetail

    elif obj.ActualSevLevel == Unknown.Severity:
        obj.SeverityDetail = Unknown.SeverityDetail

    else:
        obj.SeverityDetail = NotAvailable.SeverityDetail
      
    ## Periferiche da non considerare nel calcolo dello stato dei padri (ad esempio XXIP000)
    if obj.System.SystemId == Systems.AltrePeriferiche or obj.InMaintenance or obj.SeverityDetail == ServerNotActive.SeverityDetail:
        obj.ExcludedFromParentStatus = True
    ## Un amplificatore non influenza lo stato dei padri se correlato ad un pannello a zone online
    elif obj.Type == "DT04000":
        pzs = obj.GetRelatedDevicesOfTypes("TT10210", "TT10210V3", "TT10220", "TT10230IP", "TT10230_000", "TT10230")
        if pzs.Count > 0 and not pzs[0].IsOffline:
            obj.ExcludedFromParentStatus = True
    
    #elif obj.Type == "VP00010" and obj.StatoProcessiDiSistema.GetStreamFieldByName("Centralino VoIP") and obj.StatoProcessiDiSistema.GetStreamFieldByName("Centralino VoIP").ActualSevLevel == 2: ## Errore
    #    # nel caso un cluster in cui un nodo arbitrario pu� fare le veci dell'altro, la relazione nella tabella object_relations va inserita in ambo i versi
    #    otherNodes = obj.GetRelatedDevicesOfTypes("VP00010")
    #    if otherNodes.Count > 0:
    #        otherNode = GrisObjectPxy(otherNodes[0], state)
    #        if otherNode.StatoProcessiDiSistema.GetStreamFieldByName("Centralino VoIP").ActualSevLevel == 0: # Ok
    #            obj.ExcludedFromParentStatus = True

    #elif obj.Type == "VP00010" and obj.StatoProcessiDiSistema.GetStreamFieldByName("Servizio Asterisk") and obj.StatoProcessiDiSistema.GetStreamFieldByName("Servizio Asterisk").ActualSevLevel == 2: ## Errore
    #    # nel caso un cluster in cui un nodo arbitrario pu� fare le veci dell'altro, la relazione nella tabella object_relations va inserita in ambo i versi
    #    otherNodes = obj.GetRelatedDevicesOfTypes("VP00010")
    #    if otherNodes.Count > 0:
    #        otherNode = GrisObjectPxy(otherNodes[0], state)
    #        if otherNode.StatoProcessiDiSistema.GetStreamFieldByName("Servizio Asterisk").ActualSevLevel == 0: # Ok
    #            obj.ExcludedFromParentStatus = True

    ####
    ## Test stato periferiche STLC1000 virtuali (non fisiche, monitorate via SNMP, prive di IP o Porta) # prendono lo stato del relativo server
    if obj.Type == "STLC1000" and (obj.Address == "0" or obj.PortId == None):
        if obj.Server.RealSeverity == Offline.Severity or obj.Server.RealSeverity == Old.Severity:
            obj.RealSeverityDetail = ServerOffline.SeverityDetail
        else:
            obj.RealSeverityDetail = obj.Server.RealSeverityDetail

    elif obj.IsActive == 0:
        obj.RealSeverityDetail = DiagnosticDisabledByOperator.SeverityDetail

    elif obj.Server.RealSeverity == Offline.Severity:
        obj.RealSeverityDetail = ServerOffline.SeverityDetail

    elif obj.IsOffline and "dispositivo non raggiungibile" in (obj.Description or "").lower():
        if obj.ActualSevLevel == Error.Severity:
            ## Caso di dispositivo in offline, ma con severit� forzata ad errore
            obj.RealSeverityDetail = DiagnosticOfflineInError.SeverityDetail
        else:
            ## Caso standard di dispositivo offline, con severit� sconosciuta
            obj.RealSeverityDetail = DiagnosticOffline.SeverityDetail

    elif obj.IsOffline and "diagnostica non raggiungibile" in (obj.Description or "").lower():
        if obj.ActualSevLevel == Error.Severity:
            ## Caso di dispositivo in offline, ma con severit� forzata ad errore
            obj.RealSeverityDetail = DiagnosticOfflineInError.SeverityDetail
        else:
            ## Caso standard di dispositivo offline, con severit� sconosciuta
            obj.RealSeverityDetail = DiagnosticOffline.SeverityDetail

    elif obj.IsOffline and not ("diagnostica non raggiungibile" in (obj.Description or "").lower()):
        obj.RealSeverityDetail = Unknown.SeverityDetail

    elif obj.ActualSevLevel == Ok.Severity:
        obj.RealSeverityDetail = Ok.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
        obj.RealSeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "diagnostica non disponibile" in (obj.Description or "").lower():
        obj.RealSeverityDetail = DiagnosticNotAvailableWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
        obj.RealSeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati di monitoraggio incompleti" in (obj.Description or "").lower():
        obj.RealSeverityDetail = IncompleteDiagnosticalData.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity:
        obj.RealSeverityDetail = Warning.SeverityDetail

    elif obj.ActualSevLevel == Error.Severity:
        obj.RealSeverityDetail = Error.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
        obj.RealSeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
        obj.RealSeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "infostazioni" in (obj.Description or "").lower():
        obj.RealSeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity:
        obj.RealSeverityDetail = DiagnosticNotAvailable.SeverityDetail

    elif obj.ActualSevLevel == Unknown.Severity and "non applicabile" in (obj.Description or "").lower():
        obj.RealSeverityDetail = NA.SeverityDetail

    elif obj.ActualSevLevel == Unknown.Severity:
        obj.RealSeverityDetail = Unknown.SeverityDetail

    else:
        obj.RealSeverityDetail = NotAvailable.SeverityDetail
            
            
    ## Test stato periferiche STLC1000 virtuali (non fisiche, monitorate via SNMP, prive di IP o Porta) # prendono lo stato del relativo server, se presente
    if obj.Type == "STLC1000" and (obj.Address == "0" or obj.PortId == None) and obj.Server.SevLevelDetailLast is not None and not obj.ForcedByUser:
        obj.LastSeverityDetail = obj.Server.LastSeverityDetail

    elif obj.Type == "STLC1000" and (obj.Address == "0" or obj.PortId == None) and obj.Server.SevLevelDetailLast is None and obj.ForcedByUser:
        obj.LastSeverityDetail = NotAvailable.SeverityDetail

    elif obj.IsActive == 0:
        obj.LastSeverityDetail = DiagnosticDisabledByOperator.SeverityDetail

    elif obj.ActualSevLevel == Ok.Severity:
        obj.LastSeverityDetail = Ok.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
        obj.LastSeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "diagnostica non disponibile" in (obj.Description or "").lower():
        obj.LastSeverityDetail = DiagnosticNotAvailableWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
        obj.LastSeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity and "dati di monitoraggio incompleti" in (obj.Description or "").lower():
        obj.LastSeverityDetail = IncompleteDiagnosticalData.SeverityDetail

    elif obj.ActualSevLevel == Warning.Severity:
        obj.LastSeverityDetail = Warning.SeverityDetail

    elif obj.ActualSevLevel == Error.Severity:
        obj.LastSeverityDetail = Error.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "dati diagnostici limitati alla sola sezione mib-ii" in (obj.Description or "").lower():
        obj.LastSeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "dati snmp incongruenti" in (obj.Description or "").lower():
        obj.LastSeverityDetail = InconsistentSNMPDataWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity and "infostazioni" in (obj.Description or "").lower():
        obj.LastSeverityDetail = MIB2OnlyWarning.SeverityDetail

    elif obj.ActualSevLevel == NotActive.Severity:
        obj.LastSeverityDetail = DiagnosticNotAvailable.SeverityDetail

    elif obj.ActualSevLevel == Unknown.Severity and "non applicabile" in (obj.Description or "").lower():
        obj.LastSeverityDetail = NA.SeverityDetail

    elif obj.ActualSevLevel == Unknown.Severity:
        obj.LastSeverityDetail = Unknown.SeverityDetail

    else:
        obj.LastSeverityDetail = NotAvailable.SeverityDetail
﻿
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Library;
using GrisSuite.FormulaEngine.Model;

namespace GrisSuite.FormulaEngine.Service
{
	public class FormulaService : IFormulaService
	{
		private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
		private readonly Stopwatch _timer = new Stopwatch();

		public IList<IGrisObject> EvaluateObjects ( IList<EvaluablePackage> objectsToEval )
		{
			var launcher = new Launcher();
			var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });
			launcher.EvaluateObjects(engine, objectsToEval);
			return ( from evaluablePackage in objectsToEval select evaluablePackage.GrisObject ).ToList();
		}

		public void EvaluateAndSaveObjects ( string objectType, Guid[] objectIds )
		{
			_logger.Info("Inizio elaborazione web method 'EvaluateAndSaveObjects'...");
			_logger.Info("Parametri ricevuti: {0}, {1}.", objectType, string.Join(", ", objectIds));
			_timer.Start();

			Type objType = Type.GetType(objectType);
			this.EvaluateAndSaveObjects(objType, objectIds);

			_timer.Stop();
			_logger.Info("Elaborazione totale eseguita in {0} millisecondi.", this._timer.ElapsedMilliseconds);
		}

		private void EvaluateAndSaveObjects ( Type objectType, Guid[] objectIds )
		{
			try
			{
				var launcher = new Launcher();
				var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });

				var objectPcks = launcher.LoadObjectsFromDb(objectType, objectIds);
				launcher.EvaluateObjects(engine, objectPcks);
				launcher.SaveToDb(objectType, objectPcks);
			}
			catch ( Exception exc )
			{
				_logger.ErrorException(string.Format("{0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
				throw;
			}
		}

		public void EvaluateAndSaveObjectAndAncestors ( Guid objectId )
		{
			_logger.Info("Inizio elaborazione web method 'EvaluateAndSaveObjectAndAncestors'...");
			_logger.Info("Parametri ricevuti: {0}.", objectId);
			_timer.Start();

			try
			{
				var launcher = new Launcher();
				var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });

				var objs = GrisObject.GetObjectAndAncestors(objectId);

				if ( objs != null && objs.Count > 0 )
				{
					var objsPckg = DataAccessHelper.InitializeObjects(objs);
					launcher.EvaluateObjects(engine, objsPckg);

					List<EvaluablePackage> regPckg = objsPckg.Where(pckg => pckg.GrisObject is IRegion).ToList();

					if ( regPckg.Count > 0 )
					{
						launcher.SaveToDb(typeof(IRegion), regPckg);
					}
				}
			}
			catch ( Exception exc )
			{
				_logger.ErrorException(string.Format("{0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
				throw;
			}

			_timer.Stop();
			_logger.Info("Elaborazione totale eseguita in {0} millisecondi.", this._timer.ElapsedMilliseconds);
		}

		public void EvaluateAndSaveRegion ( Guid objectId )
		{
			_logger.Info("Inizio elaborazione web method 'EvaluateAndSaveRegion'...");
			_logger.Info("Parametri ricevuti: {0}.", objectId);
			_timer.Start();

			try
			{
				var launcher = new Launcher();
				var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });

				// questo metodo presuppone che gli oggetti da valutare siano tutti precaricati e compresi tra i discendenti del compartimento
				// se non si può soddisfare il prerequisito è meglio utilizzare il metodo EvaluateAndSaveObjectAndAncestors
				var objs = GrisObject.GetObjectsAndDescendantsByIds(typeof(IRegion), new ObjectInitializationValues { ToBeEvaluated = true, LookupInCacheOnly = true }, objectId);

				if ( objs != null && objs.Count > 0 )
				{
					var objsPckg = DataAccessHelper.InitializeObjects(objs);
					launcher.EvaluateObjects(engine, objsPckg);

					List<EvaluablePackage> regPckg = objsPckg.Where(pckg => pckg.GrisObject is IRegion).ToList();

					if ( regPckg.Count > 0 )
					{
						launcher.SaveToDb(typeof(IRegion), regPckg);
					}
				}
			}
			catch ( Exception exc )
			{
				_logger.ErrorException(string.Format("{0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
				throw;
			}

			_timer.Stop();
			_logger.Info("Elaborazione totale eseguita in {0} millisecondi.", this._timer.ElapsedMilliseconds);
		}

		public void EvaluateAndSaveAllObjects ()
		{
			_logger.Info("Inizio elaborazione web method 'EvaluateAndSaveAllObjects'...");
			_timer.Start();

			try
			{
				Guid marker = Guid.NewGuid();
				var launcher = new Launcher();
				var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });

				launcher.PrepareObjects(marker);
				var objectPcks = launcher.LoadObjectsFromDb(null);
				launcher.EvaluateObjects(engine, objectPcks);
				launcher.SaveToDb(objectPcks);
				launcher.FinalizeObjects(marker);
			}
			catch ( Exception exc )
			{
				_logger.ErrorException(string.Format("{0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
			}

			_timer.Stop();
			_logger.Info("Elaborazione totale eseguita in {0} millisecondi.", this._timer.ElapsedMilliseconds);
		}

		public IList<IGrisObject> EvaluateAllObjects()
		{
			_logger.Info("Inizio elaborazione web method 'EvaluateAllObjects'...");
			_timer.Start();

			Guid marker = Guid.NewGuid();
			var launcher = new Launcher();
			var engine = new Library.FormulaEngine(new List<string> { Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\Lib") });
			IList<EvaluablePackage> objectPcks = new List<EvaluablePackage>(0);

			try
			{
				launcher.PrepareObjects(marker);
				objectPcks = launcher.LoadObjectsFromDb(null);
				launcher.EvaluateObjects(engine, objectPcks);
				launcher.SaveToDb(objectPcks);
				launcher.FinalizeObjects(marker);
			}
			catch ( Exception exc )
			{
				_logger.ErrorException(string.Format("{0}<br />ST: {1}", exc.Message, exc.StackTrace), exc);
			}

			_timer.Stop();
			_logger.Info("Elaborazione totale eseguita in {0} millisecondi.", this._timer.ElapsedMilliseconds);

			return ( from evaluablePackage in objectPcks select evaluablePackage.GrisObject ).ToList();
		}

		public string GenerateNodeColorFormula ( Guid nodeId, string nodeName, SystemSeverityCombination[] combinations )
		{
			string fileName = string.Format("{0}_{1}_Color.py", nodeId, this.GetObjectNameForFileName(nodeName));

			this.GenerateFormulaFile(fileName, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin"), "ColorFormula.txt", combinations);

			var engineLibDirs = ConfigurationManager.AppSettings["FormulaDistributionFolders"];
			if ( !string.IsNullOrEmpty(engineLibDirs) )
			{
				foreach (var distBasePath in engineLibDirs.Split(','))
				{
					this.GenerateFormulaFile(fileName, distBasePath, "ColorFormula.txt", combinations);
				}
			}

			return fileName;
		}

		private void GenerateFormulaFile ( string fileName, string baseFolderPath, string templateFileName, SystemSeverityCombination[] combinations )
		{
			var conditionalColorAssignments = new StringBuilder();

			if ( combinations != null && combinations.Length > 0 )
			{
				int index = 0;
				foreach (var systemSeverityCombination in combinations)
				{
					var color = systemSeverityCombination.Color;

					if ( systemSeverityCombination.SystemSeverities != null && systemSeverityCombination.SystemSeverities.Length > 0 )
					{
						var severityTests = new List<string>();
						foreach ( var systemSeverityTuple in systemSeverityCombination.SystemSeverities )
						{
							severityTests.Add(string.Format("obj.GetSystemBySystemId({0}).Severity == {1}", systemSeverityTuple.SystemId, systemSeverityTuple.Severity));
						}

						conditionalColorAssignments.AppendFormat(@"
		{0} {1}:
			obj.Color = ColorTranslator.FromHtml('{2}')", index == 0 ? "if" : "elif", string.Join(" and ", severityTests), string.Format("#{0}{1}{2}", color.R.ToString("X2"), color.G.ToString("X2"), color.B.ToString("X2")));
					}
					index++;
				}
			}

			string templateFolderPath = Path.Combine(baseFolderPath, "Lib\\Templates");
			string destinationFolderPath = Path.Combine(baseFolderPath, "Lib\\ObjectScripts");
			string templateFormula;

			using ( var sr = File.OpenText(Path.Combine(templateFolderPath, templateFileName)) )
			{
				templateFormula = sr.ReadToEnd();				
			}
			
			File.WriteAllText(Path.Combine(destinationFolderPath, fileName), string.Format(templateFormula, conditionalColorAssignments));
		}

		private string GetObjectNameForFileName ( string objectName )
		{
			StringBuilder objName = new StringBuilder();
			for (int index = 0; index < objectName.Length; index++)
			{
				if (objectName[index] != ' ' && Array.IndexOf(Path.GetInvalidPathChars(), objectName[index]) == -1 && objectName[index] != '\\' && objectName[index] != '/')
				{
					objName.Append(objectName[index]);
				}
			}

			return objName.ToString();
		}
	}
}

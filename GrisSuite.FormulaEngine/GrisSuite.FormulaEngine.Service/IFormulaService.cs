﻿
using System;
using System.Collections.Generic;
using System.ServiceModel;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Library;

namespace GrisSuite.FormulaEngine.Service
{
	[ServiceContract]
	public interface IFormulaService
	{
		[OperationContract]
		IList<IGrisObject> EvaluateObjects ( IList<EvaluablePackage> objectsToEval );

		[OperationContract]
		void EvaluateAndSaveObjects ( string entityType, Guid[] objectIds );

		[OperationContract]
		void EvaluateAndSaveObjectAndAncestors ( Guid objectId );

		[OperationContract]
		void EvaluateAndSaveRegion ( Guid objectId );

		[OperationContract]
		void EvaluateAndSaveAllObjects ();

		[OperationContract]
		IList<IGrisObject> EvaluateAllObjects ();

		[OperationContract]
		string GenerateNodeColorFormula ( Guid nodeId, string nodeName, SystemSeverityCombination[] combinations );
	}
}

﻿<pre style="font-family:Lucida Console,Monaco,monospace;font-size:10pt;">
§Avviso generato il {0} alle ore {1}

Problemi al sistema <strong>{2}</strong> della stazione di '<strong>{3}</strong>'.

Le periferiche seguenti hanno generato un avviso:
§
  - Gruppo '{0}'
§
    - Periferica '{0}'
§
      - Stream '{0}'
§
        - StreamField '{0}'
           Valore '{1}'
§
</pre>
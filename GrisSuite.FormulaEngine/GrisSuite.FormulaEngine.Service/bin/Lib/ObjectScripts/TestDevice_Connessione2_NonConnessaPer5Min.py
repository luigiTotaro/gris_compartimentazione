
import sys
import clr
import System

sys.path.append('..')

from datetime import date, datetime, timedelta

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy

if isinstance(obj, Device):
    clr.Convert(obj, Device)

    obj = GrisObjectPxy(obj, state)
        
    if obj.InformazioniPorteSwitch.ConnessionePortaPrimaria2.IsInConditionForTimeSpan('StatoConnessione', Error.Severity, System.TimeSpan(0, 5, 0)):
        obj.SeverityDetail = Offline.SeverityDetail
        obj.System.Dev1ConnessaAPorta2.ForceSeverity(obj.SeverityDetail)
        obj.System.Dev2ConnessaAPorta2.ForceSeverity(obj.SeverityDetail)
# -*- coding: latin-1 -*-
#[10:20:05] dam107it: tip sul TT10220
#[10:20:50] dam107it: Stato generale\Stato amplificatore1\case{Uscita installata=0;Uscita in servizio=0; ...}
#[10:22:58] Cristian Storti: in pratica stai dicendo che se i cases sono tutt ok allora metto la periferica ok
#[10:23:00] Cristian Storti: giusto?
#[10:24:28] dam107it: ma ... visto che il PZ conosce anche lo stato dell'Ampli > se il PZ dice che l'Amli � OK ... tralasciamo l'errore dell'ampli nel conteggio
import sys
import clr
import System

sys.path.append('..')

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.MetaClasses import GrisObjectPxy
from Infrastructure.GrisSystemFormulas import *

if isinstance(obj, Device):
	clr.Convert(obj, Device)

	obj = GrisObjectPxy(obj, state)

	if obj.StatoGenerale.StatoAmplificatore1.Cases.Count > 0:
		if obj.StatoGenerale.StatoAmplificatore1.AreAllCasesInSeverity(Ok.SeverityDetail):
			obj.SeverityDetail = Ok.SeverityDetail
			obj.Parent.Amplificatore.ForceSeverity(obj.SeverityDetail)
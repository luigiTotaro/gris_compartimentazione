# -*- coding: latin-1 -*-
# script di calcolo dello stato di default per gli oggetti Server
import clr
import sys
import time
#clr.AddReferenceToFile('GrisSuite.Data.Model.dll')
#clr.AddReference("System")
import System
from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Severities import *
from datetime import date, datetime, timedelta

from Infrastructure.GrisSystemFormulas import *
from Infrastructure.MetaClasses import *

from System import DateTime, Nullable

if isinstance(obj, Server):
    obj = GrisObjectPxy(obj, state)

    lastUpdate = datetime(2000, 1, 1)
    serverMinuteTimeout = timedelta(minutes=state["serverMinuteTimeout"])

    if not obj.LastUpdate == None:
        lastUpdate = datetime(obj.LastUpdate)

    ####
        
    # Se il server ha uno stato forzato in base dati dall'utente (Sconosciuto o Diagnostica disattivata dall'operatore), non ne modifichiamo lo stato
    if obj.ForcedByUser:
        if obj.InMaintenance:
            obj.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail

            # Severit� di dettaglio reali
            if obj.IsDeleted and obj.Devices.Count == 0:
                obj.RealSeverityDetail = Old.SeverityDetail

            elif not obj.IsDeleted and obj.Devices.Count == 0:
                obj.RealSeverityDetail = IncompleteServerData.SeverityDetail

            elif (datetime.now() - lastUpdate) > serverMinuteTimeout and serverMinuteTimeout != 0:
                obj.RealSeverityDetail = Offline.SeverityDetail

            elif IsNullOrEmpty(obj.IpAddress):
                obj.RealSeverityDetail = NoIPAddress.SeverityDetail

            elif IsNullOrEmpty(obj.FullHostName):
                obj.RealSeverityDetail = NoHostName.SeverityDetail

            elif IsNullOrEmpty(obj.MacAddress):
                obj.RealSeverityDetail = NoMACAddress.SeverityDetail

            else:
                obj.RealSeverityDetail = Ok.SeverityDetail
        else:
            obj.SeverityDetail = Unknown.SeverityDetail
            obj.RealSeverityDetail = NotAvailable.SeverityDetail
    else:
        # Severit� di dettaglio
        if obj.IsDeleted and obj.Devices.Count == 0:
            obj.SeverityDetail = Old.SeverityDetail

        elif not obj.IsDeleted and obj.Devices.Count == 0:
            obj.SeverityDetail = IncompleteServerData.SeverityDetail

        elif (datetime.now() - lastUpdate) > serverMinuteTimeout and serverMinuteTimeout != 0:
            obj.SeverityDetail = Offline.SeverityDetail

        elif IsNullOrEmpty(obj.IpAddress):
            obj.SeverityDetail = NoIPAddress.SeverityDetail

        elif IsNullOrEmpty(obj.FullHostName):
            obj.SeverityDetail = NoHostName.SeverityDetail

        elif IsNullOrEmpty(obj.MacAddress):
            obj.SeverityDetail = NoMACAddress.SeverityDetail

        elif obj.InMaintenance:
            obj.SeverityDetail = DiagnosticDisabledByOperator.SeverityDetail

        elif obj.HasDuplicateIpAddress():
            obj.SeverityDetail = DuplicateIPAddress.SeverityDetail

        else:
            obj.SeverityDetail = Ok.SeverityDetail

        # Severit� di dettaglio reali
        if obj.IsDeleted and obj.Devices.Count == 0:
            obj.RealSeverityDetail = Old.SeverityDetail

        elif not obj.IsDeleted and obj.Devices.Count == 0:
            obj.RealSeverityDetail = IncompleteServerData.SeverityDetail

        elif (datetime.now() - lastUpdate) > serverMinuteTimeout and serverMinuteTimeout != 0:
            obj.RealSeverityDetail = Offline.SeverityDetail

        elif IsNullOrEmpty(obj.IpAddress):
            obj.RealSeverityDetail = NoIPAddress.SeverityDetail

        elif IsNullOrEmpty(obj.FullHostName):
            obj.RealSeverityDetail = NoHostName.SeverityDetail

        elif IsNullOrEmpty(obj.MacAddress):
            obj.RealSeverityDetail = NoMACAddress.SeverityDetail

        else:
            obj.RealSeverityDetail = Ok.SeverityDetail
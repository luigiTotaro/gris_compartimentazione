# -*- coding: latin-1 -*-
# script di calcolo dello stato per gli oggetti appartenenti al raggruppamento dispositivi generici
import clr
import sys

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Contracts.Entities import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage, SeverityWeightsAndRanges
from Infrastructure.Notifications import SetupEnvNotifications, SendEnvNotifications

from System.Collections.Generic import IList, List

def BuildSeverityRanges():
	okRange = SeverityRange()
	okRange.Id = 1
	okRange.Name = "In Servizio"
	okRange.SeverityWeight = 0.0
	okRange.MinValue = 0
	okRange.MaxValue = 3

	warRange = SeverityRange()
	warRange.Id = 2
	warRange.Name = "Anomalia Lieve"
	warRange.SeverityWeight = 4.4
	warRange.MinValue = 3
	warRange.MaxValue = 10

	errRange = SeverityRange()
	errRange.Id = 3
	errRange.Name = "Anomalia Grave"
	errRange.SeverityWeight = 10.0
	errRange.MinValue = 0
	errRange.MaxValue = 0

	ranges = List[ISeverityRange]()
	ranges.Add(okRange)
	ranges.Add(warRange)
	ranges.Add(errRange)

	return ranges

if isinstance(obj, VirtualObject):
	SetupEnvNotifications(obj, operations)

	if obj.Children.Count == 0:
		obj.ExcludedFromParentStatus = True
		obj.SeverityDetail = NotAvailable.SeverityDetail
	else:
		obj.ExcludedFromParentStatus = False
		noErrorSevRanges = BuildSeverityRanges()

		weightsAndRanges = SeverityWeightsAndRanges(noErrorSevRanges)
		obj.Severity = GetWeightedAverage(
			obj.Children.GetInSeverity(Ok.Severity).Count, 
			obj.Children.GetInSeverity(Warning.Severity).Count, 
			obj.Children.GetInSeverity(Error.Severity).Count + obj.Children.GetInSeverity(Offline.Severity).Count + obj.Children.GetInSeverity(Unknown.Severity).Count, 
			obj.Children.GetInSeverity(NotActive.Severity).Count,
			weightsAndRanges)

	SendEnvNotifications(obj, operations)
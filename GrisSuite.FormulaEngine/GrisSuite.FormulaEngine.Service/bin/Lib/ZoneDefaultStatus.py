# -*- coding: latin-1 -*-
# script di calcolo dello stato di default per gli oggetti Region
import clr
import sys

from GrisSuite.FormulaEngine.Model.Entities import *
from GrisSuite.FormulaEngine.Common.Contracts import *
from GrisSuite.FormulaEngine.Common.Severities import *

from Infrastructure.GrisSystemFormulas import GetWeightedAverage, SeverityWeightsAndRanges
from Infrastructure.MetaClasses import *

if isinstance(obj, Zone):
    obj = GrisObjectPxy(obj, state)

    if obj.AreAllObjectsByParentUnknown():
        obj.Severity = Unknown.Severity
        # Il flag ForcedSeverityLevel era valorizzato nella function object_status_calc, ma mai portato in update sulla tabella object_status
        # obj.ForcedSeverityLevel = ForcedSeverityLevel.AllChildrenAreNotReachable
    else:
        # obj.ForcedSeverityLevel = ForcedSeverityLevel.NotForced
        weightsAndRanges = SeverityWeightsAndRanges(sevRanges)
        obj.Severity = GetWeightedAverage(
            obj.Nodes.GetInSeverity(Ok.Severity).Count,
            obj.Nodes.GetInSeverity(Warning.Severity).Count,
            obj.Nodes.GetInSeverity(Error.Severity).Count + obj.Nodes.GetInSeverity(Offline.Severity).Count + obj.Nodes.GetInSeverity(Unknown.Severity).Count,
            obj.Nodes.GetInSeverity(NotActive.Severity, True).Count,
            weightsAndRanges)
﻿
using System.Drawing;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Service
{
	[DataContract]
	public class SystemSeverityCombination
	{
		[DataMember]
		public SystemSeverityTuple[] SystemSeverities { get; set; }
		[DataMember]
		public Color Color { get; set; }
	}
}
﻿
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Service
{
	[DataContract]
	public class SystemSeverityTuple
	{
		[DataMember]
		public int SystemId { get; set; }
		[DataMember]
		public int Severity { get; set; }
	}
}
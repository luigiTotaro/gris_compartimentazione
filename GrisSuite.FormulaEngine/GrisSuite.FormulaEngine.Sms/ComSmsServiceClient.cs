﻿using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Sms.ComSmsService;
using NLog;

namespace GrisSuite.FormulaEngine.Sms
{
    public class ComSmsServiceClient
    {
        private readonly Logger logger;

        public ComSmsServiceClient(Logger logger)
        {
            this.logger = logger;
        }

        private void LogInfo(string logMessage)
        {
            if (this.logger != null)
            {
                this.logger.Info(logMessage);
            }
        }

        private void LogWarn(string logMessage)
        {
            if (this.logger != null)
            {
                this.logger.Warn(logMessage);
            }
        }

        public OperationScheduleExecResult Send(string phoneNumbers,
            string message,
            string smsEndpoint,
            string smsEndpointBackup,
            string applicationId,
            string sourceAddress)
        {
            // Non ci sono test sui parametri in ingresso, perché eseguiti sulla chiamante, che logga
            string[] destinationNumbers =
                (from address in phoneNumbers.Split(',') where !string.IsNullOrEmpty(address) select address.Trim()).ToArray();

            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.UseDefaultWebProxy = true;

            EndpointAddress endpointAddress = new EndpointAddress(new Uri(smsEndpoint));
            ComSmsTypeClient client = new ComSmsTypeClient(binding, endpointAddress);

            ComSmsTypeClient clientBackup = null;
            if (!string.IsNullOrEmpty(smsEndpointBackup))
            {
                EndpointAddress endpointAddressBackup = new EndpointAddress(new Uri(smsEndpointBackup));
                clientBackup = new ComSmsTypeClient(binding, endpointAddressBackup);
            }

            ParSmSubmitRequestType request = new ParSmSubmitRequestType();
            request.ApplicationId = applicationId;
            request.DestinationAddresses = destinationNumbers;
            request.Item = message;
            request.ItemElementName = ItemChoiceType.AlphanumericMessage;

            if (!string.IsNullOrEmpty(sourceAddress))
            {
                request.SourceAddress = sourceAddress;
            }

            try
            {
                return this.SendItem(client, request);
            }
            catch (Exception ex)
            {
                // Verifichiamo se è configurato un endpoint di backup per l'invio
                if (clientBackup != null)
                {
                    WebException webEx = ex as WebException;

                    if (webEx != null)
                    {
                        if ((webEx.Status == WebExceptionStatus.ConnectFailure) || (webEx.Status == WebExceptionStatus.Timeout) ||
                            (webEx.Status == WebExceptionStatus.ConnectionClosed) ||
                            (((HttpWebResponse) webEx.Response).StatusCode == HttpStatusCode.InternalServerError) ||
                            (((HttpWebResponse) webEx.Response).StatusCode == HttpStatusCode.ServiceUnavailable))
                        {
                            this.LogInfo(
                                String.Format(
                                    "Eccezione durante chiamata a servizio invio SMS, tentativo su endpoint di backup. Tipo eccezione: {0}, Messaggio: {1}, Messaggio interno: {2}, Stato Http: {3}",
                                    webEx.GetType(), webEx.Message, (webEx.InnerException != null) ? webEx.InnerException.Message : "--",
                                    ((HttpWebResponse) webEx.Response).StatusCode));

                            return this.TrySendItemByBackup(clientBackup, request);
                        }
                    }

                    EndpointNotFoundException enfEx = ex as EndpointNotFoundException;

                    if (enfEx != null)
                    {
                        this.LogInfo(
                            String.Format(
                                "Eccezione durante chiamata a servizio invio SMS, tentativo su endpoint di backup. Tipo eccezione: {0}, Messaggio: {1}, Messaggio interno: {2}",
                                enfEx.GetType(), enfEx.Message, (enfEx.InnerException != null) ? enfEx.InnerException.Message : "--"));

                        return this.TrySendItemByBackup(clientBackup, request);
                    }
                }

                this.LogWarn(
                    String.Format("Eccezione durante chiamata a servizio invio SMS. Tipo eccezione: {0}, Messaggio: {1}, Messaggio interno: {2}",
                        ex.GetType(), ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "--"));

                OperationScheduleExecResult result = new OperationScheduleExecResult();
                result.Status = Common.OperationScheduleResultStatus.ShouldRetry;
                result.InnerException = ex;
                result.Message = ex.Message;
                return result;
            }
        }

        private OperationScheduleExecResult TrySendItemByBackup(ComSmsTypeClient clientBackup, ParSmSubmitRequestType request)
        {
            try
            {
                this.LogInfo(String.Format("Esiste un endpoint di backup configurato per l'invio"));

                return this.SendItem(clientBackup, request);
            }
            catch (Exception exBackup)
            {
                this.LogWarn(
                    String.Format(
                        "Eccezione durante chiamata a servizio invio SMS su endpoint di backup. Tipo eccezione: {0}, Messaggio: {1}, Messaggio interno: {2}",
                        exBackup.GetType(), exBackup.Message, (exBackup.InnerException != null) ? exBackup.InnerException.Message : "--"));

                OperationScheduleExecResult resultBackup = new OperationScheduleExecResult();
                resultBackup.Status = Common.OperationScheduleResultStatus.ShouldRetry;
                resultBackup.InnerException = exBackup;
                resultBackup.Message = exBackup.Message;
                return resultBackup;
            }
        }

        private OperationScheduleExecResult SendItem(ComSmsTypeClient client, ParSmSubmitRequestType request)
        {
            OperationScheduleExecResult result;

            this.LogInfo(String.Format("Creazione chiamata a servizio: {0}", client.Endpoint.Address.Uri));

            ParSmSubmitResponseType response = client.SmSubmit(request);

            if (response.Item is ErrorType)
            {
                ErrorType error = response.Item as ErrorType;
                this.LogWarn(String.Format("Errore ricevuto durante invio SMS. Codice errore: {0}, Messaggio: {1}", error.Code, error.Message));

                result = new OperationScheduleExecResult();
                result.Status = Common.OperationScheduleResultStatus.Error;
                result.Code = error.Code;
                result.Message = String.Format("Endpoint: {0}, Message: {1}", client.Endpoint.Address.Uri, error.Message);
                return result;
            }

            result = new OperationScheduleExecResult();
            result.Parameters = new System.Collections.Generic.Dictionary<string, string>();

            if (response.Item is IdElementListType)
            {
                IdElementListType idElementList = response.Item as IdElementListType;

                this.LogInfo(String.Format("Invio SMS avvenuto con successo"));

                if (idElementList.IdElement.Length > 0)
                {
                    // Elaboriamo solo il primo elemento, gli invii sono singoli
                    IdElementType id = idElementList.IdElement[0];

                    result.Parameters.Add("DestinationAddress", id.DestinationAddress);
                    result.Parameters.Add("SessionId", id.SessionId);

                    this.LogInfo(String.Format("Destinatario SMS: {0}, SessionId invio: {1}", id.DestinationAddress, id.SessionId));
                }
            }

            result.Status = Common.OperationScheduleResultStatus.Success;
            return result;
        }
    }
}
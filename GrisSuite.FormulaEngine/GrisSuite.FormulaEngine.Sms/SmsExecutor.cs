﻿using System;
using System.Configuration;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using NLog;

namespace GrisSuite.FormulaEngine.Sms
{
    public class SmsExecutor : IOperationExecutor
    {
        #region Implementation of IOperationExecutor

        private string smsEndpoint
        {
            get { return ConfigurationManager.AppSettings["SmsEndpoint"] ?? ""; }
        }

        private string smsEndpointBackup
        {
            get { return ConfigurationManager.AppSettings["SmsEndpointBackup"] ?? ""; }
        }

        private string applicationId
        {
            get { return ConfigurationManager.AppSettings["SmsApplicationID"] ?? "gris1"; }
        }

        private string sourceAddress
        {
            get { return ConfigurationManager.AppSettings["SmsSourceAddress"] ?? "9040"; }
        }

        /// <summary>
        ///     Esegue un operazione passando le informazioni di stato di cui l'operazione ha bisogno.
        /// </summary>
        /// <param name="operationSchedule">Entità che rappresenta l'operazione da eseguire.</param>
        /// <param name="logger">Istanza logger</param>
        /// <returns></returns>
        public OperationScheduleExecResult Exec(IOperationSchedule operationSchedule, Logger logger)
        {
            var state = operationSchedule.Parameters;
            if (state == null)
            {
                throw new ArgumentException("Il parametro 'state' non è inizializzato correttamente.", "operationSchedule");
            }

            if (!state.ContainsKey("phoneNum") || string.IsNullOrEmpty(state["phoneNum"]))
            {
                throw new ArgumentException("Impossibile inviare l'SMS: fornire un numero telefonico.");
            }

            if (!state.ContainsKey("message") || string.IsNullOrEmpty(state["message"]))
            {
                throw new ArgumentException("Impossibile inviare l'SMS: fornire un messaggio.");
            }

            string webServiceUrl = this.smsEndpoint;
            string webServiceUrlBackup = this.smsEndpointBackup;

            if (string.IsNullOrWhiteSpace(webServiceUrl))
            {
                if (state.ContainsKey("url") && !string.IsNullOrEmpty(state["url"]))
                {
                    // Questo accade ed è usato solamente per i test del servizio, non in produzione
                    webServiceUrl = state["url"];
                }
                else
                {
                    throw new ArgumentException("Impossibile inviare l'SMS: configurazione Endpoint per Web Service mancante.");
                }
            }

            if (string.IsNullOrWhiteSpace(webServiceUrlBackup))
            {
                if (state.ContainsKey("urlBackup") && !string.IsNullOrEmpty(state["urlBackup"]))
                {
                    // Questo accade ed è usato solamente per i test del servizio, non in produzione
                    webServiceUrlBackup = state["urlBackup"];
                }
            }

            if (string.IsNullOrWhiteSpace(this.applicationId))
            {
                throw new ArgumentException("Impossibile inviare l'SMS: configurazione Application ID per Web Service mancante.");
            }

            if (string.IsNullOrWhiteSpace(this.sourceAddress))
            {
                throw new ArgumentException("Impossibile inviare l'SMS: configurazione Numero Telefonico sorgente per Web Service mancante.");
            }

            string phoneNumber = state["phoneNum"];
            string message = state["message"];

            if (!string.IsNullOrEmpty(phoneNumber) && (!string.IsNullOrEmpty(message)))
            {
                if (logger != null)
                {
                    logger.Info("Numero telefonico destinatario: {0}, testo messaggio: \"{1}\"", phoneNumber, message);
                }

                ComSmsServiceClient smsClient = new ComSmsServiceClient(logger);
                return smsClient.Send(phoneNumber, message, webServiceUrl, webServiceUrlBackup, this.applicationId, this.sourceAddress);
            }

            OperationScheduleExecResult result = new OperationScheduleExecResult {Status = Common.OperationScheduleResultStatus.Error};
            return result;
        }

        #endregion
    }
}
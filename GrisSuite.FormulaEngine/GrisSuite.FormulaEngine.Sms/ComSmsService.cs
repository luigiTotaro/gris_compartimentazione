namespace GrisSuite.FormulaEngine.Sms.ComSmsService
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://mgwc.gsmr.rfi.it/Services/base/2007-09/ComSmsService.wsdl", ConfigurationName="IComSmsType")]
    public interface IComSmsType
    {
        
        // CODEGEN: Generating message contract since the operation SmSubmit is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://mgwc.gsmr.rfi.it/Services/base/2007-09/ComSmsService.wsdl/IComSmsType/SmSu" +
            "bmit", ReplyAction="http://mgwc.gsmr.rfi.it/Services/base/2007-09/ComSmsService.wsdl/IComSmsType/SmSu" +
            "bmitResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SmSubmitResponse SmSubmit(SmSubmitRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class ParSmSubmitRequestType
    {
        
        private string applicationIdField;
        
        private string[] destinationAddressesField;
        
        private string sourceAddressField;
        
        private object itemField;
        
        private ItemChoiceType itemElementNameField;
        
        private string profileField;
        
        private uint timeToLiveField;
        
        private bool timeToLiveFieldSpecified;
        
        private bool reportRequestField;
        
        private bool reportRequestFieldSpecified;
        
        private string authenticationCodeOriginatorField;
        
        private string notificationAddressField;
        
        private NotificationTypeType notificationTypeField;
        
        private bool notificationTypeFieldSpecified;
        
        private PidValueType notificationPidValueField;
        
        private bool notificationPidValueFieldSpecified;
        
        private ParSmSubmitRequestTypeLastResortAddressRequest lastResortAddressRequestField;
        
        private PidValueType lRadPidValueField;
        
        private bool lRadPidValueFieldSpecified;
        
        private ParSmSubmitRequestTypeDeferredDeliveryRequest deferredDeliveryRequestField;
        
        private string validityPeriodField;
        
        private string replacePidValueField;
        
        private bool moreMessageToSendField;
        
        private bool moreMessageToSendFieldSpecified;
        
        private bool priorityRequestedField;
        
        private bool priorityRequestedFieldSpecified;
        
        private string messageClassField;
        
        private ReplyPathType replyPathField;
        
        private bool replyPathFieldSpecified;
        
        private OriginatorTypeOdAddressType originatorTypeOfAddressField;
        
        private bool originatorTypeOfAddressFieldSpecified;
        
        private string extraServicesField;
        
        /// <remarks/>
        public string ApplicationId
        {
            get
            {
                return this.applicationIdField;
            }
            set
            {
                this.applicationIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("DestinationAddress", IsNullable=false)]
        public string[] DestinationAddresses
        {
            get
            {
                return this.destinationAddressesField;
            }
            set
            {
                this.destinationAddressesField = value;
            }
        }
        
        /// <remarks/>
        public string SourceAddress
        {
            get
            {
                return this.sourceAddressField;
            }
            set
            {
                this.sourceAddressField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AlphanumericMessage", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("NumericMessage", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("TransparentDataMessage", typeof(TransparentDataMessageType))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemChoiceType ItemElementName
        {
            get
            {
                return this.itemElementNameField;
            }
            set
            {
                this.itemElementNameField = value;
            }
        }
        
        /// <remarks/>
        public string Profile
        {
            get
            {
                return this.profileField;
            }
            set
            {
                this.profileField = value;
            }
        }
        
        /// <remarks/>
        public uint TimeToLive
        {
            get
            {
                return this.timeToLiveField;
            }
            set
            {
                this.timeToLiveField = value;
                this.TimeToLiveSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TimeToLiveSpecified
        {
            get
            {
                return this.timeToLiveFieldSpecified;
            }
            set
            {
                this.timeToLiveFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public bool ReportRequest
        {
            get
            {
                return this.reportRequestField;
            }
            set
            {
                this.reportRequestField = value;
                this.ReportRequestSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReportRequestSpecified
        {
            get
            {
                return this.reportRequestFieldSpecified;
            }
            set
            {
                this.reportRequestFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string AuthenticationCodeOriginator
        {
            get
            {
                return this.authenticationCodeOriginatorField;
            }
            set
            {
                this.authenticationCodeOriginatorField = value;
            }
        }
        
        /// <remarks/>
        public string NotificationAddress
        {
            get
            {
                return this.notificationAddressField;
            }
            set
            {
                this.notificationAddressField = value;
            }
        }
        
        /// <remarks/>
        public NotificationTypeType NotificationType
        {
            get
            {
                return this.notificationTypeField;
            }
            set
            {
                this.notificationTypeField = value;
                this.NotificationTypeSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NotificationTypeSpecified
        {
            get
            {
                return this.notificationTypeFieldSpecified;
            }
            set
            {
                this.notificationTypeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public PidValueType NotificationPidValue
        {
            get
            {
                return this.notificationPidValueField;
            }
            set
            {
                this.notificationPidValueField = value;
                this.NotificationPidValueSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NotificationPidValueSpecified
        {
            get
            {
                return this.notificationPidValueFieldSpecified;
            }
            set
            {
                this.notificationPidValueFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public ParSmSubmitRequestTypeLastResortAddressRequest LastResortAddressRequest
        {
            get
            {
                return this.lastResortAddressRequestField;
            }
            set
            {
                this.lastResortAddressRequestField = value;
            }
        }
        
        /// <remarks/>
        public PidValueType LRadPidValue
        {
            get
            {
                return this.lRadPidValueField;
            }
            set
            {
                this.lRadPidValueField = value;
                this.LRadPidValueSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LRadPidValueSpecified
        {
            get
            {
                return this.lRadPidValueFieldSpecified;
            }
            set
            {
                this.lRadPidValueFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public ParSmSubmitRequestTypeDeferredDeliveryRequest DeferredDeliveryRequest
        {
            get
            {
                return this.deferredDeliveryRequestField;
            }
            set
            {
                this.deferredDeliveryRequestField = value;
            }
        }
        
        /// <remarks/>
        public string ValidityPeriod
        {
            get
            {
                return this.validityPeriodField;
            }
            set
            {
                this.validityPeriodField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
        public string ReplacePidValue
        {
            get
            {
                return this.replacePidValueField;
            }
            set
            {
                this.replacePidValueField = value;
            }
        }
        
        /// <remarks/>
        public bool MoreMessageToSend
        {
            get
            {
                return this.moreMessageToSendField;
            }
            set
            {
                this.moreMessageToSendField = value;
                this.MoreMessageToSendSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MoreMessageToSendSpecified
        {
            get
            {
                return this.moreMessageToSendFieldSpecified;
            }
            set
            {
                this.moreMessageToSendFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public bool PriorityRequested
        {
            get
            {
                return this.priorityRequestedField;
            }
            set
            {
                this.priorityRequestedField = value;
                this.PriorityRequestedSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PriorityRequestedSpecified
        {
            get
            {
                return this.priorityRequestedFieldSpecified;
            }
            set
            {
                this.priorityRequestedFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
        public string MessageClass
        {
            get
            {
                return this.messageClassField;
            }
            set
            {
                this.messageClassField = value;
            }
        }
        
        /// <remarks/>
        public ReplyPathType ReplyPath
        {
            get
            {
                return this.replyPathField;
            }
            set
            {
                this.replyPathField = value;
                this.ReplyPathSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReplyPathSpecified
        {
            get
            {
                return this.replyPathFieldSpecified;
            }
            set
            {
                this.replyPathFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public OriginatorTypeOdAddressType OriginatorTypeOfAddress
        {
            get
            {
                return this.originatorTypeOfAddressField;
            }
            set
            {
                this.originatorTypeOfAddressField = value;
                this.OriginatorTypeOfAddressSpecified = true;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OriginatorTypeOfAddressSpecified
        {
            get
            {
                return this.originatorTypeOfAddressFieldSpecified;
            }
            set
            {
                this.originatorTypeOfAddressFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string ExtraServices
        {
            get
            {
                return this.extraServicesField;
            }
            set
            {
                this.extraServicesField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class TransparentDataMessageType
    {
        
        private string messageField;
        
        private string numberOfBitField;
        
        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
        public string NumberOfBit
        {
            get
            {
                return this.numberOfBitField;
            }
            set
            {
                this.numberOfBitField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class ErrorType
    {
        
        private string codeField;
        
        private string messageField;
        
        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
        
        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class IdElementListType
    {
        
        private IdElementType[] idElementField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IdElement")]
        public IdElementType[] IdElement
        {
            get
            {
                return this.idElementField;
            }
            set
            {
                this.idElementField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class IdElementType
    {
        
        private string sessionIdField;
        
        private string destinationAddressField;
        
        /// <remarks/>
        public string SessionId
        {
            get
            {
                return this.sessionIdField;
            }
            set
            {
                this.sessionIdField = value;
            }
        }
        
        /// <remarks/>
        public string DestinationAddress
        {
            get
            {
                return this.destinationAddressField;
            }
            set
            {
                this.destinationAddressField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class ParSmSubmitResponseType
    {
        
        private object itemField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Error", typeof(ErrorType))]
        [System.Xml.Serialization.XmlElementAttribute("IdElementList", typeof(IdElementListType))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes", IncludeInSchema=false)]
    public enum ItemChoiceType
    {
        
        /// <remarks/>
        AlphanumericMessage,
        
        /// <remarks/>
        NumericMessage,
        
        /// <remarks/>
        TransparentDataMessage,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public enum NotificationTypeType
    {
        
        /// <remarks/>
        Default,
        
        /// <remarks/>
        Delivery,
        
        /// <remarks/>
        NotDelivery,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Delivery-NotDelivery")]
        DeliveryNotDelivery,
        
        /// <remarks/>
        Buffered,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Buffered-Delivery")]
        BufferedDelivery,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("Buffered-NotDelivery")]
        BufferedNotDelivery,
        
        /// <remarks/>
        All,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public enum PidValueType
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0100")]
        Item0100,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0122")]
        Item0122,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0131")]
        Item0131,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0138")]
        Item0138,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0139")]
        Item0139,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0339")]
        Item0339,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0439")]
        Item0439,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("0539")]
        Item0539,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class ParSmSubmitRequestTypeLastResortAddressRequest
    {
        
        private string lastResortAddressField;
        
        /// <remarks/>
        public string LastResortAddress
        {
            get
            {
                return this.lastResortAddressField;
            }
            set
            {
                this.lastResortAddressField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public partial class ParSmSubmitRequestTypeDeferredDeliveryRequest
    {
        
        private string deferredDeliveryTimeField;
        
        /// <remarks/>
        public string DeferredDeliveryTime
        {
            get
            {
                return this.deferredDeliveryTimeField;
            }
            set
            {
                this.deferredDeliveryTimeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public enum ReplyPathType
    {
        
        /// <remarks/>
        request,
        
        /// <remarks/>
        response,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes")]
    public enum OriginatorTypeOdAddressType
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1139")]
        Item1139,
        
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("5039")]
        Item5039,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SmSubmitRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes", Order=0)]
        public ParSmSubmitRequestType ParSmSubmitRequest;
        
        public SmSubmitRequest()
        {
        }
        
        public SmSubmitRequest(ParSmSubmitRequestType ParSmSubmitRequest)
        {
            this.ParSmSubmitRequest = ParSmSubmitRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SmSubmitResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://mgwc.gsmr.rfi.it/Schemas/base/2007-09/ComSmsServiceTypes", Order=0)]
        public ParSmSubmitResponseType ParSmSubmitResponse;
        
        public SmSubmitResponse()
        {
        }
        
        public SmSubmitResponse(ParSmSubmitResponseType ParSmSubmitResponse)
        {
            this.ParSmSubmitResponse = ParSmSubmitResponse;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IComSmsTypeChannel : IComSmsType, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ComSmsTypeClient : System.ServiceModel.ClientBase<IComSmsType>, IComSmsType
    {
        
        public ComSmsTypeClient()
        {
        }
        
        public ComSmsTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName)
        {
        }
        
        public ComSmsTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress)
        {
        }
        
        public ComSmsTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress)
        {
        }
        
        public ComSmsTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SmSubmitResponse IComSmsType.SmSubmit(SmSubmitRequest request)
        {
            return base.Channel.SmSubmit(request);
        }
        
        public ParSmSubmitResponseType SmSubmit(ParSmSubmitRequestType ParSmSubmitRequest)
        {
            SmSubmitRequest inValue = new SmSubmitRequest();
            inValue.ParSmSubmitRequest = ParSmSubmitRequest;
            SmSubmitResponse retVal = ((IComSmsType)(this)).SmSubmit(inValue);
            return retVal.ParSmSubmitResponse;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Mail
{
    public class MailExecutor : IOperationExecutor
    {
        private string TemplateFilePath
        {
            get
            {
                string path = ConfigurationManager.AppSettings["MailTemplateFilePath"];
                return string.IsNullOrEmpty(path) ? Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\mail_template.txt" : path;
            }
        }

        private string EmailFrom
        {
            get { return ConfigurationManager.AppSettings["EmailFrom"] ?? ""; }
        }

        private string EmailSubject
        {
            get { return ConfigurationManager.AppSettings["EmailSubject"] ?? ""; }
        }

        private string EmailSmtpServer
        {
            get { return ConfigurationManager.AppSettings["EmailSmtpServer"] ?? ""; }
        }

        private int EmailSmtpServerPort
        {
            get
            {
                int serverPort;

                if (!int.TryParse(ConfigurationManager.AppSettings["EmailSmtpServerPort"] ?? "25", out serverPort))
                {
                    serverPort = 25;
                }

                return serverPort;
            }
        }

        private bool EmailSmtpServerEnableSsl
        {
            get
            {
                bool enableSsl;

                if (!bool.TryParse(ConfigurationManager.AppSettings["EmailSmtpServerEnableSsl"] ?? "false", out enableSsl))
                {
                    enableSsl = false;
                }

                return enableSsl;
            }
        }

        private SmtpDeliveryMethod EmailSmtpServerDeliveryMethod
        {
            get
            {
                SmtpDeliveryMethod deliveryMethod;

                if (
                    !Enum.TryParse(ConfigurationManager.AppSettings["EmailSmtpServerDeliveryMethod"] ?? "Network", true, out deliveryMethod))
                {
                    deliveryMethod = SmtpDeliveryMethod.Network;
                }

                return deliveryMethod;
            }
        }

        private bool EmailSmtpServerUseDefaultCredentials
        {
            get
            {
                bool useDefaultCredentials;

                if (
                    !bool.TryParse(ConfigurationManager.AppSettings["EmailSmtpServerUseDefaultCredentials"] ?? "false",
                        out useDefaultCredentials))
                {
                    useDefaultCredentials = false;
                }

                return useDefaultCredentials;
            }
        }

        private string EmailAccount
        {
            get { return ConfigurationManager.AppSettings["EmailAccount"] ?? ""; }
        }

        private string EmailPassword
        {
            get { return ConfigurationManager.AppSettings["EmailPassword"] ?? ""; }
        }

        #region Implementation of IOperationExecutor

        /// <summary>
        ///     Esegue un operazione passando le informazioni di stato di cui l'operazione ha bisogno.
        /// </summary>
        /// <param name="operationSchedule">Entità che rappresenta l'operazione da eseguire.</param>
        /// <param name="logger">Istanza logger</param>
        /// <returns></returns>
        public OperationScheduleExecResult Exec(IOperationSchedule operationSchedule, NLog.Logger logger)
        {
            // 29/07/2013 Implementazione non generica: cablata sulle restrizioni attuali della GUI.
            var state = operationSchedule.Parameters;
            if (state == null)
            {
                throw new ArgumentException("Il parametro 'state' non è inizializzato correttamente.", "operationSchedule");
            }
            if (!state.ContainsKey("objectStatusId") || string.IsNullOrEmpty(state["objectStatusId"]))
            {
                throw new ArgumentException("Impossibile inviare l'email: \"objectStatusId\" non inizializzato correttamente.", "operationSchedule");
            }
            if (!state.ContainsKey("to") || string.IsNullOrEmpty(state["to"]))
            {
                throw new ArgumentException("Impossibile inviare l'email: \"to\" non inizializzato correttamente.", "operationSchedule");
            }
            if (!state.ContainsKey("message") || string.IsNullOrEmpty(state["message"]))
            {
                throw new ArgumentException("Impossibile inviare l'email: \"message\" non inizializzato correttamente.", "operationSchedule");
            }

            string objectStatusId = state["objectStatusId"];
            string to = state["to"]; ;
            string message = state["message"];
            var sbMessage = new StringBuilder();
            OperationScheduleExecResult result;

            var templates = new MessageTemplates(this.TemplateFilePath);
            sbMessage.AppendFormat(templates.HeaderTemplate);

            sbMessage.AppendFormat(templates.BodyTemplate, operationSchedule.Created.ToShortDateString(),
                operationSchedule.Created.ToShortTimeString(), message);

            sbMessage.AppendFormat(templates.FooterTemplate);

            if (logger != null)
            {
                logger.Info("Invio mail a {0} message {1} ({2}) ", to, message, objectStatusId);
            }

            try
            {
                this.SendMail(this.EmailFrom, this.EmailSubject, sbMessage.ToString(),
                    to.Split(',') , this.EmailSmtpServer, this.EmailSmtpServerPort,
                    this.EmailSmtpServerEnableSsl, this.EmailSmtpServerDeliveryMethod, this.EmailSmtpServerUseDefaultCredentials,
                    this.EmailAccount, this.EmailPassword);

                result = new OperationScheduleExecResult() {Status = Common.OperationScheduleResultStatus.Success};
            }
            catch (Exception ex)
            {
                if (ex is SmtpException)
                {
                    result = new OperationScheduleExecResult();
                    result.Status = Common.OperationScheduleResultStatus.ShouldRetry;
                    result.InnerException = ex;
                    result.Message = ex.Message;
                    return result;
                }

                result = new OperationScheduleExecResult();
                result.Status = Common.OperationScheduleResultStatus.Error;
                result.InnerException = ex;
                result.Message = ex.Message;
                return result;
            }

            return result;
        }

        private void SendMail(string from,
            string subject,
            string body,
            IList<string> recipients,
            string smtpServer,
            int smtpServerPort,
            bool smtpServerEnableSsl,
            SmtpDeliveryMethod deliveryMethod,
            bool smtpServerUseDefaultCredentials,
            string account,
            string password)
        {
            if (string.IsNullOrEmpty(from))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato un mittente.", "from");
            }
            if (string.IsNullOrEmpty(subject))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato l'oggetto della mail.", "subject");
            }
            if (string.IsNullOrEmpty(body))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato un messaggio.", "body");
            }
            if (string.IsNullOrEmpty(recipients.FirstOrDefault()))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non sono stati specificati destinatari.", "recipients");
            }
            if (string.IsNullOrEmpty(smtpServer))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato un server smtp.", "smtpServer");
            }

            var msg = new MailMessage();
            foreach (var recipient in recipients)
            {
                msg.To.Add(recipient);
            }

            msg.Subject = subject;
            msg.From = new MailAddress(from);
            msg.Body = body;
            msg.IsBodyHtml = true;

            var smtp = new SmtpClient
            {
                Host = smtpServer,
                Port = smtpServerPort,
                EnableSsl = smtpServerEnableSsl,
                DeliveryMethod = deliveryMethod,
                UseDefaultCredentials = smtpServerUseDefaultCredentials,
                Credentials = new NetworkCredential(account, password)
            };

            smtp.Send(msg);
        }

        #endregion
    }

    public class MessageTemplates
    {
        public MessageTemplates(string filepath)
        {
            if (File.Exists(filepath))
            {
                using (var sr = File.OpenText(filepath))
                {
                    string fileContent = sr.ReadToEnd();
                    if (fileContent.Length > 0)
                    {
                        string[] templates = fileContent.Split('§');

                        if (templates.Length != 3)
                        {
                            throw new ApplicationException("Il file di definizione dei template di messaggio non definisce tutti i template.");
                        }

                        this.HeaderTemplate = templates[0];
                        this.BodyTemplate = templates[1];
                        this.FooterTemplate = templates[2];
                    }
                    else
                    {
                        throw new ApplicationException("Il file di definizione dei template di messaggio è vuoto.");
                    }
                }
            }
        }

        public string HeaderTemplate { get; private set; }
        public string BodyTemplate { get; private set; }
        public string FooterTemplate { get; private set; }
    }
}

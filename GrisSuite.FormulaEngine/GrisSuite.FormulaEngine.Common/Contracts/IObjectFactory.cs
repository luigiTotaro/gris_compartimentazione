﻿
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IObjectFactory
	{
		IObjectAttributeType GetAttributeType ();
		IObjectAttribute GetObjectAttribute ();
		IGrisObject GetGrisObject ();
	}
}

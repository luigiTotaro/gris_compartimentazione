﻿
using System.Collections;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IGrisAttributeCollection : IDictionary, INeedInitialization, IStorable
	{
		void SyncOriginalEntities ();
		void ForceAttribute ( string attributeName, dynamic value );
		void ResetAttribute ( string attributeName );
		IList<IObjectAttributeType> AttributeTypes { get; }
	}
}

﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IEvaluableObject
	{
		IEvaluationFormula DefaultEvaluationFormula { get; }
		SortedSet<IEvaluationFormula> CustomEvaluationFormulas { get; }
		IEvaluationContext EvaluationContext { get; set; }
		void Evaluate ();
		void Evaluate ( bool force );
		void ForceEvaluationOnDependentObjects();
		bool IsEvaluated { get; }
		bool IsSeverityEstabilished { get; }
		IEvaluationFormula EvaluatingFormula { get; set; }
		IEvaluableObject CallerObject { get; set; }
		Dictionary<string, object> EvaluationState { get; set; }
		IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<IObjectAttributeType> attributes, Dictionary<string, object> state );
		Dictionary<string, object> GetTypeEvaluationState ();
	}
}
﻿
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IGrisObject : IEvaluableObject, IStorable, INeedInitialization, INavigableObject<IGrisObject, Guid>
	{
		#region Primitive Properties

		/// <summary>
		/// Nome del GrisObject.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Identificativo del GrisObject specifico per tipo.
		/// </summary>
		[DataMember]
		long SpecificId { get; set; }

		/// <summary>
		/// Identificativo del tipo di GrisObject.
		/// </summary>
		[DataMember]
		int TypeId { get; set; }

		/// <summary>
		/// Severità dello stato.
		/// </summary>
		[DataMember]
		int SevLevel { get; set; }

		/// <summary>
		/// Severità di dettaglio dello stato.
		/// </summary>
		[DataMember]
		int? SevLevelDetail { get; set; }

		/// <summary>
		/// Identificativo del GrisObject padre.
		/// </summary>
		[DataMember]
		Guid? ParentId { get; set; }

		/// <summary>
		/// Indica se è in mantenimento.
		/// </summary>
		[DataMember]
		bool InMaintenance { get; set; }

		/// <summary>
		/// Severità dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		[DataMember]
		int? SevLevelReal { get; set; }

		/// <summary>
		/// Severità di dettaglio dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		[DataMember]
		int? SevLevelDetailReal { get; set; }

		/// <summary>
		/// Ultima severità dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		[DataMember]
		int? SevLevelLast { get; set; }

		/// <summary>
		/// Ultima severità di dettaglio dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		[DataMember]
		int? SevLevelDetailLast { get; set; }

		/// <summary>
		/// Stamp dell' ultimo aggiornamento.
		/// </summary>
		[DataMember]
		Guid? LastUpdateGuid { get; set; }

		/// <summary>
		/// Indica se il GrisObject è escluso dal calcolo dello stato del padre.
		/// </summary>
		[DataMember]
		bool ExcludeFromParentStatus { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[DataMember]
		bool ForcedByUser { get; set; }

        /// <summary>
        /// Indica se il GrisObject è in un particolare stato forzato.
        /// </summary>
        [DataMember]
        byte? ForcedSevLevel { get; set; }

        /// <summary>
        /// Indica se la notifica SMS è abilitata per quest'oggetto.
        /// </summary>
        [DataMember]
        bool IsSmsNotificationEnabled { get; set; }

        /// <summary>
        /// Indica se ci sono contatti per la notifica associati quest'oggetto
        /// </summary>
        [DataMember]
        bool HasNotificationContacts { get; set; }


		#endregion

		#region Builtin Severity properties

		int Severity { get; set; }

		int ComparableSeverity { get; }

		ObjectSeverityDetail SeverityDetail { get; set; }

		bool ExcludedFromParentStatus { get; set; }

		Color Color { get; set; }

		void ForceSeverity ( int severity );

		void ForceSeverity ( ObjectSeverityDetail severityDetail );

		#endregion

		#region Initialization

		IDictionary<string, object> InitializationValues { get; }

		#endregion

		#region Navigation members

		IGrisObject Parent { get; set; }

		IEntityCollection<IGrisObject> Children { get; }

		IGrisObject GetChildByName ( string name );

		T GetChildByName<T> ( string name ) where T : IGrisObject;

		IList<IGrisObject> GetChildrenByName ( string name );

		IList<T> GetChildrenByName<T> ( string name ) where T : IGrisObject;

		IGrisAttributeCollection Attributes { get; set; }

        IEntityCollection<IVirtualObject> VirtualObjects { get; set; }

        IEntityCollection<IGrisObject> Related { get; }

	    IList<IDevice> GetRelatedDevicesOfTypes (params string[] types);

		#endregion

		#region Cache

		IObjectCache<IGrisObject, Guid> ObjectCache { get; }

		bool LookupInCacheOnly { get; }

		#endregion 

        #region Utility
        bool? HasChildrenThatShouldSendEmail { get; set; }
        #endregion
    }
}

﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IStorable
	{
		/// <summary>
		/// Indica se l'oggetto ha modifiche da persistere.
		/// </summary>
		bool HasChanges { get; }

		/// <summary>
		/// Lista dei figli da persistere in db.
		/// </summary>
		IList<IStorable> StorableNodes { get; }

		/// <summary>
		/// Comando da lanciare per la persistenza in db.
		/// </summary>
		/// <returns></returns>
		string GetUpdateCommand ();
	}
}

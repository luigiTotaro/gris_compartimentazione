﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface INavigableObject<TEntity, TKey>
	{
		TKey Id { get; set; }

		bool IsNavigationActive { get; set; }
	
		TEntity GetCachedParent ( string propertyName );

		List<TEntity> GetCachedChildren ( string propertyName );
	}
}

﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	/// <summary>
	/// Interfaccia che definisce i membri di classi che eseguono un'operazione generica.
	/// </summary>
	public interface IOperationExecutor
	{
		/// <summary>
		/// Esegue un operazione passando le informazioni di stato di cui l'operazione ha bisogno.
		/// </summary>
        /// <param name="operationSchedule">Entità che rappresenta l'operazione da eseguire.</param>
		/// <param name="logger">Istanza logger</param>
		/// <returns></returns>
		OperationScheduleExecResult Exec ( Entities.IOperationSchedule operationSchedule, NLog.Logger logger);
	}

	/// <summary>
	/// Interfaccia che definisce i membri di classi che possono schedulare operazioni per un'invocazione successiva.
	/// </summary>
	public interface IOperationScheduler
	{
	    /// <summary>
	    /// Schedula un operazione passando le informazioni di stato di cui l'operazione ha bisogno ed indicando se va tentata anche un esecuzione sincrona.
	    /// </summary>
	    /// <param name="scheduleType">Tipo di schedulazione, serve per invocare l'executor corretto</param>
	    /// <param name="state">Stato per l'operazione.</param>
	    /// <param name="directExecution">Indica se lo scheduler deve tentare un'esecuzione sincrona prima di schedulare.</param>
        /// <param name="lastOperationOlderThanMinutes">Minuti che devono trascorrere dall'ultima operazione schedulata dello stesso tipo.</param>
	    /// <param name="logger">Istanza logger</param>
	    /// <param name="operationKeys">Chiavi da usare per poter identificare in modo univoco l'operazione</param>
        /// <param name="operationScheduleCheckType">Tipo di controllo per l'esistenza di una schedulazione analoga, all'interno della finestra temporale</param>
	    /// <returns></returns>
        ScheduleResult Schedule(string scheduleType, Dictionary<string, string> state = null, bool directExecution = false, int lastOperationOlderThanMinutes = 0, NLog.Logger logger = null, string operationKeys = "", OperationScheduleCreateIfCheckType operationScheduleCheckType = OperationScheduleCreateIfCheckType.ByParameters);
	}

    /// <summary>
    /// Tipo di controllo per l'esistenza di una schedulazione analoga, all'interno della finestra temporale
    /// </summary>
    public enum OperationScheduleCreateIfCheckType
    {
        // Controllo sui parametri della schedulazione
        ByParameters,
        // Controllo sulle chiavi univoche della schedulazione
        ByKeys,
        // Controllo sulle chiavi univoche e i parametri della schedulazione
        ByParametersAndKeys
    }

	/// <summary>
	/// Classe che rappresenta il risultato di una schedulazione di operazione.
	/// </summary>
	public class ScheduleResult
	{
		private readonly Guid _scheduleId;
		private readonly bool _success;

		public ScheduleResult ( Guid scheduleId, bool success )
		{
			_scheduleId = scheduleId;
			_success = success;
		}

		/// <summary>
		/// Indica se la schedulazione è stata eseguita correttamente
		/// </summary>
		public bool Success
		{
			get { return _success; }
		}

		/// <summary>
		/// Identificativo della schedulazione.
		/// </summary>
		public Guid ScheduleId
		{
			get { return _scheduleId; }
		}
	}
}

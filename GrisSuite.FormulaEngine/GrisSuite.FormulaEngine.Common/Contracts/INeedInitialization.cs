﻿
namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface INeedInitialization
	{
		void Initialize();
		bool IsInitialized { get; }
	}
}

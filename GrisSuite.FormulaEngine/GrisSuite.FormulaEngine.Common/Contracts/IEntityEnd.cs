﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Severities;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IEntityCollection<TEntity> : ICollection<TEntity>, IEntityEnd<TEntity> where TEntity : class, IGrisObject
	{
		/// <summary>
		/// Aggiunge un entità alla lista delle entità figlie, indicando che si tratta di figlie dirette.
		/// </summary>
		/// <param name="entity">Entità da aggiungere.</param>
		/// <param name="isDirectChild">Indica se l'entità da aggiungere è figlia diretta di quella padre nella gerarchia.</param>
		void AddEntity ( TEntity entity, bool isDirectChild = true );

		#region Aggregation Methods

		#region Get aggregation methods

		/// <summary>
		/// Restituisce la lista di entità risultante dalle esclusioni.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <param name="exclusionsByDetail">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>Lista di entità risultante dalle esclusioni.</returns>
		List<TEntity> GetExcept ( bool withExcludedFromParentStatus = false, int[] exclusions = null, ObjectSeverityDetail[] exclusionsByDetail = null );

		/// <summary>
		/// Restituisce la lista di entità in una data severità.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		List<TEntity> GetInSeverity ( int severity, bool withExcludedFromParentStatus = false,
												 params int[] exclusions );

		/// <summary>
		/// Restituisce la lista di entità in una data severità di dettaglio.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		List<TEntity> GetInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false,
												 params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Restituisce la lista di entità in una data severità o peggiore.
		/// </summary>
		/// <param name="severity">Severità in cui devono almeno essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		List<TEntity> GetAtLeastInSeverity ( int severity, bool withExcludedFromParentStatus = false,
		                                         params int[] exclusions );

		/// <summary>
		/// Restituisce la lista di entità in una data severità di dettaglio o peggiore.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono almeno essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		List<TEntity> GetAtLeastInSeverity ( ObjectSeverityDetail severityDetail,
		                                         bool withExcludedFromParentStatus = false,
		                                         params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Restituisce la severità peggiore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		int GetWorstSeverity ( bool withExcludedFromParentStatus = false, params int[] exclusions );

		/// <summary>
		/// Restituisce la severità di dettaglio peggiore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità di dettaglio contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		ObjectSeverityDetail GetWorstSeverityDetail ( bool withExcludedFromParentStatus = false,
		                                              params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Restituisce la severità migliore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		int GetBestSeverity ( bool withExcludedFromParentStatus = false, params int[] exclusions );

		/// <summary>
		/// Restituisce la severità di dettaglio migliore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità di dettaglio contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		ObjectSeverityDetail GetBestSeverityDetail ( bool withExcludedFromParentStatus = false,
		                                                     params ObjectSeverityDetail[] exclusions );

		#endregion

		#region Boolean aggregation methods

		/// <summary>
		/// Indica se esiste almeno un'entità nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		bool AnyInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions );

		/// <summary>
		/// Indica se esiste almeno un'entità nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		bool AnyInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false,
										   params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Indica se esiste un'entità che sia almeno nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		bool AnyAtLeastInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions );

		/// <summary>
		/// Indica se esiste un'entità che sia almeno nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		bool AnyAtLeastInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false,
		                                   params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Indica se tutte le entità sono nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		bool AreAllInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions );

		/// <summary>
		/// Indica se tutte le entità sono nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		bool AreAllInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false,
										   params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Indica se tutte le entità sono almeno nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		bool AreAllAtLeastInSeverity ( int severity, bool withExcludedFromParentStatus = false,
		                                              params int[] exclusions );

		/// <summary>
		/// Indica se tutte le entità sono almeno nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		bool AreAllAtLeastInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false,
		                                       params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Indica se la data percentuale di entità è nella severità passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità passata.</param>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		bool AreXPercInSeverity ( float percentage, int severity, bool withExcludedFromParentStatus = false );

		/// <summary>
		/// Indica se la data percentuale di entità è nella severità di dettaglio passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità di dettaglio passata.</param>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		bool AreXPercInSeverity ( float percentage, ObjectSeverityDetail severityDetail,
		                                  bool withExcludedFromParentStatus = false );

		/// <summary>
		/// Indica se la data percentuale di entità è almeno nella severità passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità passata.</param>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		bool AreXPercAtLeastInSeverity ( float percentage, int severity, bool withExcludedFromParentStatus = false,
		                                         params int[] exclusions );

		/// <summary>
		/// Indica se la data percentuale di entità è almeno nella severità di dettaglio passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità di dettaglio passata.</param>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		bool AreXPercAtLeastInSeverity ( float percentage, ObjectSeverityDetail severityDetail,
		                                         bool withExcludedFromParentStatus = false,
		                                         params ObjectSeverityDetail[] exclusions );

		/// <summary>
		/// Indica se la collezione contiene entità non 'escluse dal calcolo del padre'.
		/// </summary>
		/// <returns>Vero se ne contiene, Falso altrimenti.</returns>
		bool HasNotExcluded ();

		#endregion

		#endregion
	}

	public interface IEntityEnd<out TEntity> where TEntity : IGrisObject
	{
		/// <summary>
		/// Carica la/le entità correlate.
		/// </summary>
		/// <param name="lookupInCacheOnly">Indica se caricare la/le entità solo dalla cache o, qualora non presenti, anche in database.</param>
		void Load ( bool lookupInCacheOnly = false );

		/// <summary>
		/// Indica se la/le entità sono state caricate.
		/// </summary>
		bool IsLoaded { get; }

		/// <summary>
		/// Restituisce l'entità nel caso di una relazione 'padre'.
		/// </summary>
		TEntity Value { get; }

		/// <summary>
		/// Trasforma un gruppo di entità in entità di altro tipo.
		/// </summary>
		/// <typeparam name="TNewEntity">Nuovo tipo in cui devono essere trasformate le entità.</typeparam>
		/// <returns></returns>
		IEntityCollection<TNewEntity> Cast<TNewEntity> () where TNewEntity : class, IGrisObject;
	}
}

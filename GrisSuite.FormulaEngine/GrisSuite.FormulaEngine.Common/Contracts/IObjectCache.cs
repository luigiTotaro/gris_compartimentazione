
using System;
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IObjectCache<TEntity, TKey> where TEntity : INavigableObject<TEntity, TKey>
	{
		bool HasObject ( TKey id );
		TEntity Get ( TKey id );
		TEntity GetParent ( TEntity entity, string propertyName );
        List<TEntity> GetChildren ( TEntity entity, string propertyName );
		void AddObject ( TEntity obj );
		void AddObjects ( IEnumerable<TEntity> objects );
        void AddChildObjects ( TEntity parentObj, TEntity childObj );
        void AddRelatedObject ( TEntity obj, TEntity relatedObj );
        void AddRelatedObject ( TKey objId, TKey relatedObjId );
        void AddRelatedObjects ( IEnumerable<Tuple<TEntity, TEntity>> relatedObjs );
        void AddRelatedObjects ( IEnumerable<Tuple<TKey, TKey>> relatedObjIds );
		ICollection<TEntity> CachedObjects { get; }
		void Reset ();
	}
}
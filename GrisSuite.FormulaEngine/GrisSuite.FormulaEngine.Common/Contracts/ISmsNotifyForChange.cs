﻿
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface ISmsNotifyForChange
	{
		string GetSmsNotificationMessage ();

        string GetSmsNotificationKeys (string phoneNumber);

		string[] GetRecipientNumbers ();
		List<IContactEntry> GetNotificationRecipients ();
	}
}

﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Common.Contracts
{
	public interface IEvaluationContext
	{
		void Evaluate ( IEvaluableObject evalObject );
		void Evaluate ( IEvaluableObject evalObject, Dictionary<string, object> state );
	}
}
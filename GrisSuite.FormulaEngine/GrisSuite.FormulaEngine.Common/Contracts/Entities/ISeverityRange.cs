﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	/// <summary>
	/// Interfaccia di entità mappata alla tablela 'entity_state'.
	/// </summary>
	public interface ISeverityRange
	{
		/// <summary>
		/// Identificativo dell'entità.
		/// </summary>
		byte Id { get; set; }

		/// <summary>
		/// Nome dell'entità.
		/// </summary>
		string Name { get; set; }

		/// <summary>
		/// E' il peso della severità rispetto alle altre.
		/// </summary>
		decimal SeverityWeight { get; set; }

		/// <summary>
		/// Valore minimo della soglia.
		/// </summary>
		short MinValue { get; set; }

		/// <summary>
		/// Valore massimo della soglia.
		/// </summary>
		short MaxValue { get; set; }

		/// <summary>
		/// Restituisce la lista di tutte le soglie di severità.
		/// </summary>
		/// <returns></returns>
		IList<ISeverityRange> GetAll ();
	}
}

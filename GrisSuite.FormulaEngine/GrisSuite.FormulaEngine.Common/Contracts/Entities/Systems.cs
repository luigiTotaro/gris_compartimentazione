﻿
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	// nata come enum ma trasformata un struct per evitare cast espliciti durante l'utilizzo nelle formule
	[DataContract]
	public struct Systems
	{
		public const int DiffusioneSonora = 1;
		public const int InformazioneVisiva = 2;
		public const int SistemaIlluminazione = 3;
		public const int SistemaTelefonia = 4;
		public const int Rete = 5;
		public const int SistemaErogazioneEnergia = 6;
		public const int SistemaFDS = 8;
		public const int Diagnostica = 10;
		public const int Facilities = 11;
	    public const int MonitoraggioVPNVerde = 12;
	    public const int WebRadio = 13;
        public const int Orologi = 14;
        public const int Ascensori = 15;
		public const int AltrePeriferiche = 99;
	}
}

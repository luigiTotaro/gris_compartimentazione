﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IStreamHistory : IStream
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo del Messaggio ricevuto dal Collector.
		/// </summary>
		[DataMember]
		Guid LogId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza dello stream a cui appartiene lo StreamHistory.
		/// </summary>
		[XmlIgnore]
		[SoapIgnoreAttribute]
		[DataMember]
		IStream Stream { get; set; }

		#endregion
	}
}
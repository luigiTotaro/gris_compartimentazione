﻿
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IOperationScheduleType
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del tipo di schedulazione.
		/// </summary>
		short Id { get; set; }

		/// <summary>
		/// Codice univoco del tipo di schedulazione.
		/// </summary>
		string Code { get; set; }

		/// <summary>
		/// Nome dell' assembly contenente la classe per gestire l'esecuzione del tipo di operazioni schedulate.
		/// </summary>
		string AssemblyName { get; set; }

		#endregion

		#region Navigation Members

	    /// <summary>
	    /// Collezione di istanze di schedulazione.
	    /// </summary>
        List<IOperationSchedule> OperationSchedules { get; set; }

		#endregion
	}
}

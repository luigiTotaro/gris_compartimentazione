﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
    public interface IDeviceFilter
    {
        #region Primitive Properties

        /// <summary>
        /// Identificativo del filtro
        /// </summary>
        [DataMember]
        long DeviceFilterId { get; set; }

        #endregion
    }
}

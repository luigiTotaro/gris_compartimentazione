﻿
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IStreamFieldCase
	{
		#region Primitive Properties

		/// <summary>
		/// Valore del Case.
		/// </summary>
		[DataMember]
		string Value { get; set; }

		/// <summary>
		/// Severità come riportata dal collettore.
		/// </summary>
		[DataMember]
		int? ActualSevLevel { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza dello stream field a cui appartiene il case.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IStreamField StreamField { get; }

		#endregion
	}
}

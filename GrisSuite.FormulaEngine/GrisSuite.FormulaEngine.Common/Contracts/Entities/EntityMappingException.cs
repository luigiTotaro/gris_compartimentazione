﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	[Serializable]
	public class EntityMappingException : Exception
	{
		public EntityMappingException ()
		{
		}

		public EntityMappingException ( string message )
			: base(message)
		{
		}

		public EntityMappingException ( string message, Exception inner )
			: base(message, inner)
		{
		}

		protected EntityMappingException ( SerializationInfo info, StreamingContext context )
			: base(info, context)
		{
		}
	}
}

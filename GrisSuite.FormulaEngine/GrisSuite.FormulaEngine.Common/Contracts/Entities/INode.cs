﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface INode : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Nome della stazione.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Chilometrica associata alla stazione.
		/// </summary>
		[DataMember]
		int Meters { get; set; }

		/// <summary>
		/// Identificativo specifico della stazione.
		/// </summary>
		[DataMember]
		long NodeId { get; set; }

		/// <summary>
		/// Identificativo della linea a cui appartiene la stazione.
		/// </summary>
		[DataMember]
		Guid ZoneId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della linea a cui appartiene la stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IZone Zone { get; set; }

		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityEnd<IZone> ZoneReference { get; set; }

		/// <summary>
		/// Imposta la relazione con un oggetto Zone.
		/// </summary>
		/// <param name="zone">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		void SetZone ( IZone zone, bool isDirectChild );

		/// <summary>
		/// Istanze dei Sistemi presenti nella stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<INodeSystem> Systems { get; set; }

		/// <summary>
		/// Istanze dei Server presenti nella stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IServer> Servers { get; set; }

		/// <summary>
		/// Istanze delle Periferiche presenti nella stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IDevice> Devices { get; set; }

		#endregion
	}
}
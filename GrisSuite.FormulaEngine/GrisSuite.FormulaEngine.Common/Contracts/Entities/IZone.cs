﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IZone : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[DataMember]
		Guid RegionId { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[DataMember]
		Int64 ZoneId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IRegion Region { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityEnd<IRegion> RegionReference { get; set; }

		/// <summary>
		/// Imposta la relazione con un oggetto Region.
		/// </summary>
		/// <param name="region">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		void SetRegion ( IRegion region, bool isDirectChild );

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<INode> Nodes { get; set; }

		#endregion
	}
}
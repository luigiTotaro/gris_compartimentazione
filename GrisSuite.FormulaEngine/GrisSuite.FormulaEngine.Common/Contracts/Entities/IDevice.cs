﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IDevice : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Il nome della periferica.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Il tipo della periferica.
		/// </summary>
		[DataMember]
		string Type { get; set; }

		/// <summary>
		/// Numero Seriale della periferica.
		/// </summary>
		[DataMember]
		string SerialNumber { get; set; }

		/// <summary>
		/// Indirizzo IP o seriale.
		/// </summary>
		[DataMember]
		string Address { get; set; }

		/// <summary>
		/// Indica se la diagnostica è attiva.
		/// </summary>
		[DataMember]
		byte? IsActive { get; set; }

		/// <summary>
		/// Indica se è schedulata.
		/// </summary>
		[DataMember]
		byte? IsScheduled { get; set; }

		/// <summary>
		/// Versione della definizione.
		/// </summary>
		[DataMember]
		string DefinitionVersion { get; set; }

		/// <summary>
		/// Versione della definizione di protocollo.
		/// </summary>
		[DataMember]
		string ProtocolDefinitionVersion { get; set; }

		/// <summary>
		/// Severità restituita dalla periferia.
		/// </summary>
		[DataMember]
		int? ActualSevLevel { get; set; }

		/// <summary>
		/// Descrizione.
		/// </summary>
		[DataMember]
		string Description { get; set; }

		/// <summary>
		/// Indica se la periferica è offline.
		/// </summary>
		[DataMember]
		byte? IsOffline { get; set; }

        /// <summary>
        /// Descrizione della modalità di discovery del tipo di periferica.
        /// </summary>
        [DataMember]
        DiscoveryType DeviceTypeDiscovery { get; set; }

        /// <summary>
        /// Descrizione del tipo di periferica.
        /// </summary>
        [DataMember]
        string DeviceTypeDescription { get; set; }

		/// <summary>
		/// Url a cui risponde il Web Service che restituisce informazioni diagnostiche sulla periferica.
		/// </summary>
		[DataMember]
		string WSUrlPattern { get; set; }

		/// <summary>
		/// Identificativo di Tecnologia.
		/// </summary>
		[DataMember]
		int? TechnologyId { get; set; }

		/// <summary>
		/// Identificativo specifico di periferica.
		/// </summary>
		[DataMember]
		long? DeviceId { get; set; }

		/// <summary>
		/// Identificativo del Server.
		/// </summary>
		[DataMember]
		Guid? ServerId { get; set; }

		/// <summary>
		/// Identificativo del Sistema.
		/// </summary>
		[DataMember]
		Guid? NodeSystemId { get; set; }

		/// <summary>
		/// Identificativo della stazione.
		/// </summary>
		[DataMember]
		Guid? NodeId { get; set; }

		/// <summary>
		/// Identificativo della porta.
		/// </summary>
		[DataMember]
		Guid? PortId { get; set; }

        /// <summary>
        /// Indica se sulla periferica è settato un ack.
        /// </summary>
        [DataMember]
        bool IsAcknowledged { get; }

        /// <summary>
        /// Indica se deve essere inviata una notifica via email.
        /// </summary>
        bool ShouldSendNotificationByEmail { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza del Server che monitora la periferica.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IServer Server { get; set; }

		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityEnd<IServer> ServerReference { get; set; }

		/// <summary>
		/// Imposta la relazione con un oggetto Server.
		/// </summary>
		/// <param name="server">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		void SetServer ( IServer server, bool isDirectChild );

		/// <summary>
		/// Istanza del Sistema sotto cui è compresa la periferica.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		INodeSystem System { get; set; }

		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityEnd<INodeSystem> SystemReference { get; set; }

		/// <summary>
		/// Imposta la relazione con un oggetto NodeSystem.
		/// </summary>
		/// <param name="system">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		void SetSystem ( INodeSystem system, bool isDirectChild );

		/// <summary>
		/// Istanza della Stazione sotto cui è compresa la periferica.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		INode Node { get; set; }

		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityEnd<INode> NodeReference { get; set; }

		/// <summary>
		/// Imposta la relazione con un oggetto Node.
		/// </summary>
		/// <param name="node">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		void SetNode ( INode node, bool isDirectChild );

		/// <summary>
		/// Istanze degli streams della periferica.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IList<IStream> Streams { get; set; }

		/// <summary>
		/// Istanza della Porta della periferica.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IPort Port { get; set; }

		IList<IEvent> GetRaisedEvents ( DateTime dateFrom, DateTime dateTo );

		#endregion

	}

    public enum DiscoveryType : byte { Standard = 1, BaseType, DiscoveryType }
}
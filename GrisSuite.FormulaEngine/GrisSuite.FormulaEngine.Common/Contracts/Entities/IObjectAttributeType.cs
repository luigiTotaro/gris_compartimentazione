﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IObjectAttributeType
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dell' attributo.
		/// </summary>
		[DataMember]
		short Id { get; set; }

		/// <summary>
		/// Nome dell' attributo.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanze di Attributo concrete associate ai GrisObjects.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		List<IObjectAttribute> ObjectAttributes { get; set; }

		#endregion

		IList<IObjectAttributeType> GetAll ();

		IObjectAttributeType GetById ( short id );
	}
}

﻿
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IVirtualObject : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dell' oggetto specifico.
		/// </summary>
		[DataMember]
		long VirtualObjectId { get; set; }

		/// <summary>
		/// Identificativo dell' oggetto specifico.
		/// </summary>
		[DataMember]
		string Description { get; set; }

		#endregion
	}
}

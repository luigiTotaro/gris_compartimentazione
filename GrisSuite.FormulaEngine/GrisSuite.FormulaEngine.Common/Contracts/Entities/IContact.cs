﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
    public interface IContact
    {
        #region Primitive Properties

        /// <summary>
        /// Identificativo del contatto.
        /// </summary>
        long Id { get; set; }

        /// <summary>
        /// Nome del contatto.
        /// </summary>
        [DataMember]
        string Name { get; set; }

        /// <summary>
        /// Compartimento a cui si riferisce il contatto.
        /// </summary>
        [DataMember]
        long RegionId { get; set; }

        /// <summary>
        /// Indirizzo email del contatto.
        /// </summary>
        [DataMember]
        string Email { get; set; }

        /// <summary>
        /// Numero telefonico del contatto.
        /// </summary>
        [DataMember]
        string PhoneNumber { get; set; }

        #endregion
    }
}

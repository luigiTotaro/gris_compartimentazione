﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
    public interface IContactGroupEntry
    {
        #region Primitive Properties

        /// <summary>
        /// Identificativo del gruppo-contatto.
        /// </summary>
        [DataMember]
        long NotificationGroupContactId { get; set; }

        /// <summary>
        /// .....
        /// </summary>
        [DataMember]
        Guid? ObjectStatusId { get; set; }

        /// <summary>
        /// gruppo.
        /// </summary>
        [DataMember]
        string SmsGroupADName { get; set; }

        /// <summary>
        /// gruppo.
        /// </summary>
        [DataMember]
        string MailGroupADName { get; set; }
 

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
    /// <summary>
    /// Oggetto che rappresenta il risultato di esecuzione di una schedulazione
    /// </summary>
    public class OperationScheduleExecResult
    {
        #region Primitive Members

        /// <summary>
        /// Indica lo stato di ritorno di esecuzione di un'operazione schedulata
        /// </summary>
        public OperationScheduleResultStatus Status { get; set; }

        /// <summary>
        /// Parametri generici di ritorno dalla schedulazione.
        /// </summary>
        public Dictionary<string, string> Parameters { get; set; }

        /// <summary>
        /// Serializzazione stringa dei parametri, da salvare in base dati
        /// </summary>
        public string SerializedParameters
        {
            get
            {
                if ((this.Parameters != null) && (this.Parameters.Count > 0))
                {
                    return this.Serialize(this.Parameters);
                }

                return String.Empty;
            }
        }

        /// <summary>
        /// Eccezione interna all'ultima esecuzione
        /// </summary>
        public Exception InnerException { get; set; }

        /// <summary>
        /// Codice di ritorno dell'esecuzione
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Messaggio di ritorno dell'esecuzione
        /// </summary>
        public string Message { get; set; }

        #endregion

        /// <summary>
        /// Costruttore
        /// </summary>
        public OperationScheduleExecResult()
        {
            this.Status = OperationScheduleResultStatus.Unknown;
            this.Parameters = new Dictionary<string, string>();
            this.InnerException = null;
            this.Code = "-1";
            this.Message = null;
        }

        private string Serialize<T>(T obj)
        {
            var serializer = new DataContractSerializer(obj.GetType());
            using (var writer = new StringWriter())
            using (var stm = new XmlTextWriter(writer))
            {
                serializer.WriteObject(stm, obj);
                return writer.ToString();
            }
        }
    }
}
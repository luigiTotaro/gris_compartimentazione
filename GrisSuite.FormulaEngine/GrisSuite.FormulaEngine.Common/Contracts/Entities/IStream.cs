﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IStream
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dello Stream.
		/// </summary>
		[DataMember]
		int StreamId { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		[DataMember]
		long DevId { get; set; }

		/// <summary>
		/// Nome dello Stream.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Visibilità dello Stream.
		/// </summary>
		[DataMember]
		byte? Visible { get; set; }

		/// <summary>
		/// Dati binari dello Stream.
		/// </summary>
		[DataMember]
		byte[] Data { get; set; }

		/// <summary>
		/// Data di ricezione dello Stream.
		/// </summary>
		[DataMember]
		DateTime DateReceived { get; set; }

		/// <summary>
		/// Severità dello Stream come ricevuta dal collettore.
		/// </summary>
		[DataMember]
		int? ActualSevLevel { get; set; }

		/// <summary>
		/// Indica se lo Stream è stato processato.
		/// </summary>
		[DataMember]
		byte? Processed { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		[DataMember]
		Guid DeviceId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della periferica a cui appartiene lo stream.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IDevice Device { get; set; }

		/// <summary>
		/// Istanze degli stream fields dello stream.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		List<IStreamField> StreamFields { get; set; }

		/// <summary>
		/// Istanze Istanze degli streams dello StreamHistory.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		List<IStreamHistory> StreamsHistory { get; set; }

		#endregion
	}
}

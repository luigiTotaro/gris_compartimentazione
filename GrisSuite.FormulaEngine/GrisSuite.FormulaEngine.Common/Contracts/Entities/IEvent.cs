﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IEvent
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dell'evento.
		/// </summary>
		Guid Id { get; set; }
		/// <summary>
		/// Identificativo della periferica che ha inviato l'evento.
		/// </summary>
		Guid DeviceId { get; set; }
		/// <summary>
		/// Dati binari dell'evento
		/// </summary>
		byte[] Data { get; set; }
		/// <summary>
		/// Categoria dell'evento.
		/// </summary>
		byte Category { get; set; }
		/// <summary>
		/// Data di creazione del record.
		/// </summary>
		DateTime? Created { get; set; }
		/// <summary>
		/// Data di richiesta, registrata dalla device.
		/// </summary>
		DateTime Requested { get; set; }
		/// <summary>
		/// Data in cui l'evento viene registrato nel db centrale.
		/// </summary>
		DateTime Centralized { get; set; }
		/// <summary>
		/// Flag che indica un evento che dev'essere cancellato secondo logiche del Supervisor.
		/// </summary>
		bool ToBeDeleted { get; set; }

		byte? EventCode { get; set; }
		string EventDescription { get; set; }
		string EventDescriptionValue { get; set; }
		byte? InOutId { get; set; }
		string MicroId { get; set; }
		string SubEventDescription { get; set; }
		byte? FunctionDescription { get; set; }
		byte? DeviceCode { get; set; }

		// ad oggi restituiti sempre a nulla nella sp
		//string FunctionCode { get; set; }
		//string DeviceAddress { get; set; }


		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della periferica a cui appartiene l'evento.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IDevice Device { get; set; }

		#endregion
	}
}

﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface INodeSystem : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo specifico del NodeSystem.
		/// </summary>
		[DataMember]
		long? NodeSystemId { get; set; }

		/// <summary>
		/// Identificativo della stazione correlata al NodeSystem.
		/// </summary>
		[DataMember]
		Guid NodeId { get; set; }

		/// <summary>
		/// Identificativo del sistema.
		/// </summary>
		[DataMember]
		int SystemId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della stazione relativa al NodeSystem.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		INode Node { get; set; }

		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityEnd<INode> NodeReference { get; set; }

		/// <summary>
		/// Imposta la relazione con un oggetto Node.
		/// </summary>
		/// <param name="node">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		void SetNode ( INode node, bool isDirectChild );

		/// <summary>
		/// Istanze delle periferiche comprese nel NodeSystem.
		/// </summary>
		[XmlIgnore]
		[SoapIgnoreAttribute]
		[DataMember]
		IEntityCollection<IDevice> Devices { get; set; }

		#endregion
	}
}
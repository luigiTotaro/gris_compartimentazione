﻿
using System;
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IContactEntry
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo della voce di rubrica.
		/// </summary>
		[DataMember]
        long ContactId { get; set; }

		/// <summary>
		/// Voce associata al numero telefonico.
		/// </summary>
		[DataMember]
        string Name { get; set; }

		/// <summary>
		/// Numero telefonico.
		/// </summary>
		[DataMember]
		string PhoneNumber { get; set; }

        /// <summary>
        /// eMail.
        /// </summary>
        [DataMember]
        string Email { get; set; }

        /// <summary>
        /// Manda una eMail a questo contatto per almeno una delle periferiche associate a questo contatto
        /// </summary>
        [DataMember]
        bool SendEMail { get; set; }

        /// <summary>
        /// Manda un sms a questo contatto per almeno una delle periferiche associate a questo contatto
        /// </summary>
        [DataMember]
        bool SendSms { get; set; }

        #endregion
	}
}

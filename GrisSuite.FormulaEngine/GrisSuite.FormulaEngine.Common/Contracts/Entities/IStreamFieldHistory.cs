﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IStreamFieldHistory : IStreamField
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo del Messaggio ricevuto dal Collector.
		/// </summary>
		[DataMember]
		Guid LogId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza dello stream field a cui appartiene lo StreamFieldHistory.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IStreamField StreamField { get; set; }

		#endregion
	}
}
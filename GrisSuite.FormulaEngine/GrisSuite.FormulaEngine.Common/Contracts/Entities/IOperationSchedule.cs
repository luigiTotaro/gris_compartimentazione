﻿
using System;
using System.Collections.Generic;

namespace GrisSuite.FormulaEngine.Common.Contracts.Entities
{
	public interface IOperationSchedule
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo della schedulazione.
		/// </summary>
		Guid Id { get; set; }

		/// <summary>
		/// Identificativo del tipo di schedulazione.
		/// </summary>
		short TypeId { get; set; }

		/// <summary>
		/// Parametri per l'esecuzione della schedulazione.
		/// </summary>
		Dictionary<string, string> Parameters { get; set; }

		/// <summary>
		/// Data di creazione della schedulazione.
		/// </summary>
		DateTime Created { get; set; }

		/// <summary>
		/// Data di esecuzione dell'operazione.
		/// </summary>
		DateTime? Executed { get; set; }

		/// <summary>
		/// Data di annullamento dell'operazione.
		/// </summary>
		DateTime? Aborted { get; set; }

        /// <summary>
        /// Chiavi da usare per poter identificare in modo univoco l'operazione
        /// </summary>
        string OperationKeys { get; set; }

		#endregion

		#region Navigation Members

	    /// <summary>
	    /// Istanza del tipo di schedulazione.
	    /// </summary>
	    IOperationScheduleType Type { get; set; }

		#endregion
	}
}

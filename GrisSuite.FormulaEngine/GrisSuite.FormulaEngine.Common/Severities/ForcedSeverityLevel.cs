﻿
namespace GrisSuite.FormulaEngine.Common.Severities
{
	public struct ForcedSeverityLevel
	{
		public const int NotForced = 0;
		public const int AtLeastOnePZInError = 1;
		public const int STLC1000ServerNotReachable = 2;
		public const int AllChildrenAreNotReachable = 4;
		public const int IncompleteServerData = 8;
	}
}

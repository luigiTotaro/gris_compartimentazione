﻿
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Common.Severities
{
    /// <summary>
    ///   Rappresenta la severità dei valori ricevuti
    /// </summary>
	[DataContract]
	public static class Severity
    {
		/// <summary>
		///   Valore informativo (valido solo per stream e stream field)
		/// </summary>
		[DataMember]
		public static readonly int Info = -255;

    	/// <summary>
    	///   Non classificato
    	/// </summary>
		[DataMember]
		public static readonly int NotClassified = -1;

    	/// <summary>
    	///   Stato normale
    	/// </summary>
		[DataMember]
		public static readonly int Ok = 0;

    	/// <summary>
    	///   Allarme lieve
    	/// </summary>
		[DataMember]
		public static readonly int Warning = 1;

    	/// <summary>
    	///   Allarme grave
    	/// </summary>
		[DataMember]
		public static readonly int Error = 2;

    	/// <summary>
    	///   Offline
    	/// </summary>
		[DataMember]
		public static readonly int Offline = 3;

    	/// <summary>
    	///   Non attivo
    	/// </summary>
		[DataMember]
		public static readonly int NotActive = 9;

    	/// <summary>
    	///   Sconosciuto o nessuna risposta
    	/// </summary>
		[DataMember]
		public static readonly int Unknown = 255;

		/// <summary>
		/// Restituisce la Severità di Dettaglio di base corrispondente alla Severità passata
		/// </summary>
		/// <param name="severity">Severità di cui si vuole conoscere la Severità di Dettaglio di base</param>
		/// <returns></returns>
		public static ObjectSeverityDetail GetBaseSeverityDetail ( int severity )
		{
			switch ( severity )
			{
				case -1:
					{
						return NotAvailable.SeverityDetail;
					}
				case 0:
					{
						return Severities.Ok.SeverityDetail;
					}
				case 1:
					{
						return Severities.Warning.SeverityDetail;
					}
				case 2:
					{
						return Severities.Error.SeverityDetail;
					}
				case 3:
					{
						return Severities.Offline.SeverityDetail;
					}
				case 9:
					{
						return Severities.NotActive.SeverityDetail;
					}
				case 255:
					{
						return Severities.Unknown.SeverityDetail;
					}
			}

			return null;
		}
	}
}
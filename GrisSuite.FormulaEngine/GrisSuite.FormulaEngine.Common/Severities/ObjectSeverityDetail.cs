﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace GrisSuite.FormulaEngine.Common.Severities
{
	public sealed class NotAvailable : ObjectSeverityDetail
	{
		private NotAvailable ()
		{
			Id = -1;
			Description = "Stato non disponibile";
			RelatedSeverity = Severities.Severity.NotClassified;
		}

		public static NotAvailable SeverityDetail
		{
			get { return new NotAvailable(); }
		}

		public static int Severity
		{
			get { return new NotAvailable().RelatedSeverity; }
		}
	}

	public sealed class Ok : ObjectSeverityDetail
	{
		private Ok ()
		{
			Id = 0;
			Description = "In servizio";
			RelatedSeverity = Severities.Severity.Ok;
		}

		public static Ok SeverityDetail
		{
			get { return new Ok(); }
		}

		public static int Severity
		{
			get { return new Ok().RelatedSeverity; }
		}
	}

	public sealed class Warning : ObjectSeverityDetail
	{
		private Warning ()
		{
			Id = 1;
			Description = "Anomalia lieve";
			RelatedSeverity = Severities.Severity.Warning;
		}

		public static Warning SeverityDetail
		{
			get { return new Warning(); }
		}

		public static int Severity
		{
			get { return new Warning().RelatedSeverity; }
		}
	}

	public sealed class Error : ObjectSeverityDetail
	{
		private Error ()
		{
			Id = 2;
			Description = "Anomalia grave";
			RelatedSeverity = Severities.Severity.Error;
		}

		public static Error SeverityDetail
		{
			get { return new Error(); }
		}

		public static int Severity
		{
			get { return new Error().RelatedSeverity; }
		}
	}

	public sealed class Offline : ObjectSeverityDetail
	{
		private Offline ()
		{
			Id = 3;
			Description = "Offline";
			RelatedSeverity = Severities.Severity.Offline;
		}

		public static Offline SeverityDetail
		{
			get { return new Offline(); }
		}

		public static int Severity
		{
			get { return new Offline().RelatedSeverity; }
		}
	}

	public sealed class NotActive : ObjectSeverityDetail
	{
		private NotActive ()
		{
			Id = 4;
			Description = "Non attivo";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static NotActive SeverityDetail
		{
			get { return new NotActive(); }
		}

		public static int Severity
		{
			get { return new NotActive().RelatedSeverity; }
		}
	}

	public sealed class Unknown : ObjectSeverityDetail
	{
		private Unknown ()
		{
			Id = 5;
			Description = "Sconosciuto";
			RelatedSeverity = Severities.Severity.Unknown;
		}

		public static Unknown SeverityDetail
		{
			get { return new Unknown(); }
		}

		public static int Severity
		{
			get { return new Unknown().RelatedSeverity; }
		}
	}

	public sealed class DiagnosticNotAvailable : ObjectSeverityDetail
	{
		private DiagnosticNotAvailable ()
		{
			Id = 6;
			Description = "Diagnostica non disponibile";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static DiagnosticNotAvailable SeverityDetail
		{
			get { return new DiagnosticNotAvailable(); }
		}

		public static int Severity
		{
			get { return new DiagnosticNotAvailable().RelatedSeverity; }
		}
	}

	public sealed class DiagnosticDisabledByOperator : ObjectSeverityDetail
	{
		private DiagnosticDisabledByOperator ()
		{
			Id = 7;
			Description = "Diagnostica disattivata dall'operatore";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static DiagnosticDisabledByOperator SeverityDetail
		{
			get { return new DiagnosticDisabledByOperator(); }
		}

		public static int Severity
		{
			get { return new DiagnosticDisabledByOperator().RelatedSeverity; }
		}
	}

	public sealed class DuplicateIPAddress : ObjectSeverityDetail
	{
		private DuplicateIPAddress ()
		{
			Id = 8;
			Description = "Ip duplicato";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static DuplicateIPAddress SeverityDetail
		{
			get { return new DuplicateIPAddress(); }
		}

		public static int Severity
		{
			get { return new DuplicateIPAddress().RelatedSeverity; }
		}
	}

	public sealed class ServerNotActive : ObjectSeverityDetail
	{
		private ServerNotActive ()
		{
			Id = 9;
			Description = "Collettore STLC1000 non attivo";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static ServerNotActive SeverityDetail
		{
			get { return new ServerNotActive(); }
		}

		public static int Severity
		{
			get { return new ServerNotActive().RelatedSeverity; }
		}
	}

	public sealed class ServerOffline : ObjectSeverityDetail
	{
		private ServerOffline ()
		{
			Id = 10;
			Description = "Collettore STLC1000 non raggiungibile (offline)";
			RelatedSeverity = Severities.Severity.Unknown;
		}

		public static ServerOffline SeverityDetail
		{
			get { return new ServerOffline(); }
		}

		public static int Severity
		{
			get { return new ServerOffline().RelatedSeverity; }
		}
	}

	public sealed class Old : ObjectSeverityDetail
	{
		private Old ()
		{
			Id = 11;
			Description = "Storico";
			RelatedSeverity = Severities.Severity.NotClassified;
		}

		public static Old SeverityDetail
		{
			get { return new Old(); }
		}

		public static int Severity
		{
			get { return new ObjectSeverityDetail().RelatedSeverity; }
		}
	}

	public sealed class NoIPAddress : ObjectSeverityDetail
	{
		private NoIPAddress ()
		{
			Id = 12;
			Description = "Senza indirizzo IP";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static NoIPAddress SeverityDetail
		{
			get { return new NoIPAddress(); }
		}

		public static int Severity
		{
			get { return new NoIPAddress().RelatedSeverity; }
		}
	}

	public sealed class MIB2Only : ObjectSeverityDetail
	{
		private MIB2Only ()
		{
			Id = 13;
			Description = "Dati diagnostici limitati alla sola sezione MIB-II";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static MIB2Only SeverityDetail
		{
			get { return new MIB2Only(); }
		}

		public static int Severity
		{
			get { return new MIB2Only().RelatedSeverity; }
		}
	}

	public sealed class NoHostName : ObjectSeverityDetail
	{
		private NoHostName ()
		{
			Id = 14;
			Description = "Senza FullHostName";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static NoHostName SeverityDetail
		{
			get { return new NoHostName(); }
		}

		public static int Severity
		{
			get { return new NoHostName().RelatedSeverity; }
		}
	}

	public sealed class NoMACAddress : ObjectSeverityDetail
	{
		private NoMACAddress ()
		{
			Id = 15;
			Description = "Senza MAC Address";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static NoMACAddress SeverityDetail
		{
			get { return new NoMACAddress(); }
		}

		public static int Severity
		{
			get { return new NoMACAddress().RelatedSeverity; }
		}
	}

	public sealed class InconsistentSNMPData : ObjectSeverityDetail
	{
		private InconsistentSNMPData ()
		{
			Id = 16;
			Description = "Dati SNMP incongruenti";
			RelatedSeverity = Severities.Severity.NotActive;
		}

		public static InconsistentSNMPData SeverityDetail
		{
			get { return new InconsistentSNMPData(); }
		}

		public static int Severity
		{
			get { return new InconsistentSNMPData().RelatedSeverity; }
		}
	}

	public sealed class MIB2OnlyWarning : ObjectSeverityDetail
	{
		private MIB2OnlyWarning ()
		{
			Id = 17;
			Description = "Dati diagnostici limitati alla sola sezione MIB-II";
			RelatedSeverity = Severities.Severity.Warning;
		}

		public static MIB2OnlyWarning SeverityDetail
		{
			get { return new MIB2OnlyWarning(); }
		}

		public static int Severity
		{
			get { return new MIB2OnlyWarning().RelatedSeverity; }
		}
	}

	public sealed class InconsistentSNMPDataWarning : ObjectSeverityDetail
	{
		private InconsistentSNMPDataWarning ()
		{
			Id = 18;
			Description = "Dati SNMP incongruenti";
			RelatedSeverity = Severities.Severity.Warning;
		}

		public static InconsistentSNMPDataWarning SeverityDetail
		{
			get { return new InconsistentSNMPDataWarning(); }
		}

		public static int Severity
		{
			get { return new InconsistentSNMPDataWarning().RelatedSeverity; }
		}
	}

	public sealed class DiagnosticOffline : ObjectSeverityDetail
	{
		private DiagnosticOffline ()
		{
			Id = 19;
			Description = "Dispositivo non raggiungibile";
			RelatedSeverity = Severities.Severity.Unknown;
		}

		public static DiagnosticOffline SeverityDetail
		{
			get { return new DiagnosticOffline(); }
		}

		public static int Severity
		{
			get { return new DiagnosticOffline().RelatedSeverity; }
		}
	}

	public sealed class DiagnosticNotAvailableWarning : ObjectSeverityDetail
	{
		private DiagnosticNotAvailableWarning ()
		{
			Id = 20;
			Description = "Diagnostica non disponibile";
			RelatedSeverity = Severities.Severity.Warning;
		}

		public static DiagnosticNotAvailableWarning SeverityDetail
		{
			get { return new DiagnosticNotAvailableWarning(); }
		}

		public static int Severity
		{
			get { return new DiagnosticNotAvailableWarning().RelatedSeverity; }
		}
	}

	public sealed class IncompleteDiagnosticalData : ObjectSeverityDetail
	{
		private IncompleteDiagnosticalData ()
		{
			Id = 21;
			Description = "Dati di monitoraggio incompleti";
			RelatedSeverity = Severities.Severity.Warning;
		}

		public static IncompleteDiagnosticalData SeverityDetail
		{
			get { return new IncompleteDiagnosticalData(); }
		}

		public static int Severity
		{
			get { return new IncompleteDiagnosticalData().RelatedSeverity; }
		}
	}

	public sealed class IncompleteServerData : ObjectSeverityDetail
	{
		private IncompleteServerData ()
		{
			Id = 22;
			Description = "Dati del collettore incompleti";
			RelatedSeverity = Severities.Severity.Error;
		}

		public static IncompleteServerData SeverityDetail
		{
			get { return new IncompleteServerData(); }
		}

		public static int Severity
		{
			get { return new IncompleteServerData().RelatedSeverity; }
		}
	}

	public sealed class NA : ObjectSeverityDetail
	{
		private NA ()
		{
			Id = 23;
			Description = "Non applicabile";
			RelatedSeverity = Severities.Severity.Unknown;
		}

		public static NA SeverityDetail
		{
			get { return new NA(); }
		}

		public static int Severity
		{
			get { return new NA().RelatedSeverity; }
		}
	}

	public sealed class NotCompletelyOk : ObjectSeverityDetail
	{
		private NotCompletelyOk ()
		{
			Id = 24;
			Description = "In servizio, ma non al 100%";
			RelatedSeverity = Severities.Severity.Ok;
		}

		public static NotCompletelyOk SeverityDetail
		{
			get { return new NotCompletelyOk(); }
		}

		public static int Severity
		{
			get { return new NotCompletelyOk().RelatedSeverity; }
		}
	}

    public sealed class DiagnosticOfflineInError : ObjectSeverityDetail
    {
        private DiagnosticOfflineInError()
        {
            Id = 25;
            Description = "Dispositivo non raggiungibile";
            RelatedSeverity = Severities.Severity.Error;
        }

        public static DiagnosticOfflineInError SeverityDetail
        {
            get { return new DiagnosticOfflineInError(); }
        }

        public static int Severity
        {
            get { return new DiagnosticOfflineInError().RelatedSeverity; }
        }
    }

	public class ObjectSeverityDetail : IComparable
	{
		public ObjectSeverityDetail ()
		{
			Id = -1;
			Description = "Stato non disponibile";
			RelatedSeverity = Severity.NotClassified;
		}

		public int Id { get; protected set; }
		public string Description { get; protected set; }
		public int RelatedSeverity { get; protected set; }

		public static IList<ObjectSeverityDetail> KnownTypes
		{
			get
			{
				return new List<ObjectSeverityDetail>
                       {
                           NotAvailable.SeverityDetail,
                           Ok.SeverityDetail,
                           Warning.SeverityDetail,
                           Error.SeverityDetail,
                           Offline.SeverityDetail,
                           NotActive.SeverityDetail,
                           Unknown.SeverityDetail,
                           DiagnosticNotAvailable.SeverityDetail,
                           DiagnosticDisabledByOperator.SeverityDetail,
                           DuplicateIPAddress.SeverityDetail,
                           ServerNotActive.SeverityDetail,
                           ServerOffline.SeverityDetail,
                           Old.SeverityDetail,
                           NoIPAddress.SeverityDetail,
                           MIB2Only.SeverityDetail,
                           NoHostName.SeverityDetail,
                           NoMACAddress.SeverityDetail,
                           InconsistentSNMPData.SeverityDetail,
                           MIB2OnlyWarning.SeverityDetail,
                           InconsistentSNMPDataWarning.SeverityDetail,
                           DiagnosticOffline.SeverityDetail,
                           DiagnosticNotAvailableWarning.SeverityDetail,
                           IncompleteDiagnosticalData.SeverityDetail,
                           IncompleteServerData.SeverityDetail,
                           NA.SeverityDetail,
                           NotCompletelyOk.SeverityDetail,
                           DiagnosticOfflineInError.SeverityDetail,
                       };
			}
		}

		#region class plumbing

		public static ObjectSeverityDetail FromInt32 ( int status )
		{
			return KnownTypes.SingleOrDefault(os => os.Id == status);
		}

		public static implicit operator int ( ObjectSeverityDetail status )
		{
			return status.Id;
		}

		public static explicit operator ObjectSeverityDetail ( int status )
		{
			return FromInt32(status);
		}

		public override string ToString ()
		{
			return Id.ToString();
		}

		public override bool Equals ( object obj )
		{
			if ( obj == null )
			{
				return false;
			}

			ObjectSeverityDetail os = obj as ObjectSeverityDetail;
			if ( os == null )
			{
				return false;
			}

			return Id == os.Id;
		}

		public bool Equals ( ObjectSeverityDetail os )
		{
			if ( os == null )
			{
				return false;
			}

			return Id == os.Id;
		}

		public override int GetHashCode ()
		{
			return Id;
		}

		public int CompareTo ( object obj )
		{
			if ( obj is ObjectSeverityDetail )
			{
				ObjectSeverityDetail sd2 = (ObjectSeverityDetail) obj;
				return this.Id.CompareTo(sd2.Id);
			}
			throw new ArgumentException("Object is not a ObjectSeverityDetail.");
		}

		public static bool operator == ( ObjectSeverityDetail a, ObjectSeverityDetail b )
		{
			if ( ReferenceEquals(a, b) )
			{
				return true;
			}

			if ( (object) a == null || (object) b == null )
			{
				return false;
			}

			return a.Id == b.Id;
		}

		public static bool operator != ( ObjectSeverityDetail a, ObjectSeverityDetail b )
		{
			return !( a == b );
		}

		public static bool operator > ( ObjectSeverityDetail a, ObjectSeverityDetail b )
		{
			return ( a.Id > b.Id );
		}

		public static bool operator < ( ObjectSeverityDetail a, ObjectSeverityDetail b )
		{
			return ( a.Id < b.Id );
		}

		public static bool operator >= ( ObjectSeverityDetail a, ObjectSeverityDetail b )
		{
			return ( a.Id >= b.Id );
		}

		public static bool operator <= ( ObjectSeverityDetail a, ObjectSeverityDetail b )
		{
			return ( a.Id <= b.Id );
		}
		#endregion
	}
}
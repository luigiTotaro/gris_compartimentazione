﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrisSuite.FormulaEngine.Common
{
    /// <summary>
    /// Indica lo stato di ritorno di esecuzione di un'operazione schedulata
    /// </summary>
    public enum OperationScheduleResultStatus
    {
        /// <summary>
        /// Sconosciuto / Indeterminato
        /// </summary>
        Unknown,
        /// <summary>
        /// Operazione completata con successo
        /// </summary>
        Success,
        /// <summary>
        /// Operazione fallita
        /// </summary>
        Error,
        /// <summary>
        /// L'operazione è fallita, ma l'esecuzione dovrebbe essere ritentata
        /// </summary>
        ShouldRetry
    }
}

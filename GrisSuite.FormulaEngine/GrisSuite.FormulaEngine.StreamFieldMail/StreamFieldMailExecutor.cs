﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.StreamFieldMail
{
    public class StreamFieldMailExecutor : IOperationExecutor
    {
        private string TemplateFilePath
        {
            get
            {
                string path = ConfigurationManager.AppSettings["MailTemplateFilePath"];
                return string.IsNullOrEmpty(path) ? Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\node_system_mail_template.txt" : path;
            }
        }

        private string EmailFrom
        {
            get { return ConfigurationManager.AppSettings["StreamFieldEmailFrom"] ?? ""; }
        }

        private string EmailSubject
        {
            get { return ConfigurationManager.AppSettings["StreamFieldEmailSubject"] ?? ""; }
        }

        private string EmailSmtpServer
        {
            get { return ConfigurationManager.AppSettings["StreamFieldEmailSmtpServer"] ?? ""; }
        }

        private int EmailSmtpServerPort
        {
            get
            {
                int serverPort;

                if (!int.TryParse(ConfigurationManager.AppSettings["StreamFieldEmailSmtpServerPort"] ?? "25", out serverPort))
                {
                    serverPort = 25;
                }

                return serverPort;
            }
        }

        private bool EmailSmtpServerEnableSsl
        {
            get
            {
                bool enableSsl;

                if (!bool.TryParse(ConfigurationManager.AppSettings["StreamFieldEmailSmtpServerEnableSsl"] ?? "false", out enableSsl))
                {
                    enableSsl = false;
                }

                return enableSsl;
            }
        }

        private SmtpDeliveryMethod EmailSmtpServerDeliveryMethod
        {
            get
            {
                SmtpDeliveryMethod deliveryMethod;

                if (
                    !Enum.TryParse(ConfigurationManager.AppSettings["StreamFieldEmailSmtpServerDeliveryMethod"] ?? "Network", true, out deliveryMethod))
                {
                    deliveryMethod = SmtpDeliveryMethod.Network;
                }

                return deliveryMethod;
            }
        }

        private bool EmailSmtpServerUseDefaultCredentials
        {
            get
            {
                bool useDefaultCredentials;

                if (
                    !bool.TryParse(ConfigurationManager.AppSettings["StreamFieldEmailSmtpServerUseDefaultCredentials"] ?? "false",
                        out useDefaultCredentials))
                {
                    useDefaultCredentials = false;
                }

                return useDefaultCredentials;
            }
        }

        private string EmailAccount
        {
            get { return ConfigurationManager.AppSettings["StreamFieldEmailAccount"] ?? ""; }
        }

        private string EmailPassword
        {
            get { return ConfigurationManager.AppSettings["StreamFieldEmailPassword"] ?? ""; }
        }

        #region Implementation of IOperationExecutor

        /// <summary>
        ///     Esegue un operazione passando le informazioni di stato di cui l'operazione ha bisogno.
        /// </summary>
        /// <param name="operationSchedule">Entità che rappresenta l'operazione da eseguire.</param>
        /// <param name="logger">Istanza logger</param>
        /// <returns></returns>
        public OperationScheduleExecResult Exec(IOperationSchedule operationSchedule, NLog.Logger logger)
        {
            // 29/07/2013 Implementazione non generica: cablata sulle restrizioni attuali della GUI.
            var state = operationSchedule.Parameters;
            if (state == null)
            {
                throw new ArgumentException("Il parametro 'state' non è inizializzato correttamente.", "operationSchedule");
            }
            if (!state.ContainsKey("nodeSysId") || string.IsNullOrEmpty(state["nodeSysId"]))
            {
                throw new ArgumentException("Impossibile inviare l'email: nodeSysId non inizializzato correttamente.", "operationSchedule");
            }

            long nodeSysId;
            string nodeSysIdStr = state["nodeSysId"];
            var sbMessage = new StringBuilder();
            OperationScheduleExecResult result;

            if (long.TryParse(nodeSysIdStr, out nodeSysId))
            {
                var nodeSystem = NodeSystem.GetBySpecificId(nodeSysId, new Dictionary<string, object> {{"LookupInCacheOnly", false}});

                if (nodeSystem != null && nodeSystem.Id != Guid.Empty)
                {
                    // contacts lookup
                    var contacts = Contact.GetContactsByGrisObject(nodeSystem);

                    if (contacts.Count > 0)
                    {
                        var templates = new MessageTemplates(this.TemplateFilePath);
                        sbMessage.AppendFormat(templates.HeaderTemplate);

                        sbMessage.AppendFormat(templates.NodeSystemTemplate, operationSchedule.Created.ToShortDateString(),
                            operationSchedule.Created.ToShortTimeString(), nodeSystem.Name, nodeSystem.Node.Name);

                        if (nodeSystem.VirtualObjects.Count > 0)
                        {
                            foreach (var virtualObject in nodeSystem.VirtualObjects)
                            {
                                this.WriteVirtualObject(virtualObject, sbMessage, templates.VirtualObjectTemplate, templates.DeviceTemplate,
                                    templates.StreamTemplate, templates.StreamFieldTemplate);
                            }
                        }
                        else
                        {
                            foreach (var device in nodeSystem.Devices)
                            {
                                this.WriteDevice(device, sbMessage, templates.DeviceTemplate, templates.StreamTemplate, templates.StreamFieldTemplate);
                            }
                        }

                        sbMessage.AppendFormat(templates.FooterTemplate);

                        if (logger != null)
                        {
                            logger.Info("Invio mail per sistema: {0}, Stazione: {1}, Oggetto: {2}", nodeSystem.Name, nodeSystem.Node.Name,
                                this.EmailSubject);
                        }

                        try
                        {
                            this.SendMail(this.EmailFrom, this.EmailSubject, sbMessage.ToString(),
                                contacts.Where(c => c.Email != null).Select(c => c.Email).ToList(), this.EmailSmtpServer, this.EmailSmtpServerPort,
                                this.EmailSmtpServerEnableSsl, this.EmailSmtpServerDeliveryMethod, this.EmailSmtpServerUseDefaultCredentials,
                                this.EmailAccount, this.EmailPassword);

                            result = new OperationScheduleExecResult() {Status = Common.OperationScheduleResultStatus.Success};
                        }
                        catch (Exception ex)
                        {
                            if (ex is SmtpException)
                            {
                                result = new OperationScheduleExecResult();
                                result.Status = Common.OperationScheduleResultStatus.ShouldRetry;
                                result.InnerException = ex;
                                result.Message = ex.Message;
                                return result;
                            }

                            result = new OperationScheduleExecResult();
                            result.Status = Common.OperationScheduleResultStatus.Error;
                            result.InnerException = ex;
                            result.Message = ex.Message;
                            return result;
                        }

                        return result;
                    }

                    string errorMessage =
                        string.Format(
                            "Mail non inviata, non sono indicati destinatari sulla mail di notifica per il sistema: {0}, Stazione: {1}, Oggetto: {2}",
                            nodeSystem.Name, nodeSystem.Node.Name, this.EmailSubject);

                    if (logger != null)
                    {
                        logger.Warn(errorMessage);
                    }

                    result = new OperationScheduleExecResult();
                    result.Status = Common.OperationScheduleResultStatus.Error;
                    result.InnerException = null;
                    result.Message = errorMessage;
                    return result;
                }
                else
                {
                    string errorMessage = string.Format("Impossibile trovare un sistema con Id: {0}", nodeSysId);

                    if (logger != null)
                    {
                        logger.Warn(errorMessage);
                    }

                    result = new OperationScheduleExecResult();
                    result.Status = Common.OperationScheduleResultStatus.Error;
                    result.InnerException = null;
                    result.Message = errorMessage;
                    return result;
                }
            }
            else
            {
                string errorMessage = string.Format("L'Id del sistema non è un numero intero. Valore: {0}", nodeSysIdStr);

                if (logger != null)
                {
                    logger.Warn(errorMessage);
                }

                result = new OperationScheduleExecResult();
                result.Status = Common.OperationScheduleResultStatus.Error;
                result.InnerException = null;
                result.Message = errorMessage;
                return result;
            }
        }

        private void WriteVirtualObject(IVirtualObject virtualObject,
            StringBuilder sbMessage,
            string voTemplate,
            string deviceTemplate,
            string streamTemplate,
            string streamFieldTemplate)
        {
            if (virtualObject != null)
            {
                if (virtualObject.HasChildrenThatShouldSendEmail.HasValue && virtualObject.HasChildrenThatShouldSendEmail.Value)
                {
                    sbMessage.AppendFormat(voTemplate, virtualObject.Name);
                }

                if (virtualObject.VirtualObjects.Count > 0)
                {
                    foreach (var vo in virtualObject.VirtualObjects)
                    {
                        this.WriteVirtualObject(vo, sbMessage, voTemplate, deviceTemplate, streamTemplate, streamFieldTemplate);
                    }
                }
                else
                {
                    foreach (var child in virtualObject.Children)
                    {
                        IDevice dev;
                        if ((dev = child as IDevice) != null)
                        {
                            this.WriteDevice(dev, sbMessage, deviceTemplate, streamTemplate, streamFieldTemplate);
                        }
                    }
                }
            }
        }

        private void WriteDevice(IDevice device, StringBuilder sbMessage, string deviceTemplate, string streamTemplate, string streamFieldTemplate)
        {
            if (device != null)
            {
                // non controllo gli ack, informazione denormalizzata nel flag ShouldSendNotificationByEmail dal software in periferia
                if (device.ShouldSendNotificationByEmail)
                {
                    sbMessage.AppendFormat(deviceTemplate, device.Name);

                    foreach (var stream in device.Streams)
                    {
                        var streamFields = stream.StreamFields.Where(sf => sf.ShouldSendNotificationByEmail).ToList();
                        if (streamFields.Count > 0)
                        {
                            sbMessage.AppendFormat(streamTemplate, stream.Name);

                            foreach (var streamField in streamFields)
                            {
                                sbMessage.AppendFormat(streamFieldTemplate, streamField.Name, streamField.Value);
                            }
                        }
                    }
                }
            }
        }

        private void SendMail(string from,
            string subject,
            string body,
            IList<string> recipients,
            string smtpServer,
            int smtpServerPort,
            bool smtpServerEnableSsl,
            SmtpDeliveryMethod deliveryMethod,
            bool smtpServerUseDefaultCredentials,
            string account,
            string password)
        {
            if (string.IsNullOrEmpty(from))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato un mittente.", "from");
            }
            if (string.IsNullOrEmpty(subject))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato l'oggetto della mail.", "subject");
            }
            if (string.IsNullOrEmpty(body))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato un messaggio.", "body");
            }
            if (string.IsNullOrEmpty(recipients.FirstOrDefault()))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non sono stati specificati destinatari.", "recipients");
            }
            if (string.IsNullOrEmpty(smtpServer))
            {
                throw new ArgumentException("Impossibile inviare il messaggio e-mail. Non è stato specificato un server smtp.", "smtpServer");
            }

            var msg = new MailMessage();
            foreach (var recipient in recipients)
            {
                msg.To.Add(recipient);
            }

            msg.Subject = subject;
            msg.From = new MailAddress(from);
            msg.Body = body;
            msg.IsBodyHtml = true;

            var smtp = new SmtpClient
            {
                Host = smtpServer,
                Port = smtpServerPort,
                EnableSsl = smtpServerEnableSsl,
                DeliveryMethod = deliveryMethod,
                UseDefaultCredentials = smtpServerUseDefaultCredentials,
                Credentials = new NetworkCredential(account, password)
            };

            smtp.Send(msg);
        }

        #endregion
    }

    public class MessageTemplates
    {
        public MessageTemplates(string filepath)
        {
            if (File.Exists(filepath))
            {
                using (var sr = File.OpenText(filepath))
                {
                    string fileContent = sr.ReadToEnd();
                    if (fileContent.Length > 0)
                    {
                        string[] templates = fileContent.Split('§');

                        if (templates.Length != 7)
                        {
                            throw new ApplicationException("Il file di definizione dei template di messaggio non definisce tutti i template.");
                        }

                        this.HeaderTemplate = templates[0];
                        this.NodeSystemTemplate = templates[1];
                        this.VirtualObjectTemplate = templates[2];
                        this.DeviceTemplate = templates[3];
                        this.StreamTemplate = templates[4];
                        this.StreamFieldTemplate = templates[5];
                        this.FooterTemplate = templates[6];
                    }
                    else
                    {
                        throw new ApplicationException("Il file di definizione dei template di messaggio è vuoto.");
                    }
                }
            }
        }

        public string HeaderTemplate { get; private set; }
        public string NodeSystemTemplate { get; private set; }
        public string VirtualObjectTemplate { get; private set; }
        public string DeviceTemplate { get; private set; }
        public string StreamTemplate { get; private set; }
        public string StreamFieldTemplate { get; private set; }
        public string FooterTemplate { get; private set; }
    }
}
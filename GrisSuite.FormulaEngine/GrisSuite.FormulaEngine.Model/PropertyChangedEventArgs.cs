﻿
using System;

namespace GrisSuite.FormulaEngine.Model
{
	public class PropertyChangedEventArgs : EventArgs
	{
		private readonly string _propertyName;
		private readonly object _newValue;

		public PropertyChangedEventArgs ( string propertyName, object newValue )
		{
			_propertyName = propertyName;
			_newValue = newValue;
		}

		public object NewValue
		{
			get { return _newValue; }
		}

		public string PropertyName
		{
			get { return _propertyName; }
		}
	}
}

﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;

namespace GrisSuite.FormulaEngine.Model
{
	public class NavigableObject<TEntity, TKey> : INavigableObject<TEntity, TKey> where TEntity : INavigableObject<TEntity, TKey>
	{
		private static readonly object _locker = new object();
		public event EventHandler<PropertyChangedEventArgs> PropertyChanged;

		public NavigableObject ()
		{
			this.PropertyChanged += Object_PropertyChanged;
		}

		public virtual TKey Id 
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public bool IsNavigationActive { get; set; }

		protected void RaisePropertyChanged ( string propertyName, object propertyValue )
		{
			if ( this.PropertyChanged != null ) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName, propertyValue));
		}

		protected virtual void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e ) {}

		#region Cache

		private static ObjectCache<TEntity, TKey> _cache;
		public static ObjectCache<TEntity, TKey> Cache
		{
			get
			{
				if ( _cache == null )
				{
					lock ( _locker )
					{
						if ( _cache == null )
						{
							_cache = new ObjectCache<TEntity, TKey>();
						}
					}
				}

				return _cache;
			}
		}

		public virtual TEntity GetCachedParent ( string propertyName )
		{
			throw new NotImplementedException();
		}

		public virtual List<TEntity> GetCachedChildren ( string propertyName )
		{
			throw new NotImplementedException();
		}

        public virtual List<TEntity> GetCachedRelated()
	    {
	        throw new NotImplementedException();
	    }

	    #endregion
	}
}

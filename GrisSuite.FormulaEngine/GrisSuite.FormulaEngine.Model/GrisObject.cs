﻿
using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;
using GrisSuite.FormulaEngine.Model.Entities;

namespace GrisSuite.FormulaEngine.Model
{
	public class GrisObject : NavigableObject<IGrisObject, Guid>, IGrisObject, IComparable, ISmsNotifyForChange
	{
		private readonly IDictionary<string, object> _initializationValues;
		private static readonly object _locker = new object();

		public event EventHandler<SeverityChangedEventArgs> SeverityChanged;
		public event EventHandler<SeverityDetailChangedEventArgs> SeverityDetailChanged;

		public IGrisAttributeCollection Attributes { get; set; }

		public GrisObject ( IDictionary<string, object> initializationValues = null )
		{
			this._initializationValues = initializationValues;

			if ( initializationValues != null )
			{
				if ( initializationValues.ContainsKey("ToBeEvaluated") ) this.IsEvaluated = !(bool) initializationValues["ToBeEvaluated"];
				if ( initializationValues.ContainsKey("LookupInCacheOnly") ) this.LookupInCacheOnly = (bool) initializationValues["LookupInCacheOnly"];
			}
			else
			{
				this.IsEvaluated = false;
				this.LookupInCacheOnly = true;
			}

			this.IsNavigationActive = true;
		}

		#region Entity Members

		/// <summary>
		/// Identificativo del GrisObject.
		/// </summary>
		private Guid _id;
		public override Guid Id
		{
			get { return this._id; }
			set
			{
				this._id = value;
				this.RaisePropertyChanged("Id", value);
			}
		}

		/// <summary>
		/// Nome del GrisObject.
		/// </summary>
		private string _name;
		public string Name
		{
			get { return this._name; }
			set
			{
				this._name = value;
				this.RaisePropertyChanged("Name", value);
			}
		}

		/// <summary>
		/// Identificativo del GrisObject specifico per tipo.
		/// </summary>
		private long _specificId;
		public long SpecificId
		{
			get { return this._specificId; }
			set
			{
				this._specificId = value;
				this.RaisePropertyChanged("SpecificId", value);
			}
		}

		/// <summary>
		/// Identificativo del tipo di GrisObject.
		/// </summary>
		public int TypeId { get; set; }

		/// <summary>
		/// Severità dello stato.
		/// </summary>
		private int _sevLevel;
		public int SevLevel
		{
			get { return this._sevLevel; }
			set
			{
				this._sevLevel = value;
				this.RaisePropertyChanged("SevLevel", value);
			}
		}

		/// <summary>
		/// Severità di dettaglio dello stato.
		/// </summary>
		private int? _sevLevelDetail;
		public int? SevLevelDetail
		{
			get { return this._sevLevelDetail; }
			set
			{
				this._sevLevelDetail = value;
				this.RaisePropertyChanged("SevLevelDetail", value);
			}
		}

		private Guid? _parentId;
		/// <summary>
		/// Identificativo del GrisObject padre.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Parent", RelatedChildrenProperty = "Children")]
		public Guid? ParentId
		{
			get { return this._parentId; }
			set
			{
				this._parentId = value;
				this.RaisePropertyChanged("ParentId", value);
			}
		}

		/// <summary>
		/// Indica se è in mantenimento.
		/// </summary>
		public bool InMaintenance { get; set; }

		/// <summary>
		/// Severità dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		public int? SevLevelReal { get; set; }

		/// <summary>
		/// Severità di dettaglio dello stato non mascherata dallo stato InMaintenance.
		/// </summary>
		public int? SevLevelDetailReal { get; set; }

		/// <summary>
		/// Ultima severità dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		public int? SevLevelLast { get; set; }

		/// <summary>
		/// Ultima severità di dettaglio dello stato rilevata, non mascherata dallo stato Offline.
		/// </summary>
		public int? SevLevelDetailLast { get; set; }

		/// <summary>
		/// Stamp dell' ultimo aggiornamento.
		/// </summary>
		public Guid? LastUpdateGuid { get; set; }

		/// <summary>
		/// Indica se il GrisObject è escluso dal calcolo dello stato del padre.
		/// </summary>
		public bool ExcludeFromParentStatus { get; set; }

		/// <summary>
		/// Indica se il GrisObject è stato forzato nello stato attuale.
		/// </summary>
		public bool ForcedByUser { get; set; }

		/// <summary>
		/// Indica se il GrisObject è in un particolare stato forzato.
		/// </summary>
		public byte? ForcedSevLevel { get; set; }

		/// <summary>
		/// Indica se la notifica SMS è abilitata per quest'oggetto.
		/// </summary>
		public bool IsSmsNotificationEnabled { get; set; }

        /// <summary>
        /// Indica se ci sono contatti per la notifica associati quest'oggetto
        /// </summary>
        public bool HasNotificationContacts { get; set; }

		#region Navigation Properties

		/// <summary>
		/// Collezione delle formule correlate.
		/// </summary>
		private List<IObjectFormula> _objectFormulas;
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public List<IObjectFormula> ObjectFormulas
		{
			get { return this._objectFormulas ?? ( this._objectFormulas = Entities.ObjectFormula.GetObjectAttributesByGrisObjectId(this.Id) ); }
			set { this._objectFormulas = value; }
		}

        private IEntityCollection<IGrisObject> _children;
        /// <summary>
        /// Collezione degli oggetti figlio.
        /// </summary>
        [XmlIgnore]
        [SoapIgnore]
        [DataMember]
        public IEntityCollection<IGrisObject> Children
        {
            get
            {
                if (this._children == null) throw new EntityException("La collezione 'Children' non è stata inizializzata correttamente. E' possibile che la proprità 'Id' dell'entità non sia stata inizializzata.");
                return (this._children.IsLoaded) ? this._children : this.RetrieveChildCollection(this._children);
            }
            set { _children = value; }
        }

        private IEntityCollection<IGrisObject> _related;
        /// <summary>
        /// Collezione degli oggetti correlati.
        /// </summary>
        [XmlIgnore]
        [SoapIgnore]
        [DataMember]
        public IEntityCollection<IGrisObject> Related
        {
            get
            {
                if (this._related == null) throw new EntityException("La collezione 'Related' non è stata inizializzata correttamente. E' possibile che la proprità 'Id' dell'entità non sia stata inizializzata.");
                return (this._related.IsLoaded) ? this._related : this.RetrieveChildCollection(this._related);
            }
            set { _related = value; }
        }

		/// <summary>
		/// Oggetto padre.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public IGrisObject Parent
		{
			get { return this.ParentReference.Value; }
			set { this.ParentReference = new EntityParent<IGrisObject>(this, "Parent", value); }
		}

		private IEntityEnd<IGrisObject> _parentRef;
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public IEntityEnd<IGrisObject> ParentReference
		{
			get { return ( this._parentRef.IsLoaded ) ? this._parentRef : this.RetrieveParent(this._parentRef); }
			set { this._parentRef = value; }
		}

		/// <summary>
		/// Collezione degli attributi.
		/// </summary>
		private List<IObjectAttribute> _objectAttributes;
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		public List<IObjectAttribute> ObjectAttributes
		{
			get { return this._objectAttributes ?? ( this._objectAttributes = Entities.ObjectAttribute.GetObjectAttributesByGrisObjectId(this.Id) ); }
			set { this._objectAttributes = value; }
		}

		protected IEntityEnd<TEntity> RetrieveParent<TEntity> ( IEntityEnd<TEntity> parent ) where TEntity : class, IGrisObject
		{
			if ( parent == null ) return new EntityParent<TEntity>(this, "", default(TEntity));

			if ( this.IsNavigationActive ) parent.Load(this.LookupInCacheOnly);

			return parent;
		}

        protected IEntityCollection<TEntity> RetrieveChildCollection<TEntity> ( IEntityCollection<TEntity> collection ) where TEntity : class, IGrisObject
        {
            if (collection == null) return new EntityCollection<TEntity>(this, "", new List<TEntity>(0));

            if (this.IsNavigationActive) collection.Load(this.LookupInCacheOnly);
            return collection;
        }

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._parentRef = new EntityParent<IGrisObject>(this, "Parent", this.GetParent);
							this._children = new EntityCollection<IGrisObject>(this, "Children", this.GetChildren);
                            this._related = new EntityCollection<IGrisObject>(this, "Related", this.GetRelated);
							break;
						}
				}
			}
		}

		#endregion

		#endregion

		#region Initialization

		public IDictionary<string, object> InitializationValues
		{
			get { return _initializationValues; }
		}

		#endregion

		#region Builtin Attribute properties

		#region Severities

        /// <summary>
        /// Severità calcolata dal motore e dalle formule, dell'oggetto
        /// </summary>
		public int Severity
		{
			get
			{
				int status = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("Severity") &&
					int.TryParse(this.Attributes["Severity"].ToString(), out status) )
				{
					return status;
				}
				return -1;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["Severity"] = value;

					if ( this.SeverityChanged != null && this.SevLevel != value ) this.SeverityChanged(this, new SeverityChangedEventArgs(this.SevLevel, value));

					this.SevLevel = value;

					this.IsSeverityEstabilished = true;

					#region Reset delle Esclusioni dallo stato del padre

					// Per rendere possibile il reset delle esclusioni dallo stato del padre 
					// si assume che, tranne nel caso di impostazione a 'Non Attivo', quando si assegna una severità
					// si vuole che l'oggetto torni a contare per il calcolo dello stato del padre.
					if ( this.ExcludeFromParentStatus && value != NotActive.Severity ) this.ExcludedFromParentStatus = false;
					// non è stata usata la proprietà wrapper perchè la lettura di attributo, nel caso di accesso da formula di un altro oggetto, creava una dipendenza
					// anche nell' eventualità di una mera impostazione di proprietà. Nel caso specifico una dipendenza circolare.

					#endregion
				}
			}
		}

        /// <summary>
        /// Severità di dettaglio calcolata dal motore e dalle formule, dell'oggetto
        /// </summary>
		public ObjectSeverityDetail SeverityDetail
		{
			get
			{
				int detailedStatus = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("SeverityDetail") &&
					int.TryParse(this.Attributes["SeverityDetail"].ToString(), out detailedStatus) )
				{
					return (ObjectSeverityDetail) detailedStatus;
				}

				return NotAvailable.SeverityDetail;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["SeverityDetail"] = value;

					if ( this.SeverityDetailChanged != null && this.SevLevelDetail.HasValue && this.SevLevelDetail != value ) this.SeverityDetailChanged(this, new SeverityDetailChangedEventArgs((ObjectSeverityDetail) this.SevLevelDetail.Value, value));

					this.SevLevelDetail = value;

					this.IsSeverityEstabilished = true;

					this.Severity = value.RelatedSeverity;
				}
			}
		}

        /// <summary>
        /// Severità reale calcolata dal motore e dalle formule, dell'oggetto
        /// </summary>
		public int RealSeverity
		{
			get
			{
				int status = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("RealSeverity") &&
					int.TryParse(this.Attributes["RealSeverity"].ToString(), out status) )
				{
					return status;
				}
				return -1;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["RealSeverity"] = value;

					this.SevLevelReal = value;

					this.IsSeverityEstabilished = true;
				}
			}
		}

        /// <summary>
        /// Severità reale di dettaglio calcolata dal motore e dalle formule, dell'oggetto
        /// </summary>
		public ObjectSeverityDetail RealSeverityDetail
		{
			get
			{
				int detailedStatus = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("RealSeverityDetail") &&
					int.TryParse(this.Attributes["RealSeverityDetail"].ToString(), out detailedStatus) )
				{
					return (ObjectSeverityDetail) detailedStatus;
				}

				return NotAvailable.SeverityDetail;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["RealSeverityDetail"] = value;

					this.SevLevelDetailReal = value;

					this.IsSeverityEstabilished = true;

					this.RealSeverity = value.RelatedSeverity;
				}
			}
		}

		public int LastSeverity
		{
			get
			{
				int status = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("LastSeverity") &&
					int.TryParse(this.Attributes["LastSeverity"].ToString(), out status) )
				{
					return status;
				}
				return -1;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["LastSeverity"] = value;

					this.SevLevelLast = value;

					this.IsSeverityEstabilished = true;
				}
			}
		}

		public ObjectSeverityDetail LastSeverityDetail
		{
			get
			{
				int detailedStatus = -1;
				if ( this.Attributes != null &&
					this.Attributes.Contains("LastSeverityDetail") &&
					int.TryParse(this.Attributes["LastSeverityDetail"].ToString(), out detailedStatus) )
				{
					return (ObjectSeverityDetail) detailedStatus;
				}

				return NotAvailable.SeverityDetail;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["LastSeverityDetail"] = value;

					this.SevLevelDetailLast = value;

					this.IsSeverityEstabilished = true;

					this.LastSeverity = value.RelatedSeverity;
				}
			}
		}

		public int ComparableSeverity
		{
			get
			{
				var myself = this as IDevice;
				if ( myself != null && myself.IsActive.HasValue && !Convert.ToBoolean(myself.IsActive) ) return 9;
				if ( myself != null && myself.IsOffline.HasValue && Convert.ToBoolean(myself.IsOffline) ) return 3;

				return this.Severity;
			}
		}

		#endregion

		public bool ExcludedFromParentStatus
		{
			get
			{
				bool excludedFromParentStatus = false;
				if ( this.Attributes != null && this.Attributes.Contains("ExcludeFromParentStatus") &&
					 bool.TryParse(this.Attributes["ExcludeFromParentStatus"].ToString().ToLowerInvariant(), out excludedFromParentStatus) )
				{
					return excludedFromParentStatus;
				}

				return false;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["ExcludeFromParentStatus"] = value;

					this.ExcludeFromParentStatus = value;
				}
			}
		}

		public byte ForcedSeverityLevel
		{
			get
			{
				byte forcedSevLevel = 0;
				if ( this.Attributes != null && this.Attributes.Contains("ForcedSevLevel") &&
					 byte.TryParse(this.Attributes["ForcedSevLevel"].ToString(), out forcedSevLevel) )
				{
					return forcedSevLevel;
				}

				return 0;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["ForcedSevLevel"] = value;

					this.ForcedSevLevel = value;
				}
			}
		}

		public Color Color
		{
			get
			{
				if ( this.Attributes != null &&
					this.Attributes.Contains("Color") )
				{
					try
					{
						return ColorTranslator.FromHtml(this.Attributes["Color"].ToString());
					}
					catch ( FormatException )
					{
						return Color.Empty;
					}
				}
				return Color.Empty;
			}
			set
			{
				if ( this.Attributes != null )
				{
					this.Attributes["Color"] = ColorTranslator.ToHtml(value);
				}
			}
		}

		public void ResetColor ()
		{
			this.Attributes.ResetAttribute("Color");
		}

		public void ForceSeverity ( int severity )
		{
			this.Severity = severity;
			this.Attributes.ForceAttribute("Severity", severity);
		}

		public void ForceSeverity ( ObjectSeverityDetail severityDetail )
		{
			this.SeverityDetail = severityDetail;
			this.Attributes.ForceAttribute("SeverityDetail", severityDetail);
		}

		#endregion

		#region Evaluation

		public IEvaluationContext EvaluationContext { get; set; }

		public Dictionary<string, object> EvaluationState { get; set; }

		public void Evaluate ()
		{
			this.Evaluate(false);
		}

		public void Evaluate ( bool force )
		{
			// se il calcolo non è forzato
			// non vengono valutati gli oggetti già valutati o in via di valutazione
			if ( ( !this.IsEvaluated && this != this.CallerObject ) || force )
			{
				if ( !this.IsInitialized ) this.Initialize();

				if ( this.EvaluationContext != null )
				{
					// l'oggetto da valutare imposta se stesso come oggetto che ha invocato la valutazione
					this.CallerObject = this;

					lock ( _locker )
					{
						if ( !this.IsEvaluated || force ) // double-checking
						{
							this.IsEvaluated = false;
							this.IsSeverityEstabilished = false;
							this.EvaluationContext.Evaluate(this, this.EvaluationState);
							this.IsEvaluated = true;
						}
					}
				}
			}
		}

		public void ForceEvaluationOnDependentObjects ()
		{
			foreach ( var dependentObject in ObjectDependencies.Instance.GetDependentObjects(this) )
			{
				dependentObject.Evaluate(true);
			}
		}

		public bool IsEvaluated { get; private set; }

		public bool LookupInCacheOnly { get; private set; }

		public bool IsSeverityEstabilished { get; private set; }

		public IEvaluationFormula EvaluatingFormula { get; set; }

		public IEvaluableObject CallerObject { get; set; }

		public bool IsInitialized { get; private set; }

		public void Initialize ()
		{
			if ( this.CallerObject != null )
			{
				var caller = (IGrisObject) this.CallerObject;
				if ( caller.Attributes != null )
				{
					this.InitializeForEvaluation(caller.EvaluationContext, caller.Attributes.AttributeTypes, caller.LookupInCacheOnly);
				}
			}
		}

		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext )
		{
			return this.InitializeForEvaluation(evalContext, Entities.ObjectAttributeType.GetAllAttributes(), this.GetTypeEvaluationState());
		}

		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<IObjectAttributeType> attributes, bool lookupInCacheOnly = false )
		{
			return this.InitializeForEvaluation(evalContext, attributes, this.GetTypeEvaluationState());
		}

		public IEvaluableObject InitializeForEvaluation ( IEvaluationContext evalContext, IList<IObjectAttributeType> attributeTypes, Dictionary<string, object> state )
		{
			this.EvaluationContext = evalContext;

			if ( this.Attributes == null )
			{
				this.Attributes = new GrisAttributeCollection(this, attributeTypes);
			}

			if ( this.ObjectFormulas != null )
			{
				this._customFormulas = new SortedSet<IEvaluationFormula>();
				foreach ( var objectFormula in this.ObjectFormulas )
				{
					var formula = new EvaluationFormula(objectFormula);
					if ( formula.IsDefault )
					{
						this._defaultEvaluationFormula = formula;
					}
					else
					{
						this._customFormulas.Add(formula);
					}
				}

				if ( this._defaultEvaluationFormula == null ) this._defaultEvaluationFormula = new EvaluationFormula(null);
			}

			this.EvaluationState = state;

			this.IsInitialized = true;
			return this;
		}

		public virtual Dictionary<string, object> GetTypeEvaluationState ()
		{
			return null;
		}

		#endregion

		#region Formulas

		private IEvaluationFormula _defaultEvaluationFormula;
		[DataMember]
		public IEvaluationFormula DefaultEvaluationFormula
		{
			get { return this._defaultEvaluationFormula; }
			set { this._defaultEvaluationFormula = value; }
		}

		private SortedSet<IEvaluationFormula> _customFormulas;
		[DataMember]
		public SortedSet<IEvaluationFormula> CustomEvaluationFormulas
		{
			get { return this._customFormulas; }
			set { this._customFormulas = value; }
		}

		#endregion

		#region Navigation methods

		public IGrisObject GetChildByName ( string name )
		{
			return ( from grisObject in this.Children where grisObject.Name != null && grisObject.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase) select grisObject ).SingleOrDefault();
		}

		public TEntity GetChildByName<TEntity> ( string name ) where TEntity : IGrisObject
		{
			return (TEntity) this.GetChildByName(name);
		}

		public IList<IGrisObject> GetChildrenByName ( string name )
		{
			return ( from grisObject in this.Children where grisObject.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1 select grisObject ).ToList();
		}

        public IList<TEntity> GetChildrenByName<TEntity>(string name) where TEntity : IGrisObject
        {
            return this.GetChildrenByName(name).Cast<TEntity>().ToList();
        }

        public IList<IDevice> GetRelatedDevicesOfTypes(params string[] types)
        {
            return ( from dev in this.Related
                     where dev is IDevice && types.Contains(((IDevice) dev).Type)
                     select (IDevice) dev ).ToList();
        }

		#endregion

		#region Virtual Objects

		public IVirtualObject GetVirtualObjectByName ( string name )
		{
			return this.VirtualObjects.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		public IList<IVirtualObject> GetVirtualObjectsByName ( string name )
		{
			return this.VirtualObjects.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		private IEntityCollection<IVirtualObject> _virtualObjects;
		public IEntityCollection<IVirtualObject> VirtualObjects
		{
			get
			{
				return this._virtualObjects ?? ( this._virtualObjects = new EntityCollection<IVirtualObject>(this, "VirtualObjects", this.Children.OfType<IVirtualObject>()) );
			}
			set { this._virtualObjects = value; }
		}

		#endregion

        #region Data Access

        private const string MAIN_CMD = @"{0}
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    d.DevID, d.NodID as DevNodID, d.SrvID as DevSrvID, d.Name as DevName, d.[Type], d.SN, d.Addr, d.PortId as DevPortID, d.Active, d.Scheduled, d.DefinitionVersion, d.ProtocolDefinitionVersion,
    ds.SevLevel as ActualSevLevel, ds.[Description], ds.[Offline], ds.ShouldSendNotificationByEmail,
    dt.DeviceTypeDescription, dt.Obsolete, dt.DefaultName, dt.PortType, dt.SnmpCommunity, isnull(dt.DiscoveryType, 1) as DiscoveryType,
    osdn.ObjectStatusId as DevGNodID, osds.ObjectStatusId as DevGSrvID, osdns.ObjectStatusId as DevGNodSysId,
    null as VirtualObjectID, null as VirtualObjectName, null as VirtualObjectDescription,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    1 as CustomOrder
    ,CAST(CASE WHEN EXISTS( SELECT c.NotificationContactId FROM notification_contacts c WHERE c.ObjectStatusId = os.ObjectStatusId) THEN 1 ELSE 0 END AS BIT) as HasNotificationContacts
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join devices d on d.DevID = os.ObjectId and os.ObjectTypeId = 6
	left join device_status ds on ds.DevID = d.DevID
	left join device_type dt on dt.DeviceTypeID = d.[Type]
	left join node_systems ns on d.NodID = ns.NodId and dt.SystemID = ns.SystemId
	left join object_status osdn on d.NodID = osdn.ObjectId and osdn.ObjectTypeId = 3
	left join object_status osds on d.SrvID = osds.ObjectId and osds.ObjectTypeId = 5
	left join object_status osdns on ns.NodeSystemsId = osdns.ObjectId and osdns.ObjectTypeId = 4
{2}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline], null as ShouldSendNotificationByEmail,
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity, null as DiscoveryType,
    null as DevGNodID, null as DevGSrvID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName, null as VirtualObjectDescription,
    s.SrvID, s.Name as SrvName, s.Host, s.FullHostName, s.IP, s.LastUpdate, s.LastMessageType, s.NodID as SrvNodID, s.ServerVersion, s.MAC, s.IsDeleted, ossn.ObjectStatusId as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    0 as CustomOrder
    ,CAST(CASE WHEN EXISTS( SELECT c.NotificationContactId FROM notification_contacts c WHERE c.ObjectStatusId = os.ObjectStatusId) THEN 1 ELSE 0 END AS BIT) as HasNotificationContacts
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join [servers] s on s.SrvID = os.ObjectId and os.ObjectTypeId = 5
	left join object_status ossn on s.NodID = ossn.ObjectId and ossn.ObjectTypeId = 3
    inner join nodes n on n.NodID = s.NodID and ossn.ObjectTypeId = 3
{1}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline], null as ShouldSendNotificationByEmail,
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity, null as DiscoveryType,
    null as DevGNodID, null as DevGSrvID, null as DevGSrvID,
    v.VirtualObjectID, v.VirtualObjectName, v.VirtualObjectDescription,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    2 as CustomOrder
    ,CAST(CASE WHEN EXISTS( SELECT c.NotificationContactId FROM notification_contacts c WHERE c.ObjectStatusId = os.ObjectStatusId) THEN 1 ELSE 0 END AS BIT) as HasNotificationContacts
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join virtual_objects v on v.VirtualObjectID = os.ObjectId and os.ObjectTypeId = 7
{3}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline], null as ShouldSendNotificationByEmail,
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity, null as DiscoveryType,
    null as DevGNodID, null as DevGSrvID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName, null as VirtualObjectDescription,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    ns.NodeSystemsId, ns.NodId as SysNodID, ns.SystemId, sy.SystemDescription as SystemName, osnsn.ObjectStatusId as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    3 as CustomOrder
    ,CAST(CASE WHEN EXISTS( SELECT c.NotificationContactId FROM notification_contacts c WHERE c.ObjectStatusId = os.ObjectStatusId) THEN 1 ELSE 0 END AS BIT) as HasNotificationContacts
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join node_systems ns on ns.NodeSystemsId = os.ObjectId and os.ObjectTypeId = 4
	inner join device_type dt on ns.SystemId = dt.SystemID
	inner join devices d on ns.NodId = d.NodID and d.[Type] = dt.DeviceTypeID
	left join object_status osnsn on ns.NodId = osnsn.ObjectId and osnsn.ObjectTypeId = 3	
	left join systems sy on sy.SystemID = ns.SystemId
{4}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline], null as ShouldSendNotificationByEmail,
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity, null as DiscoveryType,
    null as DevGNodID, null as DevGSrvID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName, null as VirtualObjectDescription,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    n.NodID, n.ZonID as NodZonID, n.Name as NodName, n.Meters as NodMeters, osnz.ObjectStatusId as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    null as RegID, null as RegName,
    4 as CustomOrder
    ,CAST(CASE WHEN EXISTS( SELECT c.NotificationContactId FROM notification_contacts c WHERE c.ObjectStatusId = os.ObjectStatusId) THEN 1 ELSE 0 END AS BIT) as HasNotificationContacts
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join nodes n on n.NodID = os.ObjectId and os.ObjectTypeId = 3
	inner join devices d on n.NodID = d.NodID
	left join object_status osnz on n.ZonID = osnz.ObjectId and osnz.ObjectTypeId = 2
{5}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline], null as ShouldSendNotificationByEmail,
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity, null as DiscoveryType,
    null as DevGNodID, null as DevGSrvID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName, null as VirtualObjectDescription,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    z.ZonID, z.RegID as ZonRegID, z.Name as ZonName, oszr.ObjectStatusId as ZonGRegID,
    null as RegID, null as RegName,
    5 as CustomOrder
    ,CAST(CASE WHEN EXISTS( SELECT c.NotificationContactId FROM notification_contacts c WHERE c.ObjectStatusId = os.ObjectStatusId) THEN 1 ELSE 0 END AS BIT) as HasNotificationContacts
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join zones z on z.ZonID = os.ObjectId and os.ObjectTypeId = 2
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
	left join object_status oszr on z.RegID = oszr.ObjectId and oszr.ObjectTypeId = 1
{6}
union all
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    null as DevID, null as DevNodID, null as DevSrvID, null as DevName, null as [Type], null as SN, null as Addr, null as DevPortID, null as Active, null as Scheduled, null as DefinitionVersion, null as ProtocolDefinitionVersion,
    null as ActualSevLevel, null as [Description], null as [Offline], null as ShouldSendNotificationByEmail,
    null as DeviceTypeDescription, null as Obsolete, null as DefaultName, null as PortType, null as SnmpCommunity, null as DiscoveryType,
    null as DevGNodID, null as DevGSrvID, null as DevGSrvID,
    null as VirtualObjectID, null as VirtualObjectName, null as VirtualObjectDescription,
    null as SrvID, null as SrvName, null as Host, null as FullHostName, null as IP, null as LastUpdate, null as LastMessageType, null as SrvNodID, null as ServerVersion, null as MAC, null as IsDeleted, null as SrvGNodID,    
    null as NodeSystemsId, null as SysNodID, null as SystemId, null as SystemName, null as NodSysGNodID,
    null as NodID, null as NodZonID, null as NodName, null as NodMeters, null as NodGZonID,
    null as ZonID, null as ZonRegID, null as ZonName, null as ZonGRegID,
    r.RegID, r.Name as RegName,
    6 as CustomOrder
    ,CAST(CASE WHEN EXISTS( SELECT c.NotificationContactId FROM notification_contacts c WHERE c.ObjectStatusId = os.ObjectStatusId) THEN 1 ELSE 0 END AS BIT) as HasNotificationContacts
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join regions r on r.RegID = os.ObjectId and os.ObjectTypeId = 1
	inner join zones z on r.RegID = z.RegID
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
{7}
order by CustomOrder
option (maxdop 1);
";

		public static List<IGrisObject> GetAll ( IDictionary<string, object> initializationValues = null )
		{
			string srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter;
			srvFilter = "where isnull(os.SevLevelDetailId, -1) <> 11";
			devFilter = "where ([Type] <> 'NoDevice' or [Type] is null)";
			vrtFilter = sysFilter = nodFilter = zonFilter = regFilter = "";
			string cmd = string.Format(MAIN_CMD, "", srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter);
			return new Ds().Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

        public static IGrisObject GetById(Guid id, IDictionary<string, object> initializationValues = null)
        {
            string srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter;
            srvFilter = devFilter = vrtFilter = sysFilter = nodFilter = zonFilter = regFilter = "where os.ObjectStatusId = @Id";
            string cmd = string.Format(MAIN_CMD, "", srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter);
            return new Ds { { "Id", id } }.Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
        }

        public static INodeSystem GetBySpecificId(long id, IDictionary<string, object> initializationValues = null)
        {
            throw new NotImplementedException();
        }

		public virtual IGrisObject GetParent ( IDictionary<string, object> initializationValues = null )
		{
			string srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter;
			srvFilter = devFilter = vrtFilter = sysFilter = nodFilter = zonFilter = regFilter = "where os.ObjectStatusId = @ParentId";
			string cmd = string.Format(MAIN_CMD, "", srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter);
			return new Ds { { "ParentId", this.Id } }.Entity().Get<GrisObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
		}

        public virtual List<IGrisObject> GetChildren ( IDictionary<string, object> initializationValues = null )
        {
            string srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter;
            srvFilter = devFilter = vrtFilter = sysFilter = nodFilter = zonFilter = regFilter = "where os.ParentObjectStatusId = @ParentId";
            string cmd = string.Format(MAIN_CMD, "", srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter);
            return new Ds { { "ParentId", this.Id } }.Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
        }

        public virtual List<IGrisObject> GetRelated ( IDictionary<string, object> initializationValues = null )
        {
            string srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter;
            srvFilter = devFilter = vrtFilter = sysFilter = nodFilter = zonFilter = regFilter = @"inner join object_relations orel on os.ObjectStatusId = orel.RelatedObjectStatusId 
where orel.ObjectStatusId = @Id";
            string cmd = string.Format(MAIN_CMD, "", srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter);
            return new Ds { { "Id", this.Id } }.Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
        }

        public static List<Tuple<Guid, Guid>> GetRelatedObjectIds ( params Guid[] ids )
        {
            string qry = @"
select ObjectStatusId, RelatedObjectStatusId
from object_relations orel
{0}
";
            var filter = ids == null ? "" : string.Join(", ", ids.Select(id => string.Format("'{0}'", id)));
            string cmd = string.Format(qry, filter);
            return new Ds().Entity().Get(cmd, row => new Tuple<Guid, Guid>(row.GetGuid("ObjectStatusId"), row.GetGuid("RelatedObjectStatusId"))).ToList();
        }

        /// <summary>
		/// Estrae un oggetto e tutti i padri nella gerarchia dei GrisObjects.
		/// </summary>
		/// <param name="objectId">Identificativo GrisObject.</param>
		/// <param name="initializationValues">Dizionario per l'inizializzazione degli oggetti restituiti.</param>
		/// <returns>Lista di GrisObjects.</returns>
		public static List<IGrisObject> GetObjectAndAncestors ( Guid objectId, IDictionary<string, object> initializationValues = null )
		{
			string hierCte =
@"
;with SelectedBranch as 
(
	select ObjectStatusId, ParentObjectStatusId 
	from object_status
	where ObjectStatusId = @ChildObjectStatusId
	union all
	select os.ObjectStatusId, os.ParentObjectStatusId from object_status os inner join SelectedBranch sb on sb.ParentObjectStatusId = os.ObjectStatusId
)";
			return new Ds { { "ChildObjectStatusId", objectId } }.Entity().Get<IGrisObject, Guid>(string.Format(MAIN_CMD, hierCte, "", "", "", "", "", "", ""), "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="type"></param>
		/// <param name="initializationValues">Dizionario per l'inizializzazione degli oggetti restituiti.</param>
		/// <param name="ids">Identificativi GrisObject di cui si vogliono estrarre i discendenti.</param>
		/// <returns></returns>
		public static List<IGrisObject> GetObjectsAndDescendantsByIds ( Type type, IDictionary<string, object> initializationValues = null, params Guid[] ids )
		{
			string hierCte =
@"
;with RegionHierarchy  as (
	select ObjectStatusId, ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId, 
		ParentObjectStatusId, InMaintenance, SevLevelReal, SevLevelDetailIdReal, SevLevelLast, 
		SevLevelDetailIdLast, LastUpdateGuid, ExcludeFromParentStatus, ForcedByUser, ForcedSevLevel
	from object_status obst
	where ObjectStatusId in ({{0}})
	and ObjectTypeId = {0}
	union all
	select obst.ObjectStatusId, obst.ObjectId, obst.ObjectTypeId, obst.SevLevel, obst.SevLevelDetailId, 
		obst.ParentObjectStatusId, obst.InMaintenance, obst.SevLevelReal, obst.SevLevelDetailIdReal, obst.SevLevelLast, 
		obst.SevLevelDetailIdLast, obst.LastUpdateGuid, obst.ExcludeFromParentStatus, obst.ForcedByUser, obst.ForcedSevLevel
	from object_status obst
	inner join RegionHierarchy on obst.ParentObjectStatusId = RegionHierarchy.ObjectStatusId
)";
			// Gli unici tipi supportati sono Region, Zone e Node. In tutti gli altri casi subentra il caso di default che restituisce tutti gli oggetti.
			if ( type == typeof(IRegion) )
			{
				hierCte = string.Format(hierCte, 1);
			}
			else if ( type == typeof(IZone) )
			{
				hierCte = string.Format(hierCte, 2);
			}
			else if ( type == typeof(INode) )
			{
				hierCte = string.Format(hierCte, 3);
			}
			else
			{
				return GetAll();
			}

			string srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter;
			srvFilter = "where os.ObjectId in (select SrvID from RegionHierarchy inner join devices on RegionHierarchy.ObjectId = devices.DevID where ObjectTypeId = 6) and isnull(os.SevLevelDetailId, -1) <> 11";
			devFilter = "where os.ObjectStatusId in (select ObjectStatusId from RegionHierarchy) and ([Type] <> 'NoDevice' or [Type] is null)";
			vrtFilter = sysFilter = nodFilter = zonFilter = regFilter = "where os.ObjectStatusId in (select ObjectStatusId from RegionHierarchy)";

			string cmd = string.Format(MAIN_CMD, string.Format(hierCte, string.Join(", ", ids.Select(id => string.Format("'{0}'", id)))), srvFilter, devFilter, vrtFilter, sysFilter, nodFilter, zonFilter, regFilter);
			return new Ds().Entity().Get<IGrisObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

        public static void SyncObjects(Guid syncGuid, String Compartiment2Evaluate)
		{
            if (Compartiment2Evaluate == null)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("LastUpdateGuid", syncGuid);
                new Ds().Entity().Exec("tf_InsPopulateObjectStatus", param, commandType: CommandType.StoredProcedure);
            }
            else
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("LastUpdateGuid", syncGuid);
                param.Add("CompIDList", Compartiment2Evaluate);
                new Ds().Entity().Exec("tf_InsPopulateObjectStatusByCompList", param, commandType: CommandType.StoredProcedure);
            }
        }

		public static void UpdateObjectStatusPostComputation ( Guid syncGuid )
		{
			Dictionary<string, object> param = new Dictionary<string, object>();
			param.Add("LastUpdateGuid", syncGuid);
			new Ds().Entity().Exec("tf_UpdObjectStatusPostComputation", param, commandType: CommandType.StoredProcedure);
		}

		internal static GrisObject Map ( IGrouping<Guid, DataRow> groupedEntity, IDictionary<string, object> initializationValues )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();
				if ( firstRow != null )
				{
					int type;
					int.TryParse(firstRow["ObjectTypeId"].ToString(), out type);

					//if ( Convert.ToInt64(firstRow["ObjectId"]) == 1128833480982535 )
					//{
					//    Debugger.Break();
					//}

					switch ( type )
					{
						case 1:
							{
								return Entities.Region.Map(groupedEntity, initializationValues);
							}
						case 2:
							{
								return Entities.Zone.Map(groupedEntity, initializationValues);
							}
						case 3:
							{
								return Entities.Node.Map(groupedEntity, initializationValues);
							}
						case 4:
							{
								return Entities.NodeSystem.Map(groupedEntity, initializationValues);
							}
						case 5:
							{
								return Entities.Server.Map(groupedEntity, initializationValues);
							}
						case 6:
							{
								return Entities.Device.Map(groupedEntity, initializationValues);
							}
						case 7:
							{
								return Entities.VirtualObject.Map(groupedEntity, initializationValues);
							}
						default:
							{
								var go = new GrisObject(initializationValues);

								PopulateObject(firstRow, go, groupedEntity.Key);

								// attributes
								PopulateAttributes(groupedEntity, go);

								// formulas
								PopulateFormulas(groupedEntity, go);

								return go;
							}
					}
				}
			}

			return null;
		}

		internal static void PopulateObject ( DataRow source, GrisObject grisObject, Guid key )
		{
			grisObject.Id = key;
			grisObject.SpecificId = source.GetInt64("ObjectId");
			grisObject.TypeId = source.GetInt32("ObjectTypeId");
			grisObject.SevLevel = source.GetInt32("SevLevel");
			grisObject.SevLevelDetail = source.GetNullableInt32("SevLevelDetailId");
			grisObject.SevLevelReal = source.GetNullableInt32("SevLevelReal");
			grisObject.SevLevelDetailReal = source.GetNullableInt32("SevLevelDetailIdReal");
			grisObject.SevLevelLast = source.GetNullableInt32("SevLevelLast");
			grisObject.SevLevelDetailLast = source.GetNullableInt32("SevLevelDetailIdLast");
			grisObject.ParentId = source.GetNullableGuid("ParentObjectStatusId");
			grisObject.InMaintenance = source.GetBoolean("InMaintenance");
			grisObject.ExcludeFromParentStatus = source.GetBoolean("ExcludeFromParentStatus");
			grisObject.ForcedByUser = source.GetBoolean("ForcedByUser");
			grisObject.LastUpdateGuid = source.GetNullableGuid("LastUpdateGuid");
			grisObject.ForcedSevLevel = source.GetNullableByte("ForcedSevLevel");
			grisObject.IsSmsNotificationEnabled = source.GetBoolean("IsSmsNotificationEnabled");
            grisObject.HasNotificationContacts = source.GetBoolean("HasNotificationContacts"); 
            if (grisObject.GetType().Name == "Node")
            { 
                //devo vedere se esistono gruppi di active directory associati a questo nodo, per le notifiche
                bool test = ContactGroupEntry.HasContactGroupEntriesByObjectStatusId4Node(grisObject.Id);
                if (test)
                {
                    grisObject.IsSmsNotificationEnabled = true;
                    grisObject.HasNotificationContacts = true;
                }
            }
            if (grisObject.GetType().Name == "NodeSystem")
            {
                //devo vedere se esistono gruppi di active directory associati a questo sistema, per le notifiche
                bool test = ContactGroupEntry.HasContactGroupEntriesByObjectStatusId4System(grisObject.Id);
                if (test)
                {
                    grisObject.HasNotificationContacts = true;
                }
            }
        }

		internal static void PopulateAttributes ( IGrouping<Guid, DataRow> groupedEntity, GrisObject grisObject )
		{
			var attrTypes = ( from gEnt in groupedEntity
							  group gEnt by gEnt.GetInt16("AttributeTypeId")
								  into attrTyps
								  select Entities.ObjectAttributeType.Map(attrTyps) ).Distinct();

			var attrs = ( from gEnt in groupedEntity
						  where !gEnt.IsNull("AttributeId")
						  group gEnt by gEnt.GetInt64("AttributeId")
							  into attrz
							  select Entities.ObjectAttribute.Map(attrz) ).Distinct();

			foreach ( var objectAttribute in attrs )
			{
				IObjectAttribute oAttribute = objectAttribute;
				oAttribute.GrisObject = grisObject;
				oAttribute.Type = attrTypes.SingleOrDefault(at => at.Id == oAttribute.TypeId);
			}

			grisObject.ObjectAttributes = attrs.ToList();
		}

		internal static void PopulateFormulas ( IGrouping<Guid, DataRow> groupedEntity, GrisObject grisObject )
		{
			var formulas = ( from gEnt in groupedEntity
							 group gEnt by Tuple.Create(gEnt.GetGuid("ObjectStatusId"), gEnt.GetByte("FormulaIndex"))
								 into formz
								 select Entities.ObjectFormula.Map(formz) ).Distinct().ToList();

			foreach ( var objectFormula in formulas )
			{
				IObjectFormula oFormula = objectFormula;
				oFormula.GrisObject = grisObject;
			}

			grisObject.ObjectFormulas = formulas;
		}

		#endregion

		#region Persistence

		/// <summary>
		/// Indica se l'oggetto ha modifiche da persistere.
		/// </summary>
		public bool HasChanges
		{
			get { return this.Attributes != null && this.Attributes.HasChanges; }
		}

		/// <summary>
		/// Lista dei figli da persistere in db.
		/// </summary>
		public virtual IList<IStorable> StorableNodes
		{
			get
			{
				var stors = new List<IStorable>(300);
				if ( this.IsEvaluated ) stors.Add(this.Attributes);
				stors.AddRange(this.Children);
				return stors;
			}
		}

		public string GetUpdateCommand ()
		{
			//var o = this as GrisSuite.FormulaEngine.Model.Entities.Device;
			//if ((o != null) && (o.DeviceId == 281474989746552845))
			//{
			//    System.Diagnostics.Debugger.Break();
			//}

			const string UPD_CMD =
@"update object_status set SevLevelDetailId = {0}, SevLevel = {1}, SevLevelDetailIdReal = {2}, SevLevelReal = {3}, SevLevelDetailIdLast = {4}, SevLevelLast = {5}, ExcludeFromParentStatus = {6}, ForcedSevLevel = {7} where ObjectStatusId = '{8}';";
			return string.Format(UPD_CMD,
							this.SevLevelDetail.HasValue ? this.SevLevelDetail.ToString() : "NULL",
							this.SevLevel,
							this.SevLevelDetailReal.HasValue ? this.SevLevelDetailReal.ToString() : "NULL",
							this.SevLevelReal.HasValue ? this.SevLevelReal.ToString() : "NULL",
							this.SevLevelDetailLast.HasValue ? this.SevLevelDetailLast.ToString() : "NULL",
							this.SevLevelLast.HasValue ? this.SevLevelLast.ToString() : "NULL",
							this.ExcludedFromParentStatus ? "1" : "0",
							this.ForcedSeverityLevel,
							this.Id);
		}

		#endregion

		#region Cache

		public override IGrisObject GetCachedParent ( string propertyName )
		{
			return Cache.GetParent(this, propertyName);
		}

        public override List<IGrisObject> GetCachedChildren(string propertyName)
        {
            return Cache.GetChildren(this, propertyName);
        }

		public IObjectCache<IGrisObject, Guid> ObjectCache
		{
			get { return Cache; }
		}

		#endregion

		#region IComparer

		public override bool Equals ( object obj )
		{
			if ( obj == null )
			{
				return false;
			}

			var go = obj as GrisObject;
			if ( go == null )
			{
				return false;
			}

			return Id == go.Id;
		}

		public bool Equals ( GrisObject go )
		{
			if ( go == null )
			{
				return false;
			}

			return Id == go.Id;
		}

		public override int GetHashCode ()
		{
			return this.Id.GetHashCode();
		}

		public int CompareTo ( object obj )
		{
			if ( obj is GrisObject )
			{
				var go2 = (GrisObject) obj;
				return this.Id.CompareTo(go2.Id);
			}
			throw new ArgumentException("Object is not a GrisObject.");
		}

		public static bool operator == ( GrisObject a, GrisObject b )
		{
			if ( ReferenceEquals(a, b) )
			{
				return true;
			}

			if ( (object) a == null || (object) b == null )
			{
				return false;
			}

			return a.Id == b.Id;
		}

		public static bool operator != ( GrisObject a, GrisObject b )
		{
			return !( a == b );
		}

		#endregion

		#region Notifications

        private bool? _hasChildrenThatShouldSendEmail;
	    public bool? HasChildrenThatShouldSendEmail
	    {
	        get
	        {
	            if (!this._hasChildrenThatShouldSendEmail.HasValue)
	            {
	                var children = this.Children;
                    foreach (var grisObject in children)
	                {
	                    Entities.Device dev;
                        if((dev = grisObject as Entities.Device) != null)
                        {
                            if (!dev.ShouldSendNotificationByEmail) continue;
                            
                            this._hasChildrenThatShouldSendEmail = true;
                            break;
                        }
                        else
                        {
                            if (!grisObject.HasChildrenThatShouldSendEmail.HasValue ||
                                !grisObject.HasChildrenThatShouldSendEmail.Value) continue;
                            
                            this._hasChildrenThatShouldSendEmail = true;
                            break;
                        }
	                }
	            }

	            return this._hasChildrenThatShouldSendEmail;
	        }

            set { this._hasChildrenThatShouldSendEmail = value; }
	    }

		public virtual string GetSmsNotificationMessage ()
		{
			throw new NotImplementedException();
		}

        public virtual string GetSmsNotificationKeys(string phoneNumber)
		{
			throw new NotImplementedException();
		}

        public string[] GetRecipientNumbers ()
		{
			return this.GetNotificationRecipients().Select(c => c.PhoneNumber).ToArray();
		}

		private List<IContactEntry> _objectNotificationRecipients;
		public List<IContactEntry> GetNotificationRecipients ()
		{
			//TODO: precaricare in lista unica all'inizio del processo di calcolo
            if (this._objectNotificationRecipients != null)
            {
               
            }
            else
            {
                List<IContactEntry> contatti1 = Entities.ContactEntry.GetContactEntriesByObjectStatusId(this.Id);
                List<ContactEntry> contatti2 = Entities.ContactGroupEntry.GetContactEntriesByObjectStatusId4Sms(this.Id);
                for (int i = 0; i < contatti2.Count(); i++)
                {
                    //lo aggiungo solo se esiste il numero di telefono
                    if (contatti2[i].PhoneNumber!="") contatti1.Add(contatti2[i]);
                }

                this._objectNotificationRecipients = contatti1;
                
            }
            return this._objectNotificationRecipients;
		}


		#endregion
	}

	public class SeverityChangedEventArgs : EventArgs
	{
		private readonly int _oldSeverity;
		private readonly int _newSeverity;

		public SeverityChangedEventArgs ( int oldSeverity, int newSeverity )
		{
			_oldSeverity = oldSeverity;
			_newSeverity = newSeverity;
		}

		public int NewSeverity
		{
			get { return _newSeverity; }
		}

		public int OldSeverity
		{
			get { return _oldSeverity; }
		}
	}

	public class SeverityDetailChangedEventArgs : EventArgs
	{
		private readonly ObjectSeverityDetail _oldSeverityDetail;
		private readonly ObjectSeverityDetail _newSeverityDetail;

		public SeverityDetailChangedEventArgs ( ObjectSeverityDetail oldSeverityDetail, ObjectSeverityDetail newSeverityDetail )
		{
			_oldSeverityDetail = oldSeverityDetail;
			_newSeverityDetail = newSeverityDetail;
		}

		public ObjectSeverityDetail NewSeverityDetail
		{
			get { return _newSeverityDetail; }
		}

		public ObjectSeverityDetail OldSeverityDetail
		{
			get { return _oldSeverityDetail; }
		}
	}
}

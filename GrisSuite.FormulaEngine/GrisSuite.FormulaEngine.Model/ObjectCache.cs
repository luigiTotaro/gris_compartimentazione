﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using GrisSuite.FormulaEngine.Common.Contracts;

namespace GrisSuite.FormulaEngine.Model
{
	public class ObjectCache<TEntity, TKey> : IObjectCache<TEntity, TKey>
		where TEntity : INavigableObject<TEntity, TKey> 
	{
		private static readonly object _locker = new object();
		private readonly IDictionary<TKey, TEntity> _objsDict = new Dictionary<TKey, TEntity>(50000);
		private readonly IDictionary<TKey, IDictionary<string, TKey>> _parentsDict = new Dictionary<TKey, IDictionary<string, TKey>>(50000);
        private readonly IDictionary<TKey, IDictionary<string, List<TKey>>> _childrenDict = new Dictionary<TKey, IDictionary<string, List<TKey>>>(50000);

		public bool HasObject ( TKey id )
		{
			return this._objsDict.ContainsKey(id);
		}

		public TEntity Get ( TKey id )
		{
			if ( this._objsDict.ContainsKey(id) )
			{
				return this._objsDict[id];
			}

			return default(TEntity);
		}

		public TEntity GetParent ( TEntity entity, string propertyName )
		{
			IDictionary<string, TKey> propParent;
			this._parentsDict.TryGetValue(entity.Id, out propParent);
			if ( propParent != null )
			{
				TKey parentId;
				propParent.TryGetValue(propertyName, out parentId);
				if ( !parentId.Equals(default(TKey)) && this._objsDict.ContainsKey(parentId) )
				{
					return this._objsDict[parentId];
				}
			}

			// la versione linq rallenta l'esecuzione di ordini di grandezza
			//return ( from propParents in _parentsDict[grisObject.Id]
			//         join grisObjectEntry in _objsDict on propParents.Value equals grisObjectEntry.Key
			//         where propParents.Key == propertyName
			//         select grisObjectEntry.Value ).SingleOrDefault();

			return default(TEntity);
		}

		public List<TEntity> GetChildren ( TEntity entity, string propertyName )
		{
			IDictionary<string, List<TKey>> propChildren;
			this._childrenDict.TryGetValue(entity.Id, out propChildren);
			if ( propChildren != null )
			{
				List<TKey> childrenIds;
				propChildren.TryGetValue(propertyName, out childrenIds);
				if ( childrenIds != null )
				{
					var objects = new List<TEntity>();
					foreach ( var childrenId in childrenIds )
					{
						objects.Add(this._objsDict[childrenId]);
					}
					return objects;
				}
			}

			// la versione linq rallenta l'esecuzione di ordini di grandezza
			//return ( from propChildren in _childrenDict[grisObject.Id]
			//         from childId in propChildren.Value
			//         join grisObjectEntry in _objsDict on childId equals grisObjectEntry.Key
			//         where propChildren.Key == propertyName
			//         select grisObjectEntry.Value ).ToList();

			return new List<TEntity>(0);
		}

	    public void AddObject ( TEntity obj )
		{
			//if ( ( (IGrisObject) obj ).Name == "STLC1000" ) System.Diagnostics.Debugger.Break();

			foreach ( var memberInfo in obj.GetType().GetMembers() )
			{
			    var property = memberInfo as PropertyInfo;
			    if (property != null)
			    {
			        foreach ( var customAttribute in memberInfo.GetCustomAttributes(false) )
			        {
			            // selezione della proprietà marcata come Identificativo del Parent
			            var parentAttribute = customAttribute as ParentIdPropertyAttribute;
			            if (parentAttribute != null)
			            {
			                object propValue = property.GetValue(obj, null);

			                if ( propValue != null )
			                {
			                    TKey parentId;
			                    if ( propValue is TKey )
			                    {
			                        parentId = (TKey) propValue;
			                    }
			                    else
			                    {
			                        throw new EntityException(string.Format("Il tipo restituito dalla proprietà {0} dell' oggetto {1} non è del tipo atteso.", property.Name, obj.GetType()));
			                    }

                                this.AddObjectToParentDict(parentId, obj.Id, parentAttribute.ParentProperty);
                                this.AddObjectToChildrenDict(parentId, obj.Id, parentAttribute.RelatedChildrenProperty);
			                }
			            }
			        }
			    }
			}

		    lock ( _locker )
			{
				this._objsDict[obj.Id] = obj;
			}
		}

        private void AddObjectToParentDict ( TKey parentId, TKey objectId, string parentPropertyName )
        {
            lock (_locker)
            {
                IDictionary<string, TKey> propParentId;
                if (this._parentsDict.TryGetValue(objectId, out propParentId))
                {
                    propParentId[parentPropertyName] = parentId;
                }
                else
                {
                    this._parentsDict.Add(objectId, new Dictionary<string, TKey> { { parentPropertyName, parentId } });
                }
            }
        }

        private void AddObjectToChildrenDict ( TKey parentId, TKey objectId, string childPropertyName )
        {
            lock (_locker)
            {
                IDictionary<string, List<TKey>> propChildrenIdList;
                if (this._childrenDict.TryGetValue(parentId, out propChildrenIdList))
                {
                    List<TKey> childrenIdList;
                    if (propChildrenIdList.TryGetValue(childPropertyName, out childrenIdList))
                    {
                        if (!childrenIdList.Contains(objectId)) childrenIdList.Add(objectId);
                    }
                    else
                    {
                        propChildrenIdList.Add(childPropertyName, new List<TKey> { objectId });
                    }
                }
                else
                {
                    this._childrenDict.Add(parentId, new Dictionary<string, List<TKey>> { { childPropertyName, new List<TKey> { objectId } } });
                }
            }
        }

		public void AddObjects ( IEnumerable<TEntity> objects )
		{
			foreach ( var obj in objects ) this.AddObject(obj);
		}

		public void AddChildObjects ( TEntity parentObj, TEntity childObj )
		{
			this.AddObject(childObj);

			lock ( _locker )
			{
				this._objsDict[parentObj.Id] = parentObj;
			}
		}

	    public ICollection<TEntity> CachedObjects
		{
			get { return this._objsDict.Values; }
		}

        public void AddRelatedObject ( TEntity obj, TEntity relatedObj )
        {
            if (obj == null) throw new ArgumentException("Il parametro 'obj' non può essere nullo", "obj");
            if (relatedObj == null) throw new ArgumentException("Il parametro 'relatedObj' non può essere nullo", "relatedObj");

            this.AddRelatedObject(obj.Id, relatedObj.Id);

            lock (_locker)
            {
                this._objsDict[obj.Id] = obj;
                this._objsDict[relatedObj.Id] = relatedObj;
            }
        }

	    public void AddRelatedObject ( TKey objId, TKey relatedObjId )
	    {
            if (objId == null) throw new ArgumentException("Il parametro 'objId' non può essere nullo", "objId");
            if (relatedObjId == null) throw new ArgumentException("Il parametro 'relatedObjId' non può essere nullo", "relatedObjId");

            this.AddObjectToChildrenDict(objId, relatedObjId, "Related");
        }

	    public void AddRelatedObjects(IEnumerable<Tuple<TEntity, TEntity>> relatedObjs)
        {
            foreach (var relObjs in relatedObjs)
                this.AddRelatedObject(relObjs.Item1, relObjs.Item2);
        }

        public void AddRelatedObjects(IEnumerable<Tuple<TKey, TKey>> relatedObjIds)
        {
            foreach (var relObjIds in relatedObjIds)
                this.AddRelatedObject(relObjIds.Item1, relObjIds.Item2);
        }

		public void Reset ()
		{
			this._objsDict.Clear();
			this._parentsDict.Clear();
			this._childrenDict.Clear();
		}
	}
}

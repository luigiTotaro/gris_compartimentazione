﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;
using GrisSuite.FormulaEngine.Model.Entities;
using NLog;

namespace GrisSuite.FormulaEngine.Model
{
	public static class ObjectContext
	{
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
		public static List<IServer> GetServers ( bool lookupInCacheOnly = false )
		{
			var servers = (from cachedObject in GrisObject.Cache.CachedObjects
						  where cachedObject is IServer
						  select (IServer) cachedObject).ToList();

			if ( !servers.Any() && !lookupInCacheOnly )
			{
				servers = Entities.Server.GetAll();
			}

			return servers;
		}

		public static void SaveObject ( IStorable objectToSave )
		{
			if ( objectToSave.HasChanges ) new Ds().Entity().Exec(objectToSave.GetUpdateCommand());
		}

		public static void SaveBranch ( IStorable branchRoot )
		{
			List<string> updateCommands = new List<string>(30000);

			FetchAndGetUpdate(branchRoot, updateCommands);

			if ( updateCommands.Count > 0 )
			{
                IGrisObject grisObj = branchRoot as IGrisObject;
                if (grisObj != null) {
                    _logger.Info("Ramo {0} inizio salvataggio di {1} oggetti.", grisObj.Name, updateCommands.Count);
                }
                IDataEntity data = new Ds().Entity();
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(data.CommandTimeout + 30)))
				{
                    data.Exec(string.Join(Environment.NewLine, updateCommands));
                    transaction.Complete();
				}
			}
		}

		private static void FetchAndGetUpdate ( IStorable node, IList<string> updateCommands )
		{
			if ( node != null )
			{
				//var o = node as GrisObject;
				//if ( ( o != null ) && ( o.SpecificId == 284408550850567 ) )
				//{
				//    System.Diagnostics.Debugger.Break();
				//}

				if ( node.HasChanges ) updateCommands.Add(node.GetUpdateCommand());

				var storableNodes = node.StorableNodes;
				if ( storableNodes != null && storableNodes.Count > 0 )
				{
					foreach ( var subNode in storableNodes )
					{
						FetchAndGetUpdate(subNode, updateCommands);
					}
				}
			}
		}
	}
}

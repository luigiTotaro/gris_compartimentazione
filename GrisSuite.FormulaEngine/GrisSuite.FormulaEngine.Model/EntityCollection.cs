﻿
using System;
using System.Collections.Generic;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Severities;

namespace GrisSuite.FormulaEngine.Model
{
	/// <summary>
	/// Classe wrapper che permette il caricamento ritardato della collezione di entità figlie.
	/// </summary>
	/// <typeparam name="TEntity">Tipo derivante da IGrisObject</typeparam>
	public class EntityCollection<TEntity> : List<TEntity>, IEntityCollection<TEntity>
		where TEntity : class, IGrisObject
	{
		private readonly IGrisObject _parentObject;
		private readonly string _propertyName;
		private readonly Func<IDictionary<string, object>, List<TEntity>> _entitiesGetter;

		/// <summary>
		/// Costruttore della collezione.
		/// </summary>
		/// <param name="parentObject">Istanza dell' oggetto a cui appartiene la collezione di entità.</param>
		/// <param name="propertyName">Nome della proprietà dell' oggetto parent che espone la collezione di entità.</param>
		/// <param name="entities">Lista di entità da caricare.</param>
		public EntityCollection ( IGrisObject parentObject, string propertyName, IEnumerable<TEntity> entities )
		{
			if ( parentObject == null ) throw new ArgumentException("Il parametro 'parentObject' non può essere nullo", "parentObject");
			if ( entities == null ) throw new ArgumentException("Il parametro 'entities' non può essere nullo", "entities");
			var ents = entities.ToList();

			this._parentObject = parentObject;
			this._propertyName = propertyName;
			this.AddRange(ents);

			this._parentObject.ObjectCache.AddObjects(new List<TEntity>(ents).ConvertAll(ent => (IGrisObject) ent));
		}

		/// <summary>
		/// Costruttore della collezione.
		/// </summary>
		/// <param name="parentObject">Istanza dell' oggetto a cui appartiene la collezione di entità.</param>
		/// <param name="propertyName">Nome della proprietà dell' oggetto parent che espone la collezione di entità.</param>
		/// <param name="entitiesGetter">Funzione che permette di caricare le entità nella fase di lazy loading.</param>
		public EntityCollection ( IGrisObject parentObject, string propertyName, Func<IDictionary<string, object>, List<TEntity>> entitiesGetter )
		{
			if ( parentObject == null ) throw new ArgumentException("Il parametro 'parentObject' non può essere nullo", "parentObject");

			this._parentObject = parentObject;
			this._propertyName = propertyName;
			this._entitiesGetter = entitiesGetter;
		}

		/// <summary>
		/// Carica la collezione dalla cache o dal database.
		/// </summary>
		/// <param name="lookupInCacheOnly">Indica se si vuole che il caricamento venga eseguito solo dalla cache, escludendo il database.</param>
		/// <remarks>
		/// La collezione è caricabile una sola volta durante il ciclo di vita della collezione stessa.
		/// </remarks>
		public void Load ( bool lookupInCacheOnly = false )
		{
			if ( !this.IsLoaded )
			{
				this.IsLoaded = true;
				var entities = new List<TEntity>();

				var cachedEntities = this._parentObject.GetCachedChildren(this._propertyName);
				if ( cachedEntities.Count > 0 )
					entities = cachedEntities.ConvertAll(go => (TEntity) go);

				if ( !lookupInCacheOnly && this._entitiesGetter != null )
				{
					var dbEntities = this._entitiesGetter(this._parentObject.InitializationValues);

					if ( dbEntities.Count > 0 )
					{
						var newEntities = dbEntities.Except(entities).ToList();
						this._parentObject.ObjectCache.AddObjects(newEntities);
						entities = entities.Concat(newEntities).ToList();
					}
				}

				if ( this._parentObject.CallerObject != null ) // l'oggetto padre prende parte al calcolo di una formula
				{
					// la routine di assegnazione CallerObject viene mantenuta qui perchè non dovrebbe mai capitare che la collezione non venga caricata almeno una volta, 
					// questo assicura che gli oggetti abbiano sempre un CallerObject
					foreach ( var entity in entities )
					{
						if ( entity.CallerObject == null ) entity.CallerObject = this._parentObject.CallerObject;
						if ( !entity.IsInitialized ) entity.Initialize();
					}
				}

				this.AddRange(entities.Except(this));
			}
		}

		/// <summary>
		/// Indica se la collezione è già stata caricata dalla cache o dal database.
		/// </summary>
		/// <remarks>
		/// La collezione è caricabile una sola volta durante il ciclo di vita della collezione stessa.
		/// </remarks>
		public bool IsLoaded { get; private set; }

		/// <summary>
		/// Membro non implementato.
		/// </summary>
		public TEntity Value
		{
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Permette di aggiungere un' entità alla collezione registrandolo nella cache.
		/// </summary>
		/// <param name="entity">Entità da caricare nella collection.</param>
		/// <param name="isDirectChild">Indica se l'entità passata è figlio diretto nella gerarchia GrisObject.</param>
		public void AddEntity ( TEntity entity, bool isDirectChild = true )
		{
			if ( entity == null ) throw new ArgumentException("Il parametro entity non è inizializzato.", "entity");

			// aggiunge l'oggetto figlio e lo memorizza in cache
			if ( !this.Contains(entity) )
			{
				if ( isDirectChild ) entity.ParentId = this._parentObject.Id;

				this.Add(entity);
				//this.IsLoaded = true;

				this._parentObject.ObjectCache.AddChildObjects(this._parentObject, entity);
			}
		}

		/// <summary>
		/// Permette di aggiungere più entità alla collezione registrandole nella cache.
		/// </summary>
		/// <param name="entities">Lista di entità da caricare nella collection.</param>
		public void AddEntities ( IList<TEntity> entities )
		{
			foreach ( var entity in entities )
			{
				this.AddEntity(entity);
			}
		}

		/// <summary>
		/// Metodo facility di cast tra 2 tipi derivati da IGrisObject.
		/// </summary>
		/// <typeparam name="TNewEntity">Nuovo tipo derivato da IGrisObject di cui si vuole ottenere una EntityCollection</typeparam>
		/// <returns></returns>
		public IEntityCollection<TNewEntity> Cast<TNewEntity> () where TNewEntity : class, IGrisObject
		{
			return new EntityCollection<TNewEntity>(this._parentObject, this._propertyName, this.ConvertAll(origEnt => (TNewEntity) (IGrisObject) origEnt));
		}

		#region Aggregation Methods

		#region Get aggregation methods

		/// <summary>
		/// Restituisce la lista degli elementi escludendo o includento alcuni elementi.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se restituire gli elementi rilevanti per la determinazione dello stato del padre.</param>
		/// <param name="exclusions">Indica la lista delle severità degli oggetti da escludere.</param>
		/// <param name="exclusionsByDetail">Indica la lista delle severità di dettaglio degli oggetti da escludere.</param>
		/// <returns>La lista degli elementi non esclusi.</returns>
		public List<TEntity> GetExcept ( bool withExcludedFromParentStatus = false, int[] exclusions = null, ObjectSeverityDetail[] exclusionsByDetail = null )
		{
			return this.Where(c => ( withExcludedFromParentStatus || !c.ExcludedFromParentStatus ) &&
				( !( exclusions ?? new int[0] ).Contains(c.ComparableSeverity) && !( exclusionsByDetail ?? new List<ObjectSeverityDetail>().ToArray() ).Contains(c.SeverityDetail) )
				).ToList();
		}

		/// <summary>
		/// Restituisce la lista di entità in una data severità.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		public List<TEntity> GetInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			return this.GetExcept(withExcludedFromParentStatus, exclusions).Where(c => c.ComparableSeverity == severity).ToList();
		}

		/// <summary>
		/// Restituisce la lista di entità in una data severità di dettaglio.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		public List<TEntity> GetInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			return this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions).Where(c => c.SeverityDetail == severityDetail).ToList();
		}

		/// <summary>
		/// Restituisce la lista di entità in una data severità o peggiore.
		/// </summary>
		/// <param name="severity">Severità in cui devono almeno essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		public List<TEntity> GetAtLeastInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			return this.GetExcept(withExcludedFromParentStatus, exclusions).Where(c => c.ComparableSeverity >= severity).ToList();
		}

		/// <summary>
		/// Restituisce la lista di entità in una data severità di dettaglio o peggiore.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono almeno essere le entità per essere estratte.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista risultante tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>Lista di entità risultante.</returns>
		public List<TEntity> GetAtLeastInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			return this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions).Where(c => c.SeverityDetail >= severityDetail).ToList();
		}

		/// <summary>
		/// Restituisce la severità peggiore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		public int GetWorstSeverity ( bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			var filteredChildren = this.GetExcept(withExcludedFromParentStatus, exclusions);
			if ( filteredChildren != null && filteredChildren.Count > 0 ) return filteredChildren.Max(c => c.ComparableSeverity);
			return -1;
		}

		/// <summary>
		/// Restituisce la severità di dettaglio peggiore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità di dettaglio contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		public ObjectSeverityDetail GetWorstSeverityDetail ( bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			var filteredChildren = this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions);
			// questa implementazione assume come severità di dettaglio "base" di una severità quella con Id minore perchè allo stato attuale questa regola è soddisfatta nei dati presenti in db
			if ( filteredChildren != null && filteredChildren.Count > 0 ) return Severity.GetBaseSeverityDetail(filteredChildren.Max(c => c.ComparableSeverity));
			return NotAvailable.SeverityDetail;
		}

		/// <summary>
		/// Restituisce la severità migliore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		public int GetBestSeverity ( bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			var filteredChildren = this.GetExcept(withExcludedFromParentStatus, exclusions);
			if ( filteredChildren != null && filteredChildren.Count > 0 ) return filteredChildren.Min(c => c.ComparableSeverity);
			return -1;
		}

		/// <summary>
		/// Restituisce la severità di dettaglio migliore tra quelle della lista di entità.
		/// </summary>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella ricerca le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla ricerca tutte le periferiche con severità di dettaglio contenuta nell'array di severità di dettaglio passato.</param>
		/// <returns>La severità di dettaglio peggiore</returns>
		public ObjectSeverityDetail GetBestSeverityDetail ( bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			var filteredChildren = this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions);
			// questa implementazione assume come severità di dettaglio "base" di una severità quella con Id minore perchè allo stato attuale questa regola è soddisfatta nei dati presenti in db
			if ( filteredChildren != null && filteredChildren.Count > 0 ) return Common.Severities.Severity.GetBaseSeverityDetail(filteredChildren.Min(c => c.ComparableSeverity));
			return NotAvailable.SeverityDetail;
		}

		#endregion

		#region Boolean aggregation methods

		/// <summary>
		/// Indica se esiste almeno un'entità nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		public bool AnyInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			return this.GetExcept(withExcludedFromParentStatus, exclusions).Any(c => c.ComparableSeverity == severity);
		}

		/// <summary>
		/// Indica se esiste almeno un'entità nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		public bool AnyInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			return this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions).Any(c => c.SeverityDetail == severityDetail);
		}

		/// <summary>
		/// Indica se esiste un'entità che sia almeno nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		public bool AnyAtLeastInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			return ( this.GetWorstSeverity(withExcludedFromParentStatus, exclusions) >= severity );
		}

		/// <summary>
		/// Indica se esiste un'entità che sia almeno nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se ne esiste almento una, altrimenti falso.</returns>
		public bool AnyAtLeastInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			return ( this.GetWorstSeverityDetail(withExcludedFromParentStatus, exclusions) >= severityDetail );
		}

		/// <summary>
		/// Indica se tutte le entità sono nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		public bool AreAllInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			int sevCount = this.GetInSeverity(severity, withExcludedFromParentStatus, exclusions).Count;
			return ( sevCount > 0 && sevCount == this.GetExcept(withExcludedFromParentStatus, exclusions).Count );
		}

		/// <summary>
		/// Indica se tutte le entità sono nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		public bool AreAllInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			int sevCount = this.GetInSeverity(severityDetail, withExcludedFromParentStatus, exclusions).Count;
			return ( sevCount > 0 && sevCount == this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions).Count );
		}

		/// <summary>
		/// Indica se tutte le entità sono almeno nella severità passata.
		/// </summary>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		public bool AreAllAtLeastInSeverity ( int severity, bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			int sevCount = this.GetAtLeastInSeverity(severity, false, exclusions).Count;
			return ( sevCount > 0 && sevCount == this.GetExcept(withExcludedFromParentStatus, exclusions).Count );
		}

		/// <summary>
		/// Indica se tutte le entità sono almeno nella severità di dettaglio passata.
		/// </summary>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se soddisfano tutte, altrimenti falso.</returns>
		public bool AreAllAtLeastInSeverity ( ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			int sevCount = this.GetAtLeastInSeverity(severityDetail, false, exclusions).Count;
			return ( sevCount > 0 && sevCount == this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions).Count );
		}

		/// <summary>
		/// Indica se la data percentuale di entità è nella severità passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità passata.</param>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		public bool AreXPercInSeverity ( float percentage, int severity, bool withExcludedFromParentStatus = false )
		{
			return ( this.Count > 0 ) && ( (float) this.GetInSeverity(severity).Count / this.GetExcept(withExcludedFromParentStatus).Count * 100 ) > percentage;
		}

		/// <summary>
		/// Indica se la data percentuale di entità è nella severità di dettaglio passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità di dettaglio passata.</param>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		public bool AreXPercInSeverity ( float percentage, ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false )
		{
			return ( this.Count > 0 ) && ( (float) this.GetInSeverity(severityDetail).Count / this.GetExcept(withExcludedFromParentStatus).Count * 100 ) > percentage;
		}

		/// <summary>
		/// Indica se la data percentuale di entità è almeno nella severità passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità passata.</param>
		/// <param name="severity">Severità in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità contenuta nell'array di severità passato.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		public bool AreXPercAtLeastInSeverity ( float percentage, int severity, bool withExcludedFromParentStatus = false, params int[] exclusions )
		{
			return ( this.Count > 0 ) && ( (float) this.GetAtLeastInSeverity(severity, false, exclusions).Count / this.GetExcept(withExcludedFromParentStatus, exclusions).Count * 100 ) > percentage;
		}

		/// <summary>
		/// Indica se la data percentuale di entità è almeno nella severità di dettaglio passata.
		/// </summary>
		/// <param name="percentage">E' la percentuale di entità che devono avere la severità di dettaglio passata.</param>
		/// <param name="severityDetail">Severità di dettaglio in cui devono essere le entità per essere considerate.</param>
		/// <param name="withExcludedFromParentStatus">Indica se includere nella lista le entità marcate come 'Escluse dal calcolo di stato del padre'.</param>
		/// <param name="exclusions">Saranno escluse dalla lista considerata tutte le periferiche con severità di dettaglio contenuta nell'array di severità passato.</param>
		/// <returns>Vero se viene superata la percentuale, altrimenti falso.</returns>
		public bool AreXPercAtLeastInSeverity ( float percentage, ObjectSeverityDetail severityDetail, bool withExcludedFromParentStatus = false, params ObjectSeverityDetail[] exclusions )
		{
			return ( this.Count > 0 ) && ( (float) this.GetAtLeastInSeverity(severityDetail, false, exclusions).Count / this.GetExcept(withExcludedFromParentStatus, exclusionsByDetail: exclusions).Count * 100 ) > percentage;
		}

		public bool HasNotExcluded ()
		{
			return this.Any(c => !c.ExcludedFromParentStatus);
		} 

		#endregion

		#endregion
	}
}

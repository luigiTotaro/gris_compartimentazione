﻿
using System;
using System.Data;

namespace GrisSuite.FormulaEngine.Model.DataAccess
{
	public static class DataAccessUtility
	{
		#region Extending IDataReader

		public static bool IsDbNull ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName));
		}

		public static string GetString ( this IDataReader dr, string fieldName )
		{
			return dr.GetString(dr.GetOrdinal(fieldName));
		}

		public static string GetNullableString ( this IDataReader dr, string fieldName )
		{
			return dr.IsDbNull(fieldName) ? null : (string) dr[fieldName];
		}

        public static string GetEmptyNullString(this IDataReader dr, string fieldName)
        {
            return dr.IsDbNull(fieldName) ? String.Empty : (string)dr[fieldName];
        }

		public static short GetInt16 ( this IDataReader dr, string fieldName )
		{
			return dr.GetInt16(dr.GetOrdinal(fieldName));
		}

		public static short? GetNullableInt16 ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (short?) null : dr.GetInt16(dr.GetOrdinal(fieldName));
		}

		public static int GetInt32 ( this IDataReader dr, string fieldName )
		{
			return dr.GetInt32(dr.GetOrdinal(fieldName));
		}

		public static int? GetNullableInt32 ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (int?) null : dr.GetInt32(dr.GetOrdinal(fieldName));
		}

		public static long GetInt64 ( this IDataReader dr, string fieldName )
		{
			return dr.GetInt64(dr.GetOrdinal(fieldName));
		}

		public static long? GetNullableInt64 ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (long?) null : dr.GetInt64(dr.GetOrdinal(fieldName));
		}

		public static decimal GetDecimal ( this IDataReader dr, string fieldName )
		{
			return dr.GetDecimal(dr.GetOrdinal(fieldName));
		}

		public static decimal? GetNullableDecimal ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (decimal?) null : dr.GetDecimal(dr.GetOrdinal(fieldName));
		}

		public static Guid GetGuid ( this IDataReader dr, string fieldName )
		{
			return dr.GetGuid(dr.GetOrdinal(fieldName));
		}

		public static Guid? GetNullableGuid ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (Guid?) null : dr.GetGuid(dr.GetOrdinal(fieldName));
		}

		public static DateTime GetDateTime ( this IDataReader dr, string fieldName )
		{
			return dr.GetDateTime(dr.GetOrdinal(fieldName));
		}

		public static DateTime? GetNullableDateTime ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (DateTime?) null : dr.GetDateTime(dr.GetOrdinal(fieldName));
		}

		public static bool GetBoolean ( this IDataReader dr, string fieldName )
		{
			return dr.GetBoolean(dr.GetOrdinal(fieldName));
		}

		public static bool? GetNullableBoolean ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (bool?) null : dr.GetBoolean(dr.GetOrdinal(fieldName));
		}

		public static byte GetByte ( this IDataReader dr, string fieldName )
		{
			return dr.GetByte(dr.GetOrdinal(fieldName));
		}

		public static byte? GetNullableByte ( this IDataReader dr, string fieldName )
		{
			return dr.IsDBNull(dr.GetOrdinal(fieldName)) ? (byte?) null : dr.GetByte(dr.GetOrdinal(fieldName));
		}

		public static byte[] GetBytes ( this IDataReader dr, string fieldName )
		{
			return (byte[])dr[dr.GetOrdinal(fieldName)];
		}

		#endregion

		#region Extending DataRow

		public static string GetString ( this DataRow dr, string fieldName )
		{
			return (string) dr[fieldName];
		}

		public static string GetNullableString ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (string) dr[fieldName];
		}

		public static short GetInt16 ( this DataRow dr, string fieldName )
		{
			return (short) dr[fieldName];
		}

		public static short? GetNullableInt16 ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (short?) dr[fieldName];
		}

		public static int GetInt32 ( this DataRow dr, string fieldName )
		{
			return (int) dr[fieldName];
		}

		public static int? GetNullableInt32 ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (int?) dr[fieldName];
		}

		public static long GetInt64 ( this DataRow dr, string fieldName )
		{
			return (long) dr[fieldName];
		}

		public static long? GetNullableInt64 ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (long?) dr[fieldName];
		}

		public static Guid GetGuid ( this DataRow dr, string fieldName )
		{
			return (Guid) dr[fieldName];
		}

		public static Guid? GetNullableGuid ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (Guid?) dr[fieldName];
		}

		public static DateTime GetDateTime ( this DataRow dr, string fieldName )
		{
			return (DateTime) dr[fieldName];
		}

		public static DateTime? GetNullableDateTime ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (DateTime?) dr[fieldName];
		}

		public static bool GetBoolean ( this DataRow dr, string fieldName )
		{
			return (bool) dr[fieldName];
		}

		public static bool? GetNullableBoolean ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (bool?) dr[fieldName];
		}

		public static byte GetByte ( this DataRow dr, string fieldName )
		{
			return (byte) dr[fieldName];
		}

		public static byte? GetNullableByte ( this DataRow dr, string fieldName )
		{
			return dr.IsNull(fieldName) ? null : (byte?) dr[fieldName];
		}

		#endregion
	}
}

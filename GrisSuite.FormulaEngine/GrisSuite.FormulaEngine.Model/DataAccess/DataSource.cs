﻿
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Data.SqlClient;

namespace GrisSuite.FormulaEngine.Model.DataAccess
{
	interface IDataEntity
	{
		int CommandTimeout { get; set; }
		IDictionary<string, object> Parameters { get; set; }
		IEnumerable<T> Get<T> ( string command, Func<IDataReader, T> populateAction, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text );
		T Get<T> ( string command, T tds, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text ) where T : DataSet;
		IEnumerable<TEntity> Get<TEntity, TKey> ( string command, string key, Func<IGrouping<TKey, DataRow>, IDictionary<string, object>, TEntity> populateAction, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text, IDictionary<string, object> initializationValues = null );
        void Exec(string command = "", IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text);
		T ExecAndReturn<T> ( string command, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text );
	}

	sealed class DataSource : IDataEntity
	{
	    private readonly string _command;
		private int _commandTimeout;
		public int CommandTimeout   
		{
			get
			{
				if ( this._commandTimeout > 0 ) return _commandTimeout;
				if ( !int.TryParse(ConfigurationManager.AppSettings["dalCommandTimeout"], out this._commandTimeout) )
				{
					this._commandTimeout = 90;
				}
				return this._commandTimeout;
			}
			set { this._commandTimeout = value; }
		}

		public IDictionary<string, object> Parameters { get; set; }

	    public DataSource() {}
	    public DataSource(string command) { this._command = command; }

		#region DataReader Queries

        public IEnumerable<TEntity> Get<TEntity>(string command, Func<IDataReader, TEntity> populateAction, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text)
        {
            this.EnsureConnection(connectionName);
            command = this.EnsureCommand(command);

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString))
            using (var cmd = new SqlCommand(command, conn))
            {
                IDataReader reader = null;
                IList<TEntity> results = new List<TEntity>();

                if (parameters == null) parameters = this.Parameters;
                if (parameters != null) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value ?? DBNull.Value)).ToArray());

                cmd.CommandTimeout = this.CommandTimeout;
                cmd.CommandType = commandType;

                conn.Open();

                try
                {
                    reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        results.Add(populateAction(reader));
                    }

                    return results;
                }
                finally
                {
                    if (reader != null) reader.Dispose();
                }
            }
        }

		#endregion

		#region DataSet Queries

		public TEntity Get<TEntity> ( string command, TEntity tds, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text ) where TEntity : DataSet
		{
            this.EnsureConnection(connectionName);
            command = this.EnsureCommand(command);

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString))
			using ( var cmd = new SqlCommand(command, conn) )
			{
				if ( parameters == null ) parameters = this.Parameters;
				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value ?? DBNull.Value)).ToArray());

				cmd.CommandTimeout = this.CommandTimeout;
				cmd.CommandType = commandType;

				IDataAdapter da = new SqlDataAdapter(cmd);

				da.Fill(tds);
				return tds;
			}
		}

		#endregion

		#region Grouping Queries

		public IEnumerable<TEntity> Get<TEntity, TKey> ( string command, string key, Func<IGrouping<TKey, DataRow>, IDictionary<string, object>, TEntity> populateAction, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text, IDictionary<string, object> initializationValues = null )
		{
            this.EnsureConnection(connectionName);
            command = this.EnsureCommand(command);

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString))
			using ( var cmd = new SqlCommand(command, conn) )
			{
				IDataAdapter adapter = new SqlDataAdapter(cmd);
				DataSet tempDs = new DataSet();

				if ( parameters == null ) parameters = this.Parameters;
				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value ?? DBNull.Value)).ToArray());

				cmd.CommandTimeout = this.CommandTimeout;
				cmd.CommandType = commandType;

				adapter.Fill(tempDs);

				var groupedEntities = ( from DataRow row in tempDs.Tables[0].Rows//.AsParallel()
										group row by (TKey) row[key] into groupedEntity
										select groupedEntity );

				var results = new BlockingCollection<TEntity>();

				foreach ( var groupedEntity in groupedEntities )
				{
					results.Add(populateAction(groupedEntity, initializationValues));
				}

				return results.ToList();
			}
		}

		#endregion

		#region Commands

		public void Exec ( string command = "", IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text )
		{
            this.EnsureConnection(connectionName);
            command = this.EnsureCommand(command);

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString))
			using ( var cmd = new SqlCommand(command, conn) )
			{
				if ( parameters == null ) parameters = this.Parameters;
				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value ?? DBNull.Value)).ToArray());
				cmd.CommandTimeout = this.CommandTimeout;
				cmd.CommandType = commandType;

				conn.Open();
				cmd.ExecuteNonQuery();
				conn.Close();
			}
		}

		public T ExecAndReturn<T> ( string command, IDictionary<string, object> parameters = null, string connectionName = "Main", CommandType commandType = CommandType.Text )
		{
            this.EnsureConnection(connectionName);
            command = this.EnsureCommand(command);

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString))
			using ( var cmd = new SqlCommand(command, conn) )
			{
				if ( parameters == null ) parameters = this.Parameters;
				if ( parameters != null ) cmd.Parameters.AddRange(parameters.Select(par => new SqlParameter(par.Key, par.Value ?? DBNull.Value)).ToArray());
				cmd.CommandTimeout = this.CommandTimeout;
				cmd.CommandType = commandType;

				conn.Open();
				T ret = (T) cmd.ExecuteScalar();
				conn.Close();

				return ret;
			}
		}

		#endregion

        #region Utilities

        private string EnsureCommand(string command)
        {
            if (string.IsNullOrEmpty(command))
            {
                if (string.IsNullOrEmpty(this._command))
                {
                    throw new EntityException("Nessun comando specificato.");
                }

                command = this._command;
            }

            return command;
        }

        private void EnsureConnection(string connectionName)
        {
            if (ConfigurationManager.ConnectionStrings[connectionName] == null) throw new EntityException(string.Format("Impossibile trovare la stringa di connessione '{0}' nel file di configurazione", connectionName));
        }

        #endregion
    }

	sealed class Ds : Dictionary<string, object>
	{
		public IDataEntity Entity ()
		{
			return new DataSource { Parameters = this };
		}
	}
}
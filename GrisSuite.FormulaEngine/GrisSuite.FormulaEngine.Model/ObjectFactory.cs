﻿
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Model
{
	public class ObjectFactory : IObjectFactory
	{
		public IObjectAttributeType GetAttributeType ()
		{
			return new Entities.ObjectAttributeType();
		}

		public IObjectAttribute GetObjectAttribute ()
		{
			return new Entities.ObjectAttribute();
		}

		public IGrisObject GetGrisObject ()
		{
			return new GrisObject();
		}

		public IServer GetServer ()
		{
			return new Entities.Server();
		}

		public IParameter GetParameter ()
		{
			return new Entities.Parameter();
		}

		public ISeverityRange GetSeverityRange ()
		{
			return new Entities.SeverityRange();
		}
	}
}

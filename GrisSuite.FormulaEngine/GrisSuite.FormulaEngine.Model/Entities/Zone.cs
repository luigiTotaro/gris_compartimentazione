﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class Zone : GrisObject, IZone
	{
		public Zone ( IDictionary<string, object> initializationValues = null ) : base(initializationValues) { }

		#region Primitive Members

		private Guid _regionId;
		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Region", RelatedChildrenProperty = "Zones")]
		public Guid RegionId
		{
			get { return _regionId; }
			set
			{
				this._regionId = value;
				this.RaisePropertyChanged("RegionId", value);
			}
		}

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public long ZoneId { get; set; }

		#endregion

		#region Navigation Members

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IRegion Region
		{
			get { return this.RegionReference.Value; }
			set { this.RegionId = value.Id; this.RegionReference = new EntityParent<IRegion>(this, "Parent", value); }
		}

		private IEntityEnd<IRegion> _regionRef;
		public IEntityEnd<IRegion> RegionReference
		{
			get { return ( this._regionRef.IsLoaded ) ? this._regionRef : this.RetrieveParent(this._regionRef); }
			set { this._regionRef = value; }
		}

		private IEntityCollection<INode> _nodes;

		/// <summary>
		/// Imposta la relazione con un oggetto Region.
		/// </summary>
		/// <param name="region">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		public void SetRegion ( IRegion region, bool isDirectChild )
		{
			if ( region == null ) throw new ArgumentException("Il parametro 'region' non può essere nullo.", "region");
			this.RegionId = region.Id;
			this.RegionReference = new EntityParent<IRegion>(this, "Regione", region, isDirectChild);
		}

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public IEntityCollection<INode> Nodes
		{
			get { return ( this._nodes.IsLoaded ) ? this._nodes : this.RetrieveChildCollection(this._nodes); }
			set { _nodes = value; }
		}

		#endregion

		#region Aggregation Members

		#region Nodes

		public INode GetNodeByName ( string name )
		{
			return this.Nodes.SingleOrDefault(n => n.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase));
		}

		public IList<INode> GetNodesByName ( string name )
		{
			return this.Nodes.Where(n => n.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region NodeSystems
		#endregion

		#region Devices
		#endregion

		#region Servers

		public IList<IServer> GetMonitoringServers ()
		{
			return ( from node in this.Nodes
					 from device in node.Devices
                     select device.Server).Where(srv => srv != null).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
            var srvs = (from server in this.GetMonitoringServers()
                        where server.SevLevel != Common.Severities.Severity.NotActive && server.SevLevel != Common.Severities.Severity.NotClassified
                        select server).ToList();

            int numTot = srvs.Count();
            int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
            return (numOffline > 0 && numOffline == numTot);
		}

        public bool AreAllObjectsByParentUnknown ()
        {
            var children = (from c in this.Children
                        where c.SevLevel != Common.Severities.Severity.NotActive && c.SevLevel != Common.Severities.Severity.NotClassified
                        select c).ToList();

            int numTot = children.Count();
            int numUnknown = children.Count(c => c.SevLevel == Unknown.Severity);
            return (numUnknown > 0 && numUnknown == numTot);            
        }

		#endregion

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._nodes = new EntityCollection<INode>(this, "Nodes", init => Node.GetNodesByZone((Guid) e.NewValue, init));
							break;
						}
					case "RegionId":
						{
							this._regionRef = new EntityParent<IRegion>(this, "Region", init => Entities.Region.GetById(this.RegionId, init));
							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    z.ZonID, z.RegID as ZonRegID, z.Name as ZonName, oszr.ObjectStatusId as ZonGRegID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join zones z on z.ZonID = os.ObjectId and os.ObjectTypeId = 2
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
	left join object_status oszr on z.RegID = oszr.ObjectId and oszr.ObjectTypeId = 1
{0};
";

		public new static List<IZone> GetAll ( IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
			return new Ds().Entity().Get<IZone, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		public new static IZone GetById ( Guid id, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
			return new Ds { { "Id", id } }.Entity().Get<IZone, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
		}

		public static List<IZone> GetZonesByRegion ( Guid regionId, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where oszr.ObjectStatusId = @RegionId and d.[Type] <> 'NoDevice'");
			return new Ds { { "RegionId", regionId } }.Entity().Get<IZone, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		internal new static Zone Map ( IGrouping<Guid, DataRow> groupedEntity, IDictionary<string, object> initializationValues )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var zone = new Zone(initializationValues);

				PopulateObject(firstRow, zone, groupedEntity.Key);
				PopulateNode(firstRow, zone);
				// attributes
				PopulateAttributes(groupedEntity, zone);
				// formulas
				PopulateFormulas(groupedEntity, zone);

				return zone;
			}

			return null;
		}

		internal static void PopulateNode ( DataRow source, IZone zone )
		{
			zone.ZoneId = source.GetInt64("ZonID");
			zone.RegionId = source.GetGuid("ZonGRegID");
			zone.Name = source.GetString("ZonName");
		}

		#endregion
	}
}

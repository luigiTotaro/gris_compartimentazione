﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class Device : GrisObject, IDevice
	{
		public Device ( IDictionary<string, object> initializationValues = null ) : base(initializationValues) { }

		#region Primitive Members
		
		/// <summary>
		/// Il tipo della periferica.
		/// </summary>
		public string Type { get; set; }

		/// <summary>
		/// Numero Seriale della periferica.
		/// </summary>
		public string SerialNumber { get; set; }

		/// <summary>
		/// Indirizzo IP o seriale.
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Indica se la diagnostica è attiva.
		/// </summary>
		public byte? IsActive { get; set; }

		/// <summary>
		/// Indica se è schedulata.
		/// </summary>
		public byte? IsScheduled { get; set; }

		/// <summary>
		/// Versione della definizione.
		/// </summary>
		public string DefinitionVersion { get; set; }

		/// <summary>
		/// Versione della definizione di protocollo.
		/// </summary>
		public string ProtocolDefinitionVersion { get; set; }

		/// <summary>
		/// Severità restituita dalla periferia, così come è sul database, tabella device_status.
		/// </summary>
		public int? ActualSevLevel { get; set; }

		/// <summary>
		/// Descrizione.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Indica se la periferica è offline.
		/// </summary>
		public byte? IsOffline { get; set; }

		/// <summary>
		/// Descrizione del tipo di periferica.
		/// </summary>
		public string DeviceTypeDescription { get; set; }

        /// <summary>
        /// Descrizione della modalità di discovery del tipo di periferica.
        /// </summary>
        public DiscoveryType DeviceTypeDiscovery { get; set; }

		/// <summary>
		/// Url a cui risponde il Web Service che restituisce informazioni diagnostiche sulla periferica.
		/// </summary>
		public string WSUrlPattern { get; set; }

		/// <summary>
		/// Identificativo di Tecnologia.
		/// </summary>
		public int? TechnologyId { get; set; }

		/// <summary>
		/// Identificativo specifico di periferica.
		/// </summary>
		public long? DeviceId { get; set; }

	    private bool? _isAcknowledged;
	    /// <summary>
	    /// Indica se sulla periferica è settato un ack.
	    /// </summary>
	    public bool IsAcknowledged
	    {
	        get
	        {
	            if (!this._isAcknowledged.HasValue) this._isAcknowledged = GetAck(this.Id);
	            return this._isAcknowledged.Value;
	        }
	    }

	    /// <summary>
        /// Indica se deve essere inviata una notifica via email.
        /// </summary>
        public bool ShouldSendNotificationByEmail { get; set; }

		private Guid? _serverId;
		/// <summary>
		/// Identificativo del Server.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Server", RelatedChildrenProperty = "Devices")]
		public Guid? ServerId
		{
			get { return this._serverId; }
			set
			{
				this._serverId = value;
				this.RaisePropertyChanged("ServerId", value);
			}
		}

		private Guid? _nodeSystemId;
		/// <summary>
		/// Identificativo del Sistema.
		/// </summary>
		[ParentIdProperty(ParentProperty = "System", RelatedChildrenProperty = "Devices")]
		public Guid? NodeSystemId
		{
			get { return _nodeSystemId; }
			set
			{
				_nodeSystemId = value;
				this.RaisePropertyChanged("NodeSystemId", value);
			}
		}

		private Guid? _nodeId;
		/// <summary>
		/// Identificativo della stazione.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Node", RelatedChildrenProperty = "Devices")]
		public Guid? NodeId
		{
			get { return _nodeId; }
			set
			{
				_nodeId = value;
				this.RaisePropertyChanged("NodeId", value);
			}
		}

		public Guid? PortId { get; set; }

		#endregion

		#region Navigation Members

		/// <summary>
		/// Istanza del Server che monitora la periferica.
		/// </summary>
		public IServer Server
		{
			get { return this.ServerReference.Value; }
			set { this.ServerId = value.Id; this.ServerReference = new EntityParent<IServer>(this, "Server", value, false); }
		}

		private IEntityEnd<IServer> _serverRef;
		public IEntityEnd<IServer> ServerReference
		{
			get { return ( this._serverRef.IsLoaded ) ? this._serverRef : this.RetrieveParent(this._serverRef); }
			set { this._serverRef = value; }
		}

		/// <summary>
		/// Imposta la relazione con un oggetto Server.
		/// </summary>
		/// <param name="server">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		public void SetServer ( IServer server, bool isDirectChild )
		{
			if ( server == null ) throw new ArgumentException("Il parametro 'server' non può essere nullo.", "server");
			this.ServerId = server.Id;
			this.ServerReference = new EntityParent<IServer>(this, "Server", server, isDirectChild);
		}

		/// <summary>
		/// Istanza del Sistema sotto cui è compresa la periferica.
		/// </summary>
		public INodeSystem System
		{
			get { return this.SystemReference.Value; }
			set { this.NodeSystemId = value.Id; this.SystemReference = new EntityParent<INodeSystem>(this, "System", value); }
		}

		private IEntityEnd<INodeSystem> _systemRef;
		public IEntityEnd<INodeSystem> SystemReference
		{
			get { return ( this._systemRef.IsLoaded ) ? this._systemRef : this.RetrieveParent(this._systemRef); }
			set { this._systemRef = value; }
		}

		/// <summary>
		/// Imposta la relazione con un oggetto NodeSystem.
		/// </summary>
		/// <param name="system">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		public void SetSystem ( INodeSystem system, bool isDirectChild )
		{
			if ( system == null ) throw new ArgumentException("Il parametro 'system' non può essere nullo.", "system");
			this.NodeSystemId = system.Id; 
			this.SystemReference = new EntityParent<INodeSystem>(this, "System", system, isDirectChild);
		}

		/// <summary>
		/// Istanza della Stazione sotto cui è compresa la periferica.
		/// </summary>
		public INode Node
		{
			get { return this.NodeReference.Value; }
			set { this.NodeId = value.Id; this.NodeReference = new EntityParent<INode>(this, "Node", value, false); }
		}

		private IEntityEnd<INode> _nodeRef;
		public IEntityEnd<INode> NodeReference
		{
			get { return ( this._nodeRef.IsLoaded ) ? this._nodeRef : this.RetrieveParent(this._nodeRef); }
			set { this._nodeRef = value; }
		}

		private IList<IStream> _streams = null;

		/// <summary>
		/// Imposta la relazione con un oggetto Node.
		/// </summary>
		/// <param name="node">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		public void SetNode ( INode node, bool isDirectChild )
		{
			if ( node == null ) throw new ArgumentException("Il parametro 'node' non può essere nullo.", "node");
			this.NodeId = node.Id;
			this.NodeReference = new EntityParent<INode>(this, "Node", node, isDirectChild);
		}

		/// <summary>
		/// Collezione degli streams relativi alla periferica.
		/// </summary>
		public IList<IStream> Streams
		{
			get { return this._streams ?? ( this._streams = Stream.GetStreamsByDevice(this.Id) ); }
			set { _streams = value; }
		}

		public IPort Port { get; set; }

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "ServerId":
						{
							this._serverRef = new EntityParent<IServer>(this, "Server", init => Entities.Server.GetById((Guid) e.NewValue, init));
							break;
						}
					case "NodeSystemId":
						{
							this._systemRef = new EntityParent<INodeSystem>(this, "System", init => NodeSystem.GetById((Guid) e.NewValue, init));
							break;
						}
					case "NodeId":
						{
							this._nodeRef = new EntityParent<INode>(this, "Node", init => Entities.Node.GetById((Guid) e.NewValue, init));
							break;
						}
				}
			}
		}

		#endregion

		#region Aggregation Members

		#region Streams

		public IStream GetStreamByName ( string name )
		{
			return this.GetChildByName(name);
		}

		public IList<IStream> GetStreamsByName ( string name )
		{
			return this.Streams.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		// necessario per python
		public new IStream GetChildByName ( string name )
		{
			return this.Streams.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
		}

		#endregion

		#region Events

		/// <summary>
		/// Ottiene le istanze degli eventi della periferica nel periodo indicato.
		/// </summary>
		public IList<IEvent> GetRaisedEvents ( DateTime dateFrom, DateTime dateTo )
		{
			return Event.GetEventsByDeviceAndPeriod(this, dateFrom, dateTo);
		}

		#endregion

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    d.DevID, d.NodID as DevNodID, d.SrvID as DevSrvID, d.Name as DevName, d.[Type], d.SN, d.Addr, d.PortId as DevPortID, d.Active, d.Scheduled, d.DefinitionVersion, d.ProtocolDefinitionVersion,
    ds.SevLevel as ActualSevLevel, ds.[Description], ds.[Offline], ds.ShouldSendNotificationByEmail,
    dt.DeviceTypeDescription, dt.Obsolete, dt.DefaultName, dt.PortType, dt.SnmpCommunity, isnull(dt.DiscoveryType, 1) as DiscoveryType,
    osdn.ObjectStatusId as DevGNodID, osds.ObjectStatusId as DevGSrvID, osdns.ObjectStatusId as DevGNodSysId
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join devices d on d.DevID = os.ObjectId and os.ObjectTypeId = 6
	left join device_status ds on ds.DevID = d.DevID
	left join device_type dt on dt.DeviceTypeID = d.[Type]
	left join node_systems ns on d.NodID = ns.NodId and dt.SystemID = ns.SystemId
	left join object_status osdn on d.NodID = osdn.ObjectId and osdn.ObjectTypeId = 3
	left join object_status osds on d.SrvID = osds.ObjectId and osds.ObjectTypeId = 5
	left join object_status osdns on ns.NodeSystemsId = osdns.ObjectId and osdns.ObjectTypeId = 4
{0};
";

		public new static List<IDevice> GetAll ( IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where [Type] <> 'NoDevice'");
			return new Ds().Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		public new static IDevice GetById ( Guid id, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, @"
where os.ObjectStatusId = @Id
	and [Type] <> 'NoDevice'
");
			return new Ds { { "Id", id } }.Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
		}

		public static List<IDevice> GetDevicesByServer ( Guid srvId, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where osds.ObjectStatusId = @ServerId and d.[Type] <> 'NoDevice'");
			return new Ds { { "ServerId", srvId } }.Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		public static List<IDevice> GetDevicesBySystem ( Guid sysId, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ParentObjectStatusId = @SystemId and d.[Type] <> 'NoDevice'");
			return new Ds { { "SystemId", sysId } }.Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		public static List<IDevice> GetDevicesByNode ( Guid nodeId, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where osdn.ObjectStatusId = @NodeId and d.[Type] <> 'NoDevice'");
			return new Ds { { "NodeId", nodeId } }.Entity().Get<IDevice, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

        public static bool GetAck ( Guid id )
        {
            string cmd = 
@"if exists (select *
from object_devices od
inner join device_ack da on od.ObjectId = da.DevID
where od.ObjectStatusId = @Id)
begin
	select 1;
end
else
begin
	select 0;
end";
            var o = new Ds {{"Id", id}}.Entity().ExecAndReturn<int>(cmd);
            return Convert.ToBoolean(o);
        }

		public override List<IGrisObject> GetChildren ( IDictionary<string, object> initializationValues = null )
		{
			throw new NotSupportedException();
		}

		internal new static Device Map ( IGrouping<Guid, DataRow> groupedEntity, IDictionary<string, object> initializationValues )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var dev = new Device(initializationValues);

				PopulateObject(firstRow, dev, groupedEntity.Key);
				PopulateDevice(firstRow, dev);
				// attributes
				PopulateAttributes(groupedEntity, dev);
				// formulas
				PopulateFormulas(groupedEntity, dev);

				return dev;
			}

			return null;
		}

		internal static void PopulateDevice ( DataRow source, IDevice device )
		{
			//if ( source.GetInt64("DevID") == 1128833480982535 )
			//{
			//    Debugger.Break();
			//}

			device.DeviceId = source.GetInt64("DevID");
			device.ServerId = source.GetNullableGuid("DevGSrvID");
			device.NodeId = source.GetNullableGuid("DevGNodID");
			device.NodeSystemId = source.GetNullableGuid("DevGNodSysId");
			device.Name = source.GetString("DevName");
            device.Type = source.GetString("Type");
            device.DeviceTypeDiscovery = (DiscoveryType)source.GetByte("DiscoveryType");
			device.SerialNumber = source.GetString("SN");
			device.Address = source.GetString("Addr");
			device.PortId = source.GetNullableGuid("DevPortID");
			device.IsActive = source.GetNullableByte("Active");
			device.IsScheduled = source.GetNullableByte("Scheduled");
			device.DefinitionVersion = source.GetNullableString("DefinitionVersion");
			device.ProtocolDefinitionVersion = source.GetNullableString("ProtocolDefinitionVersion");
			device.ActualSevLevel = source.GetNullableInt32("ActualSevLevel");
			device.Description = source.GetNullableString("Description");
			device.IsOffline = source.GetNullableByte("Offline");
            device.ShouldSendNotificationByEmail = Convert.ToBoolean(source.GetNullableByte("ShouldSendNotificationByEmail"));
		}

		#endregion

		#region Persistence

		public override IList<IStorable> StorableNodes
		{
			get
			{
				var stors = new List<IStorable>(20);
				if ( this.IsEvaluated ) stors.Add(this.Attributes);
				return stors;
			}
		}

		#endregion
	}
}

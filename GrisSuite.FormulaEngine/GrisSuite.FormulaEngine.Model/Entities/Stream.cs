﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class Stream : IStream
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo dello Stream.
		/// </summary>
		public int StreamId { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		public long DevId { get; set; }

		/// <summary>
		/// Nome dello Stream.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Visibilità dello Stream.
		/// </summary>
		public byte? Visible { get; set; }

		/// <summary>
		/// Dati binari dello Stream.
		/// </summary>
		public byte[] Data { get; set; }

		/// <summary>
		/// Data di ricezione dello Stream.
		/// </summary>
		public DateTime DateReceived { get; set; }

		/// <summary>
		/// Severità dello Stream come ricevuta dal collettore.
		/// </summary>
		public int? ActualSevLevel { get; set; }

		/// <summary>
		/// Indica se lo Stream è stato processato.
		/// </summary>
		public byte? Processed { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		public Guid DeviceId { get; set; }

		#endregion

		#region Navigation Members

		private IDevice _device;
		/// <summary>
		/// Istanza della periferica a cui appartiene lo stream.
		/// </summary>
		public IDevice Device
		{
			get { return this._device ?? ( this._device = Entities.Device.GetById(this.DeviceId) ); }
			set { this._device = value; }
		}

		private List<IStreamField> _streamFields;
		/// <summary>
		/// Istanze degli stream fields dello stream.
		/// </summary>
		public List<IStreamField> StreamFields
		{
			get { return this._streamFields ?? ( this._streamFields = StreamField.GetStreamFieldsByStream(this.DevId, this.StreamId) ); }
			set { this._streamFields = value; }
		}

		private List<IStreamHistory> _streamsHistory;
		/// <summary>
		/// Istanze Istanze degli streams dello StreamHistory.
		/// </summary>
		public List<IStreamHistory> StreamsHistory
		{
			get { return this._streamsHistory ?? ( this._streamsHistory = StreamHistory.GetStreamsHistoryByStream(this.DevId, this.StreamId) ); }
			set { this._streamsHistory = value; }
		}

		#endregion

		#region Aggregation Members

		#region Stream Fields

		public IStreamField GetStreamFieldByName ( string name )
		{
			return this.GetChildByName(name);
		}

		public IList<IStreamField> GetStreamFieldsByName ( string name )
		{
			return this.StreamFields.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		// necessario per python
		public IStreamField GetChildByName ( string name )
		{
			return this.StreamFields.SingleOrDefault(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase));
		}

		public bool IsInConditionForTimeSpan ( byte[] data, TimeSpan forPeriod )
		{
			Func<IStreamHistory, bool> pred = ( sh => sh.Data.SequenceEqual(data) );
			return this.IsInConditionForTimeSpan(pred, forPeriod);
		}

		public bool IsInConditionForTimeSpan ( int severity, TimeSpan forPeriod )
		{
			Func<IStreamHistory, bool> pred = ( sh => sh.ActualSevLevel == severity );
			return this.IsInConditionForTimeSpan(pred, forPeriod);
		}

		private bool IsInConditionForTimeSpan ( Func<IStreamHistory, bool> predicate, TimeSpan forPeriod )
		{
			var now = DateTime.Now;
			var streamFieldsHist = from streamHistory in this.StreamsHistory
								   where streamHistory.DateReceived <= now && streamHistory.DateReceived >= now - forPeriod
								   select streamHistory;
			return streamFieldsHist.All(predicate);
		}

		#endregion

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select 
	DevID, StrID, Name, Visible, Data, [DateTime], s.SevLevel as ActualSevLevel, [Description], Processed, os.ObjectStatusId as DeviceId
from 
	streams s
	inner join object_status os on s.DevID = os.ObjectId and os.ObjectTypeId = 6
{0};
";

		public static List<IStream> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IStream>(cmd, Map).ToList();
		}

		public static IStream GetById ( long devId, int streamId )
		{
			string cmd = string.Format(MAIN_CMD, @"where s.DevID = @DevId and s.StrID = @StrId");
			return new Ds { { "DevId", devId }, { "StrId", streamId } }.Entity().Get<IStream>(cmd, Map).SingleOrDefault();
		}

		public static List<IStream> GetStreamsByDevice ( Guid deviceId )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @DeviceId");
			return new Ds { { "DeviceId", deviceId } }.Entity().Get<IStream>(cmd, Map).ToList();
		}

		internal static Stream Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var stream = new Stream();

				PopulateStream(reader, stream);

				return stream;
			}

			return null;
		}

		internal static void PopulateStream ( IDataReader reader, IStream stream )
		{
			try
			{
				stream.DevId = reader.GetInt64("DevID");
				stream.StreamId = reader.GetInt32("StrID");
				stream.Name = reader.GetString("Name");
				stream.Visible = reader.GetNullableByte("Visible");
				stream.DateReceived = reader.GetDateTime("DateTime");
				stream.ActualSevLevel = reader.GetNullableInt32("ActualSevLevel");
				stream.Processed = reader.GetNullableByte("Processed");
				stream.DeviceId = reader.GetGuid("DeviceId");

				stream.Data = reader.IsDbNull("Data") ? null : (byte[]) reader["Data"];
			}
			catch ( InvalidCastException exc )
			{
				throw new EntityMappingException(string.Format("Errore durante la mappatura dello stream: DevId = {0}, StreamId = {1}.", stream.DevId, stream.StreamId), exc);
			}
		}

		#endregion
	}
}

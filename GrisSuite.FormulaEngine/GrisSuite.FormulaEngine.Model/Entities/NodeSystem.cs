﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class NodeSystem : GrisObject, INodeSystem
	{
		public NodeSystem ( IDictionary<string, object> initializationValues = null ) : base(initializationValues) { }

		#region Primitive Members

		/// <summary>
		/// Identificativo specifico del NodeSystem.
		/// </summary>
		public long? NodeSystemId { get; set; }

		private Guid _nodeId;
		/// <summary>
		/// Identificativo della stazione correlata al NodeSystem.
		/// </summary>
		[ParentIdProperty(ParentProperty = "Node", RelatedChildrenProperty = "Systems")]
		public Guid NodeId
		{
			get { return _nodeId; }
			set
			{
				_nodeId = value;
				this.RaisePropertyChanged("NodeId", value);
			}
		}

        /// <summary>
        /// Identificativo del sistema.
        /// </summary>
        public int SystemId { get; set; }

		#endregion

		#region Navigation Members

		/// <summary>
		/// Istanza della stazione relativa al NodeSystem.
		/// </summary>
		public INode Node
		{
			get { return this.NodeReference.Value; }
			set { this.NodeId = value.Id; this.NodeReference = new EntityParent<INode>(this, "Parent", value); }
		}

		private IEntityEnd<INode> _nodeRef;
		public IEntityEnd<INode> NodeReference
		{
			get { return ( this._nodeRef.IsLoaded ) ? this._nodeRef : this.RetrieveParent(this._nodeRef); }
			set { this._nodeRef = value; }
		}

		/// <summary>
		/// Imposta la relazione con un oggetto Node.
		/// </summary>
		/// <param name="node">Istanza dell' oggetto in relazione.</param>
		/// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
		public void SetNode ( INode node, bool isDirectChild )
		{
			if ( node == null ) throw new ArgumentException("Il parametro 'node' non può essere nullo.", "node");
			this.NodeId = node.Id;
			this.NodeReference = new EntityParent<INode>(this, "Node", node, isDirectChild);
		}

		private IEntityCollection<IDevice> _devices;
		public IEntityCollection<IDevice> Devices
		{
			get { return ( this._devices.IsLoaded ) ? this._devices : this.RetrieveChildCollection(this._devices); }
			set { _devices = value; }
		}

		#endregion

		#region Aggregation Members

		#region Devices

		public IList<IDevice> GetDevicesOfType ( IEnumerable<string> deviceTypes )
		{
			return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
		}

		public int? GetWorstSeverityForDevicesOfType ( IEnumerable<string> deviceTypes )
		{
            var devs = this.Devices.Where(d => deviceTypes.Contains(d.Type) && !d.ExcludedFromParentStatus).ToList();

			if ( devs.Count == 1 ) return devs[0].ComparableSeverity;
			if ( devs.Count > 1 ) return devs.Max(d => d.ComparableSeverity);

			return null;
		}

		public IDevice GetDeviceByName ( string name )
		{
			return this.Devices.SingleOrDefault(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase));
		}

		public IList<IDevice> GetDevicesByName ( string name )
		{
			return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}

		#endregion

		#region Servers

		public IList<IServer> GetMonitoringServers ()
		{
            return (from device in this.Devices where device.Server != null select device.Server).Where(srv => srv != null).Distinct().ToList();
		}

		public IList<IServer> GetMonitoringServersInSeverity ( int severity )
		{
            return (from device in this.Devices where device.Server != null && device.Server.SevLevel == severity select device.Server).Where(srv => srv != null).Distinct().ToList();
		}

		public IList<IServer> GetMonitoringServersInSeverity ( ObjectSeverityDetail detailedSeverity )
		{
		    return
		        (from device in this.Devices
		            where device.Server != null && device.Server.SevLevelDetail.HasValue && device.Server.SevLevelDetail.Value == detailedSeverity
		            select device.Server).Where(srv => srv != null).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
			var srvs = (from server in this.GetMonitoringServers()
                        where server.SevLevel != Common.Severities.Severity.NotActive && server.SevLevel != Common.Severities.Severity.NotClassified
					    select server).ToList();

			int numTot = srvs.Count();
			int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
			return ( numOffline > 0 && numOffline == numTot );
		}

		#endregion

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._devices = new EntityCollection<IDevice>(this, "Devices", init => Device.GetDevicesBySystem((Guid) e.NewValue, init));
							break;
						}
					case "NodeId":
						{
							this._nodeRef = new EntityParent<INode>(this, "Node", init => Entities.Node.GetById((Guid) e.NewValue, init));
							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    ns.NodeSystemsId, ns.NodId as SysNodID, ns.SystemId, sy.SystemDescription as SystemName, osnsn.ObjectStatusId as NodSysGNodID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join node_systems ns on ns.NodeSystemsId = os.ObjectId and os.ObjectTypeId = 4
	inner join device_type dt on ns.SystemId = dt.SystemID
	inner join devices d on ns.NodId = d.NodID and d.[Type] = dt.DeviceTypeID
	left join object_status osnsn on ns.NodId = osnsn.ObjectId and osnsn.ObjectTypeId = 3	
	left join systems sy on sy.SystemID = ns.SystemId
{0};
";

		public new static List<INodeSystem> GetAll ( IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
			return new Ds().Entity().Get<INodeSystem, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

        public new static INodeSystem GetById(Guid id, IDictionary<string, object> initializationValues = null)
        {
            string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
            return new Ds { { "Id", id } }.Entity().Get<INodeSystem, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
        }

        public new static NodeSystem GetBySpecificId(long id, IDictionary<string, object> initializationValues = null)
        {
            string cmd = string.Format(MAIN_CMD, "where os.ObjectId = @Id and d.[Type] <> 'NoDevice'");
            return new Ds { { "Id", id } }.Entity().Get<NodeSystem, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
        }

		public static List<INodeSystem> GetSystemsByZone ( Guid zoneId, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ParentObjectStatusId = @ZoneId and d.[Type] <> 'NoDevice'");
			return new Ds { { "ZoneId", zoneId } }.Entity().Get<INodeSystem, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		internal new static NodeSystem Map ( IGrouping<Guid, DataRow> groupedEntity, IDictionary<string, object> initializationValues )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var nodeSys = new NodeSystem(initializationValues);

				PopulateObject(firstRow, nodeSys, groupedEntity.Key);
				PopulateNodeSystem(firstRow, nodeSys);
				// attributes
				PopulateAttributes(groupedEntity, nodeSys);
				// formulas
				PopulateFormulas(groupedEntity, nodeSys);

				return nodeSys;
			}

			return null;
		}

		internal static void PopulateNodeSystem ( DataRow source, INodeSystem nodeSystem )
		{
			nodeSystem.NodeSystemId = source.GetInt64("NodeSystemsId");
			nodeSystem.NodeId = source.GetGuid("NodSysGNodID");
			nodeSystem.SystemId = source.GetInt32("SystemId");
			nodeSystem.Name = source.GetString("SystemName");
		}

		#endregion
	}
}

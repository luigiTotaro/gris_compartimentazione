﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Configuration;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;
using System.DirectoryServices;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class ContactGroupEntry : IContactGroupEntry
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del contatto.
		/// </summary>
        public long NotificationGroupContactId { get; set; }

		/// <summary>
		/// Voce associata al contatto.
		/// </summary>
        public Guid? ObjectStatusId { get; set; }

		/// <summary>
		/// 
		/// </summary>
        public string SmsGroupADName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MailGroupADName { get; set; }


		#endregion

		#region Data Access

		private const string MAIN_CMD = @"select * from notification_group_contacts ";


        public static List<ContactEntry> GetContactEntriesByObjectStatusId4Sms(Guid objectId)
		{
			//prendo i gruppi AD che sono associati a questo objectId
            string cmd = MAIN_CMD + "where ObjectStatusId = '" + objectId + "' and SmsGroupADName!=''";
            List<IContactGroupEntry> listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

            if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questo nodo, provo col livello superiore, la linea
            {
                cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "')  and SmsGroupADName!=''";
                listaGruppi = new Ds { { "ObjectStatusId", objectId } }.Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

                if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questa linea, provo col livello superiore, il compartimento
                {
                    cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "'))  and SmsGroupADName!=''";
                    listaGruppi = new Ds { { "ObjectStatusId", objectId } }.Entity().Get<IContactGroupEntry>(cmd, Map).ToList();
                }
            }

            List<ContactEntry> arrayTel = new List<ContactEntry>();
            //se non ho trovato niente, getto la spugna
            if (listaGruppi.Count == 0) return arrayTel;

            //prendo il campo dei gruppi e lo splitto, in caso sono presenti + gruppi
            String gruppiStr = listaGruppi[0].SmsGroupADName;
            string[] gruppi = gruppiStr.Split(',');


            //String ActiveDirectoryConnectionString = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString"];
            //String ActiveDirectoryUsername = ConfigurationManager.AppSettings["ActiveDirectoryUsername"];
            //String ActiveDirectoryPassword = ConfigurationManager.AppSettings["ActiveDirectoryPassword"];

            String ActiveDirectoryConnectionString_group = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString_group"];
            String ActiveDirectoryUsername_group = ConfigurationManager.AppSettings["ActiveDirectoryUsername_group"];
            String ActiveDirectoryPassword_group = ConfigurationManager.AppSettings["ActiveDirectoryPassword_group"];
            String ActiveDirectoryConnectionString_user = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString_user"];
            String ActiveDirectoryUsername_user = ConfigurationManager.AppSettings["ActiveDirectoryUsername_user"];
            String ActiveDirectoryPassword_user = ConfigurationManager.AppSettings["ActiveDirectoryPassword_user"];
            String ActiveDirectoryTelephoneKey = ConfigurationManager.AppSettings["ActiveDirectoryTelephoneKey"];
            String ActiveDirectoryMailKey = ConfigurationManager.AppSettings["ActiveDirectoryMailKey"];

            //per ogni gruppo devo vedere i componenti
            DirectoryEntry de = new DirectoryEntry(ActiveDirectoryConnectionString_group, ActiveDirectoryUsername_group, ActiveDirectoryPassword_group);
            DirectorySearcher ds = new DirectorySearcher(de);


            for (int z = 0; z < gruppi.Count(); z++)
            {

                try
                {
                    String gruppo = gruppi[z].Trim();
                    if (gruppo == "") continue;
                    //ds.Filter = "(&(objectCategory=group)(name=TestGroupSms))";
                    ds.Filter = "(&(objectCategory=group)(name=" + gruppo + "))";

                    ds.SearchScope = SearchScope.Subtree;
                    SearchResult rs = ds.FindOne();

                    List<string> nomi2Find = new List<string>();

                    if (rs != null)
                    {
                        var numUtenti = rs.GetDirectoryEntry().Properties["member"].Count;


                        for (var i = 0; i < numUtenti; i++)
                        {
                            object utente = rs.GetDirectoryEntry().Properties["member"][i];
                            String[] properties = utente.ToString().Split(',');
                            String displayName = "";
                            for (int t = 0; t < properties.Count(); t++)
                            {
                                if (properties[t].StartsWith("CN="))
                                {
                                    displayName = properties[t].Substring(3);
                                    nomi2Find.Add(displayName);
                                    break;
                                }
                            }
                            /*
                            if (displayName != "")
                            {
                                //recupero l'utente
                                ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(displayName=" + displayName + "))";
                                SearchResult rs2 = ds.FindOne();
                                if (rs2 != null)
                                {
                                    String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                    String email = rs2.GetDirectoryEntry().Properties["mail"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["mail"].Value.ToString();
                                    String tel = rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value.ToString();
                                    ContactEntry temp = new ContactEntry();
                                    temp.Name = name;
                                    temp.Email = email;
                                    temp.PhoneNumber = tel;

                                    //lo aggiungo solo se non già presente
                                    if (!arrayTel.Contains(temp)) arrayTel.Add(temp);
                                }
                            }
                            */
                        }
                    }

                    if (de != null) de.Dispose();

                    //seconda parte, recupero i nomi
                    de = new DirectoryEntry(ActiveDirectoryConnectionString_user, ActiveDirectoryUsername_user, ActiveDirectoryPassword_user);
                    ds = new DirectorySearcher(de);

                    foreach (var displayName in nomi2Find)
                    {
                        if (displayName != "")
                        {
                            //recupero l'utente
                            ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(CN=" + displayName + "))";
                            SearchResult rs2 = ds.FindOne();
                            if (rs2 != null)
                            {
                                String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                String email = rs2.GetDirectoryEntry().Properties[ActiveDirectoryMailKey].Value == null ? "" : rs2.GetDirectoryEntry().Properties[ActiveDirectoryMailKey].Value.ToString();
                                String tel = rs2.GetDirectoryEntry().Properties[ActiveDirectoryTelephoneKey].Value == null ? "" : rs2.GetDirectoryEntry().Properties[ActiveDirectoryTelephoneKey].Value.ToString();

                                //String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                //String email = rs2.GetDirectoryEntry().Properties["mail"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["mail"].Value.ToString();
                                //String tel = rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value.ToString();
                                ContactEntry temp = new ContactEntry();
                                temp.Name = name;
                                temp.Email = email;
                                temp.PhoneNumber = tel;

                                //lo aggiungo solo se non già presente
                                if (!arrayTel.Contains(temp)) arrayTel.Add(temp);


                            }
                        }
                    }

                    if (de != null) de.Dispose();

                }
                catch
                {

                }
            
            
            }



            de.Dispose();

            return arrayTel;
		}


        public static List<ContactEntry> GetContactGroupEntriesByObjectStatusId4Mail(Guid objectId)
		{
            //prendo i gruppi AD che sono associati a questo objectId
            string cmd = MAIN_CMD + "where ObjectStatusId = '" + objectId + "' and MailGroupADName!=''";
            List<IContactGroupEntry> listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

            if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questo sistema, provo col livello superiore, il nodo
            {
                cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "')  and MailGroupADName!=''";
                listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

                if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questo nodo, provo col livello superiore, la linea
                {
                    cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "'))  and MailGroupADName!=''";
                    listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

                    if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questa linea, provo col livello superiore, il compartimento
                    {
                        cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "')))  and MailGroupADName!=''";
                        listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();
                    }
                }
            }

            List<ContactEntry> arrayMail = new List<ContactEntry>();
            //se non ho trovato niente, getto la spugna
            if (listaGruppi.Count == 0) return arrayMail;

            //prendo il campo dei gruppi e lo splitto, in caso sono presenti + gruppi
            String gruppiStr = listaGruppi[0].MailGroupADName;
            string[] gruppi = gruppiStr.Split(',');


            //String ActiveDirectoryConnectionString = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString"];
            //String ActiveDirectoryUsername = ConfigurationManager.AppSettings["ActiveDirectoryUsername"];
            //String ActiveDirectoryPassword = ConfigurationManager.AppSettings["ActiveDirectoryPassword"];

            String ActiveDirectoryConnectionString_group = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString_group"];
            String ActiveDirectoryUsername_group = ConfigurationManager.AppSettings["ActiveDirectoryUsername_group"];
            String ActiveDirectoryPassword_group = ConfigurationManager.AppSettings["ActiveDirectoryPassword_group"];
            String ActiveDirectoryConnectionString_user = ConfigurationManager.AppSettings["ActiveDirectoryConnectionString_user"];
            String ActiveDirectoryUsername_user = ConfigurationManager.AppSettings["ActiveDirectoryUsername_user"];
            String ActiveDirectoryPassword_user = ConfigurationManager.AppSettings["ActiveDirectoryPassword_user"];
            String ActiveDirectoryTelephoneKey = ConfigurationManager.AppSettings["ActiveDirectoryTelephoneKey"];
            String ActiveDirectoryMailKey = ConfigurationManager.AppSettings["ActiveDirectoryMailKey"];

            //per ogni gruppo devo vedere i componenti
            DirectoryEntry de = new DirectoryEntry(ActiveDirectoryConnectionString_group, ActiveDirectoryUsername_group, ActiveDirectoryPassword_group);
            DirectorySearcher ds = new DirectorySearcher(de);


            for (int z = 0; z < gruppi.Count(); z++)
            {

                try
                {
                    String gruppo = gruppi[z].Trim();
                    if (gruppo == "") continue;
                    //ds.Filter = "(&(objectCategory=group)(name=TestGroupSms))";
                    ds.Filter = "(&(objectCategory=group)(name=" + gruppo + "))";

                    ds.SearchScope = SearchScope.Subtree;
                    SearchResult rs = ds.FindOne();

                    List<string> nomi2Find = new List<string>();

                    if (rs != null)
                    {
                        var numUtenti = rs.GetDirectoryEntry().Properties["member"].Count;


                        for (var i = 0; i < numUtenti; i++)
                        {
                            object utente = rs.GetDirectoryEntry().Properties["member"][i];
                            String[] properties = utente.ToString().Split(',');
                            String displayName = "";
                            for (int t = 0; t < properties.Count(); t++)
                            {
                                if (properties[t].StartsWith("CN="))
                                {
                                    displayName = properties[t].Substring(3);
                                    nomi2Find.Add(displayName);
                                    break;
                                }
                            }
                            /*
                            if (displayName != "")
                            {
                                //recupero l'utente
                                ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(displayName=" + displayName + "))";
                                SearchResult rs2 = ds.FindOne();
                                if (rs2 != null)
                                {
                                    String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                    String email = rs2.GetDirectoryEntry().Properties["mail"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["mail"].Value.ToString();
                                    String tel = rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value.ToString();
                                    ContactEntry temp = new ContactEntry();
                                    temp.Name = name;
                                    temp.Email = email;
                                    temp.PhoneNumber = tel;

                                    //lo aggiungo solo se non già presente
                                    if (!arrayMail.Contains(temp)) arrayMail.Add(temp);
                                }
                            }*/
                        }
                    }

                    if (de != null) de.Dispose();

                    //seconda parte, recupero i nomi
                    de = new DirectoryEntry(ActiveDirectoryConnectionString_user, ActiveDirectoryUsername_user, ActiveDirectoryPassword_user);
                    ds = new DirectorySearcher(de);

                    foreach (var displayName in nomi2Find)
                    {
                        if (displayName != "")
                        {
                            //recupero l'utente
                            ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(CN=" + displayName + "))";
                            SearchResult rs2 = ds.FindOne();
                            if (rs2 != null)
                            {
                                String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                String email = rs2.GetDirectoryEntry().Properties[ActiveDirectoryMailKey].Value == null ? "" : rs2.GetDirectoryEntry().Properties[ActiveDirectoryMailKey].Value.ToString();
                                String tel = rs2.GetDirectoryEntry().Properties[ActiveDirectoryTelephoneKey].Value == null ? "" : rs2.GetDirectoryEntry().Properties[ActiveDirectoryTelephoneKey].Value.ToString();

                                //String name = rs2.GetDirectoryEntry().Properties["name"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["name"].Value.ToString();
                                //String email = rs2.GetDirectoryEntry().Properties["mail"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["mail"].Value.ToString();
                                //String tel = rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value == null ? "" : rs2.GetDirectoryEntry().Properties["telephoneNumber"].Value.ToString();
                                ContactEntry temp = new ContactEntry();
                                temp.Name = name;
                                temp.Email = email;
                                temp.PhoneNumber = tel;

                                //lo aggiungo solo se non già presente
                                if (!arrayMail.Contains(temp)) arrayMail.Add(temp);
                            
                            
                            }
                        }
                    }

                    if (de != null) de.Dispose();



                }
                catch
                { 
                
                }

            
            
            }



            de.Dispose();

            return arrayMail;
		}

        public static bool HasContactGroupEntriesByObjectStatusId4Node(Guid objectId)
        {
            //prendo i gruppi AD che sono associati a questo objectId
            string cmd = MAIN_CMD + "where ObjectStatusId = '" + objectId + "' and SmsGroupADName!=''";
            List<IContactGroupEntry> listaGruppi = new Ds ().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

            if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questo nodo, provo col livello superiore, la linea
            {
                cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "')  and SmsGroupADName!=''";
                listaGruppi = new Ds ().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

                if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questa linea, provo col livello superiore, il compartimento
                {
                    cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "'))  and SmsGroupADName!=''";
                    listaGruppi = new Ds ().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();
                }
            }

            if (listaGruppi.Count == 0) return false;
            else return true;
        }

        public static bool HasContactGroupEntriesByObjectStatusId4System(Guid objectId)
        {
            //prendo i gruppi AD che sono associati a questo objectId
            string cmd = MAIN_CMD + "where ObjectStatusId = '" + objectId + "' and MailGroupADName!=''";
            List<IContactGroupEntry> listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

            if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questo sistema, provo col livello superiore, il nodo
            {
                cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "')  and MailGroupADName!=''";
                listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

                if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questo nodo, provo col livello superiore, la linea
                {
                    cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "'))  and MailGroupADName!=''";
                    listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();

                    if (listaGruppi.Count == 0) //non ha trovato gruppi associati a questa linea, provo col livello superiore, il compartimento
                    {
                        cmd = MAIN_CMD + "where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId in (select ParentObjectStatusId from object_status where ObjectStatusId='" + objectId + "')))  and MailGroupADName!=''";
                        listaGruppi = new Ds().Entity().Get<IContactGroupEntry>(cmd, Map).ToList();
                    }                
                }
            }

            if (listaGruppi.Count == 0) return false;
            else return true;
        }


		internal static ContactGroupEntry Map ( IDataReader reader )
		{
			if ( reader != null )
			{
                var contactGroupEntry = new ContactGroupEntry();

                PopulateStream(reader, contactGroupEntry);

                return contactGroupEntry;
			}

			return null;
		}

        internal static void PopulateStream(IDataReader reader, IContactGroupEntry contactGroupEntry)
		{
			try
			{
                contactGroupEntry.NotificationGroupContactId = reader.GetInt64("NotificationGroupContactId");
                contactGroupEntry.ObjectStatusId = reader.GetGuid("ObjectStatusId");
                int colIndex = reader.GetOrdinal("SmsGroupADName");
                if (!reader.IsDBNull(colIndex))
                {
                    contactGroupEntry.SmsGroupADName = reader.GetString("SmsGroupADName");
                }
                else contactGroupEntry.SmsGroupADName = "";

                colIndex = reader.GetOrdinal("MailGroupADName");
                if (!reader.IsDBNull(colIndex))
                {
                    contactGroupEntry.MailGroupADName = reader.GetString("MailGroupADName");
                }
                else contactGroupEntry.MailGroupADName = "";
                
            }
			catch ( InvalidCastException exc )
			{
                throw new EntityMappingException(string.Format("Errore durante la mappatura del contatto: ContactId = {0}.", contactGroupEntry.NotificationGroupContactId), exc);
			}
		}

		#endregion
	}
}

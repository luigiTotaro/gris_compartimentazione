﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class ObjectAttributeType : IObjectAttributeType
	{
		/// <summary>
		/// Identificativo dell' attributo.
		/// </summary>
		public short Id { get; set; }

		/// <summary>
		/// Nome dell' attributo.
		/// </summary>
		public string Name { get; set; }

		public List<IObjectAttribute> ObjectAttributes
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		#region Data Access

		public IList<IObjectAttributeType> GetAll ()
		{
			return GetAllAttributes();
		}

		public static IList<IObjectAttributeType> GetAllAttributes ()
		{
			return new Ds().Entity().Get("select AttributeTypeId, AttributeName from object_attribute_types order by AttributeName", Map).ToList();
		}

		public IObjectAttributeType GetById ( short id )
		{
			return GetAttributeById(id);
		}

		public static IObjectAttributeType GetAttributeById ( short id )
		{
			return new Ds { { "AttributeTypeId", id } }.Entity().Get("select AttributeTypeId, AttributeName from object_attribute_types where AttributeTypeId = @AttributeTypeId", Map).SingleOrDefault();
		}

		#endregion

		#region Mapping

		internal static IObjectAttributeType Map ( IDataReader reader )
		{
			return new ObjectAttributeType
			{
				Id = reader.GetInt16(reader.GetOrdinal("AttributeTypeId")),
				Name = reader.GetString(reader.GetOrdinal("AttributeName"))
			};
		}

		internal static IObjectAttributeType Map ( IGrouping<short, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.First();
				return new ObjectAttributeType
				{
					Id = groupedEntity.Key,
					Name = firstRow.GetString("AttributeName")
				};
			}

			return null;
		}

		#endregion
	}
}

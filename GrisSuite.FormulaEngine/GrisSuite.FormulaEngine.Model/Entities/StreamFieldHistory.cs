﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class StreamFieldHistory : IStreamFieldHistory
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del Messaggio ricevuto dal Collector.
		/// </summary>
		public Guid LogId { get; set; }

		/// <summary>
		/// Identificativo di periferica.
		/// </summary>
		public long DevId { get; set; }

		/// <summary>
		/// Identificativo di Stream.
		/// </summary>
		public int StreamId { get; set; }

		/// <summary>
		/// Identificativo di Field.
		/// </summary>
		public int FieldId { get; set; }

		/// <summary>
		/// Identificativo di array.
		/// </summary>
		public int ArrayId { get; set; }

		/// <summary>
		/// Nome dello StreamField.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Severità come riportata dal collettore.
		/// </summary>
		public int? ActualSevLevel { get; set; }

		/// <summary>
		/// Valore dello StreamField.
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Descrizione dello StreamField.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Visibilità dello StreamField.
		/// </summary>
		public byte? Visible { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public Guid? ReferenceID { get; set; }

        /// <summary>
        /// Indica se deve essere inviata una notifica via email.
        /// </summary>
        public bool ShouldSendNotificationByEmail { get; set; }

	    /// <summary>
		/// Istanza dello stream a cui appartiene lo stream field.
		/// </summary>
		public IStream Stream
		{
			get { return this.StreamField.Stream; }
			set { throw new NotImplementedException(); }
		}

		#endregion

		#region Navigation Members

		private IStreamField _streamField;
		/// <summary>
		/// Istanza dello stream field a cui appartiene lo StreamFieldHistory.
		/// </summary>
		public IStreamField StreamField
		{
			get { return this._streamField ?? ( this._streamField = Entities.StreamField.GetById(this.DevId, this.StreamId, this.FieldId, this.ArrayId) ); }
			set { this._streamField = value; }
		}

		private List<IStreamFieldCase> _cases;
		/// <summary>
		/// Collezione degli IStreamFieldCase appartenenti allo stream field.
		/// </summary>
		public List<IStreamFieldCase> Cases
		{
			get { return this._cases ?? ( this._cases = this.PopulateCases() ); }
		}

		/// <summary>
		/// Collezione degli StreamFieldsHistory appartenenti allo stream field.
		/// </summary>
		public List<IStreamFieldHistory> StreamFieldsHistory
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		private List<IStreamFieldCase> PopulateCases ()
		{
			if ( !string.IsNullOrEmpty(this.Description) )
			{
				List<IStreamFieldCase> cases = new List<IStreamFieldCase>(30);
				foreach ( var valueAndStatus in this.Description.Split(';') )
				{
					var valueAndStatusArr = valueAndStatus.Split('=');
					if ( valueAndStatusArr.Length > 1 )
					{
						int val;
						if ( !int.TryParse(valueAndStatusArr[1], out val) ) val = 255;
						cases.Add(new StreamFieldCase(this)
						{
							Value = valueAndStatusArr[0],
							ActualSevLevel = val
						});
					}
					else
					{
						cases.Add(new StreamFieldCase(this)
						{
							Value = valueAndStatusArr[0],
							ActualSevLevel = 255
						});
					}
				}
				return cases;
			}

			return new List<IStreamFieldCase>(0);
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select 
	LogID, DevID, StrID, FieldID, ArrayID, Name, SevLevel as ActualSevLevel, Value, Visible
from 
	cache_stream_fields
{0};
";

		public static List<IStreamFieldHistory> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IStreamFieldHistory>(cmd, Map).ToList();
		}

		public static IStreamFieldHistory GetById ( Guid logId, long devId, int streamId, int fieldId, int arrayId )
		{
			string cmd = string.Format(MAIN_CMD, @"where LogID = @LogId and DevID = @DevId and StrID = @StrId and FieldID = @FieldId and ArrayID = @ArrayId");
			return new Ds { { "LogId", logId }, { "DevId", devId }, { "StrId", streamId }, { "FieldId", fieldId }, { "ArrayId", arrayId } }.Entity().Get<IStreamFieldHistory>(cmd, Map).SingleOrDefault();
		}

		public static List<IStreamFieldHistory> GetStreamFieldsHistoryByStreamField ( long devId, int streamId, int fieldId, int arrayId )
		{
			string cmd = string.Format(MAIN_CMD, "where DevID = @DevId and StrID = @StrId and FieldID = @FieldId and ArrayID = @ArrayId");
			return new Ds { { "DevId", devId }, { "StrId", streamId }, { "FieldId", fieldId }, { "ArrayId", arrayId } }.Entity().Get<IStreamFieldHistory>(cmd, Map).ToList();
		}

		internal static StreamFieldHistory Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var streamFieldHistory = new StreamFieldHistory();

				PopulateStreamField(reader, streamFieldHistory);

				return streamFieldHistory;
			}

			return null;
		}

		internal static void PopulateStreamField ( IDataReader reader, IStreamFieldHistory streamFieldHistory )
		{
			try
			{
				streamFieldHistory.LogId = reader.GetGuid("LogID");
				streamFieldHistory.DevId = reader.GetInt64("DevID");
				streamFieldHistory.StreamId = reader.GetInt32("StrID");
				streamFieldHistory.FieldId = reader.GetInt32("FieldID");
				streamFieldHistory.ArrayId = reader.GetInt32("ArrayID");
				streamFieldHistory.Name = reader.GetString("Name");
				streamFieldHistory.ActualSevLevel = reader.GetNullableInt32("ActualSevLevel");
				streamFieldHistory.Value = reader.GetNullableString("Value");
				streamFieldHistory.Description = reader.GetNullableString("Description");
				streamFieldHistory.Visible = reader.GetNullableByte("Visible");
			}
			catch ( InvalidCastException exc )
			{
				throw new EntityMappingException(string.Format("Errore durante la mappatura dello stream field history: LogId = {0}, DevId = {1}, StreamId = {2}, FieldId = {3}, ArrayId = {4}.", streamFieldHistory.LogId, streamFieldHistory.DevId, streamFieldHistory.StreamId, streamFieldHistory.FieldId, streamFieldHistory.ArrayId), exc);
			}
		}

		#endregion
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class ObjectFormula : IObjectFormula
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del GrisObject associato.
		/// </summary>
		public Guid GrisObjectId { get; set; }

		/// <summary>
		/// Indice di Formula del GrisObject.
		/// </summary>
		public byte Index { get; set; }

		/// <summary>
		/// Path al file contenente lo script della Formula.
		/// </summary>
		public string ScriptPath { get; set; }

		/// <summary>
		/// Indice globale di Formula.
		/// </summary>
		public long FormulaGlobalIndex { get; set; }

		#endregion

		#region Navigation Members

		private IGrisObject _grisObject;
		/// <summary>
		/// Istanza del GrisObject a cui la formula si riferisce.
		/// </summary>
		public IGrisObject GrisObject
		{
			get { return this._grisObject ?? ( this._grisObject = Model.GrisObject.GetById(this.GrisObjectId) ); }
			set { this._grisObject = value; this.GrisObjectId = value.Id; }
		}

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		public List<IObjectAttribute> ComputedObjectAttributes { get; set; }

		#endregion

		#region Data Access

		public List<IObjectFormula> GetByGrisObjectId ()
		{
			return GetObjectAttributesByGrisObjectId(this.GrisObjectId);
		}

		public static List<IObjectFormula> GetObjectAttributesByGrisObjectId ( Guid grisObjectId )
		{
			string cmd = 
@"select
	ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex
from object_formulas
where ObjectStatusId = @ObjectId";
			return new Ds { { "ObjectId", grisObjectId } }.Entity().Get(cmd, Map).ToList();
		}

		#endregion

		#region Mapping

		internal static IObjectFormula Map ( IDataReader reader )
		{
			return new ObjectFormula
			{
				GrisObjectId = reader.GetGuid("ObjectStatusId"),
				Index = reader.GetByte("FormulaIndex"),
				ScriptPath = reader.GetString("ScriptPath"),
				FormulaGlobalIndex = 0
			};
		}

		internal static IObjectFormula Map ( IGrouping<Tuple<Guid, byte>, DataRow> groupedEntity )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.First();
				return new ObjectFormula
				{
					GrisObjectId = groupedEntity.Key.Item1,
					Index = groupedEntity.Key.Item2,
					ScriptPath = firstRow.GetNullableString("ScriptPath"),
					FormulaGlobalIndex = 0
				};
			}

			return null;
		}

		#endregion
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;
using NLog.LayoutRenderers.Wrappers;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class DeviceFilter : IDeviceFilter
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del filtro.
		/// </summary>
        public long DeviceFilterId { get; set; }

		#endregion

		#region Data Access

	    private const string MAIN_CMD = @"SELECT DeviceFilterId FROM device_filters {0};";

        public static List<IDeviceFilter> GetSharedFilters()
		{
            string cmd = string.Format(MAIN_CMD, " WHERE ResourceID IS NOT NULL");
			return new Ds().Entity().Get<IDeviceFilter>(cmd, Map).ToList();
		}

	    public static void UpdateAllNotificationsFilters()
	    {
	        var shaewsFilters = GetSharedFilters();
	        foreach (var filter in shaewsFilters)
	        {
                new Ds { { "DeviceFilterId", filter.DeviceFilterId } }.Entity().Exec("gris_UpdNotificationsByFilters",null, "Main",CommandType.StoredProcedure); 
	        }
	    }
        
        
        internal static DeviceFilter Map ( IDataReader reader )
		{
		    if (reader == null) return null;
		    
            var deviceFilter = new DeviceFilter();

		    PopulateStream(reader, deviceFilter);

		    return deviceFilter;
		}



		internal static void PopulateStream ( IDataReader reader, IDeviceFilter deviceFilter )
		{
			try
			{
                deviceFilter.DeviceFilterId = reader.GetInt64("DeviceFilterId");
			}
			catch ( InvalidCastException exc )
			{
                throw new EntityMappingException(string.Format("Errore durante la mappatura del filtro: DeviceFilterId = {0}.", deviceFilter.DeviceFilterId), exc);
			}
		}

		#endregion
	}
}

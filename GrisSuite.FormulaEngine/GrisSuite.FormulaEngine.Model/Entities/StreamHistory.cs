﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class StreamHistory : IStreamHistory
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del Messaggio ricevuto dal Collector.
		/// </summary>
		public Guid LogId { get; set; }

		/// <summary>
		/// Identificativo dello Stream.
		/// </summary>
		public int StreamId { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		public long DevId { get; set; }

		/// <summary>
		/// Nome dello Stream.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Visibilità dello Stream.
		/// </summary>
		public byte? Visible { get; set; }

		/// <summary>
		/// Dati binari dello Stream.
		/// </summary>
		public byte[] Data { get; set; }

		/// <summary>
		/// Data di ricezione dello Stream.
		/// </summary>
		public DateTime DateReceived { get; set; }

		/// <summary>
		/// Severità dello Stream come ricevuta dal collettore.
		/// </summary>
		public int? ActualSevLevel { get; set; }

		/// <summary>
		/// Indica se lo Stream è stato processato.
		/// </summary>
		public byte? Processed { get; set; }

		/// <summary>
		/// Identificativo della Device.
		/// </summary>
		public Guid DeviceId { get; set; }

		/// <summary>
		/// Istanza della periferica a cui appartiene lo stream.
		/// </summary>
		public IDevice Device
		{
			get { return this.Stream.Device; }
			set { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Istanze degli stream fields dello stream.
		/// </summary>
		public List<IStreamField> StreamFields
		{
			get { return this.Stream.StreamFields; }
			set { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Istanze Istanze degli streams dello StreamHistory.
		/// </summary>
		public List<IStreamHistory> StreamsHistory
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		#endregion

		#region Navigation Members

		private IStream _stream;
		/// <summary>
		/// Istanza dello stream a cui appartiene lo StreamHistory.
		/// </summary>
		public IStream Stream
		{
			get { return this._stream ?? ( this._stream = Entities.Stream.GetById(this.DevId, this.StreamId) ); }
			set { this._stream = value; }
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select 
	LogID, DevID, StrID, Name, Visible, Data, [DateTime], cs.SevLevel as ActualSevLevel, [Description], Processed, os.ObjectStatusId as DeviceId
from 
	cache_streams cs
	inner join object_status os on cs.DevID = os.ObjectId and os.ObjectTypeId = 6
{0};
";

		public static List<IStreamHistory> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IStreamHistory>(cmd, Map).ToList();
		}

		public static IStreamHistory GetById ( Guid logId, long devId, int streamId )
		{
			string cmd = string.Format(MAIN_CMD, @"where cs.LogID = @LogId and cs.DevID = @DevId and cs.StrID = @StrId");
			return new Ds { { "LogId", logId }, { "DevId", devId }, { "StrId", streamId } }.Entity().Get<IStreamHistory>(cmd, Map).SingleOrDefault();
		}

		public static List<IStreamHistory> GetStreamsHistoryByStream ( long devId, int streamId )
		{
			string cmd = string.Format(MAIN_CMD, "where cs.DevID = @DevId and cs.StrID = @StrId");
			return new Ds { { "DevId", devId }, { "StrId", streamId } }.Entity().Get<IStreamHistory>(cmd, Map).ToList();
		}

		internal static StreamHistory Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var streamHistory = new StreamHistory();

				PopulateStreamHistory(reader, streamHistory);

				return streamHistory;
			}

			return null;
		}

		internal static void PopulateStreamHistory ( IDataReader reader, IStreamHistory streamHistory )
		{
			try
			{
				streamHistory.LogId = reader.GetGuid("LogID");
				streamHistory.DevId = reader.GetInt64("DevID");
				streamHistory.StreamId = reader.GetInt32("StrID");
				streamHistory.Name = reader.GetString("Name");
				streamHistory.Visible = reader.GetNullableByte("Visible");
				streamHistory.DateReceived = reader.GetDateTime("DateTime");
				streamHistory.ActualSevLevel = reader.GetNullableInt32("ActualSevLevel");
				streamHistory.Processed = reader.GetNullableByte("Processed");
				streamHistory.DeviceId = reader.GetGuid("DeviceId");

				streamHistory.Data = reader.IsDbNull("Data") ? null : (byte[]) reader["Data"];
			}
			catch ( InvalidCastException exc )
			{
				throw new EntityMappingException(string.Format("Errore durante la mappatura dello stream: DevId = {0}, StreamId = {1}.", streamHistory.DevId, streamHistory.StreamId), exc);
			}
		}

		#endregion
	}
}

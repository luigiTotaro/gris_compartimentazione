﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
    [Serializable]
    public class Contact : IContact
    {
        #region Primitive Members

        /// <summary>
        /// Identificativo del contatto.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Nome del contatto.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Compartimento a cui si riferisce il contatto.
        /// </summary>
        public long RegionId { get; set; }

        /// <summary>
        /// Indirizzo email del contatto.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Numero telefonico del contatto.
        /// </summary>
        public string PhoneNumber { get; set; }

        #endregion

        #region Data Access

        private const string MAIN_CMD = @"
select c.ContactId, c.Name, c.RegId, c.Email, c.PhoneNumber
from contacts c
inner join notification_contacts nc on c.ContactId = nc.ContactId
{0};
";

        public static List<IContact> GetContactsByGrisObject(IGrisObject grisObject)
        {
            return new Ds { { "Id", grisObject.Id } }.Entity().Get(string.Format(MAIN_CMD, "where nc.ObjectStatusId = @Id"), Map).ToList();
        }

        internal static IContact Map(IDataReader reader)
        {
            if (reader != null)
            {
                var contact = new Contact();

                PopulateContact(reader, contact);

                return contact;
            }

            return null;
        }

        internal static void PopulateContact(IDataReader reader, IContact contact)
        {
            contact.Id = reader.GetInt64("ContactId");
            contact.RegionId = reader.GetInt64("RegId");
            contact.Name = reader.GetString("Name");
            contact.Email = reader.GetNullableString("Email");
            contact.PhoneNumber = reader.GetNullableString("PhoneNumber");
        }

        #endregion
    }
}

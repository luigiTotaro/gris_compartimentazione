﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class OperationScheduleType : IOperationScheduleType
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del tipo di schedulazione.
		/// </summary>
		public short Id { get; set; }

		/// <summary>
		/// Codice univoco del tipo di schedulazione.
		/// </summary>
		public string Code { get; set; }

		/// <summary>
		/// Nome dell' assembly contenente la classe per gestire l'esecuzione del tipo di operazioni schedulate.
		/// </summary>
		public string AssemblyName { get; set; }

		#endregion

		#region Navigation Members

		private List<IOperationSchedule> _operationSchedules;
		/// <summary>
		/// Collezione di istanze di schedulazione.
		/// </summary>
		public List<IOperationSchedule> OperationSchedules
		{
			get { return this._operationSchedules ?? ( this._operationSchedules = OperationSchedule.GetSchedulesByScheduleType(this.Code) ); }
			set { this._operationSchedules = value; }
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = "select OperationScheduleTypeId, OperationScheduleTypeCode, AssemblyName from operation_schedule_types {0};";

		public static List<OperationScheduleType> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get(cmd, Map).ToList();
		}

		public static OperationScheduleType GetById ( short id )
		{
			string cmd = string.Format(MAIN_CMD, @"where OperationScheduleTypeId = @Id");
			return new Ds { { "Id", id } }.Entity().Get(cmd, Map).SingleOrDefault();
		}

		public static OperationScheduleType GetByCode ( string code )
		{
			string cmd = string.Format(MAIN_CMD, @"where OperationScheduleTypeCode = @Code");
			return new Ds { { "Code", code } }.Entity().Get(cmd, Map).SingleOrDefault();
		}

		internal static OperationScheduleType Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var schedType = new OperationScheduleType();

				PopulateScheduleType(reader, schedType);

				return schedType;
			}

			return null;
		}

		internal static void PopulateScheduleType ( IDataReader reader, OperationScheduleType scheduleType )
		{
			try
			{
				scheduleType.Id = reader.GetInt16("OperationScheduleTypeId");
				scheduleType.Code = reader.GetString("OperationScheduleTypeCode");
				scheduleType.AssemblyName = reader.GetString("AssemblyName");
			}
			catch ( InvalidCastException exc )
			{
				throw new EntityMappingException(string.Format("Errore durante la mappatura del tipo di schedulazione: Id = {0}.", scheduleType.Id), exc);
			}
		}

		#endregion
	}
}

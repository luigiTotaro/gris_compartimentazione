﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;
using System.Reflection;

namespace GrisSuite.FormulaEngine.Model.Entities
{
    [Serializable]
    public class Node : GrisObject, INode
    {
        public Node(IDictionary<string, object> initializationValues = null) : base(initializationValues)
        {
        }

        #region Primitive Members

        /// <summary>
        ///     Chilometrica associata alla stazione.
        /// </summary>
        public int Meters { get; set; }

        /// <summary>
        ///     Identificativo specifico della stazione.
        /// </summary>
        public long NodeId { get; set; }

        private Guid _zoneId;

        /// <summary>
        ///     Identificativo della linea a cui appartiene la stazione.
        /// </summary>
        [ParentIdProperty(ParentProperty = "Zone", RelatedChildrenProperty = "Nodes")]
        public Guid ZoneId
        {
            get { return this._zoneId; }
            set
            {
                this._zoneId = value;
                this.RaisePropertyChanged("ZoneId", value);
            }
        }

        #endregion

        #region Navigation Members

        /// <summary>
        ///     Istanza della linea a cui appartiene la stazione.
        /// </summary>
        public IZone Zone
        {
            get { return this.ZoneReference.Value; }
            set
            {
                this.ZoneId = value.Id;
                this.ZoneReference = new EntityParent<IZone>(this, "Parent", value);
            }
        }

        private IEntityEnd<IZone> _zoneRef;

        public IEntityEnd<IZone> ZoneReference
        {
            get { return (this._zoneRef.IsLoaded) ? this._zoneRef : this.RetrieveParent(this._zoneRef); }
            set { this._zoneRef = value; }
        }

        /// <summary>
        ///     Imposta la relazione con un oggetto Zone.
        /// </summary>
        /// <param name="zone">Istanza dell' oggetto in relazione.</param>
        /// <param name="isDirectChild">Indica se la relazione è diretta o meno.</param>
        public void SetZone(IZone zone, bool isDirectChild)
        {
            if (zone == null)
            {
                throw new ArgumentException("Il parametro 'zone' non può essere nullo.", "zone");
            }
            this.ZoneId = zone.Id;
            this.ZoneReference = new EntityParent<IZone>(this, "Zone", zone, isDirectChild);
        }

        private IEntityCollection<INodeSystem> _systems;

        /// <summary>
        ///     Istanze dei Sistemi presenti nella stazione.
        /// </summary>
        public IEntityCollection<INodeSystem> Systems
        {
            get { return (this._systems.IsLoaded) ? this._systems : this.RetrieveChildCollection(this._systems); }
            set { this._systems = value; }
        }

        private IEntityCollection<IServer> _servers;

        /// <summary>
        ///     Istanze dei Server presenti nella stazione.
        /// </summary>
        public IEntityCollection<IServer> Servers
        {
            get { return (this._servers.IsLoaded) ? this._servers : this.RetrieveChildCollection(this._servers); }
            set { this._servers = value; }
        }

        private IEntityCollection<IDevice> _devices;

        /// <summary>
        ///     Istanze delle Periferiche presenti nella stazione.
        /// </summary>
        public IEntityCollection<IDevice> Devices
        {
            get { return (this._devices.IsLoaded) ? this._devices : this.RetrieveChildCollection(this._devices); }
            set { this._devices = value; }
        }

        #endregion

        #region Aggregation Members

        #region NodeSystems

        public INodeSystem GetSystemByName(string name)
        {
            return this.Systems.Where(s => s.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
        }

        public IList<INodeSystem> GetSystemsByName(string name)
        {
            return this.Systems.Where(s => s.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
        }

        public INodeSystem GetSystemBySystemId(int sysId)
        {
            return this.Systems.SingleOrDefault(s => s.SystemId == sysId);
        }

        #endregion

        #region Devices

        public IList<IDevice> GetDevicesOfType(IEnumerable<string> deviceTypes)
        {
            return this.Devices.Where(d => deviceTypes.Contains(d.Type)).ToList();
        }

        public int? GetWorstSeverityForDevicesOfType(IEnumerable<string> deviceTypes)
        {
            var devs = this.Devices.GetExcept().Where(d => deviceTypes.Contains(d.Type)).ToList();

            if (devs.Count == 1)
            {
                return devs[0].ComparableSeverity;
            }
            if (devs.Count > 1)
            {
                return devs.Max(d => d.ComparableSeverity);
            }

            return null;
        }

        public IDevice GetDeviceByName(string name)
        {
            return this.Devices.SingleOrDefault(d => d.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public IList<IDevice> GetDevicesByName(string name)
        {
            return this.Devices.Where(d => d.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
        }

        #endregion

        #region Servers

        public IList<IServer> GetMonitoringServers()
        {
            return (from device in this.Devices where device.Server != null select device.Server).Where(srv => srv != null).Distinct().ToList();
        }

        public IList<IServer> GetMonitoringServersInSeverity(int severity)
        {
            return
                (from device in this.Devices where device.Server != null && device.Server.SevLevel == severity select device.Server).Where(
                    srv => srv != null).Distinct().ToList();
        }

        public IList<IServer> GetMonitoringServersInSeverity(ObjectSeverityDetail detailedSeverity)
        {
            return (from device in this.Devices
                where device.Server != null && device.Server.SevLevelDetail.HasValue && device.Server.SevLevelDetail.Value == detailedSeverity
                select device.Server).Where(srv => srv != null).Distinct().ToList();
        }

        public bool AreAllNodeServersOffline()
        {
            var srvs = (from server in this.GetMonitoringServers()
                where server.SevLevel != Common.Severities.Severity.NotActive && server.SevLevel != Common.Severities.Severity.NotClassified
                select server).ToList();

            int numTot = srvs.Count();
            int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
            return (numOffline > 0 && numOffline == numTot);
        }

        #endregion

        #endregion

        #region Initialization

        protected override void Object_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.Object_PropertyChanged(sender, e);

            if (e != null)
            {
                switch (e.PropertyName)
                {
                    case "Id":
                    {
                        this._devices = new EntityCollection<IDevice>(this, "Devices", init => Device.GetDevicesByNode((Guid) e.NewValue, init));
                        this._systems = new EntityCollection<INodeSystem>(this, "Systems",
                            init => NodeSystem.GetSystemsByZone((Guid) e.NewValue, init));
                        this._servers = new EntityCollection<IServer>(this, "Servers", init => Server.GetServersByNode((Guid) e.NewValue, init));
                        break;
                    }
                    case "ZoneId":
                    {
                        this._zoneRef = new EntityParent<IZone>(this, "Zone", init => Entities.Zone.GetById((Guid) e.NewValue, init));
                        break;
                    }
                }
            }
        }

        #endregion

        #region Data Access

        private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    n.NodID, n.ZonID as NodZonID, n.Name as NodName, n.Meters as NodMeters, osnz.ObjectStatusId as NodGZonID
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join nodes n on n.NodID = os.ObjectId and os.ObjectTypeId = 3
	inner join devices d on n.NodID = d.NodID
	left join object_status osnz on n.ZonID = osnz.ObjectId and osnz.ObjectTypeId = 2
{0};
";

        public new static List<INode> GetAll(IDictionary<string, object> initializationValues = null)
        {
            string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
            return new Ds().Entity().Get<INode, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
        }

        public new static INode GetById(Guid id, IDictionary<string, object> initializationValues = null)
        {
            string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
            return
                new Ds {{"Id", id}}.Entity()
                    .Get<INode, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues)
                    .SingleOrDefault();
        }

        public static List<INode> GetNodesByZone(Guid zoneId, IDictionary<string, object> initializationValues = null)
        {
            string cmd = string.Format(MAIN_CMD, "where osnz.ObjectStatusId = @ZoneId and d.[Type] <> 'NoDevice'");
            return
                new Ds {{"ZoneId", zoneId}}.Entity().Get<INode, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
        }

        internal new static Node Map(IGrouping<Guid, DataRow> groupedEntity, IDictionary<string, object> initializationValues)
        {
            if (groupedEntity != null && groupedEntity.Any())
            {
                var firstRow = groupedEntity.FirstOrDefault();

                var node = new Node(initializationValues);

                PopulateObject(firstRow, node, groupedEntity.Key);
                PopulateNode(firstRow, node);
                // attributes
                PopulateAttributes(groupedEntity, node);
                // formulas
                PopulateFormulas(groupedEntity, node);

                return node;
            }

            return null;
        }

        internal static void PopulateNode(DataRow source, INode node)
        {
            node.NodeId = source.GetInt64("NodID");
            node.ZoneId = source.GetGuid("NodGZonID");
            node.Name = source.GetString("NodName");
            node.Meters = source.GetInt32("NodMeters");
        }

        #endregion

        #region Persistence

        public override IList<IStorable> StorableNodes
        {
            get
            {
                var stors = new List<IStorable>(300);
                if (this.IsEvaluated)
                {
                    stors.Add(this.Attributes);
                }
                stors.AddRange(this.Children);
                stors.AddRange(this.Servers);
                return stors;
            }
        }

        #endregion

        #region Notifications

        public override string GetSmsNotificationMessage()
        {
            string devicesListString = "";

            List<IDevice> devicesList = this.GetNotificationDevicesList();

            devicesListString = string.Join(", ", devicesList.OrderByDescending(d => d.ComparableSeverity).ThenBy(d => d.Name).Select(d =>
            {
                if (d.Parent is VirtualObject) // sarà sicuramente quello dei Fondamentali
                {
                    return string.Format("{0} (fondamentali)", d.Name);
                }
                return string.Format("{0}", d.Name);
            }));

            if (String.IsNullOrEmpty(devicesListString))
            {
                // La stazione è in errore, ma la lista delle periferiche da segnalare è nulla
                int serversOffline = (from srv in this.Servers where srv.Severity == Offline.Severity select srv).Count();

                if (serversOffline > 0)
                {
                    devicesListString = "N/D (Server offline)";
                }
                else
                {
                    devicesListString = "N/D";
                }
            }

            DateTime currentOperationTimestamp = DateTime.Now;

            string smsMessageTemplate = "";

            NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

            try
            {
                //smsMessageTemplate = File.ReadAllText(string.Format("{0}\\Operations\\node_sms_template.txt",spath));
                smsMessageTemplate = File.ReadAllText(string.Format("{0}\\Operations\\node_sms_template.txt",Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))); 
            }
            catch (Exception ex)
            {
                
                _logger.Error("Errore recuperando il contenuto del file [Node.cs] - " + ex.Message);
                smsMessageTemplate = @"Avviso generato il {0} alle ore {1}, Stazione in stato di errore: {2}, Perif. problematiche: {3}.";
            }
            
            // Usiamo le stringhe di formattazione statiche per l'italiano, corrispondono a ShortDate / ShortTime
            string SMSmsg = string.Format(smsMessageTemplate, 
                                            currentOperationTimestamp.ToString("d/M/yyyy"), 
                                            currentOperationTimestamp.ToString("HH:mm"),
                                            this.Name, 
                                            devicesListString);
            //_logger.Info("SMS > " + SMSmsg);
            return SMSmsg;
        }


        public override string GetSmsNotificationKeys(string phoneNumber)
        {
            List<IDevice> devicesList = this.GetNotificationDevicesList();

            string devicesListString = String.Join("|", devicesList.OrderBy(d => d.SpecificId).Select(d => d.SpecificId));

            return String.Format("PhoneNumber:{0},NodeId:{1},DeviceIds:{2}", phoneNumber, this.SpecificId.ToString(), devicesListString);
        }

        private List<IDevice> GetNotificationDevicesList()
        {
            List<IDevice> devicesList = new List<IDevice>();

            // logiche cablate per soddisfare la specifica che chiede di elencare le periferiche coinvolte nella determinazione dell' evento critico. (Issue 11486, commento 6317)
            foreach (NodeSystem sys in this.Systems)
            {
                if (!sys.ExcludeFromParentStatus && sys.ComparableSeverity >= Error.Severity)
                {
                    if (sys.VirtualObjects.Count > 0 && sys.VirtualObjects.Any(vo => vo.Name == "Fondamentali"))
                    {
                        // in un sistema con configurazione a Raggruppamenti di default (gruppi: Fondamentali, Importanti, Complementari associati a formule di default) 
                        // l'unico ragruppamento che può determinare lo stato di errore della stazione e quello dei dispositivi Fondamentali
                        // => i dispositivi che hanno determinato lo stato saranno tutti appartenenti al gruppo.
                        var voFondam = sys.VirtualObjects.Single(vo => vo.Name == "Fondamentali");

                        var devs = from dev in voFondam.Children
                            where
                                dev is Device &&
                                (dev.Severity == Warning.Severity || dev.Severity == Error.Severity || dev.SeverityDetail == DiagnosticOffline.SeverityDetail)
                            select (Device) dev;

                        devicesList.AddRange(devs);
                    }
                    else
                    {
                        // se non esiste il raggruppamento dei Fondamentali e la stazione è comunque rossa, considero come causa una formula statistica standard
                        // => elenco le periferiche peggiori (errore, offline) considerandole fuori da raggruppamenti
                        var devs = from dev in sys.Devices
                            where dev.Severity == Error.Severity || dev.SeverityDetail == DiagnosticOffline.SeverityDetail
                            select dev;

                        devicesList.AddRange(devs);
                    }
                }
            }
            return devicesList;
        }

        #endregion
    }
}
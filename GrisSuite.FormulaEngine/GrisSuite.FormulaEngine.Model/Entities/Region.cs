﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class Region : GrisObject, IRegion
	{
		public Region ( IDictionary<string, object> initializationValues = null ) : base(initializationValues) { }

		/// <summary>
		/// Identificativo specifico del compartimento.
		/// </summary>
		public long RegionId { get; set; }

		private IEntityCollection<IZone> _zones;
		/// <summary>
		/// Istanze delle Linee appartenenti al compartimento.
		/// </summary>
		public IEntityCollection<IZone> Zones
		{
			get { return ( this._zones.IsLoaded ) ? this._zones : this.RetrieveChildCollection(this._zones); }
			set { _zones = value; }
		}

		#region Aggregation Members

		#region Zones

		public IZone GetZoneByName ( string name )
		{
			return this.Zones.SingleOrDefault(z => z.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase));
		}

		public IList<IZone> GetZonesByName ( string name )
		{
			return this.Zones.Where(z => z.Name.IndexOf(name, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
		}


		#endregion

		#region Nodes
		#endregion

		#region NodeSystems
		#endregion

		#region Devices
		#endregion

		#region Servers

		public IList<IServer> GetMonitoringServers ()
		{
			return ( from zone in this.Zones
					 from node in zone.Nodes
					 from device in node.Devices
                     select device.Server).Where(srv => srv != null).Distinct().ToList();
		}

		public bool AreAllNodeServersOffline ()
		{
            var srvs = (from server in this.GetMonitoringServers()
                        where server.SevLevel != Common.Severities.Severity.NotActive && server.SevLevel != Common.Severities.Severity.NotClassified
                        select server).ToList();

            int numTot = srvs.Count();
            int numOffline = srvs.Count(s => s.SevLevel == Offline.Severity);
            return (numOffline > 0 && numOffline == numTot);
		}

        public bool AreAllObjectsByParentUnknown()
        {
            var children = (from c in this.Children
                            where c.SevLevel != Common.Severities.Severity.NotActive && c.SevLevel != Common.Severities.Severity.NotClassified
                            select c).ToList();

            int numTot = children.Count();
            int numUnknown = children.Count(c => c.SevLevel == Unknown.Severity);
            return (numUnknown > 0 && numUnknown == numTot);
        }

		#endregion

		#endregion

		#region Initialization

		protected override void Object_PropertyChanged ( object sender, PropertyChangedEventArgs e )
		{
			base.Object_PropertyChanged(sender, e);

			if ( e != null )
			{
				switch ( e.PropertyName )
				{
					case "Id":
						{
							this._zones = new EntityCollection<IZone>(this, "Zones", init => Zone.GetZonesByRegion((Guid) e.NewValue, init));
							break;
						}
				}
			}
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select distinct
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    r.RegID, r.Name as RegName
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join regions r on r.RegID = os.ObjectId and os.ObjectTypeId = 1
	inner join zones z on r.RegID = z.RegID
	inner join nodes n on z.ZonID = n.ZonID
	inner join devices d on n.NodID = d.NodID
{0};
";

		public new static List<IRegion> GetAll ( IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where d.[Type] <> 'NoDevice'");
			return new Ds().Entity().Get<IRegion, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		public new static IRegion GetById ( Guid id, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id and d.[Type] <> 'NoDevice'");
			return new Ds { { "Id", id } }.Entity().Get<IRegion, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
		}

		internal new static Region Map ( IGrouping<Guid, DataRow> groupedEntity, IDictionary<string, object> initializationValues )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var region = new Region(initializationValues);

				PopulateObject(firstRow, region, groupedEntity.Key);
				PopulateNode(firstRow, region);
				// attributes
				PopulateAttributes(groupedEntity, region);
				// formulas
				PopulateFormulas(groupedEntity, region);

				return region;
			}

			return null;
		}

		internal static void PopulateNode ( DataRow source, IRegion region )
		{
			region.RegionId = source.GetInt64("RegID");
			region.Name = source.GetString("RegName");
		}

		#endregion
	}
}

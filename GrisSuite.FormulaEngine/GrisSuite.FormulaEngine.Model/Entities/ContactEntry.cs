﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class ContactEntry : IContactEntry
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo del contatto.
		/// </summary>
        public long ContactId { get; set; }

		/// <summary>
		/// Voce associata al contatto.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Numero telefonico.
		/// </summary>
		public string PhoneNumber { get; set; }

        /// <summary>
        /// eMail.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Manda una eMail a questo contatto per almeno una delle periferiche associate a questo contatto
        /// </summary>
        public bool SendEMail { get; set; }

        /// <summary>
        /// Manda un sms a questo contatto per almeno una delle periferiche associate a questo contatto
        /// </summary>
        public bool SendSms { get; set; }

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
SELECT
  c.ContactId,
  c.Name,
  ISNULL(c.PhoneNumber,'') as PhoneNumber,
  ISNULL(c.Email,'') as Email,
  CAST(MAX(ISNULL(CAST(rnc.SendEMail AS SMALLINT), 0)) AS BIT) AS SendEMail,
  CAST(MAX(ISNULL(CAST(rnc.SendSms AS SMALLINT), 0)) AS BIT)   AS SendSms
FROM contacts c INNER JOIN notification_contacts nc ON c.ContactId = nc.ContactId
  LEFT JOIN resource_notification_contacts rnc ON rnc.ContactId = nc.ContactId AND nc.ResourceId = rnc.ResourceId
{0}
GROUP BY c.ContactId, c.Name, c.PhoneNumber, c.Email
";

		public static List<IContactEntry> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IContactEntry>(cmd, Map).ToList();
		}

		public static IContactEntry GetById ( long ContactId )
		{
			string cmd = string.Format(MAIN_CMD, @"where c.ContactId = @ContactId");
			return new Ds { { "ContactId", ContactId } }.Entity().Get<IContactEntry>(cmd, Map).SingleOrDefault();
		}

		public static List<IContactEntry> GetContactEntriesByObjectStatusId ( Guid objectId )
		{
			string cmd = string.Format(MAIN_CMD, "where nc.ObjectStatusId = @ObjectStatusId");
			return new Ds { { "ObjectStatusId", objectId } }.Entity().Get<IContactEntry>(cmd, Map).ToList();
		}

		internal static ContactEntry Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var contactEntry = new ContactEntry();

                PopulateStream(reader, contactEntry);

                return contactEntry;
			}

			return null;
		}

		internal static void PopulateStream ( IDataReader reader, IContactEntry contactEntry )
		{
			try
			{
                contactEntry.ContactId = reader.GetInt64("ContactId");
                contactEntry.Name = reader.GetString("Name");
                contactEntry.PhoneNumber = reader.GetString("PhoneNumber");
                contactEntry.Email = reader.GetString("EMail");
                contactEntry.SendEMail = reader.GetBoolean("SendEMail");
                contactEntry.SendSms = reader.GetBoolean("SendSms");
			}
			catch ( InvalidCastException exc )
			{
                throw new EntityMappingException(string.Format("Errore durante la mappatura del contatto: ContactId = {0}.", contactEntry.ContactId), exc);
			}
		}

		#endregion
	}
}

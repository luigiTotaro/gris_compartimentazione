﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class StreamField : IStreamField
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo di periferica.
		/// </summary>
		public long DevId { get; set; }

		/// <summary>
		/// Identificativo di Stream.
		/// </summary>
		public int StreamId { get; set; }

		/// <summary>
		/// Identificativo di Field.
		/// </summary>
		public int FieldId { get; set; }

		/// <summary>
		/// Identificativo di array.
		/// </summary>
		public int ArrayId { get; set; }

		/// <summary>
		/// Nome dello StreamField.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Severità come riportata dal collettore..
		/// </summary>
		public int? ActualSevLevel { get; set; }

		/// <summary>
		/// Valore dello StreamField.
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Descrizione dello StreamField.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Visibilità dello StreamField.
		/// </summary>
		public byte? Visible { get; set; }

        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public Guid? ReferenceID { get; set; }

        /// <summary>
        /// Indica se deve essere inviata una notifica via email.
        /// </summary>
        public bool ShouldSendNotificationByEmail { get; set; }

		#endregion

		#region Navigation Members

		private IStream _stream;
		/// <summary>
		/// Istanza dello stream a cui appartiene lo StreamHistory.
		/// </summary>
		public IStream Stream
		{
			get { return this._stream ?? ( this._stream = Entities.Stream.GetById(this.DevId, this.StreamId) ); }
			set { this._stream = value; }
		}

		private List<IStreamFieldCase> _cases;
		/// <summary>
		/// Collezione degli IStreamFieldCase appartenenti allo stream field.
		/// </summary>
		public List<IStreamFieldCase> Cases
		{
			get { return this._cases ?? ( this._cases = this.PopulateCases() ); }
		}

		private List<IStreamFieldHistory> _streamFieldsHistory;
		/// <summary>
		/// Collezione degli StreamFieldsHistory appartenenti allo stream field.
		/// </summary>
		public List<IStreamFieldHistory> StreamFieldsHistory
		{
			get { return this._streamFieldsHistory ?? ( this._streamFieldsHistory = StreamFieldHistory.GetStreamFieldsHistoryByStreamField(this.DevId, this.StreamId, this.FieldId, this.ArrayId) ); }
			set { this._streamFieldsHistory = value; }
		}

		private List<IStreamFieldCase> PopulateCases ()
		{
			if ( !string.IsNullOrEmpty(this.Description) )
			{
				List<IStreamFieldCase> cases = new List<IStreamFieldCase>(30);
				foreach (var valueAndStatus in this.Description.Split(';'))
				{
					var valueAndStatusArr = valueAndStatus.Split('=');
					if ( valueAndStatusArr.Length > 1 )
					{
						int val;
						if ( !int.TryParse(valueAndStatusArr[1], out val) ) val = 255;
						cases.Add(new StreamFieldCase(this)
									{
										Value = valueAndStatusArr[0],
										ActualSevLevel = val
									});
					}
					else
					{
						cases.Add(new StreamFieldCase(this)
						          	{
						          		Value = valueAndStatusArr[0],
										ActualSevLevel = 255
						          	});
					}
				}
				return cases;
			}

			return new List<IStreamFieldCase>(0);
		}

		#endregion

		#region Aggregation Members

		public bool HasValueForTimeSpan ( string value, TimeSpan forPeriod )
		{
			Func<IStreamFieldHistory, bool> pred = ( sfh => sfh.Value == value.Trim() );
			return this.IsInConditionForTimeSpan(pred, forPeriod);
		}

		public bool IsInConditionForTimeSpan ( string caseName, int severity, TimeSpan forPeriod )
		{
			Func<IStreamFieldHistory, bool> pred = ( sfh => sfh.Description.Contains(string.Format("{0}={1}", caseName, severity)) );
			return this.IsInConditionForTimeSpan(pred, forPeriod);
		}

		public bool IsInConditionForTimeSpan ( string exactDescription, TimeSpan forPeriod )
		{
			Func<IStreamFieldHistory, bool> pred = (sfh => sfh.Description == exactDescription.Trim());
			return this.IsInConditionForTimeSpan(pred, forPeriod);
		}

		private bool IsInConditionForTimeSpan ( Func<IStreamFieldHistory, bool> predicate , TimeSpan forPeriod )
		{
			var now = DateTime.Now;
			var streamFieldsHist = from streamHistory in this.Stream.StreamsHistory
								   where streamHistory.DateReceived <= now && streamHistory.DateReceived >= now - forPeriod
								   from streamField in streamHistory.Stream.StreamFields
								   from streamFieldHistory in streamField.StreamFieldsHistory
								   select streamFieldHistory;
			return streamFieldsHist.All(predicate);
		}

		public IStreamFieldCase GetCaseByValue ( string searchValue )
		{
			return this.Cases.SingleOrDefault(c => c.Value.IndexOf(searchValue, StringComparison.InvariantCultureIgnoreCase) != -1);
		}

		public int CountCasesInSeverity ( int severity )
		{
			return this.Cases.Count(c => c.ActualSevLevel == severity);
		}

		public int CountCasesAtLeastInSeverity ( int severity )
		{
			return this.Cases.Count(c => c.ActualSevLevel >= severity);
		}

		public bool AreAllCasesInSeverity ( int severity )
		{
			return this.Cases.Count == this.CountCasesInSeverity(severity);
		}

		public bool AreAllCasesAtLeastInSeverity ( int severity )
		{
			return this.Cases.Count == this.CountCasesAtLeastInSeverity(severity);
		}

		#endregion

		#region Data Access

		private const string MAIN_CMD = @"
select 
	DevID, StrID, FieldID, ArrayID, Name, SevLevel as ActualSevLevel, Value, [Description], Visible, ShouldSendNotificationByEmail
from 
	stream_fields
{0};
";

		public static List<IStreamField> GetAll ()
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IStreamField>(cmd, Map).ToList();
		}

		public static IStreamField GetById ( long devId, int streamId, int fieldId, int arrayId )
		{
			string cmd = string.Format(MAIN_CMD, @"where DevID = @DevId and StrID = @StrId and FieldID = @FieldId and ArrayID = @ArrayId");
			return new Ds { { "DevId", devId }, { "StrId", streamId }, { "FieldId", fieldId }, { "ArrayId", arrayId } }.Entity().Get<IStreamField>(cmd, Map).SingleOrDefault();
		}

		public static List<IStreamField> GetStreamFieldsByStream ( long devId, int streamId )
		{
			string cmd = string.Format(MAIN_CMD, "where DevID = @DevId and StrID = @StrId");
			return new Ds { { "DevId", devId }, { "StrId", streamId } }.Entity().Get<IStreamField>(cmd, Map).ToList();
		}

		internal static StreamField Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var streamField = new StreamField();

				PopulateStreamField(reader, streamField);

				return streamField;
			}

			return null;
		}

		internal static void PopulateStreamField ( IDataReader reader, IStreamField streamField )
		{
			try
			{
				streamField.DevId = reader.GetInt64("DevID");
				streamField.StreamId = reader.GetInt32("StrID");
				streamField.FieldId = reader.GetInt32("FieldID");
				streamField.ArrayId = reader.GetInt32("ArrayID");
				streamField.Name = reader.GetString("Name");
				streamField.ActualSevLevel = reader.GetNullableInt32("ActualSevLevel");
				streamField.Value = reader.GetNullableString("Value");
				streamField.Description = reader.GetNullableString("Description");
                streamField.Visible = reader.GetNullableByte("Visible");
                streamField.ShouldSendNotificationByEmail = Convert.ToBoolean(reader.GetByte("ShouldSendNotificationByEmail"));
			}
			catch ( InvalidCastException exc )
			{
				throw new EntityMappingException(string.Format("Errore durante la mappatura dello stream field: DevId = {0}, StreamId = {1}, FieldId = {2}, ArrayId = {3}.", streamField.DevId, streamField.StreamId, streamField.FieldId, streamField.ArrayId), exc);
			}
		}

		#endregion
	}
}

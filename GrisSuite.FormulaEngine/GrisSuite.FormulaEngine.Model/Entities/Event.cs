﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class Event : IEvent
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo dell'evento.
		/// </summary>
		public Guid Id { get; set; }

		/// <summary>
		/// Identificativo della periferica che ha inviato l'evento.
		/// </summary>
		public Guid DeviceId { get; set; }

		/// <summary>
		/// Dati binari dell'evento
		/// </summary>
		public byte[] Data { get; set; }

		/// <summary>
		/// Categoria dell'evento.
		/// </summary>
		public byte Category { get; set; }

		/// <summary>
		/// Data di creazione del record.
		/// </summary>
		public DateTime? Created { get; set; }

		/// <summary>
		/// Data di richiesta, registrata dalla device.
		/// </summary>
		public DateTime Requested { get; set; }

		/// <summary>
		/// Data in cui l'evento viene registrato nel db centrale.
		/// </summary>
		public DateTime Centralized { get; set; }

		/// <summary>
		/// Flag che indica un evento che dev'essere cancellato secondo logiche del Supervisor.
		/// </summary>
		public bool ToBeDeleted { get; set; }

		public byte? EventCode { get; set; }
		public string EventDescription { get; set; }
		public string EventDescriptionValue { get; set; }
		public byte? InOutId { get; set; }
		public string MicroId { get; set; }
		public string SubEventDescription { get; set; }
		public byte? FunctionDescription { get; set; }
		public byte? DeviceCode { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della periferica a cui appartiene l'evento.
		/// </summary>
		public IDevice Device { get; set; }

		#endregion

		#region Data Access

		public static List<IEvent> GetEventsByDeviceAndPeriod ( IDevice device, DateTime dateFrom, DateTime dateTo )
		{
			var events = new Ds
			             	{
			             		{ "@DevID", device.SpecificId },
			             		{ "@DeviceTypeID", device.Type },
			             		{ "@CreatedFrom", dateFrom },
			             		{ "@CreatedTo", dateTo },
			             		{ "@EventCodeFilter43", true },
			             		{ "@EventCodeFilter44", true },
			             		{ "@EventCodeFilter45", true }
			             	}.Entity().Get<IEvent>("gris_GetDecodedEventData", Map, connectionName: "Events", commandType: CommandType.StoredProcedure).ToList();

			foreach (var evnt in events)
				evnt.DeviceId = device.Id;

			return events;
		}

		internal static Event Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var evnt = new Event();

				PopulateStream(reader, evnt);

				return evnt;
			}

			return null;
		}

		internal static void PopulateStream ( IDataReader reader, IEvent evnt )
		{
			try
			{
				evnt.Id = reader.GetGuid("EventID");
				evnt.Data = reader.GetBytes("EventData");
				evnt.Category = reader.GetByte("EventCategory");
				evnt.ToBeDeleted = reader.GetBoolean("ToBeDeleted");
				evnt.Created = reader.GetNullableDateTime("Created");
				evnt.Requested = reader.GetDateTime("Requested");
				evnt.Centralized = reader.GetDateTime("Centralized");
				evnt.EventCode = reader.GetNullableByte("EventCode");
				evnt.EventDescription = reader.GetNullableString("EventDescription");
				evnt.EventDescriptionValue = reader.GetNullableString("EventDescriptionValue");
				evnt.InOutId = reader.GetNullableByte("InOutId");
				evnt.MicroId = reader.GetNullableString("MicroId");
				evnt.SubEventDescription = reader.GetNullableString("SubEventDescription");
				evnt.FunctionDescription = reader.GetNullableByte("FunctionDescription");
				evnt.DeviceCode = reader.GetNullableByte("DeviceCode");
			}
			catch ( InvalidCastException exc )
			{
				throw new EntityMappingException(string.Format("Errore durante la mappatura dell'evento: EventId = {0}.", evnt.Id), exc);
			}
		}

		#endregion
	}
}

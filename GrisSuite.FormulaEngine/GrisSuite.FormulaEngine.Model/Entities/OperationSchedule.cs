﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
    public class OperationSchedule : IOperationSchedule
    {
        #region Primitive Members

        /// <summary>
        ///     Identificativo della schedulazione.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        ///     Identificativo del tipo di schedulazione.
        /// </summary>
        public short TypeId { get; set; }

        /// <summary>
        ///     Parametri per l'esecuzione della schedulazione.
        /// </summary>
        public Dictionary<string, string> Parameters { get; set; }

        /// <summary>
        ///     Data di creazione della schedulazione.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        ///     Data di esecuzione dell'operazione.
        /// </summary>
        public DateTime? Executed { get; set; }

        /// <summary>
        ///     Data di annullamento dell'operazione.
        /// </summary>
        public DateTime? Aborted { get; set; }

        /// <summary>
        /// Chiavi da usare per poter identificare in modo univoco l'operazione
        /// </summary>
        public string OperationKeys { get; set; }

        #endregion

        #region Navigation Members

        private IOperationScheduleType _type;

        /// <summary>
        ///     Istanza del tipo di schedulazione.
        /// </summary>
        public IOperationScheduleType Type
        {
            get { return this._type ?? (this._type = OperationScheduleType.GetById(this.TypeId)); }
            set { this._type = value; }
        }

        #endregion

        #region Data Access

        private const string MAIN_CMD =
            @"select {1} ops.OperationScheduleId, ops.DateCreated, ops.OperationParameters, ops.DateExecuted, ops.DateAborted, opt.OperationScheduleTypeId, opt.OperationScheduleTypeCode, opt.AssemblyName, ops.OperationKeys
from operation_schedules ops inner join operation_schedule_types opt on opt.OperationScheduleTypeId = ops.OperationScheduleTypeId
{0}
order by DateCreated;";

        public static List<IOperationSchedule> GetAll()
        {
            string cmd = String.Format(MAIN_CMD, "");
            return new Ds().Entity().Get(cmd, Map).ToList();
        }

        public static IOperationSchedule GetById(Guid id)
        {
            string cmd = String.Format(MAIN_CMD, @"where ops.OperationScheduleId = @Id", "");
            return new Ds {{"Id", id}}.Entity().Get(cmd, Map).SingleOrDefault();
        }

        public static List<IOperationSchedule> GetSchedulesByScheduleType(string typeCode)
        {
            string cmd = String.Format(MAIN_CMD, "where opt.OperationScheduleTypeCode = @TypeCode", "");
            return new Ds {{"TypeCode", typeCode}}.Entity().Get(cmd, Map).ToList();
        }

        public static DateTime? LastScheduleWithScheduleTypeAndParameterCreationDate(string typeCode, string paramContent)
        {
            string cmd = @"select max(ops.DateCreated) as LastCreated
from operation_schedules ops inner join operation_schedule_types opt on opt.OperationScheduleTypeId = ops.OperationScheduleTypeId
where opt.OperationScheduleTypeCode = @TypeCode
and ops.OperationParameters like @OperationParameters";
            return new Ds {{"TypeCode", typeCode}, {"OperationParameters", paramContent}}.Entity().ExecAndReturn<DateTime?>(cmd);
        }

        public static List<IOperationSchedule> GetNotExecutedSchedules(int takeNum = 20)
        {
            string cmd = String.Format(MAIN_CMD, "where ops.DateExecuted is null and DateAborted is null", "top " + takeNum);
            return new Ds().Entity().Get(cmd, Map).ToList();
        }

        internal static IOperationSchedule Map(IDataReader reader)
        {
            if (reader != null)
            {
                var sched = new OperationSchedule();
                var schedType = new OperationScheduleType();

                try
                {
                    sched.Id = reader.GetGuid("OperationScheduleId");
                    sched.TypeId = reader.GetInt16("OperationScheduleTypeId");
                    sched.Created = reader.GetDateTime("DateCreated");
                    sched.Executed = reader.GetNullableDateTime("DateExecuted");
                    sched.Aborted = reader.GetNullableDateTime("DateAborted");
                    sched.Parameters = (reader.IsDbNull("OperationParameters"))
                        ? null
                        : reader.GetString("OperationParameters").Deserialize<Dictionary<string, string>>();

                    schedType.Id = reader.GetInt16("OperationScheduleTypeId");
                    schedType.Code = reader.GetString("OperationScheduleTypeCode");
                    schedType.AssemblyName = reader.GetString("AssemblyName");
                    sched.OperationKeys = reader.GetEmptyNullString("OperationKeys");

                    sched.Type = schedType;
                }
                catch (InvalidCastException exc)
                {
                    throw new EntityMappingException(String.Format("Errore durante la mappatura della schedulazione: Id = {0}.", sched.Id), exc);
                }

                return sched;
            }

            return null;
        }

        public void Save()
        {
            if (this.Id == Guid.Empty)
            {
                Guid newId =
                    new Ds { { "TypeId", this.TypeId }, { "Parameters", this.Parameters.Serialize() }, { "OperationKeys", this.OperationKeys } }.Entity()
                        .ExecAndReturn<Guid>(
                            String.Format(
                                "declare @op table (Id uniqueidentifier); insert into operation_schedules (OperationScheduleTypeId, OperationParameters, OperationKeys) output inserted.OperationScheduleId into @op values (@TypeId, @Parameters, isnull(@OperationKeys, '')); select top 1 Id from @op;"));
                this.Id = newId;
            }
            else
            {
                new Ds
                {
                    {"Id", this.Id},
                    {"TypeId", this.TypeId},
                    {"Parameters", this.Parameters.Serialize()},
                    {"Executed", this.Executed},
                    {"Aborted", this.Aborted},
                    {"OperationKeys", this.OperationKeys}
                }.Entity()
                    .Exec(
                        "update operation_schedules set OperationScheduleTypeId = @TypeId, OperationParameters = @Parameters, DateExecuted = @Executed, DateAborted = @Aborted, OperationKeys = isnull(@OperationKeys, '') where OperationScheduleId = @Id;");
            }
        }

        public void CreateIf(int lastOperationOlderThanMinutes, OperationScheduleCreateIfCheckType operationScheduleCheckType)
        {
            string cmd = @"declare @op table (Id uniqueidentifier); 
if ((select isnull(datediff(mi, max(DateCreated), getdate()), '19000101')
	from operation_schedules
	where OperationScheduleTypeId = @TypeId{0})
	> @LastOperationMinuteTimeout)
begin
    insert into operation_schedules (OperationScheduleTypeId, OperationParameters, OperationKeys) output inserted.OperationScheduleId into @op 
    values (@TypeId, @Parameters, isnull(@OperationKeys, '')); 
end
else
begin
	insert into @op
	select top 1 OperationScheduleId from operation_schedules where OperationScheduleTypeId = @TypeId{0}
	order by DateCreated desc 
end
select top 1 Id from @op;";

            switch (operationScheduleCheckType)
            {
                case OperationScheduleCreateIfCheckType.ByParameters:
                    // Per verificare la finestra temporale di invii multipli, usiamo solo i parametri della schedulazione (tipo email per sistema)
                    cmd = String.Format(cmd, " and OperationParameters = @Parameters");
                    break;
                case OperationScheduleCreateIfCheckType.ByKeys:
                    // Per verificare la finestra temporale di invii multipli, non usiamo i parametri della schedulazione (solo le chiavi di univocità)
                    // Questo è importante quando il messaggio schedulato o prodotto cambia ad ogni generazione, perché, ad esempio, contiene l'ora, quindi
                    // non sarebbe mai veramente multiplo (tipo SMS)
                    cmd = String.Format(cmd, " and isnull(OperationKeys, '') = isnull(@OperationKeys, '')");
                    break;
                case OperationScheduleCreateIfCheckType.ByParametersAndKeys:
                    cmd = String.Format(cmd, " and isnull(OperationKeys, '') = isnull(@OperationKeys, '') and OperationParameters = @Parameters");
                    break;
            }

            Guid newId =
                new Ds
                {
                    {"TypeId", this.TypeId},
                    {"Parameters", this.Parameters.Serialize()},
                    {"LastOperationMinuteTimeout", lastOperationOlderThanMinutes},
                    {"OperationKeys", this.OperationKeys}
                }.Entity().ExecAndReturn<Guid>(cmd);
            this.Id = newId;
        }

        public static void SetExecuted(Guid id, int retryNumber, string status, string code, string message, string executeParameters)
        {
            string command =
                String.Format(
                    "update operation_schedules set DateExecuted = getdate(), RetryNumber = {1}, Status = '{2}', Code = '{3}', Message = '{4}', ExecuteParameters = '{5}' where OperationScheduleId = '{0}';",
                    id, retryNumber, PrepareStringValue(status), PrepareStringValue(code), PrepareStringValue(message),
                    PrepareStringValue(executeParameters));
            new Ds().Entity().Exec(command);
        }

        public static void SetAborted(Guid id, int retryNumber, string status, string code, string message, string executeParameters)
        {
            string command =
                String.Format(
                    "update operation_schedules set DateAborted = getdate(), RetryNumber = {1}, Status = '{2}', Code = '{3}', Message = '{4}', ExecuteParameters = '{5}' where OperationScheduleId = '{0}';",
                    id, retryNumber, PrepareStringValue(status), PrepareStringValue(code), PrepareStringValue(message),
                    PrepareStringValue(executeParameters));
            new Ds().Entity().Exec(command);
        }

        private static string PrepareStringValue(string value)
        {
            return value == null ? String.Empty : value.Replace("'", "''");
        }

        #endregion
    }
}
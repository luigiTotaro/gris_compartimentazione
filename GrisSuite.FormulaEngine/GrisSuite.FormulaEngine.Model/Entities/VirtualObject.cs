﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class VirtualObject : GrisObject, IVirtualObject
	{
		public VirtualObject ( IDictionary<string, object> initializationValues = null ) : base(initializationValues) { }

		/// <summary>
		/// Identificativo dell' oggetto specifico.
		/// </summary>
		public long VirtualObjectId { get; set; }

		/// <summary>
		/// Identificativo dell' oggetto specifico.
		/// </summary>
		public string Description { get; set; }

		#region Data Access

		private const string MAIN_CMD = @"
select
	os.ObjectStatusId, os.ObjectId, os.ObjectTypeId, os.SevLevel, os.SevLevelDetailId, 
    os.ParentObjectStatusId, os.InMaintenance, os.SevLevelReal, os.SevLevelDetailIdReal, os.SevLevelLast, 
    os.SevLevelDetailIdLast, os.LastUpdateGuid, os.ExcludeFromParentStatus, os.ForcedByUser, os.ForcedSevLevel, os.IsSmsNotificationEnabled,
    ofx.FormulaIndex, ofx.ScriptPath, ofx.FormulaGlobalIndex, 
    oa.AttributeId, oa.AttributeValue, oa.AttributeDataType, oa.ComputedByFormulaObjectStatusId, oa.ComputedByFormulaIndex,
	oat.AttributeTypeId, oat.AttributeName,
    v.VirtualObjectID, v.VirtualObjectName, v.VirtualObjectDescription
from object_status os
	inner join object_formulas ofx on os.ObjectStatusId = ofx.ObjectStatusId 
	left join object_attributes oa on os.ObjectStatusId = oa.ObjectStatusId
	left join object_attribute_types oat on oa.AttributeTypeId = oat.AttributeTypeId
	inner join virtual_objects v on v.VirtualObjectID = os.ObjectId and os.ObjectTypeId = 7
{0};
";

		public new static List<IVirtualObject> GetAll ( IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "");
			return new Ds().Entity().Get<IVirtualObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).ToList();
		}

		public new static IVirtualObject GetById ( Guid id, IDictionary<string, object> initializationValues = null )
		{
			string cmd = string.Format(MAIN_CMD, "where os.ObjectStatusId = @Id");
			return new Ds { { "Id", id } }.Entity().Get<IVirtualObject, Guid>(cmd, "ObjectStatusId", Map, initializationValues: initializationValues).SingleOrDefault();
		}

		internal new static VirtualObject Map ( IGrouping<Guid, DataRow> groupedEntity, IDictionary<string, object> initializationValues )
		{
			if ( groupedEntity != null && groupedEntity.Any() )
			{
				var firstRow = groupedEntity.FirstOrDefault();

				var vo = new VirtualObject(initializationValues);

				PopulateObject(firstRow, vo, groupedEntity.Key);
				PopulateVirtualObject(firstRow, vo);
				// attributes
				PopulateAttributes(groupedEntity, vo);
				// formulas
				PopulateFormulas(groupedEntity, vo);

				return vo;
			}

			return null;
		}

		internal static void PopulateVirtualObject ( DataRow source, IVirtualObject virtualObject )
		{
			virtualObject.VirtualObjectId = source.GetInt64("VirtualObjectID");
			virtualObject.Name = source.GetString("VirtualObjectName");
			virtualObject.Description = source.GetString("VirtualObjectDescription");
		}

		#endregion
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class SeverityRange : ISeverityRange
	{
		#region Primitive Members

		/// <summary>
		/// Identificativo dell'entità.
		/// </summary>
		public byte Id { get; set; }

		/// <summary>
		/// Nome dell'entità.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// E' il peso della severità rispetto alle altre.
		/// </summary>
		public decimal SeverityWeight { get; set; }

		/// <summary>
		/// Valore minimo della soglia.
		/// </summary>
		public short MinValue { get; set; }

		/// <summary>
		/// Valore massimo della soglia.
		/// </summary>
		public short MaxValue { get; set; }

		/// <summary>
		/// Restituisce la lista di tutte le soglie di severità.
		/// </summary>
		/// <returns></returns>
		public IList<ISeverityRange> GetAll ()
		{
			return GetAllRanges();
		}

		#endregion

		#region Data Access

		public static List<ISeverityRange> GetAllRanges ()
		{
			string cmd = "select EntityStateID, EntityStateName, EntityStateSeverity, EntityStateMinValue, EntityStateMaxValue from entity_state";
			return new Ds().Entity().Get(cmd, Map).ToList();
		}

		internal static ISeverityRange Map ( IDataReader reader )
		{
			if ( reader != null )
			{
				var sevRange = new SeverityRange();

				PopulateSevRange(reader, sevRange);

				return sevRange;
			}

			return null;
		}

		internal static void PopulateSevRange ( IDataReader reader, ISeverityRange severityRange )
		{
			try
			{
				severityRange.Id = reader.GetByte("EntityStateID");
				severityRange.Name = reader.GetString("EntityStateName");
				severityRange.SeverityWeight = reader.GetDecimal("EntityStateSeverity");
				severityRange.MinValue = reader.GetInt16("EntityStateMinValue");
				severityRange.MaxValue = reader.GetInt16("EntityStateMaxValue");
			}
			catch ( InvalidCastException exc )
			{
				throw new EntityMappingException(string.Format("Errore durante la mappatura del SeverityRange: EntityStateID = {0}.", severityRange.Id), exc);
			}
		}

		#endregion
	}
}

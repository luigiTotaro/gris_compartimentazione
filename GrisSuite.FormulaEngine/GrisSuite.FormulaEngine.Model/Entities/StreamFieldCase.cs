﻿
using GrisSuite.FormulaEngine.Common.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	public class StreamFieldCase : IStreamFieldCase
	{
		private readonly IStreamField _parent;

		public StreamFieldCase ( IStreamField parent )
		{
			this._parent = parent;
		}

		#region Primitive Members

		/// <summary>
		/// Valore del Case.
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Severità come riportata dal collettore.
		/// </summary>
		public int? ActualSevLevel { get; set; }

		#endregion

		#region Navigation Members

		/// <summary>
		/// Istanza dello stream field a cui appartiene il case.
		/// </summary>
		public IStreamField StreamField
		{
			get { return this._parent; }
		}

		#endregion
	}
}

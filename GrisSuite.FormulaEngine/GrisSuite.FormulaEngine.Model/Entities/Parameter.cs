﻿
using System;
using System.Data;
using System.Linq;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Model.DataAccess;

namespace GrisSuite.FormulaEngine.Model.Entities
{
	[Serializable]
	public class Parameter : IParameter
	{
		/// <summary>
		/// Nome del parametro.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Valore del parametro.
		/// </summary>
		public string Value { get; set; }

		/// <summary>
		/// Descrizione del parametro.
		/// </summary>
		public string Description { get; set; }

		#region Data Access

		public IParameter GetByName ( string name )
		{
			string qry = @"
select ParameterName, ParameterValue, ParameterDescription from [parameters]
where ParameterName = @Name;
";
			return new Ds { { "Name", name } }.Entity().Get(qry, Map).SingleOrDefault();
		}

		#endregion

		#region Mapping

		internal static IParameter Map ( IDataReader reader )
		{
			return new Parameter
			{
				Name = reader.GetString(reader.GetOrdinal("ParameterName")),
                Value = reader.GetString(reader.GetOrdinal("ParameterValue")),
                Description = reader.GetString(reader.GetOrdinal("ParameterDescription"))
			};
		}

		#endregion

	}
}

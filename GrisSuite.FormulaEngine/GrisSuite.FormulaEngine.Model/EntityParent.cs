﻿
using System;
using System.Collections.Generic;
using GrisSuite.FormulaEngine.Common.Contracts;

namespace GrisSuite.FormulaEngine.Model
{
	/// <summary>
	/// Classe wrapper che permette il caricamento ritardato di un' entità padre.
	/// </summary>
	/// <typeparam name="TEntity">Tipo derivante da IGrisObject</typeparam>
	public class EntityParent<TEntity> : IEntityEnd<TEntity> where TEntity : IGrisObject
	{
		private TEntity _value;
		private readonly IGrisObject _childObject;
		private readonly string _propertyName;
		private readonly Func<IDictionary<string, object>, TEntity> _entitiesGetter;

		/// <summary>
		/// Costruttore che permette l'inizializzazione tramite oggetto fornito dall' esterno.
		/// </summary>
		/// <param name="childObject">Istanza dell' oggetto figlio a cui si associa l'entità padre.</param>
		/// <param name="propertyName">Nome della proprietà dell' oggetto figlio che espone l'entità padre.</param>
		/// <param name="entity">Entità padre.</param>
		/// <param name="isDirectChild">Indica se l'entità passata è padre diretto nella gerarchia GrisObject.</param>
		public EntityParent ( IGrisObject childObject, string propertyName, TEntity entity, bool isDirectChild = true )
		{
			if ( entity.Equals(default(TEntity)) ) throw new ArgumentException("Il parametro 'entity' non può essere nullo", "entity");
			if ( childObject == null ) throw new ArgumentException("Il parametro 'childObject' non può essere nullo", "childObject");

			this._childObject = childObject;
			this._propertyName = propertyName;
			this._value = entity;

			if ( isDirectChild ) this._childObject.ParentId = entity.Id;
			this._childObject.ObjectCache.AddObject(entity);
			// l'inserimento in cache dell' oggetto figlio è necessario perchè
			// la relazione in cache viene stabilita sempre attraverso la prop. IdParent dell' oggetto figlio
			entity.ObjectCache.AddChildObjects(entity, this._childObject);
		}

		/// <summary>
		/// Costruttore che permette l'inizializzazione tramite funzione di lookup in database.
		/// </summary>
		/// <param name="childObject">Istanza dell' oggetto figlio a cui si associa l'entità padre.</param>
		/// <param name="propertyName">Nome della proprietà dell' oggetto figlio che espone l'entità padre.</param>
		/// <param name="entitiesGetter">Funzione che permette di caricare l' entità padre nella fase di lazy loading.</param>
		public EntityParent ( IGrisObject childObject, string propertyName, Func<IDictionary<string, object>, TEntity> entitiesGetter )
		{
			if ( childObject == null ) throw new ArgumentException("Il parametro 'childObject' non può essere nullo", "childObject");

			this._childObject = childObject;
			this._propertyName = propertyName;
			this._entitiesGetter = entitiesGetter;
		}

		/// <summary>
		/// Carica l'entità padre dalla cache o dal database.
		/// </summary>
		/// <param name="lookupInCacheOnly">Indica se si vuole che il caricamente venga eseguito solo dalla cache, escludendo il database.</param>
		/// <remarks>
		/// L'entità padre è caricabile una sola volta durante il ciclo di vita della classe stessa.
		/// </remarks>
		public void Load ( bool lookupInCacheOnly = false )
		{
			if ( !this.IsLoaded )
			{
				this.IsLoaded = true;
				var cachedEntity = this._childObject.GetCachedParent(this._propertyName);
				if ( cachedEntity != null )
				{
					this._value = (TEntity) cachedEntity;
				}
				else
				{
					if ( !lookupInCacheOnly && this._entitiesGetter != null )
					{
						var dbEntity = this._entitiesGetter(this._childObject.InitializationValues);
						if ( !dbEntity.Equals(default(TEntity)) )
						{
							this._value = dbEntity;

							this._childObject.ObjectCache.AddObject(dbEntity);
							// l'inserimento in cache dell' oggetto figlio è necessario perchè
							// la relazione in cache viene stabilita sempre attraverso la prop. IdParent dell' oggetto figlio
							dbEntity.ObjectCache.AddObject(this._childObject);
						}
					}
				}

				if ( this._value != null )
				{
					if ( this._value.CallerObject == null ) this._value.CallerObject = this._childObject.CallerObject;
					if ( !this._value.IsInitialized ) this._value.Initialize();	
				}
			}
		}

		/// <summary>
		/// Indica se l'entità padre è già stata caricata dalla cache o dal database.
		/// </summary>
		/// <remarks>
		/// L'entità padre è caricabile una sola volta durante il ciclo di vita della classe stessa.
		/// </remarks>
		public bool IsLoaded { get; private set; }

		/// <summary>
		/// Espone l'entità padre.
		/// </summary>
		public TEntity Value
		{
			get { return this._value; }
		}

		/// <summary>
		/// Membro non implementato.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="isDirectParent"></param>
		public void AddEntity ( TEntity entity, bool isDirectParent = true )
		{
			throw new NotImplementedException();
		}

		public IEntityCollection<TNewEntity> Cast<TNewEntity> () where TNewEntity : class, IGrisObject
		{
			throw new NotImplementedException();
		}

		public IEnumerable<TEntity> FilterExcludedFromParentStatus ()
		{
			throw new NotImplementedException();
		}
	}
}

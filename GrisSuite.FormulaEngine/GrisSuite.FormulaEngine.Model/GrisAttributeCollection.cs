﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using GrisSuite.FormulaEngine.Common.Contracts;
using GrisSuite.FormulaEngine.Common.Contracts.Entities;
using GrisSuite.FormulaEngine.Common.Severities;

namespace GrisSuite.FormulaEngine.Model
{
	/// <summary>
	/// Wrapper che espone le entità attributo di un GrisObject.
	/// </summary>
	[DataContract]
	public class GrisAttributeCollection : IGrisAttributeCollection
	{
		private readonly GrisObject _grisObject;
		private Dictionary<string, GrisAttribute> _internalDict;
		private readonly List<string> _modifiedAttributes = new List<string>(20);
		private readonly IList<IObjectAttributeType> _attributeTypes;

		public GrisAttributeCollection ( GrisObject grisObject, IList<IObjectAttributeType> attributeTypes, bool initializeEmptyOAttributesCollection = false )
		{
			if ( grisObject == null ) throw new ArgumentException("L' oggetto GrisAttributeCollection non può essere inizializzato con uno GrisObject nullo.", "grisObject");
			_grisObject = grisObject;
			_attributeTypes = attributeTypes;

			if ( initializeEmptyOAttributesCollection )
			{
				this._grisObject.ObjectAttributes = new List<IObjectAttribute>(0);
			}

			this.Initialize();
		}

		/// <summary>
		/// Indexer per l'accesso agli attributi.
		/// </summary>
		/// <param name="key">Nome dell'attributo.</param>
		/// <returns>Ritorna il valore dell'attributo.</returns>
		public dynamic this[string key]
		{
			get
			{
				this._grisObject.Evaluate();
				if ( this._grisObject.CallerObject != null && this._grisObject != this._grisObject.CallerObject )
				{
					// stiamo accedendo agli attributi di un oggetto invocato dalla formula di un altro oggetto: memorizzo la dipendenza
					ObjectDependencies.Instance.AddObjectDependencies(this._grisObject, this._grisObject.CallerObject);
				}
				return this.Contains(key) && this._internalDict[key].HasValue ? this._internalDict[key].Value : "";
			}
			set
			{
				if ( !this._internalDict[key].HasValue || // sto inizializzando un attributo che prima non era presente
						( !this._modifiedAttributes.Contains(key) && 
						this._internalDict[key].HasValue &&
						// Se sono dello stesso tipo o sono di tipo diverso ma Int32 e ObjectSeverityDetail posso confrontarne il valore per valutare la disuguaglianza
						( ( this._internalDict[key].Value.GetType() == value.GetType())  || ( this._internalDict[key].Value is Int32 && value is ObjectSeverityDetail ) ) &&
						// se il valore dell'attributo in modifica non è variato, non segno l'attributo come modificato (da persistere)
						this._internalDict[key].Value != value 
					) )
				{
					this._modifiedAttributes.Add(key);
				}

				this._internalDict[key].Value = value;
				this._internalDict[key].HasValue = true;

				if ( this._grisObject.EvaluatingFormula != null )
				{
					if ( this._grisObject.CallerObject != null && this._grisObject != this._grisObject.CallerObject && this._grisObject.CallerObject.EvaluatingFormula != null )
					{
						// stiamo settando agli attributi di un oggetto invocato dalla formula di un altro oggetto
						this._internalDict[key].ComputedByFormula = this._grisObject.CallerObject.EvaluatingFormula.ObjectFormula;
					}
					else
					{
						this._internalDict[key].ComputedByFormula = this._grisObject.EvaluatingFormula.ObjectFormula;
					}
				}
			}
		}

		/// <summary>
		/// Segna il valore di un attributo come da resettare.
		/// La persistenza in database degli attributi resettati ne comporta la cancellazione.
		/// </summary>
		/// <param name="attributeName"></param>
		public void ResetAttribute ( string attributeName )
		{
			if ( this._internalDict[attributeName].HasValue )
			{
				this._modifiedAttributes.Add(attributeName);
				this._internalDict[attributeName].IsResetted = true;
			}
		}

		/// <summary>
		/// Forza un attributo ad assumere un valore anche se è già stato valutato. 
		/// Il membro forza inoltre un ricalcolo degli oggetti il cui stato dipende dallo stato dell' oggetto a cui si riferisce l'attributo.
		/// </summary>
		/// <param name="attributeName">Nome dell'attributo.</param>
		/// <param name="value">Valore a cui impostare l'attributo.</param>
		public void ForceAttribute ( string attributeName, dynamic value )
		{
			this._grisObject.Evaluate();
			this[attributeName] = value;
			// TODO: implementare un flag che distingua i tipi di attributo che comportano ricalcolo dagli altri
			this._grisObject.ForceEvaluationOnDependentObjects();
		}

		public IList<IObjectAttributeType> AttributeTypes
		{
			get { return this._attributeTypes; }
		}

		public bool Contains ( string key )
		{
			if ( !this.IsInitialized ) this.Initialize();
			if ( this._internalDict == null ) return false;
			return this._internalDict.ContainsKey(key);
		}

		/// <summary>
		/// Inizializza la collezione a partire dalla collezione di entità ricevuta dal db.
		/// </summary>
		public void Initialize ()
		{
			if ( _attributeTypes.Any() )
			{
				_count = this._attributeTypes.Count;
				_internalDict = new Dictionary<string, GrisAttribute>(this._count);

				foreach ( var attributeType in _attributeTypes )
				{
					var gAttribute = new GrisAttribute();
					IObjectAttributeType attribType = attributeType;
					var oAttribute = this._grisObject.ObjectAttributes.SingleOrDefault(oa => oa.TypeId == attribType.Id);

					if ( oAttribute != null )
					{
						// in fase di inizializzazione della collezione Attributes va impostato il tipo corretto come indicato dal campo AttributeDataType
						switch ( oAttribute.AttributeDataType )
						{
							case "System.Byte":
								{
									gAttribute.Value = Convert.ToByte(oAttribute.Value);
									break;
								}
							case "System.Int16":
								{
									gAttribute.Value = Convert.ToInt16(oAttribute.Value);
									break;
								}
							case "System.Int32":
								{
									gAttribute.Value = Convert.ToInt32(oAttribute.Value);
									break;
								}
							case "System.Int64":
								{
									gAttribute.Value = Convert.ToInt64(oAttribute.Value);
									break;
								}
							case "System.Boolean":
								{
									gAttribute.Value = Convert.ToBoolean(oAttribute.Value);
									break;
								}
							case "System.DateTime":
								{
									gAttribute.Value = Convert.ToDateTime(oAttribute.Value);
									break;
								}
							case "System.Decimal":
								{
									gAttribute.Value = Convert.ToDecimal(oAttribute.Value);
									break;
								}
							default:
								{
									gAttribute.Value = oAttribute.Value;
									break;
								}
						}
						gAttribute.HasValue = true;
					}
					else
					{
						gAttribute.HasValue = false;
					}

					this._internalDict.Add(attributeType.Name, gAttribute);
				}
				this.IsInitialized = true;
			}
			else
			{
				this.IsInitialized = false;
			}
		}

		/// <summary>
		/// Indica se la collezione è stata inizializzata.
		/// </summary>
		public bool IsInitialized { get; private set; }

		/// <summary>
		/// Sincronizza le entità db originali rispetto agli attributi impostati durante l'esecuzione delle formule.
		/// </summary>
		public void SyncOriginalEntities ()
		{
			var allAttributes = from attributeType in _attributeTypes
								join objectAttribute in this._grisObject.ObjectAttributes on attributeType.Id equals
								  objectAttribute.TypeId into allAttribs
								from oAtt in allAttribs.DefaultIfEmpty()
								where this._modifiedAttributes.Contains(attributeType.Name)
								select new { AttType = attributeType, OAtt = oAtt };

			foreach ( var attPair in allAttributes )
			{
				if ( attPair.OAtt == null )
				{
					var newObjAtt = new Entities.ObjectAttribute();
					newObjAtt.TypeId = attPair.AttType.Id;
					newObjAtt.Value = this._internalDict[attPair.AttType.Name].ToString();
					newObjAtt.AttributeDataType = this._internalDict[attPair.AttType.Name].GetType().ToString();
					newObjAtt.ComputedByFormulaObjectStatusId = this._internalDict[attPair.AttType.Name].ComputedByFormula.GrisObjectId;
					newObjAtt.ComputedByFormulaIndex = this._internalDict[attPair.AttType.Name].ComputedByFormula.Index;
					this._grisObject.ObjectAttributes.Add(newObjAtt);
				}
				else
				{
					attPair.OAtt.Value = this._internalDict[attPair.AttType.Name].ToString();
					attPair.OAtt.AttributeDataType = this._internalDict[attPair.AttType.Name].GetType().ToString();
					attPair.OAtt.ComputedByFormulaObjectStatusId = this._grisObject.Id;
					attPair.OAtt.ComputedByFormulaObjectStatusId = this._internalDict[attPair.AttType.Name].ComputedByFormula.GrisObjectId;
					attPair.OAtt.ComputedByFormulaIndex = this._internalDict[attPair.AttType.Name].ComputedByFormula.Index;
				}
			}
		}

		/// <summary>
		/// Indica se l'oggetto ha modifiche da persistere
		/// </summary>
		public bool HasChanges
		{
			get { return this._modifiedAttributes.Count > 0; }
		}

		/// <summary>
		/// Indica se l'oggetto deve essere persistito in db.
		/// </summary>
		public bool MustBePersisted
		{
			get { return this.HasChanges; }
		}

		/// <summary>
		/// Lista dei figli da persistere in db.
		/// </summary>
		public IList<IStorable> StorableNodes
		{
			get { return null; }
		}

		/// <summary>
		/// Genera il comando di update o insert per l'aggiornamento dei valori degli attributi in db.
		/// </summary>
		/// <returns></returns>
		public string GetUpdateCommand ()
		{
			// il metodo esegue anche la logica propria del metodo SyncOriginalEntities
			const string UPD_CMD = "update object_attributes set AttributeValue = '{0}', AttributeDataType = '{1}', ComputedByFormulaObjectStatusId = '{2}', ComputedByFormulaIndex = '{3}' where AttributeId = {4};";
			const string INS_CMD = "if not exists (select * from object_attributes where AttributeTypeId = {0} and ObjectStatusId = '{1}') insert into object_attributes (AttributeTypeId, ObjectStatusId, AttributeValue, AttributeDataType, ComputedByFormulaObjectStatusId, ComputedByFormulaIndex) values ({0}, '{1}', '{2}', '{3}', '{4}', {5});";
			const string DEL_CMD = "delete object_attributes where AttributeId = {0};";
			var updateCmds = new List<string>(20);

			// elenco di tutti i tipi attributo previsti in cui l'attributo db (ObjectAttribute) può essere presente o meno, già memorizzato o meno
			var allAttributes = from attributeType in _attributeTypes
								join objectAttribute in this._grisObject.ObjectAttributes on attributeType.Id equals
								  objectAttribute.TypeId into allAttribs
								from oAtt in allAttribs.DefaultIfEmpty()
								where this._modifiedAttributes.Contains(attributeType.Name)
								select new { AttType = attributeType, OAtt = oAtt };

			foreach ( var attPair in allAttributes )
			{
				if ( attPair.OAtt == null )
				{
					// l'attributo non è già stato memorizzato in db, insert
					var newObjAtt = new Entities.ObjectAttribute();
					newObjAtt.TypeId = attPair.AttType.Id;
					newObjAtt.Value = this[attPair.AttType.Name].ToString();
					newObjAtt.ComputedByFormulaObjectStatusId = this._internalDict[attPair.AttType.Name].ComputedByFormula.GrisObjectId;
					newObjAtt.ComputedByFormulaIndex = this._internalDict[attPair.AttType.Name].ComputedByFormula.Index;
					this._grisObject.ObjectAttributes.Add(newObjAtt);
					updateCmds.Add(string.Format(INS_CMD, attPair.AttType.Id, this._grisObject.Id, this[attPair.AttType.Name], this.GetTypeString(this[attPair.AttType.Name]), newObjAtt.ComputedByFormulaObjectStatusId, newObjAtt.ComputedByFormulaIndex));
				}
				else
				{
					// l'attributo è già presente in db, update o delete
					if ( this._internalDict[attPair.AttType.Name].IsResetted )
					{
						updateCmds.Add(string.Format(DEL_CMD, attPair.OAtt.Id));
					}
					else
					{
						attPair.OAtt.Value = this[attPair.AttType.Name].ToString();
						attPair.OAtt.ComputedByFormulaObjectStatusId = this._internalDict[attPair.AttType.Name].ComputedByFormula.GrisObjectId;
						attPair.OAtt.ComputedByFormulaIndex = this._internalDict[attPair.AttType.Name].ComputedByFormula.Index;
						updateCmds.Add(string.Format(UPD_CMD, this[attPair.AttType.Name], this.GetTypeString(this[attPair.AttType.Name]), attPair.OAtt.ComputedByFormulaObjectStatusId, attPair.OAtt.ComputedByFormulaIndex, attPair.OAtt.Id));						
					}
				}
			}		

			if ( updateCmds.Count > 0 ) return string.Join(Environment.NewLine, updateCmds);

			return "";
		}

		public bool Contains ( object key )
		{
			return Contains(key.ToString());
		}

		public void Add ( object key, object value )
		{
			if ( this._internalDict != null && value is GrisAttribute )
			{
				this._internalDict.Add(key.ToString(), (GrisAttribute) value);
			}
		}

		public void Clear ()
		{
			if ( this._internalDict != null ) this._internalDict.Clear();
		}

		public IDictionaryEnumerator GetEnumerator ()
		{
			return this._internalDict.GetEnumerator();
		}

		public void Remove ( object key )
		{
			if ( this._internalDict != null ) this._internalDict.Remove(key.ToString());
		}

		object IDictionary.this[object key]
		{
			get { return this[key.ToString()]; }
			set { this[key.ToString()] = value; }
		}

		public ICollection Keys
		{
			get
			{
				if ( this._internalDict != null )
				{
					return this._internalDict.Keys;
				}

				return new List<string>();
			}
		}

		public ICollection Values
		{
			get
			{
				if ( this._internalDict != null )
				{
					return this._internalDict.Values;
				}

				return new List<string>();
			}
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool IsFixedSize
		{
			get { return false; }
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return GetEnumerator();
		}

		public void CopyTo ( Array array, int index )
		{
			throw new NotImplementedException();
		}

		private int _count;
		public int Count
		{
			get { return this._count; }
		}

		public object SyncRoot
		{
			get { throw new NotImplementedException(); }
		}

		public bool IsSynchronized
		{
			get { throw new NotImplementedException(); }
		}

		private string GetTypeString ( dynamic value )
		{
			if ( value is ObjectSeverityDetail ) return "System.Int32";
			return value.GetType().ToString();
		}
	}

	class GrisAttribute
	{
		public dynamic Value;
		public bool HasValue;
		public bool IsResetted;
		public IObjectFormula ComputedByFormula;
	}
}

﻿
namespace GrisSuite.FormulaEngine.Contracts.Severities
{
    /// <summary>
    ///   Rappresenta la severità dei valori ricevuti
    /// </summary>
    public static class Severity
    {
    	/// <summary>
    	///   Valore informativo (valido solo per stream e stream field)
    	/// </summary>
    	public static readonly int Info = -255;

    	/// <summary>
    	///   Non classificato
    	/// </summary>
		public static readonly int NotClassified = -1;

    	/// <summary>
    	///   Stato normale
    	/// </summary>
		public static readonly int Ok = 0;

    	/// <summary>
    	///   Allarme lieve
    	/// </summary>
		public static readonly int Warning = 1;

    	/// <summary>
    	///   Allarme grave
    	/// </summary>
		public static readonly int Error = 2;

    	/// <summary>
    	///   Offline
    	/// </summary>
		public static readonly int Offline = 3;

    	/// <summary>
    	///   Non attivo
    	/// </summary>
		public static readonly int NotActive = 9;

    	/// <summary>
    	///   Sconosciuto o nessuna risposta
    	/// </summary>
		public static readonly int Unknown = 255;
    }
}
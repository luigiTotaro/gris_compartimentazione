
using System.Runtime.Serialization;

namespace GrisSuite.FormulaEngine.Contracts
{
	/// <summary>
	/// Formula che permette la valutazione degli attributi di un oggetto.
	/// </summary>
	public interface IEvaluationFormula
	{
		/// <summary>
		/// Il path in cui si trova il file di script per la determinazione degli attributi dell' oggetto.
		/// </summary>
		string ScriptPath { get; }

		/// <summary>
		/// Indica se la formula � una formula di default.
		/// </summary>
		bool IsDefault { get; }

		/// <summary>
		/// Indica se la formula � in uno stato valido e pu� essere valutata.
		/// </summary>
		bool CanEvaluate { get; }

		/// <summary>
		/// Indica se sono avvenuti errori durante la valutazione della formula.
		/// </summary>
		[DataMember]
		bool HasErrorInEvaluation { get; }

		/// <summary>
		/// Restituisce l'errore avvenuto durante la valutazione della formula.
		/// </summary>
		[DataMember]
		string EvaluationError { get; set; }

		/// <summary>
		/// Riferimento all' oggetto possessore della formula.
		/// </summary>
		IEvaluableObject Owner { get; }
	}

	/// <summary>
	/// Tipo di formula.
	/// </summary>
	public enum EvaluationFormulaType
	{
		// Formula di default statistica.
		Default = 0,
		// Formula specifica per oggetto.
		Custom = 1
	}
}
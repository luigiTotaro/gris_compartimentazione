﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface INode : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Nome della stazione.
		/// </summary>
		[DataMember]
		string Name { get; set; }

		/// <summary>
		/// Chilometrica associata alla stazione.
		/// </summary>
		[DataMember]
		int Meters { get; set; }

		/// <summary>
		/// Identificativo specifico della stazione.
		/// </summary>
		[DataMember]
		long NodeId { get; set; }

		/// <summary>
		/// Identificativo della linea a cui appartiene la stazione.
		/// </summary>
		[DataMember]
		Guid ZoneId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della linea a cui appartiene la stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IZone Zone { get; set; }

		/// <summary>
		/// Istanze dei Sistemi presenti nella stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<INodeSystem> Systems { get; set; }

		/// <summary>
		/// Istanze dei Server presenti nella stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IServer> Servers { get; set; }

		/// <summary>
		/// Istanze delle Periferiche presenti nella stazione.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IDevice> Devices { get; set; }

		#endregion
	}
}
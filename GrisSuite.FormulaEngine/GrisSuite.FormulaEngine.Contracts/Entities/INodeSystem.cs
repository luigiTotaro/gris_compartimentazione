﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface INodeSystem : IGrisObject
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo specifico del NodeSystem.
		/// </summary>
		[DataMember]
		long? NodeSystemId { get; set; }

		/// <summary>
		/// Identificativo della stazione correlata al NodeSystem.
		/// </summary>
		[DataMember]
		Guid NodeId { get; set; }

		/// <summary>
		/// Identificativo del sistema.
		/// </summary>
		[DataMember]
		int SystemId { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// Istanza della stazione relativa al NodeSystem.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		INode Node { get; set; }

		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityEnd<INode> NodeReference { get; set; }

		/// <summary>
		/// Istanze delle periferiche comprese nel NodeSystem.
		/// </summary>
		[XmlIgnore]
		[SoapIgnoreAttribute]
		[DataMember]
		IEntityCollection<IDevice> Devices { get; set; }

		#endregion
	}
}
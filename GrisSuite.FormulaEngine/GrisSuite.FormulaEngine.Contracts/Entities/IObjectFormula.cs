﻿
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GrisSuite.FormulaEngine.Contracts.Entities
{
	public interface IObjectFormula
	{
		#region Primitive Properties

		/// <summary>
		/// Identificativo del GrisObject associato.
		/// </summary>
		[DataMember]
		Guid ObjectId { get; set; }

		/// <summary>
		/// Indice di Formula del GrisObject.
		/// </summary>
		[DataMember]
		Byte Index { get; set; }

		/// <summary>
		/// Path al file contenente lo script della Formula.
		/// </summary>
		[DataMember]
		String ScriptPath { get; set; }

		/// <summary>
		/// Indice globale di Formula.
		/// </summary>
		[DataMember]
		Int64 FormulaGlobalIndex { get; set; }

		#endregion

		#region Navigation Properties

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IGrisObject GrisObject { get; set; }

		/// <summary>
		/// No Metadata Documentation available.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[DataMember]
		IEntityCollection<IObjectAttribute> ComputedObjectAttributes { get; set; }

		#endregion
	}
}

﻿
using GrisSuite.FormulaEngine.Contracts.Entities;

namespace GrisSuite.FormulaEngine.Contracts
{
	public interface IObjectFactory
	{
		IAttribute GetAttribute ();
		IObjectAttribute GetObjectAttribute ();
		IGrisObject GetGrisObject ();
	}
}

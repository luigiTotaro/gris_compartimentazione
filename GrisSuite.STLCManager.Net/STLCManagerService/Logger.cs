﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Security;
using STLCManager.Service.LoggerSet;

namespace STLCManager.Service
{
	public class Logger
	{
		public const int I_GENERIC = 10000;

		public const int W_GENERIC = 20000;

		public const int E_GENERIC = 30000;

		//WCFLayer
		public const int E_STACK_TRACE = 40000;
		public const int E_MESSAGE_SECURITY_EXCEPTION = 40001;
		public const int E_COMMUNICATION_OBJECT_FAULTED_EXCEPTION = 40002;
		public const int E_ENDPOINT_NOT_FOUND_EXCEPTION = 40003;
		public const int E_FAULT_EXCEPTION = 40004;
		public const int E_SERVER_TOO_BUSY_EXCEPTION = 40005;

		//A tutti i livelli
		public const int E_TIMEOUT_EXCEPTION = 40100;

		public enum LogType
		{
			CONSOLE,
			LOC_FILE,
			CONSOLE_LOC_FILE
		};

		public enum EventType
		{
			INFORMATION,
			WARNING,
			ERROR,
            NOTICE
		};

        /// <summary>
        /// Recupero dell'istanza singleton della classe
        /// </summary>
        public static Logger Default
        {
            get
            {
                if (_logInstance == null)
                {
                    _logInstance = new Logger();
                }

                return _logInstance;
            }
        }

        private static Logger _logInstance = null;

		public static   CompositeLogger STLCManagerCompositeLogger;

		private static EventLogger      STLCManagerEventLogger;

        protected Logger()
        {
			try
			{
				STLCManagerEventLogger      = new EventLogger       ("GrisSuite.STLCManager");
				STLCManagerCompositeLogger  = new CompositeLogger   (new FileLogger(AppCfg.Default.sPathLogFolder + "\\STLCManager"), new EventLogger("STLCManager"));
            }
			catch (Exception ex)
			{
				LogErrorToEventLog(ex.ToString());
			}
		}

		public static void LogErrorToEventLog(string strMessage)
		{
			if (STLCManagerEventLogger != null)
			{
				STLCManagerEventLogger.AddErrorEvent(strMessage, -1);
			}
			else
			{
				EventLog.WriteEntry("GrisSuite.STLCManager", strMessage, EventLogEntryType.Error);
			}
		}

		public void Log(string strMessage, LogType logType, EventType evType, int idEvent)
		{
			try
			{
				this.LogOnFile(strMessage, logType);

				switch (evType)
				{
                    case EventType.NOTICE:

                        STLCManagerEventLogger.AddInfoEvent(strMessage, idEvent);

                        break;
                    
                    case EventType.INFORMATION:

                        if(AppCfg.Default.lvEventLogLevel == EventLogLevel.INFO)
                        { 
						    STLCManagerEventLogger.AddInfoEvent(strMessage, idEvent);
                        }

						break;

					case EventType.WARNING:

                        if ((AppCfg.Default.lvEventLogLevel == EventLogLevel.INFO) || (AppCfg.Default.lvEventLogLevel == EventLogLevel.WARNING))
                        { 
						    STLCManagerEventLogger.AddWarningEvent(strMessage, idEvent);
                        }

						break;

					case EventType.ERROR:

						STLCManagerEventLogger.AddErrorEvent(strMessage, idEvent);
						
                        break;

					default:
						STLCManagerEventLogger.AddErrorEvent("EvType non valido ** " + strMessage, idEvent);
						break;
				}
			}
			catch (Exception ex)
			{
				LogErrorToEventLog(ex.ToString());
			}
		}

		public void Log(string strMessage, LogType logType, EventType evType)
		{
			try
			{
				switch (evType)
				{
					case EventType.INFORMATION:
						this.Log(strMessage, logType, evType, I_GENERIC);
						break;

					case EventType.WARNING:
						this.Log(strMessage, logType, evType, W_GENERIC);
						break;

					case EventType.ERROR:
					default:
						this.Log(strMessage, logType, evType, E_GENERIC);
						break;
				}
			}
			catch (Exception ex)
			{
				LogErrorToEventLog(ex.ToString());
			}
		}

		public void Log(string strMessage, LogType logType)
		{
			try
			{
				this.LogOnFile(strMessage, logType);
			}
			catch (Exception ex)
			{
				this.ManageException(ex);
			}
		}

		private void LogOnFile(string strMessage, LogType logType)
		{
			try
			{
                if ((logType == LogType.LOC_FILE) || (logType == LogType.CONSOLE_LOC_FILE))
                {
                    AddSTLCManagerLog(strMessage);
                }

                if ((Environment.UserInteractive) && ((logType == LogType.CONSOLE) || (logType == LogType.CONSOLE_LOC_FILE)))
                {
                    Console.WriteLine("{0:G} -- {1}", DateTime.Now, strMessage);
                }
			}
			catch (Exception ex)
			{
				this.ManageException(ex);
			}
		}

		private void AddSTLCManagerLog(string msg)
		{
		    if (AppCfg.Default.bLogOnFileActive) // .LOG_ON_FILE_ACTIVE)
		    {
		        try
		        {
		            STLCManagerCompositeLogger.LogMessage(msg, TipoLog.LogSuFile);
		        }
		        catch (Exception ex)
		        {
		            LogErrorToEventLog(ex.ToString());
		        }
		    }
		}

		public void ManageException(Exception ex)
		{
			//Eccezioni CommWCFLayer
			if (ex.GetType() == typeof (MessageSecurityException))
			{
				this.Log("ECCEZIONE WCFLayer: MessageSecurityException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace,
					LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_MESSAGE_SECURITY_EXCEPTION);
			}
			else if (ex.GetType() == typeof (ProtocolException))
			{
				this.Log("ECCEZIONE WCFLayer: ProtocolException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace,
					LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_SERVER_TOO_BUSY_EXCEPTION);
			}
			else if (ex.GetType() == typeof (ServerTooBusyException))
			{
				this.Log("ECCEZIONE WCFLayer: ServerTooBusyException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace,
					LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_SERVER_TOO_BUSY_EXCEPTION);
			}
			else if (ex.GetType() == typeof (FaultException))
			{
				this.Log("ECCEZIONE WCFLayer: FaultException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace,
					LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_FAULT_EXCEPTION);
			}
			else if (ex.GetType() == typeof (CommunicationObjectFaultedException))
			{
				this.Log(
					"ECCEZIONE WCFLayer: CommunicationObjectFaultedException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace,
					LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_COMMUNICATION_OBJECT_FAULTED_EXCEPTION);
			}
			else if (ex.GetType() == typeof (EndpointNotFoundException))
			{
				this.Log("ECCEZIONE WCFLayer: EndpointNotFoundException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace,
					LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_ENDPOINT_NOT_FOUND_EXCEPTION);
			}
			else if (ex.GetType() == typeof (CommunicationException))
			{
				this.Log("ECCEZIONE WCFLayer: CommunicationException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace,
					LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_FAULT_EXCEPTION);
			}
			else if (ex.GetType() == typeof (TimeoutException))
			{
				this.Log("ECCEZIONE TimeoutException. " + ex.Message + " STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace, LogType.CONSOLE_LOC_FILE,
					EventType.ERROR, E_TIMEOUT_EXCEPTION);
			}
            else
			{
				this.Log("ECCEZIONE GENERICA. " + ex.GetType() + " - " + ex.Message, LogType.CONSOLE_LOC_FILE, EventType.ERROR);
				this.Log("STACK TRACE ECCEZIONE: " + ex.GetType() + " - " + ex.StackTrace, LogType.CONSOLE_LOC_FILE, EventType.ERROR, E_STACK_TRACE);

				LogErrorToEventLog(ex.ToString());

				if (Environment.UserInteractive)
				{
					Environment.Exit(1);
				}
			}
		}
	}
}
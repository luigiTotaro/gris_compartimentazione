﻿using System;
using System.ServiceModel;

namespace STLCManager.Service.ClientComm
{
    public class STLCInfoEventArgs : EventArgs
    {
        public STLCInfoRequest Request { get; set; }
        public STLCInfoResult Result { get; private set; }

        public STLCInfoEventArgs()
        {
            this.Request = new STLCInfoRequest();
            this.Result = new STLCInfoResult();
        }
    }

    public class STLCStatusEventArgs : EventArgs
    {
        public STLCStatusRequest Request { get; set; }
        public STLCStatusResult Result { get; private set; }

        public STLCStatusEventArgs()
        {
            this.Request = new STLCStatusRequest();
            this.Result = new STLCStatusResult();
        }
    }

    public class STLCModeEventArgs : EventArgs
    {
        public STLCModeRequest Request { get; set; }
        public STLCModeResult Result { get; private set; }

        public STLCModeEventArgs()
        {
            this.Request = new STLCModeRequest();
            this.Result = new STLCModeResult();
        }
    }

    public class STLCServiceStatusEventArgs : EventArgs
    {
        public STLCServiceStatusRequest Request { get; set; }
        public STLCServiceStatusResult Result { get; private set; }

        public STLCServiceStatusEventArgs()
        {
            this.Request = new STLCServiceStatusRequest();
            this.Result = new STLCServiceStatusResult();
        }
    }

    public class STLCShutdownEventArgs : EventArgs
    {
        public STLCShutdownRequest Request { get; set; }
        public STLCShutdownResult Result { get; private set; }

        public STLCShutdownEventArgs()
        {
            this.Request = new STLCShutdownRequest();
            this.Result = new STLCShutdownResult();
        }
    }

    public class STLCApplySystemConfigEventArgs : EventArgs
    {
        public STLCApplySystemConfigRequest Request { get; set; }
        public STLCApplySystemConfigResult Result { get; private set; }

        public STLCApplySystemConfigEventArgs()
        {
            this.Request = new STLCApplySystemConfigRequest();
            this.Result = new STLCApplySystemConfigResult();
        }
    }

    public class STLCNetworkEventArgs : EventArgs
    {
        public STLCNetworkRequest Request { get; set; }
        public STLCNetworkResult Result { get; private set; }

        public STLCNetworkEventArgs()
        {
            this.Request = new STLCNetworkRequest();
            this.Result = new STLCNetworkResult();
        }
    }

    public class ReloadConfigEventArgs : EventArgs
    {
        public ReloadConfigRequest Request { get; set; }
        public ReloadConfigResult Result { get; private set; }

        public ReloadConfigEventArgs()
        {
            this.Request = new ReloadConfigRequest();
            this.Result = new ReloadConfigResult();
        }
    }

    public class ReloadSystemXmlIntoDatabaseEventArgs : EventArgs
    {
        public ReloadSystemXmlIntoDatabaseRequest Request { get; set; }
        public ReloadSystemXmlIntoDatabaseResult Result { get; private set; }

        public ReloadSystemXmlIntoDatabaseEventArgs()
        {
            this.Request = new ReloadSystemXmlIntoDatabaseRequest();
            this.Result = new ReloadSystemXmlIntoDatabaseResult();
        }
    }

    public class UpdateRegionListFromWSEventArgs : EventArgs
    {
        public UpdateRegionListFromWSRequest Request { get; set; }
        public UpdateRegionListFromWSResult Result { get; private set; }

        public UpdateRegionListFromWSEventArgs()
        {
            this.Request = new UpdateRegionListFromWSRequest();
            this.Result = new UpdateRegionListFromWSResult();
        }
    }

    public class UpdateDeviceTypeListFromWSEventArgs : EventArgs
    {
        public UpdateDeviceTypeListFromWSRequest    Request     { get; set; }
        public UpdateDeviceTypeListFromWSResult     Result      { get; private set; }

        public UpdateDeviceTypeListFromWSEventArgs()
        {
            this.Request    = new UpdateDeviceTypeListFromWSRequest ();
            this.Result     = new UpdateDeviceTypeListFromWSResult  ();
        }
    }

    public class GetNeededSupervisorsEventArgs : EventArgs
    {
        public GetNeededSupervisorsRequest  Request { get; set; }
        public GetNeededSupervisorsResult   Result  { get; private set; }

        public GetNeededSupervisorsEventArgs()
        {
            this.Request    = new GetNeededSupervisorsRequest   ();
            this.Result     = new GetNeededSupervisorsResult    ();
        }
    }

    public class SetConfigOnRegistryEventArgs : EventArgs
    {
        public SetConfigOnRegistryRequest    Request { get; set; }
        public SetConfigOnRegistryResult     Result  { get; private set; }

        public SetConfigOnRegistryEventArgs()
        {
            this.Request    = new SetConfigOnRegistryRequest ();
            this.Result     = new SetConfigOnRegistryResult  ();
        }
    }

    public class EmptyDatabaseEventArgs : EventArgs
    {
        public EmptyDatabaseRequest Request { get; set; }
        public EmptyDatabaseResult Result { get; private set; }

        public EmptyDatabaseEventArgs()
        {
            this.Request = new EmptyDatabaseRequest();
            this.Result = new EmptyDatabaseResult();
        }
    }


    // NOTA: se si modifica il nome della classe "ClientCommandService" qui, è necessario aggiornare anche il riferimento a "ClientCommandService" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, Namespace = "http://telefin.it")]
    public class ClientCommandService : IClientCommandService
    {
        public event EventHandler<STLCInfoEventArgs>                    OnGetSTLCInfoCommand;
        public event EventHandler<STLCStatusEventArgs>                  OnGetSTLCStatusCommand;
        public event EventHandler<STLCModeEventArgs>                    OnSetSTLCModeCommand;
        public event EventHandler<STLCServiceStatusEventArgs>           OnSetSTLCServiceStatusCommand;
        public event EventHandler<STLCServiceStatusEventArgs>           OnGetSTLCServiceStatusCommand;
        public event EventHandler<STLCShutdownEventArgs>                OnSTLCShutdownCommand;
        public event EventHandler<STLCApplySystemConfigEventArgs>       OnSTLCApplySystemConfigCommand;
        public event EventHandler<STLCNetworkEventArgs>                 OnGetSTLCNetworkCommand;
        public event EventHandler<ReloadConfigEventArgs>                OnReloadConfigCommand;
        public event EventHandler<ReloadSystemXmlIntoDatabaseEventArgs> OnReloadSystemXmlIntoDatabaseCommand;
        public event EventHandler<UpdateRegionListFromWSEventArgs>      OnUpdateRegionListFromWSCommand;
        public event EventHandler<UpdateDeviceTypeListFromWSEventArgs>  OnUpdateDeviceTypeListFromWSCommand;
        public event EventHandler<GetNeededSupervisorsEventArgs>        OnGetNeededSupervisorsCommand;
        public event EventHandler<SetConfigOnRegistryEventArgs>         OnSetConfigOnRegistryCommand;
        public event EventHandler<EmptyDatabaseEventArgs>               OnEmptyDatabaseCommand;

        #region Membri di IClientCommandService

        public STLCInfoResult getSTLCInfo(STLCInfoRequest request)
        {
            STLCInfoEventArgs STLCIEventArgs                                            = new STLCInfoEventArgs();
            STLCIEventArgs.Request.clientType                                           = request.clientType;
            this.OnGetSTLCInfoCommand(this, STLCIEventArgs);
            return STLCIEventArgs.Result;
        }

        public STLCStatusResult getSTLCStatus(STLCStatusRequest request)
        {
            STLCStatusEventArgs STLCSEventArgs                                          = new STLCStatusEventArgs();
            STLCSEventArgs.Request.clientType                                           = request.clientType;
            this.OnGetSTLCStatusCommand(this, STLCSEventArgs);
            return STLCSEventArgs.Result;
        }

        public STLCModeResult setSTLCMode(STLCModeRequest request)
        {
            STLCModeEventArgs STLCMEventArgs                                            = new STLCModeEventArgs();
            STLCMEventArgs.Request.clientType                                           = request.clientType;
            STLCMEventArgs.Request.mode                                                 = request.mode;
            STLCMEventArgs.Request.managedTimeoutHours                                  = request.managedTimeoutHours;
            this.OnSetSTLCModeCommand(this, STLCMEventArgs);
            return STLCMEventArgs.Result;
        }

        public STLCServiceStatusResult setServiceStatus(STLCServiceStatusRequest request)
        {
            STLCServiceStatusEventArgs STLCSSEventArgs                                  = new STLCServiceStatusEventArgs();
            STLCSSEventArgs.Request.clientType                                          = request.clientType;
            STLCSSEventArgs.Request.command                                             = request.command;
            STLCSSEventArgs.Request.services                                            = request.services;
            STLCSSEventArgs.Request.group                                               = request.group;
            this.OnSetSTLCServiceStatusCommand(this, STLCSSEventArgs);
            return STLCSSEventArgs.Result;
        }

        public STLCServiceStatusResult getServiceStatus(STLCServiceStatusRequest request)
        {
            STLCServiceStatusEventArgs STLCSSEventArgs                                  = new STLCServiceStatusEventArgs();
            STLCSSEventArgs.Request.clientType                                          = request.clientType;
            STLCSSEventArgs.Request.services                                            = request.services;
            STLCSSEventArgs.Request.group                                               = request.group;
            this.OnGetSTLCServiceStatusCommand(this, STLCSSEventArgs);
            return STLCSSEventArgs.Result;
        }

        public STLCShutdownResult STLCShutdown(STLCShutdownRequest request)
        {
            STLCShutdownEventArgs STLCSEventArgs                                        = new STLCShutdownEventArgs();
            STLCSEventArgs.Request.clientType                                           = request.clientType;
            STLCSEventArgs.Request.shutdownType                                         = request.shutdownType;
            this.OnSTLCShutdownCommand(this, STLCSEventArgs);
            return STLCSEventArgs.Result;
        }

        public STLCApplySystemConfigResult applySystemConfig(STLCApplySystemConfigRequest request)
        {
            STLCApplySystemConfigEventArgs STLCASCEventArgs                             = new STLCApplySystemConfigEventArgs();
            STLCASCEventArgs.Request.clientType                                         = request.clientType;
            this.OnSTLCApplySystemConfigCommand(this, STLCASCEventArgs);
            return STLCASCEventArgs.Result;
        }

        public STLCNetworkResult getSTLCNetwork(STLCNetworkRequest request)
        {
            STLCNetworkEventArgs STLCNetEventArgs                                       = new STLCNetworkEventArgs();
            STLCNetEventArgs.Request.clientType                                         = request.clientType;
            this.OnGetSTLCNetworkCommand(this, STLCNetEventArgs);
            return STLCNetEventArgs.Result;
        }

        public ReloadConfigResult reloadConfig(ReloadConfigRequest request)
        {
            ReloadConfigEventArgs reloadConfigEventArgs                                 = new ReloadConfigEventArgs();
            reloadConfigEventArgs.Request.clientType                                    = request.clientType;
            this.OnReloadConfigCommand(this, reloadConfigEventArgs);
            return reloadConfigEventArgs.Result;
        }

        public ReloadSystemXmlIntoDatabaseResult reloadSystemXmlIntoDatabase(ReloadSystemXmlIntoDatabaseRequest request)
        {
            ReloadSystemXmlIntoDatabaseEventArgs reloadSystemXmlIntoDatabaseEventArgs   = new ReloadSystemXmlIntoDatabaseEventArgs();
            reloadSystemXmlIntoDatabaseEventArgs.Request.clientType                     = request.clientType;
            reloadSystemXmlIntoDatabaseEventArgs.Request.mode                           = request.mode;
            this.OnReloadSystemXmlIntoDatabaseCommand(this, reloadSystemXmlIntoDatabaseEventArgs);
            return reloadSystemXmlIntoDatabaseEventArgs.Result;
        }

        public UpdateRegionListFromWSResult updateRegionListFromWS(UpdateRegionListFromWSRequest request)
        {
            UpdateRegionListFromWSEventArgs updateRegionListFromWSEventArgs             = new UpdateRegionListFromWSEventArgs();
            updateRegionListFromWSEventArgs.Request.clientType                          = request.clientType;
            this.OnUpdateRegionListFromWSCommand(this, updateRegionListFromWSEventArgs);
            return updateRegionListFromWSEventArgs.Result;
        }

        public UpdateDeviceTypeListFromWSResult updateDeviceTypeListFromWS(UpdateDeviceTypeListFromWSRequest request)
        {
            UpdateDeviceTypeListFromWSEventArgs updateDeviceTypeListFromWSEventArgs     = new UpdateDeviceTypeListFromWSEventArgs();
            updateDeviceTypeListFromWSEventArgs.Request.clientType                      = request.clientType;
            this.OnUpdateDeviceTypeListFromWSCommand(this, updateDeviceTypeListFromWSEventArgs);
            return updateDeviceTypeListFromWSEventArgs.Result;
        }

        public GetNeededSupervisorsResult getNeededSupervisors(GetNeededSupervisorsRequest request)
        {
            GetNeededSupervisorsEventArgs getNeededSupervisorsEventArgs                 = new GetNeededSupervisorsEventArgs();
            getNeededSupervisorsEventArgs.Request.clientType                            = request.clientType;
            this.OnGetNeededSupervisorsCommand(this, getNeededSupervisorsEventArgs);
            return getNeededSupervisorsEventArgs.Result;
        }

        public SetConfigOnRegistryResult setConfigOnRegistry(SetConfigOnRegistryRequest request)
        {
            SetConfigOnRegistryEventArgs setConfigOnRegistryEventArgs                   = new SetConfigOnRegistryEventArgs();
            setConfigOnRegistryEventArgs.Request.clientType                             = request.clientType;
            setConfigOnRegistryEventArgs.Request.cfgOnRegistryCommand                   = request.cfgOnRegistryCommand;
            this.OnSetConfigOnRegistryCommand(this, setConfigOnRegistryEventArgs);
            return setConfigOnRegistryEventArgs.Result;
        }

        public EmptyDatabaseResult emptyDatabase(EmptyDatabaseRequest request)
        {
            EmptyDatabaseEventArgs emptyDatabaseEventArgs = new EmptyDatabaseEventArgs();
            emptyDatabaseEventArgs.Request.clientType = request.clientType;
            this.OnEmptyDatabaseCommand(this, emptyDatabaseEventArgs);
            return emptyDatabaseEventArgs.Result;
        }

        #endregion
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.ServiceModel;
using STLCManager.Service.Database;
using STLCManager.Service.Types;

namespace STLCManager.Service.ClientComm
{
    //[DataContract]
    //public class ClientServiceResult
    //{
    //    public enum status
    //    {
    //        SUCCESS,
    //        FAIL
    //    }
    //    [DataMember]
    //    public status currentStatus { get; set; }
    //    [DataMember]
    //    public string errorDescription { get; set; }
    //}

    public enum RequestingClientType
    {
        COMMAND_LINE,
        SKC,
        MAIN_SERVICE,
        UNKNOWN
    }

    public enum StatusLevel
    {
        SUCCESS,
        FAIL,
        EMPTY
    }

    public enum STLCMode
    {
        NORMAL,
        MAINTENANCE
    }

    public enum STLCShutdownType
    {
        POWER_OFF,
        REBOOT
    }

    public enum STLCSensorTemperature
    {
        CPU,
        MOTHER_BOARD,
        NOT_AVAILABLE
    }

    public enum STLCServiceCommand
    {
        START,
        STOP,
        RESTART
    }

    public enum CfgOnRegistryCommand
    {
        CREATE_ALL,
        DELETE_ALL,
        DELETE_ONLY_CONFIG,
        NOT_SPECIFIED
    }

    #region Dichiarazione strutture per getSTLCInfo()

    [DataContract]
    public class STLCInfoRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public STLCInfoRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class STLCInfoResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public string serialNumber { get; set; }

            [DataMember]
            public string version { get; set; }

            [DataMember]
            public string hostName { get; set; }

            [DataMember]
            public string hardwareProfile { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public STLCInfoResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData {serialNumber = "", version = "", hostName = ""};
        }
    }

    #endregion

    #region Dichiarazione strutture per getSTLCStatus()

    [DataContract]
    public class STLCStatusRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public STLCStatusRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class STLCStatusResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public STLCMode mode { get; set; }

            [DataMember]
            public STLCSensorTemperature sensorTemperature { get; set; }

            [DataMember]
            public int temperature { get; set; }

            [DataMember]
            public int pwmRightFan { get; set; }

            [DataMember]
            public int pwmLeftFan { get; set; }

            [DataMember]
            public bool filterFBWFEnabled { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public STLCStatusResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData
            {
                mode = STLCMode.NORMAL,
                sensorTemperature = STLCSensorTemperature.CPU,
                temperature = 0,
                pwmRightFan = 0,
                pwmLeftFan = 0
            };
        }
    }

    #endregion

    #region Dichiarazione strutture per setSTLCMode()

    [DataContract]
    public class STLCModeRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        [DataMember]
        public STLCMode mode { get; set; }

        [DataMember]
        public int managedTimeoutHours { get; set; }

        public STLCModeRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
            this.mode = STLCMode.NORMAL;
            this.managedTimeoutHours = 0;
        }
    }

    [DataContract]
    public class STLCModeResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public STLCMode mode { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public STLCModeResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData {mode = STLCMode.NORMAL};
        }
    }

    #endregion

    #region Dichiarazione strutture per setServiceStatus() e getServiceStatus()

    [DataContract]
    public class STLCServiceStatusRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        [DataMember]
        public STLCServiceCommand command { get; set; }

        [DataMember]
        public List<string> services { get; set; }

        [DataMember]
        public string group { get; set; }

        public STLCServiceStatusRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
            this.command = STLCServiceCommand.STOP;
            this.services = new List<string>();
            this.@group = null;
        }
    }

    [DataContract]
    public class STLCServiceStatusResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public List<STLCServiceStatus> servicesStatus { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public STLCServiceStatusResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData {servicesStatus = new List<STLCServiceStatus>()};
        }
    }

    #endregion

    #region Dichiarazione strutture per STLCShutdown()

    [DataContract]
    public class STLCShutdownRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        [DataMember]
        public STLCShutdownType shutdownType { get; set; }

        public STLCShutdownRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
            this.shutdownType = STLCShutdownType.POWER_OFF;
        }
    }

    [DataContract]
    public class STLCShutdownResult
    {
        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        public STLCShutdownResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
        }
    }

    #endregion

    #region Dichiarazione strutture per applySystemConfig()

    [DataContract]
    public class STLCApplySystemConfigRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public STLCApplySystemConfigRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class STLCApplySystemConfigResult
    {
        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }

            [DataMember]
            public int resetRequired { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        public STLCApplySystemConfigResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
        }
    }

    #endregion

    #region Dichiarazione strutture per getSTLCNetwork()

    [DataContract]
    public class STLCNetworkRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public STLCNetworkRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class STLCNetworkResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public List<STLCNetwork> Network { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public STLCNetworkResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData {Network = new List<STLCNetwork>()};
        }
    }

    #endregion

    #region Dichiarazione strutture per reloadConfig()

    [DataContract]
    public class ReloadConfigRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public ReloadConfigRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class ReloadConfigResult
    {
        [DataContract]
        public class ResultData
        {
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public ReloadConfigResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData();
        }
    }

    #endregion

    #region Dichiarazione strutture per reloadSystemXmlIntoDatabase()

    [DataContract]
    public class ReloadSystemXmlIntoDatabaseRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        [DataMember]
        public STLCMode mode { get; set; }

        public ReloadSystemXmlIntoDatabaseRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
            this.mode = STLCMode.NORMAL;
        }
    }

    [DataContract]
    public class ReloadSystemXmlIntoDatabaseResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public ReadOnlyCollection<string> Devices { get; set; }

            [DataMember]
            public string OperationDescription { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public ReloadSystemXmlIntoDatabaseResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData();
        }
    }

    #endregion

    #region Dichiarazione strutture per updateRegionListFromWS()

    [DataContract]
    public class UpdateRegionListFromWSRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public UpdateRegionListFromWSRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class UpdateRegionListFromWSResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public string OperationDescription { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public UpdateRegionListFromWSResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData();
        }
    }

    #endregion

    #region Dichiarazione strutture per updateDeviceTypeListFromWS()

    [DataContract]
    public class UpdateDeviceTypeListFromWSRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public UpdateDeviceTypeListFromWSRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class UpdateDeviceTypeListFromWSResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public string OperationDescription { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public UpdateDeviceTypeListFromWSResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData();
        }
    }

    #endregion

    #region Dichiarazione strutture per getNeededSupervisors()

    [DataContract]
    public class GetNeededSupervisorsRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public GetNeededSupervisorsRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class GetNeededSupervisorsResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public ReadOnlyCollection<string> Supervisors { get; set; }

            [DataMember]
            public string OperationDescription { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public GetNeededSupervisorsResult()
        {
            this.status = new ResultStatus {level = StatusLevel.FAIL};
            this.data = new ResultData();
        }
    }

    #endregion

    #region Dichiarazione strutture per setConfigOnRegistry

    [DataContract]
    public class SetConfigOnRegistryRequest
    {
        [DataMember]
        public RequestingClientType clientType              { get; set; }

        [DataMember]
        public CfgOnRegistryCommand cfgOnRegistryCommand    { get; set; }

        public SetConfigOnRegistryRequest()
        {
            this.clientType             = RequestingClientType.UNKNOWN;
            this.cfgOnRegistryCommand   = CfgOnRegistryCommand.NOT_SPECIFIED;
        }
    }

    [DataContract]
    public class SetConfigOnRegistryResult
    {
        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public CfgOnRegistryCommand cmdExecuted         { get; set; }

            [DataMember]
            public List<string>         listKeys            { get; set; }

            [DataMember]
            public StatusLevel          level               { get; set; }

            [DataMember]
            public int                  errorCode           { get; set; }

            [DataMember]
            public string               errorDescription    { get; set; }
        }

        [DataMember]
        public ResultStatus status  { get; set; }

        public SetConfigOnRegistryResult()
        {
            this.status = new ResultStatus { level = StatusLevel.FAIL, cmdExecuted = CfgOnRegistryCommand.NOT_SPECIFIED, listKeys = new List<string>() };
        }
    }

    #endregion

    #region Dichiarazione strutture per emptyDatabase()

    [DataContract]
    public class EmptyDatabaseRequest
    {
        [DataMember]
        public RequestingClientType clientType { get; set; }

        public EmptyDatabaseRequest()
        {
            this.clientType = RequestingClientType.UNKNOWN;
        }
    }

    [DataContract]
    public class EmptyDatabaseResult
    {
        [DataContract]
        public class ResultData
        {
            [DataMember]
            public ReadOnlyCollection<string> Devices { get; set; }

            [DataMember]
            public string OperationDescription { get; set; }
        }

        [DataContract]
        public class ResultStatus
        {
            [DataMember]
            public StatusLevel level { get; set; }

            [DataMember]
            public int errorCode { get; set; }

            [DataMember]
            public string errorDescription { get; set; }
        }

        [DataMember]
        public ResultStatus status { get; set; }

        [DataMember]
        public ResultData data { get; set; }

        public EmptyDatabaseResult()
        {
            this.status = new ResultStatus { level = StatusLevel.FAIL };
            this.data = new ResultData();
        }
    }

    #endregion

    // NOTA: se si modifica il nome dell'interfaccia "IClientCommandService" qui, è necessario aggiornare anche il riferimento a "IClientCommandService" in App.config.
    [ServiceContract(Namespace = "http://telefin.it")]
    public interface IClientCommandService
    {
        [OperationContract]
        STLCInfoResult                      getSTLCInfo                 (STLCInfoRequest                    request);

        [OperationContract]
        STLCStatusResult                    getSTLCStatus               (STLCStatusRequest                  request);

        [OperationContract]
        STLCModeResult                      setSTLCMode                 (STLCModeRequest                    request);

        [OperationContract]
        STLCServiceStatusResult             setServiceStatus            (STLCServiceStatusRequest           request);

        [OperationContract]
        STLCServiceStatusResult             getServiceStatus            (STLCServiceStatusRequest           request);

        [OperationContract]
        STLCShutdownResult                  STLCShutdown                (STLCShutdownRequest                request);

        [OperationContract]
        STLCApplySystemConfigResult         applySystemConfig           (STLCApplySystemConfigRequest       request);

        [OperationContract]
        STLCNetworkResult                   getSTLCNetwork              (STLCNetworkRequest                 request);

        [OperationContract]
        ReloadConfigResult                  reloadConfig                (ReloadConfigRequest                request);

        [OperationContract]
        ReloadSystemXmlIntoDatabaseResult   reloadSystemXmlIntoDatabase (ReloadSystemXmlIntoDatabaseRequest request);

        [OperationContract]
        UpdateRegionListFromWSResult        updateRegionListFromWS      (UpdateRegionListFromWSRequest      request);

        [OperationContract]
        UpdateDeviceTypeListFromWSResult    updateDeviceTypeListFromWS  (UpdateDeviceTypeListFromWSRequest  request);

        [OperationContract]
        GetNeededSupervisorsResult          getNeededSupervisors        (GetNeededSupervisorsRequest        request);

        [OperationContract]
        SetConfigOnRegistryResult           setConfigOnRegistry         (SetConfigOnRegistryRequest         request);

        [OperationContract]
        EmptyDatabaseResult                 emptyDatabase               (EmptyDatabaseRequest               request);
    }
}
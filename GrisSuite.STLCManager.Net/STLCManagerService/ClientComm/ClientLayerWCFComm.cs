﻿using System;
using System.ServiceModel;

namespace STLCManager.Service.ClientComm
{
    internal class ClientLayerWCFComm
    {
        private ClientCommandService singletonClientCommandService;
        private ServiceHost serviceHost;

        // Comunicazioni che partono dal campo
        public event EventHandler<STLCInfoEventArgs>                        OnGetSTLCInfo;
        public event EventHandler<STLCStatusEventArgs>                      OnGetSTLCStatus;
        public event EventHandler<STLCModeEventArgs>                        OnSetSTLCMode;
        public event EventHandler<STLCServiceStatusEventArgs>               OnSetSTLCServiceStatus;
        public event EventHandler<STLCServiceStatusEventArgs>               OnGetSTLCServiceStatus;
        public event EventHandler<STLCShutdownEventArgs>                    OnSTLCShutdown;
        public event EventHandler<STLCApplySystemConfigEventArgs>           OnSTLCApplySystemConfig;
        public event EventHandler<STLCNetworkEventArgs>                     OnGetSTLCNetwork;
        public event EventHandler<ReloadConfigEventArgs>                    OnReloadConfig;
        public event EventHandler<ReloadSystemXmlIntoDatabaseEventArgs>     OnReloadSystemXmlIntoDatabase;
        public event EventHandler<UpdateRegionListFromWSEventArgs>          OnUpdateRegionListFromWS;
        public event EventHandler<UpdateDeviceTypeListFromWSEventArgs>      OnUpdateDeviceTypeListFromWS;
        public event EventHandler<GetNeededSupervisorsEventArgs>            OnGetNeededSupervisors;
        public event EventHandler<SetConfigOnRegistryEventArgs>             OnSetConfigOnRegistry;
        public event EventHandler<EmptyDatabaseEventArgs>                   OnEmptyDatabase;

        public ClientLayerWCFComm()
        {
            this.singletonClientCommandService      = new ClientCommandService  ();

            this.singletonClientCommandService.OnGetSTLCInfoCommand                     += this.singletonClientCommandService_OnGetSTLCInfoCommand;
            this.singletonClientCommandService.OnGetSTLCStatusCommand                   += this.singletonClientCommandService_OnGetSTLCStatusCommand;
            this.singletonClientCommandService.OnSetSTLCModeCommand                     += this.singletonClientCommandService_OnSetSTLCModeCommand;
            this.singletonClientCommandService.OnSetSTLCServiceStatusCommand            += this.singletonClientCommandService_OnSetSTLCServiceStatusCommand;
            this.singletonClientCommandService.OnGetSTLCServiceStatusCommand            += this.singletonClientCommandService_OnGetSTLCServiceStatusCommand;
            this.singletonClientCommandService.OnSTLCShutdownCommand                    += this.singletonClientCommandService_OnSTLCShutdownCommand;
            this.singletonClientCommandService.OnSTLCApplySystemConfigCommand           += this.singletonClientCommandService_OnSTLCApplySystemConfigCommand;
            this.singletonClientCommandService.OnGetSTLCNetworkCommand                  += this.singletonClientCommandService_OnGetSTLCNetworkCommand;
            this.singletonClientCommandService.OnReloadConfigCommand                    += this.singletonClientCommandService_OnReloadConfigCommand;
            this.singletonClientCommandService.OnReloadSystemXmlIntoDatabaseCommand     += this.singletonClientCommandService_OnReloadSystemXmlIntoDatabaseCommand;
            this.singletonClientCommandService.OnUpdateRegionListFromWSCommand          += this.singletonClientCommandService_OnUpdateRegionListFromWSCommand;
            this.singletonClientCommandService.OnUpdateDeviceTypeListFromWSCommand      += this.singletonClientCommandService_OnUpdateDeviceTypeListFromWSCommand;
            this.singletonClientCommandService.OnGetNeededSupervisorsCommand            += this.singletonClientCommandService_OnGetNeededSupervisorsCommand;
            this.singletonClientCommandService.OnSetConfigOnRegistryCommand             += this.singletonClientCommandService_OnSetConfigOnRegistryCommand;
            this.singletonClientCommandService.OnEmptyDatabaseCommand                   += this.singletonClientCommandService_OnEmptyDatabaseCommand;

            this.serviceHost = new ServiceHost(this.singletonClientCommandService);

            serviceHost.Open();
        }

        public void CloseClientLayerWCFComm()
        {
            serviceHost.Close();
        }

        private void singletonClientCommandService_OnSTLCApplySystemConfigCommand(object sender, STLCApplySystemConfigEventArgs e)
        {
            this.OnSTLCApplySystemConfig(this, e);
        }

        private void singletonClientCommandService_OnSTLCShutdownCommand(object sender, STLCShutdownEventArgs e)
        {
            this.OnSTLCShutdown(this, e);
        }

        private void singletonClientCommandService_OnGetSTLCServiceStatusCommand(object sender, STLCServiceStatusEventArgs e)
        {
            this.OnGetSTLCServiceStatus(this, e);
        }

        private void singletonClientCommandService_OnSetSTLCServiceStatusCommand(object sender, STLCServiceStatusEventArgs e)
        {
            this.OnSetSTLCServiceStatus(this, e);
        }

        private void singletonClientCommandService_OnSetSTLCModeCommand(object sender, STLCModeEventArgs e)
        {
            this.OnSetSTLCMode(this, e);
        }

        private void singletonClientCommandService_OnGetSTLCStatusCommand(object sender, STLCStatusEventArgs e)
        {
            this.OnGetSTLCStatus(this, e);
        }

        private void singletonClientCommandService_OnGetSTLCInfoCommand(object sender, STLCInfoEventArgs e)
        {
            this.OnGetSTLCInfo(this, e);
        }

        private void singletonClientCommandService_OnGetSTLCNetworkCommand(object sender, STLCNetworkEventArgs e)
        {
            this.OnGetSTLCNetwork(this, e);
        }

        private void singletonClientCommandService_OnReloadConfigCommand(object sender, ReloadConfigEventArgs e)
        {
            this.OnReloadConfig(this, e);
        }

        private void singletonClientCommandService_OnReloadSystemXmlIntoDatabaseCommand(object sender, ReloadSystemXmlIntoDatabaseEventArgs e)
        {
            this.OnReloadSystemXmlIntoDatabase(this, e);
        }

        private void singletonClientCommandService_OnUpdateRegionListFromWSCommand(object sender, UpdateRegionListFromWSEventArgs e)
        {
            this.OnUpdateRegionListFromWS(this, e);
        }

        private void singletonClientCommandService_OnUpdateDeviceTypeListFromWSCommand(object sender, UpdateDeviceTypeListFromWSEventArgs e)
        {
            this.OnUpdateDeviceTypeListFromWS(this, e);
        }

        private void singletonClientCommandService_OnGetNeededSupervisorsCommand(object sender, GetNeededSupervisorsEventArgs e)
        {
            this.OnGetNeededSupervisors(this, e);
        }

        void singletonClientCommandService_OnSetConfigOnRegistryCommand(object sender, SetConfigOnRegistryEventArgs e)
        {
            this.OnSetConfigOnRegistry(this, e);
        }

        private void singletonClientCommandService_OnEmptyDatabaseCommand(object sender, EmptyDatabaseEventArgs e)
        {
            this.OnEmptyDatabase(this, e);
        }
    }
}
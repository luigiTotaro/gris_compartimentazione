using System;
using System.Collections.Generic;
using System.Text;

namespace STLCManager.Service.LoggerSet
{
    public enum TipoLog { LogSuFile = 1, LogSuListBox, LogSuEvents, LogSuTutto, LogSuProtocollo };
    public class CompositeLogger : ILogger
    {
        private ILogger[] m_loggerArray;
        
        public CompositeLogger(params ILogger[] loggers)
        {
            m_loggerArray = loggers;
        }

        public void LogMessage(string strMessage, TipoLog nTipoLog)
        {
            foreach (ILogger logger in m_loggerArray)
                logger.LogMessage(strMessage, nTipoLog);
        }
    }
}

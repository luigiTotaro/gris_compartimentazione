using System;
using System.Diagnostics;

namespace STLCManager.Service.LoggerSet
{
	internal class EventLogger : ILogger
	{
		private string m_sTypeEntry;
		private readonly EventLog STLCManagerEventLog;

		public EventLogger(string p_sType)
		{
			//Costruttore
			this.m_sTypeEntry = p_sType;

			if (!EventLog.SourceExists("GrisSuite"))
			{
				EventLog.CreateEventSource("GrisSuite", "GrisSuite");
			}

			this.STLCManagerEventLog = new EventLog("GrisSuite");
			this.STLCManagerEventLog.Source = this.m_sTypeEntry;
		}

		public void LogMessage(string strMessage, TipoLog nTipoLog)
		{
			if (nTipoLog != TipoLog.LogSuEvents)
			{
				return;
			}
			try
			{
				//EventLog.WriteEntry(m_sTypeEntry, strMessage);
				this.STLCManagerEventLog.WriteEntry(strMessage);
			}
			catch
			{
			}
		}

		public void AddInfoEvent(String strMessage, int idInformation)
		{
			try
			{
				//EventLog.WriteEntry(m_sTypeEntry, strMessage, EventLogEntryType.Information, idInformation);
				this.STLCManagerEventLog.WriteEntry(strMessage, EventLogEntryType.Information, idInformation);
			}
			catch
			{
			}
		}

		public void AddWarningEvent(String strMessage, int idWarning)
		{
			try
			{
				//EventLog.WriteEntry(m_sTypeEntry, strMessage, EventLogEntryType.Warning, idWarning);
				this.STLCManagerEventLog.WriteEntry(strMessage, EventLogEntryType.Warning, idWarning);
			}
			catch
			{
			}
		}

		public void AddErrorEvent(String strMessage, int idError)
		{
			try
			{
				//EventLog.WriteEntry(m_sTypeEntry, strMessage, EventLogEntryType.Error, idError);
				this.STLCManagerEventLog.WriteEntry(strMessage, EventLogEntryType.Error, idError);
			}
			catch
			{
			}
		}
	}
}
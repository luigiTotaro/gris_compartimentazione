using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace STLCManager.Service.LoggerSet
{
    internal class FileLogger : ILogger
    {
        private const long LOG_MAX_LEN = 1*1024*1024;
        private string m_strFileNameWhitoutLog;
        private string m_strFileName;
        private string m_strFileNameOld;
        private object m_sync = new object();

        private int m_CurrDay;

        public FileLogger(string strFileName)
        {
            this.m_strFileNameWhitoutLog = strFileName;
            this.m_strFileName = this.m_strFileNameWhitoutLog + ".log";
            this.m_strFileNameOld = this.m_strFileNameWhitoutLog + "_old.log";
            this.m_CurrDay = DateTime.Now.Day;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //	FUNZIONE			:   LogMessage    								        //
        //	DESCRIZIONE			:   Scrittura del log su file                    		//
        //	CHIAMATA			:   LogMessage(strMessage, nTipoLog)    				//
        //	PARAMETRI FORMALI															//
        //	input				:   													//
        //	output				:														//
        //	CODICI DI RITORNO	:														//
        //////////////////////////////////////////////////////////////////////////////////
        public void LogMessage(string strMessage, TipoLog nTipoLog)
        {
            if (nTipoLog != TipoLog.LogSuFile)
            {
                return;
            }

            lock (this.m_sync)
            {
                try
                {
                    using (TextWriter fileWriter = new StreamWriter(this.m_strFileName, true, Encoding.GetEncoding("iso-8859-1")))
                    {
                        fileWriter.WriteLine("{0:G} -- {1}", DateTime.Now, strMessage);

                        fileWriter.Flush();
                    }
                }
                catch
                {
                }
                ///////////////////////////////////////////////////////////////////////////
                //VERIFICO SE DEVO EFFETTUARE LA COPIA SUL FILE OLD DEL LOG
                ///////////////////////////////////////////////////////////////////////////
                try
                {
                    FileInfo fInfo = new FileInfo(this.m_strFileName);
                    if (fInfo.Length > LOG_MAX_LEN)
                    {
                        File.Delete(this.m_strFileNameOld);
                        File.Move(this.m_strFileName, this.m_strFileNameOld);
                    }
#if (COPY_FILE_AT_MIDNIGHT)
                    int newDay = DateTime.Now.Day;
                    if (m_CurrDay != newDay)
                    {
                        m_strFileNameOld = GetDayStrFileName(m_strFileNameWhitoutLog, m_CurrDay);
                        File.Delete(m_strFileNameOld);
                        File.Move(m_strFileName, m_strFileNameOld);
                        m_CurrDay = newDay;
                    }
#endif
                }
                catch
                {
                }
            }
        }
    }
}
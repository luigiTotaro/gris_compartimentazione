using System;
using System.Collections.Generic;
using System.Text;

namespace STLCManager.Service.LoggerSet
{
    public interface ILogger
    {
        void LogMessage(string strMessage, TipoLog nTipoLog);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace STLCManager.Service.AppCfgUtility
{
    public class ElementCollection : ConfigurationElementCollection 
    {
        /// <summary>
        /// Costruttore
        /// </summary>
        public ElementCollection()
        {
            this.AddElementName = "Setting"; 
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as Element).Name;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Element();
        }

        public new Element this[string name]
        {
            get { return base.BaseGet(name) as Element; }
        }

        public Element this[int ind]
        {
            get { return base.BaseGet(ind) as Element; }
        }
    }
}

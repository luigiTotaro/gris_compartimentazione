
using System;
using Microsoft.Win32;
using System.Collections.Generic;      

namespace STLCManager.Service.AppCfgUtility
{
    public enum Reg_Type
    {
        HKey_ClassesRoot,
        HKey_CurrentConfig,
        HKey_CurrentUser,
        HKey_LocalMachine,
        HKey_Users
    }

	/// <summary>
	/// An useful class to read/write/delete/count registry keys
	/// </summary>
	public class ModifyRegistry
	{
        private const string HKEY_CLASSES_ROOT      = "HKEY_CLASSES_ROOT";
        private const string HKEY_CURRENT_USER      = "HKEY_CURRENT_USER";
        private const string HKEY_LOCAL_MACHINE     = "HKEY_LOCAL_MACHINE";
        private const string HKEY_USERS             = "HKEY_USERS";
        private const string HKEY_CURRENT_CONFIG    = "HKEY_CURRENT_CONFIG";

        private bool        _validSubKey            = false;

        private Reg_Type    _regType                = Reg_Type.HKey_LocalMachine;

        private RegistryKey _baseRegistryKey        = Registry.LocalMachine;

		private string      _subKey                 = ""; // "SOFTWARE\\" + Application.ProductName.ToUpper();

        /// <summary>
        /// vale true se la sotto-chiave � presente nel registro
        /// </summary>
        public bool ValidSubKey
        {
            get { return _validSubKey; }
        }

		/// <summary>
		/// A property to set the SubKey value
		/// (default = "SOFTWARE\\" + Application.ProductName.ToUpper())
		/// </summary>
		public string SubKey
		{
            get { return _subKey; }
		}

		/// <summary>
		/// A property to set the BaseRegistryKey value.
		/// (default = Registry.LocalMachine)
		/// </summary>
		public RegistryKey BaseRegistryKey
		{
            get { return _baseRegistryKey; }
		}

        /// <summary>
        /// Ritorna il valore della radice del registro secondo un 
        /// enumaratico
        /// </summary>
        public Reg_Type RegType
        {
            get { return _regType; }
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="registryKey">Path completo della chiave del registro all'interno della quale si
        /// desidera leggere/scrivere/modificare i valori</param>
        public ModifyRegistry(string registryKey)
        {
            ParseRegKey(registryKey);
        }

        /// <summary>
        /// Verifica se esiste la sottochiave
        /// </summary>
        /// <returns></returns>
        public bool ExistSubKey()
        {
            try 
            {
                RegistryKey rk              = _baseRegistryKey;           // Opening the registry key
                RegistryKey sk1             = rk.OpenSubKey(_subKey);     // Open a subKey as read-only
            
                // If the RegistrySubKey doesn't exist -> (null)
                if (sk1 != null)
                {
                    return true;
                }
            }
            catch { }

            Logger.Default.Log(String.Format("La sotto-chiave \"{0}\" NON � stata trovata nel registro {1}", _subKey, _regType), Logger.LogType.CONSOLE_LOC_FILE);
            return false;
        }

        /// <summary>
        /// Verifica se esiste un valore all'interno della sottochiave
        /// </summary>
        /// <param name="sValueName">valore da cercare</param>
        /// <returns></returns>
        public bool ExistValue(string sValueName)
        {
            try
            {
                RegistryKey rk  = _baseRegistryKey;           // Opening the registry key
                RegistryKey sk1 = rk.OpenSubKey(_subKey);

                // If the RegistryKey exists...
                if (sk1 != null)
                {
                    string[] values = sk1.GetValueNames();

                    foreach(string value in values)
                    {
                        if(string.Compare(value, sValueName, true) == 0)
                        {
                            return true;
                        }
                    }
                }
            }
            catch { }


            return false;
        }

		/// <summary>
		/// To read a registry key.
		/// input: KeyName (string)
		/// output: value (string) 
		/// </summary>
		public string Read(string KeyName)
		{
			// Opening the registry key
			RegistryKey rk = _baseRegistryKey ;
			// Open a subKey as read-only
			RegistryKey sk1 = rk.OpenSubKey(_subKey);
			// If the RegistrySubKey doesn't exist -> (null)
			if ( sk1 == null )
			{
				return null;
			}
			else
			{
				try 
				{
					// If the RegistryKey exists I get its value
					// or null is returned.
					return (string)sk1.GetValue(KeyName);
				}
				catch (Exception e)
				{
                    Logger.Default.Log(String.Format("Errore lettura del valore dal registro {0} [ {1} ]", KeyName.ToUpper(), e.Message), Logger.LogType.CONSOLE_LOC_FILE);
					return null;
				}
			}
		}	

		/// <summary>
		/// To write into a registry key.
		/// input: KeyName (string) , Value (object)
		/// output: true or false 
		/// </summary>
		public bool Write(string KeyName, object Value)
		{
			try
			{
				// Setting
				RegistryKey rk = _baseRegistryKey ;
				// I have to use CreateSubKey 
				// (create or open it if already exits), 
				// 'cause OpenSubKey open a subKey as read-only
				RegistryKey sk1 = rk.CreateSubKey(_subKey);
				// Save the value
				sk1.SetValue(KeyName, Value);

				return true;
			}
			catch (Exception e)
			{
                Logger.Default.Log(String.Format("Errore scrittura del valore {0} sul registro {1} [ {2} ]", Value, KeyName.ToUpper(), e.Message), Logger.LogType.CONSOLE_LOC_FILE);
				return false;
			}
		}

		/// <summary>
		/// To delete a registry key.
		/// input: KeyName (string)
		/// output: true or false 
		/// </summary>
		public bool DeleteKey(string KeyName)
		{
			try
			{
				// Setting
				RegistryKey rk = _baseRegistryKey;
				RegistryKey sk1 = rk.CreateSubKey(_subKey);
				// If the RegistrySubKey doesn't exists -> (true)
				if ( sk1 == null )
					return true;
				else
					sk1.DeleteValue(KeyName);

				return true;
			}
			catch (Exception e)
			{
                Logger.Default.Log(String.Format("Errore cancellazione della chiave {0} [ {1} ]", KeyName, e.Message), Logger.LogType.CONSOLE_LOC_FILE);
				return false;
			}
		}

		/// <summary>
		/// To delete a sub key and any child.
		/// input: void
		/// output: true or false 
		/// </summary>
		public bool DeleteSubKeyTree()
		{
			try
			{
				// Setting
                RegistryKey rk = _baseRegistryKey;
				RegistryKey sk1 = rk.OpenSubKey(_subKey);
				// If the RegistryKey exists, I delete it
				if ( sk1 != null )
					rk.DeleteSubKeyTree(_subKey);

				return true;
			}
			catch (Exception e)
			{
                Logger.Default.Log(String.Format("Errore cancellazione della chiave {0} [ {1} ]", _subKey, e.Message), Logger.LogType.CONSOLE_LOC_FILE); 
                return false;
			}
		}

		/// <summary>
		/// Retrive the count of subkeys at the current key.
		/// input: void
		/// output: number of subkeys
		/// </summary>
		public int SubKeyCount()
		{
			try
			{
				// Setting
				RegistryKey rk  = _baseRegistryKey;
				RegistryKey sk1 = rk.OpenSubKey(_subKey);
				// If the RegistryKey exists...
				if ( sk1 != null )
					return sk1.SubKeyCount;
				else
					return 0; 
			}
			catch (Exception e)
			{
                Logger.Default.Log(String.Format("Errore nel recupero del numero delle sotto-chiavi di {0} [ {1} ]", _subKey, e.Message), Logger.LogType.CONSOLE_LOC_FILE); 
				return 0;
			}
		}

		/// <summary>
		/// Retrive the count of values in the key.
		/// input: void
		/// output: number of keys
		/// </summary>
		public int ValueCount()
		{
			try
			{
				// Setting
				RegistryKey rk  = _baseRegistryKey ;
				RegistryKey sk1 = rk.OpenSubKey(_subKey);

				// If the RegistryKey exists...
				if ( sk1 != null )
					return sk1.ValueCount;
				else
					return 0; 
			}
			catch (Exception e)
			{
                Logger.Default.Log(String.Format("Errore nel recupero del numero dei valori di {0} [ {1} ]", _subKey, e.Message), Logger.LogType.CONSOLE_LOC_FILE); 
                return 0;
			}
		}

        /// <summary>
        /// Ottiene la lista dei valori presenti nella sotto-chiave specificata
        /// </summary>
        /// <returns>Lista dei nomi dei valori</returns>
        public List<string> Values()
        {
            List<string> list = new List<string>();

            try
            {
                // Setting
                RegistryKey rk  = _baseRegistryKey;
                RegistryKey sk1 = rk.OpenSubKey(_subKey);

                // If the RegistryKey exists...
				if ( sk1 != null )
                {
                    list = new List<string>(sk1.GetValueNames());
                }
            }
            catch (Exception e)
            {
                Logger.Default.Log(String.Format("Errore nel recupero della lista dei valori di {0} [ {1} ]", _subKey, e.Message), Logger.LogType.CONSOLE_LOC_FILE);
            }

            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sName"></param>
        /// <returns></returns>
        public bool DeleteValue(string sName)
        {
            try
            {
                // Setting
                RegistryKey rk  = _baseRegistryKey;
                RegistryKey sk1 = rk.OpenSubKey(_subKey, true);

                // If the RegistryKey exists...
                if (sk1 != null)
                {
                    sk1.DeleteValue(sName, true);
                }
            }
            catch (Exception e)
            {
                Logger.Default.Log(String.Format("Errore nella cancellazione del valore {0} di {1} [ {2} ]", sName, _subKey, e.Message), Logger.LogType.CONSOLE_LOC_FILE);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Estrae dalla stringa passata al costruttore la chiave
        /// e la sotto-chiave all'interno della quale si intende leggere/scrivere 
        /// i valori sul registro
        /// </summary>
        /// <param name="registryKey"></param>
        /// <returns></returns>
        private bool ParseRegKey(string registryKey)
        {
            string      sRoot       = "";
            string      sSubKey     = "";

            string[]    sPath       = registryKey.Split(new Char[] { '\\' });

            sRoot                   = sPath[0];

            for (int j = 1; j < sPath.Length; j++)
            {
                sSubKey += sPath[j];

                if(j < sPath.Length) {

                    sSubKey += "\\";
                }
            }

            if(sRoot.Length != 0 && sSubKey.Length != 0)
            {
                _subKey         = sSubKey;
                _validSubKey    = ExistSubKey();

                switch(sRoot)
                {
                    case HKEY_CLASSES_ROOT:
                        _regType            = Reg_Type.HKey_ClassesRoot;
                        _baseRegistryKey    = Registry.ClassesRoot;
                        break;
                    case HKEY_CURRENT_CONFIG:
                        _regType            = Reg_Type.HKey_CurrentConfig;
                        _baseRegistryKey    = Registry.CurrentConfig;
                        break;
                    case HKEY_CURRENT_USER:
                        _regType            = Reg_Type.HKey_CurrentUser;
                        _baseRegistryKey    = Registry.CurrentUser;
                        break;
                    case HKEY_LOCAL_MACHINE:
                        _regType            = Reg_Type.HKey_LocalMachine;
                        _baseRegistryKey    = Registry.LocalMachine;
                        break;
                    case HKEY_USERS:
                        _regType            = Reg_Type.HKey_Users;
                        _baseRegistryKey    = Registry.Users;
                        break;
                    default:
                        _regType            = Reg_Type.HKey_LocalMachine;
                        _baseRegistryKey    = Registry.LocalMachine;
                        break;
                }

                return true;
            }

            return false;
        }
	}
}

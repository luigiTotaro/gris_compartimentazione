﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace STLCManager.Service.AppCfgUtility
{
    public class Element : ConfigurationElement
    {
        private const string    ATTRIBUTE_NAME                  = "name";
        private const string    ATTRIBUTE_VALUE                 = "value";
        private const string    ATTRIBUTE_TYPE                  = "type";
        private const string    ATTRIBUTE_CREATE_ON_REGISTRY    = "createOnRegistry";

        public Element()
        {
            this[ATTRIBUTE_CREATE_ON_REGISTRY]  = false;
            this[ATTRIBUTE_TYPE]                = TypeCode.String;
        }

        [ConfigurationProperty(ATTRIBUTE_NAME,               IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)this[ATTRIBUTE_NAME];                  }
            set { this[ATTRIBUTE_NAME] = value;                         }
        }

        [ConfigurationProperty(ATTRIBUTE_VALUE,              IsRequired = true, IsKey = false)]
        public string Value
        {
            get { return (string)this[ATTRIBUTE_VALUE];                 }
            set { this[ATTRIBUTE_VALUE] = value;                        }
        }

        [ConfigurationProperty(ATTRIBUTE_TYPE,               IsRequired = false, IsKey = false)]
        public TypeCode Type
        {
            get { return (TypeCode)this[ATTRIBUTE_TYPE];                }
            set { this[ATTRIBUTE_TYPE] = value;                         }
        }

        [ConfigurationProperty(ATTRIBUTE_CREATE_ON_REGISTRY, IsRequired = false, IsKey = false)]
        public bool CreateOnRegistry
        {
            get { return (bool)this[ATTRIBUTE_CREATE_ON_REGISTRY];      }
            set { this[ATTRIBUTE_CREATE_ON_REGISTRY] = value;           }
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace STLCManager.Service.AppCfgUtility
{
    public class Section : ConfigurationSection
    {
        private const string sectionName            = "STLCManagerSettings";
        private const string ATTRIBUTE_REGISTRY_KEY = "registryKey";

        [ConfigurationProperty("GlobalSettings", IsDefaultCollection = true)]
        public GlobalElementCollection GlobalSettings
        {
            get
            {
                return this["GlobalSettings"] as GlobalElementCollection;
            }
        }

        [ConfigurationProperty("LocalSettings", IsDefaultCollection = true)]
        public LocalElementCollection LocalSettings
        {
            get
            {
                return this["LocalSettings"] as LocalElementCollection;
            }
        }

        [ConfigurationProperty(ATTRIBUTE_REGISTRY_KEY, IsRequired = false, IsKey = false)]
        public string RegistryKey
        {
            get { return (string)this[ATTRIBUTE_REGISTRY_KEY]; }
            set { this[ATTRIBUTE_REGISTRY_KEY] = value; }
        }

        public Element GetLocalSettingElement(string sName)
        {
            try
            {
                IEnumerator e = LocalSettings.GetEnumerator();

                while (e.MoveNext())
                {
                    Element elementCurrent = (Element)e.Current;

                    if (string.Compare(sName, elementCurrent.Name, true) == 0)
                    {
                        return elementCurrent;
                    }
                }
            }
            catch (Exception e) { throw e; }

            return null;
        }

        public Element GetGlobalSettingElement(string sName)
        {
            try
            { 
                IEnumerator e = GlobalSettings.GetEnumerator();

                while (e.MoveNext())
                {
                    Element elementCurrent = (Element)e.Current;

                    if (string.Compare(sName, elementCurrent.Name, true) == 0)
                    {
                        return elementCurrent;
                    }
                }
            }
            catch (Exception e) { throw e; }

            return null;
        }

        public Element GetElement(string sName)
        {
            Element element = null;

            try
            {
                if ((element = GetLocalSettingElement(sName)) != null)
                {
                    return element;
                }

                if ((element = GetGlobalSettingElement(sName)) != null)
                {
                    return element;
                }
            }
            catch (Exception e) { throw e; }

            return element;
        }

        public static Section GetSection()
        {
            return (Section)ConfigurationManager.GetSection(sectionName);
        }

        public static void RefreshSection()
        {
            ConfigurationManager.RefreshSection(sectionName);
        }
    }
}

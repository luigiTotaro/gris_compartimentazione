﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using STLCManager.Service.Types;
using STLCManager.Service.Wrapper;
using Timer = System.Timers.Timer;

namespace STLCManager.Service.Function
{
	/// <summary>
	///     Classe di monitoraggio attività di sistema e gestione watchdog
	/// </summary>
	internal static class SystemWDog
	{
		#region Token

		private const int WATCH_DOG_TIME_SCHEDULING = 5*1000; // timeout di schedulazione attività
		private const int REBOOT_AFTER_STARTUP_TIME = 5*60*1000; // tempo attesa dallo startup prima di eseguire un reboot (evita condizione di loop)

		#endregion

		#region Members

		public static bool SystemFailure; // identifica l'anomalia generale del sistema

		private static bool isSwWatchDogEnabled; // watchdog software abilitato: monitoraggio attività abilitato
		private static bool isHwWatchDogEnabled; // watchdog hardware abilitato
		private static uint onServiceRunningTimeout; // tempo di intervento del watchdog hardware con servizio attivo
		private static uint onServiceStopTimeout; // tempo di intervento del watchdog hardware con servizio disattivo

		private static bool isMaintenanceEnabled; // modalità di manutenzione attiva
		private static bool isMaintenancePermanent; // modalità di manutenzione permanente (non sottoposta a timeout)
		private static Int32 maintenanceTimeout; // timeout gestione modalità di manutenzione

		private static List<ThreadCTRL> threadsUnderControl; // lista threads di cui verificare il corretto funzionamento
		private static bool threadsFailure; // uno o più thread sotto controllo non rispondono

		private static Timer timerWatchDog; // timer gestione schedulazione attività di monitoraggio.

		internal static bool RebootOnFailure; // comanda il reboot del sistema in caso di errore
		internal static bool IsRebooting; // sistema in fase di riavvio
		private static ResetSTLC ResetObject; // oggetto di gestione reboot
		private static Thread ResetThread; // thread di gestione reboot 
		private static DateTime RebootAfterTime; // reboot consentito dopo l'orario indicato (evita condizione di loop)

		#endregion

		#region Constructors

		/// <summary>
		///     Construttore classe monitoraggio sistema gestione watchdog
		/// </summary>
		static SystemWDog()
		{
			Initialize();
		}

		/// <summary>
		///     Inizializza classe monitoraggio sistema gestione watchdog
		/// </summary>
		public static void Initialize()
		{
			SystemFailure = false;

			isSwWatchDogEnabled = false;
			isHwWatchDogEnabled = false;
			OnServiceRunningTimeout = 4*60;
			OnServiceStopTimeout = 60*60;

			isMaintenanceEnabled = false;
			isMaintenancePermanent = false;

			RebootOnFailure = false;
			IsRebooting = false;
			ResetObject = null;
			ResetThread = null;
			RebootAfterTime = DateTime.Now.AddMilliseconds(REBOOT_AFTER_STARTUP_TIME);

			threadsFailure = false;
			threadsUnderControl = null;
		}

		#endregion

		#region Accessor

		/// <summary>
		///     Restituisce / imposta timeout di intervento watchdog hardware con servizio STLCManager attivo.
		///     Valore espresso in secondi.
		/// </summary>
		public static uint OnServiceRunningTimeout
		{
			get { return onServiceRunningTimeout/1000; }
			set { onServiceRunningTimeout = value*1000; }
		}

		/// <summary>
		///     Restituisce / imposta timeout di intervento watchdog hardware con servizio STLCManager disattivo.
		///     Il watchdog è impostato al presente valore nel momento in cui riceve il comando di stop.
		///     Valore espresso in secondi.
		/// </summary>
		public static uint OnServiceStopTimeout
		{
			get { return onServiceStopTimeout/(60*60*1000); }
			set { onServiceStopTimeout = value*(60*60*1000); }
		}

		/// <summary>
		///     Restituisce lo stato di manutenzione.
		/// </summary>
		public static bool IsMaintenanceEnabled
		{
			get { return isMaintenanceEnabled; }
		}

		/// <summary>
		///     Restituisce lo stato complessivo dei thread posti sotto controllo
		/// </summary>
		public static bool IsThreadsFailure
		{
			get { return threadsFailure; }
		}

		#endregion

		#region Methods

		/// <summary>
		///     Imposta timeout di intervento del watchdog
		/// </summary>
		/// <param name="sOnRunningTimeout">timeout watchdog con servizio attivo</param>
		/// <param name="sOnStopTimeout">timeout watchdog con servizio disattivo</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: timeout impostati correttamente</item>
		///         <item>false: timeout non validi</item>
		///     </list>
		/// </returns>
		public static bool SetTimeout(string sOnRunningTimeout, string sOnStopTimeout)
		{
			try
			{
				if (!String.IsNullOrEmpty(sOnRunningTimeout))
				{
					uint uOnRunning = uint.Parse(sOnRunningTimeout);
					OnServiceRunningTimeout = uOnRunning;
				}

				if (!String.IsNullOrEmpty(sOnStopTimeout))
				{
					uint uOnStop = uint.Parse(sOnStopTimeout);
					OnServiceStopTimeout = uOnStop;
				}
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		///     Abilita monitoraggio sistema ed opzionalmente watchdog hardware
		/// </summary>
		/// <param name="hwEnabled">abilita watchdog hardware</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: abilitazione eseguita correttamente</item>
		///         <item>false: abilitazione fallita. Attivata procedura di reboot</item>
		///     </list>
		/// </returns>
		public static bool Enable(bool hwEnabled, bool rebootOnFailure)
		{
			try
			{
				if (!isSwWatchDogEnabled)
				{
					// creazione lista per elencare i thread da controllare
					threadsUnderControl = new List<ThreadCTRL>();

					// thread gestione watchdog
					timerWatchDog = new Timer(WATCH_DOG_TIME_SCHEDULING);
					timerWatchDog.Elapsed += ManageWDOG;
					timerWatchDog.AutoReset = true;
					timerWatchDog.Enabled = true;
					timerWatchDog.Start();

					isSwWatchDogEnabled = true;
				}

				RebootOnFailure = rebootOnFailure;

				if (STLCConfig.IsSTLCHost && hwEnabled)
				{
					if (JidaPlatform.Default.SetWatchDog(onServiceRunningTimeout) == AppCfg.COD_NO_ERROR)
					{
						isHwWatchDogEnabled = true;
					}
					else
					{
						SystemCrash();
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemWDog.Enable': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				SystemCrash();
			}

			if (!isSwWatchDogEnabled || (STLCConfig.IsSTLCHost && hwEnabled && !isHwWatchDogEnabled))
			{
				return false;
			}

			return true;
		}

		/// <summary>
		///     Imposta lo stato del timer di monitoraggio e gestione watchdog
		/// </summary>
		public static void SetWatchDogTimer(bool status)
		{
			if (status)
			{
				timerWatchDog.Start();
				timerWatchDog.Enabled = true;
				isSwWatchDogEnabled = true;
			}
			else
			{
				timerWatchDog.Stop();
				timerWatchDog.Enabled = false;
				isSwWatchDogEnabled = false;
			}
		}

		/// <summary>
		///     Inserisce processo nella schedulazione di verifica corretto funzionamento del processo stesso
		/// </summary>
		/// <param name="name">nome processo</param>
		/// <param name="period">periodicità di attivazione processo da verificare</param>
		/// <param name="running">oggetto indicante attività da parte del processo</param>
		public static void ThreadUnderControl(string name, UInt32 period, ThreadCTRL.CheckRunning running)
		{
			try
			{
				int count = (int) (period/WATCH_DOG_TIME_SCHEDULING + 2);

				ThreadCTRL thread = new ThreadCTRL(name, count, running);

				if (threadsUnderControl != null)
				{
					threadsUnderControl.Add(thread);
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemWDog.ThreadUnderControl': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				SystemCrash();
			}
		}

		/// <summary>
		///     Elimina il thread indicato dalla lista dei threads da controllare
		/// </summary>
		/// <param name="name"></param>
		public static void ThreadDeleteControl(string name)
		{
			try
			{
				if (threadsUnderControl != null && threadsUnderControl.Count > 0)
				{
					ThreadCTRL thread = threadsUnderControl.Find(delegate(ThreadCTRL th) { return th.Name == name; });

					if (thread != null)
					{
						threadsUnderControl.Remove(thread);
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemWDog.ThreadDeleteControl': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}

		/// <summary>
		///     Imposta WatchDog per la gestione del servizio in stop
		/// </summary>
		/// <returns>eventuale codice di errore</returns>
		public static int StopService(bool showLog)
		{
			int retCode = AppCfg.COD_NO_ERROR;

			if (STLCConfig.IsSTLCHost && isHwWatchDogEnabled)
			{
				if ((retCode = JidaPlatform.Default.SetWatchDog(onServiceStopTimeout)) != AppCfg.COD_NO_ERROR)
				{
					SystemCrash();
				}
				else
				{
					if (showLog)
					{
						Logger.Default.Log(String.Format("Configurato WatchDog con timeout di {0} ore", OnServiceStopTimeout), Logger.LogType.CONSOLE_LOC_FILE,
							Logger.EventType.WARNING, AppCfg.W_COD_WATCHDOG_ACTIVACTION_SUCCESS);
					}
				}
			}
			return retCode;
		}

		/// <summary>
		///     Comanda modalità operativa normale.
		/// </summary>
		public static void NormalMode()
		{
			isMaintenanceEnabled = false;
			isMaintenancePermanent = false;
		}

		/// <summary>
		///     Comanda modalità operativa di manutenzione. Interrompe un'eventuale azione di reboot pendente.
		/// </summary>
		/// <param name="permanent">manutenzione in modalità permanente</param>
		/// <param name="timeout">timeout di gestione stato di manutenzione, alla scadenza ritorna automaticamente in normal</param>
		public static void MaintenanceMode(bool permanent, uint timeout)
		{
			isMaintenancePermanent = permanent;
			maintenanceTimeout = (Int32) (timeout*60*60*1000);
			isMaintenanceEnabled = true;

			if (ResetObject != null && ResetThread != null)
			{
                ResetObject.RequestStop();
				ResetThread.Join();

				ResetObject = null;
				ResetThread = null;
			}
		}

		/// <summary>
		///     Schedulatore attività di monitoraggio e gestione watchdog
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void ManageWDOG(object sender, ElapsedEventArgs e)
		{
			timerWatchDog.Enabled = false;

			try
			{
				bool bFailure = false;

				foreach (ThreadCTRL thread in threadsUnderControl)
				{
					if (thread.IsRunning())
					{
						bFailure = true;
					}
				}
				threadsFailure = bFailure;

				if (isMaintenanceEnabled && !isMaintenancePermanent)
				{
					maintenanceTimeout -= WATCH_DOG_TIME_SCHEDULING;

					if (maintenanceTimeout < 0)
					{
						Program.engine.engineMode.SetNormalMode();

                        Logger.Default.Log("Tempo scaduto, funzionamento NORMALE.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, //notice
							AppCfg.I_COD_NORMAL_MODE_PER_TIMEOUT);
					}
				}

				if (IsThreadsFailure || SystemFailure)
				{
					if (RebootOnFailure)
					{
						SystemReboot();
					}
				}
				else if (isHwWatchDogEnabled && !IsRebooting)
				{
					JidaPlatform.Default.TriggerWatchDog();
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemWDog.ManagerWatchDog': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				SystemCrash();
			}

			timerWatchDog.Enabled = true;
		}

		/// <summary>
		///     Crash di sistema. Registra evento e comanda reboot.
		/// </summary>
		private static void SystemCrash()
		{
			Logger.Default.Log("Crash processo di gestione watchdog.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
				AppCfg.E_COD_SERVICEMANAGER_CRASH);
			SystemReboot();
		}

		/// <summary>
		///     Comanda reset del sistema. Reboot solo se il sistema è in NORMAL mode, ignorato in MANTENANCE mode.
		/// </summary>
		private static void SystemReboot()
		{
			try
			{
				if (!isMaintenanceEnabled && ResetThread == null)
				{
					int delay = (int) RebootAfterTime.Subtract(DateTime.Now).TotalMilliseconds;

					if (delay < 0)
					{
						delay = 0;
					}

					ResetObject = new ResetSTLC(delay, ResetSTLC.Mode.REBOOT);
					ResetThread = new Thread(ResetObject.DoReset);
					ResetThread.Start();
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemWDog.SystemReset': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}

		/// <summary>
		///     Comanda SHUTDOWN del sistema. Se delay uguale a zero il metodo è sospensivo.
		///     <param name="mode">modalità di reset: SHUTDOWN o REBOOT</param>
		///     <param name="delay">ritardo reset in msec</param>
		/// </summary>
		public static void SystemReset(ResetSTLC.Mode mode, int delay)
		{
			try
			{
				if (ResetObject != null && ResetThread != null)
				{
					ResetObject.RequestStop();
					ResetThread.Join();
				}

				if (delay < 0)
				{
					delay = 0;
				}

				ResetObject = new ResetSTLC(delay, mode);
				ResetThread = new Thread(ResetObject.DoReset);
				ResetThread.Start();

				if (delay == 0)
				{
					while (!ResetThread.IsAlive)
					{
					}
					ResetThread.Join();
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemWDog.SystemReset': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}

		#endregion
	}

	/// <summary>
	///     Classe di gestione reboot di sistema.
	/// </summary>
	public class ResetSTLC
	{
		#region Members

		public enum Mode
		{
			REBOOT,
			SHUTDOWN
		};

		private volatile bool breakReset; // flag di gestione interruzione procedura di reset
		private readonly int delayTime; // attesa prima di procedere al reboot
		private readonly Mode modeReset; // modalità di reset

		#endregion

		#region Constructors

		/// <summary>
		///     Inizializza una nuova istanza della classe RebootSTLC.
		/// </summary>
		/// <param name="delay">attesa prima di procedere al reboot (msec)</param>
		public ResetSTLC(int delay, Mode mode)
		{
			this.breakReset = false;
			this.delayTime = delay;
			this.modeReset = mode;
		}

		/// <summary>
		///     Inizializza una nuova istanza della classe RebootSTLC. Reboot immediato.
		/// </summary>
		public ResetSTLC() : this(0, Mode.REBOOT)
		{
		}

		#endregion

		#region Methods

		/// <summary>
		///     Stoppa servizi ed esegue reboot dopo tempo programmato.
		/// </summary>
		public void DoReset()
		{
			try
			{
				if (this.modeReset == Mode.SHUTDOWN)
				{
                    Logger.Default.Log("Attivata procedura di reset. Shutdown previsto tra circa " + this.delayTime / 1000 + " sec.", Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.WARNING, AppCfg.I_COD_SERVICE_MANAGER_POWEROFF);
				}
				else
				{
                    Logger.Default.Log("Attivata procedura di reset. Reboot previsto tra circa " + this.delayTime / 1000 + " sec.", Logger.LogType.CONSOLE_LOC_FILE, 
						Logger.EventType.WARNING, AppCfg.I_COD_SERVICE_MANAGER_REBOOT);
				}

				// Attende prima di procedere al reboot (necessario per evitare condizione di loop)
				for (int delay = this.delayTime; delay > 0; delay -= 1000)
				{
					if (this.breakReset)
					{
						return;
					}

					Thread.Sleep(1000);
				}

				// Segnala procedura di reboot in corso.
				SystemWDog.IsRebooting = true;
				Thread.Sleep(1000);

				// Disattiva threads
				SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemSERVICES);
				Program.engine.systemServices.Disable();
				Program.engine.systemServices.Stop();

				if (STLCConfig.IsSTLCHost)
				{
					SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemLED);
					SystemLed.Disable();

					SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemFANS);
					SystemFan.Disable();

					SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemFBWF);
					Program.engine.fbwfCurrentSession.Disable();
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemWDog.DoReset': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}

			// Comanda reboot. Il comando è eseguito anche se precedentemente ci sono stati errori
			SystemWDog.IsRebooting = true;
			if (this.modeReset == Mode.SHUTDOWN)
			{
#if (DEBUG)
                Logger.Default.Log("**********", Logger.LogType.CONSOLE);
                Logger.Default.Log("* REBOOT *", Logger.LogType.CONSOLE);
                Logger.Default.Log("**********", Logger.LogType.CONSOLE);
                Thread.Sleep(10 * 1000);
                Environment.Exit(0);
#else
				WMIFunction.Default.PowerOffSTLC();
				while (true)
				{
					Thread.Sleep(1000);
				}
#endif
			}
#if (DEBUG)
                Logger.Default.Log("************", Logger.LogType.CONSOLE);
                Logger.Default.Log("* SHUTDOWN *", Logger.LogType.CONSOLE);
                Logger.Default.Log("************", Logger.LogType.CONSOLE);
                Thread.Sleep(10 * 1000);
                Environment.Exit(0);
#else
			WMIFunction.Default.RebootSTLC();
			while (true)
			{
				Thread.Sleep(1000);
			}
#endif
		}

		/// <summary>
		///     Comanda interruzione procedura di reboot.
		/// </summary>
		public void RequestStop()
		{
			this.breakReset = true;
		}

		#endregion
	}

	/// <summary>
	///     Classe di gestione verifica funzionamento thread.
	/// </summary>
	public class ThreadCTRL
	{
		#region Token

		public delegate bool CheckRunning();

		#endregion

		#region Members

		public bool IsFailure { get; private set; } // flag di segnalazione thread in errore
		public string Name { get; private set; } // nome thread
		private readonly int LoadValue; // valore di caricamento contatore per schedulazione controllo stato thread
		private int Counter; // contatore di schedulazione

		public CheckRunning isRunning; // oggetto di verifica stati thread

		#endregion

		#region Constructors

		/// <summary>
		///     Inizializza una nuova istanza per la verifica del funzionamento di un thread
		/// </summary>
		/// <param name="name">nome thread</param>
		/// <param name="load">numero di iterazioni prima di procedere al controllo</param>
		/// <param name="running">istanza di verifica funzionamento thread</param>
		public ThreadCTRL(string name, int load, CheckRunning running)
		{
			this.IsFailure = false;
			this.Name = name;
			this.LoadValue = load;
			this.Counter = this.LoadValue;
			this.isRunning = running;
		}

		#endregion

		#region Methods

		/// <summary>
		///     Verifica se il thread è operativo.
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: thread in funzione.</item>
		///         <item>false: errore, mancata schedulazione del thread sottoposto a controllo.</item>
		///     </list>
		/// </returns>
		public bool IsRunning()
		{
			if (this.Counter-- <= 0)
			{
				this.Counter = this.LoadValue;

				if (this.isRunning())
				{
					this.IsFailure = false;
				}
				else if (!this.IsFailure)
				{
					this.IsFailure = true;
					Logger.Default.Log(String.Format("ERRORE: Processo '{0}' non risponde.", this.Name), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
						Logger.E_STACK_TRACE);
				}
			}
			return this.IsFailure;
		}

		#endregion
	}
}
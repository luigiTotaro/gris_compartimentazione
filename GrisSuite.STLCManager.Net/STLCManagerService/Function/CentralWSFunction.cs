﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using STLCManager.Service.CentralWSService;
using STLCManager.Service.Database;
using STLCManager.Service.Types;

namespace STLCManager.Service.Function
{
    public sealed class CentralWSFunction
    {
        private static volatile CentralWSFunction instance;
        private static object syncRoot = new Object();

        private SqlConnection dbConnection;
        private readonly string configurationSystemFile;
        private bool canProceedAfterInitialization;

        private const byte NOT_REMOVED = 0;
        //private const byte REMOVED = 1;

        public GrisRegion[] Regions { get; private set; }
        public GrisZone[] Zones { get; private set; }
        public GrisNode[] Nodes { get; private set; }

        public GrisDeviceType[] DeviceTypes { get; private set; }
        public GrisSystem[] Systems { get; private set; }
        public GrisVendor[] Vendors { get; private set; }

        private CentralWSFunction()
        {
            this.configurationSystemFile = AppCfg.Default.sXMLSystem; // .FP_CFG_XML_SYSTEM_VALUE_PATH;

            this.InitializeStart();
        }

        public static CentralWSFunction Default
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new CentralWSFunction();
                        }
                    }
                }

                return instance;
            }
        }

        #region Inizializzazione, verifica configurazione e caricamento lista periferiche da database

        private void InitializeStart()
        {
            ConnectionStringSettings localDatabaseConnectionString = null;

            if (string.IsNullOrEmpty(this.configurationSystemFile))
            {
                Logger.Default.Log("Nome del file di configurazione con la lista dei dispositivi da monitorare non definito",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_CONFIG_XML);
                return;
            }

            if (!FileUtility.CheckFileCanRead(this.configurationSystemFile))
            {
                Logger.Default.Log(
                    string.Format(CultureInfo.InvariantCulture, "Impossibile leggere il file di configurazione con la lista dei dispositivi: {0}",
                        this.configurationSystemFile), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    AppCfg.E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_CONFIG_XML);

                return;
            }

            try
            {
                localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];
            }
            catch (ConfigurationErrorsException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.InitializeStart' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            if (localDatabaseConnectionString == null)
            {
                Logger.Default.Log("ECCEZIONE in 'CentralWSFunction.InitializeStart' - La stringa di connessione al database non è valida",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                return;
            }

            try
            {
                this.dbConnection = new SqlConnection(localDatabaseConnectionString.ConnectionString);
            }
            catch (ArgumentException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.InitializeStart' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                return;
            }

            if (this.dbConnection != null)
            {
                this.canProceedAfterInitialization = SqlDataBaseUtility.GetExistingCentralWSData(this.dbConnection.ConnectionString);

                this.Regions = new GrisRegion[] {};
                this.Zones = new GrisZone[] {};
                this.Nodes = new GrisNode[] {};
                this.DeviceTypes = new GrisDeviceType[] {};
                this.Systems = new GrisSystem[] {};
                this.Vendors = new GrisVendor[] {};
            }
            else
            {
                Logger.Default.Log("ECCEZIONE in 'CentralWSFunction.InitializeStart' - Impossibile accedere al database configurato.",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_DATABASE);
            }
        }

        #endregion

        /// <summary>
        ///     Scarica dati da CentralWS e scrive in base dati Compartimenti, Linee e Stazioni
        /// </summary>
        /// <returns>False se ci sono stati problemi, True altrimenti</returns>
        public bool DownloadRegionListDataAndUpdateDatabase()
        {
            if (!this.canProceedAfterInitialization) return false;
            if (STLCConfig.IsSTLCHost) return false;

            try
            {
               

                using (CentralSoapClient cwsc = new CentralSoapClient())
                {
                    string regionsIds;
                    string regionsChecksum = this.GetChecksumAndIds(cwsc.GetAllRegionsChecksum(), out regionsIds);

                    if (regionsChecksum.Equals(this.GetAllRegionsChecksum(regionsIds), StringComparison.Ordinal))
                    {
                        Logger.Default.Log(
                            "La lista dei compartimenti sul database locale è allineata con quella in centrale, nessuna operazione richiesta.", //notice
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, Logger.I_GENERIC);

                        this.Regions = new GrisRegion[] {};
                    }
                    else
                    {
                        List<GrisRegion> regions = new List<GrisRegion>();

                        #region Deserializzazione manuale da oggetto stringa compresso restituito da WS

                        string serializedData = CompressedBinaryConverter.ToOriginalString(cwsc.GetAllRegions());
                        string[] regionDataList = serializedData.Split('\n');

                        foreach (string regionItem in regionDataList)
                        {
                            if (!String.IsNullOrEmpty(regionItem))
                            {
                                string[] regionData = regionItem.Split('§');

                                if (regionData.Length == 2)
                                {
                                    regions.Add(new GrisRegion {RegID = long.Parse(regionData[0]), Name = regionData[1]});
                                }
                            }
                        }

                        #endregion

                        this.Regions = regions.ToArray();

                        int updatedInsertedCounter = 0;
                        int updatedInsertedErrorCounter = 0;

                        foreach (GrisRegion region in regions)
                        {
                            if (SqlDataBaseUtility.InsertRegion(this.dbConnection.ConnectionString, region.RegID, region.Name, NOT_REMOVED) > 0)
                            {
                                updatedInsertedCounter++;
                            }
                            else
                            {
                                updatedInsertedErrorCounter++;
                            }
                        }

                        if (updatedInsertedErrorCounter != 0)
                        {
                            Logger.Default.Log(                                                                                             //se ci sono errori deve diventare warning //notice
                                String.Format("Aggiornati / inseriti correttamente {0} compartimenti. {1} errori durante il salvataggio.",
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.WARNING, Logger.I_GENERIC);
                        }
                        else
                        {
                            Logger.Default.Log(                                                                                             //se ci sono errori deve diventare warning //notice
                                String.Format("Aggiornati / inseriti correttamente {0} compartimenti. {1} errori durante il salvataggio.",
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.NOTICE, Logger.I_GENERIC);
                        }
                    }

                    string zonesIds;
                    string zonesChecksum = this.GetChecksumAndIds(cwsc.GetAllZonesChecksum(), out zonesIds);

                    if (zonesChecksum.Equals(this.GetAllZonesChecksum(zonesIds), StringComparison.Ordinal))
                    {
                        Logger.Default.Log(
                            "La lista delle linee sul database locale è allineata con quella in centrale, nessuna operazione richiesta.",
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, Logger.I_GENERIC);

                        this.Zones = new GrisZone[] {};
                    }
                    else
                    {
                        List<GrisZone> zones = new List<GrisZone>();

                        #region Deserializzazione manuale da oggetto stringa compresso restituito da WS

                        string serializedData = CompressedBinaryConverter.ToOriginalString(cwsc.GetAllZones());
                        string[] zonesDataList = serializedData.Split('\n');

                        foreach (string zoneItem in zonesDataList)
                        {
                            if (!String.IsNullOrEmpty(zoneItem))
                            {
                                string[] zoneData = zoneItem.Split('§');

                                if (zoneData.Length == 3)
                                {
                                    zones.Add(new GrisZone {ZonID = long.Parse(zoneData[0]), RegID = long.Parse(zoneData[1]), Name = zoneData[2]});
                                }
                            }
                        }

                        #endregion

                        this.Zones = zones.ToArray();

                        int updatedInsertedCounter = 0;
                        int updatedInsertedErrorCounter = 0;

                        foreach (GrisZone zone in zones)
                        {
                            if (
                                SqlDataBaseUtility.InsertZone(this.dbConnection.ConnectionString, zone.ZonID, zone.RegID, zone.Name, NOT_REMOVED) >
                                0)
                            {
                                updatedInsertedCounter++;
                            }
                            else
                            {
                                updatedInsertedErrorCounter++;
                            }
                        }

                        if (updatedInsertedErrorCounter != 0)
                        {
                            Logger.Default.Log(
                                String.Format("Aggiornate / inserite correttamente {0} linee. {1} errori durante il salvataggio.", //se ci sono errori deve diventare warn //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.WARNING, Logger.I_GENERIC);
                        }
                        else
                        { 
                            Logger.Default.Log(
                                String.Format("Aggiornate / inserite correttamente {0} linee. {1} errori durante il salvataggio.", //se ci sono errori deve diventare warn //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.NOTICE, Logger.I_GENERIC);
                        }
                    }

                    string nodesIds;
                    string nodesChecksum = this.GetChecksumAndIds(cwsc.GetAllNodesChecksum(), out nodesIds);

                    if (nodesChecksum.Equals(this.GetAllNodesChecksum(nodesIds), StringComparison.Ordinal))
                    {
                        Logger.Default.Log(
                            "La lista delle stazioni sul database locale è allineata con quella in centrale, nessuna operazione richiesta.", //notice
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, Logger.I_GENERIC);

                        this.Nodes = new GrisNode[] {};
                    }
                    else
                    {
                        List<GrisNode> nodes = new List<GrisNode>();

                        #region Deserializzazione manuale da oggetto stringa compresso restituito da WS

                        string serializedData = CompressedBinaryConverter.ToOriginalString(cwsc.GetAllNodes());
                        string[] nodesDataList = serializedData.Split('\n');

                        foreach (string nodeItem in nodesDataList)
                        {
                            if (!String.IsNullOrEmpty(nodeItem))
                            {
                                string[] nodeData = nodeItem.Split('§');

                                if (nodeData.Length == 4)
                                {
                                    nodes.Add(new GrisNode
                                    {
                                        NodID = long.Parse(nodeData[0]),
                                        ZonID = long.Parse(nodeData[1]),
                                        Name = nodeData[2],
                                        Meters = int.Parse(nodeData[3])
                                    });
                                }
                            }
                        }

                        #endregion

                        this.Nodes = nodes.ToArray();

                        int updatedInsertedCounter = 0;
                        int updatedInsertedErrorCounter = 0;

                        foreach (GrisNode node in nodes)
                        {
                            if (
                                SqlDataBaseUtility.InsertNode(this.dbConnection.ConnectionString, node.NodID, node.ZonID, node.Name, NOT_REMOVED,
                                    node.Meters) > 0)
                            {
                                updatedInsertedCounter++;
                            }
                            else
                            {
                                updatedInsertedErrorCounter++;
                            }
                        }

                        if (updatedInsertedErrorCounter != 0)
                        { 
                            Logger.Default.Log(
                                String.Format("Aggiornate / inserite correttamente {0} stazioni. {1} errori durante il salvataggio.", // se ci sono errori deve divantare warn //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.WARNING, Logger.I_GENERIC);
                        }
                        else
                        {
                            Logger.Default.Log(
                                String.Format("Aggiornate / inserite correttamente {0} stazioni. {1} errori durante il salvataggio.", // se ci sono errori deve divantare warn //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.NOTICE, Logger.I_GENERIC);
                        }
                    }

                    if (SqlDataBaseUtility.RemoveAdditionalNodesFromDatabase(this.dbConnection.ConnectionString, nodesIds))
                    {
                        Logger.Default.Log("Ripulite correttamente le stazioni aggiuntive.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, Logger.I_GENERIC);    
                    }
                        
                    if (SqlDataBaseUtility.RemoveAdditionalZonesFromDatabase(this.dbConnection.ConnectionString, zonesIds))
                    {
                        Logger.Default.Log("Ripulite correttamente le linee aggiuntive.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, Logger.I_GENERIC);
                    }

                    if (SqlDataBaseUtility.RemoveAdditionalRegionsFromDatabase(this.dbConnection.ConnectionString, regionsIds))
                    {
                        Logger.Default.Log("Ripuliti correttamente i compartimenti aggiuntivi.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, Logger.I_GENERIC);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.DownloadRegionListDataAndUpdateDatabase' - : (" + ex.GetType() + ") " + ex.Message + " - " +
                    ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return false;
        }

        /// <summary>
        ///     Scarica dati da CentralWS e scrive in base dati Tipi Periferica, Sistemi e Produttori
        /// </summary>
        /// <returns>False se ci sono stati problemi, True altrimenti</returns>
        public bool DownloadDeviceTypeListAndUpdateDatabase()
        {
            if (!this.canProceedAfterInitialization) return false;
            if (STLCConfig.IsSTLCHost) return false;

            try
            {
                using (CentralSoapClient cwsc = new CentralSoapClient())
                {
                    string systemsIds;
                    string systemsChecksum = this.GetChecksumAndIds(cwsc.GetAllSystemsChecksum(), out systemsIds);

                    if (systemsChecksum.Equals(this.GetAllSystemsChecksum(systemsIds), StringComparison.Ordinal))
                    {
                        Logger.Default.Log(
                            "La lista dei sistemi sul database locale è allineata con quella in centrale, nessuna operazione richiesta.", //notice
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, Logger.I_GENERIC);

                        this.Systems = new GrisSystem[] {};
                    }
                    else
                    {
                        List<GrisSystem> systems = new List<GrisSystem>();

                        #region Deserializzazione manuale da oggetto stringa compresso restituito da WS

                        string serializedData = CompressedBinaryConverter.ToOriginalString(cwsc.GetAllSystems());
                        string[] systemsDataList = serializedData.Split('\n');

                        foreach (string systemItem in systemsDataList)
                        {
                            if (!String.IsNullOrEmpty(systemItem))
                            {
                                string[] systemData = systemItem.Split('§');

                                if (systemData.Length == 2)
                                {
                                    systems.Add(new GrisSystem {SystemID = int.Parse(systemData[0]), SystemDescription = systemData[1]});
                                }
                            }
                        }

                        #endregion

                        this.Systems = systems.ToArray();

                        int updatedInsertedCounter = 0;
                        int updatedInsertedErrorCounter = 0;

                        foreach (GrisSystem system in systems)
                        {
                            if (SqlDataBaseUtility.InsertSystem(this.dbConnection.ConnectionString, system.SystemID, system.SystemDescription) > 0)
                            {
                                updatedInsertedCounter++;
                            }
                            else
                            {
                                updatedInsertedErrorCounter++;
                            }
                        }

                        if (updatedInsertedErrorCounter != 0)
                        { 
                            Logger.Default.Log(
                                String.Format("Aggiornati / inseriti correttamente {0} sistemi. {1} errori durante il salvataggio.", // warn se ci sono errori //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.WARNING, Logger.I_GENERIC);
                        }
                        else
                        {
                            Logger.Default.Log(
                                String.Format("Aggiornati / inseriti correttamente {0} sistemi. {1} errori durante il salvataggio.", // warn se ci sono errori //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.NOTICE, Logger.I_GENERIC);
                        }
                    }

                    string vendorsIds;
                    string vendorsChecksum = this.GetChecksumAndIds(cwsc.GetAllVendorsChecksum(), out vendorsIds);

                    if (vendorsChecksum.Equals(this.GetAllVendorsChecksum(vendorsIds), StringComparison.Ordinal))
                    {
                        Logger.Default.Log(
                            "La lista dei produttori sul database locale è allineata con quella in centrale, nessuna operazione richiesta.", //notice
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, Logger.I_GENERIC);

                        this.Vendors = new GrisVendor[] {};
                    }
                    else
                    {
                        List<GrisVendor> vendors = new List<GrisVendor>();

                        #region Deserializzazione manuale da oggetto stringa compresso restituito da WS

                        string serializedData = CompressedBinaryConverter.ToOriginalString(cwsc.GetAllVendors());
                        string[] vendorsDataList = serializedData.Split('\n');

                        foreach (string vendorItem in vendorsDataList)
                        {
                            if (!String.IsNullOrEmpty(vendorItem))
                            {
                                string[] vendorData = vendorItem.Split('§');

                                if (vendorData.Length == 3)
                                {
                                    vendors.Add(new GrisVendor
                                    {
                                        VendorID = int.Parse(vendorData[0]),
                                        VendorName = vendorData[1],
                                        VendorDescription = String.IsNullOrEmpty(vendorData[2]) ? null : vendorData[2]
                                    });
                                }
                            }
                        }

                        #endregion

                        this.Vendors = vendors.ToArray();

                        int updatedInsertedCounter = 0;
                        int updatedInsertedErrorCounter = 0;

                        foreach (GrisVendor vendor in vendors)
                        {
                            if (
                                SqlDataBaseUtility.InsertVendor(this.dbConnection.ConnectionString, vendor.VendorID, vendor.VendorName,
                                    vendor.VendorDescription) > 0)
                            {
                                updatedInsertedCounter++;
                            }
                            else
                            {
                                updatedInsertedErrorCounter++;
                            }
                        }

                        if (updatedInsertedErrorCounter != 0)
                        { 
                            Logger.Default.Log(
                                String.Format("Aggiornati / inseriti correttamente {0} produttori. {1} errori durante il salvataggio.", // warn se ci sono errori //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.WARNING, Logger.I_GENERIC);
                        }
                        else
                        {
                            Logger.Default.Log(
                                String.Format("Aggiornati / inseriti correttamente {0} produttori. {1} errori durante il salvataggio.", // warn se ci sono errori //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.NOTICE, Logger.I_GENERIC);
                        }
                    }

                    string deviceTypesIds;
                    string deviceTypesChecksum = this.GetChecksumAndIds(cwsc.GetAllDeviceTypesChecksum(), out deviceTypesIds);

                    if (deviceTypesChecksum.Equals(this.GetAllDeviceTypesChecksum(deviceTypesIds), StringComparison.Ordinal))
                    {
                        Logger.Default.Log(
                            "La lista dei tipi periferica sul database locale è allineata con quella in centrale, nessuna operazione richiesta.", //notice
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, Logger.I_GENERIC);

                        this.DeviceTypes = new GrisDeviceType[] {};
                    }
                    else
                    {
                        List<GrisDeviceType> deviceTypes = new List<GrisDeviceType>();

                        #region Deserializzazione manuale da oggetto stringa compresso restituito da WS

                        string serializedData = CompressedBinaryConverter.ToOriginalString(cwsc.GetAllDeviceTypes());
                        string[] deviceTypesDataList = serializedData.Split('\n');

                        foreach (string deviceTypeItem in deviceTypesDataList)
                        {
                            if (!String.IsNullOrEmpty(deviceTypeItem))
                            {
                                string[] deviceTypeData = deviceTypeItem.Split('§');

                                if (deviceTypeData.Length == 5)
                                {
                                    deviceTypes.Add(new GrisDeviceType
                                    {
                                        DeviceTypeID = deviceTypeData[0],
                                        SystemID = int.Parse(deviceTypeData[1]),
                                        VendorID = int.Parse(deviceTypeData[2]),
                                        DeviceTypeDescription = deviceTypeData[3],
                                        WSUrlPattern = String.IsNullOrEmpty(deviceTypeData[4]) ? null : deviceTypeData[4]
                                    });
                                }
                            }
                        }

                        #endregion

                        this.DeviceTypes = deviceTypes.ToArray();

                        int updatedInsertedCounter = 0;
                        int updatedInsertedErrorCounter = 0;

                        foreach (GrisDeviceType deviceType in deviceTypes)
                        {
                            if (
                                !String.IsNullOrEmpty(SqlDataBaseUtility.InsertDeviceType(this.dbConnection.ConnectionString,
                                    deviceType.DeviceTypeID, deviceType.SystemID, deviceType.VendorID, deviceType.DeviceTypeDescription,
                                    deviceType.WSUrlPattern)))
                            {
                                updatedInsertedCounter++;
                            }
                            else
                            {
                                updatedInsertedErrorCounter++;
                            }
                        }

                        if (updatedInsertedErrorCounter != 0)
                        { 
                            Logger.Default.Log(
                                String.Format("Aggiornati / inseriti correttamente {0} tipi periferica. {1} errori durante il salvataggio.", // warn se ci sono errori //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.WARNING, Logger.I_GENERIC);
                        }
                        else
                        {
                            Logger.Default.Log(
                                String.Format("Aggiornati / inseriti correttamente {0} tipi periferica. {1} errori durante il salvataggio.", // warn se ci sono errori //notice
                                    updatedInsertedCounter, updatedInsertedErrorCounter), Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.NOTICE, Logger.I_GENERIC);
                        }
                    }

                    if (SqlDataBaseUtility.RemoveAdditionalDeviceTypesFromDatabase(this.dbConnection.ConnectionString, deviceTypesIds))
                    {
                        Logger.Default.Log("Ripuliti correttamente i tipi periferica aggiuntivi.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, Logger.I_GENERIC); 
                    }

                    if (SqlDataBaseUtility.RemoveAdditionalSystemsFromDatabase(this.dbConnection.ConnectionString, systemsIds))
                    {
                        Logger.Default.Log("Ripuliti correttamente i sistemi aggiuntivi.", Logger.LogType.CONSOLE_LOC_FILE,
                            Logger.EventType.INFORMATION, Logger.I_GENERIC);
                    }

                    if (SqlDataBaseUtility.RemoveAdditionalVendorsFromDatabase(this.dbConnection.ConnectionString, vendorsIds))
                    {
                        Logger.Default.Log("Ripuliti correttamente i produttori aggiuntivi.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, Logger.I_GENERIC);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.DownloadDeviceTypeListAndUpdateDatabase' - : (" + ex.GetType() + ") " + ex.Message + " - " +
                    ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return false;
        }

        private const int WS_DATA_CHECKSUM_POSITION = 0;
        private const int WS_DATA_IDS_POSITION = 1;

        private string GetChecksumAndIds(string wsReturnValue, out string ids)
        {
            ids = String.Empty;

            if (!String.IsNullOrEmpty(wsReturnValue))
            {
                // Il formato previsto delle stringhe restituite dal WS è "checksum;id1,id2,id3"
                string[] data = wsReturnValue.Split(';');

                if (data.Length == 2)
                {
                    ids = (data[WS_DATA_IDS_POSITION] ?? String.Empty);

                    return (data[WS_DATA_CHECKSUM_POSITION] ?? String.Empty);
                }

                return String.Empty;
            }

            return String.Empty;
        }

        private string GetAllDeviceTypesChecksum(string ids)
        {
            try
            {
                ReadOnlyCollection<GrisDeviceType> deviceTypes = SqlDataBaseUtility.GetDeviceTypeListFromDatabase(this.dbConnection.ConnectionString,
                    ids);
                string deviceTypesRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(deviceTypes);

                if (!String.IsNullOrEmpty(deviceTypesRepresentation))
                {
                    string checksum;

                    using (SHA256 sha = new SHA256Managed())
                    {
                        checksum =
                            BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(deviceTypesRepresentation)))
                                .Replace("-", string.Empty);
                    }

                    return checksum;
                }

                return String.Empty;
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.GetAllDeviceTypesChecksum' - : (" + ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return String.Empty;
        }

        private string GetAllSystemsChecksum(string ids)
        {
            try
            {
                ReadOnlyCollection<GrisSystem> systems = SqlDataBaseUtility.GetSystemListFromDatabase(this.dbConnection.ConnectionString, ids);
                string systemsRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(systems);

                if (!String.IsNullOrEmpty(systemsRepresentation))
                {
                    string checksum;

                    using (SHA256 sha = new SHA256Managed())
                    {
                        checksum =
                            BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(systemsRepresentation)))
                                .Replace("-", string.Empty);
                    }

                    return checksum;
                }

                return String.Empty;
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.GetAllSystemsChecksum' - : (" + ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return String.Empty;
        }

        private string GetAllVendorsChecksum(string ids)
        {
            try
            {
                ReadOnlyCollection<GrisVendor> vendors = SqlDataBaseUtility.GetVendorListFromDatabase(this.dbConnection.ConnectionString, ids);
                string vendorsRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(vendors);

                if (!String.IsNullOrEmpty(vendorsRepresentation))
                {
                    string checksum;

                    using (SHA256 sha = new SHA256Managed())
                    {
                        checksum =
                            BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(vendorsRepresentation)))
                                .Replace("-", string.Empty);
                    }

                    return checksum;
                }

                return String.Empty;
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.GetAllVendorsChecksum' - : (" + ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return String.Empty;
        }

        private string GetAllRegionsChecksum(string ids)
        {
            try
            {
                ReadOnlyCollection<GrisRegion> regions = SqlDataBaseUtility.GetRegionListFromDatabase(this.dbConnection.ConnectionString, ids);
                string regionsRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(regions);

                if (!String.IsNullOrEmpty(regionsRepresentation))
                {
                    string checksum;

                    using (SHA256 sha = new SHA256Managed())
                    {
                        checksum =
                            BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(regionsRepresentation)))
                                .Replace("-", string.Empty);
                    }

                    return checksum;
                }

                return String.Empty;
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.GetAllRegionsChecksum' - : (" + ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return String.Empty;
        }

        private string GetAllZonesChecksum(string ids)
        {
            try
            {
                ReadOnlyCollection<GrisZone> zones = SqlDataBaseUtility.GetZoneListFromDatabase(this.dbConnection.ConnectionString, ids);
                string zonesRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(zones);

                if (!String.IsNullOrEmpty(zonesRepresentation))
                {
                    string checksum;

                    using (SHA256 sha = new SHA256Managed())
                    {
                        checksum =
                            BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(zonesRepresentation)))
                                .Replace("-", string.Empty);
                    }

                    return checksum;
                }

                return String.Empty;
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.GetAllZonesChecksum' - : (" + ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return String.Empty;
        }

        private string GetAllNodesChecksum(string ids)
        {
            try
            {
                ReadOnlyCollection<GrisNode> nodes = SqlDataBaseUtility.GetNodeListFromDatabase(this.dbConnection.ConnectionString, ids);
                string nodesRepresentation = SqlDataBaseUtility.GetCollectionStringRepresentation(nodes);

                if (!String.IsNullOrEmpty(nodesRepresentation))
                {
                    string checksum;

                    using (SHA256 sha = new SHA256Managed())
                    {
                        checksum =
                            BitConverter.ToString(sha.ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(nodesRepresentation)))
                                .Replace("-", string.Empty);
                    }

                    return checksum;
                }

                return String.Empty;
            }
            catch (Exception ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'CentralWSFunction.GetAllNodesChecksum' - : (" + ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            return String.Empty;
        }
    }
}
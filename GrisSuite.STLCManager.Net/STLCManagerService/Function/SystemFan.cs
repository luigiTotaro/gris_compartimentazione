﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using STLCManager.Service.Types;
using STLCManager.Service.Wrapper;
using Timer = System.Timers.Timer;

namespace STLCManager.Service.Function
{
	/// <summary>
	///		Classe gestione soglie di intervento ventole
	/// </summary>
	public class FanThreshold
	{
		#region FanThreshold Members

		public int Temperature { get; set; } // temperatura di soglia
		public int Hysteresis { get; set; } // isteresi temperatura
		public int PWMRightFan { get; set; } // valore percentuale di PWM per la ventola destra
		public int PWMLeftFan { get; set; } // valore percentuale di PWM per la ventola sinistra

		#endregion

		#region FanThreshold Constructors

		/// <summary>
		///		Costruttore soglia.
		/// </summary>
		/// <param name="temperature">temperatura di intervento soglia</param>
		/// <param name="hysteresis">isteresi temperatura</param>
		/// <param name="pwrRightFan">percentuale di attivazione ventola di dx</param>
		/// <param name="pwrLeftFan">percebtuale di attivazione ventola di sx</param>
		public FanThreshold(int temperature, int hysteresis, int pwrRightFan, int pwrLeftFan)
		{
			this.Temperature = temperature;
			this.Hysteresis = hysteresis;
			this.PWMRightFan = pwrRightFan;
			this.PWMLeftFan = pwrLeftFan;
		}

		/// <summary>
		///		Costruttore soglia, tutti i valori sono impostati a zero
		/// </summary>
		public FanThreshold() : this(0, 0, 0, 0)
		{
		}

		#endregion
	}

	/// <summary>
	///		Classe creata al caricamento del codice per la gestione delle ventole di sistema
	/// </summary>
	internal static class SystemFan
	{
		/// <summary>
		///		Sensori di temperatura gestiti
		/// </summary>
		public enum SensorTemperature
		{
			SENSOR_NULL,
			SENSOR_CPU,
			SENSOR_BOARD
		}

		private const int MAX_READ_FAILURE = 3;     // Numero di letture errate consecutive per attivazione del System Failure
//        private const int MAX_READ_FAILURE = 0;   DEBUG provoca System Failure anche per una sola lettura erreta

		#region Members

		public static bool IsEnabled { get; private set; } // true se modulo gestione ventole attivo
		private static bool isRunning; // thread gestione led di sistema in funzione (true ad ogni attivazione del thread)
		public static SensorTemperature Sensor { get; set; } // sensore attivato per rillevare la temperatura
		private static uint checkTimeout; // tempo di polling di gestione ventole
		private static readonly List<FanThreshold> thresholdList; // lista soglie di gestione ventole
		public static bool IsFailure { get; private set; } // segnalazione errore gestione ventole

		private static Timer timerManageFan; // thread di gestione ventole di sistema

		public static FanThreshold CurrentThreshold { get; private set; } // soglia attiva in base alla temperatura rilevata
		public static int CurrentTemperature { get; private set; } // ultima temperatura rilevata dal modulo di controllo

		private static int numOfReadFailure { get; set; }	// numero di erroi nella lettura della temperatura

		#endregion

		#region Constructors

		/// <summary>
		///		Costruttote statico classe gestione ventole di sistema. Creata allo startup.
		/// </summary>
		static SystemFan()
		{
			IsEnabled = false;
			isRunning = false;
			IsFailure = false;
			Sensor = SensorTemperature.SENSOR_NULL;
			CheckTimeout = 60;

			thresholdList = new List<FanThreshold>();

			CurrentThreshold = new FanThreshold();
			CurrentTemperature = 0;

			numOfReadFailure = 0;
		}

		#endregion

		#region Accessor

		public static UInt32 ThreadPeriod
		{
			get { return checkTimeout; }
		}

		/// <summary>
		///		Accesso al timeout di gestione ventole. Valore interno alla classe espresso in msec, valore ricevuto/ritornato in
		///		sec.
		/// </summary>
		public static uint CheckTimeout
		{
			get { return checkTimeout/1000; }
			set { checkTimeout = value*1000; }
		}

		#endregion

		#region Methods

		/// <summary>
		///		Restituisce il flag di running (processo in eseguzione corretta) e lo azzera
		/// </summary>
		/// <returns>
		///		<list type="bool">
		///			<item> true: avvenuto ciclo di schedulazione dopo ultima chiamata</item>
		///			<item>false: nessuna schedulazione avvenuta</item>
		///		</list>
		/// </returns>
		public static bool IsRunning()
		{
			if (isRunning)
			{
				isRunning = false;
				return true;
			}
			return false;
		}

		/// <summary>
		///		Imposta soglia di governo ventole
		/// </summary>
		/// <param name="sTemp">temperatura di riferimento soglia</param>
		/// <param name="sHyst">isteresi temperatura</param>
		/// <param name="sRFan">percentuale PWM ventola di dx</param>
		/// <param name="sLFan">percentuale PWM ventola di sx</param>
		/// <returns>
		///		<list type="bool">
		///			<item> true: soglia impostata correttamente</item>
		///			<item>false: soglia non valida</item>
		///		</list>
		/// </returns>
		public static bool SetThreshold(string sTemp, string sHyst, string sRFan, string sLFan)
		{
			try
			{
				int iTemp = int.Parse(sTemp);
				int iHyst = int.Parse(sHyst);
				int iRFan = int.Parse(sRFan);
				int iLFan = int.Parse(sLFan);

				if (iHyst < 0)
				{
					return false;
				}
				if (iRFan < 0 || 100 < iRFan)
				{
					return false;
				}
				if (iLFan < 0 || 100 < iLFan)
				{
					return false;
				}

				AddThreshold(new FanThreshold(iTemp, iHyst, iRFan, iLFan));
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		///		Aggiunge soglia alla lista.
		/// </summary>
		/// <param name="threshold">soglia da aggiungere</param>
		public static void AddThreshold(FanThreshold threshold)
		{
			thresholdList.Add(threshold);
		}

		/// <summary>
		///		Azzera intera lista di definizione soglie
		/// </summary>
		public static void ClearThreshold()
		{
			thresholdList.Clear();
		}

		/// <summary>
		///		Abilita gestione ventole di stato sistema. Crea timer per gestione periodica delle ventole.
		///		Ordina la lista delle soglie di intervento in ordire decrescente di temperatura.
		/// </summary>
		/// <returns>
		///		<list type="bool">
		///			<item> true: gestione ventole di sistema abilitata</item>
		///			<item>false: errore, impossibile abilitare gestione ventole di sistema</item>
		///		</list>
		/// </returns>
		public static bool Enable()
		{
			if (STLCConfig.IsSTLCHost && !IsEnabled)
			{
				try
				{
					isRunning = false;

					SetPWMFans(0, 0);

					thresholdList.Sort(CompareThreshold);
					thresholdList.Reverse();

					timerManageFan = new Timer(checkTimeout);
					timerManageFan.Elapsed += ManageFAN;
					timerManageFan.AutoReset = true;
					timerManageFan.Enabled = true;
					timerManageFan.Start();

					IsEnabled = true;

					Logger.Default.Log("Attivata gestione SystemFan", Logger.LogType.CONSOLE_LOC_FILE);

					ManageFAN();
				}
				catch (Exception ex)
				{
					Logger.Default.Log("ECCEZIONE in 'SystemFan.Enable': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				}
			}
			return IsEnabled;
		}

		/// <summary>
		///		Comparazione soglie. La comparazione delle soglie è eseguita tramite il valore di temperatura
		/// </summary>
		/// <param name="t1">soglia t1</param>
		/// <param name="t2">soglia t2</param>
		/// <returns>
		///		<list type="int">
		///		<item>-1: t1 minore di t2</item>
		///		<item> 0: t1 uguale a t2</item>
		///		<item>+1: t1 maggiore di t2</item>
		///		</list>
		/// </returns>
		private static int CompareThreshold(FanThreshold t1, FanThreshold t2)
		{
			if (t1 == null)
			{
				return t2 == null ? 0 : -1;
			}

			if (t2 == null)
			{
				return +1;
			}

			if (t1.Temperature < t2.Temperature)
			{
				return -1;
			}
			if (t1.Temperature > t2.Temperature)
			{
				return +1;
			}

			return 0;
		}

		/// <summary>
		///		Disabilita gestione ventole di sistema.
		/// </summary>
		/// <returns>
		///		<list type="bool">
		///			<item> true: gestione ventole di sistema disabilitata</item>
		///		</list>
		/// </returns>
		public static bool Disable()
		{
			if (STLCConfig.IsSTLCHost)
			{
				if (IsEnabled)
				{
					timerManageFan.Stop();
					timerManageFan.Dispose();
					timerManageFan = null;
					IsEnabled = false;

					Logger.Default.Log("Disattivata gestione SystemFan", Logger.LogType.CONSOLE_LOC_FILE);
				}
				SetPWMFans(0, 0);
			}
			return !IsEnabled;
		}

		/// <summary>
		///		Stampa su console informazioni di debug.
		/// </summary>
		public static void Debug()
		{
			Logger.Default.Log("GESTIONE VENTOLE: " + (IsEnabled ? "Attiva" : "Disattiva"), Logger.LogType.CONSOLE);
			Logger.Default.Log(" Timeout controllo " + CheckTimeout + " sec", Logger.LogType.CONSOLE);
			switch (Sensor)
			{
				case SensorTemperature.SENSOR_CPU:
					Logger.Default.Log(" Rilevamento temperatura CPU", Logger.LogType.CONSOLE);
					break;
				case SensorTemperature.SENSOR_BOARD:
					Logger.Default.Log(" Rilevamento temperatura MB", Logger.LogType.CONSOLE);
					break;
				default:
					Logger.Default.Log(" Rilevamento temperatura N/A", Logger.LogType.CONSOLE);
					break;
			}

			foreach (FanThreshold threshold in thresholdList)
			{
				Logger.Default.Log(
					" Temp " + threshold.Temperature + " °C;" + " Isteresi " + threshold.Hysteresis + ";" + " Ventola Dx " + threshold.PWMRightFan + "%;" +
					" Ventola Sx " + threshold.PWMLeftFan + "%.", Logger.LogType.CONSOLE);
			}
		}

		/// <summary>
		///		Esegue lettura temperatura in base al sensore selezionato
		/// </summary>
		/// <param name="temperature">temperatura rilevata</param>
		/// <returns>
		///		<list type="int">
		///		<item>	  COD_NO_ERROR: lettura eseguita correttamente</item>
		///		<item>codice di errore: lettura fallita</item>"
		///		</list>
		/// </returns>
		public static int GetTemperature(out int temperature)
		{
			int retCode = AppCfg.COD_NO_ERROR;

			temperature = 0;

			if (STLCConfig.IsSTLCHost)
			{
				double tempCPU = 0;
				int tempMB = 0;
				try
				{
					switch (Sensor)
					{
						case SensorTemperature.SENSOR_CPU:
							retCode = JidaPlatform.Default.GetCPUTemperature(out tempCPU);
							temperature = (int) tempCPU;
							break;

						case SensorTemperature.SENSOR_BOARD:
							// Nota: la selezione del sensore di temperatura ad ogni lettura si è resa necessaria per ovviare 
							// alla perdita della configurazione da parte del winbond a fronte di scariche elettrostatiche.
							if ((retCode = JidaPlatform.Default.SelectSensorTemperature()) == AppCfg.COD_NO_ERROR)
							{
								retCode = JidaPlatform.Default.GetMBTemperature(out tempMB);
								if (retCode == AppCfg.COD_NO_ERROR)
								{
									temperature = tempMB;
								}
								else
								{
									numOfReadFailure++;
									Logger.Default.Log("Errore nella lettura della temperatura by Jada - numero errori= " + numOfReadFailure, Logger.LogType.CONSOLE_LOC_FILE);
								}
							}
							else
							{
								numOfReadFailure++;
								Logger.Default.Log("Errore nella selezione sensore temperatura by Jada - numero errori= " + numOfReadFailure, Logger.LogType.CONSOLE_LOC_FILE);
								retCode = AppCfg.E_COD_TEMPERATURE_SENSOR_NOT_AVAILABLE;
							}
							break;

						default:
							retCode = AppCfg.E_COD_TEMPERATURE_SENSOR_NOT_AVAILABLE;
							break;
					}
				}
				catch (Exception ex)
				{
					retCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
					Logger.Default.Log("ECCEZIONE in 'SystemFan.GetTemperature': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, retCode);
				}

				if (retCode != AppCfg.COD_NO_ERROR)
				{
					Logger.Default.Log("ERRORE in 'SystemFan.GetTemperature': temperatura sistema non disponibile.", Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, retCode);
				}
			}

			return retCode;
		}

		/// <summary>
		///		Imposta velocità ventole di sistema
		/// </summary>
		/// <param name="pwmRightFan">percentuale di attivazione ventola di dx</param>
		/// <param name="pwmLeftFan">percentuale di attivazione ventola di sx</param>
		/// <returns>
		///		<list type="int">
		///			<item>	  COD_NO_ERROR: impostazione eseguita correttamente</item>
		///			<item>codice di errore: impostazione fallita</item>"
		///		</list>
		/// </returns>
		private static int SetPWMFans(int pwmRightFan, int pwmLeftFan)
		{
			int retCode = AppCfg.COD_NO_ERROR;

			try
			{
				// Nota: la configurazione dei registri di controllo ad ogni impostazione si è resa necessaria per ovviare 
				// alla perdita della configurazione da parte del winbond a fronte di scariche elettrostatiche.
				if ((retCode = JidaPlatform.Default.SetFansControlRegister()) == AppCfg.COD_NO_ERROR)
				{
					if ((0 < pwmRightFan && pwmRightFan < 10) || (0 < pwmLeftFan && pwmLeftFan < 10))
					{
						// Da lo spunto di partenza, per assicurarsi che le ventile comincino a girare
						if (retCode == AppCfg.COD_NO_ERROR && pwmRightFan != 0)
						{
							retCode = JidaPlatform.Default.SetPWMRightFan(10);
						}

						if (retCode == AppCfg.COD_NO_ERROR && pwmLeftFan != 0)
						{
							retCode = JidaPlatform.Default.SetPWMLeftFan(10);
						}

						if (retCode == AppCfg.COD_NO_ERROR)
						{
							Thread.Sleep(100);
						}
					}

					if (retCode == AppCfg.COD_NO_ERROR)
					{
						retCode = JidaPlatform.Default.SetPWMRightFan(pwmRightFan);
					}

					if (retCode == AppCfg.COD_NO_ERROR)
					{
						retCode = JidaPlatform.Default.SetPWMLeftFan(pwmLeftFan);
					}
				}
			}
			catch (Exception ex)
			{
				retCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
				Logger.Default.Log("ECCEZIONE in 'SystemFan.SetPWMFans': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, retCode);
			}

			if (retCode != AppCfg.COD_NO_ERROR)
			{
				Logger.Default.Log("ERRORE in 'SystemFan.SetPWMFans': impostazione velocità ventole fallita.", Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, retCode);
			}
			return retCode;
		}

		/// <summary>
		///		Gestione ventole di sistema, legge la temperatura, verifica soglia e imposta velocità ventole.
		/// </summary>
		private static void ManageFAN()
		{
			int retCode = AppCfg.COD_NO_ERROR;
			int temperature = 0;

			try
			{
				timerManageFan.Enabled = false;

				isRunning = true;

				// lettura temperatura sistema
				retCode = GetTemperature(out temperature);

				if (retCode == AppCfg.COD_NO_ERROR)
				{
					CurrentTemperature = temperature;
					numOfReadFailure = 0;
					
					switch (Sensor)
					{
						case SensorTemperature.SENSOR_CPU:
							Logger.Default.Log("Temperatura CPU: " + CurrentTemperature + " °C", Logger.LogType.CONSOLE_LOC_FILE);
							break;

						case SensorTemperature.SENSOR_BOARD:
							Logger.Default.Log("Temperatura MB: " + CurrentTemperature + " °C", Logger.LogType.CONSOLE_LOC_FILE);
							break;
					}

					FanThreshold newThreshold = new FanThreshold(CurrentTemperature, 2, 0, 0);

					if (CurrentTemperature < (CurrentThreshold.Temperature - CurrentThreshold.Hysteresis))
					{
						// Temperatura in diminuzione. Ricerca nuova soglia gestione ventole
						foreach (FanThreshold threshold in thresholdList)
						{
							if ((threshold.Temperature - threshold.Hysteresis) <= CurrentTemperature)
							{
								// individuata soglia da applicare
								newThreshold = threshold;
								break;
							}
						}
					}
					else if (CurrentTemperature > CurrentThreshold.Temperature)
					{
						// Temperatura in aumento. Ricerca nuova soglia gestione ventole
						foreach (FanThreshold threshold in thresholdList)
						{
							if (threshold.Temperature <= CurrentTemperature)
							{
								// individuata soglia da applicare
								newThreshold = threshold;
								break;
							}
						}
					}
					else
					{
						// Temperatura stabile (dentro intervallo soglia: TRef-hyst <= t <= TRef)
						timerManageFan.Enabled = true;
						return;
					}

					if (newThreshold != CurrentThreshold)
					{
						CurrentThreshold = newThreshold;

						Logger.Default.Log(
							"Nuova soglia: Temperatura " + CurrentThreshold.Temperature + " °C; Isteresi " + CurrentThreshold.Hysteresis + "; Ventola Dx " +
							CurrentThreshold.PWMRightFan + "%; Ventola Sx " + CurrentThreshold.PWMLeftFan + "%.", Logger.LogType.CONSOLE_LOC_FILE);
					}
					retCode = SetPWMFans(CurrentThreshold.PWMRightFan, CurrentThreshold.PWMLeftFan);
				}

				if ((retCode != AppCfg.COD_NO_ERROR) && (numOfReadFailure > MAX_READ_FAILURE))
				{
					Disable();
					Logger.Default.Log("Attivazione SYSTEM FAILURE !!", Logger.LogType.CONSOLE_LOC_FILE);
					IsFailure = true;
				}
				else
				{
					timerManageFan.Enabled = true;
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemFan.ManageFAN': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				Disable();
			}
		}

		/// <summary>
		///		Funzione chiamata dal thread periodico. Esegue gestione delle ventole ed in caso di errore segnala disattivazione
		///		modulo.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void ManageFAN(object sender, ElapsedEventArgs e)
		{
			ManageFAN();
		}

		#endregion
	}
}
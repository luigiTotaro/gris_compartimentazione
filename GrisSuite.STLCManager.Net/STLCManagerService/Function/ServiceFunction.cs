﻿using System;
using System.ServiceProcess;
using Microsoft.Win32;
using System.Threading;
using STLCManager.Service.Types;
using TimeoutException = System.ServiceProcess.TimeoutException;
using System.Diagnostics;

namespace STLCManager.Service.Function
{
    internal class ServiceFunction
    {
        public const int NO_ERROR = AppCfg.COD_NO_ERROR;

        public const int TIMEOUT_COMMAND_MS = 10000;

        //Creazione del Singleton ServiceFuntion
        public static ServiceFunction Default = new ServiceFunction();

        protected ServiceFunction()
        {
            try
            {
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
            }
        }

        /// <summary>
        ///     Restituisce lo stato del servizio indicato
        /// </summary>
        /// <param name="serviceName">nome servizio</param>
        /// <returns>Stato del servizio</returns>
        public ServiceControllerStatus GetServiceStatus(string serviceName, string serviceProcessName)
        {
            ServiceControllerStatus scs = new ServiceControllerStatus();
            try
            {
                ServiceController sc = new ServiceController(serviceName);
                scs = sc.Status;

                if(scs == ServiceControllerStatus.Stopped)
                {
                    if (!String.IsNullOrEmpty(serviceProcessName))
                    {
                        KillProcess(serviceProcessName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
            }
            return scs;
        }

        /// <summary>
        /// Verifica che il servizio si sia effettivamente arrestato
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="serviceProcessName"></param>
        public bool CheckServiceIsStopped(string serviceName, string serviceProcessName, ServiceControl.Mode mode)
        {
            ServiceControllerStatus scs = new ServiceControllerStatus();
            try
            {
                if (!String.IsNullOrEmpty(serviceProcessName))
                {
                    int retCode = NO_ERROR;

                    Logger.Default.Log(String.Format("CheckServiceIsStopped -> ServiceControl.Mode:{0}", mode), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION);

                    if (mode == ServiceControl.Mode.RUNNING)
                    {
                        retCode = KillProcess(serviceProcessName);
                    }
                    else
                    {
                        ServiceController sc = new ServiceController(serviceName);
                        scs                  = sc.Status;

                        Logger.Default.Log(String.Format("CheckServiceIsStopped -> ServiceControllerStatus:{0}", scs), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION);

                        if (scs == ServiceControllerStatus.Stopped)
                        {
                            retCode = KillProcess(serviceProcessName);
                        }
                    }

                    if (retCode == NO_ERROR) {
                        Logger.Default.Log(String.Format("Nessuna istanza del processo {0} è attiva", serviceProcessName), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
            }

            return false;
        }

        /// <summary>
        ///     Restituisce il display name del servizio indicato
        /// </summary>
        /// <param name="serviceName">nome servizio</param>
        /// <returns>display name</returns>
        public string GetServiceDisplayName(string serviceName)
        {
            try
            {
                ServiceController sc = new ServiceController(serviceName);
                return sc.DisplayName;
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
            }
            return "";
        }

        //==============================================================================
        /// Funzione per far partire un servizio dopo un tempo di attesa con timeout di verifica dello start avvenuto
        /// 
        /// Restituisce un valore boolenao con l'esito positivo o meno della chiamata
        /// 
        /// \date [06.11.2015]
        /// \author Dino Angeli
        /// \version 0.01
        /// <param name="serviceName">nome servizio</param>
        /// <param name="timeoutMilliseconds">tempo massimo entro il quale il servizio deve essere running (millisecondi)</param>
        /// <param name="startupDelay">tempo di attesa prima di far partire il servizio (millisecondi)</param>
        /// <returns>eventuale codice di errore</returns>
        //------------------------------------------------------------------------------
        public int StartService(string serviceName, int timeoutMilliseconds, uint startupDelay, ref ServiceControl.Mode mode)
        {
            try
            {
                //           Logger.Default.Log("--- Start del servizio " + serviceName + " con delay " + (startupDelay/1000).ToString() + " sec, mode= " + mode, Logger.LogType.CONSOLE_LOC_FILE);
                if (startupDelay != 0)
                {
                    if (mode == ServiceControl.Mode.DISABLED)
                        mode = ServiceControl.Mode.WAITING_NOMON;
                    Thread.Sleep(TimeSpan.FromMilliseconds(startupDelay));
                }
                if (mode == ServiceControl.Mode.WAITING_STOPPED)
                {
                    Logger.Default.Log("Starting...  del servizio " + serviceName + " non effettuato (STLCManager in arresto).", Logger.LogType.CONSOLE_LOC_FILE);
                    return NO_ERROR;  //Sopravvenuta richiesta di stop; non faccio partire il servizio.
                }
                if (mode == ServiceControl.Mode.WAITING)
                    mode = ServiceControl.Mode.WARNING;
                else if (mode != ServiceControl.Mode.WARNING)
                    mode = ServiceControl.Mode.DISABLED;
                // Se mode == Mode.WARNING rimane uguale (ripartenza dopo uno stop)

                if (timeoutMilliseconds != 0)
                {
                    Logger.Default.Log("Starting... del servizio " + serviceName + " (timeout = " + (timeoutMilliseconds/1000).ToString() + ")", Logger.LogType.CONSOLE_LOC_FILE);
                    return StartService(serviceName, timeoutMilliseconds);
                }
                else
                {
                    Logger.Default.Log("Starting...  del servizio " + serviceName, Logger.LogType.CONSOLE_LOC_FILE);
                    return StartService(serviceName);
                }
            }

            catch (ThreadAbortException)
            {
                //ECCEZIONE dovuta allo stop del thread sospeso sulla sleep(), NON va considerata.

                //Logger.Default.Log("ECCEZIONE di tipo 'ThreadAbortException' in 'ServiceFunction.StartService(" + serviceName + ")': " + ex.GetType() + " - " + ex.ToString(),
                //    Logger.LogType.CONSOLE_LOC_FILE);
                return NO_ERROR;
            }

            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE generica in 'ServiceFunction.StartService(" + serviceName + ")': " + ex.GetType() + " - " + ex.ToString(),
                    Logger.LogType.CONSOLE_LOC_FILE);
                return NO_ERROR;
            }
        }

        //==============================================================================
        /// Funzione per far partire un servizio 
        /// 
        /// Restituisce un valore boolenao con l'esito positivo o meno della chiamata
        /// 
        /// \date [11.10.2011]
        /// \author Mario Ferro
        /// \version 0.01
        //------------------------------------------------------------------------------
        public int StartService(string serviceName, int timeoutMilliseconds)
        {
            int retCode = NO_ERROR;
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (InvalidOperationException ioEx)
            {
                retCode = AppCfg.E_COD_SERVICE_START_FAILURE;
                Logger.Default.Log("Start del servizio " + serviceName + " non eseguito correttamente. ECCEZIONE: " + ioEx.Message,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }
            catch (TimeoutException tEx)
            {
                retCode = AppCfg.E_COD_SERVICE_START_FAILURE;
                Logger.Default.Log("Start del servizio " + serviceName + " non eseguito nel tempo utile. ECCEZIONE: " + tEx.Message,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }
            catch (ThreadAbortException)
            {
                //ECCEZIONE dovuta allo stop del thread richiesto, NON va considerata.

                retCode = AppCfg.COD_NO_ERROR;
                //Logger.Default.Log("ECCEZIONE di tipo 'ThreadAbortException' in 'ServiceFunction.StartService(" + serviceName + ")': " + ex.GetType() + " - " + ex.ToString(),
                //    Logger.LogType.CONSOLE_LOC_FILE);
            }
            catch (Exception ex)
            {
                retCode = AppCfg.E_COD_SERVICE_START_FAILURE;
                Logger.Default.ManageException(ex);
            }
            return retCode;
        }

        /// <summary>
        ///     Comanda lo start del servizio indicato
        /// </summary>
        /// <param name="serviceName">nome servizio</param>
        /// <returns>eventuale codice di errore</returns>
        public int StartService(string serviceName)
        {
            int retCode = NO_ERROR;
            ServiceController service = new ServiceController(serviceName);
            try
            {
                service.Start();
            }
            catch (InvalidOperationException ioEx)
            {
                retCode = AppCfg.E_COD_SERVICE_START_FAILURE;
                Logger.Default.Log("Start del servizio " + serviceName + " non eseguito correttamente. ECCEZIONE: " + ioEx.Message,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }
            catch (ThreadAbortException)
            {
                //ECCEZIONE dovuta allo stop del thread richiesto, NON va considerata.

                retCode = AppCfg.COD_NO_ERROR;
                //Logger.Default.Log("ECCEZIONE di tipo 'ThreadAbortException' in 'ServiceFunction.StartService(" + serviceName + ")': " + ex.GetType() + " - " + ex.ToString(),
                //    Logger.LogType.CONSOLE_LOC_FILE);
            }
            catch (Exception ex)
            {
                retCode = AppCfg.E_COD_SERVICE_START_FAILURE;
                Logger.Default.ManageException(ex);
            }
            return retCode;
        }

        //==============================================================================
        /// Funzione per fermare l'esecuzione di un servizio 
        /// 
        /// Restituisce un valore boolenao con l'esito positivo o meno ddella chiamata
        /// 
        /// \date [11.10.2011]
        /// \author Mario Ferro
        /// \version 0.01
        //------------------------------------------------------------------------------
        public int StopService(string serviceName, int timeoutMilliseconds)
        {
            int retCode = NO_ERROR;

            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch (InvalidOperationException ioEx)
            {
                retCode = AppCfg.E_COD_SERVICE_STOP_FAILURE;
                Logger.Default.Log("Stop del servizio " + serviceName + " non eseguito correttamente. ECCEZIONE: " + ioEx.Message,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }
            catch (TimeoutException tEx)
            {
                retCode = AppCfg.E_COD_SERVICE_STOP_FAILURE;
                Logger.Default.Log("Stop del servizio " + serviceName + " non eseguito nel tempo utile. ECCEZIONE: " + tEx.Message,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }
            catch (Exception ex)
            {
                retCode = AppCfg.E_COD_SERVICE_STOP_FAILURE;
                Logger.Default.ManageException(ex);
            }
            return retCode;
        }

        /// <summary>
        ///     Comanda lo stop del servizio indicato
        /// </summary>
        /// <param name="serviceName">nome serzio</param>
        /// <returns>eventuale codice di errore</returns>
        public int StopService(string serviceName, string serviceProcessName = "")
        {
            int retCode = NO_ERROR;

            ServiceController service = new ServiceController(serviceName);
            try
            {
                service.Stop();
            }
            catch (InvalidOperationException ioEx)
            {
                retCode = AppCfg.E_COD_SERVICE_STOP_FAILURE;
                Logger.Default.Log("Stop del servizio " + serviceName + " non eseguito correttamente. ECCEZIONE: " + ioEx.Message,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }
            catch (Exception ex)
            {
                retCode = AppCfg.E_COD_SERVICE_STOP_FAILURE;
                Logger.Default.ManageException(ex);
            }
            // Verifica se al servizio arrestato corrisponda l'assenza del relativo processo
            ServiceFunction.Default.CheckServiceIsStopped(serviceName, serviceProcessName, ServiceControl.Mode.RUNNING);

            return retCode;
        }

        /// <summary>
        /// Forza il kill di un processo appeso
        /// </summary>
        /// <param name="serviceProcessName">nome del processo da killare</param>
        public int KillProcess(string serviceProcessName)
        {
            Process[] myProcesses;

            int retCode = NO_ERROR;

            try
            { 
                myProcesses = Process.GetProcessesByName(serviceProcessName);
                foreach (Process myProcess in myProcesses)
                {
                    Logger.Default.Log(String.Format("Kill del servizio con processo {0} [PID: {1}] in corso...", serviceProcessName, myProcess.Id), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION);
                    myProcess.Kill();
                }

                
            }
            catch (Exception ioEx)
            {
                retCode = AppCfg.E_COD_SERVICE_STOP_FAILURE;
                Logger.Default.Log("Stop del servizio con processo " + serviceProcessName + " non eseguito correttamente. ECCEZIONE: " + ioEx.Message,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }

            return retCode;
        }

        /// <summary>
        ///     Restituisce un STLCUnixTime riportante l'ultima data/ora scritta nella chiave di registro del servizio.
        ///     In caso di errore nella lettura della chiave restituisce Data 01/01/1970.
        /// </summary>
        /// <param name="serviceName">nome servizio</param>
        /// <returns>data/ora letta dalla chiave di registro.</returns>
        private STLCUnixTime GetKeyCheckUnixTimeValue(string serviceName)
        {
            STLCUnixTime timeValue = new STLCUnixTime(0L);

            try
            {
                string lastAutoCheckServicesKeyPath = AppCfg.Default.sPathLastAutoCheckServicesKey + serviceName; // .FP_LAST_AUTO_CHECK_SERVICES_KEY_VALUE_PATH + serviceName;
                timeValue.Value =
                    (long) (int) Registry.GetValue(lastAutoCheckServicesKeyPath, AppCfg.Default.sLastAutoCheckKeyName, 0); // .FP_LAST_AUTO_CHECK_KEY_NAME_VALUE_PATH, 0);
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof (NullReferenceException))
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'ServiceFunction.GetKeyCheckUnixTimeValue': NullReferenceException in GetKeyCheckTimeValue. LastAutoCheckTime non trovato",
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_COD_INVALID_REGKEY_PATH);
                }
                else
                {
                    Logger.Default.Log("ECCEZIONE in 'ServiceFunction.GetKeyCheckUnixTimeValue': " + ex.GetType().ToString() + " - " + ex.StackTrace,
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                }
            }

            return timeValue;
        }

        /// <summary>
        ///     Verifica se il servizio è bloccato. Il servizio si considera in blocco quando la data/ora scritta dal servizio
        ///     stesso
        ///     sulla chiave di registro è antecedente all'ora attuale per più di blockedTimeout secondi
        /// </summary>
        /// <param name="serviceName">nome servizio</param>
        /// <param name="blockedTimeout">tempo massimo entro il quale il servizio deve aggiornare la chiave di registro (secondi)</param>
        /// <returns></returns>
        public bool CheckServiceIsBlocked(string serviceName, DateTime dtLastCheck, out DateTime dtCurrenCheck)
        {
            dtCurrenCheck        = this.GetKeyCheckUnixTimeValue(serviceName).DateTime;

            if (dtLastCheck <= DateTime.MinValue) // Alla primo giro memorizzo solo il valore precedente e non faccio nulla
                return false;

            if(dtCurrenCheck == dtLastCheck)
            {
                //Logger.Default.Log("Chiamata funzione CheckServiceIsBlocked per servizio " + serviceName, Logger.LogType.CONSOLE_LOC_FILE);
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Formattazione della data/ora in stringa (DD/MM/YYYY hh:mm:ss)
        /// </summary>
        /// <param name="_dt">data/ora</param>
        /// <returns>stringa formattata.</returns>
        private String GetDateTimeInFormatString(DateTime? _dt)
        {
            String retDateInFormatString = "";
            try
            {
                if (_dt != null)
                {
                    DateTime difsDateTime = (DateTime) _dt;
                    retDateInFormatString = difsDateTime.Day.ToString("D2") + "/" + difsDateTime.Month.ToString("D2") + "/" +
                                            difsDateTime.Year.ToString("D4") + " " + difsDateTime.Hour.ToString("D2") + ":" +
                                            difsDateTime.Minute.ToString("D2") + ":" + difsDateTime.Second.ToString("D2");
                }
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
            }
            return retDateInFormatString;
        }
    }
}
﻿using System;
using System.Management;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using STLCManager.Service.FBWF;

 
namespace STLCManager.Service.Function
{
    class SysInfoFunction
    {
        //Creazione del Singleton SysInfoFunction
        public static SysInfoFunction Default = new SysInfoFunction();

        #region Methods
        /// <summary>
        /// Legge la variabile di sistema COMPUTERNAME.
        /// </summary>
        /// <param name="sName">nome computer</param>
        /// <param name="resultDesc">descrizione esisto operazione</param>
        /// <returns>
        ///     <list type="int">
        ///         <item>AppConst.COD_NO_ERROR      : funzione eseguita con successo</item>
        ///         <item>AppConst.FUNCTION_EXCEPTION: verificatasi eccessione</item>
        ///     </list>
        /// </returns>
        public int GetComputerName(out string sName, out string resultDesc)
        {
            int retCode = AppCfg.COD_NO_ERROR;
            resultDesc = "Operazione eseguita con successo";
            sName = "";

            try
            {
                sName = System.Environment.GetEnvironmentVariable("COMPUTERNAME");
            }
            catch (Exception ex)
            {
                retCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                resultDesc = "ECCEZIONE in 'SysInfoFunction.GetComputerName': " + ex.StackTrace;
                Logger.Default.Log(resultDesc, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return retCode;
        }
        /// <summary>
        /// Legge la variabile di sistema COMPUTERNAME.
        /// </summary>
        /// <param name="sName">nome computer</param>
        /// <returns>
        ///     <list type="int">
        ///         <item>AppConst.COD_NO_ERROR      : funzione eseguita con successo</item>
        ///         <item>AppConst.FUNCTION_EXCEPTION: verificatasi eccessione</item>
        ///     </list>
        /// </returns>
        public int GetComputerName(out string sName)
        {
            string resultDesc;

            return GetComputerName(out sName, out resultDesc);
        }


        public bool SetComputerName(string pcName)
        {
            try
            {
                string compPath = "Win32_ComputerSystem.Name='" + System.Environment.MachineName + "'";
                ManagementObject comp = new ManagementObject(new ManagementPath(compPath));

                ManagementBaseObject inputArgs = comp.GetMethodParameters("Rename");
                inputArgs["Name"] = pcName;
                // Other input args are UserName and Password.
                // If not specified the current user security context is used to execute the command.
                // See WMI sdk docs or MSDN for details.

                ManagementBaseObject outParams = comp.InvokeMethod("Rename", inputArgs, null);
                uint ret = (uint)(outParams.Properties["ReturnValue"].Value);
                if (ret == 0)
                    return true;
                else
                {
                    Logger.Default.Log("SysInfoFunction.SetComputerName: fallita (errore=" + ret + ")",
                                       Logger.LogType.CONSOLE_LOC_FILE,
                                       Logger.EventType.ERROR,
                                       Logger.E_GENERIC
                                      );
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SysInfoFunction.SetComputerName': " + ex.GetType().ToString() + " - " + ex.StackTrace,
                                   Logger.LogType.CONSOLE_LOC_FILE,
                                   Logger.EventType.ERROR,
                                   Logger.E_STACK_TRACE
                                  );
            }
            return false;
        }
        #endregion

        /// <summary>
        /// Executes a shell command synchronously.
        /// </summary>
        /// <param name="command">string command</param>
        /// <returns>string, as output of the command.</returns>
        public void ExecuteCommandSync(object command)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                // Log the commmand output.
                Logger.Default.Log("SysInfoFunction.ExecuteCommandSync" + result, 
                                   Logger.LogType.CONSOLE_LOC_FILE,
                                   Logger.EventType.INFORMATION,
                                   AppCfg.E_COD_RUN_SYSTEM_COMMAND_SUCCESS
                                  );
            }
            catch (Exception ex)
            {
                throw new FileBasedWriteFilterException("Esecuzione commando fallita.", ex);
            }
        }
    }
}

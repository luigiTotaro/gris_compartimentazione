﻿using System;
using System.Timers;
using STLCManager.Service.FBWF;
using STLCManager.Service.Types;

namespace STLCManager.Service.Function
{
	/// <summary>
	///     Classe per il controllo della memoria occupata
	/// </summary>
	public class SystemFBWF : FileBasedWriteFilter
	{
		#region Token

		private uint DEFAULT_CHECK_TIMEOUT = 20*60; // default timeout di verifica occupazione memoria (secondi)
		private uint DEFAULT_MAX_MEM_PERC = 80; // default percentuale soglia di massima occupazione memoria consentita

		#endregion

		#region Members

		public bool IsEnabled { get; private set; } // processo di controllo memoria abilitato 
		public bool IsFailure { get; private set; } // errore, memoria occupata oltre la soglia consentita
		private bool isRunning; // processo di controllo memoria in funzione (segnala avvenuto ciclo di schedulazione)

		private uint checkTimeout; // timeout di verifica occupazione memoria (millisecondi)
		private uint maxUsedMemoryPerc; // percentuale soglia di massima occupazione memoria consentita
		public uint UsedMemoryPerc { get; private set; } // percentuale memoria usata

		private Timer timerManageFBWF; // thread di gestione FBWF

		#endregion

		#region Accessor

		/// <summary>
		///     Restituisce/imposta timeout di schedulazione in secondi
		/// </summary>
		public uint CheckTimeout
		{
			get { return this.checkTimeout/1000; }
			set { this.checkTimeout = value*1000; }
		}

		/// <summary>
		///     Restituisce periodicità thread di verifica memoria (msec)
		/// </summary>
		public uint ThreadPeriod
		{
			get { return this.checkTimeout; }
		}

		#endregion

		#region Constructors

		/// <summary>
		///     Inizializza una nuova istanza della classe SystemFBWF
		/// </summary>
		/// <param name="session">identificativo sessione memoria</param>
		public SystemFBWF(Session session) : base(session)
		{
			this.IsEnabled = false;
			this.IsFailure = false;
			this.isRunning = false;

			this.CheckTimeout = this.DEFAULT_CHECK_TIMEOUT;
			this.maxUsedMemoryPerc = this.DEFAULT_MAX_MEM_PERC;

			this.UsedMemoryPerc = 0;

			this.timerManageFBWF = null;
		}

		#endregion

		#region Methods

		/// <summary>
		///     Imposta parametri di controllo memoria (timeout di schedulazione e percentuale memoria occupatata).
		/// </summary>
		/// <param name="sTimeout">periodicità di verifica memoria</param>
		/// <param name="sMemPerc">soglia memoria</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: parametri validi</item>
		///         <item>false: errore parametri non validi</item>
		///     </list>
		/// </returns>
		public bool SetController(string sTimeout, string sMemPerc)
		{
			try
			{
				if (!String.IsNullOrEmpty(sTimeout))
				{
					uint iTimeout = uint.Parse(sTimeout);
					this.CheckTimeout = iTimeout;
				}

				if (!String.IsNullOrEmpty(sMemPerc))
				{
					uint iMemPerc = uint.Parse(sMemPerc);
					if (0 < iMemPerc && iMemPerc <= 100)
					{
						this.maxUsedMemoryPerc = iMemPerc;
					}
				}
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		///     Abilita processo di verifica memoria
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: gestione memoria di sistema abilitata</item>
		///         <item>false: errore, impossibile abilitare gestione memoria di sistema</item>
		///     </list>
		/// </returns>
		public bool Enable()
		{
			if (STLCConfig.IsSTLCHost && !this.IsEnabled)
			{
				try
				{
					this.isRunning = false;

					this.timerManageFBWF = new Timer(this.checkTimeout);
					this.timerManageFBWF.Elapsed += this.ManageFBWF;
					this.timerManageFBWF.AutoReset = true;
					this.timerManageFBWF.Enabled = true;
					this.timerManageFBWF.Start();

					this.IsEnabled = true;

					Logger.Default.Log("Attivata gestione SystemFBWF.", Logger.LogType.CONSOLE_LOC_FILE);

					this.ManageFBWF();
				}
				catch (Exception ex)
				{
					Logger.Default.Log("ECCEZIONE in 'SystemFBWF.Enable': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				}
			}
			return this.IsEnabled;
		}

		/// <summary>
		///     Disabilita processo di verifica memoria.
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: gestione memoria di sistema disabilitata</item>
		///     </list>
		/// </returns>
		public bool Disable()
		{
			if (this.IsEnabled)
			{
				this.timerManageFBWF.Stop();
				this.timerManageFBWF.Dispose();
				this.timerManageFBWF = null;
				this.IsEnabled = false;

				Logger.Default.Log("Disattivata gestione SystemFBWF.", Logger.LogType.CONSOLE_LOC_FILE);
			}
			return !this.IsEnabled;
		}

		/// <summary>
		///     Restituisce il flag di running (processo in eseguzione corretta) e lo azzera
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: avvenuto ciclo di schedulazione dopo ultima chiamata</item>
		///         <item>false: nessuna schedulazione avvenuta</item>
		///     </list>
		/// </returns>
		public bool IsRunning()
		{
			if (this.isRunning)
			{
				this.isRunning = false;
				return true;
			}
			return false;
		}

		/// <summary>
		///     Restituisce lo stato di abilitazione del filtro
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: filtro abilitato</item>
		///         <item>false: filtro disabilitato</item>
		///     </list>
		/// </returns>
		public new bool IsFilterEnabled()
		{
			try
			{
				if (STLCConfig.IsSTLCHost && base.IsFilterEnabled())
				{
					return true;
				}
			}
			catch (FileBasedWriteFilterException fbwfEx)
			{
				Logger.Default.Log(fbwfEx.Message, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.WARNING, AppCfg.E_COD_FBWF_NOT_SUPPORTED);
			}

			return false;
		}

		/// <summary>
		///     Controlla stato di occupazione della memoria.
		/// </summary>
		private void ManageFBWF()
		{
			try
			{
				this.timerManageFBWF.Enabled = false;

				this.isRunning = true;

				// controllo stato memoria
				FileBasedWriteFilterMemoryUsage memoryUsage = base.GetMemoryUsage();

				this.UsedMemoryPerc = (uint) memoryUsage.CurrentSessionUsedMemoryPercentage;

				Logger.Default.Log("SystemFBWF: Memoria occupata = " + this.UsedMemoryPerc + "%", Logger.LogType.CONSOLE_LOC_FILE);

				if (this.maxUsedMemoryPerc < this.UsedMemoryPerc && !this.IsFailure)
				{
					Logger.Default.Log("SystemFBWF: Superata soglia massima di memoria utilizzabile", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
						AppCfg.E_COD_FBWF_MEM_PERC_OVERFLOW);
					this.IsFailure = true;
				}

				this.timerManageFBWF.Enabled = true;
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'SystemFBWF.ManageFBWF': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				this.Disable();
			}
		}

		/// <summary>
		///     Funzione chiamata dal thread periodico. Esegue controllo occupazione memoria in caso di errore segnala
		///     disattivazione modulo.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ManageFBWF(object sender, ElapsedEventArgs e)
		{
			this.ManageFBWF();

			if (!this.IsEnabled)
			{
				Logger.Default.Log("Disattivato FBWF", Logger.LogType.CONSOLE_LOC_FILE);
			}
		}

		#endregion
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using STLCManager.Service.Database;

namespace STLCManager.Service.Function
{
    public sealed class SystemXmlFunction
    {
        private static volatile SystemXmlFunction instance;
        private static object syncRoot = new Object();

        private SqlConnection dbConnection;
        private ReadOnlyCollection<DeviceObject> existingDevicesOnDatabase;
        private readonly string configurationSystemFile;
        private bool canProceedAfterInitialization = false;
        private bool hasLoadedDataFromSystemXml = false;

        public ReadOnlyCollection<DeviceObject> DevicesFromXmlConfig { get; private set; }

        private SystemXmlFunction()
        {
            this.configurationSystemFile = AppCfg.Default.sXMLSystem; // .FP_CFG_XML_SYSTEM_VALUE_PATH;

            this.InitializeStart();
        }

        private enum DeviceWriteMode
        {
            Insert,
            Update
        };

        public static SystemXmlFunction Default
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new SystemXmlFunction();
                        }
                    }
                }

                return instance;
            }
        }

        #region Inizializzazione, verifica configurazione e caricamento lista periferiche da database

        private void InitializeStart()
        {
            bool _xmlFilePresent = true;
            ConnectionStringSettings localDatabaseConnectionString = null;

            if (string.IsNullOrEmpty(this.configurationSystemFile))
            {
                Logger.Default.Log("Nome del file di configurazione con la lista dei dispositivi da monitorare non definito",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_CONFIG_XML);
                _xmlFilePresent = false;
            }

            else if (!FileUtility.CheckFileCanRead(this.configurationSystemFile))
            {
                Logger.Default.Log(
                    string.Format(CultureInfo.InvariantCulture, "1. Impossibile leggere il file di configurazione con la lista dei dispositivi: {0}",
                        this.configurationSystemFile), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_CONFIG_XML);
                _xmlFilePresent = false;
            }

            try
            {
                localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];
            }
            catch (ConfigurationErrorsException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SystemXmlFunction.InitializeStart' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }

            if (localDatabaseConnectionString == null)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemXmlFunction.InitializeStart' - La stringa di connessione al database non è valida",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                return;
            }

            try
            {
                this.dbConnection = new SqlConnection(localDatabaseConnectionString.ConnectionString);
            }
            catch (ArgumentException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SystemXmlFunction.InitializeStart' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                return;
            }

            if ((this.dbConnection != null) && (_xmlFilePresent))
            {
                // this.LoadExistingDeviceListFromDatabase(); // Il caricamento della lista dei devices verrà fatto successivamente.
            }
            else
            {
                Logger.Default.Log("ECCEZIONE in 'SystemXmlFunction.InitializeStart' - Impossibile accedere al database configurato o al file XML",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_DATABASE);
            }
        }

        private bool LoadExistingDeviceListFromDatabase()
        {
            if ((this.dbConnection == null) || (this.dbConnection.ConnectionString == null))
            {
                Logger.Default.Log("LoadExistingDeviceListFromDatabase() unknown connection string", Logger.LogType.CONSOLE_LOC_FILE);
                return false;
            }
            this.existingDevicesOnDatabase = SqlDataBaseUtility.GetExistingDeviceListFromDatabase(this.dbConnection.ConnectionString);
            return (this.existingDevicesOnDatabase != null);
        }

        #endregion

        /// <summary>
        ///     Carica i dati del System.xml, esegue le opportune verifiche su database, crea e ripulisce le periferiche
        /// </summary>
        /// <param name="writeOnDatabase">Indica se scrivere la lista delle periferiche su database</param>
        /// <returns>False se ci sono stati problemi, True altrimenti</returns>
        public bool ParseSystemXmlOnDatabase(bool writeOnDatabase, bool logging)
        {
            if (this.configurationSystemFile == null)
            {
                Logger.Default.Log("File System.xml non disponibile !", Logger.LogType.CONSOLE_LOC_FILE);
                this.canProceedAfterInitialization = false;
                return false;
            }
            if (!FileUtility.CheckFileCanRead(this.configurationSystemFile))
            {
                if (logging)
                    Logger.Default.Log(string.Format(CultureInfo.InvariantCulture, "File {0} non disponibile !", this.configurationSystemFile),
                                       Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.WARNING, AppCfg.I_SYSTEM_XML_NO_ERROR);
                return false;
            } 
  
            // Ricarichiamo la lista di periferiche da database, per eseguire lookup veritiere, prima di nuovi aggiornamenti o inserimenti
            this.canProceedAfterInitialization = this.LoadExistingDeviceListFromDatabase();

            // Logger.Default.Log("ParseSystemXmlOnDatabase() - canProceedAfterInitialization = " + (this.canProceedAfterInitialization ? "true" : "false"), Logger.LogType.CONSOLE_LOC_FILE);
            
            if (this.canProceedAfterInitialization)
            {
                return this.CleanupAndCreateDevices(writeOnDatabase);
            }

            this.hasLoadedDataFromSystemXml = false;

            Logger.Default.Log("ParseSystemXmlOnDatabase() - RICHIESTA NON EVASA", Logger.LogType.CONSOLE_LOC_FILE);

            // Non occorre logging, è delegato ai singoli metodi
            return false;
        }

        #region CleanupAndCreateDevices - Carica le periferiche da configurazione, ripulisce la base dati e ricrea le periferiche

        private bool CleanupAndCreateDevices(bool writeOnDatabase)
        {
            bool cleanupAndCreateDevicesComplete = false;

            this.DevicesFromXmlConfig = SystemXmlHelper.GetMonitorDeviceList(this.configurationSystemFile);

            this.hasLoadedDataFromSystemXml = true;

            if (writeOnDatabase)
            {
                if (this.MarkRowsAsRemoved())
                {
                    if ((this.DevicesFromXmlConfig != null) && (this.DevicesFromXmlConfig.Count > 0))
                    {
                        Logger.Default.Log(
                            string.Format(CultureInfo.InvariantCulture, "Caricate {0} periferiche da System.xml per il monitoraggio. Lista dei Supervisori necessari al monitoraggio: {1}",
                                this.DevicesFromXmlConfig.Count.ToString(),
                                SupervisorUtility.GetNeededSupervisorListNames(SystemXmlFunction.Default.GetNeededSupervisorList(false))),
                             Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, AppCfg.I_SYSTEM_XML_NO_ERROR);
                               
                        cleanupAndCreateDevicesComplete = true;

                        foreach (DeviceObject deviceObject in this.DevicesFromXmlConfig)
                        {
                            bool hasSameType;
                            bool deviceExists = this.CheckDeviceExistence(deviceObject.DeviceId, deviceObject.DeviceType, out hasSameType);

                            if (deviceExists)
                            {
                                // la periferica esiste in base dati
                                if (hasSameType)
                                {
                                    // la periferica esiste in base dati e ha lo stesso tipo di quella caricata da System.xml, la aggiorniamo senza cancellarne dati
                                    if (!this.UpdateInsertDevice(deviceObject, DeviceWriteMode.Update))
                                    {
                                        cleanupAndCreateDevicesComplete = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    // la periferica esiste, ma con un tipo differente, quindi deve essere cancellata per poi essere reinserita
                                    if (this.DeleteDevice(deviceObject))
                                    {
                                        // la periferica è stata correttamente cancellata, possiamo reinserirla
                                        if (!this.UpdateInsertDevice(deviceObject, DeviceWriteMode.Insert))
                                        {
                                            cleanupAndCreateDevicesComplete = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        // non è stato possibile cancellare la periferica esistente
                                        Logger.Default.Log(
                                            string.Format(CultureInfo.InvariantCulture,
                                                "ECCEZIONE in 'SystemXmlFunction.CleanupAndCreateDevices' - Periferica presente in base dati e necessaria cancellazione, ma eliminazione fallita per device: {0}",
                                                deviceObject), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                            AppCfg.E_SYSTEM_XML_FUNCTION_EXCEPTION_DURING_DATABASE_OPERATION);

                                        cleanupAndCreateDevicesComplete = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                // la periferica non esiste in base dati e dovrebbe essere creata
                                if (!this.UpdateInsertDevice(deviceObject, DeviceWriteMode.Insert))
                                {
                                    cleanupAndCreateDevicesComplete = false;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        Logger.Default.Log(
                            "In 'SystemXmlFunction.CleanupAndCreateDevices' - Non sono disponibili periferiche da monitorare nel file di configurazione del sistema.",
                            Logger.LogType.CONSOLE_LOC_FILE);
                    }

                    if (!this.DeleteRemovedRows())
                    {
                        cleanupAndCreateDevicesComplete = false;
                    }
                }
            }
            else
            {
                if ((this.DevicesFromXmlConfig != null) && (this.DevicesFromXmlConfig.Count > 0))
                {
                    Logger.Default.Log(
                        String.Format(CultureInfo.InvariantCulture,
                            "Caricate {0} periferiche da System.xml per il monitoraggio, ma scrittura su database non attiva.",
                            this.DevicesFromXmlConfig.Count.ToString()), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION,
                        AppCfg.I_SYSTEM_XML_NO_ERROR);

                    cleanupAndCreateDevicesComplete = true;
                }
            }

            return cleanupAndCreateDevicesComplete;
        }

        #endregion

        #region GetNeededSupervisorList - Restituisce una lista di supervisori, come interi numerici, associati alle varie periferiche da monitorare

        /// <summary>
        ///     Restituisce una lista di supervisori, come interi numerici, associati alle varie periferiche da monitorare
        /// </summary>
        /// <returns>Lista di interi relativi ai supervisori di monitoraggio</returns>
        public ReadOnlyCollection<int> GetNeededSupervisorList(bool enableLog)
        {
            if (!this.hasLoadedDataFromSystemXml)
            {
                this.ParseSystemXmlOnDatabase(false, enableLog);
            }

            List<int> supervisorList = new List<int>();

            if ((this.DevicesFromXmlConfig != null) && (this.DevicesFromXmlConfig.Count > 0))
            {
                foreach (DeviceObject device in this.DevicesFromXmlConfig)
                {
                    if (!supervisorList.Contains(device.SupervisorId))
                    {
                        supervisorList.Add(device.SupervisorId);
                    }
                }
            }

            supervisorList.Sort();

            ReadOnlyCollection<int> supervisors = supervisorList.AsReadOnly();

            if (enableLog)
                Logger.Default.Log(String.Format("Lista dei Supervisori necessari al monitoraggio: {0}", SupervisorUtility.GetNeededSupervisorListNames(supervisors)),
                                   Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

            return supervisors;
        }

        #endregion

        #region Verifica esistenza periferica in base a lista di periferiche caricate durante inizializzazione

        private bool CheckDeviceExistence(long deviceId, string deviceType, out bool hasSameType)
        {
            bool deviceExists = false;
            hasSameType = false;

            if ((this.existingDevicesOnDatabase != null) && (this.existingDevicesOnDatabase.Count > 0))
            {
                foreach (DeviceObject device in this.existingDevicesOnDatabase)
                {
                    if (device.DeviceId == deviceId)
                    {
                        deviceExists = true;

                        if (device.DeviceType.Equals(deviceType, StringComparison.OrdinalIgnoreCase))
                        {
                            hasSameType = true;
                        }

                        break;
                    }
                }
            }

            return deviceExists;
        }

        #endregion

        #region Aggiornamento / inserimento di una periferica in base dati

        private bool UpdateInsertDevice(DeviceObject deviceObject, DeviceWriteMode deviceWriteMode)
        {
            bool returnValue = false;

            DeviceDatabaseSupport deviceDatabaseSupport = SqlDataBaseUtility.GetStationBuildingRackPortData(this.dbConnection.ConnectionString,
                deviceObject.Topography, deviceObject.RegionId, deviceObject.RegionName, deviceObject.ZoneId, deviceObject.ZoneName,
                deviceObject.NodeId, deviceObject.NodeName, deviceObject.ServerId, deviceObject.ServerHost, deviceObject.ServerName, deviceObject.Port);

            if (deviceDatabaseSupport != null)
            {
                if (SqlDataBaseUtility.UpdateDevice(this.dbConnection.ConnectionString, deviceObject.DeviceId, deviceObject.NodeId,
                    deviceObject.ServerId, deviceObject.Name, deviceObject.DeviceType, deviceObject.SerialNumber, deviceObject.Address,
                    deviceDatabaseSupport.PortId, deviceObject.ProfileId, deviceObject.Active, deviceObject.Scheduled, 0, deviceDatabaseSupport.RackId,
                    deviceObject.RackPositionRow, deviceObject.RackPositionColumn, deviceObject.MonitoringDeviceId))
                {
                    Logger.Default.Log(
                        string.Format(CultureInfo.InvariantCulture, "Periferica correttamente {0}. {1}, {2}",
                            (deviceWriteMode == DeviceWriteMode.Insert ? "inserita" : "aggiornata"), deviceObject, deviceDatabaseSupport),
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                    returnValue = true;
                }
                else
                {
                    Logger.Default.Log(
                        string.Format(CultureInfo.InvariantCulture,
                            "ECCEZIONE in 'SystemXmlFunction.UpdateInsertDevice' - Errore nell'{0} della periferica su database. {1}, {2}",
                            (deviceWriteMode == DeviceWriteMode.Insert ? "inserimento" : "aggiornamento"), deviceObject, deviceDatabaseSupport),
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_EXCEPTION_DURING_DATABASE_OPERATION);
                }
            }
            else
            {
                Logger.Default.Log(
                    string.Format(CultureInfo.InvariantCulture,
                        "ECCEZIONE in 'SystemXmlFunction.UpdateInsertDevice' - Impossibile trovare i dati della Stazione/Fabbricato/Armadio/Interfaccia/Server su Database, per poter {0} la periferica. {1}",
                        (deviceWriteMode == DeviceWriteMode.Insert ? "creare" : "aggiornare"), deviceObject), Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_DATABASE_UNEXPECTED_DATA_ERROR);
            }

            return returnValue;
        }

        #endregion

        #region Eliminazione di una periferica da base dati

        private bool DeleteDevice(DeviceObject deviceObject)
        {
            bool returnValue = false;

            if (SqlDataBaseUtility.DeleteDevice(this.dbConnection.ConnectionString, deviceObject.DeviceId))
            {
                Logger.Default.Log(string.Format(CultureInfo.InvariantCulture, "Periferica correttamente eliminata. DeviceId={0}", deviceObject.DeviceId),
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                returnValue = true;
            }
            else
            {
                Logger.Default.Log(
                    string.Format(CultureInfo.InvariantCulture, "Errore nell'eliminazione della periferica su database. DeviceId={0}", deviceObject.DeviceId),
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_EXCEPTION_DURING_DATABASE_OPERATION);
            }

            return returnValue;
        }

        #endregion

        #region Impostazione delle righe come da eliminare da database

        private bool MarkRowsAsRemoved()
        {
            bool returnValue = false;

            if (SqlDataBaseUtility.MarkRowsAsRemoved(this.dbConnection.ConnectionString, false))
            {
                //Logger.Default.Log("Impostate tutte le righe come da eliminare/aggiornare in database.", Logger.LogType.CONSOLE_LOC_FILE,
                //    Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                returnValue = true;
            }
            else
            {
                Logger.Default.Log("Errore nell'impostazione delle righe come da eliminare/aggiornare in database.", Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_EXCEPTION_DURING_DATABASE_OPERATION);
            }

            return returnValue;
        }

        #endregion

        #region Eliminazione delle righe marcate come eliminate da base dati

        private bool DeleteRemovedRows()
        {
            bool returnValue = false;

            if (SqlDataBaseUtility.DeleteRemovedRows(this.dbConnection.ConnectionString, false))
            {
                //Logger.Default.Log("Eliminate dal database le rimanenti righe marcate come rimosse.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION,
                //    AppCfg.I_SYSTEM_XML_NO_ERROR);

                returnValue = true;
            }
            else
            {
                Logger.Default.Log("Errore nell'eliminazione dal database delle righe marcate come rimosse.", Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_EXCEPTION_DURING_DATABASE_OPERATION);
            }

            return returnValue;
        }

        #endregion
    }
}
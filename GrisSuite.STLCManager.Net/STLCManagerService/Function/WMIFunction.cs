﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
using System.Net;
using System.Net.Sockets;
using Microsoft.Win32;
using STLCManager.Service.Types;

namespace STLCManager.Service.Function
{
	internal class WMIFunction
	{
		//Creazione del Singleton WMIFunction
		public static WMIFunction Default = new WMIFunction();

		protected WMIFunction()
		{
			try
			{
			}
			catch (Exception ex)
			{
				Logger.Default.ManageException(ex);
			}
		}

		public void RebootSTLC()
		{
			try
			{
				ManagementBaseObject mboShutdown = null;
				ManagementClass mcWin32 = new ManagementClass("Win32_OperatingSystem");
				mcWin32.Get();

				// You can't shutdown without security privileges
				mcWin32.Scope.Options.EnablePrivileges = true;
				ManagementBaseObject mboShutdownParams = mcWin32.GetMethodParameters("Win32Shutdown");

				// Flag 1 means we want to shut down the system. Use "2" to reboot.
				/*
                    0 (0x0)     Log Off
                    4 (0x4)     Forced Log Off (0 + 4)
                    1 (0x1)     Shutdown
                    5 (0x5)     Forced Shutdown (1 + 4)
                    2 (0x2)     Reboot
                    6 (0x6)     Forced Reboot (2 + 4)
                    8 (0x8)     Power Off
                    12 (0xC)    Forced Power Off (8 + 4)
                */

				mboShutdownParams["Flags"] = "6";
				mboShutdownParams["Reserved"] = "0";
				foreach (ManagementObject manObj in mcWin32.GetInstances())
				{
					mboShutdown = manObj.InvokeMethod("Win32Shutdown", mboShutdownParams, null);
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.RebootSTLC': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}

		public void PowerOffSTLC()
		{
			try
			{
				ManagementBaseObject mboShutdown = null;
				ManagementClass mcWin32 = new ManagementClass("Win32_OperatingSystem");
				mcWin32.Get();

				// You can't shutdown without security privileges
				mcWin32.Scope.Options.EnablePrivileges = true;
				ManagementBaseObject mboShutdownParams = mcWin32.GetMethodParameters("Win32Shutdown");

				// Flag 1 means we want to shut down the system. Use "2" to reboot.
				/*
                    0 (0x0)     Log Off
                    4 (0x4)     Forced Log Off (0 + 4)
                    1 (0x1)     Shutdown
                    5 (0x5)     Forced Shutdown (1 + 4)
                    2 (0x2)     Reboot
                    6 (0x6)     Forced Reboot (2 + 4)
                    8 (0x8)     Power Off
                    12 (0xC)    Forced Power Off (8 + 4)
                */

				mboShutdownParams["Flags"] = "12";
				mboShutdownParams["Reserved"] = "0";
				foreach (ManagementObject manObj in mcWin32.GetInstances())
				{
					mboShutdown = manObj.InvokeMethod("Win32Shutdown", mboShutdownParams, null);
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.PowerOffSTLC': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}

		public void RenameSTLC(string newstlcname)
		{
			try
			{
				using (ManagementObject cs = new ManagementObject(@"Win32_Computersystem.Name='" + Environment.MachineName + "'"))
				{
					cs.Get();
					ManagementBaseObject inParams = cs.GetMethodParameters("Rename");
					inParams["Name"] = newstlcname;
					ManagementBaseObject outParams = cs.InvokeMethod("Rename", inParams, null);
				}
				// Check return value ...
				// Convert.ToInt32(outParams.Properties["ReturnValue"].Value));
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.RenameSTLC': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}

        public string GetFullHostName()
        {
            string domainName = "";
            string fullhostName;
            string hostname = Environment.MachineName;

            using (ManagementObject cs = new ManagementObject("Win32_ComputerSystem.Name='" + hostname + "'"))
            {
                try
                {
                    cs.Get();

                    if (cs["partOfDomain"] != null && (bool)cs["partOfDomain"])
                    {
                        domainName = cs["domain"].ToString();
                    }
                }
                catch (ManagementException)
                {
                    domainName = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters", "Domain", "").ToString();
                }
            }

            if (domainName == "")
            {
                fullhostName = hostname;
            }
            else
            {
                fullhostName = string.Format("{0}.{1}", hostname, domainName);
            }

            return fullhostName;
        }

		#region Network WMI Functions

		private static ManagementObjectCollection NetworkAdapters;

		/// <summary>
		///     Restituisce l'identificativo device sistema della porta di rete di cui è noto il nome.
		/// </summary>
		/// <param name="name">Nome della porta di rete.</param>
		/// <param name="devid">Identificativo device sistema determinato.</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: lettura riuscita, devid significativo.</item>
		///         <item>false: lettura fallita.</item>
		///     </list>
		/// </returns>
		public bool GetNetworkID(string name, out UInt32 devid)
		{
			try
			{
				if (NetworkAdapters == null)
				{
					ManagementClass objMC = new ManagementClass("Win32_NetworkAdapter");
					NetworkAdapters = objMC.GetInstances();
				}

				foreach (ManagementObject objMO in NetworkAdapters)
				{
					string netConnectionID = (string) objMO["NetConnectionID"];
					if (netConnectionID == name)
					{
						string deviceID = (string) objMO["DeviceID"];
						devid = UInt32.Parse(deviceID);

						return true;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.GetNetworkID(" + name + ")': " + ex.GetType().ToString() + " - " + ex.StackTrace,
					Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
			devid = 0;
			return false;
		}

		/// <summary>
		///     Restituisce l'identificativo device sistema della porta di rete di cui è noto tipo e numero porta.
		/// </summary>
		/// <param name="type">Tipo della porta di rete.</param>
		/// <param name="port">Numero della porta di rete.</param>
		/// <param name="devid">Identificativo device sistema determinato.</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: lettura riuscita, devid significativo.</item>
		///         <item>false: lettura fallita.</item>
		///     </list>
		/// </returns>
		public bool GetNetworkID(string type, int port, out UInt32 devid)
		{
			string name = type + port.ToString();

			return this.GetNetworkID(name, out devid);
		}

		/// <summary>
		///     Abilita DHCP.
		/// </summary>
		/// <remarks>
		///     Abilita impostazione dinamica indirizzo IP
		/// </remarks>
		/// <param name="DevID">Identificativo device sistema.</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: impostazione eseguita correttamente.</item>
		///         <item>false: impostazione fallita.</item>
		///     </list>
		/// </returns>
		public bool EnableDHCP(UInt32 DevID)
		{
			//http://msdn.microsoft.com/en-us/library/windows/desktop/aa394217%28v=vs.85%29.aspx
			try
			{
				ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection objMOC = objMC.GetInstances();

				foreach (ManagementObject objMO in objMOC)
				{
					if (((UInt32) objMO["Index"]) != DevID)
					{
						continue;
					}

					try
					{
						ManagementBaseObject objDHCP = null;
						ManagementBaseObject objReturn = null;

						objDHCP = objMO.GetMethodParameters("EnableDHCP");

						objReturn = objMO.InvokeMethod("EnableDHCP", objDHCP, null);

						UInt32 retValue = (UInt32) objReturn["returnValue"];

						if (retValue <= 1)
						{
							Logger.Default.Log("WMIFunction.EnableDHCP: eseguita", Logger.LogType.CONSOLE_LOC_FILE);
							return true;
						}
						//errore
						Logger.Default.Log("WMIFunction.EnableDHCP: fallita (error code=" + retValue + ")", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR);
						break;
					}
					catch (Exception ex)
					{
						Logger.Default.Log("WMIFunction.EnableDHCP - impostazione fallita: " + ex.Message, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
							Logger.E_STACK_TRACE);
						break;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.EnableDHCP': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
			return false;
		}

		/// <summary>
		///     Imposta IP statico
		/// </summary>
		/// <remarks>
		///     Configura indirizzo IP e maschera sottorete.
		/// </remarks>
		/// <param name="DevID">Identificativo device.</param>
		/// <param name="IPAddress">Indirizzo IP.</param>
		/// <param name="SubnetMask">Maschera sottorete.</param>
		/// <returns>
		///     <list type="bool">
		///         <item>true: impostazione eseguita correttamente.</item>
		///         <item>false: impostazione fallita.</item>
		///     </list>
		/// </returns>
		public bool SetStaticIP(UInt32 DevID, List<IPAddress> IPAddress, List<IPAddress> SubnetMask)
		{
			//http://msdn.microsoft.com/en-us/library/windows/desktop/aa394217%28v=vs.85%29.aspx
			try
			{
				ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection objMOC = objMC.GetInstances();

				foreach (ManagementObject objMO in objMOC)
				{
					if (((UInt32) objMO["Index"]) != DevID)
					{
						continue;
					}

					try
					{
						ManagementBaseObject objNewIP = null;
						ManagementBaseObject objReturn = null;

						objNewIP = objMO.GetMethodParameters("EnableStatic");

						//Set IPAddress and Subnet Mask
						string[] ip = new string[IPAddress.Count];

						for (int i = 0; i < IPAddress.Count; i++)
						{
							ip[i] = IPAddress[i].ToString();
						}

						objNewIP["IPAddress"] = ip;

						string[] subnet = new string[SubnetMask.Count];

						for (int i = 0; i < SubnetMask.Count; i++)
						{
							subnet[i] = SubnetMask[i].ToString();
						}

						objNewIP["SubnetMask"] = subnet;

						objReturn = objMO.InvokeMethod("EnableStatic", objNewIP, null);

						UInt32 retValue = (UInt32) objReturn["returnValue"];

						if (retValue <= 1)
						{
							string msg = "WMIFunction.SetStaticIP: ip=";

							foreach (string item in ip)
							{
								msg = msg + item + ",";
							}

							msg = msg + " subnet=";

							foreach (string item in subnet)
							{
								msg = msg + item + ",";
							}

							Logger.Default.Log(msg, Logger.LogType.CONSOLE_LOC_FILE);
							return true;
						}
						//errore
						Logger.Default.Log("WMIFunction.SetStaticIP: fallita (error code=" + retValue + ")", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR);
						break;
					}
					catch (Exception ex)
					{
						Logger.Default.Log("WMIFunction.SetStaticIP: fallita (" + ex.Message + ")", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
							Logger.E_STACK_TRACE);
						break;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.SetStaticIP': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
			return false;
		}

		/// <summary>
		///     Imposta Gateway.
		/// </summary>
		/// <remarks>
		///     Configura l'indirizzo del gateway, se indirizzo di gateway non definito azzera l'eventuale gateway configurato.
		/// </remarks>
		/// <param name="DevID">Identificativo device.</param>
		/// <param name="Gateway">Indirizzo del gateway.</param>
		/// <param name="Metric">Metrica.</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: impostazione eseguita correttamente.</item>
		///         <item>false: impostazione fallita.</item>
		///     </list>
		/// </returns>
		public bool SetGateway(UInt32 DevID, List<IPAddress> Gateway, List<UInt16> Metric)
		{
			//http://msdn.microsoft.com/en-us/library/windows/desktop/aa394217%28v=vs.85%29.aspx
			try
			{
				ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection objMOC = objMC.GetInstances();

				foreach (ManagementObject objMO in objMOC)
				{
					if (((UInt32) objMO["Index"]) != DevID)
					{
						continue;
					}

					try
					{
						ManagementBaseObject objGateway = null;
						ManagementBaseObject objReturn = null;

						objGateway = objMO.GetMethodParameters("SetGateways");

						//Set DefaultIPGateway and GatewayCostMetric
						string[] gateway = new string[Gateway.Count];

						for (int i = 0; i < Gateway.Count; i++)
						{
							gateway[i] = Gateway[i].ToString();
						}

						objGateway["DefaultIPGateway"] = gateway;

						int[] metric = new int[Metric.Count];

						for (int i = 0; i < Metric.Count; i++)
						{
							metric[i] = Metric[i];
						}

						objGateway["GatewayCostMetric"] = metric;

						objReturn = objMO.InvokeMethod("SetGateways", objGateway, null);

						UInt32 retValue = (UInt32) objReturn["returnValue"];

						if (retValue <= 1)
						{
							string msg = "WMIFunction.SetGateway: ip=";

							foreach (string item in gateway)
							{
								msg = msg + item + ",";
							}

							msg = msg + " metric=";

							foreach (int item in metric)
							{
								msg = msg + item + ",";
							}

							Logger.Default.Log(msg, Logger.LogType.CONSOLE_LOC_FILE);
							return true;
						}
						//errore
						Logger.Default.Log("WMIFunction.SetGateway: fallita (error code=" + retValue + ")", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR);
						break;
					}
					catch (Exception ex)
					{
						Logger.Default.Log("WMIFunction.SetGateway: fallita (" + ex.Message + ")", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
							Logger.E_STACK_TRACE);
						break;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.SetGateway': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
			return false;
		}

		/// <summary>
		///     Imposta DNS (Domain Name System)
		/// </summary>
		/// <remarks>
		///     Configura indirizzi DNS, se lista vuota azzera eventuali DNS configurati.
		/// </remarks>
		/// <param name="DevID">Identificativo device.</param>
		/// <param name="DNS">Lista indirizzi DNS da configurare.</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: impostazione eseguita correttamente.</item>
		///         <item>false: impostazione fallita.</item>
		///     </list>
		/// </returns>
		public bool SetDNS(UInt32 DevID, List<IPAddress> DNS)
		{
			//http://msdn.microsoft.com/en-us/library/windows/desktop/aa394217%28v=vs.85%29.aspx
			try
			{
				ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection objMOC = objMC.GetInstances();

				foreach (ManagementObject objMO in objMOC)
				{
					if (((UInt32) objMO["Index"]) != DevID)
					{
						continue;
					}

					try
					{
						ManagementBaseObject objDNS = null;
						ManagementBaseObject objReturn = null;

						objDNS = objMO.GetMethodParameters("SetDNSServerSearchOrder");

						//Set DefaultGateway
						string[] dnsIP = new string[DNS.Count];

						for (int i = 0; i < DNS.Count; i++)
						{
							dnsIP[i] = DNS[i].ToString();
						}

						objDNS["DNSServerSearchOrder"] = dnsIP;

						objReturn = objMO.InvokeMethod("SetDNSServerSearchOrder", objDNS, null);

						UInt32 retValue = (UInt32) objReturn["returnValue"];

						if (retValue <= 1)
						{
							string msg = "WMIFunction.SetDNS: ip=";

							foreach (string item in dnsIP)
							{
								msg = msg + item + ",";
							}

							Logger.Default.Log(msg, Logger.LogType.CONSOLE_LOC_FILE);
							return true;
						}
						//errore
						Logger.Default.Log("WMIFunction.SetDNS: fallita (error code=" + retValue + ")", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR);
						break;
					}
					catch (Exception ex)
					{
						Logger.Default.Log("WMIFunction.SetDNS: fallita (" + ex.Message + ")", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
							Logger.E_STACK_TRACE);
						break;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.SetGateway': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
			return false;
		}

		/// <summary>
		///     Carica parametri della scheda di rete.
		/// </summary>
		/// <remarks>
		///     Carica membri della classe con i parametri di configurazione scheda di rete.
		/// </remarks>
		/// <param name="network">Oggetto di definizione configurazione porta</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: lettura parametri eseguita correttamente.</item>
		///         <item>false: lettura fallita.</item>
		///     </list>
		/// </returns>
		public bool GetNetworkConfig(ref STLCNetwork network)
		{
			try
			{
				ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection objMOC = objMC.GetInstances();

				foreach (ManagementObject objMO in objMOC)
				{
					if (((UInt32) objMO["Index"]) != network.DevId)
					{
						continue;
					}

					network.IPEnabled = (bool) objMO["IPEnabled"];

					if (network.IPEnabled)
					{
						network.DHCPEnabled = (bool) objMO["DHCPEnabled"];

						string[] ipaddresses = (string[]) objMO["IPAddress"];
						string[] subnets = (string[]) objMO["IPSubnet"];
						string[] gateways = (string[]) objMO["DefaultIPGateway"];
						string[] dnsservers = (string[]) objMO["DNSServerSearchOrder"];
						UInt16[] costmetrics = (UInt16[]) objMO["GatewayCostMetric"];

						foreach (string str in ipaddresses ?? new string[0])
						{
							IPAddress ip = IPAddress.Parse(str);
							network.IPAddresses.Add(ip);
						}

						foreach (string str in subnets ?? new string[0])
						{
							if (!string.IsNullOrEmpty(str))
							{
								IPAddress ip = IPAddress.Parse(str);
								network.SubnetMasks.Add(ip);
							}
						}

						foreach (string str in gateways ?? new string[0])
						{
							IPAddress ip = IPAddress.Parse(str);
							network.IPGateways.Add(ip);
						}

						foreach (string str in dnsservers ?? new string[0])
						{
							IPAddress ip = IPAddress.Parse(str);
							network.DNSServers.Add(ip);
						}

						foreach (UInt16 metric in costmetrics ?? new UInt16[0])
						{
							network.Metrics.Add(metric);
						}

#if (DEBUG)
						Logger.Default.Log("Network: " + network.Name, Logger.LogType.CONSOLE);
						Logger.Default.Log(" dhcpenabled=" + network.DHCPEnabled, Logger.LogType.CONSOLE);

						foreach (IPAddress ip in network.IPAddresses)
						{
							Logger.Default.Log(" ip=" + ip, Logger.LogType.CONSOLE);
						}

						foreach (IPAddress ip in network.SubnetMasks)
						{
							Logger.Default.Log(" subnet=" + ip, Logger.LogType.CONSOLE);
						}

						foreach (IPAddress ip in network.IPGateways)
						{
							Logger.Default.Log(" gateway=" + ip, Logger.LogType.CONSOLE);
						}

						foreach (UInt16 metric in network.Metrics)
						{
							Logger.Default.Log(" metric=" + metric, Logger.LogType.CONSOLE);
						}

						foreach (IPAddress ip in network.DNSServers)
						{
							Logger.Default.Log(" dns=" + ip, Logger.LogType.CONSOLE);
						}
#endif
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'WMIFunction.GetNetworkConfig': " + ex.GetType().ToString() + " - " + ex.StackTrace,
					Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
			return false;
		}

		#endregion
	}
}
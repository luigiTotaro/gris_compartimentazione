﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using STLCManager.Service.Database;
using STLCManager.Service.Properties;
using Timer = System.Timers.Timer;
using STLCManager.Service.Function;

namespace STLCManager.Service.Function
{
    /// <summary>
    ///     Classe di verifica stato di funzionamento servizio
    /// </summary>
    public class ServiceControl
    {
        #region Token

        private const int STOP_SERVICE_TIMEOUT  = 10*1000;  // timeout di attesa stop del servizio

        private const int MAX_BLOCKED_SERVICE   = 3;        // numero di volte senza variazione della chiave di heartbeat trascorse le quali il servizio viene riavviato

        // Possibili stato di verifica servizio
        public enum Mode
        {
            DISABLED, // servizio non sottoposto a verifica
            RUNNING,  // servizio in stato di running
            WARNING,  // warning: servizio non operativo, tentativi di rispristino in corso
            WAITING,  // servizio in attesa di start (startup_delay)
            WAITING_NOMON,  // servizio in attesa di start (startup_delay) monitoraggio attivo
            WAITING_STOPPED, // avvenuta richiesta di stop mentre il servizio è in attesa di start (startup_delay) 
            FAILURE   // errore: impossibile riattivare il servizio
        };

        #endregion

        #region Members

        public string Name { get; private set; }            // nome servizio
        public string Group { get; private set; }           // gruppo di appartenenza
        public string ProcessName { get; private set; }     // Nome del processo associato al servizio

        public DateTime dtLastCheck { get; private set; }

        public int nItemBlocked { get; private set; } // Contatore del numero di volte che la chiave di registro gestita dal supervisore non si è aggiornata

        public uint RetryNumber { get; private set; } // numero di tentativi di ripristino servizio

        private uint retryDelay; // attesa su comando di riattivazione servizio
        private uint checkRunningTimeout; // timeout di schedulazione stato servizio
        private uint checkHeartBeatTimeout; // timeout di schedulazione segnale di vita da parte del servizio
        private uint forcedRestartTimeout; // periodicità di riavvio forzato del servizio
        private uint startupDelay; // tempo di ritardo per lo start del servizio

        private Mode mode; // stato di controllo servizio
        private int retryCount; // contatore tentativi di riattivazione in corso

        private DateTime checkRunningTime; // orario di scadenza verifica stato servizio
        private DateTime checkHeartBeatTime; // orario di scadenza verifica segnale di vita servizo
        private DateTime forcedRestartTime; // orario di scadenza riavvio forzato del servizio

        private Thread restartThread; // thread di gestione fasi di riattivazione servizio

        #endregion

        #region Accessor

        /// <summary>
        ///     Restituisce/imposta tempo di attesa riattivazione servizio (secondi)
        /// </summary>
        public uint RetryDelay
        {
            get { return this.retryDelay/1000; }
            private set { this.retryDelay = value*1000; }
        }

        /// <summary>
        ///     Restituisce/imposta periodicità verifica stato servizio (secondi)
        /// </summary>
        public uint CheckRunningTimeout
        {
            get { return this.checkRunningTimeout/1000; }
            private set { this.checkRunningTimeout = value*1000; }
        }

        /// <summary>
        ///     Restituisce/imposta periodicità verifica segnale di vita del servizio (secondi)
        /// </summary>
        public uint CheckHeartBeatTimeout
        {
            get { return this.checkHeartBeatTimeout/1000; }
            private set { this.checkHeartBeatTimeout = value*1000; }
        }

        /// <summary>
        ///     Restituisce/imposta periodicità riavvio forzato del servizio (ore)
        /// </summary>
        public uint ForcedRestartTimeout
        {
            get { return this.forcedRestartTimeout/(60*60*1000); }
            private set { this.forcedRestartTimeout = value*(60*60*1000); }
        }

        /// <summary>
        ///     Restituisce/imposta il tempo di ritardo per lo start del servizio (secondi)
        /// </summary>
        public uint StartupDelay
        {
            get { return this.startupDelay / 1000; }
            private set { this.startupDelay = value * 1000; }
        }

        /// <summary>
        ///     Identifica se il servizio è in errore
        /// </summary>
        public bool IsFailure
        {
            get { return this.mode == Mode.FAILURE; }
        }

        /// <summary>
        ///     Identifica se il servizio è in warning, servizio non operativo e tentativi di ripristino in corso
        /// </summary>
        public bool IsWarning
        {
            get { return this.mode == Mode.WARNING; }
        }

        /// <summary>
        ///     Indica se il servizio deve essere automaticamente fermato allo stop del servizio Manager
        /// </summary>
        public bool StopOnManagerStopped { get; set; }

        /// <summary>
        ///     Id del supervisore di monitoraggio associato al servizio, se esistente
        /// </summary>
        public int SupervisorId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Inizializza una nuova istanza della classe ServiceControl per la verifica del funzionamento del servizio.
        ///     Imposta tutti i parametri al valore di default
        /// </summary>
        public ServiceControl()
        {
            this.Name           = null;
            this.Group          = null;
            this.ProcessName    = null;

            this.RetryNumber = 3;
            this.RetryDelay = 30;

            this.CheckRunningTimeout = 10*60;
            this.CheckHeartBeatTimeout = 0;
            this.ForcedRestartTimeout = 0;
            this.StartupDelay = 0;

            this.mode = Mode.DISABLED;

            this.nItemBlocked = 0;

            this.dtLastCheck = DateTime.MinValue;

            this.SupervisorId = SupervisorUtility.UNKNOWN_SUPERVISOR_ID;
        }

        /// <summary>
        ///     Inizializza una nuova istanza della classe ServiceControl per la verifica del funzionamento del servizio.
        ///     Imposta i parametri generali di identificazione servizio.
        /// </summary>
        /// <param name="sName">nome servizio</param>
        /// <param name="sGroup">gruppo di appartenenza</param>
        public ServiceControl(string sName, string sGroup, string sProcessName) : this()
        {
            if (String.IsNullOrEmpty(sName))
            {
                throw new ArgumentException("Nome servizio non valido");
            }
            this.Name = sName;

            if (String.IsNullOrEmpty(sGroup))
            {
                this.Group = "";
            }
            else
            {
                this.Group = sGroup;
            }

            if(String.IsNullOrEmpty(sProcessName))
            {
                this.ProcessName = "";
            }
            else
            {
                this.ProcessName = sProcessName;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Imposta parametri di gestione rispristino servizio
        /// </summary>
        /// <param name="sNumber">numero di tentativi da effettuare per la riattivazione del servizio</param>
        /// <param name="sDelay">attesa riattivazione servizio</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: parametri settati correttamente</item>
        ///         <item>false: errore, parametri non validi</item>
        ///     </list>
        /// </returns>
        public bool SetRetry(string sNumber, string sDelay)
        {
            try
            {
                if (!String.IsNullOrEmpty(sNumber))
                {
                    uint uNumber = uint.Parse(sNumber);
                    this.RetryNumber = uNumber;
                }

                if (!String.IsNullOrEmpty(sDelay))
                {
                    uint uDelay = uint.Parse(sDelay);
                    this.RetryDelay = uDelay;
                }
            }
            catch
            {
                return false;
            }
            return true;

        }

        /// <summary>
        ///     Imposta timeout di verifica stato servizio
        /// </summary>
        /// <param name="sRunning">periodicità verifica stato servizio</param>
        /// <param name="sHeartBeat">periodicità verifica segnale di vita del servizio</param>
        /// <param name="sRestart">periodicità riavvio forzato del servizio</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: parametri settati correttamente</item>
        ///         <item>false: errore, parametri non validi</item>
        ///     </list>
        /// </returns>
        public bool SetTimeout(string sRunning, string sHeartBeat, string sRestart, string sStartupDelay)
        {
            try
            {
                uint uRunning = this.CheckRunningTimeout;
                uint uHeartBeat = this.CheckHeartBeatTimeout;
                uint uRestart = this.ForcedRestartTimeout;
                uint uStartupDelay = this.StartupDelay;

                if (!String.IsNullOrEmpty(sRunning))
                {
                    uRunning = uint.Parse(sRunning);
                }

                if (!String.IsNullOrEmpty(sHeartBeat))
                {
                    uHeartBeat = uint.Parse(sHeartBeat);
                }

                if (!String.IsNullOrEmpty(sRestart))
                {
                    uRestart = uint.Parse(sRestart);
                }

                if (!String.IsNullOrEmpty(sStartupDelay))
                {
                    uStartupDelay = uint.Parse(sStartupDelay);
                }

                //la condizione di tutti i timeouts a zero non è accettata
                if (uRunning == 0 && uHeartBeat == 0 && uRestart == 0)
                {
                    return false;
                }

                this.CheckRunningTimeout = uRunning;
                this.CheckHeartBeatTimeout = uHeartBeat;
                this.ForcedRestartTimeout = uRestart;
                this.StartupDelay = uStartupDelay;
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Abilita funzione di controllo servizio
        /// </summary>
        public void EnableChecking()
        {
            if (this.mode == Mode.WAITING_NOMON)
                this.mode = Mode.WAITING;
            else
                this.mode = Mode.RUNNING;
            this.retryCount = (int) this.RetryNumber;

            this.checkRunningTime = DateTime.Now.AddMilliseconds(this.checkRunningTimeout);
            this.checkHeartBeatTime = DateTime.Now.AddMilliseconds(this.checkHeartBeatTimeout);
            this.forcedRestartTime = DateTime.Now.AddMilliseconds(this.forcedRestartTimeout);
        }

        /// <summary>
        ///     Disattiva funzione di controllo servizio
        /// </summary>
        public void DisableChecking()
        {
            try
            {
                if (this.restartThread != null && this.restartThread.IsAlive)
                {
                    this.restartThread.Abort();
                }

                this.mode = Mode.DISABLED;
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'ServiceControl.DisableChecking': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
        }

        /// <summary>
        ///     Visualizza informazioni di debug del servizio
        /// </summary>
        public void Debug()
        {
            Logger.Default.Log(this.Name, Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" mode=" + this.mode, Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" group=" + this.Group, Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" retry=" + this.RetryNumber, Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" retry_delay=" + this.RetryDelay + " sec", Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" check_running_timeout=" + this.CheckRunningTimeout + " sec", Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" check_heartbeat_timeout=" + this.CheckHeartBeatTimeout + " sec", Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" forced_restart_timeout=" + this.ForcedRestartTimeout + " ore", Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" stop_on_manager_stopped=" + this.StopOnManagerStopped, Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(" startup_delay=" + this.StartupDelay, Logger.LogType.CONSOLE_LOC_FILE);

            if (this.SupervisorId != SupervisorUtility.UNKNOWN_SUPERVISOR_ID)
            {
                Logger.Default.Log(" supervisor_id=" + this.SupervisorId.ToString() + " (" + SupervisorUtility.DecodeSupervisorID(this.SupervisorId) + ")", Logger.LogType.CONSOLE_LOC_FILE);
            }
        }

        /// <summary>
        ///     Funzione di controllo stato del servizio
        /// </summary>
        public void Checking()
        {
 //           Logger.Default.Log("Checking(): " + this.Name + " Mode = " + this.mode, Logger.LogType.CONSOLE_LOC_FILE);

            switch (this.mode)
            {
                case Mode.DISABLED:
                case Mode.WAITING:
                case Mode.WAITING_NOMON:
                    break;

                case Mode.RUNNING:
                    if (this.TimeExpired(this.checkRunningTime, this.checkRunningTimeout))
                    {
                        this.checkRunningTime = DateTime.Now;

                        switch (ServiceFunction.Default.GetServiceStatus(this.Name, this.ProcessName))
                        {
                            case ServiceControllerStatus.Running:
                                this.checkRunningTime = this.checkRunningTime.AddMilliseconds(this.checkRunningTimeout);
                                break;

                            case ServiceControllerStatus.Stopped:
                                Logger.Default.Log("Errore: servizio '" + this.Name + "' in stop.", Logger.LogType.CONSOLE_LOC_FILE,
                                    Logger.EventType.ERROR, AppCfg.E_COD_SERVICE_STOPPED);
                                this.mode = this.StartThread() ? Mode.WARNING : Mode.FAILURE;
                                break;

                            case ServiceControllerStatus.Paused:
                                ServiceFunction.Default.StopService(this.Name);
                                break;
                        }
                    }

                    if (this.TimeExpired(this.checkHeartBeatTime, this.checkHeartBeatTimeout))
                    {
                        this.checkHeartBeatTime = DateTime.Now.AddMilliseconds(this.checkHeartBeatTimeout);

                        DateTime dtTmp;

                        if (ServiceFunction.Default.CheckServiceIsBlocked(this.Name, this.dtLastCheck, out dtTmp))
                        {
                            this.nItemBlocked++;

                            if (this.nItemBlocked > MAX_BLOCKED_SERVICE)
                            { 
                                Logger.Default.Log("Errore: servizio '" + this.Name + "' bloccato (segnale di vita assente).",
                                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_COD_SERVICE_BLOCKED);
                                this.mode = this.StartThread() ? Mode.WARNING : Mode.FAILURE;

                                this.nItemBlocked = 0;
                            }
                        }
                        else
                            this.nItemBlocked = 0; 

                        this.dtLastCheck = dtTmp;
                    }

                    if (this.TimeExpired(this.forcedRestartTime, this.forcedRestartTimeout))
                    {
                        Logger.Default.Log("Riavvio forzato servizio '" + this.Name + "'.", Logger.LogType.CONSOLE_LOC_FILE,
                            Logger.EventType.WARNING, AppCfg.I_COD_SERVICE_FORCED_RESTART);
                        this.forcedRestartTime = DateTime.Now.AddMilliseconds(this.forcedRestartTimeout);
                        this.mode = this.StartThread() ? Mode.WARNING : Mode.FAILURE;
                    }
                    break;

                case Mode.WARNING:
                    if ((this.restartThread != null) && !(this.restartThread.IsAlive))
                    {
                        //thread adibito alla riattivazione del servizo terminato
                        if (this.retryCount <= 0)
                        {
                            //errore: impossibile riattivare il servizio
                            Logger.Default.Log("Errore: riattivazione servizio '" + this.Name + "' fallita.", Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.ERROR, AppCfg.E_COD_SERVICE_RESTART_FAILURE);
                            this.mode = Mode.FAILURE;
                        }
                        else
                        {
                            //servizio riavviato: riattiva schedulazione stato servizio
                            this.checkRunningTime = DateTime.Now;
                            this.checkHeartBeatTime = DateTime.Now.AddMilliseconds(this.checkHeartBeatTimeout);
                            this.forcedRestartTime = DateTime.Now.AddMilliseconds(this.forcedRestartTimeout);
                            this.mode = Mode.RUNNING;
                        }
                    }
                    break;

                case Mode.FAILURE:
                    break;
            }
        }

        /// <summary>
        ///     Segnala la richiesta di arresto il mode per i servizi che sono in sleep per lo startup_delay.
        /// </summary>
        public void SetServiceWaitingStop()
        {
            if ((mode == Mode.WAITING) || (mode == Mode.WAITING_NOMON))
                mode = Mode.WAITING_STOPPED;
            return;
        }


        /// <summary>
        ///     Verifica se è trascorso il tempo indicato dal parametro time
        /// </summary>
        /// <param name="time">data/ora da verificare</param>
        /// <param name="timeout">timeout di verifica</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: tempo scaduto (time minore di now)</item>
        ///         <item>false: tempo non scaduto o timeout non utilizzato (zero)</item>
        ///     </list>
        /// </returns>
        private bool TimeExpired(DateTime time, uint timeout)
        {
            if (timeout != 0)
            {
                int timeElapsed = (int) time.Subtract(DateTime.Now).TotalMilliseconds;

                if (timeElapsed < 0)
                {
                    // tempo scaduto
                    return true;
                }

                if (timeout < timeElapsed)
                {
                    // l'orologio di sistema è stato settato, il timeout supera il tempo di schedulazione impostato, forza TimeExpired a true
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Attivazione thread per la gestione delle fasi di riattivazione servizio
        /// </summary>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: thread attivato</item>
        ///         <item>false: errore durante l'attivazione del thread</item>
        ///     </list>
        /// </returns>
        public bool StartThread()
        {
            try
            {
                //imposta numero di tentativi di riavviamento del servizio
                this.retryCount = (int) this.RetryNumber;

                //attiva thread adibito alla riattivazione del servizio
                this.restartThread = new Thread(this.RestartService);
                this.restartThread.Start();

                //attende che il thread sia attivo
                while (!this.restartThread.IsAlive)
                {
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'ServiceControl.StartThread': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Procedura di gestione riattivazione del servizio.
        /// </summary>
        private void RestartService()
        {
            try
            {
                //comanda eventuale stop del servizio
                ServiceControllerStatus status = ServiceFunction.Default.GetServiceStatus(this.Name, this.ProcessName);
                if (status != ServiceControllerStatus.Stopped)
                {
                    if (status == ServiceControllerStatus.StartPending)
                    {   // caso di ripartenza dopo uno stop non ancora conclusa
                        Logger.Default.Log("Servizio '" + this.Name + "' in start pending.", Logger.LogType.CONSOLE_LOC_FILE);
                        return;
                    }

                    Logger.Default.Log("Stop servizio: " + this.Name, Logger.LogType.CONSOLE_LOC_FILE);
                    while (ServiceFunction.Default.StopService(this.Name, STOP_SERVICE_TIMEOUT) != AppCfg.COD_NO_ERROR)
                    {
                        if (--this.retryCount <= 0)
                        {
                            // Numero max di tentativi superato comando fallito
                            Logger.Default.Log("Stop servizio '" + this.Name + "' fallito.", Logger.LogType.CONSOLE_LOC_FILE);
                            return;
                        }
                        else
                        {  // attendo per il retry
                            Thread.Sleep(TimeSpan.FromMilliseconds(this.retryDelay));
                        }
                    }
                }
                // Verifica se al servizio arrestato corrisponda l'assenza del relativo processo
                ServiceFunction.Default.CheckServiceIsStopped(this.Name, this.ProcessName, this.mode);

                //comanda start del servizio
                Logger.Default.Log("Start servizio : " + this.Name + " (retry= " + this.retryCount + ", retryDelay= " + this.retryDelay/1000 + " sec, startupDelay= " + this.startupDelay/1000 + ", Previous mode= " + this.mode + ")",
                    Logger.LogType.CONSOLE_LOC_FILE);
                while (ServiceFunction.Default.StartService(this.Name, (int)this.retryDelay, this.startupDelay, ref this.mode) != AppCfg.COD_NO_ERROR)
                {
                    if (--this.retryCount <= 0)
                    {
                        // Numero max di tentativi superato comando fallito
                        Logger.Default.Log("Start servizio '" + this.Name + "' fallito.", Logger.LogType.CONSOLE_LOC_FILE);
                        return;
                    }
                    else
                    {   // attendo per il retry
                        Thread.Sleep(TimeSpan.FromMilliseconds(this.retryDelay));
                    }
                }

                //servizio riavviato correttamente
                this.retryCount = (int)this.RetryNumber;
            }

            catch (ThreadAbortException ex)
            {
                //ECCEZIONE dovuta allo stop del thread sospeso sulla sleep() NON va considerata.

//                Logger.Default.Log("ECCEZIONE ThreadAbortException GESTITA in ServiceControl.RestartService(" + this.Name + ")': " + ex.GetType() + " - " + ex.ToString(),
//                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                this.retryCount = 0;
            }

            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'ServiceControl.RestartService(" + this.Name + ")': " + ex.GetType() + " - " + ex.ToString(),
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                this.retryCount = 0;
            }
        }

        #endregion
    }

    /// <summary>
    ///     Classe di gestione lista servizi da controllare
    /// </summary>
    public class SystemServices
    {
        #region Token

        private const int SERVICES_TIME_SCHEDULING = 5*1000; // timeout di schedulazione servizi 5 sec
        private const int CHECK_DB_INTERVAL = 5; // timeout di schedulazione controllo stato del DB 5 min
        #endregion

        #region Members

        private readonly List<ServiceControl> ServicesNormalList; // lista servizi da controllare
        private readonly List<ServiceControl> ServicesReverseList; // lista inversa servizi, utilizzata per la disattivazione dei servizi
        private ReadOnlyCollection<int> NeededSupervisors; // lista di IDSupervisori che sono necessari per il monitoraggio delle periferiche

        public bool IsEnabled { get; private set; } // thread di controllo stato servizi attivo
        public bool IsFailure { get; private set; } // uno o più servizi in errore
        public bool IsWarning { get; private set; } // uno o più servizi in preallarme
        private bool isRunning; // thread di controllo servizi in funzione (segnala avvenuto ciclo di schedulazione)
        public bool IsStopping { get; set; } // uno o più servizi in preallarme

        private Timer timerManageServices; // thread di gestione servizi
        private DateTime lastDbCheck;

        #endregion

        #region Contructors

        /// <summary>
        ///     Inizializza una nuova istanza della classe SystemServices per la verifica del funzionamento dei servizi.
        /// </summary>
        public SystemServices()
        {
            this.ServicesNormalList = new List<ServiceControl>();
            this.ServicesReverseList = new List<ServiceControl>();
            this.NeededSupervisors = null;

            this.IsEnabled = false;
            this.IsFailure = false;
            this.IsWarning = false;
            this.isRunning = false;

            this.timerManageServices = null;
            this.lastDbCheck = DateTime.Now;
        }

        #endregion

        #region Accessor

        /// <summary>
        ///     Restituisce la periodicità del thread di controllo servizi
        /// </summary>
        public uint ThreadPeriod
        {
            get { return SERVICES_TIME_SCHEDULING; }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Inserisce il servizio nella lista di controllo
        /// </summary>
        /// <param name="service"></param>
        public void ServiceUnderControl(ServiceControl service)
        {
            this.ServicesNormalList.Add(service);
            this.ServicesReverseList.Insert(0, service);
        }

        /// <summary>
        ///     Abilita thread di controllo servizi
        /// </summary>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: gestione controllo servizi abilitata</item>
        ///         <item>false: errore, impossibile abilitare gestione controllo servizi</item>
        ///     </list>
        /// </returns>
        public bool Enable()
        {
            if (!this.IsEnabled)
            {
                try
                {
                    this.isRunning = false;

                    foreach (ServiceControl service in this.GetAllowedServices(this.ServicesNormalList, false))
                    {
                        service.EnableChecking();
                    }

                    this.timerManageServices = new Timer(SERVICES_TIME_SCHEDULING);
                    this.timerManageServices.Elapsed += this.ManageServices;
                    this.timerManageServices.AutoReset = true;
                    this.timerManageServices.Enabled = true;
                    this.timerManageServices.Start();

                    this.IsEnabled = true;

                    Logger.Default.Log("Attivata gestione SystemServices.", Logger.LogType.CONSOLE_LOC_FILE);
                }
                catch (Exception ex)
                {
                    Logger.Default.Log("ECCEZIONE in 'SystemServices.Enable': " + ex.GetType() + " - " + ex.StackTrace,
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                }
            }

            return this.IsEnabled;
        }

        /// <summary>
        ///     Disabilita thread di controllo servizi
        /// </summary>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: gestione controllo servizi disabilitata</item>
        ///     </list>
        /// </returns>
        public bool Disable()
        {
            if (this.IsEnabled)
            {
                this.timerManageServices.Stop();
                this.timerManageServices.Dispose();
                this.timerManageServices = null;
                this.IsEnabled = false;

                foreach (ServiceControl service in this.GetAllowedServices(this.ServicesNormalList, false))
                {
                    service.DisableChecking();
                }

                Logger.Default.Log("Disattivata gestione SystemServices.", Logger.LogType.CONSOLE_LOC_FILE);
            }
            return !this.IsEnabled;
        }

        /// <summary>
        ///     Visualizza informazioni di debug di tutti i servizi presenti nella lista
        /// </summary>
        public void Debug()
        {
            Logger.Default.Log("GESTIONE SERVIZI:", Logger.LogType.CONSOLE);

            foreach (ServiceControl service in this.GetAllowedServices(this.ServicesNormalList, false))
            {
                service.Debug();
                Logger.Default.Log("", Logger.LogType.CONSOLE);
            }
        }

        /// <summary>
        ///     Restituisce il flag di running (processo in eseguzione corretta) e lo azzera
        /// </summary>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: avvenuto ciclo di schedulazione dopo ultima chiamata</item>
        ///         <item>false: nessuna schedulazione avvenuta</item>
        ///     </list>
        /// </returns>
        public bool IsRunning()
        {
            if (this.isRunning)
            {
                this.isRunning = false;
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Funzione chiamata dal thread periodico. Esegue controllo stato servizi.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManageServices(object sender, ElapsedEventArgs e)
        {
            try
            {
                this.timerManageServices.Enabled = false;

                this.isRunning = true;

                bool bFailure = false;
                bool bWarning = false;
                bool goStop = false;

                if (AppCfg.Default.bOnCheckDB)
                {
                    if (this.lastDbCheck.AddMinutes(CHECK_DB_INTERVAL) < DateTime.Now)
                    { // controllo dello stato del DB ogni 5 minuti
                        this.lastDbCheck = DateTime.Now;
                        if (!CheckSqlExpressServiceActive(false))
                            goStop = true;
                        if (!CheckDbActive(false))
                            goStop = true;
                        if (goStop)
                        { //DB indisponibile: stop STLCManager service
                            ServiceController sc = new ServiceController("STLCManagerService");
                            sc.Stop();
                            return;
                        }
                    }
                }

                foreach (ServiceControl service in this.GetAllowedServices(this.ServicesNormalList, false))
                {
                    service.Checking();

                    if (service.IsFailure)
                    {
                        bFailure = true;
                    }

                    if (service.IsWarning)
                    {
                        bWarning = true;
                    }
                }

                this.IsFailure = bFailure;
                this.IsWarning = bWarning;

                this.timerManageServices.Enabled = true;
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.ManageServices': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                this.Disable();
            }
        }

        private List<ServiceControl> GetAllowedServices(List<ServiceControl> listServices, bool enableLog)
        {
            List<ServiceControl> listAllowedServices = new List<ServiceControl>();

            if (AppCfg.Default.bStartOnlyNeededSupervisorServices)
            {
                if (this.NeededSupervisors == null)
                {
                    this.NeededSupervisors = SystemXmlFunction.Default.GetNeededSupervisorList(enableLog);
                }

                foreach (ServiceControl service in listServices)
                {
                    if ((service.SupervisorId == SupervisorUtility.UNKNOWN_SUPERVISOR_ID) || (this.NeededSupervisors.Contains(service.SupervisorId)))
                    {
                        // Aggiungiamo alla lista dei servizi da avviare solamente quelli non associati ad un supervisore o nella lista dei supervisori necessari
                        listAllowedServices.Add(service);
                    }
                    else
                    {
                        if (enableLog)
                        {
                            Logger.Default.Log(
                                String.Format(
                                    "Richiesto comando su servizio: {0}, che però è un Supervisore non necessario al monitoraggio. Esecuzione comando ignorata.",
                                    service.Name), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION,
                                AppCfg.I_SYSTEM_XML_SUPERVISOR_NOT_NEEDED_IN_MONITORING);
                        }
                    }
                }
            }
            else
            {
                listAllowedServices = listServices;
            }

            return listAllowedServices;
        }

        /// <summary>
        ///     Attiva servizi indicati dalla lista
        /// </summary>
        /// <param name="listServices">lista servizi da attivare</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item>true: tutti i servizi indicati sono attivi</item>
        ///         <item>false: errore, uno o più servizi non sono in running</item>
        ///     </list>
        /// </returns>
        private bool Start(List<ServiceControl> listServices, bool writeOnDatabase)
        {
            try
            {
                AppCfg.Default.OnFileLogAllCfg();

                if (AppCfg.Default.bOnCheckDB == true)
                {
                    //check attivazione del servizio MSSQL$EXPRESS
                    if (!CheckSqlExpressServiceActive(true))
                    {
                        Logger.Default.Log("Servizio MSSQL$EXPRESS non attivabile. Monitoraggio non attivato !",
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_COD_SERVICE_START_FAILURE);
                        return false;
                    }
                    //check della connewssione al DB 
                    if (!CheckDbActive(true))
                    {
                        Logger.Default.Log("DataBase indisponibile. Monitoraggio non attivato !",
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_COD_SERVICE_START_FAILURE);
                        return false; // se non attivo non faccio lo start del monitoraggio
                    }
                }
                else
                {
                    Logger.Default.Log("OnCheckDB=" + AppCfg.Default.bOnCheckDB.ToString() + " - non controllo l'accesso al DB", 
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);
                }

                List<ServiceControl> listAllowedServices = null;

                if (AppCfg.Default.bDownloadDeviceTypeListAndUpdateDatabaseOnStart)
                {
                    CentralWSFunction.Default.DownloadDeviceTypeListAndUpdateDatabase();                    
                }

                if (AppCfg.Default.bDownloadRegionListDataAndUpdateDatabaseOnStart)
                {
                    CentralWSFunction.Default.DownloadRegionListDataAndUpdateDatabase();
                }

                if (listServices == null || listServices.Count == 0)
                {
                    if (Settings.Default.ParseServerXmlConfig)
                    {
                        Logger.Default.Log(
                            "Richiesto caricamento dati periferiche da System.xml, ma nessun servizio configurato. Operazione ignorata.",
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.WARNING, AppCfg.W_SYSTEM_XML_NO_SERVICES_CONFIGURED);
                    }

                    return false;
                }
                else if ((listServices.Count != 1) || (listServices[0].Name != "StlcSCAgentService"))
                {
                    if (Settings.Default.ParseServerXmlConfig)
                    {
                        Logger.Default.Log("Richiesto caricamento dati periferiche da System.xml", Logger.LogType.CONSOLE_LOC_FILE, //notice
                            Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                        SystemXmlFunction.Default.ParseSystemXmlOnDatabase(writeOnDatabase, false);

                        this.NeededSupervisors = SystemXmlFunction.Default.GetNeededSupervisorList(false);
                    }

                    listAllowedServices = this.GetAllowedServices(listServices, false);

                }
                else
                {    // Caso in cui la lista contenga solo il servizio SCAgent: non faccio la ricerca dei servizi necessari perchè sarebbe vuota.
                    listAllowedServices = listServices;
                }

                if (this.IsStopping == true) // A fronte di una richista di stop, non faccio partire i servizi.
                {
                    return true;
                }

                // comanda start dei servizi non attivi
                foreach (ServiceControl service in listAllowedServices)
                {
                    ServiceControllerStatus st = ServiceFunction.Default.GetServiceStatus(service.Name, service.ProcessName);
                    Logger.Default.Log("Check stato del servizio: " + service.Name + " - " + service.ProcessName + " stato: " + st.ToString(), Logger.LogType.CONSOLE_LOC_FILE, //notice
                                Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);
                    if ((service.Group == "core") || (st != ServiceControllerStatus.Running && st != ServiceControllerStatus.StartPending))
                    { // start del servizio su un thread dedicato (forza sempre il restart per i servizi del gruppo "core")
                        service.StartThread();
                    }
                }

                // attende attivazione dei servizi
                int count = AppCfg.Default.nServiceWaitForStart;
                while (count-- >= 0)
                {
                    Thread.Sleep(1000);

                    if (CheckStatus(listAllowedServices, ServiceControllerStatus.Running, (count==0)?true:false))
                    {
                        Logger.Default.Log("Tutti i servizi sono nello stato RUNNING !", Logger.LogType.CONSOLE_LOC_FILE, //notice
                            Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                        return true;
                    }
                }
                Logger.Default.Log("Non tutti i servizi sono nello stato RUNNING !", Logger.LogType.CONSOLE_LOC_FILE, //notice
                    Logger.EventType.WARNING, AppCfg.I_SYSTEM_XML_NO_ERROR);
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.Start': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Attiva tutti i servizi configurati
        /// </summary>
        /// <returns>vedi Start(List)</returns>
        public bool Start(bool writeOnDatabase)
        {
            return this.Start(this.ServicesNormalList, writeOnDatabase);
        }

        /// <summary>
        ///     Attiva tutti i servizi del gruppo indicato
        /// </summary>
        /// <param name="sGroup">identificativo gruppo</param>
        /// <returns>vedi Start(List)</returns>
        public bool Start(string sGroup)
        {
            try
            {
                if (String.IsNullOrEmpty(sGroup))
                {
                    return this.Start(this.ServicesNormalList, false);
                }

                List<ServiceControl> listServices =
                    this.ServicesNormalList.FindAll(delegate(ServiceControl service) { return String.Compare(service.Group, sGroup, true) == 0; });

                return Start(listServices, false);
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.Start(group)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Attiva la lista dei servizi identificati per nome
        /// </summary>
        /// <param name="servicesName">lista nome servizi da attivare</param>
        /// <returns>vedi Start(List)</returns>
        public bool Start(List<string> servicesName)
        {
            try
            {
                if (servicesName == null || servicesName.Count == 0)
                {
                    return false;
                }

                List<ServiceControl> listServices = this.ServicesNormalList.FindAll(delegate(ServiceControl service)
                {
                    foreach (string name in servicesName)
                    {
                        if (!String.IsNullOrEmpty(name) && String.Compare(name, service.Name, true) == 0)
                        {
                            return true;
                        }
                    }
                    return false;
                });

                return Start(listServices, false);
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.Start(servicesName)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Disattiva tutti i servizi indicati dalla lista
        /// </summary>
        /// <param name="listServices">lista servizi da disattivare</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: tutti i servizi indicati sono disattivi</item>
        ///         <item>false: errore, uno o più servizi non sono in stop</item>
        ///     </list>
        /// </returns>
        private bool Stop(List<ServiceControl> listServices)
        {
            try
            {
                if (listServices == null || listServices.Count == 0)
                {
                    return false;
                }

                List<ServiceControl> listAllowedServices = listServices;

                // comanda stop dei servizi attivi
                foreach (ServiceControl service in listAllowedServices)
                {
                    service.SetServiceWaitingStop(); // Segnala la richiesta di arresto ai servizi in attesa dello scadere dello startup_delay

                    ServiceControllerStatus st = ServiceFunction.Default.GetServiceStatus(service.Name, service.ProcessName);
                    if (st != ServiceControllerStatus.Stopped && st != ServiceControllerStatus.StopPending)
                    {
                        ServiceFunction.Default.StopService(service.Name, service.ProcessName);
                    }
                }

                // attende disattivazione dei servizi
                int count = AppCfg.Default.nServiceWaitForStop;
                while (count-- >= 0)
                {
                    Thread.Sleep(1000);

                    if (CheckStatus(listAllowedServices, ServiceControllerStatus.Stopped, (count==0)?true:false))
                    {
                        Logger.Default.Log("Tutti i servizi sono nello stato STOPPED !", Logger.LogType.CONSOLE_LOC_FILE, //notice
                            Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.Stop': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Disattiva tutti i servizi configurati
        /// </summary>
        /// <returns>vedi Stop(List)</returns>
        public bool Stop()
        {
            return this.Stop(this.ServicesReverseList);
        }

        /// <summary>
        ///     Disattiva tutti i servizi del gruppo indicato
        /// </summary>
        /// <param name="sGroup">identificativo gruppo</param>
        /// <returns>vedi Stop(List)</returns>
        public bool Stop(string sGroup)
        {
            try
            {
                if (String.IsNullOrEmpty(sGroup))
                {
                    return this.Stop(this.ServicesReverseList);
                }

                List<ServiceControl> listServices =
                    this.ServicesReverseList.FindAll(delegate(ServiceControl service) { return String.Compare(service.Group, sGroup, true) == 0; });

                return Stop(listServices);
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.Stop(group)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Disattiva la lista dei servizi identificati per nome
        /// </summary>
        /// <param name="servicesName">lista nome servizi da disattivare</param>
        /// <returns>vedi Stop(List)</returns>
        public bool Stop(List<string> servicesName)
        {
            try
            {
                if (servicesName == null || servicesName.Count == 0)
                {
                    return false;
                }

                List<ServiceControl> listServices = this.ServicesReverseList.FindAll(delegate(ServiceControl service)
                {
                    foreach (string name in servicesName)
                    {
                        if (!String.IsNullOrEmpty(name) && String.Compare(name, service.Name, true) == 0)
                        {
                            return true;
                        }
                    }
                    return false;
                });

                return Stop(listServices);
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.Stop(servicesName)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Verifica se lo stato dei servizi indicati dalla lista corrisponde a 'status'
        /// </summary>
        /// <param name="listServices">lista servizi da verificare</param>
        /// <param name="status">stato servizio</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: tutti i servizi indicati sono nello stato indicato</item>
        ///         <item>false: errore, uno o più servizi non sono nello stato indicato</item>
        ///     </list>
        /// </returns>
        private bool CheckStatus(List<ServiceControl> listServices, ServiceControllerStatus status, bool toInfo=false)
        {
            try
            {
                if (listServices == null || listServices.Count == 0)
                {
                    return false;
                }

                List<ServiceControl> listAllowedServices = this.GetAllowedServices(listServices, false);

                // verifica stato servizi
                foreach (ServiceControl service in listAllowedServices)
                {
                    ServiceControllerStatus currentStatus = ServiceFunction.Default.GetServiceStatus(service.Name, service.ProcessName);
                    if (currentStatus != status)
                    {
                        if (toInfo)
                            Logger.Default.Log("Il servizio " + service.Name + " non è nello stato richiesto (stato corrente=" + currentStatus.ToString() + " stato richiesto=" + status.ToString() + ")",
                                            Logger.LogType.CONSOLE_LOC_FILE, //notice
                                            Logger.EventType.INFORMATION, 
                                            AppCfg.I_SYSTEM_XML_NO_ERROR);

                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.CheckStatus': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Verifica se lo stato di tutti i servizi configurati corrisponde a 'status'
        /// </summary>
        /// <param name="status">stato servizio</param>
        /// <returns>vedi CheckStatus(List)</returns>
        public bool CheckStatus(ServiceControllerStatus status)
        {
            return this.CheckStatus(this.ServicesNormalList, status);
        }

        /// <summary>
        ///     Verifica lo stato del servizio MSSQL$SQLEXPRESS, se stopped lo avvia ed effettua una connessione per verificare l'acceso al DB
        /// </summary>
        /// <param name="status">stato servizio</param>
        /// <returns>bool DB attivo</returns>
        private bool CheckSqlExpressServiceActive(bool onLog)
        {
            if (onLog) Logger.Default.Log(
                "Check del servizio MSSQL$SQLEXPRESS...",
                Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

            bool retcode = false;
            string serviceName = "MSSQL$SQLEXPRESS";
            string serviceProcessName = "";
            int n = 0;

            ServiceControllerStatus sqlprocSt = ServiceFunction.Default.GetServiceStatus(serviceName, serviceProcessName);
            while ((sqlprocSt != ServiceControllerStatus.Running) && (n < AppCfg.Default.nCheckDBRetryNum))
            {
                n++;
                if (sqlprocSt == ServiceControllerStatus.StartPending)
                {
                    if (onLog) Logger.Default.Log(
                        "Servizio MSSQL$SQLEXPRESS: StartPending -  Attendo...",
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);
                }
                else
                {
                    if (onLog) Logger.Default.Log(
                        "Starting del servizio MSSQL$SQLEXPRESS ...",
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);
                    ServiceFunction.Default.StartService(serviceName);

                }
                Thread.Sleep(AppCfg.Default.nSqlCommandTimeoutSeconds * 1000);
                sqlprocSt = ServiceFunction.Default.GetServiceStatus(serviceName, serviceProcessName);
            }
            if (sqlprocSt == ServiceControllerStatus.Running)
            {
                if (onLog) Logger.Default.Log(
                    "Servizio MSSQL$SQLEXPRESS: running",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);
                retcode = true;
            }
            else
            {
                Logger.Default.Log(
                    "Servizio MSSQL$SQLEXPRESS: non attivabile!",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.I_SYSTEM_XML_NO_ERROR);
                retcode = false;
            }
            return retcode;
        }

        /// <summary>
        ///     Verifica lo stato del servizio MSSQL$SQLEXPRESS, se stopped lo avvia ed effettua una connessione per verificare l'acceso al DB
        /// </summary>
        /// <param name="status">stato servizio</param>
        /// <returns>bool DB attivo</returns>
        private bool CheckDbActive(bool onLog)
        {
           if (onLog) Logger.Default.Log(
                "Check dello stato del DB...",
                Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

            bool retcode = false;
            int n = 0;
            while ((retcode == false) && (n < AppCfg.Default.nCheckDBRetryNum))
            {
                n++; 
                retcode = STLCManager.Service.Database.SqlDataBaseUtility.CheckDBInstance(System.Configuration.ConfigurationManager.ConnectionStrings["LocalDatabase"].ToString(), onLog);
                if (retcode == false)
                    Thread.Sleep(AppCfg.Default.nSqlCommandTimeoutSeconds * 1000);    // il tempo di attesa per il prossimo tentativo di accesso al DB e pari al timeout di risposta ai comandi
            }

            if (retcode == false)
            {
                Logger.Default.Log( 
                    "DB non attivo !",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.I_SYSTEM_XML_NO_ERROR);
            }

            return retcode;
        }


        /// <summary>
        ///     Verifica se lo stato di tutti i servizi del gruppo indicato corrisponde a 'status'
        /// </summary>
        /// <param name="sGroup">identificativo gruppo</param>
        /// <param name="status">stato servizio</param>
        /// <returns>vedi CheckStatus(List)</returns>
        public bool Stop(string sGroup, ServiceControllerStatus status)
        {
            try
            {
                if (String.IsNullOrEmpty(sGroup))
                {
                    return this.CheckStatus(this.ServicesNormalList, status);
                }

                List<ServiceControl> listServices =
                    this.ServicesNormalList.FindAll(delegate(ServiceControl service) { return String.Compare(service.Group, sGroup, true) == 0; });

                return CheckStatus(listServices, status);
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.CheckStatus(group)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Verifica se lo stato della lista dei servizi identificati per nome corrisponde a 'status'
        /// </summary>
        /// <param name="servicesName">lista nome servizi da verificare</param>
        /// <param name="status">stato servizio</param>
        /// <returns>vedi CheckStatus(List)</returns>
        public bool CheckStatus(List<string> servicesName, ServiceControllerStatus status)
        {
            try
            {
                if (servicesName == null || servicesName.Count == 0)
                {
                    return false;
                }

                List<ServiceControl> listServices = this.ServicesNormalList.FindAll(delegate(ServiceControl service)
                {
                    foreach (string name in servicesName)
                    {
                        if (!String.IsNullOrEmpty(name) && String.Compare(name, service.Name, true) == 0)
                        {
                            return true;
                        }
                    }
                    return false;
                });

                return CheckStatus(listServices, status);
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.CheckStatus(servicesName)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return false;
        }

        /// <summary>
        ///     Restituisce la lista completa dei servizi configurati
        /// </summary>
        /// <returns>lista servizi selezionati</returns>
        public List<ServiceControl> ServicesSelect()
        {
            return this.ServicesNormalList;
        }

        /// <summary>
        ///     Restituisce la lista dei servizi appartenenti al gruppo
        /// </summary>
        /// <param name="sGroup">identificativo gruppo</param>
        /// <returns>lista servizi selezionati</returns>
        public List<ServiceControl> ServicesSelect(string sGroup)
        {
            try
            {
                if (String.IsNullOrEmpty(sGroup))
                {
                    return this.ServicesNormalList;
                }

                return this.ServicesReverseList.FindAll(delegate(ServiceControl service) { return String.Compare(service.Group, sGroup, true) == 0; });
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.ServicesSelect(group)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return null;
        }

        /// <summary>
        ///     Restituisce la lista dei servizi identificati per nome
        /// </summary>
        /// <param name="servicesName">lista nome servizi da selezionare</param>
        /// <returns>lista servizi selezionati</returns>
        public List<ServiceControl> ServicesSelect(List<string> servicesName)
        {
            try
            {
                if (servicesName == null || servicesName.Count == 0)
                {
                    return null;
                }

                return this.ServicesReverseList.FindAll(delegate(ServiceControl service)
                {
                    foreach (string name in servicesName)
                    {
                        if (!String.IsNullOrEmpty(name) && String.Compare(name, service.Name, true) == 0)
                        {
                            return true;
                        }
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'SystemServices.Stop(servicesName)': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            return null;
        }

        #endregion
    }
}
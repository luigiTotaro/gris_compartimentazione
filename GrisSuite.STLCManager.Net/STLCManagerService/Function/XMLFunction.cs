﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Net;
using System.Net.Sockets;
using STLCManager.Service.Types;
using Microsoft.Win32;

namespace STLCManager.Service.Function
{
	public enum xmlActionToDo
	{
		NULL_ACTION,
		SET_CONFIG,
		RUN_SCRIPT,
		ENABLE_FILTER
	};

	internal class XMLFunction
	{
		//Creazione del Singleton XMLFunction
		public static XMLFunction Default = new XMLFunction();

		private string xmlDocPath;

		public xmlActionToDo ActionToDo { get; private set; }
		public string ComputerName { get; private set; }
		public List<STLCNetwork> NetworkPorts { get; private set; }

		public XMLFunction()
		{
			this.xmlDocPath = AppCfg.Default.sPathXmlAction; // .FP_XML_ACTION_VALUE_PATH;

			this.ActionToDo = xmlActionToDo.NULL_ACTION;
			this.ComputerName = null;
			this.NetworkPorts = new List<STLCNetwork>();
		}

		public bool CreateActionFile(STLCSystemReference SysRef, STLCNetworkList SysNetList)
		{
			this.ActionToDo = xmlActionToDo.SET_CONFIG;
			try
			{
				XmlDocument xmlDoc = new XmlDocument();

				//versione + tipo codifica
				XmlDeclaration xmlDecl = xmlDoc.CreateXmlDeclaration("1.0", Encoding.UTF8.WebName, "yes");
				xmlDoc.AppendChild(xmlDecl);

				XmlNode node;
				XmlAttribute attribute;

				//<telefin date=dd.mm.yyyy time=hh.mm.ss
				XmlNode rootNode = xmlDoc.CreateElement("telefin");
				xmlDoc.AppendChild(rootNode);

				DateTime saveNow = DateTime.Now;
				attribute = xmlDoc.CreateAttribute("date");
				attribute.Value = saveNow.ToString("dd.MM.yyyy");
				rootNode.Attributes.Append(attribute);

				attribute = xmlDoc.CreateAttribute("time");
				attribute.Value = saveNow.ToString("HH.mm.ss");
				rootNode.Attributes.Append(attribute);

				//<process run=operazione da eseguire />
				node = xmlDoc.CreateElement("process");
				attribute = xmlDoc.CreateAttribute("run");
				attribute.Value = Enum.GetName(typeof (xmlActionToDo), this.ActionToDo);
				node.Attributes.Append(attribute);
				rootNode.AppendChild(node);

				if (SysRef.ToSet)
				{
					//<system computername=nome />
					node = xmlDoc.CreateElement("system");
					attribute = xmlDoc.CreateAttribute("computername");
					attribute.Value = SysRef.HostName;
					node.Attributes.Append(attribute);
					rootNode.AppendChild(node);
				}

				//<network>
				XmlNode netNode = xmlDoc.CreateElement("network");

				foreach (STLCNetworkReference network in SysNetList)
				{
					if (network.ToSet)
					{
						XmlNode portNode = xmlDoc.CreateElement("port");

						attribute = xmlDoc.CreateAttribute("devid");
						attribute.Value = network.DevId.ToString();
						portNode.Attributes.Append(attribute);

						attribute = xmlDoc.CreateAttribute("port");
						attribute.Value = network.Name.ToString();
						portNode.Attributes.Append(attribute);

						attribute = xmlDoc.CreateAttribute("dhcp");
						attribute.Value = network.DHCPEnabled.ToString().ToLower();
						portNode.Attributes.Append(attribute);

					    attribute = xmlDoc.CreateAttribute("ip");
					    attribute.Value = network.IPAddresses.Count != 0 ? network.IPAddresses[0].ToString() : "";
					    portNode.Attributes.Append(attribute);

					    if (network.IPAddresses.Count >= 2)
					    {
					        attribute = xmlDoc.CreateAttribute("ip2");
					        attribute.Value = network.IPAddresses[1].ToString();
					        portNode.Attributes.Append(attribute);
					    }

                        if (network.IPAddresses.Count >= 3)
                        {
                            attribute = xmlDoc.CreateAttribute("ip3");
                            attribute.Value = network.IPAddresses[2].ToString();
                            portNode.Attributes.Append(attribute);
                        }

                        if (network.IPAddresses.Count >= 4)
                        {
                            attribute = xmlDoc.CreateAttribute("ip4");
                            attribute.Value = network.IPAddresses[3].ToString();
                            portNode.Attributes.Append(attribute);
                        }

                        attribute = xmlDoc.CreateAttribute("subnet");
						attribute.Value = network.SubnetMasks.Count != 0 ? network.SubnetMasks[0].ToString() : "";
						portNode.Attributes.Append(attribute);

					    if (network.SubnetMasks.Count >= 2)
					    {
                            attribute = xmlDoc.CreateAttribute("subnet2");
                            attribute.Value = network.SubnetMasks[1].ToString();
                            portNode.Attributes.Append(attribute);
					    }

                        if (network.SubnetMasks.Count >= 3)
                        {
                            attribute = xmlDoc.CreateAttribute("subnet3");
                            attribute.Value = network.SubnetMasks[2].ToString();
                            portNode.Attributes.Append(attribute);
                        }

                        if (network.SubnetMasks.Count >= 4)
                        {
                            attribute = xmlDoc.CreateAttribute("subnet4");
                            attribute.Value = network.SubnetMasks[3].ToString();
                            portNode.Attributes.Append(attribute);
                        }

						attribute = xmlDoc.CreateAttribute("gateway");
						attribute.Value = network.IPGateways.Count != 0 ? network.IPGateways[0].ToString() : "";
						portNode.Attributes.Append(attribute);

						attribute = xmlDoc.CreateAttribute("metric");
						attribute.Value = network.Metrics.Count != 0 ? network.Metrics[0].ToString() : "";
						portNode.Attributes.Append(attribute);

						if (network.DNSServers.Count > 0)
						{
							XmlNode dnsNode = xmlDoc.CreateElement("dns");
							foreach (IPAddress ip in network.DNSServers)
							{
								XmlNode dns = xmlDoc.CreateElement("item");
								attribute = xmlDoc.CreateAttribute("dns");
								attribute.Value = ip.ToString();
								dns.Attributes.Append(attribute);
								dnsNode.AppendChild(dns);
							}
							portNode.AppendChild(dnsNode);
						}
						netNode.AppendChild(portNode);
					}
				}
				rootNode.AppendChild(netNode);

				//write file
				xmlDoc.Save(this.xmlDocPath);

				return true;
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'XMLFunction.CreateFileToConfig': " + ex.GetType().ToString() + " - " + ex.StackTrace,
					Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}

			return false;
		}

		public bool LoadActionToDo()
		{
			try
			{
				XmlNode rootNode;

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.Load(this.xmlDocPath);

				// Carica azione da eseguire (campo obbligatorio)
				rootNode = xmlDoc.SelectSingleNode("//telefin/process");

				if (rootNode != null)
				{
					string action = ((XmlElement) rootNode).GetAttribute("run");

					foreach (xmlActionToDo item in Enum.GetValues(typeof (xmlActionToDo)))
					{
						if (action == Enum.GetName(typeof (xmlActionToDo), item))
						{
							this.ActionToDo = item;
							break;
						}
					}
				}
				else
				{
					//errore
					Logger.Default.Log("XMLFunction.LoadAction: file xml '" + this.xmlDocPath + "' non valido", Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR);
					return false;
				}

				// Carica eventuale nome computer
				rootNode = xmlDoc.SelectSingleNode("//telefin/system");

				if (rootNode != null)
				{
					this.ComputerName = ((XmlElement) rootNode).GetAttribute("computername");
				}

				// Caricamento eventuale configurazione porta di rete
				rootNode = xmlDoc.SelectSingleNode("//telefin/network");

				if (rootNode != null)
				{
					// Caricamento parametri configurazione porta ethernet -> Cerco gli elementi XML figli
					foreach (XmlElement item in rootNode.ChildNodes)
					{
						STLCNetwork network = new STLCNetwork();
						string strItem;

						network.DevId = UInt32.Parse(item.GetAttribute("devid"));
						network.Name = item.GetAttribute("port");

                        network.DHCPEnabled = (item.GetAttribute("dhcp")=="true")?true:false;

						strItem = item.GetAttribute("ip");
						if (!String.IsNullOrEmpty(strItem))
						{
							network.IPAddresses.Add(IPAddress.Parse(strItem));
						}

                        strItem = item.GetAttribute("ip2");
                        if (!String.IsNullOrEmpty(strItem))
                        {
                            network.IPAddresses.Add(IPAddress.Parse(strItem));
                        }

                        strItem = item.GetAttribute("ip3");
                        if (!String.IsNullOrEmpty(strItem))
                        {
                            network.IPAddresses.Add(IPAddress.Parse(strItem));
                        }

                        strItem = item.GetAttribute("ip4");
                        if (!String.IsNullOrEmpty(strItem))
                        {
                            network.IPAddresses.Add(IPAddress.Parse(strItem));
                        }

						strItem = item.GetAttribute("subnet");
						if (!String.IsNullOrEmpty(strItem))
						{
							network.SubnetMasks.Add(IPAddress.Parse(strItem));
						}

                        strItem = item.GetAttribute("subnet2");
                        if (!String.IsNullOrEmpty(strItem))
                        {
                            network.SubnetMasks.Add(IPAddress.Parse(strItem));
                        }

                        strItem = item.GetAttribute("subnet3");
                        if (!String.IsNullOrEmpty(strItem))
                        {
                            network.SubnetMasks.Add(IPAddress.Parse(strItem));
                        }

                        strItem = item.GetAttribute("subnet4");
                        if (!String.IsNullOrEmpty(strItem))
                        {
                            network.SubnetMasks.Add(IPAddress.Parse(strItem));
                        }

						strItem = item.GetAttribute("gateway");
						if (!String.IsNullOrEmpty(strItem))
						{
							network.IPGateways.Add(IPAddress.Parse(strItem));
						}

						strItem = item.GetAttribute("metric");
						if (!String.IsNullOrEmpty(strItem))
						{
							network.Metrics.Add(UInt16.Parse(strItem));
						}

						XmlNode dnsNode = item.SelectSingleNode("dns");

						if (dnsNode != null)
						{
							foreach (XmlElement dnsItem in dnsNode.ChildNodes)
							{
								IPAddress ip = IPAddress.Parse(dnsItem.GetAttribute("dns"));
								network.DNSServers.Add(ip);
							}
						}

						this.NetworkPorts.Add(network);
					}
				}
			}
			catch (Exception ex)
			{
				if (ex.GetType() != typeof (FileNotFoundException))
				{
					Logger.Default.Log("ECCEZIONE in 'XMLFunction.LoadActionToDo': " + ex.GetType().ToString() + " - " + ex.StackTrace,
						Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				}
				return false;
			}
			return true;
		}

		public void SetActionToDo(xmlActionToDo action)
		{
			try
			{
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.Load(this.xmlDocPath);

				// Carica azione da eseguire (campo obbligatorio)
				XmlNode rootNode = xmlDoc.SelectSingleNode("//telefin/process");

				XmlAttribute attribute = xmlDoc.CreateAttribute("run");
				attribute.Value = Enum.GetName(typeof (xmlActionToDo), action);
				rootNode.Attributes.Append(attribute);

				xmlDoc.Save(this.xmlDocPath);

				this.ActionToDo = action;
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'XMLFunction.SetActionToDo': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}

		public void DeleteActionFile()
		{
			try
			{
				File.Delete(this.xmlDocPath);
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'XMLFunction.DeleteActionFile': " + ex.GetType().ToString() + " - " + ex.StackTrace,
					Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
			}
		}
	}
}
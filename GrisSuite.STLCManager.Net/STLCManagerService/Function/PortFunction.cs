﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;

namespace STLCManager.Service.Function
{
    class PortFunction
    {
        // Definizione possibili errori
        public const int IO_CRITICAL_ERROR  = 259000;
        public const int IO_OPEN_FAILURE    = 259001;
        public const int IO_NOT_OPENED      = 259002;
        public const int IO_WRITE_FAILURE   = 259005;
        public const int IO_READ_FAILURE    = 259051;

        private static object m_sync = new object();

        private static IntPtr hPortFunction = IntPtr.Zero;

        [DllImport("DigitalIo.dll", EntryPoint = "?LeggiDeviceGenport@@YA_NPAXPAEK@Z", SetLastError = true,
        CharSet = CharSet.Ansi, ExactSpelling = true,
        CallingConvention = CallingConvention.StdCall)]
        private static extern unsafe bool LeggiDeviceGenport(IntPtr hGenPort, IntPtr byOut, ulong dwOffSet);

        [DllImport("DigitalIo.dll", EntryPoint = "?ScriviDeviceGenport@@YA_NPAXEK@Z", SetLastError = true,
        CharSet = CharSet.Ansi, ExactSpelling = true,
        CallingConvention = CallingConvention.StdCall)]
        private static extern unsafe bool ScriviDeviceGenport(IntPtr hGenPort, byte byIn, ulong dwOffSet);

        [DllImport("DigitalIo.dll", EntryPoint = "?AperturaGenport@@YAPAXXZ", SetLastError = true,
        CharSet = CharSet.Ansi, ExactSpelling = true,
        CallingConvention = CallingConvention.StdCall)]
        private static extern unsafe IntPtr AperturaGenport();

        [DllImport("DigitalIo.dll", EntryPoint = "?ChiusuraGenport@@YA_NPAX@Z", SetLastError = true,
        CharSet = CharSet.Ansi, ExactSpelling = true,
        CallingConvention = CallingConvention.StdCall)]
        private static extern unsafe bool ChiusuraGenport(IntPtr hGenPort);

        //////////////////////////////////////////////////////////////////////////////////
        //	FUNZIONE			:   Open					        	            	//
        //	DESCRIZIONE			:   Apertura driver IO                              	//
        //	CHIAMATA			:   Open()                  	        				//
        //	PARAMETRI FORMALI															//
        //	input				:	                                                	//
        //	output				:														//
        //	CODICI DI RITORNO	:	true     -	Apertura Ok                             //
        //                          false    -  Apertura Ko                             //
        //////////////////////////////////////////////////////////////////////////////////
        public static bool Open()
        {
            try
            {
                IntPtr tmp_hPortFunction = AperturaGenport();
                if (tmp_hPortFunction != IntPtr.Zero)
                {
                    hPortFunction = tmp_hPortFunction;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                Logger.Default.Log("ECCEZIONE in 'PortFunction.Open': " + e.Message + ".", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, IO_CRITICAL_ERROR);                                            
                return false;
            }
        }

        //////////////////////////////////////////////////////////////////////////////////
        //	FUNZIONE			:   Close					        	            	//
        //	DESCRIZIONE			:   Chiusura driver IO                              	//
        //	CHIAMATA			:   Close()                  	        				//
        //	PARAMETRI FORMALI															//
        //	input				:	                                                	//
        //	output				:														//
        //	CODICI DI RITORNO	:	true     -	Apertura Ok                             //
        //                          false    -  Apertura Ko                             //
        //////////////////////////////////////////////////////////////////////////////////
        public static bool Close()
        {
            Logger.Default.Log("Chiusura PortFunction", Logger.LogType.CONSOLE_LOC_FILE);                                            
            
            bool bRetVal;
            try
            {
                bRetVal = ChiusuraGenport(hPortFunction);
            }
            catch (Exception e)
            {
                Logger.Default.Log("ECCEZIONE in 'PortFunction.Close': " + e.Message + ".", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, IO_CRITICAL_ERROR);                                            
                return false;
            }
            return bRetVal;
        }

        //Per test software
        //public static void ReadTest()
        //{
        //    IntPtr ptrRead;
        //    ptrRead = Marshal.AllocHGlobal(1);

        //    LeggiDeviceGenport(hGenPort, ptrRead, 0);

        //    byte p_byRead = Marshal.ReadByte(ptrRead);

        //    Marshal.FreeHGlobal(ptrRead);

        //    string sTmp;
        //    sTmp = String.Format("Letto: 0x{0:X2}", p_byRead);
        //    Main.logger.LogMessage(sTmp, TipoLog.LogSuListBox);
        //}

        //////////////////////////////////////////////////////////////////////////////////
        //	FUNZIONE			:   Read					        	            	//
        //	DESCRIZIONE			:   Lettura da una porta IO                            	//
        //	CHIAMATA			:   Read(ref p_byRead, dwOffSet)	       				//
        //	PARAMETRI FORMALI															//
        //	input				:	p_byRead - Riferimento al valore letto             	//
        //							dwOffSet - Indirizzo da cui leggere                 //
        //	output				:														//
        //	CODICI DI RITORNO	:	true     -	Lettura Ok                              //
        //                          false    -  Lettura Ko                              //
        //////////////////////////////////////////////////////////////////////////////////
        public static bool Read(ref byte p_byRead, ulong dwOffSet)
        {
            IntPtr ptrRead;

            if (hPortFunction == IntPtr.Zero)
                return false;

            try
            {
                ptrRead = Marshal.AllocHGlobal(1);
                if (!LeggiDeviceGenport(hPortFunction, ptrRead, dwOffSet))
                {
                    int nErr = Marshal.GetLastWin32Error();
                    string sErr = String.Format("{0:D}", nErr);
                    Logger.Default.Log("PortFunction.Read: Errore n.ro " + sErr + ".", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, IO_READ_FAILURE);                                            
                    Marshal.FreeHGlobal(ptrRead);
                    return false;
                }
                p_byRead = Marshal.ReadByte(ptrRead);
                Marshal.FreeHGlobal(ptrRead);
            }
            catch (Exception e)
            {
                p_byRead = 0x00;
                Logger.Default.Log("ECCEZIONE in 'PortFunction.Read': " + e.Message + ".", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, IO_CRITICAL_ERROR);                                            
                return false;
            }
            return true;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //	FUNZIONE			:   Write					        	            	//
        //	DESCRIZIONE			:   Scrittura su una porta IO                       	//
        //	CHIAMATA			:   Write(nToWrite, dwOffSet)	        				//
        //	PARAMETRI FORMALI															//
        //	input				:	nToWrite - Valore da scrivere                   	//
        //							dwOffSet - Indirizzo a cui scrivere                 //
        //	output				:														//
        //	CODICI DI RITORNO	:	true     -	Scrittura Ok                            //
        //                          false    -  Scrittura Ko                            //
        //////////////////////////////////////////////////////////////////////////////////
        public static bool Write(int nToWrite, ulong dwOffSet)
        {
            if (hPortFunction == IntPtr.Zero)
                return false;

            lock (m_sync)
            {
                byte byToWrite = (byte)nToWrite;
                try
                {
                    if (!ScriviDeviceGenport(hPortFunction, byToWrite, dwOffSet))
                    {
                        int nErr = Marshal.GetLastWin32Error();
                        string sErr = String.Format("{0:D}", nErr);
                        Logger.Default.Log("PortFunction.Write: Errore n.ro " + sErr + ".", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, IO_WRITE_FAILURE);
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Logger.Default.Log("ECCEZIONE in 'PortFunction.Write': " + e.Message + ".", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, IO_CRITICAL_ERROR);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Imposta led (luce verde e luce rossa)
        /// </summary>
        /// <param name="greenLedOn">true luce verde accesa</param>
        /// <param name="redLedOn">true luce rossa accesa</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: impostazione led eseguita</item>
        ///         <item>false: impostazione led fallita</item>
        ///     </list>
        /// </returns>
        public static bool SetSystemLED(bool greenLedOn, bool redLedOn)
        {
            int nMaskOut = 0;

            if (greenLedOn) nMaskOut |= 0x0001;
            if (redLedOn  ) nMaskOut |= 0x0002;

            return Write(nMaskOut, 0);
        }
    }
}

﻿using System;
using System.Globalization;
using System.Timers;
using STLCManager.Service.Types;

namespace STLCManager.Service.Function
{
	/// <summary>
	///     Classe creata al caricamento del codice per la gestione del led di stato sistema
	/// </summary>
	internal static class SystemLed
	{
		#region Token

		// Temporizzatori in msec: LED_PATTERN_LEN non deve superare 32.
		private const int LED_PERIOD_TIME = 5000;
		private const int LED_FLASHING_TIME = 250;
		private const int LED_PATTERN_LEN = LED_PERIOD_TIME/LED_FLASHING_TIME;

		private const int LED_LIFE_TIME = 20000;
		private const int LED_LIFE_COUNT = LED_LIFE_TIME/LED_FLASHING_TIME;

		public enum Mode
		{
			SYSTEM_LED_OFF,
			SYSTEM_INIT,
			SYSTEM_RUNNING,
			SYSTEM_MAINTENANCE,
			SYSTEM_WARNING,
			SYSTEM_FAILURE, //l'uscita fisica di pilotaggio led è impostata all'atto della chiamata della metodo 'Set', non attende ciclo di schedulazione
			SYSTEM_CONFIG,
			SERVICES_RESTART
		};

		private struct Pattern
		{
			public UInt32 Red { get; set; }
			public UInt32 Green { get; set; }
		};

		#endregion

		#region Members

		private static Pattern[] definePatterns; // Definizione pattern da utilizzare in base a Mode

		private static bool isEnabled; // thread gestione led di sistema abilitato
		private static bool isRunning; // thread gestione led di sistema in funzione (true ad ogni attivazione del thread)
		private static Mode ledMode; // modalità di gestione led
		private static Pattern ledPattern; // pattern gestione led
		private static UInt32 maskPattern; // maschera gestione pattern leds
		private static int lifeCount; // contatore di vita, usato per verificare l'attività da parte del sistema, in assenza di attività spegne il led

		private static Timer timerManageLed;

		#endregion

		#region Constructors

		/// <summary>
		///     Costruttore classe, allo startup la gestione del led è disabilitata.
		/// </summary>
		static SystemLed()
		{
			isEnabled = false;
			isRunning = false;

			// Imposta pattern di default
			definePatterns = new Pattern[Enum.GetValues(typeof (Mode)).Length];

			definePatterns[(int) Mode.SYSTEM_LED_OFF].Red = 0x00000;
			definePatterns[(int) Mode.SYSTEM_LED_OFF].Green = 0x00000;

			definePatterns[(int) Mode.SYSTEM_INIT].Red = 0x55555;
			definePatterns[(int) Mode.SYSTEM_INIT].Green = 0x55555;

			definePatterns[(int) Mode.SYSTEM_RUNNING].Red = 0x00000;
			definePatterns[(int) Mode.SYSTEM_RUNNING].Green = 0x00001;

			definePatterns[(int) Mode.SYSTEM_MAINTENANCE].Red = 0xFFFFE;
			definePatterns[(int) Mode.SYSTEM_MAINTENANCE].Green = 0xFFFFF;

			definePatterns[(int) Mode.SYSTEM_WARNING].Red = 0x33333;
			definePatterns[(int) Mode.SYSTEM_WARNING].Green = 0xCCCCC;

			definePatterns[(int) Mode.SYSTEM_FAILURE].Red = 0xFFFFF;
			definePatterns[(int) Mode.SYSTEM_FAILURE].Green = 0x00000;

			definePatterns[(int) Mode.SYSTEM_CONFIG].Red = 0xFFFFF;
			definePatterns[(int) Mode.SYSTEM_CONFIG].Green = 0xFFFFF;

			definePatterns[(int) Mode.SERVICES_RESTART].Red = 0x00000;
			definePatterns[(int) Mode.SERVICES_RESTART].Green = 0x55555;
		}

		#endregion

		#region Accessor

		/// <summary>
		///     Restituisce lo stato di abilitazione led di sistema
		/// </summary>
		public static bool IsEnabled
		{
			get { return isEnabled; }
		}

		public static UInt32 ThreadPeriod
		{
			get { return LED_FLASHING_TIME; }
		}

		#endregion

		#region Methods

		/// <summary>
		///     Restituisce il flag di running (processo in eseguzione corretta) e lo azzera
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: avvenuto ciclo di schedulazione dopo ultima chiamata</item>
		///         <item>false: nessuna schedulazione avvenuta</item>
		///     </list>
		/// </returns>
		public static bool IsRunning()
		{
			if (isRunning)
			{
				isRunning = false;
				return true;
			}
			return false;
		}

		/// <summary>
		///     Configura pattern led in base al mode indicato
		/// </summary>
		/// <param name="mode">identificativo stato sistema</param>
		/// <param name="green">pattern led verde</param>
		/// <param name="red">pattern led rosso</param>
		public static void SetPattern(Mode mode, UInt32 green, UInt32 red)
		{
			int imode = (int) mode;

			if (0 <= imode && imode < definePatterns.Length)
			{
				definePatterns[imode].Green = green;
				definePatterns[imode].Red = red;
			}
		}

		/// <summary>
		///     Imposta pattern led in base al modo indicato
		/// </summary>
		/// <param name="sMode">stringa contenente in mode</param>
		/// <param name="sGreen">stringa contenente pattern in formato esadecimale</param>
		/// <param name="sRed">stringa contenente pattern in formato esadecimale</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: impostazione pattern eseguita correttamente</item>
		///         <item>false: dati non validi, pattern non impostato</item>
		///     </list>
		/// </returns>
		public static bool SetPattern(string sMode, string sGreen, string sRed)
		{
			try
			{
				Mode mode = (Mode) Enum.Parse(typeof (Mode), sMode);

				UInt32 green = 0;
				if (!String.IsNullOrEmpty(sGreen))
				{
					string sHex = sGreen.Replace("0x", "");
					green = UInt32.Parse(sHex, NumberStyles.AllowHexSpecifier);
				}

				UInt32 red = 0;
				if (!String.IsNullOrEmpty(sRed))
				{
					string sHex = sRed.Replace("0x", "");
					red = UInt32.Parse(sHex, NumberStyles.AllowHexSpecifier);
				}

				SetPattern(mode, green, red);
			}
			catch
			{
				return false;
			}
			return true;
		}

		/// <summary>
		///     Abilita gestione led di stato sistema. Crea timer per l'aggiornamento periodico dello stato del led.
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: gestione led di sistema abilitata</item>
		///         <item>false: errore, impossibile abilitare gestione led di sistema</item>
		///     </list>
		/// </returns>
		public static bool Enable()
		{
			if (STLCConfig.IsSTLCHost && !isEnabled)
			{
				ledMode = Mode.SYSTEM_LED_OFF;
				ledPattern.Red = definePatterns[(int) ledMode].Red;
				ledPattern.Green = definePatterns[(int) ledMode].Green;
				maskPattern = 0;
				lifeCount = LED_LIFE_COUNT;
				isRunning = false;

				try
				{
					if (PortFunction.Open())
					{
						if (PortFunction.SetSystemLED(false, false))
						{
							// timer gestione led di stato
							timerManageLed = new Timer(LED_FLASHING_TIME);
							timerManageLed.Elapsed += new ElapsedEventHandler(ManageLED);
							timerManageLed.AutoReset = true;
							timerManageLed.Enabled = true;
							timerManageLed.Start();

							isEnabled = true;

							Logger.Default.Log("Attivata gestione SystemLed.", Logger.LogType.CONSOLE_LOC_FILE);
						}
						else
						{
							PortFunction.Close();
						}
					}
				}
				catch (Exception ex)
				{
					Logger.Default.Log("ECCEZIONE in 'SystemLed.Enable': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, Logger.E_STACK_TRACE);
				}
			}
			return isEnabled;
		}

		/// <summary>
		///     Disabilita gestione led di sistema.
		/// </summary>
		/// <returns>
		///     <list type="bool">
		///         <item> true: gestione led di sistema disabilitata</item>
		///     </list>
		/// </returns>
		public static bool Disable()
		{
			if (isEnabled)
			{
				timerManageLed.Stop();
				timerManageLed.Dispose();
				timerManageLed = null;
				PortFunction.Close();
				isEnabled = false;
				Logger.Default.Log("Disattivata gestione SystemLed.", Logger.LogType.CONSOLE_LOC_FILE);
			}
			return !isEnabled;
		}

		/// <summary>
		///     Esegue la gestione del led di sistema, funzione chiamata periodicamente da system.timer.
		///     In mancanza del segnale di vita da parte del thred principale il led viene spento.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void ManageLED(object sender, ElapsedEventArgs e)
		{
			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					timerManageLed.Enabled = false;

					isRunning = true;

					//verifica segnale di vita
					if (lifeCount <= 0)
					{
						ledMode = Mode.SYSTEM_LED_OFF;
						ledPattern.Red = definePatterns[(int) ledMode].Red;
						ledPattern.Green = definePatterns[(int) ledMode].Green;
					}
					else
					{
						lifeCount--;
					}

					//aggiorna maschera di gestione pattern
					if (maskPattern <= 1)
					{
						ledPattern.Red = definePatterns[(int) ledMode].Red;
						ledPattern.Green = definePatterns[(int) ledMode].Green;
						maskPattern = (UInt32) 1 << LED_PATTERN_LEN - 1;
					}
					else
					{
						maskPattern >>= 1;
					}

					//configura porta fidica di controllo led di sistema
					if (!PortFunction.SetSystemLED((ledPattern.Green & maskPattern) != 0, (ledPattern.Red & maskPattern) != 0))
					{
						Disable();
					}
					else
					{
						timerManageLed.Enabled = true;
					}
				}
				catch (Exception ex)
				{
					Logger.Default.Log("ECCEZIONE in 'SystemLed.ManageLED': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, Logger.E_STACK_TRACE);
					Disable();
				}
			}
		}

		/// <summary>
		///     Imposta modalità di lampeggio led di sistema. Ristarta segnale di vita.
		/// </summary>
		/// <param name="mode">modo di lampeggio</param>
		public static void Set(Mode mode)
		{
			if (STLCConfig.IsSTLCHost)
			{
				if (ledMode != mode)
				{
					int imode = (int) mode;

					if (0 <= imode && imode < definePatterns.Length)
					{
						ledPattern = definePatterns[imode];
					}

					if (isEnabled && mode == Mode.SYSTEM_FAILURE)
					{
						//forza impostazione del led
						PortFunction.SetSystemLED((ledPattern.Green & maskPattern) != 0, (ledPattern.Red & maskPattern) != 0);
					}

					ledMode = mode;
				}
				lifeCount = LED_LIFE_COUNT;
			}
		}

		/// <summary>
		///     Imposta modalità di lampeggio led di sistema per un solo ciclo (5 sec), dopo risprende il lampeggio impostato con
		///     il metodo 'set'.
		/// </summary>
		/// <param name="mode">modo di lampeggio</param>
		public static void Flash(Mode mode)
		{
			if (STLCConfig.IsSTLCHost)
			{
				int imode = (int) mode;

				maskPattern = (UInt32) 1 << LED_PATTERN_LEN;

				if (0 <= imode && imode < definePatterns.Length)
				{
					ledPattern = definePatterns[imode];
				}

				maskPattern = (UInt32) 1 << LED_PATTERN_LEN;
			}
		}

		#endregion
	}
}
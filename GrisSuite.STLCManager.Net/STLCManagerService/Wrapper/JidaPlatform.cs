﻿using System;
using System.Configuration;
using System.Runtime.InteropServices;
using STLCManager.Service.Types;
using System.Text;

namespace STLCManager.Service.Wrapper
{
	internal class JidaPlatform
	{
		//==============================================================================
		// Definizioni
		//------------------------------------------------------------------------------
		public const int JIDA_TEMP_CPU = 0;

		//Creazione del Singleton JidaPlatform
		public static JidaPlatform Default = new JidaPlatform();

		protected JidaPlatform()
		{
			try
			{
			}
			catch (Exception ex)
			{
				Logger.Default.ManageException(ex);
			}
		}

		public const int NO_ERROR                               = AppCfg.COD_NO_ERROR;

		public const int JIDA_INIT_FAILURE                      = 259006;
		public const int JIDA_NOT_INITIALIZED                   = 259007;
		public const int JIDA_CLEAR_FAILURE                     = 259008;
		public const int JIDA_GET_FUNCTION_FAILURE              = 259009;
		public const int JIDA_BOARD_OPEN_FAILURE                = 259010;
		public const int JIDA_FUNCTION_NOT_FOUND                = 259011;
		public const int JIDA_BOARD_CLOSE_FAILURE               = 259012;
		public const int JIDA_WATCHDOG_SET_FAILURE              = 259013;
		public const int JIDA_WATCHDOG_TRIGGER_FAILURE          = 259014;
		public const int JIDA_INITIALIZED_FAILURE               = 259015;
		public const int JIDA_UNINITIALIZED_FAILURE             = 259016;
		public const int JIDA_WRITE_PWMDXFUN_REGISTER_FAILURE   = 259017;
		public const int JIDA_WRITE_PINSXFUN_REGISTER_FAILURE   = 259018;
		public const int JIDA_WRITE_PWMENSXFUN_REGISTER_FAILURE = 259019;
		public const int JIDA_WRITE_PWMSXFUN_REGISTER_FAILURE   = 259020;
		public const int JIDA_FUNCTION_FAILURE                  = 259021;
		public const int JIDA_READ_REGISTER_FAILURE             = 259022;
		public const int PWM_OUT_OF_RANGE                       = 259023;
        public const int JIDA_GET_BOARD_NAME_FAILURE            = 259024;

        // Dimensione massima del buffer contenente il nome della board
        private const int   MAX_LEN_BOARD_NAME                  = 10;

		// Definizione registri Winbond W83782D
		private const short W83782D_TEMPERATURE_SENSOR1_GET     = 0x27;
		private const short W83782D_FAN_BEEP_CONTROL            = 0x4D;
		private const short W83782D_BANK_SELECTION              = 0x4E;
		private const short W83782D_BANK0_DIODE_SELECTION       = 0x59;
		private const short W83782D_BANK0_PWMOUT2_CONTROL       = 0x5A;
		private const short W83782D_BANK0_PWMOUT1_CONTROL       = 0x5B;
		private const short W83782D_BANK0_PWMOUT_CLOCK_SELECT   = 0x5C;
		private const short W83782D_BANK0_MONITOR_CONTROL       = 0x5D;

        // Possibili nomi della board
        private const string BOARD_MBR1                         = "MBR1";
        private const string BOARD_MNP1                         = "MNP1";
        private const string BOARD_MODB                         = "MODB";

        private const byte  ETX_OH_SMBUS_ADDRESS                = 0x2a;
        private const byte  ETX_DC_SMBUS_ADDRESS                = 0x54;


// Temperature, fan, and voltage structures

// temperature in units of 1/1000th degrees celcius

		#region Strutture
/*
		[StructLayout(LayoutKind.Sequential)]
		public struct JIDATEMPERATUREINFO
		{
			public ulong dwSize;
			public ulong dwType;
			public ulong dwFlags;
			public ulong dwAlarm;
			public ulong dwRes;
			public ulong dwMin;
			public ulong dwMax;
			public ulong dwAlarmHi;
			public ulong dwHystHi;
			public ulong dwAlarmLo;
			public ulong dwHystLo;
		}
*/
        [StructLayout(LayoutKind.Sequential)]
        public struct JIDATEMPERATUREINFO
        {
            public uint dwSize;
            public uint dwType;
            public uint dwFlags;
            public uint dwAlarm;
            public uint dwRes;
            public uint dwMin;
            public uint dwMax;
            public uint dwAlarmHi;
            public uint dwHystHi;
            public uint dwAlarmLo;
            public uint dwHystLo;
        }

		#endregion

        private static string       sJidaBoardName;
		private static uint         JidaHandle;
		private JIDATEMPERATUREINFO JidaTempInfo;

		private enum RebootType
		{
			JWDModeRebootPC = 0,
			JWDModeRestartWindows
		};

		[DllImport("JIDA.DLL", EntryPoint = "JidaDllInitialize", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaDllInitialize();

        [DllImport("JIDA.DLL", EntryPoint = "JidaBoardGetNameW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool JidaBoardGetName(uint hJida, [MarshalAs(UnmanagedType.LPWStr), Out(), In()] StringBuilder pszName, [MarshalAs(UnmanagedType.U4), In()] uint dwSize);

		[DllImport("JIDA.DLL", EntryPoint = "JidaDllUninitialize", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaDllUninitialize();

		[DllImport("JIDA.DLL", EntryPoint = "JidaBoardOpenW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaBoardOpen([MarshalAs(UnmanagedType.LPWStr)] string pszClass, uint dwNum, uint dwFlags, ref uint hJida);

		[DllImport("JIDA.DLL", EntryPoint = "JidaBoardClose", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaBoardClose(uint hJida);

		[DllImport("JIDA.DLL", EntryPoint = "JidaWDogSetConfig", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaWDogSetConfig(uint hJida, uint dwType, uint timeout, uint delay, RebootType mode);

		[DllImport("JIDA.DLL", EntryPoint = "JidaWDogTrigger", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaWDogTrigger(uint hJida, uint dwType);

		[DllImport("JIDA.DLL", EntryPoint = "JidaTemperatureGetInfo", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaTemperatureGetInfo(uint hJida, uint dwType, ref JIDATEMPERATUREINFO jti);

		[DllImport("JIDA.DLL", EntryPoint = "JidaTemperatureGetCurrent", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaTemperatureGetCurrent(uint hJida, uint dwType, ref uint dwSetting, ref uint dwStatus);

		[DllImport("JIDA.DLL", EntryPoint = "JidaI2CReadRegister", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaI2CReadRegister(uint hJida, uint dwType, byte bAddr, short wReg, ref Byte pDataByte);

		[DllImport("JIDA.DLL", EntryPoint = "JidaI2CWriteRegister", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true,
			CallingConvention = CallingConvention.StdCall)]
		private static extern bool JidaI2CWriteRegister(uint hJida, uint dwType, byte bAddr, short wReg, Byte DataByte);

        /// <summary>
        /// Recupera l'indirizzo smbus da utilizzare in base al nome della board
        /// letta tramite la JidaBoardGetName
        /// </summary>
        /// <returns></returns>
        private byte W83782D_GetI2C_SMBusAddress()
        {
            byte byAddr = ETX_DC_SMBUS_ADDRESS;

            switch(sJidaBoardName.ToUpper())
            {
                case BOARD_MBR1:
                    byAddr = ETX_OH_SMBUS_ADDRESS;
                    break;
                case BOARD_MNP1:
                    byAddr = ETX_DC_SMBUS_ADDRESS;
                    break;
                case BOARD_MODB:
                    byAddr = ETX_DC_SMBUS_ADDRESS;
                    break;
                default:
                    byAddr = ETX_DC_SMBUS_ADDRESS;
                    break;
            }

            return byAddr;
        }

		/// <summary>
		///     Legge registro del componente Winbond W83782D mediante interfaccia I2C.
		/// </summary>
		/// <param name="iReg">identificativo registro da leggere</param>
		/// <param name="byValue">valore registro letto</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: lettura registro eseguita correttamente</item>
		///         <item>false: lettura registro fallita</item>
		///     </list>
		/// </returns>
		private bool W83782D_GetRegister(short iReg, out Byte byValue)
		{
			Byte byVal  = 0;
            Byte byAddr = W83782D_GetI2C_SMBusAddress(); // 0x54; //DEVICE_I2C_SMB_ADDRESS;
			uint dwType = 1;

			byValue = 0;

			try
			{
				if (JidaI2CReadRegister(JidaHandle, dwType, byAddr, iReg, ref byVal))
				{
					byValue = byVal;
					return true;
				}
				Logger.Default.Log("Errore in 'W83782D_GetRegister'. Lettura fallita " + Marshal.GetLastWin32Error().ToString(), Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, JIDA_FUNCTION_FAILURE);
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'W83782D_GetRegister': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, AppCfg.E_COD_FUNCTION_EXCEPTION);
			}
			return false;
		}

		/// <summary>
		///     Imposta registro del componente Winbond W83782D mediante interfaccia I2C.
		/// </summary>
		/// <param name="iReg">identificativo registro da impostare</param>
		/// <param name="byValue">valore da settare</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: scrittura registro eseguita correttamente</item>
		///         <item>false: scrittura registro fallita</item>
		///     </list>
		/// </returns>
		private bool W83782D_SetRegister(short iReg, Byte byValue)
		{
            Byte byAddr = W83782D_GetI2C_SMBusAddress(); // 0x54; //DEVICE_I2C_SMB_ADDRESS;
			uint dwType = 1;

			try
			{
				if (JidaI2CWriteRegister(JidaHandle, dwType, byAddr, iReg, byValue))
				{
					return true;
				}
				Logger.Default.Log("Errore in 'W83782D_SetRegister'. Scrittura fallita " + Marshal.GetLastWin32Error().ToString(), Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, JIDA_FUNCTION_FAILURE);
			}
			catch (Exception ex)
			{
				Logger.Default.Log("ECCEZIONE in 'W83782D_SetRegister': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
					Logger.EventType.ERROR, AppCfg.E_COD_FUNCTION_EXCEPTION);
			}
			return false;
		}

		/// <summary>
		///     Setta i bits del registro del componente Winbond W83782D mediante interfaccia I2C.
		/// </summary>
		/// <param name="iReg">identificativo registro da impostare</param>
		/// <param name="byMask">maschera bits da settare a 1</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: impostazione registro eseguita correttamente</item>
		///         <item>false: impostazione registro fallita</item>
		///     </list>
		/// </returns>
		private bool W83782D_SetRegBits(short iReg, Byte byMask)
		{
			Byte byValue;

			if (this.W83782D_GetRegister(iReg, out byValue))
			{
				byValue |= byMask;
				return this.W83782D_SetRegister(iReg, byValue);
			}
			return false;
		}

		/// <summary>
		///     Azzera i bits del registro del componente Winbond W83782D mediante interfaccia I2C.
		/// </summary>
		/// <param name="iReg">identificativo registro da impostare</param>
		/// <param name="byMask">maschera bits da settare a 0</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: impostazione registro eseguita correttamente</item>
		///         <item>false: impostazione registro fallita</item>
		///     </list>
		/// </returns>
		private bool W83782D_ClrRegBits(short iReg, Byte byMask)
		{
			Byte byValue;

			if (this.W83782D_GetRegister(iReg, out byValue))
			{
				byValue &= (Byte) (~byMask);
				return this.W83782D_SetRegister(iReg, byValue);
			}
			return false;
		}

		/// <summary>
		///     Azzera i bits del registro del componente Winbond W83782D mediante interfaccia I2C.
		/// </summary>
		/// <param name="iReg">identificativo registro da impostare</param>
		/// <param name="byClrMask">maschera bits da settare a 0</param>
		/// <param name="bySetMask">maschera bits da settare a 1</param>
		/// <returns>
		///     <list type="bool">
		///         <item> true: impostazione registro eseguita correttamente</item>
		///         <item>false: impostazione registro fallita</item>
		///     </list>
		/// </returns>
		private bool W83782D_ClrSetRegBits(short iReg, Byte byClrMask, Byte bySetMask)
		{
			Byte byValue;

			if (this.W83782D_GetRegister(iReg, out byValue))
			{
				byValue &= (Byte) (~byClrMask);
				byValue |= bySetMask;
				return this.W83782D_SetRegister(iReg, byValue);
			}
			return false;
		}

		//==============================================================================
		/// Funzione per caricare la JIDA
		/// 
		/// In caso di errore restituisce un valore diverso da 0
		/// 
		/// \date [30.05.2008]
		/// \author Mario Ferro
		/// \version 0.03
		//------------------------------------------------------------------------------
		public int LoadJIDA()
		{
			int ret_code = NO_ERROR;

			ret_code = this.JidaInit();

			if (ret_code == NO_ERROR)
			{
				ret_code = this.OpenCPUBoard();
			}

            if(ret_code == NO_ERROR)
            {
                ret_code = this.GetBoardName();
            }

			if (ret_code == NO_ERROR)
			{
				ret_code = this.SetInfoTemperature();
			}

			if (ret_code == NO_ERROR)
			{
				if ((ret_code = this.SelectSensorTemperature()) == NO_ERROR)
				{
					Logger.Default.Log("Selezione sensore temperatura by Jida riuscita", Logger.LogType.CONSOLE_LOC_FILE);
				}
			}

			if (ret_code == NO_ERROR)
			{
				if ((ret_code = this.SetFansControlRegister()) == NO_ERROR)
				{
					Logger.Default.Log("Impostazione controllo ventole by Jida riuscita", Logger.LogType.CONSOLE_LOC_FILE);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per inizializzare la libreria Jida.dll
		/// 
		/// In caso di errore restituisce un valore diverso da 0
		/// 
		/// \date [18.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		private int JidaInit()
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					if (!JidaDllInitialize())
					{
						ret_code = JIDA_INIT_FAILURE;
						string ret_errMsg = "Inizializzazione Jida fallita. Jida Driver incompatibile o non installato";
						Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
					else
					{
						Logger.Default.Log("Jida Driver correttamente inizializzato", Logger.LogType.CONSOLE_LOC_FILE);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'JidaInit': " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per accedere alla scheda CPU dell'STLC1000
		/// 
		/// In caso di errore restituisce un valore diverso da 0
		/// 
		/// \date [18.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		private int OpenCPUBoard()
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					if (!JidaBoardOpen("CPU", 0, 0, ref JidaHandle))
					{
						ret_code = JIDA_BOARD_OPEN_FAILURE;
						string ret_errMsg = "Apertura Jida fallita";
						Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
					else
					{
						Logger.Default.Log("Apertura Jida riuscita", Logger.LogType.CONSOLE_LOC_FILE);

                        //Logger.Default.Log(String.Format("handle {0}", JidaHandle), Logger.LogType.CONSOLE_LOC_FILE);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'OpenCPUBoard': " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

        //==============================================================================
        /// Funzione per recuperare il nome della board
        /// 
        /// In caso di errore restituisce un valore diverso da 0
        /// 
        /// \date [15.09.2015]
        /// \author Mario Ferro
        /// \version 0.01
        //------------------------------------------------------------------------------
        private int GetBoardName()
        {
            int ret_code    = NO_ERROR;
            
            if(STLCConfig.IsSTLCHost)
            {
                try
				{
                    StringBuilder tmpName = new StringBuilder(MAX_LEN_BOARD_NAME); 

                    if (!JidaBoardGetName(JidaHandle, tmpName, MAX_LEN_BOARD_NAME))
                    {
                        ret_code            = JIDA_GET_BOARD_NAME_FAILURE;
                        string ret_errMsg   = "Lettura nome board fallita";
                        Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
                    }
                    else
                    {
                        sJidaBoardName = tmpName.ToString();
                        Logger.Default.Log(String.Format("Letto nome board \"{0}\" [Indirizzo SMBus 0x{1:x}]", sJidaBoardName, W83782D_GetI2C_SMBusAddress()), Logger.LogType.CONSOLE_LOC_FILE);
                    }

                }
                catch (Exception ex)
                {
                    ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
                    string ret_errMsg = "ECCEZIONE in 'GetBoardName': " + ex.GetType().ToString() + " - " + ex.StackTrace;
                    Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
                }
            }

            return ret_code;
        }

		//==============================================================================
		/// Funzione per settare le info della Jida sulla temperatura dell'STLC1000
		/// 
		/// In caso di errore restituisce un valore diverso da 0
		/// 
		/// \date [18.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		private int SetInfoTemperature()
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					//this.JidaTempInfo.dwSize = (ulong) Marshal.SizeOf(typeof (JIDATEMPERATUREINFO));
                    this.JidaTempInfo.dwSize = (uint)Marshal.SizeOf(typeof(JIDATEMPERATUREINFO));

					if (!JidaTemperatureGetInfo(JidaHandle, 0, ref this.JidaTempInfo))
					{
						ret_code = JIDA_FUNCTION_FAILURE;
						string ret_errMsg = "Lettura Info Temperatura by Jida fallita";
						ret_errMsg = ret_errMsg + " " + Marshal.GetLastWin32Error().ToString();
						Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
					else
					{
						Logger.Default.Log("Lettura Info Temperatura by Jida riuscita", Logger.LogType.CONSOLE_LOC_FILE);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'SetInfoTemperature': " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		/// <summary>
		///     Seleziona sensore temperatura installato sulla scheda (sensore 2N3904)
		/// </summary>
		/// <returns>In caso di errore restituisce un valore diverso da 0</returns>
		public int SelectSensorTemperature()
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					if (
						!(this.W83782D_SetRegister(W83782D_BANK_SELECTION, 0x00) && this.W83782D_ClrRegBits(W83782D_BANK0_DIODE_SELECTION, 0x10) &&
						  this.W83782D_SetRegBits(W83782D_BANK0_MONITOR_CONTROL, 0x02)))
					{
						ret_code = JIDA_FUNCTION_FAILURE;
						Logger.Default.Log("Selezione sensore temperatura by Jada fallita " + Marshal.GetLastWin32Error().ToString(), Logger.LogType.CONSOLE_LOC_FILE,
							Logger.EventType.ERROR, ret_code);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					Logger.Default.Log("ECCEZIONE in 'SelectSensorTemperature': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		/// <summary>
		///     Configura registri di controllo gestione ventole
		/// </summary>
		/// <returns>In caso di errore restituisce un valore diverso da 0</returns>
		public int SetFansControlRegister()
		{
			// W83782D_FAN_BEEP_CONTROL          = 0x25: pin18=FAN3 control signal; pin19=FAN2 clock input; pin20=FAN1 clock input.
			// W83782D_BANK0_PWMOUT_CLOCK_SELECT = 0x19: PWMOUT1=clock 23.43 KHz; PWMOUT2=Enable + clock 23.43 KHz; 

			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					if (
						!(this.W83782D_SetRegister(W83782D_BANK_SELECTION, 0x00) && this.W83782D_SetRegister(W83782D_FAN_BEEP_CONTROL, 0x25) &&
						  this.W83782D_SetRegister(W83782D_BANK0_PWMOUT_CLOCK_SELECT, 0x19)))
					{
						ret_code = JIDA_FUNCTION_FAILURE;
						Logger.Default.Log("Impostazione controllo ventole by Jida fallita " + Marshal.GetLastWin32Error().ToString(), Logger.LogType.CONSOLE_LOC_FILE,
							Logger.EventType.ERROR, ret_code);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					Logger.Default.Log("ECCEZIONE in 'SetFansControlRegister': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per il Set del WatchDog
		/// 
		/// In caso di errore restituisce un valore non nullo.
		/// 
		/// \date [18.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		public int SetWatchDog(uint timeout)
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
                    //Logger.Default.Log(String.Format("SetWatchDog handle {0} timeout {1}", JidaHandle, timeout), Logger.LogType.CONSOLE_LOC_FILE);

                    bool bRetVal = JidaWDogSetConfig(JidaHandle, 0, timeout, 0, RebootType.JWDModeRebootPC);

					if (!bRetVal)
					{
						ret_code = JIDA_WATCHDOG_SET_FAILURE;
						string ret_errMsg = "Impostazione watchdog by Jida fallita";
						Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
					else
					{
						//String sLog = String.Format("WDog configurato correttamente con un timeout di {0:D} millisecondi", timeout);
						//Logger.Default.Log(sLog, Logger.LogType.CONSOLE_LOC_FILE);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'SetWatchDog': " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione di trigger Watch Dog
		/// 
		/// \date [21.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		public int TriggerWatchDog()
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					bool bRetVal = JidaWDogTrigger(JidaHandle, 0);

					if (!bRetVal)
					{
						ret_code = JIDA_WATCHDOG_TRIGGER_FAILURE;
						string ret_errMsg = "Refresh watchdog by Jida fallita";
						Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
					else
					{
						//String sLog = String.Format("WDog configurato correttamente con un timeout di {0:D} millisecondi", timeout);
						//Logger.Default.Log(sLog, Logger.LogType.CONSOLE_LOC_FILE);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'TriggerWatchDog': " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per regolare la velocità di rotazione della ventola destra
		/// dell'STLC1000
		/// 
		/// In caso di errore restituisce un valore non nullo.
		/// 
		/// \date [19.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		public int SetPWMRightFan(int pwmPercent)
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				Byte PWMValue; //PWMValue: valore che va da 0x00 (ventola spenta) a 0xFF (ventola accesa alla massima veloctà)

				try
				{
					if (pwmPercent < 0)
					{
						PWMValue = 0;
					}
					else if (pwmPercent > 100)
					{
						PWMValue = 255;
					}
					else
					{
						PWMValue = (Byte) ((255*pwmPercent)/100);
					}

					if (!(this.W83782D_SetRegister(W83782D_BANK_SELECTION, 0x00) && this.W83782D_SetRegister(W83782D_BANK0_PWMOUT1_CONTROL, PWMValue)))
					{
						ret_code = JIDA_WRITE_PWMDXFUN_REGISTER_FAILURE;
						Logger.Default.Log("Impostazione ventola dx by Jida fallita", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					Logger.Default.Log("ECCEZIONE in 'SetPWMRightFan': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per regolare la velocità di rotazione della ventola sinistra
		/// dell'STLC1000
		///  
		/// In caso di errore restituisce un valore non nullo.
		/// 
		/// \date [19.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		public int SetPWMLeftFan(int pwmPercent)
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				Byte PWMValue; //PWMValue: valore che va da 0x00 (ventola spenta) a 0xFF (ventola accesa alla massima veloctà)

				try
				{
					if (pwmPercent < 0)
					{
						PWMValue = 0;
					}
					else if (pwmPercent > 100)
					{
						PWMValue = 255;
					}
					else
					{
						PWMValue = (Byte) ((255*pwmPercent)/100);
					}

					if (!(this.W83782D_SetRegister(W83782D_BANK_SELECTION, 0x00) && this.W83782D_SetRegister(W83782D_BANK0_PWMOUT2_CONTROL, PWMValue)))
					{
						ret_code = JIDA_WRITE_PWMDXFUN_REGISTER_FAILURE;
						Logger.Default.Log("Impostazione ventola sx by Jida fallita", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					Logger.Default.Log("ECCEZIONE in 'SetPWMLeftFan': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per regolare ugualmente la velocità di rotazione di entrambe le
		/// ventole dell'STLC1000
		/// 
		/// In caso di errore restituisce un valore non nullo.
		/// 
		/// \date [19.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		public int SetPWMFans(int pwmPercent)
		{
			int ret_code = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				// Regolazione ventola destra
				try
				{
					ret_code = this.SetPWMRightFan(pwmPercent);
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'SetPWMFans' ventola dx: " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}

				// Regolazione ventola sinistra
				try
				{
					// riporto eventualmente l'errore che si verifica sulla ventola destra
					if (ret_code == NO_ERROR)
					{
						ret_code = this.SetPWMLeftFan(pwmPercent);
					}
					else
					{
						this.SetPWMLeftFan(pwmPercent);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'SetPWMFans' ventola sx: " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per rilevare la temperature in gradi Celsius della CPU dell'STLC1000
		/// 
		/// \date [21.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		public int GetCPUTemperature(out double temperature)
		{
			int ret_code = NO_ERROR;
			temperature = 0;


			if (STLCConfig.IsSTLCHost)
			{
				double dbTemperature;
				uint dwType = 0, dwSetting = 0, dwStatus = 0;

				try
				{
					if (!JidaTemperatureGetCurrent(JidaHandle, dwType, ref dwSetting, ref dwStatus))
					{
						ret_code = JIDA_FUNCTION_FAILURE;
						string ret_errMsg = "Lettura temperatura CPU fallita";
						ret_errMsg = ret_errMsg + " " + Marshal.GetLastWin32Error().ToString();
						Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
					}
					else
					{
						if (this.JidaTempInfo.dwType == JIDA_TEMP_CPU)
						{
							dbTemperature = (double) dwSetting;
							temperature = dbTemperature/1000.0;
							//Logger.Default.Log("CPU temp" + temperature.ToString(), Logger.LogType.CONSOLE_LOC_FILE);
						}
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'GetCPUTemperature': " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		//==============================================================================
		/// Funzione per rilevare la temperature in gradi Celsius della MotherBoard
		/// dell'STLC1000
		/// 
		/// \date [21.10.2011]
		/// \author Mario Ferro
		/// \version 0.01
		//------------------------------------------------------------------------------
		public int GetMBTemperature(out int temperature)
		{
			int ret_code = NO_ERROR;
			temperature = 0;

			if (STLCConfig.IsSTLCHost)
			{
				Byte byTemp = 0;

				try
				{
					if (!this.W83782D_GetRegister(W83782D_TEMPERATURE_SENSOR1_GET, out byTemp))
					{
						ret_code = JIDA_FUNCTION_FAILURE;
						Logger.Default.Log("Lettura temperatura MB fallita " + Marshal.GetLastWin32Error().ToString(), Logger.LogType.CONSOLE_LOC_FILE,
							Logger.EventType.ERROR, ret_code);
					}
					else
					{
						temperature = ((SByte) byTemp);
						//Logger.Default.Log("MB temp" + temperature.ToString(), Logger.LogType.CONSOLE_LOC_FILE);
					}
				}
				catch (Exception ex)
				{
					ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
					Logger.Default.Log("ECCEZIONE in 'GetMBTemperature': " + ex.GetType().ToString() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
						Logger.EventType.ERROR, ret_code);
				}
			}

			return ret_code;
		}

		public int UnloadJida()
		{
			int retCode = NO_ERROR;

			if (STLCConfig.IsSTLCHost)
			{
				try
				{
					if (!JidaDllUninitialize())
					{
						retCode = JIDA_UNINITIALIZED_FAILURE;
						string ret_errMsg = "UnloadJiDA fallita";
						Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
					}
				}
				catch (Exception ex)
				{
					retCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
					string ret_errMsg = "ECCEZIONE in 'UnloadJida': " + ex.GetType().ToString() + " - " + ex.StackTrace;
					Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
				}
			}

			return retCode;
		}

		public int GetSerialNumber(out string sSN, out string resultDesc)
		{
			int ret_code = NO_ERROR;
			sSN = "";
			resultDesc = "Operazione eseguita con successo";

			try
			{
				if (STLCConfig.IsSTLCHost)
				{
					int iSN = 0;
					byte DEVICE_I2C_UNKNOW_ADDRESS = 0xAC;
					uint JIDA_TYPE_UNOKNOWN = 0;

					//short iReg = 0x27;           
					Byte byTemp = 0;
					Byte byAddr = (Byte) (DEVICE_I2C_UNKNOW_ADDRESS & 0xFE);
					uint dwType = JIDA_TYPE_UNOKNOWN;
					int dwTemp = 0;

					int iReg;
					for (iReg = 0; iReg < sizeof (int); iReg++)
					{
						if (!JidaI2CReadRegister(JidaHandle, dwType, byAddr, (short) iReg, ref byTemp))
						{
							dwTemp = 0;
							ret_code = JIDA_FUNCTION_FAILURE;
							string ret_errMsg = "JidaI2CReadRegister fallita";
							ret_errMsg = ret_errMsg + " " + Marshal.GetLastWin32Error().ToString();
							resultDesc = ret_errMsg;
							Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
						}
						else
						{
							dwTemp |= (byTemp << (24 - 8*iReg));
						}
					}

					iSN = dwTemp;
					sSN = this.SerialNumberToString(iSN);
				}
				else
				{
					string sSNTemp = this.GetForcedServerIdFromConfig();

					if (String.IsNullOrEmpty(sSNTemp))
					{
						sSN = STLCNetwork.GetSerialNumberFromFirstEthernetAdapterMacAddress();
					}
					else
					{
						sSN = sSNTemp;
					}
				}
			}
			catch (Exception ex)
			{
				ret_code = AppCfg.E_COD_FUNCTION_EXCEPTION;
				string ret_errMsg = "ECCEZIONE in 'GetSerialNumber': " + ex.GetType().ToString() + " - " + ex.StackTrace;
				resultDesc = ret_errMsg;
				Logger.Default.Log(ret_errMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
			}

			return ret_code;
		}

		internal string GetForcedServerIdFromConfig()
		{
            return AppCfg.Default.sForcedServerID; // this.GetConfigValue(STLCSystemReference.FORCED_SERVER_ID, "");
		}

		private string SerialNumberToString(int srvID)
		{
			byte byAnno = (byte) (srvID >> 24);
			byte byWeek = (byte) (srvID >> 16);
			ushort wProg = (ushort) srvID;

			return String.Format("{0:00}.{1:00}.{2:000}", byAnno, byWeek, wProg);
		}

		private string GetConfigValue(string configKey, string defaultValue)
		{
			string value = ConfigurationManager.AppSettings[configKey];
			if (string.IsNullOrEmpty(value))
			{
				return defaultValue;
			}
			return value;
		}
	}
}
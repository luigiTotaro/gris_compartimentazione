﻿using System;
using System.ServiceProcess;
using STLCManager.Service.Engine;

namespace STLCManager.Service
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>

        public static STLCEngine engine;

        static void Main()
        {
            if (!Environment.UserInteractive)
            {
                //MODALITA' SERVIZIO
                ServiceBase[] ServicesToRun =
                { 
                    new STLCManagerService() 
                };
                ServiceBase.Run(ServicesToRun);
            }
            else
            {
                //MODALITA' CONSOLE
                engine = new STLCEngine();
                engine.STLCEnable();
                Console.ReadLine();
            }
        }
    }
}

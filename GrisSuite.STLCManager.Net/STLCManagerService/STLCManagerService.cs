﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using STLCManager.Service.Engine;

namespace STLCManager.Service
{
    public partial class STLCManagerService : ServiceBase
    {
        public const int NO_ERROR = AppCfg.COD_NO_ERROR;

        public STLCManagerService()
        {
            Thread.Sleep(2000); // Attesa di 2 sec per consentire lo shutdown completo del servizio in caso sid restart
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Thread th = new Thread(() =>
			{

				Program.engine = new STLCEngine();
				Program.engine.STLCEnable();

			});
			th.Start();
        }

        protected override void OnStop()
        {
            Logger.Default.Log("STLCManager service STOPPING ...", Logger.LogType.CONSOLE_LOC_FILE);

            int retCode = Program.engine.CloseService(true);

            if (retCode == NO_ERROR)
            {
                Logger.Default.Log("STLCManager service STOPPED", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.WARNING, AppCfg.I_COD_SERVICE_MANAGER_STOPPED); // se diverso da stlc1000 diventa notice
            }
            else
            {
                Logger.Default.Log("Arresto servizio STLCManager avvenuto con errori", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }
        }
    }
}

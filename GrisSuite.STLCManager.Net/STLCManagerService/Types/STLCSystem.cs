﻿using System;
using System.Collections.Generic;
using System.Net;

namespace STLCManager.Service.Types
{
    /// <summary>
    ///     Descrive il nome del sistema
    /// </summary>
    public class STLCSystemReference
    {
        #region Token

        /// <summary>
        ///     Definisce nodo ed attributo file xml per la definizione dell'HostName
        /// </summary>
        public const string hostXMLNode = "//telefin/system/server";
        public const string hostXMLAttribute = "host";

        public const string hardwareProfileXMLAttribute = "hardwareProfile";

        #endregion

        #region Costanti pubbliche

        public const string HARDWARE_PROFILE_CONFIG_ATTRIBUTE_NAME  = "HardwareProfile";
        public const string FORCED_SERVER_ID                        = "ForcedServerID";
        public const string FORCED_STLC_VERSION                     = "ForcedSTLCVersion";

        #endregion

        #region Members

        public string HostName { get; set; }
        public bool ToSet { get; set; }

        #endregion

        #region Constructors

        public STLCSystemReference()
        {
            this.HostName = null;
            this.ToSet = false;
        }

        #endregion

        #region Accessor

        /// <value>
        ///     Ottiene l'identificativo del nodo server.
        /// </value>
        /// <remarks>
        ///     Ottiene il valore del campo
        ///     <see cref="hostXMLNode" />.
        /// </remarks>
        public string HostXMLNode
        {
            get { return hostXMLNode; }
        }

        /// <value>
        ///     Ottiene l'identificativo dell'attributo host.
        /// </value>
        /// <remarks>
        ///     Ottiene il valore del campo
        ///     <see cref="HostXMLAttribute" />.
        /// </remarks>
        public string HostXMLAttribute
        {
            get { return hostXMLAttribute; }
        }

        /// <value>
        ///     Ottiene il nome dell'attributo hardwareProfile.
        /// </value>
        /// <remarks>
        ///     Ottiene il valore del campo
        ///     <see cref="HardwareProfileXMLAttribute" />.
        /// </remarks>
        public string HardwareProfileXMLAttribute
        {
            get { return hardwareProfileXMLAttribute; }
        }

        #endregion
    }

    /// <summary>
    ///     Definisce gli attributi della porta ethernet
    /// </summary>
    public enum xmlNetworkAttributes
    {
        name,
        type,
        port,
        ip,
        ip2,
        ip3,
        ip4,
        subnet,
        subnet2,
        subnet3,
        subnet4,
        gateway,
        dns1,
        dns2
    };

    /// <summary>
    ///     Descrive la singola porta ethernet
    /// </summary>
    public class STLCNetworkReference : STLCNetwork
    {
        #region Members

        //parametri caricati da file xml
        private string[] attributeValue = new string[Enum.GetValues(typeof(xmlNetworkAttributes)).Length];

        //segnala se la porta ethernet deve essere configurata
        public bool ToSet { get; set; }

        #endregion

        #region Constructors

        public STLCNetworkReference()
        {
            for (int i = 0; i < this.attributeValue.Length; i++)
            {
                this.attributeValue[i] = null;
            }

            this.ToSet = false;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Imposta valore attributo su array di stringhe.
        /// </summary>
        /// <param name="index">Identificativo attributo.</param>
        /// <param name="str">Valore attributo.</param>
        public void AttributeValue(xmlNetworkAttributes index, string str)
        {
            this.attributeValue[(int)index] = str;
        }

        /// <summary>
        ///     Carica i membri di definizione porta ethernet, verifica congruenza dei dati
        /// </summary>
        /// <remarks>
        ///     Genera eccezione se i dati non sono validi.
        /// </remarks>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: nodo di configurazione porta ethernet.</item>
        ///         <item>false: altro tipo di nodo.</item>
        ///     </list>
        /// </returns>
        public bool LoadConfigFromStrings()
        {
            string type = this.attributeValue[(int)xmlNetworkAttributes.type];

            if (type != "ETH")
            {
                return false;
            }

            this.Name = type + int.Parse(this.attributeValue[(int)xmlNetworkAttributes.port]);
            this.IPEnabled = true;

            if (this.attributeValue[(int)xmlNetworkAttributes.ip] == "auto")
            {
                this.DHCPEnabled = true;
            }
            else
            {
                this.DHCPEnabled = false;

                this.IPAddresses.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.ip]));
            }

            bool isIp2Set = false;
            bool isIp3Set = false;
            bool isIp4Set = false;

            if (!this.DHCPEnabled && !string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.ip2]))
            {
                this.IPAddresses.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.ip2]));
                isIp2Set = true;
            }

            if (!this.DHCPEnabled && !string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.ip3]))
            {
                this.IPAddresses.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.ip3]));
                isIp3Set = true;
            }

            if (!this.DHCPEnabled && !string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.ip4]))
            {
                this.IPAddresses.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.ip4]));
                isIp4Set = true;
            }

            if (!this.DHCPEnabled && !string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.subnet]))
            {
                this.SubnetMasks.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.subnet]));
            }

            if (!this.DHCPEnabled && isIp2Set)
            {
                if (string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.subnet2]))
                {
                    this.SubnetMasks.Add(IPAddress.Parse("255.255.255.0"));
                }
                else
                {
                    this.SubnetMasks.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.subnet2]));
                }
            }

            if (!this.DHCPEnabled && isIp3Set)
            {
                if (string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.subnet3]))
                {
                    this.SubnetMasks.Add(IPAddress.Parse("255.255.255.0"));
                }
                else
                {
                    this.SubnetMasks.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.subnet3]));
                }
            }

            if (!this.DHCPEnabled && isIp4Set)
            {
                if (string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.subnet4]))
                {
                    this.SubnetMasks.Add(IPAddress.Parse("255.255.255.0"));
                }
                else
                {
                    this.SubnetMasks.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.subnet4]));
                }
            }

            if (!string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.gateway]))
            {
                this.IPGateways.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.gateway]));
            }

            if (!string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.dns1]))
            {
                this.DNSServers.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.dns1]));
            }

            if (!string.IsNullOrEmpty(this.attributeValue[(int)xmlNetworkAttributes.dns2]))
            {
                this.DNSServers.Add(IPAddress.Parse(this.attributeValue[(int)xmlNetworkAttributes.dns2]));
            }
            return true;
        }

        #endregion
    }

    /// <summary>
    ///     Descrive lista porte ethernet
    /// </summary>
    public class STLCNetworkList : List<STLCNetworkReference>
    {
        #region Token

        /// <summary>
        ///     Definisce nodo file xml per la definizione delle porte ethernet
        /// </summary>
        public static string ethernetXMLNode = "//telefin/network";

        #endregion

        #region Accessor

        /// <value>
        ///     Ottiene l'identificativo del nodo network.
        /// </value>
        /// <remarks>
        ///     Ottiene il valore del campo
        ///     <see cref="ethernetXMLNode" />.
        /// </remarks>
        public string EthernetXMLNode
        {
            get { return ethernetXMLNode; }
        }

        /// <value>
        ///     Imposta ed ottiene il descrittore della porta ethernet.
        /// </value>
        /// <remarks>
        ///     Accede all'i-esimo elemento della lista.
        /// </remarks>
        public new STLCNetworkReference this[int index]
        {
            get
            {
                if (index < this.Count)
                {
                    return base[index];
                }
                return null;
            }
            set
            {
                if (index < this.Count)
                {
                    base[index] = value;
                }
            }
        }

        #endregion
    }
}
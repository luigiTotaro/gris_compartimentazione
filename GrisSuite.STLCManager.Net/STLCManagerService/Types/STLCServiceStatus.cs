﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceProcess;
using System.Runtime.Serialization;

namespace STLCManager.Service.Types
{
    //==============================================================================
    /// Struttura dati per il monitoraggio deloo stato dei servizi.
    ///
    /// \date [07.12.2011]
    /// \author Mario Ferro
    /// \version 0.01
    //------------------------------------------------------------------------------
    [DataContract]
    public class STLCServiceStatus
    {
        [DataMember]
        public string Name { get; set; }                            // Nome del servizio da controllare
        [DataMember]
        public string DisplayName { get; set; }                     // Display name del servizio da controllare
        [DataMember]
        public ServiceControllerStatus Status { get; set; }         // Stato del servizio da controllare
        [DataMember]
        public ServiceStartMode StartMode	{ get; set; }           // Modalità di avvio del servizio
	  
        public STLCServiceStatus()
        {
            Name = "";
            DisplayName = "";
            Status = ServiceControllerStatus.Stopped;
            StartMode = ServiceStartMode.Disabled;
        }       
    }
}

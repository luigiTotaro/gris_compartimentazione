﻿using System;

// Convert Unix time to C# or .Net DateTime and vice versa. 
// The unix time is the difference between the wanted DateTime value with the DateTime value of 1st January 1970. Here is the code in C#:
namespace STLCManager.Service.Types
{
    struct STLCUnixTime
    {
        private static DateTime BEGIN_UTC = new DateTime(1970, 1, 1, 0, 0, 0);
        private long utValue;
        //private DateTime dtValue;

        public STLCUnixTime(long seconds)
        {
            utValue = seconds;
        }
        
        public long Value
        {
            get { return utValue; }
            set { utValue = value; }
        }

        public DateTime DateTime
        {
            get { return BEGIN_UTC.AddSeconds((long)utValue).ToLocalTime(); }
            set { utValue = (long) ((TimeSpan)(value - BEGIN_UTC)).TotalSeconds; }
        }

        public override string ToString()
        {
            return DateTime.ToString("yyy-MM-dd HH:mm:ss"); ;
        }
    }
}



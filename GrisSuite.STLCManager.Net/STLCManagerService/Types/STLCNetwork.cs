﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.Serialization;
using STLCManager.Service.Function;

namespace STLCManager.Service.Types
{
	[DataContract]
	public class STLCNetwork
	{
		#region Members

		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public UInt32 DevId { get; set; }

		[DataMember]
		public bool IPEnabled { get; set; }

		[DataMember]
		public bool DHCPEnabled { get; set; }

		[DataMember]
		public List<IPAddress> IPAddresses { get; set; }

		[DataMember]
		public List<IPAddress> SubnetMasks { get; set; }

		[DataMember]
		public List<IPAddress> IPGateways { get; set; }

		[DataMember]
		public List<IPAddress> DNSServers { get; set; }

		[DataMember]
		public List<UInt16> Metrics { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		///     Costruttore classe STLCNetwork. Imposta parametri al valore di default
		/// </summary>
		public STLCNetwork()
		{
			this.Name = null;
			this.DevId = 0;

			this.IPEnabled = false;
			this.DHCPEnabled = false;

			this.IPAddresses = new List<IPAddress>();
			this.SubnetMasks = new List<IPAddress>();
			this.IPGateways = new List<IPAddress>();
			this.DNSServers = new List<IPAddress>();

			this.Metrics = new List<UInt16>();
		}

		/// <summary>
		///     Costruttore classe STLCNetwork. Imposta parametri con dati della porta ethernet di cui è fornito il nome
		/// </summary>
		/// <param name="name">Nome porta ethernet</param>
		public STLCNetwork(string name) : this()
		{
			UInt32 devid;
			STLCNetwork network = this;

			if (!WMIFunction.Default.GetNetworkID(name, out devid))
			{
				throw new ArgumentException(name + " nome porta non valido.");
			}

			network.DevId = devid;
			network.Name = name;

			if (!WMIFunction.Default.GetNetworkConfig(ref network))
			{
				throw new ArgumentException(name + " errore lettura configurazione porta.");
			}
		}

		#endregion

		#region Methods

		/// <summary>
		///     Restituisce l'elenco delle interfacce di rete di tipo Ethernet che sono connesse alla rete
		/// </summary>
		/// <returns>Elenco dei nomi delle schede</returns>
		/// <remarks>
		///     Visto che questo elenco è poi usato dalle funzioni WMI, si filtrano solo le schede connesse (con cavo di rete),
		///     dato che quelle non connesse non sono gestibili da WMI
		/// </remarks>
		public static string[] GetEthernetInterfacesNameEnabled()
		{
			List<string> ethernetInterfacesNameEnabled = new List<string>();

			foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
			{
				if ((adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet) && (adapter.OperationalStatus == OperationalStatus.Up))
				{
					ethernetInterfacesNameEnabled.Add(adapter.Name);
				}
			}

			return ethernetInterfacesNameEnabled.ToArray();
		}

		/// <summary>
		///     Restituisce un numero di serie calcolato dagli ultimi tre byte della prima scheda di rete via cavo e connessa
		/// </summary>
		/// <returns>
		///     Numero di serie nel formato nnn.nnn.nnn (terzultimo, penultimo e ultimo byte del MAC address della prima
		///     scheda di rete)
		/// </returns>
		/// <remarks>
		///     Visto che questo elenco è poi usato dalle funzioni WMI, si filtrano solo le schede connesse (con cavo di rete),
		///     dato che quelle non connesse non sono gestibili da WMI
		/// </remarks>
		public static string GetSerialNumberFromFirstEthernetAdapterMacAddress()
		{
			string serialNumber = string.Empty;

			foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
			{
				if ((adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet) && (adapter.OperationalStatus == OperationalStatus.Up))
				{
					byte[] macAddressBytes = adapter.GetPhysicalAddress().GetAddressBytes();
					if (macAddressBytes.Length > 3)
					{
						serialNumber = GetEncodedSerialFromBytes(macAddressBytes[macAddressBytes.Length - 3], macAddressBytes[macAddressBytes.Length - 2],
							macAddressBytes[macAddressBytes.Length - 1]);
						break;
					}
				}
			}

			return serialNumber;
		}

		private static string GetEncodedSerialFromBytes(byte byte1, byte byte2, byte byte3)
		{
			uint encodedSerialNumber = ((((uint) (byte1 << 8) | byte2) << 15) | byte3) | 0x400;

			return String.Format("{0:0}.{1:0}.{2:0}", ((encodedSerialNumber & 0xFF000000) >> 24), ((encodedSerialNumber & 0x00FF0000) >> 16),
				(encodedSerialNumber & 0x0000FFFF));
		}

		/// <summary>
		///     Stampa informazioni di debug su CONSOLE.
		/// </summary>
		/// <param name="msg">Eventuale intestazione</param>
		public void Debug(string msg)
		{
			if (!String.IsNullOrEmpty(msg))
			{
				Logger.Default.Log(msg, Logger.LogType.CONSOLE);
			}

			Logger.Default.Log(" port=" + this.Name, Logger.LogType.CONSOLE);
			Logger.Default.Log("  dhcp=" + (this.DHCPEnabled ? "enabled" : "disabled"), Logger.LogType.CONSOLE);

			foreach (IPAddress ip in this.IPAddresses)
			{
				Logger.Default.Log("  ip=" + ip, Logger.LogType.CONSOLE);
			}

			foreach (IPAddress subnet in this.SubnetMasks)
			{
				Logger.Default.Log("  subnet=" + subnet, Logger.LogType.CONSOLE);
			}

			foreach (IPAddress gateway in this.IPGateways)
			{
				Logger.Default.Log("  gateway=" + gateway, Logger.LogType.CONSOLE);
			}

			foreach (IPAddress dns in this.DNSServers)
			{
				Logger.Default.Log("  dns=" + dns, Logger.LogType.CONSOLE);
			}

			Logger.Default.Log("  devid=" + this.DevId, Logger.LogType.CONSOLE);
		}

		public bool Compare(STLCNetwork network)
		{
			if (this.DHCPEnabled != network.DHCPEnabled)
			{
				return false;
			}

			//Gateway
			if (this.IPGateways.Count != network.IPGateways.Count)
			{
				return false;
			}
			for (int i = 0; i < this.IPGateways.Count; i++)
			{
				if (!this.IPGateways[i].Equals(network.IPGateways[i]))
				{
					return false;
				}
			}

			if (!this.DHCPEnabled)
			{
				//IP
				if (this.IPAddresses.Count != network.IPAddresses.Count)
				{
					return false;
				}
				for (int i = 0; i < this.IPAddresses.Count; i++)
				{
					if (!this.IPAddresses[i].Equals(network.IPAddresses[i]))
					{
						return false;
					}
				}

				//Subnet
				if (this.SubnetMasks.Count != network.SubnetMasks.Count)
				{
					return false;
				}
				for (int i = 0; i < this.SubnetMasks.Count; i++)
				{
					if (!this.SubnetMasks[i].Equals(network.SubnetMasks[i]))
					{
						return false;
					}
				}

				//DNS
				if (this.DNSServers.Count != network.DNSServers.Count)
				{
					return false;
				}
				for (int i = 0; i < this.DNSServers.Count; i++)
				{
					if (!this.DNSServers[i].Equals(network.DNSServers[i]))
					{
						return false;
					}
				}
			}
			return true;
		}

        public static string GetNetworkInterfaceIP(string networkInterfaceName, out string physicalAddress)
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            if (nics.Length > 0)
            {
                foreach (NetworkInterface adapter in nics)
                {
                    if (adapter.Name.Equals(networkInterfaceName, StringComparison.OrdinalIgnoreCase))
                    {
                        physicalAddress = adapter.GetPhysicalAddress().ToString();
                        IPInterfaceProperties properties = adapter.GetIPProperties();
                        if (properties.UnicastAddresses.Count > 0)
                        {
                            foreach (var address in properties.UnicastAddresses)
                            {
                                if (address.Address.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    return address.Address.ToString();
                                }
                            }
                        }
                    }
                }
            }

            physicalAddress = string.Empty;
            return string.Empty;
        }

		#endregion
	}
}
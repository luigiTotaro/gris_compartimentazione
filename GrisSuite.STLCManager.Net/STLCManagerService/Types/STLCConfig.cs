﻿
namespace STLCManager.Service.Types
{
    /// <summary>
    /// Classe di definizione configurazione generale sistema STLC.
    /// </summary>
    public class STLCConfig
    {
        public static bool              IsSTLCHost              = (AppCfg.Default.hpHardwareProfile == HardwareProfile.STLC);
		public static HardwareProfile   CurrentHardwareProfile  =  AppCfg.Default.hpHardwareProfile;

        public bool IsWatchDogActive { get; set; }

        public bool IsSystemLedActive { get; set; }
        public bool IsSystemLedRunning { get; set; }

        public bool IsFanActive { get; set; }
        public bool IsFanRunning { get; set; }

        public bool IsFBWFActive { get; set; }
        public bool IsFBWFRunning { get; set; }

        public bool IsServiceActive { get; set; }
        public bool IsServiceRunning { get; set; }

        public bool RebootOnFailure { get; set; }

        public uint MaintenanceModeTimeout { get; set; }

        /// <summary>
        /// Inizializza nuova istanza della classe STLCConfig per la definizione della configurazione generale.
        /// </summary>
        public STLCConfig()
        {
            IsWatchDogActive = false;

            IsSystemLedActive = false;
            IsSystemLedRunning = false;

            IsFanActive = false;
            IsFanRunning = false;

            IsFBWFActive = false;
            IsFBWFRunning = false;

            IsServiceActive = false;
            IsServiceRunning = false;

            RebootOnFailure = false;
            
            MaintenanceModeTimeout = 1;
        }
    }
}

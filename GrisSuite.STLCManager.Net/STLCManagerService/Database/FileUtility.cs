﻿using System;
using System.IO;
using System.Text;

namespace STLCManager.Service.Database
{
	public static class FileUtility
	{
		#region Metodi pubblici

        /// <summary>
		///     Verifica se un file esiste su disco e se può essere aperto e letto
		/// </summary>
		/// <param name="fileName">Nome del file completo</param>
		/// <returns>True se il file esiste e può essere correttamente letto. False altrimenti.</returns>
		public static bool CheckFileCanRead(string fileName)
		{
			bool returnValue;

			FileStream fs = null;
			try
			{
				if (!File.Exists(fileName))
				{
					returnValue = false;
				}
				else
				{
					fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
					returnValue = fs.CanRead;
				}
			}
			catch (UnauthorizedAccessException)
			{
				returnValue = false;
			}
			catch (IOException)
			{
				returnValue = false;
			}
			finally
			{
				if (fs != null)
				{
					fs.Close();
				}
			}

			return returnValue;
		}

		/// <summary>
		///     Legge un file da disco
		/// </summary>
		/// <param name="fileName">Nome del file completo</param>
		/// <param name="fileEncoding">Encoding del file da caricare</param>
		/// <returns>Stringa che contiene il contenuto del file. Null nel caso non possa essere letto</returns>
		public static string ReadFile(string fileName, Encoding fileEncoding)
		{
			string file = null;

			if (fileEncoding != null)
			{
				FileStream fs = null;
				try
				{
					fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
					byte[] fileData = new byte[fs.Length];

					if (fs.Read(fileData, 0, (int) fs.Length) > 0)
					{
						file = fileEncoding.GetString(fileData);
					}
				}
				catch (UnauthorizedAccessException)
				{
				}
				catch (IOException)
				{
				}
				finally
				{
					if (fs != null)
					{
						fs.Close();
					}
				}
			}

			return file;
		}

		#endregion
	}
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Xml;
using STLCManager.Service.Function;
using STLCManager.Service.Properties;
using STLCManager.Service.Types;

namespace STLCManager.Service.Database
{
    /// <summary>
    ///     Rappresenta la classe di utility per l'accesso al database
    /// </summary>
    public static class SqlDataBaseUtility
    {
        /// <summary>
        ///     Aggiorna lo stato della periferica
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <param name="severityLevel">Livello di severità del Device</param>
        /// <param name="descriptionStatus">Stato del Device</param>
        /// <param name="offline">Indica se il Device è fuori linea o non raggiungibile</param>
        /// <param name="shouldSendNotificationByEmail">Indica se inviare mail di notifica</param>
        public static void UpdateDeviceStatus(string dbConnectionString,
            long deviceId,
            int severityLevel,
            string descriptionStatus,
            Byte offline,
            Byte shouldSendNotificationByEmail)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.UpdateDeviceStatus' - La stringa di connessione al database non è valida: (" + ex.GetType() +
                        ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF EXISTS (SELECT [DevID] FROM [device_status] WHERE [DevID] = @DevID) BEGIN UPDATE [device_status] SET [SevLevel] = @SevLevel, [Description] = @Description, [Offline] = @Offline, [ShouldSendNotificationByEmail] = @ShouldSendNotificationByEmail WHERE [DevID] = @DevID; END ELSE BEGIN INSERT INTO [device_status] ([DevID], [SevLevel], [Description], [Offline], [ShouldSendNotificationByEmail]) VALUES (@DevID, @SevLevel, @Description, @Offline, @ShouldSendNotificationByEmail); END";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@SevLevel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, severityLevel));
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, descriptionStatus));
                cmd.Parameters.Add(new SqlParameter("@Offline", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, offline));
                cmd.Parameters.Add(new SqlParameter("@ShouldSendNotificationByEmail", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3,
                    null, DataRowVersion.Current, shouldSendNotificationByEmail));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.UpdateDeviceStatus' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }
        }

        /// <summary>
        ///     Verifica l'esistenza di una periferica in base dati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id del Device</param>
        /// <returns>
        ///     Ritorna null nel caso ci sia un errore nell'accesso alla base dati o nell'esecuzione del comando, true se la
        ///     periferica esiste, false altrimenti
        /// </returns>
        public static bool? CheckDeviceExistence(string dbConnectionString, long deviceId)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDevices = null;
            bool? deviceExists;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.CheckDeviceExistence' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);
                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT DevID FROM [devices] WHERE (DevID = @DevID)";
                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, deviceId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDevices = cmd.ExecuteReader();

                if (drDevices.Read())
                {
                    int colDevID = drDevices.GetOrdinal("DevID");

                    do
                    {
                        if (drDevices.IsDBNull(colDevID))
                        {
                            deviceExists = false;
                        }
                        else
                        {
                            long deviceID = drDevices.GetSqlInt64(colDevID).Value;

                            deviceExists = deviceID > 0;
                        }
                    } while (drDevices.Read());
                }
                else
                {
                    deviceExists = false;
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.CheckDeviceExistence' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                deviceExists = null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.UpdateDeviceStatus' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
                deviceExists = null;
            }
            catch (ThreadAbortException)
            {
                deviceExists = null;
            }
            finally
            {
                if (drDevices != null && !drDevices.IsClosed)
                {
                    drDevices.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deviceExists;
        }

        /// <summary>
        ///     Recupera i dati di lookup per poter creare una periferica su database
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="location">Dati della topografia della periferica</param>
        /// <param name="regionId">ID del Compartimento</param>
        /// <param name="regionName">Nome del Compartimento</param>
        /// <param name="zoneId">ID della Zona</param>
        /// <param name="zoneName">Nome della Zona</param>
        /// <param name="nodeId">ID del Nodo</param>
        /// <param name="nodeName">Nome del Nodo</param>
        /// <param name="serverId">ID del Server da System.xml</param>
        /// <param name="serverHost">Host che monitora la periferica</param>
        /// <param name="serverName">Nome del Server che monitora la periferica</param>
        /// <param name="port">Porta da System.xml</param>
        /// <returns>Dati di lookup con l'Id di Stazione, Fabbricato, Armadio e Interfaccia</returns>
        public static DeviceDatabaseSupport GetStationBuildingRackPortData(string dbConnectionString,
            TopographyLocation location,
            long regionId,
            string regionName,
            long zoneId,
            string zoneName,
            long nodeId,
            string nodeName,
            int serverId,
            string serverHost,
            string serverName,
            SystemPort port)
        {
            DeviceDatabaseSupport deviceDatabaseSupport = null;

            if (location != null)
            {
                SqlConnection dbConnectionCreate = null;
                SqlCommand cmdCreate = new SqlCommand();
                SqlDataReader drDeviceDatabaseSupportCreate = null;

                try
                {
                    try
                    {
                        dbConnectionCreate = new SqlConnection(dbConnectionString);
                    }
                    catch (ArgumentException ex)
                    {
                        Logger.Default.Log(
                            "ECCEZIONE in 'SqlDatabaseUtility.GetStationBuildingRackPortData' - La stringa di connessione al database non è valida: (" +
                            ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                            Logger.E_STACK_TRACE);
                        return null;
                    }

                    // La topografia sarà inserita o aggiornata in modo multiplo per ogni device, ma questo è l'unico modo che garantisce di avere
                    // sempre il database corretto e di poter procedere alle cancellazioni di oggetti non referenziati
                    Guid stationId = InsertStation(dbConnectionString, location.StationId, location.StationName, 0);
                    if (stationId != Guid.Empty)
                    {
                        Guid buildingId = InsertBuilding(dbConnectionString, location.BuildingId, location.BuildingName, location.BuildingNotes,
                            stationId, 0);

                        if (buildingId != Guid.Empty)
                        {
                            Guid rackId = InsertRack(dbConnectionString, location.LocationId, location.LocationName, location.LocationType,
                                location.LocationNotes, buildingId, 0);

                            if (rackId != Guid.Empty)
                            {
                                serverId = InsertServer(dbConnectionString, serverId, regionId, regionName, 0, zoneId, zoneName, 0, nodeId,
                                    nodeName, 0, serverHost, serverName, 0);

                                if (serverId > 0)
                                {
                                    Guid portId = InsertPort(dbConnectionString, port.Id, port.Name, port.Type, port.Parameters, port.Status,
                                        serverId, 0);

                                    if (portId != Guid.Empty)
                                    {
                                        cmdCreate.CommandType = CommandType.Text;
                                        cmdCreate.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                                        cmdCreate.CommandText =
                                            "DECLARE @StationID uniqueidentifier; DECLARE @BuildingID uniqueidentifier; DECLARE @RackID uniqueidentifier; DECLARE @PortID uniqueidentifier; SELECT TOP (1) @StationID = station.StationID, @BuildingID = building.BuildingID, @RackID = rack.RackID FROM station INNER JOIN building ON station.StationID = building.StationID INNER JOIN rack ON building.BuildingID = rack.BuildingID WHERE (station.StationName = @StationName) AND (building.BuildingName = @BuildingName) AND (building.BuildingXMLID = @BuildingXMLID) AND (building.BuildingDescription = @BuildingDescription) AND (rack.RackName = @RackName) AND (rack.RackXMLID = @RackXMLID) AND (rack.RackType = @RackType) AND (rack.RackDescription = @RackDescription) AND (building.Removed = 0) AND (rack.Removed = 0) AND (station.Removed = 0); SELECT TOP (1) @PortID = port.PortID FROM port WHERE (SrvID = @SrvID) AND (PortXMLID = @PortXMLID) SELECT @StationID AS StationID, @BuildingID AS BuildingID, @RackID AS RackID, @PortID AS PortID";
                                        cmdCreate.Parameters.Add(new SqlParameter("@StationName", SqlDbType.VarChar, 64, ParameterDirection.Input,
                                            false, 0, 0, null, DataRowVersion.Current, location.StationName));
                                        cmdCreate.Parameters.Add(new SqlParameter("@BuildingName", SqlDbType.VarChar, 64, ParameterDirection.Input,
                                            false, 0, 0, null, DataRowVersion.Current, location.BuildingName));
                                        cmdCreate.Parameters.Add(new SqlParameter("@BuildingXMLID", SqlDbType.Int, 4, ParameterDirection.Input,
                                            false, 0, 10, null, DataRowVersion.Current, location.BuildingId));
                                        cmdCreate.Parameters.Add(new SqlParameter("@RackName", SqlDbType.VarChar, 64, ParameterDirection.Input,
                                            false, 0, 0, null, DataRowVersion.Current, location.LocationName));
                                        cmdCreate.Parameters.Add(new SqlParameter("@RackXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false,
                                            0, 10, null, DataRowVersion.Current, location.LocationId));
                                        cmdCreate.Parameters.Add(new SqlParameter("@RackType", SqlDbType.VarChar, 64, ParameterDirection.Input,
                                            false, 0, 0, null, DataRowVersion.Current, location.LocationType));
                                        cmdCreate.Parameters.Add(new SqlParameter("@RackDescription", SqlDbType.VarChar, 256,
                                            ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.LocationNotes));
                                        cmdCreate.Parameters.Add(new SqlParameter("@BuildingDescription", SqlDbType.VarChar, 256,
                                            ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current, location.BuildingNotes));
                                        cmdCreate.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0,
                                            10, null, DataRowVersion.Current, serverId));
                                        cmdCreate.Parameters.Add(new SqlParameter("@PortXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false,
                                            0, 10, null, DataRowVersion.Current, port.Id));

                                        cmdCreate.Connection = dbConnectionCreate;

                                        dbConnectionCreate.Open();

                                        drDeviceDatabaseSupportCreate = cmdCreate.ExecuteReader();

                                        if (drDeviceDatabaseSupportCreate.Read())
                                        {
                                            int colStationId = drDeviceDatabaseSupportCreate.GetOrdinal("StationID");
                                            int colBuildingId = drDeviceDatabaseSupportCreate.GetOrdinal("BuildingID");
                                            int colRackId = drDeviceDatabaseSupportCreate.GetOrdinal("RackID");
                                            int colPortId = drDeviceDatabaseSupportCreate.GetOrdinal("PortID");

                                            do
                                            {
                                                if ((!drDeviceDatabaseSupportCreate.IsDBNull(colStationId)) &&
                                                    (!drDeviceDatabaseSupportCreate.IsDBNull(colBuildingId)) &&
                                                    (!drDeviceDatabaseSupportCreate.IsDBNull(colRackId)) &&
                                                    (!drDeviceDatabaseSupportCreate.IsDBNull(colPortId)))
                                                {
                                                    deviceDatabaseSupport =
                                                        new DeviceDatabaseSupport(drDeviceDatabaseSupportCreate.GetSqlGuid(colStationId).Value,
                                                            drDeviceDatabaseSupportCreate.GetSqlGuid(colBuildingId).Value,
                                                            drDeviceDatabaseSupportCreate.GetSqlGuid(colRackId).Value,
                                                            drDeviceDatabaseSupportCreate.GetSqlGuid(colPortId).Value);
                                                }
                                            } while (drDeviceDatabaseSupportCreate.Read());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetStationBuildingRackPortData' - Errore durante l'accesso alla base dati: (" + ex.GetType() +
                        ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                }
                catch (IndexOutOfRangeException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetStationBuildingRackPortData' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);
                }
                finally
                {
                    if (drDeviceDatabaseSupportCreate != null && !drDeviceDatabaseSupportCreate.IsClosed)
                    {
                        drDeviceDatabaseSupportCreate.Close();
                    }

                    if (dbConnectionCreate != null)
                    {
                        dbConnectionCreate.Close();
                    }

                    cmdCreate.Dispose();
                }
            }

            return deviceDatabaseSupport;
        }

        /// <summary>
        ///     Aggiorna o inserisce i dati della periferica su database
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id della periferica</param>
        /// <param name="nodeId">Id del Nodo</param>
        /// <param name="serverId">Id del Server</param>
        /// <param name="deviceName">Nome della periferica</param>
        /// <param name="deviceType">Tipo della periferica</param>
        /// <param name="serialNumber">Numero di serie della periferica</param>
        /// <param name="deviceAddress">Indirizzo IP della periferica</param>
        /// <param name="portId">Id dell'interfaccia della periferica</param>
        /// <param name="profileId">Id del profilo della periferica</param>
        /// <param name="active">Stato della periferica</param>
        /// <param name="scheduled">Stato di schedulazione della periferica</param>
        /// <param name="removed">Stato di rimozione della periferica</param>
        /// <param name="rackId">Id dell'Armadio</param>
        /// <param name="rackPositionRow">Indice della posizione di riga nell'Armadio</param>
        /// <param name="rackPositionCol">Indice della posizione di colonna nell'Armadio</param>
        /// <param name="monitoringDeviceId">Identifica l'apparato che esegue il monitoraggio della periferica</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool UpdateDevice(string dbConnectionString,
            long deviceId,
            long nodeId,
            int serverId,
            string deviceName,
            string deviceType,
            string serialNumber,
            string deviceAddress,
            Guid portId,
            int profileId,
            Byte active,
            Byte scheduled,
            Byte removed,
            Guid rackId,
            int rackPositionRow,
            int rackPositionCol,
            string monitoringDeviceId)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDevice = null;
            bool writeOperationSuccessful = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.UpdateDevice' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                cmd.CommandText =
                    "IF NOT EXISTS (SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID)) BEGIN INSERT INTO [devices] ([DevID], [NodID], [SrvID], [Name], [Type], [SN], [Addr], [PortId], [ProfileID], [Active], [Scheduled], [Removed], [RackID], [RackPositionRow], [RackPositionCol], [DiscoveredType], [MonitoringDeviceID]) VALUES (@DevID, @NodID, @SrvID, @Name, @Type, @SN, @Addr, @PortId, @ProfileID, @Active, @Scheduled, @Removed, @RackID, @RackPositionRow, @RackPositionCol, NULL, @MonitoringDeviceID) END ELSE BEGIN UPDATE [devices] SET [NodID] = @NodID, [SrvID] = @SrvID, [Name] = @Name, [Type] = @Type, [SN] = @SN, [Addr] = @Addr, [PortId] = @PortId, [ProfileID] = @ProfileID, [Active] = @Active, [Scheduled] = @Scheduled, [Removed] = @Removed, [RackID] = @RackID, [RackPositionRow] = @RackPositionRow, [RackPositionCol] = @RackPositionCol, [DiscoveredType] = NULL, [MonitoringDeviceID] = @MonitoringDeviceID WHERE ([DevID] = @DevID) END; SELECT [DevID] FROM [devices] WHERE ([DevID] = @DevID);";

                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, deviceId));
                cmd.Parameters.Add(new SqlParameter("@NodID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, nodeId));
                cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current,
                    serverId));
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, deviceName));
                cmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, deviceType));
                cmd.Parameters.Add(new SqlParameter("@SN", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current,
                    serialNumber));
                cmd.Parameters.Add(new SqlParameter("@Addr", SqlDbType.VarChar, 32, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, deviceAddress));
                cmd.Parameters.Add(new SqlParameter("@PortId", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, portId));
                cmd.Parameters.Add(new SqlParameter("@ProfileID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, profileId));
                cmd.Parameters.Add(new SqlParameter("@Active", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, active));
                cmd.Parameters.Add(new SqlParameter("@Scheduled", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, scheduled));
                cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, removed));
                cmd.Parameters.Add(new SqlParameter("@RackID", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, rackId));
                cmd.Parameters.Add(new SqlParameter("@RackPositionRow", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, rackPositionRow));
                cmd.Parameters.Add(new SqlParameter("@RackPositionCol", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, rackPositionCol));
                cmd.Parameters.Add(new SqlParameter("@MonitoringDeviceID", SqlDbType.VarChar, 128, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, (string.IsNullOrEmpty(monitoringDeviceId) ? (object)DBNull.Value : monitoringDeviceId)));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDevice = cmd.ExecuteReader();

                if (drDevice.Read())
                {
                    int colDevID = drDevice.GetOrdinal("DevID");

                    do
                    {
                        if ((!drDevice.IsDBNull(colDevID)) && (drDevice.GetSqlInt64(colDevID).Value == deviceId))
                        {
                            writeOperationSuccessful = true;
                        }
                    } while (drDevice.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.UpdateDevice' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.UpdateDevice' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (drDevice != null && !drDevice.IsClosed)
                {
                    drDevice.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return writeOperationSuccessful;
        }

        /// <summary>
        ///     Elimina i dati della periferica su database
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceId">Id della periferica</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool DeleteDevice(string dbConnectionString, long deviceId)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            bool deleteOperationSuccessful = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.DeleteDevice' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                cmd.CommandText =
                    "DELETE FROM reference WHERE ReferenceID IN (SELECT ReferenceID FROM [stream_fields] WHERE (ReferenceID IS NOT NULL) AND (DevID = @DevID)); DELETE FROM [stream_fields] WHERE (DevID = @DevID); DELETE FROM [streams] WHERE (DevID = @DevID); DELETE FROM [events] WHERE (DevID = @DevID); DELETE FROM [procedures] WHERE (DevID = @DevID); DELETE FROM [device_status] WHERE (DevID = @DevID); DELETE FROM [devices] WHERE (DevID = @DevID);";

                cmd.Parameters.Add(new SqlParameter("@DevID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, deviceId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();

                deleteOperationSuccessful = true;
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.DeleteDevice' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deleteOperationSuccessful;
        }

        /// <summary>
        ///     Marca tutte le righe del database come Removed
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="updateRegionsZonesNodes">Indica se modificare anche i dati di topografia</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool MarkRowsAsRemoved(string dbConnectionString, bool updateRegionsZonesNodes)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            bool updateOperationSuccessful = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.MarkRowsAsRemoved' - La stringa di connessione al database non è valida: (" + ex.GetType() +
                        ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (updateRegionsZonesNodes)
                {
                    cmd.CommandText =
                        "UPDATE devices SET Removed = 1;UPDATE nodes SET Removed = 1;UPDATE zones SET Removed = 1;UPDATE regions SET Removed = 1;UPDATE rack SET Removed = 1;UPDATE building SET Removed = 1;UPDATE station SET Removed = 1;UPDATE port SET Removed = 1;UPDATE [servers] SET Removed = 1;";
                }
                else
                {
                    cmd.CommandText =
                        "UPDATE devices SET Removed = 1;UPDATE rack SET Removed = 1;UPDATE building SET Removed = 1;UPDATE station SET Removed = 1;UPDATE port SET Removed = 1;UPDATE [servers] SET Removed = 1;";
                }

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();

                updateOperationSuccessful = true;
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.MarkRowsAsRemoved' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return updateOperationSuccessful;
        }

        /// <summary>
        ///     Elimina tutte le righe del database marcate come Removed
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="updateRegionsZonesNodes">Indica se modificare anche i dati di topografia</param>
        /// <returns>Booleano che indica se l'operazione è avvenuta correttamente</returns>
        public static bool DeleteRemovedRows(string dbConnectionString, bool updateRegionsZonesNodes)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            bool deleteOperationSuccessful = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.DeleteRemovedRows' - La stringa di connessione al database non è valida: (" + ex.GetType() +
                        ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                cmd.CommandText = updateRegionsZonesNodes ? "DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM devices WHERE (devices.Removed = 1);DELETE FROM nodes WHERE Removed = 1;DELETE FROM zones WHERE Removed = 1;DELETE FROM regions WHERE Removed = 1;DELETE FROM rack WHERE Removed = 1;DELETE FROM building WHERE Removed = 1;DELETE FROM station WHERE Removed = 1;DELETE FROM port WHERE Removed = 1;DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM devices WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM port WHERE SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1);DELETE FROM [servers] WHERE Removed = 1;"
                                                          : "DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.Removed = 1));DELETE FROM devices WHERE (devices.Removed = 1);DELETE FROM rack WHERE Removed = 1;DELETE FROM building WHERE Removed = 1;DELETE FROM station WHERE Removed = 1;DELETE FROM port WHERE Removed = 1;DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM procedures WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM events WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM devices WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1)));DELETE FROM port WHERE SrvID IN (SELECT [servers].SrvID FROM [servers] WHERE Removed = 1);DELETE FROM [servers] WHERE Removed = 1;";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                cmd.ExecuteNonQuery();

                deleteOperationSuccessful = true;
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.DeleteRemovedRows' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deleteOperationSuccessful;
        }

        /// <summary>
        ///     Inserisce una stazione
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="stationXmlId">Id della stazione da XML</param>
        /// <param name="stationName">Nome della stazione</param>
        /// <param name="removed">Intero che indica se la stazione è stata eliminata</param>
        /// <returns>Unique Identifier della stazione inserita o trovata</returns>
        private static Guid InsertStation(string dbConnectionString, int stationXmlId, string stationName, Byte removed)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDeviceDatabaseSupport = null;
            Guid stationId = Guid.Empty;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertStation' - La stringa di connessione al database non è valida: (" + ex.GetType() +
                        ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return Guid.Empty;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "DECLARE @StationID uniqueidentifier; SELECT TOP (1) @StationID = StationID FROM station WHERE (StationXMLID = @StationXMLID) AND (StationName LIKE @StationName); IF (@StationID IS NULL) BEGIN INSERT INTO station (StationID, StationXMLID, StationName, Removed) VALUES (NEWID(), @StationXMLID, @StationName, @Removed); SELECT @StationID = StationID FROM station WHERE (StationXMLID = @StationXMLID) AND (StationName LIKE @StationName); END ELSE BEGIN UPDATE station SET Removed = @Removed WHERE (StationXMLID = @StationXMLID) AND (StationName LIKE @StationName); END SELECT @StationID AS StationID;";
                cmd.Parameters.Add(new SqlParameter("@StationXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, stationXmlId));
                cmd.Parameters.Add(new SqlParameter("@StationName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, stationName));
                cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, removed));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDeviceDatabaseSupport = cmd.ExecuteReader();

                if (drDeviceDatabaseSupport.Read())
                {
                    int colStationID = drDeviceDatabaseSupport.GetOrdinal("StationID");

                    do
                    {
                        if (!drDeviceDatabaseSupport.IsDBNull(colStationID))
                        {
                            stationId = drDeviceDatabaseSupport.GetSqlGuid(colStationID).Value;
                        }
                    } while (drDeviceDatabaseSupport.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertStation' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertStation' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                {
                    drDeviceDatabaseSupport.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return stationId;
        }

        /// <summary>
        ///     Inserisce un fabbricato
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="buildingXmlId">Id del fabbricato da XML</param>
        /// <param name="buildingName">Nome del fabbricato</param>
        /// <param name="buildingDescription">Descrizione del fabbricato</param>
        /// <param name="stationId">Identificativo della stazione che contiene il fabbricato</param>
        /// <param name="removed">Intero che indica se il fabbricato è stato eliminato</param>
        /// <returns>Unique Identifier del fabbricato inserito o trovato</returns>
        private static Guid InsertBuilding(string dbConnectionString,
            int buildingXmlId,
            string buildingName,
            string buildingDescription,
            Guid stationId,
            Byte removed)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDeviceDatabaseSupport = null;
            Guid buildingId = Guid.Empty;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertBuilding' - La stringa di connessione al database non è valida: (" + ex.GetType() +
                        ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return Guid.Empty;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "DECLARE @BuildingID uniqueidentifier; SELECT TOP (1) @BuildingID = BuildingID FROM building WHERE (BuildingXMLID = @BuildingXMLID) AND (StationID = @StationID) AND (BuildingName LIKE @BuildingName); IF (@BuildingID IS NULL) BEGIN INSERT INTO building (BuildingID, BuildingXMLID, StationID, BuildingName, BuildingDescription, Removed) VALUES (NEWID(), @BuildingXMLID, @StationID, @BuildingName, @BuildingDescription, @Removed); SELECT @BuildingID = BuildingID FROM building WHERE (BuildingXMLID = @BuildingXMLID) AND (StationID = @StationID) AND (BuildingName LIKE @BuildingName); END ELSE BEGIN UPDATE building SET Removed = @Removed, BuildingDescription = @BuildingDescription WHERE (BuildingXMLID = @BuildingXMLID) AND (StationID = @StationID) AND (BuildingName LIKE @BuildingName); END SELECT @BuildingID AS BuildingID;";
                cmd.Parameters.Add(new SqlParameter("@BuildingXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, buildingXmlId));
                cmd.Parameters.Add(new SqlParameter("@BuildingName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, buildingName));
                cmd.Parameters.Add(new SqlParameter("@BuildingDescription", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, buildingDescription));
                cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, removed));
                cmd.Parameters.Add(new SqlParameter("@StationID", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, stationId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDeviceDatabaseSupport = cmd.ExecuteReader();

                if (drDeviceDatabaseSupport.Read())
                {
                    int colBuildingID = drDeviceDatabaseSupport.GetOrdinal("BuildingID");

                    do
                    {
                        if (!drDeviceDatabaseSupport.IsDBNull(colBuildingID))
                        {
                            buildingId = drDeviceDatabaseSupport.GetSqlGuid(colBuildingID).Value;
                        }
                    } while (drDeviceDatabaseSupport.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertBuilding' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertBuilding' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                {
                    drDeviceDatabaseSupport.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return buildingId;
        }

        /// <summary>
        ///     Inserisce un supporto
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="rackXmlId">Id del supporto da XML</param>
        /// <param name="rackName">Nome del supporto</param>
        /// <param name="rackType">Tipo del supporto</param>
        /// <param name="rackDescription">Descrizione del supporto</param>
        /// <param name="buildingId">Identificativo del fabbricato che contiene il supporto</param>
        /// <param name="removed">Intero che indica se il supporto è stato eliminato</param>
        /// <returns>Unique Identifier del supporto inserito o trovato</returns>
        private static Guid InsertRack(string dbConnectionString,
            int rackXmlId,
            string rackName,
            string rackType,
            string rackDescription,
            Guid buildingId,
            Byte removed)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDeviceDatabaseSupport = null;
            Guid rackId = Guid.Empty;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertRack' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return Guid.Empty;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "DECLARE @RackID uniqueidentifier; SELECT TOP (1) @RackID = RackID FROM rack WHERE (RackXMLID = @RackXMLID) AND (BuildingID = @BuildingID) AND (RackName LIKE @RackName); IF (@RackID IS NULL) BEGIN INSERT INTO rack (RackID, RackXMLID, BuildingID, RackName, RackType, RackDescription, Removed) VALUES (NEWID(), @RackXMLID, @BuildingID, @RackName, @RackType, @RackDescription, @Removed); SELECT @RackID = RackID FROM rack WHERE (RackXMLID = @RackXMLID) AND (BuildingID = @BuildingID) AND (RackName LIKE @RackName); END ELSE BEGIN UPDATE rack SET Removed = @Removed, RackDescription = @RackDescription, RackType = @RackType WHERE (RackXMLID = @RackXMLID) AND (BuildingID = @BuildingID) AND (RackName LIKE @RackName); END SELECT @RackID AS RackID;";
                cmd.Parameters.Add(new SqlParameter("@RackXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, rackXmlId));
                cmd.Parameters.Add(new SqlParameter("@RackName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, rackName));
                cmd.Parameters.Add(new SqlParameter("@RackType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, rackType));
                cmd.Parameters.Add(new SqlParameter("@RackDescription", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, rackDescription));
                cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, removed));
                cmd.Parameters.Add(new SqlParameter("@BuildingID", SqlDbType.UniqueIdentifier, 16, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, buildingId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDeviceDatabaseSupport = cmd.ExecuteReader();

                if (drDeviceDatabaseSupport.Read())
                {
                    int colRackID = drDeviceDatabaseSupport.GetOrdinal("RackID");

                    do
                    {
                        if (!drDeviceDatabaseSupport.IsDBNull(colRackID))
                        {
                            rackId = drDeviceDatabaseSupport.GetSqlGuid(colRackID).Value;
                        }
                    } while (drDeviceDatabaseSupport.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertRack' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertRack' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                {
                    drDeviceDatabaseSupport.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return rackId;
        }

        /// <summary>
        ///     Inserisce una interfaccia
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="portXmlId">Id della porta da XML</param>
        /// <param name="portName">Nome della porta</param>
        /// <param name="portType">Tipo della porta</param>
        /// <param name="parameters">Parametri della porta</param>
        /// <param name="status">Parametri della porta</param>
        /// <param name="serverId">Identificativo del server a cui la porta è riferita</param>
        /// <param name="removed">Intero che indica se la porta è stata eliminato</param>
        /// <returns>Unique Identifier della porta inserita o trovata</returns>
        private static Guid InsertPort(string dbConnectionString,
            int portXmlId,
            string portName,
            string portType,
            string parameters,
            string status,
            int serverId,
            Byte removed)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDeviceDatabaseSupport = null;
            Guid portId = Guid.Empty;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertPort' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return Guid.Empty;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "DECLARE @PortID uniqueidentifier; SELECT TOP (1) @PortID = PortID FROM port WHERE (PortXMLID = @PortXMLID) AND (SrvID = @SrvID) AND (PortName LIKE @PortName); IF (@PortID IS NULL) BEGIN INSERT INTO port (PortID, PortXMLID, PortName, PortType, Parameters, Status, Removed, SrvID) VALUES (NEWID(), @PortXMLID, @PortName, @PortType, @Parameters, @Status, @Removed, @SrvID); SELECT @PortID = PortID FROM port WHERE (PortXMLID = @PortXMLID) AND (SrvID = @SrvID) AND (PortName LIKE @PortName); END ELSE BEGIN UPDATE port SET Removed = @Removed, PortType = @PortType, Parameters = @Parameters, Status = @Status WHERE (PortXMLID = @PortXMLID) AND (SrvID = @SrvID) AND (PortName LIKE @PortName); END SELECT @PortID AS PortID;";
                cmd.Parameters.Add(new SqlParameter("@PortXMLID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, portXmlId));
                cmd.Parameters.Add(new SqlParameter("@PortName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, portName));
                cmd.Parameters.Add(new SqlParameter("@PortType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, portType));
                cmd.Parameters.Add(new SqlParameter("@Parameters", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, parameters));
                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, status));
                cmd.Parameters.Add(new SqlParameter("@Removed", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, removed));
                cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, serverId));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDeviceDatabaseSupport = cmd.ExecuteReader();

                if (drDeviceDatabaseSupport.Read())
                {
                    int colPortID = drDeviceDatabaseSupport.GetOrdinal("PortID");

                    do
                    {
                        if (!drDeviceDatabaseSupport.IsDBNull(colPortID))
                        {
                            portId = drDeviceDatabaseSupport.GetSqlGuid(colPortID).Value;
                        }
                    } while (drDeviceDatabaseSupport.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertPort' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertPort' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                {
                    drDeviceDatabaseSupport.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return portId;
        }

        /// <summary>
        ///     Inserisce un server e relativa gerarchia
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="serverId">ID del Server da System.xml</param>
        /// <param name="regionId">ID del Compartimento</param>
        /// <param name="regionName">Nome del Compartimento</param>
        /// <param name="regionRemoved">Intero che indica se il Compartimento è rimosso</param>
        /// <param name="zoneId">ID della Zona</param>
        /// <param name="zoneName">Nome della Zona</param>
        /// <param name="zoneRemoved">Intero che indica se la Zona è rimossa</param>
        /// <param name="nodeId">ID del Nodo</param>
        /// <param name="nodeName">Nome del Nodo</param>
        /// <param name="nodeRemoved">Intero che indica se il Nodo è rimosso</param>
        /// <param name="serverHost">Host che monitora la periferica</param>
        /// <param name="serverName">Nome del Server che monitora la periferica</param>
        /// <param name="serverRemoved">Intero che indica se il Server è rimosso</param>
        /// <returns>Id del server inserito o trovato</returns>
        private static int InsertServer(string dbConnectionString,
            int serverId,
            long regionId,
            string regionName,
            byte regionRemoved,
            long zoneId,
            string zoneName,
            byte zoneRemoved,
            long nodeId,
            string nodeName,
            byte nodeRemoved,
            string serverHost,
            string serverName,
            byte serverRemoved)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDeviceDatabaseSupport = null;
            int currentServerId = 0;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertServer' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return 0;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                // In update del server non gestiamo IP e MAC, perché sono dati che sono normalmente aggiornati dallo SCAgent e non si vogliono sovrascrivere qui
                // In update del nodo non gestiamo la chilometrica (meters), perché questi dati non ci sono sul System.xml e potrebbero essere presenti e corretti dalla region list chiamata in altri aggiornamenti da CentralWS
                cmd.CommandText =
                    "DECLARE @ServerID INT; SELECT @ServerID = SrvID FROM servers WHERE (SrvID = @SrvID); IF NOT EXISTS (SELECT RegId FROM regions WHERE RegId = @RegID) BEGIN INSERT INTO regions (RegID, Name, Removed) VALUES (@RegID, @RegionName, @RegionRemoved); END ELSE BEGIN UPDATE regions SET Name = @RegionName, Removed = @RegionRemoved WHERE RegID = @RegID; END IF NOT EXISTS (SELECT ZonID FROM zones WHERE ZonId = @ZonID) BEGIN INSERT INTO zones (ZonID, RegID, Name, Removed) VALUES (@ZonID, @RegID, @ZoneName, @ZoneRemoved); END ELSE BEGIN UPDATE zones SET Name = @ZoneName, Removed = @ZoneRemoved, RegID = @RegID WHERE ZonID = @ZonID; END IF NOT EXISTS (SELECT NodId FROM nodes WHERE NodId = @NodID) BEGIN INSERT INTO nodes (NodID, ZonID, Name, Removed, Meters) VALUES (@NodID, @ZonID, @NodeName, @NodeRemoved, ISNULL(@Meters, 0)); END ELSE BEGIN UPDATE nodes SET Name = @NodeName, Removed = @NodeRemoved, ZonID = @ZonID WHERE NodID = @NodID; END IF (@ServerID IS NULL) BEGIN INSERT INTO servers (SrvID, Name, Host, FullHostName, IP, LastUpdate, LastMessageType, SupervisorSystemXML, ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC, Removed) VALUES (@SrvID, @ServerName, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @SupervisorSystemXML, @ClientSupervisorSystemXMLValidated, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey, @NodID, @ServerVersion, @MAC, @ServerRemoved); SELECT @ServerID = SrvID FROM servers WHERE (SrvID = @SrvID); END ELSE BEGIN UPDATE servers SET Name = @ServerName, Host = @Host, FullHostName = @FullHostName, SupervisorSystemXML = @SupervisorSystemXML, Removed = @ServerRemoved WHERE (SrvID = @SrvID); END SELECT @SrvID AS SrvID;";
                cmd.Parameters.Add(new SqlParameter("@SrvID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current,
                    serverId));
                cmd.Parameters.Add(new SqlParameter("@ServerName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, serverName));
                cmd.Parameters.Add(new SqlParameter("@Host", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, serverHost));
                cmd.Parameters.Add(new SqlParameter("@ServerRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, serverRemoved));
                cmd.Parameters.Add(new SqlParameter("@NodID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, nodeId));
                cmd.Parameters.Add(new SqlParameter("@RegID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, regionId));
                cmd.Parameters.Add(new SqlParameter("@RegionName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, regionName));
                cmd.Parameters.Add(new SqlParameter("@RegionRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, regionRemoved));
                cmd.Parameters.Add(new SqlParameter("@ZonID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, zoneId));
                cmd.Parameters.Add(new SqlParameter("@ZoneName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, zoneName));
                cmd.Parameters.Add(new SqlParameter("@ZoneRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, zoneRemoved));
                cmd.Parameters.Add(new SqlParameter("@NodeName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, nodeName));
                cmd.Parameters.Add(new SqlParameter("@NodeRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, nodeRemoved));
                cmd.Parameters.Add(new SqlParameter("@Meters", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current,
                    DBNull.Value));

                // FullHostname, IP e MAC sono sovrascritti nello SCAgent, li settiamo qui con gli stessi valori, per sicurezza
                if (!String.IsNullOrEmpty(serverHost))
                {
                    cmd.Parameters.Add(new SqlParameter("@FullHostName", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null,
                        DataRowVersion.Current, serverHost));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@FullHostName", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null,
                        DataRowVersion.Current, WMIFunction.Default.GetFullHostName()));
                }

                string mac;
                string ip = STLCNetwork.GetNetworkInterfaceIP(Settings.Default.NetworkAdapterName1, out mac);

                cmd.Parameters.Add(new SqlParameter("@IP", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current,
                    ip));
                cmd.Parameters.Add(new SqlParameter("@LastUpdate", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, DateTime.Now));
                cmd.Parameters.Add(new SqlParameter("@LastMessageType", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, string.Empty));

                SqlXml systemXmlContent = GetSystemXmlContent();

                cmd.Parameters.Add(new SqlParameter("@SupervisorSystemXML", SqlDbType.Xml, systemXmlContent.Value.Length, ParameterDirection.Input,
                    false, 0, 0, null, DataRowVersion.Current, systemXmlContent));

                cmd.Parameters.Add(new SqlParameter("@ClientSupervisorSystemXMLValidated", SqlDbType.Xml, -1, ParameterDirection.Input, false, 0, 0,
                    null, DataRowVersion.Current, DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@ClientValidationSign", SqlDbType.VarBinary, 128, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@ClientDateValidationRequested", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0,
                    null, DataRowVersion.Current, DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@ClientDateValidationObtained", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0,
                    null, DataRowVersion.Current, DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@ClientKey", SqlDbType.VarBinary, 148, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@ServerVersion", SqlDbType.VarChar, 32, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@MAC", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null, DataRowVersion.Current,
                    mac));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDeviceDatabaseSupport = cmd.ExecuteReader();

                if (drDeviceDatabaseSupport.Read())
                {
                    int colSrvID = drDeviceDatabaseSupport.GetOrdinal("SrvID");

                    do
                    {
                        if (!drDeviceDatabaseSupport.IsDBNull(colSrvID))
                        {
                            currentServerId = drDeviceDatabaseSupport.GetSqlInt32(colSrvID).Value;
                        }
                    } while (drDeviceDatabaseSupport.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertServer' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertServer' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (drDeviceDatabaseSupport != null && !drDeviceDatabaseSupport.IsClosed)
                {
                    drDeviceDatabaseSupport.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return currentServerId;
        }

        private static SqlXml GetSystemXmlContent()
        {
            string supervisorSystemXml = SystemXmlHelper.LoadConfigurationSystemFile(AppCfg.Default.sXMLSystem); // .FP_CFG_XML_SYSTEM_VALUE_PATH);

            using (XmlTextReader xmlTextReader = new XmlTextReader(supervisorSystemXml, XmlNodeType.Document, null))
            {
                return new SqlXml(xmlTextReader);
            }
        }

        /// <summary>
        ///     Recupera la lista di periferiche già inserite su database
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <returns>
        ///     Ritorna null nel caso ci sia un errore nell'accesso alla base dati, nell'esecuzione del comando o incoerenze sui
        ///     dati
        /// </returns>
        public static ReadOnlyCollection<DeviceObject> GetExistingDeviceListFromDatabase(string dbConnectionString)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader drDevices = null;
            List<DeviceObject> devices = new List<DeviceObject>();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetExistingDeviceListFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "SELECT DevID, Name, [Type] FROM [devices]";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                drDevices = cmd.ExecuteReader();

                if (drDevices.Read())
                {
                    int colDevID = drDevices.GetOrdinal("DevID");
                    int colName = drDevices.GetOrdinal("Name");
                    int colType = drDevices.GetOrdinal("Type");

                    do
                    {
                        long deviceID = drDevices.GetSqlInt64(colDevID).Value;

                        if ((drDevices.IsDBNull(colName)) || (drDevices.IsDBNull(colType)))
                        {
                            if (drDevices.IsDBNull(colName))
                            {
                                Logger.Default.Log(
                                    string.Format(
                                        "ECCEZIONE in 'SqlDatabaseUtility.GetExistingDeviceListFromDatabase' - Il nome periferica per la riga con Device ID: {0} è nullo o assente",
                                        deviceID), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                    AppCfg.E_SYSTEM_XML_FUNCTION_DATABASE_UNEXPECTED_DATA_ERROR);

                                return null;
                            }

                            if (drDevices.IsDBNull(colType))
                            {
                                Logger.Default.Log(
                                    string.Format(
                                        "ECCEZIONE in 'SqlDatabaseUtility.GetExistingDeviceListFromDatabase' - Il tipo periferica per la riga con Device ID: {0} è nullo o assente",
                                        deviceID), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                    AppCfg.E_SYSTEM_XML_FUNCTION_DATABASE_UNEXPECTED_DATA_ERROR);

                                return null;
                            }
                        }
                        else
                        {
                            string deviceName = drDevices.GetSqlString(colName).Value;
                            string deviceType = drDevices.GetSqlString(colType).Value;

                            devices.Add(new DeviceObject(deviceID, deviceName, deviceType));
                        }
                    } while (drDevices.Read());

                    Logger.Default.Log(
                        String.Format(CultureInfo.InvariantCulture, "Caricate {0} periferiche esistenti da database.", devices.Count),
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetExistingDeviceListFromDatabase' - Errore durante l'accesso alla base dati: (" + ex.GetType() +
                    ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetExistingDeviceListFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return null;
            }
            catch (ThreadAbortException)
            {
                return null;
            }
            finally
            {
                if (drDevices != null && !drDevices.IsClosed)
                {
                    drDevices.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return devices.AsReadOnly();
        }

        /// <summary>
        ///     Esegue una verifica sulle tabelle coinvolte nell'aggiornamento dei tipi periferica e region list
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <returns>
        ///     Ritorna false nel caso ci sia un errore nell'accesso alla base dati, nell'esecuzione del comando o incoerenze sui
        ///     dati
        /// </returns>
        public static bool GetExistingCentralWSData(string dbConnectionString)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            bool returnValue = false;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetExistingCentralWSData' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "SELECT COUNT(DeviceTypeID) AS [Rows] FROM device_type UNION ALL SELECT COUNT (SystemID) FROM systems UNION ALL SELECT COUNT (VendorID) FROM vendors UNION ALL SELECT COUNT (RegID) FROM regions UNION ALL SELECT COUNT (ZonID) FROM zones UNION ALL SELECT COUNT (NodID) FROM nodes";

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colRows = dr.GetOrdinal("Rows");

                    do
                    {
                        int rowsCount = dr.GetSqlInt32(colRows).Value;

                        returnValue = rowsCount >= 0;
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetExistingCentralWSData' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return false;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetExistingCentralWSData' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return returnValue;
        }

        public static string GetCollectionStringRepresentation<T>(ReadOnlyCollection<T> collection)
        {
            if ((collection != null) && (collection.Count > 0))
            {
                StringBuilder representation = new StringBuilder();

                foreach (object o in collection)
                {
                    representation.AppendFormat("{0}\r\n", o);
                }

                return representation.ToString();
            }

            return String.Empty;
        }

        public static ReadOnlyCollection<GrisDeviceType> GetDeviceTypeListFromDatabase(string dbConnectionString, string ids)
        {
            List<GrisDeviceType> deviceTypes = new List<GrisDeviceType>();
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetDeviceTypeListFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText =
                        "SELECT DeviceTypeID, SystemID, VendorID, DeviceTypeDescription, WSUrlPattern FROM device_type ORDER BY DeviceTypeID";
                }
                else
                {
                    cmd.CommandText =
                        String.Format(
                            "SELECT DeviceTypeID, SystemID, VendorID, DeviceTypeDescription, WSUrlPattern FROM device_type WHERE DeviceTypeID IN ({0}) ORDER BY DeviceTypeID",
                            ids);
                }

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colDeviceTypeID = dr.GetOrdinal("DeviceTypeID");
                    int colSystemID = dr.GetOrdinal("SystemID");
                    int colVendorID = dr.GetOrdinal("VendorID");
                    int colDeviceTypeDescription = dr.GetOrdinal("DeviceTypeDescription");
                    int colWSUrlPattern = dr.GetOrdinal("WSUrlPattern");

                    do
                    {
                        string deviceTypeID = dr.GetSqlString(colDeviceTypeID).Value;
                        int systemID = dr.GetSqlInt32(colSystemID).Value;
                        int vendorID = dr.GetSqlInt32(colVendorID).Value;
                        string deviceTypeDescription = dr.IsDBNull(colDeviceTypeDescription) ? null : dr.GetSqlString(colDeviceTypeDescription).Value;
                        string wsUrlPattern = dr.IsDBNull(colWSUrlPattern) ? null : dr.GetSqlString(colWSUrlPattern).Value;

                        deviceTypes.Add(new GrisDeviceType
                        {
                            DeviceTypeID = deviceTypeID,
                            SystemID = systemID,
                            VendorID = vendorID,
                            DeviceTypeDescription = deviceTypeDescription,
                            WSUrlPattern = wsUrlPattern
                        });
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetDeviceTypeListFromDatabase' - Errore durante l'accesso alla base dati: (" + ex.GetType() +
                    ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetDeviceTypeListFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return null;
            }
            catch (ThreadAbortException)
            {
                return null;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return deviceTypes.AsReadOnly();
        }

        public static ReadOnlyCollection<GrisVendor> GetVendorListFromDatabase(string dbConnectionString, string ids)
        {
            List<GrisVendor> vendors = new List<GrisVendor>();
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetVendorListFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = "SELECT VendorID, VendorName, VendorDescription FROM vendors ORDER BY VendorID";
                }
                else
                {
                    cmd.CommandText =
                        String.Format("SELECT VendorID, VendorName, VendorDescription FROM vendors WHERE VendorID IN ({0}) ORDER BY VendorID", ids);
                }

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colVendorID = dr.GetOrdinal("VendorID");
                    int colVendorName = dr.GetOrdinal("VendorName");
                    int colVendorDescription = dr.GetOrdinal("VendorDescription");

                    do
                    {
                        int vendorID = dr.GetSqlInt32(colVendorID).Value;
                        string vendorName = dr.GetSqlString(colVendorName).Value;
                        string vendorDescription = dr.IsDBNull(colVendorDescription) ? null : dr.GetSqlString(colVendorDescription).Value;

                        vendors.Add(new GrisVendor { VendorID = vendorID, VendorName = vendorName, VendorDescription = vendorDescription });
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetVendorListFromDatabase' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetVendorListFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return null;
            }
            catch (ThreadAbortException)
            {
                return null;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return vendors.AsReadOnly();
        }

        public static ReadOnlyCollection<GrisSystem> GetSystemListFromDatabase(string dbConnectionString, string ids)
        {
            List<GrisSystem> systems = new List<GrisSystem>();
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetSystemListFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = "SELECT SystemID, SystemDescription FROM systems ORDER BY SystemID";
                }
                else
                {
                    cmd.CommandText = String.Format("SELECT SystemID, SystemDescription FROM systems WHERE SystemID IN ({0}) ORDER BY SystemID", ids);
                }

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colSystemID = dr.GetOrdinal("SystemID");
                    int colSystemDescription = dr.GetOrdinal("SystemDescription");

                    do
                    {
                        int systemID = dr.GetSqlInt32(colSystemID).Value;
                        string systemDescription = dr.GetSqlString(colSystemDescription).Value;

                        systems.Add(new GrisSystem { SystemID = systemID, SystemDescription = systemDescription });
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetSystemListFromDatabase' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetSystemListFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return null;
            }
            catch (ThreadAbortException)
            {
                return null;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return systems.AsReadOnly();
        }

        public static ReadOnlyCollection<GrisRegion> GetRegionListFromDatabase(string dbConnectionString, string ids)
        {
            List<GrisRegion> regions = new List<GrisRegion>();
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetRegionListFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = "SELECT RegID, Name FROM regions ORDER BY RegID";
                }
                else
                {
                    cmd.CommandText = String.Format("SELECT RegID, Name FROM regions WHERE RegID IN ({0}) ORDER BY RegID", ids);
                }

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colRegID = dr.GetOrdinal("RegID");
                    int colName = dr.GetOrdinal("Name");

                    do
                    {
                        long regionID = dr.GetSqlInt64(colRegID).Value;
                        string name = dr.GetSqlString(colName).Value;

                        regions.Add(new GrisRegion { RegID = regionID, Name = name });
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetRegionListFromDatabase' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetRegionListFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return null;
            }
            catch (ThreadAbortException)
            {
                return null;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return regions.AsReadOnly();
        }

        public static ReadOnlyCollection<GrisZone> GetZoneListFromDatabase(string dbConnectionString, string ids)
        {
            List<GrisZone> zones = new List<GrisZone>();
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetExistiGetZoneListFromDatabasengDeviceListFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = "SELECT ZonID, RegID, Name FROM zones ORDER BY ZonID";
                }
                else
                {
                    cmd.CommandText = String.Format("SELECT ZonID, RegID, Name FROM zones WHERE ZonID IN ({0}) ORDER BY ZonID", ids);
                }

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colZonID = dr.GetOrdinal("ZonID");
                    int colRegID = dr.GetOrdinal("RegID");
                    int colName = dr.GetOrdinal("Name");

                    do
                    {
                        long zoneID = dr.GetSqlInt64(colZonID).Value;
                        long regionID = dr.GetSqlInt64(colRegID).Value;
                        string name = dr.GetSqlString(colName).Value;

                        zones.Add(new GrisZone { ZonID = zoneID, RegID = regionID, Name = name });
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetZoneListFromDatabase' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetZoneListFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return null;
            }
            catch (ThreadAbortException)
            {
                return null;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return zones.AsReadOnly();
        }

        public static ReadOnlyCollection<GrisNode> GetNodeListFromDatabase(string dbConnectionString, string ids)
        {
            List<GrisNode> nodes = new List<GrisNode>();
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetNodeListFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = "SELECT NodID, ZonID, Name, ISNULL(Meters, 0) AS Meters FROM nodes ORDER BY NodID";
                }
                else
                {
                    cmd.CommandText =
                        String.Format("SELECT NodID, ZonID, Name, ISNULL(Meters, 0) AS Meters FROM nodes WHERE NodID IN ({0}) ORDER BY NodID", ids);
                }

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNodID = dr.GetOrdinal("NodID");
                    int colZonID = dr.GetOrdinal("ZonID");
                    int colName = dr.GetOrdinal("Name");
                    int colMeters = dr.GetOrdinal("Meters");

                    do
                    {
                        long nodeID = dr.GetSqlInt64(colNodID).Value;
                        long zoneID = dr.GetSqlInt64(colZonID).Value;
                        string name = dr.GetSqlString(colName).Value;
                        int meters = dr.GetSqlInt32(colMeters).Value;

                        nodes.Add(new GrisNode { NodID = nodeID, ZonID = zoneID, Name = name, Meters = meters });
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetNodeListFromDatabase' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);

                return null;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.GetNodeListFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return null;
            }
            catch (ThreadAbortException)
            {
                return null;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return nodes.AsReadOnly();
        }

        /// <summary>
        ///     Inserisce o aggiorna un compartimento
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="regionId">ID del Compartimento</param>
        /// <param name="regionName">Nome del Compartimento</param>
        /// <param name="regionRemoved">Intero che indica se il Compartimento è rimosso</param>
        /// <returns>Id del compartimento inserito o trovato</returns>
        public static long InsertRegion(string dbConnectionString, long regionId, string regionName, byte regionRemoved)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            long newId = 0;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertRegion' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return 0;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF NOT EXISTS (SELECT RegId FROM regions WHERE RegId = @RegID) BEGIN INSERT INTO regions (RegID, Name, Removed) VALUES (@RegID, @RegionName, @RegionRemoved); END ELSE BEGIN UPDATE regions SET Name = @RegionName, Removed = @RegionRemoved WHERE RegID = @RegID; END SELECT RegId FROM regions WHERE RegId = @RegID;";
                cmd.Parameters.Add(new SqlParameter("@RegID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, regionId));
                cmd.Parameters.Add(new SqlParameter("@RegionName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, regionName));
                cmd.Parameters.Add(new SqlParameter("@RegionRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, regionRemoved));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNewId = dr.GetOrdinal("RegId");

                    do
                    {
                        if (!dr.IsDBNull(colNewId))
                        {
                            newId = dr.GetSqlInt64(colNewId).Value;
                        }
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertRegion' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertRegion' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return newId;
        }

        /// <summary>
        ///     Inserisce o aggiorna una linea
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="zoneId">ID della Linea</param>
        /// <param name="regionId">ID del Compartimento</param>
        /// <param name="zoneName">Nome della Linea</param>
        /// <param name="zoneRemoved">Intero che indica se la Linea è rimossa</param>
        /// <returns>Id della linea inserita o trovata</returns>
        public static long InsertZone(string dbConnectionString, long zoneId, long regionId, string zoneName, byte zoneRemoved)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            long newId = 0;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertZone' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return 0;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF NOT EXISTS (SELECT ZonID FROM zones WHERE ZonId = @ZonID) BEGIN INSERT INTO zones (ZonID, RegID, Name, Removed) VALUES (@ZonID, @RegID, @ZoneName, @ZoneRemoved); END ELSE BEGIN UPDATE zones SET Name = @ZoneName, Removed = @ZoneRemoved, RegID = @RegID WHERE ZonID = @ZonID; END SELECT ZonID FROM zones WHERE ZonId = @ZonID;";
                cmd.Parameters.Add(new SqlParameter("@ZonID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, zoneId));
                cmd.Parameters.Add(new SqlParameter("@RegID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, regionId));
                cmd.Parameters.Add(new SqlParameter("@ZoneName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, zoneName));
                cmd.Parameters.Add(new SqlParameter("@ZoneRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, zoneRemoved));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNewId = dr.GetOrdinal("ZonId");

                    do
                    {
                        if (!dr.IsDBNull(colNewId))
                        {
                            newId = dr.GetSqlInt64(colNewId).Value;
                        }
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertZone' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertZone' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return newId;
        }

        /// <summary>
        ///     Inserisce o aggiorna una stazione
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="nodeId">ID della Stazione</param>
        /// <param name="zoneId">ID della Linea</param>
        /// <param name="nodeName">Nome della Stazione</param>
        /// <param name="nodeRemoved">Intero che indica se la Stazione è rimossa</param>
        /// <param name="meters">Valore chilometrica</param>
        /// <returns>Id della Stazione inserita o trovata</returns>
        public static long InsertNode(string dbConnectionString, long nodeId, long zoneId, string nodeName, byte nodeRemoved, int? meters)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            long newId = 0;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertNode' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return 0;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF NOT EXISTS (SELECT NodId FROM nodes WHERE NodId = @NodID) BEGIN INSERT INTO nodes (NodID, ZonID, Name, Removed, Meters) VALUES (@NodID, @ZonID, @NodeName, @NodeRemoved, ISNULL(@Meters, 0)); END ELSE BEGIN UPDATE nodes SET Name = @NodeName, Removed = @NodeRemoved, ZonID = @ZonID, Meters = ISNULL(@Meters, 0) WHERE NodID = @NodID; END SELECT NodId FROM nodes WHERE NodId = @NodID;";
                cmd.Parameters.Add(new SqlParameter("@NodID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, nodeId));
                cmd.Parameters.Add(new SqlParameter("@ZonID", SqlDbType.BigInt, 8, ParameterDirection.Input, false, 0, 19, null,
                    DataRowVersion.Current, zoneId));
                cmd.Parameters.Add(new SqlParameter("@NodeName", SqlDbType.VarChar, 64, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, nodeName));
                cmd.Parameters.Add(new SqlParameter("@NodeRemoved", SqlDbType.TinyInt, 1, ParameterDirection.Input, false, 0, 3, null,
                    DataRowVersion.Current, nodeRemoved));
                cmd.Parameters.Add(new SqlParameter("@Meters", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null, DataRowVersion.Current,
                    (meters.HasValue ? (object)(meters.Value) : DBNull.Value)));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNewId = dr.GetOrdinal("NodId");

                    do
                    {
                        if (!dr.IsDBNull(colNewId))
                        {
                            newId = dr.GetSqlInt64(colNewId).Value;
                        }
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertNode' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertNode' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return newId;
        }

        /// <summary>
        ///     Inserisce o aggiorna un produttore
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="vendorId">ID del Produttore</param>
        /// <param name="vendorName">Nome del Produttore</param>
        /// <param name="vendorDescription">Descrizione del Produttore</param>
        /// <returns>Id del Produttore inserito o trovato</returns>
        public static int InsertVendor(string dbConnectionString, int vendorId, string vendorName, string vendorDescription)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            int newId = 0;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertVendor' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return 0;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF NOT EXISTS (SELECT VendorID FROM vendors WHERE VendorID = @VendorID) BEGIN INSERT INTO vendors (VendorID, VendorName, VendorDescription) VALUES (@VendorID, @VendorName, @VendorDescription); END ELSE BEGIN UPDATE vendors SET VendorName = @vendorName, VendorDescription = @VendorDescription WHERE VendorID = @VendorID; END SELECT VendorID FROM vendors WHERE VendorID = @VendorID;";
                cmd.Parameters.Add(new SqlParameter("@VendorID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, vendorId));
                cmd.Parameters.Add(new SqlParameter("@VendorName", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, vendorName));
                cmd.Parameters.Add(new SqlParameter("@VendorDescription", SqlDbType.VarChar, 512, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, (String.IsNullOrEmpty(vendorDescription) ? DBNull.Value : (object)vendorDescription)));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNewId = dr.GetOrdinal("VendorID");

                    do
                    {
                        if (!dr.IsDBNull(colNewId))
                        {
                            newId = dr.GetSqlInt32(colNewId).Value;
                        }
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertVendor' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertVendor' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return newId;
        }

        /// <summary>
        ///     Inserisce o aggiorna un sistema
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="systemId">ID del Sistema</param>
        /// <param name="systemDescription">Descrizione del Sistema</param>
        /// <returns>Id del Sistema inserito o trovato</returns>
        public static int InsertSystem(string dbConnectionString, int systemId, string systemDescription)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            int newId = 0;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertSystem' - La stringa di connessione al database non è valida: (" + ex.GetType() + ") " +
                        ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return 0;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF NOT EXISTS (SELECT SystemID FROM systems WHERE SystemID = @SystemID) BEGIN INSERT INTO systems (SystemID, SystemDescription) VALUES (@SystemID, @SystemDescription); END ELSE BEGIN UPDATE systems SET SystemDescription = @SystemDescription WHERE SystemID = @SystemID; END SELECT SystemID FROM systems WHERE SystemID = @SystemID;";
                cmd.Parameters.Add(new SqlParameter("@SystemID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, systemId));
                cmd.Parameters.Add(new SqlParameter("@SystemDescription", SqlDbType.VarChar, 256, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, systemDescription));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNewId = dr.GetOrdinal("SystemID");

                    do
                    {
                        if (!dr.IsDBNull(colNewId))
                        {
                            newId = dr.GetSqlInt32(colNewId).Value;
                        }
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertSystem' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " + ex.Message +
                    " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertSystem' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return newId;
        }

        /// <summary>
        ///     Inserisce o aggiorna un tipo periferica
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="deviceTypeId">ID del Tipo Periferica</param>
        /// <param name="systemId">ID del Sistema</param>
        /// <param name="vendorId">ID del Produttore</param>
        /// <param name="deviceTypeDescription">Descrizione del Tipo Periferica</param>
        /// <param name="wsUrlPattern">Modello di URL del web service del Tipo Periferica</param>
        /// <returns>Id del Tipo Periferica inserito o trovato</returns>
        public static string InsertDeviceType(string dbConnectionString,
            string deviceTypeId,
            int systemId,
            int vendorId,
            string deviceTypeDescription,
            string wsUrlPattern)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;
            string newId = null;

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.InsertDeviceType' - La stringa di connessione al database non è valida: (" + ex.GetType() +
                        ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                    return null;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText =
                    "IF NOT EXISTS (SELECT DeviceTypeID FROM device_type WHERE DeviceTypeID = @DeviceTypeID) BEGIN INSERT INTO device_type (DeviceTypeID, SystemID, VendorID, DeviceTypeDescription, WSUrlPattern) VALUES (@DeviceTypeID, @SystemID, @VendorID, @DeviceTypeDescription, @WSUrlPattern); END ELSE BEGIN UPDATE device_type SET SystemID = @SystemID, VendorID = @VendorID, DeviceTypeDescription = @DeviceTypeDescription, WSUrlPattern = @WSUrlPattern WHERE (DeviceTypeID = @DeviceTypeID); END SELECT DeviceTypeID FROM device_type WHERE DeviceTypeID = @DeviceTypeID;";
                cmd.Parameters.Add(new SqlParameter("@DeviceTypeID", SqlDbType.VarChar, 16, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, deviceTypeId));
                cmd.Parameters.Add(new SqlParameter("@SystemID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, systemId));
                cmd.Parameters.Add(new SqlParameter("@VendorID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 10, null,
                    DataRowVersion.Current, vendorId));
                cmd.Parameters.Add(new SqlParameter("@DeviceTypeDescription", SqlDbType.VarChar, 512, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, deviceTypeDescription));
                cmd.Parameters.Add(new SqlParameter("@WSUrlPattern", SqlDbType.VarChar, 512, ParameterDirection.Input, false, 0, 0, null,
                    DataRowVersion.Current, (String.IsNullOrEmpty(wsUrlPattern) ? DBNull.Value : (object)wsUrlPattern)));

                cmd.Connection = dbConnection;

                dbConnection.Open();

                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    int colNewId = dr.GetOrdinal("DeviceTypeID");

                    do
                    {
                        if (!dr.IsDBNull(colNewId))
                        {
                            newId = dr.GetSqlString(colNewId).Value;
                        }
                    } while (dr.Read());
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertDeviceType' - Errore durante l'accesso alla base dati: (" + ex.GetType() + ") " +
                    ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.InsertDeviceType' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return newId;
        }

        /// <summary>
        ///     Elimina i tipi periferica non contenuti nella lista di Id passati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="ids">Lista di Id da mantenere</param>
        /// <returns>True se l'operazione ha successo, false altrimenti</returns>
        public static bool RemoveAdditionalDeviceTypesFromDatabase(string dbConnectionString, string ids)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalDeviceTypesFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (!String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = String.Format("DELETE FROM device_type WHERE DeviceTypeID NOT IN ({0})", ids);

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalDeviceTypesFromDatabase' - Errore durante l'accesso alla base dati: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalDeviceTypesFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return false;
        }

        /// <summary>
        ///     Elimina i sistemi non contenuti nella lista di Id passati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="ids">Lista di Id da mantenere</param>
        /// <returns>True se l'operazione ha successo, false altrimenti</returns>
        public static bool RemoveAdditionalSystemsFromDatabase(string dbConnectionString, string ids)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalSystemsFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (!String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = String.Format("DELETE FROM systems WHERE SystemID NOT IN ({0})", ids);

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalSystemsFromDatabase' - Errore durante l'accesso alla base dati: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalSystemsFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return false;
        }

        /// <summary>
        ///     Elimina i produttori non contenuti nella lista di Id passati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="ids">Lista di Id da mantenere</param>
        /// <returns>True se l'operazione ha successo, false altrimenti</returns>
        public static bool RemoveAdditionalVendorsFromDatabase(string dbConnectionString, string ids)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalVendorsFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (!String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = String.Format("DELETE FROM vendors WHERE VendorID NOT IN ({0})", ids);

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalVendorsFromDatabase' - Errore durante l'accesso alla base dati: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalVendorsFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return false;
        }

        /// <summary>
        ///     Elimina i compartimenti non contenuti nella lista di Id passati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="ids">Lista di Id da mantenere</param>
        /// <returns>True se l'operazione ha successo, false altrimenti</returns>
        public static bool RemoveAdditionalRegionsFromDatabase(string dbConnectionString, string ids)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalRegionsFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (!String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = String.Format("SELECT ZonID INTO #ZonesToDelete FROM zones WHERE RegID NOT IN ({0});SELECT NodID INTO #NodesToDelete FROM nodes WHERE ZonID IN (SELECT ZonID FROM #ZonesToDelete);SELECT DevID INTO #DevicesToDelete FROM devices WHERE NodID IN (SELECT NodID FROM #NodesToDelete);SELECT SrvID INTO #ServersToDelete FROM [servers] WHERE NodID IN (SELECT NodID FROM #NodesToDelete);DELETE FROM [events] WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM [procedures] WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM reference WHERE ReferenceID IN (SELECT ReferenceID FROM stream_fields WHERE DevID IN (SELECT DevID FROM #DevicesToDelete));DELETE FROM reference WHERE DeltaDevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM stream_fields WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM streams WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM device_status WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM devices WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM devices WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM port WHERE PortID IN (SELECT PortId FROM devices WHERE DevID IN (SELECT DevID FROM #DevicesToDelete) AND PortId IS NOT NULL);DELETE FROM port WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM [servers] WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM nodes WHERE NodID IN (SELECT NodID FROM #NodesToDelete);DELETE FROM zones WHERE ZonID IN (SELECT ZonID FROM #ZonesToDelete);DELETE FROM regions WHERE RegID NOT IN ({0});DROP TABLE #DevicesToDelete;DROP TABLE #ServersToDelete;DROP TABLE #NodesToDelete;DROP TABLE #ZonesToDelete;", ids);

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalRegionsFromDatabase' - Errore durante l'accesso alla base dati: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalRegionsFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return false;
        }

        /// <summary>
        ///     Elimina le linee non contenute nella lista di Id passati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="ids">Lista di Id da mantenere</param>
        /// <returns>True se l'operazione ha successo, false altrimenti</returns>
        public static bool RemoveAdditionalZonesFromDatabase(string dbConnectionString, string ids)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalZonesFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (!String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = String.Format("SELECT NodID INTO #NodesToDelete FROM nodes WHERE ZonID NOT IN ({0});SELECT DevID INTO #DevicesToDelete FROM devices WHERE NodID IN (SELECT NodID FROM #NodesToDelete);SELECT SrvID INTO #ServersToDelete FROM [servers] WHERE NodID IN (SELECT NodID FROM #NodesToDelete);DELETE FROM [events] WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM [procedures] WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM reference WHERE ReferenceID IN (SELECT ReferenceID FROM stream_fields WHERE DevID IN (SELECT DevID FROM #DevicesToDelete));DELETE FROM reference WHERE DeltaDevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM stream_fields WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM streams WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM device_status WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM devices WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM devices WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM port WHERE PortID IN (SELECT PortId FROM devices WHERE DevID IN (SELECT DevID FROM #DevicesToDelete) AND PortId IS NOT NULL);DELETE FROM port WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM [servers] WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM nodes WHERE NodID IN (SELECT NodID FROM #NodesToDelete);DELETE FROM zones WHERE ZonID NOT IN ({0});DROP TABLE #DevicesToDelete;DROP TABLE #ServersToDelete;DROP TABLE #NodesToDelete;", ids);

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalZonesFromDatabase' - Errore durante l'accesso alla base dati: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalZonesFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return false;
        }

        /// <summary>
        ///     Elimina le stazioni non contenute nella lista di Id passati
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <param name="ids">Lista di Id da mantenere</param>
        /// <returns>True se l'operazione ha successo, false altrimenti</returns>
        public static bool RemoveAdditionalNodesFromDatabase(string dbConnectionString, string ids)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalNodesFromDatabase' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;

                if (!String.IsNullOrEmpty(ids))
                {
                    cmd.CommandText = String.Format("SELECT DevID INTO #DevicesToDelete FROM devices WHERE NodID NOT IN ({0});SELECT SrvID INTO #ServersToDelete FROM [servers] WHERE NodID NOT IN ({0});DELETE FROM [events] WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM [procedures] WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM reference WHERE ReferenceID IN (SELECT ReferenceID FROM stream_fields WHERE DevID IN (SELECT DevID FROM #DevicesToDelete));DELETE FROM reference WHERE DeltaDevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM stream_fields WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM streams WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM device_status WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM devices WHERE DevID IN (SELECT DevID FROM #DevicesToDelete);DELETE FROM devices WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM port WHERE PortID IN (SELECT PortId FROM devices WHERE DevID IN (SELECT DevID FROM #DevicesToDelete) AND PortId IS NOT NULL);DELETE FROM port WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM [servers] WHERE SrvID IN (SELECT SrvID FROM #ServersToDelete);DELETE FROM nodes WHERE NodID NOT IN ({0});DROP TABLE #DevicesToDelete;DROP TABLE #ServersToDelete;", ids);

                    cmd.Connection = dbConnection;

                    dbConnection.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalNodesFromDatabase' - Errore durante l'accesso alla base dati: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.RemoveAdditionalNodesFromDatabase' - Errore durante l'accesso alla base dati. Configurazione dei campi attesi da base dati non validi: (" +
                    ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    Logger.E_STACK_TRACE);

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return false;
        }


        /// <summary>
        ///     Verifica la disponibilità di accesso al DB
        /// </summary>
        /// <param name="dbConnectionString">Stringa di connessione al database SQL Server</param>
        /// <returns>True se l'operazione ha successo, false altrimenti</returns>
        public static bool CheckDBInstance(string dbConnectionString, bool onLog)
        {
            SqlConnection dbConnection = null;
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr = null;

            if (onLog) Logger.Default.Log(
                String.Format("CheckDBInstance:  DB connection string {0}", dbConnectionString),
                Logger.LogType.CONSOLE_LOC_FILE,
                Logger.EventType.INFORMATION,
                Logger.E_STACK_TRACE);

            try
            {
                try
                {
                    dbConnection = new SqlConnection(dbConnectionString);
                }
                catch (ArgumentException ex)
                {
                    Logger.Default.Log(
                        "ECCEZIONE in 'SqlDatabaseUtility.GetExistingCentralWSData' - La stringa di connessione al database non è valida: (" +
                        ex.GetType() + ") " + ex.Message + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        Logger.E_STACK_TRACE);

                    return false;
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = Settings.Default.SqlCommandTimeoutSeconds;
                cmd.CommandText = "USE Telefin";
                cmd.Connection = dbConnection;

                dbConnection.Open(); 

                dr = cmd.ExecuteReader();

                if (onLog) Logger.Default.Log(
                    String.Format("CheckDBInstance Telefin:  DB attivo "),
                    Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.INFORMATION,
                    Logger.E_STACK_TRACE);
 
            }
            catch (SqlException ex)
            {
                Logger.Default.Log(
                    "ECCEZIONE in 'SqlDatabaseUtility.CheckDBInstance' - Errore durante l'accesso alla base dati: (" +
                    ex.GetType() + ") " + ex.Message + " - " /*+ ex.StackTrace*/, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR
                );

                return false;
            }
            catch (ThreadAbortException)
            {
                return false;
            }
            finally
            {
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }

                cmd.Dispose();
            }

            return true;
        }
    }
}


﻿using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace STLCManager.Service.Database
{
    public static class CompressedBinaryConverter
    {
        /// <summary>
        ///     Converts a string into a byte array and then compresses it
        /// </summary>
        /// <param name="s">The string to convert</param>
        /// <returns>A compressed byte array that was the string</returns>
        public static byte[] ToByteArray(string s)
        {
            if (s == null)
            {
                return new byte[0];
            }

            byte[] bytes = Encoding.UTF8.GetBytes(s);

            using (MemoryStream msi = new MemoryStream(bytes))
            {
                using (MemoryStream mso = new MemoryStream())
                {
                    using (GZipStream gs = new GZipStream(mso, CompressionMode.Compress))
                    {
                        StreamCopyTo(msi, gs);
                    }

                    return mso.ToArray();
                }
            }
        }

        /// <summary>
        ///     Converts a byte array back into a sting and uncompresses it
        /// </summary>
        /// <param name="byteArray">Compressed byte array to convert</param>
        /// <returns>The string that was in the byte array</returns>
        public static string ToOriginalString(byte[] byteArray)
        {
            if (byteArray.Length == 0)
            {
                return null;
            }

            using (MemoryStream msi = new MemoryStream(byteArray))
            {
                using (MemoryStream mso = new MemoryStream())
                {
                    using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                    {
                        StreamCopyTo(gs, mso);
                    }

                    return Encoding.UTF8.GetString(mso.ToArray());
                }
            }
        }

        // Only useful before .NET 4
        private static void StreamCopyTo(Stream input, Stream output)
        {
            byte[] buffer = new byte[16*1024]; // Fairly arbitrary size
            int bytesRead;

            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, bytesRead);
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace STLCManager.Service.Database
{
    internal class SupervisorUtility
    {
        #region Id supervisori

        public const int UNKNOWN_SUPERVISOR_ID = -1;
        public const int SERIAL_SUPERVISOR_ID = 0;
        public const int SNMP_SUPERVISOR_ID = 1;
        public const int MODBUS_SUPERVISOR_ID = 2;
        public const int TELNET_SUPERVISOR_ID = 3;

        #endregion

        /// <summary>
        /// Ritorna il nome del supervisore associato all'Id
        /// </summary>
        /// <param name="supervisorId">Id numerico del supervisore</param>
        /// <returns>Nome del supervisore</returns>
        public static string DecodeSupervisorID(int supervisorId)
        {
            switch (supervisorId)
            {
                case UNKNOWN_SUPERVISOR_ID:
                    return "Sconosciuto";
                case SERIAL_SUPERVISOR_ID:
                    return "Seriale";
                case SNMP_SUPERVISOR_ID:
                    return "SNMP";
                case MODBUS_SUPERVISOR_ID:
                    return "ModBus";
                case TELNET_SUPERVISOR_ID:
                    return "Telnet";
                default:
                    return "N/D";
            }
        }

        /// <summary>
        /// Ritorna la descrizione testuale dei supervisori richiesti per il monitoraggio
        /// </summary>
        /// <param name="supervisorList">Lista di numeri interi relativi ai vari Id Supervisore necessari</param>
        /// <returns>Lista di nomi di supervisori richiesti</returns>
        public static string GetNeededSupervisorListNames(ReadOnlyCollection<int> supervisorList)
        {
            StringBuilder neededSupervisorListNames = new StringBuilder();

            if (supervisorList != null)
            {
                foreach (int supervisorId in supervisorList)
                {
                    if (neededSupervisorListNames.Length > 0)
                    {
                        neededSupervisorListNames.Append(", ");
                    }

                    neededSupervisorListNames.Append(DecodeSupervisorID(supervisorId));
                }
            }

            return neededSupervisorListNames.ToString();
        }

        /// <summary>
        /// Ritorna un booleano che indica se l'Id supervisore passato è contenuto tra quelli validi
        /// </summary>
        /// <param name="supervisorId">Id del Supervisore da controllare</param>
        /// <returns>Booleano che indica se l'Id è valido</returns>
        public static bool IsSupervisorIdValid(int supervisorId)
        {
            var validSupervisorIds = new List<int>
            {
                SERIAL_SUPERVISOR_ID,
                SNMP_SUPERVISOR_ID,
                MODBUS_SUPERVISOR_ID,
                TELNET_SUPERVISOR_ID
            };

            return validSupervisorIds.Contains(supervisorId);
        }
    }
}
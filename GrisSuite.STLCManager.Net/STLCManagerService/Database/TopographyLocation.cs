﻿using System.Globalization;

namespace STLCManager.Service.Database
{
    /// <summary>
    ///   Informazioni topografiche della periferica
    /// </summary>
    public class TopographyLocation
    {
        /// <summary>
        ///   ID della Stazione
        /// </summary>
        public int StationId { get; private set; }

        /// <summary>
        ///   Nome della Stazione
        /// </summary>
        public string StationName { get; private set; }

        /// <summary>
        ///   Id del Fabbricato
        /// </summary>
        public int BuildingId { get; private set; }

        /// <summary>
        ///   Nome del Fabbricato
        /// </summary>
        public string BuildingName { get; private set; }

        /// <summary>
        ///   Descrizione del Fabbricato
        /// </summary>
        public string BuildingNotes { get; private set; }

        /// <summary>
        ///   Id dell'Armadio
        /// </summary>
        public int LocationId { get; private set; }

        /// <summary>
        ///   Nome dell'Armadio
        /// </summary>
        public string LocationName { get; private set; }

        /// <summary>
        ///   Tipo dell'Armadio
        /// </summary>
        public string LocationType { get; private set; }

        /// <summary>
        ///   Descrizione dell'Armadio
        /// </summary>
        public string LocationNotes { get; private set; }

        public TopographyLocation(int stationId, string stationName, int buildingId, string buildingName, string buildingNotes, int locationId, string locationName,
                                  string locationType, string locationNotes)
        {
            this.StationId = stationId;
            this.StationName = stationName;
            this.BuildingId = buildingId;
            this.BuildingName = buildingName;
            this.BuildingNotes = buildingNotes;
            this.LocationId = locationId;
            this.LocationName = locationName;
            this.LocationType = locationType;
            this.LocationNotes = locationNotes;
        }

        /// <summary>
        ///   Esegue una ricerca lineare sulla topografia, in base a Id di Stazione, Fabbricato e Armadio
        /// </summary>
        /// <param name = "stationId">Id di Stazione</param>
        /// <param name = "buildingId">Id di Fabbricato</param>
        /// <param name = "locationId">Id di Armadio</param>
        /// <returns></returns>
        public TopographyLocation GetTopography(int stationId, int buildingId, int locationId)
        {
            if ((this.StationId == stationId) && (this.BuildingId == buildingId) && (this.LocationId == locationId))
            {
                return this;
            }

            return null;
        }

        /// <summary>
        ///   Produce una rappresentazione completa della topografia
        /// </summary>
        /// <returns>Rappresentazione testuale della topografia</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture,
                                 "Station Id: {0}, Station Name: {1}, Building Id: {2}, Building Name: {3}, Building Notes: {4}, Location Id: {5}, Location Name: {6}, Location Type: {7}, Location Notes: {8}",
                                 this.StationId, this.StationName, this.BuildingId, this.BuildingName, this.BuildingNotes, this.LocationId, this.LocationName, this.LocationType,
                                 this.LocationNotes);
        }
    }
}
﻿using System;
using System.Globalization;
using System.Net;

namespace STLCManager.Service.Database
{
    /// <summary>
    ///     Rappresenta l'oggetto di stato per il passaggio di informazioni relative al device
    /// </summary>
    public class DeviceObject
    {
        #region Costruttore

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="supervisorId">Id numerico del Supervisore che monitora la periferica</param>
        /// <param name="deviceId">ID della periferica</param>
        /// <param name="name">Nome della periferica</param>
        /// <param name="type">Tipo della periferica</param>
        /// <param name="address">Indirizzo IP della periferica</param>
        /// <param name="regionId">ID del Compartimento</param>
        /// <param name="regionName">Nome del Compartimento</param>
        /// <param name="zoneId">ID della Zona</param>
        /// <param name="zoneName">Nome della Zona</param>
        /// <param name="nodeId">ID del Nodo</param>
        /// <param name="nodeName">Nome del Nodo</param>
        /// <param name="serverId">ID del Server che monitora la periferica</param>
        /// <param name="serverHost">Host che monitora la periferica</param>
        /// <param name="serverName">Nome del Server che monitora la periferica</param>
        /// <param name="serialNumber">Numero di serie della periferica</param>
        /// <param name="systemPort">Porta della periferica</param>
        /// <param name="profileId">ID del profilo di configurazione</param>
        /// <param name="active">Indica se la periferica è attiva</param>
        /// <param name="scheduled">Indica se la periferica è schedulato</param>
        /// <param name="rackPositionColumn">Indice di colonna della posizione della periferica nella sua locazione</param>
        /// <param name="rackPositionRow">Indice di riga della posizione della periferica nella sua locazione</param>
        /// <param name="topography">Informazioni Topografiche della periferica</param>
        /// <param name="monitoringDeviceId">Identifica l'apparato che esegue il monitoraggio della periferica</param>
        public DeviceObject(int supervisorId,
            long deviceId,
            string name,
            string type,
            string address,
            long regionId,
            string regionName,
            long zoneId,
            string zoneName,
            long nodeId,
            string nodeName,
            int serverId,
            string serverHost,
            string serverName,
            string serialNumber,
            SystemPort systemPort,
            int profileId,
            byte active,
            byte scheduled,
            int rackPositionColumn,
            int rackPositionRow,
            TopographyLocation topography,
            string monitoringDeviceId)
        {
            this.SupervisorId = supervisorId;
            this.DeviceId = deviceId;
            this.Name = name;
            this.DeviceType = type;
            this.Address = address;
            this.RegionId = regionId;
            this.RegionName = regionName;
            this.ZoneId = zoneId;
            this.ZoneName = zoneName;
            this.NodeId = nodeId;
            this.NodeName = nodeName;
            this.ServerId = serverId;
            this.ServerHost = serverHost;
            this.ServerName = serverName;
            this.SerialNumber = serialNumber;
            this.Port = systemPort;
            this.ProfileId = profileId;
            this.Active = active;
            this.Scheduled = scheduled;
            this.RackPositionColumn = rackPositionColumn;
            this.RackPositionRow = rackPositionRow;
            this.Topography = topography;
            this.MonitoringDeviceId = monitoringDeviceId;
        }

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="deviceId">ID della periferica</param>
        /// <param name="name">Nome della periferica</param>
        /// <param name="type">Tipo della periferica</param>
        public DeviceObject(long deviceId,
            string name,
            string type)
        {
            this.SupervisorId = SupervisorUtility.UNKNOWN_SUPERVISOR_ID;
            this.DeviceId = deviceId;
            this.Name = name;
            this.DeviceType = type;
            this.Address = null;
            this.RegionId = 0;
            this.RegionName = String.Empty;
            this.ZoneId = 0;
            this.ZoneName = String.Empty;
            this.NodeId = 0;
            this.NodeName = String.Empty;
            this.ServerId = 0;
            this.ServerHost = String.Empty;
            this.ServerName = String.Empty;
            this.SerialNumber = String.Empty;
            this.Port = null;
            this.ProfileId = 0;
            this.Active = 0;
            this.Scheduled = 0;
            this.RackPositionColumn = 0;
            this.RackPositionRow = 0;
            this.Topography = null;
            this.MonitoringDeviceId = String.Empty;
        }

        #endregion

        #region Proprietà pubbliche

        /// <summary>
        /// Id numerico del Supervisore che monitora la periferica
        /// </summary>
        public int SupervisorId { get; private set; }

        /// <summary>
        ///     ID della periferica
        /// </summary>
        public long DeviceId { get; private set; }

        /// <summary>
        ///     Nome della periferica
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///     Tipo della periferica
        /// </summary>
        public string DeviceType { get; private set; }

        /// <summary>
        ///     Indirizzo IP della periferica
        /// </summary>
        public string Address { get; private set; }

        /// <summary>
        ///     ID del Compartimento
        /// </summary>
        public long RegionId { get; private set; }

        /// <summary>
        ///     Nome del Compartimento
        /// </summary>
        public string RegionName { get; private set; }

        /// <summary>
        ///     ID della Zona
        /// </summary>
        public long ZoneId { get; private set; }

        /// <summary>
        ///     Nome della Zona
        /// </summary>
        public string ZoneName { get; private set; }

        /// <summary>
        ///     ID del Nodo
        /// </summary>
        public long NodeId { get; private set; }

        /// <summary>
        ///     Nome del Nodo
        /// </summary>
        public string NodeName { get; private set; }

        /// <summary>
        ///     Informazioni dell'ultimo evento in ordine temporale associato alla periferica
        /// </summary>
        public EventObject LastEvent { get; set; }

        /// <summary>
        ///     ID del server che monitora la periferica
        /// </summary>
        public int ServerId { get; private set; }

        /// <summary>
        ///     Host che monitora la periferica
        /// </summary>
        public string ServerHost { get; private set; }

        /// <summary>
        ///     Nome del Server che monitora la periferica
        /// </summary>
        public string ServerName { get; private set; }

        /// <summary>
        ///     Numero di serie della periferica
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        ///     ID dell'interfaccia della periferica
        /// </summary>
        public int PortId
        {
            get { return this.Port.Id; }
        }

        /// <summary>
        ///     Porta della periferica
        /// </summary>
        public SystemPort Port { get; private set; }

        /// <summary>
        ///     ID del profilo di configurazione
        /// </summary>
        public int ProfileId { get; private set; }

        /// <summary>
        ///     Indica se la periferica è attivo
        /// </summary>
        public byte Active { get; private set; }

        /// <summary>
        ///     Indica se la periferica è schedulato
        /// </summary>
        public byte Scheduled { get; private set; }

        /// <summary>
        ///     Indice di colonna della posizione della periferica nella sua locazione
        /// </summary>
        public int RackPositionColumn { get; private set; }

        /// <summary>
        ///     Indice di riga della posizione della periferica nella sua locazione
        /// </summary>
        public int RackPositionRow { get; private set; }

        /// <summary>
        ///     Informazioni Topografiche della periferica
        /// </summary>
        public TopographyLocation Topography { get; private set; }

        /// <summary>
        ///     Identifica l'apparato che esegue il monitoraggio della periferica
        /// </summary>
        public string MonitoringDeviceId { get; private set; }

        #endregion

        #region Metodi pubblici

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture,
                "Device Id: {0}, Nome Periferica: {1}, Indirizzo IP: {2}, Type: {3}, {4}, Region Id: {5}, Region: {6}, Zone Id: {7}, Zone: {8}, Node Id: {9}, Node: {10}, Server Id: {11}, Server Host: {12}, Server Name: {13}, Serial Number: {14}, Port Id: {15}, Profile Id: {16}, Active: {17}, Scheduled: {18}, RackPositionColumn: {19}, RackPositionRow: {20}",
                this.DeviceId, this.Name, this.Address, this.DeviceType, this.Topography, this.RegionId, this.RegionName,
                this.ZoneId, this.ZoneName, this.NodeId, this.NodeName, this.ServerId, this.ServerHost, this.ServerName,
                this.SerialNumber, this.PortId, this.ProfileId, this.Active, this.Scheduled, this.RackPositionColumn,
                this.RackPositionRow);
        }

        #endregion
    }
}
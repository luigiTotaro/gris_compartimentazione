﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using System.Xml;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Win32;
using STLCManager.Service.ClientComm;
using STLCManager.Service.Database;
using STLCManager.Service.FBWF;
using STLCManager.Service.Function;
using STLCManager.Service.Types;
using STLCManager.Service.Wrapper;
using Timer = System.Timers.Timer;

namespace STLCManager.Service.Engine
{
    public class STLCEngine
    {
        private const int ENGINE_TIME_SCHEDULING = 2000;
        public const int DELAY_TO_START_SYSTEM_CONFIG = 5000;
        public const int NO_ERROR = AppCfg.COD_NO_ERROR;
        private const int XML_FILE_VERSION_NOT_VALID_TIME_REG = (5*60*1000);

        private Timer timerEngine;
        private bool isEngineRunning;
        private string SoftwareVersion;
        private bool xmlFileVersionNotValid;
        private DateTime xmlFileVersionNotValidTimeReg;
        public SystemFBWF fbwfCurrentSession = null;
        public SystemServices systemServices = null;
        public bool configInProgress;
        private ClientLayerWCFComm clientCommLayer;
        public  STLCConfig Config;
        public Thread configThread; // thread di gestione della configurazione da file XML
        public EngineMode engineMode;

        /// <summary>
        ///     Segnala sistema in allarme (errore permanente)
        /// </summary>
        public bool IsFailure
        {
            get { return this.systemServices.IsFailure || this.fbwfCurrentSession.IsFailure || SystemWDog.IsThreadsFailure || SystemFan.IsFailure; }
        }

        /// <summary>
        ///     Segnala sistema in preallarme (errore con funzione di rispristino attiva)
        /// </summary>
        public bool IsWarning
        {
            get { return this.systemServices.IsWarning; }
        }

        /// <summary>
        ///     Segnala sistema in preallarme (errore con funzione di rispristino attiva)
        /// </summary>
        public bool IsStopping()
        {
            return this.systemServices.IsStopping; 
        }

        public void IsStopping( bool _isStopping)
        {
            this.systemServices.IsStopping = _isStopping;
        }

        #region Constructor

        /// <summary>
        ///     Inizializza una nuova istanza della classe STLCEngine. Servizio di controllo attivita STLC1000.
        /// </summary>
        public STLCEngine()
        {
            AppCfg.Default.LoadConfig();

            Logger.Default.Log("", Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log("", Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log("STLCManager service STARTING ...", Logger.LogType.CONSOLE_LOC_FILE);

            AppCfg.Default.UpdateRegistry();

            this.ManagerInitialize(false);
            this.IsStopping(false);
        }

        #endregion

        #region Methods (Abilita gestione STLC)

        private int ManagerInitialize(bool _reloadConfig)
        {
            int retCode = NO_ERROR;

            try
            {
                this.timerEngine        = null;
                this.configInProgress   = false;

                this.fbwfCurrentSession = new SystemFBWF(FileBasedWriteFilter.Session.Current);
                this.systemServices     = new SystemServices();

                if (this.clientCommLayer == null)
                {
                    //Instanziamento del layer di comunicazione WCF col client
                    this.clientCommLayer                                 = new ClientLayerWCFComm();
                    this.clientCommLayer.OnGetSTLCInfo                  += this.clientCommLayer_OnGetSTLCInfo;
                    this.clientCommLayer.OnGetSTLCStatus                += this.clientCommLayer_OnGetSTLCStatus;
                    this.clientCommLayer.OnSetSTLCMode                  += this.clientCommLayer_OnSetSTLCMode;
                    this.clientCommLayer.OnSetSTLCServiceStatus         += this.clientCommLayer_OnSetSTLCServiceStatus;
                    this.clientCommLayer.OnGetSTLCServiceStatus         += this.clientCommLayer_OnGetSTLCServiceStatus;
                    this.clientCommLayer.OnSTLCShutdown                 += this.clientCommLayer_OnSTLCShutdown;
                    this.clientCommLayer.OnSTLCApplySystemConfig        += this.clientCommLayer_OnSTLCApplySystemConfig;
                    this.clientCommLayer.OnGetSTLCNetwork               += this.clientCommLayer_OnGetSTLCNetwork;
                    this.clientCommLayer.OnReloadConfig                 += this.clientCommLayer_OnReloadConfig;
                    this.clientCommLayer.OnReloadSystemXmlIntoDatabase  += this.clientCommLayer_OnReloadSystemXmlIntoDatabase;
                    this.clientCommLayer.OnUpdateRegionListFromWS       += this.clientCommLayer_OnUpdateRegionListFromWS;
                    this.clientCommLayer.OnUpdateDeviceTypeListFromWS   += this.clientCommLayer_OnUpdateDeviceTypeListFromWS;
                    this.clientCommLayer.OnGetNeededSupervisors         += this.clientCommLayer_OnGetNeededSupervisors;
                    this.clientCommLayer.OnSetConfigOnRegistry          += this.clientCommLayer_OnSetConfigOnRegistry;
                    this.clientCommLayer.OnEmptyDatabase                += this.clientCommLayer_OnEmptyDatabase;
                }

                this.SoftwareVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

                if (!_reloadConfig)
                {
                    Logger.Default.Log("", Logger.LogType.CONSOLE_LOC_FILE);
                    Logger.Default.Log(
                        "STLCManager " + this.SoftwareVersion + " Service STARTED (Profile: " + STLCConfig.CurrentHardwareProfile + ")",
                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, AppCfg.I_COD_SERVICE_MANAGER_STARTED); //notice
                    Logger.Default.Log("", Logger.LogType.CONSOLE_LOC_FILE);

                    AppCfg.Default.LogAllCfg();
                }

                #region Caricamento Jida

#if (!DEBUG)
				if (STLCConfig.IsSTLCHost)
				{
					// Caricamento Jida
					retCode = JidaPlatform.Default.LoadJIDA();
					if (retCode == NO_ERROR)
					{
						Logger.Default.Log("Libreria JIDA caricata", Logger.LogType.CONSOLE_LOC_FILE);
					}
					else
					{
						Logger.Default.Log("Caricamento libreria JIDA fallito. Errore: " + retCode, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
					}
				}
#endif

                #endregion

                //Carica la configurazione
                retCode = this.ConfigInit();
                this.engineMode = new EngineMode(systemServices, Config);
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
                retCode = AppCfg.E_COD_ENGINE_THREAD_FAILURE;
            }

            return retCode;
        }

        /// <summary>
        ///     Abilita gestione STLC.
        /// </summary>
        public void STLCEnable()
        {
            try
            {
                #region Gestione WatchDog

                if (STLCConfig.IsSTLCHost && SystemWDog.Enable(this.Config.IsWatchDogActive, this.Config.RebootOnFailure))
                {
                    Logger.Default.Log(
                        String.Format("Attivata gestione WatchDog (timeout={0} sec, on_service_stop_timeout={1} ore).",
                            SystemWDog.OnServiceRunningTimeout, SystemWDog.OnServiceStopTimeout), Logger.LogType.CONSOLE_LOC_FILE);
                }

                #endregion

                #region Gestione SystemLed

                if (STLCConfig.IsSTLCHost && this.Config.IsSystemLedActive)
                {
                    if (!SystemLed.Enable())
                    {
                        Logger.Default.Log("ERRORE: attivazione gestione SystemLed", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                            AppCfg.E_COD_LED_THREAD_FAILURE);
                    }

                    SystemLed.Set(SystemLed.Mode.SYSTEM_INIT);

                    if (this.Config.IsSystemLedRunning)
                    {
                        SystemWDog.ThreadUnderControl(AppCfg.ThreadSystemLED, SystemLed.ThreadPeriod, SystemLed.IsRunning);
                    }
                }

                #endregion

                #region Gestione SystemFan

                if (STLCConfig.IsSTLCHost && this.Config.IsFanActive)
                {
                    if (!SystemFan.Enable())
                    {
                        Logger.Default.Log("ERRORE: attivazione gestione SystemFan", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                            AppCfg.E_COD_FAN_THREAD_FAILURE);
                    }

                    if (this.Config.IsFanRunning)
                    {
                        SystemWDog.ThreadUnderControl(AppCfg.ThreadSystemFANS, SystemFan.ThreadPeriod, SystemFan.IsRunning);
                    }
                }
                else
                {
                    if (SystemFan.IsEnabled)
                    {
                        SystemFan.Disable();
                    }
                    else
                    {
                        Logger.Default.Log("Disattivata gestione SystemFan", Logger.LogType.CONSOLE_LOC_FILE);
                    }
                }
                SystemFan.Debug();

                #endregion

                #region Gestione FBWF

                if (STLCConfig.IsSTLCHost && this.fbwfCurrentSession.IsFilterEnabled() && this.Config.IsFBWFActive)
                {
                    if (!this.fbwfCurrentSession.Enable())
                    {
                        Logger.Default.Log("ERRORE: attivazione gestione SystemFBWF.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                            AppCfg.E_COD_FBWF_THREAD_FAILURE);
                    }

                    if (this.Config.IsFBWFRunning)
                    {
                        SystemWDog.ThreadUnderControl(AppCfg.ThreadSystemFBWF, this.fbwfCurrentSession.ThreadPeriod,
                            this.fbwfCurrentSession.IsRunning);
                    }
                }

                #endregion

                this.ApplySystemConfig();

                #region Gestione Servizi

                if ((this.Config != null) && (this.Config.IsServiceActive))
                {
                    if (!this.systemServices.Start(true)) // Nel caso sia impossibile fare lo start dei servizi (per es DB non attivo) stop del servizio STLCManager.
                    {
                        ServiceController sc = new ServiceController("STLCManagerService");
                        sc.Stop();
                        return;
                    }

                    if (this.IsStopping() == true) //In caso di richista di stop non attivo il monitoraggio dei servizi.
                        return;
                    
                    if (!this.systemServices.Enable())
                    {
                        Logger.Default.Log("ERRORE: attivazione gestione SystemServices.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                            AppCfg.E_COD_SERVICES_THREAD_FAILURE);
                    }

                    if (this.Config.IsServiceRunning)
                    {
                        SystemWDog.ThreadUnderControl(AppCfg.ThreadSystemSERVICES, this.systemServices.ThreadPeriod, this.systemServices.IsRunning);
                    }

                    this.systemServices.Debug();
                }

                #endregion

                #region Visualizzazione SerialNumber e Version

#if (!DEBUG)
				string sSN = "", sVersion = "";
				String errDesc = "";

				if (JidaPlatform.Default.GetSerialNumber(out sSN, out errDesc) == NO_ERROR)
				{
					Logger.Default.Log("Serial Number = " + sSN, Logger.LogType.CONSOLE_LOC_FILE);
				}
				else
				{
					Logger.Default.Log("GetSerialNumber: " + errDesc, Logger.LogType.CONSOLE_LOC_FILE);
				}

				if (this.GetSTLCVersion(out sVersion, out errDesc) == NO_ERROR)
				{
					Logger.Default.Log("STLC Version = " + sVersion, Logger.LogType.CONSOLE_LOC_FILE);
				}
				else
				{
					Logger.Default.Log("GetSTLCVersion: " + errDesc, Logger.LogType.CONSOLE_LOC_FILE);
				}
#endif

                #endregion

                // Se la versione del file xml non corrisponde a quella dell'applicazione comanda registrazione periodica messaggio di warning
                if (this.xmlFileVersionNotValid)
                {
                    this.xmlFileVersionNotValidTimeReg = DateTime.Now.AddMilliseconds(XML_FILE_VERSION_NOT_VALID_TIME_REG);
                }

                // Attiva thread periodico di sincronizzazione
                this.timerEngine = new Timer(ENGINE_TIME_SCHEDULING);
                this.timerEngine.Elapsed += this.ManageEngine;
                this.timerEngine.AutoReset = true;
                this.timerEngine.Enabled = true;
                this.timerEngine.Start();

                SystemWDog.ThreadUnderControl(AppCfg.ThreadSystemENGINE, ENGINE_TIME_SCHEDULING, this.IsEngineRunning);
                Logger.Default.Log("Attivata gestione SystemEngine.", Logger.LogType.CONSOLE_LOC_FILE);
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
            }
        }

        #endregion

        #region Methods (thread di sincronismo)

        /// <summary>
        ///     Restituisce il flag di running (processo in eseguzione corretta) e lo azzera
        /// </summary>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: avvenuto ciclo di schedulazione dopo ultima chiamata</item>
        ///         <item>false: nessuna schedulazione avvenuta</item>
        ///     </list>
        /// </returns>
        public bool IsEngineRunning()
        {
            if (this.isEngineRunning)
            {
                this.isEngineRunning = false;
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Thread di sincronizzazione, aggiorna stato complessivo sistema per gestione watchdog e led di sistema
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManageEngine(object sender, ElapsedEventArgs e)
        {
            try
            {
                this.timerEngine.Enabled = false;

                this.isEngineRunning = true;

                SystemWDog.SystemFailure = this.IsFailure;

                if (this.configInProgress)
                {
                    SystemLed.Set(SystemLed.Mode.SYSTEM_CONFIG);
                }
                else if (SystemWDog.IsMaintenanceEnabled)
                {
                    SystemLed.Set(SystemLed.Mode.SYSTEM_MAINTENANCE);
                }
                else if (this.IsFailure)
                {
                    SystemLed.Set(SystemLed.Mode.SYSTEM_FAILURE);
                }
                else if (this.IsWarning)
                {
                    SystemLed.Set(SystemLed.Mode.SYSTEM_WARNING);
                }
                else
                {
                    SystemLed.Set(SystemLed.Mode.SYSTEM_RUNNING);
                }

                this.timerEngine.Enabled = true;
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'STLCEngine.ManagerEngine': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
        }

        #endregion

        #region Funzioni esposte con WCF

        /// <summary>
        ///     Richiesta informazioni generali STLC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnGetSTLCInfo(object sender, STLCInfoEventArgs e)
        {
            String errDesc;
            string sSN = "", sVersion = "", sHostName = "";

            try
            {
                //Logger.Default.Log("Richiesta STLCInfo da parte di un Client di tipo '" + e.Request.clientType + "'", Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

#if (!DEBUG)
				int retCode = JidaPlatform.Default.GetSerialNumber(out sSN, out errDesc);
#else
                int retCode = NO_ERROR;
                sSN = "n/a";
                errDesc = "";
#endif

                // richiesta Serial Number STLC
                if (retCode == NO_ERROR)
                {
                    e.Result.data.serialNumber = sSN;
                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = retCode;
                    e.Result.status.errorDescription = "GetSerialNumber: " + errDesc;
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                }

                if (retCode == NO_ERROR)
                {
                    retCode = this.GetSTLCVersion(out sVersion, out errDesc);
                }

                if (retCode == NO_ERROR)
                {
                    e.Result.data.version = sVersion;
                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = retCode;
                    e.Result.status.errorDescription = errDesc;
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                }

                // richiesta Host name STLC
                if (retCode == NO_ERROR)
                {
                    retCode = SysInfoFunction.Default.GetComputerName(out sHostName, out errDesc);
                }

                if (retCode == NO_ERROR)
                {
                    e.Result.data.hostName = sHostName;
                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = retCode;
                    e.Result.status.errorDescription = "GetSTLCVersion: " + errDesc;
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                }

                // profilo hardware
                e.Result.data.hardwareProfile = AppCfg.Default.hpHardwareProfile.ToString(); //  .HARDWARE_PROFILE.ToString();
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnGetSTLCInfo':" + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Richiesta stato generale STLC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnGetSTLCStatus(object sender, STLCStatusEventArgs e)
        {
            try
            {
                //Logger.Default.Log("Richiesta STLCStatus da parte di un Client di tipo '" + e.Request.clientType + "'",
                //    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                e.Result.data.mode = (SystemWDog.IsMaintenanceEnabled ? STLCMode.MAINTENANCE : STLCMode.NORMAL);

                switch (SystemFan.Sensor)
                {
                    case SystemFan.SensorTemperature.SENSOR_CPU:
                        e.Result.data.sensorTemperature = STLCSensorTemperature.CPU;
                        break;
                    case SystemFan.SensorTemperature.SENSOR_BOARD:
                        e.Result.data.sensorTemperature = STLCSensorTemperature.MOTHER_BOARD;
                        break;
                    default:
                        e.Result.data.sensorTemperature = STLCSensorTemperature.NOT_AVAILABLE;
                        break;
                }

                int temperature;
                if (SystemFan.GetTemperature(out temperature) == NO_ERROR)
                {
                    e.Result.data.temperature = temperature;
                }
                else
                {
                    e.Result.data.temperature = -1;
                }

                e.Result.data.pwmRightFan = SystemFan.CurrentThreshold.PWMRightFan;
                e.Result.data.pwmLeftFan = SystemFan.CurrentThreshold.PWMLeftFan;

                e.Result.data.filterFBWFEnabled = this.fbwfCurrentSession.IsFilterEnabled();

                e.Result.status.level = StatusLevel.SUCCESS;
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE on 'OnGetSTLCStatus': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Esegue comando di impostazione modalità di funzionamento STLC (normale / manutenzione)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comandi</param>
        private void clientCommLayer_OnSetSTLCMode(object sender, STLCModeEventArgs e)
        {
            try
            {
                Logger.Default.Log("Richiesta SetSTLCMode da parte di un Client di tipo '" + e.Request.clientType + "' (Mode= " + (e.Request.mode==STLCMode.MAINTENANCE?"MAINTENANCE":"NORMAL") +")",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                switch (e.Request.mode)
                {
                    case STLCMode.MAINTENANCE:
                        this.engineMode.SetSTLCMode(STLCMode.MAINTENANCE, e.Request.managedTimeoutHours);
                        break;

                    case STLCMode.NORMAL:
                        this.engineMode.SetSTLCMode(STLCMode.NORMAL, 0);
                        break;

                    default:
                        e.Result.status.level = StatusLevel.FAIL;
                        e.Result.status.errorCode = AppCfg.E_COD_SERVICE_MODE_NOT_AVAILABLE;
                        e.Result.status.errorDescription = "ERRORE in 'OnSetSTLCMode': Parametri non validi.";
                        Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                            e.Result.status.errorCode);
                        return;
                }
                e.Result.data.mode = (SystemWDog.IsMaintenanceEnabled ? STLCMode.MAINTENANCE : STLCMode.NORMAL);
                e.Result.status.level = StatusLevel.SUCCESS;
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnSetSTLCMode': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Esegue comando di configurazione stato servizi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametro comando</param>
        private void clientCommLayer_OnSetSTLCServiceStatus(object sender, STLCServiceStatusEventArgs e)
        {
            List<string> servicesName = new List<string>();
            try
            {
                Logger.Default.Log(
                    "Richiesto SetSTLCServiceStatus(" + e.Request.command + ") da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                if (!String.IsNullOrEmpty(e.Request.group))
                {
                    Logger.Default.Log("per gruppo " + e.Request.group, Logger.LogType.CONSOLE_LOC_FILE);
                }

                else if ((e.Request.services != null) && (e.Request.services.Count > 0))
                {
                    foreach (string serviceName in e.Request.services)
                    {
                        Logger.Default.Log("per servizio " + serviceName, Logger.LogType.CONSOLE_LOC_FILE);
                        servicesName.Add(serviceName);
                    }
                }
                else
                {
                    Logger.Default.Log("per tutti i servizi", Logger.LogType.CONSOLE_LOC_FILE);
                }

                if ((e.Request.command == STLCServiceCommand.STOP) && (e.Request.group == null) && (e.Request.services == null))
                {
                    // Richiesto stop di tutti i servizi, per evitare che il thread di verifica li ripristini, impostiamo la modalità maintenance
                    this.engineMode.SetSTLCMode(STLCMode.MAINTENANCE, -1);
                }
                else if ((e.Request.command == STLCServiceCommand.START) && (e.Request.group == null) && (e.Request.services == null))
                {
                    // Richiesto avvio di tutti i servizi, quindi la modalità è quella normale
                    this.engineMode.SetNormalMode();
                }

                Thread th = new Thread(() => { this.ExecuteCommandOnServices(e.Request.command, e.Request.group, servicesName); });
                th.Start();

                e.Result.status.level = StatusLevel.SUCCESS;
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnSetSTLCServiceStatus': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Richiede lo stato dei servizi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnGetSTLCServiceStatus(object sender, STLCServiceStatusEventArgs e)
        {
            List<string> servicesName = new List<string>();
            try
            {
                bool _allServices = false;
                //Logger.Default.Log("Richiesto GetSTLCServiceStatus da parte di un Client di tipo '" + e.Request.clientType + "'",
                //    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                List<ServiceControl> servicesSelect;
                if (!String.IsNullOrEmpty(e.Request.group))
                {
                    servicesSelect = this.systemServices.ServicesSelect(e.Request.group);
//                    Logger.Default.Log("per gruppo " + e.Request.group, Logger.LogType.CONSOLE_LOC_FILE);
                }

                else if ((e.Request.services != null) && (e.Request.services.Count > 0))
                {
                    foreach (string serviceName in e.Request.services)
                    {
                        Logger.Default.Log("Richiesto GetSTLCServiceStatus da parte di un Client di tipo '" + e.Request.clientType + "'",
                            Logger.LogType.CONSOLE_LOC_FILE);
                        Logger.Default.Log("per servizio " + serviceName, Logger.LogType.CONSOLE_LOC_FILE);
                        servicesName.Add(serviceName);
                    }
                    servicesSelect = this.systemServices.ServicesSelect(servicesName);
                }
                else
                {
                    servicesSelect = this.systemServices.ServicesSelect();
                    _allServices = true;
                    //Logger.Default.Log("per tutti i servizi", Logger.LogType.CONSOLE_LOC_FILE);
                }
                var neededSupervisorList = SystemXmlFunction.Default.GetNeededSupervisorList(false);

                foreach (var service in servicesSelect)
                {
                    // non includere nella lista i supervisori non necessari
                    if (SupervisorUtility.IsSupervisorIdValid(service.SupervisorId) && (!_allServices && !neededSupervisorList.Contains(service.SupervisorId))) continue;

                    var ss = new STLCServiceStatus
                    {
                        Name = service.Name,
                        DisplayName = ServiceFunction.Default.GetServiceDisplayName(service.Name),
                        Status = ServiceFunction.Default.GetServiceStatus(service.Name, service.ProcessName),
                        StartMode = ServiceStartMode.Disabled,
                        
                    };
                    e.Result.data.servicesStatus.Add(ss);
                }
                e.Result.status.level = StatusLevel.SUCCESS;
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnGetSTLCServiceStatus': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Esegue comando su servizi
        /// </summary>
        /// <param name="command">codice comando</param>
        /// <param name="group">gruppo di servizi su cui eseguire comando</param>
        /// <param name="servicesName">lista nome servizi su cui eseguire comando</param>
        private void ExecuteCommandOnServices(STLCServiceCommand command, string group, List<string> servicesName)
        {
            try
            {
                switch (command)
                {
                    case STLCServiceCommand.RESTART:
                        SystemLed.Flash(SystemLed.Mode.SERVICES_RESTART);
                        if (!String.IsNullOrEmpty(group))
                        {
                            this.systemServices.Stop(group);
                            this.systemServices.Start(group);
                        }
                        else if (servicesName != null && servicesName.Count != 0)
                        {
                            this.systemServices.Stop(servicesName);
                            this.systemServices.Start(servicesName);
                        }
                        else
                        {
                            this.systemServices.Stop();
                            this.systemServices.Start(false);
                        }
                        break;

                    case STLCServiceCommand.START:
                        if (!String.IsNullOrEmpty(group))
                        {
                            this.systemServices.Start(group);
                        }
                        else if (servicesName != null && servicesName.Count != 0)
                        {
                            this.systemServices.Start(servicesName);
                        }
                        else
                        {
                            this.systemServices.Start(false);
                        }
                        break;

                    case STLCServiceCommand.STOP:
                        if (!String.IsNullOrEmpty(group))
                        {
                            this.systemServices.Stop(group);
                        }
                        else if (servicesName != null && servicesName.Count != 0)
                        {
                            this.systemServices.Stop(servicesName);
                        }
                        else
                        {
                            this.systemServices.Stop();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'ExecuteCommandOnServices': " + ex.GetType() + " - " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, Logger.E_STACK_TRACE);
            }
        }

        /// <summary>
        ///     Esegue comando di shutdown sistema.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnSTLCShutdown(object sender, STLCShutdownEventArgs e)
        {
            try
            {
                Logger.Default.Log(
                    "Richiesta STLCShutdown(" + e.Request.shutdownType + ") da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                if (e.Request.shutdownType == STLCShutdownType.POWER_OFF)
                {
                    SystemWDog.SystemReset(ResetSTLC.Mode.SHUTDOWN, 3000);
                }
                else
                {
                    SystemWDog.SystemReset(ResetSTLC.Mode.REBOOT, 3000);
                }
                e.Result.status.level = StatusLevel.SUCCESS;
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnSTLCShutdown': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Esegue comando di configurazione sistema
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnSTLCApplySystemConfig(object sender, STLCApplySystemConfigEventArgs e)
        {
            int retCode;
            int iResetCount;
            string errDesc;
            try
            {
                Logger.Default.Log("Richiesta ApplySystemConfig da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                retCode = this.RequestSystemConfig(out iResetCount, out errDesc);

                if (retCode == NO_ERROR)
                {
                    e.Result.status.level = StatusLevel.SUCCESS;
                    e.Result.status.resetRequired = iResetCount;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = retCode;
                    e.Result.status.errorDescription = "ERRORE in 'OnSTLCApplySystemConfig': " + errDesc;
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                }
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnSTLCApplySystemConfig': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Richiesta stato porte di rete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnGetSTLCNetwork(object sender, STLCNetworkEventArgs e)
        {
            try
            {
                //Logger.Default.Log("Richiesta STLCNetwork da parte di un Client di tipo '" + e.Request.clientType + "'",
                //    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                foreach (string ethernetInterfaceName in STLCNetwork.GetEthernetInterfacesNameEnabled())
                {
                    e.Result.data.Network.Add(new STLCNetwork(ethernetInterfaceName));
                }
                e.Result.status.level = StatusLevel.SUCCESS;
            }
            catch (Exception exc)
            {
                e.Result.data.Network.Clear();
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnGetSTLCNetwork': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Ricaricamento forzato della configurazione
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnReloadConfig(object sender, ReloadConfigEventArgs e)
        {
            try
            {
                Logger.Default.Log("Richiesta ReloadConfig da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                if (this.timerEngine != null)
                {
                    this.timerEngine.Enabled = false;
                    this.timerEngine.Stop();
                }

                if (STLCConfig.IsSTLCHost)
                {
                    SystemWDog.SetWatchDogTimer(false);
                }

                this.CloseService(false);

                int retCode = this.ManagerInitialize(true);

                if (retCode == NO_ERROR)
                {
                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                    e.Result.status.errorDescription = "ERRORE durante operazione 'OnReloadConfig'. Codice errore: " + retCode;
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        e.Result.status.errorCode);
                }

                this.STLCEnable();

                if (STLCConfig.IsSTLCHost)
                {
                    SystemWDog.SetWatchDogTimer(true);
                }
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnReloadConfig': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Ricaricamento del System.xml su database (non ferma i servizi, solo per casi particolari o manuale)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnReloadSystemXmlIntoDatabase(object sender, ReloadSystemXmlIntoDatabaseEventArgs e)
        {
            try
            {
                STLCMode _mode = e.Request.mode;

                Logger.Default.Log("Richiesta ReloadSystemXmlIntoDatabase da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                Logger.Default.Log(
                    "Richiesto caricamento dati periferiche da System.xml: i servizi di monitoraggio devono essere fermati manualmente prima dell'operazione.",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                Logger.Default.Log("Mode MAINTENANCE - Stop dei Servizi !", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, AppCfg.I_SYSTEM_XML_NO_ERROR);
                engineMode.SetSTLCMode(STLCMode.MAINTENANCE, -1);

                Thread.Sleep(2000);
                // Attiva un thread esterno per effettuare la configurazione e risponde subito positivamente al comando 
                // per evitare lo scadere del timeout in caso di configurazione con molti devices
                if (this.StartConfigThread(_mode, engineMode))
                {
                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_XML_CONFIG_VERSION_ERROR;
                    e.Result.status.errorDescription = "Impossibile creare il Thread di configurazione";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                }
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnReloadSystemXmlIntoDatabase': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, 
                                   Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Carica la lista di periferiche da System.xml e fornisce una lista dei supervisori di monitoraggio necessari
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnGetNeededSupervisors(object sender, GetNeededSupervisorsEventArgs e)
        {
            try
            {
                Logger.Default.Log("Richiesto caricamento dati periferiche da System.xml da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                //Logger.Default.Log(
                //    "Richiesto caricamento dati periferiche da System.xml",
                //    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                if (SystemXmlFunction.Default.ParseSystemXmlOnDatabase(false, false))
                {
                    if (SystemXmlFunction.Default.DevicesFromXmlConfig != null)
                    {
                        List<string> supervisorsData = new List<string>();

                        foreach (int supervisorId in SystemXmlFunction.Default.GetNeededSupervisorList((e.Request.clientType == RequestingClientType.SKC) ? false : true))
                        {
                            supervisorsData.Add(SupervisorUtility.DecodeSupervisorID(supervisorId));
                        }

                        e.Result.data.Supervisors = supervisorsData.AsReadOnly();

                        e.Result.data.OperationDescription =
                            String.Format("Caricate {0} periferiche da System.xml.\r\nLista dei Supervisori necessari al monitoraggio: {1}",
                                SystemXmlFunction.Default.DevicesFromXmlConfig.Count,
                                SupervisorUtility.GetNeededSupervisorListNames(SystemXmlFunction.Default.GetNeededSupervisorList(false)));
                    }
                    else
                    {
                        e.Result.data.OperationDescription = String.Format("Il file System.xml non contiene periferiche valide.");
                    }

                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.EMPTY;
                    e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                    e.Result.status.errorDescription = "Nessun servizio di supervisione richiesto durante operazione 'OnGetNeededSupervisors'.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                }
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnGetNeededSupervisors': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        /// Cancellazione e inserimento nel registro delle chiavi del file .config aventi attributo createOnRegistry="True"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnSetConfigOnRegistry(object sender, SetConfigOnRegistryEventArgs e)
        {
            try
            {
                Logger.Default.Log(String.Format("Richiesta SetConfigOnRegistry [{0}] da parte di un Client di tipo '{1}'", e.Request.cfgOnRegistryCommand, e.Request.clientType), Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level               = StatusLevel.FAIL;
                    e.Result.status.errorCode           = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription    = "Richiesta rifiutata. Sistema in fase di reboot.";

                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                switch(e.Request.cfgOnRegistryCommand)
                {
                    case CfgOnRegistryCommand.CREATE_ALL:

                        List<string> listCreatedKeys    = AppCfg.Default.CreateRegitrySettings      ();

                        if(listCreatedKeys.Count    > 0)
                        {
                            e.Result.status.listKeys    = listCreatedKeys;
                            e.Result.status.cmdExecuted = CfgOnRegistryCommand.CREATE_ALL;
                        }
                        else
                            e.Result.status.cmdExecuted = CfgOnRegistryCommand.NOT_SPECIFIED;
                        
                        break;
                    
                    case CfgOnRegistryCommand.DELETE_ALL:

                        List<string> listDeletedAllKeys = AppCfg.Default.DeleteRegistrySettings     (false);

                        if (listDeletedAllKeys.Count > 0)
                        {
                            e.Result.status.listKeys    = listDeletedAllKeys;
                            e.Result.status.cmdExecuted = CfgOnRegistryCommand.DELETE_ALL;
                        }
                        else
                            e.Result.status.cmdExecuted = CfgOnRegistryCommand.NOT_SPECIFIED;

                        break;

                    case CfgOnRegistryCommand.DELETE_ONLY_CONFIG:

                        List<string> listDeletedCfgKeys = AppCfg.Default.DeleteRegistrySettings     (true);

                        if (listDeletedCfgKeys.Count > 0)
                        {
                            e.Result.status.listKeys    = listDeletedCfgKeys;
                            e.Result.status.cmdExecuted = CfgOnRegistryCommand.DELETE_ONLY_CONFIG;
                        }
                        else
                            e.Result.status.cmdExecuted = CfgOnRegistryCommand.NOT_SPECIFIED;

                        break;

                    default:
                        e.Result.status.cmdExecuted = CfgOnRegistryCommand.NOT_SPECIFIED;
                        break;
                }

                e.Result.status.level               = StatusLevel.SUCCESS;
            }
            catch (Exception exc)
            {
                e.Result.status.level               = StatusLevel.FAIL;
                e.Result.status.errorCode           = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription    = "ECCEZIONE in 'OnSetConfigOnRegistry': " + exc.Message + " - " + exc.StackTrace;
         
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Scaricamento e aggiornamento dati region list da web service in centrale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnUpdateRegionListFromWS(object sender, UpdateRegionListFromWSEventArgs e)
        {
            try
            {
                Logger.Default.Log("Richiesta OnUpdateRegionListFromWS da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (STLCConfig.IsSTLCHost)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_NOTSUPPORTED;
                    e.Result.status.errorDescription = "Funzione non supportata da questo profilo hardware";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                if (CentralWSFunction.Default.DownloadRegionListDataAndUpdateDatabase())
                {
                    e.Result.data.OperationDescription = String.Format("Scaricati e aggiornati: {0} compartimenti, {1} linee, {2} stazioni.",
                        CentralWSFunction.Default.Regions.Length, CentralWSFunction.Default.Zones.Length,
                        CentralWSFunction.Default.Nodes.Length);

                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                    e.Result.status.errorDescription = "ERRORE durante operazione 'OnUpdateRegionListFromWS'.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        e.Result.status.errorCode);
                }
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnUpdateRegionListFromWS': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        /// <summary>
        ///     Scaricamento e aggiornamento dati device type list da web service in centrale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnUpdateDeviceTypeListFromWS(object sender, UpdateDeviceTypeListFromWSEventArgs e)
        {
            try
            {
                Logger.Default.Log("Richiesta OnUpdateDeviceTypeListFromWS da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (STLCConfig.IsSTLCHost)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_NOTSUPPORTED;
                    e.Result.status.errorDescription = "Funzione non supportata da questo profilo hardware";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }
                
                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }
                
                if (CentralWSFunction.Default.DownloadDeviceTypeListAndUpdateDatabase())
                {
                    e.Result.data.OperationDescription = String.Format("Scaricati e aggiornati: {0} sistemi, {1} produttori, {2} tipi periferica.",
                        CentralWSFunction.Default.Systems.Length, CentralWSFunction.Default.Vendors.Length,
                        CentralWSFunction.Default.DeviceTypes.Length);

                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                    e.Result.status.errorDescription = "ERRORE durante operazione 'OnUpdateDeviceTypeListFromWS'.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        e.Result.status.errorCode);
                }
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'OnUpdateDeviceTypeListFromWS': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    e.Result.status.errorCode);
            }
        }

        #endregion

        #region Gestione Configurazione di sistema

        /// <summary>
        ///     Gestione richiesta di configurazione sistema come da file XML.
        /// </summary>
        /// <param name="iReset">numero di reset necessari per lìapplicazione della configurazione</param>
        /// <param name="errDesc">descrizione eventuale errore</param>
        /// <returns>In caso di errore restituisce un valore non nullo</returns>
        private int RequestSystemConfig(out int iReset, out string errDesc)
        {
            int retCode;

            iReset = 0;
            STLCSystemReference SystemReference = new STLCSystemReference();
            STLCNetworkList SystemNetworks = new STLCNetworkList();

            if ((retCode = this.GetSystemConfig(ref SystemReference, ref SystemNetworks)) != NO_ERROR)
            {
                errDesc = "Dati file XML non validi";
                return retCode;
            }

#if (DEBUG)
            Logger.Default.Log("Dati caricati da file xml", Logger.LogType.CONSOLE);
            Logger.Default.Log(" hostname=" + SystemReference.HostName, Logger.LogType.CONSOLE);

            foreach (STLCNetworkReference network in SystemNetworks)
            {
                network.Debug(null);
            }
#endif

            if ((retCode = this.SetSystemConfig(ref SystemReference, ref SystemNetworks)) != NO_ERROR)
            {
                errDesc = "Configurazione sistema non riuscita";
                return retCode;
            }

            errDesc = "";

            // Calcola numero di reset necessari per applicare la nuova configurazione
            if (SystemReference.ToSet)
            {
                iReset = this.fbwfCurrentSession.IsFilterEnabled() ? 3 : 2;
            }
            else
            {
                foreach (STLCNetworkReference network in SystemNetworks)
                {
                    if (network.ToSet)
                    {
                        iReset = this.fbwfCurrentSession.IsFilterEnabled() ? 2 : 1;
                        break;
                    }
                }
            }

            // Ritarda applicazione configurazione (comporta reboot) per consentire l'invio della risposta al comando ricevuto
            if (iReset != 0)
            {
                Logger.Default.Log("Inizio 'applica configurazione'.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION,
                    AppCfg.I_COD_BEGIN_SYSTEM_CONFIGURATION);
            }
            Thread StartApplySystemConfig = new Thread(() =>
            {
                Thread.Sleep(DELAY_TO_START_SYSTEM_CONFIG);
                this.ApplySystemConfig();
            });
            StartApplySystemConfig.Start();

            return retCode;
        }

        /// <summary>
        ///     Legge parametri di configurazione sistema da file XML
        /// </summary>
        /// <param name="SysRef">oggetto di definizione dati generali sistema (hostname)</param>
        /// <param name="SysNetList">oggetto di definizione dati porte ethernet</param>
        /// <returns>In caso di errore restituisce un valore non nullo</returns>
        private int GetSystemConfig(ref STLCSystemReference SysRef, ref STLCNetworkList SysNetList)
        {
            int retCode = NO_ERROR;

            try
            {
                string XmlConfigPath = AppCfg.Default.sXMLSystem; // .FP_CFG_XML_SYSTEM_VALUE_PATH;

                XmlDocument ConfigXmlDoc = new XmlDocument();
                ConfigXmlDoc.Load(XmlConfigPath);

                // Caricamento dell'elemento di definizione nome sistema
                XmlNode serverElement = ConfigXmlDoc.SelectSingleNode(SysRef.HostXMLNode);

                if (serverElement != null)
                {
                    Logger.Default.Log("Caricato nodo Server", Logger.LogType.CONSOLE_LOC_FILE);

                    SysRef.HostName = ((XmlElement) serverElement).GetAttribute(SysRef.HostXMLAttribute);
                    SysRef.HostName = SysRef.HostName.ToUpper();
                }
                else
                {
                    Logger.Default.Log("Nodo Server non trovato", Logger.LogType.CONSOLE_LOC_FILE);
                }

                // Caricamento dell'elemento relativo alla gestione delle porte ethernet
                XmlNode networkElement;

                networkElement = ConfigXmlDoc.SelectSingleNode(SysNetList.EthernetXMLNode);

                if (networkElement != null)
                {
                    Logger.Default.Log("Caricato nodo network", Logger.LogType.CONSOLE_LOC_FILE);

                    // Caricamento parametri configurazione porta ethernet -> Cerco gli elementi XML figli
                    foreach (XmlNode item in networkElement.ChildNodes)
                    {
                        if (item.GetType() != typeof (XmlElement))
                        {
                            continue;
                        }

                        STLCNetworkReference network = new STLCNetworkReference();

                        foreach (xmlNetworkAttributes attribute in Enum.GetValues(typeof (xmlNetworkAttributes)))
                        {
                            string strItem = ((XmlElement) item).GetAttribute(Enum.GetName(typeof (xmlNetworkAttributes), attribute));

                            if (!String.IsNullOrEmpty(strItem))
                            {
                                network.AttributeValue(attribute, strItem);
                            }
                        }

                        try
                        {
                            if (network.LoadConfigFromStrings())
                            {
                                UInt32 devid;

                                if (WMIFunction.Default.GetNetworkID(network.Name, out devid))
                                {
                                    network.DevId = devid;
                                    SysNetList.Add(network);
                                }
                                else
                                {
                                    retCode = AppCfg.E_XML_CONFIG_NETWORK_NODE_ERROR;
                                    Logger.Default.Log("Errore: Network name (" + network.Name + ") non valido.", Logger.LogType.CONSOLE_LOC_FILE,
                                        Logger.EventType.ERROR, retCode);
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            retCode = AppCfg.E_XML_CONFIG_NETWORK_NODE_ERROR;
                            Logger.Default.Log("ECCEZIONE: Nodo network non valido. " + ex.StackTrace, Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.ERROR, retCode);
                            break;
                        }
                    }
                }
                else
                {
                    Logger.Default.Log("Nodo network non trovato", Logger.LogType.CONSOLE_LOC_FILE);
                }
            }
            catch (Exception ex)
            {
                retCode = AppCfg.E_XML_CONFIG_OPEN_SOURCE_CRITICAL_ERROR;
                Logger.Default.ManageException(ex);
            }

            return retCode;
        }

        /// <summary>
        ///     Setta parametri di configurazione sistema come da definizione oggetti.
        /// </summary>
        /// <param name="SysRef">definizione dati generali sistema (hostname)</param>
        /// <param name="SysNetList">definizione dati porte ethernet</param>
        /// <returns>In caso di errore restituisce un valore non nullo</returns>
        private int SetSystemConfig(ref STLCSystemReference SysRef, ref STLCNetworkList SysNetList)
        {
            bool configToSet = false;
            string pcName;

            try
            {
                if (SysInfoFunction.Default.GetComputerName(out pcName) != NO_ERROR)
                {
                    Logger.Default.Log("SetSystemConfig: errore lettura ComputerName", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR);
                    return AppCfg.E_COD_READ_SYSTEM_INFO_FAILURE;
                }

                // confronta computer name con host name richiesto
                if (SysRef.HostName != null && SysRef.HostName != pcName)
                {
                    Logger.Default.Log("SetSystemConfig: configura ComputerName", Logger.LogType.CONSOLE_LOC_FILE);
                    configToSet = SysRef.ToSet = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
                return AppCfg.E_COD_READ_SYSTEM_INFO_FAILURE;
            }

            try
            {
                foreach (STLCNetworkReference xmlNetwork in SysNetList)
                {
                    STLCNetwork stlcNetwork = new STLCNetwork(xmlNetwork.Name);

                    if (!stlcNetwork.IPEnabled)
                    {
                        Logger.Default.Log("ERRORE in 'SetSystemConfig(" + xmlNetwork.Name + ")': network disabilitata",
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR);
                        return AppCfg.E_COD_READ_NETWORK_INFO_FAILURE;
                    }

                    // confronta configurazione sistema con configurazione da file xml
                    if (!stlcNetwork.Compare(xmlNetwork))
                    {
                        if (xmlNetwork.DHCPEnabled)
                        {
                            // ip automatico (subnet non significativo, dns auto)
                            Logger.Default.Log("SetSystemConfig(" + xmlNetwork.Name + "): configura IP dinamico", Logger.LogType.CONSOLE_LOC_FILE);
                            xmlNetwork.SubnetMasks.Clear();
                            xmlNetwork.DNSServers.Clear();
                            configToSet = xmlNetwork.ToSet = true;
                        }
                        else
                        {
                            // ip statico (subnet obbligatorio, gateway facoltativo, dns statico)
                            Logger.Default.Log("SetSystemConfig(" + xmlNetwork.Name + "): configura IP statico", Logger.LogType.CONSOLE_LOC_FILE);
                            configToSet = xmlNetwork.ToSet = true;
                        }

                        if (xmlNetwork.IPGateways.Count != 0)
                        {
                            xmlNetwork.Metrics.Clear();
                            xmlNetwork.Metrics.Add(stlcNetwork.Metrics.Count != 0 ? stlcNetwork.Metrics[0] : (UInt16) 20);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
                return AppCfg.E_COD_READ_NETWORK_INFO_FAILURE;
            }

            if (configToSet && !XMLFunction.Default.CreateActionFile(SysRef, SysNetList))
            {
                Logger.Default.Log("ERRORE in 'SetSystemConfig': attivazione procedura di configurazione fallita.", Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR);
                return AppCfg.E_COD_START_CONFIG_PRC_FAILURE;
            }

            return NO_ERROR;
        }

        /// <summary>
        ///     Gestisce le varie fasi di configurazione sistema.
        /// </summary>
        private void ApplySystemConfig()
        {
            try
            {
                if (!XMLFunction.Default.LoadActionToDo())
                {
                    return;
                }

                this.configInProgress = true;
                SystemLed.Set(SystemLed.Mode.SYSTEM_CONFIG);
                this.systemServices.Stop("core");

                if (this.fbwfCurrentSession.IsFilterEnabled())
                {
                    Logger.Default.Log("Disabilita filtro FBWF.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION,
                        AppCfg.I_COD_DISABLE_FBWF);
                    this.fbwfCurrentSession.DisableFilter();
                    SystemWDog.SystemReset(ResetSTLC.Mode.REBOOT, 0);
                    return;
                }

                switch (XMLFunction.Default.ActionToDo)
                {
                    case xmlActionToDo.SET_CONFIG:
                        Logger.Default.Log("STLCEngine.ApplySystemConfig: Setta configurazione.", Logger.LogType.CONSOLE_LOC_FILE);
                        xmlActionToDo nextAction = xmlActionToDo.ENABLE_FILTER;

                        #region Cambiamento nome computer via WMI, prossimo step RUN_SCRIPT, se esecuzione corretta

                        if (XMLFunction.Default.ComputerName != null)
                        {
                            Logger.Default.Log("Configura nome computer (" + XMLFunction.Default.ComputerName + ").", Logger.LogType.CONSOLE_LOC_FILE,
                                Logger.EventType.INFORMATION, AppCfg.I_COD_SET_COMPUTER_NAME);
                            if (SysInfoFunction.Default.SetComputerName(XMLFunction.Default.ComputerName))
                            {
                                nextAction = xmlActionToDo.RUN_SCRIPT;
                            }
                        }

                        #endregion

                        #region Configurazione porte di rete

                        foreach (STLCNetwork network in XMLFunction.Default.NetworkPorts)
                        {
                            char[] charsToTrim = {','};
                            string sIP = "";
                            string sSubnet = "";
                            string sGateway = "";
                            string sMetric = "";
                            string sDNS = "";

                            if (network.DHCPEnabled)
                            {
                                foreach (IPAddress ip in network.IPGateways)
                                {
                                    sGateway += ip + ",";
                                }
                                sGateway = sGateway.TrimEnd(charsToTrim);

                                foreach (UInt16 metric in network.Metrics)
                                {
                                    sMetric += metric + ",";
                                }
                                sMetric = sMetric.TrimEnd(charsToTrim);

                                Logger.Default.Log(
                                    String.Format("Configura porta {0}: IP dinamico; Gateway={1}; Metric={2}.", network.Name, sGateway, sMetric),
                                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_COD_SET_NETWORK_PORT);

                                WMIFunction.Default.EnableDHCP(network.DevId);
                                WMIFunction.Default.SetGateway(network.DevId, network.IPGateways, network.Metrics);
                                WMIFunction.Default.SetDNS(network.DevId, new List<IPAddress>());
                            }
                            else
                            {
                                foreach (IPAddress ip in network.IPAddresses)
                                {
                                    sIP += ip + ",";
                                }
                                sIP = sIP.TrimEnd(charsToTrim);

                                foreach (IPAddress ip in network.SubnetMasks)
                                {
                                    sSubnet += ip + ",";
                                }
                                sSubnet = sSubnet.TrimEnd(charsToTrim);

                                foreach (IPAddress ip in network.IPGateways)
                                {
                                    sGateway += ip + ",";
                                }
                                sGateway = sGateway.TrimEnd(charsToTrim);

                                foreach (UInt16 metric in network.Metrics)
                                {
                                    sMetric += metric + ",";
                                }
                                sMetric = sMetric.TrimEnd(charsToTrim);

                                foreach (IPAddress ip in network.DNSServers)
                                {
                                    sDNS += ip + ",";
                                }
                                sDNS = sDNS.TrimEnd(charsToTrim);

                                Logger.Default.Log(
                                    String.Format("Configura porta {0}: IP statico; IP={1}; Subnet={2}; Gateway={3}; Metric={4}; DNS={5}.",
                                        network.Name, sIP, sSubnet, sGateway, sMetric, sDNS), Logger.LogType.CONSOLE_LOC_FILE,
                                    Logger.EventType.INFORMATION, AppCfg.I_COD_SET_NETWORK_PORT);

                                WMIFunction.Default.SetStaticIP(network.DevId, network.IPAddresses, network.SubnetMasks);
                                WMIFunction.Default.SetGateway(network.DevId, network.IPGateways, network.Metrics);
                                WMIFunction.Default.SetDNS(network.DevId, network.DNSServers);
                            }
                        }

                        #endregion

                        XMLFunction.Default.SetActionToDo(nextAction);

                        if (nextAction == xmlActionToDo.RUN_SCRIPT)
                        {
                            // E' cambiato il nome del computer, se siamo qui, quindi occorre eseguire lo script SQL per rinominare
                            // il database SQL Server, nel prossimo step. Perché il tutto funzioni, occorre un reset, visto che lo script SQL
                            // assegna i permessi con il nome macchina.
                            Logger.Default.Log("STLCEngine.ApplySystemConfig: Configurazione settata. Reboot.", Logger.LogType.CONSOLE_LOC_FILE);
                            SystemWDog.SystemReset(ResetSTLC.Mode.REBOOT, 0);
                        }
                        else
                        {
                            // Reboot non necessario. Abilita filtro. (Prossimo step: ENABLE_FILTER)
                            this.ApplySystemConfig();
                        }
                        break;

                    case xmlActionToDo.RUN_SCRIPT:
                        Logger.Default.Log("STLCEngine.ApplySystemConfig: Esegue SQL script.", Logger.LogType.CONSOLE_LOC_FILE);

                        for (int count = 0; count < 3; count++)
                        {
                            if (this.systemServices.Start("system"))
                            {
                                string sqlScript = "sqlcmd -S .\\SQLEXPRESS -E -i \"" + AppCfg.Default.sPathSQLScript + // .FP_SQL_SCRIPT_VALUE_PATH +
                                                   "\" -v NEWSERVERNAME=" + XMLFunction.Default.ComputerName;
                                Logger.Default.Log("RUN_SCRIPT: " + sqlScript, Logger.LogType.CONSOLE_LOC_FILE);
                                SysInfoFunction.Default.ExecuteCommandSync(sqlScript);
                                Logger.Default.Log("STLCEngine.ApplySystemConfig: SQL script eseguito.", Logger.LogType.CONSOLE_LOC_FILE);

                                // Abbiamo appena rinominato il Database SQL Server. Possiamo attivare direttamente il FBWF e riavviare.
                                // Per farlo, usiamo il codice relativo, con log e nessuna duplicazione di codice.
                                goto case xmlActionToDo.ENABLE_FILTER;
                            }
                            Logger.Default.Log("ERRORE in 'STLCEngine.ApplySystemConfig': servizio SQL non partito, impossibile eseguire script.",
                                Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_GENERIC);
                            XMLFunction.Default.DeleteActionFile();
                            this.fbwfCurrentSession.EnableFilter();
                            SystemWDog.SystemReset(ResetSTLC.Mode.REBOOT, 0);
                        }
                        break;

                    case xmlActionToDo.ENABLE_FILTER:
                        Logger.Default.Log("Abilita filtro FBWF.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION,
                            AppCfg.I_COD_ENABLE_FBWF);
                        this.fbwfCurrentSession.EnableFilter();
                        XMLFunction.Default.DeleteActionFile();
                        Logger.Default.Log("Fine 'applica configurazione'.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION,
                            AppCfg.I_COD_FINISH_SYSTEM_CONFIGURATION);
                        SystemWDog.SystemReset(ResetSTLC.Mode.REBOOT, 0);
                        break;

                    default:
                        Logger.Default.Log("ERRORE in 'STLCEngine.ApplySystemConfig': step non valido (" + XMLFunction.Default.ActionToDo + ").",
                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_GENERIC);
                        this.fbwfCurrentSession.EnableFilter();
                        XMLFunction.Default.DeleteActionFile();
                        SystemWDog.SystemReset(ResetSTLC.Mode.REBOOT, 0);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'STLCEngine.ApplySystemConfig': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                XMLFunction.Default.DeleteActionFile();
                this.fbwfCurrentSession.EnableFilter();
                SystemWDog.SystemReset(ResetSTLC.Mode.REBOOT, 0);
            }
        }

        //------------------
        
        /// <summary>
        ///     Attivazione thread per la gestione della configurazione da file XML
        /// </summary>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: thread attivato</item>
        ///         <item>false: errore durante l'attivazione del thread</item>
        ///     </list>
        /// </returns>
        public bool StartConfigThread(STLCMode _mode, EngineMode _engineMode )
        {
            try
            {
                Logger.Default.Log("Starting Thread ConfigThread - mode = " + _mode, Logger.LogType.CONSOLE_LOC_FILE);

                StartConfig startConfig = new StartConfig();
                startConfig.mode = _mode;
                startConfig.engineMode = _engineMode;

                this.configThread = new Thread(startConfig.ConfigThread) ;
                this.configThread.Start();

                //attende che il thread sia attivo
                while (!this.configThread.IsAlive)
                {
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Log("ECCEZIONE in 'ServiceControl.ConfigThread': " + ex.GetType() + " - " + ex.StackTrace,
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, Logger.E_STACK_TRACE);
                return false;
            }
            return true;
        }

        public class StartConfig
        {
            public STLCMode mode;
            public EngineMode engineMode;

            public void ConfigThread()
            {
                if (SystemXmlFunction.Default.ParseSystemXmlOnDatabase(true, true)) //esecuzione scrittura in DB, stampa log
                {
                    if (SystemXmlFunction.Default.DevicesFromXmlConfig != null)
                    {
                        Thread.Sleep(1000);

                        List<string> devicesData = new List<string>();

                        foreach (DeviceObject device in SystemXmlFunction.Default.DevicesFromXmlConfig)
                        {
                            devicesData.Add(device.ToString());
                        }
                        Logger.Default.Log("Lista dei device aggiornata. Numero di device= " + devicesData.Count.ToString(),
                                            Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                        if (mode == STLCMode.NORMAL)
                        {
                            Logger.Default.Log("Mode NORMAL - Start dei Servizi !",
                                                Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, AppCfg.I_SYSTEM_XML_NO_ERROR);
                            this.engineMode.SetSTLCMode(STLCMode.NORMAL, 0);
                        }
                        else
                            Logger.Default.Log("Mode MAINTENANCE ", Logger.LogType.CONSOLE_LOC_FILE);
                    }
                }
                else
                {
                    // Se la lista dei devices è vuota attivo il servizio 'STLC Collector Agent'
                    List<string> servicesName = new List<string>();
                    servicesName.Add("StlcSCAgentService");
                    this.engineMode.systemServices.Start(servicesName);
                }
                return;
            }
        }

//-----------------
        /// <summary>
        ///     Esecuzione comando di clear del database (ferma i servizi, prima della cancellazione)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">parametri comando</param>
        private void clientCommLayer_OnEmptyDatabase(object sender, EmptyDatabaseEventArgs e)
        {
            try
            {
                Logger.Default.Log("Richiesta EmptyDatabase da parte di un Client di tipo '" + e.Request.clientType + "'",
                    Logger.LogType.CONSOLE_LOC_FILE);

                if (SystemWDog.IsRebooting)
                {
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorCode = AppCfg.W_COD_SYSTEM_REBOOTING;
                    e.Result.status.errorDescription = "Richiesta rifiutata. Sistema in fase di reboot.";
                    Logger.Default.Log(e.Result.status.errorDescription, Logger.LogType.CONSOLE_LOC_FILE);
                    return;
                }

                Logger.Default.Log(
                    "Richiesto clear del DB: i servizi di monitoraggio devono essere fermati prima dell'operazione.",
                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION, AppCfg.I_SYSTEM_XML_NO_ERROR);

                Logger.Default.Log("Mode MAINTENANCE - Stop dei Servizi !", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, AppCfg.I_SYSTEM_XML_NO_ERROR);
                engineMode.SetSTLCMode(STLCMode.MAINTENANCE, -1);

                //Attesa stop dei servizi
                Thread.Sleep(2000);

                // Cancellazione DB
                if (Execute_EmptyDatabase())
                {
                    Logger.Default.Log("Cancellazione dei dati in DB eseguita.", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, AppCfg.I_SYSTEM_XML_NO_ERROR);
                    e.Result.status.level = StatusLevel.SUCCESS;
                }
                else
                {
                    Logger.Default.Log("Errore: Cancellazione dei dati in DB NON eseguita !", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_SYSTEM_XML_FUNCTION_DATABASE_UNEXPECTED_DATA_ERROR);
                    e.Result.status.level = StatusLevel.FAIL;
                    e.Result.status.errorDescription = String.Format("Store procedure error: ({0}).", AppCfg.Default.sEmptyDbSPName);
                }
                return;
            }
            catch (Exception exc)
            {
                e.Result.status.level = StatusLevel.FAIL;
                e.Result.status.errorCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                e.Result.status.errorDescription = "ECCEZIONE in 'clientCommLayer_OnEmptyDatabase': " + exc.Message + " - " + exc.StackTrace;
                Logger.Default.Log(e.Result.status.errorDescription,
                                   Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, e.Result.status.errorCode);
            }
        }

        #endregion

        #region Methods (set mode, config, close)

        public class EngineMode
        {
            public SystemServices systemServices;
            STLCConfig Config;

            public EngineMode()
            {
                this.systemServices = null;
                this.Config = null;
            }

            public EngineMode(SystemServices _systemServices, STLCConfig _config)
            {
                systemServices = _systemServices;
                Config = _config;
            }

            /// <summary>
            ///     Imposta la modalità di funzionamento del STLC
            /// </summary>
            /// <param name="mode">modo di funzionamento</param>
            /// <param name="timeout">timeout di ritorno automatico in modalità NORMAL</param>
            public void SetSTLCMode(STLCMode mode, int timeout)
            {
                switch (mode)
                {
                    case STLCMode.MAINTENANCE:
                        if (SystemWDog.IsMaintenanceEnabled)
                        {
                            Logger.Default.Log("Richiesta ignorata perchè la modalità MANUTENZIONE è già attiva.", Logger.LogType.CONSOLE_LOC_FILE);
                            break;
                        }
                        else
                        {
                            SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemSERVICES);
                            this.systemServices.Disable();
                            Thread.Sleep(1000);
                            this.systemServices.Stop("core");
                        }

                        if (timeout < 0)
                        {
                            SystemWDog.MaintenanceMode(true, 0);

                            Logger.Default.Log("Funzionamento: MANUTENZIONE permanente", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, //notice
                                AppCfg.I_COD_MAINTENANCE_MODE_ACTIVE);
                        }
                        else if (timeout == 0)
                        {
                            SystemWDog.MaintenanceMode(false, this.Config.MaintenanceModeTimeout);

                            Logger.Default.Log("Funzionamento: MANUTENZIONE con timeout di default di " + this.Config.MaintenanceModeTimeout + " ore", //notice
                                Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, AppCfg.I_COD_MAINTENANCE_MODE_ACTIVE);
                        }
                        else
                        {
                            SystemWDog.MaintenanceMode(false, (uint) timeout);

                            Logger.Default.Log("Funzionamento: MANUTENZIONE con timeout di " + timeout + " ore", Logger.LogType.CONSOLE_LOC_FILE, //notice
                                Logger.EventType.NOTICE, AppCfg.I_COD_MAINTENANCE_MODE_ACTIVE);
                        }
                        break;

                    case STLCMode.NORMAL:
                        if (!SystemWDog.IsMaintenanceEnabled)
                        {
                            Logger.Default.Log("Richiesta ignorata perchè la modalità NORMALE è già attiva.", Logger.LogType.CONSOLE_LOC_FILE);
                            break;
                        }
                        this.SetNormalMode();
                        
                        Logger.Default.Log("Funzionamento: NORMALE", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.NOTICE, //notice
                            AppCfg.I_COD_NORMAL_MODE_ACTIVE);
                        break;
                }
            }

            public void SetNormalMode()
            {
                this.systemServices.Start(false);

                if (this.Config.IsServiceActive)
                {
                    this.systemServices.Enable();

                    if (this.Config.IsServiceRunning)
                    {
                        SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemSERVICES);
                        SystemWDog.ThreadUnderControl(AppCfg.ThreadSystemSERVICES, this.systemServices.ThreadPeriod, this.systemServices.IsRunning);
                    }
                }
                SystemWDog.NormalMode();
            }
        }

    /// <summary>
        ///     Carica la configurazione del servizio dal file XML
        /// </summary>
        /// <returns>ritorna l'eventuale codice di errore, NO_ERROR se configurazione corretta.</returns>
        private int ConfigInit()
        {
            int ret_code = NO_ERROR;

            this.Config = new STLCConfig();

            try
            {

                string XmlConfigPath = AppCfg.Default.sPathXmlConfig; // .FP_CFG_XML_SERVICE_VALUE_PATH;

                XmlDocument ConfigXmlDoc = new XmlDocument();
                ConfigXmlDoc.Load(XmlConfigPath);

                // Verifica se file di configurazione allineato alla versione software
                XmlNode telefinElement = ConfigXmlDoc.SelectSingleNode("//telefin");

                if (telefinElement != null)
                {
                    Logger.Default.Log("Caricato nodo Telefin", Logger.LogType.CONSOLE_LOC_FILE);

                    string strVersion = ((XmlElement) telefinElement).GetAttribute("version");

                    if (String.IsNullOrEmpty(strVersion) || strVersion != this.SoftwareVersion)
                    {
                        ret_code = AppCfg.W_XML_CONFIG_VERSION_ERROR;
                        Logger.Default.Log("Versione file XML di configurazione non valida", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.WARNING,
                            AppCfg.W_XML_CONFIG_VERSION_ERROR);
                        Logger.Default.Log(String.Format("XML Version= {0}, Software Version= {1}", strVersion, this.SoftwareVersion), Logger.LogType.CONSOLE_LOC_FILE);
                        this.xmlFileVersionNotValid = true;
                    }
                }
                else
                {
                    ret_code = AppCfg.E_XML_CONFIG_TELEFIN_NODE_ERROR;
                    Logger.Default.Log("Errore: Nodo Telefin non trovato", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        AppCfg.E_XML_CONFIG_TELEFIN_NODE_ERROR);
                    Logger.Default.Log("STLCManager service STOPPED", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        AppCfg.W_COD_SERVICE_MANAGER_STOPPED);
                    Environment.Exit(1);
                }

                // Caricamento dell'elemento relativo alla gestione del Service Manager
                XmlNode managerElement = ConfigXmlDoc.SelectSingleNode("//telefin/manager");

                if (managerElement != null)
                {
                    Logger.Default.Log("Caricato nodo Manager", Logger.LogType.CONSOLE_LOC_FILE);

                    bool bReboot;
                    string strReboot = ((XmlElement) managerElement).GetAttribute("reboot_on_failure");
                    if (Boolean.TryParse(strReboot, out bReboot))
                    {
                        this.Config.RebootOnFailure = bReboot;
                    }

                    uint iTimeout;
                    string strTimeout = ((XmlElement) managerElement).GetAttribute("maintenance_mode_timeout");
                    if (UInt32.TryParse(strTimeout, out iTimeout))
                    {
                        this.Config.MaintenanceModeTimeout = iTimeout;
                    }
                }
                else
                {
                    ret_code = AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR;
                    Logger.Default.Log("Errore: Nodo Manager non trovato", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                    Logger.Default.Log("STLCManager service STOPPED", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        AppCfg.W_COD_SERVICE_MANAGER_STOPPED);
                    Environment.Exit(2);
                }

                // Caricamento dell'elemento relativo alla gestione del watchdog
                XmlNode watchDogElement = ConfigXmlDoc.SelectSingleNode("//telefin/manager/watchdog");

                if (watchDogElement != null)
                {
                    Logger.Default.Log("Caricato nodo watchdog", Logger.LogType.CONSOLE_LOC_FILE);

                    bool bEnabled;
                    string strEnabled = ((XmlElement) watchDogElement).GetAttribute("enabled");
                    if (Boolean.TryParse(strEnabled, out bEnabled))
                    {
                        this.Config.IsWatchDogActive = bEnabled;
                    }

                    if (this.Config.IsWatchDogActive)
                    {
                        string strOnRunningTimeout = ((XmlElement) watchDogElement).GetAttribute("timeout");
                        string strOnStopTimeout = ((XmlElement) watchDogElement).GetAttribute("on_service_stop_timeout");

                        if (!SystemWDog.SetTimeout(strOnRunningTimeout, strOnStopTimeout))
                        {
                            Logger.Default.Log(
                                String.Format("Errore: Nodo watchdog, timeouts non validi (timeout={0} on_service_stop_timeout={1}).",
                                    strOnRunningTimeout, strOnStopTimeout), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                        }
                    }
                }
                else
                {
                    ret_code = AppCfg.E_XML_CONFIG_WATCHDOG_NODE_ERROR;
                    Logger.Default.Log("Errore: Nodo WatchDog non trovato", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
                }

                // Caricamento dell'elemento relativo alla gestione led di sistema
                XmlNode systemLedElement = ConfigXmlDoc.SelectSingleNode("//telefin/manager/systemled");

                if (systemLedElement != null)
                {
                    Logger.Default.Log("Caricato nodo systemled", Logger.LogType.CONSOLE_LOC_FILE);

                    bool bEnabled;
                    string strEnabled = ((XmlElement) systemLedElement).GetAttribute("enabled");
                    if (Boolean.TryParse(strEnabled, out bEnabled))
                    {
                        this.Config.IsSystemLedActive = bEnabled;
                    }

                    if (this.Config.IsSystemLedActive)
                    {
                        // flag di monitoraggio funzionamento thread
                        bool bTrigger;
                        string strTrigger = ((XmlElement) systemLedElement).GetAttribute("system_failure_trigger");
                        if (Boolean.TryParse(strTrigger, out bTrigger))
                        {
                            this.Config.IsSystemLedRunning = bTrigger;
                        }

                        // modalità di lampeggio
                        foreach (XmlNode item in systemLedElement.ChildNodes)
                        {
                            if (item.GetType() != typeof (XmlElement))
                            {
                                continue;
                            }

                            string strMode = ((XmlElement) item).GetAttribute("mode");
                            string strGreen = ((XmlElement) item).GetAttribute("green_pattern");
                            string strRed = ((XmlElement) item).GetAttribute("red_pattern");

                            if (!SystemLed.SetPattern(strMode, strGreen, strRed))
                            {
                                Logger.Default.Log(
                                    String.Format("Errore: Nodo systemLed, pattern non valido (mode={0} green_pattern={1} red_pattern={2})", strMode,
                                        strGreen, strRed), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                    AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                            }
                        }
                    }
                }
                else
                {
                    this.Config.IsSystemLedActive = true;
                    ret_code = AppCfg.E_XML_CONFIG_SYSTEM_LED_NODE_ERROR;
                    Logger.Default.Log("Errore: Nodo systemled non trovato", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
                }

                // Caricamento dell'elemento relativo alla gestione delle ventole
                XmlNode fanElement = ConfigXmlDoc.SelectSingleNode("//telefin/manager/fan");

                if (fanElement != null)
                {
                    Logger.Default.Log("Caricato nodo fan", Logger.LogType.CONSOLE_LOC_FILE);

                    // sensore di rilevamento della temperatura (CPU/MotherBoard)
                    string strSensor = ((XmlElement) fanElement).GetAttribute("sensor");
                    if (!String.IsNullOrEmpty(strSensor))
                    {
                        switch (strSensor)
                        {
                            case "CPU":
                                SystemFan.Sensor = SystemFan.SensorTemperature.SENSOR_CPU;
                                Logger.Default.Log("Sensore CPU attivato", Logger.LogType.CONSOLE_LOC_FILE);
                                break;

                            case "MB":
                                SystemFan.Sensor = SystemFan.SensorTemperature.SENSOR_BOARD;
                                Logger.Default.Log("Sensore MotherBoard attivato", Logger.LogType.CONSOLE_LOC_FILE);
                                break;

                            default:
                                Logger.Default.Log("Sensore temperatura non disponibile", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                    AppCfg.E_COD_TEMPERATURE_SENSOR_NOT_AVAILABLE);
                                break;
                        }
                    }

                    bool bEnabled;
                    string strEnabled = ((XmlElement) fanElement).GetAttribute("enabled");
                    if (Boolean.TryParse(strEnabled, out bEnabled))
                    {
                        this.Config.IsFanActive = bEnabled;
                    }

                    if (this.Config.IsFanActive)
                    {
                        // flag di monitoraggio funzionamento thread
                        bool bTrigger;
                        string strTrigger = ((XmlElement) systemLedElement).GetAttribute("system_failure_trigger");
                        if (Boolean.TryParse(strTrigger, out bTrigger))
                        {
                            this.Config.IsFanRunning = bTrigger;
                        }

                        // timeout controllo temperatura / impostazione ventole
                        uint iTimeout;
                        string strTimeout = ((XmlElement) fanElement).GetAttribute("check_timeout");
                        if (uint.TryParse(strTimeout, out iTimeout) && iTimeout > 0)
                        {
                            SystemFan.CheckTimeout = iTimeout;
                        }

                        // soglie di temperatura per la gestione delle ventole
                        foreach (XmlNode item in fanElement.ChildNodes)
                        {
                            if (item.GetType() != typeof (XmlElement))
                            {
                                continue;
                            }

                            string sTemp = ((XmlElement) item).GetAttribute("temperature");
                            string sHyst = ((XmlElement) item).GetAttribute("hysteresis");
                            string sRFan = ((XmlElement) item).GetAttribute("pwm_right_fan");
                            string sLFan = ((XmlElement) item).GetAttribute("pwm_left_fan");

                            if (!SystemFan.SetThreshold(sTemp, sHyst, sRFan, sLFan))
                            {
                                Logger.Default.Log(
                                    String.Format(
                                        "Errore: Nodo fan, soglia non valida (temperature={0} hysteresis={1} pwm_right_fan={2} pwm_left_fan={3})",
                                        sTemp, sHyst, sRFan, sLFan), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                    AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                            }
                        }
                    }
                }
                else
                {
                    ret_code = AppCfg.E_XML_CONFIG_FAN_NODE_ERROR;
                    Logger.Default.Log("Errore: Nodo Fan non trovato", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
                }

                // Caricamento dell'elemento relativo alla gestione del FBWF
                XmlNode fbwfElement = ConfigXmlDoc.SelectSingleNode("//telefin/manager/fbwf");

                if (fbwfElement != null)
                {
                    Logger.Default.Log("Caricato nodo FBWF", Logger.LogType.CONSOLE_LOC_FILE);

                    bool bEnabled;
                    string strEnabled = ((XmlElement) fbwfElement).GetAttribute("enabled");
                    if (Boolean.TryParse(strEnabled, out bEnabled))
                    {
                        this.Config.IsFBWFActive = bEnabled;
                    }

                    if (this.Config.IsFBWFActive)
                    {
                        // flag di monitoraggio funzionamento thread
                        bool bTrigger;
                        string strTrigger = ((XmlElement) fbwfElement).GetAttribute("system_failure_trigger");
                        if (Boolean.TryParse(strTrigger, out bTrigger))
                        {
                            this.Config.IsFBWFRunning = bTrigger;
                        }

                        string strTimeout = ((XmlElement) fbwfElement).GetAttribute("check_timeout");
                        string strMaxMemPerc = ((XmlElement) fbwfElement).GetAttribute("max_mem_perc");

                        if (!this.fbwfCurrentSession.SetController(strTimeout, strMaxMemPerc))
                        {
                            Logger.Default.Log(
                                String.Format("Errore: Nodo fbwf, attributi non validi (check_timeout={0} max_mem_perc={1})", strTimeout,
                                    strMaxMemPerc), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                        }
                    }
                }
                else
                {
                    ret_code = AppCfg.E_XML_CONFIG_FBWF_NODE_ERROR;
                    Logger.Default.Log("Errore: Nodo FBWF non trovato", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                        AppCfg.E_XML_CONFIG_FBWF_NODE_ERROR);
                }

                //Caricamento servizi da controllare                                       
                XmlNode serviceElement = ConfigXmlDoc.SelectSingleNode("//telefin/manager/service");

                if (serviceElement != null)
                {
                    Logger.Default.Log("Caricato nodo service", Logger.LogType.CONSOLE_LOC_FILE);

                    bool bEnabled;
                    string strEnabled = ((XmlElement) serviceElement).GetAttribute("enabled");
                    if (Boolean.TryParse(strEnabled, out bEnabled))
                    {
                        this.Config.IsServiceActive = bEnabled;
                    }

                    if (this.Config.IsServiceActive)
                    {
                        // flag di monitoraggio funzionamento thread
                        bool bTrigger;
                        string strTrigger = ((XmlElement) serviceElement).GetAttribute("system_failure_trigger");
                        if (Boolean.TryParse(strTrigger, out bTrigger))
                        {
                            this.Config.IsServiceRunning = bTrigger;
                        }

                        // servizi configurati
                        foreach (XmlNode item in serviceElement.ChildNodes)
                        {
                            if (item.GetType() != typeof (XmlElement))
                            {
                                continue;
                            }

                            bool bServiceEnabled;
                            string sServiceEnabled = ((XmlElement) item).GetAttribute("enabled");
                            if (Boolean.TryParse(sServiceEnabled, out bServiceEnabled))
                            {
                                if (!bServiceEnabled)
                                {
                                    continue;
                                }
                            }

                            string sName = ((XmlElement) item).GetAttribute("name");
                            string sGroup = ((XmlElement) item).GetAttribute("group");

                            string sProcessName = ((XmlElement)item).GetAttribute("processname");

                            try
                            {
                                ServiceControl service = new ServiceControl(sName, sGroup, sProcessName);

                                string sRetryNumber = ((XmlElement) item).GetAttribute("retry");
                                string sRetryDelay = ((XmlElement) item).GetAttribute("retry_delay");

                                if (!service.SetRetry(sRetryNumber, sRetryDelay))
                                {
                                    Logger.Default.Log(
                                        String.Format("Errore: Nodo service, retry non valido (name={0} retry={1} retry_delay={2})", sName,
                                            sRetryNumber, sRetryDelay), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                                        AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                                }

                                string sRunningTimeout = ((XmlElement) item).GetAttribute("check_running_timeout");
                                string sHeartBeatTimeout = ((XmlElement) item).GetAttribute("check_heartbeat_timeout");
                                string sRestartTimeout = ((XmlElement) item).GetAttribute("forced_restart_timeout");
                                string sStartupDelay = ((XmlElement) item).GetAttribute("startup_delay");

                                bool stopOnManagerStopped = false;
                                if (item.Attributes["stop_on_manager_stopped"] != null)
                                {
                                    string sStopOnManagerStopped = ((XmlElement) item).GetAttribute("stop_on_manager_stopped").ToLowerInvariant();
                                    bool.TryParse(sStopOnManagerStopped, out stopOnManagerStopped);
                                }

                                if (!service.SetTimeout(sRunningTimeout, sHeartBeatTimeout, sRestartTimeout, sStartupDelay))
                                {
                                    Logger.Default.Log(
                                        String.Format(
                                            "Errore: Nodo service, timeout non validi (name={0} check_running_timeout={1} check_heartbeat_timeout{2} forced_restart_timeout={3} startup_delay={4} stop_on_manager_stopped={5})",
                                            sName, sRunningTimeout, sHeartBeatTimeout, sRestartTimeout, sStartupDelay, stopOnManagerStopped),
                                        Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                                }

                                service.StopOnManagerStopped = stopOnManagerStopped;

                                if (item.Attributes["supervisor_id"] != null)
                                {
                                    string sSupervisorId = ((XmlElement) item).GetAttribute("supervisor_id");
                                    int supervisorId;
                                    if (int.TryParse(sSupervisorId, out supervisorId))
                                    {
                                        if (SupervisorUtility.IsSupervisorIdValid(supervisorId))
                                        {
                                            service.SupervisorId = supervisorId;
                                        }
                                    }
                                }

                                this.systemServices.ServiceUnderControl(service);
                            }
                            catch (Exception ex)
                            {
                                Logger.Default.Log(String.Format("{0} (name={1} group={2} enabled={3})", ex.Message, sName, sGroup, sServiceEnabled),
                                    Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, AppCfg.E_XML_CONFIG_MANAGER_NODE_ERROR);
                            }
                        }
                    }
                }
                else
                {
                    ret_code = AppCfg.E_XML_CONFIG_SERVICE_NODE_ERROR;
                    Logger.Default.Log("Errore: Nodo service non trovato", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, ret_code);
                }
            }
            catch (Exception ex)
            {
                ret_code = AppCfg.E_XML_CONFIG_OPEN_SOURCE_CRITICAL_ERROR;
                Logger.Default.Log("Errore lettura file XML di configurazione. ECCEZIONE: " + ex.Message, Logger.LogType.CONSOLE_LOC_FILE,
                    Logger.EventType.ERROR, AppCfg.E_XML_CONFIG_OPEN_SOURCE_CRITICAL_ERROR);
                Logger.Default.Log("STLCManager service STOPPED", Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR,
                    AppCfg.W_COD_SERVICE_MANAGER_STOPPED);
                Environment.Exit(3);
                Logger.Default.ManageException(ex);
            }

            return ret_code;
        }

        /// <summary>
        ///     Gestione evento di chiusura servizio
        /// </summary>
        /// <returns>eventuale codice di errore</returns>
        public int CloseService(bool stopServices)
        {
            int retCode = NO_ERROR;

            try
            {
                IsStopping(true);

                SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemSERVICES);
                this.systemServices.Disable();

                if (stopServices)
                {
                    this.SetAllMarkedServicesStatus(STLCServiceCommand.STOP);
                }

                if (STLCConfig.IsSTLCHost)
                {
                    SystemLed.Set(SystemLed.Mode.SYSTEM_FAILURE);
                    SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemLED);
                    SystemLed.Disable();

                    SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemFANS);
                    SystemFan.Disable();

                    SystemWDog.ThreadDeleteControl(AppCfg.ThreadSystemFBWF);
                    this.fbwfCurrentSession.Disable();

                    SystemWDog.StopService(stopServices);

                    retCode = JidaPlatform.Default.UnloadJida();

                    if (retCode != NO_ERROR)
                    {
                        Logger.Default.Log("Errore " + retCode + " in fase di scaricamento della libreria JIDA", Logger.LogType.CONSOLE_LOC_FILE,
                            Logger.EventType.ERROR, retCode);
                    }
                    else
                    {
                        Logger.Default.Log("Scaricamento della libreria JIDA OK.", Logger.LogType.CONSOLE_LOC_FILE);
                    }

                    if (stopServices)
                        clientCommLayer.CloseClientLayerWCFComm();

                }
            }
            catch (Exception ex)
            {
                Logger.Default.ManageException(ex);
            }
            return retCode;
        }

        /// <summary>
        ///     Ritorna la versione del Sw dell'STLC100 installato.
        ///     Legge tale valore da chiave di registro configurata.
        /// </summary>
        /// <param name="sVersion">versione letta da chiave di registro</param>
        /// <param name="resultDesc">descrizione esito operazione</param>
        /// <returns>eventuale codice di errore, NO_ERROR se lettura eseguita correttamente</returns>
        private int GetSTLCVersion(out string sVersion, out string resultDesc)
        {
            int retCode = NO_ERROR;
            resultDesc = "Operazione eseguita con successo";
            sVersion = "";

            try
            {
                string STLCVersionKeyPath = AppCfg.Default.sForcedStlcVersion; // .GetConfigValue(STLCSystemReference.FORCED_STLC_VERSION, "");

                if (String.IsNullOrEmpty(STLCVersionKeyPath))
                {
                    STLCVersionKeyPath = AppCfg.Default.sPathKeyOnRegistry; // .FP_STLC_VERSION_KEY_VALUE_PATH;
                    sVersion = (string) Registry.GetValue(STLCVersionKeyPath, AppCfg.Default.sStlcVersionKeyName, ""); // .FP_STLC_VERSION_KEY_NAME_VALUE_PATH, "");
                }
                else
                {
                    sVersion = STLCVersionKeyPath;
                }
            }
            catch (Exception ex)
            {
                retCode = AppCfg.E_COD_FUNCTION_EXCEPTION;
                string retErrMsg = "Eccezione in GetSTLCVersion di STLCEngine. DESCRIZIONE ECCEZIONE: " + ex.StackTrace;
                if (ex.GetType() == typeof (NullReferenceException))
                {
                    retCode = AppCfg.E_COD_INVALID_REGKEY_PATH;
                    retErrMsg = "Eccezione NullReferenceException in GetSTLCVersion. Version non trovato";
                }
                resultDesc = retErrMsg;
                Logger.Default.Log(retErrMsg, Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.ERROR, retCode);
            }

            return retCode;
        }

        private void SetAllMarkedServicesStatus(STLCServiceCommand command)
        {
            List<string> servicesToStop = new List<string>();

            foreach (var service in this.systemServices.ServicesSelect())
            {
                if (service.StopOnManagerStopped)
                {
                    servicesToStop.Add(service.Name);
                }
            }

            if (servicesToStop.Count > 0)
            {
                Logger.Default.Log(
                    string.Format("Stop servizio manager e disattivazione servizi collegati. Lista servizi da fermare: {0}",
                        String.Join(", ", servicesToStop.ToArray())), Logger.LogType.CONSOLE_LOC_FILE, Logger.EventType.INFORMATION);

                this.clientCommLayer_OnSetSTLCServiceStatus(null,
                    new STLCServiceStatusEventArgs
                    {
                        Request =
                            new STLCServiceStatusRequest
                            {
                                clientType = RequestingClientType.MAIN_SERVICE,
                                command = command,
                                group = null,
                                services = servicesToStop
                            }
                    });
            }
        }

        #endregion

        public static bool Execute_EmptyDatabase()
        {
            using (SqlConnection cnn = new SqlConnection())
            {
                ConnectionStringSettings localDatabaseConnectionString = ConfigurationManager.ConnectionStrings["LocalDatabase"];
                cnn.ConnectionString = localDatabaseConnectionString.ConnectionString;

                SqlCommand cmd = new SqlCommand(AppCfg.Default.sEmptyDbSPName, cnn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    cnn.Open();
                    cmd.ExecuteNonQuery();
                    cnn.Close();
                    return (true);
                }
                catch (SqlException)
                {
                    Logger.Default.Log("Errore SQL: SP non corretto", Logger.LogType.CONSOLE_LOC_FILE);
                    return false;
                }
                catch (Exception ex)
                {
                    Logger.Default.Log("Eccezione EmptyDB", Logger.LogType.CONSOLE_LOC_FILE);
                    return false;
                }
            }
        }

    }
}
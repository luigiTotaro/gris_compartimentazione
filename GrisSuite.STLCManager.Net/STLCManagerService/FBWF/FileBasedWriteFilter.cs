﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using STLCManager.Service.Types;

namespace STLCManager.Service.FBWF
{
    public class FileBasedWriteFilter
    {
        private const uint VOLUME_LIST_BUFFER_SIZE = 2048;
        private const uint EXCLUSION_LIST_BUFFER_SIZE = 10240;

        /// <summary>
        ///   Sessione su cui eseguire le operazioni
        /// </summary>
        public enum Session
        {
            /// <summary>
            ///   Sessione corrente
            /// </summary>
            Current = 0,

            /// <summary>
            ///   Sessione successiva (dopo riavvio)
            /// </summary>
            Next = 1
        }

        private readonly Session workingSession;

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "session">Sessione su cui eseguire le operazioni</param>
        public FileBasedWriteFilter(Session session)
        {
            this.workingSession = session;
        }

        /// <summary>
        ///   Costruttore
        /// </summary>
        public FileBasedWriteFilter()
        {
            this.workingSession = Session.Current;
        }

        /// <summary>
        ///   Indica se il filtro è attivo per la sessione
        /// </summary>
        /// <returns>True se il filtro è attivo, false altrimenti</returns>
        public bool IsFilterEnabled()
        {
            if (!STLCConfig.IsSTLCHost) return false;
            bool returnValue = false;

            try
            {
                uint currentSession = 0;
                uint nextSession = 0;

                long ret_code = FBWFAPIHelper.FbwfIsFilterEnabled(ref currentSession, ref nextSession);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro currentSession o nextSession è nullo.");
                    case FBWFAPIHelper.NO_ERROR:
                        switch (this.workingSession)
                        {
                            case Session.Current:
                                returnValue = currentSession != 0;
                                break;
                            case Session.Next:
                                returnValue = nextSession != 0;
                                break;
                        }
                        break;
                    default:
                        throw new FileBasedWriteFilterException("Errore non previsto durante recupero dello stato filtro per la sessione.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }

            return returnValue;
        }

        /// <summary>
        ///  Abilita il filtro
        /// </summary>
        public void EnableFilter()
        {
            if (STLCConfig.IsSTLCHost)
            {
                try
                {
                    long ret_code = FBWFAPIHelper.FbwfEnableFilter();

                    switch (ret_code)
                    {
                        case FBWFAPIHelper.NO_ERROR:
                            break;
                        default:
                            throw new FileBasedWriteFilterException("Errore non previsto durante abilitazione filtro.");
                    }
                }
                catch (DllNotFoundException)
                {
                    throw new FileBasedWriteFilterException(
                        "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
                }                
            }
        }

        /// <summary>
        ///  Disabilita il filtro
        /// </summary>
        public void DisableFilter()
        {
            if (STLCConfig.IsSTLCHost)
            {
                try
                {
                    long ret_code = FBWFAPIHelper.FbwfDisableFilter();

                    switch (ret_code)
                    {
                        case FBWFAPIHelper.ERROR_INVALID_FUNCTION:
                            throw new FileBasedWriteFilterException("Funzione non valida durante disabilitazione filtro.");
                        case FBWFAPIHelper.NO_ERROR:
                            break;
                        default:
                            throw new FileBasedWriteFilterException("Errore non previsto durante disabilitazione filtro.");
                    }
                }
                catch (DllNotFoundException)
                {
                    throw new FileBasedWriteFilterException(
                        "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
                }                
            }
        }

        /// <summary>
        ///   Recupera l'allocazione di memoria per il filtro
        /// </summary>
        /// <returns>Classe che contiene le informazioni relative alla memoria</returns>
        public FileBasedWriteFilterMemoryUsage GetMemoryUsage()
        {
            if (!STLCConfig.IsSTLCHost) return null;
            FileBasedWriteFilterMemoryUsage memoryUsage;

            try
            {
                FBWFAPIHelper.FbwfMemoryUsage usage = new FBWFAPIHelper.FbwfMemoryUsage();

                long ret_code = FBWFAPIHelper.FbwfGetMemoryUsage(ref usage);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro usage è nullo.");
                    case FBWFAPIHelper.NO_ERROR:
                        memoryUsage = new FileBasedWriteFilterMemoryUsage(usage.CurrCacheThreshold, usage.NextCacheThreshold, usage.DirStructure,
                                                                          usage.FileData);
                        break;
                    default:
                        throw new FileBasedWriteFilterException("Errore non previsto durante recupero dell'allocazione di memoria per il filtro.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }

            return memoryUsage;
        }

        /// <summary>
        ///   Recupera la lista dei volumi protetti
        /// </summary>
        /// <returns>Lista di stringhe relative ai volumi protetti</returns>
        public List<string> GetVolumeList()
        {
            if (!STLCConfig.IsSTLCHost) return new List<string>();
            List<string> protectedVolumeList;

            try
            {
                uint bufSize = VOLUME_LIST_BUFFER_SIZE;
                char[] volumeList = new char[((int) bufSize)];

                long ret_code = FBWFAPIHelper.FbwfGetVolumeList((uint) this.workingSession, volumeList, ref bufSize);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro volumeList o la dimensione del buffer è nullo.");
                    case FBWFAPIHelper.ERROR_MORE_DATA:
                        throw new FileBasedWriteFilterException(
                            string.Format("Il buffer per la lista dei volumi è troppo piccolo. Dimensione richiesta {0} byte.", bufSize));
                    case FBWFAPIHelper.NO_ERROR:
                        protectedVolumeList = UnpackMultiString(volumeList);
                        break;
                    default:
                        throw new FileBasedWriteFilterException("Errore non previsto durante recupero della lista dei volumi protetti.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }

            return protectedVolumeList;
        }

        /// <summary>
        ///   Indica se il volume passato è protetto o meno
        /// </summary>
        /// <param name = "volume">Nome del volume</param>
        /// <returns>True se il volume è protetto, false altrimenti</returns>
        public bool IsVolumeProtected(string volume)
        {
            if (!STLCConfig.IsSTLCHost) return false;
            bool returnValue = false;

            try
            {
                uint currentSession = 0;
                uint nextSession = 0;

                long ret_code = FBWFAPIHelper.FbwfIsVolumeProtected(volume, ref currentSession, ref nextSession);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro volume è nullo.");
                    case FBWFAPIHelper.NO_ERROR:
                        switch (this.workingSession)
                        {
                            case Session.Current:
                                returnValue = currentSession != 0;
                                break;
                            case Session.Next:
                                returnValue = nextSession != 0;
                                break;
                        }
                        break;
                    default:
                        throw new FileBasedWriteFilterException("Errore non previsto durante recupero dello stato di protezione del volume corrente.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }

            return returnValue;
        }

        /// <summary>
        ///   Recupera le informazioni virtuali relative al volume
        /// </summary>
        /// <param name = "volume">Nome del volume</param>
        /// <returns>Classe che contiene le informazioni virtuali del volume</returns>
        public FileBasedWriteFilterFileSystemInfo GetVirtualSize(string volume)
        {
            if (!STLCConfig.IsSTLCHost) return null;
            FileBasedWriteFilterFileSystemInfo volumeInfo;

            try
            {
                FBWFAPIHelper.FbwfFSInfo fsInfo = new FBWFAPIHelper.FbwfFSInfo();

                long ret_code = FBWFAPIHelper.FbwfGetVirtualSize(volume, ref fsInfo);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_DRIVE:
                        throw new FileBasedWriteFilterException("Il volume specificato è valido, ma non esiste o non è protetto.");
                    case FBWFAPIHelper.ERROR_INVALID_FUNCTION:
                        throw new FileBasedWriteFilterException("Il filtro è disattivato per la sessione successiva.");
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro volume è nullo.");
                    case FBWFAPIHelper.NO_ERROR:
                        volumeInfo = new FileBasedWriteFilterFileSystemInfo(fsInfo.TotalAllocationUnits.QuadPart,
                                                                            fsInfo.AvailableAllocationUnits.QuadPart, fsInfo.SectorsPerAllocationUnit,
                                                                            fsInfo.BytesPerSector);
                        break;
                    default:
                        throw new FileBasedWriteFilterException("Errore non previsto durante recupero delle informazioni virtuali relative al volume.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }

            return volumeInfo;
        }

        /// <summary>
        ///   Recupera le informazioni attuali relative al volume
        /// </summary>
        /// <param name = "volume">Nome del volume</param>
        /// <returns>Classe che contiene le informazioni attuali del volume</returns>
        public FileBasedWriteFilterFileSystemInfo GetActualSize(string volume)
        {
            if (!STLCConfig.IsSTLCHost) return null;
            FileBasedWriteFilterFileSystemInfo volumeInfo;

            try
            {
                FBWFAPIHelper.FbwfFSInfo fsInfo = new FBWFAPIHelper.FbwfFSInfo();

                long ret_code = FBWFAPIHelper.FbwfGetActualSize(volume, ref fsInfo);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_DRIVE:
                        throw new FileBasedWriteFilterException("Il volume specificato è valido, ma non esiste o non è protetto.");
                    case FBWFAPIHelper.ERROR_INVALID_FUNCTION:
                        throw new FileBasedWriteFilterException("Il filtro è disattivato per la sessione successiva.");
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro volume è nullo.");
                    case FBWFAPIHelper.NO_ERROR:
                        volumeInfo = new FileBasedWriteFilterFileSystemInfo(fsInfo.TotalAllocationUnits.QuadPart,
                                                                            fsInfo.AvailableAllocationUnits.QuadPart, fsInfo.SectorsPerAllocationUnit,
                                                                            fsInfo.BytesPerSector);
                        break;
                    default:
                        throw new FileBasedWriteFilterException(
                            "Errore non previsto durante l'accesso al driver del volume, nel recupero informazioni attuali relative al volume.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }

            return volumeInfo;
        }

        /// <summary>
        ///   Recupera la lista dei file esclusi dalla protezione per il volume
        /// </summary>
        /// <param name = "volume">Nome del volume</param>
        /// <returns>Lista di stringhe relative ai file esclusi</returns>
        public List<string> GetExclusionList(string volume)
        {
            if (!STLCConfig.IsSTLCHost) return new List<string>();
            List<string> exclusionFileList;

            try
            {
                uint exclusionListSize = EXCLUSION_LIST_BUFFER_SIZE;
                char[] exclusionList = new char[(int) exclusionListSize];

                long ret_code = FBWFAPIHelper.FbwfGetExclusionList(volume, (uint) this.workingSession, exclusionList, ref exclusionListSize);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_DRIVE:
                        throw new FileBasedWriteFilterException("Il volume specificato è valido, ma non esiste o non è protetto.");
                    case FBWFAPIHelper.ERROR_MORE_DATA:
                        throw new FileBasedWriteFilterException(
                            string.Format("Il buffer per la lista dei file esclusi è troppo piccolo. Dimensione richiesta {0} byte.",
                                          exclusionListSize));
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro volume o exclusionList è nullo.");
                    case FBWFAPIHelper.NO_ERROR:
                        exclusionFileList = UnpackMultiString(exclusionList);
                        break;
                    default:
                        throw new FileBasedWriteFilterException(
                            "Errore non previsto durante recupero della lista dei file esclusi dalla protezione per il volume.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }

            return exclusionFileList;
        }

        /// <summary>
        ///   Recupera la lista dei file protetti per il volume e le relative informazioni di cache
        /// </summary>
        /// <param name = "volume">Nome del volume</param>
        /// <returns>Lista di oggetti con le informazioni di cache dei file protetti</returns>
        public List<FileBasedWriteFilterFileCacheDetail> GetCachedFileList(string volume)
        {
            if (!STLCConfig.IsSTLCHost) return null;
            List<FileBasedWriteFilterFileCacheDetail> cacheFileList = new List<FileBasedWriteFilterFileCacheDetail>();

            bool shouldCloseFind = false;

            try
            {
                FBWFAPIHelper.FbwfCacheDetail cacheDetail = new FBWFAPIHelper.FbwfCacheDetail();
                uint cacheDetailSize = (uint) Marshal.SizeOf(cacheDetail);

                long ret_code = FBWFAPIHelper.FbwfFindFirst(volume, ref cacheDetail, ref cacheDetailSize);

                switch (ret_code)
                {
                    case FBWFAPIHelper.ERROR_INVALID_DRIVE:
                        throw new FileBasedWriteFilterException("Il volume specificato è valido, ma non esiste o non è protetto.");
                    case FBWFAPIHelper.ERROR_MORE_DATA:
                        throw new FileBasedWriteFilterException(
                            string.Format("Il buffer per i dati di cache del file è troppo piccolo. Dimensione richiesta {0} byte.", cacheDetailSize));
                    case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                        throw new FileBasedWriteFilterException("Il parametro volume, cacheDetail o size è nullo.");
                    case FBWFAPIHelper.ERROR_INVALID_FUNCTION:
                        throw new FileBasedWriteFilterException("Il filtro è disattivato per la sessione corrente.");
                    case FBWFAPIHelper.ERROR_FILE_NOT_FOUND:
                        break;
                    case FBWFAPIHelper.ERROR_NOT_ENOUGH_MEMORY:
                        throw new FileBasedWriteFilterException(
                            "Non è disponibile una quantità di memoria sufficiente per allocare i buffer per la ricerca dei file protetti.");
                    case FBWFAPIHelper.NO_ERROR:
                        shouldCloseFind = true;

                        cacheFileList.Add(new FileBasedWriteFilterFileCacheDetail(cacheDetail.FileName, cacheDetail.CacheSize,
                                                                                  cacheDetail.OpenHandleCount));

                        do
                        {
                            cacheDetail = new FBWFAPIHelper.FbwfCacheDetail();
                            cacheDetailSize = (uint) Marshal.SizeOf(cacheDetail);

                            ret_code = FBWFAPIHelper.FbwfFindNext(ref cacheDetail, ref cacheDetailSize);

                            switch (ret_code)
                            {
                                case FBWFAPIHelper.ERROR_INVALID_DRIVE:
                                    throw new FileBasedWriteFilterException("Il volume specificato è valido, ma non esiste o non è protetto.");
                                case FBWFAPIHelper.ERROR_MORE_DATA:
                                    throw new FileBasedWriteFilterException(
                                        string.Format("Il buffer per i dati di cache del file è troppo piccolo. Dimensione richiesta {0} byte.",
                                                      cacheDetailSize));
                                case FBWFAPIHelper.ERROR_INVALID_PARAMETER:
                                    throw new FileBasedWriteFilterException("Il parametro cacheDetail o size è nullo.");
                                case FBWFAPIHelper.ERROR_INVALID_FUNCTION:
                                    throw new FileBasedWriteFilterException(
                                        "Il filtro è disattivato per la sessione corrente o non è stata eseguita una prima ricerca di file prima della presente.");
                                case FBWFAPIHelper.ERROR_NO_MORE_FILES:
                                    break;
                                case FBWFAPIHelper.ERROR_NOT_ENOUGH_MEMORY:
                                    throw new FileBasedWriteFilterException(
                                        "Non è disponibile una quantità di memoria sufficiente per allocare i buffer per la ricerca dei file protetti.");
                                case FBWFAPIHelper.NO_ERROR:
                                    cacheFileList.Add(new FileBasedWriteFilterFileCacheDetail(cacheDetail.FileName, cacheDetail.CacheSize,
                                                                                              cacheDetail.OpenHandleCount));
                                    break;
                                default:
                                    throw new FileBasedWriteFilterException(
                                        "Errore non previsto durante operazione recupero della lista dei file protetti per il volume e le relative informazioni di cache.");
                            }
                        } while (ret_code == FBWFAPIHelper.NO_ERROR);

                        break;
                    default:
                        throw new FileBasedWriteFilterException(
                            "Errore non previsto durante operazione recupero del primo file protetto per il volume e delle relative informazioni di cache.");
                }
            }
            catch (DllNotFoundException)
            {
                throw new FileBasedWriteFilterException(
                    "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
            }
            finally
            {
                if (shouldCloseFind)
                {
                    try
                    {
                        uint ret_code = FBWFAPIHelper.FbwfFindClose();

                        switch (ret_code)
                        {
                            case FBWFAPIHelper.ERROR_INVALID_FUNCTION:
                                throw new FileBasedWriteFilterException(
                                    "E' stata richiamata la chiusura della ricerca file protetti senza una relativa ricerca iniziale.");
                            case FBWFAPIHelper.NO_ERROR:
                                break;
                            default:
                                throw new FileBasedWriteFilterException(
                                    "Errore non previsto durante operazione di chiusura della ricerca file protetti.");
                        }
                    }
                    catch (DllNotFoundException)
                    {
                        throw new FileBasedWriteFilterException(
                            "Impossibile caricare la libreria \"File-Based Write Filter\". Probabile piattaforma non supportata.");
                    }
                }
            }

            return cacheFileList;
        }

        #region Metodi privati

        /// <summary>
        ///   Converte una multi-string in lista di stringhe
        /// </summary>
        /// <param name = "data">Array di caratteri unicode da convertire</param>
        /// <returns>Lista di stringhe</returns>
        private static List<string> UnpackMultiString(char[] data)
        {
            List<string> strings = new List<string>();

            int currentPosition = 0;
            int dataLen = data.Length;

            while (currentPosition < dataLen)
            {
                int nextNull = currentPosition;
                while (nextNull < dataLen && data[nextNull] != (char) 0)
                {
                    nextNull++;
                }

                if (nextNull < dataLen)
                {
                    if (nextNull - currentPosition > 0)
                    {
                        strings.Add(new String(data, currentPosition, nextNull - currentPosition));
                    }
                }
                else
                {
                    strings.Add(new String(data, currentPosition, dataLen - currentPosition));
                }

                currentPosition = nextNull + 1;
            }

            return strings;
        }

        #endregion
    }
}
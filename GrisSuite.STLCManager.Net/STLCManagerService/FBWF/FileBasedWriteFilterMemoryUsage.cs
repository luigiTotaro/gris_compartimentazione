﻿namespace STLCManager.Service.FBWF
{
    /// <summary>
    ///   Contiene le informazioni sull'uso della memoria
    /// </summary>
    public class FileBasedWriteFilterMemoryUsage
    {
        /// <summary>
        ///   Limite della cache in byte della sessione corrente
        /// </summary>
        public long CurrentSessionCacheThreshold { get; private set; }

        /// <summary>
        ///   Limite della cache in byte della sessione successiva
        /// </summary>
        public long NextSessionCacheThreshold { get; private set; }

        /// <summary>
        ///   Memoria in byte usata per archiviare la struttura di cartelle
        /// </summary>
        public long DirectoryStructure { get; private set; }

        /// <summary>
        ///   Memoria in byte usata dalla cache dei dati relativi a file
        /// </summary>
        public long FileData { get; private set; }

        /// <summary>
        ///   Memoria disponibile per la sessione corrente
        /// </summary>
        public long CurrentSessionAvailableMemory
        {
            get { return this.CurrentSessionCacheThreshold - this.DirectoryStructure - this.FileData; }
        }

        /// <summary>
        ///   Memoria occupata per la sessione corrente
        /// </summary>
        public long CurrentSessionUsedMemory
        {
            get { return this.DirectoryStructure + this.FileData; }
        }

        /// <summary>
        ///   Percentuale memoria disponibile per la sessione corrente
        /// </summary>
        public double CurrentSessionAvailableMemoryPercentage
        {
            get { return (this.CurrentSessionAvailableMemory/(double) this.CurrentSessionCacheThreshold)*100; }
        }

        /// <summary>
        ///   Percentuale memoria occupata per la sessione corrente
        /// </summary>
        public double CurrentSessionUsedMemoryPercentage
        {
            get { return ((this.CurrentSessionUsedMemory)/(double) this.CurrentSessionCacheThreshold)*100; }
        }

        /// <summary>
        ///   Costruttore
        /// </summary>
        /// <param name = "currentSessionCacheThreshold">Limite della cache in byte della sessione corrente</param>
        /// <param name = "nextSessionCacheThreshold">Limite della cache in byte della sessione successiva</param>
        /// <param name = "directoryStructure">Memoria in byte usata per archiviare la struttura di cartelle</param>
        /// <param name = "fileData">Memoria in byte usata dalla cache dei dati relativi a file</param>
        public FileBasedWriteFilterMemoryUsage(long currentSessionCacheThreshold, long nextSessionCacheThreshold, long directoryStructure,
                                               long fileData)
        {
            this.CurrentSessionCacheThreshold = currentSessionCacheThreshold;
            this.NextSessionCacheThreshold = nextSessionCacheThreshold;
            this.DirectoryStructure = directoryStructure;
            this.FileData = fileData;
        }
    }
}
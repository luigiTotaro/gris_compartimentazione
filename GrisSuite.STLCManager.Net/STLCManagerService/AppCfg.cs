﻿using System;
using System.Configuration;
using Microsoft.Win32;
using STLCManager.Service.Properties;
using STLCManager.Service.Types;
using System.Collections.Generic;

namespace STLCManager.Service
{
	public class AppCfg
	{
		#region Threads definition

		public const string                     ThreadSystemWDOG                                                = "SystemWDOG";
		public const string                     ThreadSystemLED                                                 = "SystemLED";
		public const string                     ThreadSystemFANS                                                = "SystemFANS";
		public const string                     ThreadSystemFBWF                                                = "SystemFBWF";
		public const string                     ThreadSystemSERVICES                                            = "SystemSERVICES";
		public const string                     ThreadSystemENGINE                                              = "SystemENGINE";

		#endregion

        #region Puntatori alle instanze delle classi di appoggio

        private AppCfgUtility.ModifyRegistry    _regManager;
        private AppCfgUtility.Section           _section;

        #endregion

        #region Nomi dei valori di configurazione da leggere dal file .config o dal registro

        private const string                    SQL_COMMAND_TIMEOUT_SECONDS_NAME                                = "SqlCommandTimeoutSeconds";
        private const string                    ON_CHECK_DB_NAME                                                = "OnCheckDB";
        private const string                    CHECK_DB_RETRY_NUM_NAME                                         = "CheckDBRetryNum";
        private const string                    SERVICE_WAITING_FOR_START_NAME                                  = "ServiceWaitingForStart";
        private const string                    SERVICE_WAITING_FOR_STOP_NAME                                   = "ServiceWaitingForStop";
        private const string                    PARSE_SERVER_XML_CONFIG_NAME                                    = "ParseServerXmlConfig";
        private const string                    START_ONLY_NEEDED_SUPERVISOR_SERVICES_NAME                      = "StartOnlyNeededSupervisorServices";
        private const string                    DOWNLOAD_REGION_LIST_DATA_AND_UPDATE_DATABASE_ON_START_NAME     = "DownloadRegionListDataAndUpdateDatabaseOnStart";
        private const string                    DOWNLOAD_DEVICE_TYPE_LIST_AND_UPDATE_DATABASE_ON_START_NAME     = "DownloadDeviceTypeListAndUpdateDatabaseOnStart";
        private const string                    NETWORK_ADAPTER_NAME_1_NAME                                     = "NetworkAdapterName1";
        private const string                    LOG_ON_FILE_ACTIVE_NAME                                         = "LogOnFileActive";
        private const string                    PATH_STLC_VERSION_KEY_NAME                                      = "PathSTLCVersionKey";
        private const string                    STLC_VERSION_KEY_NAME                                           = "STLCVersionKeyName";
        private const string                    PATH_XML_CONFIG_NAME                                            = "PathXMLConfig";
        private const string                    PATH_LAST_AUTO_CHECK_SERVICES_KEY_NAME                          = "PathLastAutoCheckServicesKey";
        private const string                    LAST_AUTO_CHECK_KEY_NAME                                        = "LastAutoCheckKeyName";
        private const string                    PATH_SQL_SCRIPT_NAME                                            = "PathSQLScript";
        private const string                    PATH_XML_ACTION_NAME                                            = "PathXMLAction";
        private const string                    PATH_LOG_FOLDER_NAME                                            = "PathLogFolder";
        private const string                    XML_SYSTEM_NAME                                                 = "XMLSystem";
        private const string                    EVENT_LOG_LEVEL_NAME                                            = "EventLogLevel";
        private const string                    HARDWARE_PROFILE_NAME                                           = STLCSystemReference.HARDWARE_PROFILE_CONFIG_ATTRIBUTE_NAME;
        private const string                    FORCED_SERVER_ID_NAME                                           = STLCSystemReference.FORCED_SERVER_ID;
        private const string                    FORCED_STLC_VERSION_NAME                                        = STLCSystemReference.FORCED_STLC_VERSION;
        private const string                    EMPTYDB_SP_NAME                                                 = "EmptyDbSP";

        #endregion

        #region Valori di default dei parametri di configurazione da leggere dal file .config o dal registro

        private const int                       SQL_COMMAND_TIMEOUT_SECONDS_DEFAULT                             = 120;
        private const bool                      ON_CHECK_DB_DEFAULT                                             = true;
        private const int                       CHECK_DB_RETRY_NUM_DEFAULT                                      = 5;
        private const int                       SERVICE_WAITING_FOR_START_DEFAULT                               = 30;
        private const int                       SERVICE_WAITING_FOR_STOP_DEFAULT                                = 10;
        private const bool                      LOG_ON_FILE_ACTIVE_DEFAULT                                      = false;
        private const bool                      PARSE_SERVER_XML_CONFIG_DEFAULT                                 = true;
        private const bool                      START_ONLY_NEEDED_SUPERVISOR_SERVICES_DEFAULT                   = false;
        private const bool                      DOWNLOAD_REGION_LIST_DATA_AND_UPDATE_DATABASE_ON_START_DEFAULT  = true;
        private const bool                      DOWNLOAD_DEVICE_TYPE_LIST_AND_UPDATE_DATABASE_ON_START_DEFAULT  = true;
        private const string                    NETWORK_ADAPTER_NAME_1_DEFAULT                                  = "ETH1";
        private const string                    FORCED_SERVER_ID_DEFAULT                                        = "";
        private const string                    FORCED_STLC_VERSION_DEFAULT                                     = "";
        private const string                    HARDWARE_PROFILE_STRING_DEFAULT                                 = "";
        private const string                    EVENT_LOG_LEVEL_STRING_DEFAULT                                  = "";
        private const string                    PATH_KEY_ON_REGISTRY_DEFAULT                                    = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Telefin\\STLC1000\\";
        private const string                    STLC_VERSION_KEY_NAME_DEFAULT                                   = "Version";
        private const string                    PATH_XML_CONFIG_DEFAULT                                         = "C:\\Program Files\\Telefin\\STLCManager\\STLCManagerConfig.xml";
        private const string                    PATH_LAST_AUTO_CHECK_SERVICES_KEY_DEFAULT                       = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\";
        private const string                    LAST_AUTO_CHECK_KEY_NAME_DEFAULT                                = "LastAutoCheck";
        private const string                    PATH_SQL_SCRIPT_DEFAULT                                         = "C:\\Program Files\\Telefin\\STLCManager\\AdjustSQLServerNameAndSecurity.sql";
        private const string                    PATH_XML_ACTION_DEFAULT                                         = "C:\\Program Files\\Telefin\\STLCManager\\ActionToDo.xml";
        private const string                    PATH_LOG_FOLDER_DEFAULT                                         = "C:\\Program Files\\Telefin\\STLCManager";
        private const string                    XML_SYSTEM_DEFAULT                                              = "C:\\Program Files\\Telefin\\STLCManager\\System.xml";
        private const string                    PATH_WORKING_DIR_DEFAULT                                        = "C:\\Program Files\\Telefin\\STLCManager";
        private const HardwareProfile           HARDWARE_PROFILE_DEFAULT                                        = HardwareProfile.STLC;
        private const EventLogLevel             EVENT_LOG_LEVEL_DEFAULT                                         = EventLogLevel.ERROR;
        private const string                    EMPTYDB_SP_DEFAULT                                              = "util_emptydatabase";

        #endregion

        #region Campi privati

        private int                             _nSqlCommandTimeoutSeconds                                      = SQL_COMMAND_TIMEOUT_SECONDS_DEFAULT;
        private bool                            _bOnCheckDB                                                     = ON_CHECK_DB_DEFAULT;
        private int                             _nCheckDBRetryNum                                               = CHECK_DB_RETRY_NUM_DEFAULT;
        private int                             _nServiceWaitForStart                                           = SERVICE_WAITING_FOR_START_DEFAULT;
        private int                             _nServiceWaitForStop                                            = SERVICE_WAITING_FOR_STOP_DEFAULT;
        private bool                            _bLogOnFileActive                                               = LOG_ON_FILE_ACTIVE_DEFAULT;
        private bool                            _bParseServerXmlConfig                                          = PARSE_SERVER_XML_CONFIG_DEFAULT;
        private bool                            _bStartOnlyNeededSupervisorServices                             = START_ONLY_NEEDED_SUPERVISOR_SERVICES_DEFAULT;
        private bool                            _bDownloadRegionListDataAndUpdateDatabaseOnStart                = DOWNLOAD_REGION_LIST_DATA_AND_UPDATE_DATABASE_ON_START_DEFAULT;
        private bool                            _bDownloadDeviceTypeListAndUpdateDatabaseOnStart                = DOWNLOAD_DEVICE_TYPE_LIST_AND_UPDATE_DATABASE_ON_START_DEFAULT;
        private string                          _sNetworkAdapterName1                                           = NETWORK_ADAPTER_NAME_1_DEFAULT;
        private string                          _sPathKeyOnRegistry                                             = PATH_KEY_ON_REGISTRY_DEFAULT;
        private string                          _sForcedServerID                                                = FORCED_SERVER_ID_DEFAULT;
        private string                          _sForcedStlcVersion                                             = FORCED_STLC_VERSION_DEFAULT;
        private string                          _sStlcVersionKeyName                                            = STLC_VERSION_KEY_NAME_DEFAULT;
        private string                          _sPathXmlConfig                                                 = PATH_XML_CONFIG_DEFAULT;
        private string                          _sPathLastAutoCheckServicesKey                                  = PATH_LAST_AUTO_CHECK_SERVICES_KEY_DEFAULT;
        private string                          _sLastAutoCheckKeyName                                          = LAST_AUTO_CHECK_KEY_NAME_DEFAULT;
        private string                          _sPathSQLScript                                                 = PATH_SQL_SCRIPT_DEFAULT;
        private string                          _sPathXmlAction                                                 = PATH_XML_ACTION_DEFAULT;
        private string                          _sPathLogFolder                                                 = PATH_LOG_FOLDER_DEFAULT;
        private string                          _sXMLSystem                                                     = XML_SYSTEM_DEFAULT;
        private string                          _sWorkingDir                                                    = PATH_WORKING_DIR_DEFAULT;
        private string                          _sHardwareProfile                                               = HARDWARE_PROFILE_STRING_DEFAULT;
        private HardwareProfile                 _hpHardwareProfile                                              = HARDWARE_PROFILE_DEFAULT;
        private string                          _sEventLogLevel                                                 = EVENT_LOG_LEVEL_STRING_DEFAULT;
        private EventLogLevel                   _lvEventLogLevel                                                = EVENT_LOG_LEVEL_DEFAULT;
        private string                          _sEmptyDbSPName                                                 = EMPTYDB_SP_DEFAULT;

        #endregion

        #region Proprietà pubbliche

        public int nSqlCommandTimeoutSeconds
        {
            get
            {
                return _nSqlCommandTimeoutSeconds;
            }
        }

        public bool bOnCheckDB
        {
            get { return _bOnCheckDB; }
        }

        public int nCheckDBRetryNum
        {
            get { return _nCheckDBRetryNum; }
        }

        public int nServiceWaitForStart
        {
            get { return _nServiceWaitForStart; }
        }

        public int nServiceWaitForStop
        {
            get { return _nServiceWaitForStop; }
        }

        public bool bLogOnFileActive
        {
            get { return _bLogOnFileActive; }
        }
        
        public bool bParseServerXmlConfig
        {
            get
            {
                return _bParseServerXmlConfig;
            }
        }

        public bool bStartOnlyNeededSupervisorServices
        {
            get
            {
                return _bStartOnlyNeededSupervisorServices;
            }
        }

        public bool bDownloadRegionListDataAndUpdateDatabaseOnStart
        {
            get
            {
                return _bDownloadRegionListDataAndUpdateDatabaseOnStart;
            }
        }

        public bool bDownloadDeviceTypeListAndUpdateDatabaseOnStart
        {
            get
            {
                return _bDownloadDeviceTypeListAndUpdateDatabaseOnStart;
            }
        }

        public string sNetworkAdapterName1
        {
            get
            { 
                return _sNetworkAdapterName1;
            }
        }

        public HardwareProfile hpHardwareProfile
        {
            get
            {
                return _hpHardwareProfile;
            }
        }

        public EventLogLevel lvEventLogLevel
        {
            get
            {
                return _lvEventLogLevel;
            }
        }

        public string sForcedServerID
        {
            get
            {
                return _sForcedServerID;
            }
        }

        public string sForcedStlcVersion
        {
            get
            {
                return _sForcedStlcVersion;
            }
        }

        public string sPathKeyOnRegistry
        {
            get
            {
                return _sPathKeyOnRegistry;
            }
        }

        public string sStlcVersionKeyName
        {
            get
            {
                return _sStlcVersionKeyName;
            }
        }

        public string sPathXmlConfig
        {
            get
            {
                return _sPathXmlConfig;
            }
        }

        public string sPathLastAutoCheckServicesKey
        {
            get
            {
                return _sPathLastAutoCheckServicesKey;
            }
        }

        public string sLastAutoCheckKeyName
        {
            get
            {
                return _sLastAutoCheckKeyName;
            }
        }

        public string sPathSQLScript
        {
            get
            {
                return _sPathSQLScript;
            }
        }

        public string sPathXmlAction
        {
            get
            {
                return _sPathXmlAction;
            }
        }

        public string sPathLogFolder
        {
            get
            {
                return _sPathLogFolder;
            }
        }

        public string sWorkingDir
        {
            get
            {
                return _sWorkingDir;
            }
        }

        public string sXMLSystem
        {
            get
            {
                return _sXMLSystem;
            }
        }

        public string sEmptyDbSPName
        {
            get
            {
                return _sEmptyDbSPName;
            }
        }

        #endregion

		#region Public Definition

		//==============================================================================
		// Codici dei messaggi dell'EventLog
		//------------------------------------------------------------------------------
		public const int COD_NO_ERROR                                               = 0;

		public const int I_COD_SERVICE_MANAGER_STARTED                              = (Logger.I_GENERIC + 1);
		public const int I_COD_SERVICE_MANAGER_STOPPED                              = (Logger.I_GENERIC + 2);
		public const int I_COD_SERVICE_MANAGER_REBOOT                               = (Logger.I_GENERIC + 3);
		public const int I_COD_SERVICE_MANAGER_POWEROFF                             = (Logger.I_GENERIC + 4);
		public const int I_COD_SERVICE_FORCED_RESTART                               = (Logger.I_GENERIC + 5);
		public const int I_COD_NORMAL_MODE_ACTIVE                                   = (Logger.I_GENERIC + 6);
		public const int I_COD_MAINTENANCE_MODE_ACTIVE                              = (Logger.I_GENERIC + 7);
		public const int I_COD_BEGIN_SYSTEM_CONFIGURATION                           = (Logger.I_GENERIC + 8);
		public const int I_COD_FINISH_SYSTEM_CONFIGURATION                          = (Logger.I_GENERIC + 9);
		public const int I_COD_DISABLE_FBWF                                         = (Logger.I_GENERIC + 10);
		public const int I_COD_ENABLE_FBWF                                          = (Logger.I_GENERIC + 11);
		public const int I_COD_SET_COMPUTER_NAME                                    = (Logger.I_GENERIC + 12);
		public const int I_COD_SET_NETWORK_PORT                                     = (Logger.I_GENERIC + 13);
		public const int I_COD_NORMAL_MODE_PER_TIMEOUT                              = (Logger.I_GENERIC + 14);

		public const int W_COD_SERVICE_MANAGER_STOPPED                              = (Logger.W_GENERIC + 1);
		public const int W_COD_WATCHDOG_ACTIVACTION_SUCCESS                         = (Logger.W_GENERIC + 2);
		public const int W_COD_SYSTEM_REBOOTING                                     = (Logger.W_GENERIC + 3);
		public const int W_XML_CONFIG_VERSION_ERROR                                 = (Logger.W_GENERIC + 4);

		public const int E_COD_SERVICEMANAGER_CRASH                                 = (Logger.E_GENERIC + 1);
		public const int E_COD_SERVICE_STOPPED                                      = (Logger.E_GENERIC + 2);
		public const int E_COD_SERVICE_BLOCKED                                      = (Logger.E_GENERIC + 3);
		public const int E_COD_SERVICE_RESTART_FAILURE                              = (Logger.E_GENERIC + 4);
		public const int E_COD_FAN_THREAD_FAILURE                                   = (Logger.E_GENERIC + 5);
		public const int E_COD_LED_THREAD_FAILURE                                   = (Logger.E_GENERIC + 6);
		public const int E_COD_FBWF_THREAD_FAILURE                                  = (Logger.E_GENERIC + 7);
		public const int E_COD_SERVICES_THREAD_FAILURE                              = (Logger.E_GENERIC + 8);
		public const int E_COD_ENGINE_THREAD_FAILURE                                = (Logger.E_GENERIC + 9);
		public const int E_COD_TEMPERATURE_SENSOR_NOT_AVAILABLE                     = (Logger.E_GENERIC + 10);
		public const int E_COD_SERVICE_MODE_NOT_AVAILABLE                           = (Logger.E_GENERIC + 11);
		public const int E_COD_FBWF_NOT_SUPPORTED                                   = (Logger.E_GENERIC + 12);
		public const int E_COD_READ_SYSTEM_INFO_FAILURE                             = (Logger.E_GENERIC + 13);
		public const int E_COD_READ_NETWORK_INFO_FAILURE                            = (Logger.E_GENERIC + 14);
		public const int E_COD_START_CONFIG_PRC_FAILURE                             = (Logger.E_GENERIC + 15);
		public const int E_COD_RUN_SYSTEM_COMMAND_SUCCESS                           = (Logger.E_GENERIC + 16);
		public const int E_COD_FUNCTION_EXCEPTION                                   = (Logger.E_GENERIC + 17);
		public const int E_COD_INVALID_REGKEY_PATH                                  = (Logger.E_GENERIC + 18);
		public const int E_COD_SERVICE_STOP_FAILURE                                 = (Logger.E_GENERIC + 19);
		public const int E_COD_SERVICE_START_FAILURE                                = (Logger.E_GENERIC + 20);
		public const int E_COD_FBWF_MEM_PERC_OVERFLOW                               = (Logger.E_GENERIC + 21);
        public const int E_COD_FUNCTION_NOTSUPPORTED                                = (Logger.E_GENERIC + 22);

		public const int E_XML_CONFIG_OPEN_SOURCE_CRITICAL_ERROR                    = (Logger.E_GENERIC + 100);
		public const int E_XML_CONFIG_TELEFIN_NODE_ERROR                            = (Logger.E_GENERIC + 101);
		public const int E_XML_CONFIG_MANAGER_NODE_ERROR                            = (Logger.E_GENERIC + 102);
		public const int E_XML_CONFIG_WATCHDOG_NODE_ERROR                           = (Logger.E_GENERIC + 103);
		public const int E_XML_CONFIG_FAN_NODE_ERROR                                = (Logger.E_GENERIC + 104);
		public const int E_XML_CONFIG_SERVICE_NODE_ERROR                            = (Logger.E_GENERIC + 105);
		public const int E_XML_CONFIG_FBWF_NODE_ERROR                               = (Logger.E_GENERIC + 106);
		public const int E_XML_CONFIG_NETWORK_NODE_ERROR                            = (Logger.E_GENERIC + 107);
		public const int E_XML_CONFIG_SYSTEM_LED_NODE_ERROR                         = (Logger.E_GENERIC + 108);

        public const int I_SYSTEM_XML_NO_ERROR                                      = (Logger.I_GENERIC + 200);
        public const int I_SYSTEM_XML_SUPERVISOR_NOT_NEEDED_IN_MONITORING           = (Logger.I_GENERIC + 201);

        public const int W_SYSTEM_XML_NO_SERVICES_CONFIGURED                        = (Logger.W_GENERIC + 200);

        public const int E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_DATABASE                = (Logger.E_GENERIC + 200);
        public const int E_SYSTEM_XML_FUNCTION_NO_ACCESS_TO_CONFIG_XML              = (Logger.E_GENERIC + 201);
        public const int E_SYSTEM_XML_FUNCTION_DATABASE_UNEXPECTED_DATA_ERROR       = (Logger.E_GENERIC + 202);
        public const int E_SYSTEM_XML_FUNCTION_NO_DATA_IN_CONFIG_XML                = (Logger.E_GENERIC + 203);
        public const int E_SYSTEM_XML_FUNCTION_EXCEPTION_DURING_DATABASE_OPERATION  = (Logger.E_GENERIC + 204);

		#endregion

		#region Creazione Singleton per AppConst

        private static AppCfg _instance;

        /// <summary>
        /// Recupero dell'istanza singleton della classe
        /// </summary>
        public static AppCfg Default
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AppCfg();
                }

                return _instance;
            }
        }

        protected AppCfg()
        {
        }

        public void LoadConfig()
        {
            try
            {
            this.InitWorkingDir();
  
            this._section       = AppCfgUtility.Section.GetSection();
            this._regManager    = new AppCfgUtility.ModifyRegistry(this._section.RegistryKey);

                this.LoadAllCfg();  // Inizializzazione variabili di configurazione globali/locali
            }       
            catch (Exception ex)
            {
                Logger.LogErrorToEventLog("LoadConfig: " + ex.ToString());
            }
        }


        /// <summary>
        /// Inizializzazione variabili del registro di sistema (se richiesto)
        /// </summary>
		public void UpdateRegistry()
		{
			try
			{
                this.UpdRegistryGlobalSettings();
                this.UpdRegistryLocalSettings();
            }
			catch (Exception ex)
			{
				Logger.LogErrorToEventLog("UpdateRegistry: " + ex.ToString());
			}
		}

        /// <summary>
        /// Recupera la directory di installazione del servizio
        /// </summary>
        private void InitWorkingDir()
        {
            try
            {
                this._sWorkingDir       = PATH_WORKING_DIR_DEFAULT;

                System.IO.FileInfo ff   = new System.IO.FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location);
                this._sWorkingDir       = ff.Directory.ToString();
            }
            catch { }
        }

        /// <summary>
        /// Logga su un file di testo nella stessa cartella dell'eseguibile un'eventuale eccezione
        /// rilevata nella lettura del file .config
        /// </summary>
        /// <param name="sFunction"></param>
        /// <param name="sLog"></param>
        private void LogCfgException(string sFunction, string sLog)
        {
            try
            {
                string sFileException = String.Format("{0}\\ConfigException.log", _sWorkingDir);

                using (System.IO.StreamWriter sw = System.IO.File.AppendText(sFileException))
                {
                    sw.WriteLine(String.Format("{0} - [ {1} ] {2}", DateTime.Now, sFunction, sLog));
                    sw.Close    ();
                }
            }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal EventLogLevel GetEventLogLevel()
        {
            _sEventLogLevel = _sEventLogLevel.ToLowerInvariant();

            if(String.IsNullOrEmpty(_sEventLogLevel))
            {
                return EventLogLevel.ERROR;
            }

            switch(_sEventLogLevel)
            {
                case "info":
                case "information":
                    return EventLogLevel.INFO;
                case "warn":
                case "warning":
                    return EventLogLevel.WARNING;
                default:
                    return EventLogLevel.ERROR;
            }
        }

        /// <summary>
        /// Trasforma il profilo hardware cericato dal file di configurazione nel 
        /// corrispondente valore dell'enumeratico
        /// La stringa letta dal file .config è analizzata ignorando la differenza tra
        /// maiuscole e minuscole
        /// </summary>
        /// <returns>valore dell'enumeratico caricato in RAM dalla classe singleton</returns>
		internal HardwareProfile GetHWProfileFromConfig()
		{
            _sHardwareProfile = _sHardwareProfile.ToLowerInvariant();

            if (String.IsNullOrEmpty(_sHardwareProfile)) 
			{
				return HardwareProfile.STLC;
			}

            // Dato che può essere associato al profilo hardware anche un sottotipo, dopo il punto, eliminiamo la parte rimanente, se esiste
            // Ad esempio:
            // 100332068.01 > computer industriale di Almaviva senza porte seriali
            // 100332068.02 > computer industriale di Almaviva con porte seriali

            if (_sHardwareProfile.IndexOf('.') > 0)
		    {
                _sHardwareProfile = _sHardwareProfile.Substring(0, _sHardwareProfile.IndexOf('.'));
		    }

            switch (_sHardwareProfile)
			{
				case "stlc":
					return HardwareProfile.STLC;
				case "generic":
					return HardwareProfile.Generic;
				case "100332068":
					return HardwareProfile.Almaviva;
				case "100332045":
					return HardwareProfile.ASTS;
				default:
					return HardwareProfile.Generic;
			}
		}

		#endregion

        #region Funzioni per la gestione del registro

        /// <summary>
        /// Aggiorno (se necessario) i valori delle configurazioni (specifiche del STLCManager) sul registro in base a quelle
        /// presenti nel file di configurazione
        /// </summary>
        private List<string> UpdRegistryLocalSettings()
        {
            List<string> list = new List<string>();

            try
            {
                //Logger.Default.Log("Aggiornamento dei LocalSettings nel Registry. ", Logger.LogType.CONSOLE_LOC_FILE);

                for (int j = 0; j < this._section.LocalSettings.Count; j++)
                {
                    if (!this._section.LocalSettings[j].CreateOnRegistry)
                        continue;

                    if ((this._regManager.ExistValue(this._section.LocalSettings[j].Name)) &&
                        (this._regManager.Read(this._section.LocalSettings[j].Name) == this._section.LocalSettings[j].Value))
                        continue;

                    if(this._regManager.Write(this._section.LocalSettings[j].Name, this._section.LocalSettings[j].Value))
                    {
                        list.Add(String.Format("{0, -50} = \"{1}\"", this._section.LocalSettings[j].Name, this._section.LocalSettings[j].Value));
                        Logger.Default.Log(String.Format("    - key={0}, Value={1}", this._section.LocalSettings[j].Name, this._section.LocalSettings[j].Value), Logger.LogType.CONSOLE_LOC_FILE);
                    }
                }
            }
            catch (Exception e)
            {
                LogCfgException(String.Format("UpdRegistryLocalSettings Exception -"), e.Message);
            }

            return list;
        }

        /// <summary>
        /// Aggiorno (se necessario) i valori delle configurazioni (comuni agli altri servizi della suite) sul registro in base a quelle
        /// presenti nel file di configurazione
        /// </summary>
        private List<string> UpdRegistryGlobalSettings()
        {
            List<string> list = new List<string>();
            
            try
            {
                Logger.Default.Log("Aggiornamento dei GlobalSettings nel Registry. ", Logger.LogType.CONSOLE_LOC_FILE);

                for (int j = 0; j < this._section.GlobalSettings.Count; j++)
                {
                    if (!this._section.GlobalSettings[j].CreateOnRegistry)
                        continue;

                    if (this._regManager.ExistValue(this._section.GlobalSettings[j].Name) &&
                        (this._regManager.Read(this._section.GlobalSettings[j].Name) == this._section.GlobalSettings[j].Value))
                        continue;

                    if(this._regManager.Write(this._section.GlobalSettings[j].Name, this._section.GlobalSettings[j].Value))
                    {
                        list.Add(String.Format("{0, -50} = \"{1}\"", this._section.GlobalSettings[j].Name, this._section.GlobalSettings[j].Value));
                        Logger.Default.Log(String.Format("    - key={0}, Value={1}", this._section.GlobalSettings[j].Name, this._section.GlobalSettings[j].Value), Logger.LogType.CONSOLE_LOC_FILE);
                    }
                }
            }
            catch (Exception e)
            {
                LogCfgException(String.Format("UpdRegistryGlobalSettings Exception -"), e.Message);
            }

            return list;
        }

        /// <summary>
        /// Crea sul registro le chiavi del file .config con l'attributo createOnRegistry="True"
        /// all'interno del percorso definito dall'attributo registryKey
        /// </summary>
        /// <returns>Lista delle chiavi effettivamente create</returns>
        public List<string> CreateRegitrySettings()
        {
            List<string>    listCreatedKeys = new List<string>();

            AppCfgUtility.Section.RefreshSection();                 // | Rilettura del file di configurazione
            this._section = AppCfgUtility.Section.GetSection();     // |

            listCreatedKeys.AddRange(UpdRegistryLocalSettings());
            listCreatedKeys.AddRange(UpdRegistryGlobalSettings());

            return          listCreatedKeys;
        }

        /// <summary>
        /// Elimina dal registro tutte le chiavi presenti all'interno del percorso
        /// definito dall'attributo registryKey del file .config
        /// </summary>
        /// <returns>Lista delle chiavi effettivamente eliminate</returns>
        public List<string> DeleteRegistrySettings(bool bDeleteOnlyConfig)
        {
            List<string> listDeletedKeys = new List<string>();

            foreach(string name in this._regManager.Values())
            {
                if (bDeleteOnlyConfig && this._section.GetElement(name) == null)  // Non bisogna cancellare le chiavi del registro che non sono presenti nel file .config
                    continue;

                if(this._regManager.DeleteValue(name))
                {
                    listDeletedKeys.Add(name);
                }
            }

            return listDeletedKeys;
        }

        /// <summary>
        /// Caricamento all'interno della classe singleton di tutti i parametri di configurazione necessari
        /// </summary>
        private bool LoadAllCfg()
        {
            this._sPathKeyOnRegistry                                = GetKeyName         ();

            // Global Settings
            this._sPathLogFolder        = GetCfgValue<string>(this._sPathKeyOnRegistry, PATH_LOG_FOLDER_NAME, PATH_LOG_FOLDER_DEFAULT);
            this._sXMLSystem            = GetCfgValue<string>(this._sPathKeyOnRegistry, XML_SYSTEM_NAME, XML_SYSTEM_DEFAULT);
            this._sHardwareProfile      = GetCfgValue<string>(this._sPathKeyOnRegistry, HARDWARE_PROFILE_NAME, HARDWARE_PROFILE_STRING_DEFAULT);
            this._sForcedServerID       = GetCfgValue<string>(this._sPathKeyOnRegistry, FORCED_SERVER_ID_NAME, FORCED_SERVER_ID_DEFAULT);
            this._sEmptyDbSPName        = GetCfgValue<string>(this._sPathKeyOnRegistry, EMPTYDB_SP_NAME, EMPTYDB_SP_DEFAULT);

            // Local Settings
            this._sEventLogLevel                                    = GetCfgValue<string>(this._sPathKeyOnRegistry, EVENT_LOG_LEVEL_NAME, EVENT_LOG_LEVEL_STRING_DEFAULT);
            this._bLogOnFileActive                                  = GetCfgValue<bool>  (this._sPathKeyOnRegistry, LOG_ON_FILE_ACTIVE_NAME, LOG_ON_FILE_ACTIVE_DEFAULT);
            this._nSqlCommandTimeoutSeconds                         = GetCfgValue<int>   (this._sPathKeyOnRegistry, SQL_COMMAND_TIMEOUT_SECONDS_NAME, SQL_COMMAND_TIMEOUT_SECONDS_DEFAULT);
            this._bOnCheckDB                                        = GetCfgValue<bool>  (this._sPathKeyOnRegistry, ON_CHECK_DB_NAME, ON_CHECK_DB_DEFAULT);
            this._nCheckDBRetryNum                                  = GetCfgValue<int>   (this._sPathKeyOnRegistry, CHECK_DB_RETRY_NUM_NAME, CHECK_DB_RETRY_NUM_DEFAULT);
            this._nServiceWaitForStart                              = GetCfgValue<int>   (this._sPathKeyOnRegistry, SERVICE_WAITING_FOR_START_NAME, SERVICE_WAITING_FOR_START_DEFAULT);
            this._nServiceWaitForStop                               = GetCfgValue<int>   (this._sPathKeyOnRegistry, SERVICE_WAITING_FOR_STOP_NAME, SERVICE_WAITING_FOR_STOP_DEFAULT);
            this._bParseServerXmlConfig                             = GetCfgValue<bool>  (this._sPathKeyOnRegistry, PARSE_SERVER_XML_CONFIG_NAME, PARSE_SERVER_XML_CONFIG_DEFAULT);
            this._bStartOnlyNeededSupervisorServices                = GetCfgValue<bool>  (this._sPathKeyOnRegistry, START_ONLY_NEEDED_SUPERVISOR_SERVICES_NAME, START_ONLY_NEEDED_SUPERVISOR_SERVICES_DEFAULT);
            this._bDownloadRegionListDataAndUpdateDatabaseOnStart   = GetCfgValue<bool>  (this._sPathKeyOnRegistry, DOWNLOAD_REGION_LIST_DATA_AND_UPDATE_DATABASE_ON_START_NAME, DOWNLOAD_REGION_LIST_DATA_AND_UPDATE_DATABASE_ON_START_DEFAULT);
            this._bDownloadDeviceTypeListAndUpdateDatabaseOnStart   = GetCfgValue<bool>  (this._sPathKeyOnRegistry, DOWNLOAD_DEVICE_TYPE_LIST_AND_UPDATE_DATABASE_ON_START_NAME, DOWNLOAD_DEVICE_TYPE_LIST_AND_UPDATE_DATABASE_ON_START_DEFAULT);
            this._sNetworkAdapterName1                              = GetCfgValue<string>(this._sPathKeyOnRegistry, NETWORK_ADAPTER_NAME_1_NAME, NETWORK_ADAPTER_NAME_1_DEFAULT);
            this._sForcedStlcVersion                                = GetCfgValue<string>(this._sPathKeyOnRegistry, FORCED_STLC_VERSION_NAME, FORCED_STLC_VERSION_DEFAULT);
            this._sStlcVersionKeyName                               = GetCfgValue<string>(this._sPathKeyOnRegistry, STLC_VERSION_KEY_NAME, STLC_VERSION_KEY_NAME_DEFAULT);
            this._sPathXmlConfig                                    = GetCfgValue<string>(this._sPathKeyOnRegistry, PATH_XML_CONFIG_NAME, PATH_XML_CONFIG_DEFAULT);
            this._sPathLastAutoCheckServicesKey                     = GetCfgValue<string>(this._sPathKeyOnRegistry, PATH_LAST_AUTO_CHECK_SERVICES_KEY_NAME, PATH_LAST_AUTO_CHECK_SERVICES_KEY_DEFAULT);
            this._sLastAutoCheckKeyName                             = GetCfgValue<string>(this._sPathKeyOnRegistry, LAST_AUTO_CHECK_KEY_NAME, LAST_AUTO_CHECK_KEY_NAME_DEFAULT);
            this._sPathSQLScript                                    = GetCfgValue<string>(this._sPathKeyOnRegistry, PATH_SQL_SCRIPT_NAME, PATH_SQL_SCRIPT_DEFAULT);
            this._sPathXmlAction                                    = GetCfgValue<string>(this._sPathKeyOnRegistry, PATH_XML_ACTION_NAME, PATH_XML_ACTION_DEFAULT);

            this._hpHardwareProfile                                 = GetHWProfileFromConfig();

            this._lvEventLogLevel                                   = GetEventLogLevel();

            return true;
        }

        /// <summary>
        /// Recupera il valore di un parametro di configurazione secondo il seguente ordine di priorità:
        /// 1° ritorna il valore definito in LocalSettings -> STLCManagerSetting
        /// 2° se la ricerca al punto precedente non ha avuto esito, ritorna il valore definito in GlobalSettings -> STLCManagerSetting
        /// 3° se la ricerca al punto precedente non ha avuto esito, ritorna il valore definito nel registro
        /// 4° se la ricerca al punto precedente non ha avuto esito, ritorna il valore di default
        /// In caso di eccezione nella funzione, ritorna cmq il valore di default
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="valueName"></param>
        /// <param name="defaultValue"></param>
        /// <returns>valore del parametro di configurazione in formato stringa</returns>
        private T GetCfgValue<T>(string keyName, string valueName, object defaultValue)
        {
            T sRet = (T)Convert.ChangeType(defaultValue, typeof(T));

            try
            {
                AppCfgUtility.Element elementCfg = this._section.GetElement(valueName);

                if (elementCfg == null)
                {
                    object o = Registry.GetValue(keyName, valueName, null);

                    if (o != null)
                    {
                        sRet = (T)Convert.ChangeType(o, typeof(T));
                    }

                    return sRet;
                }

                sRet = (T)Convert.ChangeType(elementCfg.Value, typeof(T));
            }
            catch (Exception e) { LogCfgException(String.Format("GetCfgValue({0})", valueName), e.Message); }

            return sRet;
        }

        /// <summary>
        /// Ritorna la chiave di registro configurata nel file .config dove vengono salvate le chiavi del registro
        /// </summary>
        /// <returns></returns>
        private string GetKeyName()
        {
            string sRet = PATH_KEY_ON_REGISTRY_DEFAULT; // SERVICE_KEY;

            try
            {
                sRet = this._section.RegistryKey;
            }
            catch (Exception e) { LogCfgException("GetKeyName", e.Message); }

            return sRet;
        }

        /// <summary>
        /// Logga a console i parametri della configurazione che verranno utilizzati nell'esecuzione di STLCManager
        /// </summary>
        public void LogAllCfg()
        {
            Logger.Default.Log("Configurazione caricata:",  Logger.LogType.CONSOLE);
/*
            Logger.Default.Log(String.Format("{0}: {1}",    "registryKey",                                                  this.GetKeyName()),                                     Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", PATH_LOG_FOLDER_NAME, this._sPathLogFolder), Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", XML_SYSTEM_NAME, this._sXMLSystem), Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", HARDWARE_PROFILE_NAME, this._sHardwareProfile), Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", FORCED_SERVER_ID_NAME, this._sForcedServerID), Logger.LogType.CONSOLE);
*/
            Logger.Default.Log(String.Format("{0}: {1}", EVENT_LOG_LEVEL_NAME,                                          this._lvEventLogLevel),                                 Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", LOG_ON_FILE_ACTIVE_NAME,                                       this._bLogOnFileActive),                                Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", SQL_COMMAND_TIMEOUT_SECONDS_NAME,                              this._nSqlCommandTimeoutSeconds),                       Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", ON_CHECK_DB_NAME,                                              this._bOnCheckDB),                                      Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", CHECK_DB_RETRY_NUM_NAME,                                       this._nCheckDBRetryNum),                                Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", SERVICE_WAITING_FOR_START_NAME,                                this._nServiceWaitForStart),                            Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", SERVICE_WAITING_FOR_STOP_NAME,                                 this._nServiceWaitForStop),                             Logger.LogType.CONSOLE);
            Logger.Default.Log(String.Format("{0}: {1}", PARSE_SERVER_XML_CONFIG_NAME,                                  this._bParseServerXmlConfig),                           Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", START_ONLY_NEEDED_SUPERVISOR_SERVICES_NAME,                    this._bStartOnlyNeededSupervisorServices),              Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", DOWNLOAD_REGION_LIST_DATA_AND_UPDATE_DATABASE_ON_START_NAME,   this._bDownloadRegionListDataAndUpdateDatabaseOnStart), Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", DOWNLOAD_DEVICE_TYPE_LIST_AND_UPDATE_DATABASE_ON_START_NAME,   this._bDownloadDeviceTypeListAndUpdateDatabaseOnStart), Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", NETWORK_ADAPTER_NAME_1_NAME,                                   this._sNetworkAdapterName1),                            Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", FORCED_STLC_VERSION_NAME,                                      this._sForcedStlcVersion),                              Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", STLC_VERSION_KEY_NAME,                                         this._sStlcVersionKeyName),                             Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", PATH_XML_CONFIG_NAME,                                          this._sPathXmlConfig),                                  Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", PATH_LAST_AUTO_CHECK_SERVICES_KEY_NAME,                        this._sPathLastAutoCheckServicesKey),                   Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", LAST_AUTO_CHECK_KEY_NAME,                                      this._sLastAutoCheckKeyName),                           Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", PATH_SQL_SCRIPT_NAME,                                          this._sPathSQLScript),                                  Logger.LogType.CONSOLE); 
            Logger.Default.Log(String.Format("{0}: {1}", PATH_XML_ACTION_NAME,                                          this._sPathXmlAction),                                  Logger.LogType.CONSOLE); 
        }

        /// <summary>
        /// Logga su file i parametri della configurazione che verranno utilizzati nell'esecuzione di STLCManager
        /// </summary>
        public void OnFileLogAllCfg()
        {
            Logger.Default.Log("Configurazione caricata:", Logger.LogType.CONSOLE_LOC_FILE);
/*
            Logger.Default.Log(String.Format("  {0}: {1}", "registryKey", this.GetKeyName()), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", PATH_LOG_FOLDER_NAME, this._sPathLogFolder), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", XML_SYSTEM_NAME, this._sXMLSystem), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", HARDWARE_PROFILE_NAME, this._sHardwareProfile), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", FORCED_SERVER_ID_NAME, this._sForcedServerID), Logger.LogType.CONSOLE_LOC_FILE);
*/
            Logger.Default.Log(String.Format("  {0}: {1}", EVENT_LOG_LEVEL_NAME, this._lvEventLogLevel), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", LOG_ON_FILE_ACTIVE_NAME, this._bLogOnFileActive), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", SQL_COMMAND_TIMEOUT_SECONDS_NAME, this._nSqlCommandTimeoutSeconds), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", ON_CHECK_DB_NAME, this._bOnCheckDB), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", CHECK_DB_RETRY_NUM_NAME, this._nCheckDBRetryNum), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", SERVICE_WAITING_FOR_START_NAME, this._nServiceWaitForStart), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", SERVICE_WAITING_FOR_STOP_NAME, this._nServiceWaitForStop), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", PARSE_SERVER_XML_CONFIG_NAME, this._bParseServerXmlConfig), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", START_ONLY_NEEDED_SUPERVISOR_SERVICES_NAME, this._bStartOnlyNeededSupervisorServices), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", DOWNLOAD_REGION_LIST_DATA_AND_UPDATE_DATABASE_ON_START_NAME, this._bDownloadRegionListDataAndUpdateDatabaseOnStart), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", DOWNLOAD_DEVICE_TYPE_LIST_AND_UPDATE_DATABASE_ON_START_NAME, this._bDownloadDeviceTypeListAndUpdateDatabaseOnStart), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", NETWORK_ADAPTER_NAME_1_NAME, this._sNetworkAdapterName1), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", FORCED_STLC_VERSION_NAME, this._sForcedStlcVersion), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", STLC_VERSION_KEY_NAME, this._sStlcVersionKeyName), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", PATH_XML_CONFIG_NAME, this._sPathXmlConfig), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", PATH_LAST_AUTO_CHECK_SERVICES_KEY_NAME, this._sPathLastAutoCheckServicesKey), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", LAST_AUTO_CHECK_KEY_NAME, this._sLastAutoCheckKeyName), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", PATH_SQL_SCRIPT_NAME, this._sPathSQLScript), Logger.LogType.CONSOLE_LOC_FILE);
            Logger.Default.Log(String.Format("  {0}: {1}", PATH_XML_ACTION_NAME, this._sPathXmlAction), Logger.LogType.CONSOLE_LOC_FILE);
        }

        #endregion
    }

    /// <summary>
    /// Indica il tipo di macchina su cui si sta eseguendo l'applicazione
    /// </summary>
	public enum HardwareProfile : short
	{
        /// <summary>
        /// Apparato STLC1000
        /// </summary>
		STLC,
        /// <summary>
        /// Diagnostica standalone, che non centralizza in Gris
        /// </summary>
		Generic,
        /// <summary>
        /// Apparati di Almaviva che centralizzano in Gris
        /// </summary>
        Almaviva,
        /// <summary>
        /// Apparati di ASTS (Ansaldo) che centralizzano in Gris
        /// </summary>
        ASTS
	}

    public enum EventLogLevel
    {
        INFO,
        WARNING,
        ERROR
    }
}
@echo off
sc stop "STLCManagerService"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc delete "STLCManagerService"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc create "STLCManagerService" binPath= "C:\Telefin\STLCManager\STLCManagerService.exe" start= auto DisplayName= "STLCManagerService"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL
sc description "STLCManagerService" "STLC Manager Service"

sc start "STLCManagerService"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

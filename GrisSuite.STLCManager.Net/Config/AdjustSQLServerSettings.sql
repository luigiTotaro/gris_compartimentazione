USE [master]

DECLARE @NewServerName varchar(100), @OldServerName varchar(100)

SELECT @OldServerName = CONVERT(varchar(100), @@SERVERNAME)
SELECT @NewServerName = CONVERT(varchar(100), '$(NEWSERVERNAME)')


/*****************************************/
/*          Adjust ServerName            */
/*****************************************/
-- Drop the server with incorrect name
EXEC sp_dropserver @server=@OldServerName
IF @@ERROR <> 0 PRINT 'Errore in sp_dropserver ' + @OldServerName + ' Error code: ' + @@ERROR

-- Add the correct server as a local server
EXEC sp_addserver @server=@NewServerName, @local='local'
IF @@ERROR <> 0 PRINT 'Errore in sp_addserver ' + @NewServerName + ' Error code: ' + @@ERROR

/*****************************************/
/*           Adjust Security             */
/*****************************************/
-- Drop the current Logins
DECLARE @gruppo varchar(100), @SQLString nvarchar(200)

SELECT @gruppo = (SELECT name FROM sys.server_principals WHERE name like '%\STLC1000SWMAdmins')
IF (@gruppo != '')
 BEGIN
 SET @SQLString = 'DROP LOGIN [' + @gruppo + ']'
 EXEC sp_executesql @SQLString
 SET @gruppo = ''
 END

SELECT @gruppo = (SELECT name FROM sys.server_principals WHERE name like '%\STLC1000SWMUsers')
IF (@gruppo != '')
 BEGIN
 SET @SQLString = 'DROP LOGIN [' + @gruppo + ']'
 EXEC sp_executesql @SQLString
 SET @gruppo = ''
 END

SELECT @gruppo = (SELECT name FROM sys.server_principals WHERE name like '%\SQLServer2005MSSQLUser%')
IF (@gruppo != '')
 BEGIN
 SET @SQLString = 'DROP LOGIN [' + @gruppo + ']'
 EXEC sp_executesql @SQLString
 END

-- Add the correct logins
DECLARE @SName varchar(100)
select @SName = CONVERT(varchar(100), SERVERPROPERTY('ServerName'))
select @SName = substring(@SName,0,charindex('\',@SName))

SET @SQLString = 'CREATE LOGIN [' + @SName + '\SQLServer2005MSSQLUser$STLC$SQLEXPRESS] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
EXEC sp_executesql @SQLString
IF @@ERROR <> 0 PRINT 'Errore in [' + @SQLString + '] Error code: ' + @@ERROR

SET @SQLString = 'CREATE LOGIN [' + @SName + '\STLC1000SWMAdmins] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
EXEC sp_executesql @SQLString
IF @@ERROR <> 0 PRINT 'Errore in [' + @SQLString + '] Error code: ' + @@ERROR

SET @SQLString = 'CREATE LOGIN [' + @SName + '\STLC1000SWMUsers] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
EXEC sp_executesql @SQLString
IF @@ERROR <> 0 PRINT 'Errore in [' + @SQLString + '] Error code: ' + @@ERROR


-- Set the default serverrole for SQLServer2005... group
SET @SQLString = 'EXEC sp_addsrvrolemember @loginame = N''' + @SName + '\SQLServer2005MSSQLUser$STLC$SQLEXPRESS'', @rolename = N''sysadmin'''
EXEC sp_executesql @SQLString
IF @@ERROR <> 0 PRINT 'Errore in [' + @SQLString + '] Error code: ' + @@ERROR


/*************************************** Telefin DB Users Creation and Permissions ******************************************/
USE [Telefin]
GO

/****** Drop Users ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'SWEBAdmins')
	DROP USER [SWEBAdmins]
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'SWEBUsers')
	DROP USER [SWEBUsers]
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'STLCAppServices')
	DROP USER [STLCAppServices]

DECLARE @SrvName varchar(100)
select @SrvName = CONVERT(varchar(100), SERVERPROPERTY('ServerName'))
select @SrvName = substring(@SrvName, 0, charindex('\', @SrvName))

DECLARE @scriptAdmins NVARCHAR(100)
DECLARE @scriptUsers NVARCHAR(100)
DECLARE @scriptAppServ NVARCHAR(100)

SET @scriptAdmins = 'CREATE USER [SWEBAdmins] FOR LOGIN [' + @SrvName + '\STLC1000SWMAdmins]'
SET @scriptUsers = 'CREATE USER [SWEBUsers] FOR LOGIN [' + @SrvName + '\STLC1000SWMUsers]'
SET @scriptAppServ = 'CREATE USER [STLCAppServices] FOR LOGIN [' + @SrvName + '\STLC1000AppServices]'

/****** Object:  User [SWEBUsers]    Script Date: 01/19/2008 11:39:41 ******/
EXEC sp_executesql @scriptUsers
/****** Object:  User [SWEBAdmins]    Script Date: 01/19/2008 11:39:41 ******/
EXEC sp_executesql @scriptAdmins
/****** Object:  User [STLCAppServices]    Script Date: 01/19/2008 11:39:41 ******/
EXEC sp_executesql @scriptAppServ
GO

GRANT CONNECT TO [STLCAppServices]
GO
GRANT EXECUTE TO [STLCAppServices]
GO
GRANT CONNECT TO [SWEBAdmins]
GO
GRANT EXECUTE TO [SWEBAdmins]
GO
GRANT CONNECT TO [SWEBUsers]
GO
GRANT EXECUTE TO [SWEBUsers]
GO

EXEC sp_addrolemember N'db_datawriter', N'STLCAppServices'
GO
EXEC sp_addrolemember N'db_datareader', N'STLCAppServices'
GO
EXEC sp_addrolemember N'db_datawriter', N'SWEBAdmins'
GO
EXEC sp_addrolemember N'db_datareader', N'SWEBAdmins'
GO
EXEC sp_addrolemember N'db_datareader', N'SWEBUsers'
GO
/*************************************** END Users Creation and Permissions ******************************************/



IF @@ERROR = 0 PRINT 'SQLEXPRESS is successfully adjusted!'

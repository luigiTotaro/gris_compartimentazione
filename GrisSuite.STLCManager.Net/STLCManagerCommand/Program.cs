﻿using System;
using System.Collections.Generic;
using System.Net;
using STLCManager.Command.Functions;
using STLCManager.Command.STLCManagerService;

namespace STLCManager.Command
{
    /// <summary>
    ///     Classe di gestione comandi a STLCManagerService da console.
    /// </summary>
    internal class Program
    {
        /// <summary>
        ///     Entry point applicazione
        /// </summary>
        /// <param name="args">parametri su riga di comando</param>
        private static void Main(string[] args)
        {
            try
            {
                SelectCmd command = new SelectCmd();
                List<string> parameters;
                SelectCmd.Code cmdCode = command.DecodeCommand(args, out parameters);

                switch (cmdCode)
                {
                    case SelectCmd.Code.Help:
                        PrintUsageInfo();
                        break;

                    case SelectCmd.Code.Info:
                        if (!Info(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.Status:
                        if (!Status(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.Network:
                        if (!Network(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.SetMode:
                        if (!SetMode(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.GetServiceStatus:
                        if (!GetServiceStatus(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.StartService:
                        if (!SetServiceCommand(STLCServiceCommand.START, parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.StopService:
                        if (!SetServiceCommand(STLCServiceCommand.STOP, parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.RestartService:
                        if (!SetServiceCommand(STLCServiceCommand.RESTART, parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.Configuration:
                        if (!Configuration(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.SystemReboot:
                        if (!Shutdown(STLCShutdownType.REBOOT, parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.SystemPowerOff:
                        if (!Shutdown(STLCShutdownType.POWER_OFF, parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.ReloadConfig:
                        if (!ReloadConfig(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.ReloadSystemXmlIntoDatabase:
                        if (!ReloadSystemXmlIntoDatabase(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;
                    case SelectCmd.Code.UpdateRegionListFromWS:
                        if (!UpdateRegionListFromWS(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;
                    case SelectCmd.Code.UpdateDeviceTypeListFromWS:
                        if (!UpdateDeviceTypeListFromWS(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;
                    case SelectCmd.Code.GetNeededSupervisors:
                        if (!GetNeededSupervisors(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;
                    case SelectCmd.Code.SetConfigOnRegistry:
                        if(!SetConfigOnRegistry(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;
                    case SelectCmd.Code.EmptyDatabase:
                        if (!EmptyDatabase(parameters))
                        {
                            PrintUnknownCommand();
                        }
                        break;

                    case SelectCmd.Code.CommandUnknown:
                    default:
                        PrintUnknownCommand();
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ECCEZIONE: " + ex.Message + " - " + ex.StackTrace);
            }
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta informazioni
        /// </summary>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool Info(List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCInfoRequest Request = new STLCInfoRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCInfoResult Result = CommandServiceClient.getSTLCInfo(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");

                Console.WriteLine("Serial Number   : " + Result.data.serialNumber + " (" + ParseSerialNumber(Result.data.serialNumber) + ")");
                Console.WriteLine("Version         : " + Result.data.version);
                Console.WriteLine("Hostname        : " + Result.data.hostName);
                Console.WriteLine("Hardware Profile: " + Result.data.hardwareProfile);
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Analizza numero di serie e lo restituisce come intero.
        /// </summary>
        /// <param name="sSN">numero di serie in formato YY.DD.NNN</param>
        /// <returns>numero di serie in formato numerico, 0 in presenza di errori</returns>
        private static uint ParseSerialNumber(string sSN)
        {
            try
            {
                string[] sSplit = sSN.Split('.');

                uint iYear = uint.Parse(sSplit[0]);
                uint iDay = uint.Parse(sSplit[1]);
                uint iProg = uint.Parse(sSplit[2]);

                return (iYear << 24) | (iDay << 16) | iProg;
            }
            catch
            {
            }

            return 0;
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta stato
        /// </summary>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool Status(List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCStatusRequest Request = new STLCStatusRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCStatusResult Result = CommandServiceClient.getSTLCStatus(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Mode       : " + Result.data.mode);
                Console.WriteLine("Fbwf       : " + String.Format(Result.data.filterFBWFEnabled ? "Enabled" : "Disabled"));
                Console.WriteLine("Temperature: " + Result.data.temperature + " °C");
                Console.WriteLine("  Sensor type: " + Result.data.sensorTemperature);
                Console.WriteLine("  Fan pwm    : dx " + Result.data.pwmRightFan + "% sx " + Result.data.pwmLeftFan + "%");
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta configurazione porte di rete
        /// </summary>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item>true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool Network(List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCNetworkRequest Request = new STLCNetworkRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCNetworkResult Result = CommandServiceClient.getSTLCNetwork(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                foreach (STLCNetwork network in Result.data.Network)
                {
                    Console.WriteLine("");

                    if (network.IPEnabled)
                    {
                        Console.WriteLine(network.Name + " : Enabled");
                        Console.WriteLine("  DHCP   : " + String.Format(network.DHCPEnabled ? "Enabled" : "Disabled"));

                        Console.WriteLine("  IP     : " + FormatIPAddresses(network.IPAddresses));
                        Console.WriteLine("  Subnet : " + FormatIPAddresses(network.SubnetMasks));
                        Console.WriteLine("  Gateway: " + FormatIPAddresses(network.IPGateways));
                        Console.WriteLine("  DNS    : " + FormatIPAddresses(network.DNSServers));
                    }
                    else
                    {
                        Console.WriteLine(network.Name + " : Disabled");
                    }
                }
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Formattazione lista di IPAddress in stringa (1.1.1.1, 2.2.2.2, ... n.n.n.n)
        /// </summary>
        /// <param name="ipArray">lista IPAddress</param>
        /// <returns>stringa ottenuta dalla formatazione</returns>
        private static string FormatIPAddresses(IPAddress[] ipArray)
        {
            string str = null;
            foreach (IPAddress ip in ipArray)
            {
                if (str == null)
                {
                    str = ip.ToString();
                }
                else
                {
                    str += ", " + ip;
                }
            }
            return str;
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta impostazione modalità di funzionamento
        /// </summary>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool SetMode(List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCModeRequest Request = new STLCModeRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCModeResult Result = new STLCModeResult();

            if (parameters.Count <= 0)
            {
                return false;
            }

            switch (parameters[0])
            {
                case ":n":
                    Request.mode = STLCMode.NORMAL;
                    break;

                case ":m":
                    Request.mode = STLCMode.MAINTENANCE;
                    Request.managedTimeoutHours = 0;

                    if (parameters.Count == 2)
                    {
                        uint uTimeout;
                        if (uint.TryParse(parameters[1], out uTimeout))
                        {
                            Request.managedTimeoutHours = uTimeout == 0 ? -1 : (int) uTimeout;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    break;

                default:
                    return false;
            }

            Result = CommandServiceClient.setSTLCMode(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Mode: " + Result.data.mode);
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta stato servizi
        /// </summary>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool GetServiceStatus(List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCServiceStatusRequest Request = new STLCServiceStatusRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCServiceStatusResult Result = new STLCServiceStatusResult();

            if (parameters.Count == 0 || (parameters.Count == 1 && parameters[0] == ":"))
            {
                Request.group = "core";
            }
            else if (parameters.Count == 1 && parameters[0][0] == ':')
            {
                Request.group = parameters[0] == ":all" ? null : parameters[0].Substring(1);
            }
            else
            {
                Request.services = new string[parameters.Count];
                for (int i = 0; i < parameters.Count; i++)
                {
                    Request.services[i] = parameters[i];
                }
            }

            Result = CommandServiceClient.getServiceStatus(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                foreach (STLCServiceStatus ss in Result.data.servicesStatus)
                {
                    Console.WriteLine("");
                    Console.WriteLine("ServiceName: " + ss.Name);
                    Console.WriteLine("DisplayName: \"" + ss.DisplayName + "\"");
                    Console.WriteLine("Status     : " + ss.Status);
                }
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta impostazione comando servizi
        /// </summary>
        /// <param name="command">comando da inviare</param>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool SetServiceCommand(STLCServiceCommand command, List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCServiceStatusRequest Request = new STLCServiceStatusRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCServiceStatusResult Result = new STLCServiceStatusResult();

            Request.command = command;

            if (parameters.Count == 0 || (parameters.Count == 1 && parameters[0] == ":"))
            {
                Request.group = "core";
            }
            else if (parameters.Count == 1 && parameters[0][0] == ':')
            {
                Request.group = parameters[0] == ":all" ? null : parameters[0].Substring(1);
            }
            else
            {
                Request.services = new string[parameters.Count];
                for (int i = 0; i < parameters.Count; i++)
                {
                    Request.services[i] = parameters[i];
                }
            }

            Result = CommandServiceClient.setServiceStatus(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Comando inviato con successo.");
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta configurazione
        /// </summary>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito (STOP / START / RESTART)</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool Configuration(List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCApplySystemConfigRequest Request = new STLCApplySystemConfigRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCApplySystemConfigResult Result = new STLCApplySystemConfigResult();

            if (parameters.Count < 1 || parameters[0] != ":system")
            {
                return false;
            }

            Result = CommandServiceClient.applySystemConfig(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Comando inviato con successo.");
                Console.WriteLine("Reset a completamento configurazione : " + Result.status.resetRequired);
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Metodo di elaborazione richiesta shutdwon sistema
        /// </summary>
        /// <param name="type">tipo shutdown (POWER_OFF / REBOOT)</param>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool Shutdown(STLCShutdownType type, List<string> parameters)
        {
            ClientCommandServiceClient CommandServiceClient = new ClientCommandServiceClient();
            STLCShutdownRequest Request = new STLCShutdownRequest {clientType = RequestingClientType.COMMAND_LINE};
            STLCShutdownResult Result = new STLCShutdownResult();

            Request.shutdownType = type;

            Result = CommandServiceClient.STLCShutdown(Request);

            if (Result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Comando inviato con successo.");
            }
            else
            {
                PrintErrorRequest(Result.status.errorCode, Result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Metodo di elaborazione ricaricamento configurazione
        /// </summary>
        /// <param name="parameters">lista parametri su riga di comando</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: comando eseguito</item>
        ///         <item>false: comando non valido</item>
        ///     </list>
        /// </returns>
        private static bool ReloadConfig(List<string> parameters)
        {
            ClientCommandServiceClient commandServiceClient = new ClientCommandServiceClient();
            ReloadConfigRequest request = new ReloadConfigRequest {clientType = RequestingClientType.COMMAND_LINE};
            ReloadConfigResult result = commandServiceClient.reloadConfig(request);

            if (result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Configurazione ricaricata con successo.");
            }
            else
            {
                PrintErrorRequest(result.status.errorCode, result.status.errorDescription);
            }

            return true;
        }

        private static bool ReloadSystemXmlIntoDatabase(List<string> parameters)
        {
            ClientCommandServiceClient commandServiceClient = new ClientCommandServiceClient();
            ReloadSystemXmlIntoDatabaseRequest request = new ReloadSystemXmlIntoDatabaseRequest {clientType = RequestingClientType.COMMAND_LINE};

            if (parameters.Count <= 0)
            {
                request.mode = STLCMode.NORMAL;
            }
            else
            {
                switch (parameters[0])
                {
                    case ":n":
                        request.mode = STLCMode.NORMAL;
                        break;

                    case ":m":
                        request.mode = STLCMode.MAINTENANCE;
                        break;
                    default:
                        return false;
                }
            }

            ReloadSystemXmlIntoDatabaseResult result = commandServiceClient.reloadSystemXmlIntoDatabase(request);

            if (result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Configurazione da System.xml ricaricata con successo.");
                Console.WriteLine(result.data.OperationDescription);
            }
            else if (result.status.level == StatusLevel.EMPTY)
            {
                Console.WriteLine("");
                Console.WriteLine(result.status.errorDescription);
            }
            else
            {
                PrintErrorRequest(result.status.errorCode, result.status.errorDescription);
            }

            return true;
        }

        private static bool UpdateRegionListFromWS(List<string> parameters)
        {
            ClientCommandServiceClient commandServiceClient = new ClientCommandServiceClient();
            UpdateRegionListFromWSRequest request = new UpdateRegionListFromWSRequest {clientType = RequestingClientType.COMMAND_LINE};
            UpdateRegionListFromWSResult result = commandServiceClient.updateRegionListFromWS(request);

            if (result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Lista compartimenti, linee e stazioni aggiornata con successo.");
                Console.WriteLine(result.data.OperationDescription);
            }
            else
            {
                PrintErrorRequest(result.status.errorCode, result.status.errorDescription);
            }

            return true;
        }

        private static bool UpdateDeviceTypeListFromWS(List<string> parameters)
        {
            ClientCommandServiceClient commandServiceClient = new ClientCommandServiceClient();
            UpdateDeviceTypeListFromWSRequest request = new UpdateDeviceTypeListFromWSRequest {clientType = RequestingClientType.COMMAND_LINE};
            UpdateDeviceTypeListFromWSResult result = commandServiceClient.updateDeviceTypeListFromWS(request);

            if (result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Lista tipi periferiche, sistemi e produttori aggiornata con successo.");
                Console.WriteLine(result.data.OperationDescription);
            }
            else
            {
                PrintErrorRequest(result.status.errorCode, result.status.errorDescription);
            }

            return true;
        }

        private static bool GetNeededSupervisors(List<string> parameters)
        {
            ClientCommandServiceClient commandServiceClient = new ClientCommandServiceClient();
            GetNeededSupervisorsRequest request = new GetNeededSupervisorsRequest {clientType = RequestingClientType.COMMAND_LINE};
            GetNeededSupervisorsResult result = commandServiceClient.getNeededSupervisors(request);

            if (result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Lista Supervisori necessari al monitoraggio aggiornata con successo.");
                Console.WriteLine(result.data.OperationDescription);
            }
            else if (result.status.level == StatusLevel.EMPTY)
            {
                Console.WriteLine("");
                Console.WriteLine(result.status.errorDescription);
            }
            else
            {
                PrintErrorRequest(result.status.errorCode, result.status.errorDescription);
            }

            return true;
        }

        private static bool SetConfigOnRegistry(List<string> parameters)
        {
            ClientCommandServiceClient commandServiceClient = new ClientCommandServiceClient();
            SetConfigOnRegistryRequest request              = new SetConfigOnRegistryRequest { clientType = RequestingClientType.COMMAND_LINE };

            if (parameters.Count != 1)
                return false;

            switch(parameters[0])
            {
                case ":create_all":
                    request.cfgOnRegistryCommand = CfgOnRegistryCommand.CREATE_ALL;
                    break;
                case ":delete_all":
                    request.cfgOnRegistryCommand = CfgOnRegistryCommand.DELETE_ALL;
                    break;
                case ":delete_cfg":
                    request.cfgOnRegistryCommand = CfgOnRegistryCommand.DELETE_ONLY_CONFIG;
                    break;
                default:
                    request.cfgOnRegistryCommand = CfgOnRegistryCommand.NOT_SPECIFIED;
                    break;
            }

            SetConfigOnRegistryResult result = commandServiceClient.setConfigOnRegistry(request);

            if (result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");

                switch(result.status.cmdExecuted)
                {
                    case CfgOnRegistryCommand.CREATE_ALL:

                        Console.WriteLine("Inserimento sul registro dei valori:\n");
                        
                        foreach(string key in result.status.listKeys)
                        {
                            Console.WriteLine(String.Format("- {0}", key));
                        }
                        
                        Console.WriteLine("\ncompletato con successo.");
                        
                        break;

                    case CfgOnRegistryCommand.DELETE_ONLY_CONFIG:
                    case CfgOnRegistryCommand.DELETE_ALL:
                        
                        Console.WriteLine("Cancellazione dal registro dei valori:\n");

                        foreach(string key in result.status.listKeys)
                        {
                            Console.WriteLine(String.Format("- {0}", key));
                        }
                        
                        Console.WriteLine("\ncompletata con successo.");

                        break;

                    default:
                        Console.WriteLine("Nessun aggiornamento effettuato alle chiavi del registro.");
                        break;
                }
            }
            else
            {
                PrintErrorRequest(result.status.errorCode, result.status.errorDescription);
            }

            return true;
        }

        private static bool EmptyDatabase(List<string> parameters)
        {
            ClientCommandServiceClient commandServiceClient = new ClientCommandServiceClient();
            EmptyDatabaseRequest request = new EmptyDatabaseRequest { clientType = RequestingClientType.COMMAND_LINE };

            if (parameters.Count <= 0)
            {
            }

            EmptyDatabaseResult result = commandServiceClient.emptyDatabase(request);

            if (result.status.level == StatusLevel.SUCCESS)
            {
                Console.WriteLine("");
                Console.WriteLine("Svuotamento DB eseguito con successo.");
                Console.WriteLine(result.data.OperationDescription);
            }
            else if (result.status.level == StatusLevel.EMPTY)
            {
                Console.WriteLine("");
                Console.WriteLine(result.status.errorDescription);
            }
            else
            {
                PrintErrorRequest(result.status.errorCode, result.status.errorDescription);
            }

            return true;
        }

        /// <summary>
        ///     Stampa help comando
        /// </summary>
        private static void PrintUsageInfo()
        {
            Console.WriteLine("\nSintassi: STLCmgr [comando]\n");

            Console.WriteLine("Comandi:");

            Console.WriteLine("\n  /info--------------------------------------------------Richiede informazioni generali.");
            Console.WriteLine("\n  /status------------------------------------------------Richiede lo stato generale.");
            Console.WriteLine("\n  /network-----------------------------------------------Richiede la configurazione delle porte di rete.");
            Console.WriteLine("\n  /set:m [timeout]---------------------------------------Imposta la modalità MAINTENANCE.");
            Console.WriteLine("                                                           timeout = tempo di manutenzione (ore)");
            Console.WriteLine("                                                           0 = tempo infinito");
            Console.WriteLine("                                                           default tempo in cfg.");
            Console.WriteLine("\n  /set:n-------------------------------------------------Imposta la modalità NORMAL");
            Console.WriteLine("\n  /service[:all|:group|s1 s2 ...]------------------------Richiede lo stato dei servizi");
            Console.WriteLine("                                                           :all      = tutti i servizi");
            Console.WriteLine("                                                           :group    = gruppo servizi");
            Console.WriteLine("                                                           s1 s2 ... = lista servizi");
            Console.WriteLine("                                                           default   = gruppo 'core'.");
            Console.WriteLine("\n  /stop[:all|:group|s1 s2 ...]---------------------------Comanda lo stop dei servizi");
            Console.WriteLine("                                                           :all      = tutti i servizi");
            Console.WriteLine("                                                           :group    = gruppo servizi");
            Console.WriteLine("                                                           s1 s2 ... = lista servizi");
            Console.WriteLine("                                                           default   = gruppo 'core'.");
            Console.WriteLine("\n  /start[:all|:group|s1 s2 ...]--------------------------Comanda lo start dei servizi");
            Console.WriteLine("                                                           :all      = tutti i servizi");
            Console.WriteLine("                                                           :group    = gruppo servizi");
            Console.WriteLine("                                                           s1 s2 ... = lista servizi");
            Console.WriteLine("                                                           default   = gruppo 'core'.");
            Console.WriteLine("\n  /restart[:all|:group|s1 s2 ...]------------------------Comanda il restart dei servizi");
            Console.WriteLine("                                                           :all      = tutti i servizi");
            Console.WriteLine("                                                           :group    = gruppo servizi");
            Console.WriteLine("                                                           s1 s2 ... = lista servizi");
            Console.WriteLine("                                                           default   = gruppo 'core'.");
            Console.WriteLine("\n  /config:system-----------------------------------------Comanda la configurazione dei parametri di sistema operativo.");
            Console.WriteLine("\n  /reboot------------------------------------------------Comanda il reboot del sistema.");
            Console.WriteLine("\n  /PowerOff----------------------------------------------Comanda lo spegnimento della macchina.");
            Console.WriteLine("\n  /EmptyDB-----------------------------------------------Comanda la cancellazione del DB.");
            Console.WriteLine("\n  /reloadconfig------------------------------------------Forza il caricamento della configurazione di STLCManager da 'STLCManagerConfig.xml'.");
            Console.WriteLine("\n  /ReloadSystemXml---------------------------------------Ricarica la lista di periferiche da 'System.xml' e crea gli oggetti in base dati.");
            Console.WriteLine("                                                           :m        = alla fine dell'esecuzione va in modalità MAINTENANCE");
            Console.WriteLine("                                                           :n        = alla fine dell'esecuzione va in modalità NORMAL");
            Console.WriteLine("                                                           default   = alla fine dell'esecuzione va in modalità NORMAL");
            Console.WriteLine("\n  /UpdateRegionList--------------------------------------Scarica e aggiorna in base dati la lista di compartimenti, linee e stazioni.");
            Console.WriteLine("\n  /UpdateDeviceTypeList----------------------------------Scarica e aggiorna in base dati la lista dei tipi periferica, sistemi e produttori.");
            Console.WriteLine("\n  /getneededsupervisors----------------------------------Ottiene la lista dei Supervisori di monitoraggio necessari per le periferiche configurate.");
            Console.WriteLine("\n  /ConfigOnRegistry[:create_all|:delete_all:delete_cfg]--Aggiorna le chiavi sul registro");
            Console.WriteLine("                                                           :create_all = inserisce i valori del file .config nel registro (se non esistono già)");
            Console.WriteLine("                                                           :delete_all = elimina tutti i valori dal percorso del registro definito nel file .config");
            Console.WriteLine("                                                           :delete_cfg = elimina i valori dal percorso del registro definito nel file .config");
            Console.WriteLine("                                                                         che sono già valorizzati nel file STLCManagerService.exe.config");
            Console.WriteLine("\n");
        }

        /// <summary>ConfigOnRegistry
        ///     Stampa informazioni di comando non valido
        /// </summary>
        private static void PrintUnknownCommand()
        {
            Console.WriteLine("\nERRORE: Comando/parametro non valido\n");
            PrintUsageInfo();
        }

        /// <summary>
        ///     Stampa errore su richiesta eseguita a STLCManagerService.
        /// </summary>
        /// <param name="iCode">codice errore ricevuto</param>
        /// <param name="sDescription">descrizione dell'errore</param>
        private static void PrintErrorRequest(int iCode, string sDescription)
        {
            Console.WriteLine("\nERRORE: Richiesta fallita.");
            Console.WriteLine("  Code       : " + iCode);
            Console.WriteLine("  Description: " + sDescription);
            Console.WriteLine("");
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace STLCManager.Command.Functions
{
    /// <summary>
    ///     Classe per verifica/decodifica comando.
    /// </summary>
    internal class SelectCmd
    {
        #region Members

        /// <summary>
        ///     Codici possibili comandi
        /// </summary>
        public enum Code
        {
            Help,
            Info,
            Status,
            Network,
            SetMode,
            GetServiceStatus,
            StartService,
            StopService,
            RestartService,
            Configuration,
            SystemReboot,
            SystemPowerOff,
            ReloadConfig,
            ReloadSystemXmlIntoDatabase,
            UpdateRegionListFromWS,
            UpdateDeviceTypeListFromWS,
            GetNeededSupervisors,
            SetConfigOnRegistry,
            EmptyDatabase,
            CommandUnknown
        };

        /// <summary>
        ///     Strutta dati per la verifica validità comandi
        /// </summary>
        private struct DefCommand
        {
            internal bool FullCompare; // true : il comando digitato deve corrispondere esattamente alla descrizione
            // false: confronto case insensitive e comando decodificato anche se digitato parzialmente ma sufficiente per individuazione univoca.
            internal string Description; // descrizione comando
            internal Code Code; // codice comando

            internal DefCommand(bool bCompare, string sDescr, Code cCode)
            {
                this.FullCompare    = bCompare;
                this.Description    = sDescr;
                this.Code           = cCode;
            }
        };

        /// <summary>
        ///     Tabella definizione comandi
        /// </summary>
        private DefCommand[] DefineCommands =
        {
            new DefCommand(false, "", Code.Help), new DefCommand(false, "/info", Code.Info),
            new DefCommand(false, "/status", Code.Status), new DefCommand(false, "/network", Code.Network),
            new DefCommand(false, "/set", Code.SetMode), new DefCommand(false, "/service", Code.GetServiceStatus),
            new DefCommand(false, "/start", Code.StartService), new DefCommand(false, "/stop", Code.StopService),
            new DefCommand(false, "/restart", Code.RestartService), new DefCommand(false, "/config", Code.Configuration),
            new DefCommand(false, "/reboot", Code.SystemReboot), new DefCommand(false, "/reloadconfig", Code.ReloadConfig),
            new DefCommand(true, "/PowerOff", Code.SystemPowerOff), new DefCommand(true, "/ReloadSystemXml", Code.ReloadSystemXmlIntoDatabase),
            new DefCommand(true, "/UpdateRegionList", Code.UpdateRegionListFromWS), new DefCommand(true, "/UpdateDeviceTypeList", Code.UpdateDeviceTypeListFromWS),
            new DefCommand(false, "/getneededsupervisors", Code.GetNeededSupervisors),
            new DefCommand(true, "/ConfigOnRegistry", Code.SetConfigOnRegistry), 
            new DefCommand(true, "/EmptyDB", Code.EmptyDatabase)
        };

        #endregion

        #region Methods

        /// <summary>
        ///     Decodifica comandi, ritorna codice comando e crea lista degli eventuali parametri su riga di comando
        /// </summary>
        /// <param name="args">argomenti presenti sulla riga di comando</param>
        /// <param name="parameters">lista parametri comando</param>
        /// <returns>codice comando decodificato</returns>
        public Code DecodeCommand(string[] args, out List<string> parameters)
        {
            parameters = new List<string>();

            if (args.Length <= 0)
            {
                return Code.Help;
            }

            string[] sSplitCmd = args[0].Split(':');

            if (0 < sSplitCmd.Length && sSplitCmd.Length <= 2)
            {
                string sCmd = sSplitCmd[0];
                Code cCmd = Code.CommandUnknown;

                foreach (DefCommand command in this.DefineCommands)
                {
                    if (this.Compare(sCmd, command.Description, command.FullCompare))
                    {
                        if (cCmd == Code.CommandUnknown)
                        {
                            cCmd = command.Code;
                        }
                        else
                        {
                            return Code.CommandUnknown;
                        }
                    }
                }

                if (cCmd != Code.CommandUnknown)
                {
                    // Individuato comando, crea lista parametri del comando
                    if (sSplitCmd.Length == 2)
                    {
                        parameters.Add(":" + sSplitCmd[1].Trim().ToLower());
                    }

                    for (int i = 1; i < args.Length; i++)
                    {
                        parameters.Add(args[i].Trim().ToLower());
                    }

                    return cCmd;
                }
            }
            return Code.CommandUnknown;
        }

        /// <summary>
        ///     Metodo di comparazione comando in base all'attributo fullCompare
        /// </summary>
        /// <param name="strA">stringa da confrontare</param>
        /// <param name="strB">stringa di riferimento</param>
        /// <param name="fullCompare">tipo di verifica</param>
        /// <returns>
        ///     <list type="bool">
        ///         <item> true: le stringhe soddisfano il confronto </item>
        ///         <item>false: stringhe diverse</item>
        ///     </list>
        /// </returns>
        private bool Compare(string strA, string strB, bool fullCompare)
        {
            if (!fullCompare)
            {
                return String.Compare(strA, 0, strB, 0, strA.Length, true) == 0;
            }

            return String.Compare(strA, strB) == 0;
        }

        #endregion
    }
}
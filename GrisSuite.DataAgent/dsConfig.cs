﻿
using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.dsConfigTableAdapters;
using GrisSuite.Common;

namespace GrisSuite.Data
{   

	public partial class dsConfig {
		public partial class RegionsRow
		{
			public Int64 SetDecodedRegId(UInt16 decodedRegId)
			{
				return (Int64)Utility.EncodeRegionID(decodedRegId);
			}

			public UInt16 GetDecodedRegId()
			{
				ushort regID;
				Utility.DecodeRegionID((ulong)this.RegID, out regID);
				return regID;
			}
		}

		public partial class ZonesRow
		{
			public Int64 SetDecodedZonId(UInt16 decodedRegId, UInt16 decodedZonId)
			{
				return (Int64)Utility.EncodeZoneID(decodedRegId, decodedZonId);
			}

			public UInt16 GetDecodedZonId()
			{
				ushort regID;
				ushort zonID;
				Utility.DecodeZoneID((ulong)this.ZonID, out zonID, out regID);
				return zonID;
			}
		}

		public partial class NodesRow
		{
			public Int64 SetDecodedNodId(UInt16 decodedRegId, UInt16 decodedZonId, UInt16 decodedNodId)
			{
				return (Int64)Utility.EncodeNodeID(decodedRegId, decodedZonId, decodedNodId);
			}

			public UInt16 GetDecodedNodId()
			{
				ushort regID;
				ushort zonID;
				ushort nodID;
				Utility.DecodeNodeID((ulong)this.NodID, out nodID, out zonID, out regID);
				return nodID;
			}
		}


		public partial class ServersDataTable
		{
		}

		private static int _commandTimeOut = global::GrisSuite.Data.Properties.Settings.Default.CommandTimeout;

		public static int Fill(dsConfig config, string[] tables)
		{
			dsConfigDataSetAdaper daConfig = new dsConfigDataSetAdaper();
			return daConfig.Fill(config, tables);
		}

		public static int Fill(dsConfig config)
		{
			return Fill(config, null);
		}

		public static dsConfig GetData()
		{
			return GetData(null);
		}

		public static dsConfig GetData(string [] tables)
		{
			dsConfig c = new dsConfig();
			Fill(c, tables);
			return c;
		}

		private static dsConfig CopyConfig(dsConfig config)
		{
			dsConfig config2 = new dsConfig();

			foreach (DataTable dt in config.Tables)
			{
				config2.Tables[dt.TableName].Merge(dt,false);
			}
			return config2;
		}

		public static Guid ReplaceWithLog(dsConfig ds, string hostSender, string[] tables, string mac)
		{

			Guid logId = Guid.Empty;

			dsConfigDataSetAdaper daConfig = new dsConfigDataSetAdaper();
			using (SqlConnection cn = daConfig.Connection)
			{
				cn.Open();
				daConfig.CommandTimeout = _commandTimeOut;
				daConfig.Transaction = cn.BeginTransaction(IsolationLevel.ReadCommitted);
				try
				{
					dsConfig dsForDelete = CopyConfig(ds);
					Util.SetRowsAsDeleted(dsForDelete); // forza la cancellazione di tutti i dati contentuti
					daConfig.Update(dsForDelete, tables, true);

					Util.SetRowsAsAdded(ds); // forza l'iserimento di tutti i dati contentuti
					daConfig.Update(ds, tables, false);

					daConfig.Transaction.Commit();

					logId = dsLog.InsLogMessage(ds, typeof(dsConfig).ToString(), hostSender, mac);

				}
				catch
				{
					if (daConfig.Transaction != null)
						daConfig.Transaction.Rollback();
					throw;
				}
				finally
				{
					if (cn.State != ConnectionState.Closed)
						cn.Close();
				}
			}



			return logId;
		}

		public static void Update(dsConfig ds, string[] tables, bool forcedUpdate )
		{
			dsConfigDataSetAdaper daConfig = new dsConfigDataSetAdaper();
			using (SqlConnection cn = daConfig.Connection)
			{
				cn.Open();
				daConfig.CommandTimeout = _commandTimeOut;
				daConfig.Transaction = cn.BeginTransaction(IsolationLevel.ReadCommitted);
				try
				{
					if (forcedUpdate)
					{
						ds.AcceptChanges();
						Util.SetRowsAsAdded(ds); // forza l'inserimento di tutti i dati
					}
					

					daConfig.Update(ds, tables, false);
					daConfig.Transaction.Commit();
				}
				catch
				{
					if (daConfig.Transaction != null)
						daConfig.Transaction.Rollback();
					throw;
				}
				finally
				{
					if (cn.State != ConnectionState.Closed)
						cn.Close();
				}
			}

		}

		public static Guid UpdateWithLog(dsConfig ds, string hostSender, string[] tables, string mac)
		{
			Guid logId = Guid.Empty;

			dsConfigDataSetAdaper daConfig = new dsConfigDataSetAdaper();
			using (SqlConnection cn = daConfig.Connection)
			{
				cn.Open();
				daConfig.CommandTimeout = _commandTimeOut;
				daConfig.Transaction = cn.BeginTransaction(IsolationLevel.ReadCommitted);
				try
				{
					Util.SetRowsAsAdded(ds); // forza l'inserimento di tutti i dati
					daConfig.Update(ds, tables, false);
					daConfig.Transaction.Commit();
					logId = dsLog.InsLogMessage(ds, typeof(dsConfig).ToString(), hostSender, mac);
				}
				catch 
				{
					if (daConfig.Transaction != null)
						daConfig.Transaction.Rollback();
					throw;
				}
				finally
				{
					if (cn.State != ConnectionState.Closed)
						cn.Close();
				}
			}
			return logId;
		}

	}

	internal class dsConfigDataSetAdaper
	{

		private StationTableAdapter taStation;
		private BuildingTableAdapter taBuilding;
		private RackTableAdapter taRack;
		private PortTableAdapter taPort;
		private ServersTableAdapter taServers;
		private RegionsTableAdapter taRegions;
		private ZonesTableAdapter taZones;
		private NodesTableAdapter taNodes;
		private DevicesTableAdapter taDevices;

		internal dsConfigDataSetAdaper()
		{
			taStation = new StationTableAdapter();
			taBuilding = new BuildingTableAdapter();
			taRack = new RackTableAdapter();
			taServers = new ServersTableAdapter();
			taPort = new PortTableAdapter();
			taRegions = new RegionsTableAdapter();
			taZones = new ZonesTableAdapter();
			taNodes = new NodesTableAdapter();
			taDevices = new DevicesTableAdapter();

			this.Connection = taServers.Connection;
		}

		internal SqlConnection Connection
		{
			get 
			{
				SqlConnection cn = taDevices.Connection;
				if (cn == null) cn = taNodes.Connection;
				if (cn == null) cn = taZones.Connection;
				if (cn == null) cn = taRegions.Connection;
				if (cn == null) cn = taServers.Connection;
				if (cn == null) cn = taStation.Connection;
				if (cn == null) cn = taBuilding.Connection;
				if (cn == null) cn = taRack.Connection;
				if (cn == null) cn = taPort.Connection;
				return cn;
			}
			set
			{
				taServers.Connection = value;
				taRegions.Connection = value;
				taZones.Connection = value;
				taNodes.Connection = value;
				taDevices.Connection = value;
				taStation.Connection = value;
				taBuilding.Connection = value;
				taRack.Connection = value;
				taPort.Connection = value;
			}
		}

		internal SqlTransaction Transaction
		{
			get
			{
				SqlTransaction tn = taDevices.Transaction;
				if (tn == null) tn = taNodes.Transaction;
				if (tn == null) tn = taZones.Transaction;
				if (tn == null) tn = taRegions.Transaction;
				if (tn == null) tn = taServers.Transaction;
				if (tn == null) tn = taStation.Transaction;
				if (tn == null) tn = taBuilding.Transaction;
				if (tn == null) tn = taRack.Transaction;
				if (tn == null) tn = taPort.Transaction;
				return tn;
			}
			set
			{
				if  (value != null)
					this.Connection = value.Connection;
				taServers.Transaction = value;
				taRegions.Transaction = value;
				taZones.Transaction = value;
				taNodes.Transaction = value;
				taDevices.Transaction = value;
				taStation.Transaction = value;
				taBuilding.Transaction = value;
				taRack.Transaction = value;
				taPort.Transaction = value;
			}
		}

		internal int CommandTimeout
		{
			get
			{
				int timeout = taDevices.CommandTimeout;
				if (timeout == 0) timeout = taNodes.CommandTimeout;
				if (timeout == 0) timeout = taZones.CommandTimeout;
				if (timeout == 0) timeout = taRegions.CommandTimeout;
				if (timeout == 0) timeout = taServers.CommandTimeout;
				if (timeout == 0) timeout = taStation.CommandTimeout;
				if (timeout == 0) timeout = taBuilding.CommandTimeout;
				if (timeout == 0) timeout = taRack.CommandTimeout;
				if (timeout == 0) timeout = taPort.CommandTimeout;
				return timeout;
			}
			set
			{
				taServers.CommandTimeout = value;
				taRegions.CommandTimeout = value;
				taZones.CommandTimeout = value;
				taNodes.CommandTimeout = value;
				taDevices.CommandTimeout = value;
				taStation.CommandTimeout = value;
				taBuilding.CommandTimeout = value;
				taRack.CommandTimeout = value;
				taPort.CommandTimeout = value;
			}
		}


		internal int Update(dsConfig config, string[] tables, bool forDelete)
		{
			int i = 0;

		   tables = InitializeTables(config, tables);

			if (forDelete)
			{

				if (Array.IndexOf<string>(tables, config.Station.TableName.ToLower()) >= 0)
					i += taStation.Update(config.Station);
				else if (Array.IndexOf<string>(tables, config.Building.TableName.ToLower()) >= 0)
					i += taBuilding.Update(config.Building);
				else if (Array.IndexOf<string>(tables, config.Rack.TableName.ToLower()) >= 0)
					i += taRack.Update(config.Rack);

				// Sono informazioni globali non vado mai in cancellazione generale.
				// Per cancellarle usare i metodi del singolo table adapter
				//if (Array.IndexOf<string>(tables, config.Regions.TableName.ToLower()) >= 0)
				//    i += taRegions.Update(config.Regions);
				//else if (Array.IndexOf<string>(tables, config.Zones.TableName.ToLower()) >= 0)
				//    i += taZones.Update(config.Zones);
				//else if (Array.IndexOf<string>(tables, config.Nodes.TableName.ToLower()) >= 0)
				//    i += taNodes.Update(config.Nodes);

				if (Array.IndexOf<string>(tables, config.Servers.TableName.ToLower()) >= 0)
				{
					i += taServers.Update(config.Servers);
				}
				else
				{
					if (Array.IndexOf<string>(tables, config.Port.TableName.ToLower()) >= 0)
						i += taPort.Update(config.Port);
					if (Array.IndexOf<string>(tables, config.Devices.TableName.ToLower()) >= 0)
						i += taDevices.Update(config.Devices);
				}
			}
			else
			{
				if (Array.IndexOf<string>(tables, config.Station.TableName.ToLower()) >= 0)
					i += taStation.Update(config.Station);
				if (Array.IndexOf<string>(tables, config.Building.TableName.ToLower()) >= 0)
					i += taBuilding.Update(config.Building);
				if (Array.IndexOf<string>(tables, config.Rack.TableName.ToLower()) >= 0)
					i += taRack.Update(config.Rack);
				if (Array.IndexOf<string>(tables, config.Servers.TableName.ToLower()) >= 0)
					i += taServers.Update(config.Servers);
				if (Array.IndexOf<string>(tables, config.Port.TableName.ToLower()) >= 0)
					i += taPort.Update(config.Port);
				if (Array.IndexOf<string>(tables, config.Regions.TableName.ToLower()) >= 0)
					i += taRegions.Update(config.Regions);
				if (Array.IndexOf<string>(tables, config.Zones.TableName.ToLower()) >= 0)
					i += taZones.Update(config.Zones);
				if (Array.IndexOf<string>(tables, config.Nodes.TableName.ToLower()) >= 0)
					i += taNodes.Update(config.Nodes);
				if (Array.IndexOf<string>(tables, config.Devices.TableName.ToLower()) >= 0)
					i += taDevices.Update(config.Devices);
			}

			return i;

		}

		internal int Fill(dsConfig config, string[] tables)
		{
			tables = InitializeTables(config, tables);

			int i = 0;

			if (Array.IndexOf<string>(tables, config.Station.TableName.ToLower()) >= 0)
				i += taStation.Fill(config.Station);
			if (Array.IndexOf<string>(tables, config.Building.TableName.ToLower()) >= 0)
				i += taBuilding.Fill(config.Building);
			if (Array.IndexOf<string>(tables, config.Rack.TableName.ToLower()) >= 0)
				i += taRack.Fill(config.Rack);
			if (Array.IndexOf<string>(tables, config.Servers.TableName.ToLower()) >= 0)
				i += taServers.Fill(config.Servers);
			if (Array.IndexOf<string>(tables, config.Port.TableName.ToLower()) >= 0)
				i += taPort.Fill(config.Port);
			if (Array.IndexOf<string>(tables, config.Regions.TableName.ToLower()) >= 0)
				i += taRegions.Fill(config.Regions);
			if (Array.IndexOf<string>(tables, config.Zones.TableName.ToLower()) >= 0)
				i += taZones.Fill(config.Zones);
			if (Array.IndexOf<string>(tables, config.Nodes.TableName.ToLower()) >= 0)
				i += taNodes.Fill(config.Nodes);
			if (Array.IndexOf<string>(tables, config.Devices.TableName.ToLower()) >= 0)
				i += taDevices.Fill(config.Devices);

			return i;

		}

		private string[] InitializeTables(dsConfig config, string[] tables)
		{
			if (tables == null || tables.Length == 0)
				tables = new string[] { config.Station.TableName, 
										config.Building.TableName, 
										config.Rack.TableName, 
										config.Servers.TableName, 
										config.Port.TableName, 
										config.Regions.TableName, 
										config.Zones.TableName, 
										config.Nodes.TableName, 
										config.Devices.TableName };

			for (int j = 0; j < tables.Length; j++)
				tables[j] = tables[j].ToLower().Trim();

			return tables;
		}


	}
}

namespace GrisSuite.Data.dsConfigTableAdapters
{
	public partial class DevicesTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class RegionsTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class ServersTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class ZonesTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}

	}

	public partial class NodesTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class StationTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class BuildingTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class RackTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class PortTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}



}

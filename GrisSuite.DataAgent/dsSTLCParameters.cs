﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.dsSTLCParametersTableAdapters;

namespace GrisSuite.Data
{
    public partial class dsSTLCParameters
    {
        partial class STLCParametersDataTable
        {
        }
    
        private static int _commandTimeOut = global::GrisSuite.Data.Properties.Settings.Default.CommandTimeout;

        public static Guid ReplaceWithLog(dsSTLCParameters ds, string hostSender, string mac)
        {
            Guid logId = Guid.Empty;
            STLCParametersTableAdapter ta = new STLCParametersTableAdapter();

            using (SqlConnection cn = ta.Connection)
            {
                cn.Open();
                ta.Transaction = cn.BeginTransaction(IsolationLevel.ReadCommitted);
                ta.CommandTimeout = _commandTimeOut;
                try
                {
                    STLCParametersQueries qry = new STLCParametersQueries();
                    qry.CommandTimeout = _commandTimeOut;
                    qry.Transaction = ta.Transaction;
                    qry.DelBySrvID(((STLCParametersRow)ds.STLCParameters.Rows[0]).SrvID);
                    Util.SetRowsAsAdded(ds);
                    ta.Update(ds);
                    ta.Transaction.Commit();
                    logId = dsLog.InsLogMessage(ds, typeof(dsSTLCParameters).ToString(), hostSender, mac);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    if (ta.Transaction != null)
                        ta.Transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (cn.State != ConnectionState.Closed)
                        cn.Close();
                }
            }
            return logId;
        }

    }
}

namespace GrisSuite.Data.dsSTLCParametersTableAdapters
{
    public partial class STLCParametersTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
            }
        }

        public int UpdateOnTransaction(dsSTLCParameters ds)
        {
            int rc = -1;
            try
            {
                this.Adapter.UpdateBatchSize = 0;
                this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                this.Transaction = this.Connection.BeginTransaction();
                rc = this.Update(ds);
                this.Transaction.Commit();
            }
            catch
            {
                if (this.Transaction != null)
                    this.Transaction.Rollback();
                throw;
            }
            return rc;
        }

        public int CommandTimeout
        {
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                    command.CommandTimeout = value;
            }
        }
    }

    public partial class STLCParametersQueries
    {

        internal SqlTransaction Transaction
        {
            get
            {
                return (SqlTransaction)this.CommandCollection[0].Transaction;
            }
            set
            {
                for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                {
                    this.CommandCollection[i].Connection = value.Connection;
                    this.CommandCollection[i].Transaction = value;
                }
            }
        }

        public int CommandTimeout
        {
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                    command.CommandTimeout = value;
            }
        }
    }
}


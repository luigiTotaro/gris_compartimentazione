﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.dsEventsTableAdapters;

namespace GrisSuite.Data
{
    public partial class dsEvents
    {
        public static Guid UpdateWithLog(dsEvents ds, string hostSender, string mac)
        {
            STLCEventsTableAdapter taEvents = new STLCEventsTableAdapter();

            Util.SetRowsAsAdded(ds);
            taEvents.CommandTimeout = Properties.Settings.Default.CommandTimeout;
            taEvents.UpdateBatchSize = Properties.Settings.Default.UpdateBatchSize;
            taEvents.UpdateOnTransaction(ds);

            return dsLog.InsLogMessage(ds, typeof(dsEvents).ToString(), hostSender, mac);
        }

    }
}

namespace GrisSuite.Data.dsEventsTableAdapters
{
    public partial class STLCEventsTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }

        public int UpdateOnTransaction(dsEvents ds)
        {
            int rc;
            using (SqlConnection cn = this.Connection)
            {
                cn.Open();
                this.Transaction = cn.BeginTransaction();

                try
                {
                    rc = this.Update(ds);
                    this.Transaction.Commit();
                }
                catch
                {
                    if (this.Transaction != null)
                        this.Transaction.Rollback();
                    throw;
                }
                finally
                {
                    if (cn.State != ConnectionState.Closed)
                        cn.Close();
                }

            }
            return rc;
        }

        public int CommandTimeout
        {
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                    command.CommandTimeout = value;
            }
        }
    }
}

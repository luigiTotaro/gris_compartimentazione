﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.Properties;
using GrisSuite.Data.dsDeviceAcksTableAdapters;

namespace GrisSuite.Data
{
    public partial class dsDeviceAcks
    {
        public static Guid UpdateWithLog(dsDeviceAcks ds, string hostSender, string mac, int? srvId)
        {
            DeviceAckTableAdapter ta = new DeviceAckTableAdapter();

            ta.DelDeviceAckByServer(srvId, mac);
            Util.SetRowsAsAdded(ds);
            ta.CommandTimeout = Settings.Default.CommandTimeout;
            ta.UpdateBatchSize = Settings.Default.UpdateBatchSize;
            ta.UpdateOnTransaction(ds);

            return dsLog.InsLogMessage(ds, typeof(dsDeviceAcks).ToString(), hostSender, mac);
        }
    }
}

namespace GrisSuite.Data.dsDeviceAcksTableAdapters
{
    public partial class DeviceAckTableAdapter
    {
        public int UpdateBatchSize
        {
            get { return this.Adapter.UpdateBatchSize; }
            set
            {
                this.Adapter.UpdateBatchSize = value;
                if (value != 1)
                {
                    this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                }
            }
        }

        public int UpdateOnTransaction(dsDeviceAcks ds)
        {
            int rc;
            using (SqlConnection cn = this.Connection)
            {
                cn.Open();
                this.Transaction = cn.BeginTransaction();

                try
                {
                    rc = this.Update(ds);
                    this.Transaction.Commit();
                }
                catch
                {
                    if (this.Transaction != null)
                    {
                        this.Transaction.Rollback();
                    }
                    throw;
                }
                finally
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                }
            }
            return rc;
        }

        public int CommandTimeout
        {
            get { return this.CommandCollection[0].CommandTimeout; }
            set
            {
                foreach (SqlCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
        }
    }
}
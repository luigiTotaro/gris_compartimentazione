﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.Properties;
using GrisSuite.Data.dsLogTableAdapters;

namespace GrisSuite.Data
{
    partial class dsLog
    {
        private static LogDBLevel _logLevel = Settings.Default.LogDBLevel;

        public static LogDBLevel LogLevel
        {
            get { return _logLevel; }
            set { _logLevel = value; }
        }

        public static Guid InsLogMessage(DataSet ds, string typeName, string hostSender)
        {
            return InsLogMessage(ds, typeName, hostSender, "");
        }

        public static Guid InsLogMessage(DataSet ds, string typeName, string hostSender, string MAC)
        {
            if (LogLevel == LogDBLevel.Off)
            {
                return new Guid();
            }

            Guid guidReturn = Guid.Empty;
            QueriesTableAdapter ta = new QueriesTableAdapter();
            byte[] msg = null;

            if (ds != null && (LogLevel == LogDBLevel.MessageCompleteAndStream || LogLevel == LogDBLevel.MessageComplete))
            {
                try
                {
                    msg = Util.CompressDataSet(ds);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }
            }

            using (SqlConnection cn = ta.DefaultConnection)
            {
                SqlTransaction tran = null;
                try
                {
                    cn.Open();
                    ta.Connection = cn;
                    tran = cn.BeginTransaction(IsolationLevel.ReadCommitted);
                    ta.Transaction = tran;

                    object rv = ta.InsertLogMessage(hostSender, msg, typeName, MAC, Environment.MachineName);
                    if (rv != null && rv != DBNull.Value)
                    {
                        guidReturn = (Guid) rv;
                        if (LogLevel == LogDBLevel.MessageCompleteAndStream || LogLevel == LogDBLevel.MessageEnvelopeAndStream)
                        {
                            if (typeName == typeof (dsDeviceStatus).ToString())
                            {
                                dsDeviceStatus dsDevStatus = (dsDeviceStatus) ds;
                                foreach (dsDeviceStatus.StreamsRow streamRow in dsDevStatus.Streams.Rows)
                                {
                                    // dalla versione 1.4.0.0 il log delle righe di stream non viene più utilizzato
                                    //ta.InsertLogStreams(guidReturn, streamRow.DevID, streamRow.StrID, streamRow.Name, streamRow.Visible, streamRow.Data, streamRow.DateTime, streamRow.SevLevel, streamRow.Description);
                                    ta.InsertLogStreams(guidReturn, streamRow.DevID, streamRow.StrID, streamRow.Name, streamRow.Visible, null, streamRow.DateTime, streamRow.SevLevel, "");
                                }
                            }
                        }
                    }

                    tran.Commit();
                }
                catch
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }
                    throw;
                }
                finally
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                }
            }

            return guidReturn;
        }

        public partial class LogMessageRow
        {
            public DataSet DecodeMessage()
            {
                DataSet ds = null;

                try
                {
                    if (!this.IsMessageNull())
                    {
                        ds = Util.DecompressDataSet(this.Message);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }

                return ds;
            }
        }
    }
}

namespace GrisSuite.Data.dsLogTableAdapters
{
    public partial class LogMessageTableAdapter
    {
        public int CommandTimeout
        {
            get { return this.CommandCollection[0].CommandTimeout; }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
        }
    }

    public partial class QueriesTableAdapter
    {
        private SqlConnection _connection;
        private SqlTransaction _transaction;

        public SqlConnection DefaultConnection
        {
            get { return (SqlConnection) this.CommandCollection[0].Connection; }
        }

        public SqlConnection Connection
        {
            get { return this._connection; }
            set
            {
                this._connection = value;
                foreach (IDbCommand command in this.CommandCollection)
                {
                    command.Connection = this._connection;
                }
            }
        }

        public SqlTransaction Transaction
        {
            get { return this._transaction; }
            set
            {
                this._transaction = value;
                foreach (IDbCommand command in this.CommandCollection)
                {
                    command.Transaction = this._transaction;
                }
            }
        }

        public int CommandTimeout
        {
            get { return this.CommandCollection[0].CommandTimeout; }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                {
                    command.CommandTimeout = value;
                }
            }
        }
    }
}
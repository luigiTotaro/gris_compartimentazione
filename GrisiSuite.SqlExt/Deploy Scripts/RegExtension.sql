USE [TelefinCentral]
GO
sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
sp_configure 'clr enabled', 1;
GO
RECONFIGURE;
GO
CREATE ASSEMBLY SqlClassLibrary 
FROM 'C:\Program Files\Telefin\SqlCLRExtension\SqlClassLibrary.dll'
WITH PERMISSION_SET = SAFE;
GO
/****** Object:  UserDefinedFunction [dbo].[GetDecodedDevId]    Script Date: 10/21/2008 16:46:36 ******/
CREATE FUNCTION [dbo].[GetDecodedDevId](@nodId [bigint])
RETURNS [smallint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedDevId]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDecodedNodId]    Script Date: 10/21/2008 16:46:37 ******/
CREATE FUNCTION [dbo].[GetDecodedNodId](@nodId [bigint])
RETURNS [smallint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedNodId]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDecodedRegId]    Script Date: 10/21/2008 16:46:38 ******/
CREATE FUNCTION [dbo].[GetDecodedRegId](@regId [bigint])
RETURNS [smallint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedRegId]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDecodedZonId]    Script Date: 10/21/2008 16:46:38 ******/
CREATE FUNCTION [dbo].[GetDecodedZonId](@zonId [bigint])
RETURNS [smallint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedZonId]
GO
/****** Object:  UserDefinedFunction [dbo].[SetDecodedDevId]    Script Date: 10/21/2008 16:46:39 ******/
CREATE FUNCTION [dbo].[SetDecodedDevId](@decodedRegId [smallint], @decodedZonId [smallint], @decodedNodId [smallint], @decodedDevId [smallint])
RETURNS [bigint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedDevId]
GO
/****** Object:  UserDefinedFunction [dbo].[SetDecodedNodId]    Script Date: 10/21/2008 16:46:40 ******/
CREATE FUNCTION [dbo].[SetDecodedNodId](@decodedRegId [smallint], @decodedZonId [smallint], @decodedNodId [smallint])
RETURNS [bigint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedNodId]
GO
/****** Object:  UserDefinedFunction [dbo].[SetDecodedRegId]    Script Date: 10/21/2008 16:46:41 ******/
CREATE FUNCTION [dbo].[SetDecodedRegId](@decodedRegId [smallint])
RETURNS [bigint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedRegId]
GO
/****** Object:  UserDefinedFunction [dbo].[SetDecodedZonId]    Script Date: 10/21/2008 16:46:42 ******/
CREATE FUNCTION [dbo].[SetDecodedZonId](@decodedRegId [smallint], @decodedZonId [smallint])
RETURNS [bigint] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedZonId]
GO

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt64 SetDecodedRegId(SqlInt16 decodedRegId)
    {
        UInt64 longValue = (UInt64)(Int16)decodedRegId;  //UInt16.Parse(decodedRegId.ToString());
        longValue |= (UInt64)0xFFFF << 16;
        longValue |= (UInt64)0xFFFF << 32;

        return (SqlInt64)(Int64)longValue;
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt16 GetDecodedRegId(SqlInt64 regId)
    {
        Int64 longValue = (Int64)regId & 0xFFFF;
        return (SqlInt16)longValue;
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt64 SetDecodedZonId(SqlInt16 decodedRegId, SqlInt16 decodedZonId)
    {
        UInt64 longValue = (UInt64)(Int16)decodedRegId;
        longValue |= (UInt64)(Int16)decodedZonId << 16;

        return (SqlInt64)(Int64)longValue;
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt16 GetDecodedZonId(SqlInt64 zonId)
    {
        Int64 longValue = (Int64)zonId & 0xFFFF0000;
        return (SqlInt16)(Int16)(UInt16)(longValue >> 16);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt64 SetDecodedNodId(SqlInt16 decodedRegId, SqlInt16 decodedZonId, SqlInt16 decodedNodId)
    {
        UInt64 longValue = (UInt64)(Int16)decodedRegId;
        longValue |= (UInt64)(Int16)decodedZonId << 16;
        longValue |= (UInt64)(Int16)decodedNodId << 32;

        return (Int64)longValue;
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt16 GetDecodedNodId(SqlInt64 nodId)
    {
        Int64 longValue = (Int64)nodId & 0xFFFF00000000;
        return (Int16)(UInt16)(longValue >> 32);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt64 SetDecodedDevId(SqlInt16 decodedRegId, SqlInt16 decodedZonId, SqlInt16 decodedNodId, SqlInt16 decodedDevId)
    {
        UInt64 longValue = (UInt64)(Int16)decodedRegId;
        longValue |= (UInt64)(Int16)decodedZonId << 16;
        longValue |= (UInt64)(Int16)decodedNodId << 32;
        longValue |= (UInt64)(Int16)decodedDevId << 48;

        return (Int64)longValue;
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt16 GetDecodedDevId(SqlInt64 nodId)
    {
        Int64 longValue = (Int64)((UInt64)(Int64)nodId & 0xFFFF000000000000);
        return (Int16)(UInt16)(longValue >> 48);
    }

};


using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using GrisSuite;
using GrisSuite.Data;

[WebService(Namespace = "http://Telefin.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class SCAInfo : System.Web.Services.WebService
{
    public SCAInfo () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public byte[] GetConfig() {
        DataSet ds = dsConfig.GetData();
        return Util.CompressDataSet(ds); ;
    }

    [WebMethod]
    public byte[] GetStatus()
    {
        DataSet ds = dsDeviceStatus.GetData();
        return Util.CompressDataSet(ds);
    }
    
}

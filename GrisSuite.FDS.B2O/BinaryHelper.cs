﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;

namespace GrisSuite.FDS.B2O
{
    public static class BinaryExtensions
    {
        public static bool BitIsTrue(this Int16 value, byte index)
        {
            return ((value & (Int16)Math.Pow(2, index)) > 0);
        }

        public static bool BitIsTrue(this UInt16 value, byte index)
        {
            return ((value & (UInt16)Math.Pow(2, index)) > 0);
        }

        public static bool BitIsTrue(this Int32 value, byte index)
        {
            return ((value & (Int32)Math.Pow(2, index)) > 0);
        }

        public static bool BitIsTrue(this UInt32 value, byte index)
        {
            return ((value & (UInt32)Math.Pow(2, index)) > 0);
        }

        public static bool BitIsTrue(this Int64 value, byte index)
        {
            return ((value & (Int64)Math.Pow(2, index)) > 0);
        }

        public static bool BitIsTrue(this UInt64 value, byte index)
        {
            return ((value & (UInt64)Math.Pow(2, index)) > 0);
        }

        public static Int64 SwapByteOrder(this Int64 value)
        {
            return EndianHelper.Swap(value);
        }

        public static UInt64 SwapByteOrder(this UInt64 value)
        {
            return EndianHelper.Swap(value);
        }

        public static Int32 SwapByteOrder(this Int32 value)
        {
            return EndianHelper.Swap(value);
        }

        public static UInt32 SwapByteOrder(this UInt32 value)
        {
            return EndianHelper.Swap(value);
        }

        public static Int16 SwapByteOrder(this Int16 value)
        {
            return EndianHelper.Swap(value);
        }

        public static UInt16 SwapByteOrder(this UInt16 value)
        {
            return EndianHelper.Swap(value);
        }

        public static Int16 GetBitsValue(this Int16 value, byte bitStart, byte bitEnd)
        {
            value = (Int16)(value >> bitStart);
            Int16 mask = (Int16)(Int16.MaxValue << (bitEnd - bitStart + 1));
            value = (Int16)((value | mask) - mask);
            return value;
        }

        public static UInt16 GetBitsValue(this UInt16 value, byte bitStart, byte bitEnd)
        {
            value = (UInt16)(value >> bitStart);
            UInt16 mask = (UInt16)(UInt16.MaxValue << (bitEnd - bitStart + 1));
            value = (UInt16)((value | mask) - mask);
            return value;
        }

        public static Int32 GetBitsValue(this Int32 value, byte bitStart, byte bitEnd)
        {
            value = value >> bitStart;
            Int32 mask = Int32.MaxValue << (bitEnd - bitStart + 1);
            value = (value | mask) - mask;
            return value;
        }

        public static UInt32 GetBitsValue( this UInt32 value, byte bitStart, byte bitEnd)
        {
            value = value >> bitStart;
            UInt32 mask = UInt32.MaxValue << (bitEnd - bitStart + 1 );
            value = (value | mask) - mask;
            return value;
        }

        public static Int64 GetBitsValue(this Int64 value, byte bitStart, byte bitEnd)
        {
            value = value >> bitStart;
            Int64 mask = Int64.MaxValue << (bitEnd - bitStart + 1);
            value = (value | mask) - mask;
            return value;
        }

        public static UInt64 GetBitsValue(this UInt64 value, byte bitStart, byte bitEnd)
        {
            value = value >> bitStart;
            UInt64 mask = UInt64.MaxValue << (bitEnd - bitStart + 1);
            value = (value | mask) - mask;
            return value;
        }

    }

    public static class BinaryHelper<T> where T: struct
    {
        internal static byte[] RawStringBytesToBytes(byte[] stringBytes)
        {
                ASCIIEncoding a = new ASCIIEncoding();
                string s = a.GetString(stringBytes);

                if (s.Length % 2 > 0)
                {
                    throw new ArgumentException("StringBytes must have a even length"); 
                }

                byte[] b = new byte[(s.Length / 2)];

                int i = 0;
                int j = 0;

                while (i <= s.Length - 2)
                {
                    string sp = new string(new[] {s[i++],s[i++]});
                    b[j++] = byte.Parse(sp, System.Globalization.NumberStyles.AllowHexSpecifier );
                }

                return b;
        }

        public static T RawStringBytesDeserialize(byte[] stringBytes)
        {
            return SwapEndian(RawDeserialize(RawStringBytesToBytes(stringBytes)));
        }

        internal static byte[] RawSerialize()
        {
            Type tt = typeof(T);
            int rawsize = Marshal.SizeOf(tt);
            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.StructureToPtr(tt, buffer, false);
            byte[] rawdatas = new byte[rawsize];
            Marshal.Copy(buffer, rawdatas, 0, rawsize);
            Marshal.FreeHGlobal(buffer);
            return rawdatas;
        }

        public static T RawDeserialize(byte[] rawdatas)
        {
            Type tt = typeof(T);
            int rawsize = Marshal.SizeOf(tt);
            if (rawsize > rawdatas.Length)
                new ArgumentException("Type raw size is bigger that rawdatas lenght");

            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.Copy(rawdatas, 0, buffer, rawsize);
            T retobj = (T)Marshal.PtrToStructure(buffer, tt);
            Marshal.FreeHGlobal(buffer);
            return retobj;
        }

        internal static T SwapEndian(T oT)
        {
            return (T)EndianHelper.SwapEndian(oT);
        }

    }

    internal static class EndianHelper
    {
        internal static short Swap(short host)
        {
            return (short)(((host & 0xff) << 8) | ((host >> 8) & 0xff));
        }

        internal static int Swap(int host)
        {
            return ((Swap((short)host) & 0xffff) << 0x10) | (Swap((short)(host >> 0x10)) & 0xffff);
        }

        internal static long Swap(long host)
        {
            return ((Swap((int)host) & 0xffffffffL) << 0x20) | (Swap((int)(host >> 0x20)) & 0xffffffffL);
        }

        internal static ushort Swap(ushort host)
        {
            return (ushort)(((host & 0xff) << 8) | ((host >> 8) & 0xff));
        }

        internal static uint Swap(uint host)
        {
            return (uint)(((Swap((ushort)host) & 0xffff) << 0x10) | (Swap((ushort)(host >> 0x10)) & 0xffff));
        }

        internal static ulong Swap(ulong host)
        {
            return (ulong)(((Swap((uint)host) & 0xffffffffL) << 0x20) | (Swap((uint)(host >> 0x20)) & 0xffffffffL));
        }

        internal static object SwapEndian(object oT)
        {

            Type tt = oT.GetType();

            if (!tt.IsValueType)
                throw new ArgumentException("SwapEndian run only on value type", "oT");

            foreach (FieldInfo field in tt.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
            {
                EndianEnum fieldEndian = EndianEnum.LittleEndian;
                object[] attr = field.GetCustomAttributes(typeof(BinaryFormatAttribute),false);
                if (attr.Length > 0)
                {
                    fieldEndian = ((BinaryFormatAttribute)attr[0]).Endian;
                }

                if (field.FieldType == typeof(UInt16) && fieldEndian == EndianEnum.LittleEndian) // il default per i 16 bit in c# è BigEndian quindi se arriva Little endian lo giro
                    field.SetValue(oT, Swap((UInt16)field.GetValue(oT)));
                else if (field.FieldType == typeof(Int16) && fieldEndian == EndianEnum.LittleEndian) // il default per i 16 bit in c# è BigEndian quindi se arriva Little endian lo giro
                    field.SetValue(oT, Swap((Int16)field.GetValue(oT)));
                else if (field.FieldType == typeof(UInt32) && fieldEndian == EndianEnum.BigEndian)
                    field.SetValue(oT, Swap((UInt32)field.GetValue(oT)));
                else if (field.FieldType == typeof(Int32) && fieldEndian == EndianEnum.BigEndian)
                    field.SetValue(oT, Swap((Int32)field.GetValue(oT)));
                else if (field.FieldType == typeof(UInt64) && fieldEndian == EndianEnum.BigEndian)
                    field.SetValue(oT, Swap((UInt64)field.GetValue(oT)));
                else if (field.FieldType == typeof(Int64) && fieldEndian == EndianEnum.BigEndian)
                    field.SetValue(oT, Swap((Int64)field.GetValue(oT)));
                else if (!field.FieldType.IsPrimitive)
                {
                    if (!field.FieldType.IsArray)
                    {
                        field.SetValue(oT, SwapEndian(field.GetValue(oT)));
                    }
                    else
                    {
                        Array a = (Array)field.GetValue(oT);
                        for (int i = 0; i < a.Length; i++)
                        {
                            a.SetValue(SwapEndian(a.GetValue(i)),i);
                        }
                    }
                }
                    
            }

            return (oT);
        }


    }

    public enum EndianEnum
    {
        LittleEndian,
        BigEndian
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class BinaryFormatAttribute : Attribute
    {
        public BinaryFormatAttribute()
        {
            this.Endian = EndianEnum.LittleEndian;
            //this.Multiplier = 1;
        }

        public EndianEnum Endian { get; set; }
        //internal decimal Multiplier { get; set; }
        //internal string Format { get; set; }
    }

}

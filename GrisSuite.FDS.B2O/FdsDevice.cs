﻿using GrisSuite.Common;

namespace GrisSuite.FDS.B2O
{
    public class FdsDevice : ObjectWithState
    {
        public FdsGeneralStatusStream GeneralStatus { get; set; } // 1 = Stato generale
        public FdsCPUsMeasureStream CPUsMeasures { get; set; } // 2 = Tensioni e temperature
        public FdsPowerStatusStream PowerStatus { get; set; } // 3 = Configurazione e stato alimentazione
        public FdsInterfacesStatusStream InterfacesStatus { get; set; } // 4 = Configurazione e stato interfacce
        public FdsE1CountersStream E1Counters { get; set; } // 5 = Contatori E1
        public FdsGeneralInfoStream GeneralInfo { get; set; } // 6 = Informazioni Generali
    }

    public class FdsDeviceMapB2O : IDeviceMapB2O
    {
        public FdsDevice GetDevice(byte[][] data, TipoFDS fdsType)
        {
            FdsDevice fds = new FdsDevice();

            var GeneralStatusMapB2O = new FdsGeneralStatusStreamMapB2O(fds, fdsType);
            var CPUsMeasureMapB2O = new FdsCPUsMeasureStreamMapB2O(fds, fdsType);
            var PowerStatusMapB2O = new FdsPowerStatusStreamMapB2O(fds, fdsType);
            var InterfacesStatusMapB2O = new FdsInterfacesStatusStreamMapB2O(fds, fdsType);
            var E1CountersMapB2O = new FdsE1CountersStreamMapB2O(fds, fdsType);
            var GeneralInfoMapB2O = new FdsGeneralInfoStreamMapB2O(fds, fdsType);

            if (data[0] != null)
            {
                fds.GeneralStatus = GeneralStatusMapB2O.GetStream(data[0]);
            }

            if (data[1] != null)
            {
                fds.CPUsMeasures = CPUsMeasureMapB2O.GetStream(data[1]);
            }

            if (data[2] != null)
            {
                fds.PowerStatus = PowerStatusMapB2O.GetStream(data[2]);
            }

            if (data[3] != null)
            {
                fds.InterfacesStatus = InterfacesStatusMapB2O.GetStream(data[3]);
            }

            if (data[4] != null)
            {
                fds.E1Counters = E1CountersMapB2O.GetStream(data[4]);
            }

            if (data[5] != null)
            {
                fds.GeneralInfo = GeneralInfoMapB2O.GetStream(data[5]);
            }

            fds.RefreshState();

            return fds;
        }

        #region IDeviceMapB2O Members

        public IObjectWithState GetStream(byte[][] data, TipoFDS fdsType)
        {
            return this.GetDevice(data, fdsType);
        }

        #endregion
    }
}
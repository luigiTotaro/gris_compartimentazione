﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GrisSuite.Common;

namespace GrisSuite.FDS.B2O
{
    #region Data Struct

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsInterfacesStatusStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        internal FdsInterfaceCardStatusStruct[] LocalCards;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        internal FdsInterfaceCardStatusStruct[] RemoteCards;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsInterfaceCardStatusStruct
    {
        internal Byte CardCode;
        [BinaryFormat(Endian = EndianEnum.BigEndian)]
        internal UInt32 CardStatus;
    };

    #endregion

    #region Data Stream

    public partial class FdsInterfacesStatusStream : ObjectWithState
    {
        public FdsInterfacesStatusStream()
        {
            this.LocalCards = new List<FdsInterfaceCardStatusStream>();
            this.RemoteCards = new List<FdsInterfaceCardStatusStream>();
        }

        public List<FdsInterfaceCardStatusStream> LocalCards { get; set; }
        public List<FdsInterfaceCardStatusStream> RemoteCards { get; set; }
    }

    public partial class FdsInterfaceCardStatusStream : ObjectWithState
    {
        public int Index { get; internal set; }
        internal FdsCardStatusBits CpuCardStatus { get; set; }
        public ValueWithState<FdsInterfaceCardTypes> CardCode { get; private set; }
        public FdsInterfaceCardStatusBits CardStatus { get; private set; }

        public FdsInterfaceCardStatusStream()
        {
            this.CardCode = new ValueWithState<FdsInterfaceCardTypes>
                                {
                                    FormatFunction = (x => string.Format("0x{0:X2}", (byte)x.Value))
                                };
            this.CardCode.ValueChanged += CardCode_ValueChanged;

        }

        void CardCode_ValueChanged(ValueWithState<FdsInterfaceCardTypes> sender, EventArgs args)
        {
            switch (this.CardCode.Value)
            {
                case FdsInterfaceCardTypes.FD20000_ConsoleDTS:
                    CardStatus = new FdsInterfaceCardFD20000StatusBits();
                    break;
                case FdsInterfaceCardTypes.FD39200_ADSE:
                    CardStatus = new FdsInterfaceCardFD39200StatusBits();
                    break;
                case FdsInterfaceCardTypes.FD40100_Telecomando:
                    CardStatus = new FdsInterfaceCardFD40100StatusBits();
                    break;
                case FdsInterfaceCardTypes.FD60X00_BCA:
                    CardStatus = new FdsInterfaceCardFD60X00StatusBits();
                    break;
                case FdsInterfaceCardTypes.SlotEmpty:
                    CardStatus = null;
                    break;
            }
        }

        protected override void OnRefreshState()
        {
            base.OnRefreshState();

            if (this.CpuCardStatus != null)
            {

                if (this.State < CpuCardStatus.State)
                    this.State = CpuCardStatus.State;

                this.StateMessages.Clear();
                if (CpuCardStatus.NonRilevataLocale.Value) this.StateMessages.Add(new StateMessage { Message = CpuCardStatus.NonRilevataLocale.Label, State = CpuCardStatus.NonRilevataLocale.State });
                if (CpuCardStatus.NonRilevataRemoto.Value) this.StateMessages.Add(new StateMessage { Message = CpuCardStatus.NonRilevataRemoto.Label, State = CpuCardStatus.NonRilevataRemoto.State });
                if (CpuCardStatus.NonCorrispondenteAllaConfigurazione.Value) this.StateMessages.Add(new StateMessage { Message = CpuCardStatus.NonCorrispondenteAllaConfigurazione.Label, State = CpuCardStatus.NonCorrispondenteAllaConfigurazione.State });
                if (CpuCardStatus.NonInstallataCorrettamente.Value) this.StateMessages.Add(new StateMessage { Message = CpuCardStatus.NonInstallataCorrettamente.Label, State = CpuCardStatus.NonInstallataCorrettamente.State });
                if (CpuCardStatus.Anomalia.Value) this.StateMessages.Add(new StateMessage { Message = CpuCardStatus.Anomalia.Label, State = CpuCardStatus.Anomalia.State });
                if (CpuCardStatus.DisattivaPerErroreInterno.Value) this.StateMessages.Add(new StateMessage { Message = CpuCardStatus.DisattivaPerErroreInterno.Label, State = CpuCardStatus.DisattivaPerErroreInterno.State });
                if (CpuCardStatus.DiversaDaIntallazioneRemota.Value) this.StateMessages.Add(new StateMessage { Message = CpuCardStatus.DiversaDaIntallazioneRemota.Label, State = CpuCardStatus.DiversaDaIntallazioneRemota.State });

            }

        }

    }

    public partial class FdsInterfaceCardStatusBits : ValueWithState<long>
    {
        public ValueWithState<bool> CardFailure { get; private set; }
        public ValueWithState<bool> CardWarning { get; private set; }
        public ValueWithState<bool> BusError { get; private set; }
        public ValueWithState<bool> V5Error { get; private set; }
        public ValueWithState<bool> V24Error { get; private set; }
        public ValueWithState<bool> LinkPCMNonAttivo { get; private set; }
        public ValueWithState<bool> F1On { get; private set; }
        public ValueWithState<bool> F2On { get; private set; }

        public FdsInterfaceCardStatusBits()
        {
            FormatString = "0x{0:X4}";

            CardFailure = new ValueWithState<bool> { Label = "Scheda in errore", VisibleValue = false,  ErrorValue = true };
            CardWarning = new ValueWithState<bool> { Label = "Scheda in allarme", VisibleValue = false,  WarningValue = true };

            BusError = new ValueWithState<bool> { Label = "Stato Bus PCM", VisibleValue = false,  ErrorValue = true };
            V5Error = new ValueWithState<bool> { Label = "Tensione Alim. +5V", VisibleValue = false,  ErrorValue = true };
            V24Error = new ValueWithState<bool> { Label = "Tensione Alim +24V", VisibleValue = false,  ErrorValue = true };
            LinkPCMNonAttivo = new ValueWithState<bool>
                                   {
                                       Label = "Collegamento PCM scheda",
                                       VisibleValue = false,
                                       StateMessageRules = new[]
                                                               { new StateMessageRule<bool>
                                                                     {
                                                                        Message = new StateMessage {Message = "disabilitato", State = StateEnum.Warning},
                                                                        Rule = (x => x.Value)
                                                                     }
                                                                 ,
                                                                 new StateMessageRule<bool>
                                                                     {
                                                                        Message = new StateMessage {Message = "abilitato", State = StateEnum.Ok},
                                                                        Rule = (x => !x.Value)
                                                                     }
                                                                }
                                   };
            F1On = new ValueWithState<bool> { Label = "F1 ON" };
            F2On = new ValueWithState<bool> { Label = "F2 ON" };
        }

        protected override void OnRefreshState()
        {
            //base.OnRefreshState();

            this.State = GetMaxState(this.Childs.ToArray(), true, this.State, new List<ObjectWithState> { this.CardFailure, this.CardWarning });
        }

    }

    public partial class FdsInterfaceCardFD60X00StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> V12Error { get; private set; }
        public ValueWithState<bool> MV12Error { get; private set; }
        public ValueWithState<bool> FrameError { get; private set; }

        public FdsInterfaceCardFD60X00StatusBits()
        {
            V12Error = new ValueWithState<bool> { Label = "Tensione Alim. +12V", ErrorValue = true, VisibleValue = false };
            MV12Error = new ValueWithState<bool> { Label = "Tensione Alim. -12V", ErrorValue = true, VisibleValue = false };
            FrameError = new ValueWithState<bool> { Label = "Collegamento periferiche", ErrorValue = true, VisibleValue = false };
        }

    }

    public partial class FdsInterfaceCardFD40100StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> V12Error { get; private set; }
        public ValueWithState<bool> MV12Error { get; private set; }
        public ValueWithState<bool> AICError { get; private set; }
        public ValueWithState<bool> FrameError { get; private set; }
        public ValueWithState<bool> XCall { get; private set; }
        public ValueWithState<bool> YCall { get; private set; }

        public FdsInterfaceCardFD40100StatusBits()
        {
            V12Error = new ValueWithState<bool> { Label = "Tensione Alim. +12V", ErrorValue = true, VisibleValue = false };
            MV12Error = new ValueWithState<bool> { Label = "Tensione Alim. -12V", ErrorValue = true, VisibleValue = false };
            AICError = new ValueWithState<bool> { Label = "Stato codec", ErrorValue = true, VisibleValue = false };
            FrameError = new ValueWithState<bool> { Label = "Collegamento periferiche", ErrorValue = true, VisibleValue = false };
            XCall = new ValueWithState<bool> { Label = "Errore X Call" };
            YCall = new ValueWithState<bool> { Label = "Errore Y Call" };
        }

    }

    public partial class FdsInterfaceCardFD39200StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> V12Error { get; private set; }
        public ValueWithState<bool> MV12Error { get; private set; }
        public ValueWithState<bool> AlimentatoreASDEAbilitato { get; private set; }
        public ValueWithState<bool> FrameError { get; private set; }
        public ValueWithState<RelazioneASDEEnum> RelazioneASDE { get; private set; }
        public ValueWithState<Byte> CorrenteASDE { get; private set; }


        public FdsInterfaceCardFD39200StatusBits()
        {
            V12Error = new ValueWithState<bool> { Label = "Tensione Alim. +12V", ErrorValue = true, VisibleValue = false};
            MV12Error = new ValueWithState<bool> { Label = "Tensione Alim. -12V", ErrorValue = true, VisibleValue = false };
            AlimentatoreASDEAbilitato = new ValueWithState<bool> { Label = "Alimentatore ASDE abilitato", VisibleValue = false };
            FrameError = new ValueWithState<bool> { Label = "Collegamento periferiche", ErrorValue = true, VisibleValue = false };
            
            RelazioneASDE = new ValueWithState<RelazioneASDEEnum> 
            {
                Label = "Relazione ASDE",
                VisibleValue = false,
                StateMessageRules = new[]
                { new StateMessageRule<RelazioneASDEEnum>
                    {
                    Message = new StateMessage {Message = "Aperta in attesa di ripristino", State = StateEnum.Warning},
                    Rule = (x => x.Value == RelazioneASDEEnum.ApertaInAttesaRipristino)
                    }
                ,
                new StateMessageRule<RelazioneASDEEnum>
                    {
                    Message = new StateMessage {Message = "In auto richiusura", State = StateEnum.Warning},
                    Rule = (x => x.Value == RelazioneASDEEnum.ApertaInRichiusura)
                    }
                ,
                new StateMessageRule<RelazioneASDEEnum>
                    {
                    Message = new StateMessage {Message = "Attiva", State = StateEnum.Ok},
                    Rule = (x => x.Value == RelazioneASDEEnum.Attiva)
                    }
                }
            };
            CorrenteASDE = new ValueWithState<Byte>
                           {
                               Label = "Corrente ASDE",
                               VisibleValue = true,
                               FormatString = "{0} mA",
                               StateMessageRules = new[]
                               {
                                   new StateMessageRule<byte>
                                       {
                                           Rule = (x => x.Value == 63),
                                           Message = new StateMessage { Message = "cortocircuito", State = StateEnum.Error } 
                                       },
                                   new StateMessageRule<byte>
                                       {
                                           Rule = (x => x.Value == 0),
                                           Message = new StateMessage { Message = "corrente nulla", State = StateEnum.Warning }
                                       },
                                   new StateMessageRule<byte>
                                       {
                                           Rule = (x => x.Value != 0 && x.Value != 63),
                                           Message = new StateMessage { Message = "ok", State = StateEnum.Ok }
                                       },
                               }
                           };
        }
    }

    public partial class FdsInterfaceCardFD20000StatusBits : FdsInterfaceCardStatusBits
    {
        public ValueWithState<bool> LocalSyncError { get; private set; }
        public ValueWithState<bool> RemoteSyncError { get; private set; }

        public FdsInterfaceCardFD20000StatusBits()
        {
            LocalSyncError = new ValueWithState<bool>
            {
                Label = "Sincronismo locale",
                VisibleValue = false,
                StateMessageRules = new[] 
                {
                   new StateMessageRule<bool>
                       {
                           Rule = (x => x.Value),
                           Message = new StateMessage { Message = "assente", State = StateEnum.Warning } 
                       },
                   new StateMessageRule<bool>
                       {
                           Rule = (x => !x.Value),
                           Message = new StateMessage { Message = "presente", State = StateEnum.Ok }
                       },
                }

            };

            RemoteSyncError = new ValueWithState<bool>
            {
                Label = "Sincronismo remoto",
                VisibleValue = false,
              StateMessageRules = new[] 
              {
                   new StateMessageRule<bool>
                       {
                           Rule = (x => x.Value),
                           Message = new StateMessage { Message = "assente", State = StateEnum.Warning } 
                       },
                   new StateMessageRule<bool>
                       {
                           Rule = (x => !x.Value),
                           Message = new StateMessage { Message = "presente", State = StateEnum.Ok }
                       },
               }
            };
        }

        protected override void OnRefreshState()
        {
            base.OnRefreshState();
        }
    }

    #endregion

    #region Stream MapB2O

    public partial class FdsInterfacesStatusStreamMapB2O : IDeviceStreamMapB2O
    {
        private readonly FdsDevice fdsDevice;
        private readonly TipoFDS fdsTipo;

        public FdsInterfacesStatusStreamMapB2O(FdsDevice fdsDevice, TipoFDS fdsType)
        {
            this.fdsDevice = fdsDevice;
            this.fdsTipo = fdsType;
        }

        #region IDeviceStreamMapB2O<FdsInterfaceCardStatusStream> Members

        public FdsInterfacesStatusStream GetStream(byte[] data)
        {
            var bs = BinaryHelper<FdsInterfacesStatusStruct>.RawStringBytesDeserialize(data);
            FdsInterfaceCardStatusStruct[] localCards = bs.LocalCards;
            FdsInterfaceCardStatusStruct[] remoteCards = bs.RemoteCards;

            var stream = new FdsInterfacesStatusStream();
            int[] indexMap = FdsConfigHelper.GetInterfacesIndexes(this.fdsTipo);
            
            int i = 0;
            foreach (FdsInterfaceCardStatusStruct card in localCards)
            {
                var cardStatus = new FdsInterfaceCardStatusStream();

                FdsCardStatusStream cardInfoFromCpu =  fdsDevice.GeneralStatus.LocalSystemStatus.Devices[i];
                cardStatus.CardCode.Value = cardInfoFromCpu.CardCode.Value;

                if (cardInfoFromCpu.CardStatus != null)
                {
                    cardStatus.CpuCardStatus = cardInfoFromCpu.CardStatus;

                    if (cardInfoFromCpu.CardStatus.NonRilevataLocale.Value ||
                        cardInfoFromCpu.CardStatus.NonRilevataRemoto.Value)
                    {
                        cardStatus.CardCode.Value = FdsInterfaceCardTypes.Unknown;
                    }
                }

                cardStatus.CardCode.Description = DecodeCardCodeDescription(cardStatus.CardCode.Value);

                SetCardStatus(card.CardStatus, cardStatus.CardStatus);

                cardStatus.Index = indexMap[i++];

                if (cardStatus.Index > 0)
                {
                    stream.LocalCards.Add(cardStatus);
                }
            }

            i = 0;
            foreach (FdsInterfaceCardStatusStruct card in remoteCards)
            {
                var cardStatus = new FdsInterfaceCardStatusStream();

                FdsCardStatusStream cardInfoFromCpu = fdsDevice.GeneralStatus.RemoteSystemStatus.Devices[i];
                cardStatus.CardCode.Value = cardInfoFromCpu.CardCode.Value;

                if (cardInfoFromCpu.CardStatus != null)
                {
                    cardStatus.CpuCardStatus = cardInfoFromCpu.CardStatus;

                    if (cardInfoFromCpu.CardStatus.NonRilevataLocale.Value ||
                        cardInfoFromCpu.CardStatus.NonRilevataRemoto.Value)
                    {
                        cardStatus.CardCode.Value = FdsInterfaceCardTypes.Unknown;
                    }
                }

                cardStatus.CardCode.Description = DecodeCardCodeDescription(cardStatus.CardCode.Value);

                SetCardStatus(card.CardStatus, cardStatus.CardStatus);

                cardStatus.Index = indexMap[i++];

                if (cardStatus.Index > 0)
                {
                    stream.RemoteCards.Add(cardStatus);
                }
            }

            return stream;

        }

        #endregion

        private static FdsInterfaceCardTypes DecodeCardCode(byte code)
        {
            object ret;
            try
            {
                ret = Enum.ToObject(typeof(FdsInterfaceCardTypes), code);
            }
            catch (ArgumentException)
            {
                ret = FdsInterfaceCardTypes.SlotEmpty;
            }
            return (FdsInterfaceCardTypes)ret;
        }

        private static string DecodeCardCodeDescription(FdsInterfaceCardTypes cardTypes)
        {
            switch (cardTypes)
            {
                case FdsInterfaceCardTypes.FD20000_ConsoleDTS:
                    return "Interfaccia console DTS";
                case FdsInterfaceCardTypes.FD39200_ADSE:
                    return "Interfaccia ASDE";
                case FdsInterfaceCardTypes.FD40100_Telecomando:
                    return "Interfaccia telecomando";
                case FdsInterfaceCardTypes.FD60X00_BCA:
                    return "Interfaccia BCA";
                case FdsInterfaceCardTypes.Unknown:
                    return "Non rilevato";
                default:
                    return string.Empty;
            }
        }

        private static void SetCardStatus(uint intCardStatus, FdsInterfaceCardStatusBits cardStatus)
        {
            if (cardStatus == null) return;


            cardStatus.CardFailure.Value = intCardStatus.BitIsTrue(27);
            cardStatus.CardWarning.Value = intCardStatus.BitIsTrue(28);

            cardStatus.BusError.Value = intCardStatus.BitIsTrue(23);
            cardStatus.V5Error.Value = intCardStatus.BitIsTrue(22);
            cardStatus.V24Error.Value = intCardStatus.BitIsTrue(21);
            cardStatus.LinkPCMNonAttivo.Value = intCardStatus.BitIsTrue(15);
            cardStatus.F1On.Value = intCardStatus.BitIsTrue(14);
            cardStatus.F2On.Value = intCardStatus.BitIsTrue(13);

            if (cardStatus is FdsInterfaceCardFD20000StatusBits)
            {
                var cs = (FdsInterfaceCardFD20000StatusBits)cardStatus;
                cs.LocalSyncError.Value = intCardStatus.BitIsTrue(20);
                cs.RemoteSyncError.Value = intCardStatus.BitIsTrue(19);
            }
            else if (cardStatus is FdsInterfaceCardFD39200StatusBits)
            {
                var cs = (FdsInterfaceCardFD39200StatusBits)cardStatus;
                cs.V12Error.Value = intCardStatus.BitIsTrue(20);
                cs.MV12Error.Value = intCardStatus.BitIsTrue(19);
                cs.AlimentatoreASDEAbilitato.Value = intCardStatus.BitIsTrue(12);
                cs.FrameError.Value = intCardStatus.BitIsTrue(10);  // collegamento tra periferiche
                if (!intCardStatus.BitIsTrue(18))
                {
                    cs.RelazioneASDE.Value = RelazioneASDEEnum.Attiva;
                }
                else
                {
                    cs.RelazioneASDE.Value = !intCardStatus.BitIsTrue(11)
                                                 ? RelazioneASDEEnum.ApertaInRichiusura
                                                 : RelazioneASDEEnum.ApertaInAttesaRipristino;
                }

                cs.CorrenteASDE.Value = (byte)intCardStatus.GetBitsValue(2, 7);
            }
            else if (cardStatus is FdsInterfaceCardFD40100StatusBits)
            {
                var cs = (FdsInterfaceCardFD40100StatusBits)cardStatus;
                cs.V12Error.Value = intCardStatus.BitIsTrue(20);
                cs.MV12Error.Value = intCardStatus.BitIsTrue(19);
                cs.FrameError.Value = intCardStatus.BitIsTrue(12);
                cs.AICError.Value = intCardStatus.BitIsTrue(18);
                cs.XCall.Value = intCardStatus.BitIsTrue(11);
                cs.YCall.Value = intCardStatus.BitIsTrue(10);
            }
            else if (cardStatus is FdsInterfaceCardFD60X00StatusBits)
            {
                var cs = (FdsInterfaceCardFD60X00StatusBits)cardStatus;
                cs.V12Error.Value = intCardStatus.BitIsTrue(20);
                cs.MV12Error.Value = intCardStatus.BitIsTrue(19);
                cs.FrameError.Value = intCardStatus.BitIsTrue(18);
            }
        }

        #region IDeviceStreamMapB2O Members

        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
        {
            return (IObjectWithState)GetStream(data);
        }

        #endregion
    }

    #endregion

}

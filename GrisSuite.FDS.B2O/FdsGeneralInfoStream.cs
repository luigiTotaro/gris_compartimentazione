﻿using System;
using System.Runtime.InteropServices;
using GrisSuite.Common;

namespace GrisSuite.FDS.B2O
{
    #region Data Struct

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsGeneralInfoStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        internal Byte[] Filler1;

        internal FdsGeneralInfoPartStruct Local;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
        internal Byte[] Filler2;

        internal FdsGeneralInfoPartStruct Remote;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsGeneralInfoPartStruct
    {
        internal Byte Filler1;
        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        internal Byte[] FirmwareUserVerCPU;

        internal Byte Filler2;
        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        internal Byte[] FirmwareKernelVerCPU;

        internal Byte Filler3;
    };

    #endregion

    #region Data Stream

    public partial class FdsGeneralInfoStream
    {
        public FdsGeneralInfoPartStream Local;
        public FdsGeneralInfoPartStream Remote;

        public FdsGeneralInfoStream()
        {
            this.Local = new FdsGeneralInfoPartStream();
            this.Remote = new FdsGeneralInfoPartStream();
            
        }
    }

    public partial class FdsGeneralInfoPartStream
    {
        public string FirmwareUserVerCPU;
        public string FirmwareKernelVerCPU;
    };

    #endregion

    #region Stream MapB2O

    public partial class FdsGeneralInfoStreamMapB2O : IDeviceStreamMapB2O
    {
        private readonly FdsDevice fdsDevice;
        private readonly TipoFDS fdsTipo;

        public FdsGeneralInfoStreamMapB2O(FdsDevice fdsDevice, TipoFDS fdsType)
        {
            this.fdsDevice = fdsDevice;
            this.fdsTipo = fdsType;
        }

        #region IDeviceStreamMapB2O<FdsInterfaceCardStatusStream> Members

        public FdsGeneralInfoStream GetStream(byte[] data)
        {
            FdsGeneralInfoStruct bs = BinaryHelper<FdsGeneralInfoStruct>.RawDeserialize(data);
            FdsGeneralInfoStream stream = new FdsGeneralInfoStream();

            System.Text.Encoding enc = System.Text.Encoding.ASCII;

            stream.Local.FirmwareKernelVerCPU = enc.GetString(bs.Local.FirmwareKernelVerCPU);
            stream.Local.FirmwareUserVerCPU = enc.GetString(bs.Local.FirmwareUserVerCPU);
            
            stream.Remote.FirmwareKernelVerCPU = enc.GetString(bs.Local.FirmwareKernelVerCPU);
            stream.Remote.FirmwareUserVerCPU = enc.GetString(bs.Local.FirmwareUserVerCPU);

            return stream;

        }

        #endregion

        #region IDeviceStreamMapB2O Members

        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
        {
            return (IObjectWithState)GetStream(data);
        }

        #endregion
    }

    #endregion
}

﻿using System;
using System.Runtime.InteropServices;
using GrisSuite.Common;

namespace GrisSuite.FDS.B2O
{
    #region Data Struct

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsCPUsMeasureStruct
    {
        internal FdsMeasureStruct Local;
        internal FdsMeasureStruct Remote;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct FdsMeasureStruct
    {
        internal Int16 Power24V;
        internal Int16 Power12V;
        internal Int16 PowerVCC;
        internal Int16 PowerM12V;
        internal Int16 FiberVolt;
        internal Int16 Temperature;
    }

    #endregion

    #region Data Stream

    public partial class FdsCPUsMeasureStream : ObjectWithState
    {
        public FdsMeasureStream Local { get; set; }
        public FdsMeasureStream Remote { get; set; }

        public FdsCPUsMeasureStream(ObjectWithState root) : base(root)
        {
            if ( !(root is FdsDevice) )
            {
                throw new ArgumentException("Root must of type FdsDevice", "root");
            }
            this.Local = new FdsMeasureStream(root);
            this.Remote = new FdsMeasureStream(root);
        }

    }

    public partial class FdsMeasureStream : ObjectWithState
    {
        public ValueWithState<decimal> Power24V { get; set; }
        public ValueWithState<decimal> Power12V { get; set; }
        public ValueWithState<decimal> Power5V { get; set; }
        public ValueWithState<decimal> PowerM12V { get; set; }
        public ValueWithState<decimal?> FiberVolt { get; set; }
        public ValueWithState<decimal> Temperature { get; set; }

        public FdsMeasureStream(ObjectWithState root)
        {
            this.Power24V = new ValueWithState<decimal>
                                {
                Label = "Tensione +24V",
                FormatString = "{0:+####0.00;-####0.00} V"
            };

            this.Power12V = new ValueWithState<decimal>
                                {
                Label = "Tensione +12V",
                FormatString = "{0:+####0.00;-####0.00} V"
            };

            this.Power5V = new ValueWithState<decimal>
                               {
                Label = "Tensione +5V",
                FormatString = "{0:+####0.00;-####0.00} V"
            };

            this.PowerM12V = new ValueWithState<decimal>
                                 {
                Label = "Tensione -12V",
                FormatString = "{0:+####0.00;-####0.00} V"
            };


            this.FiberVolt = new ValueWithState<decimal?>(root)
            {
                Label = "Tensione trasduttore",
                FormatString = "{0:+####0.00;-####0.00} V"
            };

            this.Temperature = new ValueWithState<decimal>
                                   {
                Label = "Temperatura",
                FormatString = "{0:+####0.0;-####0.0} °C"
            };
        }
    }

    #endregion

    #region Stream MapB2O

    public partial class FdsCPUsMeasureStreamMapB2O : IDeviceStreamMapB2O
    {
        private readonly FdsDevice fdsDevice;
        private readonly TipoFDS fdsTipo;

        public FdsCPUsMeasureStreamMapB2O(FdsDevice fdsDevice, TipoFDS fdsType)
        {
            this.fdsDevice = fdsDevice;
            this.fdsTipo = fdsType;
        }

        #region IDeviceStreamMapB2O<FdsMeasureStream> Members

        public FdsCPUsMeasureStream GetStream(byte[] data)
        {
            FdsCPUsMeasureStream stream = new FdsCPUsMeasureStream(fdsDevice);
            FdsCPUsMeasureStruct bs = BinaryHelper<FdsCPUsMeasureStruct>.RawStringBytesDeserialize(data);

            OnPreGetStream(bs, stream);
            
            SetFdsMeasure(bs.Local, stream.Local);

            SetFdsMeasure(bs.Remote, stream.Remote);

            OnPostGetStream(bs, stream);

            return stream;
        }

        partial void OnPreGetStream(FdsCPUsMeasureStruct bs, FdsCPUsMeasureStream stream);
        partial void OnPostGetStream(FdsCPUsMeasureStruct bs, FdsCPUsMeasureStream stream);

        #endregion

        private void SetFdsMeasure(FdsMeasureStruct measureStruct, FdsMeasureStream measureStream)
        {
            measureStream.Power24V.Value = measureStruct.Power24V * 0.001m;
            measureStream.Power12V.Value = measureStruct.Power12V * 0.001m;
            measureStream.Power5V.Value = measureStruct.PowerVCC * 0.001m;
            measureStream.PowerM12V.Value = measureStruct.PowerM12V * 0.001m;
            measureStream.FiberVolt.Value = measureStruct.FiberVolt * 0.001m;
            measureStream.Temperature.Value = measureStruct.Temperature;
        }

        #region IDeviceStreamMapB2O Members

        IObjectWithState IDeviceStreamMapB2O.GetStream(byte[] data)
        {
            return (IObjectWithState)GetStream(data);
        }

        #endregion
    }

    #endregion
}

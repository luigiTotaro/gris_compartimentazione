﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrisSuite.FDS.B2O
{
    public partial class FdsCPUsMeasureStreamMapB2O
    {
        partial void OnPostGetStream(FdsCPUsMeasureStruct bs, FdsCPUsMeasureStream stream)
        {
            if (((FdsDevice)stream.Root).GeneralStatus.LocalSystemStatus.StatusFlags.TipoInterfacciaCollegamento.Value != OtticaG703Enum.Ottica)
            {
                stream.Local.FiberVolt.Value = null;
            }

            if (((FdsDevice)stream.Root).GeneralStatus.RemoteSystemStatus.StatusFlags.TipoInterfacciaCollegamento.Value != OtticaG703Enum.Ottica)
            {
                stream.Remote.FiberVolt.Value = null;
            }
        }
    }
}

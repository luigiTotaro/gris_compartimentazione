﻿USE [master]
GO

IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'Telefin')
BEGIN
	DROP DATABASE [Telefin]
END

CREATE DATABASE [Telefin] ON  PRIMARY 
( NAME = N'Telefin', FILENAME = N'C:\Program Files\Telefin\DB\Telefin.mdf' , SIZE = 2240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Telefin_log', FILENAME = N'C:\Program Files\Telefin\DB\Telefin_log.LDF' , SIZE = 39936KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
 COLLATE Latin1_General_CI_AS
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'Telefin', @new_cmptlevel=90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Telefin].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Telefin] SET ANSI_NULL_DEFAULT ON 
GO
ALTER DATABASE [Telefin] SET ANSI_NULLS ON 
GO
ALTER DATABASE [Telefin] SET ANSI_PADDING ON 
GO
ALTER DATABASE [Telefin] SET ANSI_WARNINGS ON 
GO
ALTER DATABASE [Telefin] SET ARITHABORT ON 
GO
ALTER DATABASE [Telefin] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Telefin] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Telefin] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Telefin] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Telefin] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Telefin] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [Telefin] SET CONCAT_NULL_YIELDS_NULL ON 
GO
ALTER DATABASE [Telefin] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Telefin] SET QUOTED_IDENTIFIER ON 
GO
ALTER DATABASE [Telefin] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Telefin] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Telefin] SET AUTO_UPDATE_STATISTICS_ASYNC ON 
GO
ALTER DATABASE [Telefin] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Telefin] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Telefin] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Telefin] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Telefin] SET  READ_WRITE 
GO
ALTER DATABASE [Telefin] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Telefin] SET PAGE_VERIFY NONE  
GO
ALTER DATABASE [Telefin] SET DB_CHAINING OFF 
GO
USE [Telefin]
GO

/*************************************** Users Creation and Permissions ******************************************/
DECLARE @SrvName varchar(100)
select @SrvName = CONVERT(varchar(100), SERVERPROPERTY('ServerName'))
select @SrvName = substring(@SrvName, 0, charindex('\', @SrvName))

DECLARE @scriptAdmins NVARCHAR(100)
DECLARE @scriptUsers NVARCHAR(100)
DECLARE @scriptAppServ NVARCHAR(100)

SET @scriptAdmins = 'CREATE USER [SWEBAdmins] FOR LOGIN [' + @SrvName + '\STLC1000SWMAdmins]'
SET @scriptUsers = 'CREATE USER [SWEBUsers] FOR LOGIN [' + @SrvName + '\STLC1000SWMUsers]'
SET @scriptAppServ = 'CREATE USER [STLCAppServices] FOR LOGIN [' + @SrvName + '\STLC1000AppServices]'

/****** Object:  User [SWEBUsers]    Script Date: 01/19/2008 11:39:41 ******/
EXEC sp_executesql @scriptUsers
/****** Object:  User [SWEBAdmins]    Script Date: 01/19/2008 11:39:41 ******/
EXEC sp_executesql @scriptAdmins
/****** Object:  User [STLCAppServices]    Script Date: 01/19/2008 11:39:41 ******/
EXEC sp_executesql @scriptAppServ
GO

GRANT CONNECT TO [STLCAppServices]
GO
GRANT EXECUTE TO [STLCAppServices]
GO
GRANT CONNECT TO [SWEBAdmins]
GO
GRANT EXECUTE TO [SWEBAdmins]
GO
GRANT CONNECT TO [SWEBUsers]
GO
GRANT EXECUTE TO [SWEBUsers]
GO


USE [Telefin]
GO
EXEC sp_addrolemember N'db_datawriter', N'STLCAppServices'
GO
EXEC sp_addrolemember N'db_datareader', N'STLCAppServices'
GO
EXEC sp_addrolemember N'db_datawriter', N'SWEBAdmins'
GO
EXEC sp_addrolemember N'db_datareader', N'SWEBAdmins'
GO
EXEC sp_addrolemember N'db_datareader', N'SWEBUsers'
GO
/*************************************** END Users Creation and Permissions ******************************************/

﻿/**************************************************** STATIC DATA POPULATION **************************************************/
USE Telefin
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

GRANT UPDATE ON [dbo].[servers] TO [SWEBUsers]
GO

BEGIN TRANSACTION

INSERT INTO [dbo].[stlc_parameters] ([ParameterName], [ParameterValue], [ParameterDescription]) VALUES ('DBVersion', '2.0.7.0', 'La versione del DataBase')

INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (0, 'Informazione')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (1, 'Avviso')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (2, 'Errore')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (255, 'Sconosciuta')
INSERT INTO [dbo].[severity] ([SevLevel], [Description]) VALUES (9, 'Da configurare')

INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription]) VALUES (1, 'Produttore di default', NULL)

INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription]) VALUES (1, 'Sistema di Default')

COMMIT TRANSACTION


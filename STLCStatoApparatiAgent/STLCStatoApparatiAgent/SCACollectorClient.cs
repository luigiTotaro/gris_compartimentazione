﻿using System;
using System.Net;
using GrisSuite.AnsaldoCollectorWS;
using GrisSuite.Properties;

namespace GrisSuite
{
	internal class SCACollectorClient
	{
		internal SCACollectorClient()
		{
			this.IPVirtualePP = null;

			if (Settings.Default.DisableServerCertificateValidation)
			{
				// evita gli errori di trust su chiamate SSL
				ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;
			}
		}

		public int SrvId { get; set; }

		/// <summary>
		///     IP Virtuale del Posto Periferico
		/// </summary>
		public string IPVirtualePP { get; set; }

		/// <summary>
		///     Identificativo del posto periferico (SISCT)
		/// </summary>
		/// <remarks>
		///     Corrisponde al NodeID di periferia
		/// </remarks>
		internal static ushort DecodeNodeID(long nodId)
		{
			if (nodId > 0)
			{
				ushort originalNodeId;
				ushort originalZoneId;
				ushort originalRegionId;

				Util.DecodeNodeID((ulong) nodId, out originalNodeId, out originalZoneId, out originalRegionId);

				return originalNodeId;
			}

			return 0;
		}

		#region AnsaldoCollector Method Caller

		internal lifeResponseData Life(long nodeId)
		{
			DefaultStatoApparatiClient sac = null;

			try
			{
				if ((nodeId > 0) && (!string.IsNullOrEmpty(this.IPVirtualePP)))
				{
					sac = new DefaultStatoApparatiClient();
					lifeResponseData lrd = sac.Life(DecodeNodeID(nodeId), this.IPVirtualePP, LocalUtil.CurrentTimeMillis());

					return lrd;
				}

				if (nodeId <= 0)
				{
					LocalUtil.WriteDebugLogToFile("Warning: Parameter IdPP is empty (less than or zero). Unable to send Life message.");
				}

				if (string.IsNullOrEmpty(this.IPVirtualePP))
				{
					LocalUtil.WriteDebugLogToFile("Warning: Parameter IPVirtualePP is empty. Unable to send Life message.");
				}
			}
			catch (Exception ex)
			{
				LocalUtil.WriteDebugLogToFile(string.Format("Warning: Exception in Life method.\r\n{0}", ex.Message));
			}
			finally
			{
				if (sac != null)
				{
					sac.Close();
				}
			}

			return null;
		}

		internal variazioneResponseData Variazione(long nodeId, string descrizioneGuasto, bool flagGuasto, string idApparato, int livelloGravita)
		{
			DefaultStatoApparatiClient sac = null;

			try
			{
				if ((nodeId > 0) && (!string.IsNullOrEmpty(this.IPVirtualePP)))
				{
					sac = new DefaultStatoApparatiClient();
					variazioneResponseData vrd = sac.Variazione(DecodeNodeID(nodeId), this.IPVirtualePP, idApparato, descrizioneGuasto, flagGuasto, livelloGravita);

					return vrd;
				}

				if (nodeId <= 0)
				{
					LocalUtil.WriteDebugLogToFile("Warning: Parameter IdPP is empty (less than or zero). Unable to send Variazione message.");
				}

				if (string.IsNullOrEmpty(this.IPVirtualePP))
				{
					LocalUtil.WriteDebugLogToFile("Warning: Parameter IPVirtualePP is empty. Unable to send Variazione message.");
				}
			}
			catch (Exception ex)
			{
				LocalUtil.WriteDebugLogToFile(string.Format("Warning: Exception in Variazione method.\r\n{0}", ex.Message));
			}
			finally
			{
				if (sac != null)
				{
					sac.Close();
				}
			}

			return null;
		}

		internal statoTotaleResponseData StatoTotale(long nodeId, datoStatoTotale[] dati)
		{
			DefaultStatoApparatiClient sac = null;

			try
			{
				if ((nodeId > 0) && (!string.IsNullOrEmpty(this.IPVirtualePP)))
				{
					sac = new DefaultStatoApparatiClient();
					statoTotaleResponseData strd = sac.StatoTotale(DecodeNodeID(nodeId), this.IPVirtualePP, dati);

					return strd;
				}

				if (nodeId <= 0)
				{
					LocalUtil.WriteDebugLogToFile("Warning: Parameter IdPP is empty (less than or zero). Unable to send StatoTotale message.");
				}

				if (string.IsNullOrEmpty(this.IPVirtualePP))
				{
					LocalUtil.WriteDebugLogToFile("Warning: Parameter IPVirtualePP is empty. Unable to send StatoTotale message.");
				}
			}
			catch (Exception ex)
			{
				LocalUtil.WriteDebugLogToFile(string.Format("Warning: Exception in StatoTotale method.\r\n{0}", ex.Message));
			}
			finally
			{
				if (sac != null)
				{
					sac.Close();
				}
			}

			return null;
		}

		#endregion
	}
}
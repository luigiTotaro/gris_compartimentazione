﻿using System;
using GrisSuite.Data;
using GrisSuite.Properties;

namespace GrisSuite
{
	internal class AnsaldoMessageBuilder
	{
		internal class AnsaldoMessageBuilderData
		{
			public AnsaldoMessageBuilderData(string monitoringDeviceId)
			{
				this.DescrizioneGuasto = string.Empty;
				this.FlagGuasto = false;
				this.IdApparato = monitoringDeviceId;
				this.LivelloGravita = 0;
				this.NodeId = 0;
			}

			public string DescrizioneGuasto { get; private set; }

			public bool FlagGuasto { get; private set; }

			public string IdApparato { get; private set; }

			public int LivelloGravita { get; private set; }

			public long NodeId { get; private set; }

			public void Populate(long nodeId,
				long deviceId,
				string deviceStatusDescription,
				int deviceSevLevel,
				dsDeviceStatus.StreamsDataTable streams,
				dsDeviceStatus.StreamFieldsDataTable streamFields)
			{
				this.NodeId = nodeId;

				// A prescindere dalla severità della periferica, la descrizione minima è quella della device_status
				this.DescrizioneGuasto = deviceStatusDescription;

				this.FlagGuasto = IsDeviceSevLevelGuasto(deviceSevLevel);

				this.LivelloGravita = deviceSevLevel;

				switch (this.GetDescrizioneGuastoKindFromSettings())
				{
					case DescrizioneGuastoKind.DeviceStatus:
						// La descrizione dello stato deve contenere solo lo stato della periferica, nulla da fare
						break;
					case DescrizioneGuastoKind.DeviceStatusAndStream:
						// Può capitare che la periferica sia in severità ok, ma che contenga stream / stream fields con guasti, li ignoriamo per coerenza
						if (this.FlagGuasto)
						{
							// La descrizione dello stato deve contenere la lista degli stream in stato di guasto
							foreach (dsDeviceStatus.StreamsRow streamRow in streams.Rows)
							{
								if ((streamRow.DevID == deviceId) && (IsStreamSevLevelGuasto(streamRow.SevLevel)))
								{
									this.DescrizioneGuasto = string.Format("{0},{1}", this.DescrizioneGuasto, streamRow.Name);
								}
							}
						}
						break;
					case DescrizioneGuastoKind.DeviceStatusAndStreamFields:
						// Può capitare che la periferica sia in severità ok, ma che contenga stream / stream fields con guasti, li ignoriamo per coerenza
						if (this.FlagGuasto)
						{
							// La descrizione dello stato deve contenere la lista degli stream fields in stato di guasto
							foreach (dsDeviceStatus.StreamFieldsRow streamFieldRow in streamFields.Rows)
							{
								if ((streamFieldRow.DevID == deviceId) && IsStreamSevLevelGuasto(streamFieldRow.SevLevel))
								{
									this.DescrizioneGuasto = string.Format("{0},{1}", this.DescrizioneGuasto, streamFieldRow.Name);
								}
							}
						}
						break;
				}
			}

			public enum DescrizioneGuastoKind
			{
				DeviceStatus,
				DeviceStatusAndStream,
				DeviceStatusAndStreamFields
			}

			private DescrizioneGuastoKind GetDescrizioneGuastoKindFromSettings()
			{
				if (Settings.Default.DescrizioneGuastoModel.Equals("DeviceStatus", StringComparison.OrdinalIgnoreCase))
				{
					return DescrizioneGuastoKind.DeviceStatus;
				}

				if (Settings.Default.DescrizioneGuastoModel.Equals("DeviceStatusAndStream", StringComparison.OrdinalIgnoreCase))
				{
					return DescrizioneGuastoKind.DeviceStatusAndStream;
				}

				if (Settings.Default.DescrizioneGuastoModel.Equals("DeviceStatusAndStreamFields", StringComparison.OrdinalIgnoreCase))
				{
					return DescrizioneGuastoKind.DeviceStatusAndStreamFields;
				}

				return DescrizioneGuastoKind.DeviceStatus;
			}
		}

		public static bool IsDeviceSevLevelGuasto(int sevLevel)
		{
			// Mappa le severità Gris sul concetto di Guasto di Ansaldo
			switch (sevLevel)
			{
				case -255: // Informazione
					return false;
				case -1: //	Non classificato
					return false;
				case 0: // In servizio
					return false;
				case 1: // Anomalia lieve
					return true;
				case 2: // Anomalia grave
					return true;
				case 3: // Offline
					return true;
				case 9: //Non attivo
					return true;
				case 255: //Sconosciuto
					return true;
				default:
					return false;
			}
		}

		public static bool IsStreamSevLevelGuasto(int sevLevel)
		{
			// Mappa le severità Gris sul concetto di Guasto di Ansaldo
			switch (sevLevel)
			{
				case -255: // Informazione
					return false;
				case -1: //	Non classificato
					return false;
				case 0: // In servizio
					return false;
				case 1: // Anomalia lieve
					return true;
				case 2: // Anomalia grave
					return true;
				case 3: // Offline
					return true;
				case 9: //Non attivo
					return false;
				case 255: //Sconosciuto
					return false;
				default:
					return false;
			}
		}
	}
}
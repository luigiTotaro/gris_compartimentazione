using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Runtime.InteropServices;
using System.Web;
using GrisSuite.Properties;
using Microsoft.Win32;

namespace GrisSuite
{
	public class LocalUtil
	{
		private static readonly string _logSource = AppDomain.CurrentDomain.FriendlyName;

		public static void WriteDebugLogToFile(string text)
		{
			try
			{
				if (Settings.Default.EnableDebugLog)
				{
					string sfile = GetFileName();
					using (TextWriter wr = new StreamWriter(sfile, true))
					{
						wr.WriteLine("{0}: {1}<br/>", DateTime.Now, HtmlNewLine(text));
						wr.Close();
					}
				}
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry(_logSource, "Error on write debug log:" + ex.Message + Environment.NewLine + ex.StackTrace, EventLogEntryType.Warning);
			}
		}

		public static void WriteLog(DataRow rowOld, DataRow rowNew)
		{
			try
			{
				if (Settings.Default.EnableDebugLog)
				{
					string sfile = GetFileName();
					using (TextWriter wr = new StreamWriter(sfile, true))
					{
						DataTable dt = (rowOld != null ? rowOld.Table : rowNew.Table);

						wr.WriteLine("<TABLE BORDER=1><TR><TD>&nbsp;</TD>");
						foreach (DataColumn col in dt.Columns)
						{
							wr.Write("<TD>{0}</TD>", col.ColumnName);
						}

						if (rowOld != null)
						{
							wr.WriteLine("</TR><TR><TD>Old</TD>");
							foreach (DataColumn col in dt.Columns)
							{
								if (rowOld[col.ColumnName] == null || rowOld[col.ColumnName] == DBNull.Value)
								{
									wr.Write("<TD>(null)</TD>");
								}
								else
								{
									wr.Write("<TD>{0}</TD>", rowOld[col.ColumnName]);
								}
							}
						}
						if (rowNew != null)
						{
							wr.WriteLine("</TR><TR><TD>New</TD>");
							foreach (DataColumn col in dt.Columns)
							{
								if (rowNew[col.ColumnName] == null || rowNew[col.ColumnName] == DBNull.Value)
								{
									wr.Write("<TD>(null)</TD>");
								}
								else
								{
									wr.Write("<TD>{0}</TD>", rowNew[col.ColumnName]);
								}
							}
						}

						wr.WriteLine("</TR></TABLE>");

						wr.Close();
					}
				}
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry(_logSource, "Error on write debug log:" + ex.Message + Environment.NewLine + ex.StackTrace, EventLogEntryType.Warning);
			}
		}

		public static string HtmlNewLine(string input)
		{
			if (!String.IsNullOrEmpty(input))
			{
				return HttpUtility.HtmlEncode(input).Replace("\n", "<br/>");
			}
			return string.Empty;
		}

		internal static string GetFullHostName()
		{
			string domainName = "";
			string fullhostName;
			string hostname = Environment.MachineName;

			using (ManagementObject cs = new ManagementObject("Win32_ComputerSystem.Name='" + hostname + "'"))
			{
				try
				{
					cs.Get();

					if (cs["partOfDomain"] != null && (bool) cs["partOfDomain"])
					{
						domainName = cs["domain"].ToString();
					}
				}
				catch (ManagementException)
				{
					domainName = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters", "Domain", "").ToString();
                }
			}

			if (domainName == "")
			{
				fullhostName = hostname;
			}
			else
			{
				fullhostName = string.Format("{0}.{1}", hostname, domainName);
			}

			return fullhostName;
		}

		private static string GetFileName()
		{
			string sfile;

			try
			{
                sfile = Path.Combine(GrisSuite.AppCfg.AppCfg.Default.sPathLogFolder, AppDomain.CurrentDomain.FriendlyName.Replace(".exe", ".log.htm"));
			}
			catch
			{
                return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.FriendlyName.Replace(".exe", ".log.htm"));
			}

			if (File.Exists(sfile))
			{
				FileInfo f = new FileInfo(sfile);
				long debugLogMaxSize = Settings.Default.DebugLogMaxSize;
				if (f.Length > debugLogMaxSize && debugLogMaxSize > 0)
				{
					string sfileOld = sfile.Replace(".log.htm", "_old.log.htm");

					if (File.Exists(sfileOld))
					{
						File.Delete(sfileOld);
					}

					f.MoveTo(sfileOld);
				}
			}

			return sfile;
		}

		private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		public static long CurrentTimeMillis()
		{
			return (long) (DateTime.UtcNow - epoch).TotalSeconds;
		}

		public static DateTime FromUnixTime(long unixTime)
		{
			return epoch.AddSeconds(unixTime).ToLocalTime();
		}
	}

	internal class SystemClock
	{
		private struct SYSTEMTIME
		{
			public ushort wYear, wMonth, wDayOfWeek, wDay, wHour, wMinute, wSecond, wMilliseconds;
		}

		[DllImport("kernel32.dll")]
		private static extern uint SetSystemTime(ref SYSTEMTIME lpSystemTime);

		internal static bool Set(DateTime d)
		{
			DateTime utc = d.ToUniversalTime();

			SYSTEMTIME st = new SYSTEMTIME();
			st.wDay = (ushort) utc.Day;
			st.wDayOfWeek = (ushort) utc.DayOfWeek;
			st.wHour = (ushort) utc.Hour;
			st.wMilliseconds = (ushort) utc.Millisecond;
			st.wMinute = (ushort) utc.Minute;
			st.wMonth = (ushort) utc.Month;
			st.wSecond = (ushort) utc.Second;
			st.wYear = (ushort) (utc.Year);
			SetSystemTime(ref st);
			return (SetSystemTime(ref st) != 0);
		}
	}
}
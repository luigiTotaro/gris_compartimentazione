﻿using System;
using System.Security;
using GrisSuite.Properties;
using Microsoft.Win32;

namespace GrisSuite
{
    /// <summary>
    ///     Classe helper per il caricamento delle configurazioni da Registry o file xml dei settings
    /// </summary>
    public class ConfigHelper
    {
        private static readonly ConfigHelper defaultInstance = new ConfigHelper();

        /// <summary>
        ///     Istanza della classe
        /// </summary>
        public static ConfigHelper Default
        {
            get { return defaultInstance; }
        }

        public string DebugLogFolder
        {
            get
            {
                string debugLogFolder =
                    this.GetRegistryKey(@"SYSTEM\CurrentControlSet\Services\STLCManagerService\Paths", "PathLogFolder");
                if (string.IsNullOrEmpty(debugLogFolder))
                {
                    return Settings.Default.DebugLogFolder;
                }

                return debugLogFolder;
            }
        }

        private string GetRegistryKey(string KeyName, string ValueName)
        {
            string keyValue = null;
            RegistryKey key = null;

            try
            {
                key = Registry.LocalMachine.OpenSubKey(KeyName);
                if (key != null)
                {
                    keyValue = key.GetValue(ValueName) as String;
                }
            }
            catch (SecurityException)
            {
            }
            finally
            {
                if (key != null)
                {
                    key.Close();
                }
            }

            return keyValue;
        }
    }
}
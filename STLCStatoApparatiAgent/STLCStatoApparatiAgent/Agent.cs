using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using GrisSuite.AnsaldoCollectorWS;
using GrisSuite.Data;
using GrisSuite.Data.dsConfigTableAdapters;
using GrisSuite.Data.dsSTLCParametersTableAdapters;
using GrisSuite.Properties;

namespace GrisSuite.STLCStatoApparatiAgent
{
	public class Agent
	{
		private readonly string _fileDeviceStatusCache = AppDomain.CurrentDomain.BaseDirectory + "\\DeviceStatusCache.xml";

		private readonly string _fileConfigCache = AppDomain.CurrentDomain.BaseDirectory + "\\ConfigCache.xml";
		private readonly SCACollectorClient _collector = new SCACollectorClient();
		private bool _isDeviceStatusValid;
		private readonly List<long> nodes = new List<long>();
		private long mainNodeId;

		public Agent()
		{
			this.InitializeServers();
			this.InitializeNodes();
		}

		public Agent(bool loadData)
		{
			// Costruttore con parametro da usare nei test, se si vuole evitare di fare accesso alla base dati
			if (loadData)
			{
				this.InitializeServers();
				this.InitializeNodes();
			}
		}

		private void InitializeServers()
		{
			dsConfig.ServersDataTable servers = this.GetServerData();
			if (servers != null && servers.Count > 0)
			{
				this._collector.SrvId = servers[0].SrvID;
				this.mainNodeId = servers[0].NodID;

				LocalUtil.WriteDebugLogToFile(string.Format("Server initialized using ServerId: {0}, NodeId Principale: {1}", this._collector.SrvId,
					this.mainNodeId));
			}
			else
			{
                LocalUtil.WriteDebugLogToFile("No server rows in 'servers' table.");
			}
		}

		private void InitializeNodes()
		{
			dsConfig.NodesDataTable nodesData = this.GetNodesData();
			if (nodesData != null && nodesData.Count > 0)
			{
				StringBuilder nodeList = new StringBuilder();

				foreach (dsConfig.NodesRow row in nodesData.Rows)
				{
					this.nodes.Add(row.NodID);
					nodeList.AppendFormat("{0} (ID: {1}), ", row.Name, row.NodID);
				}

				LocalUtil.WriteDebugLogToFile(string.Format("Nodes collection initialized using Nodes: {0}",
					(nodeList.Length > 2 ? nodeList.ToString().Substring(0, nodeList.Length - 2) : string.Empty)));
			}
			else
			{
				throw new ApplicationException("Error: no node rows in 'nodes' table");
			}
		}

		#region Public Methods

		public void ClearCache()
		{
			try
			{
				if (File.Exists(this._fileDeviceStatusCache))
				{
					File.Delete(this._fileDeviceStatusCache);
				}

				if (File.Exists(this._fileConfigCache))
				{
					File.Delete(this._fileConfigCache);
				}
			}
			catch (Exception ex)
			{
				LocalUtil.WriteDebugLogToFile("Error on ClearCache: " + ex.Message);
				throw;
			}
		}

		public DateTime SendStatoApparati(bool onlyChange, bool allData)
		{
			DateTime lastDate = DateTime.MinValue;

			try
			{
				LocalUtil.WriteDebugLogToFile(string.Format("SendStatoApparati {0}", (onlyChange ? "OnlyChange" : "Complete")));

				dsDeviceStatus dsChange = new dsDeviceStatus();
				dsDeviceStatus dsDeviceStatusCache = new dsDeviceStatus();
				string[] tables = Settings.Default.SyncDeviceStatus.GetTablesNames();

				dsDeviceStatus dsDeviceStatusNew = dsDeviceStatus.GetData(tables);
				bool isCacheValid = LoadFromCache(dsDeviceStatusCache, this._fileDeviceStatusCache);

				if (onlyChange && isCacheValid)
				{
					MergeDataSet(dsDeviceStatusNew, dsDeviceStatusCache, dsChange, Settings.Default.SyncDeviceStatus);
				}
				else
				{
					dsDeviceStatusCache = dsDeviceStatusNew;
					dsChange = dsDeviceStatusNew;
				}

				FilterSentColumns(dsChange, Settings.Default.SyncDeviceStatus);

				int nRows = GetDataSetRowsCount(dsChange);
				if (nRows > 0)
				{
					if (onlyChange)
					{
						bool atLeastOneVariazione = false;

						foreach (dsDeviceStatus.DeviceStatusRow deviceStatusRow in dsChange.DeviceStatus.Rows)
						{
							AnsaldoMessageBuilder.AnsaldoMessageBuilderData ambd = new AnsaldoMessageBuilder.AnsaldoMessageBuilderData(deviceStatusRow.MonitoringDeviceID);
							ambd.Populate(deviceStatusRow.NodID, deviceStatusRow.DevID, deviceStatusRow.Description, deviceStatusRow.SevLevel, dsChange.Streams,
								dsChange.StreamFields);

							variazioneResponseData vrd = this._collector.Variazione(ambd.NodeId, ambd.DescrizioneGuasto, ambd.FlagGuasto, ambd.IdApparato,
								ambd.LivelloGravita);

							LocalUtil.WriteDebugLogToFile(string.Format("Data sent to Variazione --> NodeId: {0}, DescrizioneGuasto: {1}, FlagGuasto: {2}, IdApparato: {3}, LivelloGravita: {4}", 
								ambd.NodeId, ambd.DescrizioneGuasto, ambd.FlagGuasto, ambd.IdApparato, ambd.LivelloGravita));

							atLeastOneVariazione = true;

							if (vrd != null)
							{
								lastDate = LocalUtil.FromUnixTime(vrd.data);

								if (vrd.flagReqStatoTotale)
								{
									LocalUtil.WriteDebugLogToFile(string.Format("Variazione message requested a StatoTotale ({0})",
										(vrd.flagAllData ? "All data" : "Only defective")));

									// Se durante l'invio delle variazioni c'� una richiesta di stato totale, salviamo la cache e inviamo tutto
									this._isDeviceStatusValid = SaveCache(dsDeviceStatusCache, this._fileDeviceStatusCache);
									return this.SendStatoApparati(false, vrd.flagAllData);
								}
							}
						}

						if (!atLeastOneVariazione)
						{
							// Ci sono device con cambiamenti rispetto all'ultimo invio, ma nessuna di esse � diventata guasta, quindi non ci sono stati invii
							LocalUtil.WriteDebugLogToFile("No device status change to defective status");
							lastDate = DateTime.MaxValue;
						}
					}
					else
					{
						List<datoStatoTotale> dati = new List<datoStatoTotale>();

						foreach (dsDeviceStatus.DeviceStatusRow deviceStatusRow in dsChange.DeviceStatus.Rows)
						{
							if ((allData) || (AnsaldoMessageBuilder.IsDeviceSevLevelGuasto(deviceStatusRow.SevLevel)))
							{
								// Si inviano tutte le periferiche, a prescindere dallo stato, oppure
								// si invia la periferica che � in stato di Guasto

								AnsaldoMessageBuilder.AnsaldoMessageBuilderData ambd = new AnsaldoMessageBuilder.AnsaldoMessageBuilderData(deviceStatusRow.MonitoringDeviceID);
								ambd.Populate(deviceStatusRow.NodID, deviceStatusRow.DevID, deviceStatusRow.Description, deviceStatusRow.SevLevel, dsChange.Streams,
									dsChange.StreamFields);

								dati.Add(new datoStatoTotale
								{
									descrizioneGuasto = ambd.DescrizioneGuasto,
									flagGuasto = ambd.FlagGuasto,
									idApparato = ambd.IdApparato,
									livelloGravita = ambd.LivelloGravita,
									idPP = SCACollectorClient.DecodeNodeID(ambd.NodeId)
								});
							}
						}

						if (dati.Count > 0)
						{
							statoTotaleResponseData strd = this._collector.StatoTotale(this.mainNodeId, dati.ToArray());

							if (strd != null)
							{
								lastDate = LocalUtil.FromUnixTime(strd.data);
							}
						}
					}
				}
				else
				{
					LocalUtil.WriteDebugLogToFile("No device status change");
					lastDate = DateTime.MaxValue;
				}

				this._isDeviceStatusValid = SaveCache(dsDeviceStatusCache, this._fileDeviceStatusCache);

				return lastDate;
			}
			catch (Exception ex)
			{
				this._isDeviceStatusValid = false;
				LocalUtil.WriteDebugLogToFile("Error on SendStatoApparati: " + ex.Message);
				throw;
			}
		}

		public bool SendLife(out string report)
		{
			StringBuilder results = new StringBuilder();
			bool sendSucceeded = true;

			try
			{
				if (this.nodes != null)
				{
					bool flagReqStatoTotale = false;
					bool flagAllData = false;

					foreach (long nodeId in this.nodes)
					{
						LocalUtil.WriteDebugLogToFile(string.Format("SendLife NodeId: {0}", nodeId));

						// L'invio dei messaggi Life � per tutti i nodi censiti nella tabella nodes (possono essere multipli per le remotizzate)
						lifeResponseData lrd = this._collector.Life(nodeId);
						if (lrd != null)
						{
							if (!flagReqStatoTotale)
							{
								// Alla prima richiesta di Stato Totale, durante messaggio Life, salviamo i flag
								// Essendo i messaggi Life multipli, assumiamo che ne basti uno di richiesta Stato Totale
								flagReqStatoTotale = lrd.flagReqStatoTotale;
								flagAllData = lrd.flagAllData;
							}

							results.AppendFormat("OK TEST NodeId: {0}, SendLife, DATA: {1}\r\n", nodeId, LocalUtil.FromUnixTime(lrd.data));
						}
						else
						{
							results.AppendFormat("KO TEST NodeId: {0}: SendLife\r\n", nodeId);
							sendSucceeded = false;
						}
					}

					// Inviamo lo Stato Totale, se richiesto. Fuori dal ciclo dei nodi per il Life, perch� contempla sempre le stesse device, quindi
					// sarebbero tutti Stati Totali identici se inviati nel ciclo
					if (flagReqStatoTotale)
					{
						LocalUtil.WriteDebugLogToFile(string.Format("Life message requested a StatoTotale ({0})", (flagAllData ? "All data" : "Only defective")));

						this.SendStatoApparati(false, flagAllData);
					}
				}
				else
				{
					results.AppendFormat("KO TEST SendLife, no nodes to send");
					sendSucceeded = false;
				}
			}
			catch (Exception ex)
			{
				LocalUtil.WriteDebugLogToFile("Error on SendLife: " + ex.Message);
				throw;
			}

			report = results.ToString();

			return sendSucceeded;
		}

		public bool IsDeviceStatusValid
		{
			get { return this._isDeviceStatusValid; }
		}

		public bool TrySqlConnect()
		{
			return Util.TrySqlConnection(10, 1000);
		}

		internal dsConfig.ServersDataTable GetServerData()
		{
			dsConfig.ServersDataTable servers;

			try
			{
				// estratti solo i servers che abbiano periferiche non removed
				servers = new ServersTableAdapter().GetData();
			}
			catch (SqlException ex)
			{
				throw new ApplicationException("Error: unable to connect to SQL Server, Inner exception: " + ex);
			}

			if ((servers != null) && (servers.Count > 1))
			{
				// per un corretto funzionamento di alcuni metodi di centralizzazione � necessaria la verifica che la tabella servers contenga sempre e solo un STLC operativo (con periferiche non rimosse)
				throw new ApplicationException("More than one valid row in 'servers' table.");
			}

			if ((servers == null) || (servers.Count <= 0))
			{
                LocalUtil.WriteDebugLogToFile("No server present in DB.");
			}

			return servers;
		}

		internal dsConfig.NodesDataTable GetNodesData()
		{
			dsConfig.NodesDataTable nodesData;

			try
			{
				// estratti solo i nodes non removed
				nodesData = new NodesTableAdapter().GetData();
			}
			catch (SqlException ex)
			{
				throw new ApplicationException("Error: unable to connect to SQL Server, Inner exception: " + ex);
			}

			if ((nodesData == null) || ((nodesData != null) && (nodesData.Count <= 0)))
			{
				throw new ApplicationException("Error: no node rows in 'nodes' table");
			}

			return nodesData;
		}

		public bool UpdateServerFromEnvironment()
		{
			string fullhostname = LocalUtil.GetFullHostName();
			string mac1;
			string mac2;
			string virtualMac;
			string ip1 = Util.GetNetworkInterfaceIP(Settings.Default.NetworkAdapterName1, out mac1);
			// ReSharper disable once UnusedVariable
			string ip2 = Util.GetNetworkInterfaceIP(Settings.Default.NetworkAdapterName2, out mac2);

			string virtualIp = Util.GetNetworkInterfaceIP(Settings.Default.IPVirtualePPNetworkAdapterName, out virtualMac);
			if (string.IsNullOrEmpty(virtualIp))
			{
				throw new ApplicationException(
					string.Format(
						"Unable to get IPVirtualePP value from machine. Please check network adapter name in config param 'IPVirtualePPNetworkAdapterName' (current value: '{0}')",
						Settings.Default.IPVirtualePPNetworkAdapterName));
			}

			this._collector.IPVirtualePP = virtualIp;

			bool hasChange = false;

			dsConfig.ServersDataTable servers = this.GetServerData();

			foreach (dsConfig.ServersRow row in servers.Rows)
			{
				if (row.IsFullHostNameNull() || row.FullHostName != fullhostname)
				{
					row.FullHostName = fullhostname;
					hasChange = true;
				}
				if (row.IsIPNull() || row.IP != ip1)
				{
					row.IP = ip1;
					hasChange = true;
				}
				if (row.IsMACNull() || row.MAC != mac1)
				{
					row.MAC = mac1;
					hasChange = true;
				}
			}

			if (hasChange)
			{
				new ServersTableAdapter().Update(servers);
			}

			if (Util.ShrinkTransactionLog(Settings.Default.MBTranLogSizeLimit))
			{
				LocalUtil.WriteDebugLogToFile("Transaction Log shrink performed");
			}

			STLCParametersTableAdapter taParams = new STLCParametersTableAdapter();
			dsSTLCParameters dsParams = new dsSTLCParameters();

			taParams.Fill(dsParams.STLCParameters);

			#region aggiorna/inserisce MACAddress2 in STLCParameters

			DataRow[] rowParamMACAddress2 = dsParams.Tables[0].Select("ParameterName='MACAddress2'");
			if (rowParamMACAddress2.Length > 0)
			{
				dsSTLCParameters.STLCParametersRow row = rowParamMACAddress2[0] as dsSTLCParameters.STLCParametersRow;
				if (row != null)
				{
					if (row.ParameterValue != mac2)
					{
						row.ParameterValue = mac2;
						taParams.Update(row);
						hasChange = true;
					}
				}
			}
			else
			{
				dsSTLCParameters.STLCParametersRow row = dsParams.Tables[0].NewRow() as dsSTLCParameters.STLCParametersRow;
				if (row != null)
				{
					row.SrvID = 0;
					row.ParameterName = "MACAddress2";
					row.ParameterValue = mac2;
					dsParams.Tables[0].Rows.Add(row);
				}
				taParams.Update(dsParams);
				hasChange = true;
			}

			#endregion

			return hasChange;
		}

		#endregion

		#region Private Utility

		private static void FilterSentColumns(DataSet ds, SyncDataset syncDataset)
		{
			foreach (DataTable dt in ds.Tables)
			{
				string[] sColumnsNotSent = GetColumnsToNotSent(dt, syncDataset);
				if (sColumnsNotSent.Length > 0)
				{
					foreach (DataRow row in dt.Rows)
					{
						foreach (string colName in sColumnsNotSent)
						{
							row[colName] = DBNull.Value;
						}
					}
				}
			}
		}

		private static void MergeDataSet(DataSet dsSource, DataSet dsTarget, DataSet dsChange, SyncDataset syncDataset)
		{
			dsChange.Clear();

			foreach (DataTable dtSource in dsSource.Tables)
			{
				string[] sColumns = GetColumnsToCompare(dtSource, syncDataset);
				DataTable dtTarget = dsTarget.Tables[dtSource.TableName];
				DataTable dtChange = dsChange.Tables[dtSource.TableName];

				dtTarget.BeginLoadData();
				dtChange.BeginLoadData();

				// aggiungi e aggiorna le righe
				foreach (DataRow rowSource in dtSource.Rows)
				{
					// find target row
					DataRow rowTarget = dtTarget.Rows.Find(GetPKValues(rowSource));

					if (rowTarget == null) // la riga non era presente nella cache: va aggiunta
					{
						LocalUtil.WriteDebugLogToFile(string.Format("NewRecord:{0}", dtSource.TableName));
						LocalUtil.WriteLog(null, rowSource);
						dtTarget.Rows.Add(rowSource.ItemArray);
						dtChange.Rows.Add(rowSource.ItemArray);
					}
					else
					{
						foreach (string colName in sColumns)
						{
							// verifica che non siano variati i valori delle colonne monitorate
							if ((rowSource[colName] is byte[] && !BytesIsEqual((byte[]) rowSource[colName], (byte[]) rowTarget[colName])) ||
							    (!(rowSource[colName] is byte[]) && !rowSource[colName].Equals(rowTarget[colName])))
							{
								LocalUtil.WriteDebugLogToFile(string.Format("Column Changed:{0}.{1}", dtSource.TableName, colName));
								LocalUtil.WriteLog(rowTarget, rowSource);
								dtChange.Rows.Add(rowSource.ItemArray);
								break;
							}
						}

						rowTarget.ItemArray = rowSource.ItemArray;
					}
				}

				// cancella le righe in pi� o le marca come da cancellare se la tabella ha la colonna "IsDeleted"
				for (int i = dtTarget.Rows.Count - 1; i >= 0; i--)
				{
					DataRow rowTarget = dtTarget.Rows[i];
					if (rowTarget.RowState == DataRowState.Unchanged)
					{
						LocalUtil.WriteDebugLogToFile(string.Format("Deleted Record:{0}", rowTarget.Table.TableName));
						DataRow deletedRow;
						if (dtTarget.Columns.Contains("IsDeleted"))
						{
							deletedRow = dtChange.Rows.Add(rowTarget.ItemArray);
							deletedRow["IsDeleted"] = true;
						}
						else
						{
							deletedRow = rowTarget;
						}
						LocalUtil.WriteLog(deletedRow, null);
						dtTarget.Rows.Remove(rowTarget);
					}
				}
				dtTarget.EndLoadData();
				dtChange.EndLoadData();
			}

			dsChange.AcceptChanges();
			dsTarget.AcceptChanges();
		}

		private static bool BytesIsEqual(byte[] a, byte[] b)
		{
			bool match = true;

			if (a.Length == b.Length)
			{
				for (int i = 0; i < a.Length; i++)
				{
					if (a[i] != b[i])
					{
						match = false;
						break;
					}
				}
			}
			else
			{
				match = false;
			}
			return match;
		}

		private static object[] GetPKValues(DataRow rowSource)
		{
			DataColumn[] pkColumns = rowSource.Table.PrimaryKey;
			object[] pkValues = new object[pkColumns.Length];

			for (int i = 0; i < pkColumns.Length; i++)
			{
				pkValues[i] = rowSource[pkColumns[i]];
			}

			return pkValues;
		}

		private static string[] GetColumnsToCompare(DataTable table, SyncDataset syncDataset)
		{
			SyncTable syncTable = syncDataset.Tables.Find(x => string.Compare(x.Name, table.TableName, StringComparison.OrdinalIgnoreCase) == 0);

			string s = string.Empty;
			if (syncTable == null || string.IsNullOrEmpty(syncTable.CompareColumns) || syncTable.CompareColumns == "*")
			{
				foreach (DataColumn col in table.Columns)
				{
					s += col.ColumnName + ",";
				}
			}
			else
			{
				foreach (string colName in syncTable.CompareColumns.Split(','))
				{
					if (table.Columns.Contains(colName.ToLower().Trim()))
					{
						s += colName.ToLower().Trim() + ",";
					}
				}
			}

			return (s.Length > 0) ? s.Substring(0, s.Length - 1).Split(',') : new string[] {};
		}

		private static string[] GetColumnsToNotSent(DataTable table, SyncDataset syncDataset)
		{
			SyncTable syncTable = syncDataset.Tables.Find(x => string.Compare(x.Name, table.TableName, StringComparison.OrdinalIgnoreCase) == 0);

			if (syncTable == null || string.IsNullOrEmpty(syncTable.NotSentColumns))
			{
				return new string[] {};
			}

			string s = string.Empty;

			// considera solo i nomi di colonna che esistono nella tabella
			foreach (string colName in syncTable.NotSentColumns.Split(','))
			{
				if (table.Columns.Contains(colName.ToLower().Trim()))
				{
					s += colName.ToLower().Trim() + ",";
				}
			}

			return (s.Length > 0) ? s.Substring(0, s.Length - 1).Split(',') : new string[] {};
		}

		private static int GetDataSetRowsCount(DataSet ds)
		{
			int count = 0;
			foreach (DataTable dt in ds.Tables)
			{
				count += dt.Rows.Count;
			}
			return count;
		}

		private static bool LoadFromCache(DataSet ds, string filename)
		{
			try
			{
				ds.Clear();
				if (File.Exists(filename))
				{
					FileStream fs = new FileStream(filename, FileMode.Open);
					ds.ReadXml(fs);
					fs.Close();
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				LocalUtil.WriteDebugLogToFile(string.Format("Error on LoadCache File={0} Error={1}", filename, ex.Message));
				return false;
			}
		}

		private static bool SaveCache(DataSet ds, string filename)
		{
			try
			{
				FileStream fs = new FileStream(filename, FileMode.Create);
				ds.WriteXml(fs);
				fs.Close();
				return true;
			}
			catch (Exception ex)
			{
				LocalUtil.WriteDebugLogToFile(string.Format("Error on SaveCache File={0} Error={1} ", filename, ex.Message));
				return false;
			}
		}

		#endregion
	}
}
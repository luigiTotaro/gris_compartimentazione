﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace GrisSuite
{
    public class SyncDataset
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "Table")]
        public List<SyncTable> Tables { get; set; }

        public string[] GetTablesNames()
        {
            List<string> s = null;

            if (this.Tables != null && this.Tables.Count > 0)
            {
                s = new List<string>();
                foreach (var table in this.Tables)
                {
                    s.Add(table.Name);
                }
            }

            return s != null ? s.ToArray() : null;
        }
    }

    public class SyncTable
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "CompareColumns")]
        public string CompareColumns { get; set; }

        [XmlAttribute(AttributeName = "NotSentColumns")]
        public string NotSentColumns { get; set; }
    }
}
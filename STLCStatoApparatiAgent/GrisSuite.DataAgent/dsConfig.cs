﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.dsConfigTableAdapters;
using GrisSuite.Data.Properties;

namespace GrisSuite.Data
{
    public partial class dsConfig
    {
        public partial class RegionsRow
        {
            public Int64 SetDecodedRegId(UInt16 decodedRegId)
            {
                return (Int64) Util.EncodeRegionID(decodedRegId);
            }

            public UInt16 GetDecodedRegId()
            {
                ushort regID;
                Util.DecodeRegionID((ulong) this.RegID, out regID);
                return regID;
            }
        }

        public partial class ZonesRow
        {
            public Int64 SetDecodedZonId(UInt16 decodedRegId, UInt16 decodedZonId)
            {
                return (Int64) Util.EncodeZoneID(decodedRegId, decodedZonId);
            }

            public UInt16 GetDecodedZonId()
            {
                ushort regID;
                ushort zonID;
                Util.DecodeZoneID((ulong) this.ZonID, out zonID, out regID);
                return zonID;
            }
        }

        public partial class NodesRow
        {
            public Int64 SetDecodedNodId(UInt16 decodedRegId, UInt16 decodedZonId, UInt16 decodedNodId)
            {
                return (Int64) Util.EncodeNodeID(decodedRegId, decodedZonId, decodedNodId);
            }

            public UInt16 GetDecodedNodId()
            {
                ushort regID;
                ushort zonID;
                ushort nodID;
                Util.DecodeNodeID((ulong) this.NodID, out nodID, out zonID, out regID);
                return nodID;
            }
        }

        public partial class ServersDataTable
        {
        }

        private static int _commandTimeOut = Settings.Default.CommandTimeout;

        public static int Fill(dsConfig config, string[] tables)
        {
            dsConfigDataSetAdaper daConfig = new dsConfigDataSetAdaper();
            return daConfig.Fill(config, tables);
        }

        public static int Fill(dsConfig config)
        {
            return Fill(config, null);
        }

        public static dsConfig GetData()
        {
            return GetData(null);
        }

        public static dsConfig GetData(string[] tables)
        {
            dsConfig c = new dsConfig();
            Fill(c, tables);
            return c;
        }

        private static dsConfig CopyConfig(dsConfig config)
        {
            dsConfig config2 = new dsConfig();

            foreach (DataTable dt in config.Tables)
            {
                config2.Tables[dt.TableName].Merge(dt, false);
            }
            return config2;
        }

        public static void Update(dsConfig ds, string[] tables, bool forcedUpdate)
        {
            dsConfigDataSetAdaper daConfig = new dsConfigDataSetAdaper();
            using (SqlConnection cn = daConfig.Connection)
            {
                cn.Open();
                daConfig.CommandTimeout = _commandTimeOut;
                daConfig.Transaction = cn.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    if (forcedUpdate)
                    {
                        ds.AcceptChanges();
                        Util.SetRowsAsAdded(ds); // forza l'inserimento di tutti i dati
                    }

                    daConfig.Update(ds, tables, false);
                    daConfig.Transaction.Commit();
                }
                catch
                {
                    if (daConfig.Transaction != null)
                    {
                        daConfig.Transaction.Rollback();
                    }
                    throw;
                }
                finally
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                }
            }
        }
    }

    internal class dsConfigDataSetAdaper
    {
        private readonly StationTableAdapter taStation;
        private readonly BuildingTableAdapter taBuilding;
        private readonly RackTableAdapter taRack;
        private readonly PortTableAdapter taPort;
        private readonly ServersTableAdapter taServers;
        private readonly RegionsTableAdapter taRegions;
        private readonly ZonesTableAdapter taZones;
        private readonly NodesTableAdapter taNodes;
        private readonly DevicesTableAdapter taDevices;

        internal dsConfigDataSetAdaper()
        {
            this.taStation = new StationTableAdapter();
            this.taBuilding = new BuildingTableAdapter();
            this.taRack = new RackTableAdapter();
            this.taServers = new ServersTableAdapter();
            this.taPort = new PortTableAdapter();
            this.taRegions = new RegionsTableAdapter();
            this.taZones = new ZonesTableAdapter();
            this.taNodes = new NodesTableAdapter();
            this.taDevices = new DevicesTableAdapter();

            this.Connection = this.taServers.Connection;
        }

        internal SqlConnection Connection
        {
            get
            {
                SqlConnection cn = this.taDevices.Connection;
                if (cn == null)
                {
                    cn = this.taNodes.Connection;
                }
                if (cn == null)
                {
                    cn = this.taZones.Connection;
                }
                if (cn == null)
                {
                    cn = this.taRegions.Connection;
                }
                if (cn == null)
                {
                    cn = this.taServers.Connection;
                }
                if (cn == null)
                {
                    cn = this.taStation.Connection;
                }
                if (cn == null)
                {
                    cn = this.taBuilding.Connection;
                }
                if (cn == null)
                {
                    cn = this.taRack.Connection;
                }
                if (cn == null)
                {
                    cn = this.taPort.Connection;
                }
                return cn;
            }
            set
            {
                this.taServers.Connection = value;
                this.taRegions.Connection = value;
                this.taZones.Connection = value;
                this.taNodes.Connection = value;
                this.taDevices.Connection = value;
                this.taStation.Connection = value;
                this.taBuilding.Connection = value;
                this.taRack.Connection = value;
                this.taPort.Connection = value;
            }
        }

        internal SqlTransaction Transaction
        {
            get
            {
                SqlTransaction tn = this.taDevices.Transaction;
                if (tn == null)
                {
                    tn = this.taNodes.Transaction;
                }
                if (tn == null)
                {
                    tn = this.taZones.Transaction;
                }
                if (tn == null)
                {
                    tn = this.taRegions.Transaction;
                }
                if (tn == null)
                {
                    tn = this.taServers.Transaction;
                }
                if (tn == null)
                {
                    tn = this.taStation.Transaction;
                }
                if (tn == null)
                {
                    tn = this.taBuilding.Transaction;
                }
                if (tn == null)
                {
                    tn = this.taRack.Transaction;
                }
                if (tn == null)
                {
                    tn = this.taPort.Transaction;
                }
                return tn;
            }
            set
            {
                if (value != null)
                {
                    this.Connection = value.Connection;
                }
                this.taServers.Transaction = value;
                this.taRegions.Transaction = value;
                this.taZones.Transaction = value;
                this.taNodes.Transaction = value;
                this.taDevices.Transaction = value;
                this.taStation.Transaction = value;
                this.taBuilding.Transaction = value;
                this.taRack.Transaction = value;
                this.taPort.Transaction = value;
            }
        }

        internal int CommandTimeout
        {
            get
            {
                int timeout = this.taDevices.CommandTimeout;
                if (timeout == 0)
                {
                    timeout = this.taNodes.CommandTimeout;
                }
                if (timeout == 0)
                {
                    timeout = this.taZones.CommandTimeout;
                }
                if (timeout == 0)
                {
                    timeout = this.taRegions.CommandTimeout;
                }
                if (timeout == 0)
                {
                    timeout = this.taServers.CommandTimeout;
                }
                if (timeout == 0)
                {
                    timeout = this.taStation.CommandTimeout;
                }
                if (timeout == 0)
                {
                    timeout = this.taBuilding.CommandTimeout;
                }
                if (timeout == 0)
                {
                    timeout = this.taRack.CommandTimeout;
                }
                if (timeout == 0)
                {
                    timeout = this.taPort.CommandTimeout;
                }
                return timeout;
            }
            set
            {
                this.taServers.CommandTimeout = value;
                this.taRegions.CommandTimeout = value;
                this.taZones.CommandTimeout = value;
                this.taNodes.CommandTimeout = value;
                this.taDevices.CommandTimeout = value;
                this.taStation.CommandTimeout = value;
                this.taBuilding.CommandTimeout = value;
                this.taRack.CommandTimeout = value;
                this.taPort.CommandTimeout = value;
            }
        }

        internal int Update(dsConfig config, string[] tables, bool forDelete)
        {
            int i = 0;

            tables = this.InitializeTables(config, tables);

            if (forDelete)
            {
                if (Array.IndexOf(tables, config.Station.TableName.ToLower()) >= 0)
                {
                    i += this.taStation.Update(config.Station);
                }
                else if (Array.IndexOf(tables, config.Building.TableName.ToLower()) >= 0)
                {
                    i += this.taBuilding.Update(config.Building);
                }
                else if (Array.IndexOf(tables, config.Rack.TableName.ToLower()) >= 0)
                {
                    i += this.taRack.Update(config.Rack);
                }

                // Sono informazioni globali non vado mai in cancellazione generale.
                // Per cancellarle usare i metodi del singolo table adapter
                //if (Array.IndexOf(tables, config.Regions.TableName.ToLower()) >= 0)
                //    i += taRegions.Update(config.Regions);
                //else if (Array.IndexOf(tables, config.Zones.TableName.ToLower()) >= 0)
                //    i += taZones.Update(config.Zones);
                //else if (Array.IndexOf(tables, config.Nodes.TableName.ToLower()) >= 0)
                //    i += taNodes.Update(config.Nodes);

                if (Array.IndexOf(tables, config.Servers.TableName.ToLower()) >= 0)
                {
                    i += this.taServers.Update(config.Servers);
                }
                else
                {
                    if (Array.IndexOf(tables, config.Port.TableName.ToLower()) >= 0)
                    {
                        i += this.taPort.Update(config.Port);
                    }
                    if (Array.IndexOf(tables, config.Devices.TableName.ToLower()) >= 0)
                    {
                        i += this.taDevices.Update(config.Devices);
                    }
                }
            }
            else
            {
                if (Array.IndexOf(tables, config.Station.TableName.ToLower()) >= 0)
                {
                    i += this.taStation.Update(config.Station);
                }
                if (Array.IndexOf(tables, config.Building.TableName.ToLower()) >= 0)
                {
                    i += this.taBuilding.Update(config.Building);
                }
                if (Array.IndexOf(tables, config.Rack.TableName.ToLower()) >= 0)
                {
                    i += this.taRack.Update(config.Rack);
                }
                if (Array.IndexOf(tables, config.Servers.TableName.ToLower()) >= 0)
                {
                    i += this.taServers.Update(config.Servers);
                }
                if (Array.IndexOf(tables, config.Port.TableName.ToLower()) >= 0)
                {
                    i += this.taPort.Update(config.Port);
                }
                if (Array.IndexOf(tables, config.Regions.TableName.ToLower()) >= 0)
                {
                    i += this.taRegions.Update(config.Regions);
                }
                if (Array.IndexOf(tables, config.Zones.TableName.ToLower()) >= 0)
                {
                    i += this.taZones.Update(config.Zones);
                }
                if (Array.IndexOf(tables, config.Nodes.TableName.ToLower()) >= 0)
                {
                    i += this.taNodes.Update(config.Nodes);
                }
                if (Array.IndexOf(tables, config.Devices.TableName.ToLower()) >= 0)
                {
                    i += this.taDevices.Update(config.Devices);
                }
            }

            return i;
        }

        internal int Fill(dsConfig config, string[] tables)
        {
            tables = this.InitializeTables(config, tables);

            int i = 0;

            if (Array.IndexOf(tables, config.Station.TableName.ToLower()) >= 0)
            {
                i += this.taStation.Fill(config.Station);
            }
            if (Array.IndexOf(tables, config.Building.TableName.ToLower()) >= 0)
            {
                i += this.taBuilding.Fill(config.Building);
            }
            if (Array.IndexOf(tables, config.Rack.TableName.ToLower()) >= 0)
            {
                i += this.taRack.Fill(config.Rack);
            }
            if (Array.IndexOf(tables, config.Servers.TableName.ToLower()) >= 0)
            {
                i += this.taServers.Fill(config.Servers);
            }
            if (Array.IndexOf(tables, config.Port.TableName.ToLower()) >= 0)
            {
                i += this.taPort.Fill(config.Port);
            }
            if (Array.IndexOf(tables, config.Regions.TableName.ToLower()) >= 0)
            {
                i += this.taRegions.Fill(config.Regions);
            }
            if (Array.IndexOf(tables, config.Zones.TableName.ToLower()) >= 0)
            {
                i += this.taZones.Fill(config.Zones);
            }
            if (Array.IndexOf(tables, config.Nodes.TableName.ToLower()) >= 0)
            {
                i += this.taNodes.Fill(config.Nodes);
            }
            if (Array.IndexOf(tables, config.Devices.TableName.ToLower()) >= 0)
            {
                i += this.taDevices.Fill(config.Devices);
            }

            return i;
        }

        private string[] InitializeTables(dsConfig config, string[] tables)
        {
            if (tables == null || tables.Length == 0)
            {
                tables = new []
                {
                    config.Station.TableName, config.Building.TableName, config.Rack.TableName, config.Servers.TableName,
                    config.Port.TableName, config.Regions.TableName, config.Zones.TableName, config.Nodes.TableName,
                    config.Devices.TableName
                };
            }

            for (int j = 0; j < tables.Length; j++)
            {
                tables[j] = tables[j].ToLower().Trim();
            }

            return tables;
        }
    }
}

namespace GrisSuite.Data.dsConfigTableAdapters
{
    public partial class DevicesTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class RegionsTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class ServersTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class ZonesTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class NodesTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class StationTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class BuildingTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class RackTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }

    public partial class PortTableAdapter
    {
        internal SqlTransaction Transaction
        {
            get { return Util.GetDataAdapterTransaction(this._adapter); }
            set { Util.SetDataAdapterTransaction(this._adapter, value); }
        }

        internal int CommandTimeout
        {
            get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
            set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
        }
    }
}
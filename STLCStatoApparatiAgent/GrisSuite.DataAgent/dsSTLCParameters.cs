﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.dsSTLCParametersTableAdapters;

namespace GrisSuite.Data
{
    public partial class dsSTLCParameters
    {   
        private static int _commandTimeOut = global::GrisSuite.Data.Properties.Settings.Default.CommandTimeout;
    }
}

namespace GrisSuite.Data.dsSTLCParametersTableAdapters
{
    public partial class STLCParametersTableAdapter
    {
        public int UpdateBatchSize
        {
            get
            {
                return this.Adapter.UpdateBatchSize;
            }
            set
            {
                this.Adapter.UpdateBatchSize = value;
            }
        }

        public int UpdateOnTransaction(dsSTLCParameters ds)
        {
            int rc = -1;
            try
            {
                this.Adapter.UpdateBatchSize = 0;
                this.Adapter.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
                this.Adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                this.Adapter.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
                this.Transaction = this.Connection.BeginTransaction();
                rc = this.Update(ds);
                this.Transaction.Commit();
            }
            catch
            {
                if (this.Transaction != null)
                    this.Transaction.Rollback();
                throw;
            }
            return rc;
        }

        public int CommandTimeout
        {
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                    command.CommandTimeout = value;
            }
        }
    }

    public partial class STLCParametersQueries
    {

        internal SqlTransaction Transaction
        {
            get
            {
                return (SqlTransaction)this.CommandCollection[0].Transaction;
            }
            set
            {
                for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                {
                    this.CommandCollection[i].Connection = value.Connection;
                    this.CommandCollection[i].Transaction = value;
                }
            }
        }

        public int CommandTimeout
        {
            get
            {
                return this.CommandCollection[0].CommandTimeout;
            }
            set
            {
                foreach (IDbCommand command in this.CommandCollection)
                    command.CommandTimeout = value;
            }
        }
    }
}


using System;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Net.NetworkInformation;
using GrisSuite.Data.Properties;

namespace GrisSuite
{
    public class Util
    {
        public static void SetRowsAsAdded(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                SetRowsAsAdded(dt);
            }
        }

        public static void SetRowsAsAdded(DataTable dt)
        {
            foreach (DataRow r in dt.Rows)
            {
                r.SetAdded();
            }
        }

        public static void SetRowsAsDeleted(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                SetRowsAsDeleted(dt);
            }
        }

        public static void SetRowsAsDeleted(DataTable dt)
        {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                dt.Rows[i].Delete();
            }
        }

        public static SqlTransaction GetDataAdapterTransaction(SqlDataAdapter da)
        {
            if (da.InsertCommand != null)
            {
                return da.InsertCommand.Transaction;
            }

            if (da.UpdateCommand != null)
            {
                return da.UpdateCommand.Transaction;
            }

            if (da.DeleteCommand != null)
            {
                return da.DeleteCommand.Transaction;
            }

            return null;
        }

        public static int GetDataAdapterCommandTimeout(SqlDataAdapter da)
        {
            if (da.InsertCommand != null)
            {
                return da.InsertCommand.CommandTimeout;
            }

            if (da.UpdateCommand != null)
            {
                return da.UpdateCommand.CommandTimeout;
            }

            if (da.DeleteCommand != null)
            {
                return da.DeleteCommand.CommandTimeout;
            }

            return 0;
        }

        public static void SetDataAdapterTransaction(SqlDataAdapter da, SqlTransaction tran)
        {
            if (da.InsertCommand != null)
            {
                da.InsertCommand.Transaction = tran;
            }

            if (da.UpdateCommand != null)
            {
                da.UpdateCommand.Transaction = tran;
            }

            if (da.DeleteCommand != null)
            {
                da.DeleteCommand.Transaction = tran;
            }
        }

        public static void SetDataAdapterCommandTimeout(SqlDataAdapter da, int commandTimeout)
        {
            if (da.InsertCommand != null)
            {
                da.InsertCommand.CommandTimeout = commandTimeout;
            }

            if (da.UpdateCommand != null)
            {
                da.UpdateCommand.CommandTimeout = commandTimeout;
            }

            if (da.DeleteCommand != null)
            {
                da.DeleteCommand.CommandTimeout = commandTimeout;
            }
        }

        public static byte[] CompressDataSet(DataSet ds)
        {
            byte[] cds;

            string tempFile = Path.GetTempFileName();
            using (FileStream fs = new FileStream(tempFile, FileMode.Create))
            {
                DeflateStream compressor = new DeflateStream(fs, CompressionMode.Compress, false);
                ds.WriteXml(compressor);
                compressor.Close();
            }

            using (FileStream fs1 = new FileStream(tempFile, FileMode.Open, FileAccess.Read))
            {
                cds = new byte[fs1.Length];
                fs1.Read(cds, 0, cds.Length);
                fs1.Close();
            }

            try
            {
                File.Delete(tempFile);
            }
            catch (IOException)
            {
            } // il file � in uso

            return cds;
        }

        public static DataSet DecompressDataSet(byte[] cds)
        {
            DataSet ds = new DataSet();

            using (MemoryStream ms = new MemoryStream())
            {
                // message to stream
                ms.Write(cds, 0, cds.Length);
                ms.Position = 0;

                // stream to dataset
                DeflateStream decompressor = new DeflateStream(ms, CompressionMode.Decompress, false);
                ds.ReadXml(decompressor);
                decompressor.Close();
                ds.AcceptChanges();
            }

            return ds;
        }

        public static void FillDataSetFromCompressed(DataSet ds, byte[] cds)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // message to stream
                ms.Write(cds, 0, cds.Length);
                ms.Position = 0;

                // stream to dataset
                DeflateStream decompressor = new DeflateStream(ms, CompressionMode.Decompress, false);
                ds.ReadXml(decompressor);
                decompressor.Close();
                ds.AcceptChanges();
            }
        }

        public static bool TrySqlConnection(int nRetry, int nMilliseconds)
        {
            SqlConnection cn = new SqlConnection();
            int i = 0;
            bool connected = false;

            while (i < nRetry && !connected)
            {
                i++;
                try
                {
                    cn.ConnectionString = Settings.Default.TelefinConnectionString;
                    cn.Open();
                    connected = true;
                }
                catch
                {
                    Thread.Sleep(nMilliseconds);
                }
            }
            if (connected)
            {
                cn.Close();
            }
            cn.Dispose();

            return connected;
        }

        public static string GetNetworkInterfaceIP(string networkInterfaceName, out string physicalAddress)
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            if (nics.Length > 0)
            {
                foreach (NetworkInterface adapter in nics)
                {
                    if (adapter.Name.Equals(networkInterfaceName, StringComparison.OrdinalIgnoreCase))
                    {
                        physicalAddress = adapter.GetPhysicalAddress().ToString();
                        IPInterfaceProperties properties = adapter.GetIPProperties();
                        if (properties.UnicastAddresses.Count > 0)
                        {
                            foreach (var address in properties.UnicastAddresses)
                            {
                                if (address.Address.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    return address.Address.ToString();
                                }
                            }
                        }
                    }
                }
            }

            physicalAddress = string.Empty;
            return string.Empty;
        }

        public static string GetNetworkInterfaceMAC(string networkInterfaceName)
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            if (nics.Length > 0)
            {
                foreach (NetworkInterface adapter in nics)
                {
                    if (string.Compare(adapter.Name, networkInterfaceName, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        return adapter.GetPhysicalAddress().ToString();
                    }
                }
            }
            return string.Empty;
        }

        public static bool ShrinkTransactionLog(int triggerTranLogSize)
        {
            using (SqlConnection cnn = new SqlConnection())
            {
                cnn.ConnectionString = Settings.Default.TelefinConnectionString;

                SqlCommand cmd = new SqlCommand("util_ShrinkTransactionLog", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TriggerTranLogSize", SqlDbType.SmallInt).Value = triggerTranLogSize;

                try
                {
                    cnn.Open();
                    object ret = cmd.ExecuteScalar();
                    cnn.Close();
                    return Convert.ToBoolean(ret ?? false);
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }

        # region Gris Encoding Methods

        public enum RegCodes : byte
        {
            Torino = 1,
            Milano,
            Verona,
            Venezia,
            Trieste,
            Genova,
            Bologna,
            Firenze,
            Ancona,
            Roma,
            Napoli,
            Bari,
            ReggioCalabria,
            Palermo,
            Cagliari
        }

        public static RegCodes GetRegCode(Int64 regId)
        {
            const long mask = 0x000000000000FFFF;
            return (RegCodes) (regId & mask);
        }

        public static string GetCompCode(Int64 RegID)
        {
            const long mask = 0x000000000000FFFF;
            return (RegID & mask).ToString();
        }

        public static string GetLineCode(Int64 ZonID)
        {
            const long mask = 0x000000000000FFFF;
            return ((ZonID >> 16) & mask).ToString();
        }

        public static string GetNodeCode(Int64 NodID)
        {
            const long mask = 0x000000000000FFFF;
            return ((NodID >> 32) & mask).ToString();
        }

        /// <summary>
        ///     Decodifica un Device ID da database nei rispettivi sotto ID originali da System.XML
        /// </summary>
        /// <param name="devID">Device ID da database</param>
        /// <param name="originalDevID">Device ID originale da System.XML</param>
        /// <param name="originalNodeID">Node ID originale da System.XML</param>
        /// <param name="originalZoneID">Zone ID originale da System.XML</param>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        public static void DecodeDeviceID(ulong devID,
            out ushort originalDevID,
            out ushort originalNodeID,
            out ushort originalZoneID,
            out ushort originalRegionID)
        {
            originalDevID = (ushort) ((devID & 0xFFFF000000000000) >> 48);
            originalNodeID = (ushort) ((devID & 0xFFFF00000000) >> 32);
            originalZoneID = (ushort) ((devID & 0xFFFF0000) >> 16);
            originalRegionID = (ushort) ((devID & 0xFFFF));
        }

        /// <summary>
        ///     Decodifica un Node ID da database nei rispettivi sotto ID originali da System.XML
        /// </summary>
        /// <param name="nodID">Node ID da database</param>
        /// <param name="originalNodeID">Node ID originale da System.XML</param>
        /// <param name="originalZoneID">Zone ID originale da System.XML</param>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        public static void DecodeNodeID(ulong nodID, out ushort originalNodeID, out ushort originalZoneID, out ushort originalRegionID)
        {
            originalNodeID = (ushort) ((nodID & 0xFFFF00000000) >> 32);
            originalZoneID = (ushort) ((nodID & 0xFFFF0000) >> 16);
            originalRegionID = (ushort) ((nodID & 0xFFFF));
        }

        /// <summary>
        ///     Decodifica uno Zone ID da database nei rispettivi sotto ID originali da System.XML
        /// </summary>
        /// <param name="zonID">Zone ID da database</param>
        /// <param name="originalZoneID">Zone ID originale da System.XML</param>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        public static void DecodeZoneID(ulong zonID, out ushort originalZoneID, out ushort originalRegionID)
        {
            originalZoneID = (ushort) ((zonID & 0xFFFF0000) >> 16);
            originalRegionID = (ushort) ((zonID & 0xFFFF));
        }

        /// <summary>
        ///     Decodifica una Region ID da database nei rispettivi sotto ID originali da System.XML
        /// </summary>
        /// <param name="regID">Region ID da database</param>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        public static void DecodeRegionID(ulong regID, out ushort originalRegionID)
        {
            originalRegionID = (ushort) ((regID & 0xFFFF));
        }

        /// <summary>
        ///     Codifica un Device ID costituito dai valori da System.XML nella versione per database
        /// </summary>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        /// <param name="originalZoneID">Zone ID originale da System.XML</param>
        /// <param name="originalNodeID">Node ID originale da System.XML</param>
        /// <param name="originalDevID">Device ID originale da System.XML</param>
        /// <returns>Intero del Device ID per il database</returns>
        public static ulong EncodeDeviceID(ushort originalRegionID, ushort originalZoneID, ushort originalNodeID, ushort originalDevID)
        {
            return (originalRegionID | ((ulong) originalZoneID << 16) | ((ulong) originalNodeID << 32) | ((ulong) originalDevID << 48));
        }

        /// <summary>
        ///     Codifica un Node ID costituito dai valori da System.XML nella versione per database
        /// </summary>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        /// <param name="originalZoneID">Zone ID originale da System.XML</param>
        /// <param name="originalNodeID">Node ID originale da System.XML</param>
        /// <returns>Intero del Node ID per il database</returns>
        public static ulong EncodeNodeID(ushort originalRegionID, ushort originalZoneID, ushort originalNodeID)
        {
            return (originalRegionID | ((ulong) originalZoneID << 16) | ((ulong) originalNodeID << 32));
        }

        /// <summary>
        ///     Codifica uno Zone ID costituito dai valori da System.XML nella versione per database
        /// </summary>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        /// <param name="originalZoneID">Zone ID originale da System.XML</param>
        /// <returns>Intero dello Zone ID per il database</returns>
        public static ulong EncodeZoneID(ushort originalRegionID, ushort originalZoneID)
        {
            return (originalRegionID | ((ulong) originalZoneID << 16));
        }

        /// <summary>
        ///     Codifica un Region ID costituito dai valori da System.XML nella versione per database
        /// </summary>
        /// <param name="originalRegionID">Region ID originale da System.XML</param>
        /// <returns>Intero del Region ID per il database</returns>
        public static ulong EncodeRegionID(ushort originalRegionID)
        {
            return (originalRegionID | ((ulong) 0xFFFFFFFF0000));
        }

        /// <summary>
        ///     Ritorna la stringa con il numero di serie decodificato dal Server ID
        /// </summary>
        /// <param name="servId">Server ID</param>
        /// <returns>Stringa con il numero di serie dell'STLC, nel formato 00.00.000 (anno, mese, progressivo)</returns>
        public static string GetSNFromSrvID(ulong servId)
        {
            return string.Format("{0:00}.{1:00}.{2:000}", ((servId & 0xFF000000) >> 24), ((servId & 0xFF0000) >> 16), (servId & 0xFFFF));
        }

        # endregion
    }
}

namespace GrisSuite.Data
{
    public enum LogDBLevel
    {
        Off,
        MessageEnvelope,
        MessageComplete,
        MessageEnvelopeAndStream,
        MessageCompleteAndStream
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using GrisSuite.Data.Properties;
using GrisSuite.Data.dsDeviceStatusTableAdapters;

namespace GrisSuite.Data
{
	partial class dsDeviceStatus
	{
		private static int _commandTimeOut = Settings.Default.CommandTimeout;

		partial class DeviceStatusDataTable
		{
		}

		public static int Fill(dsDeviceStatus ds)
		{
			return Fill(ds, null);
		}

		public static int Fill(dsDeviceStatus ds, string[] tables)
		{
			dsDeviceStatusDataSetAdaper daDeviceStatus = new dsDeviceStatusDataSetAdaper();
			return daDeviceStatus.Fill(ds, tables);
		}

		public static dsDeviceStatus GetData()
		{
			return GetData(null);
		}

		public static dsDeviceStatus GetData(string[] tables)
		{
			dsDeviceStatus ds = new dsDeviceStatus();
			Fill(ds, tables);
			return ds;
		}

		private static dsDeviceStatus CopyDeviceStatus(dsDeviceStatus deviceStatus)
		{
			dsDeviceStatus deviceStatus2 = new dsDeviceStatus();

			foreach (DataTable dt in deviceStatus.Tables)
			{
				deviceStatus2.Tables[dt.TableName].Merge(dt, false);
			}
			return deviceStatus2;
		}
	}

	internal class dsDeviceStatusDataSetAdaper
	{
		private readonly DeviceStatusTableAdapter taDeviceStatus;
		private readonly StreamsTableAdapter taStreams;
		private readonly StreamFieldsTableAdapter taStreamFields;
		private readonly ReferenceTableAdapter taReference;

		internal dsDeviceStatusDataSetAdaper()
		{
			this.taDeviceStatus = new DeviceStatusTableAdapter();
			this.taStreams = new StreamsTableAdapter();
			this.taStreamFields = new StreamFieldsTableAdapter();
			this.taReference = new ReferenceTableAdapter();

			this.Connection = this.taDeviceStatus.Connection;
		}

		internal SqlConnection Connection
		{
			get
			{
				SqlConnection cn = this.taDeviceStatus.Connection;
				if (cn == null)
				{
					cn = this.taStreams.Connection;
				}
				if (cn == null)
				{
					cn = this.taStreamFields.Connection;
				}
				if (cn == null)
				{
					cn = this.taReference.Connection;
				}
				return cn;
			}
			set
			{
				this.taDeviceStatus.Connection = value;
				this.taStreams.Connection = value;
				this.taStreamFields.Connection = value;
				this.taReference.Connection = value;
			}
		}

		internal SqlTransaction Transaction
		{
			get
			{
				SqlTransaction tn = this.taDeviceStatus.Transaction;
				if (tn == null)
				{
					tn = this.taStreams.Transaction;
				}
				if (tn == null)
				{
					tn = this.taStreamFields.Transaction;
				}
				if (tn == null)
				{
					tn = this.taReference.Transaction;
				}
				return tn;
			}
			set
			{
				if (value != null)
				{
					this.Connection = value.Connection;
				}
				this.taDeviceStatus.Transaction = value;
				this.taStreams.Transaction = value;
				this.taStreamFields.Transaction = value;
				this.taReference.Transaction = value;
			}
		}

		internal int CommandTimeout
		{
			get
			{
				int timeout = this.taDeviceStatus.CommandTimeout;
				if (timeout == 0)
				{
					timeout = this.taStreams.CommandTimeout;
				}
				if (timeout == 0)
				{
					timeout = this.taStreamFields.CommandTimeout;
				}
				if (timeout == 0)
				{
					timeout = this.taReference.CommandTimeout;
				}
				return timeout;
			}
			set
			{
				this.taDeviceStatus.CommandTimeout = value;
				this.taStreams.CommandTimeout = value;
				this.taStreamFields.CommandTimeout = value;
				this.taReference.CommandTimeout = value;
			}
		}

		internal int Update(dsDeviceStatus ds, string[] tables, bool forDelete)
		{
			int i = 0;

			tables = InitializeTables(ds, tables);

			if (forDelete)
			{
				if (Array.IndexOf(tables, ds.DeviceStatus.TableName.ToLower()) >= 0)
				{
					i += this.taDeviceStatus.Update(ds.DeviceStatus);
				}
				else if (Array.IndexOf(tables, ds.Streams.TableName.ToLower()) >= 0)
				{
					i += this.taStreams.Update(ds.Streams);
				}
				else if (Array.IndexOf(tables, ds.StreamFields.TableName.ToLower()) >= 0)
				{
					i += this.taStreamFields.Update(ds.StreamFields);
				}
				else if (Array.IndexOf(tables, ds.Reference.TableName.ToLower()) >= 0)
				{
					i += this.taReference.Update(ds.Reference);
				}
			}
			else
			{
				if (Array.IndexOf(tables, ds.DeviceStatus.TableName.ToLower()) >= 0)
				{
					i += this.taDeviceStatus.Update(ds.DeviceStatus);
				}
				if (Array.IndexOf(tables, ds.Streams.TableName.ToLower()) >= 0)
				{
					i += this.taStreams.Update(ds.Streams);
				}
				if (Array.IndexOf(tables, ds.Reference.TableName.ToLower()) >= 0)
				{
					i += this.taReference.Update(ds.Reference);
				}
				if (Array.IndexOf(tables, ds.StreamFields.TableName.ToLower()) >= 0)
				{
					i += this.taStreamFields.Update(ds.StreamFields);
				}
			}

			return i;
		}

		internal int Fill(dsDeviceStatus ds, string[] tables)
		{
			tables = InitializeTables(ds, tables);

			int i = 0;

			if (Array.IndexOf(tables, ds.DeviceStatus.TableName.ToLower()) >= 0)
			{
				i += this.taDeviceStatus.Fill(ds.DeviceStatus);
			}
			if (Array.IndexOf(tables, ds.Streams.TableName.ToLower()) >= 0)
			{
				i += this.taStreams.Fill(ds.Streams);
			}
			if (Array.IndexOf(tables, ds.StreamFields.TableName.ToLower()) >= 0)
			{
				i += this.taStreamFields.Fill(ds.StreamFields);
			}
			if (Array.IndexOf(tables, ds.Reference.TableName.ToLower()) >= 0)
			{
				i += this.taReference.Fill(ds.Reference);
			}

			return i;
		}

		private static string[] InitializeTables(dsDeviceStatus ds, string[] tables)
		{
			if (tables == null || tables.Length == 0)
			{
				tables = new[] {ds.DeviceStatus.TableName, ds.Streams.TableName, ds.StreamFields.TableName, ds.Reference.TableName};
			}

			for (int j = 0; j < tables.Length; j++)
			{
				tables[j] = tables[j].ToLower().Trim();
			}

			return tables;
		}
	}
}

namespace GrisSuite.Data.dsDeviceStatusTableAdapters
{
	public partial class DeviceStatusTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class StreamsTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class StreamFieldsTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}

	public partial class ReferenceTableAdapter
	{
		internal SqlTransaction Transaction
		{
			get { return Util.GetDataAdapterTransaction(this._adapter); }
			set { Util.SetDataAdapterTransaction(this._adapter, value); }
		}

		internal int CommandTimeout
		{
			get { return Util.GetDataAdapterCommandTimeout(this._adapter); }
			set { Util.SetDataAdapterCommandTimeout(this._adapter, value); }
		}
	}
}
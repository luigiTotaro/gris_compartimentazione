﻿using System.CodeDom.Compiler;
using System.ServiceModel;

namespace STLCStatoApparatiAgentTestServer
{
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	[ServiceContract(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", ConfigurationName = "AnsaldoCollectorWS.DefaultStatoApparati")]
	public interface IDefaultStatoApparati
	{
		// CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
		[OperationContract(Action = "http://wsstatoapparati.iec.ansaldo_sts.com/DefaultStatoApparati/LifeRequest",
			ReplyAction = "http://wsstatoapparati.iec.ansaldo_sts.com/DefaultStatoApparati/LifeResponse")]
		[XmlSerializerFormat(SupportFaults = true)]
		[return: MessageParameter(Name = "return")]
		LifeResponse Life(LifeRequest request);

		// CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
		[OperationContract(Action = "http://wsstatoapparati.iec.ansaldo_sts.com/DefaultStatoApparati/VariazioneRequest" + "",
			ReplyAction = "http://wsstatoapparati.iec.ansaldo_sts.com/DefaultStatoApparati/VariazioneRespons" + "e")]
		[XmlSerializerFormat(SupportFaults = true)]
		[return: MessageParameter(Name = "return")]
		VariazioneResponse Variazione(VariazioneRequest request);

		// CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
		[OperationContract(Action = "http://wsstatoapparati.iec.ansaldo_sts.com/DefaultStatoApparati/StatoTotaleReques" + "t",
			ReplyAction = "http://wsstatoapparati.iec.ansaldo_sts.com/DefaultStatoApparati/StatoTotaleRespon" + "se")]
		[XmlSerializerFormat(SupportFaults = true)]
		[return: MessageParameter(Name = "return")]
		StatoTotaleResponse StatoTotale(StatoTotaleRequest request);
	}
}
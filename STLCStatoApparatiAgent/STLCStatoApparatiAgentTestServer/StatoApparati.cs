﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Schema;
using System.Xml.Serialization;

/// <remarks />
[GeneratedCode("System.Xml", "4.0.30319.33440")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/")]
public class lifeResponseData : object, INotifyPropertyChanged
{
	private long dataField;

	private bool flagAllDataField;

	private bool flagReqStatoTotaleField;

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
	public long data
	{
		get { return this.dataField; }
		set
		{
			this.dataField = value;
			this.RaisePropertyChanged("data");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
	public bool flagAllData
	{
		get { return this.flagAllDataField; }
		set
		{
			this.flagAllDataField = value;
			this.RaisePropertyChanged("flagAllData");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
	public bool flagReqStatoTotale
	{
		get { return this.flagReqStatoTotaleField; }
		set
		{
			this.flagReqStatoTotaleField = value;
			this.RaisePropertyChanged("flagReqStatoTotale");
		}
	}

	public event PropertyChangedEventHandler PropertyChanged;

	protected void RaisePropertyChanged(string propertyName)
	{
		PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
		if ((propertyChanged != null))
		{
			propertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

/// <remarks />
[GeneratedCode("System.Xml", "4.0.30319.33440")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/")]
public class statoTotaleResponseData : object, INotifyPropertyChanged
{
	private long dataField;

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
	public long data
	{
		get { return this.dataField; }
		set
		{
			this.dataField = value;
			this.RaisePropertyChanged("data");
		}
	}

	public event PropertyChangedEventHandler PropertyChanged;

	protected void RaisePropertyChanged(string propertyName)
	{
		PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
		if ((propertyChanged != null))
		{
			propertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

/// <remarks />
[GeneratedCode("System.Xml", "4.0.30319.33440")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/")]
public class datoStatoTotale : object, INotifyPropertyChanged
{
	private string descrizioneGuastoField;

	private bool flagGuastoField;

	private string idApparatoField;

	private int idPPField;

	private int livelloGravitaField;

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
	public string descrizioneGuasto
	{
		get { return this.descrizioneGuastoField; }
		set
		{
			this.descrizioneGuastoField = value;
			this.RaisePropertyChanged("descrizioneGuasto");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
	public bool flagGuasto
	{
		get { return this.flagGuastoField; }
		set
		{
			this.flagGuastoField = value;
			this.RaisePropertyChanged("flagGuasto");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
	public string idApparato
	{
		get { return this.idApparatoField; }
		set
		{
			this.idApparatoField = value;
			this.RaisePropertyChanged("idApparato");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 3)]
	public int idPP
	{
		get { return this.idPPField; }
		set
		{
			this.idPPField = value;
			this.RaisePropertyChanged("idPP");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 4)]
	public int livelloGravita
	{
		get { return this.livelloGravitaField; }
		set
		{
			this.livelloGravitaField = value;
			this.RaisePropertyChanged("livelloGravita");
		}
	}

	public event PropertyChangedEventHandler PropertyChanged;

	protected void RaisePropertyChanged(string propertyName)
	{
		PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
		if ((propertyChanged != null))
		{
			propertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

/// <remarks />
[GeneratedCode("System.Xml", "4.0.30319.33440")]
[Serializable]
[DebuggerStepThrough]
[DesignerCategory("code")]
[XmlType(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/")]
public class variazioneResponseData : object, INotifyPropertyChanged
{
	private long dataField;

	private bool flagAllDataField;

	private bool flagReqStatoTotaleField;

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 0)]
	public long data
	{
		get { return this.dataField; }
		set
		{
			this.dataField = value;
			this.RaisePropertyChanged("data");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 1)]
	public bool flagAllData
	{
		get { return this.flagAllDataField; }
		set
		{
			this.flagAllDataField = value;
			this.RaisePropertyChanged("flagAllData");
		}
	}

	/// <remarks />
	[XmlElement(Form = XmlSchemaForm.Unqualified, Order = 2)]
	public bool flagReqStatoTotale
	{
		get { return this.flagReqStatoTotaleField; }
		set
		{
			this.flagReqStatoTotaleField = value;
			this.RaisePropertyChanged("flagReqStatoTotale");
		}
	}

	public event PropertyChangedEventHandler PropertyChanged;

	protected void RaisePropertyChanged(string propertyName)
	{
		PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
		if ((propertyChanged != null))
		{
			propertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

[DebuggerStepThrough]
[GeneratedCode("System.ServiceModel", "4.0.0.0")]
[EditorBrowsable(EditorBrowsableState.Advanced)]
[MessageContract(WrapperName = "Life", WrapperNamespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", IsWrapped = true)]
public class LifeRequest
{
	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 0)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public int
		idPP;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 1)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		string ipVirtualePP;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 2)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public long
		data;

	public LifeRequest()
	{
	}

	public LifeRequest(int idPP, string ipVirtualePP, long data)
	{
		this.idPP = idPP;
		this.ipVirtualePP = ipVirtualePP;
		this.data = data;
	}
}

[DebuggerStepThrough]
[GeneratedCode("System.ServiceModel", "4.0.0.0")]
[EditorBrowsable(EditorBrowsableState.Advanced)]
[MessageContract(WrapperName = "LifeResponse", WrapperNamespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", IsWrapped = true)]
public class LifeResponse
{
	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 0)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		lifeResponseData @return;

	public LifeResponse()
	{
	}

	public LifeResponse(lifeResponseData @return)
	{
		this.@return = @return;
	}
}

[DebuggerStepThrough]
[GeneratedCode("System.ServiceModel", "4.0.0.0")]
[EditorBrowsable(EditorBrowsableState.Advanced)]
[MessageContract(WrapperName = "Variazione", WrapperNamespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", IsWrapped = true)]
public class VariazioneRequest
{
	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 0)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public int
		idPP;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 1)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		string ipVirtualePP;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 2)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		string idApparato;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 3)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		string descrizioneGuasto;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 4)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public bool
		flagGuasto;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 5)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public int
		livelloGravita;

	public VariazioneRequest()
	{
	}

	public VariazioneRequest(int idPP, string ipVirtualePP, string idApparato, string descrizioneGuasto, bool flagGuasto, int livelloGravita)
	{
		this.idPP = idPP;
		this.ipVirtualePP = ipVirtualePP;
		this.idApparato = idApparato;
		this.descrizioneGuasto = descrizioneGuasto;
		this.flagGuasto = flagGuasto;
		this.livelloGravita = livelloGravita;
	}
}

[DebuggerStepThrough]
[GeneratedCode("System.ServiceModel", "4.0.0.0")]
[EditorBrowsable(EditorBrowsableState.Advanced)]
[MessageContract(WrapperName = "VariazioneResponse", WrapperNamespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", IsWrapped = true)]
public class VariazioneResponse
{
	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 0)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		variazioneResponseData @return;

	public VariazioneResponse()
	{
	}

	public VariazioneResponse(variazioneResponseData @return)
	{
		this.@return = @return;
	}
}

[DebuggerStepThrough]
[GeneratedCode("System.ServiceModel", "4.0.0.0")]
[EditorBrowsable(EditorBrowsableState.Advanced)]
[MessageContract(WrapperName = "StatoTotale", WrapperNamespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", IsWrapped = true)]
public class StatoTotaleRequest
{
	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 0)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public int
		idPP;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 1)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		string ipVirtualePP;

	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 2)] [XmlArray(Form = XmlSchemaForm.Unqualified)] [XmlArrayItem("elencoDati", Form = XmlSchemaForm.Unqualified)] public datoStatoTotale[] dati;

	public StatoTotaleRequest()
	{
	}

	public StatoTotaleRequest(int idPP, string ipVirtualePP, datoStatoTotale[] dati)
	{
		this.idPP = idPP;
		this.ipVirtualePP = ipVirtualePP;
		this.dati = dati;
	}
}

[DebuggerStepThrough]
[GeneratedCode("System.ServiceModel", "4.0.0.0")]
[EditorBrowsable(EditorBrowsableState.Advanced)]
[MessageContract(WrapperName = "StatoTotaleResponse", WrapperNamespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", IsWrapped = true)]
public class StatoTotaleResponse
{
	[MessageBodyMember(Namespace = "http://wsstatoapparati.iec.ansaldo_sts.com/", Order = 0)] [XmlElement(Form = XmlSchemaForm.Unqualified)] public
		statoTotaleResponseData @return;

	public StatoTotaleResponse()
	{
	}

	public StatoTotaleResponse(statoTotaleResponseData @return)
	{
		this.@return = @return;
	}
}
﻿using System;
using System.Globalization;
using System.IO;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace STLCStatoApparatiAgentTestServer
{
    public class StatoApparati : IDefaultStatoApparati
    {
        private const string LOG_FILENAME = "srvStatoApparatiTest.htm";

        public VariazioneResponse Variazione(VariazioneRequest request)
        {
            this.WriteLog(
                string.Format(
                    "Variazione message received. descrizioneGuasto: {0}, flagGuasto: {1}, idApparato: {2}, idPP: {3}, ipVirtualePP: {4}, livelloGravita: {5}",
                    request.descrizioneGuasto, request.flagGuasto, request.idApparato, request.idPP, request.ipVirtualePP, request.livelloGravita));

            return new VariazioneResponse
            {
				@return = new variazioneResponseData { data = this.CurrentTimeMillis(), flagAllData = true, flagReqStatoTotale = true }
            };
        }

        public LifeResponse Life(LifeRequest request)
        {
            this.WriteLog(string.Format("Life message received. data: {0} ({1}), idPP: {2}, ipVirtualePP: {3}", request.data,
                this.FromUnixTime(request.data).ToString(@"dd/MM/yyyy HH\:mm\:ss"), request.idPP, request.ipVirtualePP));

			return new LifeResponse { @return = new lifeResponseData { data = this.CurrentTimeMillis(), flagAllData = true, flagReqStatoTotale = true } };
        }

        public StatoTotaleResponse StatoTotale(StatoTotaleRequest request)
        {
            StringBuilder dumpDati = new StringBuilder();

            foreach (datoStatoTotale dato in request.dati)
            {
                dumpDati.AppendFormat("\r\ndescrizioneGuasto: {0}, flagGuasto: {1}, idApparato: {2}, livelloGravita: {3}, idPP: {4}", dato.descrizioneGuasto,
                    dato.flagGuasto, dato.idApparato, dato.livelloGravita, dato.idPP);
            }

            this.WriteLog(string.Format("StatoTotale message received. idPP: {0}, ipVirtualePP: {1}, dati:{2}", request.idPP, request.ipVirtualePP,
                dumpDati));

            return new StatoTotaleResponse {@return = new statoTotaleResponseData {data = this.CurrentTimeMillis()}};
        }

        #region Utilità

        private void WriteLog(string text)
        {
            string path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, LOG_FILENAME);

            this.AppendStringToFile(path, text);
        }

        private readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public long CurrentTimeMillis()
        {
            return (long)(DateTime.UtcNow - epoch).TotalSeconds;
        }

        public DateTime FromUnixTime(long unixTime)
        {
            return epoch.AddSeconds(unixTime).ToLocalTime();
        }

        private void AppendStringToFile(string filePath, string text)
        {
            if (!String.IsNullOrEmpty(filePath))
            {
                try
                {
                    using (TextWriter fileWriter = new StreamWriter(filePath, true, Encoding.GetEncoding(1252)))
                    {
                        fileWriter.Write("<div style=\"font-family:Consolas,Courier;font-size:10pt;\">[{0}] {1}</div><hr/>",
                            DateTime.Now.ToString(@"yyyy/MM/dd HH\:mm\:ss.fff", CultureInfo.InvariantCulture), HtmlNewLine(text));

                        fileWriter.Flush();
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
                catch (IOException)
                {
                }
                catch (ObjectDisposedException)
                {
                }
                catch (ArgumentException)
                {
                }
                catch (SecurityException)
                {
                }
            }
        }

        private static string HtmlNewLine(string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                return HttpUtility.HtmlEncode(input).Replace("\n", "<br/>");
            }
            return string.Empty;
        }

        #endregion
    }
}
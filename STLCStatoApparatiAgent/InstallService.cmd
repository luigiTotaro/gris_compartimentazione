@echo off
sc stop "STLC Collector Agent Service"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc delete "SCAgentService"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

sc create "STLC Collector Agent Service" binPath= "D:\Lavoro\Projects\Telefin\GrisSuite\STLCStatoApparatiAgent\STLCStatoApparatiAgentService\bin\Debug\STLCStatoApparatiAgentService.exe" start= auto DisplayName= "STLC Collector Agent Service"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL
sc description "STLC Collector Agent Service" "STLC Collector Agent Service"

sc start "STLC Collector Agent Service"
rem Pausa circa 10 secondi
ping 1.1.1.1 -n 1 -w 10000 > NUL

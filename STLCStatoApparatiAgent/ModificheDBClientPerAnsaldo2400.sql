
IF OBJECTPROPERTY(object_id('dbo.[tf_GetDevices_sa]'), N'IsProcedure') = 1
	DROP PROCEDURE [dbo].[tf_GetDevices_sa]
GO
CREATE PROCEDURE [dbo].[tf_GetDevices_sa]
AS
	SET NOCOUNT ON;
	SELECT DevID, NodID, SrvID, Name, COALESCE(DiscoveredType, [Type]) AS [Type], SN, PortID, Addr, ProfileID, Active, Scheduled, Removed, RackID, RackPositionRow, RackPositionCol, DefinitionVersion, ProtocolDefinitionVersion, DiscoveredType, MonitoringDeviceID
	FROM devices
	WHERE ISNULL(Removed,0) = 0
GO

IF OBJECTPROPERTY(object_id('dbo.[sWEB_GetServer_sa]'), N'IsProcedure') = 1
	DROP PROCEDURE [dbo].[sWEB_GetServer_sa]
GO
CREATE PROCEDURE [dbo].[sWEB_GetServer_sa]
AS
SELECT 
		OutServers.SrvID, OutServers.Name, OutServers.Host, OutServers.FullHostName, OutServers.IP, OutServers.LastUpdate, 
		OutServers.LastMessageType, OutServers.SupervisorSystemXML, OutServers.ClientSupervisorSystemXMLValidated, 
		OutServers.ClientValidationSign, OutServers.ClientDateValidationRequested, OutServers.ClientDateValidationObtained, OutServers.ClientKey
	FROM servers OutServers
	WHERE EXISTS
	(
		SELECT *
		FROM servers InnServers
		INNER JOIN devices ON InnServers.SrvID = devices.SrvID
		WHERE InnServers.SrvID = OutServers.SrvID
	);
GO

IF OBJECTPROPERTY(object_id('dbo.[tf_GetServers_sa]'), N'IsProcedure') = 1
	DROP PROCEDURE [dbo].[tf_GetServers_sa]
GO
CREATE PROCEDURE [dbo].[tf_GetServers_sa]
AS
	SET NOCOUNT ON;

	SELECT
		SrvID, [Name], Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML,
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE SrvID IN (SELECT DISTINCT SrvID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
	AND (ISNULL([servers].Removed, 0) = 0)
GO

IF OBJECTPROPERTY(object_id('dbo.[tf_GetDeviceStatus_sa]'), N'IsProcedure') = 1
	DROP PROCEDURE [dbo].[tf_GetDeviceStatus_sa]
GO
CREATE PROCEDURE [dbo].[tf_GetDeviceStatus_sa]
AS
SET NOCOUNT ON;
SELECT device_status.DevID, device_status.SevLevel, device_status.Description, device_status.Offline, 0 as IsDeleted, device_status.ShouldSendNotificationByEmail, devices.Name, devices.MonitoringDeviceID, devices.NodID
FROM device_status
INNER JOIN devices ON device_status.DevID = devices.DevID
WHERE device_status.DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO

IF OBJECTPROPERTY(object_id('dbo.[tf_GetStreams_sa]'), N'IsProcedure') = 1
	DROP PROCEDURE [dbo].[tf_GetStreams_sa]
GO
CREATE PROCEDURE [dbo].[tf_GetStreams_sa]
AS
	SET NOCOUNT ON;
	SELECT DevID, StrID, [Name], [DateTime], SevLevel, Visible, 0 as IsDeleted, Data	
	FROM streams
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO

IF OBJECTPROPERTY(object_id('dbo.[tf_GetStream_Fields_sa]'), N'IsProcedure') = 1
	DROP PROCEDURE [dbo].[tf_GetStream_Fields_sa]
GO
CREATE PROCEDURE [dbo].[tf_GetStream_Fields_sa]
AS
	SET NOCOUNT ON;
	SELECT FieldID, ArrayID, StrID, DevID, Name, SevLevel, Value, Description, Visible, 0 as IsDeleted, ReferenceID, ShouldSendNotificationByEmail
	FROM stream_fields
	WHERE DevID IN (SELECT DISTINCT DevID FROM devices WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO

IF OBJECTPROPERTY(object_id('dbo.[tf_GetReference_sa]'), N'IsProcedure') = 1
	DROP PROCEDURE [dbo].tf_GetReference_sa
GO
CREATE PROCEDURE [dbo].[tf_GetReference_sa]
AS
SET NOCOUNT ON;

	SELECT ReferenceID, Value, DateTime, Visible, DeltaDevID, DeltaStrID, DeltaFieldID, DeltaArrayID
	FROM reference
	WHERE ReferenceID IN 
		(SELECT DISTINCT ReferenceID 
			FROM Stream_Fields INNER JOIN Devices ON Stream_Fields.DevID = Devices.DevID
			WHERE (ISNULL(Removed,0) = 0) AND (devices.MonitoringDeviceID IS NOT NULL))
GO

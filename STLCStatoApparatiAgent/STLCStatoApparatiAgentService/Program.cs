using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using GrisSuite.STLCStatoApparatiAgent;

namespace GrisSuite.STLCStatoApparatiAgentService
{
	internal static class Program
	{
		private static void Main(string[] args)
		{
			bool testComplete = false;
			bool sendStatoApparati = false;
			bool sendLife = false;
			bool createLogSource = false;

			foreach (string arg in args)
			{
				switch (arg)
				{
					case "/t":
						testComplete = true;
						break;
					case "/sa":
						sendStatoApparati = true;
						break;
					case "/l":
						sendLife = true;
						break;
					case "/cl":
						createLogSource = true;
						break;
				}
			}

			if (testComplete)
			{
				try
				{
					Console.WriteLine("Create STLCStatoApparatiAgent");
					Agent agent = new Agent();

					Console.WriteLine("UpdateServerFromEnvironment");
					agent.UpdateServerFromEnvironment();

					string report;
					bool testOK = agent.SendLife(out report);
					Console.WriteLine(report);

					DateTime data = agent.SendStatoApparati(true, true);
					if (data != DateTime.MinValue)
					{
						if (data == DateTime.MaxValue)
						{
							Console.WriteLine("OK TEST: SendStatoApparati Variazione 1/2, No change since last sending");
						}
						else
						{
							Console.WriteLine("OK TEST: SendStatoApparati Variazione 1/2, DATA: {0}", data);
						}
					}
					else
					{
						Console.WriteLine("KO TEST: SendStatoApparati Variazione 1/2");
						testOK = false;
					}

					data = agent.SendStatoApparati(true, true);
					if (data != DateTime.MinValue)
					{
						if (data == DateTime.MaxValue)
						{
							Console.WriteLine("OK TEST: SendStatoApparati Variazione 2/2, No change since last sending");
						}
						else
						{
							Console.WriteLine("OK TEST: SendStatoApparati Variazione 2/2, DATA: {0}", data);
						}
					}
					else
					{
						Console.WriteLine("KO TEST: SendStatoApparati Variazione 2/2");
						testOK = false;
					}

					data = agent.SendStatoApparati(false, true);
					if (data != DateTime.MinValue)
					{
						if (data == DateTime.MaxValue)
						{
							Console.WriteLine("OK TEST: SendStatoApparati Totale, No change since last sending");
						}
						else
						{
							Console.WriteLine("OK TEST: SendStatoApparati Totale, DATA: {0}", data);
						}
					}
					else
					{
						Console.WriteLine("KO TEST: SendStatoApparati Totale");
						testOK = false;
					}

					if (testOK)
					{
						Console.WriteLine("TEST SUCCEDEED");
					}
					else
					{
						Console.WriteLine("TEST FAILED");
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("TEST FAILED, ERROR: {0}", ex.Message);
				}
			}
			else if (sendStatoApparati)
			{
				bool testOK = true;

				try
				{
					Console.WriteLine("Create STLCStatoApparatiAgent");
					Agent agent = new Agent();

					Console.WriteLine("UpdateServerFromEnvironment");
					agent.UpdateServerFromEnvironment();

					agent.ClearCache();
					Console.WriteLine("ClearCache");

					DateTime data = agent.SendStatoApparati(false, true);
					if (data != DateTime.MinValue)
					{
						if (data == DateTime.MaxValue)
						{
							Console.WriteLine("OK TEST: SendStatoApparati Totale, No change since last sending");
						}
						else
						{
							Console.WriteLine("OK TEST: SendStatoApparati Totale, DATA: {0}", data);
						}
					}
					else
					{
						Console.WriteLine("SendStatoApparati Totale KO");
						testOK = false;
					}

					if (testOK)
					{
						Console.WriteLine("TEST SUCCEDEED");
					}
					else
					{
						Console.WriteLine("TEST FAILED");
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("TEST SendStatoApparati Totale FAILED, ERROR: {0}", ex.Message);
				}
			}
			else if (sendLife)
			{
				try
				{
					Console.WriteLine("Create STLCStatoApparatiAgent");
					Agent agent = new Agent();

					Console.WriteLine("UpdateServerFromEnvironment");
					agent.UpdateServerFromEnvironment();

					string report;
					if (agent.SendLife(out report))
					{
						Console.WriteLine(report);
						Console.WriteLine("TEST SUCCEDEED");
					}
					else
					{
						Console.WriteLine(report);
						Console.WriteLine("TEST FAILED");
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("TEST SendLife FAILED, ERROR: {0}", ex.Message);
				}
			}
			else if (createLogSource)
			{
				CreateLogSource();
			}
			else if (Environment.UserInteractive)
			{
				StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
				standardOutput.AutoFlush = true;
				Console.SetOut(standardOutput);

				Console.WriteLine(AppDomain.CurrentDomain.FriendlyName + " se lanciato a linea di comando accetta le seguenti opzioni:");
				Console.WriteLine("/t  = Test di tutti i metodi del servizio");
				Console.WriteLine("/sa  = Invio messaggio stato apparati totale");
				Console.WriteLine("/l  = Invio messaggio life");
				Console.WriteLine("/cl  = Crea la log source {0} nel log {1}", STLCStatoApparatiAgentService.LogSource, STLCStatoApparatiAgentService.LogFile);
			}
			else
			{
				ServiceBase[] ServicesToRun = {new STLCStatoApparatiAgentService()};
				ServiceBase.Run(ServicesToRun);
			}
		}

		private static void CreateLogSource()
		{
			try
			{
				// Registrazione della sorgente di log 
				if (!EventLog.SourceExists(STLCStatoApparatiAgentService.LogSource))
				{
					EventLog.CreateEventSource(STLCStatoApparatiAgentService.LogSource, STLCStatoApparatiAgentService.LogFile);
					Console.WriteLine("OK LogSource {0} in log {1} creato.", STLCStatoApparatiAgentService.LogSource, STLCStatoApparatiAgentService.LogFile);
				}
				else
				{
					Console.WriteLine("LogSource {0} gi� esistente.", STLCStatoApparatiAgentService.LogSource);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Errore nella creazione della LogSource, Error:{0}", ex.Message);
			}
		}
	}
}
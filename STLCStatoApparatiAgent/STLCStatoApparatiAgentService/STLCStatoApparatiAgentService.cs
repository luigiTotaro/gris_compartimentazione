using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using GrisSuite.Properties;
using GrisSuite.STLCStatoApparatiAgent;
using Timer = System.Timers.Timer;

namespace GrisSuite.STLCStatoApparatiAgentService
{
	public partial class STLCStatoApparatiAgentService : ServiceBase
	{
		// ReSharper disable UnusedMember.Local

		//Categoria 7 = Eventi relativi al database
		private enum GrisDatabaseEvents
		{
			OpenConnctionInfo = 4000, //Connessione al database riuscita 0
			OpenConnctionError = 4001, //Connessione al database fallita 2
			CloseConnectionInfo = 4002, //Disconnessione dal database riuscita  0
			CloseConnectionError = 4003, //Disconnessione dal database fallita  2
			QueryInfo = 4004, //Query sul database riuscita 0
			QueryError = 4005 //Query sul database fallita 2
		}

		private enum GrisEventCategory : short
		{
			DeficeLevel1Events = 1, //Eventi di periferica livello 1
			DeficeLevel2Events = 2, //Eventi di periferica livello 2
			DeficeLevel3Events = 3, //Eventi di periferica livello 3
			DeficeLevel4Events = 4, //Eventi di periferica livello 4    
			SystemEvents = 5, //Eventi di Sistema
			ComunicationEvents = 6, //Eventi relativi alle porte di comunicazione
			DatabaseEvents = 7 //Eventi relativi al database
		}

		//    Categoria 5 = Eventi di Sistema
		private enum GrisSystemEvents
		{
			// Generici 
			OkInfo = 4010, //Sistema di diagnostica in stato di funzionamento corretto 0
			Warning = 4011, //Sistema di diagnostica in stato di attenzione 1
			Error = 4012, //Sistema di diagnostica in stato di errore 2
			InitializationStateWarning = 4013, //Sistema di diagnostica in fase di inizializzazione 1
			ClosingStateWarning = 4014, //Sistema di diagnostica in fase di chiusura 1
			SchedulingStartedInfo = 4015, //Schedulazione del sistema di diagnostica avviata 0
			SchedulingStoppedWarning = 4016, //Schedulazione del sistema di diagnostica arrestata 1
			ErrorOnCostructor = 4027, //Errore nel costruttore 
			DBSemaphoreLockWarning = 2028, //Attenzione problema in fase di lock di un semaforo
			DBSemaphoreUnlockWarning = 2029, //Attenzione problema in fase di unlock di un semaforo�
			DBSemaphoreUnlockForcedWarning = 2031, //Attenzione unlock forzato per "timeout" del blocco
			ReenterOnTimerWarning = 2030
			//Rientro nell'evento di un timer prima che l'iterazione precedente sia conclusa
		}

		// ReSharper restore UnusedMember.Local

		private enum STLCStatoApparatiAgentServiceAction
		{
			SendStatoTotale = 130,
			SendVariazione = 131
		}

		private int _errorCounter;
		private readonly int _maxErrors;
		private bool _firstSending = true;

		private readonly int _startDelayMilliseconds = 30000;

		private readonly Agent _agent;

		private Timer _timerSendLife;
		private int _sendLifeInProgress;
		private readonly int _sendLifeIntervalMilliseconds = 20000;

		private Timer _timerSendVariazione;
		private int _sendVariazioneInProgress;
		private readonly int _sendVariazioneIntervalMilliseconds = 5000;

		internal static readonly string LogSource = "GrisSuite.STLCStatoApparatiAgentService";
		internal static readonly string LogFile = "GrisSuite";

		public STLCStatoApparatiAgentService()
		{
			try
			{
				LocalUtil.WriteDebugLogToFile("STLCStatoApparatiAgentService Begin Constructor");

				this.InitializeComponent();
 
                GrisSuite.AppCfg.AppCfg.Default.LogAllCfg();

				this.CanPauseAndContinue = true;
				this.CanShutdown = true;
				this.InitMyEventLog();

				this._maxErrors = Settings.Default.MaxErrors;

				this._agent = new Agent(true);

				this._startDelayMilliseconds = (Settings.Default.StartIntervalSeconds*1000);
				LocalUtil.WriteDebugLogToFile(string.Format("Start delay millseconds: {0}", this._startDelayMilliseconds));

				this._sendLifeIntervalMilliseconds = (Settings.Default.SendLifeIntervalSeconds*1000);
				this._timerSendLife = new Timer(this._startDelayMilliseconds);
				this._timerSendLife.Elapsed += this.TimerSendLife_Elapsed;
				LocalUtil.WriteDebugLogToFile(string.Format("Timer SendLife millseconds: {0}", this._sendLifeIntervalMilliseconds));

				this._sendVariazioneIntervalMilliseconds = (Settings.Default.SendVariazioneIntervalSeconds*1000);
				this._timerSendVariazione = new Timer(this._startDelayMilliseconds);
				this._timerSendVariazione.Elapsed += this.TimerSendVariazione_Elapsed;
				LocalUtil.WriteDebugLogToFile(string.Format("Timer SendVariazione millseconds: {0}", this._sendVariazioneIntervalMilliseconds));

				LocalUtil.WriteDebugLogToFile("STLCStatoApparatiAgentService End Constructor");
			}
			catch (Exception ex)
			{
				this.WriteLog("Error on STLCStatoApparatiAgentService Constructor", ex);

				if (this._timerSendLife != null)
				{
					this._timerSendLife.Stop();
				}

				if (this._timerSendVariazione != null)
				{
					this._timerSendVariazione.Stop();
				}
			}
		}

		internal void InitMyEventLog()
		{
			LocalUtil.WriteDebugLogToFile("Creating event log source");

			// Registrazione della sorgente di log 
			if (!EventLog.SourceExists(LogSource))
			{
				EventLog.CreateEventSource(LogSource, LogFile);
			}
			this.MyEventLog.Log = LogFile;
			this.MyEventLog.Source = LogSource;
		}

		private void TimerSendLife_Elapsed(object sender, ElapsedEventArgs e)
		{
			if (Interlocked.CompareExchange(ref this._sendLifeInProgress, 1, 0) == 0)
			{
				try
				{
					this._agent.UpdateServerFromEnvironment();

					string report;
					this._agent.SendLife(out report);
				}
				catch (Exception ex)
				{
					this.WriteLog("Error on SendLife", ex);
				}
				finally
				{
					this._sendLifeInProgress = 0;
					this._timerSendLife.Interval = this._sendLifeIntervalMilliseconds;
				}
			}
		}

		private void TimerSendVariazione_Elapsed(object sender, ElapsedEventArgs e)
		{
			if (Interlocked.CompareExchange(ref this._sendVariazioneInProgress, 1, 0) == 0)
			{
				try
				{
					this._agent.UpdateServerFromEnvironment();

					if (this._firstSending)
					{
						this.FirstSendStatoApparati();
					}
					else
					{
						this.SendStatoApparati();
					}
				}
				catch (Exception ex)
				{
					this._errorCounter++;
					if (this._errorCounter > 1)
					{
						this.WriteLog("Consecutive Error #{0} on SendStatoApparati", ex, this._errorCounter);
					}
					else
					{
						this.WriteLog("Error on SendStatoApparati", ex);
					}

					if (this._errorCounter >= this._maxErrors)
					{
						this._errorCounter = 0;

						this._timerSendVariazione.Interval = this._sendVariazioneIntervalMilliseconds;

						this.WriteLog("Consecutive max errors ({0}) reached, reset config and status.", null, this._maxErrors);
						this.ClearCacheAndSendStatoApparati();
					}

					this._timerSendVariazione.Interval = this._sendVariazioneIntervalMilliseconds*this._errorCounter;
				}
				finally
				{
					this._sendVariazioneInProgress = 0;
				}
			}
		}

		protected override void OnStart(string[] args)
		{
			base.OnStart(args);
			this._firstSending = true;

			this._timerSendLife.Interval = this._startDelayMilliseconds;
			this._timerSendLife.Start();

			this._timerSendVariazione.Interval = this._startDelayMilliseconds;
			this._timerSendVariazione.Start();
		}

		protected override void OnStop()
		{
			base.OnStop();

			this._timerSendLife.Stop();
			this._timerSendVariazione.Stop();
		}

		protected override void OnContinue()
		{
			base.OnContinue();
			this._firstSending = true;

			this._timerSendLife.Interval = this._startDelayMilliseconds;
			this._timerSendLife.Start();

			this._timerSendVariazione.Interval = this._startDelayMilliseconds;
			this._timerSendVariazione.Start();
		}

		protected override void OnPause()
		{
			base.OnPause();

			this._timerSendLife.Stop();
			this._timerSendVariazione.Stop();
		}

		protected override void OnShutdown()
		{
			base.OnShutdown();

			this._timerSendLife.Stop();
			this._timerSendLife.Dispose();
			this._timerSendLife = null;

			this._timerSendVariazione.Stop();
			this._timerSendVariazione.Dispose();
			this._timerSendVariazione = null;
		}

		private void FirstSendStatoApparati()
		{
			try
			{
				LocalUtil.WriteDebugLogToFile("FirstSendStatoApparati Begin");
				if (this._agent.TrySqlConnect())
				{
					if (Settings.Default.SendStatoApparatiAtServiceStart)
					{
						this.ClearCacheAndSendStatoApparati();
					}

					LocalUtil.WriteDebugLogToFile("FirstSendStatoApparati End");
				}
				else
				{
					throw new Exception("Error on TrySqlConnect");
				}
			}
			finally
			{
				this._firstSending = false;
				this._errorCounter = 0;

				this._timerSendVariazione.Interval = this._sendVariazioneIntervalMilliseconds;
			}
		}

		private void SendStatoApparati()
		{
			// invia solo le righe che sono cambiate rispetto alle colonne definite in configurazione
			this._agent.SendStatoApparati(true, false);

			this._errorCounter = 0;

			this._timerSendVariazione.Interval = this._sendVariazioneIntervalMilliseconds;
		}

		private void ClearCacheAndSendStatoApparati()
		{
			this._agent.ClearCache();
			this._agent.SendStatoApparati(false, true);
		}

		private void WriteLog(string text, Exception ex, params object[] args)
		{
			string sMessage = text;
			if (args != null)
			{
				sMessage = string.Format(text, args);
			}

			if (ex != null)
			{
				sMessage += "\r\nError: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace;
			}

			LocalUtil.WriteDebugLogToFile(sMessage);

			EventLog el = this.EventLog;
			if (!string.IsNullOrEmpty(this.MyEventLog.Source))
			{
				el = this.MyEventLog;
			}

			if (ex is SqlException)
			{
				el.WriteEntry(sMessage, EventLogEntryType.Error, (int) GrisDatabaseEvents.QueryError, (short) GrisEventCategory.DatabaseEvents);
			}
			else
			{
				el.WriteEntry(sMessage, EventLogEntryType.Error, (int) GrisSystemEvents.Error, (short) GrisEventCategory.SystemEvents);
			}
		}

		protected override void OnCustomCommand(int command)
		{
			switch (command)
			{
				case (int) STLCStatoApparatiAgentServiceAction.SendStatoTotale:
				{
					this._agent.SendStatoApparati(false, true);
					break;
				}
				case (int) STLCStatoApparatiAgentServiceAction.SendVariazione:
				{
					this._agent.SendStatoApparati(true, true);
					break;
				}
			}
		}
	}
}
﻿CREATE FUNCTION [dbo].[GetContactsByGroupId] (@GroupId INT, @Type VARCHAR(20) )
RETURNS VARCHAR(MAX) AS
-- Ritorna la lista dei destinatari di rubrica eventualmente associati ad un gruppo
BEGIN
	DECLARE @Contacts AS VARCHAR(MAX);

  IF (@Type = 'windows_user') BEGIN
		SELECT @Contacts = COALESCE(@Contacts + ', ', '') + LTRIM(RTRIM(ISNULL(c.WindowsUser,'')))
		FROM permission_groups_contacts gc
		INNER JOIN contacts c ON gc.ContactId = c.ContactId
		WHERE (gc.GroupId = @GroupId) AND LTRIM(RTRIM(ISNULL(c.WindowsUser,''))) <> ''
		ORDER BY c.Name
  END
  ELSE BEGIN
    SELECT @Contacts = COALESCE(@Contacts + ', ', '') + c.Name + ' (' + c.WindowsUser + ')'
		FROM permission_groups_contacts gc
		INNER JOIN contacts c ON gc.ContactId = c.ContactId
		WHERE (gc.GroupId = @GroupId)
		ORDER BY c.Name
  END

  RETURN LTRIM(RTRIM(ISNULL(@Contacts,'')))
END
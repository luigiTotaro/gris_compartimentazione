﻿CREATE FUNCTION gris_FnFindDevices (@NodIDs NVARCHAR(MAX), @SystemIDs NVARCHAR(MAX), @DeviceTypeIDs NVARCHAR(MAX), @SevLevelIDs NVARCHAR(MAX), @DeviceData NVARCHAR(110))
  RETURNS @devices_list TABLE(DevID BIGINT NOT NULL) AS
  BEGIN

    SET @DeviceData = '%' + LTRIM(RTRIM(@DeviceData)) + '%'

    INSERT INTO @devices_list (DevID)
    SELECT devices.DevID
    FROM devices
      INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
      INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
      INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
      INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
      INNER JOIN nodes ON nodes.NodID = devices.NodID
      INNER JOIN iter_bigintlist_to_tbl(@NodIDs) SelectedNodes ON nodes.NodID = SelectedNodes.number
      INNER JOIN iter_bigintlist_to_tbl(@SystemIDs) SelectedSystems ON device_type.SystemID = SelectedSystems.number
      INNER JOIN iter_varcharlist_to_tbl(@DeviceTypeIDs) SelectedDeviceTypes
        ON dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) = SelectedDeviceTypes.string
      INNER JOIN iter_bigintlist_to_tbl(@SevLevelIDs) SelectedSeverities
        ON object_devices.SevLevelReal = SelectedSeverities.number
      INNER JOIN servers ON devices.SrvID = servers.SrvID
      LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID
    WHERE @DeviceData = '%%'
          OR ISNULL(devices.Name, '') LIKE @DeviceData
          OR ISNULL(devices.SN, '') LIKE @DeviceData
          OR ISNULL(
                 CASE WHEN ISNULL(port.PortType, '') LIKE 'TCP Client'
                   THEN dbo.GetIPStringFromInt(devices.Addr)
                 ELSE CAST(ISNULL(devices.Addr, '') AS VARCHAR(15))
                 END, '') LIKE @DeviceData;

    RETURN
  END
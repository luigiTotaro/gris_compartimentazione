﻿CREATE TABLE [dbo].[permission_groups_contacts] (
    [GroupContactId] BIGINT IDENTITY (1, 1) NOT NULL,
    [GroupId]        INT    NOT NULL,
    [ContactId]      BIGINT NOT NULL,
    CONSTRAINT [permission_groups_contacts_contacts_ContactId_fk] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[contacts] ([ContactId]) ON DELETE CASCADE,
    CONSTRAINT [permission_groups_contacts_permission_groups_GroupID_fk] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[permission_groups] ([GroupID]) ON DELETE CASCADE
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [permission_groups_contacts_GroupContactId_uindex]
    ON [dbo].[permission_groups_contacts]([GroupContactId] ASC);


﻿CREATE TABLE [dbo].[resource_notification_contacts] (
    [ResourceNotificationContactId] INT    IDENTITY (1, 1) NOT NULL,
    [ResourceId]                    INT    NOT NULL,
    [ContactId]                     BIGINT NOT NULL,
    [SendEMail]                     BIT    DEFAULT ((0)) NOT NULL,
    [SendSms]                       BIT    DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ResourceNotificationContactId] ASC),
    CONSTRAINT [resource_notification_contacts_contacts_ContactId_fk] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[contacts] ([ContactId]) ON DELETE CASCADE,
    CONSTRAINT [resource_notification_contacts_resources_ResourceID_fk] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[resources] ([ResourceID]) ON DELETE CASCADE
);


﻿CREATE PROCEDURE [dbo].[gris_DelNotificationContacts] (@ObjectStatusId UNIQUEIDENTIFIER, @ResourceID INT) AS
SET NOCOUNT ON;
BEGIN 
  SET @ResourceID = ISNULL(@ResourceID, 0)
  DELETE FROM notification_contacts 
  WHERE ObjectStatusId = @ObjectStatusId 
        AND (@ResourceID = -1 OR @ResourceId = ISNULL(ResourceId, 0))
END
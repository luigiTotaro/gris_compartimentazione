﻿-- Luca Quintarelli 11/11/2016
-- Ritorna i gruppi che hanno permesso di accesso alle risorse
CREATE PROCEDURE gris_GetResourceAndGroups
  @ResourceTypeID INT,
	@ResourceDescription VARCHAR(1000) = '',
	@GroupName VARCHAR(500) = '',
  @ResourceID INT = -1
AS
BEGIN
	SET NOCOUNT ON;


	SELECT r.ResourceID
    , r.ResourceDescription
    , ISNULL(g.GroupName,'') as GroupName
		, c.ResourceCategoryDescription AS Category
	FROM resources r
	INNER JOIN resource_categories c ON c.ResourceCategoryID = r.ResourceCategoryID
	LEFT JOIN permissions p ON p.ResourceID = r.ResourceID
	LEFT JOIN permission_groups g ON g.GroupID = p.GroupID
	WHERE ResourceTypeID = @ResourceTypeID
    AND (@ResourceID = -1 OR @ResourceID = r.ResourceID)
		AND (ResourceDescription LIKE @ResourceDescription OR @ResourceDescription = '')
		AND (ISNULL(GroupName,'') LIKE @GroupName OR @GroupName = '')
	ORDER BY c.SortOrder, r.SortOrder, g.SortOrder;
END
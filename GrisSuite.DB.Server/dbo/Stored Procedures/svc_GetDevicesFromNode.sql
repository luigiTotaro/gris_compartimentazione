﻿CREATE PROCEDURE [dbo].[svc_GetDevicesFromNode] (@ClientNodId SMALLINT, @SystemID INT = 0, @DeviceId BIGINT = 0) AS
SET NOCOUNT ON;

WITH NodeSystemDevicesWithStatus AS
(
	SELECT devices.DevID,
	object_devices.SevLevelDetailId,
	object_devices.SevLevelReal AS RealSevLevel,
	RealSeverity.Description AS RealSevLevelDescription,
	object_devices.SevLevelLast AS LastSevLevel,
	LastSeverity.Description AS LastSevLevelDescription
	FROM devices
	INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
	INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
	INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
	INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	WHERE (dbo.GetDecodedNodId(nodes.NodID) = @ClientNodId)
	AND ((@SystemID <= 0) OR (device_type.SystemID = @SystemID))
), DevicesWithSelectedStatus AS
(
	SELECT NodeSystemDevicesWithStatus.DevID,
		-- valore
		NodeSystemDevicesWithStatus.RealSevLevel AS SevLevel,
		-- descrizione
		NodeSystemDevicesWithStatus.RealSevLevelDescription AS SevLevelDescription,
		SevLevelDetailId,
		-- flag ultimo stato
		CAST(0 AS BIT) AS LastSevLevelReturned,
		CASE WHEN SevLevelDetailId = 7 OR SevLevelDetailId = 9 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS OperatorMaintenance
	FROM NodeSystemDevicesWithStatus
	INNER JOIN devices ON devices.DevID = NodeSystemDevicesWithStatus.DevID
)
SELECT
DevID, Name, [Type], DeviceSerialNumber, [Address], SevLevel, SevLevelDescription, SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
SystemID, SystemDescription, VendorName, PortName, PortType, PortStatus,
StationName, BuildingName, BuildingDescription, RackName, RackType, RackDescription,
ServerName, ServerFullHostName, ServerIP, RackPositionRow, RackPositionCol,
(
	CASE
		WHEN OperatorMaintenance = 0 THEN SevLevelDescription
		ELSE SevLevelDescription + ' (Non attivo)'
	END
) AS SevLevelDescriptionComplete,
(
	CASE
		WHEN ((RackPositionRow IS NULL) AND (RackPositionCol IS NULL))
			THEN ''
		WHEN ((RackPositionRow IS NOT NULL) AND (RackPositionCol IS NULL))
			THEN 'Posizione: ' + CONVERT(NVARCHAR(10), RackPositionRow) + ', N/D'
		WHEN ((RackPositionRow IS NULL) AND (RackPositionCol IS NOT NULL))
			THEN 'Posizione: N/D, ' + CONVERT(NVARCHAR(10), RackPositionCol)
		WHEN ((RackPositionRow IS NOT NULL) AND (RackPositionCol IS NOT NULL))
			THEN 'Posizione: ' + CONVERT(NVARCHAR(10), RackPositionRow) + ', ' + CONVERT(NVARCHAR(10), RackPositionCol)
		ELSE ''
	END
) AS Position
FROM
(
	SELECT
		-- topografia
		regions.RegID, regions.Name AS Region, zones.Name AS Zone, nodes.Name AS Node,
		-- periferica
		devices.DevID, devices.Name, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS [Type], devices.SN AS DeviceSerialNumber,
		(
			CASE
				WHEN port.PortType LIKE 'TCP Client' THEN dbo.GetIPStringFromInt(devices.Addr)
			ELSE
				CAST(devices.Addr AS VARCHAR(15))
			END
		) AS [Address],
		-- stato
		DevicesWithSelectedStatus.SevLevel, dbo.GetCustomSeverityDescription(SevLevel, SevLevelDetailId, SevLevelDescription) AS SevLevelDescription, SevLevelDetailId AS SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
		-- tipo
		device_type.SystemID, systems.SystemDescription, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) AS VendorName,
		-- porte
		port.PortName, port.PortType, port.Status AS PortStatus,
		-- topografia locale
		ISNULL(station.StationName, '') AS StationName, ISNULL(building.BuildingName, '') AS BuildingName, ISNULL(building.BuildingDescription, '') AS BuildingDescription, ISNULL(rack.RackName, '') AS RackName, ISNULL(rack.RackType, '') AS RackType, ISNULL(rack.RackDescription, '') AS RackDescription,
		-- server
		servers.Name AS ServerName, servers.FullHostName AS ServerFullHostName, servers.IP AS ServerIP,
		ISNULL(devices.RackPositionRow, 0) AS RackPositionRow,
		ISNULL(devices.RackPositionCol, 0) AS RackPositionCol
	FROM devices
	INNER JOIN DevicesWithSelectedStatus ON devices.DevID = DevicesWithSelectedStatus.DevID
	INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
	INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN servers ON devices.SrvID = servers.SrvID
	INNER JOIN systems ON device_type.SystemID = systems.SystemID 
	LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID
	LEFT JOIN rack ON devices.RackID = rack.RackID
	LEFT JOIN building ON rack.BuildingID = building.BuildingID
	LEFT JOIN station ON building.StationID = station.StationID
) DevData
WHERE ((@DeviceId <= 0) OR (DevData.DevID = @DeviceId))
ORDER BY Name;
﻿CREATE PROCEDURE [dbo].[gris_GetPermissionGroupsByResource]
	@ResourceId INT
AS
BEGIN
	SELECT g.GroupID
    , GroupName
    , GroupDescription
    , IsBuiltIn
    , WindowsGroups
    , RegCode
    , Level
    , SortOrder
    ,	CAST(1 as BIT) AS Associated,
     dbo.GetContactsByGroupId(g.GroupID, 'windows_user') AS WindowsUsers
	FROM permission_groups g
    INNER JOIN permissions p ON g.GroupID = p.GroupID
	WHERE p.ResourceId = @ResourceId
	ORDER BY SortOrder;
END
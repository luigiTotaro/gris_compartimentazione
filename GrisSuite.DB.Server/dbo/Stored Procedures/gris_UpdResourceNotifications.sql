﻿CREATE PROCEDURE [dbo].[gris_UpdResourceNotifications](@ResourceId INT, @ContactId BIGINT, @SendSms BIT,
                                                       @SendEMail  BIT) AS
  BEGIN
    SET NOCOUNT ON;

    IF (@SendSms = 1 OR @SendEMail = 1)
      BEGIN
        IF EXISTS(SELECT ResourceNotificationContactId
                  FROM resource_notification_contacts
                  WHERE ResourceId = @ResourceId AND ContactId = @ContactId)
          BEGIN
            UPDATE resource_notification_contacts
            SET SendEMail = @SendEMail, SendSms = @SendSms
            WHERE ResourceId = @ResourceId AND ContactId = @ContactId
          END
        ELSE
          BEGIN
            INSERT INTO resource_notification_contacts (ResourceId, ContactId, SendEMail, SendSms)
            VALUES (@ResourceId, @ContactId, @SendEMail, @SendSms)
          END
      END
    ELSE
      BEGIN
        DELETE FROM resource_notification_contacts
        WHERE ResourceId = @ResourceId AND ContactId = @ContactId
      END
  END
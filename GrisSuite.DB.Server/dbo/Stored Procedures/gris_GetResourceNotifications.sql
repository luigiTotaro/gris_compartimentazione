﻿CREATE PROCEDURE [dbo].[gris_GetResourceNotifications](@ResourceId INT
                                                     , @ConcactFilter VARCHAR(2000) = ''
                                                     , @OnlyEmail BIT = 0
                                                     , @OnlySms BIT = 0) AS
  BEGIN
    SET NOCOUNT ON;
    DECLARE @Filter VARCHAR(2000);
    SET @Filter = '%' + LTRIM(RTRIM(ISNULL(@ConcactFilter, ''))) + '%';

    SELECT DISTINCT c.Name
        , CAST(ISNULL(n.SendEMail,0) as BIT) AS SendEMail
        , CASE WHEN LTRIM(RTRIM(ISNULL(c.Email,''))) = '' THEN '(non disponibile)' ELSE c.Email END AS Email
        , CAST(ISNULL(n.SendSms,0) as BIT) AS SendSms
          , CASE WHEN LTRIM(RTRIM(ISNULL(c.PhoneNumber,''))) = '' THEN '(non disponibile)' ELSE c.PhoneNumber END AS PhoneNumber
        , c.ContactId
        , r.ResourceID as ResourceId
        , (CAST(ISNULL(n.SendEMail,0) as SMALLINT) + CAST(ISNULL(n.SendSms,0) AS SMALLINT)) as Associated
    FROM resources r
        INNER JOIN permissions p ON r.ResourceID = p.ResourceID
        INNER JOIN permission_groups g on p.GroupID = g.GroupID
        INNER JOIN permission_groups_contacts gc ON gc.GroupId = g.GroupID
        INNER JOIN contacts c ON c.ContactId = gc.ContactId
        LEFT JOIN resource_notification_contacts n ON c.ContactId = n.ContactId
    WHERE r.ResourceID = @ResourceId
      AND (@Filter = '%%' OR c.Name LIKE @Filter )
      AND (@OnlyEmail = 0 OR ISNULL(n.SendEMail,0) = 1 )
      AND (@OnlySms = 0 OR ISNULL(n.SendSms,0) = 1 )
    ORDER BY  (CAST(ISNULL(n.SendEMail,0) as SMALLINT) + CAST(ISNULL(n.SendSms,0) AS SMALLINT)) DESC, Name

  END
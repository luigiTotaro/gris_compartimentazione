﻿CREATE PROCEDURE [dbo].[gris_UpdNotificationsByFilters] (@DeviceFilterId BIGINT) AS
  BEGIN
    SET NOCOUNT ON;

    DECLARE @NodIDs VARCHAR(MAX)
    DECLARE @SystemIDs VARCHAR(MAX)
    DECLARE @DeviceTypeIDs VARCHAR(MAX)
    DECLARE @SevLevelIDs VARCHAR(MAX)
    DECLARE @DeviceData VARCHAR(110)
    DECLARE @ResourceId INT

    SELECT
      @ResourceId = ResourceId,
      @NodIDs = NodIDs,
      @SystemIDs = SystemIDs,
      @DeviceTypeIDs = DeviceTypeIDs,
      @SevLevelIDs = '-255 -1 0 1 2 3 9 255', /* per le notifiche includo tutti gli stati altrimenti non intercetto i cambi di stato */
      @DeviceData = DeviceData
    FROM device_filters
    WHERE DeviceFilterId = @DeviceFilterId;

    IF ( @ResourceId IS NOT NULL)
      BEGIN
        DELETE FROM notification_contacts WHERE ResourceId = @ResourceId;

        INSERT INTO notification_contacts (ObjectStatusId, ContactId, ResourceId)
        SELECT DISTINCT o.ObjectStatusId,
          n.ContactId,
          n.ResourceId
         FROM device_filters f
          INNER JOIN resource_notification_contacts n on f.ResourceID = n.ResourceId,
         gris_FnFindDevices(@NodIDs, @SystemIDs, @DeviceTypeIDs,@SevLevelIDs, @DeviceData) as d
         INNER JOIN object_status o ON o.ObjectTypeId = 6 and o.ObjectId = d.DevID
      END
  END
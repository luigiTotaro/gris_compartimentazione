﻿CREATE PROCEDURE [dbo].[gris_GetGroupToAssociate](@GroupSearch VARCHAR(64) = '') AS
  BEGIN
    SET NOCOUNT ON;

    SELECT g.GroupId,
           g.GroupName,
          dbo.GetContactsByGroupId(g.GroupID,'') AS Contacts
    FROM permission_groups g
    WHERE (@GroupSearch = '' OR g.GroupName LIKE @GroupSearch)
    ORDER BY g.SortOrder, g.GroupName ;
  END
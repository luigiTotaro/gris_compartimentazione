﻿CREATE PROCEDURE [dbo].[gris_DelResourceNotifications] (@ResourceId INT) AS
  BEGIN
    SET NOCOUNT ON;
    DELETE FROM resource_notification_contacts WHERE ResourceId = @ResourceId;
  END
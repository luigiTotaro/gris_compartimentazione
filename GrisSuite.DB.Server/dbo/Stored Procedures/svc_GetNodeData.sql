﻿CREATE PROCEDURE svc_GetNodeData (@ClientNodId SMALLINT) AS
SET NOCOUNT ON;

SELECT
dbo.GetDecodedRegId(regions.RegID) AS RegionID,
regions.Name AS RegionName,
dbo.GetDecodedZonId(zones.ZonID) AS ZoneID,
zones.Name AS ZoneName,
dbo.GetDecodedNodId(nodes.NodID) AS NodeID,
nodes.Name AS NodeName
FROM nodes
INNER JOIN zones ON nodes.ZonID = zones.ZonID
INNER JOIN regions ON zones.RegID = regions.RegID
WHERE (dbo.GetDecodedNodId(nodes.NodID) = @ClientNodId)
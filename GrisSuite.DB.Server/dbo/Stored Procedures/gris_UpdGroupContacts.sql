﻿CREATE PROCEDURE [dbo].[gris_UpdGroupContacts](@Associated BIT,
                                                      @GroupId INT,
                                                      @ContactId  BIGINT) AS
  BEGIN
    SET NOCOUNT ON;

    IF (@Associated = 1)
      BEGIN
        IF NOT EXISTS(SELECT GroupContactId
                      FROM permission_groups_contacts
                      WHERE (GroupId = @GroupId) AND (ContactId = @ContactId))
          BEGIN
            INSERT INTO permission_groups_contacts (GroupId, ContactId)
            VALUES (@GroupId, @ContactId)
          END
      END
    ELSE IF (@Associated = 0)
      BEGIN
        DELETE FROM permission_groups_contacts
        WHERE (GroupId = @GroupId) AND (ContactId = @ContactId)
      END
  END
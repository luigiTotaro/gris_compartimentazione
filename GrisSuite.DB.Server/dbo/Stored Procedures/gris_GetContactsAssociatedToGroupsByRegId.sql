﻿CREATE PROCEDURE [dbo].gris_GetContactsAssociatedToGroupsByRegId(@GroupId INT,
                                                                 @RegIds  NVARCHAR(MAX),
                                                                 @Search  NVARCHAR(MAX)) AS
  BEGIN
    SET NOCOUNT ON;

    IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
      SET @Search = ''
    END
    SET @Search = '%' + RTRIM(@Search) + '%'

    SELECT
      @GroupId AS GroupId,
      gc.GroupContactId,
      gc.ContactId,
      contacts.Name   AS ContactName,
      contacts.PhoneNumber,
      contacts.Email,
      WindowsUser,
      CONVERT(BIT, 1) AS Associated
    FROM permission_groups_contacts gc
      INNER JOIN contacts ON gc.ContactId = contacts.ContactId
    WHERE (gc.GroupId = @GroupId)

    UNION ALL

    SELECT
      @GroupId     AS GroupId,
      CONVERT(BIGINT, -1) AS GroupContactId,
      ContactId,
      Name                AS ContactName,
      PhoneNumber,
      Email,
      WindowsUser,
      CONVERT(BIT, 0)     AS Associated
    FROM contacts
    WHERE (CHARINDEX('|' + CAST(RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
          AND (ContactId NOT IN (SELECT gc.ContactId
                                    FROM permission_groups_contacts gc
                                    WHERE (gc.GroupId = @GroupId)))
          AND ISNULL(WindowsUser, '') <> ''
          AND (@Search = '%%'
           OR contacts.Name LIKE @Search)

    ORDER BY Associated DESC, contacts.Name, contacts.PhoneNumber DESC
  END
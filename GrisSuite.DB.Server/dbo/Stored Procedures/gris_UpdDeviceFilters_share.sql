﻿CREATE PROCEDURE [dbo].[gris_UpdDeviceFilters_share](@DeviceFilterName VARCHAR(110), @IsShared BIT) AS
BEGIN
  SET NOCOUNT ON;

  SET @DeviceFilterName = LTRIM(RTRIM(ISNULL(@DeviceFilterName, '')));

  DECLARE @DeviceFilterId BIGINT;
  DECLARE @UserID INT;
  DECLARE @ResourceID INT;

  IF (LEN(@DeviceFilterName) > 0)
    BEGIN
      SELECT
        @DeviceFilterId = DeviceFilterId,
        @ResourceID = ResourceID
      FROM device_filters
      WHERE (DeviceFilterName = @DeviceFilterName);

      IF (@DeviceFilterId IS NOT NULL)
        BEGIN

          IF (@ResourceID IS NULL AND @IsShared = 1)
            BEGIN
              -- crea e associa la risorsa
              INSERT INTO resources (ResourceCode, ResourceDescription, SortOrder, ResourceCategoryID, ResourceTypeID)
              VALUES (NEWID(), @DeviceFilterName, 0, 4 /*FILTRI*/, 2 /*DeviceFilters*/)

              UPDATE device_filters
              SET ResourceID = SCOPE_IDENTITY()
              WHERE (DeviceFilterId = @DeviceFilterId);

            END
          ELSE IF (@ResourceID IS NOT NULL AND @IsShared = 0)
            BEGIN
              -- disassocia e cancella la risorsa
              UPDATE device_filters
              SET ResourceID = NULL
              WHERE (DeviceFilterId = @DeviceFilterId);

              DELETE FROM permissions
              WHERE ResourceID = @ResourceID;

              DELETE FROM resources
              WHERE ResourceID = @ResourceID;
            END

        END

    END
END
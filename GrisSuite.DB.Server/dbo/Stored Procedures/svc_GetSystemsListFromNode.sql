﻿CREATE PROCEDURE svc_GetSystemsListFromNode (@ClientNodId SMALLINT = 0) AS
SET NOCOUNT ON;

IF (@ClientNodId <= 0)
BEGIN
	SELECT systems.SystemID,
	systems.SystemDescription
	FROM systems
	ORDER BY SystemID;
END
ELSE
BEGIN
	SELECT DISTINCT systems.SystemID,
	systems.SystemDescription
	FROM devices
	INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
	INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN systems ON device_type.SystemID = systems.SystemID
	WHERE (dbo.GetDecodedNodId(nodes.NodID) = @ClientNodId)
	ORDER BY systems.SystemID;
END
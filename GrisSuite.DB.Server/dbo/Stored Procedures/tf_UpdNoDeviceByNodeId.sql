﻿CREATE PROCEDURE tf_UpdNoDeviceByNodeId (@NodID BIGINT, @Checked BIT) AS
SET NOCOUNT ON;

IF (@Checked <> 0)
BEGIN
	-- Va creata la device fittizia se non esiste
	IF NOT EXISTS (SELECT DevID FROM devices WHERE (NodID = @NodID) AND (Name LIKE 'NoDevice'))
	BEGIN
		IF NOT EXISTS (SELECT DevID FROM devices WHERE (NodID = @NodID) AND (DevID = @NodID))
		BEGIN
			-- Se la stazione ha delle periferiche associate, è possibile che ne esista una con lo stesso ID, dobbiamo evitare la violazione di chiave
			INSERT INTO devices (DevID, NodID, SrvID, Name, [Type], SN, Addr, PortId, ProfileID, Active, Scheduled, RackID, RackPositionRow, RackPositionCol, DefinitionVersion, ProtocolDefinitionVersion)
			VALUES (@NodID, @NodID, NULL, 'NoDevice', 'NoDevice', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		END
		ELSE
		BEGIN
			RAISERROR ('Impossibile aggiungere la periferica ''NoDevice'', perché esiste già una periferica con lo stesso DevID (la stazione ha già periferiche associate, quindi la ''NoDevice'' è inutile)', 16, 1 );
		END
	END
	ELSE
	BEGIN
		-- Aggiorniamo per coerenza
		UPDATE devices
		SET DevID = @NodID,
		SrvID = NULL,
		[Type] = 'NoDevice',
		SN = NULL,
		Addr = NULL,
		PortId = NULL,
		ProfileID = NULL,
		Active = NULL,
		Scheduled = NULL,
		RackID = NULL,
		RackPositionRow = NULL,
		RackPositionCol = NULL,
		DefinitionVersion = NULL,
		ProtocolDefinitionVersion = NULL
		WHERE (NodID = @NodID) AND (Name LIKE 'NoDevice');
	END
END
ELSE
BEGIN
	-- Va rimossa l'eventuale device fittizia
	IF EXISTS (SELECT DevID FROM devices WHERE (NodID = @NodID) AND (Name LIKE 'NoDevice'))
	BEGIN
		-- Queste periferiche fittizie non devono avere dati correlati associati
		DELETE FROM devices
		WHERE (NodID = @NodID) AND (Name LIKE 'NoDevice')
	END	
END
﻿CREATE PROCEDURE svc_GetStreamsFieldsDataByDevice (@DevID BIGINT) AS
SET NOCOUNT ON;
	
SELECT
devices.DevID,
streams.StrID,
streams.Name AS StreamName,
streams.SevLevel AS StreamSevLevel,
streams.DateTime AS [Date],
stream_fields.FieldID,
stream_fields.ArrayID,
stream_fields.Name AS StreamFieldName,
stream_fields.SevLevel AS StreamFieldSevLevel,
REPLACE(REPLACE(REPLACE(stream_fields.Value, CHAR(13) COLLATE SQL_Latin1_General_CP1_CI_AS, ''), CHAR(10), ''), CHAR(0), '') AS StreamFieldValue,
stream_fields.Description StreamFieldDescription
FROM devices 
INNER JOIN stream_fields ON devices.DevID = stream_fields.DevID 
INNER JOIN streams ON stream_fields.DevID = streams.DevID AND stream_fields.StrID = streams.StrID  
WHERE (devices.DevID = @DevID) 
AND (stream_fields.Visible = 1) 
AND (stream_fields.StrID IN (SELECT StrID FROM streams AS streams_1 WHERE (DevID = @DevID) AND (Visible = 1))) 
ORDER BY streams.StrID, stream_fields.FieldID, stream_fields.ArrayID
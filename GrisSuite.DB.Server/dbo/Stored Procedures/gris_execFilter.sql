﻿CREATE PROCEDURE [dbo].[gris_execFilter] (@DeviceFilterId BIGINT) AS
  BEGIN
    SET NOCOUNT ON;

    DECLARE @NodIDs VARCHAR(MAX)
    DECLARE @SystemIDs VARCHAR(MAX)
    DECLARE @DeviceTypeIDs VARCHAR(MAX)
    DECLARE @SevLevelIDs VARCHAR(MAX)
    DECLARE @DeviceData VARCHAR(110)
    DECLARE @ResourceId INT

    SELECT
      @ResourceId = ResourceId,
      @NodIDs = NodIDs,
      @SystemIDs = SystemIDs,
      @DeviceTypeIDs = DeviceTypeIDs,
      @SevLevelIDs = SevLevelIDs,
      @DeviceData = DeviceData
    FROM device_filters
    WHERE DeviceFilterId = @DeviceFilterId;

    exec gris_GetFindDevices @NodIDs, @SystemIDs, @DeviceTypeIDs, @SevLevelIDs, @DeviceData
  END

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'AlertsButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('AlertsButtonVisible', 'false', 'Visualizza il bottone di accesso alla pagina degli Allarmi')

END
GO

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'ReportsButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('ReportsButtonVisible', 'true', 'Visualizza il bottone di accesso alla pagina dei reports')

END
GO

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'MappaButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('MappaButtonVisible', 'true', 'Visualizza il bottone di accesso alla pagina della mappa')

END
GO

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'MappaButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('MappaButtonVisible', 'true', 'Visualizza il bottone di accesso alla pagina della mappa')

END
GO


IF NOT EXISTS (	SELECT GroupName	FROM dbo.permission_groups	WHERE GroupName = 'groupNoGris')
BEGIN
	INSERT INTO dbo.permission_groups( GroupName, GroupDescription, IsBuiltIn, WindowsGroups, RegCode, Level, SortOrder )
	VALUES( 'groupNoGris', 'Utenti senza l''accesso a Gris ma con l''accesso ai filtri', 1, '', 0, 99, 0 );
END;
GO

IF NOT EXISTS (	SELECT GroupName	FROM dbo.permissions p
  INNER JOIN permission_groups g on p.GroupID = g.GroupID
WHERE GroupName = 'groupNoGris' and p.ResourceID = 9 /*filters*/)
BEGIN
	DECLARE @gID AS Int;
	SET @gID = (SELECT GroupID FROM permission_groups WHERE GroupName = 'groupNoGris');
	INSERT INTO dbo.permissions( GroupId, ResourceID  )
	VALUES( @gID, 9);
END;
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.columns
    WHERE Name      = N'ResourceID'
      AND Object_ID = Object_ID(N'device_filters'))
BEGIN
  ALTER TABLE dbo.device_filters ADD ResourceID INT NULL;

  ALTER TABLE dbo.device_filters ADD CONSTRAINT
    FK_device_filters_resources FOREIGN KEY
    (ResourceID) REFERENCES dbo.resources	(ResourceID);

END
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.columns
    WHERE Name      = N'FilterVisible'
      AND Object_ID = Object_ID(N'systems'))
BEGIN
  ALTER TABLE dbo.systems ADD FilterVisible BIT NOT NULL DEFAULT 0;
END
GO


UPDATE systems SET FilterVisible = 1 WHERE SystemID = 1;  /*Diffusione Sonora*/
UPDATE systems SET FilterVisible = 1 WHERE SystemID = 2;  /*Informazione visiva*/
UPDATE systems SET FilterVisible = 0 WHERE SystemID = 3;  /*Sistema Illuminazione*/
UPDATE systems SET FilterVisible = 0 WHERE SystemID = 4;  /*Sistema Telefonia*/
UPDATE systems SET FilterVisible = 0 WHERE SystemID = 5;  /*Rete*/
UPDATE systems SET FilterVisible = 1 WHERE SystemID = 6;  /*Sistema Erogazione Energia*/
UPDATE systems SET FilterVisible = 1 WHERE SystemID = 8;  /*Sistema FDS*/
UPDATE systems SET FilterVisible = 0 WHERE SystemID = 10;	/*Diagnostica*/
UPDATE systems SET FilterVisible = 1 WHERE SystemID = 11;	/*Facilities*/
UPDATE systems SET FilterVisible = 0 WHERE SystemID = 12;	/*Monitoraggio VPN Verde*/
UPDATE systems SET FilterVisible = 0 WHERE SystemID = 13;	/*Web Radio*/
UPDATE systems SET FilterVisible = 1 WHERE SystemID = 14;	/*Orologi*/
UPDATE systems SET FilterVisible = 1 WHERE SystemID = 15;	/*Ascensori*/
UPDATE systems SET FilterVisible = 0 WHERE SystemID = 99;	/*Altre Periferiche*/

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_GetSystems]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[gris_GetSystems]
END
GO

CREATE PROCEDURE [dbo].[gris_GetSystems] AS
  BEGIN
    SET NOCOUNT ON;

    SELECT
      1                 AS Seq,
      SystemID,
      SystemDescription,
      FilterVisible
    FROM systems
    UNION ALL
    SELECT
      0                 AS Seq,
      99998             AS SystemID,
      'Seleziona il sistema' AS SystemDescription,
      CAST(1 AS BIT)    AS FilterVisible
    ORDER BY Seq, SystemID
  END
GO



IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_GetPermissionGroupsByResource]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[gris_GetPermissionGroupsByResource]
END
GO

CREATE PROCEDURE [dbo].[gris_GetPermissionGroupsByResource]
	@ResourceId INT
AS
BEGIN
	SELECT g.GroupID
    , GroupName
    , GroupDescription
    , IsBuiltIn
    , WindowsGroups
    , RegCode
    , Level
    , SortOrder
    ,	CAST(1 as BIT) AS Associated,
     dbo.GetContactsByGroupId(g.GroupID, 'windows_user') AS WindowsUsers
	FROM permission_groups g
    INNER JOIN permissions p ON g.GroupID = p.GroupID
	WHERE p.ResourceId = @ResourceId
	ORDER BY SortOrder;
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_UpdDeviceFilters_share]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[gris_UpdDeviceFilters_share]
END
GO


CREATE PROCEDURE [dbo].[gris_UpdDeviceFilters_share](@DeviceFilterName VARCHAR(110), @IsShared BIT) AS
BEGIN
  SET NOCOUNT ON;

  SET @DeviceFilterName = LTRIM(RTRIM(ISNULL(@DeviceFilterName, '')));

  DECLARE @DeviceFilterId BIGINT;
  DECLARE @UserID INT;
  DECLARE @ResourceID INT;

  IF (LEN(@DeviceFilterName) > 0)
    BEGIN
      SELECT
        @DeviceFilterId = DeviceFilterId,
        @ResourceID = ResourceID
      FROM device_filters
      WHERE (DeviceFilterName = @DeviceFilterName);

      IF (@DeviceFilterId IS NOT NULL)
        BEGIN

          IF (@ResourceID IS NULL AND @IsShared = 1)
            BEGIN
              -- crea e associa la risorsa
              INSERT INTO resources (ResourceCode, ResourceDescription, SortOrder, ResourceCategoryID, ResourceTypeID)
              VALUES (NEWID(), @DeviceFilterName, 0, 4 /*FILTRI*/, 2 /*DeviceFilters*/)

              UPDATE device_filters
              SET ResourceID = SCOPE_IDENTITY()
              WHERE (DeviceFilterId = @DeviceFilterId);

            END
          ELSE IF (@ResourceID IS NOT NULL AND @IsShared = 0)
            BEGIN
              -- disassocia e cancella la risorsa
              UPDATE device_filters
              SET ResourceID = NULL
              WHERE (DeviceFilterId = @DeviceFilterId);

              DELETE FROM permissions
              WHERE ResourceID = @ResourceID;

              DELETE FROM resources
              WHERE ResourceID = @ResourceID;
            END

        END

    END
END

GO

ALTER PROCEDURE [dbo].[gris_InsDeviceFilters] (@Username      VARCHAR(256), @DeviceFilterName VARCHAR(110),
                                               @RegIDs        VARCHAR(MAX), @RegNames VARCHAR(MAX),
                                               @ZonIDs        VARCHAR(MAX), @ZonNames VARCHAR(MAX),
                                               @NodIDs        VARCHAR(MAX), @NodNames VARCHAR(MAX),
                                               @SystemIDs     VARCHAR(MAX), @SystemNames VARCHAR(MAX),
                                               @DeviceTypeIDs VARCHAR(MAX), @DeviceTypeNames VARCHAR(MAX),
                                               @SevLevelIDs   VARCHAR(MAX), @DeviceData VARCHAR(110),
                                               @IsShared      BIT) AS
BEGIN
  SET NOCOUNT ON;

  SET @DeviceFilterName = LTRIM(RTRIM(ISNULL(@DeviceFilterName, '')));

  DECLARE @DeviceFilterId BIGINT;
  DECLARE @UserID INT;
  DECLARE @ResourceID INT;

  IF (LEN(@DeviceFilterName) > 0) BEGIN
      SELECT @DeviceFilterId = DeviceFilterId
      FROM device_filters
      WHERE (DeviceFilterName LIKE @DeviceFilterName);

      SELECT @UserID = UserID
      FROM users
      WHERE (Username LIKE @Username);

      IF (@UserID IS NULL)
        BEGIN
          -- Se l'utente non esiste in base dati, lo creiamo al volo, usando la username, unico dato disponibile su Gris
          -- l'eventuale modifica dei dati anagrafici può essere fatta su Geta
          INSERT INTO users (Username, FirstName, LastName, OrganizationID, UserGroupID, MobilePhone, Email)
          VALUES (@Username, NULL, NULL, NULL, NULL, NULL, NULL);

          SET @UserID = CAST(SCOPE_IDENTITY() AS INT);
        END

      IF (@DeviceFilterId IS NULL)
        BEGIN
          INSERT INTO device_filters (DeviceFilterName, UserID, RegIDs, RegNames, ZonIDs, ZonNames, NodIDs, NodNames, SystemIDs, SystemNames, DeviceTypeIDs, DeviceTypeNames, SevLevelIDs, DeviceData)
          VALUES (@DeviceFilterName, @UserID, @RegIDs, @RegNames, @ZonIDs, @ZonNames, @NodIDs, @NodNames, @SystemIDs,
                                     @SystemNames, @DeviceTypeIDs, @DeviceTypeNames, @SevLevelIDs, @DeviceData);

          SET @DeviceFilterId = CAST(SCOPE_IDENTITY() AS BIGINT);
          SET @ResourceID = (SELECT ResourceID FROM device_filters WHERE DeviceFilterId = @DeviceFilterId)
        END
      ELSE
        BEGIN
          UPDATE device_filters
          SET
            DeviceFilterName = @DeviceFilterName,
            UserID           = @UserID,
            RegIDs           = @RegIDs,
            RegNames         = @RegNames,
            ZonIDs           = @ZonIDs,
            ZonNames         = @ZonNames,
            NodIDs           = @NodIDs,
            NodNames         = @NodNames,
            SystemIDs        = @SystemIDs,
            SystemNames      = @SystemNames,
            DeviceTypeIDs    = @DeviceTypeIDs,
            DeviceTypeNames  = @DeviceTypeNames,
            SevLevelIDs      = @SevLevelIDs,
            DeviceData       = @DeviceData
          WHERE (DeviceFilterId = @DeviceFilterId);
        END
      -- update resource description
      UPDATE r
      SET r.ResourceDescription = f.DeviceFilterName
      FROM resources r INNER JOIN device_filters f ON r.ResourceID = ISNULL(f.ResourceID, 0)
      WHERE f.DeviceFilterId = @DeviceFilterId

      SET @ResourceID = (SELECT ResourceID FROM device_filters WHERE DeviceFilterId = @DeviceFilterId)
      SET @IsShared = ISNULL(@IsShared, 0)
      IF ((@ResourceID IS NULL AND @IsShared = 1) OR (@ResourceID IS NOT NULL AND @IsShared = 0) )BEGIN
        EXEC gris_UpdDeviceFilters_share @DeviceFilterName, @IsShared
        SET @ResourceID = (SELECT ResourceID FROM device_filters WHERE DeviceFilterId = @DeviceFilterId)
      END
  END


  SELECT
    ISNULL(@DeviceFilterId, CAST(0 AS BIGINT)) AS DeviceFilterId,
    @DeviceFilterName                          AS DeviceFilterName,
    ISNULL(@UserID, 0)                         AS UserID,
    ISNULL(@ResourceID, 0)                     AS ResourceID
END
GO

ALTER PROCEDURE [dbo].[gris_DelDeviceFilters] (@DeviceFilterId BIGINT) AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @ResourceID INT
  DECLARE @DeviceFilterName VARCHAR(110)

  SELECT @ResourceID = ResourceId, @DeviceFilterName=DeviceFilterName FROM device_filters WHERE DeviceFilterId = @DeviceFilterId

  IF (@ResourceID IS NOT NULL) BEGIN
      EXEC gris_UpdDeviceFilters_share @DeviceFilterName, 0
  END

  DELETE FROM device_filters
  WHERE DeviceFilterId = @DeviceFilterId
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_GetResourceAndGroups]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[gris_GetResourceAndGroups]
END
GO


-- Luca Quintarelli 11/11/2016
-- Ritorna i gruppi che hanno permesso di accesso alle risorse
CREATE PROCEDURE gris_GetResourceAndGroups
  @ResourceTypeID INT,
	@ResourceDescription VARCHAR(1000) = '',
	@GroupName VARCHAR(500) = '',
  @ResourceID INT = -1
AS
BEGIN
	SET NOCOUNT ON;


	SELECT r.ResourceID
    , r.ResourceDescription
    , ISNULL(g.GroupName,'') as GroupName
		, c.ResourceCategoryDescription AS Category
	FROM resources r
	INNER JOIN resource_categories c ON c.ResourceCategoryID = r.ResourceCategoryID
	LEFT JOIN permissions p ON p.ResourceID = r.ResourceID
	LEFT JOIN permission_groups g ON g.GroupID = p.GroupID
	WHERE ResourceTypeID = @ResourceTypeID
    AND (@ResourceID = -1 OR @ResourceID = r.ResourceID)
		AND (ResourceDescription LIKE @ResourceDescription OR @ResourceDescription = '')
		AND (ISNULL(GroupName,'') LIKE @GroupName OR @GroupName = '')
	ORDER BY c.SortOrder, r.SortOrder, g.SortOrder;
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_GetResourceCategories]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
  DROP PROCEDURE gris_GetResourceCategories
END
GO

-- Luca Quintarelli 11/11/2016
CREATE PROCEDURE gris_GetResourceCategories
  @ResourceTypeID INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT rc.ResourceCategoryID, ResourceCategoryDescription, rc.SortOrder
	FROM resource_categories rc
	INNER JOIN resources r ON r.ResourceCategoryID = rc.ResourceCategoryID
	WHERE r.ResourceTypeID = @ResourceTypeID
	ORDER BY rc.SortOrder;
END
GO

ALTER PROCEDURE [dbo].[gris_GetResourceByCategory] (@ResourceTypeID SMALLINT,
                                                    @ResourceCategoryID INT,
                                                    @ResourceID     INT = -1) AS
SET NOCOUNT ON;

SELECT DISTINCT
  r.ResourceID,
  r.ResourceCode,
  r.ResourceDescription,
  dbo.GetPermissionGroupsByResource(r.ResourceID) AS WindowsGroups,
  c.ResourceCategoryID,
  c.ResourceCategoryCode,
  c.ResourceCategoryDescription,
  r.SortOrder,
  ISNULL(g.GroupName,'') as GroupName
FROM resources r
  INNER JOIN resource_categories c ON r.ResourceCategoryID = c.ResourceCategoryID
  LEFT JOIN permissions p ON r.ResourceID = p.ResourceID
  LEFT JOIN permission_groups g ON g.GroupID = p.GroupID
WHERE (r.ResourceTypeID = @ResourceTypeID)
      AND ((@ResourceCategoryID = -1) OR (c.ResourceCategoryID = @ResourceCategoryID))
      AND ((@ResourceID = -1) OR (r.ResourceID = @ResourceID))
ORDER BY r.SortOrder;
GO

ALTER PROCEDURE [dbo].[gris_GetDeviceFilterDataByID] (@DeviceFilterId BIGINT) AS
BEGIN
  SET NOCOUNT ON;

  SELECT
    DeviceFilterName,
    UserID,
    RegIDs,
    RegNames,
    ZonIDs,
    ZonNames,
    NodIDs,
    NodNames,
    SystemIDs,
    SystemNames,
    DeviceTypeIDs,
    DeviceTypeNames,
    SevLevelIDs,
    DeviceData,
    (SELECT Username
     FROM users
     WHERE (UserID LIKE device_filters.UserID)) AS Username,
    CAST(CASE WHEN ResourceID IS NULL
      THEN 0
         ELSE 1 END AS BIT)                     AS IsShared,
    ResourceID
  FROM device_filters
  WHERE (DeviceFilterId = @DeviceFilterId)
END
GO

ALTER PROCEDURE [dbo].[gris_GetDeviceFiltersListByUser] (@Username VARCHAR(256)) AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @UserID INT;
  SELECT @UserID = UserID FROM users WHERE (Username LIKE @Username);

  IF (@UserID IS NULL)
  BEGIN
    -- Se l'utente non esiste in base dati, lo creiamo al volo, usando la username, unico dato disponibile su Gris
    -- l'eventuale modifica dei dati anagrafici può essere fatta su Geta
    INSERT INTO users (Username, FirstName, LastName, OrganizationID, UserGroupID, MobilePhone, Email)
    VALUES (@Username, NULL, NULL, NULL, NULL, NULL, NULL);

    SET @UserID = CAST(SCOPE_IDENTITY() AS INT);
  END

  SELECT DeviceFilterId, DeviceFilterName, UserID, ResourceID
  FROM device_filters
  WHERE (UserID = ISNULL(@UserID, 0) OR ISNULL(ResourceID, 0) <> 0 )
  ORDER BY DeviceFilterName
END
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.columns
    WHERE Name      = N'WindowsUser'
      AND Object_ID = Object_ID(N'contacts'))
BEGIN
    ALTER TABLE contacts ADD WindowsUser VARCHAR(500) NULL
END
GO

IF OBJECT_ID('dbo.permission_groups_contacts', 'U') IS NULL
BEGIN
  CREATE TABLE permission_groups_contacts
  (
      GroupContactId BIGINT NOT NULL IDENTITY,
      GroupId INT NOT NULL,
      ContactId BIGINT NOT NULL
  )

  CREATE UNIQUE INDEX permission_groups_contacts_GroupContactId_uindex ON permission_groups_contacts (GroupContactId)

  ALTER TABLE permission_groups_contacts
  ADD CONSTRAINT permission_groups_contacts_permission_groups_GroupID_fk
  FOREIGN KEY (GroupId) REFERENCES permission_groups (GroupID) ON DELETE CASCADE

  ALTER TABLE permission_groups_contacts
  ADD CONSTRAINT permission_groups_contacts_contacts_ContactId_fk
  FOREIGN KEY (ContactId) REFERENCES contacts (ContactId) ON DELETE CASCADE
END
GO

ALTER PROCEDURE [dbo].[gris_GetContactsByRegId] (@RegIds NVARCHAR(MAX)) AS
BEGIN
  SET NOCOUNT ON;

  SELECT
    contacts.ContactId,
    contacts.RegId,
    contacts.Name                                                       AS ContactName,
    contacts.PhoneNumber,
    contacts.Email,
    contacts.WindowsUser,
    regions.Name                                                        AS RegionName,
    dbo.IsContactIdAssociatedToNotificationContacts(contacts.ContactId) AS IsContactIdAssociatedToNotificationContacts
  FROM contacts
    INNER JOIN regions ON contacts.RegId = regions.RegID
  WHERE (CHARINDEX('|' + CAST(contacts.RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
  ORDER BY contacts.RegId, contacts.Name, contacts.PhoneNumber
END
GO

ALTER PROCEDURE gris_InsContact (@RegId BIGINT, @ContactName VARCHAR(2000), @PhoneNumber VARCHAR(500), @Email VARCHAR(500), @WindowsUser VARCHAR(500)) AS
SET NOCOUNT ON;

IF NOT EXISTS (SELECT ContactId FROM contacts WHERE (RegId = @RegId) AND (Name LIKE @ContactName))
BEGIN
	IF (LEN(ISNULL(@ContactName, '')) > 0)
	BEGIN
		INSERT INTO contacts
		(RegId, Name, PhoneNumber, Email, WindowsUser)
		VALUES
		(@RegId, @ContactName, @PhoneNumber, @Email, @WindowsUser);
	END
END
GO

ALTER PROCEDURE gris_UpdContact (@ContactId BIGINT, @ContactName VARCHAR(2000), @PhoneNumber VARCHAR(500), @Email VARCHAR(500), @WindowsUser VARCHAR(500)) AS
SET NOCOUNT ON;

IF NOT EXISTS (SELECT ContactId FROM contacts WHERE (@ContactId <> @ContactId) AND (Name LIKE @ContactName))
BEGIN
	IF (LEN(ISNULL(@ContactName, '')) > 0)
	BEGIN
		UPDATE contacts
		SET
		Name = @ContactName,
		PhoneNumber = @PhoneNumber,
		Email = @Email,
		WindowsUser = @WindowsUser
		WHERE (ContactId = @ContactId);
	END
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[GetContactsByGroupId]')
                AND OBJECTPROPERTY(id, N'IsScalarFunction') = 1 )
BEGIN
    DROP FUNCTION [dbo].[GetContactsByGroupId]
END
GO


CREATE FUNCTION [dbo].[GetContactsByGroupId] (@GroupId INT, @Type VARCHAR(20) )
RETURNS VARCHAR(MAX) AS
-- Ritorna la lista dei destinatari di rubrica eventualmente associati ad un gruppo
BEGIN
	DECLARE @Contacts AS VARCHAR(MAX);

  IF (@Type = 'windows_user') BEGIN
		SELECT @Contacts = COALESCE(@Contacts + ', ', '') + LTRIM(RTRIM(ISNULL(c.WindowsUser,'')))
		FROM permission_groups_contacts gc
		INNER JOIN contacts c ON gc.ContactId = c.ContactId
		WHERE (gc.GroupId = @GroupId) AND LTRIM(RTRIM(ISNULL(c.WindowsUser,''))) <> ''
		ORDER BY c.Name
  END
  ELSE BEGIN
    SELECT @Contacts = COALESCE(@Contacts + ', ', '') + c.Name + ' (' + c.WindowsUser + ')'
		FROM permission_groups_contacts gc
		INNER JOIN contacts c ON gc.ContactId = c.ContactId
		WHERE (gc.GroupId = @GroupId)
		ORDER BY c.Name
  END

  RETURN LTRIM(RTRIM(ISNULL(@Contacts,'')))
END
GO

ALTER PROCEDURE gris_GetPermissionGroupsAll
AS
BEGIN
  SET NOCOUNT ON;

  SELECT
    GroupID,
    GroupName,
    GroupDescription,
    IsBuiltIn,
    WindowsGroups,
    RegCode,
    Level,
    SortOrder,
    (CASE WHEN EXISTS(SELECT r.ResourceID
                      FROM permissions p
                        INNER JOIN resources r ON p.ResourceID = r.ResourceID
                      WHERE p.GroupID = g.GroupID )
          THEN CAST(1 AS BIT)
          ELSE CAST(0 AS BIT)
     END) | IsBuiltIn AS Associated,
    dbo.GetContactsByGroupId(GroupID, 'windows_user') AS WindowsUsers
  FROM permission_groups g
  ORDER BY SortOrder;
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_GetContactsAssociatedToGroupsByRegId]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[gris_GetContactsAssociatedToGroupsByRegId]
END
GO

CREATE PROCEDURE [dbo].gris_GetContactsAssociatedToGroupsByRegId(@GroupId INT,
                                                                 @RegIds  NVARCHAR(MAX),
                                                                 @Search  NVARCHAR(MAX)) AS
  BEGIN
    SET NOCOUNT ON;

    IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
      SET @Search = ''
    END
    SET @Search = '%' + RTRIM(@Search) + '%'

    SELECT
      @GroupId AS GroupId,
      gc.GroupContactId,
      gc.ContactId,
      contacts.Name   AS ContactName,
      contacts.PhoneNumber,
      contacts.Email,
      WindowsUser,
      CONVERT(BIT, 1) AS Associated
    FROM permission_groups_contacts gc
      INNER JOIN contacts ON gc.ContactId = contacts.ContactId
    WHERE (gc.GroupId = @GroupId)

    UNION ALL

    SELECT
      @GroupId     AS GroupId,
      CONVERT(BIGINT, -1) AS GroupContactId,
      ContactId,
      Name                AS ContactName,
      PhoneNumber,
      Email,
      WindowsUser,
      CONVERT(BIT, 0)     AS Associated
    FROM contacts
    WHERE (CHARINDEX('|' + CAST(RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
          AND (ContactId NOT IN (SELECT gc.ContactId
                                    FROM permission_groups_contacts gc
                                    WHERE (gc.GroupId = @GroupId)))
          AND ISNULL(WindowsUser, '') <> ''
          AND (@Search = '%%'
           OR contacts.Name LIKE @Search)
    ORDER BY Associated DESC, contacts.Name, contacts.PhoneNumber DESC
  END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_UpdGroupContacts]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[gris_UpdGroupContacts]
END
GO

CREATE PROCEDURE [dbo].[gris_UpdGroupContacts](@Associated BIT,
                                                      @GroupId INT,
                                                      @ContactId  BIGINT) AS
  BEGIN
    SET NOCOUNT ON;

    IF (@Associated = 1)
      BEGIN
        IF NOT EXISTS(SELECT GroupContactId
                      FROM permission_groups_contacts
                      WHERE (GroupId = @GroupId) AND (ContactId = @ContactId))
          BEGIN
            INSERT INTO permission_groups_contacts (GroupId, ContactId)
            VALUES (@GroupId, @ContactId)
          END
      END
    ELSE IF (@Associated = 0)
      BEGIN
        DELETE FROM permission_groups_contacts
        WHERE (GroupId = @GroupId) AND (ContactId = @ContactId)
      END
  END
GO


IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_GetGroupToAssociate]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE .[gris_GetGroupToAssociate]
END
GO


CREATE PROCEDURE [dbo].[gris_GetGroupToAssociate](@GroupSearch VARCHAR(64) = '') AS
  BEGIN
    SET NOCOUNT ON;

    SELECT g.GroupId,
           g.GroupName,
          dbo.GetContactsByGroupId(g.GroupID,'') AS Contacts
    FROM permission_groups g
    WHERE (@GroupSearch = '' OR g.GroupName LIKE @GroupSearch)
    ORDER BY g.SortOrder, g.GroupName ;
  END
GO

ALTER PROCEDURE dbo.gris_GetPermissionGroupsByGroupNames
  @GroupNames VARCHAR(MAX)
AS
BEGIN
  SELECT
    GroupID,
    GroupName,
    IsBuiltIn,
    WindowsGroups,
    (CASE WHEN EXISTS(SELECT resources.ResourceID
                      FROM permissions
                        INNER JOIN resources ON permissions.ResourceID = resources.ResourceID
                      WHERE (permissions.GroupID = g.GroupID))
      THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT) END) | IsBuiltIn AS Associated,
     dbo.GetContactsByGroupId(GroupID, 'windows_user') AS WindowsUsers
  FROM permission_groups g
  WHERE CHARINDEX('|' + g.GroupName + '|', @GroupNames) > 0
  ORDER BY SortOrder;
END
GO

ALTER PROCEDURE [dbo].[gris_GetPermissionBuiltinGroupsByRegCodAndLevel]
  @RegCode TINYINT,
  @Level   TINYINT
AS
BEGIN
  SET NOCOUNT ON;

  SELECT
    GroupID,
    GroupName,
    IsBuiltIn,
    WindowsGroups,
    RegCode,
    (CASE WHEN EXISTS(SELECT resources.ResourceID
                      FROM permissions
                        INNER JOIN resources ON permissions.ResourceID = resources.ResourceID
                      WHERE (permissions.GroupID = permission_groups.GroupID))
      THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT) END) | IsBuiltIn AS Associated,
     dbo.GetContactsByGroupId(GroupID, 'windows_user') AS WindowsUsers
  FROM permission_groups
  WHERE
    (RegCode = 0 OR RegCode = @RegCode)
    AND ([Level] >= @Level)
    AND (IsBuiltIn = 1)
  ORDER BY SortOrder;
END
GO


IF OBJECT_ID('dbo.resource_notification_contacts', 'U') IS NULL
BEGIN
  CREATE TABLE resource_notification_contacts
  (
      ResourceNotificationContactId INT PRIMARY KEY IDENTITY,
      ResourceId INT NOT NULL,
      ContactId BIGINT NOT NULL,
      SendEMail BIT DEFAULT 0 NOT NULL,
      SendSms BIT DEFAULT 0 NOT NULL
  );

  ALTER TABLE resource_notification_contacts
  ADD CONSTRAINT resource_notification_contacts_resources_ResourceID_fk
  FOREIGN KEY (ResourceId) REFERENCES resources (ResourceID) ON DELETE CASCADE;

  ALTER TABLE resource_notification_contacts
  ADD CONSTRAINT resource_notification_contacts_contacts_ContactId_fk
  FOREIGN KEY (ContactId) REFERENCES contacts (ContactId) ON DELETE CASCADE;
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_GetResourceNotifications]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE dbo.[gris_GetResourceNotifications]
END
GO


CREATE PROCEDURE [dbo].[gris_GetResourceNotifications](@ResourceId INT
                                                     , @ConcactFilter VARCHAR(2000) = ''
                                                     , @OnlyEmail BIT = 0
                                                     , @OnlySms BIT = 0) AS
  BEGIN
    SET NOCOUNT ON;
    DECLARE @Filter VARCHAR(2000);
    SET @Filter = '%' + LTRIM(RTRIM(ISNULL(@ConcactFilter, ''))) + '%';

    SELECT DISTINCT c.Name
        , CAST(ISNULL(n.SendEMail,0) as BIT) AS SendEMail
        , CASE WHEN LTRIM(RTRIM(ISNULL(c.Email,''))) = '' THEN '(non disponibile)' ELSE c.Email END AS Email
        , CAST(ISNULL(n.SendSms,0) as BIT) AS SendSms
          , CASE WHEN LTRIM(RTRIM(ISNULL(c.PhoneNumber,''))) = '' THEN '(non disponibile)' ELSE c.PhoneNumber END AS PhoneNumber
        , c.ContactId
        , r.ResourceID as ResourceId
        , (CAST(ISNULL(n.SendEMail,0) as SMALLINT) + CAST(ISNULL(n.SendSms,0) AS SMALLINT)) as Associated
    FROM resources r
        INNER JOIN permissions p ON r.ResourceID = p.ResourceID
        INNER JOIN permission_groups g on p.GroupID = g.GroupID
        INNER JOIN permission_groups_contacts gc ON gc.GroupId = g.GroupID
        INNER JOIN contacts c ON c.ContactId = gc.ContactId
        LEFT JOIN resource_notification_contacts n ON c.ContactId = n.ContactId and r.ResourceID = n.ResourceId
    WHERE r.ResourceID = @ResourceId
      AND (@Filter = '%%' OR c.Name LIKE @Filter )
      AND (@OnlyEmail = 0 OR ISNULL(n.SendEMail,0) = 1 )
      AND (@OnlySms = 0 OR ISNULL(n.SendSms,0) = 1 )
    ORDER BY  (CAST(ISNULL(n.SendEMail,0) as SMALLINT) + CAST(ISNULL(n.SendSms,0) AS SMALLINT)) DESC, Name

  END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_UpdResourceNotifications]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE dbo.[gris_UpdResourceNotifications]
END
GO


CREATE PROCEDURE [dbo].[gris_UpdResourceNotifications](@ResourceId INT, @ContactId BIGINT, @SendSms BIT,
                                                       @SendEMail  BIT) AS
  BEGIN
    SET NOCOUNT ON;

    IF (@SendSms = 1 OR @SendEMail = 1)
      BEGIN
        IF EXISTS(SELECT ResourceNotificationContactId
                  FROM resource_notification_contacts
                  WHERE ResourceId = @ResourceId AND ContactId = @ContactId)
          BEGIN
            UPDATE resource_notification_contacts
            SET SendEMail = @SendEMail, SendSms = @SendSms
            WHERE ResourceId = @ResourceId AND ContactId = @ContactId
          END
        ELSE
          BEGIN
            INSERT INTO resource_notification_contacts (ResourceId, ContactId, SendEMail, SendSms)
            VALUES (@ResourceId, @ContactId, @SendEMail, @SendSms)
          END
      END
    ELSE
      BEGIN
        DELETE FROM resource_notification_contacts
        WHERE ResourceId = @ResourceId AND ContactId = @ContactId
      END
  END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_DelResourceNotifications]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE dbo.[gris_DelResourceNotifications]
END
GO

CREATE PROCEDURE [dbo].[gris_DelResourceNotifications] (@ResourceId INT) AS
  BEGIN
    SET NOCOUNT ON;
    DELETE FROM resource_notification_contacts WHERE ResourceId = @ResourceId;
  END
GO


ALTER PROCEDURE gris_UpdResourcePermissions
    @GroupIds   VARCHAR(MAX),
    @ResourceID INT
AS
  BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DELETE FROM [permissions]
    WHERE ResourceID = @ResourceID;

    INSERT INTO [permissions] (GroupID, ResourceID)
      SELECT
        GroupID,
        @ResourceID
      FROM permission_groups
      WHERE CHARINDEX('|' + CAST(GroupID AS VARCHAR(4)) + '|', @GroupIds, 0) > 0;

    -- remove resource notification for user without permission
    DELETE FROM resource_notification_contacts
    WHERE ResourceNotificationContactId IN (
      SELECT ResourceNotificationContactId
      FROM resource_notification_contacts n
        INNER JOIN permission_groups_contacts gc ON n.ContactId = gc.ContactId
        LEFT JOIN permissions p ON p.GroupID = gc.GroupId AND p.ResourceID = n.ResourceId
      WHERE n.ResourceId = @ResourceID AND p.PermissionID IS NULL
    );

    SET XACT_ABORT OFF;
  END
GO


IF EXISTS(SELECT *
          FROM sysobjects
          WHERE id = object_id(N'[dbo].[gris_FnFindDevices]')
                AND OBJECTPROPERTY(id, N'IsTableFunction') = 1)
  BEGIN
    DROP FUNCTION [dbo].[gris_FnFindDevices]
  END
GO

CREATE FUNCTION gris_FnFindDevices (@NodIDs NVARCHAR(MAX), @SystemIDs NVARCHAR(MAX), @DeviceTypeIDs NVARCHAR(MAX), @SevLevelIDs NVARCHAR(MAX), @DeviceData NVARCHAR(110))
  RETURNS @devices_list TABLE(DevID BIGINT NOT NULL) AS
  BEGIN

    SET @DeviceData = '%' + LTRIM(RTRIM(@DeviceData)) + '%'

    INSERT INTO @devices_list (DevID)
    SELECT devices.DevID
    FROM devices
      INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
      INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
      INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
      INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
      INNER JOIN nodes ON nodes.NodID = devices.NodID
      INNER JOIN iter_bigintlist_to_tbl(@NodIDs) SelectedNodes ON nodes.NodID = SelectedNodes.number
      INNER JOIN iter_bigintlist_to_tbl(@SystemIDs) SelectedSystems ON device_type.SystemID = SelectedSystems.number
      INNER JOIN iter_varcharlist_to_tbl(@DeviceTypeIDs) SelectedDeviceTypes
        ON dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) = SelectedDeviceTypes.string
      INNER JOIN iter_bigintlist_to_tbl(@SevLevelIDs) SelectedSeverities
        ON object_devices.SevLevelReal = SelectedSeverities.number
      INNER JOIN servers ON devices.SrvID = servers.SrvID
      LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID
    WHERE @DeviceData = '%%'
          OR ISNULL(devices.Name, '') LIKE @DeviceData
          OR ISNULL(devices.SN, '') LIKE @DeviceData
          OR ISNULL(
                 CASE WHEN ISNULL(port.PortType, '') LIKE 'TCP Client'
                   THEN dbo.GetIPStringFromInt(devices.Addr)
                 ELSE CAST(ISNULL(devices.Addr, '') AS VARCHAR(15))
                 END, '') LIKE @DeviceData;

    RETURN
  END
GO

ALTER PROCEDURE [dbo].[gris_GetFindDevices] (@NodIDs NVARCHAR(MAX), @SystemIDs NVARCHAR(MAX), @DeviceTypeIDs NVARCHAR(MAX), @SevLevelIDs NVARCHAR(MAX), @DeviceData NVARCHAR(110)) AS
BEGIN
  SET NOCOUNT ON;

  WITH NodeSystemDevicesWithStatus AS
  (
    SELECT devices.DevID,
    object_devices.SevLevelDetailId,
    object_devices.SevLevelReal AS RealSevLevel,
    RealSeverity.Description AS RealSevLevelDescription,
    object_devices.SevLevelLast AS LastSevLevel,
    LastSeverity.Description AS LastSevLevelDescription
    FROM devices
    INNER JOIN gris_FnFindDevices(@NodIDs, @SystemIDs, @DeviceTypeIDs, @SevLevelIDs, @DeviceData) AS filter ON devices.DevID = filter.DevID
    INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
    INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
    INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
    INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
    INNER JOIN nodes ON nodes.NodID = devices.NodID
  ), DevicesWithSelectedStatus AS
  (
    SELECT NodeSystemDevicesWithStatus.DevID,
      -- valore
      NodeSystemDevicesWithStatus.RealSevLevel AS SevLevel,
      -- descrizione
      NodeSystemDevicesWithStatus.RealSevLevelDescription AS SevLevelDescription,
      SevLevelDetailId,
      -- flag ultimo stato
      CAST(0 AS BIT) AS LastSevLevelReturned,
      CASE WHEN SevLevelDetailId = 7 OR SevLevelDetailId = 9 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS OperatorMaintenance
    FROM NodeSystemDevicesWithStatus
    INNER JOIN devices ON devices.DevID = NodeSystemDevicesWithStatus.DevID
  )
  SELECT
  RegID, Region, Zone, Node, DevID, Name, [Type], DeviceSerialNumber, [Address], SevLevel, SevLevelDescription, SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
  SystemID, SystemDescription, WSUrlPattern, VendorName, PortName, PortType, PortStatus,
  StationName, BuildingName, BuildingDescription, RackName, RackType, RackDescription,
  STLC1000Name, STLC1000FullHostName, STLC1000IP, STLC1000LastUpdate, STLC1000LastMessageType,
  Ticket, RackPositionRow, RackPositionCol, SrvID, RawType, DeviceImage, SevLevelRegion, SevLevelZone, SevLevelNode, RegionColor, ZoneColor, NodeColor,
  DeviceVirtualObjectName, DeviceVirtualObjectDescription, DeviceVirtualObjectIsComputedByCustomFormula,
  (
    CASE
      WHEN OperatorMaintenance = 0 THEN SevLevelDescription
      ELSE SevLevelDescription + ' (Non attivo)'
    END
  ) AS SevLevelDescriptionComplete,
  (
    CASE
      WHEN ((RackPositionRow IS NULL) AND (RackPositionCol IS NULL))
        THEN ''
      WHEN ((RackPositionRow IS NOT NULL) AND (RackPositionCol IS NULL))
        THEN 'Posizione: ' + CONVERT(NVARCHAR(10), RackPositionRow) + ', N/D'
      WHEN ((RackPositionRow IS NULL) AND (RackPositionCol IS NOT NULL))
        THEN 'Posizione: N/D, ' + CONVERT(NVARCHAR(10), RackPositionCol)
      WHEN ((RackPositionRow IS NOT NULL) AND (RackPositionCol IS NOT NULL))
        THEN 'Posizione: ' + CONVERT(NVARCHAR(10), RackPositionRow) + ', ' + CONVERT(NVARCHAR(10), RackPositionCol)
      ELSE ''
    END
  ) AS Position,
  REPLACE(Region, 'Compartimento di ', '') + ' > ' + REPLACE(REPLACE(Zone, 'Linea diramata ', ''), 'Linea ', '') AS Topography,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 255) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C255,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 9) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C9,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 2) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C2,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 1) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C1,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 0) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C0,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = -255) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS Cm255
  FROM
  (
    SELECT
      -- topografia
      regions.RegID, regions.Name AS Region, zones.Name AS Zone, nodes.Name AS Node,
      -- periferica
      devices.DevID, devices.Name, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS [Type], devices.SN AS DeviceSerialNumber,
      (
        CASE
          WHEN port.PortType LIKE 'TCP Client' THEN dbo.GetIPStringFromInt(devices.Addr)
          WHEN port.PortType LIKE 'TCP_Client' THEN dbo.GetIPStringFromInt(devices.Addr)
        ELSE
          CAST(devices.Addr AS VARCHAR(15))
        END
      ) AS [Address],
      -- stato
      DevicesWithSelectedStatus.SevLevel, dbo.GetCustomSeverityDescription(SevLevel, SevLevelDetailId, SevLevelDescription) AS SevLevelDescription, SevLevelDetailId AS SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
      -- tipo
      device_type.SystemID, systems.SystemDescription, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 4, devices.DevID) AS WSUrlPattern, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) AS VendorName,
      -- porte
      port.PortName, port.PortType, port.Status AS PortStatus,
      -- topografia locale
      ISNULL(station.StationName, '') AS StationName, ISNULL(building.BuildingName, '') AS BuildingName, ISNULL(building.BuildingDescription, '') AS BuildingDescription, ISNULL(rack.RackName, '') AS RackName, ISNULL(rack.RackType, '') AS RackType, ISNULL(rack.RackDescription, '') AS RackDescription,
      -- server
      servers.Name AS STLC1000Name, servers.Host AS STLC1000, servers.FullHostName AS STLC1000FullHostName, servers.IP AS STLC1000IP, servers.LastUpdate AS STLC1000LastUpdate, servers.LastMessageType AS STLC1000LastMessageType,
      -- ticket
      ISNULL((SELECT alerts_tickets.TicketName FROM alerts_tickets WHERE (DevID = devices.DevID) AND IsOpen = 1 AND ISNULL(TicketName, '') <> ''),'') AS Ticket, -- il fatto che esista una sola device in questa condizione DOVREBBE essere mantenuto a livello applicativo.
      ISNULL(devices.RackPositionRow, 0) AS RackPositionRow,
      ISNULL(devices.RackPositionCol, 0) AS RackPositionCol,
      devices.SrvID,
      devices.[Type] AS RawType,
      dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 8, devices.DevID) AS DeviceImage,
      dbo.GetSeverityLevelForObject (regions.RegID, NULL, NULL) AS SevLevelRegion,
      dbo.GetSeverityLevelForObject (NULL, zones.ZonID, NULL) AS SevLevelZone,
      dbo.GetSeverityLevelForObject (NULL, NULL, Nodes.NodID) AS SevLevelNode,
      dbo.GetAttributeForObject(8 /* Color */, NULL, regions.RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
      dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, zones.ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor,
      dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, Nodes.NodID, NULL, NULL, NULL, NULL) AS NodeColor,
      dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectName,
      dbo.GetVirtualObjectDataForObject (3 /* VirtualObjectDescription */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectDescription,
      dbo.GetVirtualObjectDataForObject (4 /* IsObjectStatusIdComputedByCustomFormula */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectIsComputedByCustomFormula
    FROM devices
    INNER JOIN DevicesWithSelectedStatus ON devices.DevID = DevicesWithSelectedStatus.DevID
    INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
    INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
    INNER JOIN nodes ON nodes.NodID = devices.NodID
    INNER JOIN zones ON zones.ZonID = nodes.ZonID
    INNER JOIN regions ON regions.RegID = zones.RegID
    INNER JOIN servers ON devices.SrvID = servers.SrvID
    INNER JOIN systems ON device_type.SystemID = systems.SystemID
    INNER JOIN iter_bigintlist_to_tbl(@SevLevelIDs) SelectedSeverities ON DevicesWithSelectedStatus.SevLevel = SelectedSeverities.number
    LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID
    LEFT JOIN rack ON devices.RackID = rack.RackID
    LEFT JOIN building ON rack.BuildingID = building.BuildingID
    LEFT JOIN station ON building.StationID = station.StationID
  ) DevData
  ORDER BY SevLevel DESC, Name;
END
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.columns
    WHERE Name      = N'ResourceId'
      AND Object_ID = Object_ID(N'notification_contacts'))
BEGIN
  ALTER TABLE dbo.notification_contacts ADD ResourceId INT NULL
  ALTER TABLE dbo.notification_contacts
  ADD CONSTRAINT notification_contacts_resources_ResourceID_fk
  FOREIGN KEY (ResourceId) REFERENCES resources (ResourceID) ON DELETE CASCADE
END
GO


IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_UpdNotificationsByFilters]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE dbo.[gris_UpdNotificationsByFilters]
END
GO

CREATE PROCEDURE [dbo].[gris_UpdNotificationsByFilters] (@DeviceFilterId BIGINT) AS
  BEGIN
    SET NOCOUNT ON;

    DECLARE @NodIDs VARCHAR(MAX)
    DECLARE @SystemIDs VARCHAR(MAX)
    DECLARE @DeviceTypeIDs VARCHAR(MAX)
    DECLARE @SevLevelIDs VARCHAR(MAX)
    DECLARE @DeviceData VARCHAR(110)
    DECLARE @ResourceId INT

    SELECT
      @ResourceId = ResourceId,
      @NodIDs = NodIDs,
      @SystemIDs = SystemIDs,
      @DeviceTypeIDs = DeviceTypeIDs,
      @SevLevelIDs = '-255 -1 0 1 2 3 9 255', /* per le notifiche includo tutti gli stati altrimenti non intercetto i cambi di stato */
      @DeviceData = DeviceData
    FROM device_filters
    WHERE DeviceFilterId = @DeviceFilterId;

    IF ( @ResourceId IS NOT NULL)
      BEGIN
        DELETE FROM notification_contacts WHERE ResourceId = @ResourceId;

        INSERT INTO notification_contacts (ObjectStatusId, ContactId, ResourceId)
        SELECT DISTINCT o.ObjectStatusId,
          n.ContactId,
          n.ResourceId
         FROM device_filters f
          INNER JOIN resource_notification_contacts n on f.ResourceID = n.ResourceId,
         gris_FnFindDevices(@NodIDs, @SystemIDs, @DeviceTypeIDs,@SevLevelIDs, @DeviceData) as d
         INNER JOIN object_status o ON o.ObjectTypeId = 6 and o.ObjectId = d.DevID
      END
  END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_execFilter]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE dbo.[gris_execFilter]
END
GO

CREATE PROCEDURE [dbo].[gris_execFilter] (@DeviceFilterId BIGINT) AS
  BEGIN
    SET NOCOUNT ON;

    DECLARE @NodIDs VARCHAR(MAX)
    DECLARE @SystemIDs VARCHAR(MAX)
    DECLARE @DeviceTypeIDs VARCHAR(MAX)
    DECLARE @SevLevelIDs VARCHAR(MAX)
    DECLARE @DeviceData VARCHAR(110)
    DECLARE @ResourceId INT

    SELECT
      @ResourceId = ResourceId,
      @NodIDs = NodIDs,
      @SystemIDs = SystemIDs,
      @DeviceTypeIDs = DeviceTypeIDs,
      @SevLevelIDs = SevLevelIDs,
      @DeviceData = DeviceData
    FROM device_filters
    WHERE DeviceFilterId = @DeviceFilterId;

    exec gris_GetFindDevices @NodIDs, @SystemIDs, @DeviceTypeIDs, @SevLevelIDs, @DeviceData
  END
GO

IF NOT EXISTS(SELECT SystemID FROM systems WHERE SystemID = 15) BEGIN
  INSERT systems (SystemID, SystemDescription) VALUES (15,'Ascensori');
END
GO

UPDATE classifications SET CanFilterBySystemId = 1 WHERE ClassificationId = 4;
GO

ALTER PROCEDURE [dbo].[gris_GetContactsByRegId] (@RegIds NVARCHAR(MAX), @Search NVARCHAR(MAX)) AS
BEGIN
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    contacts.ContactId,
    contacts.RegId,
    contacts.Name                                                       AS ContactName,
    contacts.PhoneNumber,
    contacts.Email,
    contacts.WindowsUser,
    regions.Name                                                        AS RegionName,
    dbo.IsContactIdAssociatedToNotificationContacts(contacts.ContactId) AS IsContactIdAssociatedToNotificationContacts
  FROM contacts
    INNER JOIN regions ON contacts.RegId = regions.RegID
  WHERE (CHARINDEX('|' + CAST(contacts.RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
    AND (@Search = '%%'
        OR contacts.name like @Search
        OR regions.name like @Search
        OR contacts.Email like @Search
        )
  ORDER BY contacts.RegId, contacts.Name, contacts.PhoneNumber
END
GO

ALTER PROCEDURE [dbo].[gris_GetNotificationContactsByRegId](@RegIds VARCHAR(MAX), @Search VARCHAR(MAX)) AS
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    object_status.ObjectStatusId,
    object_status.IsSmsNotificationEnabled,
    regions.Name                                                                 AS RegionName,
    zones.Name                                                                   AS ZoneName,
    nodes.Name                                                                   AS NodeName,
    dbo.GetNotificationContactsByObjectStatusId(object_status.ObjectStatusId, 0) AS NotificationContacts
  FROM object_status
    INNER JOIN nodes ON object_status.ObjectId = nodes.NodID
    INNER JOIN zones ON nodes.ZonID = zones.ZonID
    INNER JOIN regions ON zones.RegID = regions.RegID
  WHERE (CHARINDEX('|' + CAST(regions.RegID AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (object_status.ObjectTypeId = 3 /* nodes */)
        AND (object_status.IsSmsNotificationEnabled <> 0)
    AND (@Search = '%%'
        OR regions.Name like @Search
        OR zones.Name like @Search
        OR nodes.Name like @Search
        )
  ORDER BY RegionName, ZoneName, NodeName
GO

ALTER PROCEDURE [dbo].[gris_GetNotificationMailSystemsToAssociateByRegId](@RegIds         VARCHAR(MAX),
                                                                           @Search VARCHAR(MAX) = '') AS
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    object_status.ObjectStatusId,
    regions.Name                                                                 AS RegionName,
    zones.Name                                                                   AS ZoneName,
    nodes.Name                                                                   AS NodeName,
    systems.SystemDescription                                                    AS SystemName,
    dbo.GetNotificationContactsByObjectStatusId(object_status.ObjectStatusId, 1) AS NotificationContacts
  FROM object_status
    INNER JOIN node_systems ON object_status.ObjectId = node_systems.NodeSystemsId AND object_status.ObjectTypeId = 4
    /* node_systems */
    INNER JOIN systems ON systems.SystemID = node_systems.SystemId
    INNER JOIN nodes ON node_systems.NodId = nodes.NodID
    INNER JOIN zones ON nodes.ZonID = zones.ZonID
    INNER JOIN regions ON zones.RegID = regions.RegID
  WHERE (CHARINDEX('|' + CAST(regions.RegID AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (@Search = '%%'
             OR regions.Name LIKE @Search
             OR zones.Name LIKE @Search
             OR nodes.Name LIKE @Search
             OR systems.SystemDescription LIKE @Search
        )
  ORDER BY RegionName, ZoneName, NodeName, systems.SystemID;
GO

ALTER PROCEDURE [dbo].[gris_GetNotificationSmsContactsToAssociateByRegId](@RegIds         VARCHAR(MAX),
                                                                           @Search VARCHAR(MAX) = '') AS
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    object_status.ObjectStatusId,
    object_status.IsSmsNotificationEnabled,
    regions.Name                                                                 AS RegionName,
    zones.Name                                                                   AS ZoneName,
    nodes.Name                                                                   AS NodeName,
    dbo.GetNotificationContactsByObjectStatusId(object_status.ObjectStatusId, 0) AS NotificationContacts
  FROM object_status
    INNER JOIN nodes ON object_status.ObjectId = nodes.NodID
    INNER JOIN zones ON nodes.ZonID = zones.ZonID
    INNER JOIN regions ON zones.RegID = regions.RegID
  WHERE (CHARINDEX('|' + CAST(regions.RegID AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (object_status.ObjectTypeId = 3 /* nodes */)
        AND (object_status.IsSmsNotificationEnabled = 0)
        AND (object_status.ObjectStatusId NOT IN (
    SELECT object_status.ObjectStatusId
    FROM object_status
      INNER JOIN nodes ON object_status.ObjectId = nodes.NodID
      INNER JOIN zones ON nodes.ZonID = zones.ZonID
      INNER JOIN regions ON zones.RegID = regions.RegID
    WHERE (CHARINDEX('|' + CAST(regions.RegID AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
          AND (object_status.ObjectTypeId = 3 /* nodes */)
          AND (object_status.IsSmsNotificationEnabled <> 0)
  ))
        AND (@Search = '%%'
             OR regions.Name LIKE @Search
             OR zones.Name LIKE @Search
             OR nodes.Name LIKE @Search)
  ORDER BY RegionName, ZoneName, NodeName
GO


ALTER PROCEDURE [dbo].[gris_GetSmsContactsAssociatedToObjectStatusIdByRegId] (@ObjectStatusId UNIQUEIDENTIFIER,
                                                                              @RegIds         NVARCHAR(MAX),
                                                                              @Search         NVARCHAR(MAX)) AS
BEGIN

  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    @ObjectStatusId AS ObjectStatusId,
    notification_contacts.NotificationContactId,
    notification_contacts.ContactId,
    contacts.Name   AS ContactName,
    contacts.PhoneNumber,
    contacts.Email,
    CONVERT(BIT, 1) AS Associated,
    regions.Name    AS RegionName
  FROM notification_contacts
    INNER JOIN contacts ON notification_contacts.ContactId = contacts.ContactId
    INNER JOIN regions ON regions.RegID = contacts.RegId
  WHERE (notification_contacts.ObjectStatusId = @ObjectStatusId)

  UNION ALL

  SELECT
    @ObjectStatusId     AS ObjectStatusId,
    CONVERT(BIGINT, -1) AS NotificationAddresseeId,
    ContactId,
    contacts.Name       AS ContactName,
    PhoneNumber,
    Email,
    CONVERT(BIT, 0)     AS Associated,
    regions.Name        AS RegionName
  FROM contacts
    INNER JOIN regions ON regions.RegID = contacts.RegId
  WHERE (CHARINDEX('|' + CAST(contacts.RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (ContactId NOT IN (
                                SELECT notification_contacts.ContactId
                                FROM notification_contacts
                                WHERE (notification_contacts.ObjectStatusId = @ObjectStatusId)
                              )
        )
        AND ISNULL(PhoneNumber, '') <> ''
        AND (@Search = '%%'
             OR regions.Name LIKE @Search
             OR contacts.Name LIKE @Search)
  ORDER BY Associated DESC, contacts.Name, contacts.PhoneNumber
END
GO

ALTER PROCEDURE [dbo].[gris_GetMailContactsAssociatedToObjectStatusIdByRegId] (@ObjectStatusId UNIQUEIDENTIFIER,
                                                                               @RegIds         NVARCHAR(MAX),
                                                                               @Search         NVARCHAR(MAX)) AS
BEGIN
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    @ObjectStatusId AS ObjectStatusId,
    notification_contacts.NotificationContactId,
    notification_contacts.ContactId,
    contacts.Name   AS ContactName,
    contacts.PhoneNumber,
    contacts.Email,
    CONVERT(BIT, 1) AS Associated,
    regions.Name    AS RegionName
  FROM notification_contacts
    INNER JOIN contacts ON notification_contacts.ContactId = contacts.ContactId
    INNER JOIN regions ON regions.RegID = contacts.RegId
  WHERE (notification_contacts.ObjectStatusId = @ObjectStatusId)

  UNION ALL

  SELECT
    @ObjectStatusId     AS ObjectStatusId,
    CONVERT(BIGINT, -1) AS NotificationAddresseeId,
    ContactId,
    contacts.Name       AS ContactName,
    PhoneNumber,
    Email,
    CONVERT(BIT, 0)     AS Associated,
    regions.Name    AS RegionName
  FROM contacts
    INNER JOIN regions ON contacts.RegId = regions.RegID
  WHERE (CHARINDEX('|' + CAST(contacts.RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (ContactId NOT IN (
              SELECT notification_contacts.ContactId
              FROM notification_contacts
              WHERE (notification_contacts.ObjectStatusId = @ObjectStatusId)
            )
        )
        AND ISNULL(Email, '') <> ''
        AND (@Search = '%%'
           OR regions.Name LIKE @Search
           OR contacts.Name LIKE @Search)
  ORDER BY Associated DESC, contacts.Name, contacts.PhoneNumber
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_DelNotificationContacts]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE dbo.[gris_DelNotificationContacts]
END
GO


CREATE PROCEDURE [dbo].[gris_DelNotificationContacts] (@ObjectStatusId UNIQUEIDENTIFIER, @ResourceID INT) AS
SET NOCOUNT ON;
BEGIN
  SET @ResourceID = ISNULL(@ResourceID, 0)
  DELETE FROM notification_contacts
  WHERE ObjectStatusId = @ObjectStatusId
        AND (@ResourceID = -1 OR @ResourceId = ISNULL(ResourceId, 0))
END
GO

IF EXISTS ( SELECT * FROM sysobjects
              WHERE  id = object_id(N'[dbo].[gris_DelGroupContacts]')
                AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE dbo.[gris_DelGroupContacts]
END
GO

CREATE PROCEDURE [dbo].[gris_DelGroupContacts](@GroupId INT) AS
BEGIN
    SET NOCOUNT ON;
    DELETE FROM permission_groups_contacts
      WHERE (GroupId = @GroupId)
END
GO

UPDATE [dbo].[parameters]
	SET [ParameterValue] = 'true'
	WHERE [ParameterName] = 'FiltersButtonVisible';
GO

UPDATE [dbo].[parameters]
	SET [ParameterValue] = '3.1.0.0'
	WHERE [ParameterName] = 'DBVersion';
GO




SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

BEGIN TRANSACTION
GO
CREATE TABLE dbo.infostaz_categories
	(
	infostazCategoryId int NOT NULL,
	infostazCategoryName varchar(200) NOT NULL,
	infostazTecnology varchar(10)
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.infostaz_categories ADD CONSTRAINT
	PK_infostaz_categories PRIMARY KEY CLUSTERED 
	(
	infostazCategoryId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO

INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (0 ,'Monitor LCD/TFT 32 pollici', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1 ,'Monitor LCD/TFT 17 pollici (sottopasso)', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (2 ,'Binario LED doppia faccia', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (3 ,'Binario LED singola faccia', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (4 ,'Monitor LED 5 righe + 1', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (5 ,'Fascia LED 1 modulo LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (6 ,'Fascia LED 2 moduli LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (7 ,'Fascia LED 3 moduli LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (8 ,'Fascia LED 4 moduli LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (9 ,'Binario LED doppia faccia con orologio analogico', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (10 ,'Monitor LED 9 righe + 1', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (11 ,'Tabellone LED arrivi e partenze 10 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (12 ,'Monitor TFT 23 pollici per totem (con pilotaggio orologio)', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (13 ,'Binario LED (2 righe) sottopasso', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (14 ,'Monitor CRT 25 pollici', 'CRT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (15 ,'Monitor CRT 28 pollici', 'CRT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (16 ,'Monitor LCD/TFT 23 pollici', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (17 ,'Quadro LED 2 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (18 ,'Quadro LED 4 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (19 ,'Quadro LED 6', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (20 ,'Quadro LED 8 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (21 ,'Indicatore di carrozza', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (22 ,'Fascia LED estesa (1 modulo)', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (23 ,'Fascia LED estesa (2 moduli)', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (24 ,'Fascia LED estesa (3 moduli)', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (25 ,'Tabellone LED arrivi e partenze, 8 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (26 ,'Monitor LCD/TFT 23 pollici (sottopasso)', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (27 ,'Monitor LCD/TFT 22 pollici', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (28 ,'Monitor LCD/TFT 22 pollici (sottopasso)', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (29 ,'Fascia LED 1 modulo LED bianchi', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (30 ,'Fascia LED 2 moduli LED bianchi', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (31 ,'Fascia LED 3 moduli LED bianchi', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (32 ,'Fascia LED 4 moduli LED bianchi', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (33 ,'Monitor TFT 22 pollici per totem (con pilotaggio orologio)', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (34 ,'Tabellone LED arrivi e partenze 10 righe', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (35 ,'Tabellone LED arrivi e partenze 4 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (100 ,'Monitor TFT 42 pollici portrait per totem', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1000 ,'Monitor riepilogativo 42 pollici', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1001 ,'Display fascia H800', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1002 ,'Quadro 55 pollici', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1003 ,'Indicatore di carrozza 22 pollici', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1004 ,'Display da binario 1600 con numero binario variabile', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1005 ,'Monitor LED 20 righe', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1006 ,'Tabellone A/P 10+2 righe con intestazione campi variabile', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1007 ,'Display fascia H400 con intestazione dinamica a led bianchi', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1008 ,'Display sottopasso stretch 43 pollici', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1009 ,'Display da binario 1200 con numero binario variabile', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1010 ,'Fascia LED-F1 White+Header', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1011 ,'Fascia 10 Righe LED', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1012 ,'Fascia 10 Righe LED White con intestazione', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1013 ,'Tabellone LED arrivi/partenze (10 righe + 2) bordo largo per loghi', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1014 ,'Tabellone LED arrivi/partenze (10 righe + 2) con intestazione dinamica bordo largo per loghi con campo treni ad alta risoluzione', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1015 ,'Tabellone LED arrivi/partenze (8 righe + 2) con intestazione dinamica bordo largo per loghi con campo treni ad alta risoluzione', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1017 ,'TFT 65`` bifacciale - faccia 1 completa di tutti i sensori', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1018 ,'TFT 65`` bifacciale - faccia 2 povera (si temp e luminosità, no scaldiglie e ventole)', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1019 ,'TFT 65`` monofacciale - faccia 1 completa di tutti i sensori', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1020 ,'Tabellone LED arrivi/partenze (8 righe + 2) con intestazione dinamica', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1021 ,'TFT 32`` standard con due alimentatori', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (1022 ,'TFT 43`` stretch Indicatore di binario da sottopasso TFT', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (500 ,'Monitor LCD/TFT 32 pollici', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (501 ,'Monitor LCD/TFT 17 pollici (sottopasso)', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (502 ,'Binario LED doppia faccia', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (503 ,'Binario LED singola faccia', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (504 ,'Monitor LED 5 righe + 1', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (505 ,'Fascia LED 1 modulo LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (506 ,'Fascia LED 2 moduli LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (507 ,'Fascia LED 3 moduli LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (508 ,'Fascia LED 4 moduli LED gialli', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (509 ,'Binario LED doppia faccia con orologio analogico', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (510 ,'Monitor LED 9 righe + 1', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (511 ,'Tabellone LED arrivi e partenze 10 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (512 ,'Monitor TFT 23 pollici per totem (con pilotaggio orologio)', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (513 ,'Binario LED (2 righe) sottopasso', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (514 ,'Monitor CRT 25 pollici', 'CRT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (515 ,'Monitor CRT 28 pollici', 'CRT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (516 ,'Monitor LCD/TFT 23 pollici', 'TFT' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (517 ,'Quadro LED 2 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (518 ,'Quadro LED 4 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (519 ,'Quadro LED 6', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (520 ,'Quadro LED 8 righe + 2', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (521 ,'Indicatore di carrozza', 'LED' )
INSERT INTO dbo.infostaz_categories (infostazCategoryId, infostazCategoryName, infostazTecnology) VALUES (540 ,'Totem Interattivo Mobility Infopoint', 'LED' )
GO

ALTER FUNCTION [dbo].[GetDeviceInfoFromStreamsByDeviceType]
(
	@DeviceTypeID VARCHAR(16),
	@InfoType TINYINT,
	@DevID BIGINT
)
RETURNS VARCHAR(512)
AS
BEGIN
	DECLARE @ReturnValue VARCHAR(512);
    DECLARE @VendorData VARCHAR(1024);
    DECLARE @CategoryData VARCHAR(1024);
	DECLARE @StrID INT;
	DECLARE @InfostazTecnology VARCHAR(10);
	
	IF (@DeviceTypeID LIKE 'INFSTAZ-IEC') 
		SET @StrID = 1
	ELSE
		SET @StrID = 2;

    IF (@InfoType = 1) -- DeviceTypeID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
			SET @InfostazTecnology = ISNULL((SELECT infostazTecnology FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');
    

            SET @ReturnValue = @DeviceTypeID;

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    SET @ReturnValue = 'AET0000';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'AEL0000';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SYTM100';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SYLB000';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SOTM000';
                END
                ELSE IF (@InfostazTecnology = 'CRT')
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = 'SOCM000';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SOLM000';
                END
            END
        END
        ELSE
        BEGIN
            SET @ReturnValue = @DeviceTypeID;
        END
    END
    ELSE IF (@InfoType = 2) -- VendorName
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))

            SELECT @ReturnValue = vendors.VendorName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = 'Aesys';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = 'Sysco';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = 'Solari';
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = vendors.VendorName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 3) -- VendorID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            
            SELECT @ReturnValue = CONVERT(VARCHAR(10), vendors.VendorID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)            

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = '2';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = '1';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = '3';
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = CONVERT(VARCHAR(10), vendors.VendorID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 4) -- WSUrlPattern
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = (SELECT TOP 1 WSUrlPattern from device_type where VendorID = 2 /*Aesys*/ and SystemID = 2 /*Video*/ );
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = (select TOP 1 WSUrlPattern from device_type where VendorID = 1 /*Sysco*/ and SystemID = 2 /*Video*/ );
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = (select TOP 1 WSUrlPattern from device_type where VendorID = 3 /*Solari*/ and SystemID = 2 /*Video*/ );
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = device_type.WSUrlPattern
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 5) -- TechnologyID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
			SET @InfostazTecnology = ISNULL((SELECT infostazTecnology FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    SET @ReturnValue = '2';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    SET @ReturnValue = '2';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = '2';
                END
                ELSE IF (@CategoryData IN (/* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15'))
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = '1';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = CONVERT(VARCHAR(10), device_type.TechnologyID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 6) -- DeviceTypeDescription
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
			SET @InfostazTecnology = ISNULL((SELECT infostazTecnology FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');
            
            SELECT @ReturnValue = device_type.DeviceTypeDescription
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)            

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'AESYS - Teleindicatore TFT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'AESYS - Teleindicatore LED generico';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SYSCO - Teleindicatore TFT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SYSCO - Teleindicatore LED generico';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SOLARI - Teleindicatore TFT generico';
                END
                ELSE IF (@InfostazTecnology = 'CRT')
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = 'SOLARI - Teleindicatore CRT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SOLARI - Teleindicatore LED generico';
                END
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = device_type.DeviceTypeDescription
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 7) -- Decodifica categoria Infostazioni
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
			SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))           
			SET @ReturnValue = ISNULL((SELECT infostazCategoryName FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');
        END
    END
    ELSE IF (@InfoType = 8) -- Immagine Periferica
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))

            SET @ReturnValue = @DeviceTypeID;

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZAE' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZSY' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;					
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZSO' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;					
            END
        END
        ELSE
        BEGIN
			SELECT @ReturnValue = device_type.ImageName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)   
        END
    END     

	RETURN ISNULL(@ReturnValue, '')
END
GO


ALTER FUNCTION [dbo].[gris_FnFindDevices] (@NodIDs NVARCHAR(MAX), @SystemIDs NVARCHAR(MAX), @DeviceTypeIDs NVARCHAR(MAX), @SevLevelIDs NVARCHAR(MAX), @DeviceData NVARCHAR(110))
  RETURNS @devices_list TABLE(DevID BIGINT NOT NULL) AS
  BEGIN

    SET @DeviceData = '%' + LTRIM(RTRIM(@DeviceData)) + '%'

	IF (@SystemIDs IS NULL or LEN(LTRIM(@SystemIDs)) = 0 ) 
	BEGIN
		SELECT @SystemIDs = COALESCE(@SystemIDs + ' ', '') + CAST(SystemId as VARCHAR)   FROM dbo.node_systems where  NodId in (SELECT number FROM dbo.iter_bigintlist_to_tbl(@NodIDs))
	END

	IF (@DeviceTypeIDs IS NULL or LEN(LTRIM(@DeviceTypeIDs)) = 0 ) 
	BEGIN
		SELECT @DeviceTypeIDs = COALESCE(@DeviceTypeIDs + ' ', '') + CAST(DeviceTypeID as VARCHAR)FROM 
		(SELECT DISTINCT
		device_type.DeviceTypeID
		FROM node_systems
		INNER JOIN nodes ON node_systems.NodId = nodes.NodID
		INNER JOIN devices ON nodes.NodID = devices.NodID
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID AND node_systems.SystemId = device_type.SystemID
		INNER JOIN systems ON node_systems.SystemId = systems.SystemID
		INNER JOIN iter_bigintlist_to_tbl(@NodIDs) SelectedNodes ON nodes.NodID = SelectedNodes.number
		INNER JOIN iter_bigintlist_to_tbl(@SystemIDs) SelectedSystems ON device_type.SystemID = SelectedSystems.number) AS A
	END

    INSERT INTO @devices_list (DevID)
    SELECT devices.DevID
    FROM devices
      INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
      INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
      INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
      INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
      INNER JOIN nodes ON nodes.NodID = devices.NodID
      INNER JOIN iter_bigintlist_to_tbl(@NodIDs) SelectedNodes ON nodes.NodID = SelectedNodes.number
      INNER JOIN iter_bigintlist_to_tbl(@SystemIDs) SelectedSystems ON device_type.SystemID = SelectedSystems.number
      INNER JOIN iter_varcharlist_to_tbl(@DeviceTypeIDs) SelectedDeviceTypes
        ON dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) = SelectedDeviceTypes.string
      INNER JOIN iter_bigintlist_to_tbl(@SevLevelIDs) SelectedSeverities
        ON object_devices.SevLevelReal = SelectedSeverities.number
      INNER JOIN servers ON devices.SrvID = servers.SrvID
      LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID
    WHERE @DeviceData = '%%'
          OR ISNULL(devices.Name, '') LIKE @DeviceData
          OR ISNULL(devices.SN, '') LIKE @DeviceData
          OR ISNULL(
                 CASE WHEN ISNULL(port.PortType, '') LIKE 'TCP Client'
                   THEN dbo.GetIPStringFromInt(devices.Addr)
                 ELSE CAST(ISNULL(devices.Addr, '') AS VARCHAR(15))
                 END, '') LIKE @DeviceData;

    RETURN
  END
GO
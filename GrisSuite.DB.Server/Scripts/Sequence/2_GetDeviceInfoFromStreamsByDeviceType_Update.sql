USE [TelefinCentralPRO]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDeviceInfoFromStreamsByDeviceType]    Script Date: 10/31/2017 15:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetDeviceInfoFromStreamsByDeviceType]
(
	@DeviceTypeID VARCHAR(16),
	@InfoType TINYINT,
	@DevID BIGINT
)
RETURNS VARCHAR(512)
AS
BEGIN
	DECLARE @ReturnValue VARCHAR(512);
    DECLARE @VendorData VARCHAR(1024);
    DECLARE @CategoryData VARCHAR(1024);
	DECLARE @StrID INT;
	DECLARE @InfostazTecnology VARCHAR(10);
	
	IF (@DeviceTypeID LIKE 'INFSTAZ-IEC') 
		SET @StrID = 1
	ELSE
		SET @StrID = 2;

    IF (@InfoType = 1) -- DeviceTypeID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
			SET @InfostazTecnology = ISNULL((SELECT infostazTecnology FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');
    

            SET @ReturnValue = @DeviceTypeID;

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    SET @ReturnValue = 'AET0000';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'AEL0000';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SYTM100';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SYLB000';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SOTM000';
                END
                ELSE IF (@InfostazTecnology = 'CRT')
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = 'SOCM000';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SOLM000';
                END
            END
			ELSE IF (ISNULL(@VendorData, '') LIKE '%Fida%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'FIT0000';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'FIL0000';
                END
            END
        END
        ELSE
        BEGIN
            SET @ReturnValue = @DeviceTypeID;
        END
    END
    ELSE IF (@InfoType = 2) -- VendorName
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))

            SELECT @ReturnValue = vendors.VendorName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = 'Aesys';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = 'Sysco';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = 'Solari';
            END
			ELSE IF (ISNULL(@VendorData, '') LIKE '%Fida%')
            BEGIN
                SET @ReturnValue = 'Fida';
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = vendors.VendorName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 3) -- VendorID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            
            SELECT @ReturnValue = CONVERT(VARCHAR(10), vendors.VendorID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)            

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = '2';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = '1';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = '3';
            END
			ELSE IF (ISNULL(@VendorData, '') LIKE '%Fida%')
            BEGIN
                SET @ReturnValue = '15';
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = CONVERT(VARCHAR(10), vendors.VendorID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 4) -- WSUrlPattern
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = (SELECT TOP 1 WSUrlPattern from device_type where VendorID = 2 /*Aesys*/ and SystemID = 2 /*Video*/ );
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = (select TOP 1 WSUrlPattern from device_type where VendorID = 1 /*Sysco*/ and SystemID = 2 /*Video*/ );
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = (select TOP 1 WSUrlPattern from device_type where VendorID = 3 /*Solari*/ and SystemID = 2 /*Video*/ );
            END
			ELSE IF (ISNULL(@VendorData, '') LIKE '%Fida%')
            BEGIN
                SET @ReturnValue = (select TOP 1 WSUrlPattern from device_type where VendorID = 15 /*Solari*/ and SystemID = 2 /*Video*/ );
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = device_type.WSUrlPattern
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 5) -- TechnologyID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
			SET @InfostazTecnology = ISNULL((SELECT infostazTecnology FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    SET @ReturnValue = '2';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    SET @ReturnValue = '2';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = '2';
                END
                ELSE IF (@CategoryData IN (/* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15'))
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = '1';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
			ELSE IF (ISNULL(@VendorData, '') LIKE '%Fida%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    SET @ReturnValue = '2';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = CONVERT(VARCHAR(10), device_type.TechnologyID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 6) -- DeviceTypeDescription
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
			SET @InfostazTecnology = ISNULL((SELECT infostazTecnology FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');
            
            SELECT @ReturnValue = device_type.DeviceTypeDescription
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)            

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'AESYS - Teleindicatore TFT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'AESYS - Teleindicatore LED generico';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SYSCO - Teleindicatore TFT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SYSCO - Teleindicatore LED generico';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SOLARI - Teleindicatore TFT generico';
                END
                ELSE IF (@InfostazTecnology = 'CRT')
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = 'SOLARI - Teleindicatore CRT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SOLARI - Teleindicatore LED generico';
                END
            END
			IF (ISNULL(@VendorData, '') LIKE '%Fida%')
            BEGIN
                IF (@InfostazTecnology = 'TFT')
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'Fida - Teleindicatore TFT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'Fida - Teleindicatore LED generico';
                END
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = device_type.DeviceTypeDescription
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 7) -- Decodifica categoria Infostazioni
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
			SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))           
			SET @ReturnValue = ISNULL((SELECT infostazCategoryName FROM infostaz_categories WHERE infostazCategoryId = CONVERT(INT,  
				CASE
				 WHEN ISNUMERIC(@CategoryData) = 1
					THEN @CategoryData
					ELSE NULL
				 END
                       )), 'LED');
        END
    END
    ELSE IF (@InfoType = 8) -- Immagine Periferica
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ%')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = @StrID) AND (DevID = @DevID))

            SET @ReturnValue = @DeviceTypeID;

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZAE' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZSY' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;					
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZSO' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;					
            END
			ELSE IF (ISNULL(@VendorData, '') LIKE '%Fida%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZFI' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;					
            END
        END
        ELSE
        BEGIN
			SELECT @ReturnValue = device_type.ImageName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)   
        END
    END     

	RETURN ISNULL(@ReturnValue, '')
END

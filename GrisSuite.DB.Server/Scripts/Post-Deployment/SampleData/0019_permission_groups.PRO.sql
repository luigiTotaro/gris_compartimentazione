﻿SET IDENTITY_INSERT [dbo].[permission_groups] ON

IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 1)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (1, 'groupAdmin', 'Nome gruppo amministrativo', 1, 'BUILTIN\Administrators', 0, 99, 0)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin', [GroupDescription] = 'Nome gruppo amministrativo', [IsBuiltIn] = 1, [WindowsGroups] = 'BUILTIN\Administrators', [RegCode] = 0, [Level] = 99, [SortOrder] = 0 WHERE [GroupID] = 1


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 2)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (2, 'groupGrisOperators', 'Nome del gruppo operatori Gris', 1, 'MASTER-RETE\STLC1000TelefinAdministrators', 0, 99, 1)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGrisOperators', [GroupDescription] = 'Nome del gruppo operatori Gris', [IsBuiltIn] = 1, [WindowsGroups] = 'MASTER-RETE\STLC1000TelefinAdministrators', [RegCode] = 0, [Level] = 99, [SortOrder] = 1 WHERE [GroupID] = 2


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 3)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (3, 'groupConfigValidation', 'Nome gruppo validazione configurazione STLC1000', 1, 'MASTER-RETE\STLC1000TelefinAdministrators', 0, 0, 2)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupConfigValidation', [GroupDescription] = 'Nome gruppo validazione configurazione STLC1000', [IsBuiltIn] = 1, [WindowsGroups] = 'MASTER-RETE\STLC1000TelefinAdministrators', [RegCode] = 0, [Level] = 0, [SortOrder] = 2 WHERE [GroupID] = 3


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 4)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (4, 'groupAlertOperators', 'Nome del gruppo operatori degli allarmi', 1, 'RFISERVIZI\GrisAlertOperators', 0, 0, 3)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAlertOperators', [GroupDescription] = 'Nome del gruppo operatori degli allarmi', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisAlertOperators', [RegCode] = 0, [Level] = 0, [SortOrder] = 3 WHERE [GroupID] = 4


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 5)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (5, 'groupAlertUsers', 'Nome del gruppo visualizzatori degli allarmi', 1, 'RFISERVIZI\GrisAlertUsers', 0, 0, 4)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAlertUsers', [GroupDescription] = 'Nome del gruppo visualizzatori degli allarmi', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisAlertUsers', [RegCode] = 0, [Level] = 0, [SortOrder] = 4 WHERE [GroupID] = 5


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 6)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (6, 'groupAll_1', 'Tutta Italia Livello 1', 1, 'RFISERVIZI\GrisItalia-L1', 0, 1, 5)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAll_1', [GroupDescription] = 'Tutta Italia Livello 1', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisItalia-L1', [RegCode] = 0, [Level] = 1, [SortOrder] = 5 WHERE [GroupID] = 6


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 7)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (7, 'groupAll_2', 'Tutta Italia Livello 2', 1, 'RFISERVIZI\GrisItalia-L2', 0, 2, 6)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAll_2', [GroupDescription] = 'Tutta Italia Livello 2', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisItalia-L2', [RegCode] = 0, [Level] = 2, [SortOrder] = 6 WHERE [GroupID] = 7


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 8)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (8, 'groupAll_3', 'Tutta Italia Livello 3', 1, 'RFISERVIZI\GrisItalia-L3', 0, 3, 7)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAll_3', [GroupDescription] = 'Tutta Italia Livello 3', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisItalia-L3', [RegCode] = 0, [Level] = 3, [SortOrder] = 7 WHERE [GroupID] = 8


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 9)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (9, 'groupComp1', '', 1, 'RFISERVIZI\GrisComp01-L1', 1, 1, 8)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp1', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp01-L1', [RegCode] = 1, [Level] = 1, [SortOrder] = 8 WHERE [GroupID] = 9


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 10)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (10, 'groupComp2', '', 1, 'RFISERVIZI\GrisComp02-L1', 2, 1, 9)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp02-L1', [RegCode] = 2, [Level] = 1, [SortOrder] = 9 WHERE [GroupID] = 10


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 11)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (11, 'groupComp3', '', 1, 'RFISERVIZI\GrisComp03-L1', 3, 1, 10)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp03-L1', [RegCode] = 3, [Level] = 1, [SortOrder] = 10 WHERE [GroupID] = 11


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 12)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (12, 'groupComp4', '', 1, 'RFISERVIZI\GrisComp04-L1', 4, 1, 11)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp4', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp04-L1', [RegCode] = 4, [Level] = 1, [SortOrder] = 11 WHERE [GroupID] = 12


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 13)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (13, 'groupComp5', '', 1, 'RFISERVIZI\GrisComp05-L1', 5, 1, 12)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp5', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp05-L1', [RegCode] = 5, [Level] = 1, [SortOrder] = 12 WHERE [GroupID] = 13


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 14)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (14, 'groupComp6', '', 1, 'RFISERVIZI\GrisComp06-L1', 6, 1, 13)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp6', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp06-L1', [RegCode] = 6, [Level] = 1, [SortOrder] = 13 WHERE [GroupID] = 14


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 15)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (15, 'groupComp7', '', 1, 'RFISERVIZI\GrisComp07-L1', 7, 1, 14)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp7', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp07-L1', [RegCode] = 7, [Level] = 1, [SortOrder] = 14 WHERE [GroupID] = 15


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 16)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (16, 'groupComp8', '', 1, 'RFISERVIZI\GrisComp08-L1', 8, 1, 15)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp8', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp08-L1', [RegCode] = 8, [Level] = 1, [SortOrder] = 15 WHERE [GroupID] = 16


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 17)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (17, 'groupComp9', '', 1, 'RFISERVIZI\GrisComp09-L1', 9, 1, 16)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp9', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp09-L1', [RegCode] = 9, [Level] = 1, [SortOrder] = 16 WHERE [GroupID] = 17


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 18)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (18, 'groupComp10', '', 1, 'RFISERVIZI\GrisComp10-L1', 10, 1, 17)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp10', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp10-L1', [RegCode] = 10, [Level] = 1, [SortOrder] = 17 WHERE [GroupID] = 18


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 19)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (19, 'groupComp11', '', 1, 'RFISERVIZI\GrisComp11-L1', 11, 1, 18)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp11', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp11-L1', [RegCode] = 11, [Level] = 1, [SortOrder] = 18 WHERE [GroupID] = 19


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 20)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (20, 'groupComp12', '', 1, 'RFISERVIZI\GrisComp12-L1', 12, 1, 19)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp12', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp12-L1', [RegCode] = 12, [Level] = 1, [SortOrder] = 19 WHERE [GroupID] = 20


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 21)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (21, 'groupComp13', '', 1, 'RFISERVIZI\GrisComp13-L1', 13, 1, 20)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp13', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp13-L1', [RegCode] = 13, [Level] = 1, [SortOrder] = 20 WHERE [GroupID] = 21


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 22)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (22, 'groupComp14', '', 1, 'RFISERVIZI\GrisComp14-L1', 14, 1, 21)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp14', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp14-L1', [RegCode] = 14, [Level] = 1, [SortOrder] = 21 WHERE [GroupID] = 22


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 23)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (23, 'groupComp15', '', 1, 'RFISERVIZI\GrisComp15-L1', 15, 1, 22)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp15', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp15-L1', [RegCode] = 15, [Level] = 1, [SortOrder] = 22 WHERE [GroupID] = 23


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 24)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (24, 'groupComp1_2', '', 1, 'RFISERVIZI\GrisComp01-L2', 1, 2, 23)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp1_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp01-L2', [RegCode] = 1, [Level] = 2, [SortOrder] = 23 WHERE [GroupID] = 24


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 25)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (25, 'groupComp2_2', '', 1, 'RFISERVIZI\GrisComp02-L2', 2, 2, 24)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp2_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp02-L2', [RegCode] = 2, [Level] = 2, [SortOrder] = 24 WHERE [GroupID] = 25


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 26)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (26, 'groupComp3_2', '', 1, 'RFISERVIZI\GrisComp03-L2', 3, 2, 25)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp3_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp03-L2', [RegCode] = 3, [Level] = 2, [SortOrder] = 25 WHERE [GroupID] = 26


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 27)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (27, 'groupComp4_2', '', 1, 'RFISERVIZI\GrisComp04-L2', 4, 2, 26)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp4_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp04-L2', [RegCode] = 4, [Level] = 2, [SortOrder] = 26 WHERE [GroupID] = 27


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 28)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (28, 'groupComp5_2', '', 1, 'RFISERVIZI\GrisComp05-L2', 5, 2, 27)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp5_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp05-L2', [RegCode] = 5, [Level] = 2, [SortOrder] = 27 WHERE [GroupID] = 28


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 29)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (29, 'groupComp6_2', '', 1, 'RFISERVIZI\GrisComp06-L2', 6, 2, 28)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp6_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp06-L2', [RegCode] = 6, [Level] = 2, [SortOrder] = 28 WHERE [GroupID] = 29


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 30)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (30, 'groupComp7_2', '', 1, 'RFISERVIZI\GrisComp07-L2', 7, 2, 29)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp7_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp07-L2', [RegCode] = 7, [Level] = 2, [SortOrder] = 29 WHERE [GroupID] = 30


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 31)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (31, 'groupComp8_2', '', 1, 'RFISERVIZI\GrisComp08-L2', 8, 2, 30)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp8_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp08-L2', [RegCode] = 8, [Level] = 2, [SortOrder] = 30 WHERE [GroupID] = 31


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 32)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (32, 'groupComp9_2', '', 1, 'RFISERVIZI\GrisComp09-L2', 9, 2, 31)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp9_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp09-L2', [RegCode] = 9, [Level] = 2, [SortOrder] = 31 WHERE [GroupID] = 32


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 33)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (33, 'groupComp10_2', '', 1, 'RFISERVIZI\GrisComp10-L2', 10, 2, 32)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp10_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp10-L2', [RegCode] = 10, [Level] = 2, [SortOrder] = 32 WHERE [GroupID] = 33


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 34)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (34, 'groupComp11_2', '', 1, 'RFISERVIZI\GrisComp11-L2', 11, 2, 33)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp11_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp11-L2', [RegCode] = 11, [Level] = 2, [SortOrder] = 33 WHERE [GroupID] = 34


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 35)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (35, 'groupComp12_2', '', 1, 'RFISERVIZI\GrisComp12-L2', 12, 2, 34)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp12_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp12-L2', [RegCode] = 12, [Level] = 2, [SortOrder] = 34 WHERE [GroupID] = 35


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 36)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (36, 'groupComp13_2', '', 1, 'RFISERVIZI\GrisComp13-L2', 13, 2, 35)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp13_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp13-L2', [RegCode] = 13, [Level] = 2, [SortOrder] = 35 WHERE [GroupID] = 36


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 37)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (37, 'groupComp14_2', '', 1, 'RFISERVIZI\GrisComp14-L2', 14, 2, 36)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp14_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp14-L2', [RegCode] = 14, [Level] = 2, [SortOrder] = 36 WHERE [GroupID] = 37


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 38)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (38, 'groupComp15_2', '', 1, 'RFISERVIZI\GrisComp15-L2', 15, 2, 37)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp15_2', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp15-L2', [RegCode] = 15, [Level] = 2, [SortOrder] = 37 WHERE [GroupID] = 38


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 39)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (39, 'groupComp1_3', '', 1, 'RFISERVIZI\GrisComp01-L3', 1, 3, 38)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp1_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp01-L3', [RegCode] = 1, [Level] = 3, [SortOrder] = 38 WHERE [GroupID] = 39


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 40)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (40, 'groupComp2_3', '', 1, 'RFISERVIZI\GrisComp02-L3', 2, 3, 39)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp2_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp02-L3', [RegCode] = 2, [Level] = 3, [SortOrder] = 39 WHERE [GroupID] = 40


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 41)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (41, 'groupComp3_3', '', 1, 'RFISERVIZI\GrisComp03-L3', 3, 3, 40)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp3_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp03-L3', [RegCode] = 3, [Level] = 3, [SortOrder] = 40 WHERE [GroupID] = 41


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 42)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (42, 'groupComp4_3', '', 1, 'RFISERVIZI\GrisComp04-L3', 4, 3, 41)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp4_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp04-L3', [RegCode] = 4, [Level] = 3, [SortOrder] = 41 WHERE [GroupID] = 42


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 43)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (43, 'groupComp5_3', '', 1, 'RFISERVIZI\GrisComp05-L3', 5, 3, 42)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp5_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp05-L3', [RegCode] = 5, [Level] = 3, [SortOrder] = 42 WHERE [GroupID] = 43


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 44)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (44, 'groupComp6_3', '', 1, 'RFISERVIZI\GrisComp06-L3', 6, 3, 43)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp6_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp06-L3', [RegCode] = 6, [Level] = 3, [SortOrder] = 43 WHERE [GroupID] = 44


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 45)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (45, 'groupComp7_3', '', 1, 'RFISERVIZI\GrisComp07-L3', 7, 3, 44)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp7_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp07-L3', [RegCode] = 7, [Level] = 3, [SortOrder] = 44 WHERE [GroupID] = 45


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 46)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (46, 'groupComp8_3', '', 1, 'RFISERVIZI\GrisComp08-L3', 8, 3, 45)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp8_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp08-L3', [RegCode] = 8, [Level] = 3, [SortOrder] = 45 WHERE [GroupID] = 46


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 47)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (47, 'groupComp9_3', '', 1, 'RFISERVIZI\GrisComp09-L3', 9, 3, 46)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp9_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp09-L3', [RegCode] = 9, [Level] = 3, [SortOrder] = 46 WHERE [GroupID] = 47


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 48)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (48, 'groupComp10_3', '', 1, 'RFISERVIZI\GrisComp10-L3', 10, 3, 47)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp10_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp10-L3', [RegCode] = 10, [Level] = 3, [SortOrder] = 47 WHERE [GroupID] = 48


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 49)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (49, 'groupComp11_3', '', 1, 'RFISERVIZI\GrisComp11-L3', 11, 3, 48)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp11_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp11-L3', [RegCode] = 11, [Level] = 3, [SortOrder] = 48 WHERE [GroupID] = 49


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 50)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (50, 'groupComp12_3', '', 1, 'RFISERVIZI\GrisComp12-L3', 12, 3, 49)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp12_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp12-L3', [RegCode] = 12, [Level] = 3, [SortOrder] = 49 WHERE [GroupID] = 50


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 51)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (51, 'groupComp13_3', '', 1, 'RFISERVIZI\GrisComp13-L3', 13, 3, 50)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp13_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp13-L3', [RegCode] = 13, [Level] = 3, [SortOrder] = 50 WHERE [GroupID] = 51


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 52)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (52, 'groupComp14_3', '', 1, 'RFISERVIZI\GrisComp14-L3', 14, 3, 51)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp14_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp14-L3', [RegCode] = 14, [Level] = 3, [SortOrder] = 51 WHERE [GroupID] = 52


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 53)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (53, 'groupComp15_3', '', 1, 'RFISERVIZI\GrisComp15-L3', 15, 3, 52)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupComp15_3', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisComp15-L3', [RegCode] = 15, [Level] = 3, [SortOrder] = 52 WHERE [GroupID] = 53


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 54)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (54, 'groupSTLCOperators', '', 1, 'MASTER-RETE\STLC1000TelefinAdministrators', 0, 0, 53)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupSTLCOperators', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'MASTER-RETE\STLC1000TelefinAdministrators', [RegCode] = 0, [Level] = 0, [SortOrder] = 53 WHERE [GroupID] = 54


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 55)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (55, 'groupRulesOperators', '', 1, 'MASTER-RETE\STLC1000TelefinAdministrators', 0, 0, 54)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupRulesOperators', [GroupDescription] = '', [IsBuiltIn] = 1, [WindowsGroups] = 'MASTER-RETE\STLC1000TelefinAdministrators', [RegCode] = 0, [Level] = 0, [SortOrder] = 54 WHERE [GroupID] = 55


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 56)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (56, 'groupGroupingOperators1', 'Gestore Raggruppamenti di compartimento Torino', 1, 'groupGroupingOperators1', 1, 99, 55)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators1', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Torino', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators1', [RegCode] = 1, [Level] = 99, [SortOrder] = 55 WHERE [GroupID] = 56


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 57)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (57, 'groupGroupingOperators2', 'Gestori Raggruppamenti di compartimento Milano', 1, 'groupGroupingOperators2', 2, 99, 56)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators2', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Milano', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators2', [RegCode] = 2, [Level] = 99, [SortOrder] = 56 WHERE [GroupID] = 57


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 58)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (58, 'groupGroupingOperators3', 'Gestori Raggruppamenti di compartimento Verona', 1, 'groupGroupingOperators3', 3, 99, 57)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators3', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Verona', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators3', [RegCode] = 3, [Level] = 99, [SortOrder] = 57 WHERE [GroupID] = 58


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 59)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (59, 'groupGroupingOperators4', 'Gestori Raggruppamenti di compartimento Venezia', 1, 'groupGroupingOperators4', 4, 99, 58)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators4', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Venezia', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators4', [RegCode] = 4, [Level] = 99, [SortOrder] = 58 WHERE [GroupID] = 59


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 60)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (60, 'groupGroupingOperators5', 'Gestori Raggruppamenti di compartimento Trieste', 1, 'groupGroupingOperators5', 5, 99, 59)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators5', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Trieste', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators5', [RegCode] = 5, [Level] = 99, [SortOrder] = 59 WHERE [GroupID] = 60


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 61)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (61, 'groupGroupingOperators6', 'Gestori Raggruppamenti di compartimento Genova', 1, 'groupGroupingOperators6', 6, 99, 60)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators6', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Genova', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators6', [RegCode] = 6, [Level] = 99, [SortOrder] = 60 WHERE [GroupID] = 61


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 62)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (62, 'groupGroupingOperators7', 'Gestori Raggruppamenti di compartimento Bologna', 1, 'groupGroupingOperators7', 7, 99, 61)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators7', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Bologna', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators7', [RegCode] = 7, [Level] = 99, [SortOrder] = 61 WHERE [GroupID] = 62


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 63)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (63, 'groupGroupingOperators8', 'Gestori Raggruppamenti di compartimento Firenze', 1, 'groupGroupingOperators8', 8, 99, 62)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators8', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Firenze', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators8', [RegCode] = 8, [Level] = 99, [SortOrder] = 62 WHERE [GroupID] = 63


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 64)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (64, 'groupGroupingOperators9', 'Gestori Raggruppamenti di compartimento Ancona', 1, 'groupGroupingOperators9', 9, 99, 63)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators9', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Ancona', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators9', [RegCode] = 9, [Level] = 99, [SortOrder] = 63 WHERE [GroupID] = 64


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 65)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (65, 'groupGroupingOperators10', 'Gestori Raggruppamenti di compartimento Roma', 1, 'groupGroupingOperators10', 10, 99, 64)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators10', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Roma', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators10', [RegCode] = 10, [Level] = 99, [SortOrder] = 64 WHERE [GroupID] = 65


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 66)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (66, 'groupGroupingOperators11', 'Gestori Raggruppamenti di compartimento Napoli', 1, 'groupGroupingOperators11', 11, 99, 65)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators11', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Napoli', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators11', [RegCode] = 11, [Level] = 99, [SortOrder] = 65 WHERE [GroupID] = 66


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 67)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (67, 'groupGroupingOperators12', 'Gestori Raggruppamenti di compartimento Bari', 1, 'groupGroupingOperators12', 12, 99, 66)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators12', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Bari', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators12', [RegCode] = 12, [Level] = 99, [SortOrder] = 66 WHERE [GroupID] = 67


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 68)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (68, 'groupGroupingOperators13', 'Gestori Raggruppamenti di compartimento Reggio Calabria', 1, 'groupGroupingOperators13', 13, 99, 67)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators13', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Reggio Calabria', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators13', [RegCode] = 13, [Level] = 99, [SortOrder] = 67 WHERE [GroupID] = 68


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 69)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (69, 'groupGroupingOperators14', 'Gestori Raggruppamenti di compartimento Palermo', 1, 'groupGroupingOperators14', 14, 99, 68)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators14', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Palermo', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators14', [RegCode] = 14, [Level] = 99, [SortOrder] = 68 WHERE [GroupID] = 69


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 70)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (70, 'groupGroupingOperators15', 'Gestori Raggruppamenti di compartimento Cagliari', 1, 'groupGroupingOperators15', 15, 99, 69)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGroupingOperators15', [GroupDescription] = 'Gestori Raggruppamenti di compartimento Cagliari', [IsBuiltIn] = 1, [WindowsGroups] = 'groupGroupingOperators15', [RegCode] = 15, [Level] = 99, [SortOrder] = 69 WHERE [GroupID] = 70


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 71)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (71, 'groupAdmin1', 'Amministratori di compartimento Torino', 1, 'groupAdmin1', 1, 99, 70)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin1', [GroupDescription] = 'Amministratori di compartimento Torino', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin1', [RegCode] = 1, [Level] = 99, [SortOrder] = 70 WHERE [GroupID] = 71


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 72)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (72, 'groupAdmin2', 'Amministratori di compartimento Milano', 1, 'groupAdmin2', 2, 99, 71)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin2', [GroupDescription] = 'Amministratori di compartimento Milano', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin2', [RegCode] = 2, [Level] = 99, [SortOrder] = 71 WHERE [GroupID] = 72


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 73)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (73, 'groupAdmin3', 'Amministratori di compartimento Verona', 1, 'groupAdmin3', 3, 99, 72)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin3', [GroupDescription] = 'Amministratori di compartimento Verona', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin3', [RegCode] = 3, [Level] = 99, [SortOrder] = 72 WHERE [GroupID] = 73


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 74)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (74, 'groupAdmin4', 'Amministratori di compartimento Venezia', 1, 'groupAdmin4', 4, 99, 73)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin4', [GroupDescription] = 'Amministratori di compartimento Venezia', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin4', [RegCode] = 4, [Level] = 99, [SortOrder] = 73 WHERE [GroupID] = 74


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 75)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (75, 'groupAdmin5', 'Amministratori di compartimento Trieste', 1, 'groupAdmin5', 5, 99, 74)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin5', [GroupDescription] = 'Amministratori di compartimento Trieste', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin5', [RegCode] = 5, [Level] = 99, [SortOrder] = 74 WHERE [GroupID] = 75


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 76)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (76, 'groupAdmin6', 'Amministratori di compartimento Genova', 1, 'groupAdmin6', 6, 99, 75)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin6', [GroupDescription] = 'Amministratori di compartimento Genova', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin6', [RegCode] = 6, [Level] = 99, [SortOrder] = 75 WHERE [GroupID] = 76


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 77)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (77, 'groupAdmin7', 'Amministratori di compartimento Bologna', 1, 'groupAdmin7', 7, 99, 76)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin7', [GroupDescription] = 'Amministratori di compartimento Bologna', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin7', [RegCode] = 7, [Level] = 99, [SortOrder] = 76 WHERE [GroupID] = 77


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 78)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (78, 'groupAdmin8', 'Amministratori di compartimento Firenze', 1, 'groupAdmin8', 8, 99, 77)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin8', [GroupDescription] = 'Amministratori di compartimento Firenze', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin8', [RegCode] = 8, [Level] = 99, [SortOrder] = 77 WHERE [GroupID] = 78


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 79)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (79, 'groupAdmin9', 'Amministratori di compartimento Ancona', 1, 'groupAdmin9', 9, 99, 78)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin9', [GroupDescription] = 'Amministratori di compartimento Ancona', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin9', [RegCode] = 9, [Level] = 99, [SortOrder] = 78 WHERE [GroupID] = 79


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 80)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (80, 'groupAdmin10', 'Amministratori di compartimento Roma', 1, 'groupAdmin10', 10, 99, 79)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin10', [GroupDescription] = 'Amministratori di compartimento Roma', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin10', [RegCode] = 10, [Level] = 99, [SortOrder] = 79 WHERE [GroupID] = 80


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 81)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (81, 'groupAdmin11', 'Amministratori di compartimento Napoli', 1, 'groupAdmin11', 11, 99, 80)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin11', [GroupDescription] = 'Amministratori di compartimento Napoli', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin11', [RegCode] = 11, [Level] = 99, [SortOrder] = 80 WHERE [GroupID] = 81


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 82)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (82, 'groupAdmin12', 'Amministratori di compartimento Bari', 1, 'groupAdmin12', 12, 99, 81)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin12', [GroupDescription] = 'Amministratori di compartimento Bari', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin12', [RegCode] = 12, [Level] = 99, [SortOrder] = 81 WHERE [GroupID] = 82


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 83)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (83, 'groupAdmin13', 'Amministratori di compartimento Reggio Calabria', 1, 'groupAdmin13', 13, 99, 82)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin13', [GroupDescription] = 'Amministratori di compartimento Reggio Calabria', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin13', [RegCode] = 13, [Level] = 99, [SortOrder] = 82 WHERE [GroupID] = 83


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 84)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (84, 'groupAdmin14', 'Amministratori di compartimento Palermo', 1, 'groupAdmin14', 14, 99, 83)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin14', [GroupDescription] = 'Amministratori di compartimento Palermo', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin14', [RegCode] = 14, [Level] = 99, [SortOrder] = 83 WHERE [GroupID] = 84


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 85)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (85, 'groupAdmin15', 'Amministratori di compartimento Cagliari', 1, 'groupAdmin15', 15, 99, 84)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupAdmin15', [GroupDescription] = 'Amministratori di compartimento Cagliari', [IsBuiltIn] = 1, [WindowsGroups] = 'groupAdmin15', [RegCode] = 15, [Level] = 99, [SortOrder] = 84 WHERE [GroupID] = 85


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 86)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (86, 'groupVPNVerdeItalia', 'Monitoraggio VPN Verde a livello Italia', 1, 'GroupVPNVerdeItalia', 0, 0, 86)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeItalia', [GroupDescription] = 'Monitoraggio VPN Verde a livello Italia', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeItalia', [RegCode] = 0, [Level] = 0, [SortOrder] = 86 WHERE [GroupID] = 86


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 87)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (87, 'groupVPNVerdeComp1', 'Monitoraggio VPN Verde di compartimento Torino', 1, 'GroupVPNVerdeComp01', 1, 99, 87)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp1', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Torino', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp01', [RegCode] = 1, [Level] = 99, [SortOrder] = 87 WHERE [GroupID] = 87


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 88)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (88, 'groupVPNVerdeComp2', 'Monitoraggio VPN Verde di compartimento Milano', 1, 'GroupVPNVerdeComp02', 2, 99, 88)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp2', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Milano', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp02', [RegCode] = 2, [Level] = 99, [SortOrder] = 88 WHERE [GroupID] = 88


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 89)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (89, 'groupVPNVerdeComp3', 'Monitoraggio VPN Verde di compartimento Verona', 1, 'GroupVPNVerdeComp03', 3, 99, 89)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp3', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Verona', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp03', [RegCode] = 3, [Level] = 99, [SortOrder] = 89 WHERE [GroupID] = 89


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 90)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (90, 'groupVPNVerdeComp4', 'Monitoraggio VPN Verde di compartimento Venezia', 1, 'GroupVPNVerdeComp04', 4, 99, 90)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp4', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Venezia', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp04', [RegCode] = 4, [Level] = 99, [SortOrder] = 90 WHERE [GroupID] = 90


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 91)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (91, 'groupVPNVerdeComp5', 'Monitoraggio VPN Verde di compartimento Trieste', 1, 'GroupVPNVerdeComp05', 5, 99, 91)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp5', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Trieste', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp05', [RegCode] = 5, [Level] = 99, [SortOrder] = 91 WHERE [GroupID] = 91


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 92)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (92, 'groupVPNVerdeComp6', 'Monitoraggio VPN Verde di compartimento Genova', 1, 'GroupVPNVerdeComp06', 6, 99, 92)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp6', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Genova', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp06', [RegCode] = 6, [Level] = 99, [SortOrder] = 92 WHERE [GroupID] = 92


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 93)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (93, 'groupVPNVerdeComp7', 'Monitoraggio VPN Verde di compartimento Bologna', 1, 'GroupVPNVerdeComp07', 7, 99, 93)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp7', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Bologna', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp07', [RegCode] = 7, [Level] = 99, [SortOrder] = 93 WHERE [GroupID] = 93


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 94)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (94, 'groupVPNVerdeComp8', 'Monitoraggio VPN Verde di compartimento Firenze', 1, 'GroupVPNVerdeComp08', 8, 99, 94)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp8', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Firenze', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp08', [RegCode] = 8, [Level] = 99, [SortOrder] = 94 WHERE [GroupID] = 94


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 95)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (95, 'groupVPNVerdeComp9', 'Monitoraggio VPN Verde di compartimento Ancona', 1, 'GroupVPNVerdeComp09', 9, 99, 95)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp9', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Ancona', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp09', [RegCode] = 9, [Level] = 99, [SortOrder] = 95 WHERE [GroupID] = 95


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 96)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (96, 'groupVPNVerdeComp10', 'Monitoraggio VPN Verde di compartimento Roma', 1, 'GroupVPNVerdeComp10', 10, 99, 96)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp10', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Roma', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp10', [RegCode] = 10, [Level] = 99, [SortOrder] = 96 WHERE [GroupID] = 96


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 97)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (97, 'groupVPNVerdeComp11', 'Monitoraggio VPN Verde di compartimento Napoli', 1, 'GroupVPNVerdeComp11', 11, 99, 97)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp11', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Napoli', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp11', [RegCode] = 11, [Level] = 99, [SortOrder] = 97 WHERE [GroupID] = 97


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 98)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (98, 'groupVPNVerdeComp12', 'Monitoraggio VPN Verde di compartimento Bari', 1, 'GroupVPNVerdeComp12', 12, 99, 98)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp12', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Bari', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp12', [RegCode] = 12, [Level] = 99, [SortOrder] = 98 WHERE [GroupID] = 98


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 99)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (99, 'groupVPNVerdeComp13', 'Monitoraggio VPN Verde di compartimento Reggio Calabria', 1, 'GroupVPNVerdeComp13', 13, 99, 99)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp13', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Reggio Calabria', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp13', [RegCode] = 13, [Level] = 99, [SortOrder] = 99 WHERE [GroupID] = 99


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 100)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (100, 'groupVPNVerdeComp14', 'Monitoraggio VPN Verde di compartimento Palermo', 1, 'GroupVPNVerdeComp14', 14, 99, 100)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp14', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Palermo', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp14', [RegCode] = 14, [Level] = 99, [SortOrder] = 100 WHERE [GroupID] = 100


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 101)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (101, 'groupVPNVerdeComp15', 'Monitoraggio VPN Verde di compartimento Cagliari', 1, 'GroupVPNVerdeComp15', 15, 99, 101)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupVPNVerdeComp15', [GroupDescription] = 'Monitoraggio VPN Verde di compartimento Cagliari', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupVPNVerdeComp15', [RegCode] = 15, [Level] = 99, [SortOrder] = 101 WHERE [GroupID] = 101


SET IDENTITY_INSERT [dbo].[permission_groups] ON


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 102)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (102, 'groupWebRadioItalia', 'Monitoraggio Web Radio a livello Italia', 1, 'GroupWebRadioItalia', 0, 0, 102)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioItalia', [GroupDescription] = 'Monitoraggio Web Radio a livello Italia', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioItalia', [RegCode] = 0, [Level] = 0, [SortOrder] = 102 WHERE [GroupID] = 102


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 103)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (103, 'groupWebRadioComp1', 'Monitoraggio Web Radio di compartimento Torino', 1, 'GroupWebRadioComp01', 1, 99, 103)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp1', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Torino', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp01', [RegCode] = 1, [Level] = 99, [SortOrder] = 103 WHERE [GroupID] = 103


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 104)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (104, 'groupWebRadioComp2', 'Monitoraggio Web Radio di compartimento Milano', 1, 'GroupWebRadioComp02', 2, 99, 104)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp2', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Milano', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp02', [RegCode] = 2, [Level] = 99, [SortOrder] = 104 WHERE [GroupID] = 104


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 105)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (105, 'groupWebRadioComp3', 'Monitoraggio Web Radio di compartimento Verona', 1, 'GroupWebRadioComp03', 3, 99, 105)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp3', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Verona', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp03', [RegCode] = 3, [Level] = 99, [SortOrder] = 105 WHERE [GroupID] = 105


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 106)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (106, 'groupWebRadioComp4', 'Monitoraggio Web Radio di compartimento Venezia', 1, 'GroupWebRadioComp04', 4, 99, 106)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp4', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Venezia', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp04', [RegCode] = 4, [Level] = 99, [SortOrder] = 106 WHERE [GroupID] = 106


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 107)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (107, 'groupWebRadioComp5', 'Monitoraggio Web Radio di compartimento Trieste', 1, 'GroupWebRadioComp05', 5, 99, 107)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp5', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Trieste', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp05', [RegCode] = 5, [Level] = 99, [SortOrder] = 107 WHERE [GroupID] = 107


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 108)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (108, 'groupWebRadioComp6', 'Monitoraggio Web Radio di compartimento Genova', 1, 'GroupWebRadioComp06', 6, 99, 108)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp6', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Genova', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp06', [RegCode] = 6, [Level] = 99, [SortOrder] = 108 WHERE [GroupID] = 108


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 109)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (109, 'groupWebRadioComp7', 'Monitoraggio Web Radio di compartimento Bologna', 1, 'GroupWebRadioComp07', 7, 99, 109)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp7', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Bologna', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp07', [RegCode] = 7, [Level] = 99, [SortOrder] = 109 WHERE [GroupID] = 109


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 110)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (110, 'groupWebRadioComp8', 'Monitoraggio Web Radio di compartimento Firenze', 1, 'GroupWebRadioComp08', 8, 99, 110)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp8', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Firenze', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp08', [RegCode] = 8, [Level] = 99, [SortOrder] = 110 WHERE [GroupID] = 110


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 111)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (111, 'groupWebRadioComp9', 'Monitoraggio Web Radio di compartimento Ancona', 1, 'GroupWebRadioComp09', 9, 99, 111)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp9', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Ancona', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp09', [RegCode] = 9, [Level] = 99, [SortOrder] = 111 WHERE [GroupID] = 111


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 112)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (112, 'groupWebRadioComp10', 'Monitoraggio Web Radio di compartimento Roma', 1, 'GroupWebRadioComp10', 10, 99, 112)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp10', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Roma', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp10', [RegCode] = 10, [Level] = 99, [SortOrder] = 112 WHERE [GroupID] = 112


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 113)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (113, 'groupWebRadioComp11', 'Monitoraggio Web Radio di compartimento Napoli', 1, 'GroupWebRadioComp11', 11, 99, 113)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp11', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Napoli', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp11', [RegCode] = 11, [Level] = 99, [SortOrder] = 113 WHERE [GroupID] = 113


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 114)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (114, 'groupWebRadioComp12', 'Monitoraggio Web Radio di compartimento Bari', 1, 'GroupWebRadioComp12', 12, 99, 114)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp12', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Bari', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp12', [RegCode] = 12, [Level] = 99, [SortOrder] = 114 WHERE [GroupID] = 114


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 115)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (115, 'groupWebRadioComp13', 'Monitoraggio Web Radio di compartimento Reggio Calabria', 1, 'GroupWebRadioComp13', 13, 99, 115)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp13', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Reggio Calabria', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp13', [RegCode] = 13, [Level] = 99, [SortOrder] = 115 WHERE [GroupID] = 115


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 116)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (116, 'groupWebRadioComp14', 'Monitoraggio Web Radio di compartimento Palermo', 1, 'GroupWebRadioComp14', 14, 99, 116)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp14', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Palermo', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp14', [RegCode] = 14, [Level] = 99, [SortOrder] = 116 WHERE [GroupID] = 116


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 117)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (117, 'groupWebRadioComp15', 'Monitoraggio Web Radio di compartimento Cagliari', 1, 'GroupWebRadioComp15', 15, 99, 117)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupWebRadioComp15', [GroupDescription] = 'Monitoraggio Web Radio di compartimento Cagliari', [IsBuiltIn] = 1, [WindowsGroups] = 'GroupWebRadioComp15', [RegCode] = 15, [Level] = 99, [SortOrder] = 117 WHERE [GroupID] = 117


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 118)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (118, 'groupGrisClientLicenseAdmin', 'Gestione licenze server', 1, 'RFISERVIZI\GrisAdmin', 0, 99, 118)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGrisClientLicenseAdmin', [GroupDescription] = 'Gestione licenze server', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GrisAdmin', [RegCode] = 0, [Level] = 99, [SortOrder] = 118 WHERE [GroupID] = 118


IF NOT EXISTS (SELECT [GroupID] FROM [dbo].[permission_groups] WHERE [GroupID] = 119)
     INSERT INTO [dbo].[permission_groups] ([GroupID], [GroupName], [GroupDescription], [IsBuiltIn], [WindowsGroups], [RegCode], [Level], [SortOrder])
     VALUES (119, 'groupGrisClientLicenseUsers', 'Visualizzazione licenze server', 1, 'RFISERVIZI\GroupGrisClientLicenseUsers', 0, 99, 119)
ELSE
     UPDATE [dbo].[permission_groups] SET [GroupName] = 'groupGrisClientLicenseUsers', [GroupDescription] = 'Visualizzazione licenze server', [IsBuiltIn] = 1, [WindowsGroups] = 'RFISERVIZI\GroupGrisClientLicenseUsers', [RegCode] = 0, [Level] = 99, [SortOrder] = 119 WHERE [GroupID] = 119

SET IDENTITY_INSERT [dbo].[permission_groups] OFF

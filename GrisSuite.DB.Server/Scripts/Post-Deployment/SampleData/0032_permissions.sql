SET IDENTITY_INSERT [dbo].[permissions] ON

IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 61)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (61, 1, 1)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 1 WHERE [PermissionID] = 61


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 64)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (64, 1, 2)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 2 WHERE [PermissionID] = 64


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 56)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (56, 1, 3)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 3 WHERE [PermissionID] = 56


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 34)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (34, 1, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 4 WHERE [PermissionID] = 34


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 70)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (70, 1, 5)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 5 WHERE [PermissionID] = 70


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 73)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (73, 1, 6)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 6 WHERE [PermissionID] = 73


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 78)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (78, 1, 7)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 7 WHERE [PermissionID] = 78


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 81)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (81, 1, 8)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 8 WHERE [PermissionID] = 81


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 84)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (84, 1, 9)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 1, [ResourceID] = 9 WHERE [PermissionID] = 84


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 63)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (63, 2, 1)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 2, [ResourceID] = 1 WHERE [PermissionID] = 63


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 72)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (72, 2, 5)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 2, [ResourceID] = 5 WHERE [PermissionID] = 72


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 77)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (77, 2, 6)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 2, [ResourceID] = 6 WHERE [PermissionID] = 77


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 79)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (79, 2, 7)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 2, [ResourceID] = 7 WHERE [PermissionID] = 79


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 82)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (82, 2, 8)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 2, [ResourceID] = 8 WHERE [PermissionID] = 82


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 85)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (85, 2, 9)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 2, [ResourceID] = 9 WHERE [PermissionID] = 85


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 69)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (69, 3, 2)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 3, [ResourceID] = 2 WHERE [PermissionID] = 69


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 74)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (74, 6, 6)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 6, [ResourceID] = 6 WHERE [PermissionID] = 74


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 75)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (75, 7, 6)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 7, [ResourceID] = 6 WHERE [PermissionID] = 75


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 62)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (62, 8, 1)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 1 WHERE [PermissionID] = 62


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 65)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (65, 8, 2)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 2 WHERE [PermissionID] = 65


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 57)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (57, 8, 3)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 3 WHERE [PermissionID] = 57


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 71)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (71, 8, 5)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 5 WHERE [PermissionID] = 71


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 76)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (76, 8, 6)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 6 WHERE [PermissionID] = 76


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 80)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (80, 8, 7)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 7 WHERE [PermissionID] = 80


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 83)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (83, 8, 8)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 8 WHERE [PermissionID] = 83


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 86)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (86, 8, 9)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 8, [ResourceID] = 9 WHERE [PermissionID] = 86


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 37)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (37, 19, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 19, [ResourceID] = 4 WHERE [PermissionID] = 37


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 66)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (66, 20, 2)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 20, [ResourceID] = 2 WHERE [PermissionID] = 66


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 67)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (67, 21, 2)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 21, [ResourceID] = 2 WHERE [PermissionID] = 67


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 68)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (68, 22, 2)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 22, [ResourceID] = 2 WHERE [PermissionID] = 68


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 38)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (38, 35, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 35, [ResourceID] = 4 WHERE [PermissionID] = 38


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 40)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (40, 36, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 36, [ResourceID] = 4 WHERE [PermissionID] = 40


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 41)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (41, 37, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 37, [ResourceID] = 4 WHERE [PermissionID] = 41


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 43)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (43, 38, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 38, [ResourceID] = 4 WHERE [PermissionID] = 43


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 35)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (35, 39, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 39, [ResourceID] = 4 WHERE [PermissionID] = 35


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 45)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (45, 40, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 40, [ResourceID] = 4 WHERE [PermissionID] = 45


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 46)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (46, 41, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 41, [ResourceID] = 4 WHERE [PermissionID] = 46


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 47)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (47, 42, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 42, [ResourceID] = 4 WHERE [PermissionID] = 47


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 48)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (48, 43, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 43, [ResourceID] = 4 WHERE [PermissionID] = 48


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 49)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (49, 44, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 44, [ResourceID] = 4 WHERE [PermissionID] = 49


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 50)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (50, 45, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 45, [ResourceID] = 4 WHERE [PermissionID] = 50


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 51)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (51, 46, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 46, [ResourceID] = 4 WHERE [PermissionID] = 51


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 36)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (36, 48, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 48, [ResourceID] = 4 WHERE [PermissionID] = 36


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 39)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (39, 50, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 50, [ResourceID] = 4 WHERE [PermissionID] = 39


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 42)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (42, 52, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 52, [ResourceID] = 4 WHERE [PermissionID] = 42


IF NOT EXISTS (SELECT [PermissionID] FROM [dbo].[permissions] WHERE [PermissionID] = 44)
     INSERT INTO [dbo].[permissions] ([PermissionID], [GroupID], [ResourceID])
     VALUES (44, 53, 4)
ELSE
     UPDATE [dbo].[permissions] SET [GroupID] = 53, [ResourceID] = 4 WHERE [PermissionID] = 44


SET IDENTITY_INSERT [dbo].[permissions] OFF


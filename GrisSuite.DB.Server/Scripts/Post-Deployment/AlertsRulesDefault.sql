DECLARE @RuleTemplateID INT

-- Template Regola 1: Persistenza in offline di una periferica
SET @RuleTemplateID = 1
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'Device in offline da pi� di n minuti'
	,'')


UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleDevicesOffline',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID


-- Template Regola 2: Persistenza di stato di una periferica
SET @RuleTemplateID = 2
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'Device nello stato x da pi� di n minuti'
	,'')


UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleDevicesOnSevLevel',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID


-- Template Regola 3: Fluttuazioni di offline di una periferica
SET @RuleTemplateID = 3
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'Device che sono entrati in offline per pi� di n volte negli ultimi di m minuti'
	,'')


UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleDevicesOfflineVar',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID



-- Template Regola 4: Fluttuazioni di stato di una periferica
SET @RuleTemplateID = 4
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'Device che sono entrati nello stato x per pi� di n volte negli ultimi di m minuti'
	,'')


UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleDevicesOnSevLevelVar',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID


-- Template Regola 5: Diagnostica DT13000 non funzionante (se DT04000=amplificatore e TT10210=pannellozone sono OK e DT13000=alimentatore in offline)
SET @RuleTemplateID = 5
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'Diagnostica DT13000 non funzionante (se DT04000=amplificatore e TT10210=pannellozone sono OK e DT13000=alimentatore in offline)'
	,'')


UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleDT13000',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID


-- Template Regola 6: Diagnostica Amplificatore DT04000 (se TT10210 � in errore e DT04000 � offline)
SET @RuleTemplateID = 6
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'Diagnostica Amplificatore DT04000 (se TT10210 � in errore e DT04000 � offline)'
	,'')


UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleDT04000',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID

-- Template Regola 7: Persistenza in offline di  STLC1000
SET @RuleTemplateID = 7
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'STLC1000 in offline da pi� di n minuti'
	,'')

UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleSTLC1000Offline',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID

-- Template Regola 8: STLC1000 in offline che hanno "sotto" nella linea n STLC1000 in offline
SET @RuleTemplateID = 8
IF NOT EXISTS(SELECT [RuleTemplateID] FROM [alerts_rules_templates] WHERE [RuleTemplateID] = @RuleTemplateID)
	INSERT INTO [alerts_rules_templates]
	([RuleTemplateID]
	,[RuleTemplateDescription]
	,[RuleTemplateExpression])
	VALUES
	(@RuleTemplateID
	,'STLC1000 in offline che hanno "sotto" nella linea n STLC1000 in offline'
	,'')

UPDATE [alerts_rules_templates]
SET [RuleTemplateExpression] ='gris_InsRuleSTLC1000OfflineOnZone',
	[RuleTemplateExpressionType] =1
WHERE [RuleTemplateID] = @RuleTemplateID


--- ==== REGOLE
DECLARE @RuleID INT
-- Persistenza in offline di una periferica per 1 ora
SET @RuleID = 1
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,1)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Device in offline da pi� di 1 ora'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@Minute(Int)=60;'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID


-- Persistenza in errore di una periferica per 1 ora
SET @RuleID = 2
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,2)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Device in errore da pi� di 1 ora'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@Minute(Int)=60;@SevLevel(Int)=2;'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID

-- Persistenza in attenzione di una periferica per 1 giorno
SET @RuleID = 3
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,2)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Device in attenzione da pi� di 1 giorno'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@Minute(Int)=1440;@SevLevel(Int)=1;'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID


-- Fluttuazioni di stato di una periferica offline 3 volte in 2 giorni
SET @RuleID = 4
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,3)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Device in offline pi� di 3 volte negli ultimi 2 giorni'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@Minute(Int)=2880;@OfflineCount(Int)=3;'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID


-- Fluttuazioni di stato di una periferica errore 5 volte in 2 giorni
SET @RuleID = 5
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,4)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Device in errore pi� di 5 volte negli ultimi 2 giorni'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@Minute(Int)=2880;@SevLevel(Int)=2;@SevLevelCount(Int)=5;'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID


-- Fluttuazioni di stato di una periferica attenzione 7 volte in 2 giorni
SET @RuleID = 6
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,4)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Device in attenzione pi� di 7 volte negli ultimi 2 giorni'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@Minute(Int)=2880;@SevLevel(Int)=1;@SevLevelCount(Int)=7;'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID


-- Diagnostica DT13000 non funzionante (se DT04000 e TT10210 sono OK e DT13000 in offline)
SET @RuleID = 7
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,5)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Diagnostica DT13000 non funzionante (se DT04000 e TT10210 sono OK e DT13000 in offline)'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID


-- Diagnostica Amplificatore DT04000 (se TT10210=pannellozone � in errore e DT04000=aplificatore � offline)
SET @RuleID = 8
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,6)


UPDATE [alerts_rules]
SET [RuleDescription] = 'Diagnostica Amplificatore DT04000 (se TT10210 � in errore e DT04000 � offline)'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) +';'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID


-- Persistenza in offline di una STLC1000 per 1 ora
SET @RuleID = 9
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',1,7)


UPDATE [alerts_rules]
SET [RuleDescription] = 'STLC1000 in offline da pi� di 1 ora'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@Minute(Int)=60;'
	,[ExecutionSequence] = @RuleID
WHERE [RuleID] = @RuleID

-- STLC1000 in offline che hanno "sotto" nella linea 3 STLC1000 in offline
SET @RuleID = 10
IF NOT EXISTS(SELECT [RuleID] FROM [alerts_rules] WHERE [RuleID] = @RuleID)
	INSERT INTO [alerts_rules]
			   ([RuleID]
			   ,[RuleDescription]
			   ,[RuleParameters]
			   ,[IsEnabled]
			   ,[RuleTemplateID])
		 VALUES
			   (@RuleID,'','',0,8)


UPDATE [alerts_rules]
SET [RuleDescription] = 'STLC1000 in offline che hanno "sotto" nella linea 3 STLC1000 in offline'
	,[RuleParameters] = '@RuleID(Int)=' + CAST(@RuleID AS VARCHAR) + ';@CountOfflineOnZone(Int)=3;'
	,[ExecutionSequence] = @RuleID
	,[IsEnabled] = 0
WHERE [RuleID] = @RuleID

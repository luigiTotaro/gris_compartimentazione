USE [master]
GO
-- Indicare nome dominio e utente corretti
CREATE LOGIN [RFIDBSGRIW01PRO\FormulaEngine] FROM WINDOWS WITH DEFAULT_DATABASE=[TelefinCentralPRO]
GO
CREATE LOGIN [RFIDBSGRIW02PRO\FormulaEngine] FROM WINDOWS WITH DEFAULT_DATABASE=[TelefinCentralPRO]
GO
USE [TelefinCentralPRO]
GO
-- Indicare nome dominio e utente corretti
CREATE USER [FormulaEngine] FOR LOGIN [RFIDBSGRIW01PRO\FormulaEngine]
GO
CREATE USER [FormulaEngine] FOR LOGIN [RFIDBSGRIW02PRO\FormulaEngine]
GO
USE [TelefinCentralPRO]
GO
CREATE ROLE FormulaEngineAccess
GRANT SELECT,INSERT,UPDATE ON dbo.object_status TO FormulaEngineAccess;
GRANT SELECT,INSERT,UPDATE,DELETE ON dbo.object_attributes TO FormulaEngineAccess;
GRANT SELECT ON dbo.devices TO FormulaEngineAccess;
GRANT SELECT ON dbo.device_status TO FormulaEngineAccess;
GRANT SELECT ON dbo.device_type TO FormulaEngineAccess;
GRANT SELECT ON dbo.regions TO FormulaEngineAccess;
GRANT SELECT ON dbo.[servers] TO FormulaEngineAccess;
GRANT SELECT ON dbo.systems TO FormulaEngineAccess;
GRANT SELECT ON dbo.node_systems TO FormulaEngineAccess;
GRANT SELECT ON dbo.zones TO FormulaEngineAccess;
GRANT SELECT ON dbo.object_formulas TO FormulaEngineAccess;
GRANT SELECT ON dbo.virtual_objects TO FormulaEngineAccess;
GRANT SELECT ON dbo.nodes TO FormulaEngineAccess;
GRANT SELECT ON dbo.object_attribute_types TO FormulaEngineAccess;
GRANT SELECT ON dbo.[parameters] TO FormulaEngineAccess;
GRANT SELECT ON dbo.entity_state TO FormulaEngineAccess;
GRANT EXECUTE ON dbo.tf_InsPopulateObjectStatus TO FormulaEngineAccess;
GRANT EXECUTE ON dbo.tf_UpdObjectStatusPostComputation TO FormulaEngineAccess;

EXEC sp_addrolemember  'FormulaEngineAccess','FormulaEngine'

--EXECUTE AS USER = 'FormulaEngine'
--Testare qui la query / chiamata a SP
--REVERT;
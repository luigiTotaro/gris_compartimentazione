SET NUMERIC_ROUNDABORT OFF
GO

----------------- entity_state -----------------------
IF NOT EXISTS (SELECT [EntityStateID] FROM [dbo].[entity_state] WHERE [EntityStateID] = 1)
     INSERT INTO [dbo].[entity_state] ([EntityStateID], [EntityStateName], [EntityStateSeverity], [EntityStateMinValue], [EntityStateMaxValue])
     VALUES (1, 'In Servizio', 0.00, 0, 3)
ELSE
     UPDATE [dbo].[entity_state] SET [EntityStateName] = 'In Servizio', [EntityStateSeverity] = 0.00, [EntityStateMinValue] = 0, [EntityStateMaxValue] = 3 WHERE [EntityStateID] = 1


IF NOT EXISTS (SELECT [EntityStateID] FROM [dbo].[entity_state] WHERE [EntityStateID] = 2)
     INSERT INTO [dbo].[entity_state] ([EntityStateID], [EntityStateName], [EntityStateSeverity], [EntityStateMinValue], [EntityStateMaxValue])
     VALUES (2, 'Anomalia Lieve', 4.40, 3, 7)
ELSE
     UPDATE [dbo].[entity_state] SET [EntityStateName] = 'Anomalia Lieve', [EntityStateSeverity] = 4.40, [EntityStateMinValue] = 3, [EntityStateMaxValue] = 7 WHERE [EntityStateID] = 2


IF NOT EXISTS (SELECT [EntityStateID] FROM [dbo].[entity_state] WHERE [EntityStateID] = 3)
     INSERT INTO [dbo].[entity_state] ([EntityStateID], [EntityStateName], [EntityStateSeverity], [EntityStateMinValue], [EntityStateMaxValue])
     VALUES (3, 'Anomalia Grave', 10.00, 7, 10)
ELSE
     UPDATE [dbo].[entity_state] SET [EntityStateName] = 'Anomalia Grave', [EntityStateSeverity] = 10.00, [EntityStateMinValue] = 7, [EntityStateMaxValue] = 10 WHERE [EntityStateID] = 3
----------------- END entity_state -----------------------

------------------- Parameters ---------------------------
IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'DBVersion') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('DBVersion', '3.0.0.0', 'La versione del DataBase')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'DevicesRefreshSleepTime') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('DevicesRefreshSleepTime', '3000', 'Tempo di attesa in millisecondi per dar modo all''STLC1000 di reinviare i dati delle periferiche')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'DeviceTypeListRevisionDate') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('DeviceTypeListRevisionDate', '2009-12-01 10:30:00.000', 'L''ultima data di revisione della Device Type List')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'DeviceTypeListVersion') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('DeviceTypeListVersion', '2.0.6.1', 'La versione della Device Type List')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'FDSVistaArmadioVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('FDSVistaArmadioVisible', 'true', 'Visualizza la vista armadio dell''FDS')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'LastCalcStatus') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('LastCalcStatus', '2010-03-10 17:20:24.500', 'Data ora ultimo calcolo degli stati')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'PageRefreshTime') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('PageRefreshTime', '600', 'Numero di secondi per il Refresh della pagina')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'RegionListVersion') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('RegionListVersion', '1.2.2.64', 'La versione della Region List')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'RegionSTLCOfflineEnabled') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('RegionSTLCOfflineEnabled', 'false', 'Va segnalata la presenza anche di un solo STLC1000 offline nel compartimento')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'TabularViewClippingRows') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('TabularViewClippingRows', '12', 'Numero di righe oltre cui subentra la visualizzazione tabulare dei dispositivi')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'ZoneSTLCOfflineEnabled') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('ZoneSTLCOfflineEnabled', 'false', 'Va segnalata la presenza anche di un solo STLC1000 offline nella linea')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'CacheLogMessagesObsoleteTimespanMinutes') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('CacheLogMessagesObsoleteTimespanMinutes', '15', 'Numero di minuti oltre cui è visualizzato un avviso relativo a problemi con il motore di scodamento righe di log')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'FiltersButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('FiltersButtonVisible', 'false', 'Visualizza il bottone di accesso alla pagina dei filtri')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'AlertsButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('AlertsButtonVisible', 'false', 'Visualizza il bottone di accesso alla pagina degli Allarmi')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'ReportsButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('ReportsButtonVisible', 'false', 'Visualizza il bottone di accesso alla pagina dei report')

END

IF NOT EXISTS (SELECT [ParameterName] FROM [dbo].[parameters] WHERE [ParameterName] = 'MappaButtonVisible') BEGIN

     INSERT INTO [dbo].[parameters] ([ParameterName], [ParameterValue], [ParameterDescription])
     VALUES ('MappaButtonVisible', 'false', 'Visualizza il bottone di accesso alla pagina della mappa')

END
------------------- END Parameters -----------------------

----------------- object_type -----------------------
IF NOT EXISTS (SELECT [ObjectTypeId] FROM [dbo].[object_type] WHERE [ObjectTypeId] = 1)
     INSERT INTO [dbo].[object_type] ([ObjectTypeId], [ObjectTypeName])
     VALUES (1, N'regions')
ELSE
     UPDATE [dbo].[object_type] SET [ObjectTypeName] = N'regions' WHERE [ObjectTypeId] = 1


IF NOT EXISTS (SELECT [ObjectTypeId] FROM [dbo].[object_type] WHERE [ObjectTypeId] = 2)
     INSERT INTO [dbo].[object_type] ([ObjectTypeId], [ObjectTypeName])
     VALUES (2, N'zones')
ELSE
     UPDATE [dbo].[object_type] SET [ObjectTypeName] = N'zones' WHERE [ObjectTypeId] = 2


IF NOT EXISTS (SELECT [ObjectTypeId] FROM [dbo].[object_type] WHERE [ObjectTypeId] = 3)
     INSERT INTO [dbo].[object_type] ([ObjectTypeId], [ObjectTypeName])
     VALUES (3, N'nodes')
ELSE
     UPDATE [dbo].[object_type] SET [ObjectTypeName] = N'nodes' WHERE [ObjectTypeId] = 3


IF NOT EXISTS (SELECT [ObjectTypeId] FROM [dbo].[object_type] WHERE [ObjectTypeId] = 4)
     INSERT INTO [dbo].[object_type] ([ObjectTypeId], [ObjectTypeName])
     VALUES (4, N'node_systems')
ELSE
     UPDATE [dbo].[object_type] SET [ObjectTypeName] = N'node_systems' WHERE [ObjectTypeId] = 4


IF NOT EXISTS (SELECT [ObjectTypeId] FROM [dbo].[object_type] WHERE [ObjectTypeId] = 5)
     INSERT INTO [dbo].[object_type] ([ObjectTypeId], [ObjectTypeName])
     VALUES (5, N'servers')
ELSE
     UPDATE [dbo].[object_type] SET [ObjectTypeName] = N'servers' WHERE [ObjectTypeId] = 5
	 

IF NOT EXISTS (SELECT [ObjectTypeId] FROM [dbo].[object_type] WHERE [ObjectTypeId] = 6)
     INSERT INTO [dbo].[object_type] ([ObjectTypeId], [ObjectTypeName])
     VALUES (6, N'devices')
ELSE
     UPDATE [dbo].[object_type] SET [ObjectTypeName] = N'devices' WHERE [ObjectTypeId] = 6


IF NOT EXISTS (SELECT [ObjectTypeId] FROM [dbo].[object_type] WHERE [ObjectTypeId] = 7)
     INSERT INTO [dbo].[object_type] ([ObjectTypeId], [ObjectTypeName])
     VALUES (7, N'virtual_object')
ELSE
     UPDATE [dbo].[object_type] SET [ObjectTypeName] = N'virtual_object' WHERE [ObjectTypeId] = 7
----------------- END object_type -----------------------

----------------- [object_attribute_types] -----------------------
IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 1)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (1, 'Severity')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'Severity' WHERE [AttributeTypeId] = 1


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 2)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (2, 'SeverityDetail')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'SeverityDetail' WHERE [AttributeTypeId] = 2


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 3)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (3, 'RealSeverity')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'RealSeverity' WHERE [AttributeTypeId] = 3


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 4)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (4, 'RealSeverityDetail')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'RealSeverityDetail' WHERE [AttributeTypeId] = 4


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 5)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (5, 'LastSeverity')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'LastSeverity' WHERE [AttributeTypeId] = 5


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 6)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (6, 'LastSeverityDetail')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'LastSeverityDetail' WHERE [AttributeTypeId] = 6


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 7)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (7, 'ExcludeFromParentStatus')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'ExcludeFromParentStatus' WHERE [AttributeTypeId] = 7


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 8)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (8, 'Color')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'Color' WHERE [AttributeTypeId] = 8


IF NOT EXISTS (SELECT [AttributeTypeId] FROM [dbo].[object_attribute_types] WHERE [AttributeTypeId] = 9)
     INSERT INTO [dbo].[object_attribute_types] ([AttributeTypeId], [AttributeName])
     VALUES (9, 'ForcedSevLevel')
ELSE
     UPDATE [dbo].[object_attribute_types] SET [AttributeName] = 'ForcedSevLevel' WHERE [AttributeTypeId] = 9
---------------- END [object_attribute_types] --------------

----------------- [alerts_rules_templates]  ----------------------
IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 1)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (1, 'Device in offline da più di n minuti', 'gris_InsRuleDevicesOffline', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'Device in offline da più di n minuti', [RuleTemplateExpression] = 'gris_InsRuleDevicesOffline', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 1


IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 2)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (2, 'Device nello stato x da più di n minuti', 'gris_InsRuleDevicesOnSevLevel', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'Device nello stato x da più di n minuti', [RuleTemplateExpression] = 'gris_InsRuleDevicesOnSevLevel', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 2


IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 3)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (3, 'Device che sono entrati in offline per più di n volte negli ultimi di m minuti', 'gris_InsRuleDevicesOfflineVar', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'Device che sono entrati in offline per più di n volte negli ultimi di m minuti', [RuleTemplateExpression] = 'gris_InsRuleDevicesOfflineVar', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 3


IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 4)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (4, 'Device che sono entrati nello stato x per più di n volte negli ultimi di m minuti', 'gris_InsRuleDevicesOnSevLevelVar', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'Device che sono entrati nello stato x per più di n volte negli ultimi di m minuti', [RuleTemplateExpression] = 'gris_InsRuleDevicesOnSevLevelVar', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 4


IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 5)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (5, 'Diagnostica DT13000 non funzionante (se DT04000=amplificatore e TT10210=pannellozone sono OK e DT13000=alimentatore in offline)', 'gris_InsRuleDT13000', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'Diagnostica DT13000 non funzionante (se DT04000=amplificatore e TT10210=pannellozone sono OK e DT13000=alimentatore in offline)', [RuleTemplateExpression] = 'gris_InsRuleDT13000', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 5


IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 6)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (6, 'Diagnostica Amplificatore DT04000 (se TT10210=pannellozone è in errore e DT04000=aplificatore è offline)', 'gris_InsRuleDT04000', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'Diagnostica Amplificatore DT04000 (se TT10210=pannellozone è in errore e DT04000=aplificatore è offline)', [RuleTemplateExpression] = 'gris_InsRuleDT04000', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 6


IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 7)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (7, 'STLC1000 in offline da più di n minuti', 'gris_InsRuleSTLC1000Offline', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'STLC1000 in offline da più di n minuti', [RuleTemplateExpression] = 'gris_InsRuleSTLC1000Offline', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 7


IF NOT EXISTS (SELECT [RuleTemplateID] FROM [dbo].[alerts_rules_templates] WHERE [RuleTemplateID] = 8)
     INSERT INTO [dbo].[alerts_rules_templates] ([RuleTemplateID], [RuleTemplateDescription], [RuleTemplateExpression], [RuleTemplateExpressionType])
     VALUES (8, 'STLC1000 in offline che hanno "sotto" nella linea n STLC1000 in offline', 'gris_InsRuleSTLC1000OfflineOnZone', 1)
ELSE
     UPDATE [dbo].[alerts_rules_templates] SET [RuleTemplateDescription] = 'STLC1000 in offline che hanno "sotto" nella linea n STLC1000 in offline', [RuleTemplateExpression] = 'gris_InsRuleSTLC1000OfflineOnZone', [RuleTemplateExpressionType] = 1 WHERE [RuleTemplateID] = 8


---------------- END [alerts_rules_templates] --------------

----------------- alerts_rules -----------------------
IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 1)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (1, 'Device in offline da più di 1 giorno', '@RuleID(Int)=1;@Minute(Int)=1440;', 1, 1, 2, 1)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Device in offline da più di 1 giorno', [RuleParameters] = '@RuleID(Int)=1;@Minute(Int)=1440;', [IsEnabled] = 1, [RuleTemplateID] = 1, [RuleSevLevel] = 2, [ExecutionSequence] = 1 WHERE [RuleID] = 1


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 2)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (2, 'Device in errore da più di 1 ora', '@RuleID(Int)=2;@Minute(Int)=60;@SevLevel(Int)=2;', 0, 2, 2, 2)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Device in errore da più di 1 ora', [RuleParameters] = '@RuleID(Int)=2;@Minute(Int)=60;@SevLevel(Int)=2;', [IsEnabled] = 0, [RuleTemplateID] = 2, [RuleSevLevel] = 2, [ExecutionSequence] = 2 WHERE [RuleID] = 2


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 3)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (3, 'Device in attenzione da più di 1 giorno', '@RuleID(Int)=3;@Minute(Int)=1440;@SevLevel(Int)=1;', 0, 2, 1, 3)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Device in attenzione da più di 1 giorno', [RuleParameters] = '@RuleID(Int)=3;@Minute(Int)=1440;@SevLevel(Int)=1;', [IsEnabled] = 0, [RuleTemplateID] = 2, [RuleSevLevel] = 1, [ExecutionSequence] = 3 WHERE [RuleID] = 3


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 4)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (4, 'Device in offline più di 3 volte negli ultimi 2 giorni', '@RuleID(Int)=4;@Minute(Int)=2880;@OfflineCount(Int)=3;', 0, 3, 1, 4)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Device in offline più di 3 volte negli ultimi 2 giorni', [RuleParameters] = '@RuleID(Int)=4;@Minute(Int)=2880;@OfflineCount(Int)=3;', [IsEnabled] = 0, [RuleTemplateID] = 3, [RuleSevLevel] = 1, [ExecutionSequence] = 4 WHERE [RuleID] = 4


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 5)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (5, 'Device in errore più di 5 volte negli ultimi 2 giorni', '@RuleID(Int)=5;@Minute(Int)=2880;@SevLevel(Int)=2;@SevLevelCount(Int)=5;', 0, 4, 1, 5)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Device in errore più di 5 volte negli ultimi 2 giorni', [RuleParameters] = '@RuleID(Int)=5;@Minute(Int)=2880;@SevLevel(Int)=2;@SevLevelCount(Int)=5;', [IsEnabled] = 0, [RuleTemplateID] = 4, [RuleSevLevel] = 1, [ExecutionSequence] = 5 WHERE [RuleID] = 5


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 6)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (6, 'Device in attenzione più di 7 volte negli ultimi 2 giorni', '@RuleID(Int)=6;@Minute(Int)=2880;@SevLevel(Int)=1;@SevLevelCount(Int)=7;', 0, 4, 1, 6)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Device in attenzione più di 7 volte negli ultimi 2 giorni', [RuleParameters] = '@RuleID(Int)=6;@Minute(Int)=2880;@SevLevel(Int)=1;@SevLevelCount(Int)=7;', [IsEnabled] = 0, [RuleTemplateID] = 4, [RuleSevLevel] = 1, [ExecutionSequence] = 6 WHERE [RuleID] = 6


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 7)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (7, 'Diagnostica DT13000 non funzionante (se DT04000 e TT10210 sono OK e DT13000 in offline)', '@RuleID(Int)=7;', 0, 5, 2, 7)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Diagnostica DT13000 non funzionante (se DT04000 e TT10210 sono OK e DT13000 in offline)', [RuleParameters] = '@RuleID(Int)=7;', [IsEnabled] = 0, [RuleTemplateID] = 5, [RuleSevLevel] = 2, [ExecutionSequence] = 7 WHERE [RuleID] = 7


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 8)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (8, 'Diagnostica Amplificatore DT04000 (se TT10210 è in errore e DT04000 è offline)', '@RuleID(Int)=8;', 0, 6, 2, 8)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'Diagnostica Amplificatore DT04000 (se TT10210 è in errore e DT04000 è offline)', [RuleParameters] = '@RuleID(Int)=8;', [IsEnabled] = 0, [RuleTemplateID] = 6, [RuleSevLevel] = 2, [ExecutionSequence] = 8 WHERE [RuleID] = 8


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 9)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (9, 'STLC1000 in offline da più di 20 minuti', '@RuleID(Int)=9;@Minute(Int)=20;', 1, 7, 2, 9)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'STLC1000 in offline da più di 20 minuti', [RuleParameters] = '@RuleID(Int)=9;@Minute(Int)=20;', [IsEnabled] = 1, [RuleTemplateID] = 7, [RuleSevLevel] = 2, [ExecutionSequence] = 9 WHERE [RuleID] = 9


IF NOT EXISTS (SELECT [RuleID] FROM [dbo].[alerts_rules] WHERE [RuleID] = 10)
     INSERT INTO [dbo].[alerts_rules] ([RuleID], [RuleDescription], [RuleParameters], [IsEnabled], [RuleTemplateID], [RuleSevLevel], [ExecutionSequence])
     VALUES (10, 'STLC1000 in offline che hanno "sotto" nella linea 3 STLC1000 in offline', '@RuleID(Int)=10;@CountOfflineOnZone(Int)=3;', 0, 8, 2, 10)
ELSE
     UPDATE [dbo].[alerts_rules] SET [RuleDescription] = 'STLC1000 in offline che hanno "sotto" nella linea 3 STLC1000 in offline', [RuleParameters] = '@RuleID(Int)=10;@CountOfflineOnZone(Int)=3;', [IsEnabled] = 0, [RuleTemplateID] = 8, [RuleSevLevel] = 2, [ExecutionSequence] = 10 WHERE [RuleID] = 10
----------------- END alerts_rules -----------------------


----------------- severity -----------------------
IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = -255)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (-255, 'Informazione', 0, 0, 0)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Informazione', [IsRelevantToDevice] = 0, [IsRelevantToServer] = 0, [IsRelevantToCustomColors] = 0 WHERE [SevLevel] = -255


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = -1)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (-1, 'Non classificato', 0, 1, 0)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Non classificato', [IsRelevantToDevice] = 0, [IsRelevantToServer] = 1, [IsRelevantToCustomColors] = 0 WHERE [SevLevel] = -1


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 0)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (0, 'In servizio', 1, 1, 1)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'In servizio', [IsRelevantToDevice] = 1, [IsRelevantToServer] = 1, [IsRelevantToCustomColors] = 1 WHERE [SevLevel] = 0


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 1)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (1, 'Anomalia lieve', 1, 0, 1)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Anomalia lieve', [IsRelevantToDevice] = 1, [IsRelevantToServer] = 0, [IsRelevantToCustomColors] = 1 WHERE [SevLevel] = 1


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 2)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (2, 'Anomalia grave', 1, 1, 1)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Anomalia grave', [IsRelevantToDevice] = 1, [IsRelevantToServer] = 1, [IsRelevantToCustomColors] = 1 WHERE [SevLevel] = 2


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 3)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (3, 'Offline', 0, 1, 0)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Offline', [IsRelevantToDevice] = 0, [IsRelevantToServer] = 1, [IsRelevantToCustomColors] = 0 WHERE [SevLevel] = 3


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 9)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (9, 'Non attivo', 1, 1, 0)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Non attivo', [IsRelevantToDevice] = 1, [IsRelevantToServer] = 1, [IsRelevantToCustomColors] = 0 WHERE [SevLevel] = 9


IF NOT EXISTS (SELECT [SevLevel] FROM [dbo].[severity] WHERE [SevLevel] = 255)
     INSERT INTO [dbo].[severity] ([SevLevel], [Description], [IsRelevantToDevice], [IsRelevantToServer], [IsRelevantToCustomColors])
     VALUES (255, 'Sconosciuto', 1, 0, 0)
ELSE
     UPDATE [dbo].[severity] SET [Description] = 'Sconosciuto', [IsRelevantToDevice] = 1, [IsRelevantToServer] = 0, [IsRelevantToCustomColors] = 0 WHERE [SevLevel] = 255
----------------- END severity -----------------------


----------------- severity_details -----------------------
IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = -1)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (-1, -1, 'Stato non disponibile')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = -1, [Description] = 'Stato non disponibile' WHERE [SevLevelDetailId] = -1


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 0)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (0, 0, 'In servizio')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 0, [Description] = 'In servizio' WHERE [SevLevelDetailId] = 0


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 1)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (1, 1, 'Anomalia lieve')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 1, [Description] = 'Anomalia lieve' WHERE [SevLevelDetailId] = 1


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 2)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (2, 2, 'Anomalia grave')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 2, [Description] = 'Anomalia grave' WHERE [SevLevelDetailId] = 2


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 3)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (3, 3, 'Offline')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 3, [Description] = 'Offline' WHERE [SevLevelDetailId] = 3


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 4)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (4, 9, 'Non attivo')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Non attivo' WHERE [SevLevelDetailId] = 4


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 5)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (5, 255, 'Sconosciuto')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 255, [Description] = 'Sconosciuto' WHERE [SevLevelDetailId] = 5


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 6)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (6, 9, 'Diagnostica non disponibile')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Diagnostica non disponibile' WHERE [SevLevelDetailId] = 6


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 7)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (7, 9, 'Diagnostica disattivata dall''operatore')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Diagnostica disattivata dall''operatore' WHERE [SevLevelDetailId] = 7


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 8)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (8, 9, 'Ip duplicato')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Ip duplicato' WHERE [SevLevelDetailId] = 8


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 9)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (9, 9, 'Collettore STLC1000 non attivo')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Collettore STLC1000 non attivo' WHERE [SevLevelDetailId] = 9


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 10)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (10, 255, 'Collettore STLC1000 non raggiungibile (offline)')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 255, [Description] = 'Collettore STLC1000 non raggiungibile (offline)' WHERE [SevLevelDetailId] = 10


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 11)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (11, -1, 'Storico')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = -1, [Description] = 'Storico' WHERE [SevLevelDetailId] = 11


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 12)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (12, 9, 'Senza indirizzo IP')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Senza indirizzo IP' WHERE [SevLevelDetailId] = 12


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 13)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (13, 9, 'Dati diagnostici limitati alla sola sezione MIB-II')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Dati diagnostici limitati alla sola sezione MIB-II' WHERE [SevLevelDetailId] = 13


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 14)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (14, 9, 'Senza FullHostName')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Senza FullHostName' WHERE [SevLevelDetailId] = 14


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 15)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (15, 9, 'Senza MAC Address')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Senza MAC Address' WHERE [SevLevelDetailId] = 15


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 16)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (16, 9, 'Dati SNMP incongruenti')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 9, [Description] = 'Dati SNMP incongruenti' WHERE [SevLevelDetailId] = 16


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 17)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (17, 1, 'Dati diagnostici limitati alla sola sezione MIB-II')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 1, [Description] = 'Dati diagnostici limitati alla sola sezione MIB-II' WHERE [SevLevelDetailId] = 17


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 18)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (18, 1, 'Dati SNMP incongruenti')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 1, [Description] = 'Dati SNMP incongruenti' WHERE [SevLevelDetailId] = 18


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 19)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (19, 255, 'Dispositivo non raggiungibile')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 255, [Description] = 'Dispositivo non raggiungibile' WHERE [SevLevelDetailId] = 19


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 20)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (20, 1, 'Diagnostica non disponibile')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 1, [Description] = 'Diagnostica non disponibile' WHERE [SevLevelDetailId] = 20


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 21)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (21, 1, 'Dati di monitoraggio incompleti')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 1, [Description] = 'Dati di monitoraggio incompleti' WHERE [SevLevelDetailId] = 21


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 22)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (22, 2, 'Dati del collettore incompleti')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 2, [Description] = 'Dati del collettore incompleti' WHERE [SevLevelDetailId] = 22


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 23)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (23, 255, 'Non applicabile')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 255, [Description] = 'Non applicabile' WHERE [SevLevelDetailId] = 23


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 24)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (24, 0, 'In servizio, ma non al 100%')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 0, [Description] = 'In servizio, ma non al 100%' WHERE [SevLevelDetailId] = 24


IF NOT EXISTS (SELECT [SevLevelDetailId] FROM [dbo].[severity_details] WHERE [SevLevelDetailId] = 25)
     INSERT INTO [dbo].[severity_details] ([SevLevelDetailId], [SevLevel], [Description])
     VALUES (25, 2, 'Dispositivo non raggiungibile')
ELSE
     UPDATE [dbo].[severity_details] SET [SevLevel] = 2, [Description] = 'Dispositivo non raggiungibile' WHERE [SevLevelDetailId] = 25

----------------- END severity_details -----------------------


----------------- vendors -----------------------

IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 1)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (1, 'Sysco', NULL)
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Sysco', [VendorDescription] = NULL WHERE [VendorID] = 1


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 2)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (2, 'Aesys', NULL)
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Aesys', [VendorDescription] = NULL WHERE [VendorID] = 2


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 3)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (3, 'Solari', 'Solari di Udine')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Solari', [VendorDescription] = 'Solari di Udine' WHERE [VendorID] = 3


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 4)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (4, 'Telefin', 'Telefin')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Telefin', [VendorDescription] = 'Telefin' WHERE [VendorID] = 4


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 5)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (5, 'Prase', NULL)
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Prase', [VendorDescription] = NULL WHERE [VendorID] = 5


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 6)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (6, 'Sysnet', NULL)
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Sysnet', [VendorDescription] = NULL WHERE [VendorID] = 6


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 7)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (7, 'APC', 'American Power Conversion Corp.')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'APC', [VendorDescription] = 'American Power Conversion Corp.' WHERE [VendorID] = 7


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 8)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (8, 'InfoStazioni', 'InfoStazioni')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'InfoStazioni', [VendorDescription] = 'InfoStazioni' WHERE [VendorID] = 8


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 9)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (9, 'Eletech', 'Eletech S.r.l.')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Eletech', [VendorDescription] = 'Eletech S.r.l.' WHERE [VendorID] = 9


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 10)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (10, 'HP', 'Hewlett-Packard')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'HP', [VendorDescription] = 'Hewlett-Packard' WHERE [VendorID] = 10


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 11)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (11, 'RAD Data Communications', 'RAD Data Communications Ltd.')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'RAD Data Communications', [VendorDescription] = 'RAD Data Communications Ltd.' WHERE [VendorID] = 11


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 12)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (12, 'Microsoft', 'Microsoft Corporation')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Microsoft', [VendorDescription] = 'Microsoft Corporation' WHERE [VendorID] = 12


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 13)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (13, 'MuLogic', 'MuLogic b.v.')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'MuLogic', [VendorDescription] = 'MuLogic b.v.' WHERE [VendorID] = 13


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 14)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (14, 'Cisco', 'Cisco Systems, Inc')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Cisco', [VendorDescription] = 'Cisco Systems, Inc' WHERE [VendorID] = 14


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 15)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (15, 'Fida', 'Fida S.p.A.')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Fida', [VendorDescription] = 'Fida S.p.A.' WHERE [VendorID] = 15


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 16)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (16, 'Patton', 'Patton Electronics Co.')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Patton', [VendorDescription] = 'Patton Electronics Co.' WHERE [VendorID] = 16


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 17)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (17, 'sdi', 's.d.i. automazione industriale')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'sdi', [VendorDescription] = 's.d.i. automazione industriale' WHERE [VendorID] = 17


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 18)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (18, 'Keymile', 'KEYMILE GmbH')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Keymile', [VendorDescription] = 'KEYMILE GmbH' WHERE [VendorID] = 18


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 19)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (19, 'Grandstream', 'Grandstream Networks')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Grandstream', [VendorDescription] = 'Grandstream Networks' WHERE [VendorID] = 19


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 20)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (20, 'Planet', 'PLANET Technology Corporation')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Planet', [VendorDescription] = 'PLANET Technology Corporation' WHERE [VendorID] = 20


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 21)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (21, 'Ansaldo STS', 'Ansaldo STS S.p.A.')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Ansaldo STS', [VendorDescription] = 'Ansaldo STS S.p.A.' WHERE [VendorID] = 21


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 22)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (22, 'Eurotel', 'Eurotel')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'Eurotel', [VendorDescription] = 'Eurotel' WHERE [VendorID] = 22


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 23)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (23, 'HW group', 'HW group')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'HW group', [VendorDescription] = 'HW group' WHERE [VendorID] = 23


IF NOT EXISTS (SELECT [VendorID] FROM [dbo].[vendors] WHERE [VendorID] = 24)
     INSERT INTO [dbo].[vendors] ([VendorID], [VendorName], [VendorDescription])
     VALUES (24, 'N/D', 'N/D')
ELSE
     UPDATE [dbo].[vendors] SET [VendorName] = 'N/D', [VendorDescription] = 'N/D' WHERE [VendorID] = 24

----------------- END vendors -----------------------

----------------- technology ------------------------

IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 1)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (1, 'CRT', 'CRT')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'CRT', [TechnologyDescription] = 'CRT' WHERE [TechnologyID] = 1


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 2)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (2, 'TFT', 'TFT')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'TFT', [TechnologyDescription] = 'TFT' WHERE [TechnologyID] = 2


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 3)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (3, 'LED', 'LED')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'LED', [TechnologyDescription] = 'LED' WHERE [TechnologyID] = 3


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 6)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (6, 'AUDIA', 'AUDIA')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'AUDIA', [TechnologyDescription] = 'AUDIA' WHERE [TechnologyID] = 6


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 7)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (7, 'AV-DIGITAL', 'AV-DIGITAL')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'AV-DIGITAL', [TechnologyDescription] = 'AV-DIGITAL' WHERE [TechnologyID] = 7


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 11)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (11, 'DT00000', 'DTS Standard - DT00000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT00000', [TechnologyDescription] = 'DTS Standard - DT00000' WHERE [TechnologyID] = 11


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 12)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (12, 'DT00L00', 'Pannello Ricezione TDS - DT00L00')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT00L00', [TechnologyDescription] = 'Pannello Ricezione TDS - DT00L00' WHERE [TechnologyID] = 12


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 13)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (13, 'DT00U00', 'Pannello remotizzazione DTS - DT00U00')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT00U00', [TechnologyDescription] = 'Pannello remotizzazione DTS - DT00U00' WHERE [TechnologyID] = 13


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 14)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (14, 'DT01000', 'DTS Sistema 64 - DT01000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT01000', [TechnologyDescription] = 'DTS Sistema 64 - DT01000' WHERE [TechnologyID] = 14


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 15)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (15, 'DT01110', 'Pannello Emergenza Posto Centrale - DT01110')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT01110', [TechnologyDescription] = 'Pannello Emergenza Posto Centrale - DT01110' WHERE [TechnologyID] = 15


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 16)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (16, 'DT01210', 'Pannello Emergenza Periferico - DT01210')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT01210', [TechnologyDescription] = 'Pannello Emergenza Periferico - DT01210' WHERE [TechnologyID] = 16


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 17)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (17, 'DT01210-DS', 'Pannello Emergenza DS - DT01210-DS')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT01210-DS', [TechnologyDescription] = 'Pannello Emergenza DS - DT01210-DS' WHERE [TechnologyID] = 17


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 18)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (18, 'DT01700', 'Pannello DTS /16 - DT01700')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT01700', [TechnologyDescription] = 'Pannello DTS /16 - DT01700' WHERE [TechnologyID] = 18


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 19)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (19, 'DT02000', 'CPU Gestione 5 Ingressi - DT02000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT02000', [TechnologyDescription] = 'CPU Gestione 5 Ingressi - DT02000' WHERE [TechnologyID] = 19


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 20)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (20, 'DT04000', 'Amplificatore - DT04000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT04000', [TechnologyDescription] = 'Amplificatore - DT04000' WHERE [TechnologyID] = 20


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 21)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (21, 'DT05000', 'DTS Standard - DT05000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT05000', [TechnologyDescription] = 'DTS Standard - DT05000' WHERE [TechnologyID] = 21


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 22)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (22, 'DT05B10', 'Pannello TX TDS - DT05B10')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT05B10', [TechnologyDescription] = 'Pannello TX TDS - DT05B10' WHERE [TechnologyID] = 22


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 23)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (23, 'DT05BC0', 'Pannello TX TDS - DT05BC0')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT05BC0', [TechnologyDescription] = 'Pannello TX TDS - DT05BC0' WHERE [TechnologyID] = 23


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 24)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (24, 'DT05L00', 'Pannello TDS - DT05L00')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT05L00', [TechnologyDescription] = 'Pannello TDS - DT05L00' WHERE [TechnologyID] = 24


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 25)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (25, 'DT05L10', 'Pannello TDS - DT05L10')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT05L10', [TechnologyDescription] = 'Pannello TDS - DT05L10' WHERE [TechnologyID] = 25


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 26)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (26, 'DT13000', 'Alimentazione - DT13000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT13000', [TechnologyDescription] = 'Alimentazione - DT13000' WHERE [TechnologyID] = 26


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 27)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (27, 'FD90000-NL', 'Sistema FDS Normale')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000-NL', [TechnologyDescription] = 'Sistema FDS Normale' WHERE [TechnologyID] = 27


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 28)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (28, 'FD90000-NR', 'Sistema FDS Normale')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000-NR', [TechnologyDescription] = 'Sistema FDS Normale' WHERE [TechnologyID] = 28


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 29)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (29, 'FD90000-RL', 'Sistema FDS Riserva')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000-RL', [TechnologyDescription] = 'Sistema FDS Riserva' WHERE [TechnologyID] = 29


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 30)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (30, 'FD90000-RR', 'Sistema FDS Riserva')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000-RR', [TechnologyDescription] = 'Sistema FDS Riserva' WHERE [TechnologyID] = 30


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 31)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (31, 'GPIO3006', 'Periferica multi IO - GPIO3006')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'GPIO3006', [TechnologyDescription] = 'Periferica multi IO - GPIO3006' WHERE [TechnologyID] = 31


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 32)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (32, 'STLC1000', 'STLC1000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'STLC1000', [TechnologyDescription] = 'STLC1000' WHERE [TechnologyID] = 32


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 33)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (33, 'TT10210', 'Pannello Zone - TT10210')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'TT10210', [TechnologyDescription] = 'Pannello Zone - TT10210' WHERE [TechnologyID] = 33


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 34)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (34, 'TT10210V3', 'Pannello Zone - TT10210V3')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'TT10210V3', [TechnologyDescription] = 'Pannello Zone - TT10210V3' WHERE [TechnologyID] = 34


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 35)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (35, 'XXIP000', 'Periferica IP generica - XXIP000')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'XXIP000', [TechnologyDescription] = 'Periferica IP generica - XXIP000' WHERE [TechnologyID] = 35


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 36)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (36, 'XXSNMP0', 'Periferica SNMP generica - XXSNMP0')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'XXSNMP0', [TechnologyDescription] = 'Periferica SNMP generica - XXSNMP0' WHERE [TechnologyID] = 36


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 37)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (37, 'Axys', 'Axys')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Axys', [TechnologyDescription] = 'Axys' WHERE [TechnologyID] = 37


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 38)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (38, 'SalzBrenner', 'Salz Brenner')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'SalzBrenner', [TechnologyDescription] = 'Salz Brenner' WHERE [TechnologyID] = 38


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 39)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (39, 'Nexia', 'Nexia')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Nexia', [TechnologyDescription] = 'Nexia' WHERE [TechnologyID] = 39


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 40)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (40, 'Workstation Windows', 'Workstation Windows')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Workstation Windows', [TechnologyDescription] = 'Workstation Windows' WHERE [TechnologyID] = 40


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 41)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (41, 'AIMSTSI', 'Sistema AIM STSI')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'AIMSTSI', [TechnologyDescription] = 'Sistema AIM STSI' WHERE [TechnologyID] = 41


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 42)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (42, 'TT10220', 'Pannello Zone DS PZi - TT10220')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'TT10220', [TechnologyDescription] = 'Pannello Zone DS PZi - TT10220' WHERE [TechnologyID] = 42


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 43)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (43, 'Sysnet E1-G703', 'Sysnet Zeus TCP/IP link E1 G703')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Sysnet E1-G703', [TechnologyDescription] = 'Sysnet Zeus TCP/IP link E1 G703' WHERE [TechnologyID] = 43


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 44)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (44, 'Sysnet SHDSL', 'Sysnet Zeus TCP/IP doppino G.SHDSL')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Sysnet SHDSL', [TechnologyDescription] = 'Sysnet Zeus TCP/IP doppino G.SHDSL' WHERE [TechnologyID] = 44


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 45)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (45, 'Sysnet generico', 'Sysnet Zeus')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Sysnet generico', [TechnologyDescription] = 'Sysnet Zeus' WHERE [TechnologyID] = 45


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 46)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (46, 'Eletech WebRadio', 'Eletech Web Radio Insertion - IP2Audio - Audio2IP')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Eletech WebRadio', [TechnologyDescription] = 'Eletech Web Radio Insertion - IP2Audio - Audio2IP' WHERE [TechnologyID] = 46


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 47)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (47, 'HP ProCurve', 'HP ProCurve Switch')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'HP ProCurve', [TechnologyDescription] = 'HP ProCurve Switch' WHERE [TechnologyID] = 47


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 48)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (48, 'RAD Multiplexer', 'RAD Multiplexer')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'RAD Multiplexer', [TechnologyDescription] = 'RAD Multiplexer' WHERE [TechnologyID] = 48


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 49)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (49, 'Workstation PDL', 'Workstation PDL')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Workstation PDL', [TechnologyDescription] = 'Workstation PDL' WHERE [TechnologyID] = 49


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 50)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (50, 'MuLogic Modem', 'MuLogic Modem for copper wire networks')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'MuLogic Modem', [TechnologyDescription] = 'MuLogic Modem for copper wire networks' WHERE [TechnologyID] = 50


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 51)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (51, 'Cisco switch', 'Cisco switch catalyst')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Cisco switch', [TechnologyDescription] = 'Cisco switch catalyst' WHERE [TechnologyID] = 51


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 52)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (52, 'DT13100', 'Alimentazione - DT13100')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'DT13100', [TechnologyDescription] = 'Alimentazione - DT13100' WHERE [TechnologyID] = 52


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 53)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (53, 'G.703', 'Drop Insert G.703')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'G.703', [TechnologyDescription] = 'Drop Insert G.703' WHERE [TechnologyID] = 53


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 54)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (54, 'Patton Router', 'Patton High Speed Router')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Patton Router', [TechnologyDescription] = 'Patton High Speed Router' WHERE [TechnologyID] = 54


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 55)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (55, 'FD90000L-NL', 'Sistema FDS Light Normale')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000L-NL', [TechnologyDescription] = 'Sistema FDS Light Normale' WHERE [TechnologyID] = 55


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 56)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (56, 'FD90000L-NR', 'Sistema FDS Light Normale')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000L-NR', [TechnologyDescription] = 'Sistema FDS Light Normale' WHERE [TechnologyID] = 56


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 57)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (57, 'FD90000L-RL', 'Sistema FDS Light Riserva')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000L-RL', [TechnologyDescription] = 'Sistema FDS Light Riserva' WHERE [TechnologyID] = 57


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 58)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (58, 'FD90000L-RR', 'Sistema FDS Light Riserva')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'FD90000L-RR', [TechnologyDescription] = 'Sistema FDS Light Riserva' WHERE [TechnologyID] = 58


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 59)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (59, 'sdi UAS/NTA', 'sdi UAS/NTA')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'sdi UAS/NTA', [TechnologyDescription] = 'sdi UAS/NTA' WHERE [TechnologyID] = 59


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 60)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (60, 'VP69000', 'Interfaccia di Diffusione Sonora')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'VP69000', [TechnologyDescription] = 'Interfaccia di Diffusione Sonora' WHERE [TechnologyID] = 60


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 61)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (61, 'VP66000', 'ATA FXS 2 linee')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'VP66000', [TechnologyDescription] = 'ATA FXS 2 linee' WHERE [TechnologyID] = 61


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 62)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (62, 'VP30006', 'Switch 8 porte')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'VP30006', [TechnologyDescription] = 'Switch 8 porte' WHERE [TechnologyID] = 62


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 63)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (63, 'VP50000', 'Modem Keymile LineRunner SCADA NG')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'VP50000', [TechnologyDescription] = 'Modem Keymile LineRunner SCADA NG' WHERE [TechnologyID] = 63


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 64)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (64, 'VP18007', 'Telefono Grandstream GXP2110')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'VP18007', [TechnologyDescription] = 'Telefono Grandstream GXP2110' WHERE [TechnologyID] = 64


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 65)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (65, 'VP00010', 'IP-PBX Centralino VoIP')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'VP00010', [TechnologyDescription] = 'IP-PBX Centralino VoIP' WHERE [TechnologyID] = 65


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 66)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (66, 'Ansaldo Posto Periferico', 'Ansaldo Posto Periferico')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'Ansaldo Posto Periferico', [TechnologyDescription] = 'Ansaldo Posto Periferico' WHERE [TechnologyID] = 66


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 67)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (67, 'ETWEBAIO', 'Web Alarm I/O')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'ETWEBAIO', [TechnologyDescription] = 'Web Alarm I/O' WHERE [TechnologyID] = 67


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 68)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (68, 'VoIP', 'VoIP')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'VoIP', [TechnologyDescription] = 'VoIP' WHERE [TechnologyID] = 68


IF NOT EXISTS (SELECT [TechnologyID] FROM [dbo].[technology] WHERE [TechnologyID] = 69)
     INSERT INTO [dbo].[technology] ([TechnologyID], [TechnologyCode], [TechnologyDescription])
     VALUES (69, 'I/O device', 'I/O device')
ELSE
     UPDATE [dbo].[technology] SET [TechnologyCode] = 'I/O device', [TechnologyDescription] = 'I/O device' WHERE [TechnologyID] = 69
	 	 
----------------- END technology --------------------

----------------- systems -----------------------

IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 1)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (1, 'Diffusione Sonora')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Diffusione Sonora' WHERE [SystemID] = 1


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 2)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (2, 'Informazione visiva')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Informazione visiva' WHERE [SystemID] = 2


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 3)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (3, 'Sistema Illuminazione')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Sistema Illuminazione' WHERE [SystemID] = 3


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 4)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (4, 'Sistema Telefonia')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Sistema Telefonia' WHERE [SystemID] = 4


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 5)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (5, 'Rete')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Rete' WHERE [SystemID] = 5


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 6)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (6, 'Sistema Erogazione Energia')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Sistema Erogazione Energia' WHERE [SystemID] = 6


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 8)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (8, 'Sistema FDS')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Sistema FDS' WHERE [SystemID] = 8


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 10)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (10, 'Diagnostica')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Diagnostica' WHERE [SystemID] = 10


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 11)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (11, 'Facilities')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Facilities' WHERE [SystemID] = 11


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 12)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (12, 'Monitoraggio VPN Verde')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Monitoraggio VPN Verde' WHERE [SystemID] = 12


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 13)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (13, 'Web Radio')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Web Radio' WHERE [SystemID] = 13


IF NOT EXISTS (SELECT [SystemID] FROM [dbo].[systems] WHERE [SystemID] = 99)
     INSERT INTO [dbo].[systems] ([SystemID], [SystemDescription])
     VALUES (99, 'Altre Periferiche')
ELSE
     UPDATE [dbo].[systems] SET [SystemDescription] = 'Altre Periferiche' WHERE [SystemID] = 99
----------------- END systems ----------------------

----------------- [classifications] -----------------------
IF NOT EXISTS (SELECT [ClassificationId] FROM [dbo].[classifications] WHERE [ClassificationId] = 1)
     INSERT INTO [dbo].[classifications] ([ClassificationId], [ClassificationName])
     VALUES (1, 'Metals')
ELSE
     UPDATE [dbo].[classifications] SET [ClassificationName] = 'Metals' WHERE [ClassificationId] = 1
-----------------END [classifications] -----------------------

----------------- [classification_values] -----------------------
IF NOT EXISTS (SELECT [ClassificationValueId] FROM [dbo].[classification_values] WHERE [ClassificationValueId] = 1)
     INSERT INTO [dbo].[classification_values] ([ClassificationValueId], [ClassificationId], [ValueName], [ValueSequence])
     VALUES (1, 1, 'platinum', 0)
ELSE
     UPDATE [dbo].[classification_values] SET [ClassificationId] = 1, [ValueName] = 'platinum', [ValueSequence] = 0 WHERE [ClassificationValueId] = 1


IF NOT EXISTS (SELECT [ClassificationValueId] FROM [dbo].[classification_values] WHERE [ClassificationValueId] = 2)
     INSERT INTO [dbo].[classification_values] ([ClassificationValueId], [ClassificationId], [ValueName], [ValueSequence])
     VALUES (2, 1, 'gold', 1)
ELSE
     UPDATE [dbo].[classification_values] SET [ClassificationId] = 1, [ValueName] = 'gold', [ValueSequence] = 1 WHERE [ClassificationValueId] = 2


IF NOT EXISTS (SELECT [ClassificationValueId] FROM [dbo].[classification_values] WHERE [ClassificationValueId] = 3)
     INSERT INTO [dbo].[classification_values] ([ClassificationValueId], [ClassificationId], [ValueName], [ValueSequence])
     VALUES (3, 1, 'silver', 2)
ELSE
     UPDATE [dbo].[classification_values] SET [ClassificationId] = 1, [ValueName] = 'silver', [ValueSequence] = 2 WHERE [ClassificationValueId] = 3


IF NOT EXISTS (SELECT [ClassificationValueId] FROM [dbo].[classification_values] WHERE [ClassificationValueId] = 4)
     INSERT INTO [dbo].[classification_values] ([ClassificationValueId], [ClassificationId], [ValueName], [ValueSequence])
     VALUES (4, 1, 'bronze', 3)
ELSE
     UPDATE [dbo].[classification_values] SET [ClassificationId] = 1, [ValueName] = 'bronze', [ValueSequence] = 3 WHERE [ClassificationValueId] = 4
-----------------END [classification_values] -----------------------

----------------- [device_type] -----------------------
IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'AEL0000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('AEL0000', 2, 2, 'Aesys AEL0000 - Monitor LED', 'http://{0}/icmonitor.wsdl', 2010, 3, 0, 'Monitor', 'TCP_Client', 'public', 'AEL0000.png', 320, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 2, [DeviceTypeDescription] = 'Aesys AEL0000 - Monitor LED', [WSUrlPattern] = 'http://{0}/icmonitor.wsdl', [Order] = 2010, [TechnologyID] = 3, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'AEL0000.png', [GlobalOrder] = 320, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'AEL0000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'AET0000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('AET0000', 2, 2, 'Aesys AET0000 - Monitor TFT', 'http://{0}/icmonitor.wsdl', 2011, 2, 0, 'Monitor', 'TCP_Client', 'public', 'AET0000.png', 330, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 2, [DeviceTypeDescription] = 'Aesys AET0000 - Monitor TFT', [WSUrlPattern] = 'http://{0}/icmonitor.wsdl', [Order] = 2011, [TechnologyID] = 2, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'AET0000.png', [GlobalOrder] = 330, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'AET0000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'AIMCTS0')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('AIMCTS0', 4, 4, 'Telefin AIMCTS0 - Sistema STSI CTS-0', NULL, 4040, 41, 0, 'Sistema STSI CTS-0', 'STLC1000_RS485', NULL, 'AIMCTS0.png', 3885, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin AIMCTS0 - Sistema STSI CTS-0', [WSUrlPattern] = NULL, [Order] = 4040, [TechnologyID] = 41, [Obsolete] = 0, [DefaultName] = 'Sistema STSI CTS-0', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'AIMCTS0.png', [GlobalOrder] = 3885, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'AIMCTS0'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'AIMCTSN')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('AIMCTSN', 4, 4, 'Telefin AIMCTSN - Sistema STSI CTS di Stazione', NULL, 4050, 41, 0, 'Sistema STSI CTS di Stazione', 'STLC1000_RS485', NULL, 'AIMCTS0.png', 3886, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin AIMCTSN - Sistema STSI CTS di Stazione', [WSUrlPattern] = NULL, [Order] = 4050, [TechnologyID] = 41, [Obsolete] = 0, [DefaultName] = 'Sistema STSI CTS di Stazione', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'AIMCTS0.png', [GlobalOrder] = 3886, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'AIMCTSN'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'ANPP000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('ANPP000', 99, 21, 'ANPP000 - ASTS Posto Periferico Applicativi', NULL, 5081, 66, 0, 'Applicativi PP', 'TCP_Client', 'public', 'ANPP000.png', 3521, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 99, [VendorID] = 21, [DeviceTypeDescription] = 'ANPP000 - ASTS Posto Periferico Applicativi', [WSUrlPattern] = NULL, [Order] = 5081, [TechnologyID] = 66, [Obsolete] = 0, [DefaultName] = 'Applicativi PP', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'ANPP000.png', [GlobalOrder] = 3521, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'ANPP000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'ANPP100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('ANPP100', 99, 21, 'ANPP100 - ASTS Posto Periferico', NULL, 5082, 66, 0, 'Posto Periferico', 'TCP_Client', 'public', 'ANPP100.png', 3522, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 99, [VendorID] = 21, [DeviceTypeDescription] = 'ANPP100 - ASTS Posto Periferico', [WSUrlPattern] = NULL, [Order] = 5082, [TechnologyID] = 66, [Obsolete] = 0, [DefaultName] = 'Posto Periferico', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'ANPP100.png', [GlobalOrder] = 3522, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'ANPP100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'AP00000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('AP00000', 6, 7, 'APC AP00000 - Gruppo di Continuita` generico', NULL, 6000, NULL, 0, 'Gruppo di Continuita`', 'TCP_Client', 'public', 'AP00000.png', 960, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 6, [VendorID] = 7, [DeviceTypeDescription] = 'APC AP00000 - Gruppo di Continuita` generico', [WSUrlPattern] = NULL, [Order] = 6000, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Gruppo di Continuita`', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'AP00000.png', [GlobalOrder] = 960, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'AP00000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'APDP3120')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('APDP3120', 6, 7, 'APC APDP3120 - Gruppo di Continuita` DP 3120', NULL, 6010, NULL, 0, 'Gruppo di Continuita`', 'TCP_Client', 'public', 'APDP3120.png', 970, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 6, [VendorID] = 7, [DeviceTypeDescription] = 'APC APDP3120 - Gruppo di Continuita` DP 3120', [WSUrlPattern] = NULL, [Order] = 6010, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Gruppo di Continuita`', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'APDP3120.png', [GlobalOrder] = 970, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'APDP3120'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT000', 5, 14, 'CISCAT000 - Periferica Cisco Catalyst Generica', NULL, 5110, 51, 0, 'Cisco Device', 'TCP_Client', 'N3T2010', 'CISCAT000.png', 3550, 1, 2)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT000 - Periferica Cisco Catalyst Generica', [WSUrlPattern] = NULL, [Order] = 5110, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Cisco Device', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT000.png', [GlobalOrder] = 3550, [ExportToList] = 1, [DiscoveryType] = 2 WHERE [DeviceTypeID] = 'CISCAT000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT001')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT001', 5, 14, 'CISCAT001 - Periferica Cisco Catalyst Generica', NULL, 5111, 51, 0, 'Cisco Device', 'TCP_Client', 'N3T2010', 'CISCAT001.png', 3600, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT001 - Periferica Cisco Catalyst Generica', [WSUrlPattern] = NULL, [Order] = 5111, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Cisco Device', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT001.png', [GlobalOrder] = 3600, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'CISCAT001'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT295012')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT295012', 5, 14, 'CISCAT295012 - Periferica Switch Cisco Catalyst 2950 12 porte', NULL, 5140, 51, 0, 'Switch C2950 12p', 'TCP_Client', 'N3T2010', 'CISCAT295012.png', 3570, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT295012 - Periferica Switch Cisco Catalyst 2950 12 porte', [WSUrlPattern] = NULL, [Order] = 5140, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2950 12p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT295012.png', [GlobalOrder] = 3570, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT295012'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT295024')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT295024', 5, 14, 'CISCAT295024 - Periferica Switch Cisco Catalyst 2950 24 porte', NULL, 5150, 51, 0, 'Switch C2950 24p', 'TCP_Client', 'N3T2010', 'CISCAT295024.png', 3571, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT295024 - Periferica Switch Cisco Catalyst 2950 24 porte', [WSUrlPattern] = NULL, [Order] = 5150, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2950 24p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT295024.png', [GlobalOrder] = 3571, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT295024'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT295048')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT295048', 5, 14, 'CISCAT295048 - Periferica Switch Cisco Catalyst 2950 48 porte', NULL, 5160, 51, 0, 'Switch C2950 48p', 'TCP_Client', 'N3T2010', 'CISCAT295048.png', 3572, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT295048 - Periferica Switch Cisco Catalyst 2950 48 porte', [WSUrlPattern] = NULL, [Order] = 5160, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2950 48p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT295048.png', [GlobalOrder] = 3572, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT295048'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT295512')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT295512', 5, 14, 'CISCAT295512 - Periferica Switch Cisco Catalyst 2955 12 porte', NULL, 5220, 51, 0, 'Switch C2955 12p', 'TCP_Client', 'N3T2010', 'CISCAT295512.png', 3558, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT295512 - Periferica Switch Cisco Catalyst 2955 12 porte', [WSUrlPattern] = NULL, [Order] = 5220, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2955 12p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT295512.png', [GlobalOrder] = 3558, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT295512'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT296008')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT296008', 5, 14, 'CISCAT296008 - Periferica Switch Cisco Catalyst 2960 8 porte', NULL, 5180, 51, 0, 'Switch C2960 8p', 'TCP_Client', 'N3T2010', 'CISCAT296008.png', 3554, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT296008 - Periferica Switch Cisco Catalyst 2960 8 porte', [WSUrlPattern] = NULL, [Order] = 5180, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2960 8p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT296008.png', [GlobalOrder] = 3554, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT296008'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT296024')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT296024', 5, 14, 'CISCAT296024 - Periferica Switch Cisco Catalyst 2960 24 porte', NULL, 5130, 51, 0, 'Switch C2960 24p', 'TCP_Client', 'N3T2010', 'CISCAT296024.png', 3552, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT296024 - Periferica Switch Cisco Catalyst 2960 24 porte', [WSUrlPattern] = NULL, [Order] = 5130, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2960 24p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT296024.png', [GlobalOrder] = 3552, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT296024'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT296048')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT296048', 5, 14, 'CISCAT296048 - Periferica Switch Cisco Catalyst 2960 48 porte', NULL, 5170, 51, 0, 'Switch C2960 48p', 'TCP_Client', 'N3T2010', 'CISCAT296048.png', 3553, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT296048 - Periferica Switch Cisco Catalyst 2960 48 porte', [WSUrlPattern] = NULL, [Order] = 5170, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2960 48p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT296048.png', [GlobalOrder] = 3553, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT296048'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT297024')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT297024', 5, 14, 'CISCAT297024 - Periferica Switch Cisco Catalyst 2970 24 porte', NULL, 5185, 51, 0, 'Switch C2970 24p', 'TCP_Client', 'CPCrfi0R', 'CISCAT000.png', 3559, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT297024 - Periferica Switch Cisco Catalyst 2970 24 porte', [WSUrlPattern] = NULL, [Order] = 5185, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C2970 24p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'CPCrfi0R', [ImageName] = 'CISCAT000.png', [GlobalOrder] = 3559, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT297024'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT3020')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT3020', 5, 14, 'CISCAT3020 - Periferica Switch Cisco Catalyst Bladeswitch 3020', NULL, 5210, 51, 0, 'Switch C3020', 'TCP_Client', 'N3T2010', 'CISCAT3020.png', 3557, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT3020 - Periferica Switch Cisco Catalyst Bladeswitch 3020', [WSUrlPattern] = NULL, [Order] = 5210, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C3020', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT3020.png', [GlobalOrder] = 3557, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT3020'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT355012')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT355012', 5, 14, 'CISCAT355012 - Periferica Switch Cisco Catalyst 3550 12 porte', NULL, 5188, 51, 0, 'Switch C3550 12p', 'TCP_Client', 'CPCrfi0R', 'CISCAT000.png', 3561, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT355012 - Periferica Switch Cisco Catalyst 3550 12 porte', [WSUrlPattern] = NULL, [Order] = 5188, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C3550 12p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'CPCrfi0R', [ImageName] = 'CISCAT000.png', [GlobalOrder] = 3561, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT355012'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT355024')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT355024', 5, 14, 'CISCAT355024 - Periferica Switch Cisco Catalyst 3550 24 porte', NULL, 5187, 51, 0, 'Switch C3550 24p', 'TCP_Client', 'CPCrfi0R', 'CISCAT000.png', 3560, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT355024 - Periferica Switch Cisco Catalyst 3550 24 porte', [WSUrlPattern] = NULL, [Order] = 5187, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C3550 24p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'CPCrfi0R', [ImageName] = 'CISCAT000.png', [GlobalOrder] = 3560, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT355024'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT356024')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT356024', 5, 14, 'CISCAT356024 - Periferica Switch Cisco Catalyst 3560 24 porte', NULL, 5189, 51, 0, 'Switch C3560 24p', 'TCP_Client', 'CPCrfi0R', 'CISCAT000.png', 3561, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT356024 - Periferica Switch Cisco Catalyst 3560 24 porte', [WSUrlPattern] = NULL, [Order] = 5189, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C3560 24p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'CPCrfi0R', [ImageName] = 'CISCAT000.png', [GlobalOrder] = 3561, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT356024'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT356048')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT356048', 5, 14, 'CISCAT356048 - Periferica Switch Cisco Catalyst 3560 48 porte', NULL, 5190, 51, 0, 'Switch C3560 48p', 'TCP_Client', 'N3T2010', 'CISCAT356048.png', 3555, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT356048 - Periferica Switch Cisco Catalyst 3560 48 porte', [WSUrlPattern] = NULL, [Order] = 5190, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C3560 48p', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT356048.png', [GlobalOrder] = 3555, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT356048'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT3750')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT3750', 5, 14, 'CISCAT3750 - Periferica Switch Cisco Catalyst 3750', NULL, 5120, 51, 0, 'Switch C3750', 'TCP_Client', 'N3T2010', 'CISCAT3750.png', 3551, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT3750 - Periferica Switch Cisco Catalyst 3750', [WSUrlPattern] = NULL, [Order] = 5120, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C3750', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT3750.png', [GlobalOrder] = 3551, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT3750'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'CISCAT4500')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('CISCAT4500', 5, 14, 'CISCAT4500 - Periferica Switch Cisco Catalyst 4500', NULL, 5200, 51, 0, 'Switch C4500', 'TCP_Client', 'N3T2010', 'CISCAT4500.png', 3556, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 14, [DeviceTypeDescription] = 'CISCAT4500 - Periferica Switch Cisco Catalyst 4500', [WSUrlPattern] = NULL, [Order] = 5200, [TechnologyID] = 51, [Obsolete] = 0, [DefaultName] = 'Switch C4500', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'CISCAT4500.png', [GlobalOrder] = 3556, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'CISCAT4500'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT00000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT00000', 4, 4, 'Telefin DT00000 - Pannello DTS Standard', NULL, 4010, 11, 0, 'Pannello DTS', 'STLC1000_RS485', NULL, 'DT00000.png', 10, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT00000 - Pannello DTS Standard', [WSUrlPattern] = NULL, [Order] = 4010, [TechnologyID] = 11, [Obsolete] = 0, [DefaultName] = 'Pannello DTS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 10, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT00000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT00L00')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT00L00', 1, 4, 'Telefin DT00L00 - Pannello TDS', NULL, 80, 12, 0, 'Pannello TDS', 'STLC1000_RS485', NULL, 'DT00000.png', 20, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT00L00 - Pannello TDS', [WSUrlPattern] = NULL, [Order] = 80, [TechnologyID] = 12, [Obsolete] = 0, [DefaultName] = 'Pannello TDS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 20, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT00L00'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT00U00')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT00U00', 4, 4, 'Telefin DT00U00 - Pannello Remotizzazione DTS', NULL, 4017, 13, 0, 'Pannello Remotizzazione DTS', 'STLC1000_RS485', NULL, 'DT00000.png', 30, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT00U00 - Pannello Remotizzazione DTS', [WSUrlPattern] = NULL, [Order] = 4017, [TechnologyID] = 13, [Obsolete] = 0, [DefaultName] = 'Pannello Remotizzazione DTS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 30, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT00U00'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT01000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT01000', 4, 4, 'Telefin DT01000 - Pannello DTS /64', NULL, 4015, 14, 0, 'Pannello DTS /64', 'STLC1000_RS485', NULL, 'DT00000.png', 40, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT01000 - Pannello DTS /64', [WSUrlPattern] = NULL, [Order] = 4015, [TechnologyID] = 14, [Obsolete] = 0, [DefaultName] = 'Pannello DTS /64', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 40, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT01000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT01110')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT01110', 4, 4, 'Telefin DT01110 - Pannello Posto Centrale Emergenza', NULL, 4011, 15, 0, 'Pannello Emergenza PC', 'STLC1000_RS485', NULL, 'DT01110.png', 50, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT01110 - Pannello Posto Centrale Emergenza', [WSUrlPattern] = NULL, [Order] = 4011, [TechnologyID] = 15, [Obsolete] = 0, [DefaultName] = 'Pannello Emergenza PC', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT01110.png', [GlobalOrder] = 50, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT01110'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT01210')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT01210', 4, 4, 'Telefin DT01210 - Pannello Periferico Emergenza', NULL, 4012, 16, 0, 'Pannello Emergenza', 'STLC1000_RS485', NULL, 'DT01110.png', 60, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT01210 - Pannello Periferico Emergenza', [WSUrlPattern] = NULL, [Order] = 4012, [TechnologyID] = 16, [Obsolete] = 0, [DefaultName] = 'Pannello Emergenza', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT01110.png', [GlobalOrder] = 60, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT01210'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT01210-DS')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT01210-DS', 4, 4, 'Telefin DT01210-DS - Pannello Emergenza solo DS', NULL, 4014, 17, 0, 'Pannello Emergenza DS', 'STLC1000_RS485', NULL, 'DT01110.png', 70, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT01210-DS - Pannello Emergenza solo DS', [WSUrlPattern] = NULL, [Order] = 4014, [TechnologyID] = 17, [Obsolete] = 0, [DefaultName] = 'Pannello Emergenza DS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT01110.png', [GlobalOrder] = 70, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT01210-DS'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT01700')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT01700', 4, 4, 'Telefin DT01700 - Pannello DTS /16', NULL, 4013, 18, 0, 'Pannello DTS /16', 'STLC1000_RS485', NULL, 'DT00000.png', 80, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT01700 - Pannello DTS /16', [WSUrlPattern] = NULL, [Order] = 4013, [TechnologyID] = 18, [Obsolete] = 0, [DefaultName] = 'Pannello DTS /16', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 80, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT01700'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT02000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT02000', 99, 4, 'Telefin DT02000 - Pannello Gestione 5 Ingressi', NULL, 99000, 19, 0, 'Scheda 5 ingressi', 'STLC1000_RS485', NULL, 'DT00000.png', 90, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 99, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT02000 - Pannello Gestione 5 Ingressi', [WSUrlPattern] = NULL, [Order] = 99000, [TechnologyID] = 19, [Obsolete] = 0, [DefaultName] = 'Scheda 5 ingressi', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 90, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT02000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT04000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT04000', 1, 4, 'Telefin DT04000 - Amplificatore', NULL, 20, 20, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'DT04000.png', 100, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT04000 - Amplificatore', [WSUrlPattern] = NULL, [Order] = 20, [TechnologyID] = 20, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT04000.png', [GlobalOrder] = 100, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT04000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT05000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT05000', 4, 4, 'Telefin DT05000 - Pannello DTS Standard', NULL, 4016, 21, 0, 'Pannello DTS', 'STLC1000_RS485', NULL, 'DT00000.png', 110, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT05000 - Pannello DTS Standard', [WSUrlPattern] = NULL, [Order] = 4016, [TechnologyID] = 21, [Obsolete] = 0, [DefaultName] = 'Pannello DTS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 110, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT05000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT05B10')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT05B10', 1, 4, 'Telefin DT05B10 - Pannello TX TDS', NULL, 90, 22, 0, 'Pannello TX TDS', 'STLC1000_RS485', NULL, 'DT00000.png', 120, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT05B10 - Pannello TX TDS', [WSUrlPattern] = NULL, [Order] = 90, [TechnologyID] = 22, [Obsolete] = 0, [DefaultName] = 'Pannello TX TDS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 120, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT05B10'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT05BC0')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT05BC0', 1, 4, 'Telefin DT05BC0 - Pannello TX TDS', NULL, 100, 23, 0, 'Pannello TX TDS', 'STLC1000_RS485', NULL, 'DT00000.png', 130, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT05BC0 - Pannello TX TDS', [WSUrlPattern] = NULL, [Order] = 100, [TechnologyID] = 23, [Obsolete] = 0, [DefaultName] = 'Pannello TX TDS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 130, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT05BC0'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT05L00')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT05L00', 1, 4, 'Telefin DT05L00 - Pannello TDS', NULL, 60, 24, 0, 'Pannello TDS', 'STLC1000_RS485', NULL, 'DT00000.png', 140, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT05L00 - Pannello TDS', [WSUrlPattern] = NULL, [Order] = 60, [TechnologyID] = 24, [Obsolete] = 0, [DefaultName] = 'Pannello TDS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 140, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT05L00'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT05L10')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT05L10', 1, 4, 'Telefin DT05L10 - Pannello TDS', NULL, 70, 25, 0, 'Pannello TDS', 'STLC1000_RS485', NULL, 'DT00000.png', 150, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT05L10 - Pannello TDS', [WSUrlPattern] = NULL, [Order] = 70, [TechnologyID] = 25, [Obsolete] = 0, [DefaultName] = 'Pannello TDS', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT00000.png', [GlobalOrder] = 150, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT05L10'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT13000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT13000', 1, 4, 'Telefin DT13000 - Sistema Alimentazione', NULL, 10, 26, 0, 'Sistema Alimentazione', 'STLC1000_RS485', NULL, 'DT13000.png', 160, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT13000 - Sistema Alimentazione', [WSUrlPattern] = NULL, [Order] = 10, [TechnologyID] = 26, [Obsolete] = 0, [DefaultName] = 'Sistema Alimentazione', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT13000.png', [GlobalOrder] = 160, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT13000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'DT13100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('DT13100', 1, 4, 'Telefin DT13100 - Sistema Alimentazione', NULL, 11, 52, 0, 'Sistema Alimentazione', 'STLC1000_RS485', NULL, 'DT13100.png', 3557, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin DT13100 - Sistema Alimentazione', [WSUrlPattern] = NULL, [Order] = 11, [TechnologyID] = 52, [Obsolete] = 0, [DefaultName] = 'Sistema Alimentazione', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'DT13100.png', [GlobalOrder] = 3557, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'DT13100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'ELETECH000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('ELETECH000', 5, 9, 'Eletech ELETECH000 - Periferica Generica', NULL, 5390, 46, 0, 'WebRadio', 'TCP_Client', 'public', 'ELETECH000.png', 3100, 1, 2)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 9, [DeviceTypeDescription] = 'Eletech ELETECH000 - Periferica Generica', [WSUrlPattern] = NULL, [Order] = 5390, [TechnologyID] = 46, [Obsolete] = 0, [DefaultName] = 'WebRadio', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'ELETECH000.png', [GlobalOrder] = 3100, [ExportToList] = 1, [DiscoveryType] = 2 WHERE [DeviceTypeID] = 'ELETECH000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'ETAUDIO2IP100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('ETAUDIO2IP100', 5, 9, 'Eletech Audio 2 IP', NULL, 5410, 46, 0, 'WebRadio', 'TCP_Client', 'public', 'ELETECH000.png', 3102, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 9, [DeviceTypeDescription] = 'Eletech Audio 2 IP', [WSUrlPattern] = NULL, [Order] = 5410, [TechnologyID] = 46, [Obsolete] = 0, [DefaultName] = 'WebRadio', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'ELETECH000.png', [GlobalOrder] = 3102, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'ETAUDIO2IP100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'ETIP2AUDIO100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('ETIP2AUDIO100', 5, 9, 'Eletech Audio 2 IP', NULL, 5420, 46, 0, 'WebRadio', 'TCP_Client', 'public', 'ELETECH000.png', 3103, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 9, [DeviceTypeDescription] = 'Eletech Audio 2 IP', [WSUrlPattern] = NULL, [Order] = 5420, [TechnologyID] = 46, [Obsolete] = 0, [DefaultName] = 'WebRadio', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'ELETECH000.png', [GlobalOrder] = 3103, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'ETIP2AUDIO100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'ETRADIO100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('ETRADIO100', 5, 9, 'Eletech Web Radio', NULL, 5400, 46, 0, 'WebRadio', 'TCP_Client', 'public', 'ELETECH000.png', 3101, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 9, [DeviceTypeDescription] = 'Eletech Web Radio', [WSUrlPattern] = NULL, [Order] = 5400, [TechnologyID] = 46, [Obsolete] = 0, [DefaultName] = 'WebRadio', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'ELETECH000.png', [GlobalOrder] = 3101, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'ETRADIO100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'ETWEBAIO')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('ETWEBAIO', 11, 22, 'Web Alarm I/O', NULL, 99051, 67, 0, 'Web Alarm I/O', 'STLC1000_RS485', NULL, 'ETWEBAIO.png', 4118, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 11, [VendorID] = 22, [DeviceTypeDescription] = 'Web Alarm I/O', [WSUrlPattern] = NULL, [Order] = 99051, [TechnologyID] = 67, [Obsolete] = 0, [DefaultName] = 'Web Alarm I/O', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'ETWEBAIO.png', [GlobalOrder] = 4118, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'ETWEBAIO'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000L-NL')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000L-NL', 8, 4, 'Telefin FD90000L-NL - Sistema FDS Light Normale Locale', NULL, 8040, 55, 0, 'Sistema FDS Light Normale Locale', 'STLC1000_RS485', NULL, 'FD90000.png', 171, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000L-NL - Sistema FDS Light Normale Locale', [WSUrlPattern] = NULL, [Order] = 8040, [TechnologyID] = 55, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Light Normale Locale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 171, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000L-NL'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000L-NR')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000L-NR', 8, 4, 'Telefin FD90000L-NR - Sistema FDS Light Normale Remoto', NULL, 8050, 56, 0, 'Sistema FDS Light Normale Remoto', 'STLC1000_RS485', NULL, 'FD90000.png', 191, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000L-NR - Sistema FDS Light Normale Remoto', [WSUrlPattern] = NULL, [Order] = 8050, [TechnologyID] = 56, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Light Normale Remoto', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 191, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000L-NR'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000L-RL')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000L-RL', 8, 4, 'Telefin FD90000L-RL - Sistema FDS Light Riserva Locale', NULL, 8060, 57, 0, 'Sistema FDS Light Riserva Locale', 'STLC1000_RS485', NULL, 'FD90000.png', 181, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000L-RL - Sistema FDS Light Riserva Locale', [WSUrlPattern] = NULL, [Order] = 8060, [TechnologyID] = 57, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Light Riserva Locale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 181, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000L-RL'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000L-RR')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000L-RR', 8, 4, 'Telefin FD90000L-RR - Sistema FDS Light Riserva Remoto', NULL, 8070, 58, 0, 'Sistema FDS Light Riserva Remoto', 'STLC1000_RS485', NULL, 'FD90000.png', 201, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000L-RR - Sistema FDS Light Riserva Remoto', [WSUrlPattern] = NULL, [Order] = 8070, [TechnologyID] = 58, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Light Riserva Remoto', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 201, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000L-RR'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000-NL')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000-NL', 8, 4, 'Telefin FD90000-NL - Sistema FDS Normale Locale', NULL, 8000, 27, 0, 'Sistema FDS Normale Locale', 'STLC1000_RS485', NULL, 'FD90000.png', 170, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000-NL - Sistema FDS Normale Locale', [WSUrlPattern] = NULL, [Order] = 8000, [TechnologyID] = 27, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Normale Locale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 170, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000-NL'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000-NR')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000-NR', 8, 4, 'Telefin FD90000-NR - Sistema FDS Normale Remoto', NULL, 8010, 28, 0, 'Sistema FDS Normale Remoto', 'STLC1000_RS485', NULL, 'FD90000.png', 190, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000-NR - Sistema FDS Normale Remoto', [WSUrlPattern] = NULL, [Order] = 8010, [TechnologyID] = 28, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Normale Remoto', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 190, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000-NR'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000-RL')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000-RL', 8, 4, 'Telefin FD90000-RL - Sistema FDS Riserva Locale', NULL, 8020, 29, 0, 'Sistema FDS Riserva Locale', 'STLC1000_RS485', NULL, 'FD90000.png', 180, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000-RL - Sistema FDS Riserva Locale', [WSUrlPattern] = NULL, [Order] = 8020, [TechnologyID] = 29, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Riserva Locale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 180, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000-RL'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'FD90000-RR')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('FD90000-RR', 8, 4, 'Telefin FD90000-RR - Sistema FDS Riserva Remoto', NULL, 8030, 30, 0, 'Sistema FDS Riserva Remoto', 'STLC1000_RS485', NULL, 'FD90000.png', 200, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 8, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin FD90000-RR - Sistema FDS Riserva Remoto', [WSUrlPattern] = NULL, [Order] = 8030, [TechnologyID] = 30, [Obsolete] = 0, [DefaultName] = 'Sistema FDS Riserva Remoto', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'FD90000.png', [GlobalOrder] = 200, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'FD90000-RR'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'GPIO3006')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('GPIO3006', 99, 4, 'Telefin GPIO3006 - Scheda I/O Generica', NULL, 99010, 31, 0, 'Scheda I/O', 'STLC1000_RS485', NULL, 'GPIO3006.png', 210, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 99, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin GPIO3006 - Scheda I/O Generica', [WSUrlPattern] = NULL, [Order] = 99010, [TechnologyID] = 31, [Obsolete] = 0, [DefaultName] = 'Scheda I/O', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'GPIO3006.png', [GlobalOrder] = 210, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'GPIO3006'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HA01000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HA01000', 4, 4, 'Telefin HA01000 - Sistema STSI', NULL, 4019, 41, 0, 'Sistema STSI', 'STLC1000_RS485', NULL, 'HA01000.png', 235, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin HA01000 - Sistema STSI', [WSUrlPattern] = NULL, [Order] = 4019, [TechnologyID] = 41, [Obsolete] = 0, [DefaultName] = 'Sistema STSI', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'HA01000.png', [GlobalOrder] = 235, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'HA01000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HA01510')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HA01510', 4, 4, 'Telefin HA01510 - Sistema STSI', NULL, 4020, 41, 0, 'Sistema STSI', 'STLC1000_RS485', NULL, 'HA01000.png', 236, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin HA01510 - Sistema STSI', [WSUrlPattern] = NULL, [Order] = 4020, [TechnologyID] = 41, [Obsolete] = 0, [DefaultName] = 'Sistema STSI', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'HA01000.png', [GlobalOrder] = 236, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'HA01510'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HD05000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HD05000', 4, 4, 'Telefin HD05000 - Sistema STSI', NULL, 4018, 41, 0, 'Sistema STSI', 'STLC1000_RS485', NULL, 'HA01000.png', 237, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin HD05000 - Sistema STSI', [WSUrlPattern] = NULL, [Order] = 4018, [TechnologyID] = 41, [Obsolete] = 0, [DefaultName] = 'Sistema STSI', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'HA01000.png', [GlobalOrder] = 237, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'HD05000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HPPCUR000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HPPCUR000', 5, 10, 'HPPCUR000 - Periferica HP Generica', NULL, 5020, 47, 0, 'HP ProCurve', 'TCP_Client', 'public', 'HPPCUR000.png', 3500, 1, 2)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 10, [DeviceTypeDescription] = 'HPPCUR000 - Periferica HP Generica', [WSUrlPattern] = NULL, [Order] = 5020, [TechnologyID] = 47, [Obsolete] = 0, [DefaultName] = 'HP ProCurve', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'HPPCUR000.png', [GlobalOrder] = 3500, [ExportToList] = 1, [DiscoveryType] = 2 WHERE [DeviceTypeID] = 'HPPCUR000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HPPCUR5304')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HPPCUR5304', 5, 10, 'HPPCUR5304 - Periferica HP ProCurve 5304XL', NULL, 5030, 47, 0, 'HP ProCurve', 'TCP_Client', 'public', 'HPPCUR5304.png', 3501, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 10, [DeviceTypeDescription] = 'HPPCUR5304 - Periferica HP ProCurve 5304XL', [WSUrlPattern] = NULL, [Order] = 5030, [TechnologyID] = 47, [Obsolete] = 0, [DefaultName] = 'HP ProCurve', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'HPPCUR5304.png', [GlobalOrder] = 3501, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'HPPCUR5304'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HPPCUR5308')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HPPCUR5308', 5, 10, 'HPPCUR5308 - Periferica HP ProCurve 5308XL', NULL, 5040, 47, 0, 'HP ProCurve', 'TCP_Client', 'public', 'HPPCUR5308.png', 3502, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 10, [DeviceTypeDescription] = 'HPPCUR5308 - Periferica HP ProCurve 5308XL', [WSUrlPattern] = NULL, [Order] = 5040, [TechnologyID] = 47, [Obsolete] = 0, [DefaultName] = 'HP ProCurve', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'HPPCUR5308.png', [GlobalOrder] = 3502, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'HPPCUR5308'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HWDAM1208')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HWDAM1208', 11, 23, 'HWDAM1208 - Damocles 1208: I/O over Ethernet', NULL, 99100, 69, 0, 'Damocles 1208', 'TCP_Client', 'public', 'HWDAM1208.png', 4119, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 11, [VendorID] = 23, [DeviceTypeDescription] = 'HWDAM1208 - Damocles 1208: I/O over Ethernet', [WSUrlPattern] = NULL, [Order] = 99100, [TechnologyID] = 69, [Obsolete] = 0, [DefaultName] = 'Damocles 1208', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'HWDAM1208.png', [GlobalOrder] = 4119, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'HWDAM1208'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'HWPOS3468')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('HWPOS3468', 11, 23, 'HWPOS3468 - Poseidon2 3468: IP thermostat with 230V/16A-rated relay outputs', NULL, 99110, 69, 0, 'Poseidon 3468', 'TCP_Client', 'public', 'HWPOS3468.png', 4120, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 11, [VendorID] = 23, [DeviceTypeDescription] = 'HWPOS3468 - Poseidon2 3468: IP thermostat with 230V/16A-rated relay outputs', [WSUrlPattern] = NULL, [Order] = 99110, [TechnologyID] = 69, [Obsolete] = 0, [DefaultName] = 'Poseidon 3468', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'HWPOS3468.png', [GlobalOrder] = 4120, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'HWPOS3468'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'INFSTAZ1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('INFSTAZ1', 2, 8, 'INFSTAZ1 - Monitor InfoStazioni', NULL, 2050, 2, 0, 'Monitor', 'TCP_Client', 'public', 'INFSTAZ1.png', 1020, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 8, [DeviceTypeDescription] = 'INFSTAZ1 - Monitor InfoStazioni', [WSUrlPattern] = NULL, [Order] = 2050, [TechnologyID] = 2, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'INFSTAZ1.png', [GlobalOrder] = 1020, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'INFSTAZ1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'MGGA5000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('MGGA5000', 6, 7, 'MGE MGGA5000 - Gruppo di Continuita` MGE Galaxy 5000', NULL, 6020, NULL, 0, 'Gruppo di Continuita`', 'TCP_Client', 'public', 'MGGA5000.png', 980, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 6, [VendorID] = 7, [DeviceTypeDescription] = 'MGE MGGA5000 - Gruppo di Continuita` MGE Galaxy 5000', [WSUrlPattern] = NULL, [Order] = 6020, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Gruppo di Continuita`', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'MGGA5000.png', [GlobalOrder] = 980, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'MGGA5000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'MGUPS000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('MGUPS000', 6, 7, 'MGE MGUPS000 - Gruppo di Continuita` MGE', NULL, 6030, NULL, 0, 'Gruppo di Continuita`', 'TCP_Client', 'public', 'MGUPS000.png', 981, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 6, [VendorID] = 7, [DeviceTypeDescription] = 'MGE MGUPS000 - Gruppo di Continuita` MGE', [WSUrlPattern] = NULL, [Order] = 6030, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Gruppo di Continuita`', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'MGUPS000.png', [GlobalOrder] = 981, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'MGUPS000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'MULCDTS2048')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('MULCDTS2048', 5, 13, 'MULCDTS2048 - Periferica MuLogic CDTS-2048', NULL, 5100, 50, 0, 'MuLogic CDTS', 'TCP_Client', 'public', 'MULCDTS2048.png', 3531, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 13, [DeviceTypeDescription] = 'MULCDTS2048 - Periferica MuLogic CDTS-2048', [WSUrlPattern] = NULL, [Order] = 5100, [TechnologyID] = 50, [Obsolete] = 0, [DefaultName] = 'MuLogic CDTS', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'MULCDTS2048.png', [GlobalOrder] = 3531, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'MULCDTS2048'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'MULO000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('MULO000', 5, 13, 'MULO000 - Periferica MuLogic Generica', NULL, 5090, 50, 0, 'MuLogic Device', 'TCP_Client', 'public', 'MULO000.png', 3530, 1, 2)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 13, [DeviceTypeDescription] = 'MULO000 - Periferica MuLogic Generica', [WSUrlPattern] = NULL, [Order] = 5090, [TechnologyID] = 50, [Obsolete] = 0, [DefaultName] = 'MuLogic Device', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'MULO000.png', [GlobalOrder] = 3530, [ExportToList] = 1, [DiscoveryType] = 2 WHERE [DeviceTypeID] = 'MULO000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'NETMIB200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('NETMIB200', 5, 24, 'NETMIB200 - Periferica di Rete Generica', NULL, 5112, 51, 1, 'Periferica di Rete Generica', 'TCP_Client', 'N3T2010', 'NETMIB200.png', 3601, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 24, [DeviceTypeDescription] = 'NETMIB200 - Periferica di Rete Generica', [WSUrlPattern] = NULL, [Order] = 5112, [TechnologyID] = 51, [Obsolete] = 1, [DefaultName] = 'Periferica di Rete Generica', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'NETMIB200.png', [GlobalOrder] = 3601, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'NETMIB200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PATTON000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PATTON000', 5, 16, 'PATTON000 - Periferica Patton Generica', NULL, 5490, 54, 0, 'Periferica Patton', 'TCP_Client', 'N3T2010', 'PATTON000.png', 3880, 1, 2)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 16, [DeviceTypeDescription] = 'PATTON000 - Periferica Patton Generica', [WSUrlPattern] = NULL, [Order] = 5490, [TechnologyID] = 54, [Obsolete] = 0, [DefaultName] = 'Periferica Patton', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'PATTON000.png', [GlobalOrder] = 3880, [ExportToList] = 1, [DiscoveryType] = 2 WHERE [DeviceTypeID] = 'PATTON000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PATTON2603')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PATTON2603', 5, 16, 'PATTON2603 - Periferica router Patton 2603 alta velocità T1/E1', NULL, 5500, 54, 0, 'Patton 2603', 'TCP_Client', 'N3T2010', 'PATTON2603.png', 3882, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 16, [DeviceTypeDescription] = 'PATTON2603 - Periferica router Patton 2603 alta velocità T1/E1', [WSUrlPattern] = NULL, [Order] = 5500, [TechnologyID] = 54, [Obsolete] = 0, [DefaultName] = 'Patton 2603', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'PATTON2603.png', [GlobalOrder] = 3882, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'PATTON2603'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PATTON3201')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PATTON3201', 5, 16, 'PATTON3201 - Periferica router Patton 3201 alta velocità G.SHDSL', NULL, 5510, 54, 0, 'Patton 3201', 'TCP_Client', 'N3T2010', 'PATTON3201.png', 3884, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 16, [DeviceTypeDescription] = 'PATTON3201 - Periferica router Patton 3201 alta velocità G.SHDSL', [WSUrlPattern] = NULL, [Order] = 5510, [TechnologyID] = 54, [Obsolete] = 0, [DefaultName] = 'Patton 3201', [PortType] = 'TCP_Client', [SnmpCommunity] = 'N3T2010', [ImageName] = 'PATTON3201.png', [GlobalOrder] = 3884, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'PATTON3201'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PB75085')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PB75085', 1, 4, 'Telefin PB75085 - Amplificatore VoIP (IeC)', NULL, 21, 68, 0, 'Amplificatore VoIP', 'TCP_Client', 'public', 'PB75085.png', 101, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin PB75085 - Amplificatore VoIP (IeC)', [WSUrlPattern] = NULL, [Order] = 21, [TechnologyID] = 68, [Obsolete] = 0, [DefaultName] = 'Amplificatore VoIP', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'PB75085.png', [GlobalOrder] = 101, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PB75085'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUBA100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUBA100', 1, 5, 'Prase PEAUBA100 - Diffusione Sonora Audia - Amplificatore 1 Barix', NULL, 260, 6, 0, 'Amplificatore 1', 'TCP_Client', 'public', 'PEAUBA001.png', 530, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUBA100 - Diffusione Sonora Audia - Amplificatore 1 Barix', [WSUrlPattern] = NULL, [Order] = 260, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore 1', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'PEAUBA001.png', [GlobalOrder] = 530, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUBA100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUBA200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUBA200', 1, 5, 'Prase PEAUBA200 - Diffusione Sonora Audia - Amplificatore 2 Barix', NULL, 270, 6, 0, 'Amplificatore 2', 'TCP_Client', 'public', 'PEAUBA002.png', 540, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUBA200 - Diffusione Sonora Audia - Amplificatore 2 Barix', [WSUrlPattern] = NULL, [Order] = 270, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore 2', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'PEAUBA002.png', [GlobalOrder] = 540, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUBA200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUBC105')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUBC105', 1, 5, 'Prase PEAUBC105 - Diffusione Sonora Audia - Unita` Centrale 1 Barix (+5 input)', NULL, 250, 6, 0, 'Unita` Centrale', 'TCP_Client', 'public', 'PEAUBC001.png', 520, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUBC105 - Diffusione Sonora Audia - Unita` Centrale 1 Barix (+5 input)', [WSUrlPattern] = NULL, [Order] = 250, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'PEAUBC001.png', [GlobalOrder] = 520, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUBC105'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA100', 1, 5, 'Prase PEAUGA100 - Diffusione Sonora Audia - Amplificatore 1', NULL, 150, 7, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAUGA100.png', 420, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA100 - Diffusione Sonora Audia - Amplificatore 1', [WSUrlPattern] = NULL, [Order] = 150, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGA100.png', [GlobalOrder] = 420, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA101')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA101', 1, 5, 'Prase PEAUGA101 - Amplificatore Fusion 1', NULL, 690, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3553, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA101 - Amplificatore Fusion 1', [WSUrlPattern] = NULL, [Order] = 690, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3553, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA101'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA102')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA102', 1, 5, 'Prase PEAUGA102 - Amplificatore Fusion 1 (3 ingressi)', NULL, 740, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3558, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA102 - Amplificatore Fusion 1 (3 ingressi)', [WSUrlPattern] = NULL, [Order] = 740, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3558, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA102'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA200', 1, 5, 'Prase PEAUGA200 - Diffusione Sonora Audia - Amplificatore 2', NULL, 160, 7, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAUGA200.png', 430, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA200 - Diffusione Sonora Audia - Amplificatore 2', [WSUrlPattern] = NULL, [Order] = 160, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGA200.png', [GlobalOrder] = 430, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA201')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA201', 1, 5, 'Prase PEAUGA201 - Amplificatore Fusion 2', NULL, 700, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3554, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA201 - Amplificatore Fusion 2', [WSUrlPattern] = NULL, [Order] = 700, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3554, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA201'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA202')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA202', 1, 5, 'Prase PEAUGA202 - Amplificatore Fusion 2 (3 ingressi)', NULL, 750, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3559, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA202 - Amplificatore Fusion 2 (3 ingressi)', [WSUrlPattern] = NULL, [Order] = 750, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3559, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA202'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA300')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA300', 1, 5, 'Prase PEAUGA300 - Diffusione Sonora Audia - Amplificatore 3', NULL, 170, 7, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAUGA300.png', 440, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA300 - Diffusione Sonora Audia - Amplificatore 3', [WSUrlPattern] = NULL, [Order] = 170, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGA300.png', [GlobalOrder] = 440, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA300'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA302')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA302', 1, 5, 'Prase PEAUGA302 - Amplificatore Fusion 3 (3 ingressi)', NULL, 760, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3560, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA302 - Amplificatore Fusion 3 (3 ingressi)', [WSUrlPattern] = NULL, [Order] = 760, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3560, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA302'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA303')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA303', 1, 5, 'Prase PEAUGA303 - Amplificatore Fusion 3 normale', NULL, 710, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3555, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA303 - Amplificatore Fusion 3 normale', [WSUrlPattern] = NULL, [Order] = 710, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3555, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA303'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA400')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA400', 1, 5, 'Prase PEAUGA400 - Diffusione Sonora Audia - Amplificatore 4', NULL, 180, 7, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAUGA400.png', 450, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA400 - Diffusione Sonora Audia - Amplificatore 4', [WSUrlPattern] = NULL, [Order] = 180, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGA400.png', [GlobalOrder] = 450, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA400'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA402')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA402', 1, 5, 'Prase PEAUGA402 - Amplificatore Fusion 4 (3 ingressi)', NULL, 770, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3561, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA402 - Amplificatore Fusion 4 (3 ingressi)', [WSUrlPattern] = NULL, [Order] = 770, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3561, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA402'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA403')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA403', 1, 5, 'Prase PEAUGA403 - Amplificatore Fusion 4 riserva', NULL, 720, 6, 0, 'Amplificatore Fusion', 'STLC1000_RS485', NULL, 'PEAUGAXXX.png', 3556, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA403 - Amplificatore Fusion 4 riserva', [WSUrlPattern] = NULL, [Order] = 720, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore Fusion', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGAXXX.png', [GlobalOrder] = 3556, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA403'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGA500')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGA500', 1, 5, 'Prase PEAUGA500 - Diffusione Sonora Audia - Amplificatore 5', NULL, 190, 7, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAUGA500.png', 460, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGA500 - Diffusione Sonora Audia - Amplificatore 5', [WSUrlPattern] = NULL, [Order] = 190, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGA500.png', [GlobalOrder] = 460, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGA500'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGC100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGC100', 1, 5, 'Prase PEAUGC100 - Diffusione Sonora Audia - Unita` Centrale 1 (no input aggiuntivi)', NULL, 110, 6, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAUGC100.png', 380, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGC100 - Diffusione Sonora Audia - Unita` Centrale 1 (no input aggiuntivi)', [WSUrlPattern] = NULL, [Order] = 110, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGC100.png', [GlobalOrder] = 380, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGC100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGC101')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGC101', 1, 5, 'Prase PEAUGC101 - Diffusione Sonora Audia - Unita` Centrale 1 (+1 input aggiuntivo)', NULL, 120, 7, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAUGC101.png', 390, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGC101 - Diffusione Sonora Audia - Unita` Centrale 1 (+1 input aggiuntivo)', [WSUrlPattern] = NULL, [Order] = 120, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGC101.png', [GlobalOrder] = 390, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGC101'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGC105')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGC105', 1, 5, 'Prase PEAUGC105 - Diffusione Sonora Audia - Unita` Centrale 1 (+5 input aggiuntivi)', NULL, 130, 7, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAUGC105.png', 400, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGC105 - Diffusione Sonora Audia - Unita` Centrale 1 (+5 input aggiuntivi)', [WSUrlPattern] = NULL, [Order] = 130, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGC105.png', [GlobalOrder] = 400, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGC105'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGC200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGC200', 1, 5, 'Prase PEAUGC200 - Diffusione Sonora Audia - Unita` Centrale 2', NULL, 140, 7, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAUGC200.png', 410, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGC200 - Diffusione Sonora Audia - Unita` Centrale 2', [WSUrlPattern] = NULL, [Order] = 140, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGC200.png', [GlobalOrder] = 410, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGC200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGM101')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGM101', 1, 5, 'Prase PEAUGM101 - Diffusione Sonora Audia - Base microfonica 1', NULL, 200, 7, 0, 'Base microfonica', 'STLC1000_RS485', NULL, 'PEAUGM101.png', 470, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGM101 - Diffusione Sonora Audia - Base microfonica 1', [WSUrlPattern] = NULL, [Order] = 200, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGM101.png', [GlobalOrder] = 470, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGM101'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGM201')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGM201', 1, 5, 'Prase PEAUGM201 - Diffusione Sonora Audia - Base microfonica 2', NULL, 210, 7, 0, 'Base microfonica', 'STLC1000_RS485', NULL, 'PEAUGM201.png', 480, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGM201 - Diffusione Sonora Audia - Base microfonica 2', [WSUrlPattern] = NULL, [Order] = 210, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGM201.png', [GlobalOrder] = 480, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGM201'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGM301')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGM301', 1, 5, 'Prase PEAUGM301 - Diffusione Sonora Audia - Base microfonica 3', NULL, 220, 7, 0, 'Base microfonica', 'STLC1000_RS485', NULL, 'PEAUGM301.png', 490, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGM301 - Diffusione Sonora Audia - Base microfonica 3', [WSUrlPattern] = NULL, [Order] = 220, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGM301.png', [GlobalOrder] = 490, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGM301'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGM401')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGM401', 1, 5, 'Prase PEAUGM401 - Diffusione Sonora Audia - Base microfonica 4', NULL, 230, 7, 0, 'Base microfonica', 'STLC1000_RS485', NULL, 'PEAUGM401.png', 500, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGM401 - Diffusione Sonora Audia - Base microfonica 4', [WSUrlPattern] = NULL, [Order] = 230, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGM401.png', [GlobalOrder] = 500, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGM401'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUGM501')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUGM501', 1, 5, 'Prase PEAUGM501 - Diffusione Sonora Audia - Base microfonica 5', NULL, 240, 7, 0, 'Base microfonica', 'STLC1000_RS485', NULL, 'PEAUGM501.png', 510, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUGM501 - Diffusione Sonora Audia - Base microfonica 5', [WSUrlPattern] = NULL, [Order] = 240, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAUGM501.png', [GlobalOrder] = 510, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUGM501'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTA100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTA100', 1, 5, 'Prase PEAUTA100 - Diffusione Sonora Audia - Amplificatore 1', NULL, 1006, 6, 0, 'Amplificatore', 'TCP_Client', NULL, 'PEAUTA100.png', 2055, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTA100 - Diffusione Sonora Audia - Amplificatore 1', [WSUrlPattern] = NULL, [Order] = 1006, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTA100.png', [GlobalOrder] = 2055, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTA100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTA200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTA200', 1, 5, 'Prase PEAUTA200 - Diffusione Sonora Audia - Amplificatore 2', NULL, 1007, 6, 0, 'Amplificatore', 'TCP_Client', NULL, 'PEAUTA200.png', 2056, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTA200 - Diffusione Sonora Audia - Amplificatore 2', [WSUrlPattern] = NULL, [Order] = 1007, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTA200.png', [GlobalOrder] = 2056, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTA200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTA300')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTA300', 1, 5, 'Prase PEAUTA300 - Diffusione Sonora Audia - Amplificatore 3', NULL, 1008, 6, 0, 'Amplificatore', 'TCP_Client', NULL, 'PEAUTA300.png', 2057, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTA300 - Diffusione Sonora Audia - Amplificatore 3', [WSUrlPattern] = NULL, [Order] = 1008, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTA300.png', [GlobalOrder] = 2057, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTA300'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTA400')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTA400', 1, 5, 'Prase PEAUTA400 - Diffusione Sonora Audia - Amplificatore 4', NULL, 1009, 6, 0, 'Amplificatore', 'TCP_Client', NULL, 'PEAUTA400.png', 2058, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTA400 - Diffusione Sonora Audia - Amplificatore 4', [WSUrlPattern] = NULL, [Order] = 1009, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTA400.png', [GlobalOrder] = 2058, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTA400'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTA500')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTA500', 1, 5, 'Prase PEAUTA500 - Diffusione Sonora Audia - Amplificatore 5', NULL, 1010, 6, 0, 'Amplificatore', 'TCP_Client', NULL, 'PEAUTA500.png', 2059, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTA500 - Diffusione Sonora Audia - Amplificatore 5', [WSUrlPattern] = NULL, [Order] = 1010, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTA500.png', [GlobalOrder] = 2059, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTA500'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTC100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTC100', 1, 5, 'Prase PEAUTC100 - Diffusione Sonora Audia - Unita` centrale 1', NULL, 1001, 6, 0, 'Unita` centrale', 'TCP_Client', NULL, 'PEAUTC100.png', 2050, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTC100 - Diffusione Sonora Audia - Unita` centrale 1', [WSUrlPattern] = NULL, [Order] = 1001, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` centrale', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTC100.png', [GlobalOrder] = 2050, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTC100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTC200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTC200', 1, 5, 'Prase PEAUTC200 - Diffusione Sonora Audia - Unita` centrale 2', NULL, 1002, 6, 0, 'Unita` centrale', 'TCP_Client', NULL, 'PEAUTC200.png', 2051, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTC200 - Diffusione Sonora Audia - Unita` centrale 2', [WSUrlPattern] = NULL, [Order] = 1002, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` centrale', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTC200.png', [GlobalOrder] = 2051, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTC200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTM100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTM100', 1, 5, 'Prase PEAUTM100 - Diffusione Sonora Audia - Base microfonica 1', NULL, 1003, 6, 0, 'Base microfonica', 'TCP_Client', NULL, 'PEAUTM100.png', 2052, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTM100 - Diffusione Sonora Audia - Base microfonica 1', [WSUrlPattern] = NULL, [Order] = 1003, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTM100.png', [GlobalOrder] = 2052, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTM100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTM200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTM200', 1, 5, 'Prase PEAUTM200 - Diffusione Sonora Audia - Base microfonica 2', NULL, 1004, 6, 0, 'Base microfonica', 'TCP_Client', NULL, 'PEAUTM200.png', 2053, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTM200 - Diffusione Sonora Audia - Base microfonica 2', [WSUrlPattern] = NULL, [Order] = 1004, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTM200.png', [GlobalOrder] = 2053, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTM200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAUTM300')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAUTM300', 1, 5, 'Prase PEAUTM300 - Diffusione Sonora Audia - Base microfonica 3', NULL, 1005, 6, 0, 'Base microfonica', 'TCP_Client', NULL, 'PEAUTM300.png', 2054, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAUTM300 - Diffusione Sonora Audia - Base microfonica 3', [WSUrlPattern] = NULL, [Order] = 1005, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Base microfonica', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'PEAUTM300.png', [GlobalOrder] = 2054, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAUTM300'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPA')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPA', 1, 5, 'Prase PEAVAMPA - Diffusione Sonora AV Digital - Amplificatori 1 e 2', NULL, 820, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPA.png', 2004, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPA - Diffusione Sonora AV Digital - Amplificatori 1 e 2', [WSUrlPattern] = NULL, [Order] = 820, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPA.png', [GlobalOrder] = 2004, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPA'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPB')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPB', 1, 5, 'Prase PEAVAMPB - Diffusione Sonora AV Digital - Amplificatori 3 e 4', NULL, 830, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPB.png', 2005, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPB - Diffusione Sonora AV Digital - Amplificatori 3 e 4', [WSUrlPattern] = NULL, [Order] = 830, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPB.png', [GlobalOrder] = 2005, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPB'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPC')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPC', 1, 5, 'Prase PEAVAMPC - Diffusione Sonora AV Digital - Amplificatori 5 e 6', NULL, 840, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPC.png', 2006, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPC - Diffusione Sonora AV Digital - Amplificatori 5 e 6', [WSUrlPattern] = NULL, [Order] = 840, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPC.png', [GlobalOrder] = 2006, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPC'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPD')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPD', 1, 5, 'Prase PEAVAMPD - Diffusione Sonora AV Digital - Amplificatori 7 e 8', NULL, 850, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPD.png', 2007, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPD - Diffusione Sonora AV Digital - Amplificatori 7 e 8', [WSUrlPattern] = NULL, [Order] = 850, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPD.png', [GlobalOrder] = 2007, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPD'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPE')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPE', 1, 5, 'Prase PEAVAMPE - Diffusione Sonora AV Digital - Amplificatori 9 e 10', NULL, 860, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPE.png', 2008, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPE - Diffusione Sonora AV Digital - Amplificatori 9 e 10', [WSUrlPattern] = NULL, [Order] = 860, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPE.png', [GlobalOrder] = 2008, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPE'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPF')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPF', 1, 5, 'Prase PEAVAMPF - Diffusione Sonora AV Digital - Amplificatori 11 e 12', NULL, 870, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPF.png', 2009, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPF - Diffusione Sonora AV Digital - Amplificatori 11 e 12', [WSUrlPattern] = NULL, [Order] = 870, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPF.png', [GlobalOrder] = 2009, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPF'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPG')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPG', 1, 5, 'Prase PEAVAMPG - Diffusione Sonora AV Digital - Amplificatori 13 e 14', NULL, 880, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPG.png', 2010, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPG - Diffusione Sonora AV Digital - Amplificatori 13 e 14', [WSUrlPattern] = NULL, [Order] = 880, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPG.png', [GlobalOrder] = 2010, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPG'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVAMPH')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVAMPH', 1, 5, 'Prase PEAVAMPH - Diffusione Sonora AV Digital - Amplificatori 15 e 16', NULL, 890, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVAMPH.png', 2011, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVAMPH - Diffusione Sonora AV Digital - Amplificatori 15 e 16', [WSUrlPattern] = NULL, [Order] = 890, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVAMPH.png', [GlobalOrder] = 2011, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVAMPH'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVDOM1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVDOM1', 1, 5, 'Prase PEAVDOM1 - Diffusione Sonora AV Digital - DOM 1', NULL, 780, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVDOM1.png', 2000, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVDOM1 - Diffusione Sonora AV Digital - DOM 1', [WSUrlPattern] = NULL, [Order] = 780, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVDOM1.png', [GlobalOrder] = 2000, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVDOM1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVDOM2')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVDOM2', 1, 5, 'Prase PEAVDOM2 - Diffusione Sonora AV Digital - DOM 2', NULL, 790, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVDOM2.png', 2001, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVDOM2 - Diffusione Sonora AV Digital - DOM 2', [WSUrlPattern] = NULL, [Order] = 790, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVDOM2.png', [GlobalOrder] = 2001, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVDOM2'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVDOM3')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVDOM3', 1, 5, 'Prase PEAVDOM3 - Diffusione Sonora AV Digital - DOM 3', NULL, 800, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVDOM3.png', 2002, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVDOM3 - Diffusione Sonora AV Digital - DOM 3', [WSUrlPattern] = NULL, [Order] = 800, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVDOM3.png', [GlobalOrder] = 2002, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVDOM3'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVDOM4')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVDOM4', 1, 5, 'Prase PEAVDOM4 - Diffusione Sonora AV Digital - DOM 4', NULL, 810, 7, 1, '', 'STLC1000_RS485', NULL, 'PEAVDOM4.png', 2003, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVDOM4 - Diffusione Sonora AV Digital - DOM 4', [WSUrlPattern] = NULL, [Order] = 810, [TechnologyID] = 7, [Obsolete] = 1, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVDOM4.png', [GlobalOrder] = 2003, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVDOM4'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA110')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA110', 1, 5, 'Prase PEAVGA110 - Diffusione Sonora AVDigital - Amplificatore 1 DOM 1', NULL, 330, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA110.png', 600, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA110 - Diffusione Sonora AVDigital - Amplificatore 1 DOM 1', [WSUrlPattern] = NULL, [Order] = 330, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA110.png', [GlobalOrder] = 600, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA110'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA120')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA120', 1, 5, 'Prase PEAVGA120 - Diffusione Sonora AVDigital - Amplificatore 2 DOM 1', NULL, 340, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA120.png', 610, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA120 - Diffusione Sonora AVDigital - Amplificatore 2 DOM 1', [WSUrlPattern] = NULL, [Order] = 340, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA120.png', [GlobalOrder] = 610, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA120'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA130')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA130', 1, 5, 'Prase PEAVGA130 - Diffusione Sonora AVDigital - Amplificatore 3 DOM 1', NULL, 350, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA130.png', 620, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA130 - Diffusione Sonora AVDigital - Amplificatore 3 DOM 1', [WSUrlPattern] = NULL, [Order] = 350, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA130.png', [GlobalOrder] = 620, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA130'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA140')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA140', 1, 5, 'Prase PEAVGA140 - Diffusione Sonora AVDigital - Amplificatore 4 DOM 1', NULL, 360, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA140.png', 630, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA140 - Diffusione Sonora AVDigital - Amplificatore 4 DOM 1', [WSUrlPattern] = NULL, [Order] = 360, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA140.png', [GlobalOrder] = 630, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA140'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA210')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA210', 1, 5, 'Prase PEAVGA210 - Diffusione Sonora AVDigital - Amplificatore 1 DOM 2', NULL, 370, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA210.png', 640, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA210 - Diffusione Sonora AVDigital - Amplificatore 1 DOM 2', [WSUrlPattern] = NULL, [Order] = 370, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA210.png', [GlobalOrder] = 640, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA210'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA220')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA220', 1, 5, 'Prase PEAVGA220 - Diffusione Sonora AVDigital - Amplificatore 2 DOM 2', NULL, 380, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA220.png', 650, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA220 - Diffusione Sonora AVDigital - Amplificatore 2 DOM 2', [WSUrlPattern] = NULL, [Order] = 380, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA220.png', [GlobalOrder] = 650, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA220'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA230')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA230', 1, 5, 'Prase PEAVGA230 - Diffusione Sonora AVDigital - Amplificatore 3 DOM 2', NULL, 390, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA230.png', 660, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA230 - Diffusione Sonora AVDigital - Amplificatore 3 DOM 2', [WSUrlPattern] = NULL, [Order] = 390, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA230.png', [GlobalOrder] = 660, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA230'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA240')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA240', 1, 5, 'Prase PEAVGA240 - Diffusione Sonora AVDigital - Amplificatore 4 DOM 2', NULL, 400, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA240.png', 670, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA240 - Diffusione Sonora AVDigital - Amplificatore 4 DOM 2', [WSUrlPattern] = NULL, [Order] = 400, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA240.png', [GlobalOrder] = 670, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA240'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA310')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA310', 1, 5, 'Prase PEAVGA310 - Diffusione Sonora AVDigital - Amplificatore 1 DOM 3', NULL, 410, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA310.png', 680, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA310 - Diffusione Sonora AVDigital - Amplificatore 1 DOM 3', [WSUrlPattern] = NULL, [Order] = 410, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA310.png', [GlobalOrder] = 680, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA310'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA320')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA320', 1, 5, 'Prase PEAVGA320 - Diffusione Sonora AVDigital - Amplificatore 2 DOM 3', NULL, 420, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA320.png', 690, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA320 - Diffusione Sonora AVDigital - Amplificatore 2 DOM 3', [WSUrlPattern] = NULL, [Order] = 420, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA320.png', [GlobalOrder] = 690, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA320'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA330')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA330', 1, 5, 'Prase PEAVGA330 - Diffusione Sonora AVDigital - Amplificatore 3 DOM 3', NULL, 430, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA330.png', 700, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA330 - Diffusione Sonora AVDigital - Amplificatore 3 DOM 3', [WSUrlPattern] = NULL, [Order] = 430, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA330.png', [GlobalOrder] = 700, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA330'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGA340')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGA340', 1, 5, 'Prase PEAVGA340 - Diffusione Sonora AVDigital - Amplificatore 4 DOM 3', NULL, 440, 6, 0, 'Amplificatore', 'STLC1000_RS485', NULL, 'PEAVGA340.png', 710, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGA340 - Diffusione Sonora AVDigital - Amplificatore 4 DOM 3', [WSUrlPattern] = NULL, [Order] = 440, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Amplificatore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGA340.png', [GlobalOrder] = 710, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGA340'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGD100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGD100', 1, 5, 'Prase PEAVGD100 - Diffusione Sonora AVDigital - Unita` Centrale DOM 1 (no input aggiuntivi)', NULL, 280, 6, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAVGD100.png', 550, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGD100 - Diffusione Sonora AVDigital - Unita` Centrale DOM 1 (no input aggiuntivi)', [WSUrlPattern] = NULL, [Order] = 280, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGD100.png', [GlobalOrder] = 550, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGD100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGD102')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGD102', 1, 5, 'Prase PEAVGD102 - Diffusione Sonora AVDigital - Unita` Centrale DOM 1 (+2 input aggiuntivi)', NULL, 290, 6, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAVGD102.png', 560, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGD102 - Diffusione Sonora AVDigital - Unita` Centrale DOM 1 (+2 input aggiuntivi)', [WSUrlPattern] = NULL, [Order] = 290, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGD102.png', [GlobalOrder] = 560, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGD102'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGD103')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGD103', 1, 5, 'Prase PEAVGD103 - Diffusione Sonora AVDigital - Unita` Centrale DOM 1 (+3 input aggiuntivi)', NULL, 300, 6, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAVGD103.png', 570, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGD103 - Diffusione Sonora AVDigital - Unita` Centrale DOM 1 (+3 input aggiuntivi)', [WSUrlPattern] = NULL, [Order] = 300, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGD103.png', [GlobalOrder] = 570, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGD103'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGD200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGD200', 1, 5, 'Prase PEAVGD200 - Diffusione Sonora AVDigital - Unita` Centrale DOM 2', NULL, 310, 6, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAVGD200.png', 580, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGD200 - Diffusione Sonora AVDigital - Unita` Centrale DOM 2', [WSUrlPattern] = NULL, [Order] = 310, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGD200.png', [GlobalOrder] = 580, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGD200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGD300')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGD300', 1, 5, 'Prase PEAVGD300 - Diffusione Sonora AVDigital - Unita` Centrale DOM 3', NULL, 320, 6, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PEAVGD300.png', 590, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGD300 - Diffusione Sonora AVDigital - Unita` Centrale DOM 3', [WSUrlPattern] = NULL, [Order] = 320, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGD300.png', [GlobalOrder] = 590, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGD300'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGL101')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGL101', 1, 5, 'Prase PEAVGL101 - Diffusione Sonora AVDigital - Linea di diffusione DOM 1', NULL, 450, 6, 0, 'Linea di diffusione', 'STLC1000_RS485', NULL, 'PEAVGL101.png', 720, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGL101 - Diffusione Sonora AVDigital - Linea di diffusione DOM 1', [WSUrlPattern] = NULL, [Order] = 450, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Linea di diffusione', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGL101.png', [GlobalOrder] = 720, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGL101'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGL201')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGL201', 1, 5, 'Prase PEAVGL201 - Diffusione Sonora AVDigital - Linea di diffusione DOM 2', NULL, 460, 6, 0, 'Linea di diffusione', 'STLC1000_RS485', NULL, 'PEAVGL201.png', 730, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGL201 - Diffusione Sonora AVDigital - Linea di diffusione DOM 2', [WSUrlPattern] = NULL, [Order] = 460, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Linea di diffusione', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGL201.png', [GlobalOrder] = 730, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGL201'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGL301')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGL301', 1, 5, 'Prase PEAVGL301 - Diffusione Sonora AVDigital - Linea di diffusione DOM 3', NULL, 470, 6, 0, 'Linea di diffusione', 'STLC1000_RS485', NULL, 'PEAVGL301.png', 740, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGL301 - Diffusione Sonora AVDigital - Linea di diffusione DOM 3', [WSUrlPattern] = NULL, [Order] = 470, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Linea di diffusione', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGL301.png', [GlobalOrder] = 740, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGL301'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGM101')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGM101', 1, 5, 'Prase PEAVGM101 - Diffusione Sonora AVDigital - Base Microfonica 1', NULL, 480, 6, 0, 'Base Microfonica', 'STLC1000_RS485', NULL, 'PEAVGM101.png', 750, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGM101 - Diffusione Sonora AVDigital - Base Microfonica 1', [WSUrlPattern] = NULL, [Order] = 480, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Base Microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGM101.png', [GlobalOrder] = 750, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGM101'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGM201')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGM201', 1, 5, 'Prase PEAVGM201 - Diffusione Sonora AVDigital - Base Microfonica 2', NULL, 490, 6, 0, 'Base Microfonica', 'STLC1000_RS485', NULL, 'PEAVGM201.png', 760, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGM201 - Diffusione Sonora AVDigital - Base Microfonica 2', [WSUrlPattern] = NULL, [Order] = 490, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Base Microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGM201.png', [GlobalOrder] = 760, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGM201'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVGM301')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVGM301', 1, 5, 'Prase PEAVGM301 - Diffusione Sonora AVDigital - Base Microfonica 3', NULL, 500, 6, 0, 'Base Microfonica', 'STLC1000_RS485', NULL, 'PEAVGM301.png', 770, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVGM301 - Diffusione Sonora AVDigital - Base Microfonica 3', [WSUrlPattern] = NULL, [Order] = 500, [TechnologyID] = 6, [Obsolete] = 0, [DefaultName] = 'Base Microfonica', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVGM301.png', [GlobalOrder] = 770, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVGM301'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVLIN1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVLIN1', 1, 5, 'Prase PEAVLIN1 - Diffusione Sonora AV Digital - Linea 1', NULL, 930, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVLIN1.png', 2015, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVLIN1 - Diffusione Sonora AV Digital - Linea 1', [WSUrlPattern] = NULL, [Order] = 930, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVLIN1.png', [GlobalOrder] = 2015, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVLIN1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVLIN2')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVLIN2', 1, 5, 'Prase PEAVLIN2 - Diffusione Sonora AV Digital - Linea 2', NULL, 940, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVLIN2.png', 2016, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVLIN2 - Diffusione Sonora AV Digital - Linea 2', [WSUrlPattern] = NULL, [Order] = 940, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVLIN2.png', [GlobalOrder] = 2016, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVLIN2'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVLIN3')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVLIN3', 1, 5, 'Prase PEAVLIN3 - Diffusione Sonora AV Digital - Linea 3', NULL, 950, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVLIN3.png', 2017, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVLIN3 - Diffusione Sonora AV Digital - Linea 3', [WSUrlPattern] = NULL, [Order] = 950, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVLIN3.png', [GlobalOrder] = 2017, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVLIN3'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVLIN4')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVLIN4', 1, 5, 'Prase PEAVLIN4 - Diffusione Sonora AV Digital - Linea 4', NULL, 960, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVLIN4.png', 2018, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVLIN4 - Diffusione Sonora AV Digital - Linea 4', [WSUrlPattern] = NULL, [Order] = 960, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVLIN4.png', [GlobalOrder] = 2018, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVLIN4'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVMIC1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVMIC1', 1, 5, 'Prase PEAVMIC1 - Diffusione Sonora AV Digital - Base 1', NULL, 900, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVMIC1.png', 2012, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVMIC1 - Diffusione Sonora AV Digital - Base 1', [WSUrlPattern] = NULL, [Order] = 900, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVMIC1.png', [GlobalOrder] = 2012, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVMIC1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVMIC2')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVMIC2', 1, 5, 'Prase PEAVMIC2 - Diffusione Sonora AV Digital - Base 2', NULL, 910, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVMIC2.png', 2013, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVMIC2 - Diffusione Sonora AV Digital - Base 2', [WSUrlPattern] = NULL, [Order] = 910, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVMIC2.png', [GlobalOrder] = 2013, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVMIC2'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAVMIC3')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAVMIC3', 1, 5, 'Prase PEAVMIC3 - Diffusione Sonora AV Digital - Base 3', NULL, 920, 7, 0, '', 'STLC1000_RS485', NULL, 'PEAVMIC3.png', 2014, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAVMIC3 - Diffusione Sonora AV Digital - Base 3', [WSUrlPattern] = NULL, [Order] = 920, [TechnologyID] = 7, [Obsolete] = 0, [DefaultName] = '', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAVMIC3.png', [GlobalOrder] = 2014, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAVMIC3'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXDC180')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXDC180', 1, 5, 'Prase PEAXDC180 - Diffusione Sonora Axys - Diffusore Intellivox DC180', NULL, 600, NULL, 0, 'Diffusore', 'RS232', NULL, 'PEAXDC180.png', 870, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXDC180 - Diffusione Sonora Axys - Diffusore Intellivox DC180', [WSUrlPattern] = NULL, [Order] = 600, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'RS232', [SnmpCommunity] = NULL, [ImageName] = 'PEAXDC180.png', [GlobalOrder] = 870, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXDC180'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXDS180')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXDS180', 1, 5, 'Prase PEAXDS180 - Diffusione Sonora Axys - Diffusore Intellivox DS180', NULL, 610, NULL, 0, 'Diffusore', 'RS232', NULL, 'PEAXDS180.png', 880, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXDS180 - Diffusione Sonora Axys - Diffusore Intellivox DS180', [WSUrlPattern] = NULL, [Order] = 610, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'RS232', [SnmpCommunity] = NULL, [ImageName] = 'PEAXDS180.png', [GlobalOrder] = 880, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXDS180'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXDS280')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXDS280', 1, 5, 'Prase PEAXDS280 - Diffusione Sonora Axys - Diffusore Intellivox DS280', NULL, 620, NULL, 0, 'Diffusore', 'RS232', NULL, 'PEAXDS280.png', 890, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXDS280 - Diffusione Sonora Axys - Diffusore Intellivox DS280', [WSUrlPattern] = NULL, [Order] = 620, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'RS232', [SnmpCommunity] = NULL, [ImageName] = 'PEAXDS280.png', [GlobalOrder] = 890, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXDS280'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXDS500')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXDS500', 1, 5, 'Prase PEAXDS500 - Diffusione Sonora Axys - Diffusore Intellivox DS500', NULL, 630, NULL, 0, 'Diffusore', 'RS232', NULL, 'PEAXDS500.png', 900, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXDS500 - Diffusione Sonora Axys - Diffusore Intellivox DS500', [WSUrlPattern] = NULL, [Order] = 630, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'RS232', [SnmpCommunity] = NULL, [ImageName] = 'PEAXDS500.png', [GlobalOrder] = 900, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXDS500'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD100', 1, 5, 'Prase PEAXGD100 - Diffusione Sonora Axys - Diffusore Intellivox 1', NULL, 510, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD100.png', 780, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD100 - Diffusione Sonora Axys - Diffusore Intellivox 1', [WSUrlPattern] = NULL, [Order] = 510, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD100.png', [GlobalOrder] = 780, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD200', 1, 5, 'Prase PEAXGD200 - Diffusione Sonora Axys - Diffusore Intellivox 2', NULL, 520, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD200.png', 790, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD200 - Diffusione Sonora Axys - Diffusore Intellivox 2', [WSUrlPattern] = NULL, [Order] = 520, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD200.png', [GlobalOrder] = 790, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD300')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD300', 1, 5, 'Prase PEAXGD300 - Diffusione Sonora Axys - Diffusore Intellivox 3', NULL, 530, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD300.png', 800, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD300 - Diffusione Sonora Axys - Diffusore Intellivox 3', [WSUrlPattern] = NULL, [Order] = 530, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD300.png', [GlobalOrder] = 800, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD300'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD400')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD400', 1, 5, 'Prase PEAXGD400 - Diffusione Sonora Axys - Diffusore Intellivox 4', NULL, 540, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD400.png', 810, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD400 - Diffusione Sonora Axys - Diffusore Intellivox 4', [WSUrlPattern] = NULL, [Order] = 540, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD400.png', [GlobalOrder] = 810, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD400'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD500')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD500', 1, 5, 'Prase PEAXGD500 - Diffusione Sonora Axys - Diffusore Intellivox 5', NULL, 550, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD500.png', 820, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD500 - Diffusione Sonora Axys - Diffusore Intellivox 5', [WSUrlPattern] = NULL, [Order] = 550, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD500.png', [GlobalOrder] = 820, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD500'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD600')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD600', 1, 5, 'Prase PEAXGD600 - Diffusione Sonora Axys - Diffusore Intellivox 6', NULL, 560, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD600.png', 830, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD600 - Diffusione Sonora Axys - Diffusore Intellivox 6', [WSUrlPattern] = NULL, [Order] = 560, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD600.png', [GlobalOrder] = 830, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD600'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD700')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD700', 1, 5, 'Prase PEAXGD700 - Diffusione Sonora Axys - Diffusore Intellivox 7', NULL, 570, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD700.png', 840, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD700 - Diffusione Sonora Axys - Diffusore Intellivox 7', [WSUrlPattern] = NULL, [Order] = 570, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD700.png', [GlobalOrder] = 840, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD700'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD800')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD800', 1, 5, 'Prase PEAXGD800 - Diffusione Sonora Axys - Diffusore Intellivox 8', NULL, 580, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD800.png', 850, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD800 - Diffusione Sonora Axys - Diffusore Intellivox 8', [WSUrlPattern] = NULL, [Order] = 580, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD800.png', [GlobalOrder] = 850, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD800'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PEAXGD900')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PEAXGD900', 1, 5, 'Prase PEAXGD900 - Diffusione Sonora Axys - Diffusore Intellivox 9', NULL, 590, 37, 0, 'Diffusore', 'STLC1000_RS485', NULL, 'PEAXGD900.png', 860, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PEAXGD900 - Diffusione Sonora Axys - Diffusore Intellivox 9', [WSUrlPattern] = NULL, [Order] = 590, [TechnologyID] = 37, [Obsolete] = 0, [DefaultName] = 'Diffusore', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PEAXGD900.png', [GlobalOrder] = 860, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PEAXGD900'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PENXGD100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PENXGD100', 1, 5, 'Prase PENXGD100 - Diffusione Sonora Nexia - Unita` DSP', NULL, 640, 39, 0, 'Unita` DSP', 'STLC1000_RS485', NULL, 'PENXGD100.png', 910, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PENXGD100 - Diffusione Sonora Nexia - Unita` DSP', [WSUrlPattern] = NULL, [Order] = 640, [TechnologyID] = 39, [Obsolete] = 0, [DefaultName] = 'Unita` DSP', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PENXGD100.png', [GlobalOrder] = 910, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PENXGD100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PESBGA102')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PESBGA102', 1, 5, 'Prase PESBGA102 - Diffusione Sonora SalzBrenner - Unita` Analyzer 1', NULL, 670, 38, 0, 'Unita` Analyzer', 'STLC1000_RS485', NULL, 'PESBGA102.png', 940, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PESBGA102 - Diffusione Sonora SalzBrenner - Unita` Analyzer 1', [WSUrlPattern] = NULL, [Order] = 670, [TechnologyID] = 38, [Obsolete] = 0, [DefaultName] = 'Unita` Analyzer', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PESBGA102.png', [GlobalOrder] = 940, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PESBGA102'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PESBGA202')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PESBGA202', 1, 5, 'Prase PESBGA202 - Diffusione Sonora SalzBrenner - Unita` Analyzer 2', NULL, 680, 38, 0, 'Unita` Analyzer', 'STLC1000_RS485', NULL, 'PESBGA202.png', 950, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PESBGA202 - Diffusione Sonora SalzBrenner - Unita` Analyzer 2', [WSUrlPattern] = NULL, [Order] = 680, [TechnologyID] = 38, [Obsolete] = 0, [DefaultName] = 'Unita` Analyzer', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PESBGA202.png', [GlobalOrder] = 950, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PESBGA202'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PESBGM100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PESBGM100', 1, 5, 'Prase PESBGM100 - Diffusione Sonora SalzBrenner - Unita` Master', NULL, 650, 38, 0, 'Unita` Centrale', 'STLC1000_RS485', NULL, 'PESBGM100.png', 920, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PESBGM100 - Diffusione Sonora SalzBrenner - Unita` Master', [WSUrlPattern] = NULL, [Order] = 650, [TechnologyID] = 38, [Obsolete] = 0, [DefaultName] = 'Unita` Centrale', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PESBGM100.png', [GlobalOrder] = 920, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PESBGM100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'PESBGZ100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('PESBGZ100', 1, 5, 'Prase PESBGZ100 - Diffusione Sonora SalzBrenner - Unita` Zone Relay', NULL, 660, 38, 0, 'Unita` Zone Relay', 'STLC1000_RS485', NULL, 'PESBGZ100.png', 930, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 5, [DeviceTypeDescription] = 'Prase PESBGZ100 - Diffusione Sonora SalzBrenner - Unita` Zone Relay', [WSUrlPattern] = NULL, [Order] = 660, [TechnologyID] = 38, [Obsolete] = 0, [DefaultName] = 'Unita` Zone Relay', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'PESBGZ100.png', [GlobalOrder] = 930, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'PESBGZ100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADASMI52L')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADASMI52L', 5, 11, 'RADASMI52L - Periferica RAD Data Comm. ASMi-52L', NULL, 5250, 48, 0, 'RAD Data ASMi', 'TCP_Client', 'public', 'RADASMI52L.png', 3515, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADASMI52L - Periferica RAD Data Comm. ASMi-52L', [WSUrlPattern] = NULL, [Order] = 5250, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data ASMi', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADASMI52L.png', [GlobalOrder] = 3515, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADASMI52L'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADASMI52SA')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADASMI52SA', 5, 11, 'RADASMI52SA - Periferica RAD Data Comm. ASMi-52SA', NULL, 5251, 48, 0, 'RAD Data ASMi', 'TCP_Client', 'public', 'RADASMI52L.png', 3519, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADASMI52SA - Periferica RAD Data Comm. ASMi-52SA', [WSUrlPattern] = NULL, [Order] = 5251, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data ASMi', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADASMI52L.png', [GlobalOrder] = 3519, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADASMI52SA'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADASMI54')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADASMI54', 5, 11, 'RADASMI54 - Periferica RAD Data Comm. ASMi-54', NULL, 5270, 48, 0, 'RAD Data ASMi', 'TCP_Client', 'public', 'RADASMI54.png', 3517, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADASMI54 - Periferica RAD Data Comm. ASMi-54', [WSUrlPattern] = NULL, [Order] = 5270, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data ASMi', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADASMI54.png', [GlobalOrder] = 3517, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADASMI54'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADASMI54L')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADASMI54L', 5, 11, 'RADASMI54L - Periferica RAD Data Comm. ASMi-54L', NULL, 5260, 48, 0, 'RAD Data ASMi', 'TCP_Client', 'public', 'RADASMI54L.png', 3516, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADASMI54L - Periferica RAD Data Comm. ASMi-54L', [WSUrlPattern] = NULL, [Order] = 5260, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data ASMi', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADASMI54L.png', [GlobalOrder] = 3516, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADASMI54L'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADDAT000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADDAT000', 5, 11, 'RADDAT000 - Periferica RAD Data Generica', NULL, 5050, 48, 0, 'RAD Data Device', 'TCP_Client', 'public', 'RADDAT000.png', 3510, 1, 2)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADDAT000 - Periferica RAD Data Generica', [WSUrlPattern] = NULL, [Order] = 5050, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data Device', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADDAT000.png', [GlobalOrder] = 3510, [ExportToList] = 1, [DiscoveryType] = 2 WHERE [DeviceTypeID] = 'RADDAT000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADMIRICIE1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADMIRICIE1', 5, 11, 'RADMIRICIE1 - Periferica RAD Data Comm. MiRICi-E1', NULL, 5280, 48, 0, 'RAD Data MiRICi', 'TCP_Client', 'public', 'RADMIRICIE1.png', 3518, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADMIRICIE1 - Periferica RAD Data Comm. MiRICi-E1', [WSUrlPattern] = NULL, [Order] = 5280, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data MiRICi', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADMIRICIE1.png', [GlobalOrder] = 3518, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADMIRICIE1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADOPT108')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADOPT108', 5, 11, 'RADOPT108 - Periferica RAD Data Comm. Optimux 108', NULL, 5070, 48, 0, 'RAD Data Optimux', 'TCP_Client', 'public', 'RADOPT108.png', 3512, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADOPT108 - Periferica RAD Data Comm. Optimux 108', [WSUrlPattern] = NULL, [Order] = 5070, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data Optimux', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADOPT108.png', [GlobalOrder] = 3512, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADOPT108'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADOPT34')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADOPT34', 5, 11, 'RADOPT34 - Periferica RAD Data Comm. Optimux 34', NULL, 5060, 48, 0, 'RAD Data Optimux', 'TCP_Client', 'public', 'RADOPT34.png', 3511, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADOPT34 - Periferica RAD Data Comm. Optimux 34', [WSUrlPattern] = NULL, [Order] = 5060, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data Optimux', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADOPT34.png', [GlobalOrder] = 3511, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADOPT34'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADRICI4E1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADRICI4E1', 5, 11, 'RADRICI4E1 - Periferica RAD Data Comm. RICi-4E1', NULL, 5240, 48, 0, 'RAD Data RICi', 'TCP_Client', 'public', 'RADRICI4E1.png', 3514, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADRICI4E1 - Periferica RAD Data Comm. RICi-4E1', [WSUrlPattern] = NULL, [Order] = 5240, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data RICi', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADRICI4E1.png', [GlobalOrder] = 3514, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADRICI4E1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'RADRICIE1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('RADRICIE1', 5, 11, 'RADRICIE1 - Periferica RAD Data Comm. RICi-E1', NULL, 5230, 48, 0, 'RAD Data RICi', 'TCP_Client', 'public', 'RADRICIE1.png', 3513, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 11, [DeviceTypeDescription] = 'RADRICIE1 - Periferica RAD Data Comm. RICi-E1', [WSUrlPattern] = NULL, [Order] = 5230, [TechnologyID] = 48, [Obsolete] = 0, [DefaultName] = 'RAD Data RICi', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'RADRICIE1.png', [GlobalOrder] = 3513, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'RADRICIE1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SNZE000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SNZE000', 5, 6, 'Sysnet SNZE000 - Zeus Generico (solo MIB-II)', NULL, 5010, NULL, 0, 'Zeus', 'TCP_Client', 'public', 'SNZE000.png', 990, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet SNZE000 - Zeus Generico (solo MIB-II)', [WSUrlPattern] = NULL, [Order] = 5010, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SNZE000.png', [GlobalOrder] = 990, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SNZE000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SOCM000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SOCM000', 2, 3, 'Solari SOCM000 - Monitor CRT', 'http://{0}:8081', 2020, 1, 0, 'Monitor', 'TCP_Client', 'solari_public', 'SOCM000.png', 340, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 3, [DeviceTypeDescription] = 'Solari SOCM000 - Monitor CRT', [WSUrlPattern] = 'http://{0}:8081', [Order] = 2020, [TechnologyID] = 1, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'solari_public', [ImageName] = 'SOCM000.png', [GlobalOrder] = 340, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SOCM000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SOLM000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SOLM000', 2, 3, 'Solari SOLM000 - Monitor LED', 'http://{0}:8081', 2021, 3, 0, 'Monitor', 'TCP_Client', 'solari_public', 'SOLM000.png', 350, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 3, [DeviceTypeDescription] = 'Solari SOLM000 - Monitor LED', [WSUrlPattern] = 'http://{0}:8081', [Order] = 2021, [TechnologyID] = 3, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'solari_public', [ImageName] = 'SOLM000.png', [GlobalOrder] = 350, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SOLM000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SOTM000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SOTM000', 2, 3, 'Solari SOTM000 - Monitor TFT', 'http://{0}:8081', 2022, 2, 0, 'Monitor', 'TCP_Client', 'solari_public', 'SOTM000.png', 360, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 3, [DeviceTypeDescription] = 'Solari SOTM000 - Monitor TFT', [WSUrlPattern] = 'http://{0}:8081', [Order] = 2022, [TechnologyID] = 2, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'solari_public', [ImageName] = 'SOTM000.png', [GlobalOrder] = 360, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SOTM000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SOXM200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SOXM200', 2, 3, 'Solari SOXM200 - Monitor Generico (solo MIB-II)', 'http://{0}:8081', 2023, NULL, 0, 'Monitor', 'TCP_Client', 'solari_public', 'SOXM200.png', 370, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 3, [DeviceTypeDescription] = 'Solari SOXM200 - Monitor Generico (solo MIB-II)', [WSUrlPattern] = 'http://{0}:8081', [Order] = 2023, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'solari_public', [ImageName] = 'SOXM200.png', [GlobalOrder] = 370, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SOXM200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'STLC1000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('STLC1000', 10, 4, 'Telefin STLC1000 - Concentratore Diagnostica', NULL, 10000, 32, 0, 'STLC1000', 'TCP_Client', 'public', 'STLC1000.png', 240, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 10, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin STLC1000 - Concentratore Diagnostica', [WSUrlPattern] = NULL, [Order] = 10000, [TechnologyID] = 32, [Obsolete] = 0, [DefaultName] = 'STLC1000', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'STLC1000.png', [GlobalOrder] = 240, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'STLC1000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYCMH00')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYCMH00', 2, 1, 'Sysco SYCMH00 - Monitor CRT tecnologia Hantarex', 'http://{0}/dispositivo.wsdl', 2030, 1, 0, 'Monitor', 'TCP_Client', 'public', 'SYCMH00.png', 250, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 1, [DeviceTypeDescription] = 'Sysco SYCMH00 - Monitor CRT tecnologia Hantarex', [WSUrlPattern] = 'http://{0}/dispositivo.wsdl', [Order] = 2030, [TechnologyID] = 1, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYCMH00.png', [GlobalOrder] = 250, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SYCMH00'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYLB000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYLB000', 2, 1, 'Sysco SYLB000 - Indicatore di binario LED', 'http://{0}/dispositivo.wsdl', 2035, 3, 0, 'Monitor', 'TCP_Client', 'public', 'SYLB000.png', 300, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 1, [DeviceTypeDescription] = 'Sysco SYLB000 - Indicatore di binario LED', [WSUrlPattern] = 'http://{0}/dispositivo.wsdl', [Order] = 2035, [TechnologyID] = 3, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYLB000.png', [GlobalOrder] = 300, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SYLB000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYLF000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYLF000', 2, 1, 'Sysco SYLF000 - Fascia LED', 'http://{0}/dispositivo.wsdl', 2034, 3, 0, 'Monitor', 'TCP_Client', 'public', 'SYLF000.png', 290, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 1, [DeviceTypeDescription] = 'Sysco SYLF000 - Fascia LED', [WSUrlPattern] = 'http://{0}/dispositivo.wsdl', [Order] = 2034, [TechnologyID] = 3, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYLF000.png', [GlobalOrder] = 290, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SYLF000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYLM000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYLM000', 2, 1, 'Sysco SYLM000 - Monitor LED', 'http://{0}/dispositivo.wsdl', 2033, 3, 0, 'Monitor', 'TCP_Client', 'public', 'SYLM000.png', 280, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 1, [DeviceTypeDescription] = 'Sysco SYLM000 - Monitor LED', [WSUrlPattern] = 'http://{0}/dispositivo.wsdl', [Order] = 2033, [TechnologyID] = 3, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYLM000.png', [GlobalOrder] = 280, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SYLM000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYLQ000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYLQ000', 2, 1, 'Sysco SYLQ000 - Quadro A-P LED', 'http://{0}/dispositivo.wsdl', 2036, 3, 0, 'Monitor', 'TCP_Client', 'public', 'SYLQ000.png', 310, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 1, [DeviceTypeDescription] = 'Sysco SYLQ000 - Quadro A-P LED', [WSUrlPattern] = 'http://{0}/dispositivo.wsdl', [Order] = 2036, [TechnologyID] = 3, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYLQ000.png', [GlobalOrder] = 310, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SYLQ000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNET000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNET000', 5, 6, 'Sysnet SYSNET000 - Periferica Generica', NULL, 5290, 45, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNET000.png', 3000, 1, 2)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet SYSNET000 - Periferica Generica', [WSUrlPattern] = NULL, [Order] = 5290, [TechnologyID] = 45, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNET000.png', [GlobalOrder] = 3000, [ExportToList] = 1, [DiscoveryType] = 2 WHERE [DeviceTypeID] = 'SYSNET000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETG384V1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETG384V1', 5, 6, 'Sysnet Zeus G384 V1', NULL, 5480, 43, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETG384V4.png', 3015, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus G384 V1', [WSUrlPattern] = NULL, [Order] = 5480, [TechnologyID] = 43, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETG384V4.png', [GlobalOrder] = 3015, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETG384V1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETG384V3')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETG384V3', 5, 6, 'Sysnet Zeus G384 V3', NULL, 5350, 43, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETG384V3.png', 3006, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus G384 V3', [WSUrlPattern] = NULL, [Order] = 5350, [TechnologyID] = 43, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETG384V3.png', [GlobalOrder] = 3006, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETG384V3'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETG384V4')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETG384V4', 5, 6, 'Sysnet Zeus G384 V4', NULL, 5320, 43, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETG384V4.png', 3003, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus G384 V4', [WSUrlPattern] = NULL, [Order] = 5320, [TechnologyID] = 43, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETG384V4.png', [GlobalOrder] = 3003, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETG384V4'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETG384V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETG384V5', 5, 6, 'Sysnet Zeus G384 V5', NULL, 5460, 43, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETG384V5.png', 3013, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus G384 V5', [WSUrlPattern] = NULL, [Order] = 5460, [TechnologyID] = 43, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETG384V5.png', [GlobalOrder] = 3013, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETG384V5'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETG684V4')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETG684V4', 5, 6, 'Sysnet Zeus G684S V4', NULL, 5330, 43, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETG684V4.png', 3004, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus G684S V4', [WSUrlPattern] = NULL, [Order] = 5330, [TechnologyID] = 43, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETG684V4.png', [GlobalOrder] = 3004, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETG684V4'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM184V4')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM184V4', 5, 6, 'Sysnet Zeus M184 V4', NULL, 5380, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM184V4.png', 3009, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M184 V4', [WSUrlPattern] = NULL, [Order] = 5380, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM184V4.png', [GlobalOrder] = 3009, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM184V4'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM184V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM184V5', 5, 6, 'Sysnet Zeus M184 V5', NULL, 5430, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM184V5.png', 3010, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M184 V5', [WSUrlPattern] = NULL, [Order] = 5430, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM184V5.png', [GlobalOrder] = 3010, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM184V5'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM190V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM190V5', 5, 6, 'Sysnet Zeus M190 V5', NULL, 5432, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM190V5.png', 3016, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M190 V5', [WSUrlPattern] = NULL, [Order] = 5432, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM190V5.png', [GlobalOrder] = 3016, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM190V5'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM384V1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM384V1', 5, 6, 'Sysnet Zeus M384 V1', NULL, 5440, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM384V1.png', 3011, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M384 V1', [WSUrlPattern] = NULL, [Order] = 5440, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM384V1.png', [GlobalOrder] = 3011, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM384V1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM384V4')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM384V4', 5, 6, 'Sysnet Zeus M384 V4', NULL, 5300, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM384V4.png', 3001, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M384 V4', [WSUrlPattern] = NULL, [Order] = 5300, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM384V4.png', [GlobalOrder] = 3001, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM384V4'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM384V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM384V5', 5, 6, 'Sysnet Zeus M384 V5', NULL, 5360, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM384V5.png', 3007, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M384 V5', [WSUrlPattern] = NULL, [Order] = 5360, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM384V5.png', [GlobalOrder] = 3007, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM384V5'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM684V1')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM684V1', 5, 6, 'Sysnet Zeus M684 V1', NULL, 5450, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM684V1.png', 3012, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M684 V1', [WSUrlPattern] = NULL, [Order] = 5450, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM684V1.png', [GlobalOrder] = 3012, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM684V1'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM684V3')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM684V3', 5, 6, 'Sysnet Zeus M684S V3', NULL, 5340, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM684V3.png', 3005, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M684S V3', [WSUrlPattern] = NULL, [Order] = 5340, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM684V3.png', [GlobalOrder] = 3005, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM684V3'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM684V4')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM684V4', 5, 6, 'Sysnet Zeus M684S V4', NULL, 5310, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM684V4.png', 3002, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M684S V4', [WSUrlPattern] = NULL, [Order] = 5310, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM684V4.png', [GlobalOrder] = 3002, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM684V4'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM684V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM684V5', 5, 6, 'Sysnet Zeus M684 V5', NULL, 5370, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM684V5.png', 3008, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M684 V5', [WSUrlPattern] = NULL, [Order] = 5370, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM684V5.png', [GlobalOrder] = 3008, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM684V5'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM6904')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM6904', 5, 6, 'Sysnet Zeus M6904', NULL, 5482, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM6904.png', 3016, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M6904', [WSUrlPattern] = NULL, [Order] = 5482, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM6904.png', [GlobalOrder] = 3016, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM6904'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETM690V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETM690V5', 5, 6, 'Sysnet Zeus M690 V5', NULL, 5470, 44, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETM690V5.png', 3014, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus M690 V5', [WSUrlPattern] = NULL, [Order] = 5470, [TechnologyID] = 44, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETM690V5.png', [GlobalOrder] = 3014, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETM690V5'

IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETG180V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETG180V5', 5, 6, 'Sysnet Zeus G180 V5', NULL, 5483, 43, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETG180V5.png', 3017, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus G180 V5', [WSUrlPattern] = NULL, [Order] = 5483, [TechnologyID] = 43, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETG180V5.png', [GlobalOrder] = 3017, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETG180V5'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYSNETS190V5')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYSNETS190V5', 5, 6, 'Sysnet Zeus S190 V5', NULL, 5484, 43, 0, 'Zeus', 'TCP_Client', 'public', 'SYSNETS190V5.png', 3018, 0, 3)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 6, [DeviceTypeDescription] = 'Sysnet Zeus S190 V5', [WSUrlPattern] = NULL, [Order] = 5484, [TechnologyID] = 43, [Obsolete] = 0, [DefaultName] = 'Zeus', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYSNETS190V5.png', [GlobalOrder] = 3018, [ExportToList] = 0, [DiscoveryType] = 3 WHERE [DeviceTypeID] = 'SYSNETS190V5'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYTM100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYTM100', 2, 1, 'Sysco SYTM100 - Monitor TFT r1 - Indicatore di sottopasso', 'http://{0}/dispositivo.wsdl', 2031, 2, 0, 'Monitor', 'TCP_Client', 'public', 'SYTM100.png', 260, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 1, [DeviceTypeDescription] = 'Sysco SYTM100 - Monitor TFT r1 - Indicatore di sottopasso', [WSUrlPattern] = 'http://{0}/dispositivo.wsdl', [Order] = 2031, [TechnologyID] = 2, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYTM100.png', [GlobalOrder] = 260, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SYTM100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'SYTM200')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('SYTM200', 2, 1, 'Sysco SYTM200 - Monitor TFT r2', 'http://{0}/dispositivo.wsdl', 2032, 2, 0, 'Monitor', 'TCP_Client', 'public', 'SYTM200.png', 270, 0, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 1, [DeviceTypeDescription] = 'Sysco SYTM200 - Monitor TFT r2', [WSUrlPattern] = 'http://{0}/dispositivo.wsdl', [Order] = 2032, [TechnologyID] = 2, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'SYTM200.png', [GlobalOrder] = 270, [ExportToList] = 0, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'SYTM200'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'TA74000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('TA74000', 4, 4, 'Telefin TA74000 - Drop Insert G.703', NULL, 4030, 53, 0, 'Drop Insert G.703', 'STLC1000_RS485', NULL, 'TA74000.png', 3562, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin TA74000 - Drop Insert G.703', [WSUrlPattern] = NULL, [Order] = 4030, [TechnologyID] = 53, [Obsolete] = 0, [DefaultName] = 'Drop Insert G.703', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'TA74000.png', [GlobalOrder] = 3562, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'TA74000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'TT10210')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('TT10210', 1, 4, 'Telefin TT10210 - Pannello Zone (V2)', NULL, 30, 33, 0, 'Pannello Zone', 'STLC1000_RS485', NULL, 'TT10210.png', 220, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin TT10210 - Pannello Zone (V2)', [WSUrlPattern] = NULL, [Order] = 30, [TechnologyID] = 33, [Obsolete] = 0, [DefaultName] = 'Pannello Zone', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'TT10210.png', [GlobalOrder] = 220, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'TT10210'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'TT10210V3')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('TT10210V3', 1, 4, 'Telefin TT10210V3 - Pannello Zone (V3)', NULL, 31, 34, 0, 'Pannello Zone', 'STLC1000_RS485', NULL, 'TT10210V3.png', 230, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin TT10210V3 - Pannello Zone (V3)', [WSUrlPattern] = NULL, [Order] = 31, [TechnologyID] = 34, [Obsolete] = 0, [DefaultName] = 'Pannello Zone', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'TT10210V3.png', [GlobalOrder] = 230, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'TT10210V3'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'TT10220')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('TT10220', 1, 4, 'Telefin TT10220 - Pannello Zone DS PZi', NULL, 32, 42, 0, 'Pannello Zone', 'STLC1000_RS485', NULL, 'TT10220.png', 231, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin TT10220 - Pannello Zone DS PZi', [WSUrlPattern] = NULL, [Order] = 32, [TechnologyID] = 42, [Obsolete] = 0, [DefaultName] = 'Pannello Zone', [PortType] = 'STLC1000_RS485', [SnmpCommunity] = NULL, [ImageName] = 'TT10220.png', [GlobalOrder] = 231, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'TT10220'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'TT10230_000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('TT10230_000', 1, 4, 'Telefin TT10230 - Pannello Zone DS PZi (Amplificatori backup)', NULL, 33, 42, 0, 'Pannello Zone', 'TCP_Client', 'public', 'TT10230_000.png', 232, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin TT10230 - Pannello Zone DS PZi (Amplificatori backup)', [WSUrlPattern] = NULL, [Order] = 33, [TechnologyID] = 42, [Obsolete] = 0, [DefaultName] = 'Pannello Zone', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'TT10230_000.png', [GlobalOrder] = 232, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'TT10230_000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'TT10230IP')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('TT10230IP', 1, 4, 'Telefin TT10230IP - Pannello Zone DS PZi', NULL, 34, 42, 0, 'Pannello Zone', 'TCP_Client', 'public', 'TT10230IP.png', 233, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 1, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin TT10230IP - Pannello Zone DS PZi', [WSUrlPattern] = NULL, [Order] = 34, [TechnologyID] = 42, [Obsolete] = 0, [DefaultName] = 'Pannello Zone', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'TT10230IP.png', [GlobalOrder] = 233, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'TT10230IP'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'UASNTA100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('UASNTA100', 5, 17, 'sdi UASNTA100 - UAS/NTA', NULL, 99050, 59, 0, 'UAS/NTA', 'TCP_Client', 'public', 'UASNTA100.png', 1050, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 17, [DeviceTypeDescription] = 'sdi UASNTA100 - UAS/NTA', [WSUrlPattern] = NULL, [Order] = 99050, [TechnologyID] = 59, [Obsolete] = 0, [DefaultName] = 'UAS/NTA', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'UASNTA100.png', [GlobalOrder] = 1050, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'UASNTA100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'UPSRFC1628')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('UPSRFC1628', 6, 24, 'UPS RFC 1628 - Gruppo di continuita` conforme a RFC 1628', NULL, 6040, NULL, 0, 'Gruppo di Continuita`', 'TCP_Client', 'public', 'UPSRFC1628.png', 982, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 6, [VendorID] = 24, [DeviceTypeDescription] = 'UPS RFC 1628 - Gruppo di continuita` conforme a RFC 1628', [WSUrlPattern] = NULL, [Order] = 6040, [TechnologyID] = NULL, [Obsolete] = 0, [DefaultName] = 'Gruppo di Continuita`', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'UPSRFC1628.png', [GlobalOrder] = 982, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'UPSRFC1628'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP00010')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP00010', 4, 4, 'Telefin VP00010 - IP-PBX Centralino VoIP', NULL, 4110, 65, 0, 'IP-PBX Centralino VoIP', 'TCP_Client', 'public', 'VP00010.png', 4110, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin VP00010 - IP-PBX Centralino VoIP', [WSUrlPattern] = NULL, [Order] = 4110, [TechnologyID] = 65, [Obsolete] = 0, [DefaultName] = 'IP-PBX Centralino VoIP', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'VP00010.png', [GlobalOrder] = 4110, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP00010'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP18007')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP18007', 4, 19, 'Grandstream VP18007 - Telefono GXP2110', NULL, 4115, 64, 0, 'Telefono GXP2110', 'TCP_Client', NULL, 'VP18007.png', 4115, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 19, [DeviceTypeDescription] = 'Grandstream VP18007 - Telefono GXP2110', [WSUrlPattern] = NULL, [Order] = 4115, [TechnologyID] = 64, [Obsolete] = 0, [DefaultName] = 'Telefono GXP2110', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'VP18007.png', [GlobalOrder] = 4115, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP18007'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP20002')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP20002', 4, 4, 'VP20002 - Alimentatore (WAGO)', NULL, 4116, 35, 0, 'Alimentatore', 'TCP_Client', NULL, 'VP20002.png', 4116, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'VP20002 - Alimentatore (WAGO)', [WSUrlPattern] = NULL, [Order] = 4116, [TechnologyID] = 35, [Obsolete] = 0, [DefaultName] = 'Alimentatore', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'VP20002.png', [GlobalOrder] = 4116, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP20002'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP20003')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP20003', 4, 4, 'VP20003 - Alimentatore con Batteria (WAGO)', NULL, 4117, 35, 0, 'Alimentatore con Batteria', 'TCP_Client', NULL, 'VP20003.png', 4117, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'VP20003 - Alimentatore con Batteria (WAGO)', [WSUrlPattern] = NULL, [Order] = 4117, [TechnologyID] = 35, [Obsolete] = 0, [DefaultName] = 'Alimentatore con Batteria', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'VP20003.png', [GlobalOrder] = 4117, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP20003'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP30006')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP30006', 4, 20, 'Planet VP30006 - Switch 8 porte IGS-801M', NULL, 4113, 62, 0, 'Switch 8 porte', 'TCP_Client', 'public', 'VP30006.png', 4113, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 20, [DeviceTypeDescription] = 'Planet VP30006 - Switch 8 porte IGS-801M', [WSUrlPattern] = NULL, [Order] = 4113, [TechnologyID] = 62, [Obsolete] = 0, [DefaultName] = 'Switch 8 porte', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'VP30006.png', [GlobalOrder] = 4113, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP30006'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP50000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP50000', 4, 18, 'Keymile VP50000 - Modem LineRunner SCADA NG', NULL, 4114, 63, 0, 'Modem LineRunner SCADA NG', 'TCP_Client', 'public', 'VP50000.png', 4114, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 18, [DeviceTypeDescription] = 'Keymile VP50000 - Modem LineRunner SCADA NG', [WSUrlPattern] = NULL, [Order] = 4114, [TechnologyID] = 63, [Obsolete] = 0, [DefaultName] = 'Modem LineRunner SCADA NG', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'VP50000.png', [GlobalOrder] = 4114, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP50000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP66000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP66000', 4, 4, 'Telefin VP66000 - ATA FXS 2 linee', NULL, 4112, 61, 0, 'ATA FXS 2 linee', 'TCP_Client', 'public', 'VP66000.png', 4112, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin VP66000 - ATA FXS 2 linee', [WSUrlPattern] = NULL, [Order] = 4112, [TechnologyID] = 61, [Obsolete] = 0, [DefaultName] = 'ATA FXS 2 linee', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'VP66000.png', [GlobalOrder] = 4112, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP66000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP69000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP69000', 4, 4, 'Telefin VP69000 - Interfaccia di Diffusione Sonora', NULL, 4111, 60, 0, 'Interfaccia di Diffusione Sonora', 'TCP_Client', 'public', 'VP69000.png', 4111, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 4, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin VP69000 - Interfaccia di Diffusione Sonora', [WSUrlPattern] = NULL, [Order] = 4111, [TechnologyID] = 60, [Obsolete] = 0, [DefaultName] = 'Interfaccia di Diffusione Sonora', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'VP69000.png', [GlobalOrder] = 4111, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP69000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'VP69100')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('VP69100', 99, 4, 'Telefin VP69100 - Gateway VoIP Diffusione Sonora (IeC)', NULL, 4118, 60, 0, 'Interfaccia di Diffusione Sonora', 'TCP_Client', 'public', 'VP69100.png', 4122, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 99, [VendorID] = 4, [DeviceTypeDescription] = 'Telefin VP69100 - Gateway VoIP Diffusione Sonora (IeC)', [WSUrlPattern] = NULL, [Order] = 4118, [TechnologyID] = 60, [Obsolete] = 0, [DefaultName] = 'Interfaccia di Diffusione Sonora', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'VP69100.png', [GlobalOrder] = 4122, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'VP69100'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'WRKPDL000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('WRKPDL000', 5, 12, 'WRKPDL000 - Workstation Windows PDL', NULL, 5080, 49, 0, 'Workstation PDL', 'TCP_Client', 'public', 'WRKPDL000.png', 3520, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 12, [DeviceTypeDescription] = 'WRKPDL000 - Workstation Windows PDL', [WSUrlPattern] = NULL, [Order] = 5080, [TechnologyID] = 49, [Obsolete] = 0, [DefaultName] = 'Workstation PDL', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'WRKPDL000.png', [GlobalOrder] = 3520, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'WRKPDL000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'WRKWIN10')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('WRKWIN10', 99, 4, 'WRKWIN10 - Postazione di lavoro Windows', NULL, 99040, 40, 0, 'Postazione di lavoro', 'TCP_Client', 'public', 'WRKWIN10.png', 1030, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 99, [VendorID] = 4, [DeviceTypeDescription] = 'WRKWIN10 - Postazione di lavoro Windows', [WSUrlPattern] = NULL, [Order] = 99040, [TechnologyID] = 40, [Obsolete] = 0, [DefaultName] = 'Postazione di lavoro', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'WRKWIN10.png', [GlobalOrder] = 1030, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'WRKWIN10'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'XXIP000')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('XXIP000', 99, 4, 'XXIP000 - Periferica IP Generica', NULL, 99030, 35, 0, 'Periferica IP', 'TCP_Client', NULL, 'XXIP000.png', 1010, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 99, [VendorID] = 4, [DeviceTypeDescription] = 'XXIP000 - Periferica IP Generica', [WSUrlPattern] = NULL, [Order] = 99030, [TechnologyID] = 35, [Obsolete] = 0, [DefaultName] = 'Periferica IP', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'XXIP000.png', [GlobalOrder] = 1010, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'XXIP000'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'XXIP00FI')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('XXIP00FI', 2, 15, 'XXIP00FI - Monitor IP Fida Generico', NULL, 2061, 2, 0, 'Monitor', 'TCP_Client', NULL, 'XXIP00FI.png', 1041, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 15, [DeviceTypeDescription] = 'XXIP00FI - Monitor IP Fida Generico', [WSUrlPattern] = NULL, [Order] = 2061, [TechnologyID] = 2, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'XXIP00FI.png', [GlobalOrder] = 1041, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'XXIP00FI'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'XXIP00SO')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('XXIP00SO', 2, 3, 'XXIP00SO - Monitor IP Solari Generico', NULL, 2060, 2, 0, 'Monitor', 'TCP_Client', NULL, 'XXIP00SO.png', 1040, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 2, [VendorID] = 3, [DeviceTypeDescription] = 'XXIP00SO - Monitor IP Solari Generico', [WSUrlPattern] = NULL, [Order] = 2060, [TechnologyID] = 2, [Obsolete] = 0, [DefaultName] = 'Monitor', [PortType] = 'TCP_Client', [SnmpCommunity] = NULL, [ImageName] = 'XXIP00SO.png', [GlobalOrder] = 1040, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'XXIP00SO'


IF NOT EXISTS (SELECT [DeviceTypeID] FROM [dbo].[device_type] WHERE [DeviceTypeID] = 'XXSNMP0')
     INSERT INTO [dbo].[device_type] ([DeviceTypeID], [SystemID], [VendorID], [DeviceTypeDescription], [WSUrlPattern], [Order], [TechnologyID], [Obsolete], [DefaultName], [PortType], [SnmpCommunity], [ImageName], [GlobalOrder], [ExportToList], [DiscoveryType])
     VALUES ('XXSNMP0', 5, 4, 'XXSNMP0 - Periferica SNMP Generica', NULL, 99020, 36, 0, 'Periferica SNMP', 'TCP_Client', 'public', 'XXSNMP0.png', 1000, 1, 1)
ELSE
     UPDATE [dbo].[device_type] SET [SystemID] = 5, [VendorID] = 4, [DeviceTypeDescription] = 'XXSNMP0 - Periferica SNMP Generica', [WSUrlPattern] = NULL, [Order] = 99020, [TechnologyID] = 36, [Obsolete] = 0, [DefaultName] = 'Periferica SNMP', [PortType] = 'TCP_Client', [SnmpCommunity] = 'public', [ImageName] = 'XXSNMP0.png', [GlobalOrder] = 1000, [ExportToList] = 1, [DiscoveryType] = 1 WHERE [DeviceTypeID] = 'XXSNMP0'
----------------- END [device_type] -----------------------

----------------- operation_schedule_types -----------------------
IF NOT EXISTS (SELECT [OperationScheduleTypeId] FROM [dbo].[operation_schedule_types] WHERE [OperationScheduleTypeId] = 1)
     INSERT INTO [dbo].[operation_schedule_types] ([OperationScheduleTypeId], [OperationScheduleTypeCode], [AssemblyName])
     VALUES (1, 'Sms', 'GrisSuite.FormulaEngine.Sms')
ELSE
     UPDATE [dbo].[operation_schedule_types] SET [OperationScheduleTypeCode] = 'Sms', [AssemblyName] = 'GrisSuite.FormulaEngine.Sms' WHERE [OperationScheduleTypeId] = 1


IF NOT EXISTS (SELECT [OperationScheduleTypeId] FROM [dbo].[operation_schedule_types] WHERE [OperationScheduleTypeId] = 2)
     INSERT INTO [dbo].[operation_schedule_types] ([OperationScheduleTypeId], [OperationScheduleTypeCode], [AssemblyName])
     VALUES (2, 'Mail', 'GrisSuite.FormulaEngine.Mail')
ELSE
     UPDATE [dbo].[operation_schedule_types] SET [OperationScheduleTypeCode] = 'Mail', [AssemblyName] = 'GrisSuite.FormulaEngine.Mail' WHERE [OperationScheduleTypeId] = 2


IF NOT EXISTS (SELECT [OperationScheduleTypeId] FROM [dbo].[operation_schedule_types] WHERE [OperationScheduleTypeId] = 3)
     INSERT INTO [dbo].[operation_schedule_types] ([OperationScheduleTypeId], [OperationScheduleTypeCode], [AssemblyName])
     VALUES (3, 'StreamFieldMail', 'GrisSuite.FormulaEngine.StreamFieldMail')
ELSE
     UPDATE [dbo].[operation_schedule_types] SET [OperationScheduleTypeCode] = 'StreamFieldMail', [AssemblyName] = 'GrisSuite.FormulaEngine.StreamFieldMail' WHERE [OperationScheduleTypeId] = 3
----------------- END operation_schedule_types -----------------------

CREATE VIEW [dbo].[object_nodes]
AS
SELECT 
dbo.object_status.ObjectStatusId,
dbo.object_status.ObjectId,
dbo.object_status.SevLevel,
dbo.severity.Description AS SevLevelDescription,
dbo.object_status.SevLevelDetailId,
dbo.object_status.ParentObjectStatusId,
dbo.object_status.InMaintenance,
dbo.object_status.SevLevelReal,
dbo.object_status.Ok,
dbo.object_status.Warning,
dbo.object_status.Error,
dbo.object_status.Maintenance,
dbo.object_status.Unknown,
dbo.object_status.ForcedSevLevel,
dbo.object_status.LastUpdateGuid,
dbo.object_status.ExcludeFromParentStatus
FROM dbo.object_status 
INNER JOIN dbo.severity ON dbo.object_status.SevLevel = dbo.severity.SevLevel
WHERE (dbo.object_status.ObjectTypeId = 3);
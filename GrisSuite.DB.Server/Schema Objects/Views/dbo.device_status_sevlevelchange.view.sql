﻿
CREATE VIEW [dbo].[device_status_sevlevelchange] AS  
WITH Q1 as
(
       SELECT ROW_NUMBER() OVER (PARTITION BY  DevID ORDER BY Created DESC) as RowNumber,
			DevID, 
			SevLevel,
			Created,
			DevType
       FROM dbo.cache_device_status 
			INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
		where cache_device_status.offline = 0
),
Q2 as
(
       SELECT ROW_NUMBER() OVER (PARTITION BY DevID ORDER BY Created DESC) as RowNumber,
			DevID,
			SevLevel,
			Created,
			DevType
       FROM dbo.cache_device_status 
			INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
		where cache_device_status.offline = 0
)
SELECT Q2.DevID, Q2.SevLevel, Q2.Created, Q2.DevType
FROM Q1 INNER JOIN Q2 ON Q1.DevID = Q2.DevID AND Q1.RowNumber - 1 = Q2.RowNumber AND Q1.SevLevel <> Q2.SevLevel
UNION
SELECT Q3.DevID, SevLevel, Q3.Created, Q3.DevType 
       FROM dbo.cache_device_status 
			INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
			INNER JOIN (
						SELECT DevID, MIN(Created) as Created, DevType
						FROM dbo.cache_device_status 
						INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
						where cache_device_status.offline = 0
						GROUP BY DevID, DevType
						) as Q3 ON cache_device_status.DevId = Q3.DevID AND cache_log_messages.Created =  Q3.Created
;



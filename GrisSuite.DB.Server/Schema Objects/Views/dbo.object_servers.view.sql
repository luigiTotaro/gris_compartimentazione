CREATE VIEW dbo.object_servers
AS
SELECT     dbo.object_status.ObjectStatusId, dbo.object_status.ObjectId, dbo.object_status.SevLevel, dbo.severity.Description AS SevLevelDescription, 
                      dbo.object_status.SevLevelDetailId, dbo.object_status.ParentObjectStatusId, dbo.object_status.InMaintenance, dbo.object_status.SevLevelReal, 
                      dbo.object_status.SevLevelDetailIdReal, dbo.object_status.LastUpdateGuid, dbo.object_status.ExcludeFromParentStatus, dbo.object_status.ForcedByUser
FROM         dbo.object_status INNER JOIN
                      dbo.severity ON dbo.object_status.SevLevel = dbo.severity.SevLevel
WHERE     (dbo.object_status.ObjectTypeId = 5)
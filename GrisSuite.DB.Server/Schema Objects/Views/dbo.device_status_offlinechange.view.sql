﻿CREATE VIEW [dbo].[device_status_offlinechange] AS  
WITH Q1 as
(
       SELECT ROW_NUMBER() OVER (PARTITION BY  DevID ORDER BY Created DESC) as RowNumber,
			DevID, 
			Offline,
			Created
       FROM dbo.cache_device_status 
			INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
),
Q2 as
(
       SELECT ROW_NUMBER() OVER (PARTITION BY DevID ORDER BY Created DESC) as RowNumber,
			DevID,
			Offline,
			Created 
       FROM dbo.cache_device_status 
			INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
)
SELECT Q2.DevID, Q2.Offline, Q2.Created
FROM Q1 INNER JOIN Q2 ON Q1.DevID = Q2.DevID AND Q1.RowNumber - 1 = Q2.RowNumber AND Q1.Offline <> Q2.Offline
UNION
SELECT Q3.DevID, Offline, Q3.Created 
       FROM dbo.cache_device_status 
			INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
			INNER JOIN (
						SELECT DevID, MIN(Created) as Created
						FROM dbo.cache_device_status 
						INNER JOIN dbo.cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID
						GROUP BY DevID
						) as Q3 ON cache_device_status.DevId = Q3.DevID AND cache_log_messages.Created =  Q3.Created
;



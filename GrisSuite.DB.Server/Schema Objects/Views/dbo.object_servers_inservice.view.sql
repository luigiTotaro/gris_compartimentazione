CREATE VIEW [dbo].[object_servers_inservice]
AS
SELECT SrvID as ObjectId
	, NodID
	, InMaintenance
	, SevLevel
	, SevLevelDescription
	, SevLevelDetailId
	, SevLevelReal
	, SevLevelDetailIdReal
FROM dbo.servers_status
WHERE (SevLevelDetailId <> 11); -- che hanno periferiche con stato associate

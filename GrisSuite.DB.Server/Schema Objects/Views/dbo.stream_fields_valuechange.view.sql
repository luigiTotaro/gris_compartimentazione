﻿

CREATE VIEW [dbo].[stream_fields_valuechange] AS  
WITH Q1 as
(
       SELECT ROW_NUMBER() OVER (PARTITION BY  DevID, StrID, FieldID, ArrayID ORDER BY Created DESC) as RowNumber,
			DevID, 
			StrID,
			FieldID,
			ArrayID,
			Value,
			SevLevel,
			Name,
			Created,
			DevType
       FROM dbo.cache_stream_fields
			INNER JOIN dbo.cache_log_messages ON cache_stream_fields.LogID = cache_log_messages.LogID
	   WHERE visible = 1
	   -- si potrebbe testare se in quel momento la device non era offline, da verificare se fattibile come performance
),
Q2 as
(
       SELECT ROW_NUMBER() OVER (PARTITION BY  DevID, StrID, FieldID, ArrayID ORDER BY Created DESC) as RowNumber,
			DevID, 
			StrID,
			FieldID,
			ArrayID,
			Value,
			SevLevel,
			Name,
			Created,
			DevType
       FROM dbo.cache_stream_fields
			INNER JOIN dbo.cache_log_messages ON cache_stream_fields.LogID = cache_log_messages.LogID
	   WHERE visible = 1
)
SELECT Q2.DevID
		, Q2.DevType
		, Q2.StrID 
		, Q2.FieldID
		, Q2.ArrayID
		, Q2.Value
		, Q2.SevLevel
		, Q2.Name
		, Q2.Created
FROM Q1 INNER JOIN Q2 ON Q1.DevID = Q2.DevID 
							AND Q1.DevType = Q2.DevType 
							AND Q1.StrID = Q2.StrID
							AND Q1.FieldID = Q2.FieldID 
							AND Q1.ArrayID = Q2.ArrayID 
							AND Q1.RowNumber - 1 = Q2.RowNumber 
							AND Q1.Value <> Q2.Value
UNION
SELECT Q3.DevID
		, Q3.DevType 
		, Q3.StrID 
		, Q3.FieldID
		, Q3.ArrayID
		, cache_stream_fields.Value
		, cache_stream_fields.SevLevel
		, cache_stream_fields.Name
		, Q3.Created
       FROM dbo.cache_stream_fields 
			INNER JOIN dbo.cache_log_messages ON cache_stream_fields.LogID = cache_log_messages.LogID
			INNER JOIN (
						SELECT DevID, StrID, FieldID, ArrayID, MIN(Created) as Created, DevType
						FROM dbo.cache_stream_fields 
						INNER JOIN dbo.cache_log_messages ON cache_stream_fields.LogID = cache_log_messages.LogID
						where cache_stream_fields.visible = 1
						GROUP BY DevID, StrID, FieldID, ArrayID, DevType
						) as Q3 ON cache_stream_fields.DevId = Q3.DevID 
										AND cache_stream_fields.StrID = Q3.StrID
										AND cache_stream_fields.FieldID = Q3.FieldID 
										AND cache_stream_fields.ArrayID = Q3.ArrayID 
										AND cache_log_messages.Created =  Q3.Created
;
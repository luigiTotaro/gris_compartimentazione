CREATE VIEW [dbo].[servers_status]
AS
WITH
ServersWithNode AS
(
SELECT     SrvID, Name, Host, FullHostName, IP, LastUpdate, LastMessageType, SupervisorSystemXML, ClientSupervisorSystemXMLValidated, 
                      ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ServerSupervisorSystemXMLValidated, ServerValidationSign, 
                      ServerDateValidationRequested, ServerDateValidationObtained, ClientKey, AccountDocValidation, DateDocValidation, AccountUTValidation, 
                      DateUTValidation, NodID, ServerVersion, MAC
FROM servers
WHERE not NodId is null
UNION ALL
SELECT SrvID, Name, Host, FullHostName, IP, LastUpdate, LastMessageType, SupervisorSystemXML, ClientSupervisorSystemXMLValidated, 
                      ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ServerSupervisorSystemXMLValidated, ServerValidationSign, 
                      ServerDateValidationRequested, ServerDateValidationObtained, ClientKey, AccountDocValidation, DateDocValidation, AccountUTValidation, 
                      DateUTValidation,  
					  (SELECT  TOP 1  NodID FROM devices WHERE SrvID = servers.SrvID) as NodID, 
					  ServerVersion, MAC
FROM servers
WHERE NodId is null
)
SELECT ServersWithNode.*
	, InMaintenance
	, SevLevel
	, SevLevelDetailId
	, SevLevelDescription
	, SevLevelReal
	, SevLevelDetailIdReal  
FROM ServersWithNode
		INNER JOIN object_servers ON ServersWithNode.SrvId = object_servers.ObjectID;
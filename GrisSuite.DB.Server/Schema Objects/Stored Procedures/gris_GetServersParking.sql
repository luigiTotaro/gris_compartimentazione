﻿CREATE PROCEDURE [gris_GetServersParking] (@IP VARCHAR(16), @SrvID BIGINT, @Host VARCHAR(64), @HardwareProfile VARCHAR(64), @ParkingType INT) AS
SET NOCOUNT ON;

SELECT
ServerParkingID,
IP,
SrvID,
Host,
Name,
[Type],
LastUpdate,
ClientRegID,
ClientZonID,
ClientNodID,
HardwareProfile,
ParkingType,
dbo.GetParkingTypeDescription(ParkingType) AS ParkingTypeDescription
FROM servers_parking
WHERE
(IP LIKE @IP)
AND ((@SrvID = -1) OR (SrvID = @SrvID))
AND (Host LIKE @Host)
AND (HardwareProfile LIKE @HardwareProfile)
AND (((@ParkingType = - 1) AND (ParkingType <> 3 /* 'Server storico, che ha centralizzato almeno una volta' */)) OR (ParkingType = @ParkingType))
ORDER BY Host
﻿CREATE PROCEDURE [dbo].[gris_InsStazioniAVFormulaGroup] (@ZonID BIGINT) AS
SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @VirtualObjectId AS BIGINT;
DECLARE @VirtualObjectStatusId AS UNIQUEIDENTIFIER;
DECLARE @ParentZoneStatusId AS UNIQUEIDENTIFIER;

SELECT @ParentZoneStatusId = object_zones.ObjectStatusId
FROM zones
INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId
WHERE zones.ZonID = @ZonID;

SELECT @VirtualObjectId = vo.VirtualObjectID
FROM object_virtuals ov
INNER JOIN virtual_objects vo ON ov.ObjectId = vo.VirtualObjectID
WHERE ov.ParentObjectStatusId = @ParentZoneStatusId
AND vo.VirtualObjectName LIKE 'Stazioni AV';

IF @VirtualObjectId IS NULL
BEGIN
	BEGIN TRAN;
	
	-- creazione virtual object
	EXEC gris_InsVirtualObject @ParentZoneStatusId, 'Stazioni AV', 'Stazioni Alta Velocità', @VirtualObjectId OUTPUT, @VirtualObjectStatusId OUTPUT;
	
	-- creazione formule di default per il raggruppamento
	INSERT INTO object_formulas VALUES (@VirtualObjectStatusId, 0, '.\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py', 0);
	INSERT INTO object_formulas VALUES (@VirtualObjectStatusId, 1, '.\Lib\GroupScripts\GroupWith_AnyElemIn_W_E_O_Get_E.py', 0);			
		
	COMMIT TRAN;
END
CREATE PROCEDURE [dbo].[util_GetFindSTLC1000]
@SrvID AS int,
@IP AS varchar(16),
@FullHostName as varchar(256),
@MAC as varchar(16),
@Stazione as varchar(64)
AS
SELECT     servers_status.SrvID, servers_status.IP, servers_status.FullHostName, servers_status.MAC, nodes.Name AS Stazione, zones.Name AS Linea, regions.Name AS Compartimento, 
                      servers_status.LastUpdate, servers_status.LastMessageType, ~servers_status.InMaintenance as InExercise, servers_status.ServerVersion
FROM         zones LEFT OUTER JOIN
                      regions ON zones.RegID = regions.RegID RIGHT OUTER JOIN
                      nodes ON zones.ZonID = nodes.ZonID RIGHT OUTER JOIN
                      servers_status ON nodes.NodID = servers_status.NodID
WHERE  (servers_status.SrvID = @SrvID OR @SrvID = '') 
AND (servers_status.IP = @IP OR @IP = '') 
AND (servers_status.FullHostName = @FullHostName OR @FullHostName = '') 
AND (servers_status.MAC = @MAC OR @MAC = '') 
AND (nodes.Name LIKE @Stazione OR @Stazione = '')
RETURN



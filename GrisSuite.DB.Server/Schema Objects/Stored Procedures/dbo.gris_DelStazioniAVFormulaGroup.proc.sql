﻿CREATE PROCEDURE [dbo].[gris_DelStazioniAVFormulaGroup] (@ZonID BIGINT) AS
SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @VirtualObjectId AS BIGINT;
DECLARE @VirtualObjectStatusId AS UNIQUEIDENTIFIER;
DECLARE @ParentZoneStatusId AS UNIQUEIDENTIFIER;
DECLARE @VirtualObjectIdStazioniGeneriche AS BIGINT;

SELECT @ParentZoneStatusId = object_zones.ObjectStatusId
FROM zones
INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId
WHERE zones.ZonID = @ZonID;

SELECT @VirtualObjectId = vo.VirtualObjectID, @VirtualObjectStatusId = ov.ObjectStatusId
FROM object_virtuals ov
INNER JOIN virtual_objects vo ON ov.ObjectId = vo.VirtualObjectID
WHERE ov.ParentObjectStatusId = @ParentZoneStatusId
AND vo.VirtualObjectName LIKE 'Stazioni AV';

SELECT @VirtualObjectIdStazioniGeneriche = vo.VirtualObjectID
FROM object_virtuals ov
INNER JOIN virtual_objects vo ON ov.ObjectId = vo.VirtualObjectID
WHERE ov.ParentObjectStatusId = @ParentZoneStatusId
AND vo.VirtualObjectName LIKE 'Stazioni Generiche';

IF @VirtualObjectId IS NOT NULL AND @VirtualObjectStatusId IS NOT NULL
BEGIN
	BEGIN TRAN;
	
	-- cancellazione attributi del raggruppamento
	DELETE FROM object_attributes WHERE ObjectStatusId = @VirtualObjectStatusId;
	
	-- cancellazione formule di default per il raggruppamento
	DELETE FROM object_formulas WHERE ObjectStatusId = @VirtualObjectStatusId;

	-- stazioni da spostare sotto il gruppo delle Stazioni Generiche
	DECLARE @ListaFigliVirtualObject AS VARCHAR(MAX);
	
	SELECT @ListaFigliVirtualObject = COALESCE(@ListaFigliVirtualObject + '|', '') + CAST(ObjectId AS VARCHAR(20))
	FROM object_status
	WHERE ParentObjectStatusId = @VirtualObjectStatusId;

	-- riversamento di tutti i nodi associati nel gruppo 'Stazioni Generiche'
	SET @ListaFigliVirtualObject = '|' + @ListaFigliVirtualObject + '|';
	EXEC gris_UpdAssociateToVirtualObject @VirtualObjectIdStazioniGeneriche, @ListaFigliVirtualObject, 3 /* nodes */;
	
	-- cancellazione virtual object
	EXEC gris_DelVirtualObject @ParentZoneStatusId, 'Stazioni AV';
		
	COMMIT TRAN;
END
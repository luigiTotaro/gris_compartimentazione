﻿CREATE PROCEDURE [dbo].[gris_DelDeviceFilters] (@DeviceFilterId BIGINT) AS
BEGIN
  SET NOCOUNT ON;
  
  DECLARE @ResourceID INT
  DECLARE @DeviceFilterName VARCHAR(110)
  
  SELECT @ResourceID = ResourceId, @DeviceFilterName=DeviceFilterName FROM device_filters WHERE DeviceFilterId = @DeviceFilterId

  IF (@ResourceID IS NOT NULL) BEGIN
      EXEC gris_UpdDeviceFilters_share @DeviceFilterName, 0
  END 
  
  DELETE FROM device_filters
  WHERE DeviceFilterId = @DeviceFilterId
END
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 03-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributeValues_Ins]
	@TargetValueID INT OUTPUT,
	@TargetAttributeID INT,
	@TargetID BIGINT, 
	@Value VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO geta.target_values (TargetAttributeID, TargetID, [Value]) VALUES (@TargetAttributeID, @TargetID, @Value);
	
	SET @TargetValueID = CAST(SCOPE_IDENTITY() AS INT);
END



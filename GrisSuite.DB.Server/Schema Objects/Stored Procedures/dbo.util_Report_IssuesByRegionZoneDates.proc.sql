﻿CREATE PROCEDURE [dbo].[util_Report_IssuesByRegionZoneDates] (@Compartimento AS VARCHAR(64), @Linea AS VARCHAR(64), @InterventiDal AS VARCHAR(10), @InterventiAl AS VARCHAR(10)) AS
SET NOCOUNT ON;
  
DECLARE @RegID BIGINT, @ZonID BIGINT, @DateIssueFrom DATETIME, @DateIssueTo DATETIME;

IF ISNULL(@Compartimento,'') = ''
   SELECT @RegID = -1;
ELSE
   SELECT TOP 1 @RegID = RegID FROM regions WHERE [name] like '%' + @Compartimento + '%';

IF ISNULL(@Linea,'') = ''
   SELECT @ZonID = -1;
ELSE
   SELECT TOP 1 @ZonID = ZonID FROM zones WHERE [name] like '%' + @Linea + '%';

IF ISNULL(@InterventiDal, '') = ''
	SELECT @DateIssueFrom = NULL;
ELSE
BEGIN
	BEGIN TRY
		SELECT @DateIssueFrom = CONVERT(DATETIME, @InterventiDal, 105);
	END TRY
	BEGIN CATCH
		SELECT @DateIssueFrom = NULL;
	END CATCH;
END

IF ISNULL(@InterventiAl, '') = ''
	SELECT @DateIssueTo = NULL;
ELSE
BEGIN
	BEGIN TRY
		SELECT @DateIssueTo = CONVERT(DATETIME, @InterventiAl, 105);
	END TRY
	BEGIN CATCH
		SELECT @DateIssueTo = NULL;
	END CATCH;
END	

EXEC [dbo].[gris_Report_IssuesByRegionZoneDates] @RegID, @ZonID, @DateIssueFrom, @DateIssueTo
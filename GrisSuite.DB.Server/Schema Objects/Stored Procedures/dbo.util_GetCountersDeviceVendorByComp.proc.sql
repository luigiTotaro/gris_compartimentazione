CREATE PROCEDURE [dbo].[util_GetCountersDeviceVendorByComp]
@Comp VARCHAR (64), @Systems VARCHAR (MAX)
AS
SET @Systems = ISNULL(@Systems,'')

SELECT Compartimento, Sistema, Produttore, Tecnologia, 
	[In servizio], [Anomalia lieve],[Anomalia grave],[In Configurazione],[Sconosciuto]+[Offline] as [Sconosciuto],[Non classificato],
	[In servizio]+[Anomalia lieve]+[Anomalia grave]+[In Configurazione]+[Sconosciuto]+[Offline]+[Non classificato] as [Totale]
FROM 
(
select r1.Name as Compartimento, 
		y.SystemDescription as Sistema, 
		v.VendorName as Produttore,
		ISNULL(h.TechnologyDescription,'') AS Tecnologia, 
		d.[Type] as Tipo, 
		dbo.GetDeviceStatusDesc(d.devid,0) as Stato
from devices d 
	inner join device_status s on s.DevId = d.DevId 
	left join severity e on s.SevLevel = e.SevLevel
	left join device_type t on d.type = t.DeviceTypeID
	left join vendors v on t.VendorID = v.VendorID
	left join systems y on t.SystemID = y.SystemID
	left join technology h on t.TechnologyID = h.TechnologyID
	left join servers r on d.SrvID = r.SrvID
    left join nodes n1 on d.NodID = n1.NodID
    left join zones z1 on n1.ZonID = z1.ZonID
    left join regions r1 on z1.RegID = r1.RegID
where r1.Name Like '%'+ @Comp +'%'
	and (@Systems = '' OR  CHARINDEX('|' + ISNULL(y.SystemDescription,'Altre Periferiche') + '|' ,@Systems,1) > 0)

) ps
PIVOT
(
Count (Tipo)
FOR Stato IN
( [In servizio], [Anomalia lieve],[Anomalia grave],[In Configurazione],[Sconosciuto],[Offline],[Non classificato] )
) AS pvt
return


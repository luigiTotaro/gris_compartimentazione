﻿CREATE PROCEDURE [dbo].[tf_GetSTLCParameters]
AS
	SELECT SrvID, ParameterName, ParameterValue, ParameterDescription
	FROM stlc_parameters
RETURN 0;
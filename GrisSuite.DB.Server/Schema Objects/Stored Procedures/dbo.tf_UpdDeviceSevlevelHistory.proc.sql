﻿CREATE PROCEDURE [dbo].[tf_UpdDeviceSevlevelHistory](
	 @HistoryDays int = 365
   , @NewCacheDays int = 1)
AS
BEGIN
	DECLARE @StartDate datetime;
	DECLARE @EndDate datetime;

	-- cancella i messaggi pià vecchi di @HistoryDays
	DECLARE @HistoryMinutes int;
	SET @HistoryMinutes = @HistoryDays * 1440;
	DELETE FROM history_device_sevlevel
	WHERE DATEDIFF(minute, Created, GETDATE()) > @HistoryMinutes;

	-- calcola l'intervallo di date da elaborare
	SET @StartDate = (SELECT MAX(Created)FROM history_device_sevlevel);

	IF(@StartDate IS NULL)
		BEGIN
			SET @StartDate = (SELECT MIN(Created)
							  FROM cache_device_status DS INNER JOIN cache_log_messages LM ON DS.LogID = LM.LogID);
		END;
	SET @EndDate = DATEADD(minute, @NewCacheDays * 1440, @StartDate);

	IF (NOT EXISTS(SELECT * FROM dbo.device_status_sevlevelchange_all WHERE Created > @StartDate AND Created <= @EndDate))
		BEGIN
			SET @StartDate = (SELECT MIN(Created)
							  FROM cache_device_status DS INNER JOIN cache_log_messages LM ON DS.LogID = LM.LogID AND Created > @StartDate);
		END;
	SET @EndDate = DATEADD(minute, @NewCacheDays * 1440, @StartDate);

	PRINT 'Elabora da ' + CONVERT(varchar, @StartDate, 120) + ' a ' + CONVERT(varchar, @EndDate, 120);

	IF OBJECT_ID('tempdb..#t')IS NOT NULL
		BEGIN
			DROP TABLE #t;
		END;

	SELECT ROW_NUMBER()OVER(PARTITION BY DevID ORDER BY Created DESC)AS RowNumber
		 , * INTO #t
	FROM dbo.device_status_sevlevelchange_all
	WHERE Created > @StartDate AND Created <= @EndDate;

	WITH D
		AS (SELECT Q2.DevID
				 , Q2.SevLevel
				 , Q2.Created
				 , Q2.DevType
				 , DATEDIFF(second, Q2.Created, Q1.Created)AS Duration
			FROM #t Q1 INNER JOIN #t Q2 ON Q1.DevID = Q2.DevID AND Q1.RowNumber = Q2.RowNumber - 1
			UNION
			SELECT Q2.DevID
				 , Q2.SevLevel
				 , Q2.Created
				 , Q2.DevType
				 , NULL AS Duration
			FROM #t Q1 RIGHT JOIN #t Q2 ON Q1.DevID = Q2.DevID AND Q1.RowNumber = Q2.RowNumber - 1
			WHERE Q1.DevID IS NULL)
		INSERT INTO history_device_sevlevel(DevID
										  , SevLevel
										  , Created
										  , DevType
										  , Duration)
		SELECT DevID
			 , SevLevel
			 , Created
			 , DevType
			 , Duration FROM D;

	-- aggiunge nodid e/o srvid per le device che non ce l'hanno già
	-- faccio la join anche per tipo per essere sicuri che nel frattempo non sia stato utilizzato lo stesso DevID per un'altra device di altro tipo.
	UPDATE history_device_sevlevel
	SET NodId = devices.NodID
	  , SrvId = devices.SrvID
	FROM history_device_sevlevel INNER JOIN devices ON history_device_sevlevel.DevID = devices.DevID
												   AND history_device_sevlevel.DevType = devices.Type
	WHERE Created >= @StartDate;

	-- calcola le durate degli stati lasciati in sospeso l'elaborazione precedente.
	DECLARE @StartDateUpdate datetime;
	SET @StartDateUpdate = DATEADD(minute, @HistoryDays * -1440, @StartDate);
	WITH H
		AS (SELECT ROW_NUMBER()OVER(PARTITION BY DevID ORDER BY Created DESC)AS RowNumber
				 , *
			FROM history_device_sevlevel
			WHERE Created >= @StartDateUpdate)
		UPDATE history_device_sevlevel
		SET Duration = DATEDIFF(second, Q2.Created, Q1.Created)
		FROM H Q1 INNER JOIN H Q2 ON Q1.DevID = Q2.DevID AND Q1.RowNumber = Q2.RowNumber - 1
				  INNER JOIN history_device_sevlevel ON history_device_sevlevel.ID = Q2.ID
		WHERE history_device_sevlevel.Duration IS NULL;
END
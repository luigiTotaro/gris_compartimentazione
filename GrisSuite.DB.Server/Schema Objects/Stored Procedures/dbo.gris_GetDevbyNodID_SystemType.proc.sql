﻿CREATE PROCEDURE [dbo].[gris_GetDevbyNodID_SystemType] 
	@NodID varchar(64),	
	@SystemID int,
    @MinuteTimeOut int = 0
AS
BEGIN
	/******************** OBSOLETA **********************/
	SET NOCOUNT ON;

	SELECT
		d.[name] AS Periferica,
		ds.DevID,
		s.host AS STLC1000,
		(CASE WHEN ((DATEDIFF(n, ISNULL(s.LastUpdate, GETDATE()), GETDATE()) < @MinuteTimeOut) OR (@MinuteTimeOut = 0)) THEN 1 ELSE 0 END) AS STLC1000_Alive,
		ds.Offline,
		(
			SELECT MAX(stream_fields.SevLevel)
			FROM device_status 
			INNER JOIN stream_fields ON device_status.DevID = stream_fields.DevID
			WHERE (device_status.DevID = ds.DevID) 
			AND (stream_fields.Visible = 1) 
			AND (stream_fields.SevLevel <> 255)
		) AS SevLevel,
		n.[Name] AS Stazione,
		d.[type]
	FROM devices d, device_status ds, servers s, nodes n
	WHERE d.devid = ds.devid 
	AND d.srvid = s.srvid 
	AND d.nodid = n.nodid
	AND d.[type] IN (SELECT DeviceTypeID FROM device_type where SystemID = @SystemID)
	AND d.nodid = @NodID
END



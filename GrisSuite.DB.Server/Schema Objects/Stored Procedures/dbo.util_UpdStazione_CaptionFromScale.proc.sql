﻿CREATE PROCEDURE util_UpdStazione_CaptionFromScale( @NomeStazione VARCHAR(64), @CaptionFromScale float)
AS
BEGIN

	update dbo.object_classification_values
	set CaptionFromScale = @CaptionFromScale
	from  dbo.object_classification_values ocv
		inner join dbo.object_nodes onode on ocv.objectstatusid = onode.objectstatusid
		inner join dbo.nodes  on nodes.nodid = onode.objectid
	where name = @NomeStazione
	
	return @@ROWCOUNT 

END
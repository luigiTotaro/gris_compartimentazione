﻿CREATE PROCEDURE [dbo].[gris_InsParameters]
(
	@ParameterName varchar(64),
	@ParameterValue varchar(256),
	@ParameterDescription varchar(1024)
)
AS
	SET NOCOUNT OFF;
	
	INSERT INTO [parameters] ([ParameterName], [ParameterValue], [ParameterDescription]) VALUES (@ParameterName, @ParameterValue, @ParameterDescription);
	SELECT ParameterName, ParameterValue, ParameterDescription FROM [parameters] WHERE (ParameterName = @ParameterName)
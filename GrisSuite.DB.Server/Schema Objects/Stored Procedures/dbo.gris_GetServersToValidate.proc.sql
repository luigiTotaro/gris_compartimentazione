﻿CREATE PROCEDURE [dbo].[gris_GetServersToValidate]
AS
	SELECT SrvID, FullHostName, IP, ServerDateValidationRequested
	FROM servers
	WHERE (ServerDateValidationRequested IS NOT NULL) AND (ServerDateValidationRequested > ISNULL(ServerDateValidationObtained, '19000101'));



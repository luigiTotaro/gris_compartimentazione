﻿-- =============================================
-- Author:        Daniele Daffini
-- Create date: 14/11/2008
-- Sposta tutte le Issue da un SrvId ad un altro
-- =============================================
CREATE PROCEDURE [dbo].[util_Replace_SrvId]
    @Original_SrvID AS INT,
    @New_SrvID AS INT  
AS
BEGIN
	SET XACT_ABORT ON;

	BEGIN TRAN;

    UPDATE geta.issues
    SET TargetID = @New_SrvID
    WHERE TargetID = @Original_SrvID  
    
	EXEC dbo.gris_DelAllbySrvID @Original_SrvID
    
    COMMIT TRAN;
END



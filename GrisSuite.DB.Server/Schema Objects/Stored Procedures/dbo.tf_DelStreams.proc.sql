﻿/****** Object:  StoredProcedure [dbo].[tf_DelStreams]    Script Date: 11/20/2006 18:18:55 ******/
CREATE PROCEDURE [dbo].[tf_DelStreams]
(
	@Original_DevID bigint,
	@Original_StrID int
)
AS
	SET NOCOUNT ON;

	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] = @Original_DevID) AND ([DeltaStrID] = @Original_StrID);

	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN (SELECT reference.ReferenceID FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
							WHERE ([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID)
	);
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID);

	DELETE FROM [stream_fields] WHERE (([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID));
	DELETE FROM [streams] WHERE (([DevID] = @Original_DevID) AND ([StrID] = @Original_StrID));



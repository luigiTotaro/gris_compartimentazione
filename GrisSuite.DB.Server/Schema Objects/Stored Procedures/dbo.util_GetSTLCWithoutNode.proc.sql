﻿CREATE PROCEDURE util_GetSTLCWithoutNode
AS
SELECT s.SrvID,Ip,MAC, Stazione as StazioneDellePeriferiche, z1.Name as Linea, r1.Name as Compartimento
FROM 
SERVERS s left join
(SELECT DISTINCT SrvID, nodes.Name as Stazione, ZonId
 FROM nodes inner join devices on nodes.nodid = devices.devid) dn
on s.SrvId = dn.SrvId
    left join zones z1 on dn.ZonID = z1.ZonID
    left join regions r1 on z1.RegID = r1.RegID
WHERE s.nodid IS NULL
RETURN



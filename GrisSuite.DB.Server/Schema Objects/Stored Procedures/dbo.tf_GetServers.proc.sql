﻿CREATE PROCEDURE [dbo].[tf_GetServers]
AS
	SET NOCOUNT ON;
	SELECT 
		SrvID, [Name], Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML, 
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers



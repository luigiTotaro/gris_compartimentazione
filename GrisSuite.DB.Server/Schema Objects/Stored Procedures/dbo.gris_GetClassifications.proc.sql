﻿CREATE PROCEDURE [dbo].[gris_GetClassifications] AS
SET NOCOUNT ON;

SELECT ClassificationId, ClassificationName, CanFilterBySystemId
FROM classifications
WHERE (IsVisible = 1)
ORDER BY ClassificationId
﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	Device nello stato x da più di n minuti
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsRuleDevicesOnSevLevel]
	@RuleID int = 0, 
	@SevLevel int = 0,
	@Minute int = 0
AS
BEGIN
WITH LastStatus 
AS (
	SELECT MAX(device_status_sevlevelchange.Created) AS  [LastStatusDate], device_status_sevlevelchange.DevID
	FROM device_status_sevlevelchange
		INNER JOIN device_status ON device_status_sevlevelchange.DevId = device_status.DevId
									AND device_status.SevLevel = device_status_sevlevelchange.SevLevel 
	WHERE (device_status.SevLevel = @SevLevel) AND (device_status.Offline = 0)
	GROUP BY device_status_sevlevelchange.DevID
	),
	LastDeviceUpdate
	AS (
	SELECT servers.LastUpdate, devices.DevId
	FROM devices INNER JOIN servers ON devices.SrvID = servers.SrvID
				 INNER JOIN device_status ON devices.DevId = device_status.DevId
	WHERE (device_status.Offline = 0)
	)
	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT DISTINCT LastDeviceUpdate.DevID as DevId, @RuleID AS RuleID
	FROM LastDeviceUpdate INNER JOIN LastStatus ON LastDeviceUpdate.DevID = LastStatus.DevID
	WHERE DATEDIFF(minute,ISNULL(LastStatusDate,'1900-01-01'),ISNULL(LastUpdate,'1900-01-01'))  > 60
END



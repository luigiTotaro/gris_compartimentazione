﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 21-03-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[util_GetPZNeverOnline] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT regions.Name as RegName, zones.Name as ZoneName, nodes.Name as NodeName, devices.DevID, devices.Name as PZ, servers.IP as IP_STLC
	FROM devices 
	INNER JOIN device_status ON devices.DevID = device_status.DevID 
	LEFT JOIN stream_fields ON devices.DevID = stream_fields.DevID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN servers ON servers.SrvID = devices.SrvID
	WHERE (device_status.Offline = 1)
	AND ((devices.type = 'TT10210') OR (devices.type = 'TT10210V3') OR (devices.type = 'TT10220'))
	AND (stream_fields.DevID IS NULL);
END



﻿CREATE PROCEDURE gris_UpdServerLicensing (@HardwareProfile varchar(64), @LicensingLastUpdateUser varchar(100), @LicensingDataClear varchar(max)) AS
SET NOCOUNT ON;

DECLARE @ServerLicensingId INT;
SET @ServerLicensingId = (SELECT ServerLicensingId FROM servers_licensing WHERE (HardwareProfile LIKE @HardwareProfile));

IF (@ServerLicensingId IS NULL)
BEGIN
	INSERT INTO servers_licensing (LicensingLastUpdate, LicensingLastUpdateUser, HardwareProfile, LicensingData)
	VALUES (GetDate(), ISNULL(@LicensingLastUpdateUser, ''), ISNULL(@HardwareProfile, ''), dbo.fn_EncryptDecryptString(@LicensingDataClear));
END
ELSE
BEGIN
	UPDATE servers_licensing
	SET
	LicensingLastUpdate = GetDate(),
	LicensingLastUpdateUser = ISNULL(@LicensingLastUpdateUser, ''),
	LicensingData = dbo.fn_EncryptDecryptString(@LicensingDataClear)
	WHERE (ServerLicensingId = @ServerLicensingId);
END
﻿CREATE PROCEDURE [dbo].[gris_GetGVFields]
@DevID BIGINT, @StrID BIGINT, @ViewUnknowns BIT, @ViewErrors BIT, @ViewWarnings BIT, @ViewOKs BIT, @ViewInfos BIT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		devices.DevID, stream_fields.FieldID, stream_fields.StrID, stream_fields.ArrayID, 
		stream_fields.Name, stream_fields.SevLevel,
		REPLACE(REPLACE(REPLACE(stream_fields.Value, CHAR(13) COLLATE SQL_Latin1_General_CP1_CI_AS, ''), CHAR(10), ''), CHAR(0), '') AS Value,
		stream_fields.Description, 
		streams.Name AS StreamName, streams.DateTime AS [Date],
		(CASE WHEN stream_fields.SevLevel = 255 THEN 1 ELSE 0 END) AS ExtraSortOrder
	FROM devices 
	INNER JOIN stream_fields ON devices.DevID = stream_fields.DevID 
	INNER JOIN streams ON stream_fields.DevID = streams.DevID AND stream_fields.StrID = streams.StrID  
	WHERE (devices.DevID = @DevID) 
	AND (stream_fields.Visible = 1) 
	AND (stream_fields.StrID IN 
			(
				SELECT StrID FROM streams AS streams_1 WHERE (DevID = @DevID) AND (StrID = @StrID) AND (Visible = 1))
			) 
	AND ((stream_fields.SevLevel <> 0) OR (@ViewOKs = 1))
	AND ((stream_fields.SevLevel <> 1) OR (@ViewWarnings = 1))
	AND ((stream_fields.SevLevel <> 2) OR (@ViewErrors = 1))
	AND ((stream_fields.SevLevel <> 255) OR (@ViewUnknowns = 1))
	AND ((stream_fields.SevLevel <> -255) OR (@ViewInfos = 1))
	ORDER BY stream_fields.StrID, FieldID, ArrayID
END



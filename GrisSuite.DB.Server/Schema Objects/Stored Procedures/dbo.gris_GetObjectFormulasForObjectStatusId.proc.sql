﻿CREATE PROCEDURE gris_GetObjectFormulasForObjectStatusId (@ObjectStatusId UNIQUEIDENTIFIER) AS
SET NOCOUNT ON;

SELECT
object_status.ObjectStatusId,
object_status.ObjectId,
object_status.ObjectTypeId,
ISNULL((SELECT FormulaIndex FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 0)), 0) AS FormulaIndex0,
ISNULL((SELECT ScriptPath FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 0)), '') AS ScriptPath0,
ISNULL((SELECT FormulaIndex FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 1)), 1) AS FormulaIndex1,
ISNULL((SELECT ScriptPath FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 1)), '') AS ScriptPath1,
ISNULL((SELECT FormulaIndex FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 2)), 2) AS FormulaIndex2,
ISNULL((SELECT ScriptPath FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 2)), '') AS ScriptPath2,
ISNULL((SELECT FormulaIndex FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 3)), 3) AS FormulaIndex3,
ISNULL((SELECT ScriptPath FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 3)), '') AS ScriptPath3,
ISNULL((SELECT FormulaIndex FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 4)), 4) AS FormulaIndex4,
ISNULL((SELECT ScriptPath FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = 4)), '') AS ScriptPath4
FROM object_status
WHERE (object_status.ObjectStatusId = @ObjectStatusId)
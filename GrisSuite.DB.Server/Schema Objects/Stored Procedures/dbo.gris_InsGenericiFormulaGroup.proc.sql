﻿
CREATE procedure [dbo].[gris_InsGenericiFormulaGroup]
(
	@SystemID as int,
	@NodID as bigint
) as
begin
	set xact_abort on;

	declare @VirtualObjectId as bigint;
	declare @VirtualObjectStatusId as uniqueidentifier;
	declare @NodeSystemId as bigint;
	declare @ParentNodeSystemStatusId as uniqueidentifier;

	select @NodeSystemId = ns.NodeSystemsId, @ParentNodeSystemStatusId = ObjectStatusId
	from node_systems ns
	inner join object_node_systems ons on ns.NodeSystemsId = ons.ObjectId
	where ns.NodId = @NodID and ns.SystemId = @SystemID;

	select @VirtualObjectId = vo.VirtualObjectID
	from object_virtuals ov
	inner join virtual_objects vo on ov.ObjectId = vo.VirtualObjectID
	where ov.ParentObjectStatusId = @ParentNodeSystemStatusId
	and vo.VirtualObjectName like 'Generici';

	if @VirtualObjectId is null
		begin	
			begin tran
			
			-- creazione virtual object
			exec gris_InsVirtualObject @ParentNodeSystemStatusId, 'Generici', 'Dispositivi Generici', @VirtualObjectId output, @VirtualObjectStatusId output;

			-- disassociazione delle periferiche dal sistema e associazione al virtual object			
			update object_devices set ParentObjectStatusId = @VirtualObjectStatusId
			from object_devices
			inner join devices on DevID = ObjectId
			inner join device_type on DeviceTypeID = [Type]
			where object_devices.ParentObjectStatusId = @ParentNodeSystemStatusId
			and SystemID = @SystemID; -- constraint per evitare spostamenti di periferiche con parent inconsistente
			
			-- creazione formule di default per il raggruppamento
			insert into object_formulas values (@VirtualObjectStatusId, 0, '.\Lib\GroupScripts\GetWeightedAverageMaxWarning.py', 0);

			-- aggiornamento formula del sistema padre
			update object_formulas set ScriptPath = '.\Lib\GroupScripts\GetWorst.py' where ObjectStatusId = @ParentNodeSystemStatusId and FormulaIndex = 0

			-- aggiornamento formula del nodo padre (in questa versione non viene supportata la configurazione con raggruppamenti di sistemi, il nodo deve essere il padre diretto del sistema)
			update object_formulas set ScriptPath = '.\Lib\GroupScripts\GetWorst.py' where ObjectStatusId = (select ParentObjectStatusId from object_status where ObjectStatusId = @ParentNodeSystemStatusId) and FormulaIndex = 0
				
			commit tran
		end

	set xact_abort off;
end
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 14-05-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributes_Upd] 
	@TargetAttributeID int,
	@TargetAttributeName varchar(256), 
	@Namespace varchar(256), 
	@IsObsolete bit, 
	@ControlTypeUI varchar(64), 
	@TargetTypeID int, 
	@EnumValues varchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	SET XACT_ABORT ON;

	DECLARE @Order TINYINT;	
	DECLARE @OldNamespace VARCHAR(256);
	SELECT @OldNamespace = [Namespace] FROM geta.target_attributes WHERE TargetAttributeID = @TargetAttributeID;
	
	IF @ControlTypeUI != 'ComboBox' BEGIN SET @EnumValues = NULL END

	BEGIN TRAN
	-- il namespace è cambiato
	IF @Namespace != @OldNamespace
	BEGIN
		-- se cambio namespace indicandone uno nuovo devo creare l'attributo principale
		IF NOT EXISTS (SELECT * FROM geta.target_attributes WHERE ([Namespace] = @Namespace))
		BEGIN
			INSERT INTO geta.target_attributes (TargetAttributeName, Namespace, IsObsolete, ControlTypeUI, TargetTypeID, MainAttribute, EnumValues, [Order])
			VALUES ('Default Attribute', @Namespace, 0, 'CheckBox', @TargetTypeID, 1, NULL, 0);
		END	-- se è l'ultimo attributo non principale rimasto per il namespace precedente
		ELSE IF ( 1 = (
				SELECT COUNT(TargetAttributeID)
				FROM geta.target_attributes
				WHERE (MainAttribute <> 1) AND ([Namespace] = @OldNamespace)
			))
		BEGIN
			DELETE FROM geta.target_attributes WHERE (MainAttribute = 1) AND ([Namespace] = @OldNamespace)
		END
		
		SET @Order = (
						SELECT ISNULL(MAX([Order]), 0)
						FROM geta.target_attributes
						WHERE ([Namespace] = @Namespace)
					) + 1;
	END

	UPDATE geta.target_attributes
	SET 
		TargetAttributeName = @TargetAttributeName, 
		[Namespace] = @Namespace, 
		IsObsolete = @IsObsolete, 
		ControlTypeUI = @ControlTypeUI, 
		TargetTypeID = @TargetTypeID, 
		EnumValues = @EnumValues,
		[Order] = ISNULL(@Order, [Order])
	WHERE TargetAttributeID = @TargetAttributeID

	COMMIT TRAN
END



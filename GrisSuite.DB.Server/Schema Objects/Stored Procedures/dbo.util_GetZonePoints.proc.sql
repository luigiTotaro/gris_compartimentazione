CREATE PROCEDURE [dbo].[util_GetZonePoints] (@Name as Varchar(64) )
AS
	SELECT zones.name as Linea, 
			object_map_points.MapPoints,
			object_zones.ObjectStatusId, 
			zones.ZonID
	FROM zones 
		INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId
		LEFT JOIN object_map_points ON object_zones.ObjectStatusId = object_map_points.ObjectStatusId
	WHERE zones.Name LIKE @Name
	ORDER BY zones.Name
RETURN
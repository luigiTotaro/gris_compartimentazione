﻿CREATE PROCEDURE [dbo].[gris_GetRegionsStatus]
@RegId BIGINT=0
AS
WITH  RegCounts AS 
	(
		SELECT RegID
			, RegName
			, [0] as STLCOK
			, [3] as STLCOffline
			, [9] as STLCInMaintenance
			, [-1] as STLCNotClassified
			, [0] + [3] + [9] +[-1] AS STLCTotalCount
			, 0 as Ok
			, 0 as Warning
			, 0 as Error
			, 0 as Unknown
			, 0 as [Offline]
			, 0 as InMaintenance
			, 0 as NotClassified
			, 0 as TotalCount
		FROM 
		(
			SELECT regions.RegID
				, regions.Name AS RegName
				, servers_status.SevLevel
				, SrvId
			FROM servers_status 
				INNER JOIN nodes	ON servers_status.NodID = nodes.NodID 
				INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions	ON zones.RegID = regions.RegID
			WHERE @RegId = 0 OR regions.RegID = @RegId
		) as A
		PIVOT
		(
		 COUNT(SrvId)
		 for SevLevel
		 in ([0], [3], [9], [-1])
		) as p
		
		UNION ALL
		
		SELECT RegID
			, RegName
			, 0 as STLCOK
			, 0 as STLCOffline
			, 0 as STLCInMaintenance
			, 0 as STLCNotClassified
			, 0 as STLCTotalCount
			, [0] as Ok
			, [1] as Warning
			, [2] as Error
			, [255] as Unknown
			, [3] as [Offline]
			, [9] as InMaintenance
			, [-1] NotClassified
			, [0] + [1] + [2] + [3] + [255] + [9] + [-1] AS TotalCount
		FROM 
		(
			SELECT regions.RegID
				, regions.Name AS RegName
				, object_devices.SevLevel
				, devices.DevID
			FROM nodes 
				INNER JOIN zones			ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions			ON zones.RegID = regions.RegID 
				INNER JOIN devices			ON nodes.NodID = devices.NodID 
				INNER JOIN object_devices	ON devices.DevID = object_devices.ObjectId
			WHERE (@RegId = 0 OR regions.RegID = @RegId)
				AND (object_devices.ExcludeFromParentStatus = 0)
		) as A
		PIVOT
		(
		 COUNT(DevID)
		 for SevLevel
		 in ([0], [1], [2], [3], [255], [9], [-1])
		) as p
	)
	SELECT RegID, RegName, SUM(STLCOK) as STLCOK,
		SUM(STLCOffline) as STLCOffline, SUM(STLCInMaintenance) as STLCInMaintenance, SUM(STLCNotClassified) as STLCNotClassified, SUM(STLCTotalCount) as STLCTotalCount, 
		SUM(RegCounts.Ok) as Ok, SUM(RegCounts.Warning) as Warning,SUM(RegCounts.Error) as Error, SUM(RegCounts.Unknown) as Unknown, SUM([Offline]) as [Offline], SUM(RegCounts.InMaintenance) as InMaintenance, SUM(NotClassified) as NotClassified, SUM(TotalCount) as TotalCount, 
		object_regions.SevLevel as [Status], object_regions.SevLevelDescription as [StatusDescription],
		dbo.GetSeverityLevelForObject (RegCounts.RegId, NULL, NULL) AS SevLevelRegion,
		dbo.GetAttributeForObject(8 /* Color */, NULL, RegCounts.RegId, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor
	FROM RegCounts
		INNER JOIN object_regions ON RegCounts.RegId = object_regions.ObjectId
	GROUP BY RegID, RegName, object_regions.SevLevel, object_regions.SevLevelDescription
	ORDER BY RegName;	
RETURN
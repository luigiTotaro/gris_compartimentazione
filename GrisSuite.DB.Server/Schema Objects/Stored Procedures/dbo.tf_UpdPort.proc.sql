﻿CREATE PROCEDURE [dbo].[tf_UpdPort]
(
	@PortID uniqueidentifier,
	@PortXMLID int,
	@PortName varchar(64),
	@PortType varchar(64),
	@Parameters varchar(64),
	@Status varchar(64),
	@Removed tinyint,
	@SrvId int
)
AS
	SET NOCOUNT OFF;

	IF ISNULL(@Removed,0) = 0
	BEGIN
		IF EXISTS (SELECT PortID FROM port WHERE (PortID = @PortID))
			UPDATE [port] SET [PortXMLID] = @PortXMLID, [PortName] = @PortName, [PortType] = @PortType, [Parameters] = @Parameters, [Status] = @Status, [Removed] = @Removed, [SrvId] = @SrvId WHERE ([PortID] = @PortID);
		ELSE
			INSERT INTO [port] ([PortID],[PortXMLID], [PortName], [PortType], [Parameters], [Status], [Removed], [SrvId]) VALUES (@PortID, @PortXMLID, @PortName, @PortType, @Parameters, @Status, @Removed, @SrvId);
	END;



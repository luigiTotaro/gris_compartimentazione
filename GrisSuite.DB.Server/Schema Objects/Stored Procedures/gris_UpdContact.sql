﻿CREATE PROCEDURE gris_UpdContact (@ContactId BIGINT, @ContactName VARCHAR(2000), @PhoneNumber VARCHAR(500), @Email VARCHAR(500), @WindowsUser VARCHAR(500)) AS
SET NOCOUNT ON;

IF NOT EXISTS (SELECT ContactId FROM contacts WHERE (@ContactId <> @ContactId) AND (Name LIKE @ContactName))
BEGIN
	IF (LEN(ISNULL(@ContactName, '')) > 0)
	BEGIN
		UPDATE contacts
		SET
		Name = @ContactName,
		PhoneNumber = @PhoneNumber,
		Email = @Email,
		WindowsUser = @WindowsUser
		WHERE (ContactId = @ContactId);
	END
END
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 26-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[UserGroups_Get]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT user_groups.UserGroupID, user_groups.UserGroupName, COUNT(users.UserID) as UsersCount
	FROM user_groups 
	LEFT JOIN users ON user_groups.UserGroupID = users.UserGroupID
	GROUP BY user_groups.UserGroupID, user_groups.UserGroupName
	ORDER BY user_groups.UserGroupName
END



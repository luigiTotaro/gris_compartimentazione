﻿CREATE PROCEDURE [dbo].[gris_GetPermissionBuiltinGroupsByRegCodAndLevel]
  @RegCode TINYINT,
  @Level   TINYINT
AS
BEGIN
  SET NOCOUNT ON;

  SELECT
    GroupID,
    GroupName,
    IsBuiltIn,
    WindowsGroups,
    RegCode,
    (CASE WHEN EXISTS(SELECT resources.ResourceID
                      FROM permissions
                        INNER JOIN resources ON permissions.ResourceID = resources.ResourceID
                      WHERE (permissions.GroupID = permission_groups.GroupID))
      THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT) END) | IsBuiltIn AS Associated,
     dbo.GetContactsByGroupId(GroupID, 'windows_user') AS WindowsUsers
  FROM permission_groups
  WHERE
    (RegCode = 0 OR RegCode = @RegCode)
    AND ([Level] >= @Level)
    AND (IsBuiltIn = 1)
  ORDER BY SortOrder;
END
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
create PROCEDURE [geta].[Users_Upd] 
	@UserID int,
	@Username varchar(256),
	@FirstName varchar(256),
	@LastName varchar(256),
	@OrganizationID int,
	@UserGroupID int,
	@MobilePhone varchar(64),
	@Email varchar(128)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE users SET 
		Username = @Username, FirstName = @FirstName, LastName = @LastName, OrganizationID = @OrganizationID, UserGroupID = @UserGroupID, MobilePhone = @MobilePhone, Email = @Email
	WHERE (UserID = @UserID)
END



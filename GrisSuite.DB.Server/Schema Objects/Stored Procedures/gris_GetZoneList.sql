﻿CREATE PROCEDURE gris_GetZoneList(@VisibleRegions NVARCHAR(MAX), @RegIDs NVARCHAR(MAX)) AS
SET NOCOUNT ON;

SELECT DISTINCT
zones.ZonID,
REPLACE(REPLACE(zones.Name, 'Linea diramata ', ''), 'Linea ', '') AS Name
FROM zones
INNER JOIN nodes ON zones.ZonID = nodes.ZonID
INNER JOIN iter_bigintlist_to_tbl(@VisibleRegions) VisibleRegions ON zones.RegID = VisibleRegions.number
WHERE ((@RegIDs = '') OR (zones.RegID IN (SELECT Selected.number FROM iter_bigintlist_to_tbl(@RegIDs) Selected)))
ORDER BY Name
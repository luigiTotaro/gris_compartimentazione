CREATE PROCEDURE [dbo].[gris_Report_DevStatusAndGetaInfo]
@RegID BIGINT, @ZonID BIGINT, @NodID BIGINT, @SystemID INT, @DeviceStatus VARCHAR (128)
AS
BEGIN
	SET NOCOUNT ON;

	WITH DevStatusInfo AS
	(
		SELECT
			regions.Name as RegionName, 
			zones.Name as ZoneName, 
			nodes.Name as NodeName, 
			systems.SystemDescription as SystemName, 
			dbo.GetDeviceStatusDesc(devices.DevId,0) as DeviceStatus,
			devices.Name as DeviceName, devices.Type as DeviceType,
			ISNULL(station.StationName,'') as StationName, 
			ISNULL(building.BuildingName,'') as BuildingName,  
			ISNULL(rack.RackName,'') as RackName,
			servers.SrvID, 
			ISNULL(servers.MAC,'') AS MAC,
			ISNULL(servers.IP,'') AS IP
 		FROM devices
		INNER JOIN device_type ON device_type.DeviceTypeID = devices.Type
		INNER JOIN systems ON systems.SystemID = device_type.SystemID
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		INNER JOIN servers ON servers.SrvID = devices.SrvId
		LEFT JOIN device_status ON devices.DevID = device_status.DevID
		LEFT JOIN rack ON devices.RackID = rack.RackID 
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID
		WHERE ((zones.RegID = @RegID) OR (@RegID = -1))
		AND ((nodes.ZonID = @ZonID) OR (@ZonID = -1))
		AND ((devices.NodID = @NodID) OR (@NodID = -1))
		AND ((device_type.SystemID = @SystemID) OR (@SystemID = -1))
		AND (device_type.SystemID <> 99) -- escludi le periferiche di Altri sistemi
		AND (ISNULL(@DeviceStatus,'') = '' OR CHARINDEX('|' + CAST(dbo.GetDeviceStatus(devices.DevId,0) AS VARCHAR(MAX)) + '|', @DeviceStatus, 0) > 0)
	), 
	STLC1000Info AS  
	(
		SELECT
			regions.Name as RegionName, 
			zones.Name as ZoneName, 
			nodes.Name as NodeName, 
			systems.SystemDescription as SystemName, 
			dbo.GetDeviceStatus(devices.DevId,0) as DeviceStatus,
			devices.Name as DeviceName, devices.Type as DeviceType,
			ISNULL(station.StationName,'') as StationName, 
			ISNULL(building.BuildingName,'') as BuildingName,  
			ISNULL(rack.RackName,'') as RackName, 
			COALESCE(CONVERT(VARCHAR(10),geta.issues.DateIssue, 103),'') as DateIssue, 
			ISNULL(geta.issues.IssueComment,'') as IssueComment, 
			ISNULL(geta.target_activities.TargetActivityName,'') as TargetActivityName,  
			ISNULL(geta.issues.TargetActivityValue,'') as TargetActivityValue,  
			ISNULL(geta.issue_activity_types.IssueActivityTypeName,'') as IssueActivityTypeName,
			ISNULL(servers.MAC,'') AS MAC,
			ISNULL(servers.IP,'') AS IP
		FROM devices
			INNER JOIN device_type ON device_type.DeviceTypeID = devices.Type AND device_type.DeviceTypeID = 'STLC1000'
			INNER JOIN systems ON systems.SystemID = device_type.SystemID
			INNER JOIN nodes ON devices.NodID = nodes.NodID
			INNER JOIN zones ON zones.ZonID = nodes.ZonID
			INNER JOIN regions ON regions.RegID = zones.RegID
			INNER JOIN servers ON servers.SrvID = devices.SrvId
			LEFT JOIN device_status ON devices.DevID = device_status.DevID
			LEFT JOIN rack ON devices.RackID = rack.RackID 
			LEFT JOIN building ON rack.BuildingID = building.BuildingID 
			LEFT JOIN station ON building.StationID = station.StationID
			LEFT JOIN geta.issues ON  geta.issues.IsClosed = 0 AND  ISNULL(geta.issues.ServerID,0) = servers.SrvId
			LEFT JOIN geta.issue_activity_types ON geta.issues.IssueActivityTypeID = geta.issue_activity_types.IssueActivityTypeID 
			LEFT JOIN geta.target_activities ON geta.issues.TargetActivityID = geta.target_activities.TargetActivityID
		WHERE servers.SrvID in (SELECT DISTINCT SrvID FROM DevStatusInfo)
	)
	SELECT RegionName, ZoneName, NodeName, SystemName, DeviceStatus,DeviceName, DeviceType, StationName, BuildingName, RackName,
			MAC, IP,'' AS DateIssue, '' AS IssueComment, '' AS TargetActivityName, '' AS TargetActivityValue, '' AS IssueActivityTypeName
	FROM DevStatusInfo
	UNION
	SELECT RegionName, ZoneName, NodeName, SystemName, DeviceStatus,DeviceName, DeviceType, StationName, BuildingName, RackName, 
			MAC, IP, DateIssue, IssueComment, TargetActivityName, TargetActivityValue, IssueActivityTypeName
	FROM STLC1000Info
	ORDER BY RegionName, ZoneName, NodeName, SystemName,DeviceType, DeviceName, DateIssue DESC, TargetActivityName
END
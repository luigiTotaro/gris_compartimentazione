CREATE PROCEDURE [tf_GetAreaStatisticsByObjectStatusId]
@ObjectStatusId UNIQUEIDENTIFIER
AS
SET NOCOUNT ON;

DECLARE @Tot INT;
DECLARE @TotOk INT, @TotWarning INT, @TotError INT;
DECLARE @TotOkPercentage DECIMAL(9,7), @TotWarningPercentage DECIMAL(9,7), @TotErrorPercentage DECIMAL(9,7);
DECLARE @TotOkWeighed DECIMAL(9,7), @TotWarningWeighed DECIMAL(9,7), @TotErrorWeighed DECIMAL(9,7);
DECLARE @OK_ID TINYINT, @WARN_ID TINYINT, @ERR_ID TINYINT;
DECLARE @ForcedSevLevel TINYINT, @SevLevel INT;
DECLARE @EntityStateNameOk VARCHAR(50), @EntityStateSeverityOk DECIMAL(5, 2), @EntityStateMinValueOk SMALLINT, @EntityStateMaxValueOk SMALLINT;
DECLARE @EntityStateNameWarning VARCHAR(50), @EntityStateSeverityWarning DECIMAL(5, 2), @EntityStateMinValueWarning SMALLINT, @EntityStateMaxValueWarning SMALLINT;
DECLARE @EntityStateNameError VARCHAR(50), @EntityStateSeverityError DECIMAL(5, 2), @EntityStateMinValueError SMALLINT, @EntityStateMaxValueError SMALLINT;
DECLARE @AvgValue DECIMAL(9, 7);
DECLARE @IsWithServerObject BIT;
DECLARE @ObjectTypeId INT;

SELECT @IsWithServerObject = dbo.IsWithServerObject(@ObjectStatusId);
SET @IsWithServerObject = ISNULL(@IsWithServerObject, 0);

SET @OK_ID = 1; -- In Servizio
SET @WARN_ID = 2; -- Anomalia Lieve
SET @ERR_ID = 3; -- Anomalia Grave

SELECT
@TotOk = ISNULL(Ok, 0),
@TotWarning = ISNULL(Warning, 0),
@TotError = ISNULL(Error, 0),
@ForcedSevLevel = ISNULL(ForcedSevLevel, 0),
@SevLevel = SevLevel,
@ObjectTypeId = ObjectTypeId
FROM object_status
WHERE (ObjectStatusId = @ObjectStatusId)

SET @TotOk = ISNULL(@TotOk, 0);
SET @TotWarning = ISNULL(@TotWarning, 0);
SET @TotError = ISNULL(@TotError, 0);
SET @ForcedSevLevel = ISNULL(@ForcedSevLevel, 0);
SET @SevLevel = ISNULL(@SevLevel, 255);
SET @ObjectTypeId = ISNULL(@ObjectTypeId, 0);

SET @Tot = @TotOk + @TotWarning + @TotError;

IF (@Tot = 0)
BEGIN
	SET @TotOkPercentage = (@TotOk + .0);
	SET @TotWarningPercentage = (@TotWarning + .0);
	SET @TotErrorPercentage = (@TotError + .0);
END
ELSE
BEGIN
	SET @TotOkPercentage = (@TotOk + .0) / (@Tot + .0);
	SET @TotWarningPercentage = (@TotWarning + .0) / (@Tot + .0);
	SET @TotErrorPercentage = (@TotError + .0) / (@Tot + .0);
END

SELECT
@EntityStateNameOk = EntityStateName,
@EntityStateSeverityOk = EntityStateSeverity,
@EntityStateMinValueOk = EntityStateMinValue,
@EntityStateMaxValueOk = EntityStateMaxValue
FROM entity_state WHERE EntityStateID = @OK_ID;

SELECT
@EntityStateNameWarning = EntityStateName,
@EntityStateSeverityWarning = EntityStateSeverity,
@EntityStateMinValueWarning = EntityStateMinValue,
@EntityStateMaxValueWarning = EntityStateMaxValue
FROM entity_state WHERE EntityStateID = @WARN_ID;

SELECT
@EntityStateNameError = EntityStateName,
@EntityStateSeverityError = EntityStateSeverity,
@EntityStateMinValueError = EntityStateMinValue,
@EntityStateMaxValueError = EntityStateMaxValue
FROM entity_state WHERE EntityStateID = @ERR_ID;

SET @TotOkWeighed = @TotOkPercentage * @EntityStateSeverityOk;
SET @TotWarningWeighed = @TotWarningPercentage * @EntityStateSeverityWarning;
SET @TotErrorWeighed = @TotErrorPercentage * @EntityStateSeverityError;

SET @AvgValue =	@TotOkWeighed + @TotWarningWeighed + @TotErrorWeighed;

SELECT
@TotOk AS TotOk,
@TotWarning AS TotWarning,
@TotError AS TotError,
@TotOkPercentage AS TotOkPercentage,
@TotWarningPercentage AS TotWarningPercentage,
@TotErrorPercentage AS TotErrorPercentage,
@EntityStateSeverityOk AS EntityStateSeverityOk,
@EntityStateSeverityWarning AS EntityStateSeverityWarning,
@EntityStateSeverityError AS EntityStateSeverityError,
@EntityStateNameOk AS EntityStateNameOk,
@EntityStateNameWarning AS EntityStateNameWarning,
@EntityStateNameError AS EntityStateNameError,
@TotOkWeighed AS TotOkWeighed,
@TotWarningWeighed AS TotWarningWeighed,
@TotErrorWeighed AS TotErrorWeighed,
@AvgValue AS AvgValue,
@SevLevel AS SevLevel,
@ForcedSevLevel AS ForcedSevLevel,
@EntityStateMinValueOk AS EntityStateMinValueOk,
@EntityStateMaxValueOk AS EntityStateMaxValueOk,
@EntityStateMinValueWarning AS EntityStateMinValueWarning,
@EntityStateMaxValueWarning AS EntityStateMaxValueWarning,
@EntityStateMinValueError AS EntityStateMinValueError,
@EntityStateMaxValueError AS EntityStateMaxValueError,
@IsWithServerObject AS IsWithServerObject,
@ObjectTypeId AS ObjectTypeId

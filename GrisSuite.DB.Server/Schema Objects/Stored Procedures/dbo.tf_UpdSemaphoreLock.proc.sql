﻿CREATE PROCEDURE [dbo].[tf_UpdSemaphoreLock]
@SemaphoreName VARCHAR (64), @HostName VARCHAR (64)=NULL
AS
IF NOT EXISTS (SELECT SemaphoreName FROM semaphores WHERE SemaphoreName = @SemaphoreName)
BEGIN
	INSERT INTO semaphores (SemaphoreName, Locked, HostName)
	VALUES (@SemaphoreName, 0, @HostName);
END

UPDATE semaphores
SET
Locked = 1,
HostName = @HostName,
LastUpdate = GETDATE()
WHERE (SemaphoreName = @SemaphoreName)
AND (Locked = 0);
		
RETURN @@ROWCOUNT;
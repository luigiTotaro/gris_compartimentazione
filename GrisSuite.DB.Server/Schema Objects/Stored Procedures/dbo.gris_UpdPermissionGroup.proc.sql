﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 03/11/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[gris_UpdPermissionGroup] 
	@GroupID INT,
	@GroupName VARCHAR(500), 
	@GroupDescription VARCHAR(2000) = '', 
	@WindowsGroups VARCHAR(MAX) = '',
	@IsBuiltIn BIT
AS
BEGIN
	SET NOCOUNT ON;
	
	SET @GroupDescription = ISNULL(@GroupDescription, '');
	SET @WindowsGroups = ISNULL(@WindowsGroups, '');

	IF (@IsBuiltIn = 1)
	BEGIN
		UPDATE permission_groups 
		SET GroupDescription = @GroupDescription, WindowsGroups = @WindowsGroups
		WHERE GroupID = @GroupID;
	END
	ELSE
	BEGIN
		UPDATE permission_groups 
		SET GroupName = @GroupName, GroupDescription = @GroupDescription, WindowsGroups = @WindowsGroups
		WHERE GroupID = @GroupID;
	END
END
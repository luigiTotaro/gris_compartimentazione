﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 11-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[DelTargetAttributes] 
	@TargetAttributeID int
AS
BEGIN
	SET NOCOUNT ON;

	-- se è l'ultimo attributo escluso quello principale cancello anche l'attributo principale
	IF ( 1 = (
				SELECT COUNT(TargetAttributeID)
				FROM geta.target_attributes
				WHERE (MainAttribute <> 1) AND ([Namespace] = ( SELECT [Namespace] FROM geta.target_attributes WHERE (TargetAttributeID = @TargetAttributeID) ))
			))
	BEGIN
		DELETE FROM geta.target_attributes 
		WHERE (TargetAttributeID = @TargetAttributeID) OR ((MainAttribute = 1) 
		AND ([Namespace] = ( SELECT [Namespace] FROM geta.target_attributes WHERE (TargetAttributeID = @TargetAttributeID) )))
	END
	ELSE
	BEGIN
		DELETE FROM geta.target_attributes WHERE (TargetAttributeID = @TargetAttributeID);
	END

END



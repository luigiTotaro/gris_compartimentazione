﻿CREATE PROCEDURE gris_IsMostRecentCacheLogMessageOlderThan (@TimespanMinutes INT = 15) AS
SET NOCOUNT ON;

SET @TimespanMinutes = ISNULL(@TimespanMinutes, -1);

IF (@TimespanMinutes <= 0)
BEGIN
	SELECT CONVERT(TINYINT, 0) AS IsMostRecentCacheLogMessageObsolete
END
ELSE
BEGIN
	IF EXISTS (SELECT LogID FROM cache_log_messages WHERE (Created >= DATEADD(n, -@TimespanMinutes, GetDate())))
	BEGIN
		SELECT CONVERT(TINYINT, 0) AS IsMostRecentCacheLogMessageObsolete
	END
	ELSE
	BEGIN
		SELECT CONVERT(TINYINT, 1) AS IsMostRecentCacheLogMessageObsolete
	END
END
﻿CREATE PROCEDURE [dbo].[gris_UpdServer]
	@SrvID int,
	@Name varchar(64),
	@Host varchar(64),
	@FullHostName varchar(256),
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64),
	@SupervisorSystemXML xml,
	@ClientSupervisorSystemXMLValidated xml,
	@ClientValidationSign varbinary(128),
	@ClientDateValidationRequested datetime,
	@ClientDateValidationObtained datetime,
	@ServerSupervisorSystemXMLValidated xml,
	@ServerValidationSign varbinary(128),
	@ServerDateValidationRequested datetime,
	@ServerDateValidationObtained datetime,
	@ClientKey varbinary(148)
AS
	UPDATE servers
	SET 
		[Name] = @Name, Host = @Host, FullHostName = @FullHostName, IP = @IP, LastUpdate = @LastUpdate, 
		LastMessageType = @LastMessageType, ClientValidationSign = @ClientValidationSign, 
		ClientDateValidationRequested = @ClientDateValidationRequested, ClientDateValidationObtained = @ClientDateValidationObtained, 
		ServerValidationSign = @ServerValidationSign, ServerDateValidationRequested = @ServerDateValidationRequested, 
		ServerDateValidationObtained = @ServerDateValidationObtained, ClientKey = @ClientKey
	WHERE (SrvID = @SrvID);



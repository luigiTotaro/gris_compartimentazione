﻿CREATE PROCEDURE [dbo].[gris_GetZonesStatus]
@RegId BIGINT
AS
WITH  ZonCounts AS 
	(
		SELECT RegID, RegName, ZonID, ZonName, [0] as STLCOK, [3] as STLCOffline, [9] as STLCInMaintenance, [-1] as STLCNotClassified, STLCTotalCount, 0 as Ok, 0 as Warning,0 as Error, 0 as Unknown, 0 as [Offline], 0 as InMaintenance, 0 as NotClassified, 0 as DevicesTotalCount
		FROM 
		(
			SELECT regions.RegID, regions.Name as RegName, zones.ZonID, zones.Name as ZonName, servers_status.SevLevel, SrvId, COUNT(SrvId) OVER (PARTITION BY zones.ZonID) as STLCTotalCount
			FROM servers_status 
				INNER JOIN nodes	ON servers_status.NodID = nodes.NodID 
				INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions	ON zones.RegID = regions.RegID
			WHERE zones.RegId = @RegId
		) as A
		PIVOT
		(
		 COUNT(SrvId)
		 for SevLevel
		 in ([0], [3], [9], [-1])
		) as p
		
		UNION ALL
		
		SELECT RegID, RegName, ZonID, ZonName, 0 as STLCOK, 0 as STLCOffline, 0 as STLCInMaintenance, 0 as STLCNotClassified, 0 as STLCTotalCount, [0] as Ok, [1] as Warning,[2] as Error, [255] as Unknown, [3] as [Offline], [9] as InMaintenance, [-1] NotClassified, DevicesTotalCount
		FROM 
		(
			SELECT regions.RegID, regions.Name as RegName, zones.ZonID, zones.Name as ZonName, object_devices.SevLevel, devices.DevID, COUNT(DevID) OVER (PARTITION BY zones.ZonID) as DevicesTotalCount
			FROM nodes 
				INNER JOIN zones			ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions			ON zones.RegID = regions.RegID 
				INNER JOIN devices			ON nodes.NodID = devices.NodID 
				INNER JOIN object_devices	ON devices.DevID = object_devices.ObjectId
			WHERE zones.RegId = @RegId
				AND object_devices.ExcludeFromParentStatus = 0			
		) as A
		PIVOT
		(
		 COUNT(DevID)
		 for SevLevel
		 in ([0], [1], [2], [3], [255], [9], [-1])
		) as p
	)
	SELECT 
		RegID, RegName, ZonID, ZonName, 
		SUM(STLCOK) as STLCOK, SUM(STLCOffline) as STLCOffline, SUM(STLCInMaintenance) as STLCInMaintenance, SUM(STLCNotClassified) as STLCNotClassified, SUM(STLCTotalCount) as STLCTotalCount, 
		SUM(ZonCounts.Ok) as Ok, SUM(ZonCounts.Warning) as Warning,SUM(ZonCounts.Error) as Error, SUM(ZonCounts.Unknown) as Unknown, SUM([Offline]) as [Offline], SUM(ZonCounts.InMaintenance) as InMaintenance, SUM(NotClassified) as NotClassified, SUM(DevicesTotalCount) as TotalCount, 
		object_zones.SevLevel as [Status], object_zones.SevLevelDescription as [StatusDescription],
		dbo.GetSeverityLevelForObject (RegID, NULL, NULL) AS SevLevelRegion,
		dbo.GetSeverityLevelForObject (NULL, ZonID, NULL) AS SevLevelZone,
		dbo.GetAttributeForObject(8 /* Color */, NULL, RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
		dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor
	FROM ZonCounts
		INNER JOIN object_zones ON ZonCounts.ZonId = object_zones.ObjectId
	GROUP BY RegID, RegName, ZonID, ZonName, object_zones.SevLevel, object_zones.SevLevelDescription
	ORDER BY RegName, ZonName;
RETURN
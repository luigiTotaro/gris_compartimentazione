﻿CREATE PROCEDURE [dbo].gris_GetHistoryStreamFieldsValue (@DevID BIGINT, @StrID INT, @FieldID INT, @DataInizio AS DATETIME = NULL, @OraInizio AS DATETIME = NULL) AS
SET NOCOUNT ON;

SET @DataInizio = ISNULL(DATEADD(dd, DATEDIFF(dd, 0, @DataInizio), 0), '19000101');
SET @OraInizio = ISNULL(DATEADD(day, -DATEDIFF(day, 0, @OraInizio), @OraInizio), '00:00:00.000');

SELECT
stream_fields_type.StreamName,
history_stream_fields_value.Name AS StreamFieldName,
history_stream_fields_value.ArrayID,
REPLACE(REPLACE(REPLACE(history_stream_fields_value.Value, CHAR(13) COLLATE SQL_Latin1_General_CP1_CI_AS, ''), CHAR(10), ''), CHAR(0), '') AS Value,
history_stream_fields_value.Created,
history_stream_fields_value.Duration,
history_stream_fields_value.SevLevel,
Severity.Description AS SeverityDescription,
dbo.FormatSeconds(history_stream_fields_value.Duration) AS DurationDescription
FROM history_stream_fields_value
INNER JOIN device_type ON history_stream_fields_value.DevType = device_type.DeviceTypeID
INNER JOIN nodes ON history_stream_fields_value.NodId = nodes.NodID
INNER JOIN severity ON history_stream_fields_value.SevLevel = severity.SevLevel
INNER JOIN stream_fields_type ON history_stream_fields_value.DevType = stream_fields_type.DevType
	AND history_stream_fields_value.StrID = stream_fields_type.StrID
	AND history_stream_fields_value.FieldID = stream_fields_type.FieldID
WHERE (history_stream_fields_value.DevID = @DevID)
AND (history_stream_fields_value.StrID = @StrID)
AND (history_stream_fields_value.FieldID = @FieldID)
AND (history_stream_fields_value.Duration IS NOT NULL)
AND (ISNULL(history_stream_fields_value.Duration, 0) > 0)
AND (@DataInizio <= DATEADD(dd, DATEDIFF(dd, 0, ISNULL(DATEADD(second, history_stream_fields_value.Duration, history_stream_fields_value.Created), '30000101')), 0))
AND (@OraInizio <= DATEADD(day, -DATEDIFF(day, 0, ISNULL(DATEADD(second, history_stream_fields_value.Duration, history_stream_fields_value.Created), '23:59:59.999')), ISNULL(DATEADD(second, history_stream_fields_value.Duration, history_stream_fields_value.Created), '23:59:59.999')))
ORDER BY history_stream_fields_value.ArrayID, history_stream_fields_value.Created DESC;
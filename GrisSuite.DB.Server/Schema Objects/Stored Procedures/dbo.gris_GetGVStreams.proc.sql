﻿CREATE PROCEDURE [dbo].[gris_GetGVStreams]
@DevID BIGINT, @ViewUnknowns BIT, @ViewErrors BIT, @ViewWarnings BIT, @ViewOKs BIT, @ViewInfos BIT
AS
BEGIN
	SET NOCOUNT ON;	
	
	SELECT DISTINCT streams.StrID, streams.Name, streams.SevLevel, streams.DateTime AS [Date]
	FROM devices 
	INNER JOIN stream_fields ON devices.DevID = stream_fields.DevID 
	INNER JOIN streams ON stream_fields.DevID = streams.DevID AND stream_fields.StrID = streams.StrID  
	WHERE (devices.DevID = @DevID) 
	AND (stream_fields.Visible = 1) 
	AND (stream_fields.StrID IN 
		(
			SELECT StrID FROM streams AS streams_1 WHERE (DevID = @DevID) AND (Visible = 1))
		) 
	AND ((stream_fields.SevLevel <> 0) OR (@ViewOKs = 1))
	AND ((stream_fields.SevLevel <> 1) OR (@ViewWarnings = 1))
	AND ((stream_fields.SevLevel <> 2) OR (@ViewErrors = 1))
	AND ((stream_fields.SevLevel <> 255) OR (@ViewUnknowns = 1))
	AND ((stream_fields.SevLevel <> -255) OR (@ViewInfos = 1))
	ORDER BY streams.StrID
END
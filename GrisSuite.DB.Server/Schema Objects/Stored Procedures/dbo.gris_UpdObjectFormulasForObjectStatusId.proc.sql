﻿CREATE PROCEDURE [dbo].[gris_UpdObjectFormulasForObjectStatusId] (@ObjectStatusId UNIQUEIDENTIFIER, @FormulaIndex0 TINYINT, @ScriptPath0 VARCHAR(2000), @FormulaIndex1 TINYINT = NULL, @ScriptPath1 VARCHAR(2000) = NULL, @FormulaIndex2 TINYINT = NULL, @ScriptPath2 VARCHAR(2000) = NULL, @FormulaIndex3 TINYINT = NULL, @ScriptPath3 VARCHAR(2000) = NULL, @FormulaIndex4 TINYINT = NULL, @ScriptPath4 VARCHAR(2000) = NULL) AS
SET NOCOUNT ON;

SET @FormulaIndex0 = ISNULL(@FormulaIndex0, 0);
SET @FormulaIndex1 = ISNULL(@FormulaIndex1, 0);
SET @FormulaIndex2 = ISNULL(@FormulaIndex2, 0);
SET @FormulaIndex3 = ISNULL(@FormulaIndex3, 0);
SET @FormulaIndex4 = ISNULL(@FormulaIndex4, 0);

SET @ScriptPath0 = LTRIM(RTRIM(ISNULL(@ScriptPath0, '')));
SET @ScriptPath1 = LTRIM(RTRIM(ISNULL(@ScriptPath1, '')));
SET @ScriptPath2 = LTRIM(RTRIM(ISNULL(@ScriptPath2, '')));
SET @ScriptPath3 = LTRIM(RTRIM(ISNULL(@ScriptPath3, '')));
SET @ScriptPath4 = LTRIM(RTRIM(ISNULL(@ScriptPath4, '')));

DECLARE @ObjectTypeId INT;
SET @ObjectTypeId = (SELECT object_status.ObjectTypeId FROM object_status WHERE object_status.ObjectStatusId = @ObjectStatusId)

IF (ISNULL(@ObjectTypeId, 0) > 0)
BEGIN
	-- La formula di default, con indice 0, deve sempre esistere, altrimenti non facciamo nulla
	IF ((@FormulaIndex0 = 0) AND (LEN(@ScriptPath0) > 9))
	BEGIN
		IF EXISTS (SELECT ObjectStatusId FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex0))
		BEGIN
			UPDATE object_formulas
			SET ScriptPath = @ScriptPath0
			WHERE (ObjectStatusId = @ObjectStatusId)
			AND (FormulaIndex = @FormulaIndex0);
		END
		ELSE
		BEGIN
			-- Non esiste la formula di default per l'oggetto, quindi la creiamo (non dovrebbe mai succedere)
			INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
			VALUES (@ObjectStatusId, @FormulaIndex0, @ScriptPath0, 0);
		END
	END
	
	-- Se è indicata una formula custom, la aggiorna se esiste, la crea, oppure la elimina
	IF (@FormulaIndex1 > 0)
	BEGIN
		IF (LEN(@ScriptPath1) > 9)
		BEGIN
			IF EXISTS (SELECT ObjectStatusId FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex1))
			BEGIN
				UPDATE object_formulas
				SET ScriptPath = @ScriptPath1
				WHERE (ObjectStatusId = @ObjectStatusId)
				AND (FormulaIndex = @FormulaIndex1);
			END
			ELSE
			BEGIN
				INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
				VALUES (@ObjectStatusId, @FormulaIndex1, @ScriptPath1, 0);
			END
		END
		ELSE IF (LEN(@ScriptPath1) = 0)
		BEGIN
			DELETE FROM object_attributes
			WHERE (ComputedByFormulaObjectStatusId = @ObjectStatusId)
			AND (ComputedByFormulaIndex = @FormulaIndex1);
					
			DELETE FROM object_formulas
			WHERE (ObjectStatusId = @ObjectStatusId)
			AND (FormulaIndex = @FormulaIndex1);
		END
	END	
	
	-- Se è indicata una formula custom, la aggiorna se esiste, la crea, oppure la elimina
	-- La formula con indice 2 indica quella dei colori custom per stazione
	IF (@FormulaIndex2 > 0)
	BEGIN
		IF (LEN(@ScriptPath2) > 9)
		BEGIN
			IF EXISTS (SELECT ObjectStatusId FROM object_formulas_custom_colors WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex2))
			BEGIN
				-- Verifichiamo che esista almeno una associazione di colori relativi alla formula custom dei colori	
				IF EXISTS (SELECT ObjectStatusId FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex2))
				BEGIN
					UPDATE object_formulas
					SET ScriptPath = @ScriptPath2
					WHERE (ObjectStatusId = @ObjectStatusId)
					AND (FormulaIndex = @FormulaIndex2);
				END
				ELSE
				BEGIN
					INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
					VALUES (@ObjectStatusId, @FormulaIndex2, @ScriptPath2, 0);
				END
			END
		END
		ELSE IF (LEN(@ScriptPath2) = 0)
		BEGIN
			DELETE FROM object_attributes
			WHERE (ComputedByFormulaObjectStatusId = @ObjectStatusId)
			AND (ComputedByFormulaIndex = @FormulaIndex2);
					
			DELETE FROM object_formulas_custom_colors
			WHERE (ObjectStatusId = @ObjectStatusId)
			AND (FormulaIndex = @FormulaIndex2);

			DELETE FROM object_formulas
			WHERE (ObjectStatusId = @ObjectStatusId)
			AND (FormulaIndex = @FormulaIndex2);
		END
	END
	
	-- Se è indicata una formula custom, la aggiorna se esiste, la crea, oppure la elimina
	IF (@FormulaIndex3 > 0)
	BEGIN
		IF (LEN(@ScriptPath3) > 9)
		BEGIN
			IF EXISTS (SELECT ObjectStatusId FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex3))
			BEGIN
				UPDATE object_formulas
				SET ScriptPath = @ScriptPath3
				WHERE (ObjectStatusId = @ObjectStatusId)
				AND (FormulaIndex = @FormulaIndex3);
			END
			ELSE
			BEGIN
				INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
				VALUES (@ObjectStatusId, @FormulaIndex3, @ScriptPath3, 0);
			END
		END
		ELSE IF (LEN(@ScriptPath3) = 0)
		BEGIN
			DELETE FROM object_attributes
			WHERE (ComputedByFormulaObjectStatusId = @ObjectStatusId)
			AND (ComputedByFormulaIndex = @FormulaIndex3);
					
			DELETE FROM object_formulas
			WHERE (ObjectStatusId = @ObjectStatusId)
			AND (FormulaIndex = @FormulaIndex3);
		END
	END
	
	-- Se è indicata una formula custom, la aggiorna se esiste, la crea, oppure la elimina
	IF (@FormulaIndex4 > 0)
	BEGIN
		IF (LEN(@ScriptPath4) > 9)
		BEGIN
			IF EXISTS (SELECT ObjectStatusId FROM object_formulas WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex4))
			BEGIN
				UPDATE object_formulas
				SET ScriptPath = @ScriptPath4
				WHERE (ObjectStatusId = @ObjectStatusId)
				AND (FormulaIndex = @FormulaIndex4);
			END
			ELSE
			BEGIN
				INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
				VALUES (@ObjectStatusId, @FormulaIndex4, @ScriptPath4, 0);
			END
		END
		ELSE IF (LEN(@ScriptPath4) = 0)
		BEGIN
			DELETE FROM object_attributes
			WHERE (ComputedByFormulaObjectStatusId = @ObjectStatusId)
			AND (ComputedByFormulaIndex = @FormulaIndex4);
					
			DELETE FROM object_formulas
			WHERE (ObjectStatusId = @ObjectStatusId)
			AND (FormulaIndex = @FormulaIndex4);
		END
	END

	-- Resetta il Severity Detail sull'oggetto stesso, per evitare che restino dati incoerenti non calcolati dal cambio di formule
	UPDATE object_status
	SET SevLevelDetailId = NULL
	WHERE ObjectStatusId = @ObjectStatusId
	AND ObjectTypeId IN (3 /* nodes */, 4 /* node_systems */, 7 /* virtual_object */)

	-- Resetta il Severity Detail dui figli diretti dell'oggetto, per evitare che restino dati incoerenti non calcolati dal cambio di formule
	UPDATE object_status
	SET SevLevelDetailId = NULL
	WHERE ParentObjectStatusId = @ObjectStatusId
	AND ObjectTypeId IN (4 /* node_systems */, 7 /* virtual_object */)
END
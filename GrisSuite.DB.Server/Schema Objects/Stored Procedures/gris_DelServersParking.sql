﻿CREATE PROCEDURE gris_DelServersParking (@ServerParkingID INT) AS
SET NOCOUNT ON;

DELETE FROM servers_parking
WHERE (ServerParkingID = @ServerParkingID)
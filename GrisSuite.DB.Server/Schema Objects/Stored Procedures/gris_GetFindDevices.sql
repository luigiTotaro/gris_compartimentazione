﻿CREATE PROCEDURE [dbo].[gris_GetFindDevices] (@NodIDs NVARCHAR(MAX), @SystemIDs NVARCHAR(MAX), @DeviceTypeIDs NVARCHAR(MAX), @SevLevelIDs NVARCHAR(MAX), @DeviceData NVARCHAR(110)) AS
BEGIN
  SET NOCOUNT ON;

  WITH NodeSystemDevicesWithStatus AS
  (
    SELECT devices.DevID,
    object_devices.SevLevelDetailId,
    object_devices.SevLevelReal AS RealSevLevel,
    RealSeverity.Description AS RealSevLevelDescription,
    object_devices.SevLevelLast AS LastSevLevel,
    LastSeverity.Description AS LastSevLevelDescription
    FROM devices
    INNER JOIN gris_FnFindDevices(@NodIDs, @SystemIDs, @DeviceTypeIDs, @SevLevelIDs, @DeviceData) AS filter ON devices.DevID = filter.DevID
    INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
    INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
    INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
    INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
    INNER JOIN nodes ON nodes.NodID = devices.NodID
  ), DevicesWithSelectedStatus AS
  (
    SELECT NodeSystemDevicesWithStatus.DevID,
      -- valore
      NodeSystemDevicesWithStatus.RealSevLevel AS SevLevel,
      -- descrizione
      NodeSystemDevicesWithStatus.RealSevLevelDescription AS SevLevelDescription,
      SevLevelDetailId,
      -- flag ultimo stato
      CAST(0 AS BIT) AS LastSevLevelReturned,
      CASE WHEN SevLevelDetailId = 7 OR SevLevelDetailId = 9 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS OperatorMaintenance
    FROM NodeSystemDevicesWithStatus
    INNER JOIN devices ON devices.DevID = NodeSystemDevicesWithStatus.DevID
  )
  SELECT
  RegID, Region, Zone, Node, DevID, Name, [Type], DeviceSerialNumber, [Address], SevLevel, SevLevelDescription, SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
  SystemID, SystemDescription, WSUrlPattern, VendorName, PortName, PortType, PortStatus,
  StationName, BuildingName, BuildingDescription, RackName, RackType, RackDescription,
  STLC1000Name, STLC1000FullHostName, STLC1000IP, STLC1000LastUpdate, STLC1000LastMessageType,
  Ticket, RackPositionRow, RackPositionCol, SrvID, RawType, DeviceImage, SevLevelRegion, SevLevelZone, SevLevelNode, RegionColor, ZoneColor, NodeColor,
  DeviceVirtualObjectName, DeviceVirtualObjectDescription, DeviceVirtualObjectIsComputedByCustomFormula,
  (
    CASE
      WHEN OperatorMaintenance = 0 THEN SevLevelDescription
      ELSE SevLevelDescription + ' (Non attivo)'
    END
  ) AS SevLevelDescriptionComplete,
  (
    CASE
      WHEN ((RackPositionRow IS NULL) AND (RackPositionCol IS NULL))
        THEN ''
      WHEN ((RackPositionRow IS NOT NULL) AND (RackPositionCol IS NULL))
        THEN 'Posizione: ' + CONVERT(NVARCHAR(10), RackPositionRow) + ', N/D'
      WHEN ((RackPositionRow IS NULL) AND (RackPositionCol IS NOT NULL))
        THEN 'Posizione: N/D, ' + CONVERT(NVARCHAR(10), RackPositionCol)
      WHEN ((RackPositionRow IS NOT NULL) AND (RackPositionCol IS NOT NULL))
        THEN 'Posizione: ' + CONVERT(NVARCHAR(10), RackPositionRow) + ', ' + CONVERT(NVARCHAR(10), RackPositionCol)
      ELSE ''
    END
  ) AS Position,
  REPLACE(Region, 'Compartimento di ', '') + ' > ' + REPLACE(REPLACE(Zone, 'Linea diramata ', ''), 'Linea ', '') AS Topography,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 255) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C255,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 9) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C9,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 2) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C2,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 1) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C1,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = 0) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS C0,
  ISNULL((SELECT COUNT(DevID) FROM stream_fields WHERE (SevLevel = -255) AND (Visible = 1) AND (stream_fields.DevID = DevData.DevID)), 0) AS Cm255
  FROM
  (
    SELECT
      -- topografia
      regions.RegID, regions.Name AS Region, zones.Name AS Zone, nodes.Name AS Node,
      -- periferica
      devices.DevID, devices.Name, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS [Type], devices.SN AS DeviceSerialNumber,
      (
        CASE
          WHEN port.PortType LIKE 'TCP Client' THEN dbo.GetIPStringFromInt(devices.Addr)
          WHEN port.PortType LIKE 'TCP_Client' THEN dbo.GetIPStringFromInt(devices.Addr)
        ELSE
          CAST(devices.Addr AS VARCHAR(15))
        END
      ) AS [Address],
      -- stato
      DevicesWithSelectedStatus.SevLevel, dbo.GetCustomSeverityDescription(SevLevel, SevLevelDetailId, SevLevelDescription) AS SevLevelDescription, SevLevelDetailId AS SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
      -- tipo
      device_type.SystemID, systems.SystemDescription, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 4, devices.DevID) AS WSUrlPattern, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) AS VendorName,
      -- porte
      port.PortName, port.PortType, port.Status AS PortStatus,
      -- topografia locale
      ISNULL(station.StationName, '') AS StationName, ISNULL(building.BuildingName, '') AS BuildingName, ISNULL(building.BuildingDescription, '') AS BuildingDescription, ISNULL(rack.RackName, '') AS RackName, ISNULL(rack.RackType, '') AS RackType, ISNULL(rack.RackDescription, '') AS RackDescription,
      -- server
      servers.Name AS STLC1000Name, servers.Host AS STLC1000, servers.FullHostName AS STLC1000FullHostName, servers.IP AS STLC1000IP, servers.LastUpdate AS STLC1000LastUpdate, servers.LastMessageType AS STLC1000LastMessageType,
      -- ticket
      ISNULL((SELECT alerts_tickets.TicketName FROM alerts_tickets WHERE (DevID = devices.DevID) AND IsOpen = 1 AND ISNULL(TicketName, '') <> ''),'') AS Ticket, -- il fatto che esista una sola device in questa condizione DOVREBBE essere mantenuto a livello applicativo.
      ISNULL(devices.RackPositionRow, 0) AS RackPositionRow,
      ISNULL(devices.RackPositionCol, 0) AS RackPositionCol,
      devices.SrvID,
      devices.[Type] AS RawType,
      dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 8, devices.DevID) AS DeviceImage,
      dbo.GetSeverityLevelForObject (regions.RegID, NULL, NULL) AS SevLevelRegion,
      dbo.GetSeverityLevelForObject (NULL, zones.ZonID, NULL) AS SevLevelZone,
      dbo.GetSeverityLevelForObject (NULL, NULL, Nodes.NodID) AS SevLevelNode,
      dbo.GetAttributeForObject(8 /* Color */, NULL, regions.RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
      dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, zones.ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor,
      dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, Nodes.NodID, NULL, NULL, NULL, NULL) AS NodeColor,
      dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectName,
      dbo.GetVirtualObjectDataForObject (3 /* VirtualObjectDescription */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectDescription,
      dbo.GetVirtualObjectDataForObject (4 /* IsObjectStatusIdComputedByCustomFormula */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectIsComputedByCustomFormula
    FROM devices
    INNER JOIN DevicesWithSelectedStatus ON devices.DevID = DevicesWithSelectedStatus.DevID
    INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
    INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
    INNER JOIN nodes ON nodes.NodID = devices.NodID
    INNER JOIN zones ON zones.ZonID = nodes.ZonID
    INNER JOIN regions ON regions.RegID = zones.RegID
    INNER JOIN servers ON devices.SrvID = servers.SrvID
    INNER JOIN systems ON device_type.SystemID = systems.SystemID
    INNER JOIN iter_bigintlist_to_tbl(@SevLevelIDs) SelectedSeverities ON DevicesWithSelectedStatus.SevLevel = SelectedSeverities.number
    LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID
    LEFT JOIN rack ON devices.RackID = rack.RackID
    LEFT JOIN building ON rack.BuildingID = building.BuildingID
    LEFT JOIN station ON building.StationID = station.StationID
  ) DevData
  ORDER BY SevLevel DESC, Name;
END
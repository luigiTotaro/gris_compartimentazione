﻿CREATE PROCEDURE [gris_GetServersParkingTypes] AS
SET NOCOUNT ON;

SELECT 0 AS ParkingTypeID, 'Sconosciuto' AS ParkingTypeDescription
UNION ALL
SELECT 1 AS ParkingTypeID, 'Server configurato, ma che non centralizza' AS ParkingTypeDescription
UNION ALL
SELECT 2 AS ParkingTypeID, 'Server non configurato, che non centralizza' AS ParkingTypeDescription
--UNION ALL
--SELECT 3 AS ParkingTypeID, 'Server storico, che ha centralizzato almeno una volta' AS ParkingTypeDescription
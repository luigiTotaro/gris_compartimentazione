﻿CREATE PROCEDURE [dbo].gris_GetHistoryDeviceSevLevel (@DevID BIGINT, @DataInizio AS DATETIME = NULL, @OraInizio AS DATETIME = NULL) AS
SET NOCOUNT ON;

SET @DataInizio = ISNULL(DATEADD(dd, DATEDIFF(dd, 0, @DataInizio), 0), '19000101');
SET @OraInizio = ISNULL(DATEADD(day, -DATEDIFF(day, 0, @OraInizio), @OraInizio), '00:00:00.000');

DECLARE @Now DATETIME;
SET @Now = GetDate();

SET @DataInizio = ISNULL(DATEADD(dd, DATEDIFF(dd, 0, @DataInizio), 0), '19000101');
SET @OraInizio = ISNULL(DATEADD(dd, -DATEDIFF(dd, 0, @OraInizio), @OraInizio), '00:00:00.000');

SELECT 
history_device_sevlevel.SevLevel,
Severity.Description AS SeverityDescription,
CASE
	WHEN history_device_sevlevel.Duration IS NULL THEN DATEDIFF(second, history_device_sevlevel.Created, @Now)
	ELSE history_device_sevlevel.Duration
END AS Duration, -- Nel caso manchi la data fine, assumiamo "adesso"
history_device_sevlevel.Created,
devices.Name AS DeviceName,
devices.[Type] AS RawType,
dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS [Type],
CASE
	WHEN history_device_sevlevel.Duration IS NULL THEN dbo.FormatSeconds(DATEDIFF(second, history_device_sevlevel.Created, @Now))
	ELSE dbo.FormatSeconds(history_device_sevlevel.Duration)
END AS DurationDescription
FROM history_device_sevlevel
INNER JOIN severity ON history_device_sevlevel.SevLevel = severity.SevLevel
INNER JOIN devices ON history_device_sevlevel.DevID = devices.DevID
WHERE
(history_device_sevlevel.DevID = @DevID)
AND (@DataInizio <= CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, history_device_sevlevel.Created))))
AND (@OraInizio <= DATEADD(dd, -DATEDIFF(dd, 0, history_device_sevlevel.Created), history_device_sevlevel.Created))
ORDER BY history_device_sevlevel.Created DESC;
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 22-09-2008
-- Description:	
-- Modified by: Andrea Bertolotto, per issue 10728 "Ricerca stazione con il GoTo"
-- Modified by: Cristian Storti, per issue 10915 => Estratto anche il numero totale di stlc, estratto il SevLevel anzich� il SevLevelReal
-- =============================================
CREATE PROCEDURE [dbo].[gris_GetNodesByInitialsOrIP]
@Initials BIT=1, @NodeNameOrIP VARCHAR (64)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF ( @Initials = 1 )
	BEGIN
		SELECT RegID, NodID, NodeName, IsDeleted, [0] as STLCOK, [9] as STLCInMaintenance, [0]+[3]+[9] as STLCTotalCount
		FROM 
		(
			SELECT DISTINCT
				regions.RegID, nodes.NodID, nodes.Name + ' (' + regions.Name + ' > ' + zones.Name + ')' as NodeName, ObjectId, object_servers_inservice.SevLevel, 1 AS MatchType, [servers].IsDeleted
			FROM object_servers_inservice
				INNER JOIN devices	ON devices.SrvID = object_servers_inservice.ObjectId
				INNER JOIN nodes	ON nodes.NodID = devices.NodID
				INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions	ON zones.RegID = regions.RegID
				INNER JOIN [servers] ON [servers].SrvID = object_servers_inservice.ObjectId
			WHERE (nodes.Name LIKE LTRIM(RTRIM(@NodeNameOrIP)))
			
			UNION ALL
			
			SELECT DISTINCT
				regions.RegID, nodes.NodID, nodes.Name + ' (' + regions.Name + ' > ' + zones.Name + ')' as NodeName, ObjectId, object_servers_inservice.SevLevel, 2 AS MatchType, [servers].IsDeleted
			FROM object_servers_inservice
				INNER JOIN devices	ON devices.SrvID = object_servers_inservice.ObjectId
				INNER JOIN nodes	ON nodes.NodID = devices.NodID 
				INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions	ON zones.RegID = regions.RegID
				INNER JOIN [servers] ON [servers].SrvID = object_servers_inservice.ObjectId
			WHERE (nodes.Name LIKE '%' + REPLACE(LTRIM(RTRIM(@NodeNameOrIP)), ' ', '%') + '%')
			AND (nodes.NodID NOT IN (
                SELECT nodes.NodID
                FROM object_servers_inservice
	                INNER JOIN devices	ON devices.SrvID = object_servers_inservice.ObjectId
	                INNER JOIN nodes	ON nodes.NodID = devices.NodID 
	                INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
	                INNER JOIN regions	ON zones.RegID = regions.RegID
                WHERE (nodes.Name LIKE LTRIM(RTRIM(@NodeNameOrIP)))			
			))
		) as A
		PIVOT
		(
			COUNT(ObjectId)
			FOR SevLevel
			IN ([0], [9], [3])
		) as p
		ORDER BY MatchType, NodeName;
	END
	ELSE
	BEGIN
		SET @NodeNameOrIP = @NodeNameOrIP + '%';

		SELECT RegID, NodID, NodeName, IsDeleted, [0] as STLCOK, [9] as STLCInMaintenance, [0]+[3]+[9] as STLCTotalCount
		FROM 
		(
			SELECT DISTINCT
				regions.RegID, nodes.NodID, nodes.Name + ' (' + regions.Name + ' > ' + zones.Name + ')' as NodeName, ObjectId, object_servers_inservice.SevLevel, [servers].IsDeleted
			FROM object_servers_inservice
				INNER JOIN servers	ON servers.SrvID = object_servers_inservice.ObjectId
				INNER JOIN devices	ON devices.SrvID = servers.SrvID
				INNER JOIN nodes	ON nodes.NodID = devices.NodID 
				INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions	ON zones.RegID = regions.RegID
			WHERE (servers.IP LIKE @NodeNameOrIP)
		) as A
		PIVOT
		(
			COUNT(ObjectId)
			FOR SevLevel
			IN ([0], [9], [3])
		) as p
		ORDER BY NodeName;
	END
END



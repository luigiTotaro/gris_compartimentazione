﻿CREATE PROCEDURE tf_UpdCachePostProcess 
	@ProcessFrom datetime
AS
BEGIN
	
	UPDATE cache_device_status
	SET OriginalSevLevel = SevLevel, SevLevel = dbo.GetTT10210DeviceSevLevel(cache_device_status.DevID,cache_log_messages.Created,cache_device_status.SevLevel)
	FROM cache_device_status 
		INNER JOIN cache_log_messages ON cache_device_status.LogID = cache_log_messages.LogID 
		INNER JOIN devices ON cache_device_status.DevID = devices.DevID
	WHERE (devices.Type = 'TT10210')
		AND (cache_device_status.Offline = 0) 
		AND (cache_device_status.SevLevel = 2) 
		AND cache_log_messages.Created >= @ProcessFrom
END
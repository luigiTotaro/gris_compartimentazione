CREATE PROCEDURE [dbo].[gris_UpdRefreshAlerts]
AS
BEGIN
	DECLARE @now AS DATETIME
	DECLARE @closedDate AS DATETIME
	
	SET @now = GETDATE()
	SET @closedDate = DATEADD(minute,-5,@now)
	
	-- storicizza i vecchi alerts (mantiene gli ulimi n giorni on line)
	EXEC tf_UpdArchiveAlerts 90

	-- inserisce/aggiorna gli offline delle periferiche STLC1000 
	EXEC [dbo].[util_InsDecivesSTLC1000] 0

	-- elimina le righe relative a device non attivi, device in stato 255, STLC1000 in Manutenzione
	DELETE FROM alerts_devices
	WHERE DevId IN (SELECT devices.DevId 
					FROM devices 
						INNER JOIN device_status ON devices.DevID = device_status.DevID 
						INNER JOIN servers_status servers ON servers.SrvId = devices.SrvId 
					WHERE ISNULL(Active,0) <> 1 or ISNULL(device_status.SevLevel,255) = 255 or servers.InMaintenance = 1)

	-- chiudi gli alerts_devices non più rilevati
	UPDATE alerts_devices_logs
	SET IsOpen = 0, CloseDate = @now, LastUpdate = @now
	FROM alerts_devices_logs 
		LEFT JOIN alerts_devices ON alerts_devices.DevID = alerts_devices_logs.DevID AND alerts_devices.RuleID = alerts_devices_logs.RuleID
	WHERE (alerts_devices_logs.IsOpen = 1) AND (alerts_devices.DevID IS NULL)

	-- aggiorna gli alerts_devices presenti su entrambe le tabelle
	UPDATE alerts_devices_logs
	SET LastUpdate = @now 
	FROM alerts_devices_logs INNER JOIN alerts_devices
			ON alerts_devices.DevID = alerts_devices_logs.DevID AND alerts_devices.RuleID = alerts_devices_logs.RuleID
	WHERE IsOpen = 1

	-- Inserisci gli alerts_devices nuovi
	INSERT INTO alerts_devices_logs (DevID, RuleID, LastUpdate, CreationDate, IsOpen)
	SELECT alerts_devices.DevID, alerts_devices.RuleID, GETDATE() AS LastUpdate, GETDATE() AS CreationDate, 1 AS IsOpen
	FROM alerts_devices 
		LEFT JOIN (SELECT DevID, RuleID FROM alerts_devices_logs WHERE IsOpen = 1)  AS AlertsDevicesLogsOpened 
			ON alerts_devices.DevID = AlertsDevicesLogsOpened.DevID AND alerts_devices.RuleID = AlertsDevicesLogsOpened.RuleID
	WHERE (AlertsDevicesLogsOpened.DevID IS NULL)
  
    -- chiudi i ticket agganciati a device che non hanno alerts aperti da più di 5 minuni
	UPDATE alerts_tickets
	SET IsOpen = 0, CloseDate = @now, LastUpdate = @now
	WHERE IsOpen = 1 
		AND DevID NOT IN (
		SELECT DISTINCT DevID FROM alerts_devices_logs
		WHERE (IsOpen = 1)OR(IsOpen = 0 AND ISNULL(CloseDate,'1900-01-01') > @closedDate)
		);

    -- aggiorna i ticket agganciati a device in errore
	UPDATE alerts_tickets
	SET LastUpdate = @now
	WHERE IsOpen = 1 AND DevID IN (SELECT DISTINCT DevID FROM alerts_devices_logs WHERE IsOpen = 1);

	-- crea nuovi tickets
	WITH devicesOnAlerts AS (SELECT DISTINCT DevID FROM alerts_devices_logs WHERE IsOpen = 1),
		alertsTicketsOpened AS (SELECT DISTINCT DevID FROM alerts_tickets WHERE IsOpen=1)
	INSERT INTO alerts_tickets (DevID, IsOpen, CreationDate)
	SELECT devicesOnAlerts.DevID, 1 AS IsOpen, @now AS CreationDate
	FROM devicesOnAlerts
		LEFT JOIN alertsTicketsOpened  ON devicesOnAlerts.DevID = alertsTicketsOpened.DevID
	WHERE (alertsTicketsOpened.DevID IS NULL)

END
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 23-03-2008
-- Description:	
-- =============================================
CREATE PROCEDURE util_GetNodesDeviceNoStreams 
AS
BEGIN
	SELECT DISTINCT regions.Name as RegName, zones.Name as ZoneName, nodes.Name as NodeName, servers.IP as IP_STLC
	FROM devices 
	LEFT JOIN stream_fields ON devices.DevID = stream_fields.DevID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN servers ON servers.SrvID = devices.SrvID
	WHERE (stream_fields.DevID IS NULL);
END



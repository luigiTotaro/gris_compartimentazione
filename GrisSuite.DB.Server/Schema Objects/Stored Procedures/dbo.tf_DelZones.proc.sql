﻿CREATE PROCEDURE [tf_DelZones]
(
	@Original_ZonID bigint
)
AS
	SET NOCOUNT ON;

	DELETE FROM [zones_subzones] WHERE (([ZonId] = @Original_ZonID))
	DELETE FROM [zones] WHERE (([ZonID] = @Original_ZonID))



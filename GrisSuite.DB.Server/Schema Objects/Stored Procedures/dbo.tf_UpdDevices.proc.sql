﻿CREATE PROCEDURE [dbo].[tf_UpdDevices]
(
	@DevID bigint,
	@NodID bigint,
	@SrvID int,
	@Name varchar(64),
	@Type varchar(16),
	@SN varchar(16),
	@PortID uniqueidentifier,
	@Addr varchar(32),
	@ProfileID int,
	@Active tinyint,
	@Scheduled tinyint,
	@Removed bit,
	@RackID uniqueidentifier,
	@RackPositionRow int,
	@RackPositionCol int,
	@DefinitionVersion varchar(8) = NULL,
	@ProtocolDefinitionVersion varchar(8) = NULL
)
AS
SET NOCOUNT OFF;

IF ISNULL(@Removed,0) = 0
BEGIN
	IF EXISTS(SELECT DevID FROM devices WHERE (DevID = @DevID))
	BEGIN
		UPDATE [devices] 
		SET
		[NodID] = @NodID, 
		[SrvID] = @SrvID, 
		[Name] = @Name, 
		[Type] = @Type, 
		[SN] = @SN, 
		[PortID] = @PortID, 
		[Addr] = @Addr, 
		[ProfileID] = @ProfileID, 
		[Active] = @Active, 
		[Scheduled] = @Scheduled, 
		RackID= @RackID,
		RackPositionRow= @RackPositionRow,
		RackPositionCol= @RackPositionCol,
		DefinitionVersion = @DefinitionVersion,
		ProtocolDefinitionVersion = @ProtocolDefinitionVersion
		WHERE ([DevID] = @DevID);
	END
	ELSE
	BEGIN
		INSERT INTO [devices] ([DevID], [NodID], [SrvID], [Name], [Type], [SN], [PortID], [Addr], [ProfileID], [Active], [Scheduled], RackID, RackPositionRow, RackPositionCol,DefinitionVersion, ProtocolDefinitionVersion)
		VALUES (@DevID, @NodID, @SrvID, @Name, @Type, @SN, @PortID, @Addr, @ProfileID, @Active, @Scheduled, @RackID,@RackPositionRow, @RackPositionCol, @DefinitionVersion, @ProtocolDefinitionVersion);
	END
END



﻿CREATE PROCEDURE [dbo].[gris_UpdServerParking] (@IP varchar(16), @SrvID bigint, @Host varchar(64), @Name varchar(64), @Type varchar(64), @ClientRegID varchar(64), @ClientZonID varchar(64), @ClientNodID varchar(64), @HardwareProfile varchar(64), @ParkingType tinyint) AS
SET NOCOUNT ON;

IF EXISTS (SELECT IP FROM servers_parking WHERE (IP LIKE @IP))
BEGIN
	UPDATE servers_parking
	SET
	SrvID = @SrvID,
	Host = ISNULL(@Host, ''),
	Name = @Name,
	[Type] = @Type,
	LastUpdate = GetDate(),
	ClientRegID = @ClientRegID,
	ClientZonID = @ClientZonID,
	ClientNodID = @ClientNodID,
	HardwareProfile = ISNULL(@HardwareProfile, ''),
	ParkingType = ISNULL(@ParkingType, 0)
	WHERE (IP LIKE @IP);
END
ELSE
BEGIN
	INSERT INTO servers_parking (IP, SrvID, Host, Name, [Type], LastUpdate, ClientRegID, ClientZonID, ClientNodID, HardwareProfile, ParkingType)
	VALUES (@IP, @SrvID, ISNULL(@Host, ''), @Name, @Type, GetDate(), @ClientRegID, @ClientZonID, @ClientNodID, ISNULL(@HardwareProfile, ''), ISNULL(@ParkingType, 0));
END
﻿CREATE PROCEDURE [dbo].[tf_GetServerById]
( @SrvID int)
AS
	SET NOCOUNT ON;
	SELECT SrvID, Name, Host, FullHostName, IP,LastUpdate, LastMessageType, SupervisorSystemXML, 
		ClientSupervisorSystemXMLValidated, ClientValidationSign, ClientDateValidationRequested, ClientDateValidationObtained, ClientKey, NodID, ServerVersion, MAC
	FROM servers
	WHERE SrvID = @SrvID



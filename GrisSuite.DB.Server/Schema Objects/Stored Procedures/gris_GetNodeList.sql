﻿CREATE PROCEDURE gris_GetNodeList(@VisibleRegions NVARCHAR(MAX), @RegIDs NVARCHAR(MAX), @ZonIDs NVARCHAR(MAX)) AS
SET NOCOUNT ON;

SELECT RegID, NodID, Name, [0] as STLCOK, [9] as STLCInMaintenance, [0]+[3]+[9] as STLCTotalCount
FROM 
(
	SELECT DISTINCT
		zones.RegID, nodes.NodID, nodes.Name, ObjectId, object_servers_inservice.SevLevel
	FROM object_servers_inservice
		INNER JOIN devices ON devices.SrvID = object_servers_inservice.ObjectId
		INNER JOIN nodes ON nodes.NodID = devices.NodID
		INNER JOIN zones ON nodes.ZonID = zones.ZonID 
		INNER JOIN [servers] ON [servers].SrvID = object_servers_inservice.ObjectId
		INNER JOIN iter_bigintlist_to_tbl(@VisibleRegions) VisibleRegions ON zones.RegID = VisibleRegions.number
	WHERE ((@RegIDs = '') OR (zones.RegID IN (SELECT Selected.number FROM iter_bigintlist_to_tbl(@RegIDs) Selected)))
	AND ((@ZonIDs = '') OR (nodes.ZonID IN (SELECT Selected.number FROM iter_bigintlist_to_tbl(@ZonIDs) Selected)))
	AND ([servers].IsDeleted = 0)
) AS A
PIVOT
(
	COUNT(ObjectId)
	FOR SevLevel
	IN ([0], [9], [3])
) AS p
ORDER BY Name;
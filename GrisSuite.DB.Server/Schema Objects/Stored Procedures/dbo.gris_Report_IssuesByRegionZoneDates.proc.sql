﻿CREATE PROCEDURE [dbo].[gris_Report_IssuesByRegionZoneDates] (@RegID BIGINT, @ZonID BIGINT, @DateIssueFrom DATETIME, @DateIssueTo DATETIME) AS
SET NOCOUNT ON;

SELECT DISTINCT
regions.Name AS [Compartimento], 
zones.Name AS [Linea], 
nodes.Name AS [Stazione], 
dbo.GetOnlyDate(geta.issues.DateIssue) AS [Data intervento], 
geta.GetOperatorsByIssue(IssueID, 1) AS [Operatore Gris],
ISNULL(activity_groups.ActivityGroupName, '') AS [Gruppo],
ISNULL(geta.target_activities.TargetActivityName, '') as [Attività],
geta.issue_states.IssueStateName AS [Stato],
ISNULL(geta.issues.IssueComment, '') AS [Note],
ISNULL(geta.issues.TargetActivityValue, '') AS [Attività valore],  
ISNULL(geta.issue_activity_types.IssueActivityTypeName, '') AS [Tipo intervento],
ISNULL(servers.MAC,'') AS [Server MAC],
ISNULL(servers.IP,'') AS [Server IP],
geta.issues.TargetData AS [Stazione Originale]	
FROM geta.issues
INNER JOIN nodes ON geta.issues.TargetID = nodes.NodID
INNER JOIN zones ON nodes.ZonID = zones.ZonID
INNER JOIN regions ON zones.RegID = regions.RegID
LEFT JOIN geta.issue_activity_types ON geta.issues.IssueActivityTypeID = geta.issue_activity_types.IssueActivityTypeID 
LEFT JOIN geta.target_activities ON geta.issues.TargetActivityID = geta.target_activities.TargetActivityID
LEFT JOIN geta.activity_groups ON geta.target_activities.ActivityGroupsID = geta.activity_groups.ActivityGroupID
LEFT JOIN geta.issue_states ON geta.issues.IssueStateID = geta.issue_states.IssueStateID
INNER JOIN servers ON servers.NodID = nodes.NodID
WHERE (geta.issues.TargetTypeID = 2) -- Per nodo
AND ((zones.RegID = @RegID) OR (@RegID = -1))
AND ((zones.ZonID = @ZonID) OR (@ZonID = -1))
AND ((geta.issues.DateIssue >= dbo.GetOnlyDate(@DateIssueFrom)) OR (@DateIssueFrom IS NULL))
AND ((geta.issues.DateIssue <= dbo.GetMidnightFromDate(@DateIssueTo)) OR (@DateIssueTo IS NULL))
AND (geta.issues.IsDeleted = 0)

UNION ALL

SELECT DISTINCT
regions.Name, 
zones.Name, 
'N/D', 
dbo.GetOnlyDate(geta.issues.DateIssue),
geta.GetOperatorsByIssue(IssueID, 1),
ISNULL(activity_groups.ActivityGroupName, ''),
ISNULL(geta.target_activities.TargetActivityName,''), 
geta.issue_states.IssueStateName,
ISNULL(geta.issues.IssueComment, ''),
ISNULL(geta.issues.TargetActivityValue, ''),
ISNULL(geta.issue_activity_types.IssueActivityTypeName, ''),
'N/D',
'N/D',
geta.issues.TargetData
FROM regions
INNER JOIN zones ON regions.RegID = zones.RegID
INNER JOIN geta.issues ON zones.ZonID = geta.issues.TargetID
LEFT JOIN geta.issue_activity_types ON geta.issues.IssueActivityTypeID = geta.issue_activity_types.IssueActivityTypeID 
LEFT JOIN geta.target_activities ON geta.issues.TargetActivityID = geta.target_activities.TargetActivityID
LEFT JOIN geta.activity_groups ON geta.target_activities.ActivityGroupsID = geta.activity_groups.ActivityGroupID
LEFT JOIN geta.issue_states ON geta.issues.IssueStateID = geta.issue_states.IssueStateID
WHERE (geta.issues.TargetTypeID = 1) -- Per linea
AND ((zones.RegID = @RegID) OR (@RegID = -1))
AND ((zones.ZonID = @ZonID) OR (@ZonID = -1))
AND ((geta.issues.DateIssue >= dbo.GetOnlyDate(@DateIssueFrom)) OR (@DateIssueFrom IS NULL))
AND ((geta.issues.DateIssue <= dbo.GetMidnightFromDate(@DateIssueTo)) OR (@DateIssueTo IS NULL))
AND (geta.issues.IsDeleted = 0)
ORDER BY [Compartimento], [Linea], [Data intervento] DESC
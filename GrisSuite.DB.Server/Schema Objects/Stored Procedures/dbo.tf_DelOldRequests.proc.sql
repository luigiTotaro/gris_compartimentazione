﻿CREATE PROCEDURE [dbo].[tf_DelOldRequests]
@KeepDays INT = 365
AS
DECLARE @limit DATETIME

SET @limit = DATEADD(d,-@KeepDays,GETDATE())

	DELETE FROM requests
	WHERE (Created < @limit)
	
RETURN
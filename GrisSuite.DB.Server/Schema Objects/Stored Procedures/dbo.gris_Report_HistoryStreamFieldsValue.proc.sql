﻿



CREATE PROCEDURE [dbo].[gris_Report_HistoryStreamFieldsValue](
	 @DataInizio AS datetime = null
   , @DataFine AS datetime = null
   , @GiorniDellaSettimana AS varchar(15) = null
   , @OraInizio AS datetime = null
   , @OraFine AS datetime = null
   , @ListaSistemi AS varchar(Max) = null
   , @Stazione AS varchar(64) = null
   , @Linea AS varchar(64) = null
   , @Compartimento AS varchar(64) = null
   , @DurataMin AS int = null
   , @DurataMax AS int = null
   , @ListaSevLevel AS varchar(64) = null
   , @ListaTipiCampi AS varchar(Max) = null	)
AS
BEGIN
	SET @DataInizio = ISNULL(DATEADD(dd, DATEDIFF(dd, 0, @DataInizio), 0), '19000101');
	SET @DataFine = ISNULL(DATEADD(dd, DATEDIFF(dd, 0, @DataFine), 0), '30000101');
	SET @OraInizio = ISNULL(DATEADD(day, -DATEDIFF(day, 0, @OraInizio), @OraInizio), '00:00:00.000');
	SET @OraFine = ISNULL(DATEADD(day, -DATEDIFF(day, 0, @OraFine), @OraFine), '23:59:59.999');
	SET @GiorniDellaSettimana = CASE WHEN RTRIM(ISNULL(@GiorniDellaSettimana, '')) = '' THEN '|1|2|3|4|5|6|7|' ELSE @GiorniDellaSettimana END;
	SET @ListaSistemi = CASE WHEN RTRIM(ISNULL(@ListaSistemi, '')) = '' THEN '' ELSE @ListaSistemi END;
	SET @Stazione = '%' + ISNULL(@Stazione, '') + '%';
	SET @Compartimento = '%' + ISNULL(@Compartimento, '') + '%';
	SET @Linea = '%' + ISNULL(@Linea, '') + '%';
	SET @DurataMin = ISNULL(@DurataMin, 0);
	SET @DurataMax = ISNULL(@DurataMax, 2147483647);
	SET @ListaSevLevel = CASE WHEN RTRIM(ISNULL(@ListaSevLevel, '')) = '' THEN '|0|1|2|9|255|' ELSE @ListaSevLevel END; 
	SET @ListaTipiCampi = CASE WHEN RTRIM(ISNULL(@ListaTipiCampi, '')) = '' THEN '' ELSE @ListaTipiCampi END;

	--- giorno della settimana contando lunedì come 1°
	SET DATEFIRST 1;

	--SELECT TOP 65529 
    SELECT TOP 65529 
		  regions.Name AS Compartimento
		 , zones.Name AS Linea
		 , nodes.Name AS Stazione
		 , H.DevID 
		 , H.DevType as CodiceTipoPeriferica
		 , Device_type.DefaultName as NomeTipoPeriferica
		 , Device_type.DeviceTypeDescription as DescrizioneTipoPeriferica
		 , FT.StreamName AS NomeStream
		 , H.Name AS NomeValore
		 , H.ArrayID AS ArrayId
		 , H.Value as Valore
		 , H.Created AS InizioValore
		 , H.Duration AS Durata
		 , CASE
		   WHEN ISNULL(H.Duration,0) > 0 THEN DATEADD(second, H.Duration, H.Created)
		   ELSE NULL
		   END AS FineValore
		 , H.SevLevel AS CodiceStato
		 , Severity.Description	AS Stato
		 , servers.IP AS STLC_IP
		 , servers.MAC AS STLC_MAC
		 , systems.SystemDescription AS Sistema
		 , H.StrID AS StreamId
		 , H.FieldID AS FieldId
	FROM dbo.history_stream_fields_value AS H INNER JOIN device_type ON H.DevType = device_type.DeviceTypeID
									  INNER JOIN nodes ON H.NodId = nodes.NodID
									  INNER JOIN servers ON H.SrvId = servers.SrvID
									  INNER JOIN severity ON H.SevLevel = severity.SevLevel
									  INNER JOIN stream_fields_type FT ON H.DevType = FT.DevType AND H.StrID = FT.StrID AND H.FieldID = FT.FieldID
									  LEFT JOIN systems ON device_type.SystemID = systems.SystemID
									  LEFT JOIN zones ON nodes.ZonID = zones.ZonID
									  LEFT JOIN regions ON zones.RegID = regions.RegID
	WHERE @DataInizio <= DATEADD(dd, DATEDIFF(dd, 0, ISNULL(DATEADD(second, H.Duration, H.Created), '30000101')), 0)
	  AND @DataFine >= DATEADD(dd, DATEDIFF(dd, 0, H.Created), 0)
	  AND @OraInizio <= DATEADD(day, -DATEDIFF(day, 0, ISNULL(DATEADD(second, H.Duration, H.Created), '23:59:59.999')), ISNULL(DATEADD(second, H.Duration, H.Created), '23:59:59.999'))
	  AND @OraFine >= DATEADD(day, -DATEDIFF(day, 0, H.Created), H.Created)
	  AND (CHARINDEX('|' + CAST(DATEPART(weekday, H.Created)AS varchar(1)) + '|', @GiorniDellaSettimana, 0) > 0
		OR CASE
		   WHEN NOT H.Duration IS NULL THEN CHARINDEX('|' + CAST(DATEPART(weekday, DATEADD(second, H.Duration, H.Created))AS varchar(1)) + '|', @GiorniDellaSettimana, 0)
		   ELSE 0
		   END > 0)
	  AND (@ListaSistemi = '' OR CHARINDEX('|' + CAST(systems.SystemID AS VARCHAR(Max)) + '|', @ListaSistemi, 1) > 0)
	  AND ISNULL(nodes.Name, '')LIKE @Stazione
	  AND ISNULL(zones.Name, '')LIKE @Linea
	  AND ISNULL(regions.Name, '')LIKE @Compartimento
	  AND ISNULL(H.Duration, 2147483647) >= @DurataMin
	  AND ISNULL(H.Duration, 2147483647) <= @DurataMax
	  AND CHARINDEX('|' + CAST(ISNULL(H.SevLevel, 0)AS varchar(max)) + '|', @ListaSevLevel, 0) > 0
	  AND (@ListaTipiCampi = '' OR CHARINDEX('|' + CAST(FT.FieldTypeId AS VARCHAR(Max)) + '|', @ListaTipiCampi, 1) > 0)
	  and NOT H.Duration  IS NULL
	ORDER BY regions.Name, zones.Name, nodes.Name, H.DevID, H.StrID, H.FieldID, H.ArrayID,H.Created 	;

END;
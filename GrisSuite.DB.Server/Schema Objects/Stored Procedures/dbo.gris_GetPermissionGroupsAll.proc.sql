﻿CREATE PROCEDURE gris_GetPermissionGroupsAll
AS
BEGIN
  SET NOCOUNT ON;

  SELECT
    GroupID,
    GroupName,
    GroupDescription,
    IsBuiltIn,
    WindowsGroups,
    RegCode,
    Level,
    SortOrder,
    (CASE WHEN EXISTS(SELECT r.ResourceID
                      FROM permissions p
                        INNER JOIN resources r ON p.ResourceID = r.ResourceID
                      WHERE p.GroupID = g.GroupID )
          THEN CAST(1 AS BIT)
          ELSE CAST(0 AS BIT)
     END) | IsBuiltIn AS Associated,
    dbo.GetContactsByGroupId(GroupID, 'windows_user') AS WindowsUsers
  FROM permission_groups g
  ORDER BY SortOrder;
END
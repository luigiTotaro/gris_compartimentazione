﻿CREATE PROCEDURE [dbo].[util_UpdServerLastUpdate] (@ForceUpdate INT = 0) AS
BEGIN
	SET NOCOUNT ON;
	
	IF (@ForceUpdate <> 0)
	BEGIN
		UPDATE dbo.servers
		SET [LastUpdate] = DATEADD(n, -(CONVERT(INT, (RAND(((DATEPART(ms, [LastUpdate])) * 10)) * 1000)) % 59), GetDate())
		WHERE ([LastUpdate] IS NOT NULL)	
	END
	ELSE
	BEGIN
		DECLARE @LastUpdate DATETIME;

		SELECT @LastUpdate = MAX(LastUpdate) FROM dbo.servers;

		IF (@LastUpdate IS NOT NULL)
		BEGIN
			UPDATE dbo.servers
			SET [LastUpdate] = DATEADD(n, -(CONVERT(INT, (RAND(((DATEPART(ms, [LastUpdate])) * 10)) * 1000)) % 59), GetDate())
			WHERE ([LastUpdate] IS NOT NULL)
			AND (dbo.GetOnlyDate(LastUpdate) = dbo.GetOnlyDate(@LastUpdate))
		END
	END
END
GO
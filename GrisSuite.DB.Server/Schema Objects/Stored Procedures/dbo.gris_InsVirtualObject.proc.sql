﻿
create procedure gris_InsVirtualObject
(
	@ParentObjectStatusId as uniqueidentifier,
	@VirtualObjectName as varchar(500),
	@VirtualObjectDescription as varchar(4000),
	@VirtualObjectId as bigint output,
	@VirtualObjectStatusId as uniqueidentifier output
) as
begin

	set xact_abort on;

	--declare @ParentObjectStatusId as uniqueidentifier;
	--declare @VirtualObjectName as varchar(500);
	--declare @VirtualObjectDescription as varchar(4000);

	--set @ParentObjectStatusId = '6eee6efe-e5d8-de11-93a6-001e0b6ca3e0';
	--set @VirtualObjectName = 'Fondamentali';
	--set @VirtualObjectDescription = 'Dispositivi Fondamentali per il funzionamento della stazione';

	--declare @VirtualObjectId as bigint;
	--declare @VirtualObjectStatusId as uniqueidentifier;

	if exists (select * from object_status where ObjectStatusId = @ParentObjectStatusId)
		begin	
			begin tran
			
			insert into virtual_objects (VirtualObjectName, VirtualObjectDescription) values (@VirtualObjectName, @VirtualObjectDescription);
			set @VirtualObjectID = SCOPE_IDENTITY();
			
			insert into object_status (
				ObjectId, ObjectTypeId, 
				SevLevel, SevLevelDetailId, 
				ParentObjectStatusId, ForcedSevLevel)
			values (@VirtualObjectId, 7 /* virt obj */, 
				9, NULL, 
				@ParentObjectStatusId, 0);
			set @VirtualObjectStatusId = (select ObjectStatusId from object_status where ObjectTypeId = 7 and ObjectId = @VirtualObjectId);
				
			commit tran
		end

	set xact_abort off;

end
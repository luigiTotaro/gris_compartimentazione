﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	Diagnostica Amplificatore DT04000 (se TT10210=pannellozone è Errore e DT04000=aplificatore è offline)
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsRuleDT04000]
	@RuleID int = 0 
AS
BEGIN
	WITH
	AmpliOffline AS (
		SELECT devices.DevId, devices.NodId
		FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID
		WHERE [Type] = 'DT04000' 
			AND (device_status.Offline = 1)
	),
	PZoneKO AS (
		SELECT devices.DevId, devices.NodId
		FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID
		WHERE ([Type] = 'TT10210' OR [Type] = 'TT10210V3' OR [Type] = 'TT10220')
			AND (device_status.Offline = 0)
			AND (device_status.SevLevel = 2)
	)
	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT DISTINCT AmpliOffline.DevId, @RuleID as RuleID
		FROM AmpliOffline 
		INNER JOIN PZoneKO ON AmpliOffline.NodId = PZoneKO.NodId
END



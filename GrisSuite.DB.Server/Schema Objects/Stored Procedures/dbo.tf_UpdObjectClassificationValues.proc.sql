﻿CREATE procedure [tf_UpdObjectClassificationValues] (
	@ObjectStatusId uniqueidentifier,
	@ClassificationValueId smallint,
	@MapLayerId int ,
	@MapOffsetX float ,
	@MapOffsetY float ,
	@CaptionFromScale float,
	@ClassificationId tinyint,
	@ForcedMapPoints varchar(max) = NULL,
	@NodeID bigint = NULL,
	@HasNoDeviceChecked bit = NULL)
AS
SET NOCOUNT ON;

DECLARE @ClassificationValueIdPrec SMALLINT;

SET @ClassificationValueIdPrec = ( SELECT object_classification_values.ClassificationValueId FROM object_classification_values 
									INNER JOIN classification_values ON object_classification_values.ClassificationValueId = classification_values.ClassificationValueId
									WHERE (classification_values.ClassificationId = @ClassificationId) AND (object_classification_values.ObjectStatusId = @ObjectStatusId));

IF (@ClassificationValueIdPrec IS NULL)
BEGIN
	INSERT INTO object_classification_values (ObjectStatusId, ClassificationValueId, MapLayerId, MapOffsetX, MapOffsetY, CaptionFromScale)
	VALUES (@ObjectStatusId, @ClassificationValueId, @MapLayerId, @MapOffsetX, @MapOffsetY, @CaptionFromScale);
END
ELSE
BEGIN
	UPDATE object_classification_values
	SET
	ClassificationValueId = @ClassificationValueId,
	MapLayerId = @MapLayerId,
	MapOffsetX = @MapOffsetX,
	MapOffsetY = @MapOffsetY, 
	CaptionFromScale = @CaptionFromScale
	WHERE (ObjectStatusId = @ObjectStatusId)
	AND (ClassificationValueId = @ClassificationValueIdPrec);
END

IF (RTRIM(ISNULL(@ForcedMapPoints, '')) <> '')
BEGIN
	-- Siamo in forzatura di coordinate
	IF EXISTS (SELECT MapPoints FROM object_map_points WHERE ObjectStatusId = @ObjectStatusId)
	BEGIN
		UPDATE object_map_points
		SET
		MapPoints = @ForcedMapPoints,
		UpdateDate = getdate()
		WHERE (ObjectStatusId = @ObjectStatusId);
	END
	ELSE
	BEGIN
		INSERT INTO object_map_points (ObjectStatusId, MapPoints)
		VALUES (@ObjectStatusId, @ForcedMapPoints);
	END
END
ELSE
BEGIN
	DECLARE @NodId BIGINT;
	SET @NodId = (SELECT ObjectId FROM object_nodes WHERE ObjectStatusId = @ObjectStatusId);
	
	IF (@NodId IS NOT NULL)
	BEGIN
		DECLARE @NodIdDecoded VARCHAR(6);
		DECLARE @MapPoints VARCHAR(MAX);
		
		IF (dbo.GetDecodedNodId(@NodId) <= 9999)
		BEGIN
			SET @MapPoints = (SELECT nodesGIS.MapPoints FROM nodesGIS WHERE NodeGISId = 'LO' + RIGHT('0000' + CAST(dbo.GetDecodedNodId(@NodId) AS VARCHAR(MAX)),4));
								  
			IF (@MapPoints IS NULL)
			BEGIN
				RAISERROR ('Coordinate geografiche non trovate', 16, 1 );
			END
								  
			IF EXISTS (SELECT MapPoints FROM object_map_points WHERE ObjectStatusId = @ObjectStatusId)
			BEGIN
				UPDATE object_map_points
				SET
				MapPoints = @MapPoints,
				UpdateDate = getdate()
				WHERE (ObjectStatusId = @ObjectStatusId);
			END
			ELSE
			BEGIN
				INSERT INTO object_map_points (ObjectStatusId,MapPoints)
				VALUES (@ObjectStatusId, @MapPoints);
			END
		END		
	END
END

IF ((@NodeID IS NOT NULL) AND (@HasNoDeviceChecked IS NOT NULL))
BEGIN
    EXEC tf_UpdNoDeviceByNodeId @NodID = @NodeID, @Checked = @HasNoDeviceChecked;
END
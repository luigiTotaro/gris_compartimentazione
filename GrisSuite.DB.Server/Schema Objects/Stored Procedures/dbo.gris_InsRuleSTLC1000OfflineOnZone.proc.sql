﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	STLC1000 in offline che hanno "sotto" nella linea n STLC1000 in offline
-- =============================================
CREATE PROCEDURE gris_InsRuleSTLC1000OfflineOnZone
	@RuleID int = 0,
	@CountOfflineOnZone int = 0
AS
	BEGIN
	WITH StartZoneMeters AS
	(
	SELECT nodes.ZonID, MIN(ISNULL(meters,0)) AS Meters
	FROM zones INNER JOIN nodes ON zones.ZonID = nodes.ZonID
	GROUP BY nodes.ZonID
	), 
	FirstServerOnZone AS
	(
	SELECT DISTINCT devices.DevID, nodes.ZonID
		FROM devices 
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN 	StartZoneMeters ON nodes.ZonID = StartZoneMeters.ZonID AND nodes.Meters = StartZoneMeters.Meters
		INNER JOIN device_status on devices.DevID = device_status.DevID
		WHERE (devices.Type = 'STLC1000') AND (device_status.Offline = 1)
	)
	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT FirstServerOnZone.DevID, @RuleID As RuleID
		FROM devices 
		INNER JOIN nodes on devices.NodID = nodes.NodID
		INNER JOIN FirstServerOnZone on nodes.ZonID = FirstServerOnZone.ZonID
		INNER JOIN device_status on devices.DevID = device_status.DevID
	WHERE (devices.Type = 'STLC1000') AND (device_status.Offline = 1)
	GROUP BY FirstServerOnZone.DevID
	HAVING COUNT(devices.DevID) > @CountOfflineOnZone;


	-- cancella tutte le regole scattate precedentemente sui devices di tipo STLC1000 inclusi nelle linee in errore
	WITH StartZoneMeters AS
	(
	SELECT nodes.ZonID, MIN(ISNULL(meters,0)) AS Meters
	FROM zones INNER JOIN nodes ON zones.ZonID = nodes.ZonID
	GROUP BY nodes.ZonID
	), 
	FirstServerOnZone AS
	(
	SELECT DISTINCT devices.DevID, nodes.ZonID
		FROM devices 
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN 	StartZoneMeters ON nodes.ZonID = StartZoneMeters.ZonID AND nodes.Meters = StartZoneMeters.Meters
		INNER JOIN device_status on devices.DevID = device_status.DevID
		WHERE (devices.Type = 'STLC1000') AND (device_status.Offline = 1)
	),
	DevicesOnAlerts AS
	(
	SELECT FirstServerOnZone.DevID, FirstServerOnZone.ZonID
		FROM devices 
		INNER JOIN nodes on devices.NodID = nodes.NodID
		INNER JOIN FirstServerOnZone on nodes.ZonID = FirstServerOnZone.ZonID
		INNER JOIN device_status on devices.DevID = device_status.DevID
	WHERE (devices.Type = 'STLC1000') AND (device_status.Offline = 1)
	GROUP BY FirstServerOnZone.DevID, FirstServerOnZone.ZonID
	HAVING COUNT(devices.DevID) > @CountOfflineOnZone
	)
	DELETE FROM alerts_devices 
	WHERE RuleID <> @RuleID AND DevID IN 
	(SELECT DISTINCT devices.DevID
		FROM devices 
		INNER JOIN nodes on devices.NodID = nodes.NodID
		INNER JOIN DevicesOnAlerts on nodes.ZonID = DevicesOnAlerts.ZonID
		INNER JOIN device_status on devices.DevID = device_status.DevID
	WHERE (devices.Type = 'STLC1000') AND (device_status.Offline = 1))
END



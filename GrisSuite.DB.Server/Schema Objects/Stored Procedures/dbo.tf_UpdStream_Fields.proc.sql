﻿CREATE PROCEDURE [dbo].[tf_UpdStream_Fields]
(
	@FieldID int,
	@ArrayID int,
	@StrID int,
	@DevID bigint,
	@Name varchar(64),
	@SevLevel int,
	@Value varchar(1024),
	@Description text,
	@Visible tinyint,
	@IsDeleted bit,
	@ReferenceID uniqueidentifier,
	@ShouldSendNotificationByEmail tinyint = 0
)
AS
SET NOCOUNT OFF;

IF EXISTS (SELECT FieldID FROM stream_fields WHERE (ArrayID = @ArrayID) AND (DevID = @DevID) AND (FieldID = @FieldID) AND (StrID = @StrID))
BEGIN
	IF @IsDeleted = 0
	BEGIN
		UPDATE [stream_fields]
		SET
		[Name] = @Name,
		[SevLevel] = @SevLevel,
		[Value] = @Value,
		[Description] = @Description,
		[Visible] = @Visible,
		[ReferenceID] = @ReferenceID,
		[ShouldSendNotificationByEmail] = ISNULL(@ShouldSendNotificationByEmail, 0)
		WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
	END
	ELSE
	BEGIN
		DELETE FROM [stream_fields]
		WHERE (([FieldID] = @FieldID) AND ([ArrayID] = @ArrayID) AND ([StrID] = @StrID) AND ([DevID] = @DevID));
	END
END
ELSE
BEGIN
	INSERT INTO [stream_fields] ([FieldID], [ArrayID], [StrID], [DevID], [Name], [SevLevel], [Value], [Description], [Visible], [ReferenceID], [ShouldSendNotificationByEmail])
	VALUES (@FieldID, @ArrayID, @StrID, @DevID, @Name, @SevLevel, @Value, @Description, @Visible, @ReferenceID, ISNULL(@ShouldSendNotificationByEmail, 0));
END



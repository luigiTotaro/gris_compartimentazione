﻿-- ================================================
-- Author:		Dam (supported by Cristian)
-- Create date: 20.05.2009
-- Description:	Elenco ultimo accesso raggruppato
-- per giorno da una certa data per compartimento
-- ================================================
CREATE PROCEDURE [dbo].[util_GetLastAccessByRegIDFromDate] 
	@RegName varchar(20), 
	@Date DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @RegID BIGINT;
	SET @RegID = (select RegID from regions where [name] like '%' + @RegName + '%');

	SELECT      max(sessions.Created) as 'Ultimo Accesso',
				sessions.[User],
				sessions.Groups,
				(select [name] from regions where [name] like '%' + @RegName + '%') AS 'Compartimento'
	FROM         requests INNER JOIN
                      sessions ON requests.SessionKey = sessions.SessionKey
	WHERE     (requests.Page LIKE '%region%') AND
				(requests.QueryString LIKE '?RegID=' + (cast(@RegID as varchar(20))) + '%') AND
				sessions.Created > @Date
	group by convert(char(10),sessions.Created,112), sessions.[User], sessions.Groups
END
﻿CREATE PROCEDURE dbo.gris_getSeverityDetails
AS
	SET NOCOUNT ON;
SELECT     SevLevelDetailId, SevLevel, Description
FROM         severity_details
ORDER BY SevLevel, SevLevelDetailId
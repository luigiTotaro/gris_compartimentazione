﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	Device che sono entrati in offline per più di n volte negli ultimi di m minuti
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsRuleDevicesOfflineVar]
	@RuleID int = 0, 
	@Minute int = 0,
	@OfflineCount int = 0
AS
BEGIN
	WITH LastDeviceUpdate
	AS (
	SELECT servers.LastUpdate, devices.DevId
	FROM devices INNER JOIN servers ON devices.SrvID = servers.SrvID
	)
	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT DISTINCT device_status_offlinechange.DevID, @RuleID AS RuleID
	FROM device_status_offlinechange 
		INNER JOIN LastDeviceUpdate ON device_status_offlinechange.DevID = LastDeviceUpdate.DevID
	WHERE (device_status_offlinechange.Created > DATEADD(minute, @Minute * -1, LastUpdate)) 
		AND (device_status_offlinechange.Offline = 1)
	GROUP BY device_status_offlinechange.DevID
	HAVING (COUNT(device_status_offlinechange.Offline) >= @OfflineCount)
END



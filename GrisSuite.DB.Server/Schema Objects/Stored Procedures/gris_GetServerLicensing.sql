﻿CREATE PROCEDURE [dbo].[gris_GetServerLicensing] AS
SET NOCOUNT ON;

SELECT
ServerLicensingId,
LicensingLastUpdate,
HardwareProfile,
AcquiredLicenses,
UsedLicenses,
(ISNULL(AcquiredLicenses, 0) - ISNULL(UsedLicenses, 0)) AS AvailableLicenses
FROM (
	SELECT
	servers_licensing.ServerLicensingId,
	servers_licensing.LicensingLastUpdate,
	servers_licensing.HardwareProfile,
	CONVERT(INT, dbo.fn_EncryptDecryptString(servers_licensing.LicensingData)) AS AcquiredLicenses,
	-- Il profilo hardware può contenere un sottocodice dopo il punto, che deve essere rimosso per la gestione licenze per cliente
	(SELECT COUNT(servers_licenses.ServerLicenseID) FROM servers_licenses WHERE (dbo.GetHardwareProfileCustomer(servers_licenses.HardwareProfile) = servers_licensing.HardwareProfile) AND (servers_licenses.IsLicensed = 1)) AS UsedLicenses
	FROM servers_licensing
) InnerData
ORDER BY HardwareProfile;
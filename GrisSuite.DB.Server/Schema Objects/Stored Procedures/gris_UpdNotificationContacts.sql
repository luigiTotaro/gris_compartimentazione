﻿CREATE PROCEDURE [dbo].[gris_UpdNotificationContacts] (@Associated BIT, @ObjectStatusId UNIQUEIDENTIFIER, @ContactId BIGINT) AS
SET NOCOUNT ON;

IF (@Associated = 1)
BEGIN
	IF NOT EXISTS (SELECT NotificationContactId FROM notification_contacts WHERE (ObjectStatusId = @ObjectStatusId) AND (ContactId = @ContactId))
	BEGIN
		INSERT INTO notification_contacts (ObjectStatusId, ContactId)
		VALUES (@ObjectStatusId, @ContactId)
	END
END
ELSE IF (@Associated = 0)
BEGIN
	DELETE FROM notification_contacts
	WHERE (ObjectStatusId = @ObjectStatusId) AND (ContactId = @ContactId)
END
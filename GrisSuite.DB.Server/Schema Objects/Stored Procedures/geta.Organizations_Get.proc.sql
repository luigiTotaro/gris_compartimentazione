﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 26-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[Organizations_Get]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT geta.organizations.OrganizationID, geta.organizations.OrganizationName, COUNT(users.UserID) as UsersCount
	FROM geta.organizations 
	LEFT JOIN users ON geta.organizations.OrganizationID = users.OrganizationID
	GROUP BY geta.organizations.OrganizationID, geta.organizations.OrganizationName
	ORDER BY geta.organizations.OrganizationName
END



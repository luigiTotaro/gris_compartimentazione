﻿CREATE PROCEDURE [dbo].[tf_UpdServerStatusByIP]
(
	@IP varchar(16),
	@LastUpdate datetime,
	@LastMessageType varchar(64)
)
AS
	SET NOCOUNT OFF;

	DECLARE @ServerCount TINYINT;
	
	SET @ServerCount = (SELECT COUNT(*) FROM [servers] WHERE ISNULL(IP,'') = @IP);
	
	IF ( @ServerCount = 1 )
	BEGIN
		UPDATE [servers] 
		SET
		[IP] = @IP,
		[LastUpdate] = @LastUpdate,
		[LastMessageType] = @LastMessageType,
		[IsDeleted] = 0 -- Se stiamo centralizzando dati di un server, questo non può essere cancellato
		WHERE ISNULL(IP,'') = @IP;
	END
	
	SELECT @ServerCount;



﻿CREATE PROCEDURE [dbo].[gris_GetServerCertification]
	@SrvID int
AS
	SELECT SrvID, AccountDocValidation, DateDocValidation, AccountUTValidation, DateUTValidation
	FROM servers
	WHERE SrvID = @SrvID;



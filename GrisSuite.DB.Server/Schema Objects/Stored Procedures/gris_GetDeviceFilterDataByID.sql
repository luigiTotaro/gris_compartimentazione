﻿CREATE PROCEDURE [dbo].[gris_GetDeviceFilterDataByID] (@DeviceFilterId BIGINT) AS
BEGIN
  SET NOCOUNT ON;

  SELECT
    DeviceFilterName,
    UserID,
    RegIDs,
    RegNames,
    ZonIDs,
    ZonNames,
    NodIDs,
    NodNames,
    SystemIDs,
    SystemNames,
    DeviceTypeIDs,
    DeviceTypeNames,
    SevLevelIDs,
    DeviceData,
    (SELECT Username
     FROM users
     WHERE (UserID LIKE device_filters.UserID)) AS Username,
    CAST(CASE WHEN ResourceID IS NULL
      THEN 0
         ELSE 1 END AS BIT)                     AS IsShared,
    ResourceID
  FROM device_filters
  WHERE (DeviceFilterId = @DeviceFilterId)
END
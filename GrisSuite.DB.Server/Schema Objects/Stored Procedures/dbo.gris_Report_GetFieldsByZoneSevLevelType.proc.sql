﻿CREATE PROCEDURE [dbo].[gris_Report_GetFieldsByZoneSevLevelType] (@RegID bigint,
						@ZonID bigint,
						@NodID bigint,
						@SevLevelList varchar(64) = '',
						@DeviceType varchar(16) = '', 
						@Message varchar(64) = '',
						@IncludeInMaintenance bit = 0)
AS
SELECT @RegID AS RegIDParameter,
       @ZonID AS ZonIDParameter,
       @NodID AS NodIDParameter,
       regions.RegID,
       regions.Name AS Compartimento,
       zones.ZonID,
       zones.Name AS Linea,
       nodes.NodID,
       nodes.Name AS Stazione,
       devices.DevID,
       devices.Type AS TipoPeriferica,
       devices.Name AS NomePeriferica,
       (CASE WHEN (CAST (ISNULL(devices.addr, '0') AS BIGINT) > 255) THEN dbo.GetIPStringFromInt(devices.addr) ELSE devices.addr END) AS Indirizzo,
       device_status.Offline,
       stream_fields.SevLevel,
       streams.Name AS NomeStream,
       streams.DateTime AS StreamDate,
       stream_fields.Name AS NomeCampo,
       stream_fields.arrayid AS ArrayIndex,
       stream_fields.Value,
       [dbo].GetFieldDescriptionBySevLevel(stream_fields.Description, '|2|', 0) AS Errori,
       [dbo].GetFieldDescriptionBySevLevel(stream_fields.Description, '|1|', 0) AS Avvisi,
       [dbo].GetFieldDescriptionBySevLevel(stream_fields.Description, '|0|', 0) AS Informazioni,
       [dbo].GetFieldDescriptionBySevLevel(stream_fields.Description, '|-255|', 0) AS Info,
       (CASE WHEN (ISNULL(object_devices.SevLevel, -1) = 9) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END) AS InMaintenance,
	   (CASE WHEN devices.Type LIKE 'INFSTAZ1' THEN
		dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) + ' - ' +
		dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) + ' - ' +
		dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 7, devices.DevID)
		ELSE 'N/D' END) AS InfostazioniData
FROM devices
     INNER JOIN
     device_status
     ON devices.DevID = device_status.DevID
     LEFT OUTER JOIN
     object_devices
     ON devices.DevID = object_devices.ObjectId
     INNER JOIN
     streams
     ON device_status.DevID = streams.DevID
     INNER JOIN
     stream_fields
     ON stream_fields.DevID = streams.DevID
        AND stream_fields.StrID = streams.StrID
     INNER JOIN
     nodes
     ON nodes.NodID = devices.NodID
     INNER JOIN
     zones
     ON zones.ZonID = nodes.ZonID
     INNER JOIN
     regions
     ON regions.RegID = zones.RegID
WHERE (regions.RegID = @RegID
       OR @RegID = -1)
      AND (zones.ZonID = @ZonID
           OR @ZonID = -1)
      AND (nodes.NodID = @NodID
           OR @NodID = -1)
      AND (@DeviceType = ''
           OR devices.type LIKE @DeviceType)
      AND (@SevLevelList = ''
           OR charindex('|' + CAST (stream_fields.SevLevel AS VARCHAR (5)) + '|', @SevLevelList) > 0)
      AND (@Message = ''
           OR charindex(@Message, stream_fields.Description) > 0)
      AND (@IncludeInMaintenance = 1
           OR ISNULL(object_devices.SevLevel, -1) <> 9)
      AND stream_fields.visible = 1
ORDER BY Stazione, TipoPeriferica, NomePeriferica, [Offline], stream_fields.SevLevel;


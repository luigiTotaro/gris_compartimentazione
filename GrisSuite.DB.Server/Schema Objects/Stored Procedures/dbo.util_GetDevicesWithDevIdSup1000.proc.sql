﻿CREATE PROCEDURE util_GetDevicesWithDevIdSup1000
AS
select d.DevId, 
	d.[Name] as Periferica, 
	d.[Type], 
	dbo.GetDecodedDevId(d.DevID) as DevId_Decoded,
	n1.Name as Stazione, z1.Name as Linea, r1.Name as Compartimento,
	s.Host,
	s.IP,
	s.MAC
from devices d
			left join nodes n1 on n1.NodID = d.NodID
			left join zones z1 on n1.ZonID = z1.ZonID
			left join regions r1 on z1.RegID = r1.RegID
			left join servers s on d.SrvId = s.SrvId
where dbo.GetDecodedDevId(DevID) >= 1000 and [Type] <> 'STLC1000'
RETURN



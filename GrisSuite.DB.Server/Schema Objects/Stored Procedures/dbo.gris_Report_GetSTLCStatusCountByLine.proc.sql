CREATE PROCEDURE [dbo].[gris_Report_GetSTLCStatusCountByLine]

AS
BEGIN

	DECLARE @All SMALLINT, @AllOnline SMALLINT, @AllOffline SMALLINT, @AllInMaint SMALLINT, @AllUndefined SMALLINT;

	SELECT 
		@AllOnline =		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = 0) THEN object_servers_inservice.ObjectId ELSE NULL END),
		@AllOffline =		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = 3) THEN object_servers_inservice.ObjectId ELSE NULL END),
		@AllInMaint =		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = 9) THEN object_servers_inservice.ObjectId ELSE NULL END),
		@AllUndefined =		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = -1) THEN object_servers_inservice.ObjectId ELSE NULL END),
		@All =				COUNT(DISTINCT object_servers_inservice.ObjectId)
	FROM object_servers_inservice

	SELECT 
		regions.Name as RegionName, zones.ZonID as ZoneId, zones.Name as ZoneName,
		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = 0) THEN object_servers_inservice.ObjectId ELSE NULL END) as [Online],
		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = 3) THEN object_servers_inservice.ObjectId ELSE NULL END) as [Offline],
		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = 9) THEN object_servers_inservice.ObjectId ELSE NULL END) as [InMaint],
		COUNT(DISTINCT CASE WHEN (object_servers_inservice.SevLevel = -1) THEN object_servers_inservice.ObjectId ELSE NULL END) as [Undefined],
		COUNT(DISTINCT object_servers_inservice.ObjectId) AS [AllByLine],
		@AllOnline as AllOnline, @AllOffline as AllOffline, @AllInMaint as AllInMaint, @AllUndefined as AllUndefined, @All as [All]
	FROM object_servers_inservice
	--INNER JOIN devices ON object_servers_inservice.ObjectId = devices.SrvID
	INNER JOIN nodes ON object_servers_inservice.NodID = nodes.NodID
	INNER JOIN zones ON nodes.ZonID = zones.ZonID
	INNER JOIN regions ON zones.RegID = regions.RegID
	GROUP BY regions.Name, zones.ZonID, zones.Name
	ORDER BY RegionName, ZoneName;

END
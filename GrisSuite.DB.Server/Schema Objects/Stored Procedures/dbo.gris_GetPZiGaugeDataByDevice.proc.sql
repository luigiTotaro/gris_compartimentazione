﻿CREATE PROCEDURE [dbo].[gris_GetPZiGaugeDataByDevice] 
	@DevID BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	-- contiene tutti i fields per determinare l'installazione o meno di amplificatori e zone
	DECLARE @EnabledAmpliZones TABLE
	(
		FieldId INT,
		[Enabled] BIT
	);
	
	INSERT INTO @EnabledAmpliZones VALUES (7, dbo.IsFlaggedInBitmask(@DevID, 1/* stream */, 7, 0/* arrayid */, 0/* bit index */));
	INSERT INTO @EnabledAmpliZones VALUES (12, dbo.IsFlaggedInBitmask(@DevID, 1, 12, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (17, dbo.IsFlaggedInBitmask(@DevID, 1, 17, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (22, dbo.IsFlaggedInBitmask(@DevID, 1, 22, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (28, dbo.IsFlaggedInBitmask(@DevID, 1, 28, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (31, dbo.IsFlaggedInBitmask(@DevID, 1, 31, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (34, dbo.IsFlaggedInBitmask(@DevID, 1, 34, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (37, dbo.IsFlaggedInBitmask(@DevID, 1, 37, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (40, dbo.IsFlaggedInBitmask(@DevID, 1, 40, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (43, dbo.IsFlaggedInBitmask(@DevID, 1, 43, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (46, dbo.IsFlaggedInBitmask(@DevID, 1, 46, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (49, dbo.IsFlaggedInBitmask(@DevID, 1, 49, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (52, dbo.IsFlaggedInBitmask(@DevID, 1, 52, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (55, dbo.IsFlaggedInBitmask(@DevID, 1, 55, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (58, dbo.IsFlaggedInBitmask(@DevID, 1, 58, 0, 0));
	INSERT INTO @EnabledAmpliZones VALUES (61, dbo.IsFlaggedInBitmask(@DevID, 1, 61, 0, 0));
	
	-- contiene tutti i fields con i dati di valore attuale, valore di riferimento e attenuazioni di amplificatori e zone
	DECLARE @AmpliZonesFields TABLE
	(
		FieldId INT,
		IsNightAttenuated BIT,
		InstalledField INT
	);
		
	INSERT INTO @AmpliZonesFields VALUES (8/* fieldid */, 0/* riferito ad attenuazione notturna */, 7 /* fieldid per l'attivazione */);
	INSERT INTO @AmpliZonesFields VALUES (9, 1, 7);
	INSERT INTO @AmpliZonesFields VALUES (10, 0, 7);
	INSERT INTO @AmpliZonesFields VALUES (11, 1, 7);
	INSERT INTO @AmpliZonesFields VALUES (13, 0, 12);
	INSERT INTO @AmpliZonesFields VALUES (14, 1, 12);
	INSERT INTO @AmpliZonesFields VALUES (15, 0, 12);
	INSERT INTO @AmpliZonesFields VALUES (16, 1, 12);
	INSERT INTO @AmpliZonesFields VALUES (18, 0, 17);
	INSERT INTO @AmpliZonesFields VALUES (19, 1, 17);
	INSERT INTO @AmpliZonesFields VALUES (20, 0, 17);
	INSERT INTO @AmpliZonesFields VALUES (21, 1, 17);
	INSERT INTO @AmpliZonesFields VALUES (23, 0, 22);
	INSERT INTO @AmpliZonesFields VALUES (24, 1, 22);
	INSERT INTO @AmpliZonesFields VALUES (25, 0, 22);
	INSERT INTO @AmpliZonesFields VALUES (26, 1, 22);
	INSERT INTO @AmpliZonesFields VALUES (29, NULL, 28);
	INSERT INTO @AmpliZonesFields VALUES (30, NULL, 28);
	INSERT INTO @AmpliZonesFields VALUES (32, NULL, 31);
	INSERT INTO @AmpliZonesFields VALUES (33, NULL, 31);
	INSERT INTO @AmpliZonesFields VALUES (35, NULL, 34);
	INSERT INTO @AmpliZonesFields VALUES (36, NULL, 34);
	INSERT INTO @AmpliZonesFields VALUES (38, NULL, 37);
	INSERT INTO @AmpliZonesFields VALUES (39, NULL, 37);
	INSERT INTO @AmpliZonesFields VALUES (41, NULL, 40);
	INSERT INTO @AmpliZonesFields VALUES (42, NULL, 40);
	INSERT INTO @AmpliZonesFields VALUES (44, NULL, 43);
	INSERT INTO @AmpliZonesFields VALUES (45, NULL, 43);
	INSERT INTO @AmpliZonesFields VALUES (47, NULL, 46);
	INSERT INTO @AmpliZonesFields VALUES (48, NULL, 46);
	INSERT INTO @AmpliZonesFields VALUES (50, NULL, 49);
	INSERT INTO @AmpliZonesFields VALUES (51, NULL, 49);
	INSERT INTO @AmpliZonesFields VALUES (53, NULL, 52);
	INSERT INTO @AmpliZonesFields VALUES (54, NULL, 52);
	INSERT INTO @AmpliZonesFields VALUES (56, NULL, 55);
	INSERT INTO @AmpliZonesFields VALUES (57, NULL, 55);
	INSERT INTO @AmpliZonesFields VALUES (59, NULL, 58);
	INSERT INTO @AmpliZonesFields VALUES (60, NULL, 58);
	INSERT INTO @AmpliZonesFields VALUES (62, NULL, 61);
	INSERT INTO @AmpliZonesFields VALUES (63, NULL, 61);

	WITH Fields AS (
		SELECT
			sf.FieldID
			, CASE WHEN CHARINDEX('amplificator', sf.Name) > 0 THEN 'A' ELSE 'Z' END as OutputType
			, CASE WHEN CHARINDEX('riferimento', sf.Name) = 0 THEN RTRIM(LTRIM(REPLACE(REPLACE(sf.Name, 'diurna', ''), 'notturna', ''))) ELSE '' END as Name
			, CASE WHEN CHARINDEX('riferimento', sf.Name) = 0 THEN sf.Value ELSE '' END as ActualValue
			, CASE WHEN CHARINDEX('riferimento', sf.Name) > 0 THEN sf.Value ELSE '' END as ReferenceValue
			, f.InstalledField
		FROM @AmpliZonesFields as f
		INNER JOIN @EnabledAmpliZones as ez ON ez.FieldId = f.InstalledField
		INNER JOIN stream_fields sf ON sf.FieldID = f.FieldId
		INNER JOIN streams s ON (s.DevID = sf.DevID AND s.StrID = sf.StrID)
		INNER JOIN devices d ON d.DevID = s.DevID
		WHERE (d.DevID = @DevID)
		AND sf.StrID = 1 
		AND sf.ArrayID = 0
		AND sf.Visible = 1
		AND ez.[Enabled] = 1
		AND (f.IsNightAttenuated IS NULL OR f.IsNightAttenuated = dbo.IsFlaggedInBitmask(@DevID, 1, 1, 0, 5)) -- ricava dalla bitmask del field 1 dello stream 1 se usare i valori di attenuazione
	), ErrorRanges AS (
		-- soglie percentuali di errore e attenzione
		SELECT
			(CASE WHEN CHARINDEX('amplificator', sf.Name) > 0 THEN 'A' ELSE 'Z' END) as OutputType
			, (CASE WHEN CHARINDEX('errore', sf.Name) > 0 THEN sf.Value ELSE '' END) as ErrorValue
			, (CASE WHEN CHARINDEX('attenzione', sf.Name) > 0 THEN sf.Value ELSE '' END) as WarningValue
		FROM devices d
		INNER JOIN streams s ON d.DevID = s.DevID
		INNER JOIN stream_fields sf ON (s.DevID = sf.DevID AND s.StrID = sf.StrID)
		WHERE (d.DevID = @DevID)
		AND sf.StrID = 2
		AND (sf.FieldID = 11 OR sf.FieldID = 12 OR sf.FieldID = 13 OR sf.FieldID = 14)
		AND sf.ArrayID = 0
		AND sf.Visible = 1
	), Result AS (
		SELECT
			MIN(FieldID) as FieldID 
			, MAX(Name) as Name
			, MAX(ActualValue) as ActualValue
			, MAX(ReferenceValue) as ReferenceValue
			, MAX(WarningValue) as WarningValue
			, MAX(ErrorValue) as ErrorValue
		FROM Fields f
		INNER JOIN ErrorRanges er ON f.OutputType = er.OutputType
		GROUP BY InstalledField
	)
	SELECT
		Name
		, CASE WHEN ((CHARINDEX('test non eseguito', [Description]) > 0) AND (CHARINDEX('0.0', ActualValue) > 0)) THEN 'N/D' ELSE ActualValue END as ActualValue
		, ReferenceValue, WarningValue, ErrorValue, [Description]
	FROM (
		SELECT Name, ActualValue, ReferenceValue, WarningValue, ErrorValue,
			-- Ci sono casi in cui la prima stringa dei valori accodati non è quella corretta, ma deve essere estratta quella con severità non Ok, ad esempio le impedenze a 0 Ohm per test non eseguito
			(SELECT dbo.GetTT10220DescriptionFromStreamFieldData(@DevID, Name, FieldID)
			   FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = r.FieldID) AS [Description]
		FROM Result r
	) data;
END
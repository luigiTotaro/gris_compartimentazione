﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	Device che sono entrati nello stato x per più di n volte negli ultimi di m minuti
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsRuleDevicesOnSevLevelVar]
	@RuleID int = 0, 
	@Minute int = 0,
	@SevLevel int = 0,
	@SevLevelCount int = 0
AS
BEGIN
	WITH LastDeviceUpdate
	AS (
	SELECT servers.LastUpdate, devices.DevId
	FROM devices INNER JOIN servers ON devices.SrvID = servers.SrvID
				 INNER JOIN device_status ON devices.DevId = device_status.DevId
	WHERE (device_status.Offline = 0)
	)
	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT DISTINCT device_status_sevlevelchange.DevID, @RuleID AS RuleID
	FROM device_status_sevlevelchange
		INNER JOIN LastDeviceUpdate ON device_status_sevlevelchange.DevID = LastDeviceUpdate.DevID
	WHERE (device_status_sevlevelchange.Created > DATEADD(minute, @Minute * -1, LastUpdate)) AND (device_status_sevlevelchange.SevLevel = @SevLevel)
	GROUP BY device_status_sevlevelchange.DevID
	HAVING (COUNT(device_status_sevlevelchange.SevLevel) >= @SevLevelCount)
END



﻿/*-- =============================================
-- Author:		Cristian Storti
-- Create date: 2007-11-08
-- Description:	Estrae gli stream fields delle periferiche di un compartimento mostrando se sono considerate in errore e gli scostamenti rispetto al valore di riferimento
-- =============================================
CREATE PROCEDURE [dbo].[gris_Report_GetDeviceStreamFieldsVariationByRegion] 
	@RegID BIGINT, 
	@FieldID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Name VARCHAR(64)
	SELECT @Name = [Name] FROM regions WHERE RegID = @RegID;

	WITH ALLRegionDevicesOfType
	AS
	(
		SELECT devices.DevID, devices.Name as DeviceName, devices.type as DeviceType
		FROM devices
		INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId -- solo quelle che hanno informazioni di stato
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN zones ON nodes.ZonID = zones.ZonID
		WHERE (zones.RegID = @RegID) 
		AND (devices.type = 'TT10210' OR devices.type = 'TT10210V3') -- solo per i pannelli a zone
	), StreamFieldsDescriptions
	AS
	(
		SELECT
			stream_fields.DevID,
			DeviceName,
			DeviceType,
			FieldID,
			stream_fields.Name AS FieldName,
			stream_fields.StrID,
			streams.Name AS StreamName,
			ArrayID,
			stream_fields.Value AS [Value], 
			reference.Value AS [ReferenceValue],
			(
				SELECT [Value]
				FROM stream_fields
				WHERE (DevID = reference.DeltaDevID) 
				AND (StrID = reference.DeltaStrID) 
				AND (FieldID = reference.DeltaFieldID) 
				AND (ArrayID = reference.DeltaArrayID)
			) AS [DeltaValue]
		FROM stream_fields
		INNER JOIN streams ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
		INNER JOIN reference ON stream_fields.ReferenceID = reference.ReferenceID
		INNER JOIN ALLRegionDevicesOfType ON ALLRegionDevicesOfType.DevID = stream_fields.DevID
		WHERE (reference.Visible = 1) 
		AND (stream_fields.Visible = 1) 
		AND (stream_fields.FieldID = @FieldID)
	)
	SELECT
		DevID,
		DeviceName,
		DeviceType,
		StrID,
		StreamName,
		FieldID,
		FieldName,
		ArrayID,
		CAST(LTRIM(RTRIM(SUBSTRING(StreamFieldsDescriptions.Value, 1, (CHARINDEX(' ', StreamFieldsDescriptions.Value))))) AS VARCHAR(10)) AS [ValueString], 
		CAST(LTRIM(RTRIM(SUBSTRING(StreamFieldsDescriptions.ReferenceValue, 1, (CHARINDEX(' ', StreamFieldsDescriptions.ReferenceValue))))) AS VARCHAR(10)) AS [ReferenceValueString], 
		CAST(LTRIM(RTRIM(SUBSTRING(StreamFieldsDescriptions.DeltaValue, 1, (CHARINDEX(' ', StreamFieldsDescriptions.DeltaValue))))) AS VARCHAR(10)) AS [DeltaValueString]
	INTO #TempResultSet
	FROM StreamFieldsDescriptions;

	WITH StreamFieldsValues
	AS
	(
		SELECT
			DevID,
			DeviceName,
			DeviceType,
			StrID,
			StreamName,
			FieldID,
			FieldName,
			ArrayID,
			CAST([ValueString] AS DECIMAL(19, 5)) AS [Value], 
			CAST([ReferenceValueString] AS DECIMAL(19, 5)) AS [ReferenceValue], 
			CAST([DeltaValueString] AS DECIMAL(19, 5)) AS [DeltaValue]
		FROM #TempResultSet
	)
	SELECT DISTINCT
		@RegID AS RegionID,
		@Name AS Region,
		DeviceType AS DevType, 
		(station.StationName + ' > ' + building.BuildingName + ' > ' + rack.RackName) AS Location,
		StreamFieldsValues.DevID,
		DeviceName,
		StrID,
		StreamName,
		FieldID,
		FieldName,
		ArrayID,
		[Value],
		[ReferenceValue],
		[DeltaValue],
		CASE WHEN (
					([Value] >= ([ReferenceValue] - [DeltaValue]))
					AND 
					([Value] <= ([ReferenceValue] + [DeltaValue]))
					) THEN 0 ELSE 1 END AS [Error],	
		dbo.IsValueInPercentRangeLesserThen([Value], [ReferenceValue], 32767, 15) AS [Lesser15Percent],
		dbo.IsValueInPercentRangeLesserThen([Value], [ReferenceValue], 15, 10) AS [Lesser10Percent],
		dbo.IsValueInPercentRangeLesserThen([Value], [ReferenceValue], 10, 5) AS [Lesser5Percent],
		dbo.IsValueInPercentComparedTo([Value], [ReferenceValue], 5) AS [Inside5Percent],
		dbo.IsValueInPercentRangeGreaterThen([Value], [ReferenceValue], 5, 10) AS [Greater5Percent],
		dbo.IsValueInPercentRangeGreaterThen([Value], [ReferenceValue], 10, 15) AS [Greater10Percent],
		dbo.IsValueInPercentRangeGreaterThen([Value], [ReferenceValue], 15, 32767) AS [Greater15Percent]
	FROM StreamFieldsValues
	INNER JOIN devices ON devices.DevID = StreamFieldsValues.DevID
	INNER JOIN rack ON devices.RackID = rack.RackID 
	INNER JOIN building ON rack.BuildingID = building.BuildingID 
	INNER JOIN station ON building.StationID = station.StationID
	ORDER BY DevID, StrID, FieldID, ArrayID;
	
	DROP TABLE #TempResultSet;

END*/




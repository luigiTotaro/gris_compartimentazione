﻿CREATE PROCEDURE [dbo].[gris_GetSeverity] AS
SET NOCOUNT ON;
SELECT SevLevel, [Description], IsRelevantToDevice, IsRelevantToServer, IsRelevantToCustomColors
FROM severity
ORDER BY SevLevel
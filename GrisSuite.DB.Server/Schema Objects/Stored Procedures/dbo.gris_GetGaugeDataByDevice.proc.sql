﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 01-07-2008
-- Description:	
-- =============================================
CREATE PROCEDURE gris_GetGaugeDataByDevice 
	@DevID bigint, 
	@DeviceType varchar(16)
AS
BEGIN
	SET NOCOUNT ON;

	IF ( @DeviceType = 'TT10210V3' )
	BEGIN
		DECLARE @ImpedenzaZonaBitMask VARBINARY(16);
		DECLARE @ParametriNotturni BIT;

		-- maschera di bit per l'identificazione dei record di definizione dei valori di impedenza zona attivi
		SELECT @ImpedenzaZonaBitMask = ISNULL(CAST(dbo.fn_hexstrtovarbin(stream_fields.Value) AS VARBINARY(16)), CAST(0 AS VARBINARY(16)))
		FROM devices
		INNER JOIN streams ON devices.DevID = streams.DevID 
		INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
		WHERE (devices.DevID = @DevID)
		AND stream_fields.StrID = 2 AND stream_fields.FieldID = 8 AND stream_fields.ArrayID = 0
		AND stream_fields.Visible = 1;

		-- utilizzare i record relativi all'attenuazione notturna
		SELECT 
			@ParametriNotturni =
			(CASE WHEN 
				(
					ISNULL(CAST(dbo.fn_hexstrtovarbin(stream_fields.Value) AS VARBINARY(16)), CAST(0 AS VARBINARY(16)))
				& POWER(2, 5))
				= POWER(2, 5)
			THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END)
		FROM devices
		INNER JOIN streams ON devices.DevID = streams.DevID 
		INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
		WHERE (devices.DevID = @DevID)
		AND stream_fields.StrID = 1 AND stream_fields.FieldID = 0 AND stream_fields.ArrayID = 0
		AND stream_fields.Visible = 1;

		WITH ImpedenzaZonaActiveIndexes AS
		(
			SELECT 0 AS [INDEX] WHERE (@ImpedenzaZonaBitMask & POWER(2, 0)) = POWER(2, 0)
			UNION ALL
			SELECT 1 WHERE (@ImpedenzaZonaBitMask & POWER(2, 1)) = POWER(2, 1)
			UNION ALL
			SELECT 2 WHERE (@ImpedenzaZonaBitMask & POWER(2, 2)) = POWER(2, 2)
			UNION ALL
			SELECT 3 WHERE (@ImpedenzaZonaBitMask & POWER(2, 3)) = POWER(2, 3)
			UNION ALL
			SELECT 4 WHERE (@ImpedenzaZonaBitMask & POWER(2, 4)) = POWER(2, 4)
			UNION ALL
			SELECT 5 WHERE (@ImpedenzaZonaBitMask & POWER(2, 5)) = POWER(2, 5)
			UNION ALL
			SELECT 6 WHERE (@ImpedenzaZonaBitMask & POWER(2, 6)) = POWER(2, 6)
			UNION ALL
			SELECT 7 WHERE (@ImpedenzaZonaBitMask & POWER(2, 7)) = POWER(2, 7)
			UNION ALL
			SELECT 8 WHERE (@ImpedenzaZonaBitMask & POWER(2, 8)) = POWER(2, 8)
			UNION ALL
			SELECT 9 WHERE (@ImpedenzaZonaBitMask & POWER(2, 9)) = POWER(2, 9)
			UNION ALL
			SELECT 10 WHERE (@ImpedenzaZonaBitMask & POWER(2, 10)) = POWER(2, 10)
			UNION ALL
			SELECT 11 WHERE (@ImpedenzaZonaBitMask & POWER(2, 11)) = POWER(2, 11)
		), TensioneAmplificatoreActiveIndexes AS
		(
			SELECT 0 AS [INDEX] WHERE 
			(
				SELECT stream_fields.Value
				FROM devices
				INNER JOIN streams ON devices.DevID = streams.DevID 
				INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
				WHERE (devices.DevID = @DevID)
				AND stream_fields.StrID = 2 AND stream_fields.FieldID = 9 AND stream_fields.ArrayID = 0
				AND stream_fields.Visible = 1
			) <> 0
			UNION ALL
			SELECT 1 WHERE
			(
				SELECT stream_fields.Value
				FROM devices
				INNER JOIN streams ON devices.DevID = streams.DevID 
				INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
				WHERE (devices.DevID = @DevID)
				AND stream_fields.StrID = 2 AND stream_fields.FieldID = 9 AND stream_fields.ArrayID = 1
				AND stream_fields.Visible = 1
			) <> 0
		), Base AS
		(
			SELECT 
				stream_fields.StrID, stream_fields.FieldID, stream_fields.ArrayID, 
				stream_fields.Name + ' ' + CAST(stream_fields.ArrayID + 1 AS VARCHAR(2)) as [Name],
				stream_fields.Value, stream_fields.Description,
				1 as FieldType -- impedenza zona
			FROM devices
			INNER JOIN streams ON devices.DevID = streams.DevID 
			INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
			WHERE (devices.DevID = @DevID)
			AND devices.Type = @DeviceType
			AND stream_fields.ArrayID IN (SELECT [INDEX] FROM ImpedenzaZonaActiveIndexes)
			AND stream_fields.Visible = 1
			UNION ALL
			SELECT 
				stream_fields.StrID, stream_fields.FieldID, stream_fields.ArrayID, 
				stream_fields.Name + ' ' + CAST(stream_fields.ArrayID + 1 AS VARCHAR(2)) as [Name],
				stream_fields.Value, stream_fields.Description,
				2 as FieldType -- tensione amplificatore
			FROM devices
			INNER JOIN streams ON devices.DevID = streams.DevID 
			INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
			WHERE (devices.DevID = @DevID)
			AND devices.Type = @DeviceType
			AND stream_fields.ArrayID IN (SELECT [INDEX] FROM TensioneAmplificatoreActiveIndexes)
			AND stream_fields.Visible = 1
		), GaugeValues AS
		(
			SELECT FieldID, ArrayID, FieldType, [Name], [Value], [Description]
			FROM Base
			WHERE (FieldType = 1 AND StrID = 1 AND FieldID = 9) 
			OR (FieldType = 2 AND StrID = 1 AND FieldID = 8)
		), GaugeRefs AS
		(
			SELECT ArrayID, FieldType, [Value] as RefValue
			FROM Base
			WHERE (FieldType = 1 AND StrID = 1 AND FieldID = 12)
			OR (FieldType = 2 AND ((@ParametriNotturni = 0 AND StrID = 1 AND FieldID = 10/* riferimento diurno */) OR (@ParametriNotturni = 1 AND StrID = 1 AND FieldID = 11/* riferimento notturno */)))
		), GaugeDeltas AS
		(
			SELECT ArrayID, FieldType, [Value] as DeltaValue
			FROM Base
			WHERE (FieldType = 1 AND StrID = 2 AND FieldID = 11)
			OR (FieldType = 2 AND ((@ParametriNotturni = 0 AND StrID = 2 AND FieldID = 10/* intervallo diurno */) OR (@ParametriNotturni = 1 AND StrID = 2 AND FieldID = 22/* intervallo notturno */)))
		)
		SELECT GaugeValues.FieldID, GaugeValues.ArrayID, [Name], [Value], RefValue, DeltaValue, [Description]
		FROM GaugeValues
		INNER JOIN GaugeRefs ON GaugeValues.ArrayID = GaugeRefs.ArrayID AND GaugeValues.FieldType = GaugeRefs.FieldType
		INNER JOIN GaugeDeltas ON GaugeRefs.ArrayID = GaugeDeltas.ArrayID AND GaugeRefs.FieldType = GaugeDeltas.FieldType
		ORDER BY GaugeValues.FieldID, GaugeValues.ArrayID;
	END
	ELSE IF ( @DeviceType = 'DT04000' )
	BEGIN
		WITH Base AS
		(
			SELECT 
				stream_fields.StrID, stream_fields.FieldID, stream_fields.ArrayID,
				stream_fields.Name + CASE WHEN (stream_fields.ArrayID <> 0) THEN ' ' + CAST(stream_fields.ArrayID + 1 AS VARCHAR(2)) ELSE '' END as [Name],
				stream_fields.Value, stream_fields.Description
			FROM streams 
			INNER JOIN devices ON streams.DevID = devices.DevID 
			INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID 
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			WHERE stream_fields.DevID = @DevID 
			AND device_type.DeviceTypeID = @DeviceType
			AND stream_fields.Visible = 1
		), GaugeValues AS
		(
			SELECT 1 as ValueType, [Name], [Value], [Description]
			FROM Base
			WHERE (StrID = 1 AND FieldID = 3 AND ArrayID = 0)
			UNION ALL
			SELECT 2 as ValueType, [Name], [Value], [Description]
			FROM Base
			WHERE (StrID = 1 AND FieldID = 4 AND ArrayID = 0)
			UNION ALL
			SELECT 3 as ValueType, [Name], [Value], [Description]
			FROM Base
			WHERE (StrID = 1 AND FieldID = 7 AND ArrayID = 0)
			UNION ALL
			SELECT 4 as ValueType, [Name], [Value], [Description]
			FROM Base
			WHERE (StrID = 1 AND FieldID = 8 AND ArrayID = 0)
		), GaugeMinValues AS
		(
			SELECT 1 as ValueType, [Value] as MinValue
			FROM Base
			WHERE (StrID = 2 AND FieldID = 5 AND ArrayID = 0)
			UNION ALL
			SELECT 2 as ValueType, [Value]
			FROM Base
			WHERE (StrID = 2 AND FieldID = 7 AND ArrayID = 0)
			UNION ALL
			SELECT 3 as ValueType, [Value]
			FROM Base
			WHERE (StrID = 2 AND FieldID = 13 AND ArrayID = 0)
			UNION ALL
			SELECT 4 as ValueType, [Value]
			FROM Base
			WHERE (StrID = 2 AND FieldID = 15 AND ArrayID = 0)
		), GaugeMaxValues AS
		(
			SELECT 1 as ValueType, [Value] as MaxValue
			FROM Base
			WHERE (StrID = 2 AND FieldID = 6 AND ArrayID = 0)
			UNION ALL
			SELECT 2 as ValueType, [Value]
			FROM Base
			WHERE (StrID = 2 AND FieldID = 8 AND ArrayID = 0)
			UNION ALL
			SELECT 3 as ValueType, [Value]
			FROM Base
			WHERE (StrID = 2 AND FieldID = 14 AND ArrayID = 0)
			UNION ALL
			SELECT 4 as ValueType, [Value]
			FROM Base
			WHERE (StrID = 2 AND FieldID = 16 AND ArrayID = 0)
		)
		SELECT [Name], [Value], NULL as RefValue, NULL as DeltaValue, MinValue, MaxValue, [Description]
		FROM GaugeValues
		INNER JOIN GaugeMinValues ON GaugeValues.ValueType = GaugeMinValues.ValueType
		INNER JOIN GaugeMaxValues ON GaugeMinValues.ValueType = GaugeMaxValues.ValueType;
	END
END



﻿create procedure util_ExcludePlatinumFromParentStatus
AS
BEGIN
	WITH platinum AS (SELECT DISTINCT ObjectStatusId, ClassificationValueId
	FROM object_classification_values
	WHERE (ClassificationValueId = 1))
	UPDATE object_status
	SET ExcludeFromParentStatus = 1
	FROM object_status LEFT OUTER JOIN
	platinum AS platinum_1 ON object_status.ObjectStatusId = platinum_1.ObjectStatusId
	WHERE     (object_status.ObjectTypeId = 3) AND (ISNULL(platinum_1.ClassificationValueId, 0) = 1)
END
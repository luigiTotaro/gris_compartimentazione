﻿CREATE PROCEDURE gris_InsContact (@RegId BIGINT, @ContactName VARCHAR(2000), @PhoneNumber VARCHAR(500), @Email VARCHAR(500), @WindowsUser VARCHAR(500)) AS
SET NOCOUNT ON;

IF NOT EXISTS (SELECT ContactId FROM contacts WHERE (RegId = @RegId) AND (Name LIKE @ContactName))
BEGIN
	IF (LEN(ISNULL(@ContactName, '')) > 0)
	BEGIN
		INSERT INTO contacts
		(RegId, Name, PhoneNumber, Email, WindowsUser)
		VALUES
		(@RegId, @ContactName, @PhoneNumber, @Email, @WindowsUser);
	END
END
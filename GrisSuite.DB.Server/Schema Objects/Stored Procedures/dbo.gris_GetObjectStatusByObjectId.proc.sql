﻿CREATE PROCEDURE [dbo].[gris_GetObjectStatusByObjectId]
(@ObjectId BIGINT, @ObjectTypeId INT)
AS
BEGIN
	DECLARE @floatZero FLOAT;
	SET @floatZero = 0;

	SELECT object_status.ObjectStatusId
		,object_status.ObjectId
		,ObjectTypeId
		,object_status.SevLevel
		,object_status.SevLevelDetailId
		,object_status.SevLevelDetailIdLast
		,object_status.SevLevelDetailIdReal
		,object_status.SevLevelLast
		,object_status.SevLevelReal
		,'' AS Name
		,ISNULL(object_map_points.MapPoints,'') as MapPoints 
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,severity.Description AS SevLevelDescription
		,@floatZero as MapOffsetX
		,@floatZero as MapOffsetY
		,@floatZero+30 as CaptionFromScale
		,dbo.IsWithServerObject(object_status.ObjectStatusId) AS WithServer
		,'' AS GroupName
		,dbo.GetAttributeForObject(8 /* Color */, object_status.ObjectStatusId, NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS CustomColor
	FROM object_status
		INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
		LEFT JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
	WHERE (object_status.ObjectId = @ObjectId)
		AND (object_status.ObjectTypeId = @ObjectTypeId)
END
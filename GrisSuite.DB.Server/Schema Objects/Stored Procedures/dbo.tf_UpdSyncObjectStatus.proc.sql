﻿CREATE PROCEDURE [dbo].[tf_UpdSyncObjectStatus]
AS

SET NOCOUNT ON;

-- aggiorna la node_systems in base ai records presenti in device_type, systems e devices
WITH NodeSystems AS
(
SELECT DISTINCT devices.NodID, systems.SystemID
FROM device_type 
	INNER JOIN systems ON device_type.SystemID = systems.SystemID 
	INNER JOIN devices ON device_type.DeviceTypeID = devices.Type
)
DELETE FROM node_systems 
FROM NodeSystems Q RIGHT JOIN  node_systems NS ON Q.NodID = NS.NodID AND Q.SystemID = NS.SystemID
WHERE (Q.NodId IS NULL);

WITH NodeSystems AS
(
SELECT DISTINCT devices.NodID, systems.SystemID
FROM device_type 
	INNER JOIN systems ON device_type.SystemID = systems.SystemID 
	INNER JOIN devices ON device_type.DeviceTypeID = devices.Type
)
INSERT INTO node_systems (NodID, SystemID)
SELECT Q.NodID, Q.SystemID
FROM NodeSystems Q LEFT JOIN  node_systems NS ON Q.NodID = NS.NodID AND Q.SystemID = NS.SystemID
WHERE (NS.NodeSystemsId IS NULL);

-- allinea region e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId)
SELECT regions.RegID, 1 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId
FROM regions LEFT JOIN  object_regions ON regions.RegID = object_regions.ObjectId
WHERE (object_regions.ObjectStatusId IS NULL) 

-- allinea zones e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId)
SELECT zones.ZonID, 2 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId
FROM zones LEFT JOIN  object_zones ON zones.ZonID = object_zones.ObjectId 
WHERE (object_zones.ObjectStatusId IS NULL)

UPDATE object_zones
SET ParentObjectStatusId = object_regions.ObjectStatusId
FROM zones INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId 
		   INNER JOIN object_regions ON zones.RegID = object_regions.ObjectId

-- allinea nodes e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId)
SELECT nodes.NodID, 3 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId
FROM nodes LEFT JOIN  object_nodes ON nodes.NodID = object_nodes.ObjectId 
WHERE (object_nodes.ObjectStatusId IS NULL)

UPDATE object_nodes
SET ParentObjectStatusId = object_zones.ObjectStatusId
FROM nodes INNER JOIN object_nodes ON nodes.NodId = object_nodes.ObjectId 
		   INNER JOIN object_zones ON nodes.ZonID = object_zones.ObjectId

-- allinea node systems e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId)
SELECT node_systems.NodeSystemsId, 4 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId
FROM node_systems LEFT JOIN  object_status ON node_systems.NodeSystemsId = object_status.ObjectId AND object_status.ObjectTypeId = 4
WHERE (object_status.ObjectStatusId IS NULL)

UPDATE object_node_systems
SET ParentObjectStatusId = object_nodes.ObjectStatusId
FROM node_systems INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId 
		   INNER JOIN object_nodes ON node_systems.NodId = object_nodes.ObjectId

-- allinea servers e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId)
SELECT servers.SrvID, 5 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId
FROM servers LEFT JOIN  object_status ON servers.SrvID = object_status.ObjectId AND object_status.ObjectTypeId = 5
WHERE (object_status.ObjectStatusId IS NULL)

-- allinea devices e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId)
SELECT devices.DevId, 6 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId
FROM devices LEFT JOIN  object_status ON devices.DevId = object_status.ObjectId AND object_status.ObjectTypeId = 6
WHERE (object_status.ObjectStatusId IS NULL)

UPDATE object_devices
SET ParentObjectStatusId = object_node_systems.ObjectStatusId
FROM devices 
		INNER JOIN device_type ON devices.[Type] = device_type.DeviceTypeId
		INNER JOIN node_systems ON devices.NodId = node_systems.NodId AND device_type.SystemId = node_systems.SystemId
		INNER JOIN object_devices ON devices.DevId = object_devices.ObjectId 
		INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId
﻿CREATE procedure [dbo].[gris_GetChildrenVirtualObject]
(
	@ObjectId as bigint,
	@ObjectTypeId int
) as
begin
	SET NOCOUNT ON;
	select 
		vo.VirtualObjectID, 
		ov.ObjectStatusId as VirtualObjectStatusID, 
		vo.VirtualObjectName, 
		vo.VirtualObjectDescription,
		ov.SevLevel,
		ov.SevLevelDetailId,
		dbo.GetVirtualObjectCustomOrder (vo.VirtualObjectName, 1) as PresentationOrder
	from virtual_objects vo
	inner join object_virtuals ov on vo.VirtualObjectID = ov.ObjectId
	inner join object_status parent on parent.ObjectStatusId = ov.ParentObjectStatusId
	where parent.ObjectId = @ObjectId
	and parent.ObjectTypeId = @ObjectTypeId
	order by dbo.GetVirtualObjectCustomOrder (vo.VirtualObjectName, 1), VirtualObjectName;	
end
go
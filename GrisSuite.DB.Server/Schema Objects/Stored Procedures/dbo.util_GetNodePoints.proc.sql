CREATE PROCEDURE [dbo].[util_GetNodePoints] (@Name as Varchar(64) )
AS
	SELECT name, object_map_points.MapPoints, object_nodes.ObjectStatusId, NodID
	FROM nodes 
		INNER JOIN object_nodes ON nodes.NodID = object_nodes.ObjectId
		LEFT JOIN object_map_points ON object_nodes.ObjectStatusId = object_map_points.ObjectStatusId
	WHERE Name LIKE @Name;
RETURN
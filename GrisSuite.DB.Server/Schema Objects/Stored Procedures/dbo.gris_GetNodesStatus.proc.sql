﻿CREATE PROCEDURE [dbo].[gris_GetNodesStatus]
@ZonId BIGINT
AS
WITH  NodCounts AS 
	(
		SELECT RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, [0] as STLCOK, [3] as STLCOffline, [9] as STLCInMaintenance, [-1] as STLCNotClassified, STLCTotalCount, 0 as Ok, 0 as Warning,0 as Error, 0 as Unknown, 0 as [Offline], 0 as InMaintenance, 0 as NotClassified, 0 as DevicesTotalCount
		FROM 
		(
			SELECT 
				regions.RegID, regions.Name AS RegName, zones.ZonID, zones.Name as ZonName, nodes.NodID, nodes.Name as NodName, nodes.Meters, 
				servers_status.SevLevel, sn.SrvId, COUNT(sn.SrvId) OVER (PARTITION BY nodes.NodID) as STLCTotalCount
			FROM servers_status 
				INNER JOIN (SELECT DISTINCT devices.SrvId, devices.NodId FROM devices ) AS sn ON servers_status.SrvID =sn.SrvID
				INNER JOIN nodes	ON nodes.NodID = sn.NodID 
				INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions	ON zones.RegID = regions.RegID
				INNER JOIN servers  ON servers_status.SrvID = servers.SrvID
			WHERE nodes.ZonId = @ZonId
				AND servers.IsDeleted = 0
		) as A
		PIVOT
		(
		 COUNT(SrvId)
		 for SevLevel
		 in ([0], [3], [9], [-1])
		) as p
		
		UNION ALL
		
		SELECT RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, 0 as STLCOK, 0 as STLCOffline, 0 as STLCInMaintenance, 0 as STLCNotClassified, 0 as STLCTotalCount, [0] as Ok, [1] as Warning,[2] as Error, [255] as Unknown, [3] as [Offline], [9] as InMaintenance, [-1] NotClassified, DevicesTotalCount
		FROM 
		(
			SELECT 
				regions.RegID, regions.Name AS RegName, zones.ZonID, zones.Name as ZonName, nodes.NodID, nodes.Name as NodName, nodes.Meters, 
				object_devices.SevLevel, devices.DevID, COUNT(DevID) OVER (PARTITION BY nodes.NodID) as DevicesTotalCount
			FROM nodes 
				INNER JOIN zones			ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions			ON zones.RegID = regions.RegID 
				INNER JOIN devices			ON nodes.NodID = devices.NodID 
				INNER JOIN object_devices	ON devices.DevID = object_devices.ObjectId
				INNER JOIN servers			ON devices.SrvID = servers.SrvID
			WHERE nodes.ZonId = @ZonId
				AND object_devices.ExcludeFromParentStatus = 0
				AND servers.IsDeleted = 0
		) as A
		PIVOT
		(
		 COUNT(DevID)
		 for SevLevel
		 in ([0], [1], [2], [3], [255], [9], [-1])
		) as p
	)
	SELECT 
		RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, 
		SUM(STLCOK) as STLCOK, SUM(STLCOffline) as STLCOffline, SUM(STLCInMaintenance) as STLCInMaintenance,  SUM(STLCNotClassified) as STLCNotClassified, SUM(STLCTotalCount) as STLCTotalCount, 
		SUM(NodCounts.Ok) as Ok, SUM(NodCounts.Warning) as Warning,SUM(NodCounts.Error) as Error, SUM(NodCounts.Unknown) as Unknown, SUM([Offline]) as [Offline], SUM(NodCounts.InMaintenance) as InMaintenance, SUM(NotClassified) as NotClassified, SUM(DevicesTotalCount) as DevicesTotalCount, 
		object_nodes.SevLevel as Status, object_nodes.SevLevelDescription as [StatusDescription],
		dbo.GetSeverityLevelForObject (RegID, NULL, NULL) AS SevLevelRegion,
		dbo.GetSeverityLevelForObject (NULL, ZonID, NULL) AS SevLevelZone,
		dbo.GetAttributeForObject(8 /* Color */, NULL, RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
		dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor,
		dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, NodID, NULL, NULL, NULL, NULL) AS NodeColor
	FROM NodCounts
		INNER JOIN object_nodes ON NodCounts.NodId = object_nodes.ObjectId
	WHERE object_nodes.SevLevel >= 0 and dbo.IsWithServerNode(object_nodes.ObjectId)= 1
	GROUP BY RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, object_nodes.SevLevel, object_nodes.SevLevelDescription
	ORDER BY Meters, RegName, ZonName, NodName;
RETURN
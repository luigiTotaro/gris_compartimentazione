﻿/****** Object:  StoredProcedure [dbo].[tf_UpdRegions]    Script Date: 11/20/2006 18:19:09 ******/
CREATE PROCEDURE [dbo].[tf_UpdRegions]
(
	@RegID bigint,
	@Name varchar(64),
	@Removed bit
)
AS
	SET NOCOUNT OFF;

	IF ISNULL(@Removed,0) = 0
	BEGIN
		IF EXISTS(SELECT RegID FROM regions WHERE (RegID = @RegID))
			UPDATE [regions] SET [RegID] = @RegID, [Name] = @Name WHERE (([RegID] = @RegID));
		ELSE
			INSERT INTO [regions] ([RegID], [Name]) VALUES (@RegID, @Name)
	END;



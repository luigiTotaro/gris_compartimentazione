﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 04-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributeValues_Upd]
	@TargetValueID INT,
	@Value VARCHAR(MAX),
	@Username VARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	SET XACT_ABORT ON;

	DECLARE @OldValue VARCHAR(MAX);
	SELECT @OldValue = [Value] FROM geta.target_values WHERE TargetValueID = @TargetValueID;

	DECLARE @UserID INT;
	SELECT @UserID = UserID FROM users WHERE Username = @Username;

	IF @OldValue IS NOT NULL AND @UserID IS NOT NULL
	BEGIN
		BEGIN TRAN
			UPDATE geta.target_values SET [Value] = @Value WHERE TargetValueID = @TargetValueID;

			INSERT INTO geta.target_values_history (TargetValueID, [Value], UserID) VALUES (@TargetValueID, @OldValue, @UserID);
		COMMIT TRAN
	END
END



﻿CREATE PROCEDURE [dbo].[tf_InsPopulateObjectStatus] (@LastUpdateGuid UNIQUEIDENTIFIER) AS
SET NOCOUNT ON;

-- aggiorna la node_systems in base ai records presenti in device_type, systems e devices
WITH NodeSystems AS
(
	SELECT DISTINCT devices.NodID, systems.SystemID
	FROM device_type 
	INNER JOIN systems ON device_type.SystemID = systems.SystemID 
	INNER JOIN devices ON device_type.DeviceTypeID = devices.Type
)
DELETE FROM node_systems 
FROM NodeSystems Q RIGHT JOIN  node_systems NS ON Q.NodID = NS.NodID AND Q.SystemID = NS.SystemID
WHERE (Q.NodId IS NULL);

WITH NodeSystems AS
(
	SELECT DISTINCT devices.NodID, systems.SystemID
	FROM device_type 
	INNER JOIN systems ON device_type.SystemID = systems.SystemID 
	INNER JOIN devices ON device_type.DeviceTypeID = devices.Type
)
INSERT INTO node_systems (NodID, SystemID)
SELECT Q.NodID, Q.SystemID
FROM NodeSystems Q
LEFT JOIN  node_systems NS ON Q.NodID = NS.NodID AND Q.SystemID = NS.SystemID
WHERE (NS.NodeSystemsId IS NULL);

-- allinea region e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId, ForcedSevLevel)
SELECT regions.RegID, 1 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId, 0 AS ForcedSevLevel
FROM regions
LEFT JOIN  object_regions ON regions.RegID = object_regions.ObjectId
WHERE (object_regions.ObjectStatusId IS NULL) 

-- allinea zones e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId, ForcedSevLevel)
SELECT zones.ZonID, 2 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId, 0 AS ForcedSevLevel
FROM zones
LEFT JOIN  object_zones ON zones.ZonID = object_zones.ObjectId 
WHERE (object_zones.ObjectStatusId IS NULL)

-- allinea i parent degli oggetti appena creati
UPDATE object_zones
SET ParentObjectStatusId = object_regions.ObjectStatusId
FROM zones
INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId 
INNER JOIN object_regions ON zones.RegID = object_regions.ObjectId
WHERE object_zones.ParentObjectStatusId IS NULL

-- allinea i parent degli oggetti con gerarchia incoerente
UPDATE object_zones
SET ParentObjectStatusId = object_regions.ObjectStatusId
FROM zones
INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId 
INNER JOIN object_regions ON zones.RegID = object_regions.ObjectId
WHERE object_zones.ParentObjectStatusId <> object_regions.ObjectStatusId
AND object_zones.ObjectStatusId NOT IN (
	-- escludiamo le zone che sono figlie di virtual object e quindi con gerarchia custom
	SELECT object_status_children.ObjectStatusId
	FROM object_status
	INNER JOIN object_status AS object_status_children ON object_status.ObjectStatusId = object_status_children.ParentObjectStatusId
	WHERE (object_status.ObjectTypeId = 7 /* virtual_object */) AND (object_status_children.ObjectTypeId = 2 /* zones */)
)

-- allinea nodes e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId, ForcedSevLevel)
SELECT nodes.NodID, 3 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId, 0 AS ForcedSevLevel
FROM nodes
LEFT JOIN  object_nodes ON nodes.NodID = object_nodes.ObjectId 
WHERE (object_nodes.ObjectStatusId IS NULL)

-- allinea i parent degli oggetti appena creati
UPDATE object_nodes
SET ParentObjectStatusId = object_zones.ObjectStatusId
FROM nodes
INNER JOIN object_nodes ON nodes.NodId = object_nodes.ObjectId 
INNER JOIN object_zones ON nodes.ZonID = object_zones.ObjectId
WHERE object_nodes.ParentObjectStatusId IS NULL

-- allinea i parent degli oggetti con gerarchia incoerente
UPDATE object_nodes
SET ParentObjectStatusId = object_zones.ObjectStatusId
FROM nodes
INNER JOIN object_nodes ON nodes.NodId = object_nodes.ObjectId 
INNER JOIN object_zones ON nodes.ZonID = object_zones.ObjectId
WHERE object_nodes.ParentObjectStatusId <> object_zones.ObjectStatusId
AND object_nodes.ObjectStatusId NOT IN (
	-- escludiamo i nodi che sono figli di virtual object e quindi con gerarchia custom
	SELECT object_status_children.ObjectStatusId
	FROM object_status
	INNER JOIN object_status AS object_status_children ON object_status.ObjectStatusId = object_status_children.ParentObjectStatusId
	WHERE (object_status.ObjectTypeId = 7 /* virtual_object */) AND (object_status_children.ObjectTypeId = 3 /* nodes */)
)

-- allinea node systems e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId, ForcedSevLevel)
SELECT node_systems.NodeSystemsId, 4 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId, 0 AS ForcedSevLevel
FROM node_systems LEFT JOIN  object_status ON node_systems.NodeSystemsId = object_status.ObjectId AND object_status.ObjectTypeId = 4
WHERE (object_status.ObjectStatusId IS NULL)

-- allinea i parent degli oggetti appena creati
UPDATE object_node_systems
SET ParentObjectStatusId = object_nodes.ObjectStatusId
FROM node_systems
INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId 
INNER JOIN object_nodes ON node_systems.NodId = object_nodes.ObjectId
WHERE object_node_systems.ParentObjectStatusId IS NULL

-- allinea i parent degli oggetti con gerarchia incoerente
UPDATE object_node_systems
SET ParentObjectStatusId = object_nodes.ObjectStatusId
FROM node_systems
INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId 
INNER JOIN object_nodes ON node_systems.NodId = object_nodes.ObjectId
WHERE object_node_systems.ParentObjectStatusId <> object_nodes.ObjectStatusId
AND object_node_systems.ObjectStatusId NOT IN (
	-- escludiamo i sistemi che sono figli di virtual object e quindi con gerarchia custom
	SELECT object_status_children.ObjectStatusId
	FROM object_status
	INNER JOIN object_status AS object_status_children ON object_status.ObjectStatusId = object_status_children.ParentObjectStatusId
	WHERE (object_status.ObjectTypeId = 7 /* virtual_object */) AND (object_status_children.ObjectTypeId = 4 /* node_systems */)
)

-- allinea servers e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId, InMaintenance)
SELECT servers.SrvID, 5 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId, 1 AS InMaintenance
FROM servers LEFT JOIN  object_status ON servers.SrvID = object_status.ObjectId AND object_status.ObjectTypeId = 5
WHERE (object_status.ObjectStatusId IS NULL)

-- allinea devices e object status
INSERT INTO object_status (ObjectId, ObjectTypeId, SevLevel, SevLevelDetailId)
SELECT devices.DevId, 6 AS ObjectTypeId, 9 AS SevLevel, NULL AS SevLevelDetailId
FROM devices LEFT JOIN  object_status ON devices.DevId = object_status.ObjectId AND object_status.ObjectTypeId = 6
WHERE (object_status.ObjectStatusId IS NULL)

-- allinea i parent degli oggetti appena creati
UPDATE object_devices
SET ParentObjectStatusId = object_node_systems.ObjectStatusId
FROM devices 
INNER JOIN device_type ON devices.[Type] = device_type.DeviceTypeId
INNER JOIN node_systems ON devices.NodId = node_systems.NodId AND device_type.SystemId = node_systems.SystemId
INNER JOIN object_devices ON devices.DevId = object_devices.ObjectId 
INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId
WHERE object_devices.ParentObjectStatusId IS NULL

-- allinea i parent degli oggetti con gerarchia incoerente
UPDATE object_devices
SET ParentObjectStatusId = object_node_systems.ObjectStatusId
FROM devices 
INNER JOIN device_type ON devices.[Type] = device_type.DeviceTypeId
INNER JOIN node_systems ON devices.NodId = node_systems.NodId AND device_type.SystemId = node_systems.SystemId
INNER JOIN object_devices ON devices.DevId = object_devices.ObjectId 
INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId
WHERE object_devices.ParentObjectStatusId <> object_node_systems.ObjectStatusId
AND object_devices.ObjectStatusId NOT IN (
	-- escludiamo le periferiche che sono figlie di virtual object e quindi con gerarchia custom
	SELECT object_status_children.ObjectStatusId
	FROM object_status
	INNER JOIN object_status AS object_status_children ON object_status.ObjectStatusId = object_status_children.ParentObjectStatusId
	WHERE (object_status.ObjectTypeId = 7 /* virtual_object */) AND (object_status_children.ObjectTypeId = 6 /* devices */)
)

-- Periferiche da non considerare nel calcolo dello stato dei padri
-- Queste periferiche non sono estratte nel motore di calcolo, quindi le impostiamo qui
UPDATE object_devices SET object_devices.ExcludeFromParentStatus = 1
FROM object_devices
INNER JOIN devices ON devices.DevID = object_devices.ObjectId
LEFT JOIN device_type ON devices.[Type] = device_type.DeviceTypeID
WHERE (devices.Name = 'NoDevice');

-- Creiamo le righe di object_formulas eventualmente mancanti per nuovi oggetti
INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
SELECT ObjectStatusId, 0, '.\Lib\DeviceDefaultStatus.py', 0
FROM object_devices
WHERE ObjectStatusId NOT IN (SELECT ObjectStatusId FROM object_formulas)

INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
SELECT ObjectStatusId, 0, '.\Lib\ServerDefaultStatus.py', 0
FROM object_servers
WHERE ObjectStatusId NOT IN (SELECT ObjectStatusId FROM object_formulas)

INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
SELECT ObjectStatusId, 0, '.\Lib\NodeSystemDefaultStatus.py', 0
FROM object_node_systems
WHERE ObjectStatusId NOT IN (SELECT ObjectStatusId FROM object_formulas)

INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
SELECT ObjectStatusId, 0, '.\Lib\NodeDefaultStatus.py', 0
FROM object_nodes
WHERE ObjectStatusId NOT IN (SELECT ObjectStatusId FROM object_formulas)

INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
SELECT ObjectStatusId, 0, '.\Lib\ZoneDefaultStatus.py', 0
FROM object_zones
WHERE ObjectStatusId NOT IN (SELECT ObjectStatusId FROM object_formulas)

INSERT INTO object_formulas (ObjectStatusId, FormulaIndex, ScriptPath, FormulaGlobalIndex)
SELECT ObjectStatusId, 0, '.\Lib\RegionDefaultStatus.py', 0
FROM object_regions
WHERE ObjectStatusId NOT IN (SELECT ObjectStatusId FROM object_formulas)
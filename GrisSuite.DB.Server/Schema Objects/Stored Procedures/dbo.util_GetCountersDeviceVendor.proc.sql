CREATE PROCEDURE [dbo].[util_GetCountersDeviceVendor]
@Systems VARCHAR (MAX)
AS
SET @Systems = ISNULL(@Systems,'');


SELECT Sistema, Produttore, Tecnologia, 
	[0] as [In servizio], [1] as [Anomalia lieve], [2] as [Anomalia grave], [9] as [In Configurazione], [255]+[3] as [Sconosciuto], [-1] as [Non classificato],
	[0]+[1]+[2]+[3]+[9]+[255]+[3]+[-1] as [Totale]
FROM 
(
	select y.SystemDescription as Sistema
		, v.VendorName as Produttore
		, ISNULL(h.TechnologyCode,'') AS Tecnologia
		, d.[Type] as Tipo
		, od.SevLevelReal as idStato
	from object_devices od
		inner join severity s on s.SevLevel = od.SevLevelReal
		inner join devices d on od.ObjectId = d.DevID
		left join device_type t on d.type = t.DeviceTypeID
		left join vendors v on t.VendorID = v.VendorID
		left join systems y on t.SystemID = y.SystemID
		left join technology h on t.TechnologyID = h.TechnologyID
	where (@Systems = '' OR  CHARINDEX('|' + CAST(y.SystemID AS VARCHAR(2)) + '|', @Systems, 1) > 0)
) ps
PIVOT
(
	Count (Tipo)
	FOR idStato IN
	( [0], [1], [2], [9], [3], [255], [-1] )
) AS pvt;
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[UserGroups_Ins]
	@UserGroupID int OUTPUT,
	@UserGroupName varchar(256)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @UGID INT
	SELECT @UGID = UserGroupID FROM user_groups WHERE UserGroupName = @UserGroupName;

	IF @UGID IS NULL
	BEGIN
		INSERT INTO user_groups (UserGroupName) VALUES (@UserGroupName);

		SET @UserGroupID = CAST(SCOPE_IDENTITY() AS INT);
	END
END



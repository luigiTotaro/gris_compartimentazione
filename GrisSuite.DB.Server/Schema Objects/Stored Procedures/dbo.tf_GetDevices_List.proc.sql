﻿CREATE PROCEDURE [dbo].[tf_GetDevices_List]
(
	@ServerName varchar(64),
	@ServerHost varchar(64),
	@NodeName varchar(64),	   -- Stazioni
	@ZoneName varchar(64),	   -- Linea
	@RegionName varchar(64),   -- Compartimenti
	@DeviceName varchar(64),
	@DevID bigint
)
AS
SET NOCOUNT ON;

SELECT     servers.Name AS ServerName, servers.Host AS ServerHost, nodes.Name AS NodeName, zones.Name AS ZoneName, regions.Name AS RegionName, 
                      devices.Name, devices.Type, devices.SN, devices.PortID, devices.Addr, devices.ProfileID, devices.Active, devices.Scheduled, devices.DevID, 
                      nodes.NodID, zones.ZonID, regions.RegID, servers.SrvID
FROM         devices INNER JOIN
                      nodes ON devices.NodID = nodes.NodID INNER JOIN
                      servers ON devices.SrvID = servers.SrvID INNER JOIN
                      zones ON nodes.ZonID = zones.ZonID INNER JOIN
                      regions ON zones.RegID = regions.RegID
WHERE (ISNULL(@ServerName,'') = '' OR  servers.Name LIKE @ServerName)
AND (ISNULL(@ServerHost,'') = '' OR servers.Host LIKE @ServerHost) 
AND (ISNULL(@NodeName,'') = '' OR nodes.Name LIKE @NodeName) 
AND (ISNULL(@ZoneName,'')= '' OR zones.Name LIKE @ZoneName) 
AND (ISNULL(@RegionName,'') = '' OR regions.Name LIKE @RegionName) 
AND (ISNULL(@DeviceName,'') = '' OR devices.Name LIKE @DeviceName) 
AND (ISNULL(@DevID,0) =0 OR devices.DevID = @DevID);



﻿CREATE PROCEDURE [dbo].[gris_InsDeviceFilters] (@Username      VARCHAR(256), @DeviceFilterName VARCHAR(110),
                                               @RegIDs        VARCHAR(MAX), @RegNames VARCHAR(MAX),
                                               @ZonIDs        VARCHAR(MAX), @ZonNames VARCHAR(MAX),
                                               @NodIDs        VARCHAR(MAX), @NodNames VARCHAR(MAX),
                                               @SystemIDs     VARCHAR(MAX), @SystemNames VARCHAR(MAX),
                                               @DeviceTypeIDs VARCHAR(MAX), @DeviceTypeNames VARCHAR(MAX),
                                               @SevLevelIDs   VARCHAR(MAX), @DeviceData VARCHAR(110),
                                               @IsShared      BIT) AS
BEGIN
  SET NOCOUNT ON;

  SET @DeviceFilterName = LTRIM(RTRIM(ISNULL(@DeviceFilterName, '')));

  DECLARE @DeviceFilterId BIGINT;
  DECLARE @UserID INT;
  DECLARE @ResourceID INT;

  IF (LEN(@DeviceFilterName) > 0) BEGIN
      SELECT @DeviceFilterId = DeviceFilterId
      FROM device_filters
      WHERE (DeviceFilterName LIKE @DeviceFilterName);

      SELECT @UserID = UserID
      FROM users
      WHERE (Username LIKE @Username);

      IF (@UserID IS NULL)
        BEGIN
          -- Se l'utente non esiste in base dati, lo creiamo al volo, usando la username, unico dato disponibile su Gris
          -- l'eventuale modifica dei dati anagrafici può essere fatta su Geta
          INSERT INTO users (Username, FirstName, LastName, OrganizationID, UserGroupID, MobilePhone, Email)
          VALUES (@Username, NULL, NULL, NULL, NULL, NULL, NULL);

          SET @UserID = CAST(SCOPE_IDENTITY() AS INT);
        END

      IF (@DeviceFilterId IS NULL)
        BEGIN
          INSERT INTO device_filters (DeviceFilterName, UserID, RegIDs, RegNames, ZonIDs, ZonNames, NodIDs, NodNames, SystemIDs, SystemNames, DeviceTypeIDs, DeviceTypeNames, SevLevelIDs, DeviceData)
          VALUES (@DeviceFilterName, @UserID, @RegIDs, @RegNames, @ZonIDs, @ZonNames, @NodIDs, @NodNames, @SystemIDs,
                                     @SystemNames, @DeviceTypeIDs, @DeviceTypeNames, @SevLevelIDs, @DeviceData);

          SET @DeviceFilterId = CAST(SCOPE_IDENTITY() AS BIGINT);
          SET @ResourceID = (SELECT ResourceID FROM device_filters WHERE DeviceFilterId = @DeviceFilterId)
        END
      ELSE
        BEGIN
          UPDATE device_filters
          SET
            DeviceFilterName = @DeviceFilterName,
            UserID           = @UserID,
            RegIDs           = @RegIDs,
            RegNames         = @RegNames,
            ZonIDs           = @ZonIDs,
            ZonNames         = @ZonNames,
            NodIDs           = @NodIDs,
            NodNames         = @NodNames,
            SystemIDs        = @SystemIDs,
            SystemNames      = @SystemNames,
            DeviceTypeIDs    = @DeviceTypeIDs,
            DeviceTypeNames  = @DeviceTypeNames,
            SevLevelIDs      = @SevLevelIDs,
            DeviceData       = @DeviceData
          WHERE (DeviceFilterId = @DeviceFilterId);
        END
      -- update resource description
      UPDATE r
      SET r.ResourceDescription = f.DeviceFilterName
      FROM resources r INNER JOIN device_filters f ON r.ResourceID = ISNULL(f.ResourceID, 0)
      WHERE f.DeviceFilterId = @DeviceFilterId

      SET @ResourceID = (SELECT ResourceID FROM device_filters WHERE DeviceFilterId = @DeviceFilterId)
      SET @IsShared = ISNULL(@IsShared, 0)
      IF ((@ResourceID IS NULL AND @IsShared = 1) OR (@ResourceID IS NOT NULL AND @IsShared = 0) )BEGIN
        EXEC gris_UpdDeviceFilters_share @DeviceFilterName, @IsShared
        SET @ResourceID = (SELECT ResourceID FROM device_filters WHERE DeviceFilterId = @DeviceFilterId)
      END
  END


  SELECT
    ISNULL(@DeviceFilterId, CAST(0 AS BIGINT)) AS DeviceFilterId,
    @DeviceFilterName                          AS DeviceFilterName,
    ISNULL(@UserID, 0)                         AS UserID,
    ISNULL(@ResourceID, 0)                     AS ResourceID
END
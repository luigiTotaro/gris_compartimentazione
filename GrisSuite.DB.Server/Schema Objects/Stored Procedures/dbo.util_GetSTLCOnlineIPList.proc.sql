﻿CREATE PROCEDURE [dbo].[util_GetSTLCOnlineIPList]
AS

DECLARE @MinuteTimeOut INT;
SET @MinuteTimeOut = 15;

SELECT IP FROM servers
WHERE NOT(DATEDIFF(n, servers.LastUpdate, GETDATE()) > @MinuteTimeOut OR servers.LastUpdate IS NULL)



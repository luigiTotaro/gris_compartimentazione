﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
create PROCEDURE [geta].[Users_Del] 
	@UserID int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE users WHERE (UserID = @UserID);
END



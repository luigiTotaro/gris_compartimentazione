﻿CREATE PROCEDURE [dbo].[gris_UpdServerValidationRequest]
	@SrvID int,
	@ClientDateValidationRequested datetime,
	@ServerDateValidationRequested datetime
AS
	UPDATE servers
	SET ClientDateValidationRequested = @ClientDateValidationRequested, ServerDateValidationRequested = @ServerDateValidationRequested
	WHERE (SrvID = @SrvID);



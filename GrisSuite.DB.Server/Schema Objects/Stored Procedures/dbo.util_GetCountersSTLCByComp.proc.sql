CREATE PROCEDURE [dbo].[util_GetCountersSTLCByComp]
@Comp VARCHAR (64)
AS
DECLARE @MinuteTimeOut INT
	SET @MinuteTimeOut = dbo.GetServerMinuteTimeOut()


SELECT Compartimento, 
	[online-0] as [Manutenzione Online], 
	[offline-0] as [Manutenzione Offline],
	[online-1] as [Online], 
	[offline-1] as [Offline],
	[online-1]+[offline-1]+[online-0]+[offline-0] as [totale]
FROM 
(
select r1.Name as Compartimento, r.SrvID,
CASE 
	WHEN (DATEDIFF(n, ISNULL(r.LastUpdate,'2000-01-01'), GETDATE()) > @MinuteTimeOut AND @MinuteTimeOut <> 0) THEN 'offline' 
	ELSE 'online'
END + '-' + CAST(~InMaintenance as CHAR(1)) as Stato
from servers_status r
    left join nodes n1 on r.NodID = n1.NodID
    left join zones z1 on n1.ZonID = z1.ZonID
    left join regions r1 on z1.RegID = r1.RegID
    
where r1.Name Like '%'+ @Comp +'%'
) ps
PIVOT
(
Count(SrvID)
FOR Stato IN
( [online-0], [offline-0],[online-1],[offline-1] )
) AS pvt
return
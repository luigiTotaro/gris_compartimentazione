﻿CREATE PROCEDURE [dbo].[tf_UpdServers] (
@SrvID INT,
@Name VARCHAR (64),
@Host VARCHAR (64),
@FullHostName VARCHAR (256),
@IP VARCHAR (16),
@LastUpdate DATETIME,
@LastMessageType VARCHAR (64),
@SupervisorSystemXML XML,
@ClientSupervisorSystemXMLValidated XML,
@ClientValidationSign VARBINARY (128),
@ClientDateValidationRequested DATETIME,
@ClientDateValidationObtained DATETIME,
@ClientKey VARBINARY (148),
@NodID BIGINT = NULL,
@ServerVersion VARCHAR (32) = NULL,
@MAC VARCHAR (16) = NULL
) AS
SET NOCOUNT OFF;

DECLARE @SupervisorSystemXML_Previous XML;

SET @MAC = RTRIM(ISNULL(@MAC,''))

IF EXISTS (SELECT SrvID FROM servers WHERE ([SrvID] = @SrvID))
BEGIN -- se c'e' conflitto di SrvID
	-- controllo anche il MAC address... non esiste nessun campo realmente univoco negli STLC,
	-- viene considerata chiave univoca la chiave composta SrvID e MAC anche se ci possono essere casi previsti in cui non e' sufficiente ad identificare un STLC
	-- es. devo cambiare il SrvID ad un STLC nuovo perche' va in conflitto con quello di uno preesistente. Lo modifico e ne scelgo uno uguale a quello di un altro STLC preesistente, che ha pere' anche lo stesso MAC. => I dati dell' STLC gia' censito in centrale vengono sovrascritti.
	IF EXISTS (SELECT SrvID FROM servers WHERE ([SrvID] = @SrvID AND ([MAC] = @MAC OR ([MAC] IS NULL AND IsDeleted = 1))))
	BEGIN -- e' considerato lo stesso STLC => vado in update
		UPDATE [servers] 
			SET [Name] = @Name, [HOST] = @Host, [FullHostName] = @FullHostName, 
			[IP] = @IP, [LastUpdate] = @LastUpdate, [LastMessageType] = @LastMessageType, [SupervisorSystemXML] = @SupervisorSystemXML,
			[ClientSupervisorSystemXMLValidated] = @ClientSupervisorSystemXMLValidated, [ClientValidationSign] = @ClientValidationSign, 
			[ClientDateValidationRequested] = @ClientDateValidationRequested, [ClientDateValidationObtained] = @ClientDateValidationObtained,
			[ClientKey] = @ClientKey, [NodID] = @NodID, [ServerVersion] = @ServerVersion, [MAC] = @MAC,
			[IsDeleted] = 0 -- Se stiamo centralizzando dati di un server, questo non può essere cancellato
		WHERE (([SrvID] = @SrvID));

		SELECT @SupervisorSystemXML_Previous = SupervisorSystemXML FROM servers WHERE (SrvID = @SrvID)		
		
		IF (CONVERT(NVARCHAR(MAX), ISNULL(@SupervisorSystemXML_Previous, '')) <> CONVERT(NVARCHAR(MAX), ISNULL(@SupervisorSystemXML, '')))
		BEGIN				
			INSERT INTO [servers_backup] ([SrvID], [Name], [HOST], [FullHostName], [IP], [SupervisorSystemXML], [NodID], [ServerVersion], [MAC], [LastBackup])
			VALUES (@SrvID, @Name, @Host, @FullHostName, @IP, @SupervisorSystemXML_Previous, @NodID, @ServerVersion, @MAC, GETDATE());
		END
	END
	ELSE
	BEGIN -- Anomalia: 2 STLC con uguale SrvID e MAC Address diverso
		RAISERROR ('Cannot update, not the same server', 16 /*Severity*/, 1 /*State*/);
	END
END
ELSE
BEGIN
	INSERT INTO [servers] ([SrvID], [Name], [HOST], [FullHostName], [IP], [LastUpdate], [LastMessageType], [SupervisorSystemXML], [ClientSupervisorSystemXMLValidated], [ClientValidationSign], [ClientDateValidationRequested], [ClientDateValidationObtained], [ClientKey], [NodID], [ServerVersion], [MAC]) 
	VALUES (@SrvID, @Name, @Host, @FullHostName, @IP, @LastUpdate, @LastMessageType, @SupervisorSystemXML, @ClientSupervisorSystemXMLValidated, @ClientValidationSign, @ClientDateValidationRequested, @ClientDateValidationObtained, @ClientKey, @NodID, @ServerVersion, @MAC);
END



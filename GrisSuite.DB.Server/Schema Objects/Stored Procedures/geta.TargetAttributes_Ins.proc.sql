﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 14-05-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributes_Ins] 
	@TargetAttributeID int output,
	@TargetAttributeName varchar(256), 
	@Namespace varchar(256), 
	@IsObsolete bit, 
	@ControlTypeUI varchar(64), 
	@TargetTypeID int, 
	@MainAttribute bit, 
	@EnumValues varchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	SET XACT_ABORT ON;

	DECLARE @Order TINYINT;
	
	IF @ControlTypeUI != 'ComboBox' BEGIN SET @EnumValues = NULL END

	BEGIN TRAN
	-- se il namespace non esiste ancora creo l'attributo principale
	IF NOT EXISTS (SELECT * FROM geta.target_attributes WHERE ([Namespace] = @Namespace))
	BEGIN
		INSERT INTO geta.target_attributes (TargetAttributeName, Namespace, IsObsolete, ControlTypeUI, TargetTypeID, MainAttribute, EnumValues, [Order])
		VALUES ('Default Attribute', @Namespace, 0, 'CheckBox', @TargetTypeID, 1, NULL, 0);
	END

	SET @Order = (
					SELECT ISNULL(MAX([Order]), 0)
					FROM geta.target_attributes
					WHERE ([Namespace] = @Namespace)
				) + 1;

	INSERT INTO geta.target_attributes
		(TargetAttributeName, [Namespace], IsObsolete, ControlTypeUI, TargetTypeID, MainAttribute, EnumValues, [Order])
	VALUES     
		(@TargetAttributeName, @Namespace, @IsObsolete, @ControlTypeUI, @TargetTypeID, @MainAttribute, @EnumValues, @Order);

	COMMIT TRAN
		
	SET @TargetAttributeID = CAST(SCOPE_IDENTITY() AS INT);

END



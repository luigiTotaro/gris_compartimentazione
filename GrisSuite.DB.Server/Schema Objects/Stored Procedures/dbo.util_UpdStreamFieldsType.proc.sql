﻿

CREATE PROCEDURE [dbo].[util_UpdStreamFieldsType]	AS
BEGIN
	-- aggiorna la tabella stream_fields_type aggiungendo i tipi di cmapo che sono censiti nelle tabella stream_fields
	WITH S AS
	(
		SELECT devices.Type as DevType, streams.StrID, stream_fields.FieldID, MAX(streams.Name) as StreamName,   MAX(stream_fields.Name) AS FieldName
		FROM         devices INNER JOIN
							  streams ON devices.DevID = streams.DevID INNER JOIN
							  stream_fields ON streams.StrID = stream_fields.StrID AND streams.DevID = stream_fields.DevID
		GROUP BY devices.Type, streams.StrID, stream_fields.FieldID
	)
	INSERT INTO stream_fields_type (DevType, StrID, FieldID, StreamName, FieldName)
	SELECT S.DevType, S.StrID, S.FieldID, S.StreamName, S.FieldName 
	FROM stream_fields_type T RIGHT JOIN S ON T.DevType = S.DevType AND T.StrID = S.StrID AND T.FieldID = S.FieldID
	WHERE T.DevType IS NULL;

	-- imposta il flag per rendere visibile il tipo di campo sui report dello storico per i tipi di campo che sono
	-- censiti nella cache
	UPDATE stream_fields_type
	SET IsVisibleOnHistoryReportFilter = 1
	FROM stream_fields_type T INNER JOIN (SELECT DISTINCT  StrID, FieldID, Name, DevType	FROM cache_stream_fields) s
		ON T.DevType = S.DevType AND T.StrID = S.StrID AND T.FieldID = S.FieldID 
END
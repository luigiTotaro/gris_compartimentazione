﻿CREATE PROCEDURE [dbo].[gris_GetServer]
	@SrvID int
AS
	SELECT 
		SrvID, [Name], Host, FullHostName, IP, LastUpdate, LastMessageType, 
		SupervisorSystemXML, ClientSupervisorSystemXMLValidated, ClientValidationSign, 
		ClientDateValidationRequested, ClientDateValidationObtained, ServerSupervisorSystemXMLValidated, 
		ServerValidationSign, ServerDateValidationRequested, ServerDateValidationObtained, ClientKey,
		(SELECT TOP 1 ServerKey FROM site) as ServerKey
	FROM servers
	WHERE SrvID = @SrvID
RETURN 0;



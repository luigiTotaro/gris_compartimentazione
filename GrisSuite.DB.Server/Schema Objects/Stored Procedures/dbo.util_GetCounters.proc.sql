﻿CREATE PROCEDURE [dbo].[util_GetCounters] AS
SET NOCOUNT ON;

declare @counter bigint
declare @t table
(id int identity(1,1) primary key,
CounterName varchar(50), 
Counter bigint not null)

insert @t (CounterName, Counter) values ('Data rilevazione: '+ CONVERT(VARCHAR(MAX),GETDATE(),120), 0)

Set @counter = (select count(distinct servers_status.SrvID) from servers_status inner join devices on servers_status.SrvID = devices.srvid)
insert @t (CounterName, Counter) values ('STLC1000 con almeno una periferica', @Counter)

Set @counter = (select count(distinct object_servers_inservice.ObjectId) from dbo.object_servers_inservice inner join devices on object_servers_inservice.ObjectId = devices.srvid)
insert @t (CounterName, Counter) values ('STLC1000 attivi', @Counter)

Set @counter =(select count(*) from dbo.object_servers_inservice INNER JOIN servers ON servers.SrvID = object_servers_inservice.ObjectId where SevLevel = 3 and (servers.IsDeleted <> 1))
insert @t (CounterName, Counter) values ('STLC1000 offline', @Counter)

Set @counter =(select count(*) from dbo.object_servers_inservice where SevLevel = 9)
insert @t (CounterName, Counter) values ('STLC1000 in manutenzione', @Counter)


insert @t (CounterName, Counter) 
select 'STLC1000 con versione: ' + ISNULL(ParameterValue,'Sconosciuta') as Versione, count(*) as n 
from object_servers_inservice
left join stlc_parameters on object_servers_inservice.objectid = stlc_parameters.srvid and parametername = 'STLCVersion'
group by ISNULL(ParameterValue,'Sconosciuta')


insert @t (CounterName, Counter) 
select 'STLC1000 con versione DB: ' + ISNULL(ParameterValue,'Sconosciuta') as Versione, count(*) as n 
from object_servers_inservice
left join stlc_parameters on object_servers_inservice.objectid = stlc_parameters.srvid and parametername = 'DBVersion'
group by ISNULL(ParameterValue,'Sconosciuta')


Set @counter = (SELECT count(*) FROM devices
		INNER JOIN device_type ON device_type.DeviceTypeID = devices.Type
		INNER JOIN systems ON systems.SystemID = device_type.SystemID
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		INNER JOIN servers ON servers.SrvID = devices.SrvId
		INNER JOIN object_servers_inservice ss ON servers.SrvID = ss.ObjectId
		INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
		LEFT JOIN rack ON devices.RackID = rack.RackID 
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID)
insert @t (CounterName, Counter) values ('Numero di periferiche', @Counter)

insert @t (CounterName, Counter)
select 'Periferiche di tipo:' + (CASE [Type]
								   WHEN 'TT10210' THEN 'Pannello Zone V2' 
								   WHEN 'TT10210V3' THEN 'Pannello Zone V3'
								   WHEN 'TT10220' THEN 'Pannello Zone DS PZi'
								   ELSE ISNULL([Type], 'Non definito') END) AS DeviceType,	Count(*) 
		from devices group by [type]
		order by [Type]


insert @t (CounterName, Counter)
select 'Periferiche in stato ' + CAST(object_devices.SevLevel AS VARCHAR(4)) + ' - ' + ISNULL(object_devices.SevLevelDescription,'Senza Stato'), count(*) 
from object_devices
group by object_devices.SevLevel, object_devices.SevLevelDescription 
order by object_devices.SevLevel

select CounterName, Counter from @t
order by Id

return 0
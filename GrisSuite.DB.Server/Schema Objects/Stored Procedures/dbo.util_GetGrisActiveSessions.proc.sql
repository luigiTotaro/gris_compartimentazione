﻿CREATE PROCEDURE util_GetGrisActiveSessions 
	@FromMinutes smallint = 10
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		sessions.SessionKey, sessions.IPAddress, sessions.Agent,
		sessions.[User], sessions.Groups, users.Username, users.FirstName, users.LastName, 
		sessions.Created as SessionCreated, MAX(requests.Created) as LastRequest
	FROM requests INNER JOIN sessions ON requests.SessionKey = sessions.SessionKey 
	INNER JOIN users ON sessions.[User] = users.Username
	WHERE requests.Created >= DATEADD(minute, -1 * @FromMinutes, GETDATE())
	GROUP BY 
		sessions.SessionKey, sessions.IPAddress, sessions.Agent,
		sessions.[User], sessions.Groups, users.Username, users.FirstName, users.LastName, 
		sessions.Created
	ORDER BY LastRequest DESC;

END
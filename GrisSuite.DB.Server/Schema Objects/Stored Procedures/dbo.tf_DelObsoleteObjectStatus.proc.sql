﻿CREATE PROCEDURE [dbo].[tf_DelObsoleteObjectStatus]
AS
SET NOCOUNT ON;

DELETE FROM object_formulas_custom_colors
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM servers RIGHT JOIN object_status ON servers.SrvId = object_status.ObjectId WHERE (servers.SrvId IS NULL) AND (object_status.ObjectTypeId = 5)))
DELETE FROM object_attributes
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM servers RIGHT JOIN object_status ON servers.SrvId = object_status.ObjectId WHERE (servers.SrvId IS NULL) AND (object_status.ObjectTypeId = 5)))
DELETE FROM object_formulas
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM servers RIGHT JOIN object_status ON servers.SrvId = object_status.ObjectId WHERE (servers.SrvId IS NULL) AND (object_status.ObjectTypeId = 5)))
DELETE FROM object_classification_values
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM servers RIGHT JOIN object_status ON servers.SrvId = object_status.ObjectId WHERE (servers.SrvId IS NULL) AND (object_status.ObjectTypeId = 5)))
DELETE FROM object_map_points
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM servers RIGHT JOIN object_status ON servers.SrvId = object_status.ObjectId WHERE (servers.SrvId IS NULL) AND (object_status.ObjectTypeId = 5)))
DELETE FROM object_status 
FROM servers RIGHT JOIN object_status ON servers.SrvId = object_status.ObjectId 
WHERE (servers.SrvId IS NULL) AND (object_status.ObjectTypeId = 5)

DELETE FROM object_formulas_custom_colors
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM devices RIGHT JOIN object_status ON devices.DevId = object_status.ObjectId WHERE (devices.DevId IS NULL) AND (object_status.ObjectTypeId = 6)))
DELETE FROM object_attributes
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM devices RIGHT JOIN object_status ON devices.DevId = object_status.ObjectId WHERE (devices.DevId IS NULL) AND (object_status.ObjectTypeId = 6)))
DELETE FROM object_formulas
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM devices RIGHT JOIN object_status ON devices.DevId = object_status.ObjectId WHERE (devices.DevId IS NULL) AND (object_status.ObjectTypeId = 6)))
DELETE FROM object_classification_values
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM devices RIGHT JOIN object_status ON devices.DevId = object_status.ObjectId WHERE (devices.DevId IS NULL) AND (object_status.ObjectTypeId = 6)))
DELETE FROM object_map_points
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM devices RIGHT JOIN object_status ON devices.DevId = object_status.ObjectId WHERE (devices.DevId IS NULL) AND (object_status.ObjectTypeId = 6)))
DELETE FROM object_status 
FROM devices RIGHT JOIN object_status ON devices.DevId = object_status.ObjectId 
WHERE (devices.DevId IS NULL) AND (object_status.ObjectTypeId = 6)

DELETE FROM object_formulas_custom_colors
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM node_systems RIGHT JOIN object_status ON node_systems.NodeSystemsId = object_status.ObjectId WHERE (node_systems.NodeSystemsId IS NULL) AND (object_status.ObjectTypeId = 4)))
DELETE FROM object_attributes
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM node_systems RIGHT JOIN object_status ON node_systems.NodeSystemsId = object_status.ObjectId WHERE (node_systems.NodeSystemsId IS NULL) AND (object_status.ObjectTypeId = 4)))
DELETE FROM object_formulas
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM node_systems RIGHT JOIN object_status ON node_systems.NodeSystemsId = object_status.ObjectId WHERE (node_systems.NodeSystemsId IS NULL) AND (object_status.ObjectTypeId = 4)))
DELETE FROM object_classification_values
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM node_systems RIGHT JOIN object_status ON node_systems.NodeSystemsId = object_status.ObjectId WHERE (node_systems.NodeSystemsId IS NULL) AND (object_status.ObjectTypeId = 4)))
DELETE FROM object_map_points
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM node_systems RIGHT JOIN object_status ON node_systems.NodeSystemsId = object_status.ObjectId WHERE (node_systems.NodeSystemsId IS NULL) AND (object_status.ObjectTypeId = 4)))
DELETE FROM object_status 
FROM node_systems RIGHT JOIN object_status ON node_systems.NodeSystemsId = object_status.ObjectId 
WHERE (node_systems.NodeSystemsId IS NULL) AND (object_status.ObjectTypeId = 4)

DELETE FROM object_formulas_custom_colors
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM nodes RIGHT JOIN object_status ON nodes.NodId = object_status.ObjectId WHERE (nodes.NodId IS NULL) AND (object_status.ObjectTypeId = 3)))
DELETE FROM object_attributes
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM nodes RIGHT JOIN object_status ON nodes.NodId = object_status.ObjectId WHERE (nodes.NodId IS NULL) AND (object_status.ObjectTypeId = 3)))
DELETE FROM object_formulas
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM nodes RIGHT JOIN object_status ON nodes.NodId = object_status.ObjectId WHERE (nodes.NodId IS NULL) AND (object_status.ObjectTypeId = 3)))
DELETE FROM object_classification_values
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM nodes RIGHT JOIN object_status ON nodes.NodId = object_status.ObjectId WHERE (nodes.NodId IS NULL) AND (object_status.ObjectTypeId = 3)))
DELETE FROM object_map_points
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM nodes RIGHT JOIN object_status ON nodes.NodId = object_status.ObjectId WHERE (nodes.NodId IS NULL) AND (object_status.ObjectTypeId = 3)))
DELETE FROM object_status 
FROM nodes RIGHT JOIN object_status ON nodes.NodId = object_status.ObjectId
WHERE (nodes.NodId IS NULL) AND (object_status.ObjectTypeId = 3)

DELETE FROM object_formulas_custom_colors
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM zones RIGHT JOIN object_status ON zones.ZonID = object_status.ObjectId WHERE (zones.ZonID IS NULL) AND (object_status.ObjectTypeId = 2)))
DELETE FROM object_attributes
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM zones RIGHT JOIN object_status ON zones.ZonID = object_status.ObjectId WHERE (zones.ZonID IS NULL) AND (object_status.ObjectTypeId = 2)))
DELETE FROM object_formulas
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM zones RIGHT JOIN object_status ON zones.ZonID = object_status.ObjectId WHERE (zones.ZonID IS NULL) AND (object_status.ObjectTypeId = 2)))
DELETE FROM object_classification_values
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM zones RIGHT JOIN object_status ON zones.ZonID = object_status.ObjectId WHERE (zones.ZonID IS NULL) AND (object_status.ObjectTypeId = 2)))
DELETE FROM object_map_points
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM zones RIGHT JOIN object_status ON zones.ZonID = object_status.ObjectId WHERE (zones.ZonID IS NULL) AND (object_status.ObjectTypeId = 2)))
DELETE FROM object_status
FROM zones RIGHT JOIN object_status ON zones.ZonID = object_status.ObjectId 
WHERE (zones.ZonID IS NULL) AND (object_status.ObjectTypeId = 2)

DELETE FROM object_formulas_custom_colors
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM regions RIGHT JOIN object_status ON regions.RegID = object_status.ObjectId WHERE (regions.RegId IS NULL) AND (object_status.ObjectTypeId = 1)))
DELETE FROM object_attributes
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM regions RIGHT JOIN object_status ON regions.RegID = object_status.ObjectId WHERE (regions.RegId IS NULL) AND (object_status.ObjectTypeId = 1)))
DELETE FROM object_formulas
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM regions RIGHT JOIN object_status ON regions.RegID = object_status.ObjectId WHERE (regions.RegId IS NULL) AND (object_status.ObjectTypeId = 1)))
DELETE FROM object_classification_values
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM regions RIGHT JOIN object_status ON regions.RegID = object_status.ObjectId WHERE (regions.RegId IS NULL) AND (object_status.ObjectTypeId = 1)))
DELETE FROM object_map_points
WHERE (ObjectStatusId IN (SELECT object_status.ObjectStatusId FROM regions RIGHT JOIN object_status ON regions.RegID = object_status.ObjectId WHERE (regions.RegId IS NULL) AND (object_status.ObjectTypeId = 1)))
DELETE FROM object_status
FROM regions RIGHT JOIN object_status ON regions.RegID = object_status.ObjectId 
WHERE (regions.RegId IS NULL) AND (object_status.ObjectTypeId = 1)
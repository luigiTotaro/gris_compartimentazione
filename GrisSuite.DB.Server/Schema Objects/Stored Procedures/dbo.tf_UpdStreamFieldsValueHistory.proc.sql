﻿

CREATE PROCEDURE dbo.tf_UpdStreamFieldsValueHistory(
	 @HistoryDays int = 365, @NewCacheDays int = 1)
AS
BEGIN
	-- cancella i messaggi pià vecchi di @HistoryDays
	DECLARE @HistoryMinutes int
	SET  @HistoryMinutes = @HistoryDays * 1440
	DELETE FROM history_stream_fields_value WHERE DATEDIFF(minute, Created, GETDATE()) > @HistoryMinutes

	-- calcola l'intervallo di date da elaborare
	DECLARE @StartDate datetime;
	DECLARE @EndDate datetime;

	SET @StartDate = (SELECT MAX(Created)FROM dbo.history_stream_fields_value);
	IF(@StartDate IS NULL)
		BEGIN
			SET @StartDate = (SELECT MIN(Created)
							  FROM cache_stream_fields DS INNER JOIN cache_log_messages LM ON DS.LogID = LM.LogID);
		END;
	SET @EndDate = DATEADD(minute, @NewCacheDays * 1440, @StartDate);
	
	IF (NOT EXISTS(SELECT * FROM dbo.stream_fields_valuechange WHERE Created > @StartDate AND Created <= @EndDate))
		BEGIN
			SET @StartDate = (SELECT MIN(Created)
							  FROM cache_device_status DS INNER JOIN cache_log_messages LM ON DS.LogID = LM.LogID AND Created > @StartDate);
		END;
	SET @EndDate = DATEADD(minute, @NewCacheDays * 1440, @StartDate);

	PRINT 'Elabora da ' + CONVERT(varchar, @StartDate, 120) + ' a ' + CONVERT(varchar, @EndDate, 120);

	IF OBJECT_ID('tempdb..#t')IS NOT NULL
		BEGIN
			DROP TABLE #t;
		END;

	SELECT ROW_NUMBER()OVER(PARTITION BY DevID
									   , DevType
									   , StrID
									   , FieldID
									   , ArrayID ORDER BY Created DESC)AS RowNumber
		 , * INTO #t
	FROM dbo.stream_fields_valuechange
	WHERE Created > @StartDate AND Created <= @EndDate;

	WITH D
		AS (SELECT Q2.DevID
				 , Q2.StrID
				 , Q2.FieldID
				 , Q2.ArrayID
				 , Q2.SevLevel
				 , Q2.Created
				 , Q2.DevType
				 , Q2.Value
				 , Q2.Name
				 , DATEDIFF(second, Q2.Created, Q1.Created)AS Duration
			FROM #t Q1 INNER JOIN #t Q2 ON Q1.DevID = Q2.DevID
									   AND Q1.DevType = Q2.DevType
									   AND Q1.StrID = Q2.StrID
									   AND Q1.FieldID = Q2.FieldID
									   AND Q1.ArrayID = Q2.ArrayID
									   AND Q1.RowNumber = Q2.RowNumber - 1
			UNION
			SELECT Q2.DevID
				 , Q2.StrID
				 , Q2.FieldID
				 , Q2.ArrayID
				 , Q2.SevLevel
				 , Q2.Created
				 , Q2.DevType
				 , Q2.Value
				 , Q2.Name
				 , NULL AS Duration
			FROM #t Q1 RIGHT JOIN #t Q2 ON Q1.DevID = Q2.DevID
									   AND Q1.DevType = Q2.DevType
									   AND Q1.StrID = Q2.StrID
									   AND Q1.FieldID = Q2.FieldID
									   AND Q1.ArrayID = Q2.ArrayID
									   AND Q1.RowNumber = Q2.RowNumber - 1
			WHERE Q1.DevID IS NULL)

		INSERT INTO history_stream_fields_value(DevID
											  , StrID
											  , FieldID
											  , ArrayID
											  , Value
											  , SevLevel
											  , Name
											  , DevType
											  , Created
											  , Duration)
		SELECT DevID
			 , StrID
			 , FieldID
			 , ArrayID
			 , Value
			 , SevLevel
			 , Name
			 , DevType
			 , Created
			 , Duration
		FROM D;

	-- aggiunge nodid e/o srvid per le device che non ce l'hanno già
	-- faccio la join anche per tipo per essere sicuri che nel frattempo non sia stato utilizzato lo stesso DevID per un'altra device di altro tipo.
	UPDATE history_stream_fields_value
	SET NodId = devices.NodID
	  , SrvId = devices.SrvID
	FROM history_stream_fields_value INNER JOIN devices ON history_stream_fields_value.DevID = devices.DevID
													   AND history_stream_fields_value.DevType = devices.Type
	WHERE Created >= @StartDate;

	-- calcola le durate degli stati lasciati in sospeso dall'elaborazione precedente.
	DECLARE @StartDateUpdate datetime;
	SET @StartDateUpdate = DATEADD(minute, @HistoryDays * -1440, @StartDate);
	WITH H
		AS (SELECT ROW_NUMBER()OVER(PARTITION BY DevID
											   , DevType
											   , StrID
											   , FieldID
											   , ArrayID ORDER BY Created DESC)AS RowNumber
				 , *
			FROM history_stream_fields_value
			WHERE Created >= @StartDateUpdate)
		UPDATE history_stream_fields_value
		SET Duration = DATEDIFF(second, Q2.Created, Q1.Created)
		FROM H Q1 INNER JOIN H Q2 ON Q1.DevID = Q2.DevID AND Q1.RowNumber = Q2.RowNumber - 1
				  INNER JOIN history_stream_fields_value ON history_stream_fields_value.ID = Q2.ID
		WHERE history_stream_fields_value.Duration IS NULL;

	-- recupera i valori di SevLevel e Offline di delle device nel momento del cambio di stato
    -- implementerò se necessario.

END
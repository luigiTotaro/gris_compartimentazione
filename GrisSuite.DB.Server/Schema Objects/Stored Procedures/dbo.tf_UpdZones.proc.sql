﻿/****** Object:  StoredProcedure [dbo].[tf_UpdZones]    Script Date: 11/20/2006 18:19:11 ******/

CREATE PROCEDURE [dbo].[tf_UpdZones]
(
	@ZonID bigint,
	@RegID bigint,
	@Name varchar(64),
	@Removed bit
)
AS
	SET NOCOUNT OFF;
	
	IF ISNULL(@Removed,0) = 0
	BEGIN
		IF EXISTS(SELECT ZonID FROM zones WHERE (ZonID = @ZonID))
			UPDATE [zones] SET [ZonID] = @ZonID, [RegID] = @RegID, [Name] = @Name WHERE (([ZonID] = @ZonID));
		ELSE
			INSERT INTO [zones] ([ZonID], [RegID], [Name]) VALUES (@ZonID, @RegID, @Name)
	END;



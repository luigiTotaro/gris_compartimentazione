﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	Diagnostica DT13000 non funzionante (se DT04000=amplificatore e TT10210=pannellozone sono OK e DT13000=alimentatore in offline)
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsRuleDT13000]
	@RuleID int = 0 
AS
BEGIN
	WITH 
	AlimOffLine AS (
		SELECT devices.DevId, devices.NodId
		FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID
		WHERE (device_status.Offline = 1)and [Type] = 'DT13000'
	),
	PZoneOK AS (
		SELECT devices.DevId, devices.NodId
		FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID
		WHERE ([Type] = 'TT10210' OR [Type] = 'TT10210V3' OR [Type] = 'TT10220')
			AND (device_status.Offline = 0)
			AND (device_status.SevLevel = 0)
	),
	AmpliOK AS (
		SELECT devices.DevId, devices.NodId
		FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID
		WHERE [Type] = 'DT04000' 
			AND (device_status.Offline = 0)
			AND (device_status.SevLevel = 0)
	)
	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT DISTINCT AlimOffLine.DevId, @RuleID as RuleID
		FROM AlimOffLine
		INNER JOIN PZoneOK ON AlimOffLine.NodId = PZoneOK.NodId
		INNER JOIN AmpliOK ON AlimOffLine.NodId = AmpliOK.NodId
END



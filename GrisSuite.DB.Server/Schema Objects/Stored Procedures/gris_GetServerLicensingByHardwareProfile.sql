﻿CREATE PROCEDURE gris_GetServerLicensingByHardwareProfile (@HardwareProfile varchar(64)) AS
SET NOCOUNT ON;

DECLARE @Licenses INT;

SELECT @Licenses = CONVERT(INT, dbo.fn_EncryptDecryptString(LicensingData))
FROM servers_licensing
WHERE (HardwareProfile LIKE @HardwareProfile);

SELECT ISNULL(@Licenses, 0) AS Licenses;
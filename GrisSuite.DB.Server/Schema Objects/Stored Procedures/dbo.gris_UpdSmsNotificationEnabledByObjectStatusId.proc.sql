﻿CREATE PROCEDURE gris_UpdSmsNotificationEnabledByObjectStatusId (@ObjectStatusId UNIQUEIDENTIFIER, @IsSmsNotificationEnabled BIT) AS
SET NOCOUNT ON;

UPDATE object_status
SET IsSmsNotificationEnabled = @IsSmsNotificationEnabled
WHERE ObjectStatusId = @ObjectStatusId
﻿CREATE PROCEDURE [dbo].[tf_DelStream_Fields]
(
	@Original_FieldID int,
	@Original_ArrayID int,
	@Original_StrID int,
	@Original_DevID bigint
)
AS
	SET NOCOUNT ON;
	
	UPDATE [reference] 
	SET 
		[DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE (
		([DeltaDevID] = @Original_DevID) AND
		([DeltaStrID] = @Original_StrID) AND
		([DeltaFieldID] = @Original_FieldID) AND
		([DeltaArrayID] = @Original_ArrayID)
		);

	UPDATE [stream_fields] 
	SET [ReferenceId] = null
	WHERE [ReferenceId] IN  (SELECT [reference].[ReferenceId]	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
								WHERE (
								([DevID] = @Original_DevID) AND
								([StrID] = @Original_StrID) AND
								([FieldID] = @Original_FieldID) AND
								([ArrayID] = @Original_ArrayID)
								)
							 );
	
	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE (
	([DevID] = @Original_DevID) AND
	([StrID] = @Original_StrID) AND
	([FieldID] = @Original_FieldID) AND
	([ArrayID] = @Original_ArrayID)
	);
	
	DELETE FROM [stream_fields] 
	WHERE (
	([DevID] = @Original_DevID) AND
	([StrID] = @Original_StrID) AND
	([FieldID] = @Original_FieldID) AND
	([ArrayID] = @Original_ArrayID)
	);



﻿CREATE PROCEDURE [dbo].[tf_UpdArchiveAlerts]
@KeepDays INT = 100
AS
DECLARE @limit DATETIME

SET @limit = DATEADD(d,-@KeepDays,GETDATE())
BEGIN TRANSACTION;

BEGIN TRY
	INSERT INTO alerts_devices_logs_archive
						  (AlertDeviceLogID, DevID, RuleID, LastUpdate, IsOpen, CreationDate, CloseDate)
	SELECT     AlertDeviceLogID, DevID, RuleID, LastUpdate, IsOpen, CreationDate, CloseDate
	FROM         alerts_devices_logs
	WHERE (IsOpen = 0) AND (CloseDate < @limit)

	DELETE FROM alerts_devices_logs
	WHERE (IsOpen = 0) AND (CloseDate < @limit)
	
	INSERT INTO alerts_tickets_archive
                      (AlertTicketID, DevID, TicketName, TicketNote, TicketDate, LastUpdate, IsOpen, CreationDate, CloseDate)
	SELECT     AlertTicketID, DevID, TicketName, TicketNote, TicketDate, LastUpdate, IsOpen, CreationDate, CloseDate
	FROM         alerts_tickets
	WHERE (IsOpen = 0) AND (CloseDate < @limit)
	
	DELETE FROM alerts_tickets
	WHERE (IsOpen = 0) AND (CloseDate < @limit)

END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
	
	DECLARE @msg VARCHAR(MAX)
	DECLARE @sev INT
	DECLARE @state INT
	SET @msg = ERROR_MESSAGE()
	SET @sev = ERROR_SEVERITY()
	SET @state = ERROR_STATE()
	RAISERROR(@msg,@sev,@state)
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

RETURN
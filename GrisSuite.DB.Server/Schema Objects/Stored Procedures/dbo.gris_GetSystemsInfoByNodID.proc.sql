﻿/*CREATE PROCEDURE [dbo].[gris_GetSystemsInfoByNodID]
	@NodID BIGINT,
	@MinuteTimeOut INT = 0
AS
	/*
	WITH DevicesWithStatus
	AS
	(
		SELECT DISTINCT 
			devices.DevID, device_status.Offline, ISNULL(device_status.ACKflag, 0) as ACK, DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) as MinuteDown, Active,
			systems.SystemID, devices.Type, streams.DevID as StreamPresent -- aggiunto il type per la modifica provvisoria che da priorità allo stato del pannello a zone
		FROM devices 
		INNER JOIN device_status ON devices.DevID = device_status.DevID 
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN systems ON systems.SystemID = device_type.SystemID
		LEFT JOIN streams ON streams.DevID = devices.DevID -- le periferiche con streams.DevID a null sono considerate OFFLINE
		INNER JOIN servers ON servers.SrvID = devices.SrvID
		WHERE ISNULL(devices.Removed, 0) = 0
		AND devices.NodID = @NodID
	), 
	NotOfflineDevices
	AS
	(
		SELECT devices.DevID, MAX(stream_fields.SevLevel) as MaxSeverity, systems.SystemID, devices.Type -- aggiunto il type per la modifica provvisoria che da priorità allo stato del pannello a zone
		FROM devices 
		INNER JOIN device_status ON devices.DevID = device_status.DevID 
		INNER JOIN stream_fields ON devices.DevID = stream_fields.DevID
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN systems ON systems.SystemID = device_type.SystemID
		INNER JOIN servers ON servers.SrvID = devices.SrvID
		WHERE (devices.Removed = 0) 
		AND (devices.Active = 1)
		AND devices.NodID = @NodID
		AND (device_status.Offline = 0) AND (DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) <= @MinuteTimeOut OR @MinuteTimeOut = 0)
		AND (ISNULL(device_status.ACKflag, 0) = 0) 
		AND (stream_fields.Visible = 1) 
		AND (ISNULL(stream_fields.ACKflag, 0) = 0) 
		AND (stream_fields.SevLevel <> 255)                                                         
		GROUP BY devices.DevID, systems.SystemID, devices.Type
	), 
	SystemStatus
	AS
	(
		SELECT 
			systems.SystemID, 
			systems.SystemDescription,
			(
				SELECT COUNT(DevID)
				FROM NotOfflineDevices 
				WHERE NotOfflineDevices.MaxSeverity = 0
				AND systems.SystemID = NotOfflineDevices.SystemID
				AND systems.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			) as Ok,
			(
				SELECT COUNT(DevID)
				FROM NotOfflineDevices 
				WHERE NotOfflineDevices.MaxSeverity = 1
				AND systems.SystemID = NotOfflineDevices.SystemID
				AND systems.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			) as Warning,
			(
				SELECT COUNT(DevID)
				FROM NotOfflineDevices 
				WHERE NotOfflineDevices.MaxSeverity = 2
				AND systems.SystemID = NotOfflineDevices.SystemID
				AND systems.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			) as Error,
			(
				SELECT COUNT(DevID)
				FROM DevicesWithStatus
				WHERE ([Offline] = 1 OR (MinuteDown > @MinuteTimeOut AND @MinuteTimeOut <> 0) OR (StreamPresent IS NULL)) -- la periferica è considerata offline se marcata come offline, se monitorata da un STLC1000 offline o quando priva di streams
				AND ACK = 0
				AND systems.SystemID = DevicesWithStatus.SystemID
				AND Active = 1
				AND systems.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			) as [Offline],	
			(
				SELECT COUNT(DevID)
				FROM DevicesWithStatus
				WHERE [Offline] = 1
				AND ACK = 1
				AND systems.SystemID = DevicesWithStatus.SystemID
				AND Active = 1
				AND systems.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			) as ACK,
			(
				SELECT COUNT(DevID)
				FROM DevicesWithStatus
				WHERE systems.SystemID = DevicesWithStatus.SystemID
			) as DevicesNumber,
			(
				SELECT COUNT(DevID)
				FROM DevicesWithStatus
				WHERE systems.SystemID = DevicesWithStatus.SystemID
				AND Active = 0
				AND systems.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			) as NotActive
		FROM systems
	)
	SELECT 
		SystemID, SystemDescription, Ok, Warning, Error, [Offline], ACK, DevicesNumber, 
		CAST((
			-- switch aggiunto per la modifica provvisoria che da priorità allo stato del pannello a zone quando diverso da OK
			CASE 
			WHEN EXISTS (SELECT DevID FROM NotOfflineDevices WHERE SystemStatus.SystemID = NotOfflineDevices.SystemID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND MaxSeverity <> 0) -- quelle in warning o errore
			THEN
				(SELECT MAX(MaxSeverity) FROM NotOfflineDevices WHERE SystemStatus.SystemID = NotOfflineDevices.SystemID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3')) -- prendo il PZ con lo stato peggiore
			WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE SystemStatus.SystemID = DevicesWithStatus.SystemID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND ([Offline] = 1 OR (MinuteDown > @MinuteTimeOut AND @MinuteTimeOut <> 0))) -- quelle offline
			THEN 3 -- OFFLINE
			ELSE
				-- parte originale
				dbo.GetEntityStateValue(Ok, Warning, Error, [Offline], ACK)
			END
		) AS SMALLINT) as [Status],
		NotActive
	FROM SystemStatus;
	*/*/
GO
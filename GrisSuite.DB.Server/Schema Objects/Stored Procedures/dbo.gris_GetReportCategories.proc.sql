﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 02/10/2010
-- Description:	
-- =============================================
CREATE PROCEDURE gris_GetReportCategories 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT rc.ResourceCategoryID, ResourceCategoryDescription, rc.SortOrder
	FROM resource_categories rc
	INNER JOIN resources r ON r.ResourceCategoryID = rc.ResourceCategoryID
	WHERE r.ResourceTypeID = 1 /* Reports */
	ORDER BY rc.SortOrder;
END
﻿CREATE PROCEDURE [dbo].[gris_GetVirtualObjectsByNodIDAndSystemID]
(
	@NodID bigint, 
	@SystemID int
)
AS
BEGIN
	SET NOCOUNT ON;
	-- Ritorna la lista di oggetti virtuali figli del sistema corrente, oppure un oggetto virtuale fittizio, nel caso la stazione non stia gestendo oggetti virtuali

	DECLARE @NodeSystem_ObjectStatusId UNIQUEIDENTIFIER;
	SELECT @NodeSystem_ObjectStatusId = object_status.ObjectStatusId FROM object_status INNER JOIN node_systems ON object_status.ObjectId = node_systems.NodeSystemsId WHERE (object_status.ObjectTypeId = 4 /* node_systems */) AND (node_systems.NodId = @NodID) AND (node_systems.SystemId = @SystemID);

	SELECT
	virtual_objects.VirtualObjectID,
	object_virtuals.ObjectStatusId AS VirtualObjectStatusID,
	virtual_objects.VirtualObjectName,
	virtual_objects.VirtualObjectDescription,
	object_virtuals.SevLevel,
	ISNULL(object_virtuals.SevLevelDetailId, - 1) AS SevLevelDetailId,
	object_virtuals.SevLevelDescription,
	dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) AS IsObjectStatusIdComputedByCustomFormula
	INTO #VO
	FROM virtual_objects
	INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId
	INNER JOIN object_status ON object_status.ObjectStatusId = object_virtuals.ParentObjectStatusId
	WHERE (object_status.ObjectStatusId = @NodeSystem_ObjectStatusId);
	
	IF EXISTS (SELECT * FROM #VO)
	BEGIN
		SELECT
		VirtualObjectID,
		VirtualObjectStatusID,
		VirtualObjectName,
		VirtualObjectDescription,
		SevLevel,
		SevLevelDetailId,
		SevLevelDescription,
		IsObjectStatusIdComputedByCustomFormula
		FROM #VO
		ORDER BY dbo.GetVirtualObjectCustomOrder(VirtualObjectName, 0);
	END
	ELSE
	BEGIN
		SELECT
		CONVERT(BIGINT, 0) AS VirtualObjectID,
		NULL AS VirtualObjectStatusID,
		'' AS VirtualObjectName,
		'' AS VirtualObjectDescription,
		-1 AS SevLevel,
		-1 AS SevLevelDetailId,
		'' AS SevLevelDescription,
		CONVERT(TINYINT, 0) AS IsObjectStatusIdComputedByCustomFormula
	END
	
	DROP TABLE #VO;
END
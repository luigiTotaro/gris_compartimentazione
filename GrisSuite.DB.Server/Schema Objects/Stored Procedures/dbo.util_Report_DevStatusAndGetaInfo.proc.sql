﻿CREATE PROCEDURE [dbo].[util_Report_DevStatusAndGetaInfo]
  @Compartimento as varchar(64),
  @Linea as varchar(64),
  @DeviceStatus VARCHAR(128)

AS
	DECLARE @RegID BIGINT, @ZonID BIGINT, @NodID BIGINT, @SystemID INT

	IF ISNULL(@Compartimento,'') = ''
	   SELECT @RegID = -1;
	ELSE
	   SELECT TOP 1 @RegID = RegID FROM regions WHERE [name] like '%' + @Compartimento + '%'

	IF ISNULL(@Linea,'') = ''
	   SELECT @ZonID = -1;
	ELSE
	   SELECT TOP 1 @ZonID = ZonID FROM zones WHERE [name] like '%' + @Linea + '%'

	SET @NodID = -1
	SET @SystemID = -1

	EXEC [dbo].[gris_Report_DevStatusAndGetaInfo] @RegID, @ZonID, @NodID, @SystemID, @DeviceStatus
RETURN
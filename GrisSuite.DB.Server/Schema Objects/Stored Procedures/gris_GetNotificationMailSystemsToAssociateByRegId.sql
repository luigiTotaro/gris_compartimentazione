﻿CREATE PROCEDURE [dbo].[gris_GetNotificationMailSystemsToAssociateByRegId](@RegIds         VARCHAR(MAX),
                                                                           @Search VARCHAR(MAX) = '') AS
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    object_status.ObjectStatusId,
    regions.Name                                                                 AS RegionName,
    zones.Name                                                                   AS ZoneName,
    nodes.Name                                                                   AS NodeName,
    systems.SystemDescription                                                    AS SystemName,
    dbo.GetNotificationContactsByObjectStatusId(object_status.ObjectStatusId, 1) AS NotificationContacts
  FROM object_status
    INNER JOIN node_systems ON object_status.ObjectId = node_systems.NodeSystemsId AND object_status.ObjectTypeId = 4
    /* node_systems */
    INNER JOIN systems ON systems.SystemID = node_systems.SystemId
    INNER JOIN nodes ON node_systems.NodId = nodes.NodID
    INNER JOIN zones ON nodes.ZonID = zones.ZonID
    INNER JOIN regions ON zones.RegID = regions.RegID
  WHERE (CHARINDEX('|' + CAST(regions.RegID AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (@Search = '%%'
             OR regions.Name LIKE @Search
             OR zones.Name LIKE @Search
             OR nodes.Name LIKE @Search
             OR systems.SystemDescription LIKE @Search
        )
  ORDER BY RegionName, ZoneName, NodeName, systems.SystemID;
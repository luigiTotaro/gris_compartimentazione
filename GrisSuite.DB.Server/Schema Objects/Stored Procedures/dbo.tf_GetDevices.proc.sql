﻿CREATE PROCEDURE [dbo].[tf_GetDevices]
AS
	SET NOCOUNT ON;
	SELECT DevID, NodID, SrvID, Name, Type, SN, PortID, Addr, ProfileID, Active, Scheduled, RackID, RackPositionRow, RackPositionCol, DefinitionVersion, ProtocolDefinitionVersion
	FROM devices



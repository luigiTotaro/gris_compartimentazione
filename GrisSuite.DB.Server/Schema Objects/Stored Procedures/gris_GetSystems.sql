﻿CREATE PROCEDURE [dbo].[gris_GetSystems] AS
SET NOCOUNT ON;

SELECT SystemID, SystemDescription
FROM systems

UNION ALL

SELECT 0 AS SystemID, 'Tutti i sistemi' AS SystemDescription

ORDER BY SystemID
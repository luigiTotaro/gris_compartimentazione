﻿CREATE PROCEDURE [dbo].[gris_GetObjectNodesByStatus] (@SevLevel INT, @LastPositionUpdateDate DATETIME = NULL, @FilterSystemID INT = NULL, @HideNotActiveNodes TINYINT = 0) AS
SET NOCOUNT ON;

DECLARE @floatZero FLOAT
SET @floatZero = 0;
	
SET @LastPositionUpdateDate = ISNULL(@LastPositionUpdateDate, '19000101');
SET @HideNotActiveNodes = ISNULL(@HideNotActiveNodes, 0);

IF (ISNULL(@FilterSystemID, 0) = 0)
BEGIN
	-- Non sono attivi i filtri per sistema, visualizzazione classica
	SELECT DISTINCT object_nodes_withDevice.ObjectStatusId -- il DISTINCT elimina le righe doppie presenti facendo join verso la server (senza la mappa non renderizza le linee)
		,object_nodes_withDevice.ObjectId
		,3 AS ObjectTypeId
		,object_nodes_withDevice.SevLevel
		,object_nodes_withDevice.SevLevelDetailId
		,object_status.SevLevelDetailIdLast
		,object_status.SevLevelDetailIdReal
		,object_status.SevLevelLast
		,object_nodes_withDevice.SevLevelReal
		,ISNULL(nodes.Name,'') AS Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints 
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,severity.Description AS SevLevelDescription
		,@floatZero AS MapOffsetX
		,@floatZero AS MapOffsetY
		,@floatZero + 30 AS CaptionFromScale
		,dbo.IsWithServerObject(object_nodes_withDevice.ObjectStatusId) AS WithServer
		,'' AS GroupName
		,dbo.GetAttributeForObject(8 /* Color */, object_status.ObjectStatusId, NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS CustomColor
	FROM object_classification_values
		INNER JOIN object_nodes_withDevice ON object_classification_values.ObjectStatusId = object_nodes_withDevice.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN nodes ON nodes.NodID = object_nodes_withDevice.ObjectId
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT OUTER JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT OUTER JOIN servers ON nodes.NodID = servers.NodID
		INNER JOIN severity ON severity.SevLevel = object_nodes_withDevice.SevLevel
	WHERE ((@SevLevel = -2) OR (object_nodes_withDevice.SevLevel = @SevLevel))
		AND (object_nodes_withDevice.SevLevel >= 0) -- per coerenza con la SP gris_GetNodesStatus, che genera la tabellare
		AND (ISNULL(servers.IsDeleted, 0) = 0) -- i nodi senza server non possono essere cancellati, servono per tracciare le linee
		AND ((@HideNotActiveNodes = 0) OR (object_nodes_withDevice.SevLevel <> 9 /* Non attivo */))
	ORDER BY Name
END
ELSE
BEGIN
	-- E' attivo il filtro per sistema: le regioni e le linee non hanno uno stato calcolato e le stazioni hanno lo stato uguale a quello del sistema
	-- indicato, se presente, oppure nessuno stato
	SELECT DISTINCT object_nodes_withDevice.ObjectStatusId -- il DISTINCT elimina le righe doppie presenti facendo join verso la server (senza la mappa non renderizza le linee)
		,object_nodes_withDevice.ObjectId
		,3 AS ObjectTypeId
		,ISNULL(SystemsData.SevLevel, 9 /* Non attivo */) AS SevLevel
		,CASE
			WHEN SystemsData.SevLevel IS NULL AND SystemsData.SevLevelDetailId IS NULL THEN 4 /* Non attivo */
			ELSE SystemsData.SevLevelDetailId
		END AS SevLevelDetailId
		,SystemsData.SevLevelDetailIdLast
		,SystemsData.SevLevelDetailIdReal
		,SystemsData.SevLevelLast
		,SystemsData.SevLevelReal
		,ISNULL(nodes.Name,'') AS Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints 
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,CASE
			WHEN SystemsData.SevLevel IS NULL THEN 'Non attivo'
			ELSE SystemsData.SevLevelDescription
		END AS SevLevelDescription
		,@floatZero AS MapOffsetX
		,@floatZero AS MapOffsetY
		,@floatZero + 30 AS CaptionFromScale
		,dbo.IsWithServerObject(object_nodes_withDevice.ObjectStatusId) AS WithServer
		,'' AS GroupName
		,NULL AS CustomColor
	FROM object_classification_values
		INNER JOIN object_nodes_withDevice ON object_classification_values.ObjectStatusId = object_nodes_withDevice.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN nodes ON nodes.NodID = object_nodes_withDevice.ObjectId
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT OUTER JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT OUTER JOIN servers ON nodes.NodID = servers.NodID
		LEFT OUTER JOIN (
			-- Nel caso di filtro per sistema attivo, le varie severità della stazione saranno determinate dal rispettivo sistema
			-- Se il sistema non esiste per quella stazione o è una stazione senza server, lo stato sarà non attivo
			SELECT
			node_systems.SystemId,
			node_systems.NodId,
			object_status.SevLevel,
			object_status.SevLevelDetailId,
			object_status.SevLevelDetailIdLast,
			object_status.SevLevelDetailIdReal,
			object_status.SevLevelLast,
			object_status.SevLevelReal,
			severity.Description AS SevLevelDescription
			FROM node_systems
			INNER JOIN object_status on object_status.ObjectId = node_systems.NodeSystemsId
			INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
			WHERE ((node_systems.SystemId IS NULL) OR (node_systems.SystemId = @FilterSystemID))
			AND (object_status.ObjectTypeId = 4 /* node_systems */)
		) SystemsData ON object_nodes_withDevice.ObjectId = SystemsData.NodId
		LEFT OUTER JOIN severity ON severity.SevLevel = SystemsData.SevLevel
	WHERE ((@SevLevel = -2) OR (ISNULL(SystemsData.SevLevel, 9 /* Non attivo */) = @SevLevel))
		AND (ISNULL(SystemsData.SevLevel, 9 /* Non attivo */) >= 0) -- per coerenza con la SP gris_GetNodesStatus, che genera la tabellare
		AND (ISNULL(servers.IsDeleted, 0) = 0) -- i nodi senza server non possono essere cancellati, servono per tracciare le linee
		AND ((@HideNotActiveNodes = 0) OR (ISNULL(SystemsData.SevLevel, 9 /* Non attivo */) <> 9 /* Non attivo */))
	ORDER BY Name
END
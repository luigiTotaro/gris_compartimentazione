﻿CREATE PROCEDURE [dbo].[gris_DelParameters]
(
	@Original_ParameterName varchar(64)
)
AS
	SET NOCOUNT OFF;
	DELETE FROM [parameters] 
	WHERE ([ParameterName] = @Original_ParameterName)
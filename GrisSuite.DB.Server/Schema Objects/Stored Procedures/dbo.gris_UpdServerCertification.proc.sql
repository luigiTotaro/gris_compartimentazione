﻿CREATE PROCEDURE [dbo].[gris_UpdServerCertification]
	@SrvID int,
	@AccountDocValidation varchar(256),
	@DateDocValidation datetime,
	@AccountUTValidation varchar(256),
	@DateUTValidation datetime,
	@DocValidation bit
AS
	IF ( @DocValidation = 1 )
	BEGIN
		UPDATE servers
		SET AccountDocValidation = @AccountDocValidation, DateDocValidation = @DateDocValidation
		WHERE SrvID = @SrvID;	
	END
	ELSE
	BEGIN
		UPDATE servers
		SET AccountUTValidation = @AccountUTValidation, DateUTValidation = @DateUTValidation
		WHERE SrvID = @SrvID;	
	END



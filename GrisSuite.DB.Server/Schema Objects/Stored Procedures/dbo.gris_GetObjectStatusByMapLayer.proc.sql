﻿CREATE PROCEDURE [dbo].[gris_GetObjectStatusByMapLayer] (@MapLayerId INT, @ClassificationId INT, @LastPositionUpdateDate DATETIME = NULL, @FilterSystemID INT = NULL, @HideNotActiveNodes TINYINT = 0) AS
SET NOCOUNT ON;
SET @LastPositionUpdateDate = ISNULL(@LastPositionUpdateDate, '19000101');
SET @HideNotActiveNodes = ISNULL(@HideNotActiveNodes, 0);

IF (ISNULL(@FilterSystemID, 0) = 0)
BEGIN
	-- Non sono attivi i filtri per sistema, visualizzazione classica
	WITH ObjectClass as
	(
		SELECT ocv.ObjectStatusId, ocv.ClassificationValueId, cv.ValueName
		FROM object_classification_values ocv
		INNER JOIN classification_values cv ON ocv.ClassificationValueId = cv.ClassificationValueId
		WHERE ClassificationId = @ClassificationId
	)
	SELECT object_regions.ObjectStatusId
		,object_regions.ObjectId
		,1 AS ObjectTypeId
		,object_regions.SevLevel
		,object_regions.SevLevelDetailId
		,object_status.SevLevelDetailIdLast
		,object_status.SevLevelDetailIdReal
		,object_status.SevLevelLast
		,object_regions.SevLevelReal
		,ISNULL(regions.Name,'')AS Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,severity.Description AS SevLevelDescription
		,object_classification_values.MapOffsetX
		,object_classification_values.MapOffsetY
		,(CASE WHEN object_classification_values.CaptionFromScale > 0 THEN object_classification_values.CaptionFromScale ELSE map_layers.CaptionFromScale END) AS CaptionFromScale
		,dbo.IsWithServerRegion(object_regions.ObjectId) as WithServer
		,ISNULL(ObjectClass.ValueName,'') AS GroupName
		,dbo.GetAttributeForObject(8 /* Color */, NULL, regions.RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS CustomColor
	FROM object_classification_values
		INNER JOIN object_regions ON object_classification_values.ObjectStatusId = object_regions.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN regions ON regions.RegID = object_regions.ObjectId
		INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT JOIN ObjectClass ON object_status.ObjectStatusId = ObjectClass.ObjectStatusId
	WHERE object_classification_values.MapLayerId = @MapLayerId

	UNION ALL

	SELECT object_zones.ObjectStatusId
		,object_zones.ObjectId
		,2 as ObjectTypeId
		,object_zones.SevLevel
		,object_zones.SevLevelDetailId
		,object_status.SevLevelDetailIdLast
		,object_status.SevLevelDetailIdReal
		,object_status.SevLevelLast
		,object_zones.SevLevelReal
		, ISNULL(zones.Name,'') as Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,severity.Description AS SevLevelDescription
		,object_classification_values.MapOffsetX
		,object_classification_values.MapOffsetY
		,(CASE WHEN object_classification_values.CaptionFromScale > 0 THEN object_classification_values.CaptionFromScale ELSE map_layers.CaptionFromScale END) AS CaptionFromScale
		,dbo.IsWithServerZone(object_zones.ObjectId) as WithServer
		,ISNULL(ObjectClass.ValueName,'') AS GroupName
		,dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, zones.ZonID, NULL, NULL, NULL, NULL, NULL) AS CustomColor
	FROM object_classification_values
		INNER JOIN object_zones ON object_classification_values.ObjectStatusId = object_zones.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN zones ON object_zones.ObjectId = zones.ZonID
		INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT JOIN ObjectClass ON object_status.ObjectStatusId = ObjectClass.ObjectStatusId
	WHERE object_classification_values.MapLayerId = @MapLayerId

	UNION ALL

	SELECT DISTINCT object_nodes_withDevice.ObjectStatusId -- il DISTINCT elimina le righe doppie presenti facendo join verso la server (senza la mappa non renderizza le linee)
		,object_nodes_withDevice.ObjectId
		,3 AS ObjectTypeId
		,object_nodes_withDevice.SevLevel
		,object_nodes_withDevice.SevLevelDetailId
		,object_status.SevLevelDetailIdLast
		,object_status.SevLevelDetailIdReal
		,object_status.SevLevelLast
		,object_nodes_withDevice.SevLevelReal
		,ISNULL(nodes.Name,'') AS Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,severity.Description AS SevLevelDescription
		,object_classification_values.MapOffsetX
		,object_classification_values.MapOffsetY
		,(CASE WHEN object_classification_values.CaptionFromScale > 0 THEN object_classification_values.CaptionFromScale ELSE map_layers.CaptionFromScale END) AS CaptionFromScale
		,dbo.IsWithServerNode(object_nodes_withDevice.ObjectId) as WithServer
		,ISNULL(ObjectClass.ValueName,'') AS GroupName
		,dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, Nodes.NodID, NULL, NULL, NULL, NULL) AS CustomColor
	FROM object_classification_values
		INNER JOIN object_nodes_withDevice ON object_classification_values.ObjectStatusId = object_nodes_withDevice.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN nodes ON nodes.NodID = object_nodes_withDevice.ObjectId
		INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT JOIN ObjectClass ON object_status.ObjectStatusId = ObjectClass.ObjectStatusId
		LEFT OUTER JOIN servers ON nodes.NodID = servers.NodID
	WHERE (object_classification_values.MapLayerId = @MapLayerId)
		AND (object_nodes_withDevice.SevLevel >= 0) -- per coerenza con la SP gris_GetNodesStatus, che genera la tabellare
		AND (ISNULL(servers.IsDeleted, 0) = 0) -- i nodi senza server non possono essere cancellati, servono per tracciare le linee
		AND ((@HideNotActiveNodes = 0) OR (object_nodes_withDevice.SevLevel <> 9 /* Non attivo */))
	ORDER BY Name
END
ELSE
BEGIN
	-- E' attivo il filtro per sistema: le regioni e le linee non hanno uno stato calcolato e le stazioni hanno lo stato uguale a quello del sistema
	-- indicato, se presente, oppure nessuno stato
	WITH ObjectClass as
	(
		SELECT ocv.ObjectStatusId, ocv.ClassificationValueId, cv.ValueName
		FROM object_classification_values ocv
		INNER JOIN classification_values cv ON ocv.ClassificationValueId = cv.ClassificationValueId
		WHERE ClassificationId = @ClassificationId
	)
	SELECT object_regions.ObjectStatusId
		,object_regions.ObjectId
		,1 AS ObjectTypeId
		,9 /* Non attivo */ AS SevLevel
		,4 /* Non attivo */ AS SevLevelDetailId
		,NULL AS SevLevelDetailIdLast
		,NULL AS SevLevelDetailIdReal
		,NULL AS SevLevelLast
		,NULL AS SevLevelReal
		,ISNULL(regions.Name,'') AS Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,'Non attivo' AS SevLevelDescription
		,object_classification_values.MapOffsetX
		,object_classification_values.MapOffsetY
		,(CASE WHEN object_classification_values.CaptionFromScale > 0 THEN object_classification_values.CaptionFromScale ELSE map_layers.CaptionFromScale END) AS CaptionFromScale
		,dbo.IsWithServerRegion(object_regions.ObjectId) as WithServer
		,ISNULL(ObjectClass.ValueName,'') AS GroupName
		,NULL AS CustomColor
	FROM object_classification_values
		INNER JOIN object_regions ON object_classification_values.ObjectStatusId = object_regions.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN regions ON regions.RegID = object_regions.ObjectId
		INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT JOIN ObjectClass ON object_status.ObjectStatusId = ObjectClass.ObjectStatusId
	WHERE object_classification_values.MapLayerId = @MapLayerId

	UNION ALL

	SELECT object_zones.ObjectStatusId
		,object_zones.ObjectId
		,2 as ObjectTypeId
		,9 /* Non attivo */ AS SevLevel
		,4 /* Non attivo */ AS SevLevelDetailId
		,NULL AS SevLevelDetailIdLast
		,NULL AS SevLevelDetailIdReal
		,NULL AS SevLevelLast
		,NULL AS SevLevelReal
		, ISNULL(zones.Name,'') AS Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,'Non attivo' AS SevLevelDescription
		,object_classification_values.MapOffsetX
		,object_classification_values.MapOffsetY
		,(CASE WHEN object_classification_values.CaptionFromScale > 0 THEN object_classification_values.CaptionFromScale ELSE map_layers.CaptionFromScale END) AS CaptionFromScale
		,dbo.IsWithServerZone(object_zones.ObjectId) as WithServer
		,ISNULL(ObjectClass.ValueName,'') AS GroupName
		,NULL AS CustomColor
	FROM object_classification_values
		INNER JOIN object_zones ON object_classification_values.ObjectStatusId = object_zones.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN zones ON object_zones.ObjectId = zones.ZonID
		INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT JOIN ObjectClass ON object_status.ObjectStatusId = ObjectClass.ObjectStatusId
	WHERE object_classification_values.MapLayerId = @MapLayerId

	UNION ALL

	SELECT DISTINCT object_nodes_withDevice.ObjectStatusId -- il DISTINCT elimina le righe doppie presenti facendo join verso la server (senza la mappa non renderizza le linee)
		,object_nodes_withDevice.ObjectId
		,3 AS ObjectTypeId
		,ISNULL(SystemsData.SevLevel, 9 /* Non attivo */) AS SevLevel
		,CASE
			WHEN SystemsData.SevLevel IS NULL AND SystemsData.SevLevelDetailId IS NULL THEN 4 /* Non attivo */
			ELSE SystemsData.SevLevelDetailId
		END AS SevLevelDetailId
		,SystemsData.SevLevelDetailIdLast
		,SystemsData.SevLevelDetailIdReal
		,SystemsData.SevLevelLast
		,SystemsData.SevLevelReal
		,ISNULL(nodes.Name,'') AS Name
		,(CASE WHEN (@LastPositionUpdateDate < object_map_points.UpdateDate) THEN ISNULL(object_map_points.MapPoints,'') ELSE '' END) as MapPoints
		,ISNULL(object_map_points.UpdateDate, '19000101') as PositionUpdateDate
		,CASE
			WHEN SystemsData.SevLevel IS NULL THEN 'Non attivo'
			ELSE SystemsData.SevLevelDescription
		END AS SevLevelDescription
		,object_classification_values.MapOffsetX
		,object_classification_values.MapOffsetY
		,(CASE WHEN object_classification_values.CaptionFromScale > 0 THEN object_classification_values.CaptionFromScale ELSE map_layers.CaptionFromScale END) AS CaptionFromScale
		,dbo.IsWithServerNode(object_nodes_withDevice.ObjectId) as WithServer
		,ISNULL(ObjectClass.ValueName,'') AS GroupName
		,NULL AS CustomColor
	FROM object_classification_values
		INNER JOIN object_nodes_withDevice ON object_classification_values.ObjectStatusId = object_nodes_withDevice.ObjectStatusId
		INNER JOIN object_status ON object_classification_values.ObjectStatusId = object_status.ObjectStatusId
		INNER JOIN nodes ON nodes.NodID = object_nodes_withDevice.ObjectId
		INNER JOIN map_layers ON object_classification_values.MapLayerId = map_layers.MapLayerId
		LEFT OUTER JOIN object_map_points ON object_status.ObjectStatusId = object_map_points.ObjectStatusId
		LEFT OUTER JOIN ObjectClass ON object_status.ObjectStatusId = ObjectClass.ObjectStatusId
		LEFT OUTER JOIN servers ON nodes.NodID = servers.NodID
		LEFT OUTER JOIN (
			-- Nel caso di filtro per sistema attivo, le varie severità della stazione saranno determinate dal rispettivo sistema
			-- Se il sistema non esiste per quella stazione o è una stazione senza server, lo stato sarà non attivo
			SELECT
			node_systems.SystemId,
			node_systems.NodId,
			object_status.SevLevel,
			object_status.SevLevelDetailId,
			object_status.SevLevelDetailIdLast,
			object_status.SevLevelDetailIdReal,
			object_status.SevLevelLast,
			object_status.SevLevelReal,
			severity.Description AS SevLevelDescription
			FROM node_systems
			INNER JOIN object_status on object_status.ObjectId = node_systems.NodeSystemsId
			INNER JOIN severity ON severity.SevLevel = object_status.SevLevel
			WHERE ((node_systems.SystemId IS NULL) OR (node_systems.SystemId = @FilterSystemID))
			AND (object_status.ObjectTypeId = 4 /* node_systems */)
		) SystemsData ON object_nodes_withDevice.ObjectId = SystemsData.NodId
		LEFT OUTER JOIN severity ON severity.SevLevel = SystemsData.SevLevel
	WHERE (object_classification_values.MapLayerId = @MapLayerId)
		AND (ISNULL(SystemsData.SevLevel, 9 /* Non attivo */) >= 0) -- per coerenza con la SP gris_GetNodesStatus, che genera la tabellare
		AND (ISNULL(servers.IsDeleted, 0) = 0) -- i nodi senza server non possono essere cancellati, servono per tracciare le linee
		AND ((@HideNotActiveNodes = 0) OR (ISNULL(SystemsData.SevLevel, 9 /* Non attivo */) <> 9 /* Non attivo */))
	ORDER BY Name
END
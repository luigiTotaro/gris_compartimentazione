CREATE PROCEDURE [dbo].[gris_Report_GetPercDeviceStatus]
(
	@RegID BIGINT = -1,
	@ZonID BIGINT = -1,
	@NodID BIGINT = -1
)
AS
BEGIN

	SET NOCOUNT ON;
	
	declare @AreaId as bigint, @AreaName as varchar(64), @AreaType as varchar(16);
	
	if (@NodID = -1 and @ZonID = -1 and @RegID = -1)
		begin
			select @AreaId = -1, @AreaName = 'Tutte le periferiche', @AreaType = 'italia';
		end
	else
		begin
			if (@RegID <> -1) 
				begin
					select @AreaId = RegID, @AreaName = [Name], @AreaType = 'region' from regions where RegID = @RegID;
				end
			if (@ZonID <> -1) 
				begin
					select @AreaId = ZonID, @AreaName = [Name], @AreaType = 'zone' from zones where ZonID = @ZonID;
				end
			if (@NodID <> -1) 
				begin
					select @AreaId = NodID, @AreaName = [Name], @AreaType = 'node' from nodes where NodID = @NodID;
				end
		end;
		

	with countInfos as
	(
		select distinct 
			SevLevel, count(*) over(partition by SevLevel) as CountBySevLevel, count(*) over() as CountAllDevices 
		from dbo.object_devices
		inner join devices on devices.DevID = dbo.object_devices.ObjectId
		inner join nodes on nodes.NodID = devices.NodID
		inner join zones on zones.ZonID = nodes.ZonID
		where
			(zones.RegID = @RegID or @RegID = -1)
			and (nodes.ZonID = @ZonID or @ZonID = -1)
			and (devices.NodID = @NodID or @NodID = -1)
	)
	select 
		severity.SevLevel as [Status], 
		severity.Description as [StatusDescription], 
		isnull((countInfos.CountBySevLevel * 1.0 / countInfos.CountAllDevices * 1.0), 0) as [Percent],
		@AreaId as FilterId,
		@AreaName as FilterDescription,
		@AreaType as FilterType
	from severity
	left join countInfos on severity.SevLevel = countInfos.SevLevel;

END



﻿CREATE Procedure [dbo].[util_EraseTestServer]
AS	
-- Erase stress test Servers
	DECLARE @SrvID int
	DECLARE servers_cur CURSOR FOR 
		SELECT SrvId
		FROM servers
		where [Name] Like 'STRESSTEST:%'
	
	OPEN servers_cur
	
	FETCH NEXT FROM servers_cur INTO @SrvID
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC [dbo].[tf_DelServers] @SrvID 
	   -- Get the next server
	   FETCH NEXT FROM servers_cur INTO @SrvID
	END
	
	CLOSE servers_cur
	DEALLOCATE servers_cur


-- Erase stress test Stations
	DECLARE @StationID uniqueidentifier
	DECLARE stations_cur CURSOR FOR 
		SELECT StationId
		FROM station
		where StationName Like 'STRESSTEST:%'
	
	OPEN stations_cur
	
	FETCH NEXT FROM stations_cur INTO @StationID
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC [dbo].[tf_DelStation] @StationID 
	   -- Get the next station
	   FETCH NEXT FROM stations_cur INTO @StationID
	END
	
	CLOSE stations_cur
	DEALLOCATE stations_cur



﻿CREATE PROCEDURE [dbo].[tf_DelServers]
(
	@Original_SrvID int,
	@Original_MAC varchar(16)
)
AS
	SET NOCOUNT ON; -- se off e non c'è la riga ADO.NET da errore
	DECLARE @OriginalSrvID int

	SET @Original_MAC = RTRIM(ISNULL(@Original_MAC,''))

	IF @Original_MAC <> '' -- Se cancello via MAC cancello il SrvId associato al MAC selezionato ignorando il SrvID passato
		BEGIN
			SET @Original_SrvID = NULL
			SELECT @Original_SrvID = SrvID FROM servers WHERE (MAC = @Original_MAC)
		END

	UPDATE [reference] 
	SET [DeltaFieldID] = null,
		[DeltaArrayID] = null, 
		[DeltaStrID] = null,
		[DeltaDevID] = null
	WHERE ([DeltaDevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))
	
	UPDATE [stream_fields] 
	SET	 ReferenceID  = null
	WHERE ReferenceID IN ( 
		SELECT Reference.ReferenceID
		FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
		WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	)

	DELETE [reference] 
	FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
	WHERE ([DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))
	
	DELETE FROM device_ack WHERE (DevID IN (SELECT DevID FROM devices WHERE SrvID = @Original_SrvID))

	DELETE FROM [stream_fields] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [streams]		WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	
	DELETE FROM [device_status] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID AND [Type] NOT LIKE 'STLC1000')
	DELETE FROM [devices]		WHERE [SrvID] = @Original_SrvID AND [Type] NOT LIKE 'STLC1000'	

	-- Tolto perchè i parametri non vengono inviati nello stesso momento in cui viene inviata 
	-- la configurazione e quindi dopo la cancellazione non verrebbero ripristinati.
	-- DELETE FROM [stlc_parameters] WHERE [SrvID] = @Original_SrvID 

	UPDATE [devices] SET PortId = null WHERE PortID in (SELECT PortId FROM [port] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [port]			WHERE [SrvID] = @Original_SrvID

	/*IF ( (SELECT ServerSupervisorSystemXMLValidated FROM [servers] WHERE [SrvID] = @Original_SrvID) IS NULL )
	BEGIN
		DELETE FROM [servers] WHERE [SrvID] = @Original_SrvID
	END*/



﻿-- Luca Quintarelli 11/11/2016
CREATE PROCEDURE gris_GetResourceCategories
  @ResourceTypeID INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT rc.ResourceCategoryID, ResourceCategoryDescription, rc.SortOrder
	FROM resource_categories rc
	INNER JOIN resources r ON r.ResourceCategoryID = rc.ResourceCategoryID
	WHERE r.ResourceTypeID = @ResourceTypeID
	ORDER BY rc.SortOrder;
END
﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 03/11/2010
-- Description:	
-- =============================================
CREATE PROCEDURE gris_DelPermissionGroups 
	@GroupID INT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM permission_groups WHERE GroupID = @GroupID;
END
CREATE PROCEDURE [dbo].[gris_Report_GetDevicesByNod_DevStatus]
@NodID BIGINT = -1, @ZonID BIGINT = -1, @RegID BIGINT = -1, @DeviceStatus INT = -2, @DeviceType VARCHAR(16) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  
		regions.Name as RegionName, zones.Name as ZoneName, nodes.Name as NodeName, 
		devices.devid as DeviceId, devices.Name as DeviceName, devices.Type as DeviceType, object_devices.SevLevel as DeviceStatus, object_devices.SevLevelDescription,
		ISNULL(station.StationName, '') as StationName, ISNULL(building.BuildingName, '') as BuildingName, ISNULL(rack.RackName, '') as RackName
	FROM object_devices
		INNER JOIN devices ON devices.DevID = object_devices.ObjectId
		INNER JOIN device_type ON devices.type = device_type.DeviceTypeID
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		LEFT JOIN rack ON devices.RackID = rack.RackID 
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID
	WHERE ((zones.RegID = @RegID) OR (@RegID = -1))
		AND ((nodes.ZonID = @ZonID) OR (@ZonID = -1))
		AND ((devices.NodID = @NodID) OR (@NodID = -1))
		AND ((devices.Type = @DeviceType) OR (@DeviceType = ''))
		AND (@DeviceStatus = object_devices.SevLevel OR @DeviceStatus = -2) -- -1 � un valore valido
	ORDER BY StationName, BuildingName, RackName;

END



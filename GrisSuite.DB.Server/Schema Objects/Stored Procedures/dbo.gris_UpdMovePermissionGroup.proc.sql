﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 03/11/2010
-- Description:	
-- =============================================
CREATE PROCEDURE gris_UpdMovePermissionGroup 
	@GroupID INT,
	@Direction BIT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	DECLARE @SwapGroupID INT, @SwapSortOrder TINYINT, @CurrentSortOrder TINYINT;
	SELECT @CurrentSortOrder = SortOrder FROM permission_groups WHERE GroupID = @GroupID;

	IF ( @Direction = 1 AND @CurrentSortOrder > 0 )
	BEGIN
		SELECT @SwapGroupID = GroupID, @SwapSortOrder = SortOrder 
		FROM permission_groups 
		WHERE SortOrder = @CurrentSortOrder - 1;
		
		BEGIN TRAN
		UPDATE permission_groups SET SortOrder = @CurrentSortOrder - 1 WHERE GroupID = @GroupID;
		UPDATE permission_groups SET SortOrder = @CurrentSortOrder WHERE GroupID = @SwapGroupID;
		COMMIT TRAN
	END
	
	IF ( @Direction = 0 )
	BEGIN
		SELECT @SwapGroupID = GroupID, @SwapSortOrder = SortOrder 
		FROM permission_groups 
		WHERE SortOrder = @CurrentSortOrder + 1;
		
		BEGIN TRAN
		UPDATE permission_groups SET SortOrder = @CurrentSortOrder + 1 WHERE GroupID = @GroupID;
		UPDATE permission_groups SET SortOrder = @CurrentSortOrder WHERE GroupID = @SwapGroupID;
		COMMIT TRAN
	END	

	SET XACT_ABORT OFF;
END
﻿CREATE PROCEDURE [dbo].[tf_UpdBuilding]
(
	@BuildingID uniqueidentifier,
	@BuildingXMLID int,
	@StationID uniqueidentifier,
	@BuildingName varchar(64),
	@BuildingDescription varchar(256),
	@Removed tinyint
)
AS
	SET NOCOUNT OFF;
	
	IF ISNULL(@Removed,0) = 0
	BEGIN
		IF EXISTS(SELECT BuildingID FROM [building] WHERE ([BuildingID] = @BuildingID))
			UPDATE [building] SET [BuildingXMLID] = @BuildingXMLID, [StationID] = @StationID, [BuildingName] = @BuildingName, [BuildingDescription] = @BuildingDescription, Removed = @Removed WHERE [BuildingID] = @BuildingID;
		ELSE
			INSERT INTO [building] ([BuildingID], [BuildingXMLID], [StationID], [BuildingName], [BuildingDescription], [Removed]) VALUES (@BuildingID, @BuildingXMLID, @StationID, @BuildingName, @BuildingDescription, @Removed);
	END;



﻿CREATE PROCEDURE [dbo].[gris_GetReportsByCategory]
	(@ResourceCategoryID INT)
AS
	SELECT DISTINCT res.ResourceID as ReportID, ResourceCode as ReportCode, ResourceDescription as ReportName, ISNULL(GroupName, '') as GroupName, res.SortOrder
	FROM resources res
	LEFT JOIN [permissions] per ON res.ResourceID = per.ResourceID
	LEFT JOIN permission_groups pg ON pg.GroupID = per.GroupID
	WHERE ResourceCategoryID = @ResourceCategoryID
		AND ResourceTypeID = 1 /* Reports */
	ORDER BY res.SortOrder;
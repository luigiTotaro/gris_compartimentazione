﻿CREATE PROCEDURE [dbo].[gris_GetRequestsForExceptions]
	@DateFrom DATETIME, 
	@DateTo DATETIME,
	@RequestMinuteInterval SMALLINT = 5
AS

	/*DECLARE @DateFrom DATETIME;
	DECLARE @DateTo DATETIME;
	DECLARE @RequestMinuteInterval SMALLINT;

	SET @DateFrom = '20080509';
	SET @DateTo = '20080509';
	SET @RequestMinuteInterval = 4;*/


	WITH UserRequests AS
	(
		SELECT DISTINCT 
			sessions.SessionKey, sessions.[User], 
			requests.QueryString, requests.Created
		FROM [sessions] 
		INNER JOIN [requests] ON sessions.SessionKey = requests.SessionKey
		INNER JOIN trace_messages ON sessions.SessionKey = trace_messages.SessionKey
		WHERE Exception IS NOT NULL
		AND CONVERT(CHAR(10), trace_messages.Created, 112) BETWEEN CONVERT(CHAR(10), @DateFrom, 112) AND CONVERT(CHAR(10), @DateTo, 112)
		AND DATEDIFF(mi, trace_messages.Created, requests.Created) BETWEEN @RequestMinuteInterval * -1 AND @RequestMinuteInterval
	)
	SELECT [User], QueryString, NULL AS Exception, NULL as FormValues, NULL as SessionValues, Created
	FROM UserRequests 
	UNION ALL
	SELECT [User], NULL AS QueryString, trace_messages.Exception, trace_messages.FormValues, trace_messages.SessionValues, trace_messages.Created
	FROM trace_messages 
	INNER JOIN [sessions] ON trace_messages.SessionKey = sessions.SessionKey
	WHERE CONVERT(CHAR(10), trace_messages.Created, 112) BETWEEN CONVERT(CHAR(10), @DateFrom, 112) AND CONVERT(CHAR(10), @DateTo, 112)
	ORDER BY Created;



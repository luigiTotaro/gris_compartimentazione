﻿CREATE PROCEDURE dbo.tf_InsSTLCParameters
(
	@SrvID int,
	@ParameterValue varchar(256),
	@ParameterName varchar(64),
	@ParameterDescription varchar(1024)
)
AS
	SET NOCOUNT OFF;
	INSERT INTO [stlc_parameters] ([SrvID], [ParameterValue], [ParameterName], [ParameterDescription]) VALUES (@SrvID, @ParameterValue, @ParameterName, @ParameterDescription);



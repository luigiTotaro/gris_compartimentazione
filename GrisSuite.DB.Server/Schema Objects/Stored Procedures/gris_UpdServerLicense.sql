﻿CREATE PROCEDURE [dbo].[gris_UpdServerLicense] (@ServerLicenseID int, @IsLicensed tinyint, @LicenseLastUpdateUser varchar(100)) AS
SET NOCOUNT ON;

IF (@IsLicensed = 1)
BEGIN
	-- Si sta assegnando la licenza al server
	DECLARE @HardwareProfile VARCHAR(64);
	-- Il profilo hardware può contenere un sottocodice dopo il punto, che deve essere rimosso per la gestione licenze per cliente
	SET @HardwareProfile = dbo.GetHardwareProfileCustomer((SELECT HardwareProfile FROM servers_licenses WHERE (ServerLicenseID = @ServerLicenseID)));

	DECLARE @LicenseCountByHardwareProfile int;
	SET @LicenseCountByHardwareProfile = ISNULL((SELECT CONVERT(INT, dbo.fn_EncryptDecryptString(LicensingData)) FROM servers_licensing WHERE (HardwareProfile = @HardwareProfile)), 0);

	DECLARE @CurrentLicensesCountByHardwareProfile int;
	SET @CurrentLicensesCountByHardwareProfile = (SELECT COUNT(ServerLicenseID) FROM servers_licenses WHERE (HardwareProfile = @HardwareProfile) AND (IsLicensed = 1));

	IF (@CurrentLicensesCountByHardwareProfile < @LicenseCountByHardwareProfile)
	BEGIN
		UPDATE servers_licenses
		SET IsLicensed = @IsLicensed,
		LicenseLastUpdate = GetDate(),
		LicenseLastUpdateUser = @LicenseLastUpdateUser
		WHERE (ServerLicenseID = @ServerLicenseID)
	END
END
ELSE IF (@IsLicensed = 0)
BEGIN
	-- Si sta rimuovendo la licenza dal server
	UPDATE servers_licenses
	SET IsLicensed = @IsLicensed,
	LicenseLastUpdate = GetDate(),
	LicenseLastUpdateUser = @LicenseLastUpdateUser
	WHERE (ServerLicenseID = @ServerLicenseID)
END
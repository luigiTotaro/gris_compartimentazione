﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 05-08-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributes_Get] 
(
	@TargetTypeID int = -1,
	@Namespace varchar(256) = ''
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TargetAttributeID, TargetAttributeName, [Namespace], IsObsolete, ControlTypeUI, TargetTypeID, MainAttribute, EnumValues, [Order]
	FROM geta.target_attributes
	WHERE (TargetTypeID = @TargetTypeID OR @TargetTypeID = -1)
	AND ([Namespace] = @Namespace OR @Namespace = '')
END



-- =============================================
-- Author:		Cristian Storti
-- Create date: 11-03-2008
-- Description:	
-- =============================================
/*CREATE PROCEDURE [dbo].[gris_GetAreaStatus]
@ID BIGINT, @AreaType TINYINT=0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @MinuteTimeOut INT
	SET @MinuteTimeOut = dbo.GetServerMinuteTimeOut()

	IF ( @AreaType = 0 ) -- Compartimenti
	BEGIN

		WITH Topography
		AS
		(
			SELECT
				regions.RegID, regions.Name as RegName, zones.ZonID, zones.Name as ZonName, nodes.NodID, nodes.Name as NodName, nodes.Meters,
				(SELECT COUNT(IntDevs.DevID) FROM devices IntDevs GROUP BY IntDevs.NodID HAVING IntDevs.NodID = devices.NodID) as DevicesTotalCount,
				(
					SELECT COUNT(DISTINCT servers.SrvID)
					FROM servers
					INNER JOIN devices intDevices ON servers.SrvID = intDevices.SrvID 
					INNER JOIN device_status ON intDevices.DevID = device_status.DevID
					WHERE 
						intDevices.NodID = devices.NodID AND
						(DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) > @MinuteTimeOut AND @MinuteTimeOut <> 0) --AND 
						-- servers.InExercise = 1
				) as STLCOffline, -- numero di stlc1000 offline nella stazione, un stlc è contato come offline se non comunica da un tempo superiore a quello di timeout, se monitora periferiche con uno stato e se non è in maintenance mode
				CASE WHEN (SUM(CAST(servers.InExercise AS TINYINT)) > 0) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as AlmostOneSTLCInExercise, -- flag che mi dice se c'è almeno un stlc1000 in esercizio nella stazione
				(
					SELECT COUNT(DISTINCT servers.SrvID)
					FROM servers
					INNER JOIN devices intDevices ON servers.SrvID = intDevices.SrvID 
					INNER JOIN device_status ON intDevices.DevID = device_status.DevID
					WHERE 
						intDevices.NodID = devices.NodID AND
						servers.InExercise = 0
				) as STLCInMaintMode -- numero di stlc1000 in maintenance mode nella stazione
			FROM devices 
			INNER JOIN servers ON devices.SrvID = servers.SrvID 
			INNER JOIN nodes ON devices.NodID = nodes.NodID
			INNER JOIN zones ON nodes.ZonID = zones.ZonID 
			INNER JOIN regions ON zones.RegID = regions.RegID
			GROUP BY regions.RegID, regions.Name, zones.ZonID, zones.Name, nodes.NodID, nodes.Name, nodes.Meters, devices.NodID
		),
		DevicesWithStatus
		AS
		(
			SELECT 
				devices.DevID, devices.Type, device_type.SystemID, devices.NodID,
				device_status.SevLevel as Severity, 
				CASE WHEN ([Offline] = 1 OR (DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) >= @MinuteTimeOut AND @MinuteTimeOut <> 0)) THEN 1 ELSE 0 END as [Offline], 
				devices.Active
			FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID 
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN servers ON servers.SrvID = devices.SrvID
			WHERE device_type.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			AND servers.InExercise = 1 -- solo se l'STLC1000 non è in maintenance mode
			-- tutte le periferiche d' Italia
		),
		DeviceSeverityAggregation
		AS
		(
			SELECT DISTINCT
				NodID, SystemID, Severity, [Offline], Active, 
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID, Severity, [Offline], Active) CountBySeverity,
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID) DeviceWithStatusCount
			FROM DevicesWithStatus
		),
		BaseStatus
		AS
		(
			SELECT 
				NodID, SystemID, 
				SUM(CASE WHEN Severity = 0 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Ok,
				SUM(CASE WHEN Severity = 1 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Warning,
				SUM(CASE WHEN Severity = 2 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Error,
				SUM(CASE WHEN Severity = 255 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Unknown,
				SUM(CASE WHEN [Offline] = 1 AND Active = 1 THEN CountBySeverity ELSE 0 END) as [Offline],
				SUM(CASE WHEN Active = 0 THEN CountBySeverity ELSE 0 END) as NotActive,
				DeviceWithStatusCount
			FROM DeviceSeverityAggregation
			GROUP BY NodID, SystemID, DeviceWithStatusCount
		),
		SystemStatus
		AS
		(
			SELECT
				Topography.RegID, Topography.RegName, Topography.ZonID, Topography.ZonName, Topography.NodID, Topography.NodName, Topography.Meters, Topography.AlmostOneSTLCInExercise, Topography.STLCOffline, Topography.STLCInMaintMode, BaseStatus.SystemID,
				ISNULL(Ok, 0) AS Ok, ISNULL(Warning, 0) AS Warning, ISNULL(Error, 0) AS Error, ISNULL(Unknown, 0) AS Unknown, ISNULL([Offline], 0) AS [Offline], ISNULL(NotActive, 0) AS NotActive, ISNULL(DeviceWithStatusCount, 0) AS DeviceWithStatusCount, ISNULL(DevicesTotalCount, 0) AS DevicesTotalCount, 
				CAST((
					-- switch aggiunto per la modifica provvisoria che da priorità allo stato del pannello a zone quando diverso da OK
					CASE 
					WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND Severity <> 0 AND ([Offline] = 0)) -- quelle in warning o errore
					THEN
						(SELECT CASE WHEN MAX(Severity) = 255 THEN 3 ELSE MAX(Severity) END FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3')) -- prendo il PZ con lo stato peggiore
					WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND ([Offline] = 1 )) -- quelle offline
					THEN 3 -- OFFLINE
					ELSE
						-- parte originale
						dbo.GetEntityStateValue(ISNULL(Ok, 0), ISNULL(Warning, 0), ISNULL(Error, 0), ISNULL([Offline], 0), 0)
					END
				) AS SMALLINT) as [Status]
			FROM Topography
			LEFT JOIN BaseStatus ON Topography.NodID = BaseStatus.NodID
		),
		NodeStatus
		AS
		(
			SELECT
				RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, AlmostOneSTLCInExercise, STLCOffline, STLCInMaintMode,
				SUM(Ok) as Ok, SUM(Warning) as Warning, SUM(Error) as Error, SUM(Unknown) AS Unknown, SUM([Offline]) as [Offline],  SUM(NotActive) as NotActive, SUM(DeviceWithStatusCount) as DeviceWithStatusCount, DevicesTotalCount,
				dbo.GetEntityStateValue(
										SUM(CASE WHEN ([Status] = 0) THEN 1 ELSE 0 END) --OK
										,SUM(CASE WHEN ([Status] = 1) THEN 1 ELSE 0 END) --WARNING
										,SUM(CASE WHEN ([Status] = 2) THEN 1 ELSE 0 END) --ERROR
										,SUM(CASE WHEN ([Status] = 3) THEN 1 ELSE 0 END)  --OFFLINE
										,0 --MAINTENACE
										) as [Status]	
			FROM SystemStatus
			GROUP BY RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, AlmostOneSTLCInExercise, STLCOffline, STLCInMaintMode, DevicesTotalCount
		),
		ZoneStatus
		AS
		(
			SELECT
				RegID, RegName, ZonID, ZonName, 
				CASE WHEN (SUM(CAST(AlmostOneSTLCInExercise AS TINYINT)) > 0) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as AlmostOneSTLCInExercise, SUM(STLCOffline) as STLCOffline, SUM(STLCInMaintMode) as STLCInMaintMode,
				SUM(Ok) as Ok, SUM(Warning) as Warning, SUM(Error) as Error, SUM(Unknown) AS Unknown, SUM([Offline]) as [Offline],  SUM(NotActive) as NotActive, SUM(DeviceWithStatusCount) as DeviceWithStatusCount, SUM(DevicesTotalCount) AS DevicesTotalCount,
				dbo.GetEntityStateValue(
										SUM(CASE WHEN ([Status] = 0) THEN 1 ELSE 0 END), --OK
										SUM(CASE WHEN ([Status] = 1) THEN 1 ELSE 0 END), --WARNING
										SUM(CASE WHEN ([Status] = 2) THEN 1 ELSE 0 END), --ERROR
										SUM(CASE WHEN ([Status] = 3) THEN 1 ELSE 0 END)  --OFFLINE
										,0 --MAINTENACE
										) as [Status]	
			FROM NodeStatus
			GROUP BY RegID, RegName, ZonID, ZonName
		)
		SELECT
			RegID, RegName,
			CASE WHEN (SUM(CAST(AlmostOneSTLCInExercise AS TINYINT)) > 0) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as AlmostOneSTLCInExercise, SUM(STLCOffline) as STLCOffline, SUM(STLCInMaintMode) as STLCInMaintMode,
			SUM(Ok) as Ok, SUM(Warning) as Warning, SUM(Error) as Error, SUM(Unknown) AS Unknown, SUM([Offline]) as [Offline],  SUM(NotActive) as NotActive, SUM(DeviceWithStatusCount) as DeviceWithStatusCount, SUM(DevicesTotalCount) AS DevicesTotalCount,
			dbo.GetEntityStateValue(
									SUM(CASE WHEN ([Status] = 0) THEN 1 ELSE 0 END), --OK
									SUM(CASE WHEN ([Status] = 1) THEN 1 ELSE 0 END), --WARNING
									SUM(CASE WHEN ([Status] = 2) THEN 1 ELSE 0 END), --ERROR
									SUM(CASE WHEN ([Status] = 3) THEN 1 ELSE 0 END)  --OFFLINE
										,0 --MAINTENACE
									) as [Status]	
		FROM ZoneStatus
		GROUP BY RegID, RegName
		ORDER BY RegName;

	END
	ELSE IF ( @AreaType = 1 ) -- Linee
	BEGIN

		WITH Topography
		AS
		(
			SELECT
				regions.RegID, regions.Name as RegName, zones.ZonID, zones.Name as ZonName, nodes.NodID, nodes.Name as NodName, nodes.Meters,
				(SELECT COUNT(IntDevs.DevID) FROM devices IntDevs GROUP BY IntDevs.NodID HAVING IntDevs.NodID = devices.NodID) as DevicesTotalCount,
				(
					SELECT COUNT(DISTINCT servers.SrvID)
					FROM servers
					INNER JOIN devices intDevices ON servers.SrvID = intDevices.SrvID 
					INNER JOIN device_status ON intDevices.DevID = device_status.DevID
					WHERE 
						intDevices.NodID = devices.NodID AND
						(DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) > @MinuteTimeOut AND @MinuteTimeOut <> 0) --AND 
						--servers.InExercise = 1
				) as STLCOffline, -- numero di stlc1000 offline nella stazione, un stlc è contato come offline se non comunica da un tempo superiore a quello di timeout, se monitora periferiche con uno stato e se non è in maintenance mode
				CASE WHEN (SUM(CAST(servers.InExercise AS TINYINT)) > 0) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as AlmostOneSTLCInExercise, -- flag che mi dice se c'è almeno un stlc1000 in esercizio nella stazione
				(
					SELECT COUNT(DISTINCT servers.SrvID)
					FROM servers
					INNER JOIN devices intDevices ON servers.SrvID = intDevices.SrvID 
					INNER JOIN device_status ON intDevices.DevID = device_status.DevID
					WHERE 
						intDevices.NodID = devices.NodID AND
						servers.InExercise = 0
				) as STLCInMaintMode -- numero di stlc1000 in maintenance mode nella stazione
			FROM devices 
			INNER JOIN servers ON devices.SrvID = servers.SrvID 
			INNER JOIN nodes ON devices.NodID = nodes.NodID
			INNER JOIN zones ON nodes.ZonID = zones.ZonID 
			INNER JOIN regions ON zones.RegID = regions.RegID
			WHERE zones.RegID = @ID -- tutte le periferiche del compartimento
			GROUP BY regions.RegID, regions.Name, zones.ZonID, zones.Name, nodes.NodID, nodes.Name, nodes.Meters, devices.NodID
		),
		DevicesWithStatus
		AS
		(
			SELECT 
				devices.DevID, devices.Type, device_type.SystemID, devices.NodID,
				device_status.SevLevel as Severity, 
				CASE WHEN ([Offline] = 1 OR (DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) >= @MinuteTimeOut AND @MinuteTimeOut <> 0)) THEN 1 ELSE 0 END as [Offline], 
				devices.Active
			FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID 
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN servers ON servers.SrvID = devices.SrvID
			INNER JOIN nodes ON nodes.NodID = devices.NodID
			INNER JOIN zones ON zones.ZonID = nodes.ZonID
			WHERE zones.RegID = @ID -- tutte le periferiche del compartimento
			AND device_type.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			AND servers.InExercise = 1 -- solo se l'STLC1000 non è in maintenance mode
		),
		DeviceSeverityAggregation
		AS
		(
			SELECT DISTINCT
				NodID, SystemID, Severity, [Offline], Active, 
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID, Severity, [Offline], Active) CountBySeverity,
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID) DeviceWithStatusCount
			FROM DevicesWithStatus
		),
		BaseStatus
		AS
		(
			SELECT 
				NodID, SystemID, 
				SUM(CASE WHEN Severity = 0 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Ok,
				SUM(CASE WHEN Severity = 1 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Warning,
				SUM(CASE WHEN Severity = 2 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Error,
				SUM(CASE WHEN Severity = 255 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Unknown,
				SUM(CASE WHEN [Offline] = 1 AND Active = 1 THEN CountBySeverity ELSE 0 END) as [Offline],
				SUM(CASE WHEN Active = 0 THEN CountBySeverity ELSE 0 END) as NotActive,
				DeviceWithStatusCount
			FROM DeviceSeverityAggregation
			GROUP BY NodID, SystemID, DeviceWithStatusCount
		),
		SystemStatus
		AS
		(
			SELECT
				Topography.RegID, Topography.RegName, Topography.ZonID, Topography.ZonName, Topography.NodID, Topography.NodName, Topography.Meters, Topography.AlmostOneSTLCInExercise, Topography.STLCOffline, Topography.STLCInMaintMode, BaseStatus.SystemID,
				ISNULL(Ok, 0) AS Ok, ISNULL(Warning, 0) AS Warning, ISNULL(Error, 0) AS Error, ISNULL(Unknown, 0) AS Unknown, ISNULL([Offline], 0) AS [Offline], ISNULL(NotActive, 0) AS NotActive, ISNULL(DeviceWithStatusCount, 0) AS DeviceWithStatusCount, ISNULL(DevicesTotalCount, 0) AS DevicesTotalCount, 
				CAST((
					-- switch aggiunto per la modifica provvisoria che da priorità allo stato del pannello a zone quando diverso da OK
					CASE 
					WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND Severity <> 0 AND ([Offline] = 0)) -- quelle in warning o errore
					THEN
						(SELECT CASE WHEN MAX(Severity) = 255 THEN 3 ELSE MAX(Severity) END FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3')) -- prendo il PZ con lo stato peggiore
					WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND ([Offline] = 1 )) -- quelle offline
					THEN 3 -- OFFLINE
					ELSE
						-- parte originale
						dbo.GetEntityStateValue(ISNULL(Ok, 0), ISNULL(Warning, 0), ISNULL(Error, 0), ISNULL([Offline], 0),0)
					END
				) AS SMALLINT) as [Status]
			FROM Topography
			LEFT JOIN BaseStatus ON Topography.NodID = BaseStatus.NodID
		),
		NodeStatus
		AS
		(
			SELECT
				RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, AlmostOneSTLCInExercise, STLCOffline, STLCInMaintMode,
				SUM(Ok) as Ok, SUM(Warning) as Warning, SUM(Error) as Error, SUM(Unknown) AS Unknown, SUM([Offline]) as [Offline],  SUM(NotActive) as NotActive, SUM(DeviceWithStatusCount) as DeviceWithStatusCount, DevicesTotalCount,
				dbo.GetEntityStateValue(
										SUM(CASE WHEN ([Status] = 0) THEN 1 ELSE 0 END), --OK
										SUM(CASE WHEN ([Status] = 1) THEN 1 ELSE 0 END), --WARNING
										SUM(CASE WHEN ([Status] = 2) THEN 1 ELSE 0 END), --ERROR
										SUM(CASE WHEN ([Status] = 3) THEN 1 ELSE 0 END)  --OFFLINE
										,0 --MAINTENACE
										) as [Status]	
			FROM SystemStatus
			GROUP BY RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, AlmostOneSTLCInExercise, STLCOffline, STLCInMaintMode, DevicesTotalCount
		)
		SELECT
			RegID, RegName, ZonID, ZonName, 
			CASE WHEN (SUM(CAST(AlmostOneSTLCInExercise AS TINYINT)) > 0) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as AlmostOneSTLCInExercise, SUM(STLCOffline) as STLCOffline, SUM(STLCInMaintMode) as STLCInMaintMode,
			SUM(Ok) as Ok, SUM(Warning) as Warning, SUM(Error) as Error, SUM(Unknown) AS Unknown, SUM([Offline]) as [Offline],  SUM(NotActive) as NotActive, SUM(DeviceWithStatusCount) as DeviceWithStatusCount, SUM(DevicesTotalCount) AS DevicesTotalCount,
			dbo.GetEntityStateValue(
									SUM(CASE WHEN ([Status] = 0) THEN 1 ELSE 0 END), --OK
									SUM(CASE WHEN ([Status] = 1) THEN 1 ELSE 0 END), --WARNING
									SUM(CASE WHEN ([Status] = 2) THEN 1 ELSE 0 END), --ERROR
									SUM(CASE WHEN ([Status] = 3) THEN 1 ELSE 0 END)  --OFFLINE
										,0 --MAINTENACE
									) as [Status]	
		FROM NodeStatus
		GROUP BY RegID, RegName, ZonID, ZonName
		ORDER BY RegName, ZonName;

	END
	ELSE IF ( @AreaType = 2 ) -- Stazioni
	BEGIN

		WITH Topography
		AS
		(
			SELECT
				regions.RegID, regions.Name as RegName, zones.ZonID, zones.Name as ZonName, nodes.NodID, nodes.Name as NodName, nodes.Meters,
				(SELECT COUNT(IntDevs.DevID) FROM devices IntDevs GROUP BY IntDevs.NodID HAVING IntDevs.NodID = devices.NodID) as DevicesTotalCount,
				(
					SELECT COUNT(DISTINCT servers.SrvID)
					FROM servers
					INNER JOIN devices intDevices ON servers.SrvID = intDevices.SrvID 
					INNER JOIN device_status ON intDevices.DevID = device_status.DevID
					WHERE 
						intDevices.NodID = devices.NodID AND
						(DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) > @MinuteTimeOut AND @MinuteTimeOut <> 0) --AND 
						--servers.InExercise = 1
				) as STLCOffline, -- numero di stlc1000 offline nella stazione, un stlc è contato come offline se non comunica da un tempo superiore a quello di timeout, se monitora periferiche con uno stato e se non è in maintenance mode
				CASE WHEN (SUM(CAST(servers.InExercise AS TINYINT)) > 0) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as AlmostOneSTLCInExercise, -- flag che mi dice se c'è almeno un stlc1000 in esercizio nella stazione
				(
					SELECT COUNT(DISTINCT servers.SrvID)
					FROM servers
					INNER JOIN devices intDevices ON servers.SrvID = intDevices.SrvID 
					INNER JOIN device_status ON intDevices.DevID = device_status.DevID
					WHERE 
						intDevices.NodID = devices.NodID AND
						servers.InExercise = 0
				) as STLCInMaintMode -- numero di stlc1000 in maintenance mode nella stazione
			FROM devices 
			INNER JOIN servers ON devices.SrvID = servers.SrvID 
			INNER JOIN nodes ON devices.NodID = nodes.NodID
			INNER JOIN zones ON nodes.ZonID = zones.ZonID 
			INNER JOIN regions ON zones.RegID = regions.RegID
			WHERE nodes.ZonID = @ID -- tutte le periferiche della linea
			GROUP BY regions.RegID, regions.Name, zones.ZonID, zones.Name, nodes.NodID, nodes.Name, nodes.Meters, devices.NodID
		),
		DevicesWithStatus
		AS
		(
			SELECT 
				devices.DevID, devices.Type, device_type.SystemID, devices.NodID,
				device_status.SevLevel as Severity, 
				CASE WHEN ([Offline] = 1 OR (DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) >= @MinuteTimeOut AND @MinuteTimeOut <> 0)) THEN 1 ELSE 0 END as [Offline], 
				devices.Active
			FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID 
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN servers ON servers.SrvID = devices.SrvID
			INNER JOIN nodes ON nodes.NodID = devices.NodID
			WHERE nodes.ZonID = @ID -- tutte le periferiche della linea
			AND device_type.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
			AND servers.InExercise = 1 -- solo se l'STLC1000 non è in maintenance mode
		),
		DeviceSeverityAggregation
		AS
		(
			SELECT DISTINCT
				NodID, SystemID, Severity, [Offline], Active, 
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID, Severity, [Offline], Active) CountBySeverity,
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID) DeviceWithStatusCount
			FROM DevicesWithStatus
		),
		BaseStatus
		AS
		(
			SELECT 
				NodID, SystemID, 
				SUM(CASE WHEN Severity = 0 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Ok,
				SUM(CASE WHEN Severity = 1 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Warning,
				SUM(CASE WHEN Severity = 2 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Error,
				SUM(CASE WHEN Severity = 255 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Unknown,
				SUM(CASE WHEN [Offline] = 1 AND Active = 1 THEN CountBySeverity ELSE 0 END) as [Offline],
				SUM(CASE WHEN Active = 0 THEN CountBySeverity ELSE 0 END) as NotActive,
				DeviceWithStatusCount
			FROM DeviceSeverityAggregation
			GROUP BY NodID, SystemID, DeviceWithStatusCount
		),
		SystemStatus
		AS
		(
			SELECT
				Topography.RegID, Topography.RegName, Topography.ZonID, Topography.ZonName, Topography.NodID, Topography.NodName, Topography.Meters, Topography.AlmostOneSTLCInExercise, Topography.STLCOffline, Topography.STLCInMaintMode, BaseStatus.SystemID,
				ISNULL(Ok, 0) AS Ok, ISNULL(Warning, 0) AS Warning, ISNULL(Error, 0) AS Error, ISNULL(Unknown, 0) AS Unknown, ISNULL([Offline], 0) AS [Offline], ISNULL(NotActive, 0) AS NotActive, ISNULL(DeviceWithStatusCount, 0) AS DeviceWithStatusCount, ISNULL(DevicesTotalCount, 0) AS DevicesTotalCount, 
				CAST((
					-- switch aggiunto per la modifica provvisoria che da priorità allo stato del pannello a zone quando diverso da OK
					CASE 
					WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND Severity <> 0 AND ([Offline] = 0)) -- quelle in warning o errore
					THEN
						(SELECT CASE WHEN MAX(Severity) = 255 THEN 3 ELSE MAX(Severity) END FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3')) -- prendo il PZ con lo stato peggiore
					WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE Topography.NodID = DevicesWithStatus.NodID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND ([Offline] = 1 )) -- quelle offline
					THEN 3 -- OFFLINE
					ELSE
						-- parte originale
						dbo.GetEntityStateValue(ISNULL(Ok, 0), ISNULL(Warning, 0), ISNULL(Error, 0), ISNULL([Offline], 0),0)
					END
				) AS SMALLINT) as [Status]
			FROM Topography
			LEFT JOIN BaseStatus ON Topography.NodID = BaseStatus.NodID
		)
		SELECT
			RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, AlmostOneSTLCInExercise, STLCOffline, STLCInMaintMode,
			SUM(Ok) as Ok, SUM(Warning) as Warning, SUM(Error) as Error, SUM(Unknown) AS Unknown, SUM([Offline]) as [Offline],  SUM(NotActive) as NotActive, SUM(DeviceWithStatusCount) as DeviceWithStatusCount, DevicesTotalCount,
			dbo.GetEntityStateValue(
									SUM(CASE WHEN ([Status] = 0) THEN 1 ELSE 0 END), --OK
									SUM(CASE WHEN ([Status] = 1) THEN 1 ELSE 0 END), --WARNING
									SUM(CASE WHEN ([Status] = 2) THEN 1 ELSE 0 END), --ERROR
									SUM(CASE WHEN ([Status] = 3) THEN 1 ELSE 0 END)  --OFFLINE
										,0 --MAINTENACE
									) as [Status]	
		FROM SystemStatus
		GROUP BY RegID, RegName, ZonID, ZonName, NodID, NodName, Meters, AlmostOneSTLCInExercise, STLCOffline, STLCInMaintMode, DevicesTotalCount
		ORDER BY Meters, RegName, ZonName, NodName;

	END
	ELSE IF ( @AreaType = 3 ) -- Sistemi
	BEGIN

		WITH DevicesWithStatus
		AS
		(
			SELECT 
				devices.DevID, devices.Type, device_type.SystemID, devices.NodID,
				device_status.SevLevel as Severity, 
				CASE WHEN ([Offline] = 1 OR (DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) >= @MinuteTimeOut AND @MinuteTimeOut <> 0)) THEN 1 ELSE 0 END as [Offline], 
				devices.Active,
				(SELECT COUNT(IntDevs.DevID) FROM devices IntDevs GROUP BY IntDevs.NodID HAVING IntDevs.NodID = devices.NodID) as DevicesTotalCount
			FROM devices 
			INNER JOIN device_status ON devices.DevID = device_status.DevID 
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN servers ON servers.SrvID = devices.SrvID
			WHERE devices.NodID = @ID
			AND device_type.SystemID <> 99 -- le periferiche del sistema generico non vengono incluse nel conteggio dello stato
		), 
		DeviceSeverityAggregation
		AS
		(
			SELECT DISTINCT
				NodID, SystemID, Severity, [Offline], Active, 
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID, Severity, [Offline], Active) CountBySeverity,
				COUNT(DevID) OVER (PARTITION BY NodID, SystemID) DeviceWithStatusCount,
				DevicesTotalCount
			FROM DevicesWithStatus
		),
		BaseStatus
		AS
		(
			SELECT 
				NodID, SystemID, 
				SUM(CASE WHEN Severity = 0 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Ok,
				SUM(CASE WHEN Severity = 1 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Warning,
				SUM(CASE WHEN Severity = 2 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Error,
				SUM(CASE WHEN Severity = 255 AND [Offline] = 0 AND Active = 1 THEN CountBySeverity ELSE 0 END) as Unknown,
				SUM(CASE WHEN [Offline] = 1 AND Active = 1 THEN CountBySeverity ELSE 0 END) as [Offline],
				SUM(CASE WHEN Active = 0 THEN CountBySeverity ELSE 0 END) as NotActive,
				DeviceWithStatusCount, DevicesTotalCount
			FROM DeviceSeverityAggregation
			GROUP BY NodID, SystemID, DeviceWithStatusCount, DevicesTotalCount
		)
		SELECT 
			systems.SystemID, SystemDescription, 
			ISNULL(Ok, 0) AS Ok, ISNULL(Warning, 0) AS Warning, ISNULL(Error, 0) AS Error, ISNULL(Unknown, 0) AS Unknown, ISNULL([Offline], 0) AS [Offline], ISNULL(NotActive, 0) AS NotActive, ISNULL(DeviceWithStatusCount, 0) AS DeviceWithStatusCount,
			ISNULL((
					SELECT COUNT(IntDevs.DevID) 
					FROM devices IntDevs 
					INNER JOIN device_type IntDevType ON IntDevs.Type = IntDevType.DeviceTypeID
					WHERE IntDevs.NodID = @ID AND IntDevType.SystemID = systems.SystemID
					GROUP BY IntDevs.NodID, IntDevType.SystemID
			), 0) as DevicesTotalCount,
			CAST((
				-- switch aggiunto per la modifica provvisoria che da priorità allo stato del pannello a zone quando diverso da OK
				CASE 
				WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE systems.SystemID = DevicesWithStatus.SystemID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND Severity <> 0 AND ([Offline] = 0)) -- quelle in warning o errore
				THEN
					(SELECT CASE WHEN MAX(Severity) = 255 THEN 3 ELSE MAX(Severity) END FROM DevicesWithStatus WHERE systems.SystemID = DevicesWithStatus.SystemID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3')) -- prendo il PZ con lo stato peggiore
				WHEN EXISTS (SELECT DevID FROM DevicesWithStatus WHERE systems.SystemID = DevicesWithStatus.SystemID AND ([Type] = 'TT10210' OR [Type] = 'TT10210V3') AND ([Offline] = 1 )) -- quelle offline
				THEN 3 -- OFFLINE
				ELSE
					-- parte originale
					dbo.GetEntityStateValue(ISNULL(Ok, 0), ISNULL(Warning, 0), ISNULL(Error, 0), ISNULL([Offline], 0),0)
				END
			) AS SMALLINT) as [Status]
		FROM systems
		LEFT JOIN BaseStatus ON systems.SystemID = BaseStatus.SystemID
		ORDER BY systems.SystemID;
		
	END

END*/



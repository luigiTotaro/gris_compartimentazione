﻿CREATE PROCEDURE [dbo].[gris_GetOperationSchedulesMail] (@OperationScheduleTypeId SMALLINT) AS
SET NOCOUNT ON;

WITH XMLNAMESPACES(DEFAULT 'http://schemas.microsoft.com/2003/10/Serialization/Arrays')
SELECT
NodeSystemsMail.OperationScheduleId,
'Nodo: ' + nodes.Name + ' - Sistema: ' + systems.SystemDescription AS OperationParameters,
NodeSystemsMail.DateCreated,
NodeSystemsMail.DateExecuted,
NodeSystemsMail.DateAborted
FROM
(
	SELECT
	OperationScheduleId,
	OperationScheduleTypeId,
	CONVERT(XML, OperationParameters).value('(/ArrayOfKeyValueOfstringstring/KeyValueOfstringstring/Value)[1]', 'BIGINT') AS NodeSystemsId,
	DateCreated,
	DateExecuted,
	DateAborted
	FROM operation_schedules
) NodeSystemsMail
INNER JOIN node_systems ON node_systems.NodeSystemsId = NodeSystemsMail.NodeSystemsId
INNER JOIN nodes ON node_systems.NodId = nodes.NodID
INNER JOIN systems ON node_systems.SystemId = systems.SystemID
WHERE (NodeSystemsMail.OperationScheduleTypeId = @OperationScheduleTypeId)
ORDER BY DateCreated DESC
﻿CREATE PROCEDURE [dbo].[util_GetSTLCJoined]
(
	@CertStatus tinyint
	/*
	@CertStatus = 0 -> nessun filtro
	@CertStatus = 1 -> certificato in base alla documentazione
	@CertStatus = 2 -> certificato in base a ispezione UT
	@CertStatus = 3 -> entrambi
	*/
)
AS
	SELECT DISTINCT 
		regions.Name AS RegionName, zones.Name AS ZoneName, nodes.Name AS NodeName, servers.FullHostName, 
		servers.IP, AccountDocValidation, DateDocValidation, AccountUTValidation, DateUTValidation,
		(
			SELECT 
				COUNT(DISTINCT servers.SrvID)
			FROM servers
			INNER JOIN devices ON devices.SrvID = servers.SrvID 
			WHERE 
				((@CertStatus = 0) OR 
				(@CertStatus = 1 AND (DateDocValidation IS NOT NULL)) OR
				(@CertStatus = 2 AND (DateUTValidation IS NOT NULL)) OR
				(@CertStatus = 3 AND (DateDocValidation IS NOT NULL) AND (DateUTValidation IS NOT NULL)))	
		) as STLCCount,
		(
			SELECT COUNT(DISTINCT nodes.NodID)
			FROM servers 
			INNER JOIN devices ON servers.SrvID = devices.SrvID 
			INNER JOIN nodes ON devices.NodID = nodes.NodID 
			INNER JOIN zones ON nodes.ZonID = zones.ZonID 
			INNER JOIN regions ON zones.RegID = regions.RegID
		) as NodeCount
	FROM servers 
	INNER JOIN devices ON servers.SrvID = devices.SrvID 
	INNER JOIN nodes ON devices.NodID = nodes.NodID 
	INNER JOIN zones ON nodes.ZonID = zones.ZonID 
	INNER JOIN regions ON zones.RegID = regions.RegID
	WHERE (@CertStatus = 0) OR 
	(@CertStatus = 1 AND (DateDocValidation IS NOT NULL)) OR
	(@CertStatus = 2 AND (DateUTValidation IS NOT NULL)) OR
	(@CertStatus = 3 AND (DateDocValidation IS NOT NULL) AND (DateUTValidation IS NOT NULL));



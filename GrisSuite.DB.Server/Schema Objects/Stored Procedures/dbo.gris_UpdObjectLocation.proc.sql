﻿CREATE PROCEDURE [dbo].[gris_UpdObjectLocation]
	@XLocation FLOAT, 
	@YLocation FLOAT,
	@NodID BIGINT
AS
	DECLARE @ObjectStatusId AS UNIQUEIDENTIFIER;
	DECLARE @LocatedObjectStatusId AS UNIQUEIDENTIFIER;

	SELECT @LocatedObjectStatusId = object_locations.ObjectStatusId, @ObjectStatusId = object_nodes.ObjectStatusId
	FROM object_locations 
	RIGHT JOIN object_nodes ON object_locations.ObjectStatusId = object_nodes.ObjectStatusId
	WHERE (object_nodes.ObjectId = @NodID);

	IF ( @ObjectStatusId IS NOT NULL )
	BEGIN
		IF ( @LocatedObjectStatusId IS NULL )
		BEGIN
			INSERT INTO [object_locations]
					   ([ObjectStatusId]
					   ,[ObjectLocationAreaId]
					   ,[XLocation]
					   ,[YLocation])
				 VALUES
					   (@ObjectStatusId
					   ,1
					   ,@XLocation
					   ,@YLocation)
		END
		ELSE
		BEGIN
			UPDATE object_locations SET XLocation = @XLocation, YLocation = @YLocation
			FROM object_locations 
			WHERE (object_locations.ObjectStatusId = @ObjectStatusId)
		END
	END
-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 22/10/2008
-- Description:	Crea i devices di tipo STLC1000
-- =============================================
CREATE PROCEDURE [dbo].[util_InsDecivesSTLC1000] (@CreaNuoviSTLC BIT = 0)
AS
BEGIN
    DECLARE	@DateAlertLimit datetime
	SET @DateAlertLimit = DATEADD(minute,-60, GETDATE());

	IF @CreaNuoviSTLC = 1
	BEGIN
		-- cancella i device di tipo STLC1000 che hanno nodid diverso dal servers
		delete from devices
		from servers s inner join devices d on s.srvid = d.srvid
		where s.nodid <> d.nodid and [type] = 'STLC1000';

		-- inserisce le periferiche STLC1000 mancanti
		WITH 
		DevicesSTLC as 
		(
		SELECT * FROM devices WHERE devices.[Type] = 'STLC1000'
		),
		ServerNoDeviceSTLC as 
		(
		SELECT servers.SrvID, servers.NodID 
		FROM servers LEFT JOIN DevicesSTLC ON servers.SrvID = DevicesSTLC.SrvID 
		WHERE not (servers.NodID  is null) and (DevicesSTLC.devid is null)
		),
		ServersDevices as 
		( 
		SELECT SrvID, NodID, 
				dbo.SetDecodedDevId(dbo.GetDecodedRegId(NodID),dbo.GetDecodedZonId(NodID),dbo.GetDecodedNodId(NodID),999 + ROW_NUMBER() OVER(PARTITION BY NodID ORDER BY SrvID DESC)) AS STLCDevID
		from ServerNoDeviceSTLC
		)
		INSERT INTO [devices]
				   ([DevID]
				   ,[NodID]
				   ,[SrvID]
				   ,[Name]
				   ,[Type]
				   ,[SN]
				   ,[Addr]
				   ,[PortId]
				   ,[ProfileID]
				   ,[Active]
				   ,[Scheduled]
				   ,[RackID]
				   ,[RackPositionRow]
				   ,[RackPositionCol]
				   ,[DefinitionVersion]
				   ,[ProtocolDefinitionVersion])
		SELECT  ServersDevices.STLCDevID
				,ServersDevices.NodID 		
				,ServersDevices.SrvID
				,'STLC1000' as [Name]
				,'STLC1000' as [Type]
				,'00000000' as [SN]
				,0 as [Addr]
				,NULL as [PortId]
				,1 as [ProfileID]
				,1 as [Active]
				,1 as [Scheduled]
				,NULL as [RackID]
				,1 as [RackPositionRow]
				,1 as [RackPositionCol]
				,NULL as [DefinitionVersion]
				,NULL as [ProtocolDefinitionVersion]
		  FROM ServersDevices 
		  WHERE STLCDevID NOT IN (SELECT DISTINCT DevID FROM [devices])
	
		-- inserisce gli status delle periferiche STLC1000 appena inserite
		INSERT INTO [device_status]
				   ([DevID]
				   ,[SevLevel]
				   ,[Description]
				   ,[Offline])
		SELECT devices.DevID, 
				0 AS [SevLevel], 
				'' AS [Description], 
				(CASE WHEN servers.LastUpdate <= @DateAlertLimit THEN 1 ELSE 0 END)  AS [Offline]  
			FROM devices 
			INNER JOIN servers ON devices.SrvID = servers.SrvID
			LEFT JOIN device_status ON devices.DevID = device_status.DevID
			WHERE TYPE = 'STLC1000' 
				AND device_status.DevID IS NULL
	END

	-- aggiorna gli offline delle periferiche STLC1000
	UPDATE device_status
	SET [Offline] = CASE WHEN servers.LastUpdate <= @DateAlertLimit THEN 1 ELSE 0 END 
	FROM devices 
	INNER JOIN servers ON devices.SrvID = servers.SrvID
	LEFT JOIN device_status ON devices.DevID = device_status.DevID
	WHERE devices.type = 'STLC1000'

END






CREATE PROCEDURE [dbo].[gris_GetMapLayersByClassification]
(@ClassificationId tinyint)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT map_layers.MapLayerId
			, map_layers.ClassificationValueId
			, map_layers.ShapefileUri
			, map_layers.VisibleFromScale
			, map_layers.VisibleToScale
			, map_layers.IsSensitive
			, map_layers.ShowElementsSymbols
			, map_layers.ShowElementsTooltips
			, map_layers.SymbolSize
			, classification_values.ValueName AS GroupName
			, classification_values.ValueSequence AS GroupSequence
			, map_layers.LayerSequence
			, map_layers.MapLayerTypeId
			, map_layers.ShowElementsStatus
	FROM         map_layers INNER JOIN
						  classification_values ON map_layers.ClassificationValueId = classification_values.ClassificationValueId
	WHERE ClassificationId = @ClassificationId                      
	ORDER BY GroupSequence, map_layers.LayerSequence

END
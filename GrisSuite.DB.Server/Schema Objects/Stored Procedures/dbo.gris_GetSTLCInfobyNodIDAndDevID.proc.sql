﻿CREATE PROCEDURE [dbo].[gris_GetSTLCInfobyNodIDAndDevID]
(
	@NodID bigint, 
	@DevID bigint
)
AS
BEGIN
	SET NOCOUNT ON;

	WITH serverOfDevices
	AS
	(
		-- Recupera i server legati alle device di quel nodo (comprese con le remotizzate)
		-- Revisione per issue 11294 "GRIS - non è possibile cancellare da interfaccia la barra STLC (IP duplicato)"
		SELECT DISTINCT devices.SrvId 
		FROM devices
		INNER JOIN servers ON devices.SrvID = servers.SrvID
		WHERE(devices.NodID = @NodID)
		AND (devices.DevID = @DevID OR @DevID = -1)
		AND (servers.IsDeleted <> 1)
		
		UNION
		
		-- Recupera il server legato al nodo, nel caso non sia indicata la device
		-- Questo caso subentra quando l'STLC è rimasto censito e non cancellato, ma non contiene più nessuna device associata,
		-- su interfaccia sarà quindi visibile ed eliminabile da base dati con apposito bottone (caso tipico di sostituzione STLC)
		-- Vedi issue 11143 "Sostituzione STLC"
		SELECT SrvID
		FROM servers
		WHERE (@DevID = -1)
		AND (NodID = @NodID)
		AND (IsDeleted <> 1)
	)
	SELECT DISTINCT ISNULL(servers_status.SrvID, -1) AS SrvID
		, ISNULL(servers_status.Name, '') AS Name
		, ISNULL(servers_status.Host, '') AS Host
		, ISNULL(servers_status.FullHostName, '') AS FullHostName
		, ISNULL(servers_status.IP, '') AS IP
		, servers_status.LastUpdate
		, ISNULL(servers_status.LastMessageType, '') AS LastMessageType
		, ISNULL(servers_status.NodID, -1) AS NodID
		, servers_status.InMaintenance
		, servers_status.SevLevel
		, ISNULL(servers_status.SevLevelDetailId, 3) AS SevLevelDetailId
		, isnull(servers_status.SevLevelReal, 3) AS SevLevelReal
		, ISNULL(severity.Description, '') AS SevLevelDesc
		, ISNULL(severity_details.Description,'') AS SevLevelDetailDesc
		, dbo.GetRegIDFromNodID(servers_status.NodID) AS RegID
	FROM servers_status 
		INNER JOIN serverOfDevices ON servers_status.SrvId = serverOfDevices.SrvId
		LEFT JOIN severity ON servers_status.SevLevel = severity.SevLevel
		LEFT JOIN severity_details ON servers_status.SevLevelDetailId = severity_details.SevLevelDetailId;
END



﻿CREATE PROCEDURE [dbo].[gris_Report_GetDevicesByNodeAndSystem_DevStatus]
@RegID BIGINT, @ZonID BIGINT, @NodID BIGINT, @SystemID INT, @DeviceStatus VARCHAR(1024) = '', @DeviceType VARCHAR(8000) = '', @ServerStatus VARCHAR(1024) = '', @ServerInMaintenance INT = -1
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		regions.RegID, regions.Name as RegionName, 
		zones.ZonID, zones.Name as ZoneName, 
		nodes.NodID, nodes.Name as NodeName, 
		systems.SystemDescription as SystemName, 
		devices.DevID, devices.Name as DeviceName, dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) as DeviceType, object_devices.SevLevel, object_devices.SevLevelDescription as DeviceStatus, 
		ss.SevLevel as ServerSevLevel, ss.SevLevelReal as ServerSevLevelReal,
		ISNULL(station.StationName, '') as StationName, ISNULL(building.BuildingName, '') as BuildingName, ISNULL(rack.RackName, '') as RackName,
		(
			CASE WHEN CAST(SUBSTRING(dbo.GetIPStringFromInt(devices.Addr), 0,CHARINDEX('.', dbo.GetIPStringFromInt(devices.Addr))) AS TINYINT) > 0 
			THEN dbo.GetIPStringFromInt(devices.Addr)
			ELSE ''
			END
		) AS IP,
		dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 7 /* Decodifica categoria Infostazioni */, devices.DevID) as DeviceCategory,
		dbo.GetCustomSeverityDescription (object_devices.SevLevel, object_devices.SevLevelDetailId, (SELECT RealSeverity.Description FROM severity_details RealSeverity WHERE RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal)) AS SevLevelDescription,
		servers.LastUpdate AS STLC1000LastUpdate,
		CASE WHEN devices.Type LIKE 'STLC1000' THEN dbo.GetSNFromSrvID(devices.SrvID) ELSE ISNULL(devices.SN, '') END AS SN
	FROM devices
		INNER JOIN device_type ON device_type.DeviceTypeID = devices.Type
		INNER JOIN systems ON systems.SystemID = device_type.SystemID
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		INNER JOIN servers ON servers.SrvID = devices.SrvId
		INNER JOIN object_servers_inservice ss ON servers.SrvID = ss.ObjectId
		INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
		LEFT JOIN rack ON devices.RackID = rack.RackID 
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID
	WHERE ((zones.RegID = @RegID) OR (@RegID = -1))
		AND ((nodes.ZonID = @ZonID) OR (@ZonID = -1))
		AND ((devices.NodID = @NodID) OR (@NodID = -1))
		AND ((device_type.SystemID = @SystemID) OR (@SystemID = -1))
		AND (
			(@DeviceType = '') -- prende tutto
			OR (CHARINDEX('|-2|', @DeviceType) > 0) -- prende tutto
			OR (CHARINDEX('|' + CONVERT(NVARCHAR(16), dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID)) + '|', @DeviceType) > 0)
		)		
		AND (devices.Name NOT LIKE 'NoDevice')
		AND (
			(@DeviceStatus = '') -- prende tutto
			OR (CHARINDEX('|-2|', @DeviceStatus) > 0) -- prende tutto
			OR (CHARINDEX('|' + CONVERT(NVARCHAR(10), object_devices.SevLevel) + '|', @DeviceStatus) > 0)
		)
		AND (
			(@ServerStatus = '') -- prende tutto
			OR (CHARINDEX('|-2|', @ServerStatus) > 0) -- prende tutto
			OR (CHARINDEX('|' + CONVERT(NVARCHAR(10), ss.SevLevelReal) + '|', @ServerStatus) > 0)
		)
		AND (@ServerInMaintenance = -1 OR (@ServerInMaintenance = 0 AND ss.SevLevel <> 9) OR (@ServerInMaintenance = 1 AND ss.SevLevel = 9))
	ORDER BY RegionName, ZoneName, NodeName, SystemName,DeviceType, DeviceName;
END
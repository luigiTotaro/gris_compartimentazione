﻿CREATE PROCEDURE [dbo].[gris_GetZonesStatusByNode]
@ObjectStatusId UNIQUEIDENTIFIER
AS
DECLARE @ZonId BIGINT

-- identifica ZonId
IF (@ObjectStatusId is null)
	SET @ZonId = 0;

IF (@ZonId is null)
	SET @ZonId = (select ObjectId from object_zones where ObjectStatusId = @ObjectStatusId);

IF (@ZonId is null)
	SET @ZonId =  (select object_zones.ObjectId from object_nodes
					inner join object_zones on object_nodes.ParentObjectStatusId = object_zones.ObjectStatusId
					where object_nodes.ObjectStatusId = @ObjectStatusId);

WITH  ZonCounts AS 
	(
		SELECT RegID
			, RegName
			, ZonID
			, ZonName
			, [0] as STLCOK
			, [3] as STLCOffline
			, [9] as STLCInMaintenance
			, [-1] as STLCNotClassified
			, [0] + [3] + [9] +[-1] AS STLCTotalCount
			, 0 as Ok
			, 0 as Warning
			, 0 as Error
			, 0 as Unknown
			, 0 as [Offline]
			, 0 as InMaintenance
			, 0 as NotClassified
			, 0 as TotalCount
		FROM 
		(
			SELECT regions.RegID
				, regions.Name AS RegName
				, zones.ZonID
				, zones.Name as ZonName
				, servers_status.SevLevel
				, SrvId
			FROM servers_status 
				INNER JOIN nodes	ON servers_status.NodID = nodes.NodID 
				INNER JOIN zones	ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions	ON zones.RegID = regions.RegID
			WHERE @ZonID = 0 OR zones.ZonID = @ZonID
		) as A
		PIVOT
		(
		 COUNT(SrvId)
		 for SevLevel
		 in ([0], [3], [9], [-1])
		) as p
		
		UNION ALL
		
		SELECT RegID
			, RegName
			, ZonID
			, ZonName
			, 0 as STLCOK
			, 0 as STLCOffline
			, 0 as STLCInMaintenance
			, 0 as STLCNotClassified
			, 0 as STLCTotalCount
			, [0] as Ok
			, [1] as Warning
			, [2] as Error
			, [255] as Unknown
			, [3] as [Offline]
			, [9] as InMaintenance
			, [-1] NotClassified
			, [0] + [1] + [2] + [3] + [255] + [9] + [-1] AS TotalCount
		FROM 
		(
			SELECT DISTINCT regions.RegID
				, regions.Name AS RegName
				, zones.ZonID
				, zones.Name as ZonName
				, object_nodes_withDevice.SevLevel
				, object_nodes_withDevice.ObjectID
			FROM nodes 
				INNER JOIN zones			ON nodes.ZonID = zones.ZonID 
				INNER JOIN regions			ON zones.RegID = regions.RegID 
				INNER JOIN object_nodes_withDevice	ON nodes.NodID = object_nodes_withDevice.ObjectId
				INNER JOIN devices			ON nodes.NodID = devices.NodID 
				INNER JOIN object_devices	ON devices.DevID = object_devices.ObjectId
			WHERE (@ZonID = 0 OR zones.ZonID = @ZonID)
				AND (object_devices.ExcludeFromParentStatus = 0)			
		) as A
		PIVOT
		(
		 COUNT(ObjectID)
		 for SevLevel
		 in ([0], [1], [2], [3], [255], [9], [-1])
		) as p
	)
	SELECT RegID, RegName, ZonID, ZonName, 
		SUM(STLCOK) as STLCOK,
		SUM(STLCOffline) as STLCOffline, 
		SUM(STLCInMaintenance) as STLCInMaintenance, 
		SUM(STLCNotClassified) as STLCNotClassified, 
		SUM(STLCTotalCount) as STLCTotalCount, 
		SUM(ZonCounts.Ok) as Ok, 
		SUM(ZonCounts.Warning) as Warning,
		SUM(ZonCounts.Error) + SUM(ZonCounts.Unknown) + SUM([Offline]) as Error, 
		0 as Unknown, 
		0 as [Offline], 
		0 as InMaintenance, -- SUM(ZonCounts.InMaintenance)
	    0 as NotClassified, -- SUM(NotClassified)
		SUM(ZonCounts.Ok) + SUM(ZonCounts.Warning) + SUM(ZonCounts.Error) + SUM(ZonCounts.Unknown) + SUM([Offline]) as TotalCount, -- SUM(TotalCount)
		object_regions.SevLevel as [Status], 
		object_regions.SevLevelDescription as [StatusDescription],
		dbo.GetSeverityLevelForObject (RegID, NULL, NULL) AS SevLevelRegion,
		dbo.GetSeverityLevelForObject (NULL, ZonID, NULL) AS SevLevelZone,
		dbo.GetAttributeForObject(8 /* Color */, NULL, RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
		dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor
	FROM ZonCounts
		INNER JOIN object_regions ON ZonCounts.RegId = object_regions.ObjectId
	GROUP BY RegID, RegName, ZonID, ZonName, object_regions.SevLevel, object_regions.SevLevelDescription
	ORDER BY RegName, ZonName;
RETURN
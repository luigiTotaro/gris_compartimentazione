﻿CREATE PROCEDURE gris_GetServersLicensesByStatus (@SrvID bigint, @IP varchar(16), @HardwareProfile varchar(64), @IsLicensed tinyint) AS
SET NOCOUNT ON;

SELECT
ServerLicenseID,
SrvID,
IP,
LicenseLastUpdate,
LicenseLastUpdateUser,
IsLicensed,
HardwareProfile,
dbo.GetSNFromSrvID(SrvID) AS SN
FROM servers_licenses
WHERE (IsLicensed = @IsLicensed)
AND ((@SrvID < 0) OR (SrvID = @SrvID))
AND (IP LIKE @IP)
AND (HardwareProfile LIKE @HardwareProfile)
ORDER BY SrvID
﻿CREATE PROCEDURE [dbo].[util_CheckStation] (@StationName VARCHAR(64))
AS
SELECT  
		nodes.NodID, 
		'LO' + RIGHT('0000' + CAST(dbo.GetDecodedNodId(nodes.nodid) AS VARCHAR(MAX)),4) as DecodedNodId ,
		nodes.Name, 
		object_map_points.MapPoints, 
		classification_values.ValueName, 
		object_nodes.SevLevel, 
		object_nodes.ObjectStatusId, 
		object_classification_values.ClassificationValueId, 
		object_classification_values.MapLayerId
FROM         classification_values INNER JOIN
                      object_classification_values ON classification_values.ClassificationValueId = object_classification_values.ClassificationValueId RIGHT OUTER JOIN
                      object_map_points RIGHT OUTER JOIN
                      object_nodes ON object_map_points.ObjectStatusId = object_nodes.ObjectStatusId ON 
                      object_classification_values.ObjectStatusId = object_nodes.ObjectStatusId RIGHT OUTER JOIN
                      nodes ON object_nodes.ObjectId = nodes.NodID
WHERE     (nodes.Name LIKE @StationName)


select * from nodesgis where name like @StationName

return
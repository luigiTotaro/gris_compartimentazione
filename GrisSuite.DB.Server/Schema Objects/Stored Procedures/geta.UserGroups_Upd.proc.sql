﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[UserGroups_Upd] 
	@UserGroupID int,
	@UserGroupName varchar(256)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE user_groups SET UserGroupName = @UserGroupName WHERE (UserGroupID = @UserGroupID);
END



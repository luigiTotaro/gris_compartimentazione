﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 26-03-2008
-- Description:	
-- =============================================
CREATE PROCEDURE util_GetNodesNoMonitors 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT regions.Name as RegName, zones.Name as ZoneName, nodes.Name as NodeName, servers.IP as IP_STLC
	FROM devices 
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN servers ON servers.SrvID = devices.SrvID
	WHERE nodes.NodID NOT IN 
	(
		SELECT DISTINCT devices.NodID
		FROM devices 
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		WHERE device_type.SystemID = 2
	);
END



﻿CREATE PROCEDURE dbo.tf_UpdSTLCParameters
(
	@SrvID int,
	@ParameterValue varchar(256),
	@ParameterName varchar(64),
	@ParameterDescription varchar(1024),
	@Original_SrvID int,
	@Original_ParameterName varchar(64)
)
AS
	SET NOCOUNT OFF;
	UPDATE [stlc_parameters] SET [SrvID] = @SrvID, [ParameterValue] = @ParameterValue, [ParameterName] = @ParameterName, [ParameterDescription] = @ParameterDescription WHERE (([SrvID] = @Original_SrvID) AND ([ParameterName] = @Original_ParameterName));



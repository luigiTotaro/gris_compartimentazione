﻿CREATE PROCEDURE [dbo].[gris_DelContact] (@ContactId BIGINT) AS
SET NOCOUNT ON;

IF (dbo.IsContactIdAssociatedToNotificationContacts(@ContactId) = 0)
BEGIN
	DELETE FROM contacts
	WHERE (ContactId = @ContactId);
END
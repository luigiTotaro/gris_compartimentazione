-- =============================================
-- Author:		Cristian Storti
-- Create date: 13-10-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[gris_GetDeviceAndViolatedRules]
@AlertTicketID UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		(regions.Name + ' -> ' + zones.Name + ' -> ' + nodes.Name) as Location, zones.RegID, nodes.Name as NodeName, VendorName, 
		devices.type as DeviceType, devices.Name as DeviceName, device_type.ImageName, object_devices.SevLevelReal as Severity, object_devices.SevLevelDetailId as SeverityDetail,
		(CASE WHEN (SELECT SevLevel FROM object_servers_inservice WHERE object_servers_inservice.ObjectId = devices.SrvID) = 3 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END) as STLCOffline,
		alerts_tickets.TicketName, alerts_tickets.TicketNote, alerts_tickets.TicketDate, 
		alerts_devices_logs.IsOpen as IsAlertLogOpen,
		alerts_rules.RuleDescription, alerts_devices_logs.CreationDate as AlertCreationDate,
		RuleSevLevel
	FROM alerts_tickets 
	INNER JOIN alerts_devices_logs ON alerts_tickets.DevID = alerts_devices_logs.DevID 
		AND alerts_devices_logs.CreationDate >= alerts_tickets.CreationDate
		AND alerts_devices_logs.CreationDate <= ISNULL(alerts_tickets.CloseDate, '99991231')
	INNER JOIN alerts_rules ON alerts_rules.RuleID = alerts_devices_logs.RuleID 
	INNER JOIN devices ON devices.DevID = alerts_tickets.DevID
	LEFT JOIN object_devices ON devices.DevID = object_devices.ObjectID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN device_type ON devices.type = device_type.DeviceTypeID
	INNER JOIN servers ON servers.SrvID = devices.SrvID
	INNER JOIN vendors ON vendors.VendorID = device_type.VendorID
	WHERE (alerts_tickets.AlertTicketID = @AlertTicketID)
	ORDER BY IsAlertLogOpen DESC, AlertCreationDate;

END



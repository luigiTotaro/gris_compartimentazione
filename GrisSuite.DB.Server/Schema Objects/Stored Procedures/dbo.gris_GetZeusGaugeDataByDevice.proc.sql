﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[gris_GetZeusGaugeDataByDevice] 
	@DevID BIGINT,
	@DeviceType VARCHAR(16)
AS
BEGIN
	WITH Modem1 AS
	(
		SELECT sf.Value as FieldValue, SUBSTRING(sf.Description, 0, CHARINDEX('=', sf.Description)) as FieldDescription, sf.FieldID
		FROM device_status ds
		INNER JOIN streams s ON ds.DevID = s.DevID 
		INNER JOIN stream_fields sf ON s.DevID = sf.DevID AND s.StrID = sf.StrID
		WHERE (ds.devid = @DevID)
		AND s.StrID = 3
		AND sf.FieldID IN (4, 5, 6)
	), Modem2 AS
	(
		SELECT sf.Value as FieldValue, SUBSTRING(sf.Description, 0, CHARINDEX('=', sf.Description)) as FieldDescription, sf.FieldID
		FROM device_status ds
		INNER JOIN streams s ON ds.DevID = s.DevID 
		INNER JOIN stream_fields sf ON s.DevID = sf.DevID AND s.StrID = sf.StrID
		WHERE (ds.devid = @DevID)
		AND s.StrID = 3
		AND sf.FieldID IN (12, 13, 14)
	), Modem3 AS
	(
		SELECT sf.Value as FieldValue, SUBSTRING(sf.Description, 0, CHARINDEX('=', sf.Description)) as FieldDescription, sf.FieldID
		FROM device_status ds
		INNER JOIN streams s ON ds.DevID = s.DevID 
		INNER JOIN stream_fields sf ON s.DevID = sf.DevID AND s.StrID = sf.StrID
		WHERE (ds.devid = @DevID)
		AND s.StrID = 3
		AND sf.FieldID IN (20, 21, 22)
	), Modem4 AS
	(
		SELECT sf.Value as FieldValue, SUBSTRING(sf.Description, 0, CHARINDEX('=', sf.Description)) as FieldDescription, sf.FieldID
		FROM device_status ds
		INNER JOIN streams s ON ds.DevID = s.DevID 
		INNER JOIN stream_fields sf ON s.DevID = sf.DevID AND s.StrID = sf.StrID
		WHERE (ds.devid = @DevID)
		AND s.StrID = 3
		AND sf.FieldID IN (28, 29, 30)
	)
	SELECT TOP 1 'Modem #1' as Name
		, (
			SELECT '' AS FieldDescription -- Descrizione Velocità minima loop modem (scelta come descrizione rappresentativa per il modem selezionato)
		) as [Description]
		, (
			SELECT '0 Kb/s' AS FieldDescription -- Velocità minima loop modem
		) as InferiorLimitValue
		, (
			SELECT FieldDescription FROM Modem1
			WHERE FieldID = 5 -- Velocità massima loop modem
		) as ReferenceValue
		, (
			SELECT FieldDescription FROM Modem1
			WHERE FieldID = 6 -- Velocità loop modem
		) as ActualValue
		, (
			SELECT FieldDescription FROM Modem1
			WHERE FieldID = 4 -- Soglia attenzione velocità loop modem
		) as WarningValue
		, (
			SELECT FieldDescription FROM Modem1
			WHERE FieldID = 4 -- Soglia errore velocità loop modem
		) as ErrorValue
	FROM Modem1
	WHERE FieldID IS NOT NULL
	UNION ALL
	SELECT TOP 1 'Modem #2' as Name
		, (
			SELECT '' AS FieldDescription -- Descrizione Velocità minima loop modem (scelta come descrizione rappresentativa per il modem selezionato)
		) as [Description]
		, (
			SELECT '0 Kb/s' AS FieldDescription -- Velocità minima loop modem
		) as InferiorLimitValue
		, (
			SELECT FieldDescription FROM Modem2
			WHERE FieldID = 13 -- Velocità massima loop modem
		) as ReferenceValue
		, (
			SELECT FieldDescription FROM Modem2
			WHERE FieldID = 14 -- Velocità loop modem
		) as ActualValue
		, (
			SELECT FieldDescription FROM Modem2
			WHERE FieldID = 12 -- Soglia attenzione velocità loop modem
		) as WarningValue
		, (
			SELECT FieldDescription FROM Modem2
			WHERE FieldID = 12 -- Soglia errore velocità loop modem
		) as ErrorValue
	FROM Modem2
	WHERE FieldID IS NOT NULL
	UNION ALL
	SELECT TOP 1 'Modem #3' as Name
		, (
			SELECT '' AS FieldDescription -- Descrizione Velocità minima loop modem (scelta come descrizione rappresentativa per il modem selezionato)
		) as [Description]
		, (
			SELECT '0 Kb/s' AS FieldDescription -- Velocità minima loop modem
		) as InferiorLimitValue
		, (
			SELECT FieldDescription FROM Modem3
			WHERE FieldID = 21 -- Velocità massima loop modem
		) as ReferenceValue
		, (
			SELECT FieldDescription FROM Modem3
			WHERE FieldID = 22 -- Velocità loop modem
		) as ActualValue
		, (
			SELECT FieldDescription FROM Modem3
			WHERE FieldID = 20 -- Soglia attenzione velocità loop modem
		) as WarningValue
		, (
			SELECT FieldDescription FROM Modem3
			WHERE FieldID = 20 -- Soglia errore velocità loop modem
		) as ErrorValue
	FROM Modem3
	WHERE FieldID IS NOT NULL
	UNION ALL
	SELECT TOP 1 'Modem #4' as Name
		, (
			SELECT '' AS FieldDescription -- Descrizione Velocità minima loop modem (scelta come descrizione rappresentativa per il modem selezionato)
		) as [Description]
		, (
			SELECT '0 Kb/s' AS FieldDescription -- Velocità minima loop modem
		) as InferiorLimitValue
		, (
			SELECT FieldDescription FROM Modem4
			WHERE FieldID = 29 -- Velocità massima loop modem
		) as ReferenceValue
		, (
			SELECT FieldDescription FROM Modem4
			WHERE FieldID = 30 -- Velocità loop modem
		) as ActualValue
		, (
			SELECT FieldDescription FROM Modem4
			WHERE FieldID = 28 -- Soglia attenzione velocità loop modem
		) as WarningValue
		, (
			SELECT FieldDescription FROM Modem4
			WHERE FieldID = 28 -- Soglia errore velocità loop modem
		) as ErrorValue
	FROM Modem4
	WHERE FieldID IS NOT NULL;
END
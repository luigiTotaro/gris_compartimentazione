﻿CREATE procedure [dbo].[gris_GetVirtualObjectDevices]
(
	@VirtualObjectID bigint
) as
begin
	
	select 
		d.DevID, d.Name, d.[Type], d.Addr, isnull(p.PortName, '') as PortName, isnull(p.PortType, '') as PortType
	from virtual_objects vo
	inner join object_virtuals ov on vo.VirtualObjectID = ov.ObjectId
	inner join object_devices od on od.ParentObjectStatusId = ov.ObjectStatusId
	inner join devices d on d.DevID = od.ObjectId
	left join port p on d.PortId = p.PortID
	where vo.VirtualObjectID = @VirtualObjectID
	order by d.Name;
end
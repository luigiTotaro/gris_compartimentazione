﻿CREATE PROCEDURE [dbo].[gris_GetDevicesByNodID_DevState]
	@NodID bigint,
	@DeviceStatus tinyint = 0
AS
BEGIN
	/******************** OBSOLETA **********************/
	SET NOCOUNT ON;

	IF @DeviceStatus = 3
		BEGIN
			SELECT COUNT(ds1.Offline) AS NumDevices
			FROM devices AS d1 
			INNER JOIN device_status AS ds1 ON d1.DevID = ds1.DevID
			WHERE (ds1.Offline = 1)AND (d1.NodID = @NodID)
		END
	ELSE
		BEGIN
			WITH NodDevices(StationName, BuildingName, RackID, RackName, DevID, DevName, DevType, MaxSevLevel)
			AS
			(
				SELECT station.StationName, building.BuildingName, devices.RackID, rack.RackName, devices.DevID, devices.Name, devices.Type, MAX(stream_fields.SevLevel) AS MaxSevLevel
				FROM devices 
				INNER JOIN device_status ON devices.DevID = device_status.DevID 
				INNER JOIN stream_fields ON devices.DevID = stream_fields.DevID 
				INNER JOIN rack ON devices.RackID = rack.RackID 
				INNER JOIN building ON rack.BuildingID = building.BuildingID 
				INNER JOIN station ON building.StationID = station.StationID
				WHERE (devices.NodID = @NodID) 
				AND (device_status.Offline = 0) 
				AND (stream_fields.Visible = 1) 
				AND (stream_fields.SevLevel <> 255)
				GROUP BY station.StationName, building.BuildingName, devices.RackID, rack.RackName, devices.DevID, devices.Name, devices.Type
			)
			SELECT StationName, BuildingName, RackID, RackName, DevID, DevName, DevType
			FROM NodDevices
			WHERE (MaxSevLevel = @DeviceStatus);
		END
END



﻿CREATE PROCEDURE tf_UpdCachePreProcess 
	@Now datetime, 
	@CacheDays int
AS
BEGIN
      DECLARE @CacheMinutes int
      SET  @CacheMinutes = @CacheDays * 1440
      DELETE FROM cache_log_messages WHERE DATEDIFF(minute, Created, @Now) > @CacheMinutes
      RETURN @@ROWCOUNT 
END

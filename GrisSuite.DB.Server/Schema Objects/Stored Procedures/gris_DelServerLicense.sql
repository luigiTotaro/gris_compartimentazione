﻿CREATE PROCEDURE [dbo].[gris_DelServerLicense] (@ServerLicenseID int, @IsLicensed tinyint, @LicenseLastUpdateUser varchar(100)) AS
SET NOCOUNT ON;

DELETE FROM servers_licenses
WHERE (ServerLicenseID = @ServerLicenseID)
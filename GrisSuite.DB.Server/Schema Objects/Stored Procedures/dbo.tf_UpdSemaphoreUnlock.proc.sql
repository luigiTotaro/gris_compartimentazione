CREATE PROCEDURE [dbo].[tf_UpdSemaphoreUnlock]
@SemaphoreName VARCHAR (64), @HostName VARCHAR (64) = NULL
AS
UPDATE semaphores SET Locked = 0, HostName = @HostName, LastUpdate = GETDATE() WHERE SemaphoreName = @semaphoreName 
		
RETURN @@ROWCOUNT
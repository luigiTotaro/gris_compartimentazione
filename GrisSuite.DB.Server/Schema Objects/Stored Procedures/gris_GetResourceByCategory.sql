﻿CREATE PROCEDURE [dbo].[gris_GetResourceByCategory] (@ResourceTypeID SMALLINT,
                                                    @ResourceCategoryID INT,
                                                    @ResourceID     INT = -1) AS
SET NOCOUNT ON;

SELECT DISTINCT
  r.ResourceID,
  r.ResourceCode,
  r.ResourceDescription,
  dbo.GetPermissionGroupsByResource(r.ResourceID) AS WindowsGroups,
  c.ResourceCategoryID,
  c.ResourceCategoryCode,
  c.ResourceCategoryDescription,
  r.SortOrder,
  ISNULL(g.GroupName,'') as GroupName
FROM resources r
  INNER JOIN resource_categories c ON r.ResourceCategoryID = c.ResourceCategoryID
  LEFT JOIN permissions p ON r.ResourceID = p.ResourceID
  LEFT JOIN permission_groups g ON g.GroupID = p.GroupID
WHERE (r.ResourceTypeID = @ResourceTypeID)
      AND ((@ResourceCategoryID = -1) OR (c.ResourceCategoryID = @ResourceCategoryID))
      AND ((@ResourceID = -1) OR (r.ResourceID = @ResourceID))
ORDER BY r.SortOrder;
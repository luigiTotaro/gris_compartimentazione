﻿CREATE PROCEDURE util_GetSTLCWithoutDevSTLC
AS		

		SELECT s.SrvID, s.MAC, s.IP, n1.Name as Stazione, z1.Name as Linea, r1.Name as Compartimento
		FROM servers s
			left join (SELECT SrvId FROM devices WHERE [Type] = 'STLC1000') ds ON s.SrvID = ds.SrvID 
			left join nodes n1 on n1.NodID = s.NodID
			left join zones z1 on n1.ZonID = z1.ZonID
			left join regions r1 on z1.RegID = r1.RegID
		WHERE (ds.SrvId is null) 
				and not (s.NodID  is null) -- non vengono considerati gli STLC senza riferimento alla stazione
RETURN



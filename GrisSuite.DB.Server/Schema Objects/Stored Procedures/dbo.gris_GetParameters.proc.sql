﻿CREATE PROCEDURE [dbo].[gris_GetParameters]
AS
	SET NOCOUNT ON;
	SELECT ParameterName, ParameterValue, ParameterDescription
	FROM [parameters] 
	ORDER BY ParameterName
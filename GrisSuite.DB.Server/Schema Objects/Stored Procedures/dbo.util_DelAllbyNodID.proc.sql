﻿CREATE PROCEDURE [dbo].[util_DelAllbyNodID] @NodID BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	BEGIN TRAN;

	DELETE FROM stream_fields WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.NodID = @NodID));

	DELETE FROM reference WHERE ReferenceID NOT IN (SELECT ReferenceID FROM stream_fields);

	DELETE FROM streams WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.NodID = @NodID));

	DELETE FROM device_status WHERE DevID IN (SELECT devices.DevID FROM devices WHERE (devices.NodID = @NodID));

	DELETE FROM devices WHERE (devices.NodID = @NodID);
	
	DELETE FROM rack WHERE RackID NOT IN (SELECT DISTINCT RackID FROM devices);
	DELETE FROM building WHERE BuildingID NOT IN (SELECT BuildingID FROM rack);
	DELETE FROM station WHERE StationID NOT IN (SELECT StationID FROM building);
	
	DELETE FROM port WHERE PortID NOT IN (SELECT DISTINCT PortID FROM devices);
	
	UPDATE servers
	SET
		IP = NULL,
		NodID = NULL
	WHERE (NodID = @NodID)

	COMMIT TRAN;
END



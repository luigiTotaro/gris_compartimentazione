﻿CREATE PROCEDURE [dbo].[gris_GetNotificationContactsByRegId](@RegIds VARCHAR(MAX), @Search VARCHAR(MAX)) AS
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    object_status.ObjectStatusId,
    object_status.IsSmsNotificationEnabled,
    regions.Name                                                                 AS RegionName,
    zones.Name                                                                   AS ZoneName,
    nodes.Name                                                                   AS NodeName,
    dbo.GetNotificationContactsByObjectStatusId(object_status.ObjectStatusId, 0) AS NotificationContacts
  FROM object_status
    INNER JOIN nodes ON object_status.ObjectId = nodes.NodID
    INNER JOIN zones ON nodes.ZonID = zones.ZonID
    INNER JOIN regions ON zones.RegID = regions.RegID
  WHERE (CHARINDEX('|' + CAST(regions.RegID AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (object_status.ObjectTypeId = 3 /* nodes */)
        AND (object_status.IsSmsNotificationEnabled <> 0)
    AND (@Search = '%%'
        OR regions.Name like @Search
        OR zones.Name like @Search
        OR nodes.Name like @Search
        )
  ORDER BY RegionName, ZoneName, NodeName
﻿
CREATE procedure [dbo].[gris_InsComplementariFormulaGroup]
(
	@SystemID as int,
	@NodID as bigint
) as
begin
	set xact_abort on;

	declare @VirtualObjectId as bigint;
	declare @VirtualObjectStatusId as uniqueidentifier;
	declare @NodeSystemId as bigint;
	declare @ParentNodeSystemStatusId as uniqueidentifier;

	select @NodeSystemId = ns.NodeSystemsId, @ParentNodeSystemStatusId = ObjectStatusId
	from node_systems ns
	inner join object_node_systems ons on ns.NodeSystemsId = ons.ObjectId
	where ns.NodId = @NodID and ns.SystemId = @SystemID;

	select @VirtualObjectId = vo.VirtualObjectID
	from object_virtuals ov
	inner join virtual_objects vo on ov.ObjectId = vo.VirtualObjectID
	where ov.ParentObjectStatusId = @ParentNodeSystemStatusId
	and vo.VirtualObjectName like 'Complementari';

	if @VirtualObjectId is null
		begin	
			begin tran
			
			-- creazione virtual object
			exec gris_InsVirtualObject @ParentNodeSystemStatusId, 'Complementari', 'Dispositivi Complementari per il funzionamento della stazione', @VirtualObjectId output, @VirtualObjectStatusId output;

			-- creazione formule di default per il raggruppamento
			insert into object_formulas values (@VirtualObjectStatusId, 0, '.\Lib\GroupScripts\GroupWith_AllElemIn_Ok_Get_Ok_OrGet_Ok-.py', 0);
				
			commit tran
		end

	set xact_abort off;
end
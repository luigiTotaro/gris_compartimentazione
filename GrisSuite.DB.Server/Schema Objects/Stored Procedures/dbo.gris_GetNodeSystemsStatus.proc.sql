﻿CREATE PROCEDURE [dbo].[gris_GetNodeSystemsStatus] (@NodId BIGINT, @FilterSystemID INT = NULL) AS
SET NOCOUNT ON;

SET @FilterSystemID = ISNULL(@FilterSystemID, 0);

WITH SystemsCounts AS 
(
	SELECT NodID, SystemID, [0] as Ok, [1] as Warning,[2] as Error, [255] as Unknown, [3] as [Offline], [9] as InMaintenance, [-1] NotClassified
	FROM 
	(
		SELECT nodes.NodID, device_type.SystemID, devices.DevID, object_devices.SevLevel
		FROM nodes 
			INNER JOIN devices ON nodes.NodID = devices.NodID
			INNER JOIN device_type ON devices.[Type] = device_type.DeviceTypeID
			INNER JOIN object_devices ON object_devices.ObjectId = devices.DevId
		WHERE nodes.NodId = @NodId
			AND object_devices.ExcludeFromParentStatus = 0
	) as A
	PIVOT
	(
		COUNT(DevID)
		for SevLevel
		in ([0],[1],[2],[3],[255],[9],[-1])
	) as p
)
SELECT NodeSystemsId, nodes.NodID, nodes.Name as NodName, systems.SystemID, systems.SystemDescription, 
ISNULL(SystemsCounts.Ok, 0) as Ok, 
ISNULL(SystemsCounts.Warning, 0) as Warning,
ISNULL(SystemsCounts.Error, 0) as Error, 
ISNULL(SystemsCounts.Unknown, 0) as Unknown, 
ISNULL([Offline], 0) as [Offline], 
0 as InMaintenance, 
ISNULL(NotClassified, 0) as NotClassified, 
ISNULL(SystemsCounts.Ok, 0) + ISNULL(SystemsCounts.Warning, 0) + ISNULL(SystemsCounts.Error, 0) + ISNULL(SystemsCounts.Unknown, 0) + ISNULL([Offline], 0) + 0 + ISNULL(NotClassified, 0) as Total,
object_node_systems.SevLevel as Status,
dbo.GetRawDeviceCountByNodeAndSystem (nodes.NodID, systems.SystemID) AS RawTotalDevices
FROM nodes
	INNER JOIN node_systems ON nodes.NodID = node_systems.NodId
	INNER JOIN systems ON node_systems.SystemId = systems.SystemID
	INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId
	LEFT JOIN SystemsCounts ON node_systems.NodId = SystemsCounts.NodId AND node_systems.SystemId = SystemsCounts.SystemID
WHERE nodes.NodId = @NodId
AND ((@FilterSystemID = 0) OR (node_systems.SystemId = @FilterSystemID))
ORDER BY SystemID

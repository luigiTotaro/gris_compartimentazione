CREATE PROCEDURE [dbo].[util_UpdNodesFromGIS]

AS
BEGIN
	--- Crea nuovi nodes associati alle linee da nodesGIS
	WITH nodesGISL AS
	(
	SELECT zones.ZonId 
		, nodesGIS.NodeGISId
		, nodesGIS.Name
		, nodesGIS.MapPoints
	FROM zones 
		INNER JOIN zones_subzones ON zones.ZonID = zones_subzones.ZonId 
		INNER JOIN subzones ON zones_subzones.SubZoneId = subzones.SubZoneId 
		INNER JOIN nodesGIS ON subzones.NodeGISIdA = nodesGIS.NodeGisId
	UNION
	SELECT zones.ZonId 
		, nodesGIS.NodeGISId
		, nodesGIS.Name
		, nodesGIS.MapPoints
	FROM zones 
		INNER JOIN zones_subzones ON zones.ZonID = zones_subzones.ZonId 
		INNER JOIN subzones ON zones_subzones.SubZoneId = subzones.SubZoneId 
		INNER JOIN nodesGIS ON subzones.NodeGISIdB = nodesGIS.NodeGisID
	), nodesGISL2 as 
	(
	SELECT MIN(ZonId) AS ZonId, NodeGISId, Name, MapPoints
	FROM nodesGISL
	GROUP BY NodeGISId, Name, MapPoints
	)
	, NodesUsed AS
	(
	select distinct 'LO' + RIGHT('0000' + CAST(dbo.GetDecodedNodId(nodid) AS VARCHAR(MAX)),4) as DecodedNodId
	from nodes where dbo.GetDecodedNodId(nodid) <= 9999
	)
	INSERT INTO nodes (ZonId, NodID, Name, Meters)
	SELECT zones.ZonID
		, dbo.SetDecodedNodId(dbo.GetDecodedRegId(RegID),dbo.GetDecodedZonId(zones.ZonID), CAST( SUBSTRING(NodeGISId,3,4) as bigint)) as NodId
		, nodesGISL2.Name
		, -10 as Meters
		 FROM nodesGISL2 
		 INNER JOIN zones ON zones.ZonID = nodesGISL2.ZonID
	LEFT JOIN NodesUsed ON nodesGISL2.NodeGISId = NodesUsed.DecodedNodId
	WHERE NodesUsed.DecodedNodId IS NULL and not (nodesGISL2.Name in ('ACQUAVIVA',
	'APRICENA',
	'CALDERARA BARGELLINO',
	'CATANZARO',
	'MELZO',
	'Nuovo Salario',
	'Pisa C.le',
	'Prato Borgonuovo',
	'ROMA TERMINI',
	'ROMA TIBURTINA',
	'Segrate',
	'Taggia Arma'));
	
	-- Crea no device
	insert into devices (DevID,NodID,Name,Type)
	select dbo.SetDecodedDevId(
	dbo.GetDecodedRegId(zones.RegId),
	dbo.GetDecodedZonId(nodes.ZonID),
	dbo.GetDecodedNodId(nodes.NodID),0) as DevID,
	NodID, 'NoDevice' as Name, 'NoDevice' as Type 
	from nodes inner join zones on nodes.ZonID = zones.ZonID
	where not (NodID in (select distinct NodID from devices));

	--- Esegui calcolo per creare nuovi object_status
	EXEC dbo.tf_UpdCalculateObjectStatus;

	--- Aggiorna classificazioni
	WITH S AS
	(
		SELECT object_nodes.ObjectStatusId, 
			classification_values.ClassificationValueId, 
			mlayer.MapLayerId, 0 as MapOffsetX, 0 as MapOffsetY, 0 as CaptionFromScale
		FROM nodesGIS 
			INNER JOIN nodes ON nodesGIS.NodeGISId = 'LO' + RIGHT('0000' + CAST(dbo.GetDecodedNodId(nodid) AS VARCHAR(MAX)),4)
			INNER JOIN classification_values ON classification_values.ValueName = METALLO
			INNER JOIN object_nodes ON object_nodes.ObjectId = nodes.NodID	
			INNER JOIN (SELECT * FROM map_layers WHERE maplayerID IN (3,4,5,6,16)) AS mlayer ON mlayer.ClassificationValueId = classification_values.ClassificationValueId
		WHERE NOT (nodesGIS.name like '%CANCELLATA%') AND dbo.GetDecodedNodId(nodid) <= 9999
	)
	UPDATE T
	SET T.ClassificationValueId = S.ClassificationValueId, T.MapLayerId = S.MapLayerId
	FROM dbo.object_classification_values T
		INNER JOIN S ON T.ObjectStatusId = S.ObjectStatusId;
		
	WITH S AS
	(
		SELECT object_nodes.ObjectStatusId, 
			classification_values.ClassificationValueId, 
			mlayer.MapLayerId, 0 as MapOffsetX, 0 as MapOffsetY, 0 as CaptionFromScale
		FROM nodesGIS 
			INNER JOIN nodes ON nodesGIS.NodeGISId = 'LO' + RIGHT('0000' + CAST(dbo.GetDecodedNodId(nodid) AS VARCHAR(MAX)),4)
			INNER JOIN classification_values ON classification_values.ValueName = METALLO
			INNER JOIN object_nodes ON object_nodes.ObjectId = nodes.NodID	
			INNER JOIN (SELECT * FROM map_layers WHERE maplayerID IN (3,4,5,6,16)) AS mlayer ON mlayer.ClassificationValueId = classification_values.ClassificationValueId
		WHERE NOT (nodesGIS.name like '%CANCELLATA%') AND dbo.GetDecodedNodId(nodid) <= 9999
	)
	INSERT INTO object_classification_values (ObjectStatusId, ClassificationValueId, MapLayerId, MapOffsetX, MapOffsetY, CaptionFromScale)
	SELECT S.ObjectStatusId, S.ClassificationValueId, S.MapLayerId, S.MapOffsetX, S.MapOffsetY, S.CaptionFromScale
	FROM S LEFT JOIN dbo.object_classification_values T ON S.ObjectStatusId = T.ObjectStatusId
	WHERE T.ObjectStatusId IS NULL;
		

	-- Aggiorna coordinate geografiche
	--MERGE dbo.object_map_points AS T
	
	WITH S AS
	(
	SELECT  ObjectStatusId, MapPoints   
	FROM object_nodes 
		INNER JOIN nodes ON object_nodes.ObjectId = nodes.NodID 
		INNER JOIN nodesGIS on nodesGIS.NodeGISId = 'LO' + RIGHT('0000' + CAST(dbo.GetDecodedNodId(nodid) AS VARCHAR(MAX)),4)
		WHERE dbo.GetDecodedNodId(nodid) <= 9999
	)
	UPDATE T
	SET T.MapPoints = S.MapPoints
	FROM dbo.object_map_points AS T INNER JOIN S ON T.ObjectStatusId = S.ObjectStatusId;
	
	WITH S AS
	(
	SELECT  ObjectStatusId, MapPoints   
	FROM object_nodes 
		INNER JOIN nodes ON object_nodes.ObjectId = nodes.NodID 
		INNER JOIN nodesGIS on nodesGIS.NodeGISId = 'LO' + RIGHT('0000' + CAST(dbo.GetDecodedNodId(nodid) AS VARCHAR(MAX)),4)
		WHERE dbo.GetDecodedNodId(nodid) <= 9999
	)
	INSERT INTO object_map_points (ObjectStatusId, MapPoints)
	SELECT S.ObjectStatusId, S.MapPoints
	FROM S LEFT JOIN dbo.object_map_points AS T ON S.ObjectStatusId = T.ObjectStatusId
	WHERE T.ObjectStatusId IS NULL;

	-- ProperCase e restta meters
	UPDATE nodes SET Name = dbo.ProperCase(Name), Meters = 0 WHERE Meters = -10

	-- Cancella object_status fantasma
	DELETE FROM  object_classification_values
	WHERE ObjectStatusId IN (SELECT object_nodes.ObjectStatusId
	FROM nodes 
		right JOIN object_nodes ON nodes.NodID = object_nodes.ObjectId
	WHERE nodes.NodID IS NULL)

	DELETE FROM object_map_points
	WHERE ObjectStatusId IN (SELECT object_nodes.ObjectStatusId
	FROM nodes 
		right JOIN object_nodes ON nodes.NodID = object_nodes.ObjectId
	WHERE nodes.NodID IS NULL)
	
	DELETE FROM object_status
	WHERE ObjectStatusId IN (SELECT object_nodes.ObjectStatusId
	FROM nodes 
		right JOIN object_nodes ON nodes.NodID = object_nodes.ObjectId
	WHERE nodes.NodID IS NULL)

	DELETE FROM  object_classification_values
	WHERE ObjectStatusId IN (SELECT object_zones.ObjectStatusId
	FROM zones 
		right JOIN object_zones ON zones.ZonID = object_zones.ObjectId
	WHERE zones.ZonID IS NULL)

	DELETE FROM object_map_points
	WHERE ObjectStatusId IN (SELECT object_zones.ObjectStatusId
	FROM zones 
		right JOIN object_zones ON zones.ZonID = object_zones.ObjectId
	WHERE zones.ZonID IS NULL)

	DELETE FROM object_status
	WHERE ObjectStatusId IN (SELECT object_zones.ObjectStatusId
	FROM zones 
		right JOIN object_zones ON zones.ZonID = object_zones.ObjectId
	WHERE zones.ZonID IS NULL)

END
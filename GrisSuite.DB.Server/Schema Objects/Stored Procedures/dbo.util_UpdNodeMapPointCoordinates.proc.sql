﻿CREATE PROCEDURE util_UpdNodeMapPointCoordinates(@NodeName VARCHAR(64), @NewLatitude DECIMAL(15,8), @NewLongitude DECIMAL(15,8)) AS
SET NOCOUNT ON;

IF ((SELECT COUNT(NodId) FROM nodes WHERE name LIKE @NodeName) = 1)
BEGIN
	DECLARE @NodID BIGINT
	SET @NodId = (SELECT NodId FROM nodes WHERE name LIKE @NodeName)

	DECLARE @ObjectStatusId uniqueidentifier
	SELECT @ObjectStatusId = ObjectStatusId FROM object_status WHERE objectid = @NodId AND ObjectTypeId = 3 -- Nodes

	DECLARE @MapPoints VARCHAR(MAX)
	SELECT @MapPoints = MapPoints FROM object_map_points
	WHERE ObjectStatusId = @ObjectStatusId

	IF (@MapPoints LIKE 'point%')
	BEGIN
		DECLARE @NewMapPoints VARCHAR(MAX)

		SET @NewMapPoints = 'point ' + CONVERT(VARCHAR(20), @NewLatitude) + ' ' + CONVERT(VARCHAR(20), @NewLongitude)
		SELECT @NewMapPoints

		UPDATE object_map_points
		SET MapPoints = @NewMapPoints
		WHERE ObjectStatusId = @ObjectStatusId
	END
END
ELSE
BEGIN
	SELECT 'Esistono più nodi - stazioni con il nome ' + @NodeName
END
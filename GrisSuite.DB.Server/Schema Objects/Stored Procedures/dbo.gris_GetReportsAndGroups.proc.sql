﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 04/11/2010
-- Description:	
-- =============================================
CREATE PROCEDURE gris_GetReportsAndGroups 
	@ReportName VARCHAR(1000) = '', 
	@GroupName VARCHAR(500) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		ResourceCategoryDescription as ReportCategory
		, r.ResourceID as ReportID, ResourceDescription as ReportName, g.GroupName
	FROM resources r
	INNER JOIN resource_categories c ON c.ResourceCategoryID = r.ResourceCategoryID
	INNER JOIN [permissions] p ON p.ResourceID = r.ResourceID
	INNER JOIN permission_groups g ON g.GroupID = p.GroupID
	WHERE ResourceTypeID = 1 /* Reports */
		AND (ResourceDescription LIKE @ReportName OR @ReportName = '')
		AND (GroupName LIKE @GroupName OR @GroupName = '')
	ORDER BY c.SortOrder, r.SortOrder, g.SortOrder;
END
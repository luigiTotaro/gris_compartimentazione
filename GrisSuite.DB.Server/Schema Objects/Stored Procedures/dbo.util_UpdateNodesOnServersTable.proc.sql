﻿CREATE PROCEDURE [dbo].[util_UpdateNodesOnServersTable]
AS
BEGIN
	UPDATE servers SET servers.NodID = Map.NodID
	FROM servers
	INNER JOIN 
	(
	SELECT servers.SrvID, MIN(nodes.NodID) AS NodID
	FROM servers 
	INNER JOIN devices ON servers.SrvID = devices.SrvID 
	INNER JOIN nodes ON devices.NodID = nodes.NodID
	GROUP BY servers.SrvID
	HAVING COUNT(DISTINCT nodes.NodID) = 1
	) AS Map ON Map.SrvID = servers.SrvID
	WHERE servers.NodID IS NULL;
END



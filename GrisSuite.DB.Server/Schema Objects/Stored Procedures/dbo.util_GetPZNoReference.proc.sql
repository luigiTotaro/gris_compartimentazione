﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 21-03-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[util_GetPZNoReference] 
	@MinuteTimeOut int = 120
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT regions.Name as RegName, zones.Name as ZoneName, nodes.Name as NodeName, devices.DevID, devices.Name as PZ, servers.IP as IP_STLC
	FROM devices 
	INNER JOIN device_status ON devices.DevID = device_status.DevID 
	INNER JOIN stream_fields ON devices.DevID = stream_fields.DevID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN servers ON servers.SrvID = devices.SrvID
	WHERE ((device_status.Offline = 0) AND (DATEDIFF(n, ISNULL(servers.LastUpdate, GETDATE()), GETDATE()) <= @MinuteTimeOut))
	AND ((devices.type = 'TT10210') OR (devices.type = 'TT10210V3') OR (devices.type = 'TT10220'))
	AND (devices.DevID NOT IN
		(
			SELECT DISTINCT DevID
			FROM stream_fields 
			WHERE ReferenceID IS NOT NULL
		));
END



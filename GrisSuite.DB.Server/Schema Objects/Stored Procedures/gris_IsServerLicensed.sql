﻿CREATE PROCEDURE [dbo].[gris_IsServerLicensed] (@SrvID bigint, @IP varchar(16), @HardwareProfile varchar(64)) AS
-- Questa SP è invocata durante la centralizzazione, per sapere se un server (non STLC) ha l'opportuna licenza
-- Gli STLC, saranno riconosciuti sul client e la chiamata non avverrà, quindi situazione non gestita. I server STLC non saranno mai presenti sulla
-- tabella servers_licenses, perché licenziati di default
-- La chiave della tabella è il ServerID. Il profilo hardware permette di distinguere eventuali clienti multipli
SET NOCOUNT ON;

DECLARE @IsLicensed TINYINT;
SELECT @IsLicensed = IsLicensed FROM servers_licenses WHERE (SrvID = @SrvID);

IF (@IsLicensed IS NULL)
BEGIN
	-- Non è stata trovata la riga per il server indicato, che sta cercando la sua licenza. Lo censiamo, in modo che possa
	-- essere licenziato da interfaccia.
	IF (@SrvID > 0)
	BEGIN
		INSERT INTO servers_licenses (SrvID, IP, LicenseLastUpdate, LicenseLastUpdateUser, IsLicensed, HardwareProfile)
		VALUES (@SrvID, @IP, GetDate(), 'SCACollector', 0 /* Non licenziato */, ISNULL(@HardwareProfile, ''));
	END

	SET @IsLicensed = 0; /* Non licenziato */
END
ELSE
BEGIN
	UPDATE servers_licenses
	SET IP = @IP,
	HardwareProfile = ISNULL(@HardwareProfile, '')
	WHERE (SrvID = @SrvID)
END

SELECT CONVERT(TINYINT, ISNULL(@IsLicensed, 0 /* Non licenziato */)) AS IsLicensed;
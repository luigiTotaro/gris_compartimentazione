﻿CREATE PROCEDURE [dbo].[gris_UpdObjectFormulaCustomColorsForObjectStatusId] (@ObjectStatusId UNIQUEIDENTIFIER, @FormulaIndex TINYINT, @AudioSystemSevLevel INT, @VideoSystemSevLevel INT, @HtmlColor CHAR(7)) AS
SET NOCOUNT ON;

IF (LEN(ISNULL(@HtmlColor, '')) = 7)
BEGIN
	IF EXISTS (SELECT ObjectFormulasCustomColorId FROM object_formulas_custom_colors WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex) AND (AudioSystemSevLevel = @AudioSystemSevLevel) AND (VideoSystemSevLevel = @VideoSystemSevLevel))
	BEGIN
		UPDATE object_formulas_custom_colors
		SET HtmlColor = @HtmlColor
		WHERE (ObjectStatusId = @ObjectStatusId)
		AND (FormulaIndex = @FormulaIndex)
		AND (AudioSystemSevLevel = @AudioSystemSevLevel)
		AND (VideoSystemSevLevel = @VideoSystemSevLevel);
	END
	ELSE
	BEGIN
		INSERT INTO object_formulas_custom_colors
		(ObjectStatusId, FormulaIndex, AudioSystemSevLevel, VideoSystemSevLevel, HtmlColor)
		VALUES
		(@ObjectStatusId, @FormulaIndex, @AudioSystemSevLevel, @VideoSystemSevLevel, @HtmlColor);
	END
END
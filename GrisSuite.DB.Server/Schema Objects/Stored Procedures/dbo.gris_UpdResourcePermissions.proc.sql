﻿CREATE PROCEDURE gris_UpdResourcePermissions
    @GroupIds   VARCHAR(MAX),
    @ResourceID INT
AS
  BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DELETE FROM [permissions]
    WHERE ResourceID = @ResourceID;

    INSERT INTO [permissions] (GroupID, ResourceID)
      SELECT
        GroupID,
        @ResourceID
      FROM permission_groups
      WHERE CHARINDEX('|' + CAST(GroupID AS VARCHAR(4)) + '|', @GroupIds, 0) > 0;

    -- remove resource notification for user without permission
    DELETE FROM resource_notification_contacts
    WHERE ResourceNotificationContactId IN (
      SELECT ResourceNotificationContactId
      FROM resource_notification_contacts n
        INNER JOIN permission_groups_contacts gc ON n.ContactId = gc.ContactId
        LEFT JOIN permissions p ON p.GroupID = gc.GroupId AND p.ResourceID = n.ResourceId
      WHERE n.ResourceId = @ResourceID AND p.PermissionID IS NULL
    );

    SET XACT_ABORT OFF;
  END
﻿CREATE PROCEDURE gris_GetOperationSchedules (@OperationScheduleTypeId SMALLINT) AS
SET NOCOUNT ON;

SELECT OperationScheduleId, OperationParameters, DateCreated, DateExecuted, DateAborted
FROM operation_schedules
WHERE (OperationScheduleTypeId = @OperationScheduleTypeId)
ORDER BY DateCreated DESC
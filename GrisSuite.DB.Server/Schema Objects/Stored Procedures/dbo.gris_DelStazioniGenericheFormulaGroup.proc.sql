﻿CREATE PROCEDURE [dbo].[gris_DelStazioniGenericheFormulaGroup] (@ZonID BIGINT) AS
SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @VirtualObjectId AS BIGINT;
DECLARE @VirtualObjectStatusId AS UNIQUEIDENTIFIER;
DECLARE @ParentZoneStatusId AS UNIQUEIDENTIFIER;

SELECT @ParentZoneStatusId = object_zones.ObjectStatusId
FROM zones
INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId
WHERE zones.ZonID = @ZonID;

SELECT @VirtualObjectId = vo.VirtualObjectID, @VirtualObjectStatusId = ov.ObjectStatusId
FROM object_virtuals ov
INNER JOIN virtual_objects vo ON ov.ObjectId = vo.VirtualObjectID
WHERE ov.ParentObjectStatusId = @ParentZoneStatusId
AND vo.VirtualObjectName LIKE 'Stazioni Generiche';

IF @VirtualObjectId IS NOT NULL AND @VirtualObjectStatusId IS NOT NULL
BEGIN
	BEGIN TRAN;
	
	-- cancellazione attributi del raggruppamento
	DELETE FROM object_attributes WHERE ObjectStatusId = @VirtualObjectStatusId;
	
	-- cancellazione formule di default per il raggruppamento
	DELETE FROM object_formulas WHERE ObjectStatusId = @VirtualObjectStatusId;
	
	-- disassociazione dei nodi dal virtual object e associazione alla linea
	UPDATE object_status SET ParentObjectStatusId = @ParentZoneStatusId
	WHERE ObjectTypeId = 3 /* nodi */ AND ParentObjectStatusId = @VirtualObjectStatusId;
		
	-- cancellazione virtual object
	EXEC gris_DelVirtualObject @ParentZoneStatusId, 'Stazioni Generiche';

	-- aggiornamento formula del sistema padre
	UPDATE object_formulas SET ScriptPath = '.\Lib\ZoneDefaultStatus.py' WHERE ObjectStatusId = @ParentZoneStatusId AND FormulaIndex = 0;

	COMMIT TRAN;
END
﻿CREATE PROCEDURE [dbo].[tf_DelStation]
(
	@Original_StationID uniqueidentifier
)
AS
	SET NOCOUNT ON; -- se off e non c'è la riga ADO.NET da errore

	UPDATE Devices SET RackID = null, RackPositionRow = null,  RackPositionCol = null 
	WHERE [RackID] IN (SELECT RackID FROM [rack] INNER JOIN [building] ON [rack].[BuildingID] = [building].[BuildingID]
					   WHERE ([StationID] = @Original_StationID))
	DELETE [rack] 
		FROM [rack] INNER JOIN [building] ON [rack].[BuildingID] = [building].[BuildingID] 
		WHERE ([StationID] = @Original_StationID)
	DELETE FROM [building] WHERE ([StationID] = @Original_StationID)
	DELETE FROM [station] WHERE ([StationID] = @Original_StationID)



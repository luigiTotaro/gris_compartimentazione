﻿CREATE PROCEDURE [dbo].[gris_UpdCleanAlertsDevices]
AS
	BEGIN TRANSACTION
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alerts_devices]') AND type in (N'U'))
	DROP TABLE [dbo].[alerts_devices]
	CREATE TABLE dbo.alerts_devices
		(
		DevID bigint NOT NULL,
		RuleID int NOT NULL
		)  ON [PRIMARY]
	ALTER TABLE dbo.alerts_devices ADD CONSTRAINT
		PK_alerts_devices PRIMARY KEY CLUSTERED 
		(
		DevID,
		RuleID
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	COMMIT



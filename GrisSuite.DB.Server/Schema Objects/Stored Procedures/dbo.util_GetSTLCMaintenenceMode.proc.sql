-- =============================================
-- Author:  Marchi Damiano
-- Create date: 19/09/2008
-- Description: Estrazione di tutti gli apparati STLC1000 in Maintenance Mode
-- =============================================
CREATE PROCEDURE [dbo].[util_GetSTLCMaintenenceMode] 
 
AS
BEGIN
 SET NOCOUNT ON;
 SELECT  regions.Name AS Compartimento,
    zones.Name AS Linea,
    nodes.Name AS Stazione,
    servers_status.IP AS 'STLC IP',
    servers_status.Host AS 'STLC Host'
 FROM  servers_status INNER JOIN
                nodes ON servers_status.NodID = nodes.NodID INNER JOIN
                zones ON nodes.ZonID = zones.ZonID INNER JOIN
                regions ON zones.RegID = regions.RegID
 WHERE  (servers_status.InMaintenance = 0)
 
END



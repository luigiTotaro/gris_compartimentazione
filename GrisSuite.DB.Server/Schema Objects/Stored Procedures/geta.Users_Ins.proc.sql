﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
create PROCEDURE [geta].[Users_Ins]
	@UserID int OUTPUT,
	@Username varchar(256),
	@FirstName varchar(256),
	@LastName varchar(256),
	@OrganizationID int,
	@UserGroupID int,
	@MobilePhone varchar(64),
	@Email varchar(128)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @UsrID INT
	SELECT @UsrID = UserID FROM users WHERE Username = @Username;

	IF @UsrID IS NULL
	BEGIN

		INSERT INTO users
			(Username, FirstName, LastName, OrganizationID, UserGroupID, MobilePhone, Email)
		VALUES
			(@Username, @FirstName, @LastName, @OrganizationID, @UserGroupID, @MobilePhone, @Email);

		SET @UserID = CAST(SCOPE_IDENTITY() AS INT);
	END
END



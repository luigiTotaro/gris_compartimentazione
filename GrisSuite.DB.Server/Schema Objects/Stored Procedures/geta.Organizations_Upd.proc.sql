﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[Organizations_Upd] 
	@OrganizationID int,
	@OrganizationName varchar(256)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE geta.organizations SET OrganizationName = @OrganizationName WHERE (OrganizationID = @OrganizationID);
END



/*CREATE PROCEDURE gris_ReportDevicesFieldsByZoneSevLevelType (@ZoneName varchar(64) = '', 
						@NodeName varchar(64) = '', 
						@SevLevelList varchar(64) = '',
						@DeviceType varchar(16) = '', 
						@Message varchar(64) = '')
AS

SELECT TOP 10000 zones.Name as Linea,
		nodes.Name AS Stazione, 
		devices.Type as TipoPeriferica,
		devices.Name AS NomePeriferica, 
		(CASE WHEN (CAST(ISNULL(devices.addr,'0') as BIGINT) > 255) THEN dbo.GetIPStringFromInt(devices.addr) ELSE devices.addr	END) AS Indirizzo,
		device_status.Offline,
		stream_fields.SevLevel, 
		streams.Name AS NomeStream, 
		stream_fields.Name AS NomeCampo, 
		stream_fields.arrayid AS ArrayIndex,
		stream_fields.Value, 
		[dbo].GetFieldDescriptionBySevLevel(stream_fields.Description, '|2|',0 ) as Errori,
		[dbo].GetFieldDescriptionBySevLevel(stream_fields.Description, '|1|',0) as Avvisi,
		[dbo].GetFieldDescriptionBySevLevel(stream_fields.Description, '|0|',0) as Informazioni
FROM devices 
	INNER JOIN device_status ON devices.DevID = device_status.DevID 
	INNER JOIN streams  ON device_status.DevID = streams.DevID
	INNER JOIN stream_fields ON stream_fields.DevID = streams.DevID AND stream_fields.StrID = streams.StrID
	INNER JOIN nodes ON nodes.NodID = devices.NodID 
	INNER JOIN zones ON zones.ZonID = nodes.ZonID 
where (@ZoneName = '' OR zones.name like @ZoneName)
	and (@NodeName = '' OR nodes.name like @NodeName)
	and (@DeviceType = '' OR devices.type like @DeviceType)
	and (@SevLevelList = '' OR charindex('|' + CAST(stream_fields.SevLevel as varchar(3)) + '|', @SevLevelList) > 0)
	and (@Message = '' OR charindex(@Message, stream_fields.Description ) > 0)
	and stream_fields.visible = 1
order by Linea, Stazione, TipoPeriferica, NomePeriferica, [Offline], SevLevel
RETURN*/
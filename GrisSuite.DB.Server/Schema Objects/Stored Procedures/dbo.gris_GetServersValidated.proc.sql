﻿CREATE PROCEDURE [dbo].[gris_GetServersValidated]
AS
	SELECT SrvID, FullHostName, IP, ServerDateValidationRequested, ServerDateValidationObtained
	FROM servers
	WHERE (ServerDateValidationRequested IS NOT NULL) AND (ServerDateValidationObtained IS NOT NULL) AND (ServerDateValidationRequested <= ServerDateValidationObtained);



﻿CREATE PROCEDURE gris_GetVirtualObjectsTreeByNode (@NodID BIGINT) AS
SET NOCOUNT ON;

SELECT
nodes.Name,
object_nodes.ObjectStatusId AS ObjectStatusIdNode,
node_systems.NodeSystemsId,
systems.SystemID,
systems.SystemDescription,
object_node_systems.ObjectStatusId AS ObjectStatusIdNodeSystem,
object_virtuals.ObjectStatusId AS ObjectStatusIdVirtualObject,
virtual_objects.VirtualObjectName,
virtual_objects.VirtualObjectDescription
FROM virtual_objects
INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId
RIGHT OUTER JOIN nodes
INNER JOIN object_nodes ON nodes.NodID = object_nodes.ObjectId
INNER JOIN node_systems ON nodes.NodID = node_systems.NodId
INNER JOIN systems ON node_systems.SystemId = systems.SystemID
INNER JOIN object_node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId
ON object_virtuals.ParentObjectStatusId = object_node_systems.ObjectStatusId
WHERE (nodes.NodID = @NodID)
ORDER BY systems.SystemID, dbo.GetVirtualObjectCustomOrder (virtual_objects.VirtualObjectName, 1)
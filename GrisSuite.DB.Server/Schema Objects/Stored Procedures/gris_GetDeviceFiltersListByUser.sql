﻿CREATE PROCEDURE [dbo].[gris_GetDeviceFiltersListByUser] (@Username VARCHAR(256)) AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @UserID INT;
  SELECT @UserID = UserID FROM users WHERE (Username LIKE @Username);

  IF (@UserID IS NULL)
  BEGIN
    -- Se l'utente non esiste in base dati, lo creiamo al volo, usando la username, unico dato disponibile su Gris
    -- l'eventuale modifica dei dati anagrafici può essere fatta su Geta
    INSERT INTO users (Username, FirstName, LastName, OrganizationID, UserGroupID, MobilePhone, Email)
    VALUES (@Username, NULL, NULL, NULL, NULL, NULL, NULL);

    SET @UserID = CAST(SCOPE_IDENTITY() AS INT);
  END

  SELECT DeviceFilterId, DeviceFilterName, UserID, ResourceID
  FROM device_filters
  WHERE (UserID = ISNULL(@UserID, 0) OR ISNULL(ResourceID, 0) <> 0 )
  ORDER BY DeviceFilterName
END
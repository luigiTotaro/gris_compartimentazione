﻿CREATE PROCEDURE util_GetStazione_CaptionFromScale( @NomeStazione VARCHAR(64))
AS
BEGIN

	select onode.objectstatusid, nodes.nodid, name, ocv.CaptionFromScale, l.CaptionFromScale as CaptionFromScaleLayer
	from  dbo.object_classification_values ocv
		inner join dbo.object_nodes onode on ocv.objectstatusid = onode.objectstatusid
		inner join dbo.nodes  on nodes.nodid = onode.objectid
		inner join dbo.map_layers l on l.MapLayerId = ocv.MapLayerId
	where name LIKE @NomeStazione

END
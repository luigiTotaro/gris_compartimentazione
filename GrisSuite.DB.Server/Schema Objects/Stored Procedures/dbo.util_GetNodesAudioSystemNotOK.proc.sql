﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 26-03-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[util_GetNodesAudioSystemNotOK] 
AS
BEGIN
	SET NOCOUNT ON;

	-- non vengono contemplate le stazioni che non hanno nessuna dei tre tipi di periferica
	WITH AudioNodes
	AS
	(
		SELECT NodID
		FROM
		(
			SELECT DISTINCT NodID
			FROM devices
			WHERE ([Type] = 'DT04000')
			UNION ALL
			SELECT DISTINCT NodID
			FROM devices
			WHERE ([Type] = 'DT13000')
			UNION ALL
			SELECT DISTINCT NodID
			FROM devices
			WHERE ([Type] = 'TT10210')
			UNION ALL
			SELECT DISTINCT NodID
			FROM devices
			WHERE ([Type] = 'TT10210V3')
			UNION ALL
			SELECT DISTINCT NodID
			FROM devices
			WHERE ([Type] = 'TT10220')						
		) AS AudioNodes
		GROUP BY NodID
		HAVING COUNT(NodID) < 3
	)
	SELECT DISTINCT regions.Name as RegName, zones.Name as ZoneName, nodes.Name as NodeName, servers.IP as IP_STLC
	FROM devices 
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN servers ON servers.SrvID = devices.SrvID
	INNER JOIN AudioNodes ON AudioNodes.NodID = nodes.NodID;
END



﻿CREATE PROCEDURE [dbo].[tf_UpdObjectStatusPostComputation] (@LastUpdateGuid UNIQUEIDENTIFIER) AS
SET NOCOUNT ON;

-- Aggiorna i dati dei conteggi di tutti gli oggetti con figli
UPDATE object_status
SET
Ok = object_status_counters.Ok,
Warning = object_status_counters.Warning,
Error = object_status_counters.Error,
Maintenance = object_status_counters.Maintenance,
Unknown = object_status_counters.Unknown
FROM dbo.object_status_counters() AS object_status_counters
INNER JOIN object_status ON object_status.ObjectStatusId = object_status_counters.ObjectStatusId

-- Aggiorna parametro ultimo calcolo
IF (EXISTS(SELECT ParameterValue FROM parameters WHERE ParameterName = 'LastCalcStatus'))
BEGIN
	UPDATE parameters SET ParameterValue = CONVERT(VARCHAR(MAX),GETDATE(),121) WHERE ParameterName = 'LastCalcStatus'
END
ELSE	
BEGIN
	INSERT INTO parameters (ParameterName,ParameterValue,ParameterDescription)
	VALUES ('LastCalcStatus', CONVERT(VARCHAR(MAX),GETDATE(),121), 'Data ora ultimo calcolo degli stati')
END
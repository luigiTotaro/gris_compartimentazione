﻿CREATE PROCEDURE [dbo].[gris_Report_HistoryDeviceSevLevel](
	 @DataInizio AS datetime = null
   , @DataFine AS datetime = null
   , @GiorniDellaSettimana AS varchar(15) = null
   , @OraInizio AS datetime = null
   , @OraFine AS datetime = null
   , @ListaSistemi AS varchar(Max) = null
   , @Stazione AS varchar(64) = null
   , @Linea AS varchar(64) = null
   , @Compartimento AS varchar(64) = null
   , @TipoPeriferica AS varchar(64) = null
   , @DurataMin AS int = null
   , @DurataMax AS int = null
   , @ListaSevLevel AS varchar(64) = null)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Now DATETIME;
	SET @Now = GetDate();

	SET @DataInizio = ISNULL(DATEADD(dd, DATEDIFF(dd, 0, @DataInizio), 0), '19000101');
	SET @DataFine = ISNULL(DATEADD(dd, DATEDIFF(dd, 0, @DataFine), 0), @Now);
	SET @OraInizio = ISNULL(DATEADD(dd, -DATEDIFF(dd, 0, @OraInizio), @OraInizio), '00:00:00.000');
	SET @OraFine = ISNULL(DATEADD(dd, -DATEDIFF(dd, 0, @OraFine), @OraFine), '23:59:59.999');
	SET @GiorniDellaSettimana = CASE WHEN RTRIM(ISNULL(@GiorniDellaSettimana, '')) = '' THEN '|1|2|3|4|5|6|7|' ELSE @GiorniDellaSettimana END;
	SET @ListaSistemi = CASE WHEN RTRIM(ISNULL(@ListaSistemi, '')) = '' THEN '' ELSE @ListaSistemi END;
	SET @Stazione = '%' + ISNULL(@Stazione, '') + '%';
	SET @Compartimento = '%' + ISNULL(@Compartimento, '') + '%';
	SET @Linea = '%' + ISNULL(@Linea, '') + '%';
	SET @DurataMin = ISNULL(@DurataMin, 0);
	SET @DurataMax = ISNULL(@DurataMax, 2147483647);
	SET @ListaSevLevel = CASE WHEN RTRIM(ISNULL(@ListaSevLevel, '')) = '' THEN '|0|1|2|9|255|' ELSE @ListaSevLevel END; 
	SET @TipoPeriferica  = '%' + ISNULL(@TipoPeriferica, '') + '%';

	--- giorno della settimana contando lunedì come 1°
	SET DATEFIRST 1;

	SELECT TOP 65529 H.DevID 
		 , H.DevType as CodiceTipoPeriferica
		 , Device_type.DefaultName as NomeTipoPeriferica
		 , Device_type.DeviceTypeDescription as DescrizioneTipoPeriferica
		 , H.SevLevel AS CodiceStato
		 , Severity.Description	AS Stato
		 , H.Duration AS Durata
		 , H.Created AS InizioStato
		 , CASE
		   WHEN ISNULL(H.Duration,0) > 0 THEN DATEADD(second, H.Duration, H.Created)
		   ELSE NULL
		   END AS FineStato
		 , nodes.Name AS Stazione
		 , zones.Name AS Linea
		 , regions.Name AS Compartimento
		 , servers.IP AS STLC_IP
		 , servers.MAC AS STLC_MAC
		 , systems.SystemDescription AS Sistema
		 , devices.Name AS NomePeriferica
	FROM history_device_sevlevel AS H INNER JOIN device_type ON H.DevType = device_type.DeviceTypeID
									  INNER JOIN nodes ON H.NodId = nodes.NodID
									  INNER JOIN servers ON H.SrvId = servers.SrvID
									  INNER JOIN severity ON H.SevLevel = severity.SevLevel
									  LEFT JOIN systems ON device_type.SystemID = systems.SystemID
									  LEFT JOIN zones ON nodes.ZonID = zones.ZonID
									  LEFT JOIN regions ON zones.RegID = regions.RegID
									  INNER JOIN devices ON H.DevID = devices.DevID
	WHERE
	  @DataFine >= CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, ISNULL(DATEADD(second, H.Duration, H.Created), @Now)))) -- Nel caso manchi la data fine, assumiamo "adesso"
	  AND @DataInizio <= CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, H.Created)))
	  AND @OraFine >= DATEADD(dd, -DATEDIFF(dd, 0, ISNULL(DATEADD(second, H.Duration, H.Created), '23:59:59.999')), ISNULL(DATEADD(second, H.Duration, H.Created), '23:59:59.999'))
	  AND @OraInizio <= DATEADD(dd, -DATEDIFF(dd, 0, H.Created), H.Created)
	  AND (CHARINDEX('|' + CAST(DATEPART(weekday, H.Created)AS varchar(1)) + '|', @GiorniDellaSettimana, 0) > 0
		OR CASE
		   WHEN NOT H.Duration IS NULL THEN CHARINDEX('|' + CAST(DATEPART(weekday, DATEADD(second, H.Duration, H.Created))AS varchar(1)) + '|', @GiorniDellaSettimana, 0)
		   ELSE 0
		   END > 0)
	  AND (@ListaSistemi = '' OR CHARINDEX('|' + CAST(systems.SystemID AS VARCHAR(Max)) + '|', @ListaSistemi, 1) > 0)
	  AND ISNULL(nodes.Name, '')LIKE @Stazione
	  AND ISNULL(zones.Name, '')LIKE @Linea
	  AND ISNULL(regions.Name, '')LIKE @Compartimento
	  AND ISNULL(H.Duration, 2147483647) >= @DurataMin
	  AND ISNULL(H.Duration, 2147483647) <= @DurataMax
	  AND CHARINDEX('|' + CAST(ISNULL(H.SevLevel, 0)AS varchar(max)) + '|', @ListaSevLevel, 0) > 0
	  AND ISNULL(H.DevType, '') LIKE @TipoPeriferica
	ORDER BY regions.Name, zones.Name, nodes.Name, H.DevType, H.DevID, H.Created;
END;
﻿CREATE PROCEDURE [dbo].[util_UpdStateSeverity]
@PesoOk DECIMAL (5, 2), @PesoWarning DECIMAL (5, 2), @PesoError DECIMAL (5, 2), @SogliaMaxOk SMALLINT, @SogliaMaxWarning SMALLINT
AS
BEGIN
	DECLARE @SogliaMaxError SMALLINT
	SET @SogliaMaxError = CAST(@PesoError AS SMALLINT)
	IF @SogliaMaxError < @PesoError 
		SET @SogliaMaxError = @SogliaMaxError + 1; 

	UPDATE [entity_state] SET [EntityStateSeverity] = @PesoOk, [EntityStateMinValue] = CAST(@PesoOk AS SMALLINT), [EntityStateMaxValue] = @SogliaMaxOk WHERE [EntityStateID] = 1
	UPDATE [entity_state] SET [EntityStateSeverity] = @PesoWarning, [EntityStateMinValue] = @SogliaMaxOk, [EntityStateMaxValue] = @SogliaMaxWarning WHERE [EntityStateID] = 2
	UPDATE [entity_state] SET [EntityStateSeverity] = @PesoError, [EntityStateMinValue] = @SogliaMaxWarning, [EntityStateMaxValue] = @SogliaMaxError WHERE [EntityStateID] = 3
END

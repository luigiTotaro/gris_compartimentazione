﻿


CREATE procedure gris_DelVirtualObject
(
	@ParentObjectStatusId as uniqueidentifier,
	@VirtualObjectName as varchar(500)
) as
begin

	set xact_abort on;

	--declare @ParentObjectStatusId as uniqueidentifier;
	--declare @VirtualObjectName as varchar(500);

	--set @ParentObjectStatusId = '6eee6efe-e5d8-de11-93a6-001e0b6ca3e0';
	--set @VirtualObjectName = 'Fondamentali';

	declare @VirtualObjectId as bigint;
	declare @VirtualObjectStatusId as uniqueidentifier;

	select @VirtualObjectId = vo.VirtualObjectID, @VirtualObjectStatusId = ov.ObjectStatusId
	from object_virtuals ov
	inner join virtual_objects vo on ov.ObjectId = vo.VirtualObjectID
	where ov.ParentObjectStatusId = @ParentObjectStatusId and vo.VirtualObjectName like @VirtualObjectName;

	if @VirtualObjectId is not null and @VirtualObjectStatusId is not null
		begin	
			begin tran
			
			delete from object_status where ObjectStatusId = @VirtualObjectStatusId;
			delete from virtual_objects where VirtualObjectID = @VirtualObjectId;
				
			commit tran
		end

	set xact_abort off;

end
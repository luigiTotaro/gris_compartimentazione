﻿CREATE PROCEDURE [dbo].[tf_GetDeviceAckByRegions] (@RegIDs VARCHAR(MAX), @RegID BIGINT = -1, @ZonID BIGINT = -1, @NodID BIGINT = -1, @DevID BIGINT = -1) AS
SET NOCOUNT ON;

SELECT
device_ack.DeviceAckID,
device_ack.DevID,
device_ack.StrID,
device_ack.FieldID,
device_ack.AckDate,
device_ack.AckDurationMinutes,
device_ack.SupervisorID,
device_ack.Username,
DATEADD(Minute, device_ack.AckDurationMinutes, device_ack.AckDate) AS AckExpiration,
devices.Name AS DeviceName,
nodes.NodID,
nodes.Name AS NodeName,
zones.ZonID,
zones.Name AS ZoneName,
regions.RegID,
regions.Name AS RegionName,
ISNULL(streams.Name, '') AS StreamName,
ISNULL(stream_fields.Name, '') AS StreamFieldName
FROM device_ack
INNER JOIN devices ON device_ack.DevID = devices.DevID
INNER JOIN nodes ON devices.NodID = nodes.NodID
INNER JOIN zones ON nodes.ZonID = zones.ZonID
INNER JOIN regions ON zones.RegID = regions.RegID
LEFT OUTER JOIN stream_fields ON device_ack.DevID = stream_fields.DevID AND device_ack.StrID = stream_fields.StrID AND device_ack.FieldID = stream_fields.FieldID
LEFT OUTER JOIN streams ON device_ack.DevID = streams.DevID AND device_ack.StrID = streams.StrID
WHERE (CHARINDEX('|' + CAST(regions.RegID AS VARCHAR(20)) + '|', @RegIDs, 0) > 0)
AND ((@RegID = -1) OR (regions.RegID = @RegID))
AND ((@ZonID = -1) OR (zones.ZonID = @ZonID))
AND ((@NodID = -1) OR (nodes.NodID = @NodID))
AND ((@DevID = -1) OR (devices.DevID = @DevID))
ORDER BY RegionName, ZoneName, NodeName, DeviceName, streams.StrID, stream_fields.FieldID
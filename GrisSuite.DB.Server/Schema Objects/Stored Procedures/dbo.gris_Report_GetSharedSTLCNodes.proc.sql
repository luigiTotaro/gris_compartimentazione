﻿CREATE PROCEDURE [dbo].[gris_Report_GetSharedSTLCNodes]

AS
SET NOCOUNT ON;

WITH NodesWithStlc AS
(
	SELECT DISTINCT servers.NodID, nodes.Name as NodeName FROM servers INNER JOIN nodes ON nodes.NodID = servers.NodID
)
SELECT DISTINCT 
	regions.Name as Region, zones.Name as Zone, nodes.Name as RemoteNode, servers.FullHostName, servers.IP, ISNULL(NodesWithStlc.NodeName, '') as LocalNode
FROM servers 
INNER JOIN devices ON servers.SrvID = devices.SrvID 
INNER JOIN nodes ON devices.NodID = nodes.NodID
INNER JOIN zones ON zones.ZonID = nodes.ZonID
INNER JOIN regions ON regions.RegID = zones.RegID
LEFT JOIN NodesWithStlc ON NodesWithStlc.NodID = servers.NodID
WHERE servers.SrvID IN 
(
	SELECT servers.SrvID
	FROM servers 
	INNER JOIN devices ON servers.SrvID = devices.SrvID 
	INNER JOIN nodes ON devices.NodID = nodes.NodID
	GROUP BY servers.SrvID
	HAVING COUNT(DISTINCT nodes.NodID) > 1
)
ORDER BY Region, Zone/*, RemoteNode*/, IP;
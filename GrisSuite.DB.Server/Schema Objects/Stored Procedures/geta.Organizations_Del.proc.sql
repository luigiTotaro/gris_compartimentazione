﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[Organizations_Del] 
	@OrganizationID int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE geta.organizations WHERE (OrganizationID = @OrganizationID);
END



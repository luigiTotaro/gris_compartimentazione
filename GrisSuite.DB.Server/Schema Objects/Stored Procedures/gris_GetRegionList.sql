﻿CREATE PROCEDURE gris_GetRegionList(@VisibleRegions NVARCHAR(MAX)) AS
SET NOCOUNT ON;

SELECT DISTINCT
regions.RegID,
REPLACE(regions.Name, 'Compartimento di ', '') AS Name
FROM regions
INNER JOIN zones ON regions.RegID = zones.RegID
INNER JOIN iter_bigintlist_to_tbl(@VisibleRegions) VisibleRegions ON regions.RegID = VisibleRegions.number
ORDER BY Name
﻿CREATE procedure [dbo].[gris_UpdAssociateToVirtualObject]
(
	@VirtualObjectId as bigint,
	@ObjectIds as varchar(max),
	@ObjectTypeId int
) as
begin
	if exists (select VirtualObjectID from virtual_objects where VirtualObjectID = @VirtualObjectId)
	begin 
		update object_status set ParentObjectStatusId = (select ObjectStatusId from object_status where ObjectId = @VirtualObjectId and ObjectTypeId = 7)
		where ObjectStatusId in 
		(
			select os.ObjectStatusId
			from object_status os
			where os.ObjectTypeId = @ObjectTypeId 
			and ((charindex('|' + cast(os.ObjectId as varchar(20)) + '|', @ObjectIds) > 0))		
		);
	end	
end
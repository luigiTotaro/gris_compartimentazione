﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[Organizations_Ins]
	@OrganizationID int OUTPUT,
	@OrganizationName varchar(256)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @OrgID INT
	SELECT @OrgID = OrganizationID FROM geta.organizations WHERE OrganizationName = @OrganizationName;

	IF @OrgID IS NULL
	BEGIN
		INSERT INTO geta.organizations (OrganizationName) VALUES (@OrganizationName);

		SET @OrganizationID = CAST(SCOPE_IDENTITY() AS INT);
	END
END



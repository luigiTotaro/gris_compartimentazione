﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 26-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[Users_Get]
(
	@UserGroupID int = -1,
	@OrganizationID int = -1
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		users.UserID, users.Username, users.FirstName, users.LastName, 
		(users.Username + ISNULL(' (' + users.LastName + ' ' + users.FirstName + ')', '')) as [Name], users.OrganizationID, 
		users.UserGroupID, users.MobilePhone, users.Email, COUNT(geta.issue_users.IssueUserID) as IssuesCount
	FROM users 
	LEFT JOIN geta.issue_users ON users.UserID = geta.issue_users.UserID
	WHERE (UserGroupID = @UserGroupID OR @UserGroupID = -1) AND (OrganizationID = @OrganizationID OR @OrganizationID = -1)
	GROUP BY 
		users.UserID, users.Username, users.FirstName, users.LastName, users.OrganizationID, 
		users.UserGroupID, users.MobilePhone, users.Email
	ORDER BY users.Username;
END



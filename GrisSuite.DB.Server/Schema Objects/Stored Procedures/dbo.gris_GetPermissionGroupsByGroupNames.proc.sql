﻿CREATE PROCEDURE dbo.gris_GetPermissionGroupsByGroupNames
  @GroupNames VARCHAR(MAX)
AS
BEGIN
  SELECT
    GroupID,
    GroupName,
    IsBuiltIn,
    WindowsGroups,
    (CASE WHEN EXISTS(SELECT resources.ResourceID
                      FROM permissions
                        INNER JOIN resources ON permissions.ResourceID = resources.ResourceID
                      WHERE (permissions.GroupID = g.GroupID))
      THEN CAST(1 AS BIT)
     ELSE CAST(0 AS BIT) END) | IsBuiltIn AS Associated,
     dbo.GetContactsByGroupId(GroupID, 'windows_user') AS WindowsUsers
  FROM permission_groups g
  WHERE CHARINDEX('|' + g.GroupName + '|', @GroupNames) > 0
  ORDER BY SortOrder;
END
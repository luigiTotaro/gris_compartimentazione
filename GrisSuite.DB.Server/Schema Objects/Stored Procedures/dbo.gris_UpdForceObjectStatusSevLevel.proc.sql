CREATE PROCEDURE [dbo].[gris_UpdForceObjectStatusSevLevel]
@ObectStatusId UNIQUEIDENTIFIER, @SevLevelDetailId INT
AS
BEGIN
	IF (@SevLevelDetailId IS NULL)
	BEGIN
		UPDATE object_status SET SevLevelDetailId = 5/*Sconosciuto*/, ForcedByUser = 0
		WHERE ObjectStatusId = @ObectStatusId;
	END
	ELSE
	BEGIN
		UPDATE object_status SET SevLevelDetailId = @SevLevelDetailId, ForcedByUser = 1
		WHERE ObjectStatusId = @ObectStatusId;
	END
END
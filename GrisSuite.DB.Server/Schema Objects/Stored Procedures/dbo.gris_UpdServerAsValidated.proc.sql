﻿CREATE PROCEDURE [dbo].[gris_UpdServerAsValidated]
	@SrvID int,
	@ServerSupervisorSystemXMLValidated xml,
	@ClientValidationSign varbinary(128),
	@ServerValidationSign varbinary(128),
	@ServerDateValidationObtained datetime,
	@ClientKey varbinary(148)
AS
	UPDATE servers
	SET 
		ServerSupervisorSystemXMLValidated = @ServerSupervisorSystemXMLValidated,
		ClientSupervisorSystemXMLValidated = @ServerSupervisorSystemXMLValidated,
		ClientValidationSign = @ClientValidationSign, 
		ServerValidationSign = @ServerValidationSign, 
		ServerDateValidationObtained = @ServerDateValidationObtained, 
		ClientKey = @ClientKey
	WHERE (SrvID = @SrvID);



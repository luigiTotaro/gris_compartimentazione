﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 27-06-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[UserGroups_Del] 
	@UserGroupID int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE user_groups WHERE (UserGroupID = @UserGroupID);
END



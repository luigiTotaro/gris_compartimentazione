﻿CREATE PROCEDURE [dbo].[gris_GetObjectFormulaCustomColorsForObjectStatusId] (@ObjectStatusId UNIQUEIDENTIFIER, @FormulaIndex TINYINT) AS
SET NOCOUNT ON;

SELECT
ObjectFormulasCustomColorId,
ObjectStatusId,
FormulaIndex,
AudioSystemSevLevel,
VideoSystemSevLevel,
HtmlColor
FROM object_formulas_custom_colors
WHERE (ObjectStatusId = @ObjectStatusId) AND (FormulaIndex = @FormulaIndex)
ORDER BY AudioSystemSevLevel, VideoSystemSevLevel
﻿CREATE PROCEDURE [dbo].[gris_DelAllbySrvID] 
	@Original_SrvID BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRAN;

	UPDATE [reference]
    SET
        [DeltaFieldID] = NULL,
        [DeltaArrayID] = NULL,
        [DeltaStrID] = NULL,
        [DeltaDevID] = NULL
    WHERE ([DeltaDevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))

    UPDATE [stream_fields]
    SET ReferenceID  = NULL
    WHERE ReferenceID IN (
        SELECT Reference.ReferenceID
        FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
        WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
    )

    DELETE [reference]
    FROM [reference] INNER JOIN [stream_fields] ON [reference].ReferenceID = [stream_fields].ReferenceID
    WHERE ([DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID))

	DELETE FROM device_ack WHERE DevID IN (SELECT DevID FROM devices WHERE SrvID = @Original_SrvID)
    DELETE FROM [stream_fields] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
    DELETE FROM [streams] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
	DELETE FROM [procedures] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
    DELETE FROM [device_status] WHERE [DevID] IN (SELECT DevID FROM [devices] WHERE [SrvID] = @Original_SrvID)
    
    DELETE FROM object_attributes
	WHERE (ComputedByFormulaObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		INNER JOIN devices ON object_status.ObjectId = devices.DevID
		WHERE (object_status.ObjectTypeId = 6)
		AND (devices.SrvID = @Original_SrvID)
	));

	DELETE FROM object_attributes
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		INNER JOIN devices ON object_status.ObjectId = devices.DevID
		WHERE (object_status.ObjectTypeId = 6)
		AND (devices.SrvID = @Original_SrvID)
	));

	DELETE FROM object_formulas_custom_colors
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		INNER JOIN devices ON object_status.ObjectId = devices.DevID
		WHERE (object_status.ObjectTypeId = 6)
		AND (devices.SrvID = @Original_SrvID)
	));

	DELETE FROM object_formulas
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		INNER JOIN devices ON object_status.ObjectId = devices.DevID
		WHERE (object_status.ObjectTypeId = 6)
		AND (devices.SrvID = @Original_SrvID)
	));
	
	DELETE FROM object_status
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		INNER JOIN devices ON object_status.ObjectId = devices.DevID
		WHERE (object_status.ObjectTypeId = 6)
		AND (devices.SrvID = @Original_SrvID)
	));
    
    DELETE FROM [devices] WHERE [SrvID] = @Original_SrvID
    UPDATE [devices] SET PortId = NULL WHERE PortID IN (SELECT PortId FROM [port] WHERE [SrvID] = @Original_SrvID)
    DELETE FROM [port] WHERE [SrvID] = @Original_SrvID
	DELETE FROM [stlc_parameters] WHERE [SrvID] = @Original_SrvID

    DELETE FROM object_attributes
	WHERE (ComputedByFormulaObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		WHERE (object_status.ObjectTypeId = 5)
		and (object_status.ObjectId = @Original_SrvID)
	));

	DELETE FROM object_attributes
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		WHERE (object_status.ObjectTypeId = 5)
		and (object_status.ObjectId = @Original_SrvID)
	));

	DELETE FROM object_formulas_custom_colors
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		WHERE (object_status.ObjectTypeId = 5)
		and (object_status.ObjectId = @Original_SrvID)
	));

	DELETE FROM object_formulas
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		WHERE (object_status.ObjectTypeId = 5)
		and (object_status.ObjectId = @Original_SrvID)
	));
	
	DELETE FROM object_status
	WHERE (ObjectStatusId IN (
		SELECT object_status.ObjectStatusId
		FROM object_status
		WHERE (object_status.ObjectTypeId = 5)
		and (object_status.ObjectId = @Original_SrvID)
	));
	
	UPDATE servers
	SET
		IP = NULL,
		NodID = NULL,
		MAC = NULL,
		IsDeleted = 1
	WHERE ([SrvID] = @Original_SrvID)

	COMMIT TRAN;
END



﻿CREATE PROCEDURE [dbo].[gris_Report_GetDevicesByTypeAndStatus]
(
	@DeviceType varchar(16) = '',
	@SevLevelList varchar(1024) = '|2|'
)
AS
BEGIN

	DECLARE @SevLevelListDesc VARCHAR(1024);

	SELECT @SevLevelListDesc = ISNULL(@SevLevelListDesc, '') + [Description] + '|' FROM severity
	WHERE (@SevLevelList <> '' AND (CHARINDEX('|' + CONVERT(NVARCHAR(5), severity.SevLevel) + '|', @SevLevelList) > 0))

	SELECT DISTINCT
		@DeviceType as ParamDevType, @SevLevelListDesc as ParamSevLevelList,
		regions.RegID, regions.Name AS RegionName, zones.ZonID, zones.Name AS ZoneName, nodes.NodID, nodes.Name AS NodeName, 
		devices.DevID as DeviceID, devices.Name as DeviceName, devices.Type + ISNULL(' - ' + device_type.DeviceTypeDescription, '') as DeviceType,
		object_devices.SevLevel as DevStatus, object_devices.SevLevelDescription as DevStatusDescription, servers.IP as ServerIP
	FROM devices 
	INNER JOIN object_devices ON devices.DevID = object_devices.ObjectID
	INNER JOIN device_type ON device_type.DeviceTypeID = devices.type
	INNER JOIN servers ON devices.SrvID = servers.SrvID
	INNER JOIN nodes ON devices.NodID = nodes.NodID 
	INNER JOIN zones ON nodes.ZonID = zones.ZonID 
	INNER JOIN regions ON zones.RegID = regions.RegID
	WHERE (device_type.DeviceTypeID = @DeviceType OR @DeviceType = '')
	AND (@SevLevelList <> '' AND (CHARINDEX('|' + CONVERT(NVARCHAR(5), object_devices.SevLevel) + '|', @SevLevelList) > 0))
	ORDER BY DevStatus, RegionName, ZoneName, NodeName;
END
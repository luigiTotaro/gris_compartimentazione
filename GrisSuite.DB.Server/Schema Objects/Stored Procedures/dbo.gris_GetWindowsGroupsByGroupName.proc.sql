﻿CREATE PROCEDURE [dbo].[gris_GetWindowsGroupsByGroupName]
	@GroupName VARCHAR(500)
AS
	SELECT WindowsGroups FROM permission_groups WHERE GroupName = @GroupName;
RETURN
CREATE PROCEDURE [dbo].[gris_UpdInMaintenanceServer](@InMaintenance BIT, @Original_SrvID BIGINT)
AS
	DECLARE @ObjectStatusId UNIQUEIDENTIFIER;
	DECLARE @ObjectStatusIdDev UNIQUEIDENTIFIER;
	SELECT @ObjectStatusId = ObjectStatusId FROM object_servers WHERE ObjectId = @Original_SrvID;
	SELECT @ObjectStatusIdDev = ObjectStatusId FROM object_devices WHERE ObjectId IN (SELECT DevID FROM devices WHERE SrvID = @Original_SrvID and Type = 'STLC1000');
	
	UPDATE dbo.object_status
	SET InMaintenance = @InMaintenance
	WHERE ObjectStatusId = @ObjectStatusId;
	
	UPDATE dbo.object_devices
	SET InMaintenance = @InMaintenance
	WHERE ObjectStatusId = @ObjectStatusIdDev;
	
	IF (@InMaintenance = 1)
	BEGIN
		EXEC gris_UpdForceObjectStatusSevLevel @ObjectStatusId, 7; -- Diagnostica disattivata da un operatore
		EXEC gris_UpdForceObjectStatusSevLevel @ObjectStatusIdDev, 7;
	END
	ELSE
	BEGIN
		EXEC gris_UpdForceObjectStatusSevLevel @ObjectStatusId, NULL;
		EXEC gris_UpdForceObjectStatusSevLevel @ObjectStatusIdDev, NULL;
	END
RETURN
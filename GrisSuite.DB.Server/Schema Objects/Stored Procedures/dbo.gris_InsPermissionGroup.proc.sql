﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 03/11/2010
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsPermissionGroup] 
	@GroupName VARCHAR(500), 
	@GroupDescription VARCHAR(2000) = '', 
	@WindowsGroups VARCHAR(MAX) = ''
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SortOrder TINYINT;
	SET @SortOrder = (SELECT MAX(SortOrder) + 1 FROM permission_groups);

	SET @GroupDescription = ISNULL(@GroupDescription, '');
	SET @WindowsGroups = ISNULL(@WindowsGroups, '');

	INSERT INTO permission_groups (GroupName, GroupDescription, IsBuiltIn, WindowsGroups, RegCode, [Level], SortOrder)
	VALUES (@GroupName, @GroupDescription, 0, @WindowsGroups, 0, 0, @SortOrder);
END
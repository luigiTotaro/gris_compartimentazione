﻿CREATE PROCEDURE [dbo].[gris_InsStazioniGenericheFormulaGroup] (@ZonID BIGINT) AS
SET XACT_ABORT ON;
SET NOCOUNT ON;

DECLARE @VirtualObjectId AS BIGINT;
DECLARE @VirtualObjectStatusId AS UNIQUEIDENTIFIER;
DECLARE @ParentZoneStatusId AS UNIQUEIDENTIFIER;

SELECT @ParentZoneStatusId = object_zones.ObjectStatusId
FROM zones
INNER JOIN object_zones ON zones.ZonID = object_zones.ObjectId
WHERE zones.ZonID = @ZonID;

SELECT @VirtualObjectId = vo.VirtualObjectID
FROM object_virtuals ov
INNER JOIN virtual_objects vo ON ov.ObjectId = vo.VirtualObjectID
WHERE ov.ParentObjectStatusId = @ParentZoneStatusId
AND vo.VirtualObjectName LIKE 'Stazioni Generiche';

IF @VirtualObjectId IS NULL
BEGIN
	BEGIN TRAN;
	
	-- creazione virtual object
	EXEC gris_InsVirtualObject @ParentZoneStatusId, 'Stazioni Generiche', 'Stazioni Generiche', @VirtualObjectId OUTPUT, @VirtualObjectStatusId OUTPUT;

	-- disassociazione dei nodi dalla linea e associazione al virtual object			
	UPDATE object_status SET ParentObjectStatusId = @VirtualObjectStatusId
	WHERE ObjectTypeId = 3 /* nodi */ AND ParentObjectStatusId = @ParentZoneStatusId;
	
	-- creazione formule di default per il raggruppamento
	INSERT INTO object_formulas VALUES (@VirtualObjectStatusId, 0, '.\Lib\GroupScripts\GetWeightedAverageMaxWarning.py', 0);

	-- aggiornamento formula della linea padre
	UPDATE object_formulas SET ScriptPath = '.\Lib\GroupScripts\GetWorst.py' WHERE ObjectStatusId = @ParentZoneStatusId AND FormulaIndex = 0

	COMMIT TRAN;
END
﻿CREATE PROCEDURE [dbo].[gris_GetContactsByRegId] (@RegIds NVARCHAR(MAX), @Search NVARCHAR(MAX)) AS
BEGIN
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    contacts.ContactId,
    contacts.RegId,
    contacts.Name                                                       AS ContactName,
    contacts.PhoneNumber,
    contacts.Email,
    contacts.WindowsUser,
    regions.Name                                                        AS RegionName,
    dbo.IsContactIdAssociatedToNotificationContacts(contacts.ContactId) AS IsContactIdAssociatedToNotificationContacts
  FROM contacts
    INNER JOIN regions ON contacts.RegId = regions.RegID
  WHERE (CHARINDEX('|' + CAST(contacts.RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
    AND (@Search = '%%'
        OR contacts.name like @Search
        OR regions.name like @Search
        OR contacts.Email like @Search
        )
  ORDER BY contacts.RegId, contacts.Name, contacts.PhoneNumber
END
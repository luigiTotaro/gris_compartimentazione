﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	Device in offline da più di n minuti
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsRuleDevicesOffline]
	@RuleID int = 0, 
	@Minute int = 0
AS
BEGIN
	WITH LastOffline 
	AS (
	SELECT MAX(device_status_offlinechange.Created) AS  [LastOffLineDate], device_status_offlinechange.DevID
	FROM device_status_offlinechange
		INNER JOIN device_status ON device_status_offlinechange.DevId = device_status.DevId
								 AND device_status_offlinechange.Offline = device_status.Offline
	WHERE (device_status_offlinechange.Offline = 1)
	GROUP BY  device_status_offlinechange.DevID, device_status_offlinechange.Offline
	),
	LastDeviceUpdate
	AS (
	SELECT servers.LastUpdate, devices.DevId, devices.[Type]
	FROM devices INNER JOIN servers ON devices.SrvID = servers.SrvID
	)
	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT DISTINCT LastDeviceUpdate.DevID as DevId, @RuleID AS RuleID
	FROM LastDeviceUpdate INNER JOIN LastOffline ON LastDeviceUpdate.DevID = LastOffline.DevID
	WHERE DATEDIFF(minute,ISNULL(LastOffLineDate,'1900-01-01'),ISNULL(LastUpdate,'1900-01-01'))  > @Minute
		 AND [Type] <> 'STLC1000'
END



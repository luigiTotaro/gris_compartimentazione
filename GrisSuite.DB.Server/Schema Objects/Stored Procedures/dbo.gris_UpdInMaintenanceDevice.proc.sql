﻿CREATE PROCEDURE [dbo].[gris_UpdInMaintenanceDevice](@InMaintenance BIT, @DevId BIGINT)
AS

	DECLARE @SrvId AS BIGINT
	DECLARE @DevType AS VARCHAR(16)
	
	SELECT @SrvId = SrvID, @DevType = [Type] FROM devices WHERE DevID = @DevId 

	IF (@DevType <> 'STLC1000' ) -- non è un STLC1000
		BEGIN
			--- Non settare il flag maintenance della periferica se STLC è già in Maintenance
			IF ( NOT EXISTS (SELECT ObjectId FROM object_servers WHERE ObjectId = @SrvId AND SevLevel = 9 ))
			BEGIN
				UPDATE dbo.object_devices
				SET InMaintenance = @InMaintenance
				WHERE ObjectId = @DevId
			END 	
		END
	ELSE
		BEGIN
			EXEC dbo.gris_UpdInMaintenanceServer @InMaintenance, @SrvId
		END
RETURN
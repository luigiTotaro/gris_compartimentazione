﻿
CREATE procedure [dbo].[gris_DelImportantiFormulaGroup]
(
	@SystemID as int,
	@NodID as bigint
) as
begin

	set xact_abort on;

	--declare @SystemID as int;
	--declare @NodID as bigint;

	--set @SystemID = 1;
	--set @NodID = 2933574139911;

	declare @VirtualObjectId as bigint;
	declare @VirtualObjectStatusId as uniqueidentifier;
	declare @NodeSystemId as bigint;
	declare @ParentNodeSystemStatusId as uniqueidentifier;
	declare @VirtualObjectIdGenerici as bigint;

	select @NodeSystemId = ns.NodeSystemsId, @ParentNodeSystemStatusId = ObjectStatusId
	from node_systems ns
	inner join object_node_systems ons on ns.NodeSystemsId = ons.ObjectId
	where ns.NodId = @NodID and ns.SystemId = @SystemID;

	select @VirtualObjectId = vo.VirtualObjectID, @VirtualObjectStatusId = ov.ObjectStatusId
	from object_virtuals ov
	inner join virtual_objects vo on ov.ObjectId = vo.VirtualObjectID
	where ov.ParentObjectStatusId = @ParentNodeSystemStatusId
	and vo.VirtualObjectName like 'Importanti';
	
	select @VirtualObjectIdGenerici = vo.VirtualObjectID
	from object_virtuals ov
	inner join virtual_objects vo on ov.ObjectId = vo.VirtualObjectID
	where ov.ParentObjectStatusId = @ParentNodeSystemStatusId
	and vo.VirtualObjectName like 'Generici';

	if @VirtualObjectId is not null and @VirtualObjectStatusId is not null
		begin	
			begin tran
			
			-- cancellazione attributi del raggruppamento
			delete from object_attributes where ObjectStatusId = @VirtualObjectStatusId;
			
			-- cancellazione formule di default per il raggruppamento
			delete from object_formulas where ObjectStatusId = @VirtualObjectStatusId;
			
			-- periferiche da spostare sotto il gruppo dei generici
			declare @importantiFormulaGroupDevices as varchar(max);
			
			select @importantiFormulaGroupDevices = COALESCE(@importantiFormulaGroupDevices + '|', '') + CAST(ObjectId as varchar(20))
			from object_status
			where ParentObjectStatusId = @VirtualObjectStatusId;

			-- riversamento di tutte le periferiche associate nel gruppo 'Generici'
			set @importantiFormulaGroupDevices = '|' + @importantiFormulaGroupDevices + '|';
			exec gris_UpdAssociateToVirtualObject @VirtualObjectIdGenerici, @importantiFormulaGroupDevices, 6 /* Devices */;
			
			-- cancellazione virtual object
			exec gris_DelVirtualObject @ParentNodeSystemStatusId, 'Importanti';
				
			commit tran
		end

	set xact_abort off;

end
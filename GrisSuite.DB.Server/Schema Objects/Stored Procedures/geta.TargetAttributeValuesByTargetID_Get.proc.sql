﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 06-08-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [geta].[TargetAttributeValuesByTargetID_Get] 
	@TargetID BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TargetValueID, TargetAttributeID, TargetID, [Value]
	FROM geta.target_values
	WHERE (TargetID = @TargetID)
END



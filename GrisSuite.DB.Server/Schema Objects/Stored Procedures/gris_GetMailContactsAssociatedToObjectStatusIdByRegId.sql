﻿CREATE PROCEDURE [dbo].[gris_GetMailContactsAssociatedToObjectStatusIdByRegId] (@ObjectStatusId UNIQUEIDENTIFIER,
                                                                               @RegIds         NVARCHAR(MAX),
                                                                               @Search         NVARCHAR(MAX)) AS
BEGIN
  SET NOCOUNT ON;

  IF (@Search IS NULL OR RTRIM(@Search) = '') BEGIN
    SET @Search = ''
  END
  SET @Search = '%' + RTRIM(@Search) + '%'

  SELECT
    @ObjectStatusId AS ObjectStatusId,
    notification_contacts.NotificationContactId,
    notification_contacts.ContactId,
    contacts.Name   AS ContactName,
    contacts.PhoneNumber,
    contacts.Email,
    CONVERT(BIT, 1) AS Associated,
    regions.Name    AS RegionName
  FROM notification_contacts
    INNER JOIN contacts ON notification_contacts.ContactId = contacts.ContactId
    INNER JOIN regions ON regions.RegID = contacts.RegId
  WHERE (notification_contacts.ObjectStatusId = @ObjectStatusId)

  UNION ALL

  SELECT
    @ObjectStatusId     AS ObjectStatusId,
    CONVERT(BIGINT, -1) AS NotificationAddresseeId,
    ContactId,
    contacts.Name       AS ContactName,
    PhoneNumber,
    Email,
    CONVERT(BIT, 0)     AS Associated,
    regions.Name    AS RegionName
  FROM contacts
    INNER JOIN regions ON contacts.RegId = regions.RegID
  WHERE (CHARINDEX('|' + CAST(contacts.RegId AS VARCHAR(20)) + '|', @RegIds, 0) > 0)
        AND (ContactId NOT IN (
              SELECT notification_contacts.ContactId
              FROM notification_contacts
              WHERE (notification_contacts.ObjectStatusId = @ObjectStatusId)
            )
        )
        AND ISNULL(Email, '') <> ''
        AND (@Search = '%%'
           OR regions.Name LIKE @Search
           OR contacts.Name LIKE @Search)
  ORDER BY Associated DESC, contacts.Name, contacts.PhoneNumber
END
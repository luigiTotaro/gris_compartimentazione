﻿
CREATE procedure gris_DelGenericiFormulaGroup
(
	@SystemID as int,
	@NodID as bigint
) as
begin

	set xact_abort on;

	--declare @SystemID as int;
	--declare @NodID as bigint;

	--set @SystemID = 1;
	--set @NodID = 2933574139911;

	declare @VirtualObjectId as bigint;
	declare @VirtualObjectStatusId as uniqueidentifier;
	declare @NodeSystemId as bigint;
	declare @ParentNodeSystemStatusId as uniqueidentifier;

	select @NodeSystemId = ns.NodeSystemsId, @ParentNodeSystemStatusId = ObjectStatusId
	from node_systems ns
	inner join object_node_systems ons on ns.NodeSystemsId = ons.ObjectId
	where ns.NodId = @NodID and ns.SystemId = @SystemID;

	select @VirtualObjectId = vo.VirtualObjectID, @VirtualObjectStatusId = ov.ObjectStatusId
	from object_virtuals ov
	inner join virtual_objects vo on ov.ObjectId = vo.VirtualObjectID
	where ov.ParentObjectStatusId = @ParentNodeSystemStatusId
	and vo.VirtualObjectName like 'Generici';

	if @VirtualObjectId is not null and @VirtualObjectStatusId is not null
		begin	
			begin tran
			
			-- cancellazione attributi del raggruppamento
			delete from object_attributes where ObjectStatusId = @VirtualObjectStatusId;
			
			-- cancellazione formule di default per il raggruppamento
			delete from object_formulas where ObjectStatusId = @VirtualObjectStatusId;
			
			-- disassociazione delle periferiche dal virtual object e associazione al sistema
			update object_devices set ParentObjectStatusId = @ParentNodeSystemStatusId
			from object_devices
			inner join devices on DevID = ObjectId
			inner join device_type on DeviceTypeID = [Type]
			where ParentObjectStatusId = @VirtualObjectStatusId
			and SystemID = @SystemID; -- constraint per evitare spostamenti di periferiche con parent inconsistente
				
			-- cancellazione virtual object
			exec gris_DelVirtualObject @ParentNodeSystemStatusId, 'Generici';

			-- aggiornamento formula del sistema padre
			update object_formulas set ScriptPath = '.\Lib\NodeSystemDefaultStatus.py' where ObjectStatusId = @ParentNodeSystemStatusId and FormulaIndex = 0;

			-- aggiornamento formula del nodo padre (in questa versione non viene supportata la configurazione con raggruppamenti di sistemi, il nodo deve essere il padre diretto del sistema)
			update object_formulas set ScriptPath = '.\Lib\NodeDefaultStatus.py' where ObjectStatusId = (select ParentObjectStatusId from object_status where ObjectStatusId = @ParentNodeSystemStatusId) and FormulaIndex = 0;
				
			commit tran
		end

	set xact_abort off;

end
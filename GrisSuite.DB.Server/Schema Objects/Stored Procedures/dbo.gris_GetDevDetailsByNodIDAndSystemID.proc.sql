﻿CREATE PROCEDURE [dbo].[gris_GetDevDetailsByNodIDAndSystemID] (@NodID BIGINT, @SystemID INT, @LastStatusServers VARCHAR (MAX), @VirtualObjectID BIGINT = 0) AS
SET NOCOUNT ON;

IF (ISNULL(@VirtualObjectID, 0) > 0)
BEGIN
	SELECT devices.DevID
	INTO #VD
	FROM virtual_objects
	INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId
	INNER JOIN object_devices ON object_devices.ParentObjectStatusId = object_virtuals.ObjectStatusId
	INNER JOIN devices ON devices.DevID = object_devices.ObjectId
	WHERE virtual_objects.VirtualObjectID = @VirtualObjectID;	

	IF ( @SystemID = 2 )
	BEGIN
		WITH NodeSystemDevicesWithStatus AS
		(
			SELECT devices.DevID, 
				object_devices.SevLevelDetailId,
				object_devices.SevLevelReal as RealSevLevel, RealSeverity.Description as RealSevLevelDescription, 
				object_devices.SevLevelLast as LastSevLevel, LastSeverity.Description as LastSevLevelDescription
			FROM devices
			INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
			INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
			INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN nodes ON nodes.NodID = devices.NodID
			WHERE (nodes.NodID = @NodID)
			AND (device_type.SystemID = @SystemID)
			AND (object_devices.SevLevel <> -1 OR @SystemID = 99)
		), DevicesWithSelectedStatus AS 
		(
			SELECT NodeSystemDevicesWithStatus.DevID,
				-- valore
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevel ELSE NodeSystemDevicesWithStatus.RealSevLevel END as SevLevel,
				-- descrizione
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevelDescription ELSE NodeSystemDevicesWithStatus.RealSevLevelDescription END as SevLevelDescription,
				SevLevelDetailId,
				-- flag ultimo stato
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as LastSevLevelReturned,
				
				CASE WHEN SevLevelDetailId = 7 OR SevLevelDetailId = 9  THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as OperatorMaintenance
			FROM NodeSystemDevicesWithStatus
			INNER JOIN devices ON devices.DevID = NodeSystemDevicesWithStatus.DevID
		)
		SELECT 
			-- topografia
			regions.Name as Region, zones.Name as Zone, nodes.Name AS Node,
			-- periferica
			devices.DevID, devices.Name,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS Type,
			devices.SN as DeviceSerialNumber,
			(
				CASE
					WHEN port.PortType LIKE 'TCP Client' THEN dbo.GetIPStringFromInt(devices.Addr)
					WHEN port.PortType LIKE 'TCP_Client' THEN dbo.GetIPStringFromInt(devices.Addr)
				ELSE
					CAST(devices.Addr AS VARCHAR(15))
				END
			) as [Address],	
			-- stato
			SevLevel,
			dbo.GetCustomSeverityDescription (SevLevel, SevLevelDetailId, SevLevelDescription) AS SevLevelDescription,
			SevLevelDetailId as SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
			-- tipo
			device_type.SystemID,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 4, devices.DevID) AS WSUrlPattern,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) AS VendorName,
			-- porte
			port.PortName, port.PortType, port.Status as PortStatus,
			-- topografia locale
			ISNULL(station.StationName, '') AS StationName, ISNULL(building.BuildingName, '') AS BuildingName, ISNULL(building.BuildingDescription, '') AS BuildingDescription, ISNULL(rack.RackName, '') AS RackName, ISNULL(rack.RackType, '') AS RackType, ISNULL(rack.RackDescription, '') AS RackDescription,
			-- server
			servers.Name AS STLC1000Name, servers.Host AS STLC1000, servers.FullHostName as STLC1000FullHostName, servers.IP as STLC1000IP, servers.LastUpdate as STLC1000LastUpdate, servers.LastMessageType as STLC1000LastMessageType,
			-- ticket
			ISNULL((SELECT alerts_tickets.TicketName FROM alerts_tickets WHERE (DevID = devices.DevID) AND IsOpen = 1 AND ISNULL(TicketName, '') <> ''),'') as Ticket, -- il fatto che esista una sola device in questa condizione DOVREBBE essere mantenuto a livello applicativo.
			ISNULL(devices.RackPositionRow, 0) AS RackPositionRow,
			ISNULL(devices.RackPositionCol, 0) AS RackPositionCol,
			devices.SrvID,
			devices.Type AS RawType,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 8, devices.DevID) AS DeviceImage,
			dbo.GetSeverityLevelForObject (regions.RegID, NULL, NULL) AS SevLevelRegion,
			dbo.GetSeverityLevelForObject (NULL, zones.ZonID, NULL) AS SevLevelZone,
			dbo.GetSeverityLevelForObject (NULL, NULL, Nodes.NodID) AS SevLevelNode,
			dbo.GetAttributeForObject(8 /* Color */, NULL, regions.RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, zones.ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, Nodes.NodID, NULL, NULL, NULL, NULL) AS NodeColor,
			dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectName,
			dbo.GetVirtualObjectDataForObject (3 /* VirtualObjectDescription */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectDescription,
			dbo.GetVirtualObjectDataForObject (4 /* IsObjectStatusIdComputedByCustomFormula */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectIsComputedByCustomFormula
		FROM devices
		INNER JOIN DevicesWithSelectedStatus ON devices.DevID = DevicesWithSelectedStatus.DevID
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
		INNER JOIN nodes ON nodes.NodID = devices.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		INNER JOIN servers ON devices.SrvID = servers.SrvID
		INNER JOIN #VD ON #VD.DevID = DevicesWithSelectedStatus.DevID
		LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID 
		LEFT JOIN rack ON devices.RackID = rack.RackID
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID
		ORDER BY dbo.GetVirtualObjectCustomOrder(dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL), 0), SevLevel DESC, devices.Name ASC;
	END
	ELSE
	BEGIN
		WITH NodeSystemDevicesWithStatus AS
		(
			SELECT devices.DevID, 
				object_devices.SevLevelDetailId,
				object_devices.SevLevelReal as RealSevLevel, RealSeverity.Description as RealSevLevelDescription, 
				object_devices.SevLevelLast as LastSevLevel, LastSeverity.Description as LastSevLevelDescription
			FROM devices
			INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
			INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
			INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN nodes ON nodes.NodID = devices.NodID
			WHERE (nodes.NodID = @NodID)
			AND (device_type.SystemID = @SystemID)
			AND (object_devices.SevLevel <> -1 OR @SystemID = 99)
		), DevicesWithSelectedStatus AS 
		(
			SELECT NodeSystemDevicesWithStatus.DevID,
				-- valore
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevel ELSE NodeSystemDevicesWithStatus.RealSevLevel END as SevLevel,
				-- descrizione
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevelDescription ELSE NodeSystemDevicesWithStatus.RealSevLevelDescription END as SevLevelDescription,
				SevLevelDetailId,
				-- flag ultimo stato
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as LastSevLevelReturned,
				CASE WHEN SevLevelDetailId = 7 OR SevLevelDetailId = 9  THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as OperatorMaintenance
			FROM NodeSystemDevicesWithStatus
			INNER JOIN devices ON devices.DevID = NodeSystemDevicesWithStatus.DevID
		)
		SELECT 
			-- topografia
			regions.Name as Region, zones.Name as Zone, nodes.Name AS Node,
			-- periferica
			devices.DevID, devices.Name,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS Type,
			devices.SN as DeviceSerialNumber,
			(
				CASE
					WHEN port.PortType LIKE 'TCP Client' THEN dbo.GetIPStringFromInt(devices.Addr)
					WHEN port.PortType LIKE 'TCP_Client' THEN dbo.GetIPStringFromInt(devices.Addr)
				ELSE
					CAST(devices.Addr AS VARCHAR(15))
				END
			) as [Address],	
			-- stato
			SevLevel,
			dbo.GetCustomSeverityDescription (SevLevel, SevLevelDetailId, SevLevelDescription) AS SevLevelDescription,
			SevLevelDetailId as SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
			-- tipo
			device_type.SystemID,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 4, devices.DevID) AS WSUrlPattern,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) AS VendorName,
			-- porte
			port.PortName, port.PortType, port.Status as PortStatus,
			-- topografia locale
			ISNULL(station.StationName, '') AS StationName, ISNULL(building.BuildingName, '') AS BuildingName, ISNULL(building.BuildingDescription, '') AS BuildingDescription, ISNULL(rack.RackName, '') AS RackName, ISNULL(rack.RackType, '') AS RackType, ISNULL(rack.RackDescription, '') AS RackDescription,
			-- server
			servers.Name AS STLC1000Name, servers.Host AS STLC1000, servers.FullHostName as STLC1000FullHostName, servers.IP as STLC1000IP, servers.LastUpdate as STLC1000LastUpdate, servers.LastMessageType as STLC1000LastMessageType,
			-- ticket
			ISNULL((SELECT alerts_tickets.TicketName FROM alerts_tickets WHERE (DevID = devices.DevID) AND IsOpen = 1 AND ISNULL(TicketName, '') <> ''),'') as Ticket, -- il fatto che esista una sola device in questa condizione DOVREBBE essere mantenuto a livello applicativo.
			ISNULL(devices.RackPositionRow, 0) AS RackPositionRow,
			ISNULL(devices.RackPositionCol, 0) AS RackPositionCol,
			devices.SrvID,
			devices.Type AS RawType,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 8, devices.DevID) AS DeviceImage,
			dbo.GetSeverityLevelForObject (regions.RegID, NULL, NULL) AS SevLevelRegion,
			dbo.GetSeverityLevelForObject (NULL, zones.ZonID, NULL) AS SevLevelZone,
			dbo.GetSeverityLevelForObject (NULL, NULL, Nodes.NodID) AS SevLevelNode,
			dbo.GetAttributeForObject(8 /* Color */, NULL, regions.RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, zones.ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, Nodes.NodID, NULL, NULL, NULL, NULL) AS NodeColor,
			dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectName,
			dbo.GetVirtualObjectDataForObject (3 /* VirtualObjectDescription */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectDescription,
			dbo.GetVirtualObjectDataForObject (4 /* IsObjectStatusIdComputedByCustomFormula */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectIsComputedByCustomFormula
		FROM devices
		INNER JOIN DevicesWithSelectedStatus ON devices.DevID = DevicesWithSelectedStatus.DevID
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
		INNER JOIN nodes ON nodes.NodID = devices.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		INNER JOIN servers ON devices.SrvID = servers.SrvID
		INNER JOIN #VD ON #VD.DevID = DevicesWithSelectedStatus.DevID
		LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID 
		LEFT JOIN rack ON devices.RackID = rack.RackID
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID
		ORDER BY dbo.GetVirtualObjectCustomOrder(dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL), 0), [Order], devices.Name;
	END
	
	DROP TABLE #VD;
END
ELSE
BEGIN
	IF ( @SystemID = 2 )
	BEGIN
		WITH NodeSystemDevicesWithStatus AS
		(
			SELECT devices.DevID, 
				object_devices.SevLevelDetailId,
				object_devices.SevLevelReal as RealSevLevel, RealSeverity.Description as RealSevLevelDescription, 
				object_devices.SevLevelLast as LastSevLevel, LastSeverity.Description as LastSevLevelDescription
			FROM devices
			INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
			INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
			INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN nodes ON nodes.NodID = devices.NodID
			WHERE (nodes.NodID = @NodID)
			AND (device_type.SystemID = @SystemID)
			AND (object_devices.SevLevel <> -1 OR @SystemID = 99)
		), DevicesWithSelectedStatus AS 
		(
			SELECT NodeSystemDevicesWithStatus.DevID,
				-- valore
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevel ELSE NodeSystemDevicesWithStatus.RealSevLevel END as SevLevel,
				-- descrizione
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevelDescription ELSE NodeSystemDevicesWithStatus.RealSevLevelDescription END as SevLevelDescription,
				SevLevelDetailId,
				-- flag ultimo stato
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as LastSevLevelReturned,
				
				CASE WHEN SevLevelDetailId = 7 OR SevLevelDetailId = 9  THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as OperatorMaintenance
			FROM NodeSystemDevicesWithStatus
			INNER JOIN devices ON devices.DevID = NodeSystemDevicesWithStatus.DevID
		)
		SELECT 
			-- topografia
			regions.Name as Region, zones.Name as Zone, nodes.Name AS Node,
			-- periferica
			devices.DevID, devices.Name,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS Type,
			devices.SN as DeviceSerialNumber,
			(
				CASE
					WHEN port.PortType LIKE 'TCP Client' THEN dbo.GetIPStringFromInt(devices.Addr)
					WHEN port.PortType LIKE 'TCP_Client' THEN dbo.GetIPStringFromInt(devices.Addr)
				ELSE
					CAST(devices.Addr AS VARCHAR(15))
				END
			) as [Address],	
			-- stato
			SevLevel,
			dbo.GetCustomSeverityDescription (SevLevel, SevLevelDetailId, SevLevelDescription) AS SevLevelDescription,
			SevLevelDetailId as SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
			-- tipo
			device_type.SystemID,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 4, devices.DevID) AS WSUrlPattern,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) AS VendorName,
			-- porte
			port.PortName, port.PortType, port.Status as PortStatus,
			-- topografia locale
			ISNULL(station.StationName, '') AS StationName, ISNULL(building.BuildingName, '') AS BuildingName, ISNULL(building.BuildingDescription, '') AS BuildingDescription, ISNULL(rack.RackName, '') AS RackName, ISNULL(rack.RackType, '') AS RackType, ISNULL(rack.RackDescription, '') AS RackDescription,
			-- server
			servers.Name AS STLC1000Name, servers.Host AS STLC1000, servers.FullHostName as STLC1000FullHostName, servers.IP as STLC1000IP, servers.LastUpdate as STLC1000LastUpdate, servers.LastMessageType as STLC1000LastMessageType,
			-- ticket
			ISNULL((SELECT alerts_tickets.TicketName FROM alerts_tickets WHERE (DevID = devices.DevID) AND IsOpen = 1 AND ISNULL(TicketName, '') <> ''),'') as Ticket, -- il fatto che esista una sola device in questa condizione DOVREBBE essere mantenuto a livello applicativo.
			ISNULL(devices.RackPositionRow, 0) AS RackPositionRow,
			ISNULL(devices.RackPositionCol, 0) AS RackPositionCol,
			devices.SrvID,
			devices.Type AS RawType,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 8, devices.DevID) AS DeviceImage,
			dbo.GetSeverityLevelForObject (regions.RegID, NULL, NULL) AS SevLevelRegion,
			dbo.GetSeverityLevelForObject (NULL, zones.ZonID, NULL) AS SevLevelZone,
			dbo.GetSeverityLevelForObject (NULL, NULL, Nodes.NodID) AS SevLevelNode,
			dbo.GetAttributeForObject(8 /* Color */, NULL, regions.RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, zones.ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, Nodes.NodID, NULL, NULL, NULL, NULL) AS NodeColor,
			dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectName,
			dbo.GetVirtualObjectDataForObject (3 /* VirtualObjectDescription */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectDescription,
			dbo.GetVirtualObjectDataForObject (4 /* IsObjectStatusIdComputedByCustomFormula */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectIsComputedByCustomFormula
		FROM devices
		INNER JOIN DevicesWithSelectedStatus ON devices.DevID = DevicesWithSelectedStatus.DevID
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
		INNER JOIN nodes ON nodes.NodID = devices.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		INNER JOIN servers ON devices.SrvID = servers.SrvID 
		LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID 
		LEFT JOIN rack ON devices.RackID = rack.RackID
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID
		ORDER BY dbo.GetVirtualObjectCustomOrder(dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL), 0), SevLevel DESC, devices.Name ASC;
	END
	ELSE
	BEGIN
		WITH NodeSystemDevicesWithStatus AS
		(
			SELECT devices.DevID, 
				object_devices.SevLevelDetailId,
				object_devices.SevLevelReal as RealSevLevel, RealSeverity.Description as RealSevLevelDescription, 
				object_devices.SevLevelLast as LastSevLevel, LastSeverity.Description as LastSevLevelDescription
			FROM devices
			INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
			INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
			INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			INNER JOIN nodes ON nodes.NodID = devices.NodID
			WHERE (nodes.NodID = @NodID)
			AND (device_type.SystemID = @SystemID)
			AND (object_devices.SevLevel <> -1 OR @SystemID = 99)
		), DevicesWithSelectedStatus AS 
		(
			SELECT NodeSystemDevicesWithStatus.DevID,
				-- valore
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevel ELSE NodeSystemDevicesWithStatus.RealSevLevel END as SevLevel,
				-- descrizione
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN NodeSystemDevicesWithStatus.LastSevLevelDescription ELSE NodeSystemDevicesWithStatus.RealSevLevelDescription END as SevLevelDescription,
				SevLevelDetailId,
				-- flag ultimo stato
				CASE WHEN (@LastStatusServers <> '' AND charindex('|' + CAST(devices.SrvID AS VARCHAR(20)) + '|', @LastStatusServers) > 0)
				THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as LastSevLevelReturned,
				CASE WHEN SevLevelDetailId = 7 OR SevLevelDetailId = 9  THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as OperatorMaintenance
			FROM NodeSystemDevicesWithStatus
			INNER JOIN devices ON devices.DevID = NodeSystemDevicesWithStatus.DevID
		)
		SELECT 
			-- topografia
			regions.Name as Region, zones.Name as Zone, nodes.Name AS Node,
			-- periferica
			devices.DevID, devices.Name,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 1, devices.DevID) AS Type,
			devices.SN as DeviceSerialNumber,
			(
				CASE
					WHEN port.PortType LIKE 'TCP Client' THEN dbo.GetIPStringFromInt(devices.Addr)
					WHEN port.PortType LIKE 'TCP_Client' THEN dbo.GetIPStringFromInt(devices.Addr)
				ELSE
					CAST(devices.Addr AS VARCHAR(15))
				END
			) as [Address],	
			-- stato
			SevLevel,
			dbo.GetCustomSeverityDescription (SevLevel, SevLevelDetailId, SevLevelDescription) AS SevLevelDescription,
			SevLevelDetailId as SevLevelDetail, LastSevLevelReturned, OperatorMaintenance,
			-- tipo
			device_type.SystemID,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 4, devices.DevID) AS WSUrlPattern,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 2, devices.DevID) AS VendorName,
			-- porte
			port.PortName, port.PortType, port.Status as PortStatus,
			-- topografia locale
			ISNULL(station.StationName, '') AS StationName, ISNULL(building.BuildingName, '') AS BuildingName, ISNULL(building.BuildingDescription, '') AS BuildingDescription, ISNULL(rack.RackName, '') AS RackName, ISNULL(rack.RackType, '') AS RackType, ISNULL(rack.RackDescription, '') AS RackDescription,
			-- server
			servers.Name AS STLC1000Name, servers.Host AS STLC1000, servers.FullHostName as STLC1000FullHostName, servers.IP as STLC1000IP, servers.LastUpdate as STLC1000LastUpdate, servers.LastMessageType as STLC1000LastMessageType,
			-- ticket
			ISNULL((SELECT alerts_tickets.TicketName FROM alerts_tickets WHERE (DevID = devices.DevID) AND IsOpen = 1 AND ISNULL(TicketName, '') <> ''),'') as Ticket, -- il fatto che esista una sola device in questa condizione DOVREBBE essere mantenuto a livello applicativo.
			ISNULL(devices.RackPositionRow, 0) AS RackPositionRow,
			ISNULL(devices.RackPositionCol, 0) AS RackPositionCol,
			devices.SrvID,
			devices.Type AS RawType,
			dbo.GetDeviceInfoFromStreamsByDeviceType(devices.Type, 8, devices.DevID) AS DeviceImage,
			dbo.GetSeverityLevelForObject (regions.RegID, NULL, NULL) AS SevLevelRegion,
			dbo.GetSeverityLevelForObject (NULL, zones.ZonID, NULL) AS SevLevelZone,
			dbo.GetSeverityLevelForObject (NULL, NULL, Nodes.NodID) AS SevLevelNode,
			dbo.GetAttributeForObject(8 /* Color */, NULL, regions.RegID, NULL, NULL, NULL, NULL, NULL, NULL) AS RegionColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, zones.ZonID, NULL, NULL, NULL, NULL, NULL) AS ZoneColor,
			dbo.GetAttributeForObject(8 /* Color */, NULL, NULL, NULL, Nodes.NodID, NULL, NULL, NULL, NULL) AS NodeColor,
			dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectName,
			dbo.GetVirtualObjectDataForObject (3 /* VirtualObjectDescription */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectDescription,
			dbo.GetVirtualObjectDataForObject (4 /* IsObjectStatusIdComputedByCustomFormula */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL) AS DeviceVirtualObjectIsComputedByCustomFormula
		FROM devices
		INNER JOIN DevicesWithSelectedStatus ON devices.DevID = DevicesWithSelectedStatus.DevID
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
		INNER JOIN nodes ON nodes.NodID = devices.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		INNER JOIN regions ON regions.RegID = zones.RegID
		INNER JOIN servers ON devices.SrvID = servers.SrvID 
		LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID 
		LEFT JOIN rack ON devices.RackID = rack.RackID
		LEFT JOIN building ON rack.BuildingID = building.BuildingID 
		LEFT JOIN station ON building.StationID = station.StationID
		ORDER BY dbo.GetVirtualObjectCustomOrder(dbo.GetVirtualObjectDataForObject (2 /* VirtualObjectName */, NULL, NULL, NULL, NULL, NULL, devices.DevID, NULL), 0), [Order], devices.Name;
	END
END



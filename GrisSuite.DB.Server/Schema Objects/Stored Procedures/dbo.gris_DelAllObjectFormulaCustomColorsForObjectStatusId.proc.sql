﻿CREATE PROCEDURE [dbo].[gris_DelAllObjectFormulaCustomColorsForObjectStatusId] (@ObjectStatusId UNIQUEIDENTIFIER, @FormulaIndex TINYINT) AS
SET NOCOUNT ON;

DELETE FROM object_attributes
WHERE (ComputedByFormulaObjectStatusId = @ObjectStatusId)
AND (ComputedByFormulaIndex = @FormulaIndex);

DELETE FROM object_formulas_custom_colors
WHERE (ObjectStatusId = @ObjectStatusId)
AND (FormulaIndex = @FormulaIndex);

DELETE FROM object_formulas
WHERE (ObjectStatusId = @ObjectStatusId)
AND (FormulaIndex = @FormulaIndex);
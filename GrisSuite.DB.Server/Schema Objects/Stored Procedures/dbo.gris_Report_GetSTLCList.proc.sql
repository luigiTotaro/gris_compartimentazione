﻿CREATE PROCEDURE [dbo].[gris_Report_GetSTLCList] (@RegId BIGINT, @ZonId BIGINT, @NodId BIGINT, @Online INT, @InMaintenance INT, @ServerVersionList VARCHAR (1024)='')
AS
	SET NOCOUNT ON;

	SET @RegId = ISNULL(@RegId,-1)
	SET @ZonId = ISNULL(@ZonId,-1)
	SET @NodId = ISNULL(@NodId,-1)
	SET @Online = ISNULL(@Online, -1)
	SET @InMaintenance = ISNULL(@InMaintenance, -1);
	DECLARE @MinuteTimeOut INT

	SET @MinuteTimeOut = dbo.GetServerMinuteTimeOut();

	SELECT 
			ISNULL(regions.Name,'') AS RegionName, 
			ISNULL(zones.Name,'') AS ZoneName, 
			ISNULL(nodes.Name,'') AS NodeName, 
			ISNULL(servers_status.FullHostName,'') AS FullHostName, 
			ISNULL(servers_status.IP,'') AS IP, 
			servers_status.AccountDocValidation, 
			servers_status.DateDocValidation, 
			servers_status.AccountUTValidation, 
			servers_status.DateUTValidation, 
			servers_status.SrvID, 
			ISNULL(servers_status.Name,'') as ServerName, 
			ISNULL(servers_status.Host,'') AS Host, 
			servers_status.LastUpdate, 
			ISNULL(servers_status.LastMessageType,'') AS LastMessageType, 
			CASE WHEN (servers_status.SevLevel = 9) THEN 1 ELSE 0 END as InMaintenance,
			ISNULL(stlc_parameters.ParameterValue, '') AS ServerVesion, 
			ISNULL(servers_status.MAC,'') as MAC,
			CASE WHEN (servers_status.SevLevelReal <> 3) THEN 1 ELSE 0 END as IsOnline,
			ISNULL(regions.RegID,0)as RegID,
			ISNULL(zones.ZonID,0) as ZonID,
			ISNULL(nodes.NodID,0) as NodID,
			(SELECT COUNT(DISTINCT NodID) FROM devices WHERE SrvID = servers_status.SrvID GROUP BY SrvID) AS NodeCount
	FROM servers_status
	LEFT JOIN stlc_parameters ON servers_status.SrvID = stlc_parameters.SrvID AND stlc_parameters.ParameterName = 'STLCVersion'
	INNER JOIN nodes ON servers_status.NodID = nodes.NodID 
	LEFT JOIN zones ON nodes.ZonID = zones.ZonID 
	LEFT JOIN regions ON zones.RegID = regions.RegID
	WHERE (@RegId = -1 OR (ISNULL(regions.RegId,0) = @RegId))
		AND (@ZonId = -1 OR (ISNULL(zones.ZonID,0) = @ZonId))	
		AND (@NodID = -1 OR (ISNULL(nodes.NodID,0) = @NodID))	
		AND	(@InMaintenance = -1 
				OR (@InMaintenance = 1 AND servers_status.SevLevel = 9)
				OR (@InMaintenance = 0 AND servers_status.SevLevel <> 9)
			)
		AND (@Online = -1
				OR (@Online = 1 AND servers_status.SevLevelReal <> 3) 
				OR (@Online = 0 AND servers_status.SevLevelReal = 3)
			)
		AND (@ServerVersionList = '' -- prende tutto
				OR (CHARINDEX('|-1|', @ServerVersionList) > 0 AND ISNULL(stlc_parameters.ParameterValue, '') <> '') -- � stata flaggata l'opzione 'tutte le versioni'
				OR (CHARINDEX('|-2|', @ServerVersionList) > 0 AND ISNULL(stlc_parameters.ParameterValue, '') = '') -- � stata flaggata l'opzione 'senza versione'
				OR (CHARINDEX('|' + CONVERT(NVARCHAR(10), stlc_parameters.ParameterValue) + '|', @ServerVersionList) > 0)
			)
		AND ((SELECT COUNT(devices.DevID) FROM devices WHERE devices.SrvID = servers_status.SrvID) > 0)
	ORDER BY regions.Name, zones.Name, nodes.Name,  servers_status.FullHostName;


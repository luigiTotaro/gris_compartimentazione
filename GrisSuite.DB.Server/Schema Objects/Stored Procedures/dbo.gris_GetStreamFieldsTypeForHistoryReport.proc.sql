﻿CREATE PROCEDURE gris_GetStreamFieldsTypeForHistoryReport
AS
BEGIN
	SELECT stream_fields_type.FieldTypeId
		 , Description = device_type.DeviceTypeID + '/' + device_type.DefaultName + '/' + stream_fields_type.StreamName + '/' + stream_fields_type.FieldName
	FROM device_type INNER JOIN stream_fields_type ON device_type.DeviceTypeID = stream_fields_type.DevType
	WHERE IsVisibleOnHistoryReportFilter = 1
	ORDER BY Description;
END;
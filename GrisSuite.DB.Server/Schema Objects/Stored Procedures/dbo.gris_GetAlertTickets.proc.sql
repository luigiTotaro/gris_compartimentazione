-- =============================================
-- Author:		Cristian Storti
-- Create date: 09-10-2008
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[gris_GetAlertTickets] 
(
	@TicketName VARCHAR(30) = '',
	@New BIT = 1, 
	@Processed BIT = 1,
	@Discarded BIT = 1,
	@SrvID INT = -1,
	@AlertRuleID INT = -1, 
	@CreationDate DATETIME = NULL,
	@StartRowIndex INT = 0,
	@MaximumRows INT = 20
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EndRowIndex INT;

	SET @CreationDate = ISNULL(@CreationDate, GETDATE());
	SET @TicketName = ISNULL(@TicketName, '') + '%';
	SET @EndRowIndex = @StartRowIndex + @MaximumRows;
	SET @StartRowIndex = @StartRowIndex + 1;
	SET @MaximumRows = @MaximumRows + 1; -- restituisco una riga in +, perché funzioni la paginazione custom

	WITH MainResultset
	AS
	(
		SELECT
			ROW_NUMBER() OVER (ORDER BY alerts_tickets.CreationDate DESC) as RowNumber,
			nodes.Name as NodeName, servers.Name as ServerName, device_type.SystemID, RegID, AlertTicketID, alerts_tickets.DevID, devices.Name as DeviceName, 
			TicketName, TicketNote, TicketDate, alerts_tickets.IsOpen, alerts_tickets.CreationDate, alerts_tickets.CloseDate,
			(CASE 
				WHEN(@New = 1 AND alerts_tickets.IsOpen = 1 AND ISNULL(TicketName, '') = '') THEN 'NEW' 
				WHEN(@Processed = 1 AND alerts_tickets.IsOpen = 1 AND ISNULL(TicketName, '') <> '') THEN 'PROCESSED'
				WHEN(@Discarded = 1 AND alerts_tickets.IsOpen = 0) THEN 'DISCARDED'
			END) as AlertType,
			alerts_rules.RuleID, RuleSevLevel
		FROM alerts_tickets
		INNER JOIN alerts_devices_logs ON alerts_tickets.DevID = alerts_devices_logs.DevID 
			AND alerts_devices_logs.CreationDate >= alerts_tickets.CreationDate
			AND alerts_devices_logs.CreationDate <= ISNULL(alerts_tickets.CloseDate, '99991231')
		INNER JOIN alerts_rules ON alerts_rules.RuleID = alerts_devices_logs.RuleID
		INNER JOIN devices ON devices.DevID = alerts_tickets.DevID -- non devo mostrare allarmi per periferiche che non esistono più
		INNER JOIN device_status ON devices.DevID = device_status.DevID
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN servers_status servers ON servers.SrvID = devices.SrvID
		INNER JOIN nodes ON nodes.NodID = devices.NodID
		INNER JOIN zones ON zones.ZonID = nodes.ZonID
		WHERE 
			(FLOOR(CAST(alerts_tickets.CreationDate AS INT)) = FLOOR(CAST(@CreationDate AS INT))) AND
			(@TicketName = '%' OR TicketName LIKE @TicketName) AND -- evito di permettere ricerche per '%'
			((@New = 1 AND alerts_tickets.IsOpen = 1 AND ISNULL(TicketName, '') = '') OR (@Processed = 1 AND alerts_tickets.IsOpen = 1 AND ISNULL(TicketName, '') <> '') OR (@Discarded = 1 AND alerts_tickets.IsOpen = 0)) AND
			(servers.InMaintenance = 0) AND (servers.SrvID = @SrvID OR @SrvID = -1) AND
			(alerts_devices_logs.RuleID = @AlertRuleID OR @AlertRuleID = -1) AND
			(device_status.SevLevel <> 255)
	),
	AlertAggregation
	AS
	(
		SELECT
				NodeName, ServerName, SystemID, RegID, DevID, DeviceName,
				AlertTicketID, AlertType,
				TicketName, TicketNote, TicketDate, 
				IsOpen, CreationDate, CloseDate, MAX(RuleSevLevel) as RuleSevLevel
		FROM MainResultset
		--WHERE RowNumber >= @StartRowIndex AND RowNumber <= @EndRowIndex
		GROUP BY 
				NodeName, ServerName, SystemID, RegID, DevID, DeviceName,
				AlertTicketID, AlertType,
				TicketName, TicketNote, TicketDate, 
				IsOpen, CreationDate, CloseDate
	)
	SELECT
			NodeName, ServerName, SystemID, RegID, DevID, DeviceName,
			AlertTicketID, AlertType,
			TicketName, TicketNote, TicketDate, 
			IsOpen, CreationDate, CloseDate, RuleSevLevel,	
			(SELECT COUNT(DISTINCT AlertTicketID) FROM MainResultset) as AlertTotalCount
	FROM AlertAggregation
	ORDER BY CreationDate DESC;

END
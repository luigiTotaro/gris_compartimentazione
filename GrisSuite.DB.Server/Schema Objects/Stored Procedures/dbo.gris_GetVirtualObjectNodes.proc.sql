﻿CREATE PROCEDURE [dbo].[gris_GetVirtualObjectNodes] (@VirtualObjectID BIGINT) AS
SET NOCOUNT ON;
	
SELECT nodes.NodID, nodes.Name, nodes.Meters
FROM virtual_objects
INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId
INNER JOIN object_nodes ON object_nodes.ParentObjectStatusId = object_virtuals.ObjectStatusId
INNER JOIN nodes ON nodes.NodID = object_nodes.ObjectId
WHERE virtual_objects.VirtualObjectID = @VirtualObjectID
ORDER BY nodes.Meters, nodes.Name;
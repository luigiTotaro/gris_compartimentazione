﻿CREATE PROCEDURE [dbo].[gris_UpdParameters]
(
	@ParameterName varchar(64),
	@ParameterValue varchar(256),
	@ParameterDescription varchar(1024),
	@Original_ParameterName varchar(64)
)
AS
	SET NOCOUNT OFF;
	UPDATE [parameters] 
	SET [ParameterName] = @ParameterName, 
		[ParameterValue] = @ParameterValue, 
		[ParameterDescription] = @ParameterDescription 
	WHERE [ParameterName] = @Original_ParameterName;
	
	SELECT ParameterName, ParameterValue, ParameterDescription FROM [parameters] WHERE (ParameterName = @ParameterName)
﻿CREATE PROCEDURE gris_GetSystemList(@NodIDs NVARCHAR(MAX)) AS
SET NOCOUNT ON;

SELECT DISTINCT
systems.SystemID,
systems.SystemDescription
FROM node_systems
INNER JOIN nodes ON node_systems.NodId = nodes.NodID
INNER JOIN devices ON nodes.NodID = devices.NodID
INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID AND node_systems.SystemId = device_type.SystemID
INNER JOIN systems ON node_systems.SystemId = systems.SystemID
INNER JOIN iter_bigintlist_to_tbl(@NodIDs) SelectedNodes ON nodes.NodID = SelectedNodes.number
ORDER BY systems.SystemID
﻿CREATE PROCEDURE [dbo].[gris_UpdForceMultipleObjectStatusSevLevel]
@ObectStatusIds VARCHAR(MAX), @SevLevelDetailId INT
AS
BEGIN
	IF (@SevLevelDetailId IS NULL)
	BEGIN
		UPDATE object_status SET SevLevelDetailId = 5/*Sconosciuto*/, ForcedByUser = 0
		WHERE CHARINDEX('|' + CAST(ObjectStatusId AS VARCHAR(50)) + '|', @ObectStatusIds) > 0;
	END
	ELSE
	BEGIN
		UPDATE object_status SET SevLevelDetailId = @SevLevelDetailId, ForcedByUser = 1
		WHERE CHARINDEX('|' + CAST(ObjectStatusId AS VARCHAR(50)) + '|', @ObectStatusIds) > 0;
	END
END
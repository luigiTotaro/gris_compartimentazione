﻿-- =============================================
-- Author:		Luca Quintarelli
-- Create date: 27/10/2008
-- Description:	STLC1000 in offline da più di n minuti
-- =============================================
CREATE PROCEDURE [dbo].[gris_InsRuleSTLC1000Offline]
	@RuleID int = 0,
	@Minute int = 0
AS
BEGIN
    DECLARE	@DateAlertLimit datetime
	SET @DateAlertLimit = DATEADD(minute,@Minute * -1, GETDATE())

	INSERT INTO alerts_devices (DevID, RuleID)
	SELECT devices.DevID, @RuleID as RuleID
	FROM device_status 
		INNER JOIN devices ON device_status.DevId = devices.DevId
		INNER JOIN servers ON servers.SrvId = devices.SrvId
	WHERE  devices.[Type] = 'STLC1000' and device_status.Offline = 1
	GROUP BY  devices.DevID
	HAVING MAX(servers.LastUpdate) <= @DateAlertLimit;

END



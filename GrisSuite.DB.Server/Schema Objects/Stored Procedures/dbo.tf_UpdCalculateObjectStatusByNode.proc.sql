CREATE PROCEDURE [dbo].[tf_UpdCalculateObjectStatusByNode]
@NodId BIGINT
AS
SET NOCOUNT ON;

DECLARE @MinuteTimeOut INT
SET @MinuteTimeOut = dbo.GetServerMinuteTimeOut();

-- Calcola lo stato dei servers
UPDATE object_servers
SET   SevLevelDetailId = dbo.GetServerStatusDetail(ObjectId, @MinuteTimeOut, 0)
	, SevLevelDetailIdReal = dbo.GetServerStatusDetail(ObjectId, @MinuteTimeOut, 1)
WHERE ObjectId In (SELECT DISTINCT SrvId FROM devices WHERE NodID = @NodId )
	AND ForcedByUser = 0;
	
UPDATE object_servers
SET   SevLevel = (SELECT sd.SevLevel FROM severity_details sd WHERE sd.SevLevelDetailId = object_servers.SevLevelDetailId)
	, SevLevelReal = (SELECT sd.SevLevel FROM severity_details sd WHERE sd.SevLevelDetailId = object_servers.SevLevelDetailIdReal)
WHERE ObjectId In (SELECT DISTINCT SrvId FROM devices WHERE NodID = @NodId );

-- Calcola lo stato dei devices (basato sullo stato dei servers)
UPDATE object_devices
SET SevLevelDetailId = dbo.GetDeviceStatusDetail(ObjectId, 0, 0)
WHERE ObjectId In (SELECT DISTINCT DevId FROM devices WHERE NodID = @NodId )
	AND ForcedByUser = 0;

UPDATE object_devices
SET SevLevel = (SELECT sd.SevLevel FROM severity_details sd WHERE sd.SevLevelDetailId = object_devices.SevLevelDetailId)
WHERE ObjectId In (SELECT DISTINCT DevId FROM devices WHERE NodID = @NodId );

UPDATE object_devices
SET SevLevelDetailIdReal = dbo.GetDeviceStatusDetail(ObjectId, 1, 0)
WHERE ObjectId In (SELECT DISTINCT DevId FROM devices WHERE NodID = @NodId )
	AND ForcedByUser = 0;

UPDATE object_devices
SET SevLevelReal = (SELECT sd.SevLevel FROM severity_details sd WHERE sd.SevLevelDetailId = object_devices.SevLevelDetailIdReal)
WHERE ObjectId In (SELECT DISTINCT DevId FROM devices WHERE NodID = @NodId );

UPDATE object_devices
SET SevLevelDetailIdLast = dbo.GetDeviceStatusDetail(ObjectId, 1, 1)
WHERE ObjectId In (SELECT DISTINCT DevId FROM devices WHERE NodID = @NodId )
	AND ForcedByUser = 0;

UPDATE object_devices
SET SevLevelLast = (SELECT sd.SevLevel FROM severity_details sd WHERE sd.SevLevelDetailId = object_devices.SevLevelDetailIdLast)
WHERE ObjectId In (SELECT DISTINCT DevId FROM devices WHERE NodID = @NodId );

-- Imposta i SevLevels delle devices di tipo STLC1000 uguale al SevLevel dei servers corrispondenti,
-- escludendo gli STLC1000 che sono periferiche reali (con monitoraggio SNMP)
UPDATE object_status 
SET SevLevel = object_servers_inservice.SevLevel 
	, SevLevelReal = object_servers_inservice.SevLevelReal
	, SevLevelDetailId = object_servers_inservice.SevLevelDetailId
	, SevLevelDetailIdReal = object_servers_inservice.SevLevelDetailIdReal
FROM object_status
INNER JOIN devices ON devices.DevID = object_status.ObjectId
INNER JOIN object_servers_inservice ON object_servers_inservice.ObjectId = devices.SrvID
WHERE (devices.Type = 'STLC1000') AND (devices.NodID = @NodId)
AND ((devices.Addr = '0') OR (devices.PortId IS NULL))

-- Calcola lo stato dei sistemi (basato sullo stato dei devices)
UPDATE object_node_systems
SET SevLevel = c.SevLevel,
Ok = c.Ok,
Warning = c.Warning,
Error = c.Error,
Maintenance = c.Maintenance,
Unknown = c.Unknown,
ForcedSevLevel = c.ForcedSevLevel
FROM dbo.node_systems_status_calc() AS c
INNER JOIN object_node_systems ON object_node_systems.ObjectStatusId = c.ObjectStatusId
WHERE object_node_systems.ObjectId IN (SELECT DISTINCT NodeSystemsId FROM node_systems WHERE NodId = @NodId)

-- Calcola lo stato delle stazioni (basato sullo stato dei sistemi)
UPDATE object_nodes
SET SevLevel = c.SevLevel,
Ok = c.Ok,
Warning = c.Warning,
Error = c.Error,
Maintenance = c.Maintenance,
Unknown = c.Unknown,
ForcedSevLevel = c.ForcedSevLevel
FROM dbo.node_status_calc() AS c
INNER JOIN object_nodes ON object_nodes.ObjectStatusId = c.ObjectStatusId
WHERE object_nodes.ObjectId = @NodId

-- Calcola lo stato delle linee (basato sullo stato delle stazioni)
UPDATE object_zones
SET SevLevel = c.SevLevel,
Ok = c.Ok,
Warning = c.Warning,
Error = c.Error,
Maintenance = c.Maintenance,
Unknown = c.Unknown,
ForcedSevLevel = 0
FROM dbo.object_status_calc(2) AS c
INNER JOIN object_zones ON object_zones.ObjectStatusId = c.ObjectStatusId
WHERE object_zones.ObjectId IN (SELECT ZonId FROM nodes WHERE NodID = @NodId) 

-- Calcola lo stato dei comparitmenti (basato sullo stato delle linee)
UPDATE object_regions
SET SevLevel = c.SevLevel,
Ok = c.Ok,
Warning = c.Warning,
Error = c.Error,
Maintenance = c.Maintenance,
Unknown = c.Unknown,
ForcedSevLevel = 0
FROM dbo.object_status_calc(1) AS c
INNER JOIN object_regions ON object_regions.ObjectStatusId = c.ObjectStatusId
WHERE object_regions.ObjectId IN (SELECT RegId FROM nodes INNER JOIN zones ON nodes.ZonID = zones.ZonID WHERE NodID = @NodId) 

SET NOCOUNT OFF;

RETURN
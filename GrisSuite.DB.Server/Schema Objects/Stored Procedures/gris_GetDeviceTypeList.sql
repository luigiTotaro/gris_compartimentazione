﻿CREATE PROCEDURE gris_GetDeviceTypeList(@NodIDs NVARCHAR(MAX), @SystemIDs NVARCHAR(MAX)) AS
SET NOCOUNT ON;

SELECT DISTINCT
device_type.DeviceTypeID,
device_type.DeviceTypeDescription,
device_type.[Order]
FROM node_systems
INNER JOIN nodes ON node_systems.NodId = nodes.NodID
INNER JOIN devices ON nodes.NodID = devices.NodID
INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID AND node_systems.SystemId = device_type.SystemID
INNER JOIN systems ON node_systems.SystemId = systems.SystemID
INNER JOIN iter_bigintlist_to_tbl(@NodIDs) SelectedNodes ON nodes.NodID = SelectedNodes.number
INNER JOIN iter_bigintlist_to_tbl(@SystemIDs) SelectedSystems ON device_type.SystemID = SelectedSystems.number
ORDER BY device_type.[Order]
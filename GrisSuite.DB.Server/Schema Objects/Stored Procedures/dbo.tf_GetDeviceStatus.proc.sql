﻿CREATE PROCEDURE [dbo].[tf_GetDeviceStatus]
AS
SET NOCOUNT ON;
SELECT DevID, SevLevel, Description, Offline, 0 as IsDeleted, ShouldSendNotificationByEmail
FROM device_status



﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 05/11/2010
-- Description:	
-- =============================================
CREATE PROCEDURE gris_GetResourceGroupAssociation 
	@ResourceID INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		pg.GroupID, GroupName, IsBuiltIn, WindowsGroups
		, (
			CASE WHEN EXISTS(SELECT * FROM [permissions] p WHERE p.GroupID = pg.GroupID AND p.ResourceID = @ResourceID ) 
			THEN CAST(1 as BIT) 
			ELSE CAST(0 as BIT) 
			END
		) as Associated
	FROM permission_groups pg
	ORDER BY Associated DESC, pg.SortOrder;
END
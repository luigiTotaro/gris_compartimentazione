﻿CREATE PROCEDURE [dbo].[util_GetSTLCOffline]
(
	@RegID bigint = -1,
	@ZonID bigint = -1,
	@NodID bigint = -1,
	@IncludeInMaintenance bit = 0
)
AS
	SET NOCOUNT ON;

	WITH LocalizedServers AS
	(
		SELECT 
			object_servers_inservice.ObjectId,
			regions.RegID, regions.Name as RegionName, zones.ZonID, zones.Name as ZoneName, nodes.NodID, nodes.Name as NodeName, 
			servers.SrvID, servers.Name as ServerName, servers.Host, servers.FullHostName, servers.IP, servers.LastUpdate, 
			object_servers_inservice.SevLevel as STLCStatus, object_servers_inservice.SevLevelReal as STLCRealStatus, SevLevelDescription STLCStatusDescription,
			servers.IsDeleted
		FROM object_servers_inservice
		INNER JOIN servers ON servers.SrvID = object_servers_inservice.ObjectId
		LEFT JOIN nodes ON nodes.NodID = servers.NodID
		LEFT JOIN zones ON zones.ZonID = nodes.ZonID 
		LEFT JOIN regions ON regions.RegID = zones.RegID
		WHERE (zones.RegID = @RegID OR @RegID = -1)
		AND (nodes.ZonID = @ZonID OR @ZonID = -1)
		AND (servers.NodID = @NodID OR @NodID = -1)
	)
	SELECT 
		RegID, REPLACE(RegionName, 'Compartimento di ', '') as RegionName, ZonID, REPLACE(ZoneName, 'Linea ', '') as ZoneName, NodID, NodeName, 
		SrvID, ServerName, Host, FullHostName, IP, DATEDIFF(hh, LastUpdate, GETDATE()) as OfflineHours, LastUpdate, STLCStatus, STLCStatusDescription,
		COUNT(ObjectId) OVER() AS STLCCount,
		(
			SELECT COUNT(NodID) -- vengono contate solo le stazioni con un stlc installato fisicamente sul posto (non sono contate le stazioni degli STLC per cui il dato è mancante)
			FROM LocalizedServers
		) AS MonitorizedNodeCount,
		SUM(dbo.GetMonitoredNodesCountByServerId(SrvID)) OVER() AS STLCMonitorizedNodesCount
	FROM LocalizedServers
	WHERE (LocalizedServers.IsDeleted <> 1)
		AND ((LocalizedServers.STLCStatus = 3) OR (@IncludeInMaintenance = 1 AND LocalizedServers.STLCStatus = 9 AND LocalizedServers.STLCStatus = 3))
	GROUP BY 
		RegID, REPLACE(RegionName, 'Compartimento di ', ''), ZonID, REPLACE(ZoneName, 'Linea ', ''), NodID, NodeName, 
		SrvID, ServerName, Host, FullHostName, IP, DATEDIFF(hh, LastUpdate, GETDATE()), LastUpdate, STLCStatus, STLCStatusDescription,
		ObjectId, IsDeleted
	ORDER BY STLCStatus DESC, RegionName, ZoneName, NodeName, FullHostName;



﻿CREATE FUNCTION [dbo].[FormatSeconds] (@SecondiTotali BIGINT)
RETURNS NVARCHAR(MAX) AS  
BEGIN
	DECLARE @Durata NVARCHAR(MAX);
	SET @Durata = '';

	SET @SecondiTotali = ISNULL(@SecondiTotali, 0);

	IF (@SecondiTotali > 0)
	BEGIN
		DECLARE @SecondiMinuto INT;
		SET @SecondiMinuto = 60;

		DECLARE @SecondiOra INT;
		SET @SecondiOra = 3600;

		DECLARE @SecondiGiorno INT;
		SET @SecondiGiorno = 86400;

		DECLARE @Giorni INT;
		SET @Giorni = 0;

		DECLARE @Ore INT;
		SET @Ore = 0;

		DECLARE @Minuti INT;
		SET @Minuti = 0;

		DECLARE @Secondi INT;
		SET @Secondi = 0;

		IF (@SecondiTotali >= @SecondiGiorno)
		BEGIN
			SET @Giorni = @SecondiTotali / @SecondiGiorno;
			SET @SecondiTotali = @SecondiTotali - (@Giorni * @SecondiGiorno);
		END

		IF (@SecondiTotali >= @SecondiOra)
		BEGIN
			SET @Ore = @SecondiTotali / @SecondiOra;
			SET @SecondiTotali = @SecondiTotali - (@Ore * @SecondiOra);
		END

		IF (@SecondiTotali >= @SecondiMinuto)
		BEGIN
			SET @Minuti = @SecondiTotali / @SecondiMinuto;
			SET @SecondiTotali = @SecondiTotali - (@Minuti * @SecondiMinuto);
		END

		IF (@SecondiTotali >= 0)
		BEGIN
			SET @Secondi = @SecondiTotali;
		END

		IF (@Giorni > 0)
		BEGIN
			SET @Durata = CONVERT(NVARCHAR(20), @Giorni) + ' gg ' + dbo.PadCharToString(@Ore, 2, '0') + ':' + dbo.PadCharToString(@Minuti, 2, '0') + ':' + dbo.PadCharToString(@Secondi, 2, '0');
		END
		ELSE
		BEGIN
			SET @Durata = dbo.PadCharToString(@Ore, 2, '0') + ':' + dbo.PadCharToString(@Minuti, 2, '0') + ':' + dbo.PadCharToString(@Secondi, 2, '0');
		END
	END

	RETURN @Durata;
END
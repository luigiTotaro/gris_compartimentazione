﻿CREATE FUNCTION [dbo].[GetDeviceStatusDetail]
(@DevID BIGINT, @IgnoreMaintenance BIT = 0, @IgnoreOffline BIT = 0)
RETURNS INT
AS
BEGIN
	DECLARE @SevLevelDetail INT
	DECLARE @DeviceActive TINYINT
	DECLARE @DeviceOffline BIT
	DECLARE @DeviceSevLevel INT
	DECLARE @ServerSevLevel INT
	DECLARE @ServerSevLevelReal INT
	DECLARE @ObjectInMaintenance BIT
	DECLARE @Description VARCHAR(256)

	SELECT  @DeviceActive = ISNULL(devices.Active, 0)
			, @DeviceSevLevel = ISNULL(device_status.SevLevel, -1)
			, @DeviceOffline = ISNULL(device_status.Offline, 0)
			, @ServerSevLevel = ISNULL(object_servers.SevLevel, 0)
			, @ServerSevLevelReal = ISNULL(object_servers.SevLevelReal, 0)
			, @ObjectInMaintenance = ISNULL(object_devices.InMaintenance, 0)
			, @Description = ISNULL(device_status.Description, '')
	FROM devices
		LEFT JOIN object_servers ON devices.srvid = object_servers.objectid
		LEFT JOIN device_status ON devices.devid = device_status.devid
		LEFT JOIN object_devices ON devices.devid = object_devices.objectid
		LEFT JOIN device_type ON devices.[type] = device_type.deviceTypeId
	WHERE devices.devid = @DevID

	-- lo stato Maintenance Mode va ignorato anche per lo stato del server della periferica,
	-- perché il suo valore concorre alla determinazione dello stato della periferica
	IF ( 1 = @IgnoreMaintenance ) SELECT @ServerSevLevel = @ServerSevLevelReal;

	SET @SevLevelDetail =
	            CASE
					WHEN (@ObjectInMaintenance = 1 AND @IgnoreMaintenance = 0)
						THEN 7 -- Diagnostica disattivata dall'operatore
					WHEN (@DeviceActive = 0) -- disattivazione sull'STLC1000 non mascherabile
						THEN 7 -- Diagnostica disattivata dall'operatore
					WHEN (@ServerSevLevel = 9 AND @IgnoreMaintenance = 0)
						THEN 9 -- Server di monitoraggio in configurazione
					WHEN (@ServerSevLevel = 3 AND @IgnoreOffline = 0)
						THEN 10 -- Server di monitoraggio non raggiungibile (offline)
					WHEN (@DeviceOffline = 1  AND @IgnoreOffline = 0  AND (NOT @Description LIKE '%Diagnostica non raggiungibile%'))
						THEN 5  -- Sconosciuto [Offline uniformato a Sconosciuto, by design]
					WHEN (@DeviceOffline = 1  AND @IgnoreOffline = 0  AND @Description LIKE '%Diagnostica non raggiungibile%')
						THEN 19 -- Diagnostica non raggiungibile
					WHEN (@DeviceSevLevel = 0)
						THEN 0 -- In servizio
					WHEN ((@DeviceSevLevel = 1) AND (@Description LIKE '%Dati SNMP incongruenti%'))
						THEN 18 -- Dati SNMP incongruenti
					WHEN ((@DeviceSevLevel = 1) AND (@Description LIKE '%Diagnostica non disponibile%'))
						THEN 20 -- Diagnostica non disponibile
					WHEN ((@DeviceSevLevel = 1) AND (@Description LIKE '%Dati diagnostici limitati alla sola sezione MIB-II%'))
						THEN 17 -- Dati diagnostici limitati alla sola sezione MIB-II
					WHEN ((@DeviceSevLevel = 1) AND (@Description LIKE '%Dati di monitoraggio incompleti%'))
						THEN 21 -- Dati di monitoraggio incompleti
					WHEN (@DeviceSevLevel = 1)
						THEN 1 -- Anomalia lieve
					WHEN (@DeviceSevLevel = 2)
						THEN 2 -- Anomalia grave
					WHEN ((@DeviceSevLevel = 9) AND (@Description LIKE '%Dati diagnostici limitati alla sola sezione MIB-II%'))
						THEN 17 -- Dati diagnostici limitati alla sola sezione MIB-II
					WHEN ((@DeviceSevLevel = 9) AND (@Description LIKE '%Dati SNMP incongruenti%'))
						THEN 18 -- Dati SNMP incongruenti
					WHEN ((@DeviceSevLevel = 9) AND (@Description LIKE '%InfoStazioni%'))
						THEN 17 -- Dati diagnostici limitati alla sola sezione MIB-II
					WHEN (@DeviceSevLevel = 9)
						THEN 6 -- Diagnostica non disponibile
					WHEN ((@DeviceSevLevel = 255) AND (@Description LIKE '%Non applicabile%'))
						THEN 23 -- Periferica in stato sconosciuto, che contiene solo stream e stream field in stato Informativo
					WHEN (@DeviceSevLevel = 255)
						THEN 5 -- Sconosciuto
					ELSE -1 -- Non classificato
				END

	RETURN ISNULL(@SevLevelDetail, -1)
END
CREATE FUNCTION [dbo].[GetDeviceStatus] (@DevID BIGINT, @GetReal BIT=0)
RETURNS INT
AS
BEGIN
	DECLARE @SevLevel INT
	DECLARE @SevLevelReal INT

	SELECT @SevLevel = SevLevel,  @SevLevelReal = SevLevelReal
	FROM object_devices 
	WHERE ObjectId = @DevID

	IF @GetReal = 0
		SET @SevLevel = @SevLevelReal
	
	RETURN ISNULL(@SevLevel,-1)
	
END
﻿CREATE FUNCTION [IsObjectNodeRelatedToServerInSevLevelDetail]
(@ObjectId BIGINT, @SevLevelDetailId INT)
RETURNS BIT
AS
BEGIN
	-- Verifica se esiste almeno una device associata al nodo passato,
	-- che sia agganciata ad un server nello stato di dettaglio severity indicato
	DECLARE @IsObjectNodeRelatedToServerInSevLevelDetail BIT;
	SET @IsObjectNodeRelatedToServerInSevLevelDetail = 0;
	
	IF EXISTS (
		SELECT node_systems.NodId
		FROM devices
		INNER JOIN nodes ON devices.NodID = nodes.NodID
		INNER JOIN node_systems ON nodes.NodID = node_systems.NodId
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		AND node_systems.SystemId = device_type.SystemID
		INNER JOIN object_servers ON devices.SrvID = object_servers.ObjectId
		WHERE (object_servers.SevLevelDetailId = @SevLevelDetailId)
		AND (node_systems.NodId = @ObjectId)
	)
	BEGIN
		SET @IsObjectNodeRelatedToServerInSevLevelDetail = 1;
	END

	RETURN @IsObjectNodeRelatedToServerInSevLevelDetail;
END
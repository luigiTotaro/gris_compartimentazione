﻿CREATE FUNCTION [geta].[GetOperatorsByIssue] (@IssueID INT, @IncludeRole TINYINT)
RETURNS VARCHAR(MAX) AS  
-- Ritorna la lista degli operatori associati ad una issue, eventualmente con ruolo
BEGIN 
	DECLARE @OperatorsByIssue AS VARCHAR(MAX)

	IF (ISNULL(@IncludeRole, 0) <> 0)
	BEGIN
		SELECT @OperatorsByIssue = COALESCE(@OperatorsByIssue + ', ', '') + COALESCE(users.LastName + ' ' + users.FirstName, users.Username) + ISNULL(' (' + geta.issue_user_roles.IssueUserRoleName + ')', '')
		FROM geta.issue_users
		INNER JOIN geta.issue_user_roles ON geta.issue_users.IssueUserRoleID = geta.issue_user_roles.IssueUserRoleID
		INNER JOIN users ON geta.issue_users.UserID = users.UserID
		WHERE (geta.issue_users.IssueID = @IssueID)
	END
	ELSE
	BEGIN
		SELECT @OperatorsByIssue = COALESCE(@OperatorsByIssue + ', ', '') + COALESCE(users.LastName + ' ' + users.FirstName, users.Username)
		FROM geta.issue_users
		INNER JOIN geta.issue_user_roles ON geta.issue_users.IssueUserRoleID = geta.issue_user_roles.IssueUserRoleID
		INNER JOIN users ON geta.issue_users.UserID = users.UserID
		WHERE (geta.issue_users.IssueID = @IssueID)
	END
	
	RETURN ISNULL(@OperatorsByIssue, '');
END
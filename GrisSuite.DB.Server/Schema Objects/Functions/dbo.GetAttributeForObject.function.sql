﻿CREATE FUNCTION [dbo].[GetAttributeForObject] (@AttributeTypeId SMALLINT, @ObjectStatusId UNIQUEIDENTIFIER, @RegID BIGINT, @ZonID BIGINT, @NodID BIGINT, @NodeSystemsId BIGINT, @SrvID BIGINT, @DevID BIGINT, @VirtualObjectID BIGINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @AttributeValue VARCHAR(MAX);
	SET @AttributeValue = NULL;
	
	IF (ISNULL(@AttributeTypeId, 0) > 0)
	BEGIN
		IF (@ObjectStatusId IS NOT NULL)
		BEGIN
			SET @AttributeValue = (SELECT AttributeValue FROM object_attributes WHERE (ObjectStatusId = @ObjectStatusId) AND (AttributeTypeId = @AttributeTypeId))
		END	
		ELSE IF (ISNULL(@RegID, 0) > 0)
		BEGIN
			SET @AttributeValue = (SELECT object_attributes.AttributeValue FROM object_attributes INNER JOIN object_status ON object_attributes.ObjectStatusId = object_status.ObjectStatusId WHERE (object_status.ObjectId = @RegID) AND (object_status.ObjectTypeId = 1 /* regions */) AND (object_attributes.AttributeTypeId = @AttributeTypeId))
		END
		ELSE IF (ISNULL(@ZonID, 0) > 0)
		BEGIN
			SET @AttributeValue = (SELECT object_attributes.AttributeValue FROM object_attributes INNER JOIN object_status ON object_attributes.ObjectStatusId = object_status.ObjectStatusId WHERE (object_status.ObjectId = @ZonID) AND (object_status.ObjectTypeId = 2 /* zones */) AND (object_attributes.AttributeTypeId = @AttributeTypeId))
		END
		ELSE IF (ISNULL(@NodID, 0) > 0)
		BEGIN
			SET @AttributeValue = (SELECT object_attributes.AttributeValue FROM object_attributes INNER JOIN object_status ON object_attributes.ObjectStatusId = object_status.ObjectStatusId WHERE (object_status.ObjectId = @NodID) AND (object_status.ObjectTypeId = 3 /* nodes */) AND (object_attributes.AttributeTypeId = @AttributeTypeId))
		END
		ELSE IF (ISNULL(@NodeSystemsId, 0) > 0)
		BEGIN
			SET @AttributeValue = (SELECT object_attributes.AttributeValue FROM object_attributes INNER JOIN object_status ON object_attributes.ObjectStatusId = object_status.ObjectStatusId WHERE (object_status.ObjectId = @NodeSystemsId) AND (object_status.ObjectTypeId = 4 /* node_systems */) AND (object_attributes.AttributeTypeId = @AttributeTypeId))
		END
		ELSE IF (ISNULL(@SrvID, 0) > 0)
		BEGIN
			SET @AttributeValue = (SELECT object_attributes.AttributeValue FROM object_attributes INNER JOIN object_status ON object_attributes.ObjectStatusId = object_status.ObjectStatusId WHERE (object_status.ObjectId = @SrvID) AND (object_status.ObjectTypeId = 5 /* servers */) AND (object_attributes.AttributeTypeId = @AttributeTypeId))
		END
		ELSE IF (ISNULL(@DevID, 0) > 0)
		BEGIN
			SET @AttributeValue = (SELECT object_attributes.AttributeValue FROM object_attributes INNER JOIN object_status ON object_attributes.ObjectStatusId = object_status.ObjectStatusId WHERE (object_status.ObjectId = @DevID) AND (object_status.ObjectTypeId = 6 /* devices */) AND (object_attributes.AttributeTypeId = @AttributeTypeId))
		END
		ELSE IF (ISNULL(@VirtualObjectID, 0) > 0)
		BEGIN
			SET @AttributeValue = (SELECT object_attributes.AttributeValue FROM object_attributes INNER JOIN object_status ON object_attributes.ObjectStatusId = object_status.ObjectStatusId WHERE (object_status.ObjectId = @VirtualObjectID) AND (object_status.ObjectTypeId = 7 /* virtual_object */) AND (object_attributes.AttributeTypeId = @AttributeTypeId))
		END								
	END

	RETURN @AttributeValue;
END
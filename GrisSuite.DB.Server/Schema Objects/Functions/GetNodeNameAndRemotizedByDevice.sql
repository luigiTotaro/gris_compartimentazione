﻿CREATE FUNCTION [dbo].[GetNodeNameAndRemotizedByDevice] (@DevID BIGINT)
RETURNS VARCHAR (1024)
AS
BEGIN
	-- Ritorna il nome di una stazione, eventualmente seguito dal nome della stazione che la remotizza, relativo alla periferica passata
	DECLARE @NodeName VARCHAR(1024);
	DECLARE @DeviceNodID BIGINT;
	DECLARE @DeviceServerNodID BIGINT;
	DECLARE @DeviceServerNodeName VARCHAR(1024);

	SELECT @DeviceNodID = nodes.NodID, @NodeName = nodes.Name
	FROM devices
	INNER JOIN nodes ON devices.NodID = nodes.NodID
	WHERE (devices.DevID = @DevID);

	SELECT @DeviceServerNodID = ISNULL(nodes.NodID, 0), @DeviceServerNodeName = ISNULL(nodes.Name, '')
	FROM devices
	INNER JOIN servers ON devices.SrvID = servers.SrvID
	INNER JOIN nodes ON servers.NodID = nodes.NodID
	WHERE (devices.DevID = @DevID);

	IF (@DeviceNodID <> @DeviceServerNodID)
	BEGIN
		-- La periferica è monitorata da un server in un nodo diverso da quello in cui è censita
		-- la device stessa (stazione con almeno una periferica remotizzata)
		SET @NodeName = @NodeName + ' (' + @DeviceServerNodeName + ')';
	END

	RETURN ISNULL(@NodeName, '');
END
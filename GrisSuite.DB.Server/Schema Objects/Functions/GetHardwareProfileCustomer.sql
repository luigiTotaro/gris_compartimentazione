﻿CREATE FUNCTION [dbo].[GetHardwareProfileCustomer] (@HardwareProfile VARCHAR(64))
RETURNS VARCHAR(64) AS
BEGIN
	DECLARE @HardwareProfileCustomer VARCHAR(64);
	SET @HardwareProfile = ISNULL(@HardwareProfile, '');

	IF (CHARINDEX('.', @HardwareProfile) > 0)
	BEGIN
		SET @HardwareProfileCustomer = SUBSTRING(@HardwareProfile, 0, CHARINDEX('.', @HardwareProfile));
	END
	ELSE
	BEGIN
		SET @HardwareProfileCustomer = @HardwareProfile;
	END

	RETURN ISNULL(@HardwareProfileCustomer, '');
END
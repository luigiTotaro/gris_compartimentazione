﻿CREATE FUNCTION [dbo].[GetIntStringFromIPString]
(
	@IP VARCHAR(16)
)
RETURNS VARCHAR(32)
AS
BEGIN
	DECLARE @Addr BIGINT;
	SET @Addr = 0;

	IF (@IP IS NOT NULL)
	BEGIN
		DECLARE @Octet VARCHAR(3);

		IF (CHARINDEX('.', @IP) > 1)
		BEGIN
			SET @Octet = SUBSTRING(@IP, 1, CHARINDEX('.', @IP) - 1);
			SET @Addr = @Addr + CONVERT(BIGINT, @Octet) * POWER(2, 24);
			SET @IP = SUBSTRING(@IP, CHARINDEX('.', @IP) + 1, LEN(@IP) - CHARINDEX('.', @IP));

			IF (CHARINDEX('.', @IP) > 1)
			BEGIN
				SET @Octet = SUBSTRING(@IP, 1, CHARINDEX('.', @IP) - 1);
				SET @Addr = @Addr + CONVERT(BIGINT, @Octet) * POWER(2, 16);
				SET @IP = SUBSTRING(@IP, CHARINDEX('.', @IP) + 1, LEN(@IP) - CHARINDEX('.', @IP));

				IF (CHARINDEX('.', @IP) > 1)
				BEGIN			
					SET @Octet = SUBSTRING(@IP, 1, CHARINDEX('.', @IP) - 1);
					SET @Addr = @Addr + CONVERT(BIGINT, @Octet) * POWER(2, 8);
					SET @IP = SUBSTRING(@IP, CHARINDEX('.', @IP) + 1, LEN(@IP) - CHARINDEX('.', @IP));

					SET @Octet = @IP;
					SET @Addr = @Addr + CONVERT(BIGINT, @Octet);
				END
				ELSE
				BEGIN
					SET @Addr = 0;
				END
			END
			ELSE
			BEGIN
				SET @Addr = 0;
			END
		END
		ELSE
		BEGIN
			SET @Addr = 0;
		END
	END

	RETURN CONVERT(VARCHAR(32), @Addr);
END
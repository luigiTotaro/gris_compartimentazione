﻿CREATE FUNCTION [dbo].[GetMidnightFromDate](@InputDate AS DATETIME)
RETURNS DATETIME AS  
BEGIN 
	DECLARE @MidnightFromDate AS DATETIME;
	SET @MidnightFromDate = CONVERT(DATETIME, CONVERT(NVARCHAR, YEAR(@InputDate)) + '-' + CONVERT(NVARCHAR, MONTH(@InputDate)) + '-' + CONVERT(NVARCHAR, DAY(@InputDate)) + ' 23:59:59', 102);
	
	RETURN @MidnightFromDate;
END
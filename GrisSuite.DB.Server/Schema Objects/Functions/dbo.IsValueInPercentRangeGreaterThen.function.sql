﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 2007-11-08
-- Description:	Calcola se un valore è compreso nell' intervallo superiore definito da una data percentuale rispetto ad un valore di riferimento
-- =============================================
CREATE FUNCTION [dbo].[IsValueInPercentRangeGreaterThen]
(
	@Value DECIMAL(19, 5),
	@ReferenceValue DECIMAL(19, 5),
	@Percent1 SMALLINT,
	@Percent2 SMALLINT
)
RETURNS SMALLINT
AS
BEGIN
	DECLARE @Result SMALLINT
	DECLARE @GDecimalPercent DECIMAL(19, 5)
	DECLARE @LDecimalPercent DECIMAL(19, 5)
	DECLARE @1 DECIMAL(19, 5)
	DECLARE @100 DECIMAL(19, 5)

	SET @1 = 1;
	SET @100 = 100;

	IF ( @Percent1 > 0 ) SET @Percent1 = ABS(@Percent1);
	IF ( @Percent2 > 0 ) SET @Percent2 = ABS(@Percent2);

	IF ( @Percent1 >= @Percent2 )
		BEGIN
			SET @GDecimalPercent = CAST(@Percent1 AS DECIMAL(19, 5));
			SET @LDecimalPercent = CAST(@Percent2 AS DECIMAL(19, 5));
		END
	ELSE
		BEGIN
			SET @GDecimalPercent = CAST(@Percent2 AS DECIMAL(19, 5));
			SET @LDecimalPercent = CAST(@Percent1 AS DECIMAL(19, 5));
		END

	SET @Result = CASE WHEN (
								(@Value <= (@ReferenceValue * (@1 + @GDecimalPercent / @100)))
								AND
								(@Value > (@ReferenceValue * (@1 + @LDecimalPercent / @100)))
							) 
							THEN 1 
							ELSE 0 
							END

	RETURN @Result

END



CREATE FUNCTION [dbo].[IsWithServerZone]
(@ZonId BIGINT)
RETURNS BIT
AS
BEGIN
	DECLARE @WithServers BIT
	
	IF EXISTS (SELECT zones.ZonId 
				FROM zones
				INNER JOIN nodes ON	zones.ZonId = nodes.ZonId
				INNER JOIN devices ON nodes.NodId = devices.NodId 
				LEFT JOIN [servers] ds ON ds.SrvID = devices.SrvID
				LEFT JOIN [servers] ns ON ns.NodID = nodes.NodID
				WHERE zones.ZonId  = @ZonId AND ([Type] = 'STLC1000' OR ds.SrvID IS NOT NULL OR ns.SrvID IS NOT NULL))
	   SET @WithServers = 1;
	ELSE
	   SET @WithServers = 0; 
	   
	RETURN @WithServers;

END
﻿CREATE FUNCTION [dbo].[AreAllObjectsByParentUnknown]
(@ParentObjectId BIGINT, @ParentObjectTypeId INT)
RETURNS BIT
AS
BEGIN
	DECLARE @ObjectsNum SMALLINT;
	DECLARE @UnknownObjectsNum SMALLINT;
	
	SELECT @UnknownObjectsNum = ISNULL(SUM([255]), 0), @ObjectsNum = ISNULL(COUNT(ObjectStatusId), 0)
	FROM
	(
		SELECT child.ObjectStatusId, child.SevLevel
		FROM object_status parent 
		INNER JOIN object_status child ON parent.ObjectStatusId = child.ParentObjectStatusId 
		WHERE parent.ObjectTypeId = @ParentObjectTypeId AND parent.ObjectId = @ParentObjectId
		AND child.SevLevel <> 9 -- nel conteggio non vengono considerati i nodi in configurazione
		AND child.SevLevel <> -1
	) AS A
	PIVOT
	(
	 COUNT(SevLevel)
	 FOR SevLevel
	 IN ([255])
	) AS p;
	
	IF @UnknownObjectsNum <> 0 AND @ObjectsNum = @UnknownObjectsNum RETURN 1;
	RETURN 0;
END
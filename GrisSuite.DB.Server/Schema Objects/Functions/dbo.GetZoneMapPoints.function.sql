﻿CREATE FUNCTION [dbo].[GetZoneMapPoints] (@ZonId bigint)
RETURNS VARCHAR(MAX) AS  
BEGIN 
	DECLARE @points AS VARCHAR(MAX)
	SET @points = ''
	
	SELECT @points = @points + SubString(MapPoints,9,2000000) + '|'
	FROM zones_subzones
	     inner join subzones on zones_subzones.SubZoneId = subzones.SubZoneId
	WHERE zones_subzones.ZonId = @ZonId	     
	
	SET @points = ISNULL(@points, '')
	IF LEN(@points) > 0
		SET @points =  'polyline ' + SUBSTRING(@points, 1, LEN(@points)-1)

	RETURN @points 
END
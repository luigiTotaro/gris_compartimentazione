﻿CREATE FUNCTION dbo.IsObjectStatusIdComputedByCustomFormula (@ObjectStatusId UNIQUEIDENTIFIER, @AttributeTypeId SMALLINT)
RETURNS TINYINT AS
BEGIN
	DECLARE @IsObjectStatusIdComputedByCustomFormula TINYINT;
	SET @IsObjectStatusIdComputedByCustomFormula = 0;
	
	IF EXISTS (
		SELECT AttributeId
		FROM object_attributes
		WHERE (ObjectStatusId = @ObjectStatusId)
		AND (ComputedByFormulaObjectStatusId = ObjectStatusId) -- Verifica che l'attributo sia stato calcolato dalla formula associata all'oggetto stesso
		AND (ComputedByFormulaIndex = 1) -- Formula Custom, non di Default per l'oggetto
		AND (AttributeTypeId = @AttributeTypeId)
	)
	BEGIN
		SET @IsObjectStatusIdComputedByCustomFormula = 1;
	END
	
	RETURN @IsObjectStatusIdComputedByCustomFormula;
END
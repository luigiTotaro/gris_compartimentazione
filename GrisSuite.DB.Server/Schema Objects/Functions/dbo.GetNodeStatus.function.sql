/*CREATE FUNCTION [dbo].[GetNodeStatus]
(@NodID BIGINT, @GetReal BIT = 0)
RETURNS INT
AS
BEGIN

	DECLARE @SevLevel INT;
	DECLARE @NodeSevLevel INT;
	DECLARE @NodeSevLevelDescription VARCHAR(128);
	DECLARE @NodeOffline BIT;
	DECLARE @NodeInExercise BIT;
		
	WITH SevLevelCountTable AS 
	(
		SELECT 
			nodes.NodID, devices.SrvID,
			dbo.GetDeviceStatus(devices.DevID, @GetReal) as SevLevel
		FROM nodes 
		INNER JOIN devices ON nodes.NodID = devices.NodID 
		INNER JOIN device_type ON device_type.DeviceTypeID = devices.Type
		WHERE nodes.NodID = @NodID
		AND device_type.SystemID <> 99 -- Sistema generico, non entra nel calcolo dello stato di una stazione
	)
	SELECT 
		@NodeSevLevel = dbo.GetEntityStateValue(
												SUM(CASE WHEN (SevLevel = 0) THEN CAST(1 AS TINYINT) ELSE CAST(0 AS TINYINT) END),		--OK
												SUM(CASE WHEN (SevLevel = 1) THEN CAST(1 AS TINYINT) ELSE CAST(0 AS TINYINT) END),		--WARNING
												SUM(CASE WHEN (SevLevel = 2) THEN CAST(1 AS TINYINT) ELSE CAST(0 AS TINYINT) END),		--ERROR
												SUM(CASE WHEN (SevLevel = 255) THEN CAST(1 AS TINYINT) ELSE CAST(0 AS TINYINT) END)		--SCONOSCIUTO
												,0
												),
		@NodeInExercise = MAX(CAST(servers.InExercise AS TINYINT)), 
		@NodeOffline = MAX(CASE WHEN (DATEDIFF(n, servers.LastUpdate, GETDATE()) > 60) THEN 1 ELSE 0 END)
	FROM SevLevelCountTable
	INNER JOIN servers ON SevLevelCountTable.SrvID = servers.SrvID;


	SET @SevLevel = CASE 
						WHEN (@NodeInExercise = 0 ) AND (@GetReal = 0) THEN 9
						WHEN (@NodeOffline = 1) AND (@GetReal = 0) THEN 255 -- offline uniformato a Sconosciuto
						WHEN (@NodeSevLevel <> -1) THEN @NodeSevLevel 
						ELSE -1
					END;

	RETURN @SevLevel;
END*/


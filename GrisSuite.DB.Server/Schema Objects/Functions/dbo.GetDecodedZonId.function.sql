CREATE FUNCTION [dbo].[GetDecodedZonId]
(@zonId BIGINT)
RETURNS SMALLINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedZonId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedZonId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedZonId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 35, 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedZonId', NULL, NULL


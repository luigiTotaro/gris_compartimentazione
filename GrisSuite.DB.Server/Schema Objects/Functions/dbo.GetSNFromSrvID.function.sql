﻿CREATE FUNCTION [dbo].[GetSNFromSrvID] (@SrvID INT)
-- Ritorna il numero di serie di un server, dato il Server ID intero
RETURNS VARCHAR(11)
AS
BEGIN
	SET @SrvID = ISNULL(@SrvID, 0);

	DECLARE @SN VARCHAR(11);
	DECLARE @Anno VARCHAR(2);
	DECLARE @Settimana VARCHAR(2);
	DECLARE @Progressivo VARCHAR(5);

	SET @Anno = CONVERT(VARCHAR, (@SrvID & 0xFF000000) / POWER(2, 24));
	SET @Settimana = CONVERT(VARCHAR, (@SrvID & 0x00FF0000) / POWER(2, 16));
	SET @Progressivo = CONVERT(VARCHAR, (@SrvID & 0x0000FFFF));

	IF (LEN(@Anno) < 2)
	BEGIN
		SET @Anno = REPLICATE('0', 2 - LEN(@Anno)) + @Anno;
	END

	IF (LEN(@Settimana) < 2)
	BEGIN
		SET @Settimana = REPLICATE('0', 2 - LEN(@Settimana)) + @Settimana;
	END

	IF (LEN(@Progressivo) < 5)
	BEGIN
		SET @Progressivo = REPLICATE('0', 5 - LEN(@Progressivo)) + @Progressivo;
	END

	SET @SN = @Anno + '.' + @Settimana + '.' + @Progressivo;

	RETURN ISNULL(@SN, '');
END
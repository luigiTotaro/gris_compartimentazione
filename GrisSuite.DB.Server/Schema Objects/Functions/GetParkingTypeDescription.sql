﻿CREATE FUNCTION [dbo].[GetParkingTypeDescription] (@ParkingType TINYINT)
RETURNS VARCHAR(2000) AS
BEGIN
	DECLARE @ParkingTypeDescription VARCHAR(2000);
	SET @ParkingTypeDescription = 'N/D';
	
	IF (@ParkingType = 0)
	BEGIN
		SET @ParkingTypeDescription = 'Sconosciuto';
	END
	ELSE IF (@ParkingType = 1)
	BEGIN
		SET @ParkingTypeDescription = 'Server configurato, ma che non centralizza';
	END
	ELSE IF (@ParkingType = 2)
	BEGIN
		SET @ParkingTypeDescription = 'Server non configurato, che non centralizza';
	END
	ELSE IF (@ParkingType = 3)
	BEGIN
		SET @ParkingTypeDescription = 'Server storico, che ha centralizzato almeno una volta';
	END

	RETURN @ParkingTypeDescription;
END
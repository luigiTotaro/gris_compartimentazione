﻿CREATE FUNCTION [dbo].[fn_EncryptDecryptString] (@pClearString VARCHAR(MAX))
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @vEncryptedString NVARCHAR(MAX);
	DECLARE @vIdx INT;
	DECLARE @vBaseIncrement INT;

	SET @vIdx = 1;
	SET @vBaseIncrement = 128;
	SET @vEncryptedString = '';
	SET @pClearString = ISNULL(@pClearString, '');

	WHILE (@vIdx <= LEN(@pClearString))
	BEGIN
		SET @vEncryptedString = @vEncryptedString + NCHAR(ASCII(SUBSTRING(@pClearString, @vIdx, 1)) ^ 129);
		SET @vIdx = @vIdx + 1;
	END

	RETURN ISNULL(@vEncryptedString, '');
END
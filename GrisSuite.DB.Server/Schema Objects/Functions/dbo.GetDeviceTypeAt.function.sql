﻿CREATE FUNCTION [dbo].[GetDeviceTypeAt] 
(
	@DevID bigint,
	@T as DateTime
)
RETURNS VARCHAR(16)
AS
BEGIN
	DECLARE @DevType VARCHAR(16)

	SELECT TOP (1) @DevType = [Type]
	FROM cache_log_messages 
		INNER JOIN cache_devices ON cache_log_messages.LogID = cache_devices.LogID
	WHERE (cache_devices.DevId = @DevID) 
		AND (cache_log_messages.Created) <=  @T
	order by  cache_log_messages.Created DESC

	IF @DevType IS NULL
		SELECT @DevType = [Type] FROM Devices WHERE DevId = @DevID

	RETURN @DevType

END
﻿CREATE FUNCTION dbo.GetPermissionGroupsByResource (@ResourceID INT)
RETURNS VARCHAR(MAX) AS  
BEGIN 
	DECLARE @PermissionGroupsByResource AS VARCHAR(MAX);

	SELECT @PermissionGroupsByResource = COALESCE(@PermissionGroupsByResource + ',', '') + WindowsGroups
	FROM [permissions]
	INNER JOIN permission_groups ON permission_groups.GroupID = [permissions].GroupID
	WHERE ([permissions].ResourceID = @ResourceID)
	
	RETURN ISNULL(@PermissionGroupsByResource, '');
END
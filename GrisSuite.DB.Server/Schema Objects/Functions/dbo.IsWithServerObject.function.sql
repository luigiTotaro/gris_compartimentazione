﻿CREATE FUNCTION [dbo].[IsWithServerObject](@ObjectStatusId UNIQUEIDENTIFIER)

RETURNS BIT
AS
BEGIN
	DECLARE @WithServers BIT
	DECLARE @ObjectTypeId INT
	DECLARE @ObjectId BIGINT
	
	SELECT @ObjectTypeId = ObjectTypeId
			,@ObjectId = ObjectId
	FROM object_status 
	WHERE object_status.ObjectStatusId = @ObjectStatusId
	 
	SET  @ObjectTypeId = ISNULL(@ObjectTypeId,0)
	SET  @ObjectId = ISNULL(@ObjectId,0)
	
	IF (@ObjectTypeId = 3) --3	nodes
	   SET @WithServers = dbo.IsWithServerNode(@ObjectId)
	ELSE IF (@ObjectTypeId = 2) --2	zones
	   SET @WithServers = dbo.IsWithServerZone(@ObjectId)
	ELSE IF (@ObjectTypeId = 1) --1	regions
		SET @WithServers = dbo.IsWithServerRegion(@ObjectId)
	ELSE
		SET @WithServers = 0;   
	   
	RETURN @WithServers;

END
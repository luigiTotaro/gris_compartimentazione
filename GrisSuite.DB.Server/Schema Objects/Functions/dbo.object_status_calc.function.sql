﻿CREATE FUNCTION [object_status_calc]
(@ObjectTypeId INT)
RETURNS 
    @object_status_cal TABLE (
        [ObjectStatusId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY CLUSTERED ([ObjectStatusId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
        [Ok]						INT NOT NULL,
        [Warning]					INT NOT NULL,
        [Error]						INT NOT NULL,
        [Maintenance]				INT NOT NULL,
        [Unknown]					INT NOT NULL,
		[SevLevel]					INT NOT NULL,
        [ForcedSevLevel]			INT NOT NULL)
AS
BEGIN

	INSERT INTO @object_status_cal (ObjectStatusId, Ok, Warning, Error, Maintenance, Unknown, SevLevel, ForcedSevLevel)
	SELECT id, MAX([0]), MAX([1]), MAX([2]), MAX([9]), MAX([255]), MAX(SevLevel), MAX(ForcedSevLevel)
	FROM
	(
		SELECT object_status.ObjectStatusId as id, 0 as [0], 0 as [1], 0 as [2], 0 as [9], 0 as [255], 255 AS SevLevel, 4 AS ForcedSevLevel
		FROM object_status
		WHERE (dbo.AreAllObjectsByParentUnknown(object_status.ObjectId, @ObjectTypeId) = 1)

		UNION ALL
	
		SELECT id, [0], [1], [2], [9], [255]+[3] as [255], dbo.GetEntityStateValue([0],[1],[2]+[3]+[255],[9]), 0 AS ForcedSevLevel
		FROM 
		(
			SELECT P.ObjectStatusId AS Id, C.SevLevel, C.ObjectStatusId as ChildId
			FROM object_status AS P 
				INNER JOIN object_status AS C ON P.ObjectStatusId = C.ParentObjectStatusId 
			WHERE P.ObjectTypeId = @ObjectTypeId
			AND C.ExcludeFromParentStatus = 0
		) as A
		PIVOT
		(
		 COUNT(ChildId)
		 for SevLevel
		 in ([0], [1], [2], [9], [255], [3])
		) as p
	) AS B
	GROUP BY id
	
	RETURN 
END



﻿CREATE FUNCTION GetRawDeviceCountByNodeAndSystem (@NodID BIGINT, @SystemID INT)
RETURNS INT
AS
BEGIN
	DECLARE @RawDeviceCountByNodeAndSystem INT;
	SET @RawDeviceCountByNodeAndSystem = -1;
	
	WITH NodeSystemDevicesWithStatus AS
	(
		SELECT devices.DevID
		FROM devices
		INNER JOIN object_devices ON devices.DevID = object_devices.ObjectId
		INNER JOIN severity_details RealSeverity ON RealSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdReal
		INNER JOIN severity_details LastSeverity ON LastSeverity.SevLevelDetailId = object_devices.SevLevelDetailIdLast
		INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
		INNER JOIN nodes ON nodes.NodID = devices.NodID
		WHERE (nodes.NodID = @NodID)
		AND (device_type.SystemID = @SystemID)
		AND (object_devices.SevLevel <> -1 OR @SystemID = 99)
	)
	SELECT @RawDeviceCountByNodeAndSystem = COUNT(devices.DevID)
	FROM devices
	INNER JOIN NodeSystemDevicesWithStatus ON devices.DevID = NodeSystemDevicesWithStatus.DevID
	INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
	INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
	INNER JOIN nodes ON nodes.NodID = devices.NodID
	INNER JOIN zones ON zones.ZonID = nodes.ZonID
	INNER JOIN regions ON regions.RegID = zones.RegID
	INNER JOIN servers ON devices.SrvID = servers.SrvID 
	LEFT JOIN port ON devices.PortId = port.PortID AND servers.SrvID = port.SrvID 
	LEFT JOIN rack ON devices.RackID = rack.RackID
	LEFT JOIN building ON rack.BuildingID = building.BuildingID 
	LEFT JOIN station ON building.StationID = station.StationID
	
	RETURN ISNULL(@RawDeviceCountByNodeAndSystem, -1);
END
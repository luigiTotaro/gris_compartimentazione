﻿CREATE FUNCTION [dbo].[GetNodeNameAndRemotized] (@NodID BIGINT)
RETURNS VARCHAR (1024)
AS
BEGIN
	DECLARE @NodeName VARCHAR(1024);

	SELECT @NodeName = nodes.Name
	FROM nodes
	WHERE (nodes.NodID = @NodID);

	-- Verifica se esiste almeno una device per la stazione, monitorata da un server nello stesso nodo
	-- In questo caso, la stazione non è considerata remotizzata, anche se ci sono altre periferiche sotto altri server, in nodi differenti
	-- Una stazione in cui almeno una periferica è monitorata da un server presente nella stazione stessa, non sarà remotizzata
	IF NOT EXISTS (
		SELECT
		servers.NodID
		FROM nodes 
		LEFT JOIN devices ON nodes.NodID = devices.NodID 
		LEFT JOIN servers ON devices.SrvID = servers.SrvID
		LEFT JOIN nodes srvNodes ON srvNodes.NodID = servers.NodID
		WHERE (nodes.NodID = @NodID)
		AND (ISNULL(servers.NodID, 0) = ISNULL(nodes.NodID, 0))
	)
	BEGIN
		DECLARE @Remotizzanti TABLE (NodIDRemotizzante BIGINT, NomeRemotizzante VARCHAR(64));

		INSERT INTO @Remotizzanti
		SELECT
		servers.NodID,
		srvNodes.Name
		FROM nodes 
		LEFT JOIN devices ON nodes.NodID = devices.NodID 
		LEFT JOIN servers ON devices.SrvID = servers.SrvID
		LEFT JOIN nodes srvNodes ON srvNodes.NodID = servers.NodID
		WHERE (nodes.NodID = @NodID)
		AND (ISNULL(servers.NodID, 0) <> ISNULL(nodes.NodID, 0)) -- E' remotizzante se fa parte di un nodo diverso da quello della stazione
		AND (servers.NodID IS NOT NULL) -- Elimina la stazione stessa, nel caso che non abbia nessun STLC
		GROUP BY srvNodes.Name, servers.NodID, nodes.NodID
		HAVING (COUNT(DISTINCT servers.SrvID) > 0); -- Per remotizzare deve contenere almeno un STLC

		DECLARE @NomiRemotizzanti VARCHAR(MAX);

		-- Prevediamo supporto stazioni remotizzanti multiple
		SELECT @NomiRemotizzanti = COALESCE(@NomiRemotizzanti + ', ', '') + NomeRemotizzante
		FROM @Remotizzanti
		ORDER BY NomeRemotizzante;

		SET @NodeName = @NodeName + ISNULL(' (' + @NomiRemotizzanti + ')', '');
	END

	RETURN ISNULL(@NodeName, '');
END
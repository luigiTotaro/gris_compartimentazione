CREATE FUNCTION [dbo].[SetDecodedZonId]
(@decodedRegId SMALLINT, @decodedZonId SMALLINT)
RETURNS BIGINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedZonId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedZonId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedZonId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 26, 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedZonId', NULL, NULL


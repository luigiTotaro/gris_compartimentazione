﻿CREATE FUNCTION [dbo].[IsContactIdAssociatedToNotificationContacts](@ContactId BIGINT)
RETURNS BIT
AS
BEGIN
	DECLARE @IsContactIdAssociatedToNotificationContacts BIT;
	
	SET @IsContactIdAssociatedToNotificationContacts = 0;
	
	IF EXISTS (SELECT NotificationContactId FROM notification_contacts WHERE (ContactId = @ContactId))
	BEGIN
		SET @IsContactIdAssociatedToNotificationContacts = 1;
	END
	
	RETURN @IsContactIdAssociatedToNotificationContacts;
END
﻿CREATE FUNCTION [dbo].[GetTrattaPoints] (@or_id varchar(10))
RETURNS VARCHAR(MAX) AS  
-- Ritorna la lista delle persone in staff associate ad un corso, separata da virgole
BEGIN 
	DECLARE @points AS VARCHAR(MAX)
	SET @points = ''

	SELECT @points = @points + [Y-Coordinate] + ' ' + [X-Coordinate] + ','
	FROM [ext_rete_trate]
	where or_id =  @or_id
	
	SET @points = ISNULL(@points, '')
	IF LEN(@points) > 0
		SET @points =  SUBSTRING(@points, 1, LEN(@points)-1)

	RETURN @points 
END
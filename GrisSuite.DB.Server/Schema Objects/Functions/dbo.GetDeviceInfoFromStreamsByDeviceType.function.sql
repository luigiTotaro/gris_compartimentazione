﻿CREATE FUNCTION [GetDeviceInfoFromStreamsByDeviceType]
(
	@DeviceTypeID VARCHAR(16),
	@InfoType TINYINT,
	@DevID BIGINT
)
RETURNS VARCHAR(512)
AS
BEGIN
	DECLARE @ReturnValue VARCHAR(512);
    DECLARE @VendorData VARCHAR(1024);
    DECLARE @CategoryData VARCHAR(1024);

    IF (@InfoType = 1) -- DeviceTypeID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))

            SET @ReturnValue = @DeviceTypeID;

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'AET0000';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'AEL0000';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SYTM100';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SYLB000';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SOTM000';
                END
                ELSE IF (@CategoryData IN (/* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15'))
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = 'SOCM000';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SOLM000';
                END
            END
        END
        ELSE
        BEGIN
            SET @ReturnValue = @DeviceTypeID;
        END
    END
    ELSE IF (@InfoType = 2) -- VendorName
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))

            SELECT @ReturnValue = vendors.VendorName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = 'Aesys';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = 'Sysco';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = 'Solari';
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = vendors.VendorName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 3) -- VendorID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))
            
            SELECT @ReturnValue = CONVERT(VARCHAR(10), vendors.VendorID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)            

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = '2';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = '1';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = '3';
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = CONVERT(VARCHAR(10), vendors.VendorID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            INNER JOIN vendors ON device_type.VendorID = vendors.VendorID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 4) -- WSUrlPattern
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                SET @ReturnValue = 'http://{0}/icmonitor.wsdl';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                SET @ReturnValue = 'http://{0}/dispositivo.wsdl';
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                SET @ReturnValue = 'http://{0}:8081';
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = device_type.WSUrlPattern
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 5) -- TechnologyID
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = '2';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = '2';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = '2';
                END
                ELSE IF (@CategoryData IN (/* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15'))
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = '1';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = '3';
                END
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = CONVERT(VARCHAR(10), device_type.TechnologyID)
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 6) -- DeviceTypeDescription
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))
            
            SELECT @ReturnValue = device_type.DeviceTypeDescription
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)            

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'AESYS - Teleindicatore TFT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'AESYS - Teleindicatore LED generico';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SYSCO - Teleindicatore TFT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SYSCO - Teleindicatore LED generico';
                END
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
                IF (@CategoryData IN (/* Monitor LCD/TFT 32 pollici */ '0', /* Monitor LCD/TFT 17 pollici (sottopasso) */ '1', /* Monitor TFT 23 pollici per totem (con pilotaggio orologio) */ '12', /* Monitor LCD/TFT 23 pollici */ '16', /* Indicatore di carrozza */ '21', /* Monitor LCD/TFT 23 pollici (sottopasso) */ '26', /* Monitor LCD/TFT 22 pollici */ '27', /* Monitor LCD/TFT 22 pollici (sottopasso) */ '28', /* Monitor TFT 22 pollici per totem (con pilotaggio orologio) */ '33', /* Monitor TFT 42 pollici portrait per totem */ '100', /* Monitor riepilogativo 42 pollici */ '1000', /* Display fascia H800 */ '1001', /* Quadro 55 pollici */ '1002', /* Indicatore di carrozza 22 pollici */ '1003', /* Display da binario 1600 con numero binario variabile */ '1004', /* Tabellone A/P 10+2 righe con intestazione campi variabile */ '1006', /* Display sottopasso stretch 43 pollici */ '1008', /* Display da binario 1200 con numero binario variabile */ '1009'))
                BEGIN
                    -- Categorie TFT
                    SET @ReturnValue = 'SOLARI - Teleindicatore TFT generico';
                END
                ELSE IF (@CategoryData IN (/* Monitor CRT 25 pollici */ '14', /* Monitor CRT 28 pollici */ '15'))
                BEGIN
                    -- Categorie CRT
                    SET @ReturnValue = 'SOLARI - Teleindicatore CRT generico';
                END
                ELSE
                BEGIN
                    -- Categorie LED
                    SET @ReturnValue = 'SOLARI - Teleindicatore LED generico';
                END
            END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = device_type.DeviceTypeDescription
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)
        END
    END
    ELSE IF (@InfoType = 7) -- Decodifica categoria Infostazioni
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
			SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))           

			IF (@CategoryData = '0')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LCD/TFT 32 pollici');
			END
			ELSE IF (@CategoryData = '1')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LCD/TFT 17 pollici (sottopasso)');
			END
			ELSE IF (@CategoryData = '2')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Binario LED doppia faccia');
			END
			ELSE IF (@CategoryData = '3')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Binario LED singola faccia');
			END
			ELSE IF (@CategoryData = '4')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LED 5 righe + 1');
			END
			ELSE IF (@CategoryData = '5')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 1 modulo LED gialli');
			END
			ELSE IF (@CategoryData = '6')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 2 moduli LED gialli');
			END
			ELSE IF (@CategoryData = '7')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 3 moduli LED gialli');
			END
			ELSE IF (@CategoryData = '8')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 4 moduli LED gialli');
			END
			ELSE IF (@CategoryData = '9')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Binario LED doppia faccia con orologio analogico');
			END
			ELSE IF (@CategoryData = '10')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LED 9 righe + 1');
			END
			ELSE IF (@CategoryData = '11')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Tabellone LED arrivi e partenze 10 righe + 2');
			END
			ELSE IF (@CategoryData = '12')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor TFT 23 pollici per totem (con pilotaggio orologio)');
			END
			ELSE IF (@CategoryData = '13')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Binario LED (2 righe) sottopasso');
			END
			ELSE IF (@CategoryData = '14')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor CRT 25 pollici');
			END
			ELSE IF (@CategoryData = '15')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor CRT 28 pollici');
			END
			ELSE IF (@CategoryData = '16')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LCD/TFT 23 pollici');
			END
			ELSE IF (@CategoryData = '17')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Quadro LED 2 righe + 2');
			END
			ELSE IF (@CategoryData = '18')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Quadro LED 4 righe + 2');
			END
			ELSE IF (@CategoryData = '19')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Quadro LED 6');
			END
			ELSE IF (@CategoryData = '20')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Quadro LED 8 righe + 2');
			END
			ELSE IF (@CategoryData = '21')
			BEGIN
				SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Indicatore di carrozza');
			END
			ELSE IF (@CategoryData = '22')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED estesa (1 modulo)');
            END
            ELSE IF (@CategoryData = '23')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED estesa (2 moduli)');
            END
            ELSE IF (@CategoryData = '24')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED estesa (3 moduli)');
            END
            ELSE IF (@CategoryData = '25')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Tabellone LED arrivi e partenze, 8 righe + 2');
            END
            ELSE IF (@CategoryData = '26')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LCD/TFT 23 pollici (sottopasso)');
            END
            ELSE IF (@CategoryData = '27')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LCD/TFT 22 pollici');
            END
            ELSE IF (@CategoryData = '28')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LCD/TFT 22 pollici (sottopasso)');
            END
            ELSE IF (@CategoryData = '29')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 1 modulo LED bianchi');
            END
            ELSE IF (@CategoryData = '30')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 2 moduli LED bianchi');
            END
            ELSE IF (@CategoryData = '31')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 3 moduli LED bianchi');
            END
            ELSE IF (@CategoryData = '32')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED 4 moduli LED bianchi');
            END
            ELSE IF (@CategoryData = '33')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor TFT 22 pollici per totem (con pilotaggio orologio)');
            END
            ELSE IF (@CategoryData = '34')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Tabellone LED arrivi e partenze 10 righe');
            END
            ELSE IF (@CategoryData = '35')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Tabellone LED arrivi e partenze 4 righe + 2');
            END
            ELSE IF (@CategoryData = '100')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor TFT 42 pollici portrait per totem');
            END
            ELSE IF (@CategoryData = '1000')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor riepilogativo 42 pollici');
            END
            ELSE IF (@CategoryData = '1001')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Display fascia H800');
            END
            ELSE IF (@CategoryData = '1002')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Quadro 55 pollici');
            END
            ELSE IF (@CategoryData = '1003')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Indicatore di carrozza 22 pollici');
            END
            ELSE IF (@CategoryData = '1004')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Display da binario 1600 con numero binario variabile');
            END
            ELSE IF (@CategoryData = '1005')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Monitor LED 20 righe');
            END
            ELSE IF (@CategoryData = '1006')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Tabellone A/P 10+2 righe con intestazione campi variabile');
            END
            ELSE IF (@CategoryData = '1007')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Display fascia H400 con intestazione dinamica a led bianchi');
            END
            ELSE IF (@CategoryData = '1008')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Display sottopasso stretch 43 pollici');
            END
            ELSE IF (@CategoryData = '1009')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Display da binario 1200 con numero binario variabile');
            END			
            ELSE IF (@CategoryData = '1010')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia LED-F1 White+Header');
            END
            ELSE IF (@CategoryData = '1011')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia 10 Righe LED');
            END
            ELSE IF (@CategoryData = '1012')
            BEGIN
                SELECT @ReturnValue = CONVERT(VARCHAR(200), 'Fascia 10 Righe LED White con intestazione');
            END
			ELSE
			BEGIN
			    SELECT @ReturnValue = CONVERT(VARCHAR(200), 'N/D');
			END
        END
        ELSE
        BEGIN
            SELECT @ReturnValue = CONVERT(VARCHAR(200), 'N/D');
        END
    END
    ELSE IF (@InfoType = 8) -- Immagine Periferica
    BEGIN
        IF (@DeviceTypeID LIKE 'INFSTAZ1')
        BEGIN
            SET @VendorData = (SELECT Value FROM stream_fields WHERE (FieldID = 0) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))
            SET @CategoryData = (SELECT Value FROM stream_fields WHERE (FieldID = 5) AND (ArrayID = 0) AND (StrID = 2) AND (DevID = @DevID))

            SET @ReturnValue = @DeviceTypeID;

            IF (ISNULL(@VendorData, '') LIKE '%Aesys%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZAE' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Sysco%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZSY' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;					
            END
            ELSE IF (ISNULL(@VendorData, '') LIKE '%Solari%')
            BEGIN
				IF (ISNUMERIC(@CategoryData) = 1)
					SET @ReturnValue = 'INFSTAZSO' + ISNULL(dbo.PadCharToString(@CategoryData, 2, '0'), '');
				ELSE
					SET @ReturnValue = @DeviceTypeID;					
            END
        END
        ELSE
        BEGIN
			SELECT @ReturnValue = device_type.ImageName
            FROM devices
            INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
            WHERE (devices.DevID = @DevID)   
        END
    END     

	RETURN ISNULL(@ReturnValue, '')
END
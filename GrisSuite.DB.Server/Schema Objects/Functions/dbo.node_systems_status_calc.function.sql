﻿CREATE FUNCTION [dbo].[node_systems_status_calc]
( )
RETURNS 
    @object_status_cal TABLE (
        [ObjectStatusId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY CLUSTERED ([ObjectStatusId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
        [Ok]						INT NOT NULL,
        [Warning]					INT NOT NULL,
        [Error]						INT NOT NULL,
        [Maintenance]				INT NOT NULL,
        [Unknown]					INT NOT NULL,
		[SevLevel]					INT NOT NULL,
        [ForcedSevLevel]			INT NOT NULL)
AS
BEGIN
	INSERT INTO @object_status_cal (ObjectStatusId, Ok, Warning, Error, Maintenance, Unknown, SevLevel, ForcedSevLevel)
	SELECT id, MAX([0]), MAX([1]), MAX([2]), MAX([9]), MAX([255]), MAX(SevLevel), MAX(ForcedSevLevel)
	FROM
	(
		SELECT object_node_systems.ObjectStatusId as Id, 0 as [0], 0 as [1], 0 as [2], 0 as [9], 0 as [255], 2 AS SevLevel, 8 AS ForcedSevLevel
		FROM object_node_systems
		WHERE (dbo.IsObjectNodeSystemRelatedToServerInSevLevelDetail(object_node_systems.ObjectId, 22 /* Dati del collettore incompleti */) = 1)
		
		UNION ALL
			
		SELECT object_node_systems.ObjectStatusId as Id, 0 as [0], 0 as [1], 0 as [2], 0 as [9], 0 as [255], 255 AS SevLevel, 2 AS ForcedSevLevel
		FROM object_node_systems inner join object_nodes on object_node_systems.ParentObjectStatusId = object_nodes.ObjectStatusId
		WHERE dbo.AreAllServersByNodeOffline(object_nodes.ObjectId) = 1

		UNION ALL
		
		SELECT object_devices.ParentObjectStatusId as Id,0 as [0], 0 as [1], 0 as [2], 0 as [9], 0 as [255], MAX(CASE WHEN object_devices.SevLevel > 2 THEN 2 ELSE object_devices.SevLevel END) AS SevLevel, 1 AS ForcedSevLevel
		FROM object_devices INNER JOIN devices ON object_devices.ObjectId = devices.DevID 
		WHERE (devices.Type = 'TT10210') OR (devices.Type = 'TT10210V3') OR (devices.Type = 'TT10220')
		AND NOT object_devices.ExcludeFromParentStatus = 1
		GROUP BY object_devices.ParentObjectStatusId
		HAVING MAX(object_devices.SevLevel) IN (1,2,255)

		UNION ALL
	
		SELECT Id, [0], [1], [2], [9], [3]+[255] AS [255], dbo.GetEntityStateValue([0],[1],[2]+[3]+[255],[9]) AS SevLevel, 0 AS ForcedSevLevel
		FROM 
		(
			SELECT P.ObjectStatusId AS Id, C.SevLevel, C.ObjectStatusId as ChildId
			FROM object_status AS P INNER JOIN object_status AS C ON P.ObjectStatusId = C.ParentObjectStatusId 
			WHERE (P.ObjectTypeId = 4)
			AND C.ExcludeFromParentStatus = 0
		) AS A
		PIVOT
		(
		 COUNT(ChildId)
		 FOR SevLevel
		 IN ([0], [1], [2], [9], [255], [3])
		) AS p
	) AS B
	GROUP BY id
	
	RETURN 
END
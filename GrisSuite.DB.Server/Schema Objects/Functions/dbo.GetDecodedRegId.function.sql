CREATE FUNCTION [dbo].[GetDecodedRegId]
(@regId BIGINT)
RETURNS SMALLINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedRegId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedRegId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedRegId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 19, 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedRegId', NULL, NULL


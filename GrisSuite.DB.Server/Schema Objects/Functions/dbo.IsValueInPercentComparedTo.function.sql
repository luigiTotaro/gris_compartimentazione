﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 2007-11-08
-- Description:	Calcola se un valore è compreso nell' intervallo (- e +) definito da una data percentuale rispetto ad un valore di riferimento
-- =============================================
CREATE FUNCTION [dbo].[IsValueInPercentComparedTo]
(
	@Value DECIMAL(19, 5),
	@ReferenceValue DECIMAL(19, 5),
	@Percent SMALLINT
)
RETURNS SMALLINT
AS
BEGIN
	DECLARE @Result SMALLINT
	DECLARE @DecimalPercent DECIMAL(19, 5)
	DECLARE @1 DECIMAL(19, 5)
	DECLARE @100 DECIMAL(19, 5)

	IF ( @Percent > 0 ) SET @Percent = ABS(@Percent);

	SET @1 = 1;
	SET @100 = 100;
	SET @DecimalPercent = CAST(@Percent AS DECIMAL(19, 5));

	SET @Result = CASE WHEN (
								(@Value >= (@ReferenceValue * (@1 - @DecimalPercent / @100)))
								AND 
								(@Value <= (@ReferenceValue * (@1 + @DecimalPercent / @100)))
							) 
							THEN 1 
							ELSE 0 
							END

	RETURN @Result

END



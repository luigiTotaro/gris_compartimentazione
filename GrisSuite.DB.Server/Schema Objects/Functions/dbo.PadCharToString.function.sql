﻿CREATE FUNCTION [dbo].[PadCharToString](@Input VARCHAR(MAX), @OutputLength INT, @PaddingChar CHAR(1))
RETURNS VARCHAR(MAX) AS  
BEGIN 
	IF (LEN(@Input) >= @OutputLength)
		RETURN @Input;
	ELSE
		RETURN REPLICATE(@PaddingChar, @OutputLength - LEN(@Input)) + @Input;

	RETURN @Input;
END
﻿CREATE FUNCTION dbo.GetMonitoredNodesCountByServerId (@SrvID BIGINT)
RETURNS DECIMAL(10, 2) AS
BEGIN
	DECLARE @MonitoredNodesCountByServerId DECIMAL(10, 2);
	DECLARE @NodesCountByServerId DECIMAL(10, 2);
	DECLARE @ServersCountByNodeId DECIMAL(10, 2);
	
	-- Numero di stazioni monitorate dal server
	SET @NodesCountByServerId = CONVERT(DECIMAL(10, 2), (SELECT COUNT(DISTINCT nodes.NodID) FROM devices INNER JOIN nodes ON devices.NodID = nodes.NodID WHERE (devices.SrvID = @SrvID)));
	
	-- Numero di server installati nelle stazioni monitorate
	SET @ServersCountByNodeId = CONVERT(DECIMAL(10, 2), (SELECT COUNT(DISTINCT servers.SrvID) FROM devices INNER JOIN servers ON devices.SrvID = servers.SrvID INNER JOIN nodes ON devices.NodID = nodes.NodID WHERE (nodes.NodID IN (SELECT nodes.NodID FROM devices INNER JOIN nodes ON devices.NodID = nodes.NodID WHERE (devices.SrvID = @SrvID)))));
	
	IF (@ServersCountByNodeId = 0)
	BEGIN
		SET @MonitoredNodesCountByServerId = CONVERT(DECIMAL(10, 2), 0);
	END
	ELSE
	BEGIN
		SET @MonitoredNodesCountByServerId = @NodesCountByServerId / @ServersCountByNodeId;
	END	

	RETURN ISNULL(@MonitoredNodesCountByServerId, CONVERT(DECIMAL(10, 2), 0));
END
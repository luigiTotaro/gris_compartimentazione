﻿CREATE FUNCTION [dbo].[fn_splitstring]
(
    @String NVARCHAR(MAX),
    @Delimiter NCHAR(1)
)
RETURNS TABLE 
AS
RETURN 
(
    WITH Split(stpos,endpos) 
    AS(
        SELECT CONVERT(BIGINT, 0) AS stpos, CHARINDEX(@Delimiter,@String) AS endpos
        UNION ALL
        SELECT CONVERT(BIGINT, endpos+1), CHARINDEX(@Delimiter,@String,endpos+1)
            FROM Split
            WHERE endpos > 0
    )
    SELECT 'Id' = ROW_NUMBER() OVER (ORDER BY (SELECT 1)),
        'Data' = SUBSTRING(@String,stpos,COALESCE(NULLIF(endpos,0),LEN(@String)+1)-stpos)
    FROM Split
)
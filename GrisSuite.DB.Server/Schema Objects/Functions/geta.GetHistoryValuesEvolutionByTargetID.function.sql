﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 06-06-2008
-- Description:	
-- =============================================
CREATE FUNCTION [geta].[GetHistoryValuesEvolutionByTargetID]
(	
	@TargetID int
)
RETURNS TABLE 
AS
RETURN 
(

	WITH HistoricalValues (HistID, TargetValueID, [Value], DateCreation) AS
	(
		SELECT TargetValueHistoryID, geta.target_values.TargetValueID, geta.target_values_history.[Value], DateCreation
		FROM geta.target_values 
		INNER JOIN geta.target_values_history ON geta.target_values.TargetValueID = geta.target_values_history.TargetValueID
		WHERE (geta.target_values.TargetID = @TargetID)

		UNION

		SELECT NULL, TargetValueID, [Value], CAST('99991231' AS DATETIME) -- unisco i valori attuali per la concatenazione delle righe di storico + recenti
		FROM geta.target_values
		WHERE (TargetID = @TargetID)
	), 
	SequentialHistoricalValues AS -- aggiungo i valori di sequenza
	(
		SELECT HistID, TargetValueID, [Value], ROW_NUMBER() OVER ( PARTITION BY TargetValueID ORDER BY DateCreation ) AS Sequence FROM HistoricalValues	
	), 
	SequentialHistoricalValuesConcat AS -- concateno il valore di ogni riga di storico con quello della successiva
	(
		SELECT HistID, TargetValueID, ([Value] + '|' + 
													(
														SELECT [Value]
														FROM SequentialHistoricalValues InnerSeq 
														WHERE InnerSeq.TargetValueID = SequentialHistoricalValues.TargetValueID AND InnerSeq.Sequence = SequentialHistoricalValues.Sequence + 1
													)
								) AS [ValueEvolution]
		FROM SequentialHistoricalValues
	)
	SELECT HistID, TargetValueID, ValueEvolution FROM SequentialHistoricalValuesConcat WHERE ValueEvolution IS NOT NULL

)



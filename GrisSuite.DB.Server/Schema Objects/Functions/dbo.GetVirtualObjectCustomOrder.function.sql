﻿CREATE FUNCTION [dbo].[GetVirtualObjectCustomOrder] (@VirtualObjectName VARCHAR(500), @Mode TINYINT = NULL)
RETURNS INT
AS
BEGIN
	DECLARE @OrderBy INT;
	
	SET @Mode = isnull(@Mode, 0);
	SET @VirtualObjectName = ISNULL(@VirtualObjectName, '');
	
	IF (@Mode = 0 /* Modalità per estrazione su griglie */)
	BEGIN
		IF (@VirtualObjectName LIKE 'Fondamentali')
		BEGIN
			SET @OrderBy = 1;
		END
		ELSE IF (@VirtualObjectName LIKE 'Importanti')
		BEGIN
			SET @OrderBy = 2;
		END
		ELSE IF (@VirtualObjectName LIKE 'Complementari')
		BEGIN
			SET @OrderBy = 3;
		END
		ELSE IF (@VirtualObjectName LIKE 'Generici')
		BEGIN
			SET @OrderBy = 4;
		END
		ELSE IF (@VirtualObjectName LIKE 'Stazioni AV')
		BEGIN
			SET @OrderBy = 10;
		END	
		ELSE IF (@VirtualObjectName LIKE 'Stazioni Generiche')
		BEGIN
			SET @OrderBy = 11;
		END
	END
	ELSE IF (@Mode = 1 /* Modalità per estrazione su liste Regole */)
	BEGIN
		IF (@VirtualObjectName LIKE 'Generici')
		BEGIN
			SET @OrderBy = 1;
		END
		ELSE IF (@VirtualObjectName LIKE 'Fondamentali')
		BEGIN
			SET @OrderBy = 2;
		END
		ELSE IF (@VirtualObjectName LIKE 'Importanti')
		BEGIN
			SET @OrderBy = 3;
		END
		ELSE IF (@VirtualObjectName LIKE 'Complementari')
		BEGIN
			SET @OrderBy = 4;
		END
		ELSE IF (@VirtualObjectName LIKE 'Stazioni Generiche')
		BEGIN
			SET @OrderBy = 10;
		END	
		ELSE IF (@VirtualObjectName LIKE 'Stazioni AV')
		BEGIN
			SET @OrderBy = 11;
		END
	END
				
	RETURN ISNULL(@OrderBy, 100);
END
CREATE FUNCTION [dbo].[GetDeviceStatusDesc]
(@DevID BIGINT, @GetReal BIT = 0)
RETURNS VARCHAR (128)
AS
BEGIN
	DECLARE @SevLevelDescription VARCHAR(128)

	SET @SevLevelDescription = (SELECT Description FROM severity WHERE SevLevel = dbo.GetDeviceStatus(@DevID,@GetReal))

	RETURN @SevLevelDescription
END
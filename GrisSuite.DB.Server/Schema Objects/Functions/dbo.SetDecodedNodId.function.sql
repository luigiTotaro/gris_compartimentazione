CREATE FUNCTION [dbo].[SetDecodedNodId]
(@decodedRegId SMALLINT, @decodedZonId SMALLINT, @decodedNodId SMALLINT)
RETURNS BIGINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedNodId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedNodId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedNodId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 42, 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedNodId', NULL, NULL


﻿CREATE FUNCTION [dbo].[GetNotificationContactsByObjectStatusId] (@ObjectStatusId UNIQUEIDENTIFIER, @Email BIT)
RETURNS VARCHAR(MAX) AS  
-- Ritorna la lista dei destinatari di rubrica eventualmente associati ad un oggetto
BEGIN 
	DECLARE @NotificationContacts AS VARCHAR(MAX);

	IF @Email = 1
	BEGIN
		SELECT @NotificationContacts = COALESCE(@NotificationContacts + ', ', '') + contacts.Name + ' (' + contacts.Email + ')'
		FROM notification_contacts
		INNER JOIN contacts ON notification_contacts.ContactId = contacts.ContactId
		WHERE (notification_Contacts.ObjectStatusId = @ObjectStatusId)
		ORDER BY contacts.Name
	END
	ELSE
	BEGIN
		SELECT @NotificationContacts = COALESCE(@NotificationContacts + ', ', '') + contacts.Name + ' (' + contacts.PhoneNumber + ')'
		FROM notification_contacts
		INNER JOIN contacts ON notification_contacts.ContactId = contacts.ContactId
		WHERE (notification_Contacts.ObjectStatusId = @ObjectStatusId)
		ORDER BY contacts.Name
	END
		
	RETURN ISNULL(@NotificationContacts, '');
END
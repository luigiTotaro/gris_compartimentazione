﻿-- =============================================
-- Author:		Luigi F.
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION fn_GetAmpliEnabledMask 
(
	-- Add the parameters for the function here
	@logid uniqueidentifier
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int;

	-- Add the T-SQL statements to compute the return value here
with tmp as (
select
	--cache_stream_fields.LogID AS LID,
	cache_stream_fields.ArrayID as AID,
	cache_stream_fields.Value
from cache_stream_fields
	where cache_stream_fields.LogID = @logid
			and cache_stream_fields.StrID = 2
			and cache_stream_fields.FieldID = 9
	)
	select @Result = sum(POWER(2,AID)) 	from tmp
	WHERE Value > 0

--	SELECT @Result = mask

	-- Return the result of the function
	RETURN @Result

END



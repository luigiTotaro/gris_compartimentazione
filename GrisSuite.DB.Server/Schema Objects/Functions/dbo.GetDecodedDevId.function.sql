CREATE FUNCTION [dbo].[GetDecodedDevId]
(@nodId BIGINT)
RETURNS SMALLINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedDevId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedDevId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedDevId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 70, 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedDevId', NULL, NULL


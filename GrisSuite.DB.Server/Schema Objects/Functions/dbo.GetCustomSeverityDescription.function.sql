﻿CREATE FUNCTION [dbo].[GetCustomSeverityDescription] (@SevLevel INT, @SevLevelDetailId INT, @DefaultDescription VARCHAR(255))
-- Restituisce una descrizione di stato custom nel caso che la periferica sia in stato sconosciuto con collettore STLC offline.
-- Serve per mascherare la scritta "Collettore STLC1000 non raggiungibile (offline)" a livello di stazione,
-- come da issue 11427 Funzionalità “Ultimo stato conosciuto”, e restituire "Sconosciuto" in quel caso
RETURNS VARCHAR(255) AS
BEGIN
	DECLARE @CustomSeverityDescription VARCHAR(255);
	
	IF ((@SevLevel = 255) AND (@SevLevelDetailId = 10))
	BEGIN
		SELECT @CustomSeverityDescription = [Description] FROM severity_details WHERE (SevLevel = 255 /* Sconosciuto */) AND (SevLevelDetailId = 5 /* Sconosciuto */);
	END
	ELSE IF ((@SevLevel = 255) AND (@SevLevelDetailId = 7))
	BEGIN
		SELECT @CustomSeverityDescription = [Description] FROM severity_details WHERE (SevLevel = 255 /* Sconosciuto */) AND (SevLevelDetailId = 5 /* Sconosciuto */);
	END
	ELSE
	BEGIN
		SET @CustomSeverityDescription = @DefaultDescription;
	END
	
	RETURN ISNULL(@CustomSeverityDescription, ISNULL(@DefaultDescription, ''))
END
-- =============================================
-- Author:		Cristian Storti
-- Create date: 07-03-2008
-- Modify date: 11-07-2008
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[GetEntityStateValue]
(@TotOk SMALLINT, @TotWarning SMALLINT, @TotError SMALLINT, @TotMaintenance SMALLINT)
RETURNS SMALLINT
AS
BEGIN
	DECLARE @StateValue SMALLINT;
	DECLARE @AvgValue DECIMAL(9, 7);
	DECLARE @Tot SMALLINT;

	DECLARE @OK_ID TINYINT, @WARN_ID TINYINT, @ERR_ID TINYINT;
	SET @OK_ID = 1; 
	SET @WARN_ID = 2; 
	SET @ERR_ID = 3;

	SET @Tot = @TotOk + @TotWarning + @TotError;

	SET @StateValue = -1;

	IF ( @Tot > 0 )
	BEGIN
		SET @AvgValue =	
							((@TotOk + .0) / (@Tot + .0) * (SELECT EntityStateSeverity FROM entity_state WHERE EntityStateID = @OK_ID)) +
							((@TotWarning + .0) / (@Tot + .0) * (SELECT EntityStateSeverity FROM entity_state WHERE EntityStateID = @WARN_ID)) +
							((@TotError + .0) / (@Tot + .0) * (SELECT EntityStateSeverity FROM entity_state WHERE EntityStateID = @ERR_ID));

		SET @StateValue =	CASE 
								WHEN (@AvgValue >= (SELECT CAST(EntityStateMinValue AS DECIMAL(9, 7)) FROM entity_state WHERE EntityStateID = @OK_ID)	AND @AvgValue <= (SELECT CAST(EntityStateMaxValue AS DECIMAL(9, 7)) FROM entity_state WHERE EntityStateID = @OK_ID))	THEN 0
								WHEN (@AvgValue >  (SELECT CAST(EntityStateMinValue AS DECIMAL(9, 7)) FROM entity_state WHERE EntityStateID = @WARN_ID) AND @AvgValue <= (SELECT CAST(EntityStateMaxValue AS DECIMAL(9, 7)) FROM entity_state WHERE EntityStateID = @WARN_ID))	THEN 1
								WHEN (@AvgValue >  (SELECT CAST(EntityStateMinValue AS DECIMAL(9, 7)) FROM entity_state WHERE EntityStateID = @ERR_ID)	AND @AvgValue <= (SELECT CAST(EntityStateMaxValue AS DECIMAL(9, 7)) FROM entity_state WHERE EntityStateID = @ERR_ID))	THEN 2
							END
	END
	ELSE IF (@TotMaintenance > 0)
	   SET @StateValue = 9 -- se esistono solo dispositivi in maintenance metti l'entity in maintenace

	RETURN @StateValue;
END



﻿CREATE FUNCTION [dbo].[GetRegIDFromNodID] (@NodID BIGINT)
RETURNS BIGINT
AS
BEGIN
	DECLARE @RegID BIGINT;
	
	SELECT @RegID = zones.RegID
	FROM nodes INNER JOIN zones ON nodes.ZonID = zones.ZonID
	WHERE (nodes.NodID = @NodID)
	
	RETURN ISNULL(@RegID, -1);
END
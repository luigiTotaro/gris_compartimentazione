﻿CREATE FUNCTION [dbo].[GetTT10210DeviceSevLevel] 
(
	@DevId bigint,
	@T as DateTime,
	@SevLevel int = 0
)
RETURNS INT
AS
BEGIN
	IF @SevLevel =  2 and dbo.GetTT10210AttenuazioneNotturna(@DevId, @T) = 1 
		BEGIN
			WITH streamFieldsAt AS
			(
			SELECT DevId,StrID,FieldID,ArrayID, Max(Created) as Created
			FROM cache_stream_fields INNER JOIN	cache_log_messages ON cache_stream_fields.LogID =  cache_log_messages.LogID
			WHERE (DevId = @DevId) 
				AND Created <= @T
				AND NOT (StrID = 1 AND FieldID = 7) -- Stato amplificatore DS
				AND NOT (StrID = 1 AND FieldID = 8) -- Tensione amplificatore DS
				AND (SevLevel <> 255)
			GROUP BY DevId,StrID,FieldID,ArrayID
			)
			SELECT @SevLevel = Max(SevLevel) FROM cache_stream_fields c 
					INNER JOIN streamFieldsAt a ON c.DevId = a.DevId AND c.StrID = a.StrID AND c.FieldID = a.FieldID AND c.ArrayID = a.ArrayID 
					INNER JOIN cache_log_messages l ON l.LogID = c.LogID AND l.Created = a.Created
		END 

	RETURN ISNULL(@SevLevel,0)

END

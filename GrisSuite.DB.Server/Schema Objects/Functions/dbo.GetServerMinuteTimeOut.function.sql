﻿CREATE FUNCTION GetServerMinuteTimeOut()
RETURNS INT
AS
BEGIN
	DECLARE @ServerMinuteTimeOut INT

	SET @ServerMinuteTimeOut = ISNULL((SELECT CAST(ParameterValue AS INT)FROM Parameters WHERE ParameterName = 'ServerMinuteTimeOut'),60)

	RETURN @ServerMinuteTimeOut
END
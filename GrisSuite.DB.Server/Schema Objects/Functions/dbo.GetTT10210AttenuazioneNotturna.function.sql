﻿CREATE FUNCTION [dbo].[GetTT10210AttenuazioneNotturna] 
(
	@DevID bigint,
	@T as DateTime
)
RETURNS BIT
AS
BEGIN
	DECLARE @AttenuazioneNotturna bit

	SELECT TOP (1) 
		@AttenuazioneNotturna = (CASE WHEN ((dbo.fn_hexstrtobigint(cache_stream_fields.Value) & 0x20) > 0) THEN 1 ELSE 0 END)
	FROM cache_log_messages 
		INNER JOIN cache_stream_fields ON cache_log_messages.LogID = cache_stream_fields.LogID
	WHERE (cache_stream_fields.DevId = @DevID) 
		AND (cache_stream_fields.StrID = 1) -- Stato Generale
		AND (cache_stream_fields.FieldID = 0) -- Parametri di stato
		AND (cache_stream_fields.ArrayID = 0)
		AND (cache_log_messages.Created) <=  @T
	order by  cache_log_messages.Created DESC

	RETURN ISNULL(@AttenuazioneNotturna,0)

END
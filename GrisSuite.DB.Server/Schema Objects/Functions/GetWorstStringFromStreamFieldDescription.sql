﻿CREATE FUNCTION dbo.GetWorstStringFromStreamFieldDescription(@SevLevel INT, @Description VARCHAR(MAX), @Value VARCHAR(1024))
RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE @ParsedDescription VARCHAR(MAX);
	SET @Description = ISNULL(@Description, '');
	SET @Value = ISNULL(@Value, '');
	SET @ParsedDescription = '';

	IF (CHARINDEX('=', @Description) = 0)
	BEGIN
		-- La descrizione non contiene il separatore per messaggi multipli, restituiamo il valore stesso
		SET @ParsedDescription = @Description;
	END
	ELSE
	BEGIN
		IF (@SevLevel = 0)
		BEGIN
			-- Caso standard, la severità è Ok, restituiamo la prima stringa, senza la severità
			SET @ParsedDescription = SUBSTRING(@Description, 0, CHARINDEX('=', @Description));
		END
		ELSE
		BEGIN
			-- La severità non è in Ok, restituiamo la stringa associata ad una delle peggiori severità
			-- Non si usa il Value, anche se il caso per cui è creata questa UDF è quello di Impedenza = '0.0 Ohm'
			SET @ParsedDescription = (
				SELECT TOP 1
				SUBSTRING(Data, 0, CHARINDEX('=', Data))
				FROM dbo.fn_splitstring(@Description, ';')
				ORDER BY CONVERT(BIGINT, SUBSTRING(Data, CHARINDEX('=', Data) + 1, LEN(Data) - CHARINDEX('=', Data))) DESC
			);
		END
	END

	RETURN ISNULL(@ParsedDescription, '');
END
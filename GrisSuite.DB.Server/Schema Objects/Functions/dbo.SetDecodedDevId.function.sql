CREATE FUNCTION [dbo].[SetDecodedDevId]
(@decodedRegId SMALLINT, @decodedZonId SMALLINT, @decodedNodId SMALLINT, @decodedDevId SMALLINT)
RETURNS BIGINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedDevId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedDevId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedDevId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 59, 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedDevId', NULL, NULL


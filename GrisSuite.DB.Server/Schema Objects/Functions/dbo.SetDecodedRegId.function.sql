CREATE FUNCTION [dbo].[SetDecodedRegId]
(@decodedRegId SMALLINT)
RETURNS BIGINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[SetDecodedRegId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedRegId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedRegId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 9, 'SCHEMA', N'dbo', 'FUNCTION', N'SetDecodedRegId', NULL, NULL


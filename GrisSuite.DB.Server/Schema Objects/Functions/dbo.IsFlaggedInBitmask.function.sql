﻿-- =============================================
-- Author:		Cristian Storti
-- Create date: 25-10-2010
-- Description:	
-- =============================================
CREATE FUNCTION IsFlaggedInBitmask 
(
	@DevID BIGINT
	, @StrID INT
	, @FieldID INT
	, @ArrayID INT
	, @BitPosition TINYINT
)
RETURNS int
AS
BEGIN
	-- lsb nelle definizioni
	DECLARE @Flagged AS BIT;
	
	SELECT 
		@Flagged =
		(CASE WHEN 
			(
				ISNULL(CAST(dbo.fn_hexstrtovarbin(stream_fields.Value) AS VARBINARY(16)), CAST(0 AS VARBINARY(16)))
				& POWER(2, @BitPosition))
				= POWER(2, @BitPosition)
		THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END)
	FROM devices
	INNER JOIN streams ON devices.DevID = streams.DevID 
	INNER JOIN stream_fields ON streams.DevID = stream_fields.DevID AND streams.StrID = stream_fields.StrID
	WHERE (devices.DevID = @DevID)
	AND stream_fields.StrID = @StrID AND stream_fields.FieldID = @FieldID AND stream_fields.ArrayID = @ArrayID
	AND stream_fields.Visible = 1;
	
	RETURN ISNULL(@Flagged, 0);

END
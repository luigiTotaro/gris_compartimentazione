﻿CREATE FUNCTION [dbo].[GetLastSevLevelReturned] (@DevID BIGINT)
-- Restituisce un booleano che indica se la periferica sia in stato sconosciuto con collettore STLC offline.
-- Serve per identificare le periferiche associate ad un STLC offline, che quindi ritornano automaticamente l'ultimo stato conosciuto.
-- come da issue 11427 Funzionalità “Ultimo stato conosciuto”
-- Rivista a seguito di issue 14985 “Anomalie con stato "Non Attivo" con STLC offline”, perché non valida nel caso di STLC offline, ma messo in maintenance
-- Nello stato In Maintenance del server, la severità diventa Non Attivo, coprendo i casi di server offline, che sono indistinguibili dalla device sconosciuta associata a server messo in maintenance
-- L'unico modo di verificare se il server relativo alla device è offline, per presentare l'ultimo stato conosciuto, è quello di verificare il server stesso, non gli stati
RETURNS BIT AS
BEGIN
	DECLARE @IsLastSevLevelReturned BIT;
	SET @IsLastSevLevelReturned = 0;
	
	DECLARE @ServerMinuteTimeOut INT;
	SELECT @ServerMinuteTimeOut = CONVERT(INT, [ParameterValue]) FROM [parameters] WHERE [ParameterName] = 'ServerMinuteTimeOut';
	SET @ServerMinuteTimeOut = ISNULL(@ServerMinuteTimeOut, 60);

	DECLARE @LastUpdateServerMinutesAgo INT;
	SELECT @LastUpdateServerMinutesAgo = DATEDIFF(minute, ISNULL(servers.LastUpdate, '2000-1-1' /* Coerente con default motore di calcolo - ServerDefaultStatus.py */), GetDate()) FROM devices INNER JOIN servers ON devices.SrvID = servers.SrvID WHERE (devices.DevID = @DevID)

	IF (@LastUpdateServerMinutesAgo > @ServerMinuteTimeOut)
	BEGIN
		SET @IsLastSevLevelReturned = CAST(1 AS BIT);
	END
	ELSE
	BEGIN
		SET @IsLastSevLevelReturned = CAST(0 AS BIT);
	END
	
	RETURN ISNULL(@IsLastSevLevelReturned, CAST(0 AS BIT));
END
﻿CREATE FUNCTION [GetServerStatusDetail]
(@SrvID BIGINT, @ServerMinuteTimeOut INT=60, @IgnoreMaintenance BIT=0)
RETURNS INT
AS
BEGIN
	DECLARE @SevLevelDetail INT
	DECLARE @InMaintenance BIT
	
	SELECT @InMaintenance = InMaintenance FROM object_servers WHERE ObjectId = @SrvID

	SET @SevLevelDetail = (
					 SELECT
						CASE
							 -- Il flag IsDeleted è impostato nella SP di cancellazione server [gris_DelAllbySrvID]
							 WHEN ((ISNULL((SELECT s.IsDeleted FROM servers s WHERE (s.SrvID = @SrvID)), 0) = 1) AND (NOT EXISTS (SELECT * FROM devices d INNER JOIN device_status ds ON d.DevID = ds.DevID WHERE SrvId = @SrvID)))
								THEN 11 -- Storico, Dato archiviato

							 WHEN ((ISNULL((SELECT s.IsDeleted FROM servers s WHERE (s.SrvID = @SrvID)), 0) = 0) AND (NOT EXISTS (SELECT * FROM devices d INNER JOIN device_status ds ON d.DevID = ds.DevID WHERE SrvId = @SrvID)))
								THEN 22 -- Dati del collettore incompleti
						
							 WHEN (DATEDIFF(n, ISNULL(srv.LastUpdate, CONVERT(DATETIME,'2000-01-01',102)), GETDATE()) > @ServerMinuteTimeOut AND @ServerMinuteTimeOut <> 0)
								THEN 3 -- Non raggiungibile (Offline)

							 WHEN ISNULL(IP, '') = ''
								THEN 12 -- un STLC per essere considetato valido deve avere un ip

							 WHEN ISNULL(FullHostName, '') = ''
								THEN 14 -- un STLC per essere considetato valido deve avere un FullHostName

							 WHEN ISNULL(MAC, '') = ''
								THEN 15 -- un STLC per essere considerato valido deve avere un MAC

							 WHEN (@InMaintenance = 1 AND @IgnoreMaintenance = 0)
								THEN 7 -- Diagnostica disattivata dall'operatore (Server side)
  
							 WHEN ((EXISTS (SELECT ip FROM servers WHERE ip = srv.ip GROUP BY ip HAVING COUNT(*) > 1)) AND @IgnoreMaintenance = 0)
								THEN 8 -- Ip duplicato

							 ELSE 0 -- ok
						END
					  FROM servers srv 
					  WHERE srv.SrvId = @SrvID
					)

	RETURN ISNULL(@SevLevelDetail, -1)
END



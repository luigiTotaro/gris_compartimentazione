﻿CREATE FUNCTION [dbo].[object_status_counters] ()
RETURNS
    @object_status_cal TABLE (
        [ObjectStatusId] UNIQUEIDENTIFIER NOT NULL,
        [ObjectTypeId] INT NOT NULL,
        [Ok] INT NOT NULL,
        [Warning] INT NOT NULL,
        [Error] INT NOT NULL,
        [Maintenance] INT NOT NULL,
        [Unknown] INT NOT NULL)
AS
BEGIN
	-- node_systems_status_calc
	INSERT INTO @object_status_cal (ObjectStatusId, ObjectTypeId, Ok, Warning, Error, Maintenance, Unknown)
	SELECT Id, ObjectTypeId, [0], [1], [2], [9], [3]+[255] AS [255]
	FROM 
	(
		SELECT P.ObjectStatusId AS Id, P.ObjectTypeId, C.SevLevel, C.ObjectStatusId as ChildId
		FROM object_status AS P INNER JOIN object_status AS C ON P.ObjectStatusId = C.ParentObjectStatusId 
		WHERE (P.ObjectTypeId = 4)
		AND C.ExcludeFromParentStatus = 0
	) AS A
	PIVOT
	(
	 COUNT(ChildId)
	 FOR SevLevel
	 IN ([0], [1], [2], [9], [255], [3])
	) AS p
	
	-- node_status_calc
	INSERT INTO @object_status_cal (ObjectStatusId, ObjectTypeId, Ok, Warning, Error, Maintenance, Unknown)
	SELECT Id, ObjectTypeId, [0], [1], [2], [9], [3]+[255] as [255]
	FROM
	(
		SELECT P.ObjectStatusId AS Id, P.ObjectTypeId, C.SevLevel, C.ObjectStatusId as ChildId
		FROM object_status AS P INNER JOIN object_status AS C ON P.ObjectStatusId = C.ParentObjectStatusId
		INNER JOIN node_systems ON C.ObjectId = node_systems.NodeSystemsId
		WHERE (P.ObjectTypeId = 3)
		AND C.ExcludeFromParentStatus = 0
	) AS A
	PIVOT
	(
	 COUNT(ChildId)
	 FOR SevLevel
	 IN ([0], [1], [2], [9], [255], [3])
	) AS p
	
	-- object_status_calc (2)
	INSERT INTO @object_status_cal (ObjectStatusId, ObjectTypeId, Ok, Warning, Error, Maintenance, Unknown)
	SELECT Id, ObjectTypeId, [0], [1], [2], [9], [255]+[3] as [255]
	FROM 
	(
		SELECT P.ObjectStatusId AS Id, P.ObjectTypeId, C.SevLevel, C.ObjectStatusId as ChildId
		FROM object_status AS P 
		INNER JOIN object_status AS C ON P.ObjectStatusId = C.ParentObjectStatusId 
		WHERE (P.ObjectTypeId = 2)
		AND C.ExcludeFromParentStatus = 0
	) AS A
	PIVOT
	(
	 COUNT(ChildId)
	 FOR SevLevel
	 IN ([0], [1], [2], [9], [255], [3])
	) AS p
	
	-- object_status_calc (1)
	INSERT INTO @object_status_cal (ObjectStatusId, ObjectTypeId, Ok, Warning, Error, Maintenance, Unknown)
	SELECT Id, ObjectTypeId, [0], [1], [2], [9], [255]+[3] as [255]
	FROM 
	(
		SELECT P.ObjectStatusId AS Id, P.ObjectTypeId, C.SevLevel, C.ObjectStatusId as ChildId
		FROM object_status AS P 
		INNER JOIN object_status AS C ON P.ObjectStatusId = C.ParentObjectStatusId 
		WHERE (P.ObjectTypeId = 1)
		AND C.ExcludeFromParentStatus = 0
	) AS A
	PIVOT
	(
	 COUNT(ChildId)
	 FOR SevLevel
	 IN ([0], [1], [2], [9], [255], [3])
	) AS p	

	RETURN;
END
﻿CREATE FUNCTION GetSeverityLevelForObject (@RegID BIGINT, @ZonID BIGINT, @NodID BIGINT)
RETURNS INT
AS
BEGIN
	DECLARE @SevLevel INT;

	IF (ISNULL(@RegID, 0) > 0)
	BEGIN
		SET @SevLevel = (SELECT SevLevel FROM object_status WHERE (ObjectId = @RegID) AND (ObjectTypeId = 1 /* regions */))
	END
	ELSE IF (ISNULL(@ZonID, 0) > 0)
	BEGIN
		SET @SevLevel = (SELECT SevLevel FROM object_status WHERE (ObjectId = @ZonID) AND (ObjectTypeId = 2 /* zones */))
	END
	ELSE IF (ISNULL(@NodID, 0) > 0)
	BEGIN
		SET @SevLevel = (SELECT SevLevel FROM object_status WHERE (ObjectId = @NodID) AND (ObjectTypeId = 3 /* nodes */))
	END

	RETURN ISNULL(@SevLevel, -1);
END
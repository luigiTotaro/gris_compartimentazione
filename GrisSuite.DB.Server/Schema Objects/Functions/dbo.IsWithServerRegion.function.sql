CREATE FUNCTION [dbo].[IsWithServerRegion]
(@RegId BIGINT)
RETURNS BIT
AS
BEGIN
	DECLARE @WithServers BIT
	
	IF EXISTS (SELECT regions.RegId 
				FROM regions
				INNER JOIN zones ON	regions.RegId = zones.RegId
				INNER JOIN nodes ON	zones.ZonId = nodes.ZonId
				INNER JOIN devices ON nodes.NodId = devices.NodId 
				LEFT JOIN [servers] ds ON ds.SrvID = devices.SrvID
				LEFT JOIN [servers] ns ON ns.NodID = nodes.NodID
				WHERE regions.RegId  = @RegId AND ([Type] = 'STLC1000' OR ds.SrvID IS NOT NULL OR ns.SrvID IS NOT NULL))
	   SET @WithServers = 1;
	ELSE
	   SET @WithServers = 0; 
	   
	RETURN @WithServers;

END
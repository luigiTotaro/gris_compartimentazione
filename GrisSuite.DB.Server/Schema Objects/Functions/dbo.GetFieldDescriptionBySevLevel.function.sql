﻿CREATE FUNCTION [dbo].[GetFieldDescriptionBySevLevel](@FieldDescription varchar(max), @SevLevelList varchar(20), @ViewSevLevel bit = 0)
	RETURNS varchar(max) 
AS 
BEGIN
	DECLARE @Delimiter AS VARCHAR (5);
	DECLARE @SubDelimiter AS VARCHAR (5);
	DECLARE @LenString AS INT;
	DECLARE @LenSubString AS INT;
	DECLARE @SevLevel AS VARCHAR (5);
	DECLARE @Message AS VARCHAR (MAX);
	DECLARE @out AS VARCHAR (MAX);
	DECLARE @MessageAndSevLevel AS VARCHAR (MAX);

	SET @out = '';
	SET @Delimiter = ';';
	SET @SubDelimiter = '=';

	WHILE len(@FieldDescription) > 0
		BEGIN
			SELECT @LenString = (CASE charindex(@Delimiter, @FieldDescription) WHEN 0 THEN len(@FieldDescription) ELSE (charindex(@Delimiter, @FieldDescription) - 1) END);
			SET @MessageAndSevLevel = substring(@FieldDescription, 1, @LenString);
			SELECT @LenSubString = (CASE charindex(@SubDelimiter, @MessageAndSevLevel) WHEN 0 THEN len(@MessageAndSevLevel) ELSE (charindex(@SubDelimiter, @MessageAndSevLevel) - 1) END);
			SET @Message = substring(@MessageAndSevLevel, 1, @LenSubString);
			
			IF (len(@MessageAndSevLevel) - @LenSubString - 1) > 0
				SET @SevLevel = rtrim(RIGHT(@MessageAndSevLevel, len(@MessageAndSevLevel) - @LenSubString - 1));
			ELSE
				SET @SevLevel = '0';

			IF charindex('|' + @SevLevel + '|', @SevLevelList) > 0
				IF (@ViewSevLevel = 1)
					SET @out = @out + @Message + '=' + @SevLevel + ';';
				ELSE
					SET @out = @out + @Message + ';';
					
			SELECT @FieldDescription = (CASE (len(@FieldDescription) - @LenString) WHEN 0 THEN '' ELSE RIGHT(@FieldDescription, len(@FieldDescription) - @LenString - 1) END);
		END

	RETURN @out;
END
﻿CREATE FUNCTION [IsObjectNodeSystemRelatedToServerInSevLevelDetail]
(@ObjectId BIGINT, @SevLevelDetailId INT)
RETURNS BIT
AS
BEGIN
	-- Verifica se esiste almeno una device associata al sistema passato,
	-- che sia agganciata ad un server nello stato di dettaglio severity indicato
	DECLARE @IsObjectNodeSystemRelatedToServerInSevLevelDetail BIT;
	SET @IsObjectNodeSystemRelatedToServerInSevLevelDetail = 0;
	
	IF EXISTS (
			SELECT node_systems.NodeSystemsId
			FROM devices
			INNER JOIN nodes ON devices.NodID = nodes.NodID
			INNER JOIN node_systems ON nodes.NodID = node_systems.NodId
			INNER JOIN device_type ON devices.Type = device_type.DeviceTypeID
			AND node_systems.SystemId = device_type.SystemID
			INNER JOIN object_servers ON devices.SrvID = object_servers.ObjectId
			WHERE (object_servers.SevLevelDetailId = @SevLevelDetailId)
			AND (node_systems.NodeSystemsId = @ObjectId)
	)
	BEGIN
		SET @IsObjectNodeSystemRelatedToServerInSevLevelDetail = 1;
	END

	RETURN @IsObjectNodeSystemRelatedToServerInSevLevelDetail;
END
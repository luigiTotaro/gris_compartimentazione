﻿CREATE FUNCTION [dbo].[GetDeviceStatusDetailDesc]
(@DevID BIGINT)
RETURNS VARCHAR (128)
AS
BEGIN
	DECLARE @SevLevelDescription VARCHAR(128)

	SET @SevLevelDescription = (SELECT Description FROM severity_detail WHERE SevLevelDetailId = dbo.GetDeviceStatusDetail(@DevID))

	RETURN ISNULL(@SevLevelDescription,'')
END
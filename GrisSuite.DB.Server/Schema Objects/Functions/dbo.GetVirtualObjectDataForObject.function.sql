﻿CREATE FUNCTION [dbo].[GetVirtualObjectDataForObject] (@DataType TINYINT, @RegID BIGINT, @ZonID BIGINT, @NodID BIGINT, @NodeSystemsId BIGINT, @SrvID BIGINT, @DevID BIGINT, @VirtualObjectID BIGINT)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @VirtualObjectDataValue VARCHAR(MAX);
	SET @VirtualObjectDataValue = NULL;
	
	IF (ISNULL(@DataType, 0) > 0)
	BEGIN
		IF (ISNULL(@RegID, 0) > 0)
		BEGIN
			IF (@DataType = 1 /* VirtualObjectID */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectID FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_regions ON object_regions.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN regions ON regions.RegID = object_regions.ObjectId WHERE (regions.RegID = @RegID))
			END
			ELSE IF (@DataType = 2 /* VirtualObjectName */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectName FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_regions ON object_regions.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN regions ON regions.RegID = object_regions.ObjectId WHERE (regions.RegID = @RegID))
			END
			ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectDescription FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_regions ON object_regions.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN regions ON regions.RegID = object_regions.ObjectId WHERE (regions.RegID = @RegID))
			END
			ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_regions ON object_regions.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN regions ON regions.RegID = object_regions.ObjectId WHERE (regions.RegID = @RegID))
			END			
		END
		ELSE IF (ISNULL(@ZonID, 0) > 0)
		BEGIN
			IF (@DataType = 1 /* VirtualObjectID */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectID FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_zones ON object_zones.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN zones ON zones.ZonID = object_zones.ObjectId WHERE (zones.ZonID = @ZonID))
			END
			ELSE IF (@DataType = 2 /* VirtualObjectName */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectName FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_zones ON object_zones.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN zones ON zones.ZonID = object_zones.ObjectId WHERE (zones.ZonID = @ZonID))
			END
			ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectDescription FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_zones ON object_zones.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN zones ON zones.ZonID = object_zones.ObjectId WHERE (zones.ZonID = @ZonID))
			END
			ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_zones ON object_zones.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN zones ON zones.ZonID = object_zones.ObjectId WHERE (zones.ZonID = @ZonID))
			END			
		END
		ELSE IF (ISNULL(@NodID, 0) > 0)
		BEGIN
			IF (@DataType = 1 /* VirtualObjectID */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectID FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_nodes ON object_nodes.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN nodes ON nodes.NodID = object_nodes.ObjectId WHERE (nodes.NodID = @NodID))
			END
			ELSE IF (@DataType = 2 /* VirtualObjectName */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectName FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_nodes ON object_nodes.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN nodes ON nodes.NodID = object_nodes.ObjectId WHERE (nodes.NodID = @NodID))
			END
			ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectDescription FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_nodes ON object_nodes.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN nodes ON nodes.NodID = object_nodes.ObjectId WHERE (nodes.NodID = @NodID))
			END
			ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_nodes ON object_nodes.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN nodes ON nodes.NodID = object_nodes.ObjectId WHERE (nodes.NodID = @NodID))
			END
		END
		ELSE IF (ISNULL(@NodeSystemsId, 0) > 0)
		BEGIN
			IF (@DataType = 1 /* VirtualObjectID */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectID FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_node_systems ON object_node_systems.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId WHERE (node_systems.NodeSystemsId = @NodeSystemsId))
			END
			ELSE IF (@DataType = 2 /* VirtualObjectName */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectName FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_node_systems ON object_node_systems.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId WHERE (node_systems.NodeSystemsId = @NodeSystemsId))
			END
			ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectDescription FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_node_systems ON object_node_systems.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId WHERE (node_systems.NodeSystemsId = @NodeSystemsId))
			END
			ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_node_systems ON object_node_systems.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN node_systems ON node_systems.NodeSystemsId = object_node_systems.ObjectId WHERE (node_systems.NodeSystemsId = @NodeSystemsId))
			END			
		END
		ELSE IF (ISNULL(@SrvID, 0) > 0)
		BEGIN
			IF (@DataType = 1 /* VirtualObjectID */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectID FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_servers ON object_servers.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN [servers] ON [servers].SrvID = object_servers.ObjectId WHERE ([servers].SrvID = @SrvID))
			END
			ELSE IF (@DataType = 2 /* VirtualObjectName */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectName FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_servers ON object_servers.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN [servers] ON [servers].SrvID = object_servers.ObjectId WHERE ([servers].SrvID = @SrvID))
			END
			ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectDescription FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_servers ON object_servers.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN [servers] ON [servers].SrvID = object_servers.ObjectId WHERE ([servers].SrvID = @SrvID))
			END
			ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_servers ON object_servers.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN [servers] ON [servers].SrvID = object_servers.ObjectId WHERE ([servers].SrvID = @SrvID))
			END			
		END
		ELSE IF (ISNULL(@DevID, 0) > 0)
		BEGIN
			IF (@DataType = 1 /* VirtualObjectID */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectID FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_devices ON object_devices.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN devices ON devices.DevID = object_devices.ObjectId WHERE (devices.DevID = @DevID))
			END
			ELSE IF (@DataType = 2 /* VirtualObjectName */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectName FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_devices ON object_devices.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN devices ON devices.DevID = object_devices.ObjectId WHERE (devices.DevID = @DevID))
			END
			ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectDescription FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_devices ON object_devices.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN devices ON devices.DevID = object_devices.ObjectId WHERE (devices.DevID = @DevID))
			END
			ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId INNER JOIN object_devices ON object_devices.ParentObjectStatusId = object_virtuals.ObjectStatusId INNER JOIN devices ON devices.DevID = object_devices.ObjectId WHERE (devices.DevID = @DevID))
			END			
		END
		ELSE IF (ISNULL(@VirtualObjectID, 0) > 0)
		BEGIN
			IF (@DataType = 1 /* VirtualObjectID */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectID FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId WHERE (virtual_objects.VirtualObjectID = @VirtualObjectID))
			END
			ELSE IF (@DataType = 2 /* VirtualObjectName */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectName FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId WHERE (virtual_objects.VirtualObjectID = @VirtualObjectID))
			END
			ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT virtual_objects.VirtualObjectDescription FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId WHERE (virtual_objects.VirtualObjectID = @VirtualObjectID))
			END
			ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
			BEGIN
				SET @VirtualObjectDataValue = (SELECT dbo.IsObjectStatusIdComputedByCustomFormula(object_virtuals.ObjectStatusId, 1 /* Severity */) FROM virtual_objects INNER JOIN object_virtuals ON virtual_objects.VirtualObjectID = object_virtuals.ObjectId WHERE (virtual_objects.VirtualObjectID = @VirtualObjectID))
			END			
		END								
	END

	IF (@VirtualObjectDataValue IS NULL)
	BEGIN
		IF (@DataType = 1 /* VirtualObjectID */)
		BEGIN
			SET @VirtualObjectDataValue = '0';
		END
		ELSE IF (@DataType = 2 /* VirtualObjectName */)
		BEGIN
			SET @VirtualObjectDataValue = '';
		END
		ELSE IF (@DataType = 3 /* VirtualObjectDescription */)
		BEGIN
			SET @VirtualObjectDataValue = '';
		END
		ELSE IF (@DataType = 4 /* IsObjectStatusIdComputedByCustomFormula */)
		BEGIN
			SET @VirtualObjectDataValue = '0';
		END		
	END
			
	RETURN @VirtualObjectDataValue;
END
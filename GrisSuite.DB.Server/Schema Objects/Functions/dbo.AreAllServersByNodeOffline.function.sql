﻿CREATE FUNCTION [dbo].[AreAllServersByNodeOffline]
(
	@NodID BIGINT
)
RETURNS BIT
AS
BEGIN
	DECLARE @ServerNum TINYINT;
	DECLARE @OfflineServerNum TINYINT;
	
	SELECT @OfflineServerNum = ISNULL(SUM([3]), 0), @ServerNum = ISNULL(COUNT(ObjectStatusId), 0)
	FROM
	(
		SELECT DISTINCT object_servers.ObjectStatusId, object_servers.SevLevel
		FROM object_nodes
		INNER JOIN devices ON object_nodes.ObjectId = devices.NodID 
		INNER JOIN object_servers ON devices.SrvID = object_servers.ObjectId 
		WHERE object_nodes.ObjectId = @NodID
		AND object_servers.SevLevel <> 9 -- nel conteggio non vengono considerati i server in configurazione
		AND object_servers.SevLevel <> -1
	) AS A
	PIVOT
	(
	 COUNT(SevLevel)
	 FOR SevLevel
	 IN ([0], [3])
	) AS p;
	
	IF @OfflineServerNum <> 0 AND @ServerNum = @OfflineServerNum RETURN 1;
	RETURN 0;
END
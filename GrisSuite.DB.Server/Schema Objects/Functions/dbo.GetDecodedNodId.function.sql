CREATE FUNCTION [dbo].[GetDecodedNodId]
(@nodId BIGINT)
RETURNS SMALLINT
AS
 EXTERNAL NAME [SqlClassLibrary].[UserDefinedFunctions].[GetDecodedNodId]






GO
EXEC sp_addextendedproperty N'AutoDeployed', N'yes', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedNodId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFile', N'DecodDevId.cs', 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedNodId', NULL, NULL
GO
EXEC sp_addextendedproperty N'SqlAssemblyFileLine', 52, 'SCHEMA', N'dbo', 'FUNCTION', N'GetDecodedNodId', NULL, NULL


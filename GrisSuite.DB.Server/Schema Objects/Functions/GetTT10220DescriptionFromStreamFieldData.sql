﻿CREATE FUNCTION [GetTT10220DescriptionFromStreamFieldData](@DevID BIGINT, @FieldName VARCHAR(64), @FieldID INT)
RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE @Description VARCHAR(MAX);
	SET @Description = '';

	IF (CHARINDEX('amplificator', @FieldName) > 0)
	BEGIN
		-- Amplificatori
		DECLARE @AmpliSevLevel INT;
		SET @AmpliSevLevel = 0; -- OK

		-- Per i tre possibili amplificatori, lo stato dipende dalla severità dello stream field con lo stato
		-- Ad esempio, la Tensione Amplificatore 1 (nello StreamField 8) dipende dallo Stato Amplificatore 1 (nello StreamField 7)
		-- Dato che le varie maschere di bit codificano varie forme di errore e anomalia su bit differenti, si guarda la severità
		-- come calcolata nelle definizioni seriali (TT10220d.xml) senza accedere ai singoli bit
		IF (@FieldID = 8)
		BEGIN
			SELECT @AmpliSevLevel = ISNULL(SevLevel, 0) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = 7;
		END
		ELSE IF (@FieldID = 13)
		BEGIN
			SELECT @AmpliSevLevel = ISNULL(SevLevel, 0) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = 12;
		END
		ELSE IF (@FieldID = 18)
		BEGIN
			SELECT @AmpliSevLevel = ISNULL(SevLevel, 0) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = 17;
		END

		IF (@AmpliSevLevel <> 0)
		BEGIN
			-- L'amplificatore è in errore o anomalia, quindi la stringa da restituire per la descrizione non è quella standard del valore
			IF (@FieldID = 8)
			BEGIN
				SELECT @Description = dbo.GetWorstStringFromStreamFieldDescription(SevLevel, [Description], [Value]) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = 7;
			END
			ELSE IF (@FieldID = 13)
			BEGIN
				SELECT @Description = dbo.GetWorstStringFromStreamFieldDescription(SevLevel, [Description], [Value]) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = 12;
			END
			ELSE IF (@FieldID = 18)
			BEGIN
				SELECT @Description = dbo.GetWorstStringFromStreamFieldDescription(SevLevel, [Description], [Value]) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = 17;
			END
		END
		ELSE
		BEGIN
			-- Amplificatore installato e funzionante, la descrizione è quella riportata dal valore
			SELECT @Description = dbo.GetWorstStringFromStreamFieldDescription(SevLevel, [Description], [Value]) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = @FieldID;
		END
	END
	ELSE
	BEGIN
		-- Zone, la descrizione è quella riportata dal valore
		SELECT @Description = dbo.GetWorstStringFromStreamFieldDescription(SevLevel, [Description], [Value]) FROM stream_fields WHERE DevID = @DevID AND StrID = 1 AND FieldID = @FieldID;
	END

	RETURN ISNULL(@Description, '');
END
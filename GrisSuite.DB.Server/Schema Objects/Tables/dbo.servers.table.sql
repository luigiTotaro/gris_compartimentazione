﻿CREATE TABLE [dbo].[servers] (
    [SrvID]                              BIGINT          NOT NULL,
    [Name]                               VARCHAR (64)    NULL,
    [Host]                               VARCHAR (64)    NULL,
    [FullHostName]                       VARCHAR (256)   NULL,
    [IP]                                 VARCHAR (16)    NULL,
    [LastUpdate]                         DATETIME        NULL,
    [LastMessageType]                    VARCHAR (64)    NULL,
    [SupervisorSystemXML]                XML             NULL,
    [ClientSupervisorSystemXMLValidated] XML             NULL,
    [ClientValidationSign]               VARBINARY (128) NULL,
    [ClientDateValidationRequested]      DATETIME        NULL,
    [ClientDateValidationObtained]       DATETIME        NULL,
    [ServerSupervisorSystemXMLValidated] XML             NULL,
    [ServerValidationSign]               VARBINARY (128) NULL,
    [ServerDateValidationRequested]      DATETIME        NULL,
    [ServerDateValidationObtained]       DATETIME        NULL,
    [ClientKey]                          VARBINARY (148) NULL,
    [AccountDocValidation]               VARCHAR (256)   NULL,
    [DateDocValidation]                  DATETIME        NULL,
    [AccountUTValidation]                VARCHAR (256)   NULL,
    [DateUTValidation]                   DATETIME        NULL,
    [NodID]                              BIGINT          NULL,
    [ServerVersion]                      VARCHAR (32)    NULL,
    [MAC]                                VARCHAR (16)    NULL,
    [IsDeleted]                          BIT             NULL
);









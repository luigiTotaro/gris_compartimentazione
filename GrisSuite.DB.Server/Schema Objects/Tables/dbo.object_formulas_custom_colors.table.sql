﻿CREATE TABLE [dbo].[object_formulas_custom_colors] (
    [ObjectFormulasCustomColorId] INT              IDENTITY (1, 1) NOT NULL,
    [ObjectStatusId]              UNIQUEIDENTIFIER NOT NULL,
    [FormulaIndex]                TINYINT          NOT NULL,
    [AudioSystemSevLevel]         INT              NOT NULL,
    [VideoSystemSevLevel]         INT              NOT NULL,
    [HtmlColor]                   CHAR (7)         NOT NULL
);




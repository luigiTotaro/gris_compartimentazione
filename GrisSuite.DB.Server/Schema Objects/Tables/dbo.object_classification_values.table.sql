CREATE TABLE [dbo].[object_classification_values] (
    [ObjectStatusId]        UNIQUEIDENTIFIER NOT NULL,
    [ClassificationValueId] SMALLINT         NOT NULL,
    [MapLayerId]            INT              NOT NULL,
    [MapOffsetX]            FLOAT            NOT NULL,
    [MapOffsetY]            FLOAT            NOT NULL,
    [CaptionFromScale]      FLOAT            NOT NULL
);










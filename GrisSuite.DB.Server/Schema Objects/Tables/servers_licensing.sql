﻿CREATE TABLE [dbo].[servers_licensing] (
    [ServerLicensingId]       INT            IDENTITY (1, 1) NOT NULL,
    [LicensingLastUpdate]     DATETIME       NOT NULL,
    [LicensingLastUpdateUser] VARCHAR (100)  NOT NULL,
    [HardwareProfile]         VARCHAR (64)   NOT NULL,
    [LicensingData]           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_servers_licensing] PRIMARY KEY CLUSTERED ([ServerLicensingId] ASC),
    CONSTRAINT [UQ_servers_licensing_HardwareProfile] UNIQUE NONCLUSTERED ([HardwareProfile] ASC)
);


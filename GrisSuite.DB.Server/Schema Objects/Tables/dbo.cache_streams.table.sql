﻿CREATE TABLE [dbo].[cache_streams]
(
[DevID] [bigint] NOT NULL,
[StrID] [int] NOT NULL,
[LogID] [uniqueidentifier] NOT NULL,
[Name] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[Visible] [tinyint] NULL,
[Data] [image] NULL,
[DateTime] [datetime] NOT NULL,
[SevLevel] [int] NULL,
[Description] [text] COLLATE Latin1_General_CI_AS NULL,
[Processed] [tinyint] NULL,
[DevType] [varchar] (16) COLLATE Latin1_General_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]



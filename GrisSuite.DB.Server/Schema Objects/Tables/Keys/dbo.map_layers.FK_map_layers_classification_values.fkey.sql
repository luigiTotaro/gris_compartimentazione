﻿ALTER TABLE [dbo].[map_layers]
    ADD CONSTRAINT [FK_map_layers_classification_values] FOREIGN KEY ([ClassificationValueId]) REFERENCES [dbo].[classification_values] ([ClassificationValueId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


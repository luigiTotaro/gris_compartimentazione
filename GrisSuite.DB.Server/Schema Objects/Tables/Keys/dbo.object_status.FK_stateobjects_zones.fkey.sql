﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_stateobjects_zones] FOREIGN KEY ([ObjectId]) REFERENCES [dbo].[zones] ([ZonID]) ON DELETE CASCADE ON UPDATE NO ACTION NOT FOR REPLICATION;


GO
ALTER TABLE [dbo].[object_status] NOCHECK CONSTRAINT [FK_stateobjects_zones];


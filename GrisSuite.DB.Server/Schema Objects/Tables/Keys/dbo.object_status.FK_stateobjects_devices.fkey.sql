﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_stateobjects_devices] FOREIGN KEY ([ObjectId]) REFERENCES [dbo].[devices] ([DevID]) ON DELETE NO ACTION ON UPDATE NO ACTION NOT FOR REPLICATION;


GO
ALTER TABLE [dbo].[object_status] NOCHECK CONSTRAINT [FK_stateobjects_devices];


﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_object_status_object_type] FOREIGN KEY ([ObjectTypeId]) REFERENCES [dbo].[object_type] ([ObjectTypeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


﻿ALTER TABLE [dbo].[object_classification_values]
    ADD CONSTRAINT [FK_object_classification_values_map_layers] FOREIGN KEY ([MapLayerId]) REFERENCES [dbo].[map_layers] ([MapLayerId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


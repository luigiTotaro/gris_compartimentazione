﻿ALTER TABLE [dbo].[severity_details]
    ADD CONSTRAINT [FK_severity_details_severity] FOREIGN KEY ([SevLevel]) REFERENCES [dbo].[severity] ([SevLevel]) ON DELETE NO ACTION ON UPDATE NO ACTION;


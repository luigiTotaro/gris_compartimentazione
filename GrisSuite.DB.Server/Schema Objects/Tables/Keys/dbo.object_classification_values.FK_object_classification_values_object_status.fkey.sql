﻿ALTER TABLE [dbo].[object_classification_values]
    ADD CONSTRAINT [FK_object_classification_values_object_status] FOREIGN KEY ([ObjectStatusId]) REFERENCES [dbo].[object_status] ([ObjectStatusId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


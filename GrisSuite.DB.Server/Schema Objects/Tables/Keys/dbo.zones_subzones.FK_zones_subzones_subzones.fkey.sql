﻿ALTER TABLE [dbo].[zones_subzones]
    ADD CONSTRAINT [FK_zones_subzones_subzones] FOREIGN KEY ([SubZoneId]) REFERENCES [dbo].[subzones] ([SubZoneId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


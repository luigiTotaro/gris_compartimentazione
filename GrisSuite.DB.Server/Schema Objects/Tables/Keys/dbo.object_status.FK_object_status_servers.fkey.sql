﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_object_status_servers] FOREIGN KEY ([ObjectId]) REFERENCES [dbo].[servers] ([SrvID]) ON DELETE CASCADE ON UPDATE NO ACTION NOT FOR REPLICATION;


GO
ALTER TABLE [dbo].[object_status] NOCHECK CONSTRAINT [FK_object_status_servers];


﻿ALTER TABLE [dbo].[object_formulas_custom_colors]
    ADD CONSTRAINT [FK_object_formulas_custom_colors_severity_audio] FOREIGN KEY ([AudioSystemSevLevel]) REFERENCES [dbo].[severity] ([SevLevel]) ON DELETE NO ACTION ON UPDATE NO ACTION;


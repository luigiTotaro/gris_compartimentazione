﻿ALTER TABLE [dbo].[alerts_tickets_archive]
    ADD CONSTRAINT [PK_alerts_tickets_archive] PRIMARY KEY NONCLUSTERED ([AlertTicketID] ASC) WITH (FILLFACTOR = 80, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


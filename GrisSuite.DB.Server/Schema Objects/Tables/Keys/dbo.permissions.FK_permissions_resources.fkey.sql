﻿ALTER TABLE [dbo].[permissions]
    ADD CONSTRAINT [FK_permissions_resources] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[resources] ([ResourceID]) ON DELETE NO ACTION ON UPDATE NO ACTION;


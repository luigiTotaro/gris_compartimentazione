﻿ALTER TABLE [dbo].[object_attributes]
    ADD CONSTRAINT [FK_object_attributes_object_formulas] FOREIGN KEY ([ComputedByFormulaObjectStatusId], [ComputedByFormulaIndex]) REFERENCES [dbo].[object_formulas] ([ObjectStatusId], [FormulaIndex]) ON DELETE NO ACTION ON UPDATE NO ACTION;


﻿ALTER TABLE [dbo].[zones_subzones]
    ADD CONSTRAINT [FK_zones_subzones_zones] FOREIGN KEY ([ZonId]) REFERENCES [dbo].[zones] ([ZonID]) ON DELETE NO ACTION ON UPDATE NO ACTION;


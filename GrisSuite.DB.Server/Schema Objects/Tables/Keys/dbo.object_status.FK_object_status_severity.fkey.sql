﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_object_status_severity] FOREIGN KEY ([SevLevel]) REFERENCES [dbo].[severity] ([SevLevel]) ON DELETE NO ACTION ON UPDATE NO ACTION;


﻿ALTER TABLE [dbo].[object_attribute_types]
    ADD CONSTRAINT [PK_object_attribute_type] PRIMARY KEY CLUSTERED ([AttributeTypeId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


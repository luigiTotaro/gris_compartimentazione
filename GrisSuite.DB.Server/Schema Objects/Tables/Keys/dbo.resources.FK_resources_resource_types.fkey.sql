﻿ALTER TABLE [dbo].[resources]
    ADD CONSTRAINT [FK_resources_resource_types] FOREIGN KEY ([ResourceTypeID]) REFERENCES [dbo].[resource_types] ([ResourceTypeID]) ON DELETE NO ACTION ON UPDATE NO ACTION;


﻿ALTER TABLE [dbo].[object_attributes]
    ADD CONSTRAINT [FK_object_attributes_object_status] FOREIGN KEY ([ObjectStatusId]) REFERENCES [dbo].[object_status] ([ObjectStatusId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


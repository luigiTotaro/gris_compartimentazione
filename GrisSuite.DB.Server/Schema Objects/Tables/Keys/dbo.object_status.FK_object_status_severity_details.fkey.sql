﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_object_status_severity_details] FOREIGN KEY ([SevLevelDetailId]) REFERENCES [dbo].[severity_details] ([SevLevelDetailId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


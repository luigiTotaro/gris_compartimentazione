﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_object_status_node_systems] FOREIGN KEY ([ObjectId]) REFERENCES [dbo].[node_systems] ([NodeSystemsId]) ON DELETE NO ACTION ON UPDATE NO ACTION NOT FOR REPLICATION;


GO
ALTER TABLE [dbo].[object_status] NOCHECK CONSTRAINT [FK_object_status_node_systems];


﻿ALTER TABLE [dbo].[object_status]
    ADD CONSTRAINT [FK_object_status_virtual_objects] FOREIGN KEY ([ObjectId]) REFERENCES [dbo].[virtual_objects] ([VirtualObjectID]) ON DELETE NO ACTION ON UPDATE NO ACTION NOT FOR REPLICATION;


GO
ALTER TABLE [dbo].[object_status] NOCHECK CONSTRAINT [FK_object_status_virtual_objects];


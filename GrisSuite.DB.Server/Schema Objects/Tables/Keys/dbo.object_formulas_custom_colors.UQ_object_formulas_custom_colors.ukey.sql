﻿ALTER TABLE [dbo].[object_formulas_custom_colors]
    ADD CONSTRAINT [UQ_object_formulas_custom_colors] UNIQUE NONCLUSTERED ([ObjectStatusId] ASC, [FormulaIndex] ASC, [AudioSystemSevLevel] ASC, [VideoSystemSevLevel] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY];


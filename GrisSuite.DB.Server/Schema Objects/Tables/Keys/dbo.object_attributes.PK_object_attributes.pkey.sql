﻿ALTER TABLE [dbo].[object_attributes]
    ADD CONSTRAINT [PK_object_attributes] PRIMARY KEY CLUSTERED ([AttributeId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


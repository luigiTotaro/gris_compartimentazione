﻿ALTER TABLE [dbo].[classification_values]
    ADD CONSTRAINT [PK_object_classification_values] PRIMARY KEY CLUSTERED ([ClassificationValueId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


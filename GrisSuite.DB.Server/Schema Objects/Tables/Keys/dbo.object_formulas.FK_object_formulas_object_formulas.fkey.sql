﻿ALTER TABLE [dbo].[object_formulas]
    ADD CONSTRAINT [FK_object_formulas_object_formulas] FOREIGN KEY ([ObjectStatusId], [FormulaIndex]) REFERENCES [dbo].[object_formulas] ([ObjectStatusId], [FormulaIndex]) ON DELETE NO ACTION ON UPDATE NO ACTION;


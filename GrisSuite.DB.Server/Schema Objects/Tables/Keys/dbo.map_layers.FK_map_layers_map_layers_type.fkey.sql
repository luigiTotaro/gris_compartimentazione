﻿ALTER TABLE [dbo].[map_layers]
    ADD CONSTRAINT [FK_map_layers_map_layers_type] FOREIGN KEY ([MapLayerTypeId]) REFERENCES [dbo].[map_layers_type] ([MapLayerTypeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


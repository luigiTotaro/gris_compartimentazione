﻿ALTER TABLE [dbo].[classification_values]
    ADD CONSTRAINT [FK_classification_values_classifications] FOREIGN KEY ([ClassificationId]) REFERENCES [dbo].[classifications] ([ClassificationId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


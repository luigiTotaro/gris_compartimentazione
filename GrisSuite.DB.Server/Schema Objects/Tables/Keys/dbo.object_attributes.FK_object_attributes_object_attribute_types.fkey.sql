﻿ALTER TABLE [dbo].[object_attributes]
    ADD CONSTRAINT [FK_object_attributes_object_attribute_types] FOREIGN KEY ([AttributeTypeId]) REFERENCES [dbo].[object_attribute_types] ([AttributeTypeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


﻿ALTER TABLE [dbo].[object_formulas_custom_colors]
    ADD CONSTRAINT [PK_object_formulas_custom_colors] PRIMARY KEY CLUSTERED ([ObjectFormulasCustomColorId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


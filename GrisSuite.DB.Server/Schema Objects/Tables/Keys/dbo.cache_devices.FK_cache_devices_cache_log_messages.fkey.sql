﻿ALTER TABLE dbo.cache_devices ADD CONSTRAINT
	FK_cache_devices_cache_log_messages FOREIGN KEY
	(
	LogID
	) REFERENCES dbo.cache_log_messages
	(
	LogID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 

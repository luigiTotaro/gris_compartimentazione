﻿ALTER TABLE [dbo].[resources]
    ADD CONSTRAINT [FK_resources_resource_categories] FOREIGN KEY ([ResourceCategoryID]) REFERENCES [dbo].[resource_categories] ([ResourceCategoryID]) ON DELETE NO ACTION ON UPDATE NO ACTION;


﻿ALTER TABLE [dbo].[object_classification_values]
    ADD CONSTRAINT [PK_object_classification_values_1] PRIMARY KEY CLUSTERED ([ObjectStatusId] ASC, [ClassificationValueId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


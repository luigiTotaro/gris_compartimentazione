﻿ALTER TABLE [dbo].[alerts_rules]  WITH CHECK ADD  CONSTRAINT [FK_alerts_rules_alerts_rules_templates] FOREIGN KEY([RuleTemplateID])
REFERENCES [dbo].[alerts_rules_templates] ([RuleTemplateID])


GO
ALTER TABLE [dbo].[alerts_rules] CHECK CONSTRAINT [FK_alerts_rules_alerts_rules_templates]


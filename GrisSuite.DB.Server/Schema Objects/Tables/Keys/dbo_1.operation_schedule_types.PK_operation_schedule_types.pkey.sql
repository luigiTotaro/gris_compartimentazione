﻿ALTER TABLE [dbo].[operation_schedule_types]
    ADD CONSTRAINT [PK_operation_schedule_types] PRIMARY KEY CLUSTERED ([OperationScheduleTypeId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


﻿ALTER TABLE [dbo].[permissions]
    ADD CONSTRAINT [FK_permissions_permission_groups] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[permission_groups] ([GroupID]) ON DELETE NO ACTION ON UPDATE NO ACTION;


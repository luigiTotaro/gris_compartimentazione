﻿CREATE TABLE [dbo].[alerts_rules_templates](
	[RuleTemplateID] [int] NOT NULL,
	[RuleTemplateDescription] [varchar](256) NOT NULL,
	[RuleTemplateExpression] [varchar](max) NOT NULL,
	[RuleTemplateExpressionType] [int] CONSTRAINT [DF_alerts_rules_templates_RuleTemplateExpressionType]  DEFAULT ((0)) NOT NULL,
 CONSTRAINT [PK_alerts_rules_templates] PRIMARY KEY CLUSTERED 
(
	[RuleTemplateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)

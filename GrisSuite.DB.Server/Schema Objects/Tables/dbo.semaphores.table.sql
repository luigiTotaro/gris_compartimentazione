CREATE TABLE [dbo].[semaphores] (
    [SemaphoreName] VARCHAR (64) NOT NULL,
    [Locked]        BIT          NOT NULL,
    [HostName]      VARCHAR (64) NULL,
    [LastUpdate]    DATETIME     NOT NULL
);



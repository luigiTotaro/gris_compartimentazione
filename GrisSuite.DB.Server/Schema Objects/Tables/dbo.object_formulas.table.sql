﻿CREATE TABLE [dbo].[object_formulas] (
    [ObjectStatusId]     UNIQUEIDENTIFIER NOT NULL,
    [FormulaIndex]       TINYINT          NOT NULL,
    [ScriptPath]         VARCHAR (2000)   NOT NULL,
    [FormulaGlobalIndex] BIGINT           NOT NULL
);


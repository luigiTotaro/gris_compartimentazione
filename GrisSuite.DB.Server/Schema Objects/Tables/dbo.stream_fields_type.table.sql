﻿CREATE TABLE [dbo].[stream_fields_type] (
    [FieldTypeId]                    INT          IDENTITY (1, 1) NOT NULL,
    [DevType]                        VARCHAR (16) NOT NULL,
    [StrID]                          INT          NOT NULL,
    [FieldID]                        INT          NOT NULL,
    [StreamName]                     VARCHAR (64) NOT NULL,
    [FieldName]                      VARCHAR (64) NOT NULL,
    [IsVisibleOnHistoryReportFilter] BIT          NOT NULL
);


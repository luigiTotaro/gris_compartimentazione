﻿CREATE TABLE [dbo].[device_ack] (
    [DeviceAckID]        UNIQUEIDENTIFIER NOT NULL,
    [DevID]              BIGINT           NOT NULL,
    [StrID]              INT              NULL,
    [FieldID]            INT              NULL,
    [AckDate]            DATETIME         NOT NULL,
    [AckDurationMinutes] INT              NOT NULL,
    [SupervisorID]       TINYINT          NOT NULL,
    [Username]           VARCHAR (256)    NOT NULL
);


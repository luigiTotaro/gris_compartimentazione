CREATE TABLE [dbo].[subzones] (
    [SubZoneId]   INT           NOT NULL,
    [SubZoneCode] VARCHAR (16)  NOT NULL,
    [Name]        VARCHAR (64)  NOT NULL,
    [MapPoints]   VARCHAR (MAX) NOT NULL,
    [NodeGISIdA]  VARCHAR (6)   NULL,
    [NodeGISIdB]  VARCHAR (6)   NULL
);






CREATE TABLE [dbo].[devices] (
    [DevID]                     BIGINT           NOT NULL,
    [NodID]                     BIGINT           NULL,
    [SrvID]                     BIGINT           NULL,
    [Name]                      VARCHAR (64)     NULL,
    [Type]                      VARCHAR (16)     NULL,
    [SN]                        VARCHAR (16)     NULL,
    [Addr]                      VARCHAR (32)     NULL,
    [PortId]                    UNIQUEIDENTIFIER NULL,
    [ProfileID]                 INT              NULL,
    [Active]                    TINYINT          NULL,
    [Scheduled]                 TINYINT          NULL,
    [RackID]                    UNIQUEIDENTIFIER NULL,
    [RackPositionRow]           INT              NULL,
    [RackPositionCol]           INT              NULL,
    [DefinitionVersion]         VARCHAR (8)      NULL,
    [ProtocolDefinitionVersion] VARCHAR (8)      NULL
);





﻿CREATE TABLE [dbo].[severity] (
    [SevLevel]                 INT           NOT NULL,
    [Description]              VARCHAR (128) NULL,
    [IsRelevantToDevice]       BIT           NOT NULL,
    [IsRelevantToServer]       BIT           NOT NULL,
    [IsRelevantToCustomColors] BIT           NOT NULL
);







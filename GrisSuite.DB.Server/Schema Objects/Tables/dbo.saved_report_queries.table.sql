﻿CREATE TABLE [dbo].[saved_report_queries]
(
[QueryName] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[QueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[QueryOwner] [tinyint] NOT NULL
) TEXTIMAGE_ON [PRIMARY]



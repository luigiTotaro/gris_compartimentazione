﻿CREATE TABLE [geta].[target_activities]
(
[TargetActivityID] [int] NOT NULL IDENTITY(1, 1),
[TargetActivityName] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[IsObsolete] [bit] NOT NULL,
[ControlTypeUI] [varchar] (64) COLLATE Latin1_General_CI_AS NOT NULL,
[TargetTypeID] [int] NOT NULL,
[EnumValues] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Order] [tinyint] NOT NULL,
[ActivityGroupsID] [int] NULL
) TEXTIMAGE_ON [PRIMARY]



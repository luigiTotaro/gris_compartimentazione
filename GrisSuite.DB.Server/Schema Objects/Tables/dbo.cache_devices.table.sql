﻿CREATE TABLE dbo.cache_devices
	(
	[LogID] [uniqueidentifier] NOT NULL,
	[DevID] [bigint] NOT NULL,
	[Type] [varchar] (16) COLLATE Latin1_General_CI_AS NULL
	)
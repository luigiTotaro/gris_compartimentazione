﻿CREATE TABLE [dbo].[cache_stream_fields_type] (
    [FieldTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [DevType]     VARCHAR (16)   NOT NULL,
    [StrID]       INT            NOT NULL,
    [FieldID]     INT            NOT NULL,
    [Name]        VARCHAR (64)   NOT NULL,
    [Format]      VARCHAR (50)   NOT NULL,
    [Factor]      DECIMAL (6, 3) NOT NULL
);


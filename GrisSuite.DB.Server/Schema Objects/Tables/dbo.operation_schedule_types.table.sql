﻿CREATE TABLE [dbo].[operation_schedule_types] (
    [OperationScheduleTypeId]   SMALLINT      NOT NULL,
    [OperationScheduleTypeCode] VARCHAR (20)  NOT NULL,
    [AssemblyName]              VARCHAR (200) NOT NULL
);


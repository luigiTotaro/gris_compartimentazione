﻿CREATE TABLE [geta].[issues] (
    [IssueID]             INT           IDENTITY (1, 1) NOT NULL,
    [TargetID]            BIGINT        NOT NULL,
    [TargetTypeID]        INT           NULL,
    [TargetActivityID]    INT           NULL,
    [IssueActivityTypeID] INT           NOT NULL,
    [TargetActivityValue] VARCHAR (MAX) NULL,
    [IssueComment]        VARCHAR (MAX) NULL,
    [DateCreation]        DATETIME      NOT NULL,
    [DateIssue]           DATETIME      NOT NULL,
    [IssueStateID]        INT           NOT NULL,
    [IssueChildID]        INT           NULL,
    [IsClosed]            BIT           NOT NULL,
    [IsDeleted]           BIT           NOT NULL,
    [IssueElapsedTime]    INT           NOT NULL,
    [ServerID]            BIGINT        NULL,
    [TargetData]          VARCHAR (MAX) NULL
);





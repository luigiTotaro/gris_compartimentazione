﻿CREATE TABLE [dbo].[operation_schedules] (
    [OperationScheduleId]     UNIQUEIDENTIFIER CONSTRAINT [DF_operation_schedules_OperationScheduleId] DEFAULT (newsequentialid()) NOT NULL,
    [OperationScheduleTypeId] SMALLINT         NOT NULL,
    [OperationParameters]     VARCHAR (MAX)    NULL,
    [DateCreated]             DATETIME         CONSTRAINT [DF_operation_schedules_DateCreated] DEFAULT (getdate()) NOT NULL,
    [DateExecuted]            DATETIME         NULL,
    [DateAborted]             DATETIME         NULL,
    [RetryNumber]             INT              CONSTRAINT [DF_operation_schedules_RetryNumber] DEFAULT ((1)) NOT NULL,
    [Status]                  VARCHAR (1000)   CONSTRAINT [DF_operation_schedules_Status] DEFAULT ('') NOT NULL,
    [Code]                    VARCHAR (100)    CONSTRAINT [DF_operation_schedules_Code] DEFAULT ('') NOT NULL,
    [Message]                 VARCHAR (MAX)    CONSTRAINT [DF_operation_schedules_Message] DEFAULT ('') NOT NULL,
    [ExecuteParameters]       VARCHAR (MAX)    CONSTRAINT [DF_operation_schedules_ExecuteParameters] DEFAULT ('') NOT NULL,
    [DeliveryParameters]      VARCHAR (MAX)    CONSTRAINT [DF_operation_schedules_DeliveryParameters] DEFAULT ('') NOT NULL,
    [OperationKeys]           VARCHAR (MAX)    NOT NULL,
    CONSTRAINT [PK_operation_schedules] PRIMARY KEY CLUSTERED ([OperationScheduleId] ASC),
    CONSTRAINT [FK_operation_schedules_operation_schedule_types] FOREIGN KEY ([OperationScheduleTypeId]) REFERENCES [dbo].[operation_schedule_types] ([OperationScheduleTypeId])
);






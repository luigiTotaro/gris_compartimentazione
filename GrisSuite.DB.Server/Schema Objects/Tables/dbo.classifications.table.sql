﻿CREATE TABLE [dbo].[classifications] (
    [ClassificationId]    TINYINT       NOT NULL,
    [ClassificationName]  VARCHAR (100) NOT NULL,
    [IsVisible]           BIT           CONSTRAINT [DF_classifications_IsVisible] DEFAULT ((0)) NOT NULL,
    [CanFilterBySystemId] BIT           NOT NULL,
    CONSTRAINT [PK_object_classifications] PRIMARY KEY CLUSTERED ([ClassificationId] ASC)
);










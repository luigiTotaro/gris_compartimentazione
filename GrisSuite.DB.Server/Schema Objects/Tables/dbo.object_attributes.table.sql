﻿CREATE TABLE [dbo].[object_attributes] (
    [AttributeId]                     BIGINT           IDENTITY (1, 1) NOT NULL,
    [AttributeTypeId]                 SMALLINT         NOT NULL,
    [ObjectStatusId]                  UNIQUEIDENTIFIER NOT NULL,
    [AttributeValue]                  VARCHAR (MAX)    NULL,
    [AttributeDataType]               VARCHAR (100)    NOT NULL,
    [ComputedByFormulaObjectStatusId] UNIQUEIDENTIFIER NULL,
    [ComputedByFormulaIndex]          TINYINT          NULL
);




﻿CREATE TABLE [dbo].[technology] (
    [TechnologyID]          INT           NOT NULL,
    [TechnologyCode]        VARCHAR (64)  NOT NULL,
    [TechnologyDescription] VARCHAR (256) NOT NULL
);


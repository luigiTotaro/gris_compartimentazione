﻿CREATE TABLE [dbo].[alerts_devices_logs]
(
[AlertDeviceLogID] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[DevID] [bigint] NOT NULL,
[RuleID] [int] NOT NULL,
[LastUpdate] [datetime] NOT NULL,
[IsOpen] [bit] NOT NULL,
[CreationDate] [datetime] NOT NULL,
[CloseDate] [datetime] NULL
)



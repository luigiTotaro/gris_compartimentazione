﻿CREATE TABLE [dbo].[permission_groups] (
    [GroupID]          INT            IDENTITY (1, 1) NOT NULL,
    [GroupName]        VARCHAR (500)  NOT NULL,
    [GroupDescription] VARCHAR (2000) NOT NULL,
    [IsBuiltIn]        BIT            NOT NULL,
    [WindowsGroups]    VARCHAR (MAX)  NOT NULL,
    [RegCode]          TINYINT        NOT NULL,
    [Level]            TINYINT        NOT NULL,
    [SortOrder]        TINYINT        NOT NULL
);




















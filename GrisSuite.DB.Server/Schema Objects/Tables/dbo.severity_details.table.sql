﻿CREATE TABLE [dbo].[severity_details] (
    [SevLevelDetailId] INT           NOT NULL,
    [SevLevel]         INT           NOT NULL,
    [Description]      VARCHAR (255) NOT NULL
);


﻿CREATE TABLE [dbo].[resources] (
    [ResourceID]          INT            NOT NULL IDENTITY (1, 1),
    [ResourceCode]        VARCHAR (100)  NOT NULL,
    [ResourceDescription] VARCHAR (1000) NULL,
    [SortOrder]           SMALLINT       NULL,
    [ResourceCategoryID]  INT            NOT NULL,
    [ResourceTypeID]      SMALLINT       NOT NULL
);


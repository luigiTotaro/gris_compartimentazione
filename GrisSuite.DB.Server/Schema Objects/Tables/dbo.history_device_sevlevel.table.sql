﻿CREATE TABLE [dbo].[history_device_sevlevel] (
    [ID]       UNIQUEIDENTIFIER CONSTRAINT [DF_hhistory_device_sevlevel_ID] DEFAULT (newsequentialid()) NOT NULL,
    [DevID]    BIGINT           NOT NULL,
    [SevLevel] INT              NOT NULL,
    [Created]  DATETIME         NOT NULL,
    [DevType]  VARCHAR (16)     NULL,
    [NodId]    BIGINT           NULL,
    [SrvId]    BIGINT           NULL,
    [Duration] INT              NULL,
    CONSTRAINT [PK_history_device_sevlevel] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);




GO
CREATE NONCLUSTERED INDEX [IX_history_device_sevlevel_DevType]
    ON [dbo].[history_device_sevlevel]([DevType] ASC)
    INCLUDE([DevID], [SevLevel], [Created], [NodId], [SrvId], [Duration]);


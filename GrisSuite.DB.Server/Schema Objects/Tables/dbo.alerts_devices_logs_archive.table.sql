﻿CREATE TABLE [dbo].[alerts_devices_logs_archive] (
    [AlertDeviceLogID] UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [DevID]            BIGINT           NOT NULL,
    [RuleID]           INT              NOT NULL,
    [LastUpdate]       DATETIME         NOT NULL,
    [IsOpen]           BIT              NOT NULL,
    [CreationDate]     DATETIME         NOT NULL,
    [CloseDate]        DATETIME         NULL
);


CREATE TABLE [dbo].[nodesGIS] (
    [NodeGISId]      VARCHAR (6)   NOT NULL,
    [Name]           VARCHAR (64)  NOT NULL,
    [MapPoints]      VARCHAR (MAX) NOT NULL,
    [Metallo]        VARCHAR (64)  NULL,
    [Dci]            VARCHAR (64)  NULL,
    [TipoLocalita]   VARCHAR (64)  NULL,
    [ClasseCDS]      VARCHAR (64)  NULL,
    [Presenziamento] VARCHAR (64)  NULL,
    [Dimensioni]     VARCHAR (64)  NULL,
    [Network]        VARCHAR (64)  NULL,
    [StatoRete2000]  VARCHAR (64)  NULL
);




﻿CREATE TABLE [dbo].[device_status] (
    [DevID]                         BIGINT        CONSTRAINT [DF_device_status_DevID] DEFAULT ((0)) NOT NULL,
    [SevLevel]                      INT           NULL,
    [Description]                   VARCHAR (256) NULL,
    [Offline]                       TINYINT       NULL,
    [ShouldSendNotificationByEmail] TINYINT       CONSTRAINT [DF_device_status_ShouldSendNotificationByEmail] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_device_status] PRIMARY KEY CLUSTERED ([DevID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_device_status_devices] FOREIGN KEY ([DevID]) REFERENCES [dbo].[devices] ([DevID]),
    CONSTRAINT [FK_device_status_severity] FOREIGN KEY ([SevLevel]) REFERENCES [dbo].[severity] ([SevLevel])
);










GO



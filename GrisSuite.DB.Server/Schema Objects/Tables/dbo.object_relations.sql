﻿CREATE TABLE [dbo].[object_relations] (
    [ObjectRelationId]      INT              IDENTITY (1, 1) NOT NULL,
    [ObjectStatusId]        UNIQUEIDENTIFIER NOT NULL,
    [RelatedObjectStatusId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_object_relations] PRIMARY KEY CLUSTERED ([ObjectRelationId] ASC)
);


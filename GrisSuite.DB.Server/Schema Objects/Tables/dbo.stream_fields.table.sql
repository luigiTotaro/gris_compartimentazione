﻿CREATE TABLE [dbo].[stream_fields] (
    [DevID]                         BIGINT           CONSTRAINT [DF_stream_fields_DevID] DEFAULT ((0)) NOT NULL,
    [StrID]                         INT              NOT NULL,
    [FieldID]                       INT              NOT NULL,
    [ArrayID]                       INT              NOT NULL,
    [Name]                          VARCHAR (64)     NULL,
    [SevLevel]                      INT              NULL,
    [Value]                         VARCHAR (1024)   NULL,
    [Description]                   TEXT             NULL,
    [Visible]                       TINYINT          NULL,
    [ReferenceID]                   UNIQUEIDENTIFIER NULL,
    [ShouldSendNotificationByEmail] TINYINT          CONSTRAINT [DF_stream_fields_ShouldSendNotificationByEmail] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_stream_fields] PRIMARY KEY CLUSTERED ([DevID] ASC, [StrID] ASC, [FieldID] ASC, [ArrayID] ASC) WITH (FILLFACTOR = 80, ALLOW_PAGE_LOCKS = OFF)
);







GO
CREATE NONCLUSTERED INDEX [SCR_Tuning_IX_stream_fields_02072013]
    ON [dbo].[stream_fields]([DevID] ASC, [StrID] ASC, [Visible] ASC)
    INCLUDE([FieldID], [ArrayID], [Name], [SevLevel], [Value]);


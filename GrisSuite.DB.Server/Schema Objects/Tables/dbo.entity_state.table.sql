﻿CREATE TABLE [dbo].[entity_state]
(
[EntityStateID] [tinyint] NOT NULL,
[EntityStateName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EntityStateSeverity] DECIMAL (5, 2) NOT NULL,
[EntityStateMinValue] [smallint] NOT NULL,
[EntityStateMaxValue] [smallint] NOT NULL
)


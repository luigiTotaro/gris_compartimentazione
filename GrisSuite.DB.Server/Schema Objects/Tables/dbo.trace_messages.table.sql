﻿CREATE TABLE [dbo].[trace_messages]
(
[TraceMessageID] [int] NOT NULL IDENTITY(1, 1),
[SessionKey] [char] (24) COLLATE Latin1_General_CI_AS NOT NULL,
[Message] [varchar] (2048) COLLATE Latin1_General_CI_AS NOT NULL,
[Exception] [xml] NULL,
[FormValues] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SessionValues] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Created] [datetime] NOT NULL
) TEXTIMAGE_ON [PRIMARY]



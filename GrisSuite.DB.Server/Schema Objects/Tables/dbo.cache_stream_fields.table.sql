﻿CREATE TABLE [dbo].[cache_stream_fields]
(
[DevID] [bigint] NOT NULL,
[StrID] [int] NOT NULL,
[FieldID] [int] NOT NULL,
[ArrayID] [int] NOT NULL,
[Name] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[SevLevel] [int] NULL,
[Value] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[Visible] [tinyint] NULL,
[Description] [text] COLLATE Latin1_General_CI_AS NULL,
[LogID] [uniqueidentifier] NOT NULL,
[ReferenceID] [uniqueidentifier] NULL,
[DevType] [varchar] (16) COLLATE Latin1_General_CI_AS NULL
)
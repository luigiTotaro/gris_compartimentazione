﻿CREATE TABLE [dbo].[contacts] (
    [ContactId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [RegId]       BIGINT         NOT NULL,
    [Name]        VARCHAR (2000) NOT NULL,
    [PhoneNumber] VARCHAR (500)  CONSTRAINT [DF_telephone_book_PhoneNumber] DEFAULT ('') NULL,
    [Email]       VARCHAR (500)  NULL,
    [WindowsUser] VARCHAR (500)  NULL,
    CONSTRAINT [PK_telephone_book] PRIMARY KEY CLUSTERED ([ContactId] ASC) WITH (FILLFACTOR = 80)
);




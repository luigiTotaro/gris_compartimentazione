﻿CREATE TABLE [dbo].[virtual_objects] (
    [VirtualObjectID]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [VirtualObjectName] VARCHAR (500) NOT NULL,
    [VirtualObjectDescription] VARCHAR (4000) NULL
);


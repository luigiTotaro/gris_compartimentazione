CREATE TABLE [dbo].[port] (
    [PortID]     UNIQUEIDENTIFIER NOT NULL,
    [PortXMLID]  INT              NULL,
    [PortName]   VARCHAR (64)     NULL,
    [PortType]   VARCHAR (64)     NULL,
    [Parameters] VARCHAR (64)     NULL,
    [Status]     VARCHAR (64)     NULL,
    [Removed]    TINYINT          NULL,
    [SrvID]      BIGINT           NOT NULL
);





﻿CREATE TABLE [dbo].[requests]
(
[RequestID] [bigint] NOT NULL IDENTITY(1, 1),
[SessionKey] [char] (24) COLLATE Latin1_General_CI_AS NOT NULL,
[Page] [varchar] (128) COLLATE Latin1_General_CI_AS NULL,
[QueryString] [varchar] (128) COLLATE Latin1_General_CI_AS NULL,
[Created] [datetime] NOT NULL
)



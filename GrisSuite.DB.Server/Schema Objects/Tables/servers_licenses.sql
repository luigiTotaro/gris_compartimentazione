﻿CREATE TABLE [dbo].[servers_licenses] (
    [ServerLicenseID]       INT           IDENTITY (1, 1) NOT NULL,
    [SrvID]                 BIGINT        NOT NULL,
    [IP]                    VARCHAR (16)  NOT NULL,
    [LicenseLastUpdate]     DATETIME      NOT NULL,
    [LicenseLastUpdateUser] VARCHAR (100) NOT NULL,
    [IsLicensed]            TINYINT       CONSTRAINT [DF_servers_licenses_IsLicensed] DEFAULT ((0)) NOT NULL,
    [HardwareProfile]       VARCHAR (64)  NOT NULL,
    CONSTRAINT [PK_servers_licenses] PRIMARY KEY CLUSTERED ([ServerLicenseID] ASC),
    CONSTRAINT [UQ_servers_licenses_SrvID] UNIQUE NONCLUSTERED ([SrvID] ASC)
);


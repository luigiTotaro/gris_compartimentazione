﻿CREATE TABLE [dbo].[resource_types] (
    [ResourceTypeID]          SMALLINT       NOT NULL,
    [ResourceTypeCode]        VARCHAR (100)  NOT NULL,
    [ResourceTypeDescription] VARCHAR (1000) NULL
);


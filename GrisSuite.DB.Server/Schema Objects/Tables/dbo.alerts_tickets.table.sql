﻿CREATE TABLE [dbo].[alerts_tickets]
(
[AlertTicketID] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[DevID] [bigint] NOT NULL,
[TicketName] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[TicketNote] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[TicketDate] [datetime] NULL,
[LastUpdate] [datetime] NOT NULL,
[IsOpen] [bit] NOT NULL,
[CreationDate] [datetime] NOT NULL,
[CloseDate] [datetime] NULL
) TEXTIMAGE_ON [PRIMARY]



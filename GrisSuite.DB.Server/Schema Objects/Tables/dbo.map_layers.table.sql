CREATE TABLE [dbo].[map_layers] (
    [MapLayerId]            INT           NOT NULL,
    [ClassificationValueId] SMALLINT      NOT NULL,
    [LayerSequence]         REAL          NOT NULL,
    [ShapefileUri]          VARCHAR (255) NULL,
    [VisibleFromScale]      FLOAT         NOT NULL,
    [VisibleToScale]        FLOAT         NOT NULL,
    [IsSensitive]           BIT           NOT NULL,
    [ShowElementsSymbols]   BIT           NOT NULL,
    [ShowElementsTooltips]  BIT           NOT NULL,
    [SymbolSize]            FLOAT         NOT NULL,
    [MapLayerTypeId]        TINYINT       NOT NULL,
    [ShowElementsStatus]    BIT           NOT NULL,
    [CaptionFromScale]      FLOAT         NOT NULL
);












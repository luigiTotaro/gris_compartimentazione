﻿CREATE TABLE [dbo].[resource_categories] (
    [ResourceCategoryID]          INT            NOT NULL IDENTITY (1, 1),
    [ResourceCategoryCode]        VARCHAR (100)  NOT NULL,
    [ResourceCategoryDescription] VARCHAR (1000) NULL,
    [SortOrder]                   TINYINT        NOT NULL
);


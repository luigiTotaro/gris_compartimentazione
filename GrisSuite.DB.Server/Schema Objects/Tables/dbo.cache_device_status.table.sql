﻿CREATE TABLE [dbo].[cache_device_status]
(
[DevID] [bigint] NOT NULL,
[SevLevel] [int] NULL,
[Description] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[LogID] [uniqueidentifier] NOT NULL,
[Offline] [tinyint] NULL,
[OriginalSevLevel] [int] NULL,
[DevType] [varchar] (16) COLLATE Latin1_General_CI_AS NULL
)



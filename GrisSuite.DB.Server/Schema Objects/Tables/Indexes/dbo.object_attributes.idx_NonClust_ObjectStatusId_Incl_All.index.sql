﻿CREATE NONCLUSTERED INDEX [idx_NonClust_ObjectStatusId_Incl_All]
    ON [dbo].[object_attributes]([ObjectStatusId] ASC)
    INCLUDE([AttributeId], [AttributeTypeId], [AttributeValue], [AttributeDataType], [ComputedByFormulaObjectStatusId], [ComputedByFormulaIndex]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


﻿CREATE NONCLUSTERED INDEX [idx_nonclust_deltadevid_deltastrid_deltafieldid_deltaarrayid]
    ON [dbo].[reference]([DeltaDevID] ASC, [DeltaStrID] ASC, [DeltaFieldID] ASC, [DeltaArrayID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


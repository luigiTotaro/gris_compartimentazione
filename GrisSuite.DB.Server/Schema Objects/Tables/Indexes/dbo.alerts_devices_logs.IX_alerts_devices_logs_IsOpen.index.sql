﻿CREATE NONCLUSTERED INDEX [IX_alerts_devices_logs_IsOpen]
    ON [dbo].[alerts_devices_logs]([IsOpen] ASC)
    INCLUDE([AlertDeviceLogID], [DevID], [RuleID], [LastUpdate], [CreationDate], [CloseDate]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


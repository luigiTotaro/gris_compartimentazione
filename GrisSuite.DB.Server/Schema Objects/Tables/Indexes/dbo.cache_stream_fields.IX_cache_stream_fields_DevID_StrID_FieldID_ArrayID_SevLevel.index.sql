﻿CREATE NONCLUSTERED INDEX [IX_cache_stream_fields_DevID_StrID_FieldID_ArrayID_SevLevel]
    ON [dbo].[cache_stream_fields]([DevID] ASC, [StrID] ASC, [FieldID] ASC, [ArrayID] ASC, [SevLevel] ASC)
    INCLUDE([LogID]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


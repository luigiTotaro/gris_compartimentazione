﻿CREATE NONCLUSTERED INDEX [idx_nc_ServerDateValidationRequested_ServerDateValidationObtained_i_SrvID_FullHostName_IP] ON [dbo].[servers] ([ServerDateValidationRequested], [ServerDateValidationObtained]) INCLUDE ([SrvID], [FullHostName], [IP]) ON [PRIMARY]



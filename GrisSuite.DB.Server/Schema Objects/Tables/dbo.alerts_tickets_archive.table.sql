﻿CREATE TABLE [dbo].[alerts_tickets_archive] (
    [AlertTicketID] UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [DevID]         BIGINT           NOT NULL,
    [TicketName]    VARCHAR (30)     NULL,
    [TicketNote]    VARCHAR (MAX)    NULL,
    [TicketDate]    DATETIME         NULL,
    [LastUpdate]    DATETIME         NOT NULL,
    [IsOpen]        BIT              NOT NULL,
    [CreationDate]  DATETIME         NOT NULL,
    [CloseDate]     DATETIME         NULL
);


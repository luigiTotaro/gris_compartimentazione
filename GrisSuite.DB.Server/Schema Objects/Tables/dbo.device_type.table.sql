﻿CREATE TABLE [dbo].[device_type] (
    [DeviceTypeID]          VARCHAR (16)  NOT NULL,
    [SystemID]              INT           NOT NULL,
    [VendorID]              INT           NOT NULL,
    [DeviceTypeDescription] VARCHAR (512) NULL,
    [WSUrlPattern]          VARCHAR (512) NULL,
    [Order]                 INT           NULL,
    [TechnologyID]          INT           NULL,
    [Obsolete]              BIT           CONSTRAINT [DF_device_type_Obsolete] DEFAULT ((0)) NOT NULL,
    [DefaultName]           VARCHAR (512) CONSTRAINT [DF_device_type_DefaultName] DEFAULT ('') NOT NULL,
    [PortType]              VARCHAR (20)  CONSTRAINT [DF_device_type_PortType] DEFAULT ('TCP_Client') NOT NULL,
    [SnmpCommunity]         VARCHAR (50)  NULL,
    [ImageName]             VARCHAR (100) NULL,
    [GlobalOrder]           INT           CONSTRAINT [DF_device_type_GlobalOrder] DEFAULT ((0)) NOT NULL,
    [ExportToList]          BIT           CONSTRAINT [DF_device_type_ExportToList] DEFAULT ((1)) NOT NULL,
    [DiscoveryType]         TINYINT       CONSTRAINT [DF_device_type_DiscoveryType] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_device_type] PRIMARY KEY CLUSTERED ([DeviceTypeID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_device_type_DiscoveryType] CHECK ([DiscoveryType]=(1) OR [DiscoveryType]=(2) OR [DiscoveryType]=(3)),
    CONSTRAINT [CK_device_type_PortType] CHECK ([PortType]='TCP_Client' OR [PortType]='STLC1000_RS422' OR [PortType]='STLC1000_RS485' OR [PortType]='RS232' OR [PortType]='serial'),
    CONSTRAINT [FK_device_type_systems] FOREIGN KEY ([SystemID]) REFERENCES [dbo].[systems] ([SystemID]),
    CONSTRAINT [IX_device_type_SystemAndOrder] UNIQUE NONCLUSTERED ([SystemID] ASC, [Order] ASC) WITH (FILLFACTOR = 90)
);

















GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1 = Standard, 2 = Tipo base per discovery, 3 = Tipo rilevato durante discovery', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'device_type', @level2type = N'COLUMN', @level2name = N'DiscoveryType';


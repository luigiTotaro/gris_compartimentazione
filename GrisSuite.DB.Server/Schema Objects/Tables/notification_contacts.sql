﻿CREATE TABLE [dbo].[notification_contacts] (
    [NotificationContactId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ObjectStatusId]        UNIQUEIDENTIFIER NOT NULL,
    [ContactId]             BIGINT           NOT NULL,
    [ResourceId]            INT              NULL,
    CONSTRAINT [PK_notification_addressees] PRIMARY KEY CLUSTERED ([NotificationContactId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_notification_contacts_contacts] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[contacts] ([ContactId]),
    CONSTRAINT [notification_contacts_resources_ResourceID_fk] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[resources] ([ResourceID]) ON DELETE CASCADE
);




﻿CREATE TABLE [geta].[activity_groups]
(
[ActivityGroupID] [int] NOT NULL IDENTITY(1, 1),
[ActivityGroupName] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[OnlyOpenActivityState] [bit] NOT NULL,
[Order] [tinyint] NOT NULL
)



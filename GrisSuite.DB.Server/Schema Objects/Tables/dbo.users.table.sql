﻿CREATE TABLE [dbo].[users]
(
[UserID] [int] NOT NULL IDENTITY(1, 1),
[Username] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[FirstName] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[OrganizationID] [int] NULL,
[UserGroupID] [int] NULL,
[MobilePhone] [varchar] (64) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (128) COLLATE Latin1_General_CI_AS NULL
)


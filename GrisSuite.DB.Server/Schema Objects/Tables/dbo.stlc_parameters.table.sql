CREATE TABLE [dbo].[stlc_parameters] (
    [SrvID]                BIGINT         NOT NULL,
    [ParameterName]        VARCHAR (64)   NOT NULL,
    [ParameterValue]       VARCHAR (256)  NOT NULL,
    [ParameterDescription] VARCHAR (1024) NULL
);





﻿CREATE TABLE [dbo].[cache_reference]
(
[LogID] [uniqueidentifier] NOT NULL,
[ReferenceID] [uniqueidentifier] NOT NULL,
[Value] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[DateTime] [datetime] NOT NULL,
[Visible] [tinyint] NULL,
[DeltaDevID] [bigint] NULL,
[DeltaStrID] [int] NULL,
[DeltaFieldID] [int] NULL,
[DeltaArrayID] [int] NULL,
[DevType] [varchar] (16) COLLATE Latin1_General_CI_AS NULL
)



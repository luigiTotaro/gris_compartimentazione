﻿CREATE TABLE [dbo].[events_define] (
    [DevType]   VARCHAR (16)  NOT NULL,
    [EventCode] INT           NOT NULL,
    [EventDesc] VARCHAR (256) NULL
);


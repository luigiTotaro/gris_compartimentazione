﻿CREATE TABLE [dbo].[sessions]
(
[SessionID] [bigint] NOT NULL IDENTITY(1, 1),
[SessionKey] [char] (24) COLLATE Latin1_General_CI_AS NOT NULL,
[IPAddress] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[Agent] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[User] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[Groups] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[Created] [datetime] NOT NULL
)



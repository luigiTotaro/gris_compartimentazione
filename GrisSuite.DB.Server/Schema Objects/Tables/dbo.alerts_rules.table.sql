﻿CREATE TABLE [dbo].[alerts_rules](
	[RuleID] [int] NOT NULL,
	[RuleDescription] [varchar](256) NOT NULL,
	[RuleParameters] [varchar](max) NOT NULL,
	[IsEnabled] [bit] NOT NULL CONSTRAINT [DF_alerts_rules_IsEnabled]  DEFAULT ((1)),
	[RuleTemplateID] [int] NOT NULL,
	[RuleSevLevel] [int] NOT NULL CONSTRAINT [DF_alerts_rules_RuleSevLevel]  DEFAULT ((0)),
	[ExecutionSequence] [int] NOT NULL CONSTRAINT [DF_alerts_rules_ExecutionSequence]  DEFAULT ((0)),
 CONSTRAINT [PK_alerts_rules] PRIMARY KEY CLUSTERED 
(
	[RuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)



﻿CREATE TABLE [dbo].[servers_parking] (
    [ServerParkingID] INT          IDENTITY (1, 1) NOT NULL,
    [IP]              VARCHAR (16) NOT NULL,
    [SrvID]           BIGINT       NULL,
    [Host]            VARCHAR (64) NOT NULL,
    [Name]            VARCHAR (64) NULL,
    [Type]            VARCHAR (64) NULL,
    [LastUpdate]      DATETIME     NOT NULL,
    [ClientRegID]     VARCHAR (64) NULL,
    [ClientZonID]     VARCHAR (64) NULL,
    [ClientNodID]     VARCHAR (64) NULL,
    [HardwareProfile] VARCHAR (64) NOT NULL,
    [ParkingType]     TINYINT      CONSTRAINT [DF_servers_parking_ParkingType] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_servers_parking] PRIMARY KEY CLUSTERED ([ServerParkingID] ASC) WITH (FILLFACTOR = 80)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Unknown = 0, -- Sconosciuto
ConfiguredServer = 1, -- Server con configurazione, con problemi su database o simili
MissingConfigurationServer = 2, -- Server privo di System.xml
PreviouslyParkedServer = 3, -- Server che è stato parcheggiato, mantenuto per storico, ma che ha centralizzato almeno una volta', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'servers_parking', @level2type = N'COLUMN', @level2name = N'ParkingType';


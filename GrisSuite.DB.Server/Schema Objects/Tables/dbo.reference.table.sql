﻿CREATE TABLE [dbo].[reference]
(
[ReferenceID] [uniqueidentifier] NOT NULL,
[Value] [varchar] (256) NULL,
[DateTime] [datetime] NOT NULL,
[Visible] [tinyint] NULL,
[DeltaDevID] [bigint] NULL,
[DeltaStrID] [int] NULL,
[DeltaFieldID] [int] NULL,
[DeltaArrayID] [int] NULL
)



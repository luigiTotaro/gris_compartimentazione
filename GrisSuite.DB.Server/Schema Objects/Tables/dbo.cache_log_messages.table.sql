CREATE TABLE [dbo].[cache_log_messages] (
    [LogID]        UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [HostSender]   VARCHAR (64)     NOT NULL,
    [Created]      DATETIME         NOT NULL,
    [MessageType]  VARCHAR (255)    NULL,
    [MAC]          VARCHAR (16)     NULL,
    [HostReceiver] VARCHAR (64)     NULL
);





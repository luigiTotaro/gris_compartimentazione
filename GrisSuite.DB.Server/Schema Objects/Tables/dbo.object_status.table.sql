CREATE TABLE [dbo].[object_status] (
    [ObjectStatusId]          UNIQUEIDENTIFIER ROWGUIDCOL NOT NULL,
    [ObjectId]                BIGINT           NOT NULL,
    [ObjectTypeId]            INT              NOT NULL,
    [SevLevel]                INT              NOT NULL,
    [SevLevelDetailId]        INT              NULL,
    [ParentObjectStatusId]    UNIQUEIDENTIFIER NULL,
    [InMaintenance]           BIT              NOT NULL,
    [SevLevelReal]            INT              NULL,
    [SevLevelDetailIdReal]    INT              NULL,
    [SevLevelLast]            INT              NULL,
    [SevLevelDetailIdLast]    INT              NULL,
    [Ok]                      INT              NULL,
    [Warning]                 INT              NULL,
    [Error]                   INT              NULL,
    [Maintenance]             INT              NULL,
    [Unknown]                 INT              NULL,
    [ForcedSevLevel]          TINYINT          NULL,
    [LastUpdateGuid]          UNIQUEIDENTIFIER NULL,
    [ExcludeFromParentStatus] BIT              NOT NULL,
    [ForcedByUser]            BIT              NOT NULL,
    [IsSmsNotificationEnabled] BIT			   NOT NULL
);












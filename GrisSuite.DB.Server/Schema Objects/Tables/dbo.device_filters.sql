﻿CREATE TABLE [dbo].[device_filters] (
    [DeviceFilterId]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [DeviceFilterName] VARCHAR (110) NOT NULL,
    [UserID]           INT           NULL,
    [RegIDs]           VARCHAR (MAX) CONSTRAINT [DF_device_filters_RegIDs] DEFAULT ('') NOT NULL,
    [RegNames]         VARCHAR (MAX) CONSTRAINT [DF_device_filters_RegNames] DEFAULT ('') NOT NULL,
    [ZonIDs]           VARCHAR (MAX) CONSTRAINT [DF_device_filters_ZoneIDs] DEFAULT ('') NOT NULL,
    [ZonNames]         VARCHAR (MAX) CONSTRAINT [DF_device_filters_ZonNames] DEFAULT ('') NOT NULL,
    [NodIDs]           VARCHAR (MAX) CONSTRAINT [DF_device_filters_NodIDs] DEFAULT ('') NOT NULL,
    [NodNames]         VARCHAR (MAX) CONSTRAINT [DF_device_filters_NodNames] DEFAULT ('') NOT NULL,
    [SystemIDs]        VARCHAR (MAX) CONSTRAINT [DF_device_filters_SystemIDs] DEFAULT ('') NOT NULL,
    [SystemNames]      VARCHAR (MAX) CONSTRAINT [DF_device_filters_SystemNames] DEFAULT ('') NOT NULL,
    [DeviceTypeIDs]    VARCHAR (MAX) CONSTRAINT [DF_device_filters_DeviceTypeIDs] DEFAULT ('') NOT NULL,
    [DeviceTypeNames]  VARCHAR (MAX) CONSTRAINT [DF_device_filters_DeviceTypeNames] DEFAULT ('') NOT NULL,
    [SevLevelIDs]      VARCHAR (MAX) CONSTRAINT [DF_device_filters_SevLevelIDs] DEFAULT ('') NOT NULL,
    [DeviceData]       VARCHAR (110) CONSTRAINT [DF_device_filters_DeviceData] DEFAULT ('') NOT NULL,
    [ResourceID]       INT           NULL,
    CONSTRAINT [PK_device_filters] PRIMARY KEY CLUSTERED ([DeviceFilterId] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_device_filters_resources] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[resources] ([ResourceID])
);




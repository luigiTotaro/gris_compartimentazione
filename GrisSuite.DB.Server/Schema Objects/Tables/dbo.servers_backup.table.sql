﻿CREATE TABLE [dbo].[servers_backup] (
    [SrvID]               INT           NOT NULL,
    [Name]                VARCHAR (64)  NULL,
    [Host]                VARCHAR (64)  NULL,
    [FullHostName]        VARCHAR (256) NULL,
    [IP]                  VARCHAR (16)  NULL,
    [SupervisorSystemXML] XML           NULL,
    [NodID]               BIGINT        NULL,
    [ServerVersion]       VARCHAR (32)  NULL,
    [MAC]                 VARCHAR (16)  NULL,
    [LastBackup]          DATETIME      NOT NULL
);


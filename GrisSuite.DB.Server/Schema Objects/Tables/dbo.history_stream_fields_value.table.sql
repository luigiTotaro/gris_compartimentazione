﻿CREATE TABLE [dbo].[history_stream_fields_value] (
    [ID]       UNIQUEIDENTIFIER NOT NULL,
    [DevID]    BIGINT           NOT NULL,
    [StrID]    INT              NOT NULL,
    [FieldID]  INT              NOT NULL,
    [ArrayID]  INT              NOT NULL,
    [Value]    VARCHAR (1024)   NULL,
    [SevLevel] INT              NULL,
    [Name]     VARCHAR (64)     NULL,
    [DevType]  VARCHAR (16)     NULL,
    [NodId]    BIGINT           NULL,
    [SrvId]    BIGINT           NULL,
    [Created]  DATETIME         NOT NULL,
    [Duration] INT              NULL
);


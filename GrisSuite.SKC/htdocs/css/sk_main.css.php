<?php
/**
* Telefin STLC1000 Consolle
*
* sk_main.css.php - Modulo per la generazione del foglio di stile principale.
*
* @author Enrico Alborali
* @version 1.0.4.1 27/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/
// Imposto intestazione per il foglio di stile
header('Content-type: text/css');
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");

$_pointer = ($_conf_console_pointer_visible == true)?"pointer":"none";
?>

body {
	background-color: #060000;
	padding: 0px;
	margin: 0px;
	border: 0px;
	/*cursor: <?php print($_pointer); ?>;*/
	min-width: 1000px;
	overflow: hidden;
}
a{
	/*cursor: <?php print($_pointer); ?>;*/
	padding: 0px;
	margin: 0px;
	border: 0px;
	text-decoration: none;
}
img{
	padding: 0px;
	margin: 0px;
	border: 0px;
}
div{
	padding: 0px;
	margin: 0px;
	border: 0px;
}

#mainbox{
	position: absolute;
	left: 0px;
	min-width: inherit;
	z-index: 0;
}

#topbar{
	position: absolute;
	top: 0px;
	left: 50%;
	margin-left: -500px;
	padding: 0px;
	width: 1000px;
	height: 120px;
	background-color: transparent;
	min-width: inherit;
	z-index: 1000;
}

#topbar_top{
	position: absolute;
	top: 0px;
	left: 0px;
	width: inherit;
	min-width: inherit;
	height: 20px;
	background-color: #060000;
	z-index: 1000;
}
#username{
	position: absolute;
	padding: 0px;
	margin: 0px;
	visibility: visible;
	right: 0px;
	top: 3px;
	width: 300px;
	height: 18px;
	text-align: right;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	text-shadow: #333333 0px 1px 0px;
	color: #ffffff;
	font-family: "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 1002;
}
#topbar_middle{
	position: absolute;
	top: 20px;
	left: 0px;
	width: inherit;
	min-width: inherit;
	height: 60px;
	background-color: #060000;
	z-index: 1000;
}
#topbar_middle_left{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 10px;
	height: 60px;
	z-index: 1001;
}
#topbar_middle_bg{
	position: absolute;
	top: 0px;
	left: 10px;
	width: 980px;
	height: 60px;
	z-index: 1001;
}
#topbar_middle_right{
	position: absolute;
	top: 0px;
	left: 990px;
	width: 10px;
	height: 60px;
	z-index: 1001;
}
#topbar_message{
	position: absolute;
	top: 110px;
	left: 40px;
	width: 920px;
	height: 40px;
	background-color: transparent;
	z-index: 998;
	display: none;
}
#topbar_message p{
	position: absolute;
	width: 900px;
	height: 13px;
	margin: 0px;
	padding: 0px;
	padding-left: 16px;
	padding-right: 16px;
	padding-top: 11px;
	padding-bottom: 6px;
	border: 0px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	text-shadow: #333333 0px 1px 0px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	cursor: pointer;
	z-index: 999;
	display: inherit;
}
.message_panel{
	position: absolute;
	z-index: 998;
	display: inherit;
}
#logo{
	position: absolute;
	top: 0px;
	left: 10px;
	width: 150px;
	height: 60px;
	padding: 0px;
	margin: 0px;
	border: 0px;
	z-index: 1002;
}
#app_name{
	position: absolute;
	top: 0px;
	left: 200px;
	width: 600px;
	height: 60px;
	z-index: 1002;
}
.topbar_page_button{
	position: absolute;
	top: 0px;
	width: 60px;
	height: 60px;
	cursor: pointer;
	z-index: 1001;
}
.topbar_page_button_selected{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 60px;
	height: 60px;
	z-index: 1002;
	display: inherit;
}
.topbar_page_button_image{
	position: absolute;
	left: 5px;
	top: 5px;
	width: 50px;
	height: 50px;
	z-index: 1003;
	display: inherit;
}
.topbar_page_button_title{
	position: absolute;
	left: 0px;
	top: 40px;
	width: 60px;
	height: 20px;
	margin: 0px;
	spacing: 0px;
	pagging: 0px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 10px;
	text-shadow: #000000 0px 0px 3px;
	color: #ffffff;
	font-family: "Lucida Grande", Tahoma, Verdana, Arial, sans-serif;
	cursor: pointer;
	z-index: 1004;
	display: inherit;
}
.topbar_page_button_over{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 60px;
	height: 60px;
	z-index: 1005;
	display: none;
}
.topbar_page_button_click{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 60px;
	height: 60px;
	z-index: 1006;
	display: none;
}
#topbar_bottom{
	position: absolute;
	top: 80px;
	left: 0px;
	width: inherit;
	min-width: inherit;
	height: 30px;
	background-color: #4C4C4C;
	z-index: 1000;
}
#navbar_left{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 10px;
	height: 30px;
	z-index: 1001;
}
#navbar_bg{
	position: absolute;
	top: 0px;
	left: 10px;
	width: 980px;
	height: 30px;
	z-index: 1001;
}
#navbar_right{
	position: absolute;
	top: 0px;
	left: 990px;
	width: 10px;
	height: 30px;
	z-index: 1001;
}

#topbar_shadow{
	position: absolute;
	top: 110px;
	left: 0px;
	width: inherit;
	min-width: inherit;
	height: 10px;
	background-color: transparent;
	z-index: 999;
}
#topbar_bg{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 998px;
	height: 10px;
	z-index: 1001;
}
#waiting{
	position: absolute;
	left: 50%;
	top: 0px;
	bottom: 0px;
	border-bottom: 0px;
	margin-left: -500px;
	padding: 0px;
	width: 1000px;
	background-color: transparent;
	z-index: 3000;
	display: none;
}
#waiting_bg{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 100%;
	height: 100%;
	z-index: 3000;
	display: inherit;
}
.waiting_icon{
	position: absolute;
	top: 50%;
	left: 475px;
	width: 50px;
	height: 50px;
	z-index: 3001;
	display: none;
}
.waiting_text{
	position: absolute;
	top: 50%;
	left: 0px;
	width: 1000px;
	height: 50px;
	margin: 0px;
	margin-top: 60px;
	padding-left: 5px;
	padding-right: 5px;
	padding-top: 5px;
	padding-bottom: 5px;
	border: 0px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 14px;
	text-decoration: none;
	color: #ffffff;
	text-shadow: 0px 1px 0px #111111;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 3001;
	display: inherit;
}
#content{
	position: absolute;
	left: 50%;
	top: 110px;
	bottom: 50px;
	border-bottom: 0px;
	border-top: 0px;
	border-left: 1px;
	border-right: 1px;
	border-style: solid;
	border-color: #202020;
	margin-left: -500px;
	padding: 0px;
	width: 998px;
	min-height: 100px;
	height: auto;
	background-color: #4C4C4C;
	z-index: 100;
}
#content_middle{
	position: absolute;
	top: 10px;
	left: 0px;
	width: 998px;
	height: 100%;
	background-color: #4C4C4C;
}
#content_details{
	position: absolute;
	top: 10px;
	left: 0px;
	height: auto;
	background-color: #4C4C4C;
}
#content_add{
	position: absolute;
	top: 10px;
	left: 0px;
	height: auto;
	background-color: #4C4C4C;
}
#bottombar{
	position: absolute;
	bottom: 0px;
	left: 50%;
	margin-left: -500px;
	padding: 0px;
	width: 1000px;
	height: 50px;
	background-color: transparent;
	min-width: inherit;
	z-index: 1000;
}
#bottombar_message{
	position: absolute;
	bottom: 50px;
	left: 40px;
	width: 920px;
	height: 40px;
	background-color: transparent;
	z-index: 998;
	display: none;
}
#bottombar_message p{
	position: absolute;
	width: 900px;
	height: 13px;
	margin: 0px;
	padding: 0px;
	padding-left: 16px;
	padding-right: 16px;
	padding-top: 6px;
	padding-bottom: 11px;
	border: 0px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	text-shadow: #333333 0px 1px 0px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	cursor: pointer;
	z-index: 999;
	display: inherit;
}
#bottombar_top{
	position: absolute;
	top: 0px;
	left: 0px;
	width: inherit;
	min-width: inherit;
	height: 30px;
	background-color: #404040;
	z-index: 1000;
}
#bottombar_bottom{
	position: absolute;
	top: 30px;
	left: 0px;
	width: inherit;
	min-width: inherit;
	height: 20px;
	background-color: #060000;
	z-index: 1000;
}
#app_version{
	position: absolute;
	padding: 0px;
	margin: 0px;
	left: 5px;
	top: 2px;
	width: 240px;
	height: 18px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	text-shadow: #333333 0px 1px 0px;
	color: #999999;
	font-family: "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 1002;
	visibility: visible;
}
#app_copyright{
	position: absolute;
	padding: 0px;
	margin: 0px;
	left: 250px;
	top: 2px;
	width: 490px;
	height: 18px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	text-shadow: #333333 0px 1px 0px;
	color: #999999;
	font-family: "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 1002;
	visibility: visible;
}
#app_credits{
	position: absolute;
	padding: 0px;
	margin: 0px;
	right: 5px;
	top: 2px;
	width: 240px;
	height: 18px;
	text-align: right;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	text-shadow: #333333 0px 1px 0px;
	color: #999999;
	font-family: "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 1002;
	visibility: visible;
}
.item{
	position: absolute;
	margin: 0px;
	padding: 0px;
	border: 0px;
	width: 220px;
	height: 100px;
	visibility: hidden;
	z-index: 200;
}
.item_panel_bg{
	position: absolute;
	left: 10px;
	top: 10px;
	width: 200px;
	height: 80px;
	background-color: #313131;
	visibility: inherit;
}
.item_panel_top_left{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.item_panel_top_right{
	position: absolute;
	right: 0px;
	top: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.item_panel_bottom_left{
	position: absolute;
	left: 0px;
	bottom: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.item_panel_bottom_right{
	position: absolute;
	right: 0px;
	bottom: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.item_panel_top{
	position: absolute;
	left: 10px;
	top: 0px;
	width: 200px;
	height: 10px;
	visibility: inherit;
}
.item_panel_bottom{
	position: absolute;
	left: 10px;
	bottom: 0px;
	width: 200px;
	height: 10px;
	visibility: inherit;
}
.item_panel_left{
	position: absolute;
	left: 0px;
	top: 10px;
	width: 10px;
	height: 80px;
	visibility: inherit;
}
.item_panel_right{
	position: absolute;
	right: 0px;
	top: 10px;
	width: 10px;
	height: 80px;
	visibility: inherit;
}
.item_panel_name{
	position: absolute;
	left: 65px;
	top: 10px;
	width: 145px;
	height: 30px;
	padding: 0px;
	margin: 0px;
	visibility: inherit;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	text-shadow: #333333 0px 1px 0px;
	color: #ffffff;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	overflow: hidden;
	text-overflow: ellipsis;
	visibility: inherit;
}
.item_panel_type{
	position: absolute;
	left: 65px;
	top: 45px;
	width: 145px;
	height: 15px;
	padding: 0px;
	margin: 0px;
	visibility: inherit;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	text-shadow: #333333 0px 1px 0px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	visibility: inherit;
}
.item_panel_status{
	position: absolute;
	left: 10px;
	top: 70px;
	width: 200px;
	height: 15px;
	padding: 0px;
	margin: 0px;
	visibility: inherit;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	text-shadow: #333333 0px 1px 0px;
	color: green;
	font-family: "Lucida Grande", Verdana, Arial, sans-serif;
	visibility: hidden;
}
.item_icon{
	position: absolute;
	left: 10px;
	top: 10px;
	width: 50px;
	height: 50px;
	visibility: visible;
	z-index: 100;
}
.item_icon_status_up{
	position: absolute;
	left: 10px;
	top: 10px;
	width: 50px;
	height: 50px;
	visibility: visible;
	z-index: 101;
}
.item_icon_status_down{
	position: absolute;
	left: 10px;
	top: 10px;
	width: 50px;
	height: 50px;
	visibility: visible;
	z-index: 99;
}

.item_name{
	position: absolute;
	left: 2px;
	top: 71px;
	width: 66px;
	height: 24px;
	padding: 0px;
	margin: 0px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 9px;
	text-shadow: #333333 0px 1px 0px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	overflow: hidden;
	text-overflow: ellipsis;
	visibility: visible;
	z-index: 100;
}
.item_detail{
	position: absolute;
	left: 14px;
	top: 6px;
	width: 970px;
	height: 500px;
	color: #ffffcc;
}

.item_panel_pill{
	position: absolute;
	top: 60px;
	height: 30px;
	z-index: 211;
}
.item_panel_pill_color{
	position: absolute;
	top: 66px;
	height: 20px;
	z-index: 210;
}
.item_panel_pill_number{
	position: absolute;
	top: 70px;
	height: 20px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 10px;
	text-shadow: #000000 0px 0px 2px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 212;
}

/* PANNELLO DETTAGLI */
.details_panel{
	position: absolute;
	margin: 0px;
	padding: 0px;
	border: 0px;
	left: 10px;
	top: 5px;
	width: 978px;
	height: 500px;
	z-index: 300;
	display: none;
}
.fixed_details_panel{
	position: absolute;
	margin: 0px;
	padding: 0px;
	border: 0px;
	left: 10px;
	top: 5px;
	width: 978px;
	height: 500px;
	z-index: 300;
	display: inherit;
}
.details_panel_top_left{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.details_panel_top{
	position: absolute;
	left: 10px;
	top: 0px;
	width: 958px;
	height: 10px;
	visibility: inherit;
}
.details_panel_top_right{
	position: absolute;
	right: 0px;
	top: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.details_panel_left{
	position: absolute;
	left: 0px;
	top: 10px;
	width: 10px;
	height: 480px;
	visibility: inherit;
}
.details_panel_bg{
	position: absolute;
	left: 10px;
	right: 10px;
	top: 10px;
	bottom: 10px;
	background-color: #313131;
	visibility: inherit;
}
.details_panel_right{
	position: absolute;
	right: 0px;
	top: 10px;
	width: 10px;
	height: 480px;
	visibility: inherit;
}
.details_panel_bottom_left{
	position: absolute;
	left: 0px;
	bottom: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.details_panel_bottom{
	position: absolute;
	left: 10px;
	bottom: 0px;
	width: 958px;
	height: 10px;
	visibility: inherit;
}
.details_panel_bottom_right{
	position: absolute;
	right: 0px;
	bottom: 0px;
	width: 10px;
	height: 10px;
	visibility: inherit;
}
.details_panel_button{
	position: absolute;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 306;
}
.details_panel_button_icon{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 307;
}
.details_panel_button_over{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 307;
}
.details_panel_button_click{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 307;
}
.add_panel_button{
	position: absolute;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 306;
}
.add_panel_button_icon{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 307;
}
.add_panel_button_over{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 307;
}
.add_panel_button_click{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 307;
}

.details_panel_icon{
	position: absolute;
	left: 10px;
	top: 10px;
	width: 100px;
	height: 100px;
	visibility: inherit;
	z-index: 302;
}
.details_panel_vcut_top{
	position: absolute;
	width: 1px;
	height: 40px;
	visibility: inherit;
	z-index: 302;
}
.details_panel_vcut_bg{
	position: absolute;
	width: 1px;
	visibility: inherit;
	z-index: 302;
}
.details_panel_vcut_bottom{
	position: absolute;
	width: 1px;
	height: 40px;
	visibility: inherit;
	z-index: 302;
}
.details_panel_hcut_bg{
	position: absolute;
	height: 1px;
	visibility: inherit;
	z-index: 302;
}
.details_panel_hcut_right{
	position: absolute;
	height: 1px;
	width: 40px;
	visibility: inherit;
	z-index: 302;
}
.details_panel_hlight{
	position: absolute;
	left: 34px;
	top: 75px;
	width: 900px;
	height: 40px;
	visibility: inherit;
	z-index: 301;
}
.details_panel_hshadow{
	position: absolute;
	left: 5px;
	top: 115px;
	width: 968px;
	height: 5px;
	visibility: inherit;
	z-index: 302;
}
.details_panel_selection_bg{
	position: absolute;
	height: 30px;
	visibility: inherit;
	z-index: 304;
}
.details_panel_selection_right{
	position: absolute;
	height: 30px;
	width: 13px;
	visibility: inherit;
	z-index: 334;
}
.details_panel_selection_bottom{
	position: absolute;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 334;
}
.details_panel_icon_status{
	position: absolute;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 305;
}
.details_panel_icon_array{
	position: absolute;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 305;
}
.details_panel_info{
	position: absolute;
	left: 120px;
	right: 120px;
	top: 10px;
	height: 100px;
	background-color: transparent;
	/*z-index: 305;*/
}
.details_panel_info_btns{
	background-color: transparent;
	z-index: 300;
}
.details_panel_info_name{
	position: absolute;
	height: 12px;
	width: 180px;
	margin: 0px;
	margin-top: 2px;
	margin-bottom: 2px;
	padding: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	text-shadow: #333333 0px 1px 0px;
	color: #999999;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 305;
}
.details_panel_info_value{
	position: absolute;
	height: 12px;
	width: 180px;
	margin: 0px;
	margin-top: 2px;
	margin-bottom: 2px;
	padding: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	text-shadow: #333333 0px 1px 0px;
	color: #cccccc;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	z-index: 305;
}
.details_panel_info_value:hover{
	color: #ffffff;
	text-shadow: 1px 0px 10px #333333, 2px 0px 10px #333333, 3px 0px 10px #333333, 4px 0px 9px #333333, 5px 0px 8px #333333, 6px 0px 7px #333333, 0px 1px 0px #111111;
	overflow: visible;
	z-index: 315;
}
.details_panel_info_link{
	position: absolute;
	height: 16px;
	width: 180px;
	margin: 0px;
	padding: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 14px;
	text-shadow: #333333 0px 1px 0px;
	color: #cccccc;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	cursor: pointer;
	z-index: 305;
}
.details_panel_info_link:hover{
	color: #ffffff;
	text-shadow: 1px 0px 10px #313131, 2px 0px 10px #313131, 3px 0px 10px #313131, 4px 0px 9px #313131, 5px 0px 8px #313131, 6px 0px 7px #313131, 0px 1px 0px #111111;
	overflow: visible;
	z-index: 315;
}
.details_panel_info_link:active{
	color: #999999;
	text-shadow: 0px 1px 0px #000000;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
.add_panel_form{
	position: absolute;
	left: 120px;
	right: 120px;
	top: 10px;
	height: 100px;
	background-color: transparent;
	z-index: 305;
	display: inherit;
}
.add_panel_form_btns{
	background-color: transparent;
	z-index: 300;
	display: inherit;
}
.details_panel_form{
	position: absolute;
	left: 120px;
	right: 120px;
	top: 10px;
	height: 100px;
	background-color: transparent;
	z-index: 305;
	display: none;
}
.details_panel_form_btns{
	background-color: transparent;
	z-index: 300;
	display: none;
}
.details_panel_form_label{
	position: absolute;
	height: 16px;
	width: 180px;
	margin: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #999999;
	text-shadow: 0px 1px 0px #111111;
	z-index: 305;
}
.details_panel_form_input{
	position: absolute;
	height: 16px;
	margin: 0px;
	margin-top: 2px;
	margin-bottom: 2px;
	padding-left: 2px;
	padding-right: 2px;
	padding-top: 5px;
	padding-bottom: 5px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #cccccc;
	text-shadow: 0px 1px 0px #111111;
	background-color: #555555;
	z-index: 305;
}
.details_panel_form_select{
	position: absolute;
	height: 16px;
	margin: 0px;
	margin-top: 2px;
	margin-bottom: 2px;
	padding-left: 2px;
	padding-right: 30px;
	padding-top: 5px;
	padding-bottom: 5px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #cccccc;
	text-shadow: 0px 1px 0px #111111;
	background-color: #555555;
	z-index: 305;
}
.details_panel_form_search{
	position: absolute;
	height: 16px;
	margin: 0px;
	margin-top: 2px;
	margin-bottom: 2px;
	padding-left: 2px;
	padding-right: 30px;
	padding-top: 5px;
	padding-bottom: 5px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #cccccc;
	text-shadow: 0px 1px 0px #111111;
	background-color: #555555;
	z-index: 305;
}
.details_panel_select_down_icon{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: inherit;
	z-index: 305;
}
.details_panel_select_up_icon{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 306;
}
.details_panel_search_icon{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 307;
}
.details_panel_text_name{
	position: absolute;
	height: 16px;
	width: 180px;
	margin: 0px;
	padding: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #cccccc;
	text-shadow: 0px 1px 0px #111111;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	cursor: pointer;
	z-index: 305;
}
.details_panel_text_name:hover{
	color: #ffffff;
	text-shadow: 1px 0px 10px #313131, 2px 0px 10px #313131, 3px 0px 10px #313131, 4px 0px 9px #313131, 5px 0px 8px #313131, 6px 0px 7px #313131, 0px 1px 0px #111111;
	overflow: visible;
	z-index: 335;
}
.details_panel_text_name:active{
	color: #999999;
	text-shadow: 0px 1px 0px #000000;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
.details_panel_text_value{
	position: absolute;
	height: 16px;
	margin: 0px;
	padding: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: right;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #999999;
	text-shadow: 0px 1px 0px #111111;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	cursor: pointer;
	z-index: 305;
}
.details_panel_text_value:hover{
	color: #cccccc;
	text-shadow: 1px 0px 10px #313131, 2px 0px 10px #313131, 3px 0px 10px #313131, 4px 0px 9px #313131, 5px 0px 8px #313131, 6px 0px 7px #313131, 0px 1px 0px #111111;
	overflow: visible;
	z-index: 335;
}
.details_panel_text_value:active{
	color: #666666;
	text-shadow: 0px 1px 0px #000000;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
.details_panel_text_description{
	position: absolute;
	height: 16px;
	width: 180px;
	margin: 0px;
	padding: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	color: #cccccc;
	text-shadow: 0px 1px 0px #111111;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	z-index: 305;
}
.details_panel_text_description:hover{
	color: #cccccc;
	text-shadow: 1px 0px 10px #313131, 2px 0px 10px #313131, 3px 0px 10px #313131, 4px 0px 9px #313131, 5px 0px 8px #313131, 6px 0px 7px #313131, 0px 1px 0px #111111;
	overflow: visible;
	z-index: 335;
}

/* MENU A TENDINA PER I PANNELLI */
div.panel_select_menu{
	position: absolute;
	z-index: 320;
	display: none;
}
div.panel_select_menu img{
	position: absolute;
	border: 0px;
	z-index: 321;
	display: inherit;
}
div.panel_select_menu div{
	position: absolute;
	border: 0px;
	z-index: 321;
	display: inherit;
}
.panel_select_menu_item{
	position: absolute;
	height: 16px;
	margin: 0px;
	padding: 0px;
	padding-left: 16px;
	padding-right: 34px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #cccccc;
	text-shadow: 0px 1px 0px #111111;
	cursor: pointer;
	z-index: 322;
	display: inherit;
}
.panel_select_menu_item:hover{
	color: #ffffff;
	text-shadow: 0px 1px 0px #111111;
}
.panel_select_menu_item:active{
	color: #999999;
	text-shadow: 0px 1px 0px #000000;
}
.panel_select_menu_top_left{
	left: -2px;
	top: -2px;
	width: 10px;
	height: 10px;
}
.panel_select_menu_top{
	left: 8px;
	right: 8px;
	top: -2px;
	height: 10px;
}
.panel_select_menu_top_right{
	right: -2px;
	top: -2px;
	width: 10px;
	height: 10px;
}
.panel_select_menu_left{
	left: -2px;
	top: 8px;
	bottom: 8px;
	width: 10px;
}
.panel_select_menu_bg{
	left: 8px;
	right: 8px;
	top: 8px;
	bottom: 8px;
	background-color: #313131;
}
.panel_select_menu_right{
	right: -2px;
	top: 8px;
	bottom: 8px;
	width: 10px;
}
.panel_select_menu_bottom_left{
	left: -2px;
	bottom: -2px;
	width: 10px;
	height: 10px;
}
.panel_select_menu_bottom{
	left: 8px;
	right: 8px;
	bottom: -2px;
	height: 10px;
}
.panel_select_menu_bottom_right{
	right: -2px;
	bottom: -2px;
	width: 10px;
	height: 10px;
}

/* PALLINO STATUS */
.icon_status{
	position: absolute;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 320;
	display: inherit
}
.icon_status_color{
	position: absolute;
	left: 7px;
	top: 7px;
	width: 16px;
	height: 16px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	background-color: #313131;
	z-index: 321;
	display: inherit;
}
.icon_status_symbol{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 322;
	display: inherit;
}
.icon_status_hole{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 323;
	display: inherit;
}
.icon_status_hole_selected{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 324;
	display: none;
}
.icon_status_ack{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 325;
	display: none;
}
.icon_status_ack_over{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 326;
	display: none;
	cursor: <?php print($_pointer); ?>;
}
.icon_status_ack_click{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 327;
	display: none;
	cursor: <?php print($_pointer); ?>;
}
.icon_status_ack_panel_sub_layer{
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	margin: 0px;
	padding: 0px;
	border: 0px;
	background-color: transparent;
	z-index: 327;
	display: none;
}
.icon_status_ack_panel{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 328;
	display: none;
}
.icon_status_ack_panel_hole{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 329;
	display: inherit;
}
.icon_status_ack_panel_left{
	position: absolute;
	left: 0px;
	top: 30px;
	width: 30px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 329;
	display: inherit;
}
.icon_status_ack_panel_center{
	position: absolute;
	left: 30px;
	top: 30px;
	width: 160px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 329;
	display: inherit;
}
.icon_status_ack_panel_right{
	position: absolute;
	left: 190px;
	top: 30px;
	width: 10px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 329;
	display: inherit;
}
.icon_status_ack_add{
	position: absolute;
	left: 0px;
	top: 30px;
	width: 200px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 330;
	display: inherit;
}
.icon_status_ack_view{
	position: absolute;
	left: 0px;
	top: 30px;
	width: 200px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 330;
	display: none;
}
.icon_status_ack_edit{
	position: absolute;
	left: 0px;
	top: 30px;
	width: 200px;
	height: 30px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	z-index: 330;
	display: none;
}
.icon_status_ack_label{
	position: absolute;
	height: 16px;
	width: 180px;
	margin: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #111111;
	text-shadow: 0px 1px 0px #cccccc;
	z-index: 331;
}
.icon_status_ack_input{
	position: absolute;
	height: 14px;
	margin: 0px;
	margin-top: 3px;
	margin-bottom: 3px;
	padding-left: 2px;
	padding-right: 2px;
	padding-top: 5px;
	padding-bottom: 5px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #111111;
	text-shadow: 0px 1px 0px #cccccc;
	background-color: #999999;
	z-index: 331;
}
.icon_status_ack_text{
	position: absolute;
	height: 16px;
	width: 180px;
	margin: 0px;
	padding-left: 0px;
	padding-right: 0px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	color: #111111;
	text-shadow: 0px 1px 0px #cccccc;
	z-index: 331;
}
.icon_status_ack_button{
	position: absolute;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 331;
}
.icon_status_ack_button_icon{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	visibility: inherit;
	z-index: 332;
}
.icon_status_ack_button_over{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 333;
}
.icon_status_ack_button_click{
	position: absolute;
	top: 0px;
	left: 0px;
	width: 30px;
	height: 30px;
	display: none;
	z-index: 334;
}

/* VISTA ARMADIO */
.details_panel_form_rack{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	padding: 0px;
	margin: 0px;
	border: 0px;
	z-index: 398;
	display: none;
}
.rack_sub_layer{
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	margin: 0px;
	padding: 0px;
	border: 0px;
	background-color: transparent;
	z-index: 399;
	display: inherit;
}
.rack{
	position: absolute;
	z-index: 400;
	display: inherit;
}
.rack_border{
	position: absolute;
	padding: 0px;
	margin: 0px;
	border: 0px;
	z-index: 401;
	display: inherit;
}
#rack_top_left{
	left: 0px;
	top: 0px;
	width: 10px;
	height: 10px;
}
#rack_top{
	left: 10px;
	top: 0px;
	height: 10px;
}
#rack_top_right{
	right: 0px;
	top: 0px;
	width: 10px;
	height: 10px;
}
#rack_left{
	left: 0px;
	top: 10px;
	width: 10px;
	height: 180px;
}
.rack_content{
	position: absolute;
	padding: 0px;
	margin: 0px;
	border: 0px;
	left: 10px;
	top: 10px;
	width: 180px;
	height: 180px;
	background-color: #505050;
	z-index: 401;
	display: inherit;
}
.rack_slot{
	position: absolute;
	padding: 0px;
	margin: 0px;
	border: 2px;
	border-color: #202020;
	width: 30px;
	height: 30px;
	background-color: #404040;
	z-index: 402;
	display: inherit;
}
.rack_slot_selectable{
	position: absolute;
	padding: 0px;
	margin: 0px;
	border: 2px;
	border-color: #202020;
	width: 30px;
	height: 30px;
	background-color: #303030;
	z-index: 402;
	display: inherit;
}
.rack_slot_selectable:hover{
	background-color: #606060;
}
.rack_slot_selectable:active{
	background-color: #202020;
}
.rack_slot_selected{
	background-color: #cda400;
}
.rack_slot_icon{
	position: absolute;
	padding: 0px;
	margin: 0px;
	border: 0px;
	width: 30px;
	height: 30px;
	z-index: 403;
	display: inherit;
}
#rack_right{
	right: 0px;
	top: 10px;
	width: 10px;
	height: 180px;
}
#rack_bottom_left{
	left: 0px;
	bottom: 0px;
	width: 10px;
	height: 10px;
}
#rack_bottom{
	left: 10px;
	bottom: 0px;
	height: 10px;
}
#rack_bottom_right{
	right: 0px;
	bottom: 0px;
	width: 10px;
	height: 10px;
}
.rack_slot_number{
	position: absolute;
	height: 20px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	width: 30px;
	height: 10px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 9px;
	font-weight: bold;
	text-shadow: #ffffff 0px 0px 2px;
	color: #000000;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 404;
}

/* BARRA DI NAVIGAZIONE */
.navbar_separator{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1101;
}
.navbar_icon{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1102;
}
.navbar_button_over{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1106;
	display: none;
}
.navbar_button_click{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1106;
	display: none;
}
.navbar_selector_button{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	cursor: pointer;
	z-index: 1108;
}
.navbar_filter_button{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	cursor: pointer;
	z-index: 1108;
}
.navbar_arrow_button{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	cursor: pointer;
	z-index: 1108;
}
.navbar_selector{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 40px;
	z-index: 1104;
	display: none;
}
.navbar_filter_{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 40px;
	z-index: 1101;
}
.navbar_selected{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 40px;
	z-index: 1104;
	display: none;
}
.navbar_menu{
	position: absolute;
	top: 0px;
	z-index: 1105;
	display: none;
}
.navbar_menu_top_left{
	position: absolute;
	left: -2px;
	top: -2px;
	width: 10px;
	height: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_top{
	position: absolute;
	left: 8px;
	top: -2px;
	height: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_top_right{
	position: absolute;
	right: -2px;
	top: -2px;
	width: 10px;
	height: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_left{
	position: absolute;
	left: -2px;
	top: 8px;
	width: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_bg{
	position: absolute;
	left: 8px;
	top: 8px;
	background-color: #b1b1b1;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_right{
	position: absolute;
	right: -2px;
	top: 8px;
	width: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_bottom_left{
	position: absolute;
	left: -2px;
	bottom: -2px;
	width: 10px;
	height: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_bottom{
	position: absolute;
	left: 8px;
	bottom: -2px;
	height: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_bottom_right{
	position: absolute;
	right: -2px;
	bottom: -2px;
	width: 10px;
	height: 10px;
	z-index: 1106;
	display: inherit;
}
.navbar_menu_separator{
	position: absolute;
	left: 0px;
	height: 1px;
	z-index: 1107;
	display: inherit;
}
.navbar_menu_check{
	position: absolute;
	right: 0px;
	width: 30px;
	height: 30px;
	z-index: 1107;
}
.navbar_menu_item{
	position: absolute;
	height: 16px;
	margin: 0px;
	padding: 0px;
	padding-left: 16px;
	padding-right: 34px;
	padding-top: 7px;
	padding-bottom: 7px;
	border: 0px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	text-shadow: #333333 0px 1px 0px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	cursor: pointer;
	z-index: 1107;
	display: inherit;
}
.navbar_menu_item:hover{
	text-shadow: #444444 0px 1px 0px;
	color: #ffffff;
}
.navbar_menu_item:active{
	text-shadow: #eeeeee 0px 1px 0px;
	color: #333333;
}
.pill_color{
	position: absolute;
	top: 5px;
	width: 30px;
	height: 20px;
	z-index: 1100;
}
.navbar_text{
	position: absolute;
	top: 6px;
	margin: 0px;
	padding: 0px;
	border: 0px;
	width: 140px;
	height: 16px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 14px;
	text-shadow: #333333 0px 1px 0px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	cursor: pointer;
	z-index: 1107;
}
.navbar_text:hover{
	text-shadow: #444444 0px 1px 0px;
	color: #ffffff;
}
.navbar_text:active{
	text-shadow: #eeeeee 0px 1px 0px;
	color: #333333;
}

/* BALLOON HINT */
.balloon{
	position: absolute;
	width: 120px;
	height: 30px;
	background-color: transparent;
	z-index: 1200;
	display: none;
}
.balloon_left{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 10px;
	height: 30px;
	z-index: 1201;
	display: inherit;
}
.balloon_center{
	position: absolute;
	left: 10px;
	top: 0px;
	width: 100px;
	height: 30px;
	z-index: 1201;
	display: inherit;
}
.balloon_right{
	position: absolute;
	right: 0px;
	top: 0px;
	width: 10px;
	height: 30px;
	z-index: 1201;
	display: inherit;
}
.balloon_arrow{
	position: absolute;
	top: 0px;
	width: 10px;
	height: 30px;
	z-index: 1202;
	display: inherit;
}
.balloon_text{
	position: absolute;
	left: 0px;
	top: 0px;
	width: 110px;
	height: 15px;
	margin: 0px;
	padding: 0px;
	padding-left: 5px;
	padding-right: 5px;
	padding-top: 5px;
	padding-bottom: 10px;
	border: 0px;
	text-align: center;
	font-style: normal;
	font-weight: normal;
	font-size: 12px;
	text-decoration: none;
	color: #ffffff;
	text-shadow: 0px 1px 0px #111111;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 1203;
	display: inherit;
}

/* BOTTONI BARRA SUPERIORE */
.topbar_button{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1001;
}
.topbar_button_image{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1002;
	display: inherit;
}
.topbar_button_image_light{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1002;
	display: none;
}
.topbar_button_over{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1003;
	display: none;
}
.topbar_button_click{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1004;
	display: none;
}
.topbar_button_separators{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	cursor: pointer;
	z-index: 1005;
	display: inherit;
}
.topbar_icon{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1001;
}
.topbar_icon_image{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1002;
	display: inherit;
}
.topbar_text{
	position: absolute;
	top: 0px;
	height: 30px;
	z-index: 1001;
}
.topbar_text_par{
	position: absolute;
	top: 6px;
	margin: 0px;
	padding: 0px;
	padding-left: 5px;
	padding-bottom: 5px;
	border: 0px;
	height: 16px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 14px;
	text-shadow: 0px 1px 0px #333333;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	cursor: pointer;
	z-index: 1006;
}
.topbar_text_par:hover{
	text-shadow: 0px 1px 0px #444444;
	color: #ffffff;
}
.topbar_text_par:active{
	text-shadow: 0px 1px 0px #eeeeee;
	color: #333333;
}
/* BOTTONI BARRA INFERIORE */
.bottombar_button{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1001;
}
.bottombar_simple_button{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1001;
}
.bottombar_button_image{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1002;
	display: inherit;
}
.bottombar_button_image_light{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1002;
	display: none;
}
.bottombar_button_over{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1003;
	display: none;
}
.bottombar_button_click{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1004;
	display: none;
}
.bottombar_button_separators{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	cursor: pointer;
	z-index: 1005;
	display: inherit;
}
.bottombar_icon{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1001;
}
.bottombar_icon_image{
	position: absolute;
	top: 0px;
	width: 30px;
	height: 30px;
	z-index: 1002;
	display: inherit;
}
.bottombar_text{
	position: absolute;
	top: 0px;
	height: 30px;
	z-index: 1001;
}
.bottombar_text_par{
	position: absolute;
	top: 6px;
	margin: 0px;
	padding: 0px;
	padding-left: 5px;
	padding-bottom: 5px;
	border: 0px;
	height: 16px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 14px;
	text-shadow: 0px 1px 0px #333333;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	z-index: 1006;
}
.bottombar_wheel{
	position: absolute;
	top: 2px;
	left: 102px;
	width: 25px;
	height: 25px;
	z-index: 1100;
	display: none;
}
.bottombar_log{
	position: absolute;
	height: 16px;
	margin: 0px;
	padding: 0px;
	padding-left: 16px;
	padding-right: 34px;
	padding-top: 3px;
	padding-bottom: 3px;
	border: 0px;
	text-align: left;
	font-style: normal;
	font-weight: normal;
	font-size: 9px;
	text-shadow: #333333 0px 1px 0px;
	color: #eeeeee;
	font-family: Tahoma, "Lucida Grande", Verdana, Arial, sans-serif;
	cursor: pointer;
	z-index: 1101;
	display: none;
}
.ui-autocomplete {
	max-height: 300px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
	/* add padding to account for vertical scrollbar */
	padding-right: 20px;
}
<?php
/**
* Telefin STLC1000 Consolle
*
* managerConfiguration.php - Script per l'aggiornamento di un file xml da linea di comando
*
* @author Davide Ferraretto
* @version 1.0.4.3 25/02/2016
* @copyright 2011-2016 Telefin S.p.A.
*/

$is_cliScript = true;
$is_new_conf = false;
$_default_building_note = "";

// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo configurazione
require_once("conf/sk_config.php");

// Includo il file per fare il parsing del file xml passato da input
require_once("version.php");
require_once("lib/lib_xml.php");
require_once("lib/lib_code.php");
require_once("lib/lib_soap.php");
require_once("lib/lib_log.php");
require_once("lib/lib_var.php");
require_once("modules/sk_core.php");
require_once("modules/sk_class_node.php");
require_once("modules/sk_app_stlcManagerService.php");


// nome dei vari parametri d'ingresso
// help
$HELP_PARAMETER = '/help';
// nome parametro per upload di un file xml
$UPLOAD_CONF_PARAMETER = '/upload_conf';
// nome parametro per generare una nuova configurazione dato l codice localit�
$NEW_CONF_PARAMETER = '/new_conf';

// funzione per recuperare le informazioni principali da STLCManagerService tramite chiamata SOAP
// recupera le informazioni e le inserisce nella configurazione passata da input
function getInfoBySTLCManager( &$conf ){
	if( isset($conf) === true && $conf !== null && isAliveStlcManagerService() === true){
		$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
	}
	
	if (isset($_client)){
		// Preparo i parametri per la richiesta SOAP
		$_request->clientType = 'SKC';
		$_params->request = $_request;
	
		// Chiamo il metodo SOAP con i parametri che ho generato
		$_result = $_client->getSTLCInfo($_params);
	
		if (isset($_result))
		{
			// Recupero i dati del risultato
			$_result_data = $_result->getSTLCInfoResult;
		
			if (isset($_result_data))
			{
				if ($_result_data->status->level == "SUCCESS")
				{
					$_info_data = $_result_data->data;
					
					// Converto SerialNumber STLC1000 e salvo come SrvID
					$_srvid = serialNumberToServerId($_info_data->serialNumber);
					
					$_info = $conf["info"];
					
					// Salvo SerialNumebr e Versione STLC1000
					$_info["sk_sn"]	= $_info_data->serialNumber;
					$_info["sk_version"] = $_info_data->version;
					$_info["sk_hostname"] = $_info_data->hostName;
					$_info['sk_id'] = $_srvid;
					
					$conf["info"] = $_info;
				}
			}
		}
	}
}

// inizializzazione di alcune variabili globali
function init_global_variables(){
	global $_conf_apps;
	$_conf_app0 = $_conf_apps[0];
	
	global $_station_type_list;
	global $_device_type_list;
	global $_port_type_list;
	global $_building_type_list;
	global $_rack_type_list;
	
	$_station_type_list 	= coreLoadXMLData($_conf_app0["name"],"LoadXMLStationTypeList",$_conf_app0["station_type_list_path"]);
	$_device_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLDeviceTypeList",$_conf_app0["device_type_list_path"]);
	$_port_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLPortTypeList",$_conf_app0["port_type_list_path"]);
	$_building_type_list 	= coreLoadXMLData($_conf_app0["name"],"LoadXMLBuildingTypeList",$_conf_app0["building_type_list_path"]);
	$_rack_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLRackTypeList",$_conf_app0["rack_type_list_path"]);
}

function upload_configuration($_conf){
	global $_conf_apps;
	global $is_new_conf;
	global $_configuration;
	$_configuration = $_conf;
	$is_new_conf = false;
	$_return = false;
	// === FASE 1 = Salvataggio system.xml ===
	$_conf_app0 = $_conf_apps[0];
	$now   = new DateTime();
	$_timestamp = $now->format( 'YmdHis' );
	$_config_url = $_conf_app0["config_url"];
	$_config_prev_url = str_replace(".xml", "", $_config_url)."_prev.xml";
	$_config_back_url = str_replace(".xml", "", $_config_url)."_".$_timestamp.".xml";
	
	// recupero informazioni tramite chiamata SOAP
	getInfoBySTLCManager($_configuration);
	
	// cablaggio valori
	coreFixConfig($_configuration);
	
	// Salvo prev
	print("Copia di $_config_url come $_config_prev_url\n");
	copy($_config_url,$_config_prev_url);
	// Salvo nuovo
	print("Salvataggio nuova configurazione\n");
	$_return = spvstandardSaveXMLConfig($_conf_app0["config_url"],$_configuration);
	// Salvo backup
	print("Copia di $_config_url come $_config_back_url\n");
	copy($_config_url,$_config_back_url);
	
	return ($_return !== null && $_return !== false);
}

/* dato il path di file xml aggiorna il file system.xml di sistema*/
function upload_new_xml($xml_file){
	global $_conf_apps;
	global $_upload_request;
	
	$_upload_request = true;
	
	$res = false;
	
	init_global_variables();
	
	print("Parsing: " . $xml_file."\n");
	
	$conf = coreLoadXMLConfig($_conf_apps[0]['name'], $xml_file);
	if( $conf === false ){
		print("Parsing fallito\n");
	} else {
		print("Recuperata configurazione dal file: " . $xml_file."\n");
		// verifico se torna true, perch� potrebbe tornare tipi differenti
		if(upload_configuration($conf) === true){
			$res = true;
		}
	}
	
	return $res;
}

// torna il primo tipo di fabbricato disponibile
// null altrimenti
function get_first_building_type($building_type_list){
	$ret = null;
	
	if($building_type_list !== null && isset($building_type_list[0])){
		$ret = $building_type_list[0];
	}
	
	return $ret;
}

// cerca un fabbricato dato un codice
function search_building_type_by_code($building_type_list, $code){
	$ret = null;
	
	if($building_type_list !== null){
		for($i = 0; $i < count($building_type_list); $i++){
			$build_type = $building_type_list[$i];
			if($build_type->code == $code){
				$ret = $build_type;
				break;
			}
		}
	}
	
	return $ret;
}

// cerca la region list e ritorna la relativa configurazione d'oggetti
function search_node_by_location_code($region_list, $id_nodo){
	$ret = null;
	$id_nodo = intval ($id_nodo);
	
	// per ogni region
	for($i = 0; $i < count($region_list) && $ret === null; $i++){
		$_zone_list = $region_list[$i]['zone_list'];
		// per ogni lista zona
		for($j = 0; $j < count($_zone_list) && $ret === null; $j++){
			// lista nodi
			$_node_list = $_zone_list[$j]['node_list'];
			for($k = 0; $k < count($_node_list) && $ret === null; $k++){
				// nodo
				$_node = $_node_list[$k]['data'];
				if($_node->id == $id_nodo){
					$ret = $_node;
					$ret = Array();
					$ret['node'] = $_node;
					$ret['zone'] = $_zone_list[$j]['data'];
					$ret['region'] = $region_list[$i]['data'];;
					break;
				}
			}
		}
	}
	
	return $ret;
}

// dato un odice localit� genera un nuovo file xml
function new_conf_xml($location_code,$offset = 0){
	global $_conf_apps;
	global $_conf_default_building_code;
	global $_conf_default_building_note;
	global $_conf_console_app_auto_add_tcpip;
	global $is_new_conf;
	$is_new_conf = true;
	
	init_global_variables();
	
	$conf_app = $_conf_apps[0];
	$res = false;
	
	
	print("Caricamento region list\n");
	$_region_list = coreLoadXMLData($conf_app["name"],"LoadXMLRegionList",$conf_app["region_list_path"]);
	if( $_region_list === null || $_region_list === false ){
		print("Errore nell'ottenere la region list\n");
	} else {
		// ricerca location code
		print("Ricerca location code " . $location_code . "\n");
		$location = search_node_by_location_code($_region_list, $location_code);
		
		if($location !== null){
			// genero la configurazione
			
			// creazione regione
			$_region = new region();
			$_region->id = $location['region']->id;
			$_region->name = $location['region']->name;
			$_region->type = $location['region']->type;
			
			// creazione zona
			$_zone = new zone();
			$_zone->id = $location['zone']->id;
			$_zone->name = $location['zone']->name;
			$_zone->type = $location['zone']->type;
			$_zone->regionId = $_region->id;
			
			// creazione nodo
			$_node = new node();
			$_node->id = $location['node']->id;
			$_node->name = $location['node']->name;
			$_node->type = $location['node']->type;
			$_node->devIdOffset = $offset;
			$_node->zoneId = $_zone->id;
			$_node->regionId = $_region->id;
			
			// creazione stazione
			$_station = new station();
			$_station->name = $_node->name;
			$_station->id = 0;
			$_station->nodeId = $_node->id;
			$_station->name = $_node->name;
			$_station->type = 'station';
			$_node->stationId = $_station->id;
			
			// creazione fabbricato
			$_building = new building();
			$_building->id = 0;
			$_building->station_id = $_station->id;
			$build_type = null;
			$_building_type_list = coreLoadXMLData($conf_app["name"],"LoadXMLBuildingTypeList",$conf_app["building_type_list_path"]);
			if($_building_type_list === null || $_building_type_list === false){
				print("Errore nel recuperare la lista di building_types");
				return false;
			}
			
			if(isset($_conf_default_building_code) && $_conf_default_building_code !== null){
				print("Recupero building type da codice " . $_conf_default_building_code . "\n");
				$build_type = search_building_type_by_code($_building_type_list, $_conf_default_building_code);
				if($build_type === null){
					print("Errore nel recuperare il building_type dal code " . $_conf_default_building_code . "\n");
				}
			}
			
			if($build_type == null){
				print("Recupero primo building_type in lista\n");
				$build_type = get_first_building_type($_building_type_list);
				if($build_type === null){
					print("Errore nel il primo building_type in lista\n");
					return false;
				}
			}
			
			$_building->type = $build_type->code;
			$_building->name = $build_type->name;
			
			// setto la nota per il fabbricato
			if(isset($_conf_default_building_note) && $_conf_default_building_note !== null){
				$_building->note = $_conf_default_building_note;
			} else {
				$_building->note = $_default_building_note;
			}
			
			$configuration = coreLoadXMLConfig($conf_app['name'], "");
			$configuration["regions"][] = $_region;
			$configuration["zones"][] = $_zone;
			$configuration["nodes"][] = $_node;
			$configuration["stations"][] = $_station;
			$configuration["buildings"][] = $_building;
			coreFixConfig($configuration);
			
			$res = upload_configuration($configuration);
		} else {
			print("Location code " . $location_code . " non trovato trovato\n");
		}
	}
	
	
	return $res;
}

// torna una stringa che mostra come utilizzare lo script
function usage(){
	global $argv;
	global $HELP_PARAMETER;
	global$UPLOAD_CONF_PARAMETER;
	global$NEW_CONF_PARAMETER;
	
	$str = "$argv[0] [$HELP_PARAMETER | $UPLOAD_CONF_PARAMETER \"xml file path\" | $NEW_CONF_PARAMETER \"location code\"]\n";
	return $str;
}

// torna il messaggio relativo ad un uno scorretto dello script
function error_usage(){
	return("Error usage!!!\n".usage());
}

// funzione d'uscita software
function script_exit($res = true){
	if($res === 0 || $res === true){
		print("Esecuzione terminata con successo\n");
		exit(0);
	}
	
	if($res === false){
		$res = -1;
	} 
	
	print("Uscita con errore!!!\n");
	exit($res);
}

// controllo parametri da linea di comando
$argv_size = count($argv);
$res = true;

if($argv_size >= 2){
	if( $argv[1] == $HELP_PARAMETER ){
		if($argv_size == 2){
			print(usage());
		} else {
			print(error_usage());
			$res = false;
		}
	} else if($argv[1] == $UPLOAD_CONF_PARAMETER ) {
		if($argv_size == 3){
			$res = upload_new_xml($argv[2]);
		} else {
			print(error_usage());
			$res = false;
		}
	} else if($argv[1] == $NEW_CONF_PARAMETER ) {
		if($argv_size == 3){
			$res = new_conf_xml($argv[2]);
		} else if($argv_size == 4){
			$res = new_conf_xml($argv[2], $argv[3]); 
		} else {
			print(error_usage());
			$res = false;
		}
	} else {
		print(error_usage());
		$res = false;
	}
} else {
	print(error_usage());
	$res = false;
}

// uscita 
script_exit($res);

?>
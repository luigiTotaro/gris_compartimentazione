<?php
/**
* Telefin STLC1000 Consolle
*
* lib_auth.php - Libreria per la gestione dell'autenticazione.
*
* @author Enrico Alborali
* @version 1.0.2.0 25/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

$_auth_realm = $_conf_console_app_name;
$_auth_users = array();

/**
* Funzione per aggiungere un utente all'elenco degli utenti autorizzati
*/
function authAddUser($name,$password,$profile)
{
	global $_auth_users;
	
	$_user = array();
	
	$_user['username'] = $name;
	$_user['password'] = $password;
	$_user['profile'] = $profile;
	
	$_auth_users[$name] = $_user;
	
	return($_user);
}

/**
* Funzione per generare l'header per la richiesta di autenticazione
*/
function authHeader($mode="digest")
{
	global $_auth_realm;
	
	$_html = codeInit();
	
	if ($mode == "digest")
	{
		if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
		header('HTTP/1.1 401 Unauthorized');
		header('WWW-Authenticate: Digest realm="'.$_auth_realm.
           '",qop="auth,auth-int",nonce="'.uniqid().'",opaque="'.md5($_auth_realm).'"');
        die(langGetString("msg","sk_auth_unauthorized"));
		}
	}
	else
	{
		if (empty($_SERVER['PHP_AUTH_USER'])) {
		header('WWW-Authenticate: Basic realm="'.$_auth_realm.'"');
    	header('HTTP/1.0 401 Unauthorized');
		die(langGetString("msg","sk_auth_unauthorized"));
		}
	}
}

/**
* function to parse the http auth header
*/
function authHttpDigestParse($txt)
{
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}

/**
* Funzione per verificare l'autenticazione utente
*/
function authCheckUser($mode="digest")
{
	global $_auth_users, $_auth_realm;
	
	$_user = false;
	
	if ($mode== "basic")
	{
		$_user = $_auth_users[strtolower($_SERVER['PHP_AUTH_USER'])];
		
		if (isset($_user))
		{
			if ($_user['password'] == $_SERVER['PHP_AUTH_PW'])
				return($_user);
			else
				return false;
		}
		else
		{
			return false;
		}
	}

	// analyze the PHP_AUTH_DIGEST variable
	if (!($data = authHttpDigestParse($_SERVER['PHP_AUTH_DIGEST'])) ||
    !isset($_auth_users[$data['username']]))
    {
    	unset($_SERVER['PHP_AUTH_DIGEST']);
    	return false;
	}

	$_user = $_auth_users[strtolower($data['username'])];
	
	// generate the valid response
	$A1 = md5(strtolower($_user['username']). ':' . $_auth_realm . ':' . $_user['password']);
	$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
	$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

	if ($data['response'] != $valid_response)
	{
		unset($_SERVER['PHP_AUTH_DIGEST']);
		return false;
	}
	   
	// ok, valid username & password
	return($_user);
}

/**
* Funzione per verificare l'autenticazione utente da GRIS
*/
function authCheckGRISUser(&$grisUser)
{
	global $_auth_users;
	
	$_user = false;

	$_ga		= $_REQUEST["ga"];
	
	if (isset($_ga))
	{
		// 128-bit (16 bytes) IV 
		$_iv =  '0122081005393486';
		// 256-bit (32 bytes) key 
		$_key = "56302830723461730820862519915943";
	
		// Apertura mcrypt
		$_cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
		$_iv_size = mcrypt_enc_get_iv_size($_cipher);
		// Inizializzazione mcrypt
		if (mcrypt_generic_init($_cipher, $_key, $_iv) != -1)
		{
			$_c = pack("H*" , $_ga);
			$_text = mdecrypt_generic($_cipher, $_c);
			$_token = explode(":",$_text);
			
			$_username = $_token[0];
			$_password_md5 = $_token[1];
			$_ip = $_token[2];
			$_gris_username = str_replace(" ", "", (string)$_token[3]);
			$_gris_username = str_replace(chr(0), "", (string)$_token[3]);
			
			if (isset($_auth_users[strtolower($_username)]))
			{
				$_user = $_auth_users[strtolower($_username)];
				
				if ($_ip == $_SERVER['SERVER_ADDR'])
				{
					if ($_password_md5 == md5($_user['password']))
					{
						//print("<!-- GRIS username: ".$_gris_username." -->");
						$grisUser = $_gris_username;
					}
					else
					{
						$_user = false;
					}
				}
				else
				{
					$_user = false;
				}
			}
			else
			{
				$_user = false;
			}
			
			// Deinizializzazione mcrypt
	 		mcrypt_generic_deinit($_cipher);
			
		}
		// Chiusura mcrypt
		mcrypt_module_close($_cipher);	
	}
	
	return($_user);
}

/**
* Funzione per verificare la corrispondenza del profilo utente
*/
function authCheckProfile($profile)
{
	$_verified = false;

	if ($profile != null)
	{
		$_user = authGetUser();
		$_user_profile = $_user["profile"];
		
		$_profiles = explode(",", $profile);
		
		foreach ($_profiles as $_profile)
		{
			if ($_user_profile == $_profile)
			{
				// Profilo verificato
				$_verified = true;
				break;
			}
			//print("<!-- user: ".serialize($_user)." user_profile: ".$_user_profile." profile: ".$_profile." -->");
		}
	}
	else
	{
		// Profilo non specificato quindi lo considero implicitamente verificato
		$_verified = true;
	}
	
	return($_verified);
}

/**
*
*/
function authSetUser($user,$grisUser=null)
{
	session_start();
	
	if ($grisUser !== null)
	{
		// Salvo l'username dell'utente GRIS
		$user['gris_username'] = str_replace(" ", "", $grisUser);
	}
	// Salvo l'IP dell'utente
	$user['ip'] = $_SERVER['REMOTE_ADDR'];
	
	$_SESSION['user'] = serialize($user);
	
	session_write_close();
	
	return(true);
}

/**
*
*/
function authGetUser()
{
	session_start();
	
	$_user = unserialize($_SESSION['user']);
	
	session_write_close();
	
	return($_user);
}

/**
* Funzione 
*/
function authLogout()
{
	session_start();
	
	unset($_SERVER['PHP_AUTH_DIGEST']);
	unset($_SERVER['PHP_AUTH_USER']);
	unset($_SESSION['user']);
	
	session_write_close();
}

/**
*
*/
function authUpdateActivity()
{
	session_start();
	
	$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
	
	session_write_close();
}

/**
*
*/
function authCheckActivity()
{
	global $_conf_console_session_activity_timeout;
	global $_conf_console_session_activity_timeout_admin_multiplier;
	
	if (isset($_SESSION['LAST_ACTIVITY']))
	{
		if (!isset($_conf_console_session_activity_timeout_admin_multiplier))
			$_conf_console_session_activity_timeout_admin_multiplier = 10;
		$_last_activity = time() - $_SESSION['LAST_ACTIVITY'];
		if (authCheckProfile('admin'))
			$_conf_console_session_activity_timeout *= $_conf_console_session_activity_timeout_admin_multiplier;
		if ($_last_activity > $_conf_console_session_activity_timeout)
		{
	    	session_start();

			$_SESSION['configuration'] = null;
			unset($_SESSION['configuration']);
			$_SESSION['page'] = "devices";
			$_SESSION['zoom'] = 4;
	
			$_SERVER['PHP_AUTH_DIGEST'] = "none";
			//unset($_SERVER['PHP_AUTH_DIGEST']);
			/*
			$_SERVER['PHP_AUTH_USER'] = null;
			unset($_SERVER['PHP_AUTH_USER']);
			$_SESSION['user'] = null;
			unset($_SESSION['user']);
			*/
			
			// Chiudo la sessione
			session_write_close();
			
			return(0);
		}
		else
		{
			return($_conf_console_session_activity_timeout-$_last_activity);
		}
		
	}
	else
		return(0);
}

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* lib_db.php - Libreria per la gestione accesso al DB.
*
* @author Enrico Alborali
* @version 0.0.1 14/07/2011
* @copyright 2011 Telefin S.p.A.
*/

/**
* Funzione per la formattazione degli errori riguardanti il DB
*/
function dbFormatErrors( $errors )
{
	$_text = codeInit();
	
	$_text .= "DB error information";
	foreach ( $errors as $error )
	{
		$_text .= codeChar(1)."SQLSTATE: ".$error['SQLSTATE'];
		$_text .= codeChar(1)."Code: ".$error['code'];
		$_text .= codeChar(1)."Message: ".$error['message'];
	}
	
	return($_text);
}

/**
* Funzione per connettersi al DB
*/
function dbConnect()
{
	global $_conf_console_db_server;
	global $_conf_console_db_name;
	
	$_options = array("Database"=>$_conf_console_db_name,"ReturnDatesAsStrings" => false);
	$_conn = sqlsrv_connect( $_conf_console_db_server, $_options);
	
	return($_conn);
}

/**
* Funzione per chiudere la connessione al DB
*/
function dbClose($_conn)
{
	sqlsrv_close($_conn);
}

/**
* Funzione per eseguire una query SQL sul DB
*/
function dbQuery($conn,$sql)
{
	$_params = array();
	$_options = array("Scrollable" => SQLSRV_CURSOR_KEYSET);

	$_result = sqlsrv_query($conn, $sql, $_params, $_options);

	return($_result);
}

/**
* Funzione per estrarre il risultato di una query
*/
function dbExtractResult($result)
{
	if ($result === false)
	{
		$_data = null;
	}
	else
	{
		$_data = array();
		
		if (sqlsrv_has_rows($result))
		{
			$_count = sqlsrv_num_rows($result);
			
			if ($_count > 0)
			while($row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC))
			{
				$_data[] = $row;
			}
		}
	}
	
	return($_data);
}



?>
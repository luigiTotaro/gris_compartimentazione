<?php
/**
* Telefin STLC1000 Consolle
*
* lib_ajax.php - Libreria per la generazione del codice AJAX.
*
* @author Enrico Alborali
* @version 1.0.2.0 27/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Funzione per generare l'url in base al nome di un worker AJAX
*/
function ajaxGetUrl($name,$dataType = "xml")
{
	global $_conf_console_ajax_base_url;

	$_url = $_conf_console_ajax_base_url.$dataType."/sk_".strtolower($name).".".$dataType.".php";
	
	return $_url;
}

/**
* Classe per costruire il codice Javascript per funzionalità AJAX
*/
class ajaxWorker
{
	public $name;
	
	private $repeat;
	private $repeat_timeout;
	private $receiver_code;
	private $sender_condition;
	
	/**
	* Metodo costruttore
	*/
	function __construct($name,$repeat=false,$repeat_timeout=1000,$receiver_code="",$sender_condition=null)
	{
		$this->name = $name;
		$this->repeat = $repeat;
		$this->repeat_timeout = $repeat_timeout;
		$this->receiver_code = codeInit().$receiver_code;
		$this->sender_condition = $sender_condition;
	}

	/**
	* Metodo per generare il codice JavaScript per gestire le funzionalità AJAX del worker
	*/
	function BuildJavaScript($dataType = "xml",$data = "",$waiting=false)
	{
		global $_conf_console_ajax_cross_domain_mode;
				
		$_code = codeInit();
		
		$_code .= codeChr(1,0)."var ".$this->name."Send = false;";
		$_code .= codeChr(1,0)."var ".$this->name."SendForced = false;";
		
		$_code .= codeFunctionComment("Funzione per gestire le funzioni di invio AJAX per ".$this->name,0);
		
		// Controllo se devo inserire il codice per la ripetizione continua della chiamata
		if ($this->repeat)
			$_code_repeat = codeChr(1,1)."if (norepeat==false) setTimeout('ajax".$this->name."Sender()',".$this->repeat_timeout.");";
		else
			$_code_repeat = "";
		// COntrollo se devo usare il codice in modalita' cross-domain
		if ($_conf_console_ajax_cross_domain_mode)
		{
			$_ajax_url = $dataType."/sk_proxy.".$dataType.".php";
			$_ajax_data	= "'"."name=".$this->name."".(($data=="")?"'":"&'+")."".$data;
			$_ajax_null = "'"."name=".$this->name."'";
		}
		else
		{
			$_ajax_url = ajaxGetUrl($this->name,$dataType);
			$_ajax_data = $data;
			$_ajax_null = "''";
		}
		
		if ($data === "")
		{
			$_ja_data = codeChr(1,1)."sendData = ".$_ajax_null.";";
		} 
		else
		{
			$_ja_data = codeChr(1,1)."if (".$data." != null) sendData = ".$_ajax_data.";"
					.codeChr(1,1)."else sendData = ".$_ajax_null.";";
		}
		
		if (isset($this->sender_condition))
		{
			$_code_condition = " && (".$this->sender_condition.")";
		}
		else
		{
			$_code_condition = "";
		}
		
		if ($waiting === true)
		{
			$_js_waiting_start = codeChr(1,1)."if (nowaiting!=true) waitingStart();";
			$_js_waiting_stop = codeChr(1,1)."waitingStop();";
		}
		else
		{
			$_js_waiting_start = "";
			$_js_waiting_stop = "";
		}
							
		$_code .= codeChr(1,0)."function ajax".$this->name."Sender(norepeat,forcesend,nowaiting) {"
					.$_js_waiting_start
					.codeChr(1,1)."if (norepeat == null) norepeat = false;"
					.codeChr(1,1)."var sendData;"
					.$_ja_data
					.codeChr(1,1)."if (norepeat == true) sendData += (sendData=='')?'manual=true':'&manual=true';"
					// Verifico che non sia in corso una richiesta
					.codeChr(1,1)."if (forcesend==true || (".$this->name."Send==false".$_code_condition.")){"
						.codeChr(1,2)."sendnow = true;"
						.codeChr(1,2).$this->name."Send = true;"
						.codeChr(1,2)."if (forcesend==true) ".$this->name."SendForced = true;"
						.codeChr(1,2)."else if (".$this->name."SendForced==true) sendnow = false;"
						.codeChr(1,2)."if (sendnow==true) bottombar_wheelStart();"
						.codeChr(1,2)."if (sendnow==true) $.ajax({"
							.codeChr(1,3)."type: 'GET',"
							.codeChr(1,3)."url: '".$_ajax_url."',"
							.codeChr(1,3)."dataType: '".$dataType."',"
							//.codeChr(1,2)."data: '".$_ajax_data."',"
							.codeChr(1,3)."data: sendData,"
							.codeChr(1,3)."success: ajax".$this->name."Receiver,"
							.codeChr(1,3)."timeout: 60000,"
							.codeChr(1,3)."error: ajax".$this->name."Error"
						.codeChr(1,2)."});"
					.codeChr(1,1)."}"
					.$_code_repeat
				.codeChr(1,0)."}";
				
		$_code .= codeFunctionComment("Funzione per gestire le funzioni di ricezione AJAX per ".$this->name,0);
		
		$_code .= codeChr(1,0)."function ajax".$this->name."Receiver(".$dataType.") {"
					.$_js_waiting_stop
					.codeChr(1,1).$this->receiver_code
					.codeChr(1,1).$this->name."Send = false;"
					.codeChr(1,2).$this->name."SendForced = false;"
					.codeChr(1,1)."bottombar_wheelStop();"
					.codeChr(1,1).'$("#bottombar_button_sk_image_light").hide();'
				.codeChr(1,0)."}";
			
		$_code .= codeFunctionComment("Funzione per gestire le funzioni di errore AJAX per ".$this->name,0);
				
		$_code .= codeChr(1,0)."function ajax".$this->name."Error(".$dataType.") {"
					.codeChr(1,1).$this->name."Send = false;"
					.codeChr(1,1)."bottombar_wheelStop();"
					.$_js_waiting_stop
					.codeChr(1,1).'$("#bottombar_button_sk_image_light").show();'
				.codeChr(1,0)."}";
		
		return($_code);
	}
}




?>
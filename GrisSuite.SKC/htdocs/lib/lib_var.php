<?php
/**
* Telefin STLC1000 Consolle
*
* lib_xml.php - Libreria per la generazione del codice XML.
*
* @author Enrico Alborali
* @version 1.0.0.5 18/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
     * Returns true if the call came from jQuery or prototype.
     * @return        boolean
     */
function varIsAjax()
{
	return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
}

/**
* Funzione per recuperare una variabile passata come parametro GET o POST alla pagina
*/
function varGetRequest($name)
{
	$_request = null;
	
	if (isset($_REQUEST[$name]))
	{
		$_request = $_REQUEST[$name];
		
		// Converto gli apici e i doppi apici in apostrofo
		$_request = str_replace("'", chr(96), $_request);
		$_request = str_replace('"', chr(96), $_request);
	}
		
	return($_request);
}
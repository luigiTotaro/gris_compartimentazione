<?php
/**
* Telefin STLC1000 Consolle
*
* lib_soap.php - Libreria per la comunicazione SOAP.
*
* @author Enrico Alborali
* @version 1.0.4.0 24/10/2013
* @copyright 2011-2013 Telefin S.p.A.
*/

/**
* Funzione per connettersi come client ad un servizio soap
*/
function soapConnect($_service,$server="localhost",$_port="8731",$_trace=1,$_exceptions=true)
{
	if ($server === null)
		$_server = "172.16.1.101";
	else
		$_server = $server;
		
	$_url = 'http://'.$_server.':'.$_port.'/'.$_service.'/?wsdl';
	//$_url = "http://172.16.1.101:8731/STLCManager.Service.ClientComm/ClientCommandService/?wsdl";
	
	try{
		$_client = new SoapClient($_url,
			array(
				'soap_version'=>SOAP_1_1,
				'cache_wsdl'=>WSDL_CACHE_NONE,
				'trace' => $_trace,
				'exceptions' => $_exceptions));
	}
	catch(Exception $e){
		logEvent("Catturata eccezione in soapConnect: ".$e->getMessage().".",2);
	}
	
	return($_client);
}

?>
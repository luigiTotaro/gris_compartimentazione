<?php
/**
* Telefin STLC1000 Consolle
*
* lib_code.php - Libreria di utilita' per la generazione di codice.
*
* @author Enrico Alborali
* @version 1.0.0.6 20/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Funzione per inizializzare una stringa per codice generico
*/
function codeInit()
{
	$_code = "";
	
	return($_code);
}

/**
* Funzione per generare i caratteri speciali
*/
function codeChr($chr13,$chr9=0,$format='html')
{
	global $_conf_console_code_indent;
	global $_conf_console_code_scramble;
	
	$_code = '';
	
	if ($_conf_console_code_indent)
	{
		for ($i = 0; $i < $chr13; $i++) 
		{
    		$_code .= chr(13).chr(10);
		}
		for ($i = 0; $i < $chr9; $i++) 
		{
    		$_code .= chr(9);
		}
	}
	if ($_conf_console_code_scramble)
	{
		if ($format === 'html')
		{
			$_code .= '<!--';
			$_l = rand(1,256);
			for ($i = 0; $i < $_l; $i++) 
			{
				$_c = rand(ord('0'),ord('9'));
    			$_code .= chr($_c);
    			$_c = rand(ord('0'),ord('9'));
    			$_code .= chr($_c);
    			$_c = rand(ord('A'),ord('Z'));
    			$_code .= chr($_c);
    			$_c = rand(ord('a'),ord('z'));
    			$_code .= chr($_c);
    			$_c = rand(ord(' '),ord('z'));
    			$_code .= chr($_c);
			}
			$_code .= '-->';
		}
	}
		
	return($_code);
}

/**
* Funzione per gestire il caso in cui una stringa va in overflow
*/
function codeStrOverflow($string,$start,$stop,$append="")
{
	if(strlen($string) > $stop)
	{
		$_newstring = substr("".$string."",$start,$stop).$append;
	}
	else
	{
		$_newstring = $string;
	}
	return $_newstring; 
}

/**
* Funzione per generare il testo di prova Lorem Ipsum
*/
function codeLoremIpsum($repeat=1)
{
	$_code = codeInit();
	
	for ($i = 0; $i < $repeat; $i++) 
	{
    	$_code .= "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ";
	}

	return($_code);
}

/**
* Funzione per generare il commento di intestazione funzione
*/
function codeFunctionComment($text,$tab=0)
{
	$_code = codeInit();
	
	$_code .= codeChr(2,$tab)."/**"
			 .codeChr(1,$tab)."* ".$text
			 .codeChr(1,$tab)."*/";
	
	return($_code);
}

/**
* Funzione per rilevare il tempo
*/
function codeMicroTime()
{
	$_time = microtime(true);
	
	return($_time);
}

/**
* Funzione per convertira un IP Address in formato m_Address (intero 32bit big endian) in stringa con punti tipo "192.168.0.1"
*/
function codeMAddressToIp($mAddress)
{
	$_ip = null;
	
	if (isset($mAddress))
	{
		/* OLD
		$_ip_packed = pack("V", (string)$mAddress);
		$_ip_unpacked = unpack("Nip", $_ip_packed);
		$_ip_long = $_ip_unpacked["ip"];
		$_ip_float = (float)$_ip_long;
		$_ip = long2ip($_ip_float);
		*/
		
		$_ip_inverted = long2ip((float)$mAddress);
		$_ip_exploded = explode(".",$_ip_inverted);
		$_ip = $_ip_exploded[3].".".$_ip_exploded[2].".".$_ip_exploded[1].".".$_ip_exploded[0];
	}
				
	return($_ip);
}

?>
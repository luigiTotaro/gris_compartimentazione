<?php
/**
* Telefin STLC1000 Consolle
*
* lib_xml.php - Libreria per la generazione del codice XML.
*
* @author Enrico Alborali
* @version 0.0.1 14/07/2011
* @copyright 2011 Telefin S.p.A.
*/

/**
* Funzione per costruire l'intestazione del file xml
*/
function xmlBuildHeader($version="1.0",$encoding="iso-8859-1",$standalone="yes")
{
	$_xml = codeInit();
	
	$_xml .= '<?xml version="'.$version.'" encoding="'.$encoding.'" standalone="'.$standalone.'"?>';
	
	return($_xml);
}

/**
* Funzione per costruire una stringa commento in formato XML
*/
function xmlBuildComment($text)
{
	$_xml  = codeInit();
	
	$_xml .= codeChr(1,0)."<!-- ".$text." -->";
	
	return($_xml);
}

/**
* Funzione per trasformare una stringa da inserire nel codice XML 
*/
function xmlSafeString($string)
{
	$_safe_string = str_replace('"', "'", $string);
	$_safe_string = str_replace("<", "(", $_safe_string);
	$_safe_string = str_replace(">", ")", $_safe_string);
	
	return($_safe_string);
}

/**
* Funzione per generare una serie di elementi XML popolati con i dati di un array
*/
function xmlArray2Elements($data,$element_name="item",$id=null)
{
	$_xml = codeInit();

	logDebug("xmlArray2Elements:STEP1");

	if (isset($data) && is_array($data) && count($data) > 0)
	{
		$_id_counter = 0;

		logDebug("xmlArray2Elements:STEP2");
	
		foreach ($data as $key => $value)
		{
			logDebug("xmlArray2Elements:STEP3n");
			
			if ($element_name === null)
				$_xml .= codeChr(1,2).'<'.xmlSafeString($key).' ';
			else
				$_xml .= codeChr(1,2).'<'.xmlSafeString($element_name).' ';
			if ($id != null && $id === true) $_xml .= 'id="'.xmlSafeString($_id_counter).'" ';
			
			if (is_array($value))
			{
				logDebug("xmlArray2Elements:STEP4");
			
				foreach ($value as $subkey => $subvalue)
				{
					logDebug("xmlArray2Elements:STEP5n");
				
					// Aggiungo un attributo per ogni elemento del subarray
					$_xml .= xmlSafeString($subkey).'="'.xmlSafeString($subvalue).'" ';
				}
			}
			else
			{
				$_xml .= 'value="'.xmlSafeString($value).'" ';
			}
    		
    		$_xml .= ' />';
    		$_id_counter++;
		}
	}
	else
	{
		$_xml .= codeChr(1,2).'<error descr="No data" />';
	}
	
	return($_xml);
}

/**
* Funzione per generare il codice per l'intestazione XML
*/
function xmlBuildHead()
{
	$_xml = codeInit();

	logDebug("xmlBuildHead:STEP1");

	$_xml .= '<?xml version="1.0" encoding="UTF-8"?>';
	
	logDebug("xmlBuildHead:STEP2");
		
	return($_xml);
}

/**
* Funzione per generare il codice per il contenuto XML
*/
function xmlBuildContent($data=null,$element_name="item",$id=null)
{
	$_xml = codeInit();
	
	logDebug("xmlBuildContent:STEP1");

	if ($data != null)
		$_data_elements = xmlArray2Elements($data,$element_name,$id);
	else
		$_data_elements = "";

	logDebug("xmlBuildContent:STEP2");

	$_xml .= codeChr(1,0).'<telefin>'
		.codeChr(1,1).'<data>'
		.$_data_elements
		.codeChr(1,1).'</data>'
		.codeChr(1,0).'</telefin>';	
	
	logDebug("xmlBuildContent:STEP3");
	
	return($_xml);
}

/**
* Funzione per generare tutta la struttura del codice XML
*/
function xmlBuild($data=null,$element_name="item",$id=null)
{
	$_xml = codeInit();
	
	logDebug("xmlBuild:STEP1");
	
	$_xml .= xmlBuildHead();
	
	logDebug("xmlBuild:STEP2");
	
	$_xml .= xmlBuildContent($data,$element_name,$id);
	
	logDebug("xmlBuild:STEP3");
	
	return($_xml);
}

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* lib_html.php - Libreria per la generazione del codice HTML.
*
* @author Enrico Alborali
* @version 1.0.4.2 08/09/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

/**
* Funzione per generare il codice HTML per un div
*/
function htmlDivitis($id,$class=null,$innerHtml='',$style=null,$title=null)
{
	$_html = codeInit();
	
	if ($id == null)
		$_html_id = '';
	else
		$_html_id = 'id="'.$id.'" ';
		
	if ($class == null)
		$_html_class = "";
	else
		$_html_class = 'class="'.$class.'" ';
	
	if ($style == null)
		$_html_style = '';
	else
		$_html_style = 'style="'.$style.'" ';
		
	if ($title == null)
		$_html_title = '';
	else
		$_html_title = 'title="'.$title.'" ';
	
	$_html .= '<div '.$_html_id.$_html_class.$_html_style.$_html_title.'>'.$innerHtml.'</div>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un paragrafo di testo
*/
function htmlParagraph($id,$class,$text='',$style=null)
{
	$_html = codeInit();
	
	if ($id === null)
		$_html_id = '';
	else
		$_html_id = 'id="'.$id.'" ';
		
	if ($class === null)
		$_html_class = "";
	else
		$_html_class = 'class="'.$class.'" ';
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = 'style="'.$style.'" ';
	
	$_html .= '<p '.$_html_id.$_html_class.$_html_style.'>'.$text.'</p>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un link (anchor)
*/
function htmlAnchor($id,$class,$href,$innerHtml='',$target=null,$style=null)
{
	$_html = codeInit();
	
	if ($id === null)
		$_html_id = '';
	else
		$_html_id = 'id="'.$id.'" ';
		
	if ($class === null)
		$_html_class = "";
	else
		$_html_class = 'class="'.$class.'" ';
	
	if ($target === null)
		$_html_target = '';
	else
		$_html_target = 'target="'.$target.'" ';
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = 'style="'.$style.'" ';
	
	$_html .= '<a '.$_html_id.$_html_class.$_html_style.$_html_target.'href="'.$href.'">'.$innerHtml.'</a>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un'immagine
*/
function htmlImage($id,$class,$imageFile,$skin="default",$style=null)
{
	$_html = codeInit();
	
	if ($id === null)
		$_html_id = '';
	else
		$_html_id = 'id="'.$id.'" ';
		
	if ($class === null)
		$_html_class = "";
	else
		$_html_class = 'class="'.$class.'" ';
	
	if ($skin === null)
		$_html_src = 'src="images/'.$imageFile.'" ';
	else
		$_html_src = 'src="images/skins/'.$skin.'/'.$imageFile.'" ';
		
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = 'style="'.$style.'" ';
	
	$_html .= '<img '.$_html_id.$_html_class.$_html_style.$_html_src.'>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un'icona per barra (es: navbar o bottombar)
*/
function htmlNavbarIcon($bar,$id,$hpos,$image,$style=null,$left=true,$skin=null,$profile=null,$lightIcon=false,$lightOn=false)
{
	$_html = codeInit();
	$_html_images = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	if ($left === true)
	{
		$_html_hpos = 'left: '.$hpos.'px; ';
	}
	else
	{
		$_html_hpos = 'right: '.$hpos.'px; ';
	}
	
	if ($lightIcon == true)
	{
		if ($lightOn == true)
		{
			$_html_light = "display: inherit; ";
			
		}
		else
		{
			$_html_light = "";
		}
		$_html_image_light = codeChr(1,5).htmlImage( $bar.'_button_'.$id.'_image_light', $bar.'_button_image_light', 'sk_'.$bar.'_icon_'.$image.'_1.png', $skin, $_html_style.$_html_light );
	}
	else
	{
		$_html_image_light = "";
	}
	
	$_html_images .= 
		 codeChr(1,5).htmlImage( $bar.'_button_'.$id.'_image', $bar.'_icon_image', 'sk_'.$bar.'_icon_'.$image.'.png', $skin, $_html_style )
		 .$_html_image_light
		 ;
		
	$_html .= htmlDivitis( $bar.'_button_'.$id, $bar.'_icon', $_html_images, $_html_hpos.$style );
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un bottone per barra (es: navbar o bottombar)
*/
function htmlNavbarButton($bar,$id,$hpos,$image,$style=null,$left=true,$skin=null,$profile=null,$lightIcon=false,$lightOn=false,$simple=false,$balloonText=null)
{
	$_html = codeInit();
	$_html_images = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	if ($left === true)
	{
		$_html_hpos = 'left: '.$hpos.'px; ';
	}
	else
	{
		$_html_hpos = 'right: '.$hpos.'px; ';
	}
	
	if ($lightIcon == true)
	{
		if ($lightOn == true)
		{
			$_html_light = "display: inherit; ";
			
		}
		else
		{
			$_html_light = "";
		}
		$_html_image_light = codeChr(1,5).htmlImage( $bar.'_button_'.$id.'_image_light', $bar.'_button_image_light', 'sk_'.$bar.'_icon_'.$image.'_1.png', $skin, $_html_style.$_html_light );
	}
	else
	{
		$_html_image_light = "";
	}
	
	$_html_images .= 
		 codeChr(1,5).htmlImage( $bar.'_button_'.$id.'_image', $bar.'_button_image', 'sk_'.$bar.'_icon_'.$image.'.png', $skin, $_html_style )
		.$_html_image_light
		.codeChr(1,5).htmlImage( $bar.'_button_'.$id.'_over', $bar.'_button_over', 'sk_'.$bar.'_button_over.png', $skin, $_html_style )
		.codeChr(1,5).htmlImage( $bar.'_button_'.$id.'_click', $bar.'_button_click', 'sk_'.$bar.'_button_click.png', $skin, $_html_style )
		.codeChr(1,5).htmlImage( $bar.'_button_'.$id.'_separators', $bar.'_button_separators', 'sk_'.$bar.'_button_separators.png', $skin, $_html_style )
		;
	
	$_simple_class = ($simple)?"_simple":"";	

	$_html .= htmlDivitis( $bar.'_button_'.$id, $bar.$_simple_class.'_button', $_html_images, $_html_hpos.$style );
	
	if ($balloonText !== null)
	{
		$_balloon_width = 10+ceil(strlen($balloonText)*6.7);
		$_html .= htmlBalloonBuild($id,null,$hpos,0, $_balloon_width,$balloonText,$left);
	}
	else
		$_html .= htmlDivitis( null, null, "" );
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un testo per barra (es: navbar o bottombar)
*/
function htmlNavbarText($bar,$id,$hpos,$width,$text,$style=null,$left=true,$skin=null,$profile=null)
{
	$_html = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if ($left === true)
	{
		$_html_hpos = 'left: '.$hpos.'px; ';
	}
	else
	{
		$_html_hpos = 'right: '.$hpos.'px; ';
	}
	
	if (isset($width))
	{
		$_html_width = 'width: '.$width.'px; ';
		$_html_p_width = 'width: '.($width-10).'px; ';
	}
	else
	{
		$_html_width = '';
		$_html_p_width = '';
	}
	
	$_html_p = htmlParagraph( $bar.'_text_par_'.$id, $bar.'_text_par', $text, $_html_p_width.$style );
	
	$_html .= htmlDivitis( $bar.'_text_'.$id, $bar.'_text', $_html_p, $_html_hpos.$_html_width.$style );
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per costruire un menu di navigazione
*/
function htmlNavbarMenu($_page_levels)
{
	global $_level;
	global $_conf_console_app_navbar_serial_selector_visible,$_conf_console_app_navbar_ethernet_selector_visible;
	
	$_html = codeInit();
	$_level_index = 0;
	
	foreach ($_page_levels as $_page_level)
	{
		$_name 			= $_page_level['name'];
		$_profile 		= $_page_level['profile'];
		$_default_mode 	= $_page_level['default_mode'];
		$_info_mode 	= $_page_level['info_mode'];
		$_details_mode 	= $_page_level['details_mode'];
		$_edit_mode 	= $_page_level['edit_mode'];
		$_add_mode 		= $_page_level['add_mode'];
						
		// Controllo del profilo e abilitazione visualizzazione livelli
		if ((($_name=='serial' && $_conf_console_app_navbar_serial_selector_visible!==false)
		||($_name=='ethernet' && $_conf_console_app_navbar_ethernet_selector_visible!==false)
		||($_name!='ethernet' && $_name!='serial')) && authCheckProfile($_profile))
		{
			$_html .= coreBuildNavbarSelectorButton($_level_index,$_name,($_level == $_name)?true:false,"Seleziona livello ".$_name);
			$_level_index++;
		}
	}
		
	return($_html);
}

/**
* Funzione per generare il codice HTML per una linea di taglio verticale
*/
function htmlPanelVerticalCut($side,$left,$top,$height)
{
	$_html = codeInit();
	
	if ($height <= 80)
	{
		$_height_bg = $height-40;
		
		$_html .=	codeChr(1,5).htmlImage(null,'details_panel_vcut_bg','sk_panel_vcut_bg.png','default',$side.': '.$left.'px; top: '.$top.'px; height: '.$_height_bg.'px;')
					.codeChr(1,5).htmlImage(null,'details_panel_vcut_bottom','sk_panel_vcut_bottom.png','default',$side.': '.$left.'px; top: '.($top+$_height_bg).'px; height: 40px;');

	}
	else
	{
		$_height_bg = $height-80;
		
		$_html .=	codeChr(1,5).htmlImage(null,'details_panel_vcut_top','sk_panel_vcut_top.png','default',$side.': '.$left.'px; top: '.$top.'px; height: 40px;')
					.codeChr(1,5).htmlImage(null,'details_panel_vcut_bg','sk_panel_vcut_bg.png','default',$side.': '.$left.'px; top: '.($top+40).'px; height: '.$_height_bg.'px;')
					.codeChr(1,5).htmlImage(null,'details_panel_vcut_bottom','sk_panel_vcut_bottom.png','default',$side.': '.$left.'px; top: '.($top+40+$_height_bg).'px; height: 40px;');
	}

	return($_html);
}

/**
* Funzione per generare il codice HTML per una linea di taglio "a L"
*/
function htmlPanelLCut($side,$left,$top,$height,$width)
{
	$_html = codeInit();
	
	$_height_bg = $height-1;
	$_width_bg = $width-40;
		
	$_html .=	codeChr(1,5).htmlImage(null,'details_panel_vcut_bg','sk_panel_vcut_bg.png','default',$side.': '.$left.'px; top: '.$top.'px; height: '.$_height_bg.'px;')
				.codeChr(1,5).htmlImage(null,'details_panel_hcut_bg','sk_panel_vcut_bg.png','default',$side.': '.$left.'px; top: '.($top+$_height_bg).'px; height: 1px; width: '.$_width_bg.'px;')
				.codeChr(1,5).htmlImage(null,'details_panel_hcut_right','sk_panel_hcut_right.png','default',$side.': '.($left+$_width_bg).'px; top: '.($top+$_height_bg).'px; height: 1px; width: 40px;');

	return($_html);
}

/**
* Funzione per generare il codice HTML per un bottone da pannello
*/
function htmlPanelButton($panel,$id,$class,$hpos,$top,$image,$style=null,$left=true,$profile=null,$balloonText=null)
{
	$_html = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	if ($left === true)
	{
		$_html_hpos = 'left: '.$hpos.'px; ';
	}
	else
	{
		$_html_hpos = 'right: '.$hpos.'px; ';
	}
	
	$_html_img = codeChr(1,5).htmlImage($panel.'_button_'.$id.'_icon',$class.'_icon','sk_button_'.$image.'.png','default')
		.codeChr(1,5).htmlImage($panel.'_button_'.$id.'_over',$class.'_over','sk_button_over.png','default')
		.codeChr(1,5).htmlImage($panel.'_button_'.$id.'_click',$class.'_click','sk_button_click.png','default');
	
	$_html .= codeChr(1,5).htmlDivitis($panel.'_button_'.$id,$class,$_html_img,$_html_hpos.'top: '.$top.'px;'.$_html_style);
	
	if ($balloonText !== null)
	{
		$_balloon_width = 10+ceil(strlen($balloonText)*6.7);
		$_html .= htmlBalloonBuild("panel_button_".$id,null,$hpos, $top, $_balloon_width,$balloonText,$left);
	}
	else
		$_html .= htmlDivitis( null, null, "" );
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un testo per il pannello ACK
*/
function htmlAckPanelText($id,$class,$left,$top,$textWidth,$text=null,$style=null,$profile=null)
{
	$_html = codeInit();
	
	// Controllo del profilo visualizzazione
	if (!authCheckProfile($profile)) return($_html);
	
	if ($class === null)
		$_class = "icon_status";
	else
		$_class = $class;
		
	$_base = $_class."_ack";
	
	if ($id === null)
	{
		$_id_text 	= '';
	}
	else
	{
		$_id_text 	= $_base."_".$id."_text";
	}
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	// Testo
	$_html .=	codeChr(1,5).'<p id="'.$_id_text.'_'.$id.'" class="'.$_base.'_text" style="left: '.($left).'px; top: '.$top.'px; width: '.$textWidth.'px;">'.$text.'</p>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un input con etichetta, valore e testo per il pannello ACK
*/
function htmlAckPanelFormInput($id,$class,$left,$top,$labelWidth,$label,$valueWidth,$value='',$textWidth,$text=null,$style=null,$profile=null)
{
	$_html = codeInit();
	
	// Controllo del profilo visualizzazione
	if (!authCheckProfile($profile)) return($_html);
	
	if ($class === null)
		$_class = "icon_status";
	else
		$_class = $class;
		
	$_base = $_class."_ack";
	
	if ($id === null)
	{
		$_id_label 	= '';
		$_id_input 	= '';
		$_id_text 	= '';
	}
	else
	{
		$_id_label 	= $_base."_".$id."_label";
		$_id_input 	= $_base."_".$id."_input";
		$_id_text 	= $_base."_".$id."_text";
	}
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	// Etichetta
	$_html .=	codeChr(1,5).'<label id="'.$_id_label.'" class="'.$_base.'_label" for="'.$_id_input.'" style="left: '.$left.'px; top: '.$top.'px; width: '.$labelWidth.'px;">'.$label.'</label>';
	// Input
	$_html .= codeChr(1,5).'<input id="'.$_id_input.'" class="'.$_base.'_input" type="text" name="'.$_id_input.'" value="'.$value.'" style="left: '.($left+5+$labelWidth).'px; top: '.$top.'px; width: '.$valueWidth.'px;">';
	// Testo
	if ($text !== null)
		$_html .=	codeChr(1,5).'<p id="'.$_id_text.'" class="'.$_base.'_text" style="left: '.($left+5+$labelWidth+10+$valueWidth).'px; top: '.$top.'px; width: '.$textWidth.'px;">'.$text.'</p>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un bottone da pannello ACK
*/
function htmlAckPanelButton($id,$class,$left,$top,$image,$style=null,$profile=null,$balloonText=null)
{
	$_html = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if ($class === null)
		$_class = "icon_status";
	else
		$_class = $class;
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	$_html_img = codeChr(1,5).htmlImage($_class.'_ack_button_'.$id.'_icon',$_class.'_ack_button_icon','sk_button_'.$image.'.png','default')
		.codeChr(1,5).htmlImage($_class.'_ack_button_'.$id.'_over',$_class.'_ack_button_over','sk_button_over.png','default')
		.codeChr(1,5).htmlImage($_class.'_ack_button_'.$id.'_click',$_class.'_ack_button_click','sk_button_click.png','default');
	
	$_html .= codeChr(1,5).htmlDivitis($_class.'_ack_button_'.$id,$_class.'_ack_button',$_html_img,'left: '.$left.'px; top: '.$top.'px;'.$_html_style);
	
	if ($balloonText !== null)
	{
		$_balloon_width = 10+ceil(strlen($balloonText)*6.7);
		$_html .= htmlBalloonBuild("ack_panel_button_".$id, null, $left, $top, $_balloon_width, $balloonText, true);
	}
	else
		$_html .= htmlDivitis( null, null, "" );
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per il pannellino di gestione ACK
*/
function htmlAckPanel($id,$class,$hpos,$top,$style=null,$left=true,$profile=null,$ack=null)
{
	$_html = codeInit();
	//*
	// Controllo del profilo visualizzazione
	if (!authCheckProfile($profile)) return($_html);
	
	if ($class === null)
		$_class = "icon_status";
	else
		$_class = $class;
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
	if ($left === true)
	{
		$_html_hpos = 'left: '.$hpos.'px; ';
	}
	else
	{
		$_html_hpos = 'right: '.$hpos.'px; ';
	}
	if ($ack === null)
	{
		$_style_add = 'display: inherit; ';
		$_style_view = 'display: none; ';
		$_style_edit = 'display: none; ';
	}
	else
	{
		$_style_add = 'display: none; ';
		$_style_view = 'display: inherit; ';
		$_style_edit = 'display: none; ';
	}
	
	// Aggiungi ACK
	$_inner_add_html = 
		 htmlAckPanelFormInput( "add_duration", $_class, 5, 0, 60, "Tacita per", 25, "3", 30, "giorni", null, null )
		.htmlAckPanelButton( "add", 	$_class, 140, 0, "ok", 		null, null, "Tacita" )
		.htmlAckPanelButton( "close", 	$_class, 170, 0, "close", 	null, null, "Annulla" )
		;
	$_add_html = codeChr(1,5).htmlDivitis($_class."_ack_add_".$id, $_class."_ack_add", $_inner_add_html, $_style_add);
	
	// Visualizza ACK
	if ($ack === null)
		$_inner_view_html = "";
	else
	{
		$_ack_date_time = $ack['AckDate'];
		$_ack_duration_min = $ack['AckDurationMinutes'];
		$_ack_duration_sec = $_ack_duration_min*60;
		$_ack_date_time_offset = $_ack_date_time->modify('+'.$_ack_duration_sec.' second');
		$_ack_date_time_offset = $_ack_date_time->format('d/m/Y H:i:s');
		
		$_inner_view_html = 
		 htmlAckPanelText( $id, $_class, 5, 0, 135, $_ack_date_time_offset, null, null )
		.htmlAckPanelButton( "edit", 	$_class, 140, 0, "edit", 	null, null, "Modifica tacitazione" )
		.htmlAckPanelButton( "close",	$_class, 170, 0, "close", 	null, null, "Chiudi" )
		;
	}
	$_view_html = codeChr(1,5).htmlDivitis($_class."_ack_view_".$id, $_class."_ack_view", $_inner_view_html, $_style_view);
	
	// Modifica ACK
	if ($ack === null)
		$_inner_edit_html = "";
	else
	{
		$_ack_id = $ack['DeviceAckID'];
		
		$_inner_edit_html = 
		 htmlAckPanelFormInput( "edit_id", $_class, 5, 0, 60, "", 25, $_ack_id, 30, null, "diplay: none; readonly: true;", null )
		.htmlAckPanelFormInput( "edit_duration", $_class, 5, 0, 60, "Tacita per", 25, "3", 30, "giorni", null, null )
		.htmlAckPanelButton( "save", 	$_class, 140, 0, "ok", 		null, null, "Aggiorna tacitazione" )
		.htmlAckPanelButton( "cancel", 	$_class, 170, 0, "cancel", 	null, null, "Annulla" )
		;
	}
	$_edit_html = codeChr(1,5).htmlDivitis($_class."_ack_edit_".$id, $_class."_ack_edit", $_inner_edit_html, $_style_edit);
	
	// Pannello ACK
	$_inner_html = codeChr(1,6).htmlImage($_class."_ack_panel_hole_".$id, $_class."_ack_panel_hole","gui/sk_ack_panel_hole.png",'default')
		.codeChr(1,6).htmlImage($_class."_ack_panel_left_".$id, $_class."_ack_panel_left","gui/sk_ack_panel_left.png",'default')
		.codeChr(1,6).htmlImage($_class."_ack_panel_center_".$id, $_class."_ack_panel_center","gui/sk_ack_panel_center.png",'default')
		.codeChr(1,6).htmlImage($_class."_ack_panel_right_".$id, $_class."_ack_panel_right","gui/sk_ack_panel_right.png",'default')
		.$_add_html
		.$_view_html
		.$_edit_html
		;
	$_html = codeChr(1,5).htmlDivitis($_class."_ack_panel_".$id, $_class."_ack_panel", $_inner_html, $_html_hpos.'top: '.$top.'px;'.$_html_style);
	//*/
	return($_html);
}

/**
* Funzione per generare il codice HTML per un pallino di stato
*/
function htmlIconStatus($id,$class,$hpos,$top,$color,$symbol=null,$selected=false,$style=null,$left=true,$profile=null,$ackProfile=null,$ack=null,$isTable=null)
{
	$_html = codeInit();
	
	// Controllo del profilo visualizzazione
	if (!authCheckProfile($profile)) return($_html);
	
	if ($class === null)
		$_class = "icon_status";
	else
		$_class = $class;
	
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
	if ($selected === true)
	{
		$_html_unselected = 'display: none;';
		$_html_selected = 'display: inherit;';
	}
	else
	{
		$_html_unselected = null;
		$_html_selected = null;
	}	
	if ($left === true)
	{
		$_html_hpos = 'left: '.$hpos.'px; ';
	}
	else
	{
		$_html_hpos = 'right: '.$hpos.'px; ';
	}
	if ($ack !== null)
	{
		$_html_ack = 'display: inherit; ';
	}
	else
	{
		$_html_ack = '';
	}
	
	// Colore
	$_inner_html =   	 codeChr(1,6).htmlDivitis($_class."_color_".$id, $_class."_color", "", "background-color: ".$color);
	// Simbolo (opzionale)
	if ($symbol !== null)
		$_inner_html .=  codeChr(1,6).htmlImage($_class."_symbol_".$id, $_class."_symbol","gui/sk_icon_status_".$symbol.".png",'default');
	// Buco normale
	$_inner_html .= 	 codeChr(1,6).htmlImage($_class."_hole_".$id, $_class."_hole","gui/sk_icon_status_hole.png",'default',$_html_unselected)
						.codeChr(1,6).htmlImage($_class."_hole_selected_".$id, $_class."_hole_selected","gui/sk_icon_status_hole_selected.png",'default',$_html_selected)
						.codeChr(1,6).htmlImage($_class."_ack_".$id, $_class."_ack","gui/sk_icon_status_ack.png",'default',$_html_ack)
						;
	
	// Controllo del profilo per l'ACK
	if (authCheckProfile($ackProfile) && ($isTable === null || $isTable === false))
	{
		$_inner_html .=  codeChr(1,6).htmlImage($_class."_ack_over_".$id, $_class."_ack_over","gui/sk_icon_status_ack_over.png",'default')
						.codeChr(1,6).htmlImage($_class."_ack_click_".$id, $_class."_ack_click","gui/sk_icon_status_ack_click.png",'default')
						;
		$_html .= codeChr(1,5).htmlDivitis($_class."_".$id, $_class, $_inner_html, $_html_hpos.'top: '.$top.'px;'.$_html_style);
		// Pannellino ACK
		//$_html_ack_panel = htmlAckPanel($id,$_class,$hpos,$top,$style,$left,$ackProfile,$ack);
		$_html_ack_panel = htmlAckPanel($id,$_class,$hpos,$top,$style,$left,null,$ack);
		$_html .= $_html_ack_panel;
	}
	else
		$_html .= codeChr(1,5).htmlDivitis($_class."_".$id, $_class, $_inner_html, $_html_hpos.'top: '.$top.'px;'.$_html_style);
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un dialog jQuery UI
*/
function htmlDialog($id,$title,$text)
{
	$_html = codeInit();

	$_p = htmlParagraph($id."_dialog_p",null,$text,null);
	$_html .= codeChr(1,5).htmlDivitis($id."_dialog",null,$_p,null,$title);

	return($_html);
}

/**
* Funzione per generare il codice per un menu a tendina dato un array di elementi id,nome
*/
function htmlPanelSelectMenu($x,$y,$width,$name,$items,$text=null,$textWidth=null,$buttonActive=true,$buttonUp=false)
{
	$_html_menu = codeInit();
			
	$_id_counter = 0;
	
	if (isset($items) && is_array($items) && count($items) > 0)
	{
		$_height = 30*(count($items)+1);
	}
	else
	{
		$_height = 30*(2);
	}
	
	$_html_menu .= codeChr(1,5).htmlImage(null,'panel_select_menu_top_left','sk_panel_menu_top_left.png','default')
			.codeChr(1,5).htmlImage(null,'panel_select_menu_top','sk_panel_menu_top.png','default','width:'.($width-16).'px;')
			.codeChr(1,5).htmlImage(null,'panel_select_menu_top_right','sk_panel_menu_top_right.png','default')
			.codeChr(1,5).htmlImage(null,'panel_select_menu_left','sk_panel_menu_left.png','default','height:'.($_height-16).'px;')
			.codeChr(1,5).htmlDivitis(null,'panel_select_menu_bg','&nbsp;','width:'.($width-16).'px; height:'.($_height-16).'px;')
			.codeChr(1,5).htmlImage(null,'panel_select_menu_right','sk_panel_menu_right.png','default','height:'.($_height-16).'px;')
			.codeChr(1,5).htmlImage(null,'panel_select_menu_bottom_left','sk_panel_menu_bottom_left.png','default')
			.codeChr(1,5).htmlImage(null,'panel_select_menu_bottom','sk_panel_menu_bottom.png','default','width:'.($width-16).'px;')
			.codeChr(1,5).htmlImage(null,'panel_select_menu_bottom_right','sk_panel_menu_bottom_right.png','default');
	
	// Costruisco il menu a tendina
	if (isset($items) && is_array($items) && count($items) > 0)
	{
		// Eventuale testo
		if (isset($text))
		{
			if ($textWidth === null) $_text_width = $width;
			else $_text_width = $textWidth;
			
			$_html_menu .= codeChr(1,5).htmlParagraph('panel_select_'.$name.'_menu_text','panel_select_menu_text',$text,'left:'.(0).'px; top: '.(0).'px; width: '.($_text_width).'px;');
		}
				
		foreach ($items as $_item)
		{
			if (isset($_item) && is_array($_item))
			{
				$_html_menu .= codeChr(1,5).htmlParagraph('panel_select_'.$name.'_menu_item_'.$_item['id'],'panel_select_menu_item',$_item['name'],'left:'.(0).'px; top: '.(0+(30*($_id_counter+1))).'px; width: '.($width-50).'px;');
				$_html_menu .= codeChr(1,5).htmlImage(null,'panel_select_menu_separator','sk_panel_menu_separator.png','default','top: '.(30*($_id_counter+1)).'px;width:'.($width).'px;');
				$_id_counter++;
			}
			else
			{
				// Elemento errato
			}
		}
	}
	else
	{
		// Nessun elemento
		$_html_menu .= codeChr(1,5).htmlParagraph('panel_select_'.$name.'_menu_item_'.$_item['id'],'panel_select_menu_item','Nessun elemento','left:'.(0).'px; top: '.(0+(30*($_id_counter+1))).'px; width: '.($width-50).'px;');
		$_html_menu .= codeChr(1,5).htmlImage(null,'panel_select_menu_separator','sk_panel_menu_separator.png','default','top: '.(30*($_id_counter+1)).'px;width:'.($width).'px;');
	}
		
	$_html_style = 'left:'.($x).'px; top:'.($y).'px; width: '.$width.'px; height: '.$_height.'px;';
	$_html = htmlDivitis('panel_select_'.$name.'_menu','panel_select_menu',$_html_menu,$_html_style);
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un info con etichetta e valore
*/
function htmlPanelInfo($id,$left,$top,$nameWidth,$name,$valueWidth,$value='',$linked=false,$color=null,$profile=null)
{
	$_html = codeInit();
	
	// Controllo del profilo visualizzazione
	if (!authCheckProfile($profile)) return($_html);
	
	if ($id === null)
	{
		$_html_id_p1 = '';
		$_html_id_p2 = '';
	}
	else
	{
		$_html_id_p1 = 'id="details_panel_info_'.$id.'_name" ';
		$_html_id_p2 = 'id="details_panel_info_'.$id.'_value" ';
	}
	
	if ($linked === true)
	{
		$_html_class = 'details_panel_info_link';
	}
	else
	{
		$_html_class = 'details_panel_info_value';
	}
	
	if ($color === null)
		$_html_color = '';
	else
		$_html_color = ' color: '.$color.';';
	
	if (isset($name) && $name != '') $name .= ':';
	
	$_html .=	codeChr(1,5).'<p '.$_html_id_p1.'class="details_panel_info_name" style="left: '.$left.'px; top: '.$top.'px; width: '.$nameWidth.'px;">'.$name.'</p>'
				.codeChr(1,5).'<p '.$_html_id_p2.'class="'.$_html_class.'" style="left: '.($left+5+$nameWidth).'px; top: '.$top.'px; width: '.$valueWidth.'px;'.$_html_color.'">'.$value.'</p>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un input con etichetta e valore
*/
function htmlPanelFormInput($form,$id,$left,$top,$nameWidth,$name,$valueWidth,$value='',$color=null,$style=null,$readOnly=false,$profile=null)
{
	$_html = codeInit();
	
	// Controllo del profilo visualizzazione
	if (!authCheckProfile($profile)) return($_html);
	
	$_inner_html = codeInit();
	
	if ($id === null)
	{
		$_id_div	= "";
		$_id_label 	= '';
		$_id_input 	= '';
	}
	else
	{
		$_id_div 	= $form.'_form_'.$id.'_element';
		$_id_label 	= $form.'_form_'.$id.'_label';
		$_id_input 	= $form.'_form_'.$id.'_input';
	}
	
	if ($name == ".")
	{
		$_name_html = $name;
	}
	else
	{
		$_name_html = $name.":";
	}
		
	if ($color === null)
		$_html_color = '';
	else
		$_html_color = ' color: '.$color.';';
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	if ($readOnly === true)
		$_html_read_only = ' readonly="readonly"';
	else
		$_html_read_only = '';
	
	// Etichetta
	$_inner_html .=	codeChr(1,5).'<label id="'.$_id_label.'" class="details_panel_form_label" for="'.$_id_input.'" style="left: '.$left.'px; top: '.$top.'px; width: '.$nameWidth.'px;">'.$_name_html.'</label>';
	// Input
	$_inner_html .= codeChr(1,5).'<input id="'.$_id_input.'" class="details_panel_form_input" type="text" name="'.$_id_input.'" value="'.$value.'" style="left: '.($left+5+$nameWidth).'px; top: '.$top.'px; width: '.$valueWidth.'px;'.$_html_color.'"'.$_html_read_only.'>';
	
	// Divitis
	$_html .= htmlDivitis($_id_div,'details_panel_element',$_inner_html,$_html_style);
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per una select con etichetta, valore e lista
*/
function htmlPanelFormSelect($form,$id,$left,$top,$nameWidth,$name,$valueWidth,$value='',$color=null,$style=null,$readOnly=false)
{
	$_html = codeInit();
	$_inner_html = codeInit();
	
	if ($id === null)
	{
		$_id_div	= "";
		$_id_label 	= '';
		$_id_input 	= '';
		$_id_down 	= "";
		$_id_up 	= "";
		$_id_search	= "";
	}
	else
	{
		$_id_form	= $form."_form";
		$_id_div 	= $form.'_form_'.$id.'_element';
		$_id_label 	= $form.'_form_'.$id.'_label';
		$_id_input 	= $form.'_form_'.$id.'_select';
		$_id_down 	= $id.'_select_down';
		$_id_up 	= $id.'_select_up';
		$_id_search	= $id.'_select_search';
	}
	
	if ($color === null)
		$_html_color = '';
	else
		$_html_color = ' color: '.$color.';';
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
	if ($readOnly === true)
		$_html_readonly = ' readonly="readonly"';
	else
		$_html_readonly = '';
	
	// Etichetta
	$_inner_html .=	codeChr(1,5).'<label id="'.$_id_label.'" class="details_panel_form_label" for="'.$_id_input.'" style="left: '.$left.'px; top: '.$top.'px; width: '.$nameWidth.'px;">'.$name.':</label>';
	// Input
	$_inner_html .= codeChr(1,5).'<input id="'.$_id_input.'" class="details_panel_form_select" type="text" name="'.$_id_input.'" value="'.$value.'" style="left: '.($left+5+$nameWidth).'px; top: '.$top.'px; width: '.($valueWidth-30).'px;'.$_html_color.'"'.$_html_readonly.'>';
	
	$_html_hpos = 'left: '.($left+$nameWidth+5+$valueWidth-30).'px; ';
	$_html_top = 'top: '.$top.'px; ';
	
	// Icona Down
	$_inner_html .= codeChr(1,5).htmlImage($_id_form.'_image_'.$_id_down,'details_panel_select_down_icon','sk_button_select_down.png','default',$_html_hpos.$_html_top);
	// Icona Up
	$_inner_html .= codeChr(1,5).htmlImage($_id_form.'_image_'.$_id_up,'details_panel_select_up_icon','sk_button_select_up.png','default',$_html_hpos.$_html_top);
	// Icona Search
	$_inner_html .= codeChr(1,5).htmlImage($_id_form.'_image_'.$_id_search,'details_panel_search_icon','sk_button_select_search.png','default',$_html_hpos.$_html_top);
	
	// Divitis
	$_html .= htmlDivitis($_id_div,'details_panel_element',$_inner_html,$_html_style);
						
	return($_html);
}

/**
* Funzione per generare il codice HTML per un input con etichetta e autocompletamento
*/
function htmlPanelFormSearch($form,$id,$left,$top,$nameWidth,$name,$valueWidth,$value='',$color=null,$style=null)
{
	$_html = codeInit();
	$_inner_html = codeInit();
	
	if ($id === null)
	{
		$_id_div	= "";
		$_id_label 	= '';
		$_id_input 	= '';
		$_id_down 	= "";
		$_id_up 	= "";
		$_id_search	= "";
	}
	else
	{
		$_id_form	= $form."_form";
		$_id_div 	= $form.'_form_'.$id.'_element';
		$_id_label 	= $form.'_form_'.$id.'_label';
		$_id_input 	= $form.'_form_'.$id.'_search';
		$_id_down 	= $id.'_select_down';
		$_id_up 	= $id.'_select_up';
		$_id_search	= $id.'_search_icon';
	}
	
	if ($color === null)
		$_html_color = '';
	else
		$_html_color = ' color: '.$color.';';
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
	
	// Etichetta
	$_inner_html .=	codeChr(1,5).'<label id="'.$_id_label.'" class="details_panel_form_label" for="'.$_id_input.'" style="left: '.$left.'px; top: '.$top.'px; width: '.$nameWidth.'px;">'.$name.':</label>';
	// Input
	$_inner_html .= codeChr(1,5).'<input id="'.$_id_input.'" class="details_panel_form_search" type="text" name="'.$_id_input.'" value="'.$value.'" style="left: '.($left+5+$nameWidth).'px; top: '.$top.'px; width: '.($valueWidth-30).'px;'.$_html_color.'">';
			
	$_html_hpos = 'left: '.($left+$nameWidth+5+$valueWidth-30).'px; ';
	$_html_top = 'top: '.$top.'px; ';
	
	// Icona
	$_inner_html .= codeChr(1,5).htmlImage($_id_form.'_image_'.$_id_search,'details_panel_search_icon','sk_button_select_search.png','default',$_html_hpos.$_html_top);
	// Divitis
	$_html .= htmlDivitis($_id_div,'details_panel_element',$_inner_html,$_html_style);
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per una checkbox con etichetta e valore
*/
function htmlPanelFormCheckBox($form,$id,$left,$top,$nameWidth,$name,$valueWidth,$value='',$color=null,$style=null,$readOnly=false)
{
	$_html = codeInit();
	$_inner_html = codeInit();
	
	if ($id === null)
	{
		$_id_div	= "";
		$_id_label 	= '';
		$_id_input 	= '';
	}
	else
	{
		$_id_div 	= $form.'_form_'.$id.'_element';
		$_id_label 	= $form.'_form_'.$id.'_label';
		$_id_input 	= $form.'_form_'.$id.'_input';
	}
	
	if ($name == ".")
	{
		$_name_html = $name;
	}
	else
	{
		$_name_html = $name.":";
	}
		
	if ($color === null)
		$_html_color = '';
	else
		$_html_color = ' color: '.$color.';';
	if ($style === null)
		$_html_style = '';
	else
		$_html_style = $style;
		
	if ($readOnly === true)
		$_html_read_only = ' readonly="readonly"';
	else
		$_html_read_only = '';
	
	// Etichetta SX
	$_inner_html .=	codeChr(1,5).'<label id="'.$_id_label.'" class="details_panel_form_label" style="left: '.$left.'px; top: '.$top.'px; width: '.$nameWidth.'px;">'.$_name_html.'</label>';
	// Input Checkbox
	$_inner_html .= codeChr(1,5).'<input id="'.$_id_input.'" class="details_panel_form_checkbox" type="checkbox" name="'.$_id_input.'" value="'.$value.'" style="left: '.($left+5+$nameWidth).'px; top: '.$top.'px; width: '.$valueWidth.'px;'.$_html_color.'"'.$_html_read_only.'>';
	// Etichetta DX
	$_inner_html .=	codeChr(1,5).'<label id="'.$_id_label.'_dx" class="details_panel_form_label" for="'.$_id_input.'" style="left: '.($left+5+$nameWidth+20).'px; top: '.$top.'px; width: '.$nameWidth.'px;">'.$_name_html.'</label>';
	
	// Divitis
	$_html .= htmlDivitis($_id_div,'details_panel_element',$_inner_html,$_html_style);
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un testo con solo etichetta
*/
function htmlPanelTextName($id,$left,$top,$sev,$nameWidth,$name,$linked=false,$selected=false,$array=false,$maxNameChar=null,$skin='default',$ackLevel=null,$ack=null,$isTable=null)
{
	$_html = codeInit();
	
	if ($id === null)
	{
		$_html_id = '';
	}
	else
	{
		$_html_id = 'id="'.$id.'_name" ';
	}
	
	if ($linked === true)
	{
		$_html_class = 'class="details_panel_text_name" ';
	}
	else
	{
		$_html_class = 'class="details_panel_text_description" ';
	}
	
	// Eventuale troncamento del nome 
	if ($maxNameChar === null || !is_int($maxNameChar))
	{
		$_name = htmlspecialchars(utf8_encode($name));
	}
	else
	{
		$_name = codeStrOverflow(htmlspecialchars(utf8_encode($name)),0,$maxNameChar,"&hellip;");
	}
	
	// Icona array
	if ($array === true)
	{
		$_name_width = $nameWidth-30;
		$_selection_width = $nameWidth;
		$_html .= codeChr(1,5).htmlImage(null,'details_panel_icon_array','sk_icon_array.png',$skin,'left: '.($left+30+$_name_width).'px; top: '.$top.'px;');
	}
	else
	{
		$_name_width = $nameWidth;
		$_selection_width = $nameWidth;
	}
	
	$_html_style = 'style="left: '.($left+30).'px; top: '.$top.'px; width: '.$_name_width.'px;" ';
		
	// Sfondo di selezione
	if ($selected === true)
	{
		$_html .= codeChr(1,5).htmlImage(null,'details_panel_selection_bg','sk_details_selection_bg.png','default','left: '.$left.'px; top: '.$top.'px; width: '.(30+$_selection_width).'px; display: inherit;')
			.codeChr(1,5).htmlImage(null,'details_panel_selection_right','sk_details_selection_right.png','default','left: '.($left+30+$_selection_width).'px; top: '.$top.'px; display: inherit;');
	}
	else
	{
		$_html .= codeChr(1,5).htmlImage(null,'details_panel_selection_bg','sk_details_selection_bg.png','default','left: '.$left.'px; top: '.$top.'px; width: '.(30+$_selection_width).'px; display: none;')
			.codeChr(1,5).htmlImage(null,'details_panel_selection_right','sk_details_selection_right.png','default','left: '.($left+30+$_selection_width).'px; top: '.$top.'px; display: none;');
	}
	$_color_status		= coreGetStatusColor($sev,false,true);
	$_symbol_status		= coreGetStatusSymbol($sev);
	// Icona di stato
	if ($ackLevel !== null)
		$_html .= htmlIconStatus( $ackLevel."_".$id, null, $left, $top, $_color_status, $_symbol_status, $selected, null, true, null, "IaP_installer,IeC_installer,installer,admin",$ack,$isTable);
	else
		$_html .= htmlIconStatus( $id, null, $left, $top, $_color_status, $_symbol_status, $selected, null, true, null, "none",$ack,$isTable);
	//$_html .= codeChr(1,5).htmlImage(null,'details_panel_icon_status','sk_icon_status_'.$sev.'.png',$skin,'left: '.$left.'px; top: '.$top.'px;');
	// Etichetta nome
	$_html .= codeChr(1,5).'<p '.$_html_id.$_html_class.$_html_style.'>'.$_name.'</p>';
		
	return($_html);
}

/**
* Funzione per generare il codice HTML per un testo con etichetta e valore
*/
function htmlPanelTextValue($id,$left,$top,$sev,$nameWidth,$name,$valueWidth,$value='',$linked=false,$selected=false,$maxNameChar=null,$maxValueChar=null,$terminal=false,$skin='default',$ackLevel=null,$ack=null,$isTable=null)
{
	$_html = codeInit();
	
	if ($id === null)
	{
		$_html_id_p1 = '';
		$_html_id_p2 = '';
	}
	else
	{
		$_html_id_p1 = 'id="'.$id.'_name" ';
		$_html_id_p2 = 'id="'.$id.'_value" ';
	}
	
	if ($linked === true)
	{
		$_html_class = 'details_panel_info_link';
	}
	else
	{
		$_html_class = 'details_panel_info_value';
	}
	
	// Eventuale troncamento del nome 
	if ($maxNameChar === null || !is_int($maxNameChar))
	{
		$_name = htmlspecialchars(utf8_encode($name));
	}
	else
	{
		$_name = codeStrOverflow(htmlspecialchars(utf8_encode($name)),0,$maxNameChar,"&hellip;");
	}
	// Eventuale troncamento del valore 
	if ($maxValueChar === null || !is_int($maxValueChar))
	{
		$_value = htmlspecialchars(utf8_encode($value));
	}
	else
	{
		$_value = codeStrOverflow(htmlspecialchars(utf8_encode($value)),0,$maxValueChar,"&hellip;");
	}
	
	// Sfondo di selezione
	if ($selected === true)
	{
		if ($terminal === false)
			$_html .= codeChr(1,5).htmlImage(null,'details_panel_selection_bg','sk_details_selection_bg.png','default','left: '.$left.'px; top: '.$top.'px; width: '.(30+$nameWidth+5+$valueWidth).'px; display: inherit;')
				.codeChr(1,5).htmlImage(null,'details_panel_selection_right','sk_details_selection_right.png','default','left: '.($left+30+$nameWidth+5+$valueWidth).'px; top: '.$top.'px; display: inherit;');
		else
			$_html .= codeChr(1,5).htmlImage(null,'details_panel_selection_bg','sk_details_selection_bg.png','default','left: '.$left.'px; top: '.$top.'px; width: '.(30+$nameWidth+5+$valueWidth).'px; display: inherit;')
				.codeChr(1,5).htmlImage(null,'details_panel_selection_right','sk_details_selection_bg.png','default','left: '.($left+30+$nameWidth+5+$valueWidth).'px; top: '.$top.'px; display: inherit;');
	}
	else
	{
		if ($terminal === false)
			$_html .= codeChr(1,5).htmlImage(null,'details_panel_selection_bg','sk_details_selection_bg.png','default','left: '.$left.'px; top: '.$top.'px; width: '.(30+$nameWidth+5+$valueWidth).'px; display: none;')
				.codeChr(1,5).htmlImage(null,'details_panel_selection_right','sk_details_selection_right.png','default','left: '.($left+30+$nameWidth+5+$valueWidth).'px; top: '.$top.'px; display: none;');
		else
			$_html .= codeChr(1,5).htmlImage(null,'details_panel_selection_bg','sk_details_selection_bg.png','default','left: '.$left.'px; top: '.$top.'px; width: '.(30+$nameWidth+5+$valueWidth).'px; display: none;')
				.codeChr(1,5).htmlImage(null,'details_panel_selection_right','sk_details_selection_bg.png','default','left: '.($left+30+$nameWidth+5+$valueWidth).'px; top: '.$top.'px; display: none;');
	}
	
	$_color_status		= coreGetStatusColor($sev,false,true);
	$_symbol_status		= coreGetStatusSymbol($sev);
	// Icona di stato
	if ($ackLevel !== null)
		$_html .= htmlIconStatus( $ackLevel."_".$id, null, $left, $top, $_color_status, $_symbol_status, $selected, null, true, null, "IaP_installer,IeC_installer,installer,admin", $ack,$isTable);
	else
		$_html .= htmlIconStatus( $id, null, $left, $top, $_color_status, $_symbol_status, $selected, null, true, null, "none", $ack,$isTable);
	
	// Etichetta nome
	$_html .= codeChr(1,5).'<p '.$_html_id_p1.'class="details_panel_text_name" style="left: '.($left+30).'px; top: '.$top.'px; width: '.$nameWidth.'px;">'.$_name.'</p>';
	// Valore
	$_html .= codeChr(1,5).'<p '.$_html_id_p2.'class="details_panel_text_value" style="left: '.($left+30+$nameWidth+5).'px; top: '.$top.'px; width: '.$valueWidth.'px;" >'.$_value.'</p>';
	
	return($_html);
}

/**
* Funzione per costruire il codice HTML per visualizzare un "pillolone" di stato
*/
function htmlPillBuild( $x=0, $y=0, $width=200, $data, $showValues=true, $oldValues=false )
{
	$_html = codeInit(); // 60 66 70
	
	// Imposto le lunghezze dei pezzi di pillolone
	$_pill_left_width 	= 15; 
	$_pill_right_width 	= 15;
	$_pill_bg_width 	= $width-$_pill_left_width-$_pill_right_width;
	$_safe_width		= $width-60;

	
	// Recupero i valori da visualizzare
	if (isset($data) && is_array($data)){
		$_status_255 	= $data['255'];
		$_status_2 		= $data['2'];
		$_status_1 		= $data['1'];
		$_status_0 		= $data['0'];
		$_status_m255 	= $data['-255'];
	}
	else{
		$_status_255 	= 0;
		$_status_2 		= 0;
		$_status_1 		= 0;
		$_status_0 		= 0;
		$_status_m255 	= 0;
	}
	
	if ($oldValues == false || ($_status_255 == 0 && $_status_2 == 0 && $_status_1 == 0 && $_status_0 == 0 && $_status_m255 == 0)){
	
	$_status_sum = $_status_255+$_status_2+$_status_1+$_status_0+$_status_m255;
	
	// Calcolo preliminare delle lunghezze fasce pillolone
	if ($_status_255 > 0) 
		$_width_255 = 10+floor(($_safe_width*$_status_255)/$_status_sum);
	else
		$_width_255 = 0;
	if ($_status_2 > 0) 
		$_width_2 = 10+floor(($_safe_width*$_status_2)/$_status_sum);
	else
		$_width_2 = 0;
	if ($_status_1 > 0) 
		$_width_1 = 10+floor(($_safe_width*$_status_1)/$_status_sum);
	else
		$_width_1 = 0;
	if ($_status_0 > 0) 
		$_width_0 = 10+floor(($_safe_width*$_status_0)/$_status_sum);
	else
		$_width_0 = 0;
	if ($_status_m255 > 0) 
		$_width_m255 = 10+floor(($_safe_width*$_status_m255)/$_status_sum);
	else
		$_width_m255 = 0;
	
	// Aggiungo 5 px alla fascia piu' a sinistra
	if ($_width_255 > 0) $_width_255 += 5;
	else if ($_width_2 > 0) $_width_2 += 5;
	else if ($_width_1 > 0) $_width_1 += 5;
	else if ($_width_0 > 0) $_width_0 += 5;
	else if ($_width_m255 > 0) $_width_m255 += 5;
	
	// Aggiungo 5 px alla fascia piu' a destra
	if ($_width_m255 > 0) $_width_m255 += 5;
	else if ($_width_0 > 0) $_width_0 += 5;
	else if ($_width_1 > 0) $_width_1 += 5;
	else if ($_width_2 > 0) $_width_2 += 5;
	else if ($_width_255 > 0) $_width_255 += 5;
		
	$_width_sum = $_width_255+$_width_2+$_width_1+$_width_0+$_width_m255;
	$_width_remainder = $width-$_width_sum;
	// Distribuisco l'eventuale resto sulle fasce
	if ($_width_remainder > 0) {
		if ($_width_remainder % 2) {
			if ($_width_255 > 0) $_width_255++;
			else if ($_width_m255 > 0) $_width_m255++;
			else if ($_width_2 > 0) $_width_2++;
			else if ($_width_0 > 0) $_width_0++;
			else if ($_width_1 > 0) $_width_1++;
			else $_width_remainder++;
			$_width_remainder--;
		}
		while ($_width_remainder > 0)
		{
			if ($_width_remainder % 2) {
				if ($_width_255 > 0) $_width_255++;
				else if ($_width_2 > 0) $_width_2++;
				else if ($_width_1 > 0) $_width_1++;
				else if ($_width_0 > 0) $_width_0++;
				else if ($_width_m255 > 0) $_width_m255++;
			}
			else
			{
				if ($_width_0 > 0) $_width_0++;
				else if ($_width_1 > 0) $_width_1++;
				else if ($_width_2 > 0) $_width_2++;
				else if ($_width_255 > 0) $_width_255++;
				else if ($_width_m255 > 0) $_width_m255++;
			}
			$_width_remainder--;
		}
	}	
	
	$_offset = 0;

	// Fasce colorate del pillolone
	if ($_width_255 > 0)
	{
		$_html .= codeChr(1,5).'<div class="item_panel_pill_color" style="left:'.($x+$_offset).'px;top:'.($y+6).'px;width:'.$_width_255.'px;background-color: #333333;"></div>';
		if ($showValues === true) $_html .= codeChr(1,5).'<p class="item_panel_pill_number" style="left:'.($x+$_offset).'px;top:'.($y+10).'px;width:'.$_width_255.'px;">'.$_status_255.'</p>';
		$_offset += $_width_255;
	}
	if ($_width_2 > 0)
	{
		$_html .= codeChr(1,5).'<div class="item_panel_pill_color" style="left:'.($x+$_offset).'px;top:'.($y+6).'px;width:'.$_width_2.'px;background-color: red;"></div>';
		if ($showValues === true) $_html .= codeChr(1,5).'<p class="item_panel_pill_number" style="left:'.($x+$_offset).'px;top:'.($y+10).'px;width:'.$_width_2.'px;">'.$_status_2.'</p>';
		$_offset += $_width_2;
	}
	if ($_width_1 > 0)
	{
		$_html .= codeChr(1,5).'<div class="item_panel_pill_color" style="left:'.($x+$_offset).'px;top:'.($y+6).'px;width:'.$_width_1.'px;background-color: #cda400;"></div>';
		if ($showValues === true) $_html .= codeChr(1,5).'<p class="item_panel_pill_number" style="left:'.($x+$_offset).'px;top:'.($y+10).'px;width:'.$_width_1.'px;">'.$_status_1.'</p>';
		$_offset += $_width_1;
	}
	if ($_width_0 > 0)
	{
		$_html .= codeChr(1,5).'<div class="item_panel_pill_color" style="left:'.($x+$_offset).'px;top:'.($y+6).'px;width:'.$_width_0.'px;background-color: green;"></div>';
		if ($showValues === true) $_html .= codeChr(1,5).'<p class="item_panel_pill_number" style="left:'.($x+$_offset).'px;top:'.($y+10).'px;width:'.$_width_0.'px;">'.$_status_0.'</p>';
		$_offset += $_width_0;
	}
	if ($_width_m255 > 0)
	{
		$_html .= codeChr(1,5).'<div class="item_panel_pill_color" style="left:'.($x+$_offset).'px;top:'.($y+6).'px;width:'.$_width_m255.'px;background-color: gray;"></div>';
		if ($showValues === true) $_html .= codeChr(1,5).'<p class="item_panel_pill_number" style="left:'.($x+$_offset).'px;top:'.($y+10).'px;width:'.$_width_m255.'px;">'.$_status_m255.'</p>';
		$_offset += $_width_m255;
	}
	
	if ($_width_255 == 0 && $_width_2 == 0 && $_width_1 == 0 && $_width_0 == 0 && $_width_m255 == 0)
	{
		$_txt = 'Nessun dato disponibile';
		$_html .= codeChr(1,5).'<p class="item_panel_pill_number" style="left:'.($x).'px;top:'.($y+10).'px;width:'.$width.'px;">'.$_txt.'</p>';
	}
	}
	// oldValues = true (dati non aggiornati)
	else{
		$_txt = 'Dati non aggiornati';
		$_html .= codeChr(1,5).'<p class="item_panel_pill_number" style="left:'.($x).'px;top:'.($y+10).'px;width:'.$width.'px;">'.$_txt.'</p>';
	}
	
	// Forma del pillolone
	$_html .= codeChr(1,5).htmlImage(null,'item_panel_pill','sk_panel_pill_left.png',"default",'left:'.($x).'px;top:'.($y+0).'px;width:'.$_pill_left_width.'px;')
		     .codeChr(1,5).htmlImage(null,'item_panel_pill','sk_panel_pill_bg.png',"default",'left:'.($x+$_pill_left_width).'px;top:'.($y+0).'px;width:'.$_pill_bg_width.'px;')
		     .codeChr(1,5).htmlImage(null,'item_panel_pill','sk_panel_pill_right.png',"default",'left:'.($x+$_pill_left_width+$_pill_bg_width).'px;top:'.($y+0).'px;width:'.$_pill_right_width.'px;');
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un pannello item
*/
function htmlPanelBuild($category,$i=0,$id,$type,$name,$statusCode,$pillData)
{
	global $_configuration;
	
	$_html = codeInit();
	
	$_offset_x = 20;
	$_offset_y = 15;
	
	$_c = $i%14;
	$_r = (int)floor($i/14);
	
	$_icon_x = $_offset_x + ($_c * 70);
	$_icon_y = $_offset_y + ($_r * 80);
	
	$_panel_x = $_icon_x - 10;
	$_panel_y = $_icon_y - 10;
	
	$_item_name = $name;
	$_type_code = $type;
		
	$_layer_down = '';
	$_layer_up = '';
	
	$_layer_down = codeChr(1,5).htmlImage('item_panel_icon_'.$id.'_status_down','item_icon_status_down','gui/sk_icon_50_'.$statusCode.'_down.png',"default");
	$_layer_up = codeChr(1,5).htmlImage('item_panel_icon_'.$id.'_status_up','item_icon_status_up','gui/sk_icon_50_'.$statusCode.'_up.png',"default");
		
	if ($category == "station")
	{
		$_station_type_list = $_configuration["station_type_list"];
		$_type_name = getStationTypeNameFromCode($_station_type_list,$_type_code);
	}
	else if ($category == "building")
	{
		$_type_name = codeInit();
	}
	else if ($category == "rack")
	{
		$_rack_type_list = $_configuration["rack_type_list"];
		$_type_name = getRackTypeNameFromCode($_rack_type_list,$_type_code);
	}
	else if ($category == "device")
	{
		$_type_name = $type;
		$_html_pill = htmlPillBuild( 10, 60, 200, $pillData, true, ($statusCode==254)?true:false );
	}
	else
	{
		$_type_name = codeInit();
		$_html_pill = codeInit();
	}
	
	$_html .= codeChr(1,4).'<div id="item_panel_'.$id.'" class="item" style="left:'.$_panel_x.'px;top:'.$_panel_y.'px;">'
				.codeChr(1,5).htmlImage(null,'item_panel_top_left','sk_panel_top_left.png',"default")
				.codeChr(1,5).htmlImage(null,'item_panel_top','sk_panel_top.png',"default")
				.codeChr(1,5).htmlImage(null,'item_panel_top_right','sk_panel_top_right.png',"default")
				.codeChr(1,5).htmlImage(null,'item_panel_left','sk_panel_left.png',"default")
				.codeChr(1,5).'<!--img id="item_panel_bg_'.$id.'" class="item_panel_bg" src="images/sk_panel_bg.png"-->'
				.codeChr(1,5).'<div class="item_panel_bg">&nbsp;</div>'
				.codeChr(1,5).htmlImage(null,'item_panel_right','sk_panel_right.png',"default")
				.codeChr(1,5).htmlImage(null,'item_panel_bottom_left','sk_panel_bottom_left.png',"default")
				.codeChr(1,5).htmlImage(null,'item_panel_bottom','sk_panel_bottom.png',"default")
				.codeChr(1,5).htmlImage(null,'item_panel_bottom_right','sk_panel_bottom_right.png',"default")
				.codeChr(1,5).'<p class="item_panel_name">'.$_item_name.'</p>'
				.codeChr(1,5).'<p class="item_panel_type">'.$_type_name.'</p>'
				.codeChr(1,5).'<p id="item_panel_'.$id.'_status" class="item_panel_status">IN FUNZIONE</p>'
				.$_html_pill
				.$_layer_down
				.codeChr(1,5).htmlImage('item_panel_icon_'.$id,'item_icon',$category.'s/'.$_type_code.'_50.png',"default")
				.$_layer_up
				.codeChr(1,5).'<p class="item_name">'.$_item_name.'</p>'
			.codeChr(1,4).'</div>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un balloonn di informazioni
*/
function htmlBalloonBuild($id,$class,$hpos,$top,$width,$text,$left=true,$skin="default")
{
	$_html = codeInit();
	
	$_x = $left;
	$_y = $top;
	
	$_balloon_text = $text;
	
	$_hpos_offset = (int)floor(($width-30)/2);
	if ($hpos <= 10)
		$_hpos_offset = 10;
	if (($hpos-$_hpos_offset) < 0)
		$_hpos = 0;
	else
		$_hpos = $hpos-$_hpos_offset;
	/*
	if ($_hpos_offset > 10 || $_hpos_offset < 10)
		$_hpos_offset = 10;
	*/	
	
	
	$_top = $top-29;
	
	$_style_center = 'width: '.($width-20).'px;';
	
	if ($left === true)
	{
		$_html_hpos = 'left: '.$_hpos.'px; top: '.$_top.'px; width: '.$width.'px;';
		$_style_arrow = 'left: '.($hpos-$_hpos+10).'px;';
	}
	else
	{
		$_html_hpos = 'right: '.$_hpos.'px; top: '.$_top.'px; width: '.$width.'px;';
		$_style_arrow = 'right: '.($hpos-$_hpos+10).'px;';
	}
	
	$_style_text = 'width: '.($width-10).'px;';
	
	$_inner_html = 
		 codeChr(1,5).htmlImage(null,'balloon_left','gui/sk_balloon_left.png',$skin)
		.codeChr(1,5).htmlImage(null,'balloon_center','gui/sk_balloon_center.png',$skin,$_style_center)
		.codeChr(1,5).htmlImage(null,'balloon_arrow','gui/sk_balloon_arrow.png',$skin,$_style_arrow)
		.codeChr(1,5).htmlImage(null,'balloon_right','gui/sk_balloon_right.png',$skin)
		.codeChr(1,5).htmlParagraph(null,'balloon_text',$_balloon_text,$_style_text)
		;
	
	$_html .= codeChr(1,4).htmlDivitis("balloon_".$id,"balloon",$_inner_html,$_html_hpos,null);
	
	return($_html);
}

/**
* Funzione per generare l'HTML per un pannello di dettagli item
*/
function htmlDetailsPanelBuild($id,$iconUrl,$innerHtml,$panelHeight=500,$infoPanelHeight=120)
{
	$_html = codeInit();
	
	$_html .= codeChr(1,4).'<div id="'.$id.'" class="details_panel" style="height: '.($panelHeight).'px;">'
				.htmlDivitis("icon_status_ack_panel_sub_layer","icon_status_ack_panel_sub_layer","",null,null)
				//.htmlDivitis("details_rack_sub_layer","rack_sub_layer","",null,null)
				// Pannello
				.codeChr(1,5).htmlImage(null,'details_panel_top_left','sk_panel_top_left.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_top','sk_panel_top.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_top_right','sk_panel_top_right.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_left','sk_panel_left.png',"default",'height: '.($panelHeight-20).'px;')
				.codeChr(1,5).'<div class="details_panel_bg" style="height: '.($panelHeight-20).'px;"></div>'
				.codeChr(1,5).htmlImage(null,'details_panel_right','sk_panel_right.png',"default",'height: '.($panelHeight-20).'px;')
				.codeChr(1,5).htmlImage(null,'details_panel_bottom_left','sk_panel_bottom_left.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_bottom','sk_panel_bottom.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_bottom_right','sk_panel_bottom_right.png',"default")
				// Bottone chiudi
				.htmlPanelButton("details","close","details_panel_button",-10,-10,'close',null,false)
				
				// Icona periferica 100x100
				.codeChr(1,5).htmlImage('details_panel_icon','details_panel_icon',$iconUrl,'default')
				
				// Taglio in alto a SX
				.codeChr(1,5).htmlPanelVerticalCut('left',115,5, $infoPanelHeight-10)
				// Taglio in alto a DS
				.codeChr(1,5).htmlPanelVerticalCut('right',115,5, $infoPanelHeight-10)
				
				// Luce orizzontale
				.codeChr(1,5).htmlImage(null,'details_panel_hlight','sk_details_hlight.png','default','top: '.($infoPanelHeight-45).'px;')
				// Ombra orizzontale
				.codeChr(1,5).htmlImage(null,'details_panel_hshadow','sk_panel_hshadow.png','default','top: '.($infoPanelHeight-5).'px;')
				
				.$innerHtml
				
				//.codeChr(1,5).'<p class="item_name">'.codeStrOverflow($_type_name,0,15,$append="&hellip;").'</p>'
			.codeChr(1,4).'</div>';
				
	return($_html);
}

/**
* Funzione per generare l'HTML per un pannello form x aggiungere item
*/
function htmlAddPanelBuild($id,$iconUrl,$innerHtml,$panelHeight=200)
{
	$_html = codeInit();
	
	$_html .= codeChr(1,4).'<div id="'.$id.'" class="details_panel" style="height: '.($panelHeight).'px;">'
				//.htmlDivitis("add_rack_sub_layer","rack_sub_layer","",null,null)
				// Pannello
				.codeChr(1,5).htmlImage(null,'details_panel_top_left','sk_panel_top_left.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_top','sk_panel_top.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_top_right','sk_panel_top_right.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_left','sk_panel_left.png',"default",'height: '.($panelHeight-20).'px;')
				.codeChr(1,5).'<div class="details_panel_bg" style="height: '.($panelHeight-20).'px;"></div>'
				.codeChr(1,5).htmlImage(null,'details_panel_right','sk_panel_right.png',"default",'height: '.($panelHeight-20).'px;')
				.codeChr(1,5).htmlImage(null,'details_panel_bottom_left','sk_panel_bottom_left.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_bottom','sk_panel_bottom.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_bottom_right','sk_panel_bottom_right.png',"default")
				// Bottone chiudi
				.htmlPanelButton("add","close","add_panel_button",-10,-10,'close',null,false)
							
				// Icona periferica 100x100
				.codeChr(1,5).htmlImage('details_panel_icon','details_panel_icon',$iconUrl,'default')
				
				// Taglio in alto a sx
				.codeChr(1,5).htmlPanelVerticalCut('left',115,5, $panelHeight-10)
				// Taglio in alto a dx
				.codeChr(1,5).htmlPanelVerticalCut('right',115,5, $panelHeight-10)
				
				.$innerHtml
				
				//.codeChr(1,5).'<p class="item_name">'.codeStrOverflow($_type_name,0,15,$append="&hellip;").'</p>'
			.codeChr(1,4).'</div>';
				
	return($_html);
}

/**
* Funzione per generare l'HTML per un pannello fisso
*/
function htmlFixedPanelBuild($id,$iconUrl,$innerHtml,$panelHeight=200)
{
	$_html = codeInit();
	
	$_html .= codeChr(1,4).'<div id="'.$id.'" class="fixed_details_panel" style="height: '.($panelHeight).'px;">'
				// Pannello
				.codeChr(1,5).htmlImage(null,'details_panel_top_left','sk_panel_top_left.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_top','sk_panel_top.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_top_right','sk_panel_top_right.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_left','sk_panel_left.png',"default",'height: '.($panelHeight-20).'px;')
				.codeChr(1,5).'<div class="details_panel_bg" style="height: '.($panelHeight-20).'px;"></div>'
				.codeChr(1,5).htmlImage(null,'details_panel_right','sk_panel_right.png',"default",'height: '.($panelHeight-20).'px;')
				.codeChr(1,5).htmlImage(null,'details_panel_bottom_left','sk_panel_bottom_left.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_bottom','sk_panel_bottom.png',"default")
				.codeChr(1,5).htmlImage(null,'details_panel_bottom_right','sk_panel_bottom_right.png',"default")
							
				// Icona periferica 100x100
				.codeChr(1,5).htmlImage('details_panel_icon','details_panel_icon',$iconUrl,'default')
				
				// Taglio in alto a sx
				.codeChr(1,5).htmlPanelVerticalCut('left',115,5, 100)
				// Taglio in alto a dx
				.codeChr(1,5).htmlPanelVerticalCut('right',115,5, 100)
				
				.$innerHtml
				
				//.codeChr(1,5).'<p class="item_name">'.codeStrOverflow($_type_name,0,15,$append="&hellip;").'</p>'
			.codeChr(1,4).'</div>';
				
	return($_html);
}

/**
* Funzione per generare un intestazione per codice HTML
*/
function htmlBuildCodeHeader($title=null)
{
	global $_conf_console_app_name;
	
	$_html = codeInit();
	
	if ($title === null) $_html_title = " - Codice generato automaticamente.";
	else $_html_title = " - ".$title;
	
	$_html .= "<!--"
		.codeChr(1,0)."* ".$_conf_console_app_name
		.codeChr(1,0)."* "
		.codeChr(1,0)."* ".$_SERVER["SCRIPT_NAME"].$_html_title
		.codeChr(1,0)."* "
		.codeChr(1,0)."* @version ".getVersion()
		.codeChr(1,0)."* @copyright 2011 Telefin S.p.A."
		.codeChr(1,0)."-->"
		.codeChr(1,0);
		
	return($_html);
}

/**
* Funzione per costruire una stringa commento in formato HTML
*/
function htmlBuildComment($text)
{
	$_html  = codeInit();
	
	$_html .= codeChr(1,0)."<!-- ".$text." -->";
	
	return($_html);
}

/**
* Funzione per generare l'head HTML
*/
function htmlHeadBuild($title)
{
	global $_configuration;
	
	$_stations = $_configuration["stations"];

	$_html = codeInit();
	
	$_sk_name = "";
	if (isset($_stations) && count($_stations)>0)
	{
		$_station = $_stations[0];
		if (isset($_station))
		{
			$_sk_name = $_station->name." - ";
		}
	}
	
	$_html .= codeChr(1,1).'<head>'
				.codeChr(1,2).'<title>'.$_sk_name.$title.'</title>'
				.codeChr(1,2).'<meta http-equiv="Content-Type" content="text/html;charset=utf-8">'
				//.codeChr(1,2).'<meta name="apple-mobile-web-app-capable" content="yes">'
				//.codeChr(1,2).'<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">'
				//.codeChr(1,2).'<link rel="apple-touch-icon" href="images/sk_topbar_bg.png"/>'
				//.codeChr(1,2).'<link rel="apple-touch-startup-image" href="images/sk_splash_tall.png" />'
				//.codeChr(1,2).'<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">'
				.codeChr(1,2).'<link rel="stylesheet" href="css/sk_main.css.php" type="text/css">'
				.codeChr(1,2).'<link rel="stylesheet" href="css/custom+theme/jquery-ui-1.8.16.custom.css" type="text/css">'
				.codeChr(1,2).'<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>'
				.codeChr(1,2).'<script src="js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>'
				.codeChr(1,2).'<script src="js/jquery.mousewheel.min.js" type="text/javascript"></script>'
				.codeChr(1,2).'<script src="js/sk_main.js.php" type="text/javascript"></script>'
				.codeChr(1,2).'<script src="js/fileuploader.js" type="text/javascript"></script>'
			.codeChr(1,1).'</head>';
	
	return($_html);
}

/**
* Funzione per generare il codice HTML per un bottone di selezione pagina (su topbar)
*/
function htmlTopBarPageButtonBuild($index,$page,$selected=false,$profile=null)
{
	$_html = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if (isset($page)){
		$_html_button = codeInit();
		
		if ($selected == true){
			$_html_button .= codeChr(1,5).htmlImage( 'topbar_page_button_'.$page.'_selected', 'topbar_page_button_selected', 'sk_topbar_page_button_selected.png', 'default' );
		}
		$_html_button .= codeChr(1,5).htmlImage( 'topbar_page_button_'.$page.'_image', 'topbar_page_button_image', 'sk_topbar_page_button_icon_'.$page.'.png', 'default' );
		$_html_button .= codeChr(1,5).htmlParagraph( 'topbar_page_button_'.$page.'_title', 'topbar_page_button_title', langGetString("gui","sk_topbar_".$page."_button_title") );		
		if ($selected == false){
			$_html_button .= codeChr(1,5).htmlImage( 'topbar_page_button_'.$page.'_over', 'topbar_page_button_over', 'sk_topbar_page_button_over.png', 'default' );
			$_html_button .= codeChr(1,5).htmlImage( 'topbar_page_button_'.$page.'_click', 'topbar_page_button_click', 'sk_topbar_page_button_click.png', 'default' );
		}
	
		$_right = 10+($index*60);
					
		$_html .= codeChr(1,4).htmlDivitis( 'topbar_page_button_'.$page, 'topbar_page_button', $_html_button, 'right:'.$_right.'px;');
	}
				
	return($_html);
}

/**
* Funzione per generare la barra superiore della pagina
*/
function htmlTopBarBuild($htmlMenu)
{
	global $_pages;
	global $_page;
	global $_conf_console_app_logo;
	
	$_html = codeInit();
	$_html_topbar_pages_buttons = codeInit();
	
	$_user = authGetUser();
	
	foreach ($_pages as $_index => $_item)
	{
		$_title = $_item['title'];
		$_profile = $_item['profile'];
	
		if ($_title == $_page) $_selected = true;
		else $_selected = false;
	
		$_html_topbar_pages_buttons .= htmlTopBarPageButtonBuild( $_index, $_title, $_selected, $_profile );
	}
	
	if (isset($_user['gris_username']))
	{
		$_user_info = $_user['gris_username'].' ('.ucfirst($_user['profile']);
	}
	else
	{
		$_user_info = $_user['username'].' ('.ucfirst($_user['profile']);
	}
	
	$_html .= codeChr(1,2).'<div id="topbar">'
					// Barra nera superiore
					.codeChr(1,3).'<div id="topbar_top">'
						.codeChr(1,4).'<p id="username">'.langGetString("gui","sk_topbar_user").': '.$_user_info.')</p>'
					.codeChr(1,3).'</div>'
					// Barra centrale logo e bottoni pagine
					.codeChr(1,3).'<div id="topbar_middle">'
						.codeChr(1,4).htmlImage('topbar_middle_left',null,'sk_topbar_left.png',"default")
						.codeChr(1,4).htmlImage('topbar_middle_bg',null,'sk_topbar_bg.png',"default")
						.codeChr(1,4).htmlImage('topbar_middle_right',null,'sk_topbar_right.png',"default")
						.codeChr(1,4).htmlImage('logo',null,'sk_logo_rfi.png',"default")
						.codeChr(1,4).htmlImage('app_name',null,'sk_app_name_'.$_conf_console_app_logo.'.png',"default")
						.$_html_topbar_pages_buttons
					.codeChr(1,3).'</div>'
					// Barra menu
					.codeChr(1,3).'<div id="topbar_bottom">'
						.codeChr(1,4).htmlImage('navbar_left',null,'sk_navbar_left.png',"default")
						.codeChr(1,4).htmlImage('navbar_bg',null,'sk_navbar_bg.png',"default")
						.codeChr(1,4).htmlImage('navbar_right',null,'sk_navbar_right.png',"default")
						.$htmlMenu
					.codeChr(1,3).'</div>'
					// Area messaggi superiore
					.codeChr(1,3).'<div id="topbar_message">'
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_left.png',"default",'top: 0px; left: 0px; width: 10px; height: 30px;')
						.codeChr(1,4).'<div id="topbar_message_p" class="message_panel" style="top: 0px; left: 10px; width: 900px; height: 30px; background-color: #313131;"></div>'
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_right.png',"default",'top: 0px; right: 0px; width: 10px; height: 30px;')
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_bottom_left.png',"default",'top: 30px; left: 0px; width: 10px; height: 10px;')
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_bottom.png',"default",'top: 30px; left: 10px; width: 900px; height: 10px;')
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_bottom_right.png',"default",'top: 30px; right: 0px; width: 10px; height: 10px;')
					.codeChr(1,3).'</div>'
					// Ombra barra superiore
					.codeChr(1,3).'<div id="topbar_shadow">'
						.codeChr(1,4).htmlImage('topbar_bg',null,'sk_topbar_shadow.png',"default")
					.codeChr(1,3).'</div>'
					
				.codeChr(1,2).'</div>';
	
	return($_html);
}

/**
* Funzione per generare il contenuto della pagina
*/
function htmlContentBuild($htmlContent,$htmlDetails,$htmlAdd="")
{
	$_html = codeInit();
	
	$_html .= 	codeChr(1,2).htmlDivitis( "content", null,
					 codeChr(1,3).htmlDivitis( 'content_middle', null, $htmlContent )
					.codeChr(1,3).htmlDivitis( 'content_details', null, $htmlDetails )
					.codeChr(1,3).htmlDivitis( 'content_add', null, $htmlAdd )
					.htmlDialog('content','','')
				)
				.codeChr(1,2).htmlDivitis( "waiting", null,
					 codeChr(1,3).htmlImage('waiting_bg',null,'gui/sk_waiting_bg.png',"default")
					.codeChr(1,3).htmlImage('waiting_0',"waiting_icon",'gui/sk_waiting_01.png',"default")
					.codeChr(1,3).htmlImage('waiting_1',"waiting_icon",'gui/sk_waiting_02.png',"default")
					.codeChr(1,3).htmlImage('waiting_2',"waiting_icon",'gui/sk_waiting_03.png',"default")
					.codeChr(1,3).htmlImage('waiting_3',"waiting_icon",'gui/sk_waiting_04.png',"default")
					.codeChr(1,3).htmlImage('waiting_4',"waiting_icon",'gui/sk_waiting_05.png',"default")
					.codeChr(1,3).htmlImage('waiting_5',"waiting_icon",'gui/sk_waiting_06.png',"default")
					.codeChr(1,3).htmlImage('waiting_6',"waiting_icon",'gui/sk_waiting_07.png',"default")
					.codeChr(1,3).htmlImage('waiting_7',"waiting_icon",'gui/sk_waiting_08.png',"default")
					.codeChr(1,3).htmlImage('waiting_8',"waiting_icon",'gui/sk_waiting_09.png',"default")
					.codeChr(1,3).htmlImage('waiting_9',"waiting_icon",'gui/sk_waiting_10.png',"default")
					.codeChr(1,3).htmlImage('waiting_10',"waiting_icon",'gui/sk_waiting_11.png',"default")
					.codeChr(1,3).htmlImage('waiting_11',"waiting_icon",'gui/sk_waiting_12.png',"default")
					.codeChr(1,3).htmlParagraph('waiting_text','waiting_text','')
				)
				;
	
	return($_html);
}

/**
* Funzione per generare la barra inferiore della pagina
*/
function htmlBottomBarBuild()
{
	global $_conf_console_app_copyright, $_conf_console_app_vendor, $_conf_console_app_bottombar_fbwf_icon_visible;
	global $_configuration;
	global $_edit_level;
	
	$_info = $_configuration["info"];
	$_servers = $_configuration["servers"];
	$_stations = $_configuration["stations"];
	
	if (isset($_info["sk_hostname"]) && $_info["sk_hostname"] != "")
	{
		$_sk_hostname = $_info["sk_hostname"];
	}
	else
	{
		$_sk_hostname = "";
	}
	if (isset($_info["sk_version"]) && $_info["sk_version"] != "")
	{
		$_sk_version = langGetString("gui","sk_bottombar_sk_version")." ".$_info["sk_version"];
	}
	else
	{
		$_sk_version = "";
	}
	if ($_edit_level == 1 || $_edit_level == 2)
	{
		$_apply_on = true;
	}
	else
	{
		$_apply_on = false;
	}
	if ($_info["sk_mode"] == "MAINTENANCE")
	{
		$_maintenance_on = true;
	}
	else
	{
		$_maintenance_on = false;
	}
	if ($_info["services_status"] == "0")
	{
		$_services_on = true;
	}
	else
	{
		$_services_on = false;
	}
	if (empty($_info["sk_fbwf"]) || $_info["sk_fbwf"] === true)
	{
		$_fbwf_on = false;
	}
	else
	{
		$_fbwf_on = true;
	}
	$_sk_name = "";
	if (isset($_stations) && count($_stations)>0)
	{
		$_station = $_stations[0];
		if (isset($_station))
		{
			$_sk_name = $_station->name;
		}
	}

	$_html = codeInit();
	
	$_html .= codeChr(1,2).'<div id="bottombar">'
					// Area messaggi inferiore
					.codeChr(1,3).'<div id="bottombar_message">'
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_top_left.png',"default",'top: 0px; left: 0px; width: 10px; height: 10px;')
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_top.png',"default",'top: 0px; left: 10px; width: 900px; height: 10px;')
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_top_right.png',"default",'top: 0px; right: 0px; width: 10px; height: 10px;')
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_left.png',"default",'top: 10px; left: 0px; width: 10px; height: 30px;')
						.codeChr(1,4).'<div id="bottombar_message_p" class="message_panel" style="top: 10px; left: 10px; width: 900px; height: 30px; background-color: #313131;"></div>'
						.codeChr(1,4).htmlImage(null,'message_panel','sk_panel_right.png',"default",'top: 10px; right: 0px; width: 10px; height: 30px;')
					.codeChr(1,3).'</div>'
					.codeChr(1,3).'<div id="bottombar_top">'
						.codeChr(1,4).htmlImage('navbar_left',null,'sk_bottombar_left.png','default')
						.codeChr(1,4).htmlImage('navbar_bg',null,'sk_navbar_bg.png','default')
						.htmlNavbarIcon("bottombar","sk",10,"sk",null,true,'default',null,true)
						.htmlNavbarIcon("bottombar","services",40,"services",null,true,'default',null,true,$_services_on)
						.(($_conf_console_app_bottombar_fbwf_icon_visible!==false)?htmlNavbarIcon("bottombar","fbwf",70,"fbwf",null,true,'default',"admin",true,$_fbwf_on):'')
						.htmlNavbarText("bottombar","hostname",130,150,$_sk_hostname,null,true,'default',null)
						.htmlNavbarText("bottombar","version",280,100,$_sk_version,null,true,'default',null)
						.htmlNavbarText("bottombar","skname",375,240,$_sk_name,"text-align: center;",true,'default',null)
						.htmlNavbarButton("bottombar","maintenance",10,"maintenance",null,false,'default',"IaP_installer,IeC_installer,installer,admin",true,$_maintenance_on,false,"Imposta modalita'")
						.htmlNavbarButton("bottombar","reload",40,"cancel",null,false,'default',"IaP_installer,IeC_installer,installer,admin", false, false, false, "Ricarica configurazione attiva")
						.htmlNavbarButton("bottombar","apply",70,"apply",null,false,'default',"IaP_installer,IeC_installer,installer,admin",true,$_apply_on, false, "Applica configurazione")
						.htmlNavbarButton("bottombar","revert",100,"revert",null,false,'default',"admin",true, false, false, "Recupera configurazione precedente")
						.htmlNavbarButton("bottombar","download",130,"download",null,false,'default',"admin",true,false,false, "Scarica la configurazione in uso")
						.htmlNavbarButton("bottombar","upload",160,"upload",null,false,'default',"admin",true,false,true, "Invia un file di configurazione")
												
						/* Temperatura CPU
						.htmlNavbarIcon("bottombar","temp_cpu",270,"temp",null,true,'default',null)
						.htmlNavbarText("bottombar","temp_cpu",300,100,"MB 47&deg;C",null,true,'default',null)
						//*/
						/* Temperatura MB
						.htmlNavbarIcon("bottombar","temp_mb",270,"temp",null,true,'default',"admin")
						.htmlNavbarText("bottombar","temp_mb",300,100,"MB: 68&deg;C",null,true,'default',"admin")
						//*/
						/* Ventole
						.htmlNavbarIcon("bottombar","fan_1",400,"fan",null,true,'default',"admin")
						.htmlNavbarText("bottombar","fan_1",430,100,"Fan 1: 0%",null,true,'default',"admin")
						.htmlNavbarIcon("bottombar","fan_2",530,"fan",null,true,'default',"admin")
						.htmlNavbarText("bottombar","fan_2",560,100,"Fan 2: 0%",null,true,'default',"admin")
						//*/
						.codeChr(1,4).htmlImage('wheel_0','bottombar_wheel','sk_spinner_0.png','default')
						.codeChr(1,4).htmlImage('wheel_1','bottombar_wheel','sk_spinner_1.png','default')
						.codeChr(1,4).htmlImage('wheel_2','bottombar_wheel','sk_spinner_2.png','default')
						.codeChr(1,4).htmlImage('wheel_3','bottombar_wheel','sk_spinner_3.png','default')
						.codeChr(1,4).htmlImage('wheel_4','bottombar_wheel','sk_spinner_4.png','default')
						.codeChr(1,4).htmlImage('wheel_5','bottombar_wheel','sk_spinner_5.png','default')
						.codeChr(1,4).htmlImage('wheel_6','bottombar_wheel','sk_spinner_6.png','default')
						.codeChr(1,4).htmlImage('wheel_7','bottombar_wheel','sk_spinner_7.png','default')
						.codeChr(1,4).htmlImage('wheel_8','bottombar_wheel','sk_spinner_8.png','default')
						.codeChr(1,4).htmlImage('wheel_9','bottombar_wheel','sk_spinner_9.png','default')
						.codeChr(1,4).htmlImage('wheel_10','bottombar_wheel','sk_spinner_10.png','default')
						.codeChr(1,4).htmlImage('wheel_11','bottombar_wheel','sk_spinner_11.png','default')
						.codeChr(1,4).htmlImage('navbar_right',null,'sk_bottombar_right.png','default')
						.codeChr(1,4).htmlParagraph('log','bottombar_log','LOG: ')
					.codeChr(1,3).'</div>'
					.codeChr(1,3).'<div id="bottombar_bottom">'
						.codeChr(1,4).'<p id="app_version">'.langGetString("gui","sk_bottombar_version").' '.getVersion().'</p>'
						.codeChr(1,4).'<p id="app_copyright">'.langGetString("gui","sk_bottombar_copyright").' '.$_conf_console_app_copyright.'</p>'
						.codeChr(1,4).'<p id="app_credits">'.langGetString("gui","sk_bottombar_credits").' '.$_conf_console_app_vendor.'</p>'
					.codeChr(1,3).'</div>'
				.codeChr(1,2).'</div>';
	
	return($_html);
}

/**
* Funzione per generare il body HTML
*/
function htmlBodyBuild($htmlMenu,$jsMenu,$htmlContent,$jsContent)
{
	global $_configuration;
	global $_conf_console_html_color_code;

	$_html = codeInit();
	$_html_add = codeInit();
	
	$_page = coreGetPageFromSession();
	$_level = coreGetLevelFromSession();
	
	$_data = array();
	
	$_data['level'] = $_level;
	
	//$_html_add = devicesPageBuildAddPanelHTML($_data);
	$_html_add = corePageBuildAddPanels($_page,$_level);
	
	//$_html_details = devicesPageBuildDetailsPanelHTML($_data);
	$_html_details = corePageBuildDetailsPanels($_page,$_level);
	
	$_page_ajax_workers_prefix = coreGetPageAjaxWorkersPrefix();
	
	$_onload_js = 'ajax'.$_page_ajax_workers_prefix.'InfoSender();ajax'.$_page_ajax_workers_prefix.'DetailsSender();ajaxStatusSender();ajaxServiceStatusSender();ajaxInfoSender();ajaxNetworkSender();bottombar_wheelAnimate();waitingAnimate();createUploader();';
	
	$_html .= codeChr(1,1).'<body ontouchmove="BlockMove(event);" onload="'.$_onload_js.'" onbeforeunload="return skcCloseCheck();">'
				.htmlTopBarBuild($htmlMenu)
				.htmlContentBuild($htmlContent,$_html_details,$_html_add)
				.htmlBottomBarBuild()
				.$jsContent
				.$jsMenu
			.codeChr(1,1).'</body>';
	
	return($_html);
}

/**
* Funzione per generare tutta la struttura HTML
*/
function htmlBuild($page,$pageHtml)
{
	global $_conf_console_app_name;

	$_html = codeInit();
	
	$_html .= '<!DOCTYPE html>'
				.codeChr(1,0).'<html>'
				.htmlHeadBuild($_conf_console_app_name)
				.$pageHtml
				.codeChr(1,0).'</html>';
	
	return($_html);
}
?>
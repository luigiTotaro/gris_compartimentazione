<?php
/**
* Telefin STLC1000 Consolle
*
* lib_lang.php - Libreria gestione lingua.
*
* @author Enrico Alborali
* @version 0.0.1 14/11/2011
* @copyright 2011 Telefin S.p.A.
*/

/**
* Funzione per ottenere una stringa in lingua
*/
function langGetString($res,$id,$params=null)
{
	global $_res;
	
	$_string = "n/a";
	
	$_strings = $_res[$res];
	$_string = $_strings[$id];
	
	if ($params != null) $_string = langApplyStringParams($_string, $params);
	
	return($_string);
}

/**
* Funzione per applicare i parametri per la formattazione di una stringa in lingua
*/
function langApplyStringParams($format,$params)
{
	$_string = codeInit();
	
	if (count($params) == 0)
		$_string = $format;
	else if (count($params) == 1)
		$_string = sprintf( $format, $params[0] );
	else if (count($params) == 2)
		$_string = sprintf( $format, $params[0], $params[1] );
	else if (count($params) == 3)
		$_string = sprintf( $format, $params[0], $params[1], $params[2] );
	else if (count($params) == 4)
		$_string = sprintf( $format, $params[0], $params[1], $params[2], $params[3] );
	else if (count($params) == 5)
		$_string = sprintf( $format, $params[0], $params[1], $params[2], $params[3], $params[4] );
		
	return($_string);
}


?>
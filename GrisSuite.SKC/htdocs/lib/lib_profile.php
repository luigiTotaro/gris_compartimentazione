<?php
/**
* Telefin STLC1000 Consolle
*
* lib_profile.php - Libreria per la gestione dei profili
*
* @author Davide Ferraretto
* @version 1.0.2.0 16/02/2016
* @copyright 2011-2012 Telefin S.p.A.
*/

require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_class_profile.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_class_activity.php');

$_profiles = array();

/**
* Funzione per aggiungere un profilo
*/
function addProfile($id,$name)
{
	global $_profiles;
	
	$profile = new Profile();
	
	$profile->id = $id;
	$profile->name = $name;
	$profile->activityList = Array();
	
	$_profiles[$id] = $profile;
	
	return($profile);
}

function addActivity($idProfile, $id, $name, $class, $from, $to, $dow, $mode, $interval = null)
{
	global $_profiles;
	
	$activity = new Activity();
	$activity->id = $id;
	$activity->name = $name;
	$activity->class = $class;
	$activity->from = $from;
	$activity->to = $to;
	$activity->dow = $dow;
	$activity->mode = $mode;
	$activity->interval = $interval;
	
	if( isset($_profiles[$idProfile]) === true ){
		$_profiles[$idProfile]->activityList[$id] = $activity;
	}
	
	return($activity);
}

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* lib_log.php - Libreria per la gestione dei log dell'applicazione web.
*
* @author Enrico Alborali
* @version 0.0.1 14/07/2011
* @copyright 2011 Telefin S.p.A.
*/

/**
* Funzione per generare il campo data/ora per un evento di log.
*/
function logBuildDateTime($logType=0)
{
	// Log Type 0 = Default
	$_log_date_time = date("D M j G:i:s Y");
	
	return($_log_date_time);
}

/**
* Funzione per generare una stringa in base alla severita' di log.
*/
function logBuildSeverityString($logSeverity=0,$logType=0)
{
	// Log Type 0 = Default
	switch($logSeverity)
	{
		case 2: //Error
			$_log_severity = "error";
		break;
		case 1: //Warning
			$_log_severity = "warn";
		break;
		case 0: //Notice
		default:
			$_log_severity = "notice";
		break;
		case -1: //Debug
			$_log_severity = "debug";
		break;
	}
	
	return($_log_severity);
}

/**
* Funzione per generare il testo per una riga di evento di log
*/
function logBuildText($logMsg,$logSeverity=0,$logType=0)
{
	// Log Type 0 = Default
	$_log_date_time = logBuildDateTime($logType);
	$_log_severity = logBuildSeverityString($logSeverity,$logType);
	$_log_text = "[".$_log_date_time."] [".$_log_severity."] ".$logMsg."\n";
	
	return($_log_text);
}

/**
* Funzione per scrivere una riga di evento sul file di log.
*/
function logEvent($logMsg,$logSeverity=0,$logType=0)
{
	global $_conf_log_file;
	
	$_log_text = logBuildText($logMsg,$logSeverity,$logType);
	
	error_log($_log_text, 3, $_conf_log_file);
	
	return($_log_text);
}

/**
* Funzione per scrivere una riga di evento sul file di log solo se in modalità di debug.
*/
function logDebug($logMsg,$logType=0)
{
	global $_conf_log_debug_mode;
	
	if ($_conf_log_debug_mode == true)
	{
		$_log_text = logEvent($logMsg,-1,$logType);
	}
	else
	{
		$_log_text = null;
	}
	
	return($_log_text);
}

?>
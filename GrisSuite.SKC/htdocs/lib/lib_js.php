<?php
/**
* Telefin STLC1000 Consolle
*
* lib_js.php - Libreria per la generazione del codice Javascript.
*
* @author Enrico Alborali
* @version 1.0.4.0 07/05/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Funzione per costruire una stringa intestazione in formato JavaScript
*/
function jsBuildCodeHeader($title=null)
{
	global $_conf_console_app_name;
	global $_conf_console_app_vendor;
	
	$_js = codeInit();
	
	if ($title === null) $_js_title = " - Script generato automaticamente.";
	else $_js_title = " - ".$title;
	
	$_js .= "/**"
		.codeChr(1,0)."* ".$_conf_console_app_name
		.codeChr(1,0)."* "
		.codeChr(1,0)."* ".$_SERVER["SCRIPT_NAME"].$_js_title
		.codeChr(1,0)."* "
		.codeChr(1,0)."* @version ".getVersion()
		.codeChr(1,0)."* @copyright 2011-2012 ".$_conf_console_app_vendor
		.codeChr(1,0)."*/"
		.codeChr(1,0);
		
	return($_js);
}

/**
* Funzione per costruire un tag di apertura script
*/
function jsBuildScriptOpen($tab=2)
{
	$_js = codeInit();
	
	$_js .= codeChr(1,$tab).'<script type="text/javascript">';
		
	return($_js);
}

/**
* Funzione per costruire un tag di chiusura script
*/
function jsBuildScriptClose($tab=2)
{
	$_js = codeInit();
	
	$_js .= codeChr(1,$tab).'</script>';
		
	return($_js);
}

/**
* Funzione per costruire una stringa commento in formato JavaScript
*/
function jsBuildComment($text)
{
	$_js = codeInit();
	
	$_js .= codeChr(1,0)."/* ".$text." */";
	
	return($_js);
}

/**
* Funzione per generare correttamente la chiamata a event.preventDefault
*/
function jsEventPreventDefault()
{
	$_js = codeInit();
	
	$_js .= '(event.preventDefault) ? event.preventDefault() : event.returnValue = false;';
	
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati ad un bottone di pagina della topbar
*/
function jsPageButtonEventManager($buttonType)
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'$(".'.$buttonType.'_page_button")'
			.codeChr(1,4).'.mouseover(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseout(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").show();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseup(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
				.codeChr(1,5).'$(this).click();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.click(function(event) {'
				.codeChr(1,5)."waitingStart();"
				.codeChr(1,5).'var this_id = $(this).attr("id");'
				.codeChr(1,5).'exit_nocheck = true;'
				.codeChr(1,5).'var url = "?page="+this_id.replace("'.$buttonType.'_page_button_", "");'
				.codeChr(1,5).'window.location = url;'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
		.codeChr(1,3).';';
	
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati ad un bottone di una barra
*/
function jsBarButtonEventManager($buttonType=null)
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'$(".'.$buttonType.'_button")'
			.codeChr(1,4).'.mouseover(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseenter(function() {'
				.codeChr(1,5).'$(this).next().show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseleave(function() {'
				.codeChr(1,5).'$(this).next().hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseout(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").show();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
				.codeChr(1,4).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseup(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.click(function(event) {'
				.codeChr(1,5).'eval($(this).attr("id")+"Click();");'
				.codeChr(1,4).jsEventPreventDefault()
			.codeChr(1,4).'})'
		.codeChr(1,3).';';
	
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati ad un bottone selettore di una barra
*/
function jsBarSelectorButtonEventManager($buttonType="navbar_selector_button")
{			
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'$(".'.$buttonType.'").mouseover(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseout(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'	
		.codeChr(1,3).'.mousedown(function(event) {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().show();'
			.jsEventPreventDefault()
		.codeChr(1,3).'})'	
		.codeChr(1,3).'.mouseup(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseenter(function() {'
			.codeChr(1,4).'$(this).next().show();' //fadeTo
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseleave(function() {'
			.codeChr(1,4).'$(this).next().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.click(function(event) {'
			.codeChr(1,4).'eval($(this).attr("id")+"Click();");'
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		;
	
	return($_js);
}

/**
* Funzione per costruire il codice JS per gestire gli eventi legati ad un bottone filtro di stato periferica
*/
function jsBarStatusFilterEventManager($buttonType="navbar_filter_button"){
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'var barStatusFilterClickTimer = null, barStatusFilterClicks = 0;';
	
	$_js .= codeChr(1,3).'$(".'.$buttonType.'").mouseover(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseout(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'	
		.codeChr(1,3).'.mousedown(function(event) {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().show();'
			.jsEventPreventDefault()
		.codeChr(1,3).'})'	
		.codeChr(1,3).'.mouseup(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseenter(function() {'
			.codeChr(1,4).'$(this).next().show();' //fadeTo
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseleave(function() {'
			.codeChr(1,4).'$(this).next().hide();'
		.codeChr(1,3).'})'
		
		/*
		.codeChr(1,3).'.click(function(event) {'
			
			//.codeChr(1,4).'eval("alert("+$(this).attr("id")+"_active);");'
			//.codeChr(1,4).'eval($(this).attr("id")+"Click();");'
			
			.codeChr(1,4).'if (barStatusFilterClickTimer == null) {'
				.codeChr(1,5).'barStatusFilterClickTimer = setTimeout(function() {'
					.codeChr(1,6).'barStatusFilterClicks = 0;'
					.codeChr(1,6).'barStatusFilterClickTimer = null;'
					// single click code
					//.codeChr(1,6).'alert("single");'
					
					.codeChr(1,6).'$(this).prev().toggle();'
					.codeChr(1,6).'eval($(this).attr("id")+"_active=!"+$(this).attr("id")+"_active;");'
					
				.codeChr(1,5).'}, 250);'
			.codeChr(1,4).'}'

			.codeChr(1,4).'if(barStatusFilterClicks === 1) {'
				.codeChr(1,5).'clearTimeout(barStatusFilterClickTimer);'
				.codeChr(1,5).'barStatusFilterClickTimer = null;'
				.codeChr(1,5).'barStatusFilterClicks = -1;'
				// double click code
				.codeChr(1,5).'alert("double");'
				
				// Disattivo tutti i filtri
				.codeChr(1,5).'$(".navbar_selected").hide();'
				.codeChr(1,5).'$(this).prev().view();'
				.codeChr(1,5).'eval($(this).attr("id")+"_active=true;");'
				
			.codeChr(1,4).'}'
			.codeChr(1,4).'barStatusFilterClicks++;'
			
			.codeChr(1,4).'device_status_filterClick();'
			.codeChr(1,4).'ajaxDevicesInfoSender(true,true);'
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		//*/
		//*
		.codeChr(1,3).'.click(function(event) {'
			
			
			.codeChr(1,4).'$(this).prev().toggle();'
			.codeChr(1,4).'eval($(this).attr("id")+"_active=!"+$(this).attr("id")+"_active;");'
			
			
			
			
			.codeChr(1,4).'if (barStatusFilterClickTimer == null) {'
				.codeChr(1,5).'barStatusFilterClickTimer = setTimeout(function() {'
					.codeChr(1,6).'barStatusFilterClicks = 0;'
					.codeChr(1,6).'barStatusFilterClickTimer = null;'
					// single click code
					
					
					
				.codeChr(1,5).'}, 250);'
			.codeChr(1,4).'}'
			
			
			.codeChr(1,4).'if(barStatusFilterClicks === 1) {'
				.codeChr(1,5).'clearTimeout(barStatusFilterClickTimer);'
				.codeChr(1,5).'barStatusFilterClickTimer = null;'
				.codeChr(1,5).'barStatusFilterClicks = -1;'
				// double click code
				
				// Disattivo tutti i filtri
				.codeChr(1,5).'$(".navbar_selected").hide();'
				.codeChr(1,5).'$(this).prev().show();'
				.codeChr(1,5).'device_status_filter_offline_active=false;'
				.codeChr(1,5).'device_status_filter_error_active=false;'
				.codeChr(1,5).'device_status_filter_warning_active=false;'
				.codeChr(1,5).'device_status_filter_ok_active=false;'
				.codeChr(1,5).'device_status_filter_unknown_active=false;'
				.codeChr(1,5).'device_status_filter_iec_active=false;'
				.codeChr(1,5).'eval($(this).attr("id")+"_active=true;");'
				
			.codeChr(1,4).'}'
			
			
			
			.codeChr(1,4).'barStatusFilterClicks++;'
			
			//.codeChr(1,4).'eval($(this).attr("id")+"Click();");'
			.codeChr(1,4).'device_status_filterClick();'
			.codeChr(1,4).'ajaxDevicesInfoSender(true,true);'
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		//*/
		;
	
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati ad un bottone freccia di una barra
*/
function jsBarArrowButtonEventManager($buttonType="navbar_arrow_button")
{			
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'$(".'.$buttonType.'").mouseover(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseout(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'	
		.codeChr(1,3).'.mousedown(function(event) {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().show();'
			.jsEventPreventDefault()
		.codeChr(1,3).'})'	
		.codeChr(1,3).'.mouseup(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseenter(function() {'
			.codeChr(1,4).'$(this).next().show();' //fadeTo
		.codeChr(1,3).'})'
		.codeChr(1,3).'.mouseleave(function() {'
			.codeChr(1,4).'$(this).next().hide();'
		.codeChr(1,3).'})'
		.codeChr(1,3).'.click(function(event) {'
			.codeChr(1,4).'eval($(this).attr("id")+"Click();");'
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		;
	
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati ad un testo di una barra
*/
function jsBarTextEventManager()
{
	$_js = codeInit();
	
	$_js .= codeChr(1,1).'$(".navbar_text").click(function(event) {'
				.codeChr(1,2).'eval($(this).attr("id")+"Click();");'
				.codeChr(1,2).jsEventPreventDefault()
			.codeChr(1,1).'});'
			;
	
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati ad un bottone del pannello
*/
function jsPanelButtonEventManager($buttonType)
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'$(".'.$buttonType.'_button")'
			.codeChr(1,4).'.mouseover(function() {'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseout(function() {'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_over").hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_click").show();'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_over").hide();'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseup(function() {'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_over").show();'
				.codeChr(1,5).'$(this).click();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseenter(function() {'
				.codeChr(1,5).'$(this).next().show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseleave(function() {'
				.codeChr(1,5).'$(this).next().hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.click(function(event) {'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$buttonType.'_button_over").show();'
				.codeChr(1,5).'eval($(this).attr("id")+"Click();");'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
		.codeChr(1,3).';';
			
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati alla selezione di dati di una periferica (stream, field e value)
*/
function jsDeviceDataSelectionEventManager($class=null,$profile=null)
{
	$_js = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_js);
	
	if ($class === null)
		$_class = "device_details_panel";
	else
		$_class = $class;
	
	// Gestione selezione stream
	$_js .= codeChr(1,3).'$("#'.$_class.'_streams").find(".details_panel_text_name").click(function(event) {'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".icon_status_hole").show();'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".icon_status_hole_selected").hide();'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".details_panel_selection_bg").hide();'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".details_panel_selection_right").hide();'
			.codeChr(1,4).'$(this).prev().prev().children(".icon_status_hole_selected").show();'
			.codeChr(1,4).'$(this).prev().prev().children(".icon_status_hole").hide();'
			.codeChr(1,4).'$(this).prev().prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$("#'.$_class.'_descriptions").hide();'
			.codeChr(1,4).'$("#'.$_class.'_values").hide();'
			.codeChr(1,4).'$("#'.$_class.'_fields").hide();'
				.codeChr(1,5).'if (navbar_active_level == "device"){'
					.codeChr(1,6).'var selected_stream_str = $(this).attr("id");'
					.codeChr(1,6).'selected_stream_str = selected_stream_str.replace("_name", "");'
					.codeChr(1,6).'selected_stream = selected_stream_str;'
					.codeChr(1,6).'selected_field = null;'
					.codeChr(1,6).'selected_value = \'0\';'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true,true);'
				.codeChr(1,5).'}'
			.codeChr(1,4).'var offset = (parseFloat($("#device_details_panel").css("top")))-5;'
			//*
			.codeChr(1,4).'$("#device_details_panel").animate({'
				.codeChr(1,5).'top: "-=" +offset'
			.codeChr(1,4).'}, 0);'
			//*/
			.codeChr(1,4).jsEventPreventDefault()
		.codeChr(1,3).'});'
		.codeChr(1,3).'$("#'.$_class.'_streams").find(".details_panel_text_value").click(function(event) {'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".icon_status_hole").show();'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".icon_status_hole_selected").hide();'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".details_panel_selection_bg").hide();'
			.codeChr(1,4).'$("#'.$_class.'_streams").find(".details_panel_selection_right").hide();'
			.codeChr(1,4).'$(this).prev().prev().prev().children(".icon_status_hole_selected").show();'
			.codeChr(1,4).'$(this).prev().prev().prev().children(".icon_status_hole").hide();'
			.codeChr(1,4).'$(this).prev().prev().prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().prev().prev().show();'
			.codeChr(1,4).'$("#'.$_class.'_descriptions").hide();'
			.codeChr(1,4).'$("#'.$_class.'_values").hide();'
			.codeChr(1,4).'$("#'.$_class.'_fields").hide();'
				.codeChr(1,5).'if (navbar_active_level == "device"){'
					.codeChr(1,6).'var selected_stream_str = $(this).attr("id");'
					.codeChr(1,6).'selected_stream_str = selected_stream_str.replace("_value", "");'
					.codeChr(1,6).'selected_stream = selected_stream_str;'
					.codeChr(1,6).'selected_field = null;'
					.codeChr(1,6).'selected_value = \'0\';'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true,true);'
				.codeChr(1,5).'}'
			.codeChr(1,4).'var offset = (parseFloat($("#device_details_panel").css("top")))-5;'
			//*
			.codeChr(1,4).'$("#device_details_panel").animate({'
				.codeChr(1,5).'top: "-=" +offset'
			.codeChr(1,4).'}, 0);'
			//*/
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		;
		
	// Gestione selezione field
	$_js .= codeChr(1,3).'$("#device_details_panel_fields").find(".details_panel_text_name").click(function(event) {'
			.codeChr(1,4).'$("#'.$_class.'_fields").find(".icon_status_hole").show();'
			.codeChr(1,4).'$("#'.$_class.'_fields").find(".icon_status_hole_selected").hide();'
			.codeChr(1,4).'$("#'.$_class.'_fields").find(".details_panel_selection_bg").hide();'
			.codeChr(1,4).'$("#'.$_class.'_fields").find(".details_panel_selection_right").hide();'
			.codeChr(1,4).'$(this).prev().prev().children(".icon_status_hole_selected").show();'
			.codeChr(1,4).'$(this).prev().prev().children(".icon_status_hole").hide();'
			.codeChr(1,4).'$(this).prev().prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$("#'.$_class.'_descriptions").hide();'
			.codeChr(1,4).'$("#'.$_class.'_values").hide();'
				.codeChr(1,5).'if (navbar_active_level == "device"){'
					.codeChr(1,6).'var selected_field_str = $(this).attr("id");'
					.codeChr(1,6).'selected_field_str = selected_field_str.replace("_name", "");'
					.codeChr(1,6).'selected_field = selected_field_str;'
					.codeChr(1,6).'selected_value = \'0\';'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true);'
				.codeChr(1,5).'}'
			.codeChr(1,4).'var offset = (parseFloat($("#device_details_panel").css("top")))-5;'
			//*
			.codeChr(1,4).'$("#device_details_panel").animate({'
				.codeChr(1,5).'top: "-=" +offset'
			.codeChr(1,4).'}, 0);'
			//*/
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		;
		
	// Gestione selezione value
	$_js .= codeChr(1,3).'$("#'.$_class.'_values").find(".details_panel_text_name").click(function(event) {'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".icon_status_hole").show();'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".icon_status_hole_selected").hide();'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".details_panel_selection_bg").hide();'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".details_panel_selection_right").hide();'
			.codeChr(1,4).'$(this).prev().children(".icon_status_hole_selected").show();'
			.codeChr(1,4).'$(this).prev().children(".icon_status_hole").hide();'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().show();'
			.codeChr(1,4).'$("#'.$_class.'_descriptions").hide();'
				.codeChr(1,5).'if (navbar_active_level == "device"){'
					.codeChr(1,6).'var selected_value_str = $(this).attr("id");'
					.codeChr(1,6).'selected_value_str = selected_value_str.replace("_name", "");'
					.codeChr(1,6).'selected_value = selected_value_str;'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true);'
				.codeChr(1,5).'}'
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		.codeChr(1,3).'$("#'.$_class.'_values").find(".details_panel_text_value").click(function(event) {'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".icon_status_hole").show();'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".icon_status_hole_selected").hide();'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".details_panel_selection_bg").hide();'
			.codeChr(1,4).'$("#'.$_class.'_values").find(".details_panel_selection_right").hide();'
			.codeChr(1,4).'$(this).prev().prev().children(".icon_status_hole_selected").show();'
			.codeChr(1,4).'$(this).prev().prev().children(".icon_status_hole").hide();'
			.codeChr(1,4).'$(this).prev().prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$("#'.$_class.'_descriptions").hide();'
			.codeChr(1,4).'if (navbar_active_level == "device"){'
				.codeChr(1,5).'var selected_value_str = $(this).attr("id");'
				.codeChr(1,5).'selected_value_str = selected_value_str.replace("_value", "");'
				.codeChr(1,5).'selected_value = selected_value_str;'
				.codeChr(1,5).'ajaxDevicesDetailsSender(true);'
			.codeChr(1,4).'}'
			.jsEventPreventDefault()
		.codeChr(1,3).'});'
		;
			
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per gestire gli eventi legati ad un "ack" su pallino stato
*/
function jsIconStatusAckEventManager($class=null,$profile=null)
{
	$_js = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_js);
	
	if ($class === null)
		$_class = "icon_status";
	else
		$_class = $class;
	
	$_js .= codeChr(1,3).'$(".'.$_class.'")'
			.codeChr(1,4).'.mouseover(function() {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseout(function() {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_over").hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_click").show();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_over").hide();'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseup(function() {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_over").show();'
				.codeChr(1,5).'$(this).click();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.click(function(event) {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_over").show();'
				.codeChr(1,5).'detailsEnableAckMode("#"+$(this).attr("id"));'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
		.codeChr(1,3).';';
		
	$_js .= codeChr(1,3).'$(".'.$_class.'_ack_button")'
			.codeChr(1,4).'.mouseover(function() {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseout(function() {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_over").hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_click").show();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_over").hide();'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseup(function() {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_over").show();'
				.codeChr(1,5).'$(this).click();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseenter(function() {'
				.codeChr(1,5).'$(this).next().show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseleave(function() {'
				.codeChr(1,5).'$(this).next().hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.click(function(event) {'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_click").hide();'
				.codeChr(1,5).'$(this).children(".'.$_class.'_ack_button_over").show();'
				.codeChr(1,5).'var element = $(this).parent().parent().attr("id");'
				.codeChr(1,5).'var name = element.replace("icon_status_ack_panel_","");'
				.codeChr(1,5).'var names = name.split("_");'
				.codeChr(1,5).'eval($(this).attr("id")+"Click(\""+names[0]+"\",\""+names[1]+"\",\""+element+"\");");'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
		.codeChr(1,3).';';
		
	$_js .= codeChr(1,3).'$(".icon_status_ack_panel_sub_layer")'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.click(function(event) {'
				.codeChr(1,5).'detailsDisableAckMode();'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'})'
		.codeChr(1,3).';';
			
	return($_js);
}

/**
* Funzione per costruire il codice Javascript per la funzione formChrChk
*/
function jsFormCharCheck()
{
	$_js = codeInit();
	$_js .= codeChr(1,0).codeFunctionComment("Funzione per convertire le stringhe dei form",0);
	$_js .= codeChr(1,0).'function formChrChk(form_value){'
			.codeChr(1,1)."var value = form_value;"
			//.codeChr(1,1)."value = escape(value);"
			.codeChr(1,1)."if (value != null){"
			.codeChr(1,1)."value = value.replace(/à/g, 'a'+String.fromCharCode(96));"
			.codeChr(1,1)."value = value.replace(/è/g, 'e'+String.fromCharCode(96));"
			.codeChr(1,1)."value = value.replace(/é/g, 'e'+String.fromCharCode(96));"
			.codeChr(1,1)."value = value.replace(/ì/g, 'i'+String.fromCharCode(96));"
			.codeChr(1,1)."value = value.replace(/ò/g, 'o'+String.fromCharCode(96));"
			.codeChr(1,1)."value = value.replace(/ù/g, 'u'+String.fromCharCode(96));"
			.codeChr(1,1)."value = value.replace(/</g, '(');"
			.codeChr(1,1)."value = value.replace(/>/g, ')');"
			.codeChr(1,1)."value = value.replace('+', '');"
			.codeChr(1,1)."value = value.replace('%', '');"
			.codeChr(1,1)."} else value = '';"
			.codeChr(1,1)."return(value);"
		.codeChr(1,0).'}';
		
	return($_js);
}

/**
* Funzione per generare il codice javscript che gestisce i campi input select dei form
*/
function jsPanelFormSelect($form,$id,$valueList,$readOnly=false,$selectJs=null)
{
	$_js = codeInit();
	
	$_id_form	= $form."_form";
	$_id_label 	= $_id_form.'_'.$id.'_label';
	$_id_input 	= $_id_form.'_'.$id.'_select';
	$_id_up 	= $_id_form.'_image_'.$id.'_select_up';
	$_id_down 	= $_id_form.'_image_'.$id.'_select_down';
	$_id_search	= $_id_form.'_image_'.$id.'_select_search';
	
	if (isset($selectJs))
		$_select_js = $selectJs;
	else
		$_select_js = "";
	
	// Gestione autocompletamento
	$_js .= codeFunctionComment("Funzione di autocompletamento per #".$_id_input,3);
	$_js .= codeChr(1,3).'$( "#'.$_id_input.'" ).autocomplete({'
			.codeChr(1,4).'source: '.$valueList.','
			.codeChr(1,4).'minLength: 0,'
			.codeChr(1,4).'appendTo: "#'.$_id_form.'",'
			.codeChr(1,4).'select: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_up.'").hide();'
				.codeChr(1,5).'$("#'.$_id_down.'").show();'
				.codeChr(1,5).'$("#'.$_id_search.'").hide();'
				.codeChr(1,5).'$( "#'.$_id_input.'" ).val(ui.item.value)'
				.codeChr(1,5).$_select_js
			.codeChr(1,4).'},'
			.codeChr(1,4).'search: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_up.'").hide();'
				.codeChr(1,5).'$("#'.$_id_down.'").hide();'
				.codeChr(1,5).'$("#'.$_id_search.'").show();'
			.codeChr(1,4).'},'
			.codeChr(1,4).'open: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_up.'").show();'
				.codeChr(1,5).'$("#'.$_id_down.'").hide();'
				.codeChr(1,5).'$("#'.$_id_search.'").hide();'
			.codeChr(1,4).'},'
			.codeChr(1,4).'close: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_up.'").hide();'
				.codeChr(1,5).'$("#'.$_id_down.'").show();'
				.codeChr(1,5).'$("#'.$_id_search.'").hide();'
			.codeChr(1,4).'},'
			.codeChr(1,4).'change: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_up.'").hide();'
				.codeChr(1,5).'$("#'.$_id_down.'").show();'
				.codeChr(1,5).'$("#'.$_id_search.'").hide();'
			.codeChr(1,4).'}'
		.codeChr(1,3).'});'
		;
	
	// Se non editabile
	if ($readOnly==true)
	{
		// Gestione Doubleclick
		$_js .= codeChr(1,3).'$( "#'.$_id_input.'" ).dblclick(function(event){'
			.codeChr(1,4).'$(this).autocomplete("search","");'
        	.codeChr(1,4).jsEventPreventDefault()
        .codeChr(1,3).'});'
		;
		// Gestione Click
		$_js .= codeChr(1,3).'$( "#'.$_id_input.'" ).click(function(event){'
			.codeChr(1,4).'$(this).autocomplete("search","");'
        	.codeChr(1,4).jsEventPreventDefault()
        .codeChr(1,3).'});'
		;
	}
	// Se editabile
	else
	{
		// Gestione Doubleclick
		$_js .= codeChr(1,3).'$( "#'.$_id_input.'" ).dblclick(function(event){'
			.codeChr(1,4).'$(this).val(\'\');'
        	.codeChr(1,4).'$(this).autocomplete("search");'
        	.codeChr(1,4).jsEventPreventDefault()
        .codeChr(1,3).'});'
		;
	}
	
	// Gestione Click Icona Down
	$_js .= codeChr(1,3).'$( "#'.$_id_down.'" ).click(function(event){'
			.codeChr(1,4).'$("#'.$_id_input.'").autocomplete("search","");'
        	.codeChr(1,4).jsEventPreventDefault()
        .codeChr(1,3).'});'
		;
	// Gestione Click Icona Up
	$_js .= codeChr(1,3).'$( "#'.$_id_up.'" ).click(function(event){'
			.codeChr(1,4).'$("#'.$_id_input.'").autocomplete("close");'
        	.codeChr(1,4).jsEventPreventDefault()
        .codeChr(1,3).'});'
		;
	
	
	return($_js);
}

/**
* Funzione per generare il codice javscript che gestisce i campi input search dei form
*/
function jsPanelFormSearch($form,$id,$valueList,$selectJs=null)
{
	$_js = codeInit();
	
	$_id_form	= $form."_form";
	$_id_label 	= $_id_form.'_'.$id.'_label';
	$_id_input 	= $_id_form.'_'.$id.'_search';
	$_id_icon 	= $_id_form.'_image_'.$id.'_search_icon';
	
	if (isset($selectJs))
		$_select_js = $selectJs;
	else
		$_select_js = "";
	
	// Gestione autocompletamento
	$_js .= codeFunctionComment("Funzione di autocompletamento per #".$_id_input,3);
	$_js .= codeChr(1,3).'$("#'.$_id_input.'").autocomplete({'
			.codeChr(1,4).'source: '.$valueList.','
			.codeChr(1,4).'minLength: 0,'
			.codeChr(1,4).'appendTo: "#'.$_id_form.'",'
			.codeChr(1,4).'select: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_icon.'").hide();'
				.codeChr(1,5).'$( "#'.$_id_input.'" ).val(ui.item.value)'
				.codeChr(1,5).$_select_js
			.codeChr(1,4).'},'
			.codeChr(1,4).'search: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_icon.'").show();'
			.codeChr(1,4).'},'
			.codeChr(1,4).'open: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_icon.'").hide();'
			.codeChr(1,4).'},'
			.codeChr(1,4).'change: function (event, ui){'
				.codeChr(1,5).'$("#'.$_id_icon.'").hide();'
			.codeChr(1,4).'}'
		.codeChr(1,3).'});'
		;
	// Gestione Doubleclick
	$_js .= codeChr(1,3).'$("#'.$_id_input.'").dblclick(function(event){'
			.codeChr(1,4).'$(this).val(\'\');'
        	.codeChr(1,4).'$(this).autocomplete("search");'
        	.codeChr(1,4).jsEventPreventDefault()
        .codeChr(1,3).'});'
		;
	// Gestione Click (Solo se non editabile)
	$_js .= codeChr(1,3).'$( "#'.$_id_input.'" ).click(function(event){'
			.codeChr(1,4).'$(this).autocomplete("search","");'
        	.codeChr(1,4).jsEventPreventDefault()
        .codeChr(1,3).'});'
		;
				
	return($_js);
}

/**
* Funzione per generare il codice JavaScript per gestire l'upload AJAX
*/
function jsBuildUploader()
{
	global $_conf_console_html_color_code;
	
	$_js = codeInit();   
	
	$_js .= codeFunctionComment("Funzione per gestire l'upload AJAX",0);    
    	
    $_js .= codeChr(1,0).'function createUploader(){'
				.codeChr(1,1).'var lol = new qq.FileUploaderBasic({'
					.codeChr(1,2).'button: $("#bottombar_button_upload")[0],'
					.codeChr(1,2).'buttonClick: $("#bottombar_button_upload_click")[0],'
                	.codeChr(1,2).'buttonOver: $("#bottombar_button_upload_over")[0],'
                	.codeChr(1,2).'action: "json/sk_uploadconfig.php",'
                	.codeChr(1,2).'allowedExtensions: ["xml","xcb"],'
                	.codeChr(1,2).'multiple: false,'
                	.codeChr(1,2).'debug: false,'
                	.codeChr(1,2).'onSubmit: function(id, fileName)'
                	.codeChr(1,2).'{'
                		.codeChr(1,3).'waitingStart();'
                	.codeChr(1,2).'},'
                	.codeChr(1,2).'onProgress: function(id, fileName, loaded, total){'
                		.codeChr(1,3).'waitingText("Inviati "+(loaded)+" Byte su "+(total)+" Byte totali");'
                		//.codeChr(1,3).'showMessage("bottombar",loaded+" KB inviati su "+(total/1024)KB+".","'.$_conf_console_html_color_code["green"].'",false);'
                	.codeChr(1,2).'},'
                	.codeChr(1,2).'onComplete: function(id, fileName, responseJSON)'
                	.codeChr(1,2).'{'
                		.codeChr(1,3).'if (responseJSON.success == true)'
                		.codeChr(1,3).'{'
                			.codeChr(1,4).'exit_nocheck = true;'
                			.codeChr(1,4).'var url = "?reload=true&upload=true";'
                			.codeChr(1,4).'window.location = url;'
                		.codeChr(1,3).'}'
                		.codeChr(1,3).'else'
                			.codeChr(1,4).'waitingStop();'
                		.codeChr(1,3).'},'
                	.codeChr(1,2).'onCancel: function(id, fileName)'
                	.codeChr(1,2).'{'
                		.codeChr(1,3).'waitingStop();'
                		.codeChr(1,3).'showMessage("bottombar","Upload cancellato.","'.$_conf_console_html_color_code["red"].'",false);'
                	.codeChr(1,2).'},'
                	.codeChr(1,2).'onUpload: function(id, fileName, xhr)'
                	.codeChr(1,2).'{'
                		.codeChr(1,3).'waitingStart("");'
                	.codeChr(1,2).'},'
                	.codeChr(1,2).'messages: {'
                		.codeChr(1,3).'typeError: "{file} ha un\'estensione non valida. Solo file {extensions} sono permessi.",'
                		.codeChr(1,3).'sizeError: "{file} ha una dimensione troppo grande, la dimensione massima e\' {sizeLimit}.",'
                		.codeChr(1,3).'minSizeError: "{file} ha una dimensione troppo piccola, la dimensione minima e\' {minSizeLimit}.",'
                		.codeChr(1,3).'emptyError: "{file} e\' vuoto o non valido.",'
                		.codeChr(1,3).'onLeave: "Un upload e\' in corso. Uscendo ora si interrompera\' il trasferimento."    '        
                	.codeChr(1,2).'},'
                	.codeChr(1,2).'showMessage: function(message){'
                		.codeChr(1,3).'showMessage("bottombar",message,"'.$_conf_console_html_color_code["red"].'",true);'
                	.codeChr(1,2).'}'
				.codeChr(1,1).'});'
			.codeChr(1,0).'}'
	;
    
    return($_js);
}
/**
* Funzione per costruire il codice JavaScript per gestire il bottone di download
*/
function jsBuildDownloadButton()
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'$("#bottombar_button_download")'
			.codeChr(1,4).'.mouseover(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseout(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").show();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
				//.codeChr(1,4).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseup(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseenter(function() {'
				.codeChr(1,5).'$(this).next().show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseleave(function() {'
				.codeChr(1,5).'$(this).next().hide();'
			.codeChr(1,4).'})'
			/*
			.codeChr(1,4).'.click(function(event) {'
				//.codeChr(1,5).'eval($(this).attr("id")+"Click();");'
				//.codeChr(1,4).jsEventPreventDefault()
			.codeChr(1,4).'})'*/
		.codeChr(1,3).';';
	
	return($_js);
}

/**
* Funzione per costruire il codice JavaScript per gestire il bottone di upload
*/
function jsBuildUploadButton()
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'$("#bottombar_button_upload")'
			/*
			.codeChr(1,4).'.mouseover(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseout(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mousedown(function(event) {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").show();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").hide();'
				//.codeChr(1,4).jsEventPreventDefault()
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseup(function() {'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_click").hide();'
				.codeChr(1,5).'$(this).children("#"+$(this).attr("id")+"_over").show();'
			.codeChr(1,4).'})'
			*/
			.codeChr(1,4).'.mouseenter(function() {'
				.codeChr(1,5).'$(this).next().show();'
			.codeChr(1,4).'})'
			.codeChr(1,4).'.mouseleave(function() {'
				.codeChr(1,5).'$(this).next().hide();'
			.codeChr(1,4).'})'
			/*
			.codeChr(1,4).'.click(function(event) {'
				//.codeChr(1,5).'eval($(this).attr("id")+"Click();");'
				//.codeChr(1,4).jsEventPreventDefault()
			.codeChr(1,4).'})'*/
		.codeChr(1,3).';';
	
	return($_js);
}

/**
* Funzione per scrivere una riga di debug in un log su interfaccia tramite Javascript
*/
function jsBuildLogFunction()
{
	global $_conf_log_visual_element, $_conf_log_visual_mode;

	$_js = codeInit();
	
	$_js .= codeFunctionComment("Funzione per scrivere una riga di log visuale",0);
	
	$_js .= codeChr(1,0).'function visualLog(text,appendText){';
	
	if ($_conf_log_visual_mode === true)
	{
		$_js .= codeChr(1,1).'if (appendText==true) $("#'.$_conf_log_visual_element.'").append(text);';
		$_js .= codeChr(1,1).'else $("#'.$_conf_log_visual_element.'").html(text);';
	}
		
	$_js .=	codeChr(1,0).'}';

	return($_js);
}

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_main.js.php - Modulo per la generazione del codice Javascript principale.
*
* @author Enrico Alborali
* @version 1.0.4.1 25/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/
// Imposto l'intestazione per il codice Javascript
header('Content-type: application/javascript');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria per il codice JavaScript
require_once("../lib/lib_js.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo il modulo Core
require_once("../modules/sk_core.php");

if (!isset($_conf_console_app_hardware_device_code)) $_conf_console_app_hardware_device_code = 'STLC1000';

// Recupero il prefisso per i workers AJAX
$_page_ajax_workers_prefix = coreGetPageAjaxWorkersPrefix();

// Recupero il livello attuale per la pagina attiva
$_page_ajax_active_level = coreGetLevelFromSession();

// Recupero il livello di edit
$_edit_level = coreGetEditLevelFromSession(true);

$_js = codeInit();

// Costruisco e stampo l'intestazione del JavaScript
$_js .= jsBuildCodeHeader("Script principale applicazione.");

// Codice per la funzione che gestisce i log visuali
$_js .= jsBuildLogFunction();

// Variabili globali per la visualizzazione del contenuto
$_js .= codeChr(1,0).'var navbar_active_level="'.$_page_ajax_active_level.'";'
		.codeChr(1,0).'var force_edit_level = null;'
		.codeChr(1,0).'var edit_level = '.$_edit_level.';'
		;
		
$_js .= codeChr(1,0).'var exit_nocheck = false;'
			.codeChr(1,0).'function skcCloseCheck()'
			.codeChr(1,0).'{'
				.codeChr(1,1).'if ($("#bottombar_button_apply_image_light").is(":visible") && exit_nocheck == false){'
					.codeChr(1,2).'exit_nocheck = false;'
					.codeChr(1,2).'return "Sono state effettuate alcune modifiche ma non sono state applicate. Chiudendo il browser andranno perse.";'
				.codeChr(1,1).'}'
			.codeChr(1,0).'}';

// Script AJAX per aggiornare il contenuto della pagina
$_js_htmlWorker = codeInit();
$_js_htmlWorker .= codeChr(1,1).'if ( (navbar_active_level == "info" && details_edit_mode == false) || (navbar_active_level != "info") ){'
								.codeChr(1,2).'var content_top = $("#content_middle").position().top-10;'
								.codeChr(1,2).'$("#content_middle").empty();'
								.codeChr(1,2).'$("#content_middle").replaceWith(html);'
								.codeChr(1,2).'if (content_top != 10) $("#content_middle").animate({'
									.codeChr(1,3).'top: "+=" +content_top'
								.codeChr(1,2).'}, 0);'
								/*
								.codeChr(1,2).'$("#content_middle").find("*").mousedown(function(event){'.jsEventPreventDefault().'})'
      							//*/
								.codeChr(1,2).'html = null;'
							.codeChr(1,1).'}else{'
								.codeChr(1,2).'html = null;'
							.codeChr(1,1).'}';
$_js_htmlWorker .=  codeChr(1,1).'if (navbar_active_level == "info") detailsPanelLinkEvents(); else pageLinkEvents();';

$_htmlWorker = new ajaxWorker( $_page_ajax_workers_prefix."Info", true, $_conf_console_ajax_repeat_timeout, $_js_htmlWorker );
if ($_page_ajax_workers_prefix=='Devices')
	$_js .= $_htmlWorker->BuildJavaScript("html","'level='+navbar_active_level+'&filter_offline='+device_status_filter_offline_active+'&filter_error='+device_status_filter_error_active+'&filter_warning='+device_status_filter_warning_active+'&filter_ok='+device_status_filter_ok_active+'&filter_unknown='+device_status_filter_unknown_active+'&filter_iec='+device_status_filter_iec_active");
else
	$_js .= $_htmlWorker->BuildJavaScript("html","'level='+navbar_active_level");

// Script AJAX per aggiornare il contenuto del pannello dettagli
$_js_device_details_worker = codeInit();

$_js_device_details_worker .= codeChr(1,1).'if ( navbar_active_level != "info" && details_edit_mode == false && selected_item != null && details_view_mode == true && details_ack_mode == false ){'
								.codeChr(1,2).'var content_top = $("#"+navbar_active_level+"_details_panel").position().top-5;'
								.codeChr(1,2).'$("#"+navbar_active_level+"_details_panel").empty();'
								.codeChr(1,2).'$("#"+navbar_active_level+"_details_panel").replaceWith(html);'
								.codeChr(1,2).'$("#"+navbar_active_level+"_details_panel").show();'
								.'if (navbar_active_level == "device") deviceSendEditFormData();'		
								// Ricavo automaticamente l'id dello stream selezionato (se non disponibile)
								.codeChr(1,2).'if (selected_stream == null){'
									.codeChr(1,3).'var selected_stream_str = $("#device_details_panel_streams").find(".icon_status_hole_selected").attr("id");'
									.codeChr(1,3).'if (selected_stream_str != null){'
										.codeChr(1,4).'selected_stream_str = selected_stream_str.replace("icon_status_hole_selected_stream_", "");'
										.codeChr(1,4).'selected_stream = selected_stream_str;'
									.codeChr(1,3).'}'
								.codeChr(1,2).'}'
								// Riposizionamento
								.codeChr(1,2).'if (content_top != 10) $("#"+navbar_active_level+"_details_panel").animate({'
									.codeChr(1,3).'top: "+=" +content_top'
								.codeChr(1,2).'}, 0);'
								/*
								.codeChr(1,2).'$("#"+navbar_active_level+"_details_panel").find("*").mousedown(function(event){'.jsEventPreventDefault().'})'
      							//*/
								.codeChr(1,2).'html = null;'
							.codeChr(1,1).'}else{'
								.codeChr(1,2).'html = null;'
							.codeChr(1,1).'}';

$_js_device_details_worker .= 
							 //codeChr(1,1).'pageLinkEvents();'
							 codeChr(1,1).'detailsPanelLinkEvents();'
							;

$_js_device_details_worker_condition = 'navbar_active_level != "info" && details_edit_mode == false && selected_item != null && details_view_mode == true';

$_x_details_worker = new ajaxWorker( $_page_ajax_workers_prefix."Details", true, $_conf_console_ajax_repeat_timeout, $_js_device_details_worker, $_js_device_details_worker_condition );
$_js .= $_x_details_worker->BuildJavaScript("html","'level='+navbar_active_level+'&selected_item='+selected_item+'&strid='+selected_stream+'&fieldid='+selected_field+'&valueid='+selected_value");

// Script AJAX per aggiornare lo stato STLC1000
$_js_sk_status_worker = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'var mode = $(xml).find("mode").attr("value");'
			.codeChr(1,2).'if (mode == "MAINTENANCE"){'
				.codeChr(1,3).'$("#bottombar_button_maintenance_image_light").show();'
				.codeChr(1,3).'$("#details_panel_info_mode_value").html("In manutenzione");'
			.codeChr(1,2).'}else{'
				.codeChr(1,3).'$("#bottombar_button_maintenance_image_light").hide();'
				.codeChr(1,3).'$("#details_panel_info_mode_value").html("Normale");'
			.codeChr(1,2).'}'
			.(($_conf_console_app_bottombar_fbwf_icon_visible!==false)?(codeChr(1,2).'var fbwf =  $(xml).find("fbwf").attr("value");'
			.codeChr(1,2).'if (fbwf == "false"){'
				.codeChr(1,3).'$("#bottombar_button_fbwf_image_light").show();'
			.codeChr(1,2).'}else{'
				.codeChr(1,3).'$("#bottombar_button_fbwf_image_light").hide();'
			.codeChr(1,2).'}'):'')
			.codeChr(1,2).'var activity = parseInt($(xml).find("activity").attr("value"));'
			.codeChr(1,2).'if (activity == 0 && waitingActive == false){'
				.codeChr(1,3).'description = "Sessione scaduta per inattivita\'.";'
				.codeChr(1,3).'showMessage("bottombar",description,"'.$_conf_console_html_color_code["red"].'",false);'
				.codeChr(1,2).'exit_nocheck = true;'
				//.codeChr(1,2).'$("#bottombar_button_apply_image_light").hide();'
				.codeChr(1,3).'self.close();'
				.codeChr(1,3).'var url = "?exit=true";'
				.codeChr(1,3).'window.location = url;'
			.codeChr(1,2).'}else if (activity < 120 && waitingActive == false) {'
				.codeChr(1,3).'description = "Attenzione! La tua sessione sta\' per scadere.";'
				.codeChr(1,3).'showMessage("bottombar",description,"'.$_conf_console_html_color_code["yellow"].'",true);'
			.codeChr(1,2).'}'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'if (description == null) description = "Errore sconosciuto durante l\'aggiornamento dello stato di questo '.$_conf_console_app_hardware_device_code.'.";'
			.codeChr(1,2).'showMessage("bottombar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;

$_sk_status_worker = new ajaxWorker( "Status", true, $_conf_console_ajax_Status_repeat_timeout * 1000, $_js_sk_status_worker );
$_js .= $_sk_status_worker->BuildJavaScript("xml");

// Script AJAX per aggiornare i parametri delle porte ethernet dell'STLC1000
$_js_sk_network_worker = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'var ethNum = "";'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'if (selected_item == "0"){'
				.codeChr(1,3).'ethNum = "1";'
			.codeChr(1,2).'}else if (selected_item == "1"){'
				.codeChr(1,3).'ethNum = "2";'
			.codeChr(1,2).'}else if (selected_item == "2"){'
				.codeChr(1,3).'ethNum = "3";'
			.codeChr(1,2).'}else if (selected_item == "3"){'
				.codeChr(1,3).'ethNum = "4";'
			.codeChr(1,2).'}'
			
			.codeChr(1,2).'var eth_auto = $(xml).find("eth"+ethNum+"_auto").attr("value");'
			.codeChr(1,2).'var eth_ip = $(xml).find("eth"+ethNum+"_ip").attr("value");'
			.codeChr(1,2).'var eth_mask = $(xml).find("eth"+ethNum+"_mask").attr("value");'
			.codeChr(1,2).'var eth_ip2 = $(xml).find("eth"+ethNum+"_ip2").attr("value");'
			.codeChr(1,2).'var eth_mask2 = $(xml).find("eth"+ethNum+"_mask2").attr("value");'
			.codeChr(1,2).'var eth_ip3 = $(xml).find("eth"+ethNum+"_ip3").attr("value");'
			.codeChr(1,2).'var eth_mask3 = $(xml).find("eth"+ethNum+"_mask3").attr("value");'
			.codeChr(1,2).'var eth_ip4 = $(xml).find("eth"+ethNum+"_ip4").attr("value");'
			.codeChr(1,2).'var eth_mask4 = $(xml).find("eth"+ethNum+"_mask4").attr("value");'
			.codeChr(1,2).'var eth_gw = $(xml).find("eth"+ethNum+"_gw").attr("value");'
			.codeChr(1,2).'var eth_dns1 = $(xml).find("eth"+ethNum+"_dns1").attr("value");'
			.codeChr(1,2).'var eth_dns2 = $(xml).find("eth"+ethNum+"_dns2").attr("value");'
			
			.codeChr(1,2).'if (eth_auto=="true") eth_auto="attivo";'
			.codeChr(1,2).'else if (eth_auto=="false") eth_auto="non attivo";'
			.codeChr(1,2).'if (eth_auto) $("#details_panel_info_auto_active_value").html(eth_auto);'
			.codeChr(1,2).'if (eth_ip) $("#details_panel_info_ip_active_value").html(eth_ip);'
			.codeChr(1,2).'if (eth_mask) $("#details_panel_info_subnet_active_value").html(eth_mask);'
			.codeChr(1,2).'if (eth_ip2) $("#details_panel_info_ip2_active_value").html(eth_ip2);'
			.codeChr(1,2).'if (eth_mask2) $("#details_panel_info_subnet2_active_value").html(eth_mask2);'
			.codeChr(1,2).'if (eth_ip3) $("#details_panel_info_ip3_active_value").html(eth_ip3);'
			.codeChr(1,2).'if (eth_mask3) $("#details_panel_info_subnet3_active_value").html(eth_mask3);'
			.codeChr(1,2).'if (eth_ip4) $("#details_panel_info_ip4_active_value").html(eth_ip4);'
			.codeChr(1,2).'if (eth_mask4) $("#details_panel_info_subnet4_active_value").html(eth_mask4);'
			.codeChr(1,2).'if (eth_gw) $("#details_panel_info_gateway_active_value").html(eth_gw);'
			.codeChr(1,2).'if (eth_dns1 && !eth_dns2) $("#details_panel_info_dns_active_value").html(eth_dns1);'
			.codeChr(1,2).'if (!eth_dns1 && eth_dns2) $("#details_panel_info_dns_active_value").html(eth_dns2);'
			.codeChr(1,2).'if (eth_dns1 && eth_dns2) $("#details_panel_info_dns_active_value").html(eth_dns1+","+eth_dns2);'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'if (description == null) description = "Errore sconosciuto durante il recupero parametri ethernet.";'
			.codeChr(1,2).'showMessage("bottombar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;

$_sk_network_worker = new ajaxWorker( "Network", true, $_conf_console_ajax_Network_repeat_timeout * 1000, $_js_sk_network_worker );
$_js .= $_sk_network_worker->BuildJavaScript("xml");

// Script AJAX per aggiornare le informazioni STLC1000
$_js_sk_info_worker = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'var hostname = $(xml).find("hostname").attr("value");'
			.codeChr(1,2).'$("#bottombar_text_par_hostname").html(hostname);'
			.codeChr(1,2).'$("#details_panel_info_host_active_value").html(hostname);'
			.codeChr(1,2).'$("#details_panel_info_host_detected_value").html(hostname);'
			.codeChr(1,2).'var version = $(xml).find("version").attr("value");'
			.codeChr(1,2).'var version2 = "v. "+version;'
			.codeChr(1,2).'$("#bottombar_text_par_version").html(version2);'
			.codeChr(1,2).'$("#details_panel_info_version_value").html(version);'
			.codeChr(1,2).'var srvid = $(xml).find("srvid").attr("value");'
			.codeChr(1,2).'$("#details_panel_info_id_active_value").html(srvid);'
			.codeChr(1,2).'$("#details_panel_info_id_detected_value").html(srvid);'
			.codeChr(1,2).'var sn = $(xml).find("serial_number").attr("value");'
			.codeChr(1,2).'$("#details_panel_info_sn_value").html(sn);'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'if (description == null) description = "Errore sconosciuto durante l\'aggiornamento delle informazioni di questo '.$_conf_console_app_hardware_device_code.'.";'
			.codeChr(1,2).'showMessage("bottombar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;

$_sk_info_worker = new ajaxWorker( "Info", true, $_conf_console_ajax_Info_repeat_timeout * 1000, $_js_sk_info_worker );
$_js .= $_sk_info_worker->BuildJavaScript("xml");

// Script AJAX per aggiornare lo stato dei servizi STLC1000
$_js_sk_service_status_worker = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'var services_status = $(xml).find("services_status").attr("value");'
			.codeChr(1,2).'if (services_status == "1"){'
				.codeChr(1,3).'$("#bottombar_button_services_image_light").hide();'
				.codeChr(1,3).'$("#details_panel_info_services_status_value").html("In esecuzione");'
			.codeChr(1,2).'}else if (services_status == "0"){'
				.codeChr(1,3).'$("#bottombar_button_services_image_light").show();'
				.codeChr(1,3).'$("#details_panel_info_services_status_value").html("Uno o pi&ugrave; servizi non in esecuzione");'
			.codeChr(1,2).'}else{'
				.codeChr(1,3).'$("#bottombar_button_services_image_light").hide();'
				.codeChr(1,3).'$("#details_panel_info_services_status_value").html("n/d");'
			.codeChr(1,2).'}'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'if (description == null) description = "Errore sconosciuto durante il recupero dello stato dei servizi di questo '.$_conf_console_app_hardware_device_code.'.";'
			.codeChr(1,2).'showMessage("bottombar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;

$_sk_service_status_worker = new ajaxWorker( "ServiceStatus", true, $_conf_console_ajax_ServiceStatus_repeat_timeout * 1000, $_js_sk_service_status_worker );
$_js .= $_sk_service_status_worker->BuildJavaScript("xml");

// Script per recupero configurazione STLC1000
unset($_inner_js);
unset($_worker);
$_inner_js = codeInit();
$_inner_js .= codeChr(1,1).'var result = $(xml).find("result").attr("value");';
$_inner_js .= codeChr(1,1).'if (result == "success"){';
$_inner_js .= codeChr(1,2).'showMessage("bottombar","Recupero della precedente configurazione per questo '.$_conf_console_app_hardware_device_code.' eseguita correttamente.","'.$_conf_console_html_color_code["green"].'",true);'
			.codeChr(1,2).'exit_nocheck = true;';
$_inner_js .= codeChr(1,2).'var url = "?reload=false";';
$_inner_js .= codeChr(1,2).'window.location = url;';
$_inner_js .= codeChr(1,1).'}else{';
$_inner_js .= codeChr(1,2).'showMessage("bottombar","Errore durante il recupero della configurazione per questo '.$_conf_console_app_hardware_device_code.'.","'.$_conf_console_html_color_code["red"].'",true);';
$_inner_js .= codeChr(1,1).'}';
$_worker = new ajaxWorker( "Revert", false, 0, $_inner_js );
$_js .= $_worker->BuildJavaScript("xml","",true);

// Script per presa in carico configurazione STLC1000
unset($_inner_js);
unset($_worker);
$_inner_js = codeInit();
$_inner_js .= codeChr(1,1).'var result = $(xml).find("result").attr("value");';
$_inner_js .= codeChr(1,1).'var desc = $(xml).find("description").attr("value");';
$_inner_js .= codeChr(1,1).'if (result == "success"){';
$_inner_js .= codeChr(1,2).'ajaxServiceStatusSender(true);';
$_inner_js .= codeChr(1,2).'if (desc == "Salvataggio riuscito, ma StlcManagerService down"){';
$_inner_js .= codeChr(1,3).'showMessage("bottombar","Configurazione per questo '.$_conf_console_app_hardware_device_code.' presa in carico correttamente, ma e necessario riavviare manualmente","'.$_conf_console_html_color_code["yellow"].'",true);';
$_inner_js .= codeChr(1,2).'}else{';
$_inner_js .= codeChr(1,3).'showMessage("bottombar","Configurazione per questo '.$_conf_console_app_hardware_device_code.' presa in carico correttamente.","'.$_conf_console_html_color_code["green"].'",true);';
$_inner_js .= codeChr(1,2).'}';
$_inner_js	.=codeChr(1,2).'edit_level = $(xml).find("edit_level").attr("value");'
			.codeChr(1,2).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
			.codeChr(1,2).'else $("#bottombar_button_apply_image_light").hide();'
			.codeChr(1,2).'var reset_required = $(xml).find("reset_required").attr("value");'
			.codeChr(1,2).'if (reset_required > 0){'
				.codeChr(1,3).'waitingStart("Sono necessari "+(reset_required)+" riavvii della macchina per effettuare le impostazioni richieste.<br>Il sistema sara\' di nuovo disponibile tra massimo "+(reset_required*3.0)+" minuti.");'
				.codeChr(1,3).'var delay = reset_required*3*1000*60;'
				.codeChr(1,3).'setTimeout("reloadAfterReboot()",delay);'
			.codeChr(1,2).'}'
			;
			
$_inner_js .= codeChr(1,1).'}else{';
$_inner_js .= codeChr(1,2).'showMessage("bottombar","Errore durante la presa in carico della configurazione per questo '.$_conf_console_app_hardware_device_code.'.","'.$_conf_console_html_color_code["red"].'",true);';
$_inner_js .= codeChr(1,1).'}';
$_worker = new ajaxWorker( "Apply", false, 0, $_inner_js );
$_js .= $_worker->BuildJavaScript("xml","'force_edit_level='+force_edit_level",true);


// Script AJAX per impostazione Modalita' STLC1000
unset($_inner_js);
unset($_worker);
$_inner_js = codeInit();
$_inner_js .= codeChr(1,1).'var result = $(xml).find("result").attr("value");';
$_inner_js .= codeChr(1,1).'if (result == "success"){';
$_inner_js .= codeChr(1,2).'var mode = $(xml).find("mode").attr("value");';
$_inner_js .= codeChr(1,2).'var mode_string;';
$_inner_js .= codeChr(1,2).'ajaxStatusSender(true);';
$_inner_js .= codeChr(1,2).'if (mode == "MAINTENANCE") mode_string = "di manutenzione";';
$_inner_js .= codeChr(1,2).'else mode_string = "normale";';
$_inner_js .= codeChr(1,2).'showMessage("bottombar","Impostazione della <b>modalit&agrave; "+mode_string+"</b> per questo '.$_conf_console_app_hardware_device_code.' eseguita correttamente.","'.$_conf_console_html_color_code["green"].'",true);';
$_inner_js .= codeChr(1,1).'}else{';
$_inner_js .= codeChr(1,2).'showMessage("bottombar","Impostazione della modalit&agrave; per questo '.$_conf_console_app_hardware_device_code.' fallita.","'.$_conf_console_html_color_code["red"].'",true);';
$_inner_js .= codeChr(1,1).'}';
$_worker = new ajaxWorker( "Mode", false, 0, $_inner_js );
$_js .= $_worker->BuildJavaScript("xml","'mode=maintenance'",true);

// Script jQuery per mostrare una finestra di dialogo
$_js .= codeChr(1,0).codeFunctionComment("Funzione per mostrare una finestra di dialogo",0);
$_js .= codeChr(1,0).'function showDialog(dialog,title,text,buttons,modal){'
			// Recupero gli elementi
			.codeChr(1,1).'var div_id = "#"+dialog+"_dialog";'
			.codeChr(1,1).'var p_id = "#"+dialog+"_dialog_p";'
			.codeChr(1,1).'var $div = $(div_id);'
			.codeChr(1,1).'var $p = $(p_id);'
			// Istanzio il dialog
			.codeChr(1,1).'$div.dialog();'
			// Imposto il titolo
			.codeChr(1,1).'$div.dialog("option","title",title);'
			// Imposto il testo
			.codeChr(1,1).'$p.html(text);'
			// Imposto i parametri di default			
			.codeChr(1,1).'$div.dialog("option","autoOpen",false);'
			.codeChr(1,1).'$div.dialog("option","draggable",false);'
			.codeChr(1,1).'$div.dialog("option","position","center");'
			.codeChr(1,1).'$div.dialog("option","resizable",false);'
			.codeChr(1,1).'$div.dialog("option","stack",true);'
			.codeChr(1,1).'$div.dialog("option","zIndex",2000);'
			// Imposto se modale o non modale
			.codeChr(1,1).'$div.dialog("option","modal",modal);'
			// Imposto i bottoni
			.codeChr(1,1).'$div.dialog("option","buttons",buttons);'
			// Visualizzo la finestra
			.codeChr(1,1).'$div.dialog("open");'
		.codeChr(1,0).'}';

// Variabili globali per i messaggi
$_js .= codeChr(1,0).'var topbar_message_stop_hide = false;';
$_js .= codeChr(1,0).'var bottombar_message_stop_hide = false;';

// Script jQuery per mostrare un messaggio
$_js .= codeChr(1,0).codeFunctionComment("Funzione per mostrare un messaggio",0);
$_js .= codeChr(1,0).'function showMessage(bar,text,textColor,autoClose){'
			.codeChr(1,1).'if ($("#"+bar+"_message").css("display")!="none" && bar=="topbar") topbar_message_stop_hide=true;'
			.codeChr(1,1).'if ($("#"+bar+"_message").css("display")!="none" && bar=="bottombar") bottombar_message_stop_hide=true;'
			.codeChr(1,1).'var element = "#"+bar+"_message_p";'
			.codeChr(1,1).'var $element = $(element);'
			.codeChr(1,1).'var htmlText = "<p>"+text+"</p>";'
			.codeChr(1,1).'$element.html(htmlText);'
			.codeChr(1,1).'$("#"+bar+"_message").find("p").css("color",textColor);'
			.codeChr(1,1).'$("#"+bar+"_message").click(function() {'
				.codeChr(1,2).'hideMessage(bar);'
			.codeChr(1,1).'});'
			.codeChr(1,1).'$("#"+bar+"_message").slideDown(\'fast\');'
			.codeChr(1,1).'if (autoClose==true && bar=="topbar") setTimeout(function(){hideMessage("topbar")},'.$_conf_console_message_autoclose_timeout.');'
			.codeChr(1,1).'else if (autoClose==true && bar=="bottombar") setTimeout(function(){hideMessage("bottombar")},'.$_conf_console_message_autoclose_timeout.');'
		.codeChr(1,0).'}';
		
// Script jQuery per nascondere un messaggio
$_js .= codeChr(1,0).codeFunctionComment("Funzione per nascondere un messaggio",0);
$_js .= codeChr(1,0).'function hideMessage(bar){'
			.codeChr(1,1).'if ((bar=="topbar" && topbar_message_stop_hide==false)'
			.codeChr(1,1).'||(bar=="bottombar" && bottombar_message_stop_hide==false)) $("#"+bar+"_message").slideUp(\'fast\');'
			.codeChr(1,1).'if (bar=="topbar" && topbar_message_stop_hide==true) topbar_message_stop_hide=false;'
			.codeChr(1,1).'if (bar=="bottombar" && bottombar_message_stop_hide==true) bottombar_message_stop_hide=false;'
		.codeChr(1,0).'}';
		
// Gestione Click Livello Zoom Generica
$_js .=	codeChr(1,3).'function navbar_selector_buttonClick(level){'
			.codeChr(1,4).'details_button_closeClick();'
			.codeChr(1,4).'add_button_closeClick();'
			.codeChr(1,4).'navbar_active_level = level;'
			// Nascondo tutti i selettori
			.codeChr(1,4).'$(".navbar_selector").hide();'
			// Visualizzo solo il selettore interessato
			.codeChr(1,4).'var i = "#navbar_"+level+"_selector";'
			.codeChr(1,4).'$(i).show();'
			// Nascondo tutti i filtri
			.codeChr(1,4).'device_status_filter_offline_active=true;'
			.codeChr(1,4).'device_status_filter_error_active=true;'
			.codeChr(1,4).'device_status_filter_warning_active=true;'
			.codeChr(1,4).'device_status_filter_ok_active=true;'
			.codeChr(1,4).'device_status_filter_unknown_active=true;'
			.codeChr(1,4).'$(".navbar_filter").hide();'
			// Visualizzo solo i filtri interessati
			.codeChr(1,4).'var f = ".navbar_"+level+"_filter";'
			.codeChr(1,4).'$(f).show();'
			// Verifico se devo spegnere il filtro IeC
			.'
			if (level=="device"){
				if (device_status_filter_iec_active == false){
					$("#navbar_iec_selected").hide();
				}
				else{
					$("#navbar_iec_selected").show();
				}
			}
			'
			// Nascondo tutti i menu
			.codeChr(1,4).'$(".navbar_menu").hide();'
			// Attivo solo il menu interessato
			.codeChr(1,4).'navbar_filter_menuActivate(level);'
			.codeChr(1,4).'$("#content_middle").empty();'
			.codeChr(1,4).'ajax'.$_page_ajax_workers_prefix.'InfoSender(true,true);'
		.codeChr(1,3).'}';

// Codice gestione click bottoni barra inferiore

/*
$_js .= codeChr(1,0).codeFunctionComment("Funzione per gestire il click sul bottone upload",0);
$_js .= codeChr(1,0).'function bottombar_button_uploadClick(){'
			.codeChr(1,1).'var next=confirm("Verra\' caricata una configurazione da un file system.xml presente sul tuo computer. Vuoi procedere comunque?");'
			.codeChr(1,1).'if (next== true){'
				.codeChr(1,2).'var input = $("#bottombar_button_upload").children("input");'
				.codeChr(1,2).'input.change();'
			.codeChr(1,1).'}else{}'
		.codeChr(1,0).'}';
		*/
		
$_js .= "\nfunction ajaxGetCurrentConfigurationError(){\n";
$_js .= "       showMessage(\"bottombar\",\"Configurazione non esistente\",\"".$_conf_console_html_color_code["red"]."\",true);\n";
$_js .= "}\n";

$_js .= "\nfunction ajaxGetCurrentConfigurationSuccess(){\n";
$_js .= "window.open(\"xml/sk_getconfigurationcurrent.xml.php\");\n";
$_js .= "}\n";

// funzione per scaricare la configurazione attualmente in uso
$_js .= "\nfunction bottombar_button_downloadClick(){\n";
$_js .= "\$.ajax({\n";
$_js .= "    url:'xml/sk_getconfigurationcurrent.xml.php',\n";
$_js .= "    type:'GET',\n";
$_js .= "    timeout: 60000,\n";
$_js .= "    error: ajaxGetCurrentConfigurationError,\n";
$_js .= "    success: ajaxGetCurrentConfigurationSuccess\n";
$_js .= "});\n";
$_js .= "}\n";

$_js .= codeChr(1,0).codeFunctionComment("Funzione per gestire il click sul bottone revert",0);
$_js .= codeChr(1,0).'function bottombar_button_revertClick(){'
			.codeChr(1,1).'var next=confirm("La precedente configurazione verra\' ricaricata sostituendo quella attualmente in uso. Vuoi procedere comunque?");'
			.codeChr(1,1).'if (next== true){'
				.codeChr(1,2).'ajaxRevertSender(true);'
			.codeChr(1,1).'}else{}'
		.codeChr(1,0).'}';
		
$_js .= codeChr(1,0).codeFunctionComment("Funzione per gestire il click sul bottone apply",0);
$_js .= codeChr(1,0).'function bottombar_button_applyClick(){'
			.codeChr(1,1).'var hostname_cfg = $("#details_panel_info_host_value").html();'
			.codeChr(1,1).'var hostname_active = $("#details_panel_info_host_active_value").html();'
			.codeChr(1,1).'if (hostname_active != null && hostname_active != "" && hostname_active != hostname_cfg){'
				.codeChr(1,2).'var next=confirm("L\'hostname configurato ("+hostname_cfg+") e\' diverso da quello rilevato ("+hostname_active+"). Applicando la modifica questo '.$_conf_console_app_hardware_device_code.' verra\' riavviato. Vuoi procedere comunque?");'
				.codeChr(1,2).'if (next== true){force_edit_level=2;ajaxApplySender(true);force_edit_level=null;}';
// il messaggio deve comparire solo nel caso sia in presenza di un STLC1000 e quando la configurazione di rete � stata cambiata
if ( $_conf_console_app_hardware_device_code === 'STLC1000'){
	$_js	.=		 codeChr(1,1).'}else {';
	$_js	.=		"ajaxIsChangedNetConfiguration();}";
} else {
$_js	.=		codeChr(1,1).'}else ajaxApplySender(true);';
}
$_js .= codeChr(1,0).'}';
		
// se � un stlc1000 allora creo le funzioni per ottenere dinamicamente le info sui cambiamenti di stato
if($_conf_console_app_hardware_device_code === 'STLC1000'){
	$_js .= "\nfunction ajaxIsChangedNetConfigurationError(){\n";
	$_js .= 'showMessage("bottombar","Errore nel contattare il server. Configurazione non applicabile","'.$_conf_console_html_color_code["red"].'",true);';
	$_js .= "}\n";
	
	$_js .= "\nfunction ajaxIsChangedNetConfigurationSuccess(xml){\n";
	$_js .= 'var result_isChanged = $(xml).find("result_isChanged").attr("value");';
	// parametri non cambiati
	$_js .= "\nif(result_isChanged == '0'){";
	$_js .= "\najaxApplySender(true);";
	// parametri cambiati
	$_js .= "\n} else if(result_isChanged == '1'){";
	$_js .= 'var next=confirm("Alcuni parametri di rete di questo '.$_conf_console_app_hardware_device_code.' sono stati modificati. Le modifiche effettuate potrebbero richiedere uno o piu\' riavvii della macchina. Vuoi procedere comunque?");';
	$_js .= 'if (next== true) ajaxApplySender(true);';
	// parametri rilevati non completi
	$_js .= "\n} else if(result_isChanged == '2'){";
	$_js .= 'var next=confirm("Alcuni parametri di rete di questo '.$_conf_console_app_hardware_device_code.' non sono stati rilevati. Le modifiche effettuate potrebbero richiedere uno o piu\' riavvii della macchina. Vuoi procedere comunque?");';
	$_js .= 'if (next== true) ajaxApplySender(true);';
	$_js .= "\n} else {";
	$_js .= 'showMessage("bottombar","Sessione scaduta","'.$_conf_console_html_color_code["red"].'",true);';
	$_js .= "}\n";
	$_js .= "}\n";
	
	$_js .= "\nfunction ajaxIsChangedNetConfiguration(){\n";
	$_js .= "\$.ajax({\n";
	$_js .= "    url:'xml/sk_getIsChangedNetConfiguration.xml.php',\n";
	$_js .= "    type:'GET',\n";
	$_js .= "    timeout: 60000,\n";
	$_js .= "    error: ajaxIsChangedNetConfigurationError,\n";
	$_js .= "    success: ajaxIsChangedNetConfigurationSuccess\n";
	$_js .= "});\n";
	$_js .= "}\n";
}
		
$_js .= codeChr(1,0).codeFunctionComment("Funzione per gestire il click sul bottone reload",0);
$_js .= codeChr(1,0).'function bottombar_button_reloadClick(){'
			.codeChr(1,1).'var next=confirm("La configurazione attualmente in uso verra\' ricaricata e le eventuali modifiche effettuate andranno perse. Vuoi procedere comunque?");'
			.codeChr(1,1).'if (next== true){'
				.codeChr(1,2).'waitingStart();'
				.codeChr(1,2).'var url = "?reload=true";'
				.codeChr(1,2).'exit_nocheck = true;'
				.codeChr(1,2).'window.location = url;'
			.codeChr(1,1).'}else{}'
		.codeChr(1,0).'}';
		
$_js .= codeChr(1,0).codeFunctionComment("Funzione per gestire il click sul bottone maintenance",0);
$_js .= codeChr(1,0).'function bottombar_button_maintenanceClick(){'
			.codeChr(1,1).'ajaxModeSender(true);'
		.codeChr(1,0).'}';
		
$_js .= codeChr(1,0).codeFunctionComment("Funzione per effettuare un reload dopo una serie di reboot",0);
$_js .= codeChr(1,0).'function reloadAfterReboot(){'
			.codeChr(1,1).'waitingStart();'
			.codeChr(1,1).'var url = "?reload=true";'
			.codeChr(1,1).'exit_nocheck = true;'
			.codeChr(1,1).'window.location = url;'
		.codeChr(1,0).'}';

$_js .= jsFormCharCheck();

// Gestione rotellina di attivita AJAX		
$_js .= codeChr(1,3).'var wheelActive=0;'
		.codeChr(1,3).'var wheelStep=0;'
		// Funzione start rotellina
		.codeChr(1,3).'function bottombar_wheelStart(){'
			.codeChr(1,4).'wheelActive++;'
		.codeChr(1,3).'}'
		// Funzione stop rotellina
		.codeChr(1,3).'function bottombar_wheelStop(){'
			.codeChr(1,4).'if (wheelActive>0) wheelActive--;'
			.codeChr(1,4).'if (wheelActive==0) wheelStep = 0;'
			.codeChr(1,5).'$(".bottombar_wheel").hide();'
		.codeChr(1,3).'}'
		// Funzione animazione rotellina
		.codeChr(1,3).'function bottombar_wheelAnimate(){'
			.codeChr(1,4).'if (wheelActive>0){'
				// Mostro lo step attuale della rotellina e nascondo quello precedente
				.codeChr(1,5).'var thisWheelStep = wheelStep;'
				.codeChr(1,5).'var thisWheel = "#wheel_"+thisWheelStep;'
				.codeChr(1,5).'var prevWheel = "#wheel_"+((thisWheelStep>0)?(thisWheelStep-1):"11");'
				.codeChr(1,5).'$(thisWheel).show();'
				.codeChr(1,5).'$(prevWheel).hide();'
				// Incremento lo step
				.codeChr(1,5).'if (wheelStep<=10) wheelStep++;'
				.codeChr(1,5).'else wheelStep=0;'
			.codeChr(1,4).'}'
			.codeChr(1,4).'setTimeout("bottombar_wheelAnimate()",50);'
		.codeChr(1,3).'}';
		
// Gestione Waiting Animation		
$_js .= codeChr(1,3).'var waitingActive=false;'
		.codeChr(1,3).'var waitingStep=0;'
		// Funzione testo waiting
		.codeChr(1,3).'function waitingText(text){'
			.codeChr(1,4).'if (text != null){'
				.codeChr(1,5).'$("#waiting_text").html(text);'
				.codeChr(1,5).'$("#waiting_text").show();'
			.codeChr(1,4).'}else{'
				.codeChr(1,5).'$("#waiting_text").empty();'
				.codeChr(1,5).'$("#waiting_text").hide();'
			.codeChr(1,4).'}'
		.codeChr(1,3).'}'
		// Funzione start waiting
		.codeChr(1,3).'function waitingStart(text){'
			.codeChr(1,4).'waitingActive = true;'
			.codeChr(1,4).'$("#waiting").show();'
			.codeChr(1,4).'if (text != null){'
				.codeChr(1,5).'waitingText(text);'
			.codeChr(1,4).'}else{'
				.codeChr(1,5).'waitingText(null);'
			.codeChr(1,4).'}'
		.codeChr(1,3).'}'
		// Funzione stop waiting
		.codeChr(1,3).'function waitingStop(){'
			.codeChr(1,4).'waitingActive = false;'
			.codeChr(1,4).'waitingStep = 0;'
			.codeChr(1,4).'$("#waiting").hide();'
			//.codeChr(1,5).'$("#waiting").hide();'
		.codeChr(1,3).'}'
		// Funzione animazione rotellina
		.codeChr(1,3).'function waitingAnimate(){'
			.codeChr(1,4).'if (waitingActive){'
				.codeChr(1,5).'$(".waiting_icon").hide();'
				.codeChr(1,5).'$("#waiting_"+waitingStep).show();'
				.codeChr(1,5).'if (waitingStep<=10) waitingStep++;'
				.codeChr(1,5).'else waitingStep=0;'
			.codeChr(1,4).'}'
			.codeChr(1,4).'setTimeout("waitingAnimate()",50);'
		.codeChr(1,3).'}';

$_page = coreGetPageFromSession();
if ($_page == "devices") $_js_addpanel = codeChr(1,1).'addPanelLinkMouseWheelEvents();';
else $_js_addpanel = "";

// Codice per attivare gli ascoltatori eventi
$_js .= codeChr(1,0).codeFunctionComment("Attivazione ascoltatori eventi quando la pagine e' pronta",0);
$_js .= codeChr(1,0).'$(document).ready(function() {'
			// Escludo gli eventi di selezione e di immagini e testi
      		.codeChr(1,1).'$("img").mousedown(function(event){'.jsEventPreventDefault().'})'
      		.codeChr(1,1).'$("p").mousedown(function(event){'.jsEventPreventDefault().'})'
      		//.codeChr(1,1).'$("#content_add").find("*").mousedown(function(event){'.jsEventPreventDefault().'})'
      		/*
      		.codeChr(1,1).'$("img").selectStart(function(event){event.returnValue = false;})'
      		.codeChr(1,1).'$("p").selectStart(function(event){event.returnValue = false;})'
      		.codeChr(1,1).'$("#content_add").find("*").selectStart(function(event){event.returnValue = false;})'
      		*/
      		.$_js_addpanel
      		// Manager eventi bottoni pagine barra superiore
      		.jsPageButtonEventManager("topbar")
      		// Manager eventi bottoni barra superiore
      		.jsBarButtonEventManager("topbar")
      		// Manager eventi bottoni barra inferiore
      		.jsBarButtonEventManager("bottombar")
      		
      		.jsBuildDownloadButton()
      		.jsBuildUploadButton()
      	.codeChr(1,0).'})';

$_js .= jsBuildUploader();

print($_js);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(jsBuildComment("JavaScript built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));
?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_serialconfig.xml.php - Modulo per la configurazione dei parametri di una porta seriale in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.1 06/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

// Recupero la modalita'
$_action = varGetRequest("action");

$_data = array();
$_data['action'] = $_action;

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_ports				= $_configuration["ports"];
$_port_type_list	= $_configuration["port_type_list"];

// Gestire il form per modificare una porta seriale
if ($_action == "edit_form"){
	$_item_id	= varGetRequest("id");
	$_type_name	= varGetRequest("type");
	
	$_data['id'] 		= $_item_id;
	$_data['type_name'] = $_type_name;
	
	$_item			= getPortFromId($_ports,$_item_id);
	$_item_index	= getPortIndexFromId($_ports,$_item_id);
		
	if (isset($_item))
	{
		$_new_type	= getPortTypeFromName($_port_type_list,$_type_name);
				
		$_data['new_type'] 	= $_new_type->code;
		$_data['old_type'] 	= $_item->type;
		
		// Cambio tipo porta
		if ($_data['new_type'] != $_data['old_type']){
			// Recupero valori di default per il nuovo tipo di porta
			$_baud		= $_new_type->defaultBaud;
			$_echo 		= $_new_type->defaultEcho;
			$_data_bit	= $_new_type->defaultData;
			$_stop		= $_new_type->defaultStop;
			$_parity	= $_new_type->defaultParity;
			$_timeout	= $_new_type->defaultTimeout;
		}	
		// Tipo porta invariato
		else{
			// Recupero valori configurati
			$_baud		= $_item->baud;
			$_echo 		= $_item->echo;
			$_data_bit	= $_item->data;
			$_stop		= $_item->stop;
			$_parity	= $_item->parity;
			$_timeout	= $_item->timeout;
		}
				
		$_data['baud']		= $_baud;
		$_data['echo']		= $_echo;
		$_data['data']		= $_data_bit;
		$_data['stop']		= $_stop;
		$_data['parity']	= $_parity;
		$_data['timeout']	= $_timeout; // Attualmente non editabile nel form
		
		$_data['result'] = 'success';
		$_data['silent'] = 'true';
	}
	else{
		$_data['result'] 		= 'failure';
		$_data['description'] 	= 'Porta seriale non valida.';
	}
}
else if ($_action == "edit"){
	$_item_id	= varGetRequest("id");
	
	$_name		= varGetRequest("name");
	$_type_name	= varGetRequest("type");
	$_com		= varGetRequest("com");
	$_baud		= varGetRequest("baud");
	$_echo		= varGetRequest("echo");
	$_data_bit	= varGetRequest("data");
	$_stop		= varGetRequest("stop");
	$_parity	= varGetRequest("parity");
	
	$_item			= getPortFromId($_ports,$_item_id);
	$_item_index	= getPortIndexFromId($_ports,$_item_id);
	
	if (isset($_item))
	{
		$_type				= getPortTypeCodeFromName($_port_type_list,$_type_name);
	
		$_item->type 		= $_type;
		$_item->name 		= $_name;
		/*
		$_item->timeout 	= $_timeout;
		$_item->persistent 	= $_persistent;
		*/
		$_item->com 		= $_com;
		$_item->baud 		= $_baud;
		$_item->echo 		= $_echo;
		$_item->data 		= $_data_bit;
		$_item->stop 		= $_stop;
		$_item->parity 		= $_parity;
		/*
		$_item->ip 			= $_ip;
		$_item->port		= $_port;
		$_item->subnet 		= $_subnet;
		$_item->gateway 	= $_gateway;
		$_item->dns1 		= $_dns1;
		$_item->dns2 		= $_dns2;
		*/
		
		$_ports[$_item_index]	= $_item;
		
		$_configuration["ports"] = $_ports;
		
		coreSetConfigurationToSession($_configuration);
		
		$_data['edit_level'] 	= "1";
		coreSetEditLevelToSession(1,true);
		
		$_data['result'] = 'success';
		$_data['description'] = 'Porta seriale modificata con successo.';
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Porta seriale non valida.';
	}
}
else{
	$_data['result'] = 'failure';
	$_data['description'] = 'Parametro \'action\' non riconosciuto.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
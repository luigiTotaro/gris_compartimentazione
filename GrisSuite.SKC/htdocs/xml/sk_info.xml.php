<?php
/**
* Telefin STLC1000 Consolle
*
* sk_info.xml.php - Modulo per per recuperare le info STLC1000 in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.11 27/06/2013
* @copyright 2011-2013 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");

require_once("../modules/sk_core.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_app_stlcManagerService.php');

$_data = array();

if(isAliveStlcManagerService() === true){
	$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
}

if (isset($_client))
{	
	// Preparo i parametri per la richiesta SOAP
	$_request->clientType = 'SKC';
	$_params->request = $_request;

	// Chiamo il metodo SOAP con i parametri che ho generato
	$_result = $_client->getSTLCInfo($_params);

	if (isset($_result))
	{
		// Recupero i dati del risultato
		$_result_data = $_result->getSTLCInfoResult;
	
		if (isset($_result_data))
		{
			if ($_result_data->status->level == "SUCCESS")
			{
				
				
				$_info_data = $_result_data->data;
				
				// Converto SerialNumber STLC1000 e salvo come SrvID
				$_srvid = serialNumberToServerId($_info_data->serialNumber);
				
				// Recupero l'attuale configurazione
				$_configuration	= coreGetConfigurationFromSession();
				
				$_info = $_configuration["info"];
				
				// Salvo SerialNumebr e Versione STLC1000
				$_info["sk_sn"]	= $_info_data->serialNumber;
				$_info["sk_version"] = $_info_data->version;
				$_info["sk_hostname"] = $_info_data->hostName;
				$_info['sk_id'] = $_srvid;
				
				$_configuration["info"] = $_info;
				/*
				$_server = getServer($_servers);
				$_server->id = $_srvid;
				$_servers[0] = $_server;
				$_configuration["servers"] = $_servers;
				*/
				
				coreSetConfigurationToSession($_configuration);
			
				// Risultato
				$_data['result'] = 'success';
				$_data['description'] = "Informazioni recuperate con successo.";
				$_data['serial_number'] = $_info_data->serialNumber; 
				$_data['srvid'] = $_srvid;
				$_data['version'] = $_info_data->version;
				$_data['hostname'] = $_info_data->hostName;
				
				session_start();
				
				if ((isset($_SESSION['local_region_list']) && $_SESSION['local_region_list']===true)
				|| (isset($_SESSION['local_device_type_list']) && $_SESSION['local_device_type_list']===true)){
					// Notifica mancato download Region List e/o Device Type List
					if (isset($_SESSION['local_region_list']) && $_SESSION['local_region_list']===true){
						$_desc_1 = "Impossibile scaricare Region List da server remoto. ";
						$_SESSION['local_region_list'] = false;
					}
					if (isset($_SESSION['local_device_type_list']) && $_SESSION['local_device_type_list']===true){
						$_desc_2 = "Impossibile scaricare Device Type List da server remoto. ";
						$_SESSION['local_device_type_list'] = false;
					}
					$_data['result'] = 'failure';
					$_data['description'] =	$_desc_1.$_desc_2;
				}
				
				session_write_close();
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = $_result_data->errorDescription;
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Errore durante l\'esecuzione del comando.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Impossibile connettere il client al server.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

?>
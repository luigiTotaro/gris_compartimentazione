<?php
/**
* Telefin STLC1000 Consolle
*
* sk_apply.xml.php - Modulo per applicare la configurazione STLC1000 in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.4.3 22/10/2015
* @copyright 2011-2015 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

require_once("../modules/sk_app_stlcManagerService.php");



$_data = array();

// === FASE 1 = Salvataggio system.xml ===
$_conf_app0 = $_conf_apps[0];
$now   = new DateTime();
$_timestamp = $now->format( 'YmdHis' );
$_config_url = $_conf_app0["config_url"];
$_config_prev_url = str_replace(".xml", "", $_config_url)."_prev.xml";
$_config_back_url = str_replace(".xml", "", $_config_url)."_".$_timestamp.".xml";

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession(false);

// Salvo prev
copy($_config_url,$_config_prev_url);
// Salvo nuovo
$_return = spvstandardSaveXMLConfig($_conf_app0["config_url"],$_configuration);
// Salvo backup
copy($_config_url,$_config_back_url);

$_cnt = true;

// controllo che il stlcmanager sia vivo
$_cntAliveStlcManagerService = isAliveStlcManagerService();

// se siamo in presenza di un STLC1000, salvo il file di configurazione senza riavviare
if (isset($_conf_console_app_hardware_device_code) && $_conf_console_app_hardware_device_code !== 'STLC1000'){
	$_cnt = false;
}

if ($_return != false && $_cntAliveStlcManagerService === true)
{	
	$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");

	if (isset($_client))
	{	
		// === FASE 2.A = Ricaricamento system.xml ===
		
		// Preparo i parametri per la richiesta SOAP
		$_request->clientType = 'SKC';
		$_request->mode = 'NORMAL';
		
		$_params->request = $_request;
		
		$_result = $_client->reloadSystemXmlIntoDatabase($_params);
		
		if (isset($_result))
		{
			// Recupero i dati del risultato
			$_result_data = $_result->reloadSystemXmlIntoDatabaseResult;
	
			if (isset($_result_data))
			{
				if ($_result_data->status->level == "SUCCESS")
				{
					// Risultato
					$_data['result'] = 'success';
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = $_result_data->status->errorDescription;
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante l\'esecuzione del comando.';
		}
		/*
		// === FASE 2.B = Riavvio del servizio SCAgent ===
		
		// Procedo solo se la fase precedente è stata eseguita con successo
		if ($_data['result'] == 'success')
		{
			// Preparo i parametri per la richiesta SOAP
			$_request->clientType = 'SKC';
			$_request->command = 'RESTART';
			$_request->services = array(0 => 'StlcSCAgentService' );
			
			$_params->request = $_request;

			// Chiamo il metodo SOAP con i parametri che ho generato
			$_result = $_client->setServiceStatus($_params);
	
			if (isset($_result))
			{
				// Recupero i dati del risultato
				$_result_data = $_result->setServiceStatusResult;
		
				if (isset($_result_data))
				{
					if ($_result_data->status->level == "SUCCESS")
					{
						// Risultato
						$_data['result'] = 'success';
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = $_result_data->status->errorDescription;
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Errore durante l\'esecuzione del comando.';
			}
		
			$_edit_level = coreGetEditLevelFromSession(true);
		}
		*/
		
		// === FASE 2.C = Impostazione NORMAL MODE ===
		// Procedo solo se la fase precedente è stata eseguita con successo
		if ($_edit_level == 0 || $_edit_level == 1)
		{
			/*
			// Procedo solo se la fase precedente è stata eseguita con successo
			if ($_data['result'] == 'success')
			{
				// Recupero l'attuale configurazione
				$_configuration	= coreGetConfigurationFromSession();
				$_info = $_configuration["info"];
				$_mode = $_info["sk_mode"];
				
				// Procedo solo se la modalità non è già NORMAL
				if ($_mode != "NORMAL")
				{
					// Preparo i parametri per la richiesta SOAP
					$_mode = "NORMAL";
					$_request->clientType = 'SKC';
					$_request->mode = $_mode;
					$_params->request = $_request;
					
					// Chiamo il metodo SOAP con i parametri che ho generato
					$_result = $_client->setSTLCMode($_params);
					
					if (isset($_result))
					{
						// Recupero i dati del risultato
						$_result_data = $_result->setSTLCModeResult;
						
						if (isset($_result_data))
						{
							if ($_result_data->status->level == "SUCCESS")
							{
								$_edit_level = 0;
								$_data['edit_level'] 		= $_edit_level;
								$_data['reset_required'] 	= 0;
								
								// Risultato
								$_data['result'] = 'success';
				
								$_info["sk_mode"] = $_mode;
								$_configuration["info"] = $_info;
								
								coreSetConfigurationToSession($_configuration);
								
								$_data['mode'] = $_mode;
							}
							else
							{
								$_data['result'] = 'failure';
								$_data['description'] = $_result_data->status->errorDescription;
							}
						}
						else
						{
							$_data['result'] = 'failure';
							$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
						}
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = 'Errore durante l\'esecuzione del comando.';
					}
				}
				else
				{
					$_edit_level = 0;
					$_data['edit_level'] 		= $_edit_level;
					$_data['reset_required'] 	= 0;
				}
			}
			*/
		}
		
		// === FASE 3 = Cambio IP e Hostname ===
		
		else if ($_edit_level == 2 && $_cnt === true)
		{
			if ($_data['result'] == 'success')
			{
				// Preparo i parametri per la richiesta SOAP
				$_request->clientType = 'SKC';
				$_params->request = $_request;
	
				// Chiamo il metodo SOAP con i parametri che ho generato
				$_result = $_client->applySystemConfig($_params);
	
				if (isset($_result))
				{
					// Recupero i dati del risultato
					$_result_data = $_result->applySystemConfigResult;
	
					if (isset($_result_data))
					{
						if ($_result_data->status->level == "SUCCESS")
						{
							$_edit_level = 0;
							$_data['edit_level'] 		= $_edit_level;
							$_data['reset_required'] 	= $_result_data->status->resetRequired;
							
							// Risultato
							$_data['result'] = 'success';
						}
						else
						{
							$_data['result'] = 'failure';
							$_data['description'] = $_result_data->status->errorDescription;
						}
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Errore durante l\'esecuzione del comando.';
				}
			}
		}
		else
		{
			$_edit_level = 0;
			$_data['edit_level'] 		= $_edit_level;
			$_data['reset_required'] 	= 0;
				
			// Risultato
			$_data['result'] = 'success';
		}
		
		coreSetEditLevelToSession($_edit_level,true);
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Impossibile connettere il client al server.';
	}
} else if($_cntAliveStlcManagerService === false && $_return != false){
	$_data['result'] = 'success';
	$_data['description'] = 'Salvataggio riuscito, ma StlcManagerService down';
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = "Errore durante il salvataggio del file di configurazione.";
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
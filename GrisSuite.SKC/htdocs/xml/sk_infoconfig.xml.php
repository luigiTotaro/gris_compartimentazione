<?php
/**
* Telefin STLC1000 Consolle
*
* sk_rackconfig.xml.php - Modulo per la configurazione delle informazioni STLC1000 in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.1 06/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

// Recupero la modalita'
$_action = varGetRequest("action");

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_data = array();
$_data['action'] = $_action;

$_servers			= $_configuration["servers"];

// Modificare STLC1000
if ($_action == "edit")
{
	$_host			= varGetRequest("hostname");
	$_srvid			= varGetRequest("id");
	
	$_server 		= getServer($_servers);
	
	if (isset($_server))
	{
		if (isset($_srvid) && $_srvid != "")
		{
			if (isset($_host) && $_host != "")
			{
				$_server->id = $_srvid;
				$_server->host = $_host;
				
				$_configuration["edit"]["host"] = $_host;
				$_configuration["edit"]["srvid"] = $_srvid;
		
				$_servers[0] = $_server;
				
				$_configuration["servers"] = $_servers;
					
				// Salvo la configurazione in sessione
				coreSetConfigurationToSession($_configuration);
				
				$_data['edit_level'] 	= "2";
				coreSetEditLevelToSession(2,true);
									
				$_data['result'] = 'success';
				$_data['description'] = 'Informazioni modificate con successo.';
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Hostname non valido.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'ID non valido.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Oggetto server non valido.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Parametro \'action\' non riconosciuto.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_buildingconfig.xml.php - Modulo per la configurazione dei parametri di un edificio in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.1 06/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

// Recupero la modalita'
$_action = varGetRequest("action");

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_info				= $_configuration["info"];

$_data = array();
$_data['action'] = $_action;


$_building_type_list	= $_configuration['building_type_list'];

$_stations			= $_configuration["stations"];
$_buildings			= $_configuration["buildings"];

// Aggiungere un edificio
if ($_action == "add")
{
	$_name			= varGetRequest("name");
	$_station_name	= varGetRequest("station");
	$_note			= varGetRequest("note");
	$_type_name		= varGetRequest("type");
	
	$_station_id 	= getStationIdFromStationLongName($_stations,$_station_name);
	$_type 			= getBuildingTypeCodeFromName($_building_type_list,$_type_name);	
	
	if (isset($_station_id))
	{
		if (isset($_type))
		{
			if (isset($_name) && $_name != "")
			{
				if (isFreeBuildingName($_buildings,$_station_id,$_name))
				{
					$_building_id	= getFirstFreeBuildingId($_buildings,$_station_id);
				
					if (isset($_building_id))
					{
						if ($_note == "") $_note = "Nessuna";
					
						$_building = new building();
						$_building->id 			= $_building_id;
						$_building->name 		= $_name;
						$_building->type 		= $_type;
						$_building->note 		= $_note;
						$_building->stationId 	= $_station_id;
					
						$_buildings[] = $_building;
						$_configuration["buildings"] = $_buildings;
					
						coreSetConfigurationToSession($_configuration);
						
						$_data['edit_level'] 	= "1";
						coreSetEditLevelToSession(1,true);
					
						$_data['result'] = 'success';
						$_data['description'] = 'Edificio aggiunto con successo.';
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = 'Impossibile assegnare un indice.';
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['name'] = $_name;
					$_data['description'] = 'Nome edificio gia\' utilizzato per questa stazione.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Nome edificio non specificato.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Tipo edificio non valido.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Stazione non valida.';
	}
}
// Modificare un edificio
else if ($_action == "edit")
{
	$_building_id	= varGetRequest("id");
	$_name			= varGetRequest("name");
	$_station_name	= varGetRequest("station");
	$_note			= varGetRequest("note");
	$_type_name		= varGetRequest("type");
	
	$_station_id 	= getStationIdFromStationLongName($_stations,$_station_name);
	$_type 			= getBuildingTypeCodeFromName($_building_type_list,$_type_name);	
	
	if (isset($_station_id))
	{
		if (isset($_type))
		{
			if (isset($_name) && $_name != "")
			{
				$_building_index	= getBuildingIndexFromBuildingId($_buildings,$_building_id);
				$_building 			= getBuildingFromBuildingId($_buildings,$_building_id);
				
				if ($_building->stationId == $_station_id)
					$_building_name = $_building->name;
				else
					$_building_name = null;
						
				if (isset($_building_index))
				{
					if (isFreeBuildingName($_buildings,$_station_id,$_name,$_building_name))
					{
						// Se cambio stazione devo calcolare un nuovo id edificio
						if ($_building->stationId != $_station_id)
						{
							$_racks = $_configuration["locations"];
							$_devices = $_configuration["devices"];
							
							$_building_id = getFirstFreeBuildingId($_buildings,$_station_id);
							
							// Estraggo il node dall'id stazione dell'edificio
							$_station = getStationFromId($_stations,$_station_id);
							$_node_id = $_station->nodeId;
							$_nodes	= $_configuration["nodes"];
							$_node = getNodeFromId($_nodes,$_node_id);
							
							$_zone_id = $_node->zoneId;
							$_region_id = $_node->regionId;
							
							// Aggiorno la topografia di eventuali armadi di questo edificio
							if (rackUpdateTopographyId($_racks,$_building->stationId,$_building->id,$_station_id,$_building_id) == true)
							{
								$_configuration["locations"] = $_racks;
							}
							// Aggiorno la topografia di eventuali periferiche di armadio di questo edificio
							if (deviceUpdateTopographyId($_devices,$_building->stationId,$_building->id,null,$_station_id,$_building_id,null,$_node_id,$_zone_id,$_region_id) == true)
							{
								$_configuration["devices"] = $_devices;
							}
							
							// Eseguo lo shift di eventuali edifici con id successivo a quello eliminato
							$_shifted = buildingShiftNextId($_buildings,$_racks,$_devices,$_building->stationId,$_building->id);
							if ($_shifted)
							{
								$_configuration["buildings"]	= $_buildings;
								$_configuration["locations"]	= $_racks;
								$_configuration["devices"]		= $_devices;
							}
							
							// Salvo il nuovo id edificio
							$_building->id 		= $_building_id;
						}
						
						if ($_note == "") $_note = "Nessuna";
						
						$_building->name 		= $_name;
						$_building->type 		= $_type;
						$_building->note 		= $_note;
						$_building->stationId 	= $_station_id;
					
						$_buildings[$_building_index] = $_building;
						$_configuration["buildings"] = $_buildings;
					
						coreSetConfigurationToSession($_configuration);
						
						$_data['edit_level'] 	= "1";
						coreSetEditLevelToSession(1,true);
					
						$_data['result'] = 'success';
						$_data['description'] = 'Edificio modificato con successo.';
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = 'Nome edificio gia\' utilizzato per questa stazione.';
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['name'] = $_name;
					$_data['id'] = $_building_id;
					$_data['description'] = 'Edificio non valido.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Nome edificio non specificato.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Tipo edificio non valido.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Stazione non valida.';
	}
}
// Eliminare un edificio
else if ($_action == "delete")
{
	$_building_id	= varGetRequest("id");

	$_building_index	= getBuildingIndexFromBuildingId($_buildings,$_building_id);

	if (isset($_building_index))
	{
		$_building = $_buildings[$_building_index];
		if (isset($_building))
		{
			$_racks			= $_configuration["locations"];
			$_devices		= $_configuration["devices"];
		
			if(!buildingIsUsedByRack($_racks,$_building->stationId,$_building->id))
			{
				// Salvo l'id dell'edificio che devo cancellare
				$_deleted_building_id = $_building->id;
				$_deleted_building_station_id = $_building->stationId;
				
				unset($_buildings[$_building_index]);
				$_configuration["buildings"] = $_buildings;
				
				// Eseguo lo shift di eventuali edifici con id successivo a quello eliminato
				$_shifted = buildingShiftNextId($_buildings,$_racks,$_devices,$_deleted_building_station_id,$_deleted_building_id);
				
				if ($_shifted)
				{
					$_configuration["buildings"]	= $_buildings;
					$_configuration["locations"]	= $_racks;
					$_configuration["devices"]		= $_devices;
				}
				
				coreSetConfigurationToSession($_configuration);
				
				$_data['edit_level'] 	= "1";
				coreSetEditLevelToSession(1,true);
				
				$_data['result'] = 'success';
				$_data['description'] = 'Edificio eliminato con successo.';
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'L\'edificio e\' associato ad almeno un armadio.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Oggetto edificio non valido.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Indice edificio non valido.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Parametro \'action\' non riconosciuto.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_network.xml.php - Modulo per per recuperare le info porte ethernet STLC1000 in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.4.1 28/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}


// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");

require_once("../modules/sk_core.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_app_stlcManagerService.php');

$_data = array();
if(isAliveStlcManagerService() === true){
	$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
}

if (isset($_client))
{	
	// Preparo i parametri per la richiesta SOAP
	$_request->clientType = 'SKC';
	$_params->request = $_request;

	// Chiamo il metodo SOAP con i parametri che ho generato
	$_result = $_client->getSTLCNetwork($_params);

	if (isset($_result))
	{
		// Recupero i dati del risultato
		$_result_data = $_result->getSTLCNetworkResult;
	
		if (isset($_result_data))
		{
			if ($_result_data->status->level == "SUCCESS")
			{
				$_network_data = $_result_data->data;
				
				$_network_eth_list = $_network_data->Network->STLCNetwork;
				
				// === Recupero numero ETH ===
				$_eth_count = coreExtractEthernetCount($_network_eth_list);
								
				// Risultato
				$_data['result'] = 'success';
				$_data['description'] = "Informazioni network recuperate con successo.";
				
				$_data['eth_count'] = $_eth_count;
				
				// Recupero l'attuale configurazione
				$_configuration	= coreGetConfigurationFromSession();
				
				$_network_active = $_configuration["network_active"];
				
				for ($_e = 1; $_e <= $_eth_count; $_e++) {
					if ($_eth_count == 1) $_eth = $_network_eth_list;
					else $_eth = $_network_eth_list[$_e-1];
					
					$_eth_params = coreExtractEthernetParams($_eth);
					
					$_data['eth'.$_e.'_name'] 		= $_eth->Name;
					$_data['eth'.$_e.'_auto'] 		= $_eth_params['auto'];
					$_data['eth'.$_e.'_ip'] 		= codeMAddressToIp($_eth_params['ip']->m_Address);
					if (isset($_eth_params['mask']))
						$_data['eth'.$_e.'_mask'] 	= codeMAddressToIp($_eth_params['mask']->m_Address);
					
					if (isset($_eth_params['ip2']))
						$_data['eth'.$_e.'_ip2'] 	= codeMAddressToIp($_eth_params['ip2']->m_Address);
					if (isset($_eth_params['mask2']))
						$_data['eth'.$_e.'_mask2'] 	= codeMAddressToIp($_eth_params['mask2']->m_Address);
					if (isset($_eth_params['ip3']))
						$_data['eth'.$_e.'_ip3'] 	= codeMAddressToIp($_eth_params['ip3']->m_Address);
					if (isset($_eth_params['mask3']))
						$_data['eth'.$_e.'_mask3'] 	= codeMAddressToIp($_eth_params['mask3']->m_Address);
					if (isset($_eth_params['ip4']))
						$_data['eth'.$_e.'_ip4'] 	= codeMAddressToIp($_eth_params['ip4']->m_Address);
					if (isset($_eth_params['mask4']))
						$_data['eth'.$_e.'_mask4'] 	= codeMAddressToIp($_eth_params['mask4']->m_Address);
					
					if (isset($_eth_params['gw']))
						$_data['eth'.$_e.'_gw'] 	= codeMAddressToIp($_eth_params['gw']->m_Address);
					if (isset($_eth_params['dns1']))
						$_data['eth'.$_e.'_dns1'] 	= codeMAddressToIp($_eth_params['dns1']->m_Address);
					if (isset($_eth_params['dns2']))
						$_data['eth'.$_e.'_dns2'] 	= codeMAddressToIp($_eth_params['dns2']->m_Address);
					
					$_eth_port_num = str_replace( 'ETH', '', $_eth->Name );
					
					$_network_active['eth'.$_eth_port_num.'_auto'] 	= $_data['eth'.$_e.'_auto'];
					$_network_active['eth'.$_eth_port_num.'_ip'] 	= $_data['eth'.$_e.'_ip'];
					$_network_active['eth'.$_eth_port_num.'_mask'] 	= $_data['eth'.$_e.'_mask'];
					$_network_active['eth'.$_eth_port_num.'_ip2'] 	= $_data['eth'.$_e.'_ip2'];
					$_network_active['eth'.$_eth_port_num.'_mask2'] = $_data['eth'.$_e.'_mask2'];
					$_network_active['eth'.$_eth_port_num.'_ip3'] 	= $_data['eth'.$_e.'_ip3'];
					$_network_active['eth'.$_eth_port_num.'_mask3'] = $_data['eth'.$_e.'_mask3'];
					$_network_active['eth'.$_eth_port_num.'_ip4'] 	= $_data['eth'.$_e.'_ip4'];
					$_network_active['eth'.$_eth_port_num.'_mask4'] = $_data['eth'.$_e.'_mask4'];
					$_network_active['eth'.$_eth_port_num.'_gw'] 	= $_data['eth'.$_e.'_gw'];
					$_network_active['eth'.$_eth_port_num.'_dns1'] 	= $_data['eth'.$_e.'_dns1'];
					$_network_active['eth'.$_eth_port_num.'_dns2'] 	= $_data['eth'.$_e.'_dns2'];
				}
				
				$_configuration["network_active"] = $_network_active;
				
				// Salvo la configurazione in sessione
				coreSetConfigurationToSession($_configuration);
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = $_result_data->status->errorDescription;
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Errore durante l\'esecuzione del comando.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Impossibile connettere il client al server.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

?>
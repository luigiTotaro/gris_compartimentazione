<?php
/**
* Telefin STLC1000 Consolle
*
* sk_servicestatus.xml.php - Modulo per recuperare lo stato dei servizi in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.4.3 22/10/2015
* @copyright 2011-2015 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}



// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");

require_once("../modules/sk_core.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_app_stlcManagerService.php');

$_data = array();
if(isAliveStlcManagerService() === true){
	$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
}

if (isset($_client))
{	
	// === SYSTEM ===
	// Preparo i parametri per la richiesta SOAP
	$_request->clientType = 'SKC';
	$_request->group = 'system';
	$_params->request = $_request;

	// Chiamo il metodo SOAP con i parametri che ho generato
	$_result = $_client->getServiceStatus($_params);

	if (isset($_result))
	{
		// Recupero i dati del risultato
		$_result_data = $_result->getServiceStatusResult;
	
		if (isset($_result_data))
		{
			if ($_result_data->status->level == "SUCCESS")
			{
				// Risultato
				$_data['result'] = 'success';
				
				$_service_status_list = $_result_data->data->servicesStatus->STLCServiceStatus;
				
				$_running = true;
				
				if (is_array($_service_status_list))
				{
					foreach ($_service_status_list as $_service_status)
					{
						$_service = array();
						$_service['name'] = $_service_status->Name;
						$_service['status'] = $_service_status->Status;
						$_data['service_'.count($_data)] = $_service;
						if ($_service_status->Status != "Running") $_running = false;
					}
				}
				else
				{
					$_service_status = $_service_status_list;
					$_service = array();
					$_service['name'] = $_service_status->Name;
					$_service['status'] = $_service_status->Status;
					$_data['service_'.count($_data)] = $_service;
					if ($_service_status->Status != "Running") $_running = false;
				}
				
				// === CORE === 
				// Preparo i parametri per la richiesta SOAP
				$_request->clientType = 'SKC';
				$_request->group = 'core';
				$_params->request = $_request;

				// Chiamo il metodo SOAP con i parametri che ho generato
				$_result = $_client->getServiceStatus($_params);
				
				if (isset($_result))
				{
					// Recupero i dati del risultato
					$_result_data = $_result->getServiceStatusResult;
				
					if (isset($_result_data))
					{
						if ($_result_data->status->level == "SUCCESS")
						{
							// Risultato
							$_data['result'] = 'success';
							
							$_service_status_list = $_result_data->data->servicesStatus->STLCServiceStatus;
							
							//$_running = true;
							
							if (is_array($_service_status_list))
							{
								foreach ($_service_status_list as $_service_status)
								{
									$_service = array();
									$_service['name'] = $_service_status->Name;
									$_service['status'] = $_service_status->Status;
									$_data['service_'.count($_data)] = $_service;
									if ($_service_status->Status != "Running") $_running = false;
								}
							}
							else
							{
								$_service_status = $_service_status_list;
								$_service = array();
								$_service['name'] = $_service_status->Name;
								$_service['status'] = $_service_status->Status;
								$_data['service_'.count($_data)] = $_service;
								if ($_service_status->Status != "Running") $_running = false;
							}
				
							// Recupero l'attuale configurazione
							$_configuration	= coreGetConfigurationFromSession();
							$_info = $_configuration["info"];
							$_info["services_status"] = $_running;
							$_data["services_status"] = ($_running)?"1":"0";
							$_configuration["info"] = $_info;
							coreSetConfigurationToSession($_configuration);
						}
						else
						{
							$_data['result'] = 'failure';
							$_data['description'] = $_result_data->status->errorDescription;
						}
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Errore durante l\'esecuzione del comando.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = $_result_data->status->errorDescription;
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Errore durante l\'esecuzione del comando.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Impossibile connettere il client al server.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_getIsChangedNetConfiguration.xml.php - Modulo per verificare se la configurazione rilevata � differente da quella configurata
*
* @author Davide Ferraretto
* @version 1.0.0.0 18/03/2016
* @copyright 2011-2016 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");
// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();
$_data = array();

if( $_configuration === false ){
	 $_data['result_isChanged'] = -1;
} else {
	 $res = coreIsChangedNetConfiguration();
	 $_data['result_isChanged'] = ''.$res;
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));


?>
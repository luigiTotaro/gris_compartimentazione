<?php
/**
* Telefin STLC1000 Consolle
*
* sk_ackconfig.xml.php - Modulo per la configurazione di ACK in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.5 01/03/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

// Recupero la modalita'
$_action = varGetRequest("action");

$_data = array();
$_data['action'] = $_action;

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_devices = $_configuration["devices"];

// Aggiungere un ACK
if ($_action == "add")
{
	$_level 		= varGetRequest("level");
	$_devid 		= varGetRequest("device");
	if ($_level == "stream" || $_level == "field")
		$_strid 	= varGetRequest("stream");
	else
		$_strid 	= "NULL";
	if ($_level == "field")
		$_fieid 	= varGetRequest("field");
	else
		$_fieid 	= "NULL";
	
	$_duration 		= varGetRequest("duration");
	if (isset($_duration) && is_numeric($_duration))
	{
		$_duration		= (float)$_duration;
		if ($_duration < 1) $_duration = 1;
		else if ($_duration > 365) $_duration = 365;
		$_duration = (int)($_duration*60*24);
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Durata della tacitazione non valida.';
	}
		
	if ($_data['result'] != 'failure')
	{
 		$_datetime = date('Y-m-d H:i:s.000',$_SERVER['REQUEST_TIME']);
	
		$_user = authGetUser();
		if (isset($_user['gris_username']))
		{
			$_ack_username = $_user['gris_username'];
		}
		else
		{
			$_ack_username = $_user['username'];
		}
	
		$_device = getDeviceFromDeviceId($_devices,$_devid);
		if (isset($_device))
		{
			$_supervisor_id = $_device->supervisorId;
		}
		else
		{
			$_supervisor_id = 0;
		}
	
// Inserimento ACK
$_sql = codeInit();
$_sql .= <<<HEREDOC
INSERT INTO device_ack (DevID, StrID, FieldID, AckDate, AckDurationMinutes, SupervisorID, Username)
VALUES ($_devid, $_strid, $_fieid, '$_datetime', $_duration, $_supervisor_id, '$_ack_username')
HEREDOC;
$_conn = dbConnect();
$_result = dbQuery($_conn,$_sql);

		$_data['query'] = $_sql;
		$_data['result'] = 'success';
		$_data['description'] = 'Tacitazione aggiunta per '.($_duration/(60*24)).' giorni.';
	}
}
// Modificare un ACK
else if ($_action == "edit")
{
	$_ackid 		= varGetRequest("id");
	$_level 		= varGetRequest("level");
	$_devid 		= varGetRequest("device");
	if ($_level == "stream" || $_level == "field")
		$_strid 	= varGetRequest("stream");
	else
		$_strid 	= "NULL";
	if ($_level == "field")
		$_fieid 	= varGetRequest("field");
	else
		$_fieid 	= "NULL";
	
	$_duration 		= varGetRequest("duration");
	if (isset($_duration) && is_numeric($_duration))
	{
		$_duration		= (float)$_duration;
		if ($_duration < 1) $_duration = 0;
		else if ($_duration > 365) $_duration = 365;
		$_duration = (int)($_duration*60*24);
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Durata della tacitazione non valida.';
	}
 	
 	if ($_data['result'] != 'failure')
	{
 		// DELETE
 		if ($_duration == 0)
 		{
			// Eliminazione ACK
			$_sql = codeInit();

$_sql .= <<<HEREDOC
DELETE FROM device_ack
WHERE DeviceAckID='$_ackid'
HEREDOC;
		
			$_conn = dbConnect();
			$_result = dbQuery($_conn,$_sql);

 			$_data['query'] = $_sql;
 			$_data['result'] = 'success';
			$_data['description'] = 'Tacitazione eliminata.';
 	
 		}
		// UPDATE
		else
 		{
 			$_datetime = date('Y-m-d H:i:s.000',$_SERVER['REQUEST_TIME']);
 		
 			$_user = authGetUser();
 			if (isset($_user['gris_username']))
			{
				$_ack_username = $_user['gris_username'];
			}
			else
			{
				$_ack_username = $_user['username'];
			}
		
			$_device = getDeviceFromDeviceId($_devices,$_devid);
			if (isset($_device))
			{
				$_supervisor_id = $_device->supervisorId;
			}
			else
			{
				$_supervisor_id = 0;
			}
		
			// Modifica ACK
			$_sql = codeInit();

$_sql .= <<<HEREDOC
UPDATE device_ack
SET AckDate='$_datetime', AckDurationMinutes=$_duration, SupervisorID=$_supervisor_id, Username='$_ack_username'
WHERE DeviceAckID='$_ackid'
HEREDOC;

			$_conn = dbConnect();
			$_result = dbQuery($_conn,$_sql);

			$_data['query'] = $_sql;
 			$_data['result'] = 'success';
			$_data['description'] = 'Tacitazione rinnovata per '.($_duration/(60*24)).' giorni.';
 		}
 	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Tipo azione non riconosciuta.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

dbClose($_conn);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
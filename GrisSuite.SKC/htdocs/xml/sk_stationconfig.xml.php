<?php
/**
* Telefin STLC1000 Consolle
*
* sk_stationconfig.xml.php - Modulo per la configurazione dei parametri di una stazione in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.4.1 26/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

//require_once("../modules/sk_class_device.php");

// Recupero la modialita'
$_action = varGetRequest("action");

$_data = array();
$_data['action'] = $_action;

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_conf_app0 = $_conf_apps[0];
$_configuration['region_list'] = coreLoadXMLData($_conf_app0["name"],"LoadXMLRegionList",$_conf_app0["region_list_path"]);

$_region_list		= $_configuration['region_list'];
$_station_type_list	= $_configuration['station_type_list'];

$_stations	= $_configuration["stations"];
$_servers	= $_configuration["servers"];
$_regions	= $_configuration["regions"];
$_zones		= $_configuration["zones"];
$_nodes		= $_configuration["nodes"];

// Aggiungere una stazione
if ($_action == "add")
{
	$_name		= varGetRequest("name");
	$_type		= varGetRequest("type");
	$_offset	= varGetRequest("offset");

	$_node_name 	= getNodeNameFromLongName($_region_list,$_name);
	$_region_id		= getRegionIdFromLongName($_region_list,$_name);
	$_zone_id 		= getZoneIdFromLongName($_region_list,$_name);
	$_node_id 		= getNodeIdFromLongName($_region_list,$_name);
		
	$_station_type 	= getStationTypeCodeFromName($_station_type_list,$_type);
	
	$_id_offset		= (isset($_offset) && $_offset != '' && $_offset>=0)?$_offset:0;
	
	if (isset($_station_type))
	{
		if (isset($_node_name) && isset($_node_id))
		{
			if (isFreeStationName($_stations,$_node_name))
			{
				$_station_id	= getFirstFreeStationId($_stations);
				
				if (isset($_station_id))
				{
					$_server_id = getServerId($_servers);
					// Cerco se node, zone e region sono gia configurati
					
					$_region	= getRegionFromId($_regions,$_region_id);
					$_zone		= getZoneFromId($_zones,$_zone_id);
					$_node 		= getNodeFromId($_nodes,$_node_id);
					
					if ($_region == null)
					{
						$_region_name 	= getRegionNameFromLongName($_region_list,$_name);
						$_region = new region();
						$_region->id = $_region_id;
						$_region->name = $_region_name;
						
						$_region->serverId = $_server_id;
												
						$_regions[] = $_region;
						$_configuration["regions"] = $_regions;
					}
					if ($_zone == null)
					{
						$_zone_name 	= getZoneNameFromLongName($_region_list,$_name);
						$_zone = new zone();
						$_zone->id = $_zone_id;
						$_zone->name = $_zone_name;
						
						$_zone->serverId = $_server_id;
						$_zone->regionId = $_region_id;
												
						$_zones[] = $_zone;
						$_configuration["zones"] = $_zones;
					}
					if ($_node == null)
					{
						$_node = new node();
						$_node->id = $_node_id;
						$_node->name = $_node_name;
						$_node->stationId = $_station_id;
						
						$_node->serverId = $_server_id;
						$_node->regionId = $_region_id;
						$_node->zoneId = $_zone_id;
						
						$_node->devIdOffset = $_id_offset;
						
						if (count($_nodes)==0) $_node->local = "true";
						else $_node->local = "false";
																		
						$_nodes[] = $_node;
						$_configuration["nodes"] = $_nodes;
					}
					
					$_station = new station();
					$_station->id = $_station_id;
					$_station->name = $_node_name;
					$_station->type = $_station_type;
					$_station->nodeId = $_node_id;
					
					$_stations[] = $_station;
					$_configuration["stations"] = $_stations;
					
					coreSetConfigurationToSession($_configuration);
					
					$_data['edit_level'] 	= "1";
					coreSetEditLevelToSession(1,true);
					
					$_data['result'] = 'success';
					$_data['description'] = 'Stazione aggiunta con successo.';
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Impossibile assegnare un indice.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['id'] = $_station_id;
				$_data['name'] = $_station_name;
				$_data['type'] = $_station_type;
				$_data['description'] = 'Stazione gia\' configurata.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Stazione non valida.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Tipo stazione non valido.';
	}
}
// Modificare una stazione
else if ($_action == "edit")
{
	$_name			= varGetRequest("name");
	$_type			= varGetRequest("type");
	$_station_id	= varGetRequest("id");
	
	$_node_name 	= getNodeNameFromLongName($_region_list,$_name);
	$_region_id		= getRegionIdFromLongName($_region_list,$_name);
	$_zone_id 		= getZoneIdFromLongName($_region_list,$_name);
	$_node_id 		= getNodeIdFromLongName($_region_list,$_name);
	
	$_station_type 	= getStationTypeCodeFromName($_station_type_list,$_type);
	
	if (isset($_station_type))
	{
		if (isset($_node_name) && isset($_node_id))
		{
			if (isset($_station_id))
			{
				$_station_index = getStationIndexFromStationId($_stations,$_station_id);
			
				if (isset($_station_index))
				{
					$_buildings		= $_configuration["buildings"];
				
					$_station = $_stations[$_station_index];
					
					if(!stationIsUsedByBuilding($_buildings,$_station->id) || $_node_id == $_station->nodeId)
					{
						$_station_id		=	$_station->id;
						$_station_node_id	=	$_station->nodeId;
					
						if (isFreeStationName($_stations,$_node_name,$_station->name))
						{
							$_server_id = getServerId($_servers);
							
							deleteStationNode($_nodes,$_station_node_id,$_station_id);
							$_configuration["nodes"]		= $_nodes;
							
							// Cerco se node, zone e region sono gia configurati
							$_region	= getRegionFromId($_regions,$_region_id);
							$_zone		= getZoneFromId($_zones,$_zone_id);
							$_node 		= getNodeFromId($_nodes,$_node_id);
							
							// Aggiungo region mancante 
							if ($_region == null)
							{
								$_region_name 	= getRegionNameFromLongName($_region_list,$_name);
								$_region = new region();
								$_region->id = $_region_id;
								$_region->name = $_region_name;
						
								$_region->serverId = $_server_id;
												
								$_regions[] = $_region;
								
							}
							// Aggiungo zone mancante
							if ($_zone == null)
							{
								$_zone_name 	= getZoneNameFromLongName($_region_list,$_name);
								$_zone = new zone();
								$_zone->id = $_zone_id;
								$_zone->name = $_zone_name;
						
								$_zone->serverId = $_server_id;
								$_zone->regionId = $_region_id;
												
								$_zones[] = $_zone;
								
							}
							// Aggiungo node mancante
							if ($_node == null)
							{
								$_node = new node();
								$_node->id = $_node_id;
								$_node->name = $_node_name;
								$_node->stationId = $_station_id;
						
								$_node->serverId = $_server_id;
								$_node->regionId = $_region_id;
								$_node->zoneId = $_zone_id;
								
								if (count($_nodes)==0) $_node->local = "true";
								else $_node->local = "false";
																		
								$_nodes[] = $_node;
								
							}
					
							$_station->id 		= $_station_id;
							$_station->name 	= $_node_name;
							$_station->type 	= $_station_type;
							$_station->nodeId 	= $_node_id;
					
							$_stations[$_station_index] = $_station;
													
							// Pulizia <system>
							$_cleared = clearSystem($_regions,$_zones,$_nodes);
							
							$_configuration["stations"] = $_stations;
							$_configuration["regions"] = $_regions;
							$_configuration["zones"] = $_zones;
							$_configuration["nodes"] = $_nodes;
							
							// Correggo gli ID delle device della stazione che ho modificato
							$_devices	= $_configuration["devices"];
							foreach ($_devices as $_device_index => $_device){
								if ($_device->stationId == $_station_id){
									//print('<!-- DEV '.$_device->name.' -->');
									$_device->nodeId 	= $_node_id;
									$_device->zoneId 	= $_zone_id;
									$_device->regionId 	= $_region_id;
									$_devices[$_device_index] = $_device;
								}
							}
							$_configuration["devices"] = $_devices;
							
							coreSetConfigurationToSession($_configuration);
							
							$_data['edit_level'] 	= "1";
							coreSetEditLevelToSession(1,true);
					
							$_data['result'] = 'success';
							$_data['description'] = 'Stazione modificata con successo.';
						}
						else
						{
							$_data['result'] = 'failure';
							$_data['id'] = $_station_id;
							$_data['name'] = $_station_name;
							$_data['type'] = $_station_type;
							$_data['description'] = 'Stazione gia\' configurata.';
						}
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['id'] = $_station_id;
						$_data['name'] = $_station_name;
						$_data['type'] = $_station_type;
						$_data['description'] = 'Non e\' possibile modificare la stazione perche\' e\' associata ad almeno un edificio.';
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['id'] = $_station_id;
					$_data['name'] = $_station_name;
					$_data['type'] = $_station_type;
					$_data['description'] = 'Indice stazione non valido.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['id'] = $_station_id;
				$_data['name'] = $_station_name;
				$_data['type'] = $_station_type;
				$_data['description'] = 'Stazione non valida.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Stazione non valida.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Tipo stazione non valido.';
	}
}
// Eliminare una stazione
else if ($_action == "delete")
{
	$_station_id	= varGetRequest("id");
	
	$_station_index = getStationIndexFromStationId($_stations,$_station_id);
	
	if (isset($_station_index))
	{
		$_station = $_stations[$_station_index];
		if (isset($_station))
		{
			$_buildings		= $_configuration["buildings"];
			$_racks			= $_configuration["locations"];
			$_regions		= $_configuration["regions"];
			$_zones			= $_configuration["zones"];
			$_nodes			= $_configuration["nodes"];
			$_devices		= $_configuration["devices"];
		
			if(!stationIsUsedByBuilding($_buildings,$_station->id))
			{
				// Salvo l'id della stazione che devo cancellare
				$_deleted_station_id = $_station->id;
				$_deleted_station_node_id = $_station->nodeId;
				
				// Cancello la stazione
				unset($_stations[$_station_index]);
				$_configuration["stations"] = $_stations;
				
				deleteStationNode($_nodes,$_deleted_station_node_id,$_deleted_station_id);
				
				$_configuration["nodes"]		= $_nodes;
				
				$_cleared = clearSystem($_regions,$_zones,$_nodes);
				
				if ($_cleared)
				{
					$_configuration["regions"]		= $_regions;
					$_configuration["zones"]		= $_zones;
					$_configuration["nodes"]		= $_nodes;
					$_configuration["devices"]		= $_devices;
				}			
					
				// Eseguo lo shift di eventuali stazioni con id successivo a quella eliminata
				$_shifted = stationShiftNextId($_stations,$_buildings,$_racks,$_nodes,$_devices,$_deleted_station_id);
				
				if ($_shifted)
				{
					$_configuration["stations"] 	= $_stations;
					$_configuration["buildings"]	= $_buildings;
					$_configuration["locations"]	= $_racks;
					$_configuration["nodes"]		= $_nodes;
					$_configuration["devices"]		= $_devices;
				}
				
				coreSetConfigurationToSession($_configuration);
				
				$_data['edit_level'] 	= "1";
				coreSetEditLevelToSession(1,true);
					
				$_data['result'] = 'success';
				$_data['description'] = 'Stazione eliminata con successo.';
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'La stazione e\' associata ad almeno un edificio.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Oggetto stazione non valido.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Indice stazione non valido.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Parametro \'action\' non riconosciuto.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_ethernetconfig.xml.php - Modulo per la configurazione dei parametri di una porta ethernet in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.4.1 25/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

// Recupero la modalita'
$_action = varGetRequest("action");

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_data = array();
$_data['action'] = $_action;

$_network			= $_configuration["network"];

if ($_action == "edit")
{
	$_item_id	= varGetRequest("id");
	
	$_name		= varGetRequest("name");
	$_port		= varGetRequest("port");
	$_ip		= varGetRequest("ip");
	$_subnet	= varGetRequest("subnet");
	$_ip2		= varGetRequest("ip2");
	$_subnet2	= varGetRequest("subnet2");
	$_ip3		= varGetRequest("ip3");
	$_subnet3	= varGetRequest("subnet3");
	$_ip4		= varGetRequest("ip4");
	$_subnet4	= varGetRequest("subnet4");
	$_gateway	= varGetRequest("gateway");
	$_dns1		= varGetRequest("dns1");
	$_dns2		= varGetRequest("dns2");
	
	$_item			= getPortFromId($_network,$_item_id);
	$_item_index	= getPortIndexFromId($_network,$_item_id);
	
	if (isset($_item))
	{
		$_item->type 		= "ETH";
		$_item->name 		= $_name;
		/*
		$_item->timeout 	= $_timeout;
		$_item->persistent 	= $_persistent;
		$_item->com 		= $_com;
		$_item->baud 		= $_baud;
		$_item->echo 		= $_echo;
		$_item->data 		= $_data;
		$_item->stop 		= $_stop;
		$_item->parity 		= $_parity;
		*/
		//$_item->port		= $_port;
		
		// Controllo dati IP (con eventuale slittamento)
		for ($a = 0; $a < 3; $a++){
			if (!isset($_ip) || $_ip == '') {
				$_ip 		= $_ip2;
				$_subnet 	= $_subnet2;
				$_ip2		= null;
				$_subnet2	= null;
			}
			if (!isset($_ip2) || $_ip2 == '') {
				$_ip2 		= $_ip3;
				$_subnet2	= $_subnet3;
				$_ip3		= null;
				$_subnet3	= null;
			}
			if (!isset($_ip3) || $_ip3 == '') {
				$_ip3 		= $_ip4;
				$_subnet3 	= $_subnet4;
				$_ip4		= null;
				$_subnet4	= null;
			}
			if ($_ip4 == '') {
				$_ip4		= null;
				$_subnet4	= null;
			}
		}
		
		$_item->ip 			= $_ip;
		$_item->subnet 		= $_subnet;
		$_item->ip2 		= $_ip2;
		$_item->subnet2 	= $_subnet2;
		$_item->ip3 		= $_ip3;
		$_item->subnet3 	= $_subnet3;
		$_item->ip4 		= $_ip4;
		$_item->subnet4 	= $_subnet4;
		$_item->gateway 	= $_gateway;
		$_item->dns1 		= $_dns1;
		$_item->dns2 		= $_dns2;
		
		$_network[$_item_index]	= $_item;
		
		$_configuration["network"] = $_network;
		
		coreSetConfigurationToSession($_configuration);
		
		$_data['edit_level'] 	= "2";
		coreSetEditLevelToSession(2,true);
		
		$_data['result'] = 'success';
		$_data['description'] = 'Porta ethernet modificata con successo.';
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Porta ethernet non valida.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Parametro \'action\' non riconosciuto.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_apply.xml.php - Modulo per applicare la configurazione STLC1000 in modalità AJAX.
*
* @author Enrico Alborali
* @version 0.0.1 12/12/2011
* @copyright 2011 Telefin S.p.A.
*/

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}


// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");

require_once("../modules/sk_core.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_app_stlcManagerService.php');
$_data = array();

// === FASE 1 = Salvataggio system.xml ===

spvstandardSaveXMLConfig($_conf_app0["config_url"],$_configuration);

// === FASE 2 = Riavvio dei servizi ===
if(isAliveStlcManagerService() === true){
	$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
}

if (isset($_client))
{	
	// Preparo i parametri per la richiesta SOAP
	$_request->clientType = 'SKC';
	$_request->command = 'RESTART';
	
	$_services = array();
	/*
	$_services[] = "SPVServerDaemon";
	$_services[] = "SnmpSupervisorService";
	*/
	$_services[] = "SCAgentService";

	$_request->services = $_services;
	$_params->request = $_request;

	// Chiamo il metodo SOAP con i parametri che ho generato
	$_result = $_client->setServiceStatus($_params);

	if (isset($_result))
	{
		// Recupero i dati del risultato
		$_result_data = $_result->setServiceStatusResult;
	
		if (isset($_result_data))
		{
			if ($_result_data->status->level == "SUCCESS")
			{
				// Risultato
				$_data['result'] = 'success';
				
				// Dati grezzi
				$_data['data_1'] = serialize($_result_data);
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = $_result_data->status->errorDescription;
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Errore durante l\'esecuzione del comando.';
	}
	
// === FASE 3 = Cambio IP e Hostname ===	
	if ($_data['result'] == 'success')
	{
	// Preparo i parametri per la richiesta SOAP
	$_request->clientType = 'SKC';
	$_params->request = $_request;
	
	// Chiamo il metodo SOAP con i parametri che ho generato
	$_result = $_client->applySystemConfig($_params);
	
	if (isset($_result))
	{
		// Recupero i dati del risultato
		$_result_data = $_result->applySystemConfigResult;
	
		if (isset($_result_data))
		{
			if ($_result_data->status->level == "SUCCESS")
			{
				// Risultato
				$_data['result'] = 'success';
				
				// Dati grezzi
				$_data['data_2'] = serialize($_result_data);
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = $_result_data->status->errorDescription;
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Errore durante l\'esecuzione del comando.';
	}
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Impossibile connettere il client al server.';
}




$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

?>
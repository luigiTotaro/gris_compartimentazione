<?php
/**
* Telefin STLC1000 Consolle
*
* sk_config_device.xml.php - Modulo per la configurazione dei parametri di una periferica in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.4.0 24/10/2013
* @copyright 2011-2013 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

// Recupero la modialita'
$_action = varGetRequest("action");

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_info				= $_configuration["info"];

$_data = array();
$_data['action'] = $_action;

$_devices			= $_configuration["devices"];
$_device_type_list	= $_configuration["device_type_list"];
$_ports				= $_configuration["ports"];
$_locations 		= $_configuration['locations'];
$_stations			= $_configuration["stations"];
$_servers			= $_configuration["servers"];
$_nodes				= $_configuration["nodes"];

// Gestire il form per aggiungere una periferica
if ($_action == "add_form")
{
	session_write_close();
	
	$_type_name		= varGetRequest("type");
	$_serial_size	= varGetRequest("serial_size");
	$_action_option	= varGetRequest("action_option");
	
	// Salvo l'opzione
	$_data['action_option']	= $_action_option;
	
	$_type 			= getDeviceTypeFromName($_device_type_list,$_type_name);
		
	if (isset($_type))
	{
		$_similar_count				= deviceSimilarNameCount($_devices,$_type->defaultName);
		if ($_similar_count>0)
			$_data['default_name']		= $_type->defaultName." (".($_similar_count+1).")";
		else
			$_data['default_name']		= $_type->defaultName;
		$_data['enable_name']		= "true";
		
		/*
		if (count($_locations)>0)
		{
			$_location = $_locations[0];
			$_default_location_name = $_location->getDisplayName();
		}
		else
		{
			$_default_location_name = "";
		}
		*/
		$_default_location_name = '';
		$_data['default_location']	= $_default_location_name;
		$_data['enable_location']	= 'true';
		
		$_data['default_position']	= '';
		$_data['enable_position']	= 'true';
		
		$_data['enable_sn']			= 'true';
		$_data['default_md_id']		= '';
		$_data['enable_md_id']		= 'true';
		
		$_first_port = getFirstPortFromType($_ports,$_type->portType);
		if (!isset($_first_port))
			$_first_port = getFirstPortFromType($_ports,str_replace('STLC1000_','' ,$_type->portType));
		if (!isset($_first_port))
			$_first_port = $_ports[1];
		
		$_data['default_port']		= $_first_port->getDisplayName();
		$_data['enable_port']		= 'true';
		
		// Porta seriale
		if (portTypeIsSerial($_type->portType))
		{
			$_data['bind_port']				= "json/sk_getports.json.php?category=serial";
			if ($_serial_size == "8")
			{
				$_data['bind_serial_modem']	= "modem_addr_8_list";
				$_data['bind_serial_addr']	= "device_addr_8_list";
			}
			else
			{
				$_data['bind_serial_modem']	= "modem_addr_16_list";
				$_data['bind_serial_addr']	= "device_addr_16_list";
			}
			$_data['enable_serial_size']	= "true";
			$_data['enable_serial_modem']	= "true";
			$_data['enable_serial_addr']	= "true";
			
			if (deviceTypeIsSTSI($_type->code))
			{
				$_data['enable_cts_addr']	= "true";
			}
			else
			{
				$_data['enable_cts_addr']	= "false";
			}
			$_data['enable_ip']	= "false";
		}
		// Ethernet (IP/SNMP)
		else
		{
			$_data['bind_port']				= "json/sk_getports.json.php?category=ethernet";
			$_data['enable_serial_size']	= "false";
			$_data['enable_serial_modem']	= "false";
			$_data['enable_serial_addr']	= "false";
			$_data['enable_cts_addr']		= "false";
			$_data['enable_ip']				= "true";
			
			$_server_ip = $_SERVER['SERVER_ADDR'];
			$_server_ip_token = explode(".",$_server_ip);
						
			$_data['default_ip1']			= $_server_ip_token[0];
			$_data['default_ip2']			= $_server_ip_token[1];
			$_data['default_ip3']			= $_server_ip_token[2];
			$_data['default_ip4']			= "";
			if ($_type->code == "XXIP000")
			{
				$_data['enable_community']	= "false";
			}
			else
			{
				$_data['enable_community']	= "true";
				$_data['default_community']	= $_type->snmpCommunity;
				/*
				if (strpos($_type->code,"SO")===0)
				{
					$_data['default_community']	= "solari_public";
				}
				else
				{
					$_data['default_community']	= "public";
				}
				*/
			}
		}
		
		$_data['result'] = 'success';
		$_data['silent'] = 'true';
	}
}
// Gestire il form per modificare una periferica
else if ($_action == "edit_form")
{
	session_write_close();

	$_device_id		= varGetRequest("id");
	$_type_name		= varGetRequest("type");
	$_serial_size	= varGetRequest("serial_size");
			
	$_type 			= getDeviceTypeFromName($_device_type_list,$_type_name);
	$_prev_device	= getDeviceFromDeviceId($_devices,$_device_id);
	$_prev_type		= getDeviceTypeFromCode($_device_type_list,$_prev_device->type);
	
	if (isset($_type))
	{
		// Recupero la porta precedentemente selezionata
		if (isset($_prev_device) && isset($_prev_device->port))
			$_prev_port = getPortFromId($_ports,$_prev_device->port);
		// Recupero la porta di default per il tipo selezionato
		$_default_port	= getFirstPortFromType($_ports,$_type->portType);
				
		// Porta seriale
		if (portTypeIsSerial($_type->portType))
		{
			$_data['bind_port']				= "json/sk_getports.json.php?category=serial";
			if ($_serial_size == "8")
			{
				$_data['bind_serial_modem']	= "modem_addr_8_list";
				$_data['bind_serial_addr']	= "device_addr_8_list";
			}
			else
			{
				$_data['bind_serial_modem']	= "modem_addr_16_list";
				$_data['bind_serial_addr']	= "device_addr_16_list";
			}
			$_data['enable_serial_size']	= "true";
			$_data['enable_serial_modem']	= "true";
			$_data['enable_serial_addr']	= "true";
			
			if (deviceTypeIsSTSI($_type->code))
			{
				$_data['enable_cts_addr']	= "true";
			}
			else
			{
				$_data['enable_cts_addr']	= "false";
			}
			$_data['enable_ip']	= "false";
			
			if (portTypeIsSerial($_prev_port->type))
				$_auto_port = $_prev_port;
			else
				$_auto_port = $_default_port;
		}
		// Ethernet (IP/SNMP)
		else
		{
			$_data['bind_port']				= "json/sk_getports.json.php?category=ethernet";
			$_data['enable_serial_size']	= "false";
			$_data['enable_serial_modem']	= "false";
			$_data['enable_serial_addr']	= "false";
			$_data['enable_cts_addr']		= "false";
			$_data['enable_ip']				= "true";
			
			if (portTypeIsSerial($_prev_type->portType))
			{
				$_server_ip = $_SERVER['SERVER_ADDR'];
				$_server_ip_token = explode(".",$_server_ip);
						
				$_data['default_ip1']			= $_server_ip_token[0];
				$_data['default_ip2']			= $_server_ip_token[1];
				$_data['default_ip3']			= $_server_ip_token[2];
				$_data['default_ip4']			= "";
			}
			if ($_type->code == "XXIP000")
			{
				$_data['enable_community']	= "false";
			}
			else
			{
				$_data['enable_community']	= "true";
				$_data['default_community']	= $_type->snmpCommunity;
				/*
				if (strpos($_type->code,"SO")===0)
				{
					$_data['default_community']	= "solari_public";
				}
				else
				{
					$_data['default_community']	= "public";
				}
				*/
			}
			
			if (portTypeIsSerial($_prev_port->type))
				$_auto_port = $_default_port;
			else
				$_auto_port = $_prev_port;
		}
		
		$_data['default_port']		= $_auto_port->getDisplayName();
		$_data['enable_port']		= "true";
		
		$_data['result'] = 'success';
		$_data['silent'] = 'true';
	}
}
// Aggiungere una periferica
else if ($_action == "add")
{
	$_name			= varGetRequest("name");
	$_sn			= varGetRequest("sn");
	$_md_id			= varGetRequest("md_id");
	$_type_name		= varGetRequest("type");
	$_location_name	= varGetRequest("location");
	$_port_name		= varGetRequest("port");
	$_position		= varGetRequest("position");
	
	$_serial_size	= varGetRequest("serial_size");
	$_serial_modem	= varGetRequest("serial_modem");
	$_serial_addr	= varGetRequest("serial_addr");
	$_cts_addr		= varGetRequest("cts_addr");
	
	$_ip1			= varGetRequest("ip1");
	$_ip2			= varGetRequest("ip2");
	$_ip3			= varGetRequest("ip3");
	$_ip4			= varGetRequest("ip4");
	$_community		= varGetRequest("community");	
	
	$_profile		= "1";
	$_active		= "true";
	$_scheduled		= "true";
	
	$_type 			= getDeviceTypeCodeFromName($_device_type_list,$_type_name);
	$_location		= getRackFromRackLongName($_locations,$_location_name);
	$_port			= getPortFromPortLongName($_ports,$_port_name);
	
	if (isset($_type))
	{
		if (isset($_location))
		{
			if (isset($_port))
			{
				if (isset($_name) && $_name != "")
				{
					// Recupero informazioni topografia
					$_location_id			= $_location->id;
					$_station_id			= $_location->stationId;
					$_building_id			= $_location->buildingId;
					
					// Recupero informazioni system 
					$_station				= getStationFromId($_stations,$_station_id);
					$_server_id				= getServerId($_servers);						
					$_node_id				= $_station->nodeId;
					// Recupero il nodo di appartenenza della nuova periferica per verificare se e' impostato un offset per l'id
					$_node 					= getNodeFromId($_nodes,$_node_id);
					$_zone_id				= $_node->zoneId;
					$_region_id				= $_node->regionId;
					
					$_device_id	= getFirstFreeDeviceId($_devices,$_node->devIdOffset,$_node->id);
				
					if (portTypeIsSerial($_port->type)){
						$_address = portEncodeSerialAddress($_serial_addr,$_serial_size,$_serial_modem);
					}
					else{
						$_address = portEncodeIPAddress($_ip1,$_ip2,$_ip3,$_ip4);
					}
				
					if (isset($_address) && $_address != ""){
						if (isset($_position) && $_position != ""){
							if (isset($_device_id)){
								// Creo un nuovo oggetto periferica
								$_device = new device();
								
								// Salvo i parametri
								if (isset($_device) && $_device != null){
									$_device->id			= $_device_id;
									$_device->type			= $_type;
									$_device->name			= $_name;
						
									$_device->serverId 		= $_server_id;
									$_device->regionId 		= $_region_id;
									$_device->zoneId 		= $_zone_id;
									$_device->nodeId 		= $_node_id;
						
									$_device->stationId		= $_station_id;
									$_device->buildingId	= $_building_id;
									$_device->locationId	= $_location_id;
									$_device->position		= $_position;
						
									$_port_id				= $_port->id;
						
									if (isset($_port)){
										if ($_port->type == "TCP_Client")
										{
											if (strstr($_type, "PEAUT"))
											{
												$_supervisor_id = 3; // Supervisore Telnet
											}
											else if ($_type == "VP20002" || $_type == "VP20003" || $_type == "VP20005"){
												$_supervisor_id = 2; // Supervisore Modbus/TCP
											}
											else{
												$_supervisor_id = 1; // Supervisore SNMP
											}
										}
										else
										{
											$_supervisor_id = 0; // Supervisore Standard (Seriale)
										}
									}
									else{
										$_supervisor_id = 0;
									}
						
									$_device->addr			= $_address;
									$_device->modemSize		= $_serial_size;
									$_device->modemAddr		= portIntToHexAddress($_serial_modem);
									$_device->ctsAddr		= portIntToHexAddress($_cts_addr);
									$_device->supervisorId	= $_supervisor_id;
									$_device->snmpCommunity	= $_community;
						
									$_device->sn			= $_sn;
									$_device->monitoringDeviceId	= $_md_id;
									$_device->port			= $_port_id;
									$_device->profile		= $_profile;
									$_device->active		= $_active;
									$_device->scheduled		= $_scheduled;
						
									$_devices[] = $_device;
									logDebug("New device object: ".serialize($_device));
									$_configuration["devices"] = $_devices;
						
									coreSetConfigurationToSession($_configuration);
						
									$_data['edit_level'] 	= "1";
									coreSetEditLevelToSession(1,true);
						
									// Risultato
									$_data['result'] = 'success';
									$_data['description'] = 'Periferica aggiunta con successo.';
									//$_data['raw_data'] = serialize($_device);
									$_data['count'] = count($_devices);
								}
								else{
									$_data['result'] = 'failure';
									$_data['description'] = 'Errore durante la creazione dell\'oggetto.';
								}
							}
							else{
								$_data['result'] = 'failure';
								$_data['description'] = 'Impossibile assegnare un indice.';
							}
						}
						else{
							$_data['result'] = 'failure';
							$_data['description'] = 'Posizione non valida.';
						}
					}
					else{
						$_data['result'] = 'failure';
						$_data['description'] = 'Indirizzo non valido.';
					}
				}
				else{
					$_data['result'] = 'failure';
					$_data['description'] = 'Nome periferica non specificato.';
				}
			}
			else{
				$_data['result'] = 'failure';
				$_data['description'] = 'Porta non valida.';
			}
		}
		else{
			$_data['result'] = 'failure';
			$_data['description'] = 'Ubicazione non valida.';
		}
	}
	else{
		$_data['result'] = 'failure';
		$_data['description'] = 'Tipo periferica non valido.';
	}
}
// Modificare una periferica esistente
else if ($_action == "edit")
{
	$_device_id		= varGetRequest("id");
	$_name			= varGetRequest("name");
	$_sn			= varGetRequest("sn");
	$_md_id			= varGetRequest("md_id");
	$_type_name		= varGetRequest("type");
	$_location_name	= varGetRequest("location");
	$_port_name		= varGetRequest("port");
	$_position		= varGetRequest("position");
	
	$_serial_size	= varGetRequest("serial_size");
	$_serial_modem	= varGetRequest("serial_modem");
	$_serial_addr	= varGetRequest("serial_addr");
	$_cts_addr		= varGetRequest("cts_addr");
	
	$_ip1			= varGetRequest("ip1");
	$_ip2			= varGetRequest("ip2");
	$_ip3			= varGetRequest("ip3");
	$_ip4			= varGetRequest("ip4");
	$_community		= varGetRequest("community");
	
	$_profile		= "1";
	$_active		= "true";
	$_scheduled		= "true";
	
	$_type 			= getDeviceTypeCodeFromName($_device_type_list,$_type_name);
	$_location		= getRackFromRackLongName($_locations,$_location_name);
	$_port			= getPortFromPortLongName($_ports,$_port_name);
	
	if (isset($_type))
	{
		if (isset($_location))
		{
			if (isset($_port))
			{
				if (isset($_name) && $_name != "")
				{
				if (portTypeIsSerial($_port->type))
				{
					$_address = portEncodeSerialAddress($_serial_addr,$_serial_size,$_serial_modem);
				}
				else
				{
					$_address = portEncodeIPAddress($_ip1,$_ip2,$_ip3,$_ip4);
				}
				
				if (isset($_address) && $_address != "")
				{
				if (isset($_position) && $_position != "")
				{
				
				$_device_index	= getDeviceIndexFromDeviceId($_devices,$_device_id);
				
				if (isset($_device_index))
				{
					$_device 	= $_devices[$_device_index];
					
					// Creo un nuovo oggetto periferica
					//$_device = new device();
					// Salvo i parametri
					if (isset($_device) && $_device != null)
					{
						//$_device->id			= $_device_id;
						$_device->type			= $_type;
						$_device->name			= $_name;
						
						$_location_id			= $_location->id;
						$_station_id			= $_location->stationId;
						$_building_id			= $_location->buildingId;
						
						$_station				= getStationFromId($_stations,$_station_id);
						$_server_id				= getServerId($_servers);						
						$_node_id				= $_station->nodeId;
						$_node 					= getNodeFromId($_nodes,$_node_id);
						$_zone_id				= $_node->zoneId;
						$_region_id				= $_node->regionId;
						
						$_device->serverId 		= $_server_id;
						$_device->regionId 		= $_region_id;
						$_device->zoneId 		= $_zone_id;
						$_device->nodeId 		= $_node_id;
						
						$_device->stationId		= $_station_id;
						$_device->buildingId	= $_building_id;
						$_device->locationId	= $_location_id;
						$_device->position		= $_position;
						
						$_port_id				= $_port->id;
						
						if (isset($_port))
						{
							if ($_port->type == "TCP_Client")
							{
								if (strstr($_type, "PEAUT"))
								{
									$_supervisor_id = 3; // Supervisore Telnet
								}
								else if ($_type == "VP20002" || $_type == "VP20003"){
									$_supervisor_id = 2; // Supervisore Modbus/TCP
								}
								else{
									$_supervisor_id = 1; // Supervisore SNMP
								}
							}
							else
							{
								$_supervisor_id = 0; // Supervisore Standard (Seriale)
							}
						}
						else
						{
							$_supervisor_id = 0;
						}
						
						$_device->addr			= $_address;
						$_device->modemSize		= $_serial_size;
						$_device->modemAddr		= portIntToHexAddress($_serial_modem);
						$_device->ctsAddr		= portIntToHexAddress($_cts_addr);
						$_device->supervisorId	= $_supervisor_id;
						$_device->snmpCommunity	= $_community;
						
						$_device->sn			= $_sn;
						$_device->monitoringDeviceId	= $_md_id;
						$_device->port			= $_port_id;
						
						/*
						$_device->profile		= $_profile;
						$_device->active		= $_active;
						$_device->scheduled		= $_scheduled;
						*/
						
						$_devices[$_device_index] = $_device;
						$_configuration["devices"] = $_devices;
						
						coreSetConfigurationToSession($_configuration);
						
						$_data['edit_level'] 	= "1";
						coreSetEditLevelToSession(1,true);
						
						// Risultato
						$_data['result'] = 'success';
						$_data['description'] = 'Periferica modificata con successo.';
						//$_data['raw_data'] = serialize($_device);
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = 'Errore durante la creazione dell\'oggetto.';
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Periferica non valida.';
				}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Posizione non valida.';
				}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Indirizzo non valido.';
				}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Nome periferica non specificato.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Porta non valida.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Ubicazione non valida.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Tipo periferica non valido.';
	}
}
// Eliminare una periferica esistente
else if ($_action == "delete")
{
	$_device_id	= varGetRequest("id");
	
	$_device_index	= getDeviceIndexFromDeviceId($_devices,$_device_id);

	if (isset($_device_index))
	{
		unset($_devices[$_device_index]);
		$_configuration["devices"] = $_devices;
		
		coreSetConfigurationToSession($_configuration);
		
		$_data['edit_level'] 	= "1";
		coreSetEditLevelToSession(1,true);
					
		$_data['result'] = 'success';
		$_data['description'] = 'Periferica eliminata con successo.';
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Periferica non valida.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Parametro \'action\' non riconosciuto.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
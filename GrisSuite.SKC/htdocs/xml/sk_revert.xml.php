<?php
/**
* Telefin STLC1000 Consolle
*
* sk_apply.xml.php - Modulo per applicare la configurazione STLC1000 in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.1 06/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

$_data = array();

// === FASE 1 = Ricarico configurazione precedente system.xml ===

$_conf_app0 = $_conf_apps[0];

$_config_url = $_conf_app0["config_url"];
$_config_prev_url = str_replace(".xml", "", $_config_url)."_prev.xml";

$_station_type_list 	= coreLoadXMLData($_conf_app0["name"],"LoadXMLStationTypeList",$_conf_app0["station_type_list_path"]);
$_device_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLDeviceTypeList",$_conf_app0["device_type_list_path"]);
$_port_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLPortTypeList",$_conf_app0["port_type_list_path"]);
$_building_type_list 	= coreLoadXMLData($_conf_app0["name"],"LoadXMLBuildingTypeList",$_conf_app0["building_type_list_path"]);
$_rack_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLRackTypeList",$_conf_app0["rack_type_list_path"]);
	
// Carico la configurazione precedente
$_configuration = coreLoadXMLConfig($_conf_app0["name"],$_config_prev_url,$_port_type_list, true);
	
$_configuration['station_type_list'] 	= $_station_type_list;
$_configuration['device_type_list'] 	= $_device_type_list;
$_configuration['port_type_list'] 		= $_port_type_list;
$_configuration['building_type_list'] 	= $_building_type_list;
$_configuration['rack_type_list'] 		= $_rack_type_list;

if(isset($_configuration['edit'])){
	unset($_configuration['edit']);
}

// Salvo in sessione la configurazione precedente	
coreSetConfigurationToSession($_configuration,true);

$_data['edit_level'] 	= "2";
coreSetEditLevelToSession(2,true);

// Risultato
$_data['result'] = 'success';

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_rackconfig.xml.php - Modulo per la configurazione dei parametri di un armadio in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.1 06/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

// Recupero la modalita'
$_action = varGetRequest("action");

$_data = array();
$_data['action'] = $_action;

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession();

$_rack_type_list	= $_configuration['rack_type_list'];
$_stations			= $_configuration["stations"];
$_buildings			= $_configuration["buildings"];
$_racks				= $_configuration["locations"];

// Aggiungere un armadio
if ($_action == "add")
{
	$_name			= varGetRequest("name");
	$_building_name	= varGetRequest("building");
	$_note			= varGetRequest("note");
	$_type_name		= varGetRequest("type");
	
	$_building		= getBuildingFromBuildingLongName($_buildings,$_building_name);
	$_type 			= getRackTypeCodeFromName($_rack_type_list,$_type_name);	
	
	if (isset($_building))
	{
		if (isset($_type))
		{
			if (isset($_name) && $_name != "")
			{
			if (isFreeRackName($_racks,$_building->stationId,$_building->id,$_name))
			{
				$_rack_id = getFirstFreeRackId($_racks,$_building->stationId,$_building->id);
				
				if (isset($_rack_id))
				{
					if ($_note == "") $_note = "Nessuna";
					
					$_rack = new rack();
					$_rack->id 			= $_rack_id;
					$_rack->name 		= $_name;
					$_rack->type 		= $_type;
					$_rack->note 		= $_note;
					$_rack->buildingId 	= $_building->id;
					$_rack->stationId 	= $_building->stationId;
					
					$_racks[] = $_rack;
					$_configuration["locations"] = $_racks;
					
					coreSetConfigurationToSession($_configuration);
					
					$_data['edit_level'] 	= "1";
					coreSetEditLevelToSession(1,true);
					
					$_data['result'] = 'success';
					$_data['description'] = 'Armadio aggiunto con successo.';
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['description'] = 'Impossibile assegnare un indice.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['name'] = $_name;
				$_data['description'] = 'Nome armadio gia\' utilizzato per questo edificio.';
			}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Nome armadio non specificato.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Tipo armadio non valido.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Edificio non valido.';
	}
}
// Modificare un armadio
else if ($_action == "edit")
{
	$_rack_id		= varGetRequest("id");
	$_name			= varGetRequest("name");
	$_building_name	= varGetRequest("building");
	$_note			= varGetRequest("note");
	$_type_name		= varGetRequest("type");
	
	$_building		= getBuildingFromBuildingLongName($_buildings,$_building_name);
	$_type 			= getRackTypeCodeFromName($_rack_type_list,$_type_name);
	
	if (isset($_building))
	{
		if (isset($_type))
		{
			if (isset($_name) && $_name != "")
			{
			$_rack_index	= getRackIndexFromRackId($_racks,$_rack_id);
			$_rack 			= getRackFromRackId($_racks,$_rack_id);
				
				if (isset($_rack_index))
				{
					if ($_rack->stationId == $_building->stationId && $_rack->buildingId == $_building->id)
						$_rack_name = $_rack->name;
					else
						$_rack_name = null;
					
					if (isFreeRackName($_racks,$_building->stationId,$_building->id,$_name,$_rack_name))
					{
						// Se cambio stazione o edificio devo calcolare un nuovo id armadio
						if ($_rack->stationId != $_building->stationId || $_rack->buildingId != $_building->id)
						{
							$_devices = $_configuration["devices"];
							
							$_rack_id = getFirstFreeRackId($_racks,$_building->stationId,$_building->id);
							
							// Estraggo il node dall'id stazione dell'edificio
							$_station = getStationFromId($_stations,$_building->stationId);
							$_node_id = $_station->nodeId;
							$_nodes	= $_configuration["nodes"];
							$_node = getNodeFromId($_nodes,$_node_id);
							
							$_zone_id = $_node->zoneId;
							$_region_id = $_node->regionId;
														
							// Aggiorno la topografia di eventuali periferiche di questo armadio
							if (deviceUpdateTopographyId($_devices,$_rack->stationId,$_rack->buildingId,$_rack->id,$_building->stationId,$_building->id,$_rack_id,$_node_id,$_zone_id,$_region_id) == true)
							{
								$_configuration["devices"] = $_devices;
							}
							// Eseguo lo shift di eventuali armadi con id successivo a quello eliminato
							$_shifted = rackShiftNextId($_racks,$_devices,$_rack->stationId,$_rack->buildingId,$_rack->id);
							if ($_shifted)
							{
								$_configuration["locations"]	= $_racks;
								$_configuration["devices"]		= $_devices;
							}
																					
							// Salvo il nuovo id armadio
							$_rack->id 		= $_rack_id;
						}
					
						if ($_note == "") $_note = "Nessuna";
					
						$_rack->name 		= $_name;
						$_rack->type 		= $_type;
						$_rack->note 		= $_note;
						$_rack->buildingId 	= $_building->id;
						$_rack->stationId 	= $_building->stationId;
					
						$_racks[$_rack_index] = $_rack;
						$_configuration["locations"] = $_racks;
					
						coreSetConfigurationToSession($_configuration);
						
						$_data['edit_level'] 	= "1";
						coreSetEditLevelToSession(1,true);
					
						$_data['result'] = 'success';
						$_data['description'] = 'Armadio modificato con successo.';
					}
					else
					{
						$_data['result'] = 'failure';
						$_data['description'] = 'Nome armadio gia\' utilizzato per questo edificio.';
					}
				}
				else
				{
					$_data['result'] = 'failure';
					$_data['name'] = $_name;
					$_data['id'] = $_building_id;
					$_data['description'] = 'Armadio non valido.';
				}
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'Nome armadio non specificato.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Tipo armadio non valido.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Edificio non valido.';
	}
}
// Eliminare un armadio
else if ($_action == "delete")
{
	$_rack_id	= varGetRequest("id");

	$_rack_index	= getRackIndexFromRackId($_racks,$_rack_id);

	if (isset($_rack_index))
	{
		$_rack = $_racks[$_rack_index];
		if (isset($_rack))
		{
			$_devices			= $_configuration["devices"];
		
			if(!rackIsUsedByDevice($_devices,$_rack->stationId,$_rack->buildingId,$_rack->id))
			{
				// Salvo l'id dell'armadio che devo cancellare
				$_deleted_rack_id = $_rack->id;
				$_deleted_rack_station_id = $_rack->stationId;
				$_deleted_rack_building_id = $_rack->buildingId;
				
				unset($_racks[$_rack_index]);
				$_configuration["locations"] = $_racks;
				
				// Eseguo lo shift di eventuali armadi con id successivo a quello eliminato
				$_shifted = rackShiftNextId($_racks,$_devices,$_deleted_rack_station_id,$_deleted_rack_building_id,$_deleted_rack_id);
				
				if ($_shifted)
				{
					$_configuration["locations"]	= $_racks;
					$_configuration["devices"]		= $_devices;
				}
				
				coreSetConfigurationToSession($_configuration);
				
				$_data['edit_level'] 	= "1";
				coreSetEditLevelToSession(1,true);
				
				$_data['result'] = 'success';
				$_data['description'] = 'Armadio eliminato con successo.';
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = 'L\'armadio e\' associato ad almeno una periferica.';
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Oggetto armadio non valido.';
		}	
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Indice armadio non valido.';
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Parametro \'action\' non riconosciuto.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
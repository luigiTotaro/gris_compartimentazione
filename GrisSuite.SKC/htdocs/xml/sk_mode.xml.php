<?php
/**
* Telefin STLC1000 Consolle
*
* sk_mode.xml.php - Modulo per applicare la modalita' STLC1000 in modalità AJAX.
*
* @author Enrico Alborali
* @version 1.0.2.1 30/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}

// Imposto l'intestazione per il file XML
header ("content-type: text/xml");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_app_stlcManagerService.php');

$_data = array();

// === FASE 1 = Recupero stato STLC1000 ===
if(isAliveStlcManagerService() === true){
	$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
}

if (isset($_client))
{	
	// Preparo i parametri per la richiesta SOAP
	$_request->clientType = 'SKC';
	$_params->request = $_request;
		
	// Chiamo il metodo SOAP con i parametri che ho generato
	$_result = $_client->getSTLCStatus($_params);

	if (isset($_result))
	{
		// Recupero i dati del risultato
		$_result_data = $_result->getSTLCStatusResult;
	
		if (isset($_result_data))
		{
			if ($_result_data->status->level == "SUCCESS")
			{
				// Risultato
				$_data['result'] = 'success';
				
				$_service_status_data = $_result_data->data;
				
				$_data['prev_mode'] = $_service_status_data->mode; 
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = $_result_data->status->errorDescription;
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Errore durante l\'esecuzione del comando.';
	}
	
// === FASE 2 = Cambio STLC1000 Mode ===	
	if ($_data['result'] == 'success')
	{
	// Preparo i parametri per la richiesta SOAP
	$_request->clientType = 'SKC';
	if ($_data['prev_mode'] == "NORMAL") $_mode = "MAINTENANCE";
	else $_mode = "NORMAL";
	$_request->mode = $_mode;
	$_params->request = $_request;
	
	// Chiamo il metodo SOAP con i parametri che ho generato
	$_result = $_client->setSTLCMode($_params);
	
	if (isset($_result))
	{
		// Recupero i dati del risultato
		$_result_data = $_result->setSTLCModeResult;
	
		if (isset($_result_data))
		{
			if ($_result_data->status->level == "SUCCESS")
			{
				// Risultato
				$_data['result'] = 'success';
				
				// Recupero l'attuale configurazione
				$_configuration	= coreGetConfigurationFromSession();

				$_info = $_configuration["info"];
				
				$_info["sk_mode"] = $_mode;
				$_configuration["info"] = $_info;
				
				coreSetConfigurationToSession($_configuration);
				
				$_data['mode'] = $_mode;
			}
			else
			{
				$_data['result'] = 'failure';
				$_data['description'] = $_result_data->status->errorDescription;
			}
		}
		else
		{
			$_data['result'] = 'failure';
			$_data['description'] = 'Errore durante il recupero dei dati del risultato.';
		}
	}
	else
	{
		$_data['result'] = 'failure';
		$_data['description'] = 'Errore durante l\'esecuzione del comando.';
	}
	}
}
else
{
	$_data['result'] = 'failure';
	$_data['description'] = 'Impossibile connettere il client al server.';
}

$_xml = xmlBuild($_data,null,true);

print($_xml);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(xmlBuildComment("XML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

authUpdateActivity();

?>
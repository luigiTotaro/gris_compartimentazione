<?php
/**
* Telefin STLC1000 Consolle
*
* sk_getconfiguration.xml.php - Modulo per generare una configurazione STLC1000 in formatio XML.
*
* @author Enrico Alborali
* @version 1.0.3.2 10/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header("Content-Type: text/xml");

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
require_once("../lib/lib_lang.php");
// Includo la libreria XML
require_once("../lib/lib_xml.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria SOAP
require_once("../lib/lib_soap.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

require_once("../modules/sk_core.php");

$_data = array();

// === FASE 1 = Salvataggio system.xml ===

$_conf_app0 = $_conf_apps[0];
$now   = new DateTime();
$_timestamp = $now->format( 'YmdHis' );
$_config_url = $_conf_app0["config_url"];
$_config_prev_url = str_replace(".xml", "", $_config_url)."_prev.xml";
$_config_back_url = str_replace(".xml", "", $_config_url)."_".$_timestamp.".xml";



// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession(true);

$_info = $_configuration["info"];
if (isset($_info["sk_hostname"]))
{
	$_sk_hostname = "_".$_info["sk_hostname"];
}
else
{
	$_sk_hostname = "";
}

$_stations = $_configuration["stations"];
$_station = $_stations[0];
if (isset($_station))
{
	$_sk_name = "_".$_station->name;
}
else
{
	$_sk_name = "";
}

$_timestamp = "_".$now->format( 'YmdHis' );

$_filename = "system".$_sk_hostname.$_sk_name.$_timestamp.".xml";

header('Content-Disposition: attachment; filename="'.$_filename.'"');

$_xml = spvstandardBuildXMLConfig($_configuration);

print($_xml);

authUpdateActivity();

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* updateSystemXML.php - Script per aggiornare il SystemXML.
*
* @author Enrico Alborali
* @version 1.0.3.2 10/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

// Includo il modulo di versione
require_once("version.php");
// Includo il modulo di configurazione
require_once("conf/sk_config.php");
// Includo la libreria della lingua
require_once("lib/lib_lang.php");
// Includo i moduli di risorse lingua
require_once("lang/".$_conf_console_app_lang."/sk_".$_conf_console_app_lang."_res.php");
// Includo la libreria di log
require_once("lib/lib_log.php");
// Includo la libreria per le variabili
require_once("lib/lib_var.php");
// Includo la libreria per il codice
require_once("lib/lib_code.php");
// Includo la libreria SOAP
require_once("lib/lib_soap.php");
// Includo la libreria di autenticazione
require_once("lib/lib_auth.php");
// Includo il modulo security
global $_conf_security_file;
if( isset($_conf_security_file) === true ){
	require_once ($_conf_security_file);
} else {
	require_once("conf/sk_sec.php");
}
// Includo il modulo core
require_once("modules/sk_core.php");
// Includo la libreria HTML
require_once("lib/lib_html.php");
// Includo la libreria XML
require_once("lib/lib_xml.php");
// Includo la libreria JavaScript
require_once("lib/lib_js.php");
// Includo la libreria AJAX
require_once("lib/lib_ajax.php");

$_configuration	= coreGetConfigurationFromSession(true);

$_conf_app0 = $_conf_apps[0];
$now   = new DateTime();
$_timestamp = $now->format( 'YmdHis' );
$_config_url = $_conf_app0["config_url"];
$_config_back_url = str_replace(".xml", "", $_config_url)."_".$_timestamp.".xml";

print("Eseguo un backup dell'attuale configurazione: ".$_config_back_url."\n");

copy($_config_url,$_config_back_url);

print("Scrivo la configurazione aggiornata: ".$_config_url."\n");

$_return = spvstandardSaveXMLConfig($_config_url,$_configuration);

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_rackposition.html.php - Modulo generazione rack per la selezione della posizione.
*
* @author Enrico Alborali
* @version 1.0.3.6 23/03/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file HTML
header ("content-type: text/html; charset=UTF-8");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria HTML
require_once("../lib/lib_html.php");
// Includo la libreria JavaScript
require_once("../lib/lib_js.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");
// Includo il modulo core
require_once("../modules/sk_core.php");

// 
$_device_id		= varGetRequest("id");
$_type_name		= varGetRequest("type");
$_location_name	= varGetRequest("location");
$_position		= varGetRequest("position");

$_configuration 	= coreGetConfigurationFromSession(true);
$_devices 			= $_configuration["devices"];
$_device_type_list	= $_configuration["device_type_list"];
$_locations 		= $_configuration["locations"];
$_rack_type_list	= $_configuration["rack_type_list"];

$_device		= getDeviceFromDeviceId($_devices,$_device_id);
$_device_type	= getDeviceTypeCodeFromName($_device_type_list,$_type_name);
$_rack			= getRackFromRackLongName($_locations,$_location_name);
$_rack_type		= getRackTypeFromCode($_rack_type_list,$_rack->type);

$_slots = devicesRackSlotsBuild($_devices,$_rack,$_rack_type);

$_html = devicesRackBuild("position","rack_position",440,30,$_rack_type,$_slots,true,$_position,$_device);

print($_html);

// Stampo tempo di generazione file
$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
print(htmlBuildComment("HTML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

//if (varGetRequest('manual')=="true") 
	authUpdateActivity();

?>
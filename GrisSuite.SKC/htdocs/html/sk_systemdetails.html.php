<?php
/**
* Telefin STLC1000 Consolle
*
* sk_systemdetails.html.php - Modulo per l'esportazione informazioni sistema in formato HTML per chiamate AJAX.
*
* @author Enrico Alborali
* @version 1.0.2.1 30/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/html");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria HTML
require_once("../lib/lib_html.php");
// Includo la libreria JavaScript
require_once("../lib/lib_js.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

// Includo il modulo core
require_once("../modules/sk_core.php");

$_configuration = coreGetConfigurationFromSession(true);

$_level = varGetRequest('level');

coreSetLevelToSession($_level);

$_item_id		= varGetRequest('selected_item');

$_devid = varGetRequest('devid');
$_strid = varGetRequest('strid');
$_fieldid = varGetRequest('fieldid');
$_valueid = varGetRequest('valueid');

if ($_strid == null || $_strid == "null") $_strid = 1;
if ($_fieldid == null || $_fieldid == "null") $_fieldid = 0;
if ($_valueid == null || $_valueid == "null") $_valueid = 0;

$_selection = array();
$_selection['ItemID'] = $_item_id;
$_selection['StrID'] = $_strid;
$_selection['FieldID'] = $_fieldid;
$_selection['ValueID'] = $_valueid;

$_data = array();
$_data['level'] = $_level;
$_data['device'] = $_device;
$_data['stream'] = $_stream;
$_data['field'] = $_field;
$_data['selection'] = $_selection;

$_html = systemPageBuildDetailsPanelHTML($_data);

print($_html);

$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
//print(htmlBuildComment("HTML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

if (varGetRequest('manual')=="true") authUpdateActivity();

?>
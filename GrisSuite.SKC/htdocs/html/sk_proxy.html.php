<?php
/**
* Telefin STLC1000 Consolle
*
* sk_proxy.html.php - Modulo proxy per accedere ai file HTML cross domain.
*
* @author Enrico Alborali
* @version 0.0.1 14/07/2011
* @copyright 2011 Telefin S.p.A.
*/
// Imposto l'intestazione per il file HTML
header ("content-type: text/html");

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria HTML
require_once("../lib/lib_html.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");

if (isset($_GET["name"])) 
	$_name = $_GET["name"];
else
{
	if (isset($_POST["name"])) 
		$_name = $_POST["name"];
	else
		$_name = null;
}

if ($_name != null)
{
	$_xml_url = ajaxGetUrl($_name,"html");
	
	$_xml = file_get_contents($_xml_url,FILE_TEXT);
	
	if ($_xml == false)
	{
		$_subdata = array('descr'=>'Unable to read remote file.');
		$_data = array($_subdata);
	
		//$_xml = xmlBuild($_data,"error",false);
	}
}
else
{
	$_subdata = array('descr'=>'Name param is empty.');
	$_data = array($_subdata);
	
	//$_xml = xmlBuild($_data,"error",false);
}

print($_xml);

?>
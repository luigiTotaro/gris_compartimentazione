<?php
/**
* Telefin STLC1000 Consolle
*
* sk_devicesinfo.html.php - Modulo per l'esportazione informazioni periferiche in formato HTML per chiamate AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.5 01/03/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il file XML
header ("content-type: text/html");
$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria HTML
require_once("../lib/lib_html.php");
// Includo la libreria JavaScript
require_once("../lib/lib_js.php");
// Includo la libreria AJAX
require_once("../lib/lib_ajax.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

// Includo il modulo core
require_once("../modules/sk_core.php");

$_configuration = coreGetConfigurationFromSession(true);

$_level = varGetRequest('level');

coreSetLevelToSession($_level);

$_devid 	= varGetRequest('devid');
$_strid 	= varGetRequest('strid');
$_fieldid 	= varGetRequest('fieldid');
$_valueid 	= varGetRequest('valueid');

if ($_strid == null || $_strid == "null") $_strid = 1;
if ($_fieldid == null || $_fieldid == "null") $_fieldid = 0;
if ($_valueid == null || $_valueid == "null") $_valueid = 0;

// Recupero eventuali filtro di stato
$_filter_offline 	= (varGetRequest('filter_offline')==='false')?0x00:0x01;
$_filter_error 		= (varGetRequest('filter_error')==='false')?0x00:0x02;
$_filter_warning 	= (varGetRequest('filter_warning')==='false')?0x00:0x04;
$_filter_ok 		= (varGetRequest('filter_ok')==='false')?0x00:0x08;
$_filter_unknown 	= (varGetRequest('filter_unknown')==='false')?0x00:0x10;



$_filter_iec_in_session = coreGetIeCFilterFromSession();
//print('<!-- IeC in session:'.(int)$_filter_iec_in_session.' -->');
$_filter_iec 		= (varGetRequest('filter_iec')==='true')?true:((varGetRequest('filter_iec')==='false')?false:(($_filter_iec_in_session===true)?true:false));
coreSetIeCFilterToSession($_filter_iec);
//print('<!-- IeC selected:'.(int)$_filter_iec.' -->');

$_selection = array();
$_selection['StrID'] = $_strid;
$_selection['FieldID'] = $_fieldid;
$_selection['ValueID'] = $_valueid;

// Recupero info device
$_sql = codeInit();

$_sql .= <<<HEREDOC
SELECT devices.*,device_status.*,COALESCE(C255,0) AS f255,COALESCE(C9,0) AS f9,COALESCE(C2,0) AS f2,COALESCE(C1,0) AS f1,COALESCE(C0,0) AS f0,COALESCE(Cm255,0) AS fm255 from devices
LEFT JOIN device_status ON devices.DevID = device_status.DevID
LEFT JOIN (SELECT COUNT(DevID) AS C255,DevID from stream_fields WHERE SevLevel = 255 AND Visible = 1 group by DevID) AS sf255 ON sf255.DevID = devices.DevID
LEFT JOIN (SELECT COUNT(DevID) AS C9,DevID from stream_fields WHERE SevLevel = 9 AND Visible = 1 group by DevID) AS sf9 ON sf9.DevID = devices.DevID
LEFT JOIN (SELECT COUNT(DevID) AS C2,DevID from stream_fields WHERE SevLevel = 2 AND Visible = 1 group by DevID) AS sf2 ON sf2.DevID = devices.DevID
LEFT JOIN (SELECT COUNT(DevID) AS C1,DevID from stream_fields WHERE SevLevel = 1 AND Visible = 1 group by DevID) AS sf1 ON sf1.DevID = devices.DevID
LEFT JOIN (SELECT COUNT(DevID) AS C0,DevID from stream_fields WHERE SevLevel = 0 AND Visible = 1 group by DevID) AS sf0 ON sf0.DevID = devices.DevID
LEFT JOIN (SELECT COUNT(DevID) AS Cm255,DevID from stream_fields WHERE SevLevel = -255 AND Visible = 1 group by DevID) AS sfm255 ON sfm255.DevID = devices.DevID
HEREDOC;

$_filter_mask = $_filter_offline+$_filter_error+$_filter_warning+$_filter_ok+$_filter_unknown;

$_filter = array();
$_filter['filter_mask'] = $_filter_mask;

$_filter['iec'] = $_filter_iec;

/* DEBUG
print $_filter_mask;
//*/

$_html = codeInit();

// 
try{
	$_conn = dbConnect();
}
catch(Exception $e){
	$_conn = null;
}

if ($_conn)
{
	try{
		$_result = dbQuery($_conn,$_sql);
		$_device = dbExtractResult($_result);
		
		$_data = array();
		$_data['level'] = $_level;
		$_data['device'] = $_device;
		$_data['filter'] = $_filter;
	}
	catch(Exception $e){
		$_data = null;
	}

	$_html .= devicesPageBuildContentHTML($_data);
	
	//print('<!-- IeC:'.(int)$_filter_iec.' -->');
	
	
	dbClose($_conn);
}
else
{
	$_html .= devicesPageBuildContentHTML($_data, 'Impossibile connettersi al DB.');
	//echo "Connection could not be established.<br />";
	print_r( sqlsrv_errors(SQLSRV_ERR_ALL), true);
}

print($_html);

/*
$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
//print(htmlBuildComment("HTML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));
*/

//print_r($_configuration);

if (varGetRequest('manual')=="true") authUpdateActivity();

?>
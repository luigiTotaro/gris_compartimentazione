<?php
/**
* Telefin STLC1000 Consolle
*
* sk_devicesdetails.html.php - Modulo generazione dettaglio in HTML per chiamate AJAX.
*
* @author Enrico Alborali
* @version 1.0.3.7 02/04/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Above Headers Sent By PHP.
// Imposto l'intestazione per il file HTML
header ("content-type: text/html; charset=UTF-8");

$_time_start = microtime(true);

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
require_once("../lib/lib_db.php");
// Includo la libreria HTML
require_once("../lib/lib_html.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");

// Includo il modulo core
require_once("../modules/sk_core.php");

//$_html = "<div id=\"content_middle\" style=\"height: 130px;\" ><div id=\"device_details_panel\" style=\"height: 130px;\" ><p><br><br><br><br><br><br>BACO!</p></div></div>";
//*
$_configuration = coreGetConfigurationFromSession(true);

$_level = varGetRequest('level');

coreSetLevelToSession($_level);

$_item_id		= varGetRequest('selected_item');
$_stationid 	= varGetRequest('stationid');
$_buildingid	= varGetRequest('buildingid');
$_rackid		= varGetRequest('rackid');
$_devid 		= varGetRequest('devid');
$_strid 		= varGetRequest('strid');
$_fieldid 		= varGetRequest('fieldid');
$_valueid 		= varGetRequest('valueid');

// Recupero info device
$_sql = codeInit();
$_sql .= <<<HEREDOC
SELECT * from devices
LEFT JOIN device_status ON devices.DevID = device_status.DevID
LEFT JOIN rack ON devices.RackID = rack.RackID
LEFT JOIN port ON devices.PortID = port.PortID
LEFT JOIN building ON rack.BuildingID = building.BuildingID
LEFT JOIN station ON building.StationID = station.StationID
HEREDOC;

//WHERE devices.DevID = $_devid

if ($_level == "station" && $_item_id != null && $_item_id != "null") $_sql .= codeChr(1,0)."WHERE station.StationID = ".$_item_id;
if ($_level == "building" && $_item_id != null && $_item_id != "null") $_sql .= codeChr(1,0)."WHERE building.BuildingID = ".$_item_id;
if ($_level == "rack" && $_item_id != null && $_item_id != "null") $_sql .= codeChr(1,0)."WHERE rack.RackID = ".$_item_id;
if ($_level == "device" && $_item_id != null && $_item_id != "null") $_sql .= codeChr(1,0)."WHERE devices.DevID = ".$_item_id;

//print("<!-- QUERY ".$_sql." -->");

$_conn = dbConnect();
$_result = dbQuery($_conn,$_sql);
$_device = dbExtractResult($_result);

// Recupero stream
$_sql = codeInit();
$_sql .= <<<HEREDOC
SELECT * from streams
WHERE streams.DevID = $_item_id
AND streams.Visible = 1
HEREDOC;
$_conn = dbConnect();
$_result = dbQuery($_conn,$_sql);
$_stream = dbExtractResult($_result);

// Verifico la selezione stream
if ($_strid == null || $_strid == "null") 
{
	$_base_stream = $_stream[0];
	if (isset($_base_stream))
	{
		if (isset($_base_stream["StrID"]))
		{
			$_strid = $_base_stream["StrID"];
		}
		else
		{
			$_strid = 1;
		}
	}
}

// Recupero field
$_sql = codeInit();
$_sql .= <<<HEREDOC
SELECT * from stream_fields
WHERE stream_fields.DevID = $_item_id
AND stream_fields.StrID = $_strid
AND stream_fields.Visible = 1
HEREDOC;
$_conn = dbConnect();
$_result = dbQuery($_conn,$_sql);
$_field = dbExtractResult($_result);

// Verifico la selezione field
if ($_fieldid == null || $_fieldid == "null") 
{
	$_base_field = $_field[0];
	if (isset($_base_field))
	{
		if (isset($_base_field["FieldID"]))
		{
			$_fieldid = $_base_field["FieldID"];
		}
		else
		{
			$_fieldid = 1;
		}
	}
}

// Verifico la selezione value
if ($_valueid == null || $_valueid == "null") $_valueid = 0;

// Recupero ACK
$_sql = codeInit();
$_sql .= <<<HEREDOC
SELECT * from device_ack
WHERE device_ack.DevID = $_item_id
HEREDOC;
$_conn = dbConnect();
$_result = dbQuery($_conn,$_sql);
$_ack = dbExtractResult($_result);

// Preparo la selezione
$_selection = array();
$_selection['ItemID'] 	= $_item_id;
$_selection['StrID'] 	= $_strid;
$_selection['FieldID'] 	= $_fieldid;
$_selection['ValueID'] 	= $_valueid;

// Preparo la struttura dati
$_data = array();
$_data['level'] 	= $_level;
$_data['device'] 	= $_device;
$_data['stream'] 	= $_stream;
$_data['field'] 	= $_field;
$_data['ack'] 		= $_ack;
$_data['selection'] = $_selection;

dbClose($_conn);
//*
$_html = devicesPageBuildDetailsPanelHTML($_data);
//*/

print($_html);

// Stampo tempo di generazione file
$_time_end = microtime(true);
$_time = $_time_end-$_time_start;
//print(htmlBuildComment("HTML built on ".date('l jS \of F Y h:i:s A',$_SERVER['REQUEST_TIME'])." in ".$_time." seconds."));

if (varGetRequest('manual')=="true") authUpdateActivity();

?>
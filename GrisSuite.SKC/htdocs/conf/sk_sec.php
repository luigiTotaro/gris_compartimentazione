<?php
/**
* Telefin STLC1000 Consolle
*
* sk_sec.php - Modulo informazioni di sicurezza.
*
* @author Enrico Alborali
* @version 1.0.2.0 25/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/* UTENTI AUTORIZZATI */
authAddUser('admin','Adm1nSKC','admin');
authAddUser('iap','IaP#Telefin','IaP_installer');
authAddUser('installatore','IaP#Telefin','IaP_installer');
authAddUser('guest','guest','operator');

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_profiles.php - Profili ulizzati caricati nel system.xml
*
* @author Davide Ferraretto
* @version 1.0.2.0 16/02/2016
* @copyright 2011-2012 Telefin S.p.A.
*/
						
/* PROFILI UTILIZZATI NEL SYSTEM XML */
/* Parametri:
 * id, name
 * */
addProfile(1,'Profilo di Schedulazione Standard');

// activity associate al profilo 1
/* Parametri:
 * idProfilo, idActivity, name, class, from, to, dow, mode, interval(di default null)
 * */
//addActivity(1, 0, 'Informazioni', 'DEV_INF', '00:00:00', '23:59:59', 'FF', 'interval', '00:01:00'); // Configurazione per AIM Standalone
addActivity(1, 0, 'Informazioni', 'DEV_INF', '00:00:00', '23:59:59', 'FF', 'startup');
addActivity(1, 1, 'Stato Generale', 'STS_GEN', '00:00:00', '23:59:59', 'FF', 'interval', '00:01:00');
addActivity(1, 2, 'Stato Hardware', 'STS_DEV', '00:00:00', '23:59:59', 'FF', 'interval', '00:07:00');
addActivity(1, 3, 'Stato Monitor', 'STS_MON', '00:00:00', '23:59:59', 'FF', 'interval', '00:11:00');
addActivity(1, 4, 'Stato Standard', 'STS_STD', '00:00:00', '23:59:59', 'FF', 'interval', '00:13:00');
addActivity(1, 5, 'Stato Rete', 'STS_NET', '00:00:00', '23:59:59', 'FF', 'interval', '00:30:00');
addActivity(1, 6, 'Set DataOra', 'DT_SET', '00:00:00', '23:59:59', 'FF', 'interval', '06:00:00');
addActivity(1, 7, 'Eventi Periferiche', 'DEV_EVE', '00:00:00', '23:59:59', 'FF', 'interval', '00:15:00');
//addActivity(1, 8, 'Eventi Sistema', 'SYS_EVE', '00:00:00', '23:59:59', 'FF', 'interval', '00:30:00');

?>
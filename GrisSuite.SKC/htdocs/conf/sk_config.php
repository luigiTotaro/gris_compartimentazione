<?php
/**
* Telefin STLC1000 Consolle
*
* sk_config.php - Modulo per la configurazione dell'applicazione web.
*
* @author Enrico Alborali
* @version 1.0.4.0 10/04/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

// Parametri log ed errori
error_reporting(E_ERROR | E_PARSE );
ini_set("display_errors", 1);
$_conf_log_file				= "C:\PROGRA~1\Telefin\Log\skc.log";
$_conf_log_debug_mode		= false;
$_conf_log_visual_element	= "log";
$_conf_log_visual_mode		= false;

$_conf_console_app_name			= "Telefin STLC1000 Consolle";
$_conf_console_app_vendor 		= "Telefin S.p.A.";
$_conf_console_app_lang 		= "it";
$_conf_console_app_copyright 	= "&copy; 2015-2016 RFI";
$_conf_console_app_logo 		= "stlc1000consolle";

$_conf_console_app_auto_add_tcpip					= true; // Default=true
$_conf_console_app_auto_add_serial					= true; // Default=true
$_conf_console_app_auto_add_ethernet 				= true; // Defualt=true
$_conf_console_app_bottombar_fbwf_icon_visible 		= true; // Default=true
$_conf_console_app_navbar_serial_selector_visible 	= true; // Default=true
$_conf_console_app_navbar_ethernet_selector_visible = true; // Defualt=true
$_conf_console_app_system_edit_enabled 				= true; // Default=true
$_conf_console_app_network_import_enabled 			= true; // Default=true
$_conf_console_app_system_info_edit_enabled 		= true; // Default=true

$_conf_console_app_hardware_device_code	= 'STLC1000';
$_conf_console_app_hardware_serial_ports = '{"ports": [{"id": 1, "name": "Porta COM1 (RS232)", "com": 1, "type": "RS232"},{"id": 2, "name": "Porta COM2 (RS232)", "com": 2, "type": "RS232"},{"id": 3, "name": "Porta COM3 (RS485)", "com": 3, "type": "STLC1000_RS485"},{"id": 4, "name": "Porta COM4 (RS485)", "com": 4, "type": "STLC1000_RS485"}]}';
$_conf_console_app_hardware_serial_ports_import_rule = '{"ports_import_rule": [{"from_type": "TCP_Client", "to_type": "TCP_Client"},{"from_type": "RS232", "to_type": "RS232"},{"from_type": "RS485", "to_type": "STLC1000_RS485", "failover_type": "STLC1000_RS422"},{"from_type": "RS422", "to_type": "STLC1000_RS422", "failover_type": "STLC1000_RS485"},{"from_type": "STLC1000_RS485", "to_type": "STLC1000_RS485", "failover_type": "STLC1000_RS422"},{"from_type": "STLC1000_RS422", "to_type": "STLC1000_RS422", "failover_type": "STLC1000_RS485"}]}';
$_conf_console_pointer_visible = true;
$_conf_console_message_autoclose_timeout = 5000; //msec

$_conf_console_code_indent = true;
$_conf_console_code_scramble = false;

$_conf_console_ajax_cross_domain_mode	= false;
$_conf_console_ajax_base_url 			= "";
$_conf_console_ajax_repeat_timeout 		= 30000;

$_conf_console_session_activity_timeout				= 3600; //sec
$_conf_console_ajax_DevicesInfo_repeat_timeout 		= 14; //sec
$_conf_console_ajax_DevicesDetails_repeat_timeout 	= 15; //sec
$_conf_console_ajax_SystemInfo_repeat_timeout 		= 58; //sec
$_conf_console_ajax_SystemDetails_repeat_timeout 	= 60; //sec
$_conf_console_ajax_Status_repeat_timeout 			= 46; //sec
$_conf_console_ajax_Info_repeat_timeout 			= 115; //sec
$_conf_console_ajax_ServiceStatus_repeat_timeout 	= 120; //sec
$_conf_console_ajax_Network_repeat_timeout			= 125; //sec

$_conf_console_html_color_code = array();
$_conf_console_html_color_code["green"] 	= "#33ff33";
$_conf_console_html_color_code["yellow"] 	= "#cda400";
$_conf_console_html_color_code["red"] 		= "#ff3333";
$_conf_console_html_color_code["gray"] 		= "#999999";
$_conf_console_html_color_code["dark_gray"] = "#333333";
$_conf_console_html_color_code["white"] 	= "#ffffff";
$_conf_console_html_color_code["black"] 	= "#000000";

$_conf_console_db_server = ".\SQLEXPRESS";
$_conf_console_db_name = "Telefin";

$_conf_console_upload_dir = "C:/PROGRA~1/Telefin/Config/";
$_conf_console_upload_filename = "system_upload";

$_conf_apps = array();

// Supervisore Classico
$_conf_app = array();
$_conf_app['name'] 			= "spvstandard";
$_conf_app['description'] 	= "Supervisore Standard";
$_conf_app['config_type'] 	= "xml";
$_conf_app['config_url'] 					= "C:/PROGRA~1/Telefin/Config/system.xml";
$_conf_app['region_list_path'] 				= "C:/PROGRA~1/Telefin/SKC/xml/region_list.xml";
$_conf_app['remote_region_list_url'] 		= "http://10.246.100.12/centralws/central.asmx/GetRegionList";
$_conf_app['remote_region_list_path'] 		= "C:/PROGRA~1/Telefin/SKC/xml/remote_region_list.xml";
$_conf_app['device_type_list_path'] 		= "C:/PROGRA~1/Telefin/SKC/xml/device_type_list.xml";
$_conf_app['remote_device_type_list_url'] 	= "http://10.246.100.12/centralws/central.asmx/GetDeviceTypeList";
$_conf_app['remote_device_type_list_path'] 	= "C:/PROGRA~1/Telefin/SKC/xml/remote_device_type_list.xml";
$_conf_app['station_type_list_path'] 		= "C:/PROGRA~1/Telefin/SKC/xml/station_type_list.xml";
$_conf_app['building_type_list_path'] 		= "C:/PROGRA~1/Telefin/SKC/xml/building_type_list.xml";
$_conf_app['rack_type_list_path'] 			= "C:/PROGRA~1/Telefin/SKC/xml/rack_type_list.xml";
$_conf_app['port_type_list_path'] 			= "C:/PROGRA~1/Telefin/SKC/xml/port_type_list.xml";
$_conf_apps[] = $_conf_app;


// indica qual'� il file relativo alla security
// se non vinene specificato nulla cerca il file "conf/sk_sec.php"
//$_conf_security_file = "conf/sk_sec.php";

// parametri di configurazione per il file server
//$_conf_xml_server_name = "Server STLC100012"; //nome parametro su file > ForcedServerName
//$_conf_xml_server_type = "STLC10001a"; //nome parametro su file > ForcedServerType
//$_conf_xml_server_srvid = "12.456.23"; //nome parametro su file > ForcedServerID
//$_conf_xml_server_host = "STLC1"; //nome parametro su file > VirtualHostName


// file di configurazione dal quale ottenere i vari attributi, nel caso non siano settati da xml
//$_conf_xml_file = "C:/Program Files (x86)/Telefin/STLCManager/STLCManagerService.exe.config";


// parametri per gli script php utilizzati in modalit� offline(senza web server)
// tutti i parametri sono opzionali
//$_conf_default_building_code = "line";
//$_conf_default_building_note = "";

?>
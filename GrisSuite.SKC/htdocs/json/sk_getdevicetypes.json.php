<?php
/**
* Telefin STLC1000 Consolle
*
* sk_getdevicetypes.json.php - Modulo per la generazione della lista dei tipi periferica in formato JSON.
*
* @author Enrico Alborali
* @version 1.0.2.1 30/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il codice Json
header('Content-type: application/json');

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
// require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
//require_once("../lib/lib_lang.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");
// Includo il modulo core
require_once("../modules/sk_core.php");

$_term		= $_REQUEST["term"];

$_result = array();

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession(true);

$_device_type_list = $_configuration["device_type_list"];

if (isset($_device_type_list) && count($_device_type_list) > 0)
{
	foreach ($_device_type_list as $_device_type)
	{
		if (isset($_device_type))
		{
			if ($_term == "" || stripos($_device_type->getDisplayName(),$_term) !== false) {
				$_row = $_device_type->getDisplayName();
				$_result[] = $_row;
			}
		}
	}
}

if (count($_result) == 0) $_result[] = "Nessun risultato trovato";

$_json = json_encode($_result);

print($_json);

authUpdateActivity();

?>
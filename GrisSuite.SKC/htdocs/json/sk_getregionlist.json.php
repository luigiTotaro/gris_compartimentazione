<?php
/**
* Telefin STLC1000 Consolle
*
* sk_getregionlist.json.php - Modulo per la generazione della lista delle stazioni in formato JSON.
*
* @author Enrico Alborali
* @version 1.0.0.4 13/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il codice Json
header('Content-type: application/json');

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
// require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
//require_once("../lib/lib_lang.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");
// Includo il modulo core
require_once("../modules/sk_core.php");

$_term		= $_REQUEST["term"];

$_result = array();

$_conf_app0 = $_conf_apps[0];
$_configuration['region_list'] = coreLoadXMLData($_conf_app0["name"],"LoadXMLRegionList",$_conf_app0["region_list_path"]);

$_region_list = $_configuration['region_list'];

if (isset($_region_list) && count($_region_list) > 0)
{
	foreach ($_region_list as $_region_item)
	{
		if (isset($_region_item))
		{
			$_region	= $_region_item['data'];
			$_zone_list = $_region_item['zone_list'];
			
			if (isset($_zone_list))
			foreach ($_zone_list as $_zone_item)
			{
				if (isset($_zone_item))
				{
					$_zone		= $_zone_item['data'];
					$_node_list	= $_zone_item['node_list'];
					
					if (isset($_node_list))
					foreach ($_node_list as $_node_item)
					{
						if (isset($_node_item))
						{
							$_node	= $_node_item['data'];
							
							if ($_term == "" || stripos($_node->name,$_term) !== false) {
								// We found a string inside string
								//$_id		= $_region->id.".".$_zone->id.".".$_node->id;
								$_keyword 	= $_region->name." > ".$_zone->name." > ".$_node->name;
							
								//$_row['id']	= $_id;
        						$_row['value'] 	= $_keyword;
							
								$_result[] = $_row;
							}
						}
					}		
				}
			}
		}
	}
}

if (count($_result) == 0) $_result[] = "Nessun risultato trovato";

$_json = json_encode($_result);

print($_json);

authUpdateActivity();

?>
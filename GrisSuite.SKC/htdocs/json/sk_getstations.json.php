<?php
/**
* Telefin STLC1000 Consolle
*
* sk_getstations.json.php - Modulo per la generazione della lista delle stazioni configurate in formato JSON.
*
* @author Enrico Alborali
* @version 1.0.2.1 30/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/
// Imposto l'intestazione per il codice Json
header('Content-type: application/json');

// Includo il modulo di versione
require_once("../version.php");
// Includo il modulo di configurazione
require_once("../conf/sk_config.php");
// Includo la libreria di log
require_once("../lib/lib_log.php");
// Includo la libreria per le variabili
require_once("../lib/lib_var.php");
// Includo la libreria per il codice
require_once("../lib/lib_code.php");
// Includo la libreria di accesso al DB
// require_once("../lib/lib_db.php");
// Includo la libreria delle lingua
//require_once("../lib/lib_lang.php");
// Includo la libreria di autenticazione
require_once("../lib/lib_auth.php");
// Includo il modulo core
require_once("../modules/sk_core.php");

$_result = array();

// Recupero l'attuale configurazione
$_configuration	= coreGetConfigurationFromSession(true);

$_stations = $_configuration["stations"];

if (isset($_stations) && count($_stations) > 0)
{
	foreach ($_stations as $_station)
	{
		if (isset($_station))
		{
			$_value = $_station->getDisplayName();
		
			$_row['value'] 	= $_value;
			//$_row['label'] 	= $_value;
		
			$_result[] = $_row;
		}
	}
}

$_json = json_encode($_result);

print($_json);

authUpdateActivity();

?>
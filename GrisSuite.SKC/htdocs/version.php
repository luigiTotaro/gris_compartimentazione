<?php
/**
* Telefin STLC1000 Consolle
*
* version.php - File informazioni di versione.
*
* @author Enrico Alborali
* @version 1.0.3.12 02/08/2013
* @copyright 2011-2013 Telefin S.p.A.
*/
 
DEFINE('VERSION', '1.0.4.6');

/**
* Funzione per ottenere la versione del software
*/
function getVersion()
{
	$_version = VERSION;
 
	return $_version;
}

/**
* Funzione per stampare la versione del software
*/
function printVersion()
{
	$_version = getVersion();
 
	print $_version;
 
	return $_version;
}
 
?>
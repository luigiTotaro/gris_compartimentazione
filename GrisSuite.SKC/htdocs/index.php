<?php
/**
* Telefin STLC1000 Consolle
*
* index.php - Modulo principale.
*
* @author Enrico Alborali
* @version 1.0.3.2 10/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

// Includo il modulo di versione
require_once("version.php");
// Includo il modulo di configurazione
require_once("conf/sk_config.php");
// Includo la libreria della lingua
require_once("lib/lib_lang.php");
// Includo i moduli di risorse lingua
require_once("lang/".$_conf_console_app_lang."/sk_".$_conf_console_app_lang."_res.php");
// Includo la libreria di log
require_once("lib/lib_log.php");
// Includo la libreria per le variabili
require_once("lib/lib_var.php");
// Includo la libreria per il codice
require_once("lib/lib_code.php");
// Includo la libreria SOAP
require_once("lib/lib_soap.php");
// Includo la libreria di autenticazione
require_once("lib/lib_auth.php");
// Includo il modulo security
global $_conf_security_file;
if( isset($_conf_security_file) === true ){
	require_once ($_conf_security_file);
} else {
	require_once("conf/sk_sec.php");
}

// Eventuale comando di uscita
$_exit 		= $_REQUEST["exit"];
if ($_exit == "true")
{
	// Inizializzo la sessione
	//session_start();
	
	if (isset($_SESSION['user']))
	{
		$_SESSION['configuration'] = null;
		unset($_SESSION['configuration']);
		$_SESSION['page'] = "devices";
		$_SESSION['zoom'] = 4;
	
		$_SESSION['user'] = null;
		unset($_SESSION['user']);
	
		$_SERVER['PHP_AUTH_DIGEST'] = "none";
		//unset($_SERVER['PHP_AUTH_DIGEST']);
		$_SERVER['PHP_AUTH_USER'] = "none";
		unset($_SERVER['PHP_AUTH_USER']);
	
		// Chiudo la sessione
		session_write_close();
	
		authHeader();
	}
	else
	{
		// Header di autenticazione
		authHeader();

		// Controllo autenticazione
		$_checked = authCheckUser();
				
		if ($_checked !== false)
		{
			header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
			
			// Salvo i dati utente locale
			authSetUser($_checked);
		}
		else
		{
			$_SERVER['PHP_AUTH_DIGEST'] = "none";
			unset($_SERVER['PHP_AUTH_DIGEST']);
			$_SERVER['PHP_AUTH_USER'] = "none";
			unset($_SERVER['PHP_AUTH_USER']);
		}
	}
	exit();
}

// Eventuale comando di ricaricamento configurazione
$_reload 		= $_REQUEST["reload"];
if ($_reload == "true")
{
	// Inizializzo la sessione
	session_start();
	
	$_SESSION['configuration'] = null;
	unset($_SESSION['configuration']);
	$_SESSION['page'] = "devices";
	$_SESSION['zoom'] = 4;
		
	// Chiudo la sessione
	session_write_close();
	
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
}

// Recupero la pagina attiva
$_page		= $_REQUEST["page"];
if (empty($_page))
{
	$_page = "devices";
}
// Recupero il livello attivo
$_level		= $_REQUEST["level"];

// Includo il modulo core
require_once("modules/sk_core.php");
// Includo la libreria HTML
require_once("lib/lib_html.php");
// Includo la libreria JavaScript
require_once("lib/lib_js.php");
// Includo la libreria AJAX
require_once("lib/lib_ajax.php");

logDebug("==============================[ ".$_conf_console_app_name." ]==============================");

$_gris_username = null;

// Tento l'autenticazione da GRIS
$_gris_checked = authCheckGRISUser($_gris_username);

if ($_gris_checked === false)
{
	$_user = authGetUser();
		
	if (empty($_user))
	{
		// Header di autenticazione
		authHeader();

		// Controllo autenticazione
		$_checked = authCheckUser();
	}
}

if ($_gris_checked === false && $_checked === false)
{
	authHeader();
}
else
{
	header('Content-language: '.$_conf_console_app_lang);
	
	if (empty($_user) && $_gris_checked)
	{
		// Salvo i dati utente da GRIS
		authSetUser($_gris_checked,$_gris_username);
	}
	else if (empty($_user) && $_checked)
	{
		// Salvo i dati utente locale
		authSetUser($_checked);
	}	
	
	// Salvo in sessione la pagina attiva
	coreSetPageToSession($_page);
	// Recupero il livello della pagina attiva
	if (isset($_level))
	{
		// Salvo in sessione il livello
		coreSetLevelToSession($_level);
	}
	else
	{
		$_funcGetPageLevel = $_page."GetPageLevel";
		$_level = $_funcGetPageLevel();
		coreSetLevelToSession($_level);
	}
	
	$_configuration	= coreGetConfigurationFromSession(true);
	
	// Stampo la consolle
	coreConsolePrint($_page);
	
	$_upload 		= $_REQUEST["upload"];
	if ($_upload != "true") coreDeleteMessageFromSession();
		
	authUpdateActivity();
}
?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_it_res.php - File risorse lingua italiana per la GUI.
*
* @author Enrico Alborali
* @version 0.0.1 14/11/2011
* @copyright 2011 Telefin S.p.A.
*/

$_res_msg = array();
$_res_msg['sk_auth_unauthorized'] = "Non sei autorizzato.";
$_res['msg'] = $_res_msg;

$_res_gui = array();

$_res_gui['sk_topbar_user'] = "Utente";
$_res_gui['sk_topbar_devices_button_title'] = "Periferiche";
$_res_gui['sk_topbar_system_button_title'] = "Sistema";
$_res_gui['sk_bottombar_sk_version'] = "v.";
$_res_gui['sk_bottombar_version'] = "Versione";
$_res_gui['sk_bottombar_copyright'] = "Copyright";
$_res_gui['sk_bottombar_credits'] = "Designed by";

$_res_devices = array();

/* LOG */
$_res_devices['xml_sk_deviceconfig_1'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_2'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_3'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_4'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_5'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_6'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_7'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_8'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_9'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_10'] = "Errore durante la creazione dell'oggetto.";
$_res_devices['xml_sk_deviceconfig_11'] = "Errore durante la creazione dell'oggetto.";


$_res = array();

$_res['gui'] = $_res_gui;
$_res['devices'] = $_res_devices;

?>
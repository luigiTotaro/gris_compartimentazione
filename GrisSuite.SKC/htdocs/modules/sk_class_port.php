<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_port.php - Classe porte di comunicazione dell'STLC1000.
*
* @author Enrico Alborali
* @version 1.0.4.1 26/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

/**
* Funzione per verificare se la categoria della porta e' seriale
*/
function portTypeIsSerial($type)
{
	$_is_serial = null;
	
	switch ($type) {
    	case "serial":
        case "RS232":
        case "STLC1000_RS422":
        case "STLC1000_RS485":
        case "RS422":
        case "RS485":
           	$_is_serial = true;
        	break;
    	case "TCP_Client":
        	$_is_serial = false;
        	break;
        case "ETH":
        	$_is_serial = false;
        	break;
    	default:
       		$_is_serial = false;
	}
	
	return($_is_serial);
}

/**
* Funzione per codificare un indirizzo seriale
*/
function portEncodeSerialAddress($serial_addr,$serial_size="16",$serial_modem="--")
{
	$_address = null;
	
	if ($serial_modem == "--" || $serial_modem == "FF") $serial_modem = "0";
	if ($serial_size == "16")
	{
		if (((int)$serial_modem) >= 0 && ((int)$serial_modem) <= 15 && ((int)$serial_addr) >= 0 && ((int)$serial_addr) <= 15)
			$_address = bcmul($serial_modem,"16");
	}
	else if ($serial_size == "8")
	{
		if (((int)$serial_modem) >= 0 && ((int)$serial_modem) <= 31 && ((int)$serial_addr) >= 0 && ((int)$serial_addr) <= 7)
			$_address = bcmul($serial_modem,"8");
	}
	
	if (isset($_address))
	{
		$_address = bcadd($_address,$serial_addr);
		$_address = sprintf("%02X",(int)$_address);
	}
	
	return($_address);
}

/**
* Funzione per rappresentare un indirizzo seriale
*/
function portDecodeSerialAddress($address,$serial_size="16",$serial_modem="--",$full=false)
{
	$_address_text = "";
	
	if (empty($serial_modem) || $serial_modem == "--" || $serial_modem == "FF")
	{
		$_intaddr = sscanf($address, "%02X");
		$_address_text = sprintf("%02d",(int)$_intaddr[0]);
	}
	else
	{
		$_intaddr = sscanf($address, "%02X");
		
		if ($serial_size == "16")
		{
			$_serial_addr = (int)((int)$_intaddr[0])%16;
		}
		else
		{
			$_serial_addr = (int)((int)$_intaddr[0])%8;
		}
		if ($full)
			$_address_text = "S:".$serial_size." M:".$serial_modem." A:".sprintf("%02d",$_serial_addr);
		else
			$_address_text = sprintf("%02d",$_serial_addr);
	}
	
	return($_address_text);
}

/**
* Funzione per codificare un indirizzo IP
*/
function portEncodeIPAddress($ip1,$ip2,$ip3,$ip4)
{
	$_address = null;
	
	if (isset($ip1) && isset($ip2) && isset($ip3) && isset($ip4) && $ip1!="" && $ip2!="" && $ip3!="" && $ip4!=""
	&& $ip1>=0 && $ip1<=255 && $ip2>=0 && $ip2<=255 && $ip3>=0 && $ip3<=255 && $ip4>=0 && $ip4<=255
	&& is_numeric($ip1) && is_numeric($ip2) && is_numeric($ip3) && is_numeric($ip4))
	{
		$_address = $ip1.".".$ip2.".".$ip3.".".$ip4;
	}
		
	return($_address);
}

/**
* Funzione per rappresentare un indirizzo seriale
*/
function portDecodeIPAddress($address)
{
	$_address_tokens = "";
	
	if (isset($address) && $address != "")
	{
		$_address_tokens = explode(".", $address);
	}
		
	return($_address_tokens);
}

/**
*
*/
function portHexToIntAddress($hexAddress)
{
	$_int_address = null;
	
	if (isset($hexAddress))
	{
		if ($hexAddress == "--" || $hexAddress == "FF")
		{
			$_int_address = "--";
		}
		else
		{
			$_intaddr = sscanf($hexAddress, "%02X");
			$_int_address = sprintf("%02d",$_intaddr[0]);
		}
	}
	
	return($_int_address);
}

/**
*
*/
function portIntToHexAddress($intAddress)
{
	$_hex_address = null;
	
	if (isset($intAddress))
	{
		if ($intAddress == "--" || $intAddress == "FF")
		{
			$_hex_address = "--";
		}
		else
		{
			$_hexaddr = sscanf($intAddress, "%02d");
			$_hex_address = sprintf("%02X",$_hexaddr[0]);
		}
	}
	
	return($_hex_address);
}

/**
* Implementazione della classe dei parametri Port
*/
class port_params{
	public $type;
	public $name;
	public $timeout;
	public $persistent;
	public $com;
	public $baud;
	public $echo;
	public $data;
	public $stop;
	public $parity;
	public $ip;
	public $subnet;
	public $ip2;
	public $subnet2;
	public $ip3;
	public $subnet3;
	public $ip4;
	public $subnet4;
	public $gateway;
	public $dns1;
	public $dns2;
	
	private $active;
	
	/**
	* Costruttore classe Port
	*/
	function __construct($type=null,$name=null,$timeout=null,$persistent=null,$com=null,$baud=null,$echo=null,$data=null,$stop=null,$parity=null,$port=null,$ip=null,$subnet=null,$ip2=null,$subnet2=null,$ip3=null,$subnet3=null,$ip4=null,$subnet4=null,$gateway=null,$dns1=null,$dns2=null){
		$this->type 		= $type;
		$this->name 		= $name;
		$this->timeout 		= $timeout;
		$this->persistent 	= $persistent;
		$this->com 			= $com;
		$this->baud 		= $baud;
		$this->echo 		= $echo;
		$this->data 		= $data;
		$this->stop 		= $stop;
		$this->parity 		= $parity;
		$this->port			= $port;
		$this->ip 			= $ip;
		$this->subnet 		= $subnet;
		$this->ip2 			= $ip2;
		$this->subnet2 		= $subnet2;
		$this->ip3 			= $ip3;
		$this->subnet3 		= $subnet3;
		$this->ip4 			= $ip4;
		$this->subnet4 		= $subnet4;
		$this->gateway 		= $gateway;
		$this->dns1 		= $dns1;
		$this->dns2 		= $dns2;
	}
}

/**
* Implementazione della classe Port
*/
class port extends port_params{
	public $id;
		
	/**
	* Costruttore classe Port
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di una porta di comunicazione
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== port:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "item";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		$_item_timeout		= null;
		$_item_persistent	= null;
		// Parametri porte seriali
		$_item_com			= null;
		$_item_baud			= null;
		$_item_echo			= null;
		$_item_data			= null;
		$_item_stop			= null;
		$_item_parity		= null;
		// Parametri porte socket e ethernet
		$_item_ip			= null;
		$_item_port			= null;
		// Parametri porte ETH
		$_item_subnet		= null;
		// Eventuali indirizzi IP secondari per porte ETH
		$_item_ip_2			= null;
		$_item_subnet_2		= null;
		$_item_ip_3			= null;
		$_item_subnet_3		= null;
		$_item_ip_4			= null;
		$_item_subnet_4		= null;
		$_item_gateway		= null;
		$_item_dns_1		= null;
		$_item_dns_2		= null;

		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('id');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_timeout 		= $xmlElement->getAttribute('timeout');
			$_item_persistent	= $xmlElement->getAttribute('persistent');
			// Recupero parametri porte seriali
			$_item_com 			= $xmlElement->getAttribute('com');
			$_item_baud 		= $xmlElement->getAttribute('baud');
			$_item_echo			= $xmlElement->getAttribute('echo');
			$_item_data			= $xmlElement->getAttribute('data');
			$_item_stop 		= $xmlElement->getAttribute('stop');
			$_item_parity 		= $xmlElement->getAttribute('parity');
			// Recupero parametri porte socket e ethernet
			$_item_ip			= $xmlElement->getAttribute('ip');
			$_item_port			= $xmlElement->getAttribute('port');
			// Recupero parametri porte socket
			$_item_subnet		= $xmlElement->getAttribute('subnet');
			$_item_ip_2			= $xmlElement->getAttribute('ip2');
			$_item_subnet_2		= $xmlElement->getAttribute('subnet2');
			$_item_ip_3			= $xmlElement->getAttribute('ip3');
			$_item_subnet_3		= $xmlElement->getAttribute('subnet3');
			$_item_ip_4			= $xmlElement->getAttribute('ip4');
			$_item_subnet_4		= $xmlElement->getAttribute('subnet4');
			$_item_gateway		= $xmlElement->getAttribute('gateway');
			$_item_dns_1		= $xmlElement->getAttribute('dns1');
			$_item_dns_2		= $xmlElement->getAttribute('dns2');
		
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "serial";
				if (!isset($_item_name))
					$_item_name = "Porta";
					
				logDebug("Extracted port ".$_item_id.":".$_item_type.",".$_item_name.",".$_item_timeout.",".$_item_persistent.",".$_item_com.",".$_item_baud.",".$_item_echo.",".$_item_data.",".$_item_stop.",".$_item_stop.",".$_item_parity.",".$_item_ip.",".$_item_port);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
				$this->timeout		= $_item_timeout;
				$this->persistent	= $_item_persistent;
				// Salvo parametri porte seriali
				$this->com			= $_item_com;
				$this->baud			= $_item_baud;
				$this->echo			= $_item_echo;
				$this->data			= $_item_data;
				$this->stop			= $_item_stop;
				$this->parity		= $_item_parity;
				// Salvo parametri porte socket e ethernet
				$this->ip			= $_item_ip;
				$this->port			= $_item_port;
				// Salvo parametri porte ethernet
				$this->subnet		= $_item_subnet;
				$this->ip2			= $_item_ip_2;
				$this->subnet2		= $_item_subnet_2;
				$this->ip3			= $_item_ip_3;
				$this->subnet3		= $_item_subnet_3;
				$this->ip4			= $_item_ip_4;
				$this->subnet4		= $_item_subnet_4;
				$this->gateway		= $_item_gateway;
				$this->dns1			= $_item_dns_1;
				$this->dns2			= $_item_dns_2;
			}
			else
			{
				logEvent("Impossibile recuperare l'id della porta di comunicazione.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML per la porta
	*/
	function buildXMLConfig()
	{
		logDebug("=== port:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		if (portTypeIsSerial($this->type))
			$_xml .= codeChr(1,2).'<item id="'.$this->id.'" name="'.$this->name.'" type="'.$this->type.'" com="'.$this->com.'" baud="'.$this->baud.'" echo="'.$this->echo.'" data="'.$this->data.'" stop="'.$this->stop.'" parity="'.$this->parity.'" timeout="'.$this->timeout.'" persistent="'.$this->persistent.'" />';
		else if ($this->type == "ETH"){
			$_extra_ip = '';
			if (isset($this->ip2)) $_extra_ip .= ' ip2="'.$this->ip2.'" subnet2="'.$this->subnet2.'"';
			if (isset($this->ip3)) $_extra_ip .= ' ip3="'.$this->ip3.'" subnet3="'.$this->subnet3.'"';
			if (isset($this->ip4)) $_extra_ip .= ' ip4="'.$this->ip4.'" subnet4="'.$this->subnet4.'"';
			$_xml .= codeChr(1,2).'<item id="'.$this->id.'" name="'.$this->name.'" type="'.$this->type.'" port="'.$this->port.'" ip="'.$this->ip.'" subnet="'.$this->subnet.'"'.$_extra_ip.' gateway="'.$this->gateway.'" dns1="'.$this->dns1.'" dns2="'.$this->dns2.'" />';
		}
		else
			$_xml .= codeChr(1,2).'<item id="'.$this->id.'" name="'.$this->name.'" type="'.$this->type.'" timeout="'.$this->timeout.'" persistent="'.$this->persistent.'" ip="'.$this->ip.'" port="'.$this->port.'"/>';
				
		return($_xml);
	}
	
	/**
	* Metodo per ottenere il PortID a 64 bit in formato stringa per un oggetto port
	*/
	function getPortId()
	{
		$_id = $this->id;
		$_id = bcmul($_id,"65536");
		
		//$_id = bcadd($_id,"0");
		$_id = bcmul($_id,"65536");
		
		//$_id = bcadd($_id,"0");
		$_id = bcmul($_id,"65536");
		
		//$_id = bcadd($_id,"0");
		
		return($_id);
	}
	
	/**
	* Metodo per ottenere il DisplayName dell'armadio
	*/
	function getDisplayName()
	{
		global $_configuration;
		
		$_name = $this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere una porta dalla lista
*/
function getPortFromId($portList,$portId)
{
	$_port = null;
	
	$_list = $portList;
	$_id = $portId;
	
	foreach ($_list as $_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->id == $_id)
			{
				$_port = $_item;
			 	break;
			}
		}
	}
	
	return($_port);
}

/**
* Funzione per ottenere l'indice di una porta dalla lista
*/
function getPortIndexFromId($portList,$portId)
{
	$_port_index = null;
	
	$_list = $portList;
	$_id = $portId;
	
	foreach ($_list as $_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->id == $_id)
			{
				$_port_index = $_index;
			 	break;
			}
		}
	}
	
	return($_port_index);
}

/**
* Funzione per ottenere una porta dalla lista
*/
function getPortDisplayNameFromId($portList,$portId)
{
	$_name = null;
	
	$_list = $portList;
	$_id = $portId;
	
	foreach ($_list as $_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->id == $_id)
			{
				$_name = $_item->getDisplayName();
			 	break;
			}
		}
	}
	
	return($_name);
}

/**
* Funzione per ottenere id porta dal suo display name
*/
function getPortIdFromPortLongName($portList,$portLongName)
{
	$_id = null;

	$_list 		= $portList;
	$_long_name = $portLongName;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_value = $_item->getDisplayName();
				
				if ($_value == $_long_name)
				{
					$_id = $_item->id;
					break;
				}
			}
		}
	}
	
	return($_id);
}

/**
* Funzione per ottenere una porta dal suo display name
*/
function getPortFromPortLongName($portList,$portLongName)
{
	$_port = null;

	$_list 		= $portList;
	$_long_name = $portLongName;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_value = $_item->getDisplayName();
				
				if ($_value == $_long_name)
				{
					$_port = $_item;
					break;
				}
			}
		}
	}
	
	return($_port);
}

/**
* Funzione per ottenere la prima porta di un certo tipo
*/
function getFirstPortFromType($portList,$portType)
{
	$_port = null;

	$_list 		= $portList;
	$_port_type = $portType;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_item_type = $_item->type;
				
				if ($_item_type == $_port_type)
				{
					$_port = $_item;
					break;
				}
			}
		}
	}
	
	return($_port);
}

/**
* Funzione per ottenere il primo id disponibile per una porta
*/
function getFirstFreePortId($list)
{
	$_list = $list;
	
	$_found = false;
	$_id = 0;
	
	while (!$_found && $_id < 65535)
	{
		$_found = true;
	
		foreach ($_list as $_item_index => $_item)
		{
			if (isset($_item))
			{
				if ($_item->id == $_id)
				{
					$_found = false;
			 		$_id++;
			 		break;
				}
			}
		}
	}
	
	if (!$_found) $_id = null;
	
	return($_id);
}

/**
* Implementazione della classe port type
*/
class port_type{
	// Parametri comuni
	public $id;
	public $code;
	public $name;
	public $max;
	public $maxNum;
	public $minNum;
	public $category;
	public $addrNames; // array
	public $addrMinValues; // array
	public $addrMaxValues; // array
	// Parametri di default
	public $defaultName;
	public $defaultNum; // COM e PORT
	// Parametri di default porte seriali (baud="2600" echo="true" data="8" stop="0" parity="N" timeout="2600")
	public $defaultTimeout;
	public $defaultPersistent;
	public $defaultBaud;
	public $defaultEcho;
	public $defaultData;
	public $defaultStop;
	public $defaultParity;
	// Parametri di default porte socket e ethernet
	public $defaultIP;
	// Parametri di default porte ETH
	public $defaultSubnet;
	public $defaultGateway;
	public $defaultDNS1;
	public $defaultDNS2; 
	
	/**
	* Costruttore classe port type
	*/
	function __construct($id){
		$this->id = $id; 
	}

	/**
	* Funzione per estrarre la configurazione di un port type
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== device_type:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "type";
	
		// Parametri comuni
		$_item_code 	= null;
		$_item_name 	= null;
		$_item_max		= null;
		$_item_max_num	= null;
		$_item_min_num	= null;
		$_item_category	= null;
		$_item_addr_names		= null;
		$_item_addr_min_values	= null;
		$_item_addr_max_values	= null;
		// Parametri di default comuni
		$_item_default_name	= null;
		$_item_default_num	= null;
		// Parametri di default porte seriali
		$_item_default_timeout		= null;
		$_item_default_persistent	= null;
		$_item_default_baud			= null;
		$_item_default_echo			= null;
		$_item_default_data			= null;
		$_item_default_stop			= null;
		$_item_default_parity		= null;
		// Parametri di default porte socket e ethernet
		$_item_default_ip	= null;
		// Parametri di default porte ethernet
		$_item_default_subnet	= null;
		$_item_default_gateway	= null;
		$_item_default_dns1		= null;
		$_item_default_dns2		= null;
				
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			//<type name="Porta seriale RS232" default_num="1" default_name="Porta COM%NUM% RS232" code="STLC1000_RS232" max="2"/>
		
			// Recupero parametri comuni
			$_item_code 		= $xmlElement->getAttribute('code');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_max			= $xmlElement->getAttribute('max');
			$_item_max_num		= $xmlElement->getAttribute('max_num');
			$_item_min_num		= $xmlElement->getAttribute('min_num');
			$_item_category		= $xmlElement->getAttribute('category');
			// Recupero parametri di default comuni
			$_item_default_name	= $xmlElement->getAttribute('default_name');
			$_item_default_num	= $xmlElement->getAttribute('default_num');
			
			// Verifico se e' specificata una categoria
			if (empty($_item_category))
				$_item_category = "serial";
			
			switch ($_item_category)
			{
				case "ethernet":
					$_item_default_subnet	= $xmlElement->getAttribute('default_subnet');
					$_item_default_gateway	= $xmlElement->getAttribute('default_gateway');
					$_item_default_dns1		= $xmlElement->getAttribute('default_dns1');
					$_item_default_dns2		= $xmlElement->getAttribute('default_dns2');
				case "socket":
					$_item_default_ip	= $xmlElement->getAttribute('default_ip');
				break;
				case "serial":
				default:
					$_item_default_timeout		= $xmlElement->getAttribute('default_timeout');
					$_item_default_persistent	= $xmlElement->getAttribute('default_persistent');
					$_item_default_baud			= $xmlElement->getAttribute('default_baud');
					$_item_default_echo			= $xmlElement->getAttribute('default_echo');
					$_item_default_data			= $xmlElement->getAttribute('default_data');
					$_item_default_stop			= $xmlElement->getAttribute('default_stop');
					$_item_default_parity		= $xmlElement->getAttribute('default_parity');
				break;
			}
						
			// Verifico parametri recuperati
			if (isset($_item_code))
			{
				if (empty($_item_default_name))
					$_item_default_name = $_item_code;
				if (empty($_item_name))
				{
					$_item_name = $_item_code;
					logEvent("Impossibile recuperare il nome del tipo porta.",1);
				}
				
				logDebug("Extracted port type ".$_item_code.":".$_item_name);
				
				// Salvo parametri comuni
				$this->code		= $_item_code;
				$this->name		= $_item_name;
				$this->max		= $_item_max;
				$this->maxNum	= $_item_max_num;
				$this->minNum	= $_item_min_num;
				$this->category	= $_item_category;
				
				// Parametri di default
				$this->defaultName	= $_item_default_name;
				$this->defaultNum	= $_item_default_num; // COM e PORT
				
				// Parametri di default porte seriali
				$this->defaultTimeout		= $_item_default_timeout;
				$this->defaultPersistent	= $_item_default_persistent;
				$this->defaultBaud			= $_item_default_baud;
				$this->defaultEcho			= $_item_default_echo;
				$this->defaultData			= $_item_default_data;
				$this->defaultStop			= $_item_default_stop;
				$this->defaultParity		= $_item_default_parity;
				
				// Parametri di default porte socket e ethernet
				$this->defaultIP	= $_item_default_ip;
				
				// Parametri di default porte ethernet
				$this->defaultSubnet	= $_item_default_subnet;
				$this->defaultGateway	= $_item_default_gateway;
				$this->defaultDNS1		= $_item_default_dns1;
				$this->defaultDNS2		= $_item_default_dns2;
			}
			else
			{
				logEvent("Impossibile recuperare il codice del tipo porta.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per ottenere il display name per un port type
	*/
	function getDisplayName()
	{
		$_name = $this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere il codice di un tipo porta dal nome
*/
function getPortTypeCodeFromName($typeList,$typeName)
{
	$_code = null;
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->name == $typeName)
			{
				$_code = $_type->code;
			 	break;
			}
		}
	}
	
	return($_code);
}

/**
* Funzione per ottenere un tipo porta dal nome
*/
function getPortTypeFromName($typeList,$typeName)
{
	$_port_type = null;
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->name == $typeName)
			{
				$_port_type = $_type;
			 	break;
			}
		}
	}
	
	return($_port_type);
}

/**
* Funzione per ottenere un tipo porta dal nome
*/
function getPortTypeFromCode($typeList,$typeCode)
{
	$_port_type = null;
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->code == $typeCode)
			{
				$_port_type = $_type;
			 	break;
			}
		}
	}
	
	return($_port_type);
}

/**
* Funzione per ottenere il nome di un tipo porta dal codice
*/
function getPortTypeNameFromCode($typeList,$typeCode)
{
	$_name = "Tipo Sconosciuto";
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->code == $typeCode)
			{
				$_name = $_type->name;
			 	break;
			}
		}
	}
	
	return($_name);
}

/**
* Funzione per ottenere l'id di una porta dal nome (che devono essere tutti diversi)
*/
function getPortIdFromName($ports,$portName)
{
	$_port_id = null;
	
	foreach ($ports as $_port_index => $_port)
	{
		if (isset($_port))
		{
			if ($_port->name == $portName)
			{
				$_port_id = $_port->id;
			 	break;
			}
		}
	}
	
	return($_port_id);
}

$_template_port_STLC1000_RS232 = new port_params();

?>
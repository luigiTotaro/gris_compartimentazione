<?php
	// torna true se StlcManagerService � vivo, false altrimenti
	function isAliveStlcManagerService(){
		$res = false;
		$service_name = 'StlcManagerService';
		$cmd = 'sc query ' . $service_name;
		
		$cmd_out = shell_exec($cmd);
		if( $cmd_out !== null ){
			$res = ( preg_match('/.*STATE.*:.*4.*RUNNING/', $cmd_out) === 1 || preg_match('/.*STATO.*:.*4.*RUNNING/', $cmd_out) === 1);
		}
		
		return $res;
	}
?>

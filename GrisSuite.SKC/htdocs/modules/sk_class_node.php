<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_node.php - Classe per le stazioni (node).
*
* @author Enrico Alborali
* @version 1.0.4.1 26/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

/**
* Implementazione della classe dei parametri node
*/
class node_params{
	public $type;
	public $name;
	public $serverId;
	public $regionId;
	public $zoneId;
	public $stationId;
	public $modemSize;
	public $modemAddr;
	public $local;
	public $devIdOffset;
	
	/**
	* Costruttore classe parametri node
	*/
	function __construct($type,$name,$host){
		$this->type = $type; 
		$this->name = $name;
	}
}

/**
* Implementazione della classe node
*/
class node extends node_params{
	public $id;
	
	public $severity;
	public $status;
	
	/**
	* Costruttore classe node
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di un node
	*/
	function extractXMLConfig($xmlElement,$depth=5)
	{
		logDebug("=== node:extractXMLConfig ===");
		
		$_depth_to_match = $depth;
		$_name_to_match = "node";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		$_item_modem_size	= null;
		$_item_modem_addr	= null;
		$_item_local		= null;
		$_item_id_offset	= null; 
		
		logDebug("Verify: $xmlElement->depth = ".$xmlElement->depth.":".$_depth_to_match." $xmlElement->name = ".$xmlElement->name.":".$_name_to_match);
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			logDebug("Extracting...");
		
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('NodID');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_station 		= $xmlElement->getAttribute('station');
			$_item_modem_size	= $xmlElement->getAttribute('modem_size');
			$_item_modem_addr	= $xmlElement->getAttribute('modem_addr');
			$_item_local		= $xmlElement->getAttribute('local');
			$_item_id_offset	= $xmlElement->getAttribute('id_offset');
			
			if (!isset($_item_id))
			{
				$_item_id 			= $xmlElement->getAttribute('id');
			}
			
			
			logDebug("Extracted id ".$_item_id);
						
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "node";
				if (!isset($_item_name))
				{
					$_item_name = "Stazione sconosciuta";
					logEvent("Impossibile recuperare il nome della stazione.",1);
				}
				if (!isset($_item_modem_size))
				{
					$_item_modem_size = "16";
				}
				if (!isset($_item_local))
				{
					$_item_local = "false";
				}
				if (!isset($_item_station))
				{
					$_item_station = "0";
				}
				if (!isset($_item_id_offset) || $_item_id_offset==''){
					$_item_id_offset = 0;
				}
				
				logDebug("Extracted node ".$_item_id.":".$_item_type.":".$_item_name);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
				$this->stationId	= $_item_station;
				$this->modemSize	= $_item_modem_size;
				$this->modemAddr	= $_item_modem_addr;
				$this->local		= $_item_local;
				$this->devIdOffset	= $_item_id_offset;
			}
			else
			{
				logEvent("Impossibile recuperare l'id della stazione.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML per il nodo
	*/
	function buildXMLConfig()
	{
		logDebug("=== node:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		$_xml .= codeChr(1,5).'<node NodID="'.$this->id.'" name="'.$this->name.'" type="'.$this->type.'" modem_size="'.$this->modemSize.'" modem_addr="'.$this->modemAddr.'" local="'.$this->local.'" station="'.$this->stationId.'" id_offset="'.$this->devIdOffset.'" >';
		$_xml .= '%DEVICES%';
		$_xml .= codeChr(1,5).'</node>';
				
		return($_xml);
	}
}

/**
* Funzione per ottenere una stazione dalla lista
*/
function getNodeFromId($nodeList,$nodeId)
{
	$_node = null;
	
	$_list = $nodeList;
	$_id = $nodeId;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->id == $_id)
			{
				$_node = $_item;
			 	break;
			}
		}
	}
	
	return($_node);
}

/**
* Funzione per eliminare un nodo associato ad una stazione
*/
function deleteStationNode(&$nodeList,$nodeId,$stationId)
{
	$_deleted = false;
	
	foreach ($nodeList as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->id == $nodeId)
			{
				if ($_item->stationId == $stationId)
				{
					unset($nodeList[$_item_index]);
					$_deleted = true;
					break;
				}
			}
		}
	}
	
	return($_deleted);
}

/**
* Funzione
*/
function getNodeIdFromLongName($regionList,$longName)
{
	$_id = null;

	$_region_list = $regionList;
	$_long_name = $longName;
	
	if (isset($_region_list) && count($_region_list) > 0)
	{
		foreach ($_region_list as $_region_item)
		{
			if (isset($_region_item))
			{
				$_region	= $_region_item['data'];
				$_zone_list = $_region_item['zone_list'];
			
				if (isset($_zone_list))
				foreach ($_zone_list as $_zone_item)
				{
					if (isset($_zone_item))
					{
						$_zone		= $_zone_item['data'];
						$_node_list	= $_zone_item['node_list'];
						
						if (isset($_node_list))
						foreach ($_node_list as $_node_item)
						{
							if (isset($_node_item))
							{
								$_node	= $_node_item['data'];
								
								$_value = str_replace('"','\"',$_region->name." > ".$_zone->name." > ".$_node->name);
								
								if ($_value == $_long_name)
								{
									$_id = $_node->id;
									break;
								}
							}
						}		
					}
				}
			}
		}
	}
	
	return($_id);
}

/**
*
*/
function getNodeNameFromLongName($regionList,$longName)
{
	$_id = null;

	$_region_list = $regionList;
	$_long_name = $longName;
	
	if (isset($_region_list) && count($_region_list) > 0)
	{
		foreach ($_region_list as $_region_item)
		{
			if (isset($_region_item))
			{
				$_region	= $_region_item['data'];
				$_zone_list = $_region_item['zone_list'];
			
				if (isset($_zone_list))
				foreach ($_zone_list as $_zone_item)
				{
					if (isset($_zone_item))
					{
						$_zone		= $_zone_item['data'];
						$_node_list	= $_zone_item['node_list'];
						
						if (isset($_node_list))
						foreach ($_node_list as $_node_item)
						{
							if (isset($_node_item))
							{
								$_node	= $_node_item['data'];
								
								$_value = str_replace('"','\"',$_region->name." > ".$_zone->name." > ".$_node->name);
								
								if ($_value == $_long_name)
								{
									$_id = $_node->name;
									break;
								}
							}
						}		
					}
				}
			}
		}
	}
	
	return($_id);
}

?>
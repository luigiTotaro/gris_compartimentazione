<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_zone.php - Classe per le linee (zone).
*
* @author Enrico Alborali
* @version 0.0.1 14/07/2011
* @copyright 2011 Telefin S.p.A.
*/

/**
* Implementazione della classe dei parametri zone
*/
class zone_params{
	public $type;
	public $name;
	public $serverId;
	public $regionId;
	
	/**
	* Costruttore classe parametri zone
	*/
	function __construct($type,$name,$host){
		$this->type = $type; // stlc1000
		$this->name = $name;
	}
}

/**
* Implementazione della classe zone
*/
class zone extends zone_params{
	public $id;
	
	public $severity;
	public $status;
	
	/**
	* Costruttore classe zone
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di una zone
	*/
	function extractXMLConfig($xmlElement,$depth=4)
	{
		logDebug("=== zone:extractXMLConfig ===");
		
		$_depth_to_match = $depth;
		$_name_to_match = "zone";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('ZonID');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
						
			if (empty($_item_id))
			{
				$_item_id 			= $xmlElement->getAttribute('id');
			}
			
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "zone";
				if (!isset($_item_name))
				{
					$_item_name = "Linea sconosciuta";
					logEvent("Impossibile recuperare il nome della linea.",1);
				}
				
				logDebug("Extracted zone ".$_item_id.":".$_item_type.":".$_item_name);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
			}
			else
			{
				logEvent("Impossibile recuperare l'id della linea.",2);
			}
		}
	
		return($this);
	}

	/**
	* Metodo per generare la configurazione XML per la zona
	*/
	function buildXMLConfig()
	{
		logDebug("=== zone:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		$_xml .= codeChr(1,4).'<zone ZonID="'.$this->id.'" name="'.$this->name.'" type="'.$this->type.'">';
		$_xml .= '%NODES%';
		$_xml .= codeChr(1,4).'</zone>';
				
		return($_xml);
	}
}

/**
* Funzione per ottenere un id zone dalla lista
*/
function getZoneFromId($zoneList,$zoneId)
{
	$_zone = null;
	
	$_list = $zoneList;
	$_id = $zoneId;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->id == $_id)
			{
				$_zone = $_item;
			 	break;
			}
		}
	}
	
	return($_zone);
}

/**
* Funzione
*/
function getZoneIdFromLongName($regionList,$longName)
{
	$_id = null;

	$_region_list = $regionList;
	$_long_name = $longName;
	
	if (isset($_region_list) && count($_region_list) > 0)
	{
		foreach ($_region_list as $_region_item)
		{
			if (isset($_region_item))
			{
				$_region	= $_region_item['data'];
				$_zone_list = $_region_item['zone_list'];
			
				if (isset($_zone_list))
				foreach ($_zone_list as $_zone_item)
				{
					if (isset($_zone_item))
					{
						$_zone		= $_zone_item['data'];
						$_node_list	= $_zone_item['node_list'];
						
						if (isset($_node_list))
						foreach ($_node_list as $_node_item)
						{
							if (isset($_node_item))
							{
								$_node	= $_node_item['data'];
								
								$_value = str_replace('"','\"',$_region->name." > ".$_zone->name." > ".$_node->name);
								
								if ($_value == $_long_name)
								{
									$_id = $_zone->id;
									break;
								}
							}
						}		
					}
				}
			}
		}
	}
	
	return($_id);
}

/**
* Funzione
*/
function getZoneNameFromLongName($regionList,$longName)
{
	$_name = null;

	$_region_list = $regionList;
	$_long_name = $longName;
	
	if (isset($_region_list) && count($_region_list) > 0)
	{
		foreach ($_region_list as $_region_item)
		{
			if (isset($_region_item))
			{
				$_region	= $_region_item['data'];
				$_zone_list = $_region_item['zone_list'];
			
				if (isset($_zone_list))
				foreach ($_zone_list as $_zone_item)
				{
					if (isset($_zone_item))
					{
						$_zone		= $_zone_item['data'];
						$_node_list	= $_zone_item['node_list'];
						
						if (isset($_node_list))
						foreach ($_node_list as $_node_item)
						{
							if (isset($_node_item))
							{
								$_node	= $_node_item['data'];
								
								$_value = str_replace('"','\"',$_region->name." > ".$_zone->name." > ".$_node->name);
								
								if ($_value == $_long_name)
								{
									$_name = $_zone->name;
									break;
								}
							}
						}		
					}
				}
			}
		}
	}
	
	return$_name;
}

?>
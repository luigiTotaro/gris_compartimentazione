<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_activity.php
*
* @author Davide Ferraretto
* @version 1.0.3.1 16/02/2016
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Implementazione della classe Activity
*/
class Activity{
	public $id;
	public $name;
	public $class;
	public $from;
	public $to;
	public $dow;
	public $mode;
	public $interval;
		
	/**
	* Costruttore classe Activity
	*/
	function __construct(){
		$this->id = null;
		$this->name = null;
		$this->class = null;
		$this->from = null;
		$this->to = null;
		$this->dow = null;
		$this->mode = null;
		$this->interval = null;
	}

	/**
	* Funzione per estrarre la configurazione di un activity
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== activity:extractXMLConfig ===");
		
		$_item_id = null;
		$_item_name = null;
		$_item_class = null;
		$_item_from = null;
		$_item_to = null;
		$_item_dow = null;
		$_item_mode = null;
		$_item_interval = null;
		
		if(isset($xmlElement['id']) === true && trim($xmlElement['id'])!==''){
			$_item_id = $xmlElement['id'];
		}
			
		if(isset($xmlElement['name']) === true && trim($xmlElement['name'])!==''){
			$_item_name = $xmlElement['name'];
		}
			
		if(isset($xmlElement['class']) === true && trim($xmlElement['class'])!==''){
			$_item_class = $xmlElement['class'];
		}
			
		if(isset($xmlElement['from']) === true && trim($xmlElement['from'])!==''){
			$_item_from = $xmlElement['from'];
		}
			
		if(isset($xmlElement['to']) === true && trim($xmlElement['to'])!==''){
			$_item_to = $xmlElement['to'];
		}
			
		if(isset($xmlElement['dow']) === true && trim($xmlElement['dow'])!==''){
			$_item_dow = $xmlElement['dow'];
		}
			
		if(isset($xmlElement['mode']) === true && trim($xmlElement['mode'])!==''){
			$_item_mode = $xmlElement['mode'];
		}
			
		if(isset($xmlElement['interval']) === true && trim($xmlElement['interval'])!==''){
			$_item_interval = $xmlElement['interval'];
		}
		
		// verifica dati raccolti
		if($_item_id !== null && trim($_item_id)!=='' &&
		$_item_name !== null && trim($_item_name)!=='' &&
		$_item_class !== null && trim($_item_class)!=='' &&
		$_item_from !== null && trim($_item_from)!=='' &&
		$_item_to !== null && trim($_item_to)!=='' &&
		$_item_dow !== null && trim($_item_dow)!=='' &&
		$_item_mode !== null && trim($_item_mode)!==''){
			$this->id = $_item_id;
			$this->name = $_item_name;
			$this->class = $_item_class;
			$this->from = $_item_from;
			$this->to = $_item_to;
			$this->dow = $_item_dow;
			$this->mode = $_item_mode;
			$this->interval = $_item_interval;
		} else {
			logEvent("Errore nel parsing di un activity da xml");
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML
	*/
	function buildXMLConfig()
	{
		logDebug("=== activity:buildXMLConfig ===");
		
		$_xml = '<activity id="'.$this->id.'" name="'.$this->name.'" class="'.$this->class.'" from="'.$this->from.'" to="'.$this->to.'" dow="'.$this->dow.'" mode="'.$this->mode.'"'.( $this->interval !== null ?' interval="'.$this->interval.'"' : '').'/>';
				
		return($_xml);
	}
}
?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_device.php - Classe per le periferiche (device).
*
* @author Enrico Alborali
* @version 1.0.4.0 24/10/2013
* @copyright 2011-2013 Telefin S.p.A.
*/

/**
* Implementazione della classe dei parametri device
*/
class device_params{
	public $type;
	public $name;
	public $serverId;
	public $regionId;
	public $zoneId;
	public $nodeId;
	public $stationId;
	public $buildingId;
	public $locationId;
	public $position;
	public $modemSize;
	public $modemAddr;
	public $addr;
	public $ctsAddr;
	public $subnet;
	public $snmpCommunity;
	public $sn;
	public $monitoringDeviceId;
	public $port;
	public $supervisorId;
	public $profile;
	public $active;
	public $scheduled;
	
	/**
	* Costruttore classe parametri device
	*/
	function __construct($type,$name,$host){
		$this->type = $type; 
		$this->name = $name;
	}
}

/**
* Implementazione della classe device
*/
class device extends device_params{
	public $id;
	
	public $severity;
	public $status;
	
	/**
	* Costruttore classe device
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di un device
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== device:extractXMLConfig ===");
		
		$_depth_to_match = 6;
		$_name_to_match = "device";
	
		// Parametri comuni
		$_item_id 				= null;
		$_item_type 			= null;
		$_item_name 			= null;
		$_item_station			= null;
		$_item_building			= null;
		$_item_location			= null;
		$_item_position			= null;
		$_item_addr				= null;
		$_item_subnet			= null;
		$_item_modem_size		= null;
		$_item_modem_addr		= null;
		$_item_cts_addr			= null;
		$_item_sn				= null;
		$_item_monitoring_device_id	= null; 
		$_item_port				= null;
		$_item_snmp_community	= null;
		$_item_profile			= null;
		$_item_active			= null;
		$_item_scheduled		= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 				= $xmlElement->getAttribute('DevID');
			$_item_type 			= $xmlElement->getAttribute('type');
			$_item_name 			= $xmlElement->getAttribute('name');
			$_item_station 			= $xmlElement->getAttribute('station');
			$_item_building			= $xmlElement->getAttribute('building');
			$_item_location			= $xmlElement->getAttribute('location');
			$_item_position			= $xmlElement->getAttribute('position');
			$_item_addr				= $xmlElement->getAttribute('addr');
			$_item_subnet			= $xmlElement->getAttribute('subnet');
			$_item_modem_size		= $xmlElement->getAttribute('size');
			$_item_modem_addr		= $xmlElement->getAttribute('modem');
			$_item_cts_addr			= $xmlElement->getAttribute('cts');
			$_item_sn				= $xmlElement->getAttribute('SN');
			$_item_monitoring_device_id	= $xmlElement->getAttribute('monitoring_device_id');
			$_item_port				= $xmlElement->getAttribute('port');
			$_item_supervisor_id	= $xmlElement->getAttribute('supervisor_id');
			$_item_snmp_community	= $xmlElement->getAttribute('snmp_community');
			$_item_profile			= $xmlElement->getAttribute('profile');
			$_item_active			= $xmlElement->getAttribute('active');
			$_item_scheduled		= $xmlElement->getAttribute('scheduled');
						
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "device";
				if (!isset($_item_name))
				{
					$_item_name = "Periferica sconosciuta";
					logEvent("Impossibile recuperare il nome della periferica.",1);
				}
				
				if (empty($_item_modem_size) || $_item_modem_size == "")
					$_item_modem_size = "16";
				if (empty($_item_modem_addr) || $_item_modem_addr == "")
					$_item_modem_addr = "--";
				
				logDebug("Extracted device ".$_item_id.":".$_item_type.":".$_item_name);
				
				if (empty($_item_supervisor_id) || $_item_supervisor_id == "")
					$_item_supervisor_id = "0";
				
				// Salvo parametri comuni					
				$this->id				= $_item_id;
				$this->type				= $_item_type;
				$this->name				= $_item_name;
				$this->stationId		= $_item_station;
				$this->buildingId		= $_item_building;
				$this->locationId		= $_item_location;
				$this->position			= $_item_position;
				$this->addr				= $_item_addr;
				$this->subnet			= $_item_subnet;
				$this->modemSize		= $_item_modem_size;
				$this->modemAddr		= $_item_modem_addr;
				$this->ctsAddr			= $_item_cts_addr;
				$this->sn				= $_item_sn;
				$this->monitoringDeviceId	= $_item_monitoring_device_id;
				$this->port				= $_item_port;
				$this->supervisorId		= $_item_supervisor_id;
				$this->snmpCommunity	= $_item_snmp_community;
				$this->profile			= $_item_profile;
				$this->active			= $_item_active;
				$this->scheduled		= $_item_scheduled;
			}
			else
			{
				logEvent("Impossibile recuperare l'id della periferica.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML per la periferica
	*/
	function buildXMLConfig()
	{
		global $_configuration;
		
		logDebug("=== device:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		$_ports = $_configuration["ports"];
		
		$_port = getPortFromId($_ports,$this->port);
		
		if (isset($_port))
		{
			if ($_port->type == "TCP_Client")
			{
				//$_supervisor_id = 1;
				if (empty($this->snmpCommunity)) $this->snmpCommunity = "public";
				$_xml_snmp_commuinity = ' snmp_community="'.$this->snmpCommunity.'"';
			}
			else
			{
				//$_supervisor_id = 0;
				$_xml_snmp_commuinity = '';
			}
		}
		else
		{
			$_supervisor_id = 0;
		}
		
		$_xml .= codeChr(1,6).'<device DevID="'.$this->id.'" name="'.utf8_decode($this->name).'" type="'.$this->type.'" station="'.$this->stationId.'" building="'.$this->buildingId.'" location="'.$this->locationId.'" position="'.$this->position.'" addr="'.$this->addr.'" subnet="'.$this->subnet.'" size="'.$this->modemSize.'" modem="'.$this->modemAddr.'" cts="'.$this->ctsAddr.'" SN="'.$this->sn.'" monitoring_device_id="'.$this->monitoringDeviceId.'" port="'.$this->port.'" profile="'.$this->profile.'" active="'.$this->active.'" scheduled="'.$this->scheduled.'" supervisor_id="'.$this->supervisorId.'"'.$_xml_snmp_commuinity.' />';
				
		return($_xml);
	}
	
	/**
	* Metodo per ottenere il DevId a 64 bit in formato stringa per un oggetto device
	*/
	function getDevId()
	{
		$_dev_id = $this->id;
		$_dev_id = bcmul($_dev_id,"65536");
		
		$_dev_id = bcadd($_dev_id,$this->nodeId);
		$_dev_id = bcmul($_dev_id,"65536");
		
		$_dev_id = bcadd($_dev_id,$this->zoneId);
		$_dev_id = bcmul($_dev_id,"65536");
		
		$_dev_id = bcadd($_dev_id,$this->regionId);
		
		return($_dev_id);
	}
}

/**
* Funzione per ottenere l'indice relativo ad una periferica trovata nella configurazione
*/
function getDeviceIndex($devices,$deviceId,$regionId,$zoneId,$nodeId)
{
	$_index = null;
	
	foreach ($devices as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($_device->id == $deviceId
			 && $_device->regionId == $regionId
			 && $_device->zoneId == $zoneId
			 && $_device->nodeId == $nodeId)
			 {
			 	$_index = $_device_index;
			 	break;
			 }
		}
	}
	
	return($_index);
}

/**
* Funzione per ottenere l'indice relativo ad una periferica trovata nella configurazione
*/
function getDeviceIndexFromDeviceId($devices,$deviceId)
{
	$_index = null;
	
	foreach ($devices as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($_device->getDevId() == $deviceId)
			 {
			 	$_index = $_device_index;
			 	break;
			 }
		}
	}
	
	return($_index);
}

/**
* Funzione per ottenere l'oggetto relativo ad una periferica trovata nella configurazione
*/
function getDeviceFromDeviceId($devices,$deviceId)
{
	$_item = null;
	
	foreach ($devices as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($_device->getDevId() == $deviceId)
			 {
			 	$_item = $_device;
			 	break;
			 }
		}
	}
	
	return($_item);
}

/**
* Funzione per ottenere il primo id disponibile per una periferica
*/
function getFirstFreeDeviceId($list,$devIdOffset=0,$nodeId=null)
{
	$_list = $list;
	
	$_found = false;
	
	$_id = ($devIdOffset>0 && $devIdOffset<65534)?$devIdOffset:0;
	
	while (!$_found && $_id < 65535)
	{
		$_found = true;
	
		foreach ($_list as $_item_index => $_item)
		{
			if (isset($_item))
			{
				if ($_item->id == $_id && ($nodeId==null || $_item->nodeId==$nodeId))
				{
					$_found = false;
			 		$_id++;
			 		break;
				}
			}
		}
	}
	
	if (!$_found) $_id = null;
	
	return($_id);
}

/**
* 
*/
function deviceSimilarNameCount($list,$name)
{
	$_list = $list;
	
	$_count = 0;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if (stripos($_item->name,$name) !== false)
			{
				$_count++;
		 	}
		}
	}
	
	return($_count);
}

/**
* Funzione per aggiornare gli id della topografia delle periferiche
*/
function deviceUpdateTopographyId(&$deviceList,$oldStationId,$oldBuildingId,$oldRackId,$stationId,$buildingId,$rackId,$nodeId,$zoneId,$regionId)
{
	$_updated = false;
	
	// Cerco periferiche nell'armadio
	foreach ($deviceList as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($_device->stationId == $oldStationId 
			&& ($_device->buildingId == $oldBuildingId || $oldBuildingId == null) 
			&& ($_device->locationId == $oldRackId || $oldRackId == null))
			{
				// Aggiorno gli id della periferica
				$_device->stationId = $stationId;
				$_device->nodeId 	= $nodeId;
				$_device->zoneId 	= $zoneId;
				$_device->regionId 	= $regionId;
				if ($buildingId !== null) 
					$_device->buildingId 	= $buildingId;
				if ($rackId !== null)
					$_device->locationId 		= $rackId;
				// Salvo la periferica
				$deviceList[$_device_index] = $_device;
				$_updated = true;
			}
		}	
	}
	
	return($_updated);
}

/**
* Implementazione della classe device type
*/
class device_type{
	public $id;
	public $code;
	public $name;
	public $defaultName;
	public $portType;
	public $category;
	public $snmpCommunity;
	
	/**
	* Costruttore classe device type
	*/
	function __construct($id){
		$this->id = $id; 
	}

	/**
	* Funzione per estrarre la configurazione di un device type
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== device_type:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "type";
	
		// Parametri comuni
		$_item_code 		= null;
		$_item_name 		= null;
		$_item_default_name	= null;
		$_item_port_type	= null;
		$_item_category		= null;
		$_item_snmp_community	= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			//<type name="Telefin DT00000 - Pannello DTS Standard" default_name="Pannello DTS" code="DT00000" port_type="STLC1000_RS485"/>
		
			// Recupero parametri comuni
			$_item_code 		= $xmlElement->getAttribute('code');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_default_name	= $xmlElement->getAttribute('default_name');
			$_item_port_type	= $xmlElement->getAttribute('port_type');
			$_item_category		= $xmlElement->getAttribute('category');
			$_item_snmp_community	= $xmlElement->getAttribute('snmp_community');
						
			// Verifico parametri recuperati
			if (isset($_item_code))
			{
				if (!isset($_item_default_name))
					$_item_default_name = $_item_code;
				if (!isset($_item_name))
				{
					$_item_name = $_item_code;
					logEvent("Impossibile recuperare il nome del tipo periferica.",1);
				}
				if (!isset($_item_snmp_community) || $_item_snmp_community == '')
					$_item_snmp_community = 'public';
				
				logDebug("Extracted device type ".$_item_code.":".$_item_name);
				
				// Salvo parametri comuni					
				$this->code			= $_item_code;
				$this->name			= $_item_name;
				$this->defaultName	= $_item_default_name;
				$this->portType		= $_item_port_type;
				$this->category		= $_item_category;
				$this->snmpCommunity	= $_item_snmp_community;
			}
			else
			{
				logEvent("Impossibile recuperare il codice del tipo periferica.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per ottenere il display name per un device type
	*/
	function getDisplayName()
	{
		//$_name = $this->code." - ".$this->name;
		$_name = $this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere un oggetto tipo periferica dal nome
*/
function getDeviceTypeFromName($deviceTypeList,$deviceTypeName)
{
	$_type = null;
	
	foreach ($deviceTypeList as $_device_type_index => $_device_type)
	{
		if (isset($_device_type))
		{
			$_type_name = str_replace('+','',$_device_type->name);
			if ($_device_type->name == $deviceTypeName || $_type_name == $deviceTypeName)
			{
				$_type = $_device_type;
			 	break;
			}
		}
	}
	
	return($_type);
}

/**
* Funzione per ottenere il codice di un tipo periferica dal nome
*/
function getDeviceTypeCodeFromName($deviceTypeList,$deviceTypeName)
{
	$_code = null;
	
	foreach ($deviceTypeList as $_device_type_index => $_device_type)
	{
		if (isset($_device_type))
		{
			$_type_name = str_replace('+','',$_device_type->name);
			if ($_device_type->name == $deviceTypeName || $_type_name == $deviceTypeName)
			{
				$_code = $_device_type->code;
			 	break;
			}
		}
	}
	
	return($_code);
}

/**
* Funzione per ottenere l'oggetto di un tipo periferica dal codice
*/
function getDeviceTypeFromCode($deviceTypeList,$deviceTypeCode)
{
	$_type = null;
	
	foreach ($deviceTypeList as $_device_type_index => $_device_type)
	{
		if (isset($_device_type))
		{
			if ($_device_type->code == $deviceTypeCode)
			{
				$_type = $_device_type;
			 	break;
			}
		}
	}
	
	return($_type);
}

/**
* Funzione per ottenere il nome di un tipo periferica dal codice
*/
function getDeviceTypeNameFromCode($deviceTypeList,$deviceTypeCode)
{
	$_name = "Tipo Sconosciuto";
	
	foreach ($deviceTypeList as $_device_type_index => $_device_type)
	{
		if (isset($_device_type))
		{
			if ($_device_type->code == $deviceTypeCode)
			{
				$_name = $_device_type->name;
			 	break;
			}
		}
	}
	
	return($_name);
}

/**
* Funzione per verificare se il tipo di periferica e' STSI
*/
function deviceTypeIsSTSI($deviceType)
{
	$_is_stsi = false;
	
	if (isset($deviceType))
	{
		if ((strpos($deviceType,"HA") === 0) || (strpos($deviceType,"HD") === 0) || (strpos($deviceType,"AIMCTS") === 0))
		{
			$_is_stsi = true;
		}
	}
	
	return($_is_stsi);
}

?>
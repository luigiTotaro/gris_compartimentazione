<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_region.php - Classe per i compartimenti (region).
*
* @author Enrico Alborali
* @version 1.0.0.5 18/01/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Implementazione della classe dei parametri region
*/
class region_params{
	public $type;
	public $name;
	public $serverId;
	
	/**
	* Costruttore classe parametri region
	*/
	function __construct($type,$name,$host){
		$this->type = $type; // stlc1000
		$this->name = $name;
	}
}

/**
* Implementazione della classe region
*/
class region extends region_params{
	public $id;
	
	public $severity;
	public $status;
	
	/**
	* Costruttore classe region
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di una region
	*/
	function extractXMLConfig($xmlElement,$depth=3)
	{
		logDebug("=== region:extractXMLConfig ===");
		
		$_depth_to_match = $depth;
		$_name_to_match = "region";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('RegID');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
			
			if (empty($_item_id))
			{
				$_item_id 			= $xmlElement->getAttribute('id');
			}
			
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "region";
				if (!isset($_item_name))
				{
					$_item_name = "Compartimento sconosciuto";
					logEvent("Impossibile recuperare il nome del compartimento.",1);
				}
				
				logDebug("Extracted region ".$_item_id.":".$_item_type.":".$_item_name);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
			}
			else
			{
				logEvent("Impossibile recuperare l'id del compartimento.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML per la regione
	*/
	function buildXMLConfig()
	{
		logDebug("=== region:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		$_xml .= codeChr(1,3).'<region RegID="'.$this->id.'" name="'.$this->name.'" type="'.$this->type.'">';
		$_xml .= '%ZONES%';
		$_xml .= codeChr(1,3).'</region>';
				
		return($_xml);
	}
}

/**
* Funzione per ottenere un id region dalla lista
*/
function getRegionFromId($regionList,$regionId)
{
	$_region = null;
	
	$_list = $regionList;
	$_id = $regionId;
	
	if (isset($_list) && count($_list)>0)
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->id == $_id)
			{
				$_region = $_item;
			 	break;
			}
		}
	}
	
	return($_region);
}

/**
* Funzione
*/
function getRegionIdFromLongName($regionList,$longName)
{
	$_id = null;

	$_region_list = $regionList;
	$_long_name = $longName;
	
	if (isset($_region_list) && count($_region_list) > 0)
	{
		foreach ($_region_list as $_region_item)
		{
			if (isset($_region_item))
			{
				$_region	= $_region_item['data'];
				$_zone_list = $_region_item['zone_list'];
			
				if (isset($_zone_list))
				foreach ($_zone_list as $_zone_item)
				{
					if (isset($_zone_item))
					{
						$_zone		= $_zone_item['data'];
						$_node_list	= $_zone_item['node_list'];
						
						if (isset($_node_list))
						foreach ($_node_list as $_node_item)
						{
							if (isset($_node_item))
							{
								$_node	= $_node_item['data'];
								
								$_value = str_replace('"','\"',$_region->name." > ".$_zone->name." > ".$_node->name);
								
								if ($_value == $_long_name)
								{
									$_id = $_region->id;
									break;
								}
							}
						}		
					}
				}
			}
		}
	}
	
	return($_id);
}

/**
* Funzione
*/
function getRegionNameFromLongName($regionList,$longName)
{
	$_name = null;

	$_region_list = $regionList;
	$_long_name = $longName;
	
	if (isset($_region_list) && count($_region_list) > 0)
	{
		foreach ($_region_list as $_region_item)
		{
			if (isset($_region_item))
			{
				$_region	= $_region_item['data'];
				$_zone_list = $_region_item['zone_list'];
			
				if (isset($_zone_list))
				foreach ($_zone_list as $_zone_item)
				{
					if (isset($_zone_item))
					{
						$_zone		= $_zone_item['data'];
						$_node_list	= $_zone_item['node_list'];
						
						if (isset($_node_list))
						foreach ($_node_list as $_node_item)
						{
							if (isset($_node_item))
							{
								$_node	= $_node_item['data'];
								
								$_value = str_replace('"','\"',$_region->name." > ".$_zone->name." > ".$_node->name);
								
								if ($_value == $_long_name)
								{
									$_name = $_region->name;
									break;
								}
							}
						}		
					}
				}
			}
		}
	}
	
	return$_name;
}

/**
* Funzione per effettuare la pulizia della sezione <system>
*/
function clearSystem(&$regionList,&$zoneList,&$nodeList)
{
	$_cleared = false;
	
	// Pulisco i zone che non hanno node
	foreach ($zoneList as $_index => $_item)
	{
		if (isset($_item))
		{
			$_used = false;
			foreach ($nodeList as $_sub_index => $_sub_item)
			{
				if (isset($_sub_item))
				{
					if ($_sub_item->zoneId == $_item->id)
					{
						$_used = true;
						break;
					}
				}
			}
			if (!$_used) 
			{
				unset($zoneList[$_index]);
				$_cleared = true;
			}
		}						
	}
	
	// Pulisco i region che non hanno zone
	foreach ($regionList as $_index => $_item)
	{
		if (isset($_item))
		{
			$_used = false;
			foreach ($zoneList as $_sub_index => $_sub_item)
			{
				if (isset($_sub_item))
				{
					if ($_sub_item->regionId == $_item->id)
					{
						$_used = true;
						break;
					}
				}
			}
			if (!$_used) 
			{
				unset($regionList[$_index]);
				$_cleared = true;
			}
		}						
	}
	
	return($_cleared);
}

?>
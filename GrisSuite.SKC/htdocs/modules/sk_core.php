<?php
/**
* Telefin STLC1000 Consolle
*
* sk_core.php - Modulo core dell'applicazione web.
*
* @author Enrico Alborali
* @version 1.0.4.0 19/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

// Includo classe Port
require_once("sk_class_port.php");
// Includo classe Station
require_once("sk_class_station.php");
// Includo classe Building
require_once("sk_class_building.php");
// Includo classe Rack
require_once("sk_class_rack.php");
// Includo classe Server
require_once("sk_class_server.php");
// Includo classe Region
require_once("sk_class_region.php");
// Includo classe Zone
require_once("sk_class_zone.php");
// Includo classe Node
require_once("sk_class_node.php");
// Includo classe Device
require_once("sk_class_device.php");
// Includo modulo applicazione Supervisore Standard
require_once("sk_app_spvstandard.php");

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}
require_once($_SERVER['DOCUMENT_ROOT'].'/conf/sk_config.php');

// Inizializzo pagine
$_pages = array();

global $is_cliScript;

// Includo modulo pagina Sistema (visualizzazione e configurazione)
require_once("sk_page_system.php");
// Includo modulo pagina Periferiche (visualizzazione e configurazione)
require_once("sk_page_devices.php");

/**
* Funzione per generare il codice della consolle
*/
function coreConsoleBuild($page)
{
	$_html = codeInit();
	
	$_page_build_func = $page."PageBuildHTML";
	
	$_page_html = $_page_build_func(null);
	
	$_html .= htmlBuild($page,$_page_html);
	
	return($_html);
}

/**
* Funzione per stampare il codice della consolle
*/
function coreConsolePrint($page)
{
	$_html = coreConsoleBuild($page);
	
	print($_html);
	
	return($_html);
}

/**
* Funzione per caricare informazioni di un'applicazione da file XML
*/
function coreLoadXMLData($application,$func,$xmlPath)
{
	$_xml_path = $xmlPath;
	
	// Inizializzo il reader XML
	$_data_xml = new XMLReader();
	
	try
	{
		$_success = $_data_xml->open($_xml_path);
	}
	catch(Excepion $_e)
	{
		$_success = false;
	}
	
	if ($_success)
	{
		// Configurazione aperta
	}
	else
	{
		// TODO: Gestire il caso in cui non riesco a scaricare la configurazione
	}
	
	$_func = $application.$func;
	
	$_data = $_func($_data_xml);
	
	//logDebug("Complete configuration: ".serialize($_config));
	
	return($_data);
}

/**
 * Funzione per cablare la configurazione
 */
function coreFixConfig( &$_config ){
	if($_config === null){
		return;
	}
	
	global $_conf_xml_file;
	
	global $_conf_xml_server_name;
	global $_conf_xml_server_type;
	global $_conf_xml_server_srvid;
	global $_conf_xml_server_host;
	
	$server_name = null;
	$server_type = null;
	$server_host = null;
	$server_srvid = null;
	
	if(isset($_conf_xml_file) === true && trim($_conf_xml_file)!==''){
		// se esiste il file faccui il parsing del file di configurazione
		$xml_params = parsingSTLCManagetServiceConfing($_conf_xml_file);
	} else {
		$xml_params = null;
	}
	
	// passo i parametri per cablare i dati
	// prima verifica se i parametri sono stati specificati da file di configurazione
	if(isset($_conf_xml_server_name) && $_conf_xml_server_name !== null && $_conf_xml_server_name != ''){
		$server_name = $_conf_xml_server_name;
	} else if( $xml_params !== null && isset($xml_params['name']) === true && $xml_params['name'] != ''){
		// verifico se � definita da file di configurazione
		$server_name = $xml_params['name'];
	}
	
	if(isset($_conf_xml_server_type) && $_conf_xml_server_type !== null && $_conf_xml_server_type != ''){
		$server_type = $_conf_xml_server_type;
	} else if( $xml_params !== null && isset($xml_params['type']) === true && $xml_params['type'] != ''){
		// verifico se � definita da file di configurazione
		$server_type = $xml_params['type'];
	}
	
	if( isset($_config["edit"]) && isset($_config["edit"]["srvid"]) && $_config["edit"]["srvid"] !== NULL && $_config["edit"]["srvid"] != '' ){
		$server_srvid = $_config["edit"]["srvid"];
	} else if(isset($_conf_xml_server_srvid) && $_conf_xml_server_srvid !== null && $_conf_xml_server_srvid != ''){
		$server_srvid = $_conf_xml_server_srvid;
		$server_srvid = stringToSerialNumber($server_srvid);
	} else if( $xml_params !== null && isset($xml_params['SrvID']) === true && $xml_params['SrvID'] != ''){
		// verifico se � definita da file di configurazione
		$server_srvid = $xml_params['SrvID'];
		$server_srvid = stringToSerialNumber($server_srvid);
	} else if( isset($_config["info"]) === true ){
		// Controllo SrvID
		$_info = $_config["info"];
		$_servers = $_config["servers"];
		$_server = getServer($_servers);
		if (isset($_info["sk_id"]) && $_info["sk_id"] != "" && $_info["sk_id"] != $_server->id)
		{
			$_server->id = $_info["sk_id"];
			setServer($_servers,$_server);
		}
	}
	
	if( isset($_config["edit"]) && isset($_config["edit"]["host"]) && $_config["edit"]["host"] !== NULL && $_config["edit"]["host"] != '' ){
		$server_host = $_config["edit"]["host"];
	} else if(isset($_conf_xml_server_host) && $_conf_xml_server_host !== null && $_conf_xml_server_host != ''){
		$server_host = $_conf_xml_server_host;
	} else if( $xml_params !== null && isset($xml_params['host']) === true && $xml_params['host'] != ''){
		// verifico se � definita da file di configurazione
		$server_host = $xml_params['host'];
	} else if( isset($_config["info"]) === true ){
		// Controllo SrvID
		$_info = $_config["info"];
		$_servers = $_config["servers"];
		$_server = getServer($_servers);
		if (isset($_info["sk_hostname"]) && $_info["sk_hostname"] != "" && $_info["sk_hostname"] != $_server->host)
		{
			$_server->host = $_info["sk_hostname"];
			setServer($_servers,$_server);
		}
	}

	
	$_servers	= $_config["servers"];
	
	foreach ($_servers as $_server_index => $_server)
	{
		if($server_name !== null && empty($server_name) === false){
			$_server->name = $server_name;
		}
		
		if($server_type !== null && empty($server_type) === false){
			$_server->type = $server_type;
		}
		
		if($server_srvid !== null && empty($server_srvid) === false){
			if($server_srvid !== null){
				$_server->id = $server_srvid;
			}
		}
		
		if($server_host !== null && empty($server_host) === false){
			$_server->host = $server_host;
		}
	}
}

/**
* Funzione per caricare la configurazione di un'applicazione da file XML
*/
function coreLoadXMLConfig($application,$appXmlConfigUrl)
{
	global $is_cliScript;
	global $is_new_conf;
	$_config_url = $appXmlConfigUrl;
	
	// Inizializzo il reader XML
	$_config_xml = new XMLReader();
	
	try
	{
		$_success = $_config_xml->open($_config_url);
	}
	catch(Excepion $_e)
	{
		$_success = false;
	}
	
	if ($_success)
	{
		// Configurazione aperta
	}
	else
	{
		// TODO: Gestire il caso in cui non riesco a scaricare la configurazione
		if(isset($is_cliScript) && $is_cliScript === true && (!isset($is_new_conf) || $is_new_conf === false)){
			return false;
		}
	}
	
	$_func = $application."LoadXMLConfig";
	
	$_config = $_func($_config_xml);
	
	//logDebug("Complete configuration: ".serialize($_config));
	
	return($_config);
}

/**
* Funzione per caricare tutte le configurazioni delle applicazioni
*/
function coreLoadAllXMLConfigs()
{

}

/**
* Funzione per generare il codice per un bottone selettore per barra di navigazione
*/
function coreBuildNavbarSelectorButton($index,$name,$selected=false,$balloonText=null)
{
	$_html = codeInit();
	
	if ($selected === true) $_html_selected = 'display:inherit;';
	else $_html_selected = '';
	
	$_html_px = 10+($index*30);
	
	$_html .= codeChr(1,4).htmlImage('navbar_'.$name.'_icon','navbar_icon','sk_topbar_icon_'.$name.'.png','default','left:'.($_html_px).'px;')
			.codeChr(1,4).htmlImage('navbar_'.$name.'_over','navbar_button_over','sk_navbar_over.png','default','left:'.($_html_px).'px;')
			.codeChr(1,4).htmlImage('navbar_'.$name.'_click','navbar_button_click','sk_navbar_click.png','default','left:'.($_html_px).'px;')
			.codeChr(1,4).htmlImage('navbar_'.$name.'_selector','navbar_selector','sk_navbar_selector.png','default','left:'.($_html_px).'px;'.$_html_selected.'')
			.codeChr(1,4).htmlImage('navbar_'.$name.'','navbar_selector_button','sk_navbar_button.png','default','left:'.($_html_px).'px;')
			;
	
	if ($balloonText !== null)
	{
		$_balloon_width = 10+ceil(strlen($balloonText)*6.7);
		$_html .= htmlBalloonBuild('navbar_'.$index, null, $_html_px, 0, $_balloon_width, $balloonText, true);
	}
	else
		$_html .= htmlDivitis( null, null, "" );
			
	return($_html);
}

/**
* Funzione per generare il codice per un bottone di filtro per stato
*/
function htmlNavbarStatusFilter($level,$side,$px,$name,$color,$selected=true,$balloonText=null,$profile=null,$visible=true){
	global $_conf_console_html_color_code,$_level;
	
	$_html = codeInit();
	
	if ($_level=='device') $_display = 'inherit';
	else $_display = 'none';
	
	if ($selected === true) $_html_selected = 'display:inherit;';
	else $_html_selected = '';
	
	if ($visible != true) $_html_visible = 'display:none;';
	else $_html_visible = '';
	
	$_html .= 
		 htmlDivitis(null,'pill_color navbar_filter navbar_'.$level.'_filter','',$side.':'.$px.'px;width:30px;background-color:'.$_conf_console_html_color_code[$color].';'.$_html_visible) 
		.codeChr(1,4).htmlImage('navbar_'.$name.'_icon','navbar_icon navbar_filter navbar_'.$level.'_filter','sk_navbar_status.png','default',$side.':'.$px.'px;'.$_html_visible)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_over','navbar_button_over','sk_navbar_over.png','default',$side.':'.$px.'px;'.$_html_visible)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_click','navbar_button_click','sk_navbar_click.png','default',$side.':'.$px.'px;'.$_html_visible)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_selected','navbar_selected navbar_filter navbar_'.$level.'_filter','sk_navbar_selected.png','default',$side.':'.$px.'px;'.$_html_selected.''.$_html_visible)
		.codeChr(1,4).htmlImage($level.'_status_filter_'.$name.'','navbar_filter_button navbar_filter navbar_'.$level.'_filter','sk_navbar_button.png','default',$side.':'.$px.'px;'.$_html_visible)
		;
	
	//$_html .= htmlDivitis(null,'navbar_filter'.$_level_class,$_html_filter,$side.':'.$px.'px;display:'.$_display.';');
			
	if ($balloonText !== null){
		$_balloon_width = 10+ceil(strlen($balloonText)*6.7);
		$_html .= htmlBalloonBuild('navbar_'.$name, null, $px, 0, $_balloon_width, $balloonText, (($side=='left')?true:false));
	}else
		$_html .= htmlDivitis( null, null, '' );
	
	if (isset($level))
		$_level_class = ' navbar_'.$level.'_filter';
	else
		$_level_class = '';
	
	return($_html);
}

/**
* Funzione per generare il codice per un bottone di filtro custom
*/
function htmlNavbarCustomFilter($level,$side,$px,$name,$selected=true,$balloonText=null,$profile=null,$visible=true){
	global $_conf_console_html_color_code,$_level;
	
	$_html = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if ($_level=='device') $_display = 'inherit';
	else $_display = 'none';
	
	if ($selected === true) $_html_selected = 'display:inherit;';
	else $_html_selected = '';
	
	if ($visible != true) $_html_visible = 'display:none;';
	else $_html_visible = '';
	
	/*htmlDivitis(null,'pill_color navbar_filter navbar_'.$level.'_filter','',$side.':'.$px.'px;width:30px;background-color:'.$_conf_console_html_color_code[$color].';') */
	
	$_html .= 
		codeChr(1,4).htmlImage('navbar_'.$name.'_icon','navbar_icon navbar_filter navbar_'.$level.'_filter','sk_navbar_filter_'.$name.'.png','default',$side.':'.$px.'px;'.$_html_visible)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_over','navbar_button_over','sk_navbar_over.png','default',$side.':'.$px.'px;'.$_html_visible)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_click','navbar_button_click','sk_navbar_click.png','default',$side.':'.$px.'px;'.$_html_visible)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_selected','navbar_selected navbar_filter navbar_'.$level.'_filter','sk_navbar_filter_'.$name.'_selected.png','default',$side.':'.$px.'px; top: 0px; height: 30px; '.$_html_selected.''.$_html_visible)
		.codeChr(1,4).htmlImage($level.'_status_filter_'.$name.'','navbar_filter_button navbar_filter navbar_'.$level.'_filter','sk_navbar_button.png','default',$side.':'.$px.'px;'.$_html_visible)
		;
	
	//$_html .= htmlDivitis(null,'navbar_filter'.$_level_class,$_html_filter,$side.':'.$px.'px;display:'.$_display.';');
			
	if ($balloonText !== null){
		$_balloon_width = 10+ceil(strlen($balloonText)*6.7);
		$_html .= htmlBalloonBuild('navbar_'.$name, null, $px, 0, $_balloon_width, $balloonText, (($side=='left')?true:false));
	}else
		$_html .= htmlDivitis( null, null, '' );
	
	if (isset($level))
		$_level_class = ' navbar_'.$level.'_filter';
	else
		$_level_class = '';
	
	return($_html);
}

/**
* Funzione per generare il codice per un bottone apertura e chiusura a tendina
*/
function coreBuildNavbarArrowButton($left,$name,$active=true,$up=false,$ballonText=null,$profile=null)
{
	$_html = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html);
	
	if ($active === false)
	{
		$_html_bc = 'display:inherit;';
		$_html_up = 'display:none;';
		$_html_down = 'display:none;';
		$_html_other = 'display:none;';
	}
	else
	{
		$_html_bc = 'display:none;';
		$_html_other = 'display:inherit;';
		if ($up === true)
		{
			$_html_up = 'display:inherit;';
			$_html_down = 'display:none;';
		}
		else
		{
			$_html_up = 'display:none;';
			$_html_down = 'display:inherit;';
		}
	}
	
	$_html_px = $left;
	
	$_html .= codeChr(1,4).htmlImage('navbar_'.$name.'_icon_bc','navbar_separator','sk_navbar_bc.png','default','left:'.($_html_px).'px;'.$_html_bc)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_icon_down','navbar_icon','sk_navbar_arrow_down.png','default','left:'.($_html_px).'px;'.$_html_down)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_icon','navbar_icon','sk_navbar_arrow_up.png','default','left:'.($_html_px).'px; z-index: 1107; '.$_html_up)
		.codeChr(1,4).htmlImage('navbar_'.$name.'_over','navbar_button_over','sk_navbar_over.png','default','left:'.($_html_px).'px;')
		.codeChr(1,4).htmlImage('navbar_'.$name.'_click','navbar_button_click','sk_navbar_click.png','default','left:'.($_html_px).'px;')
		.codeChr(1,4).htmlImage('navbar_'.$name,'navbar_arrow_button','sk_navbar_button.png','default','left:'.($_html_px).'px;'.$_html_other);
		
	//*
	if ($balloonText !== null)
	{
		$_balloon_width = 10+ceil(strlen($balloonText)*6.7);
		$_html .= htmlBalloonBuild('navbar_'.$index, null, $_html_px, 0, $_balloon_width, $balloonText, true);
	}
	else
		$_html .= htmlDivitis( null, null, "" );
	//*/	
	return($_html);
}

/**
* Funzione per generare il codice per un testo da inserire sulla barra di navigazione
*/
function coreBuildNavbarText($x,$name,$text,$textWidth)
{
	$_html = codeInit();
	
	$_html .= codeChr(1,4).'<p id="navbar_'.$name.'_text" class="navbar_text" style="left: '.$x.'px; width: '.$textWidth.'px;">'.$text.'</p>';
	
	return($_html);
}

/**
* Funzione per generare il codice per un menu a tendina dato un array di elementi id,nome
*/
function coreBuildNavbarMenu($x,$width,$name,$items,$text,$textWidth=null,$buttonActive=true,$buttonUp=false,$profile=null)
{
	$_html_menu = codeInit();
	
	// Controllo del profilo
	if (!authCheckProfile($profile)) return($_html_menu);
	
	if ($textWidth === null) $_text_width = $width;
	else $_text_width = $textWidth;
	
	$_html_menu .= coreBuildNavbarText($x,$name,$text,$_text_width);
	$_html_menu .= coreBuildNavbarArrowButton($x+$_text_width,$name,$buttonActive,$buttonUp,"TEST BALLOON");
		
	$_id_counter = 0;
	// Costruisco il menu a tendina
	if (isset($items) && is_array($items) && count($items) > 0)
	{
		$_height = 30*(count($items)+1);
	
		$_html_menu .= codeChr(1,4).'<div id="navbar_'.$name.'_menu" class="navbar_menu" style="left:'.($x-($width-$_text_width)+30).'px; width: '.$width.'px; height: '.$_height.'px;" >';
		
		$_html_menu .= codeChr(1,5).htmlImage(null,'navbar_menu_top_left','sk_navbar_menu_top_left.png','default')
			.codeChr(1,5).htmlImage(null,'navbar_menu_top','sk_navbar_menu_top.png','default','width:'.($width-16).'px;')
			.codeChr(1,5).htmlImage(null,'navbar_menu_top_right','sk_navbar_menu_top_right.png','default')
			.codeChr(1,5).htmlImage(null,'navbar_menu_left','sk_navbar_menu_left.png','default','height:'.($_height-16).'px;')
			.codeChr(1,5).'<div class="navbar_menu_bg" style="width:'.($width-16).'px; height:'.($_height-16).'px;" ></div>'
			.codeChr(1,5).htmlImage(null,'navbar_menu_right','sk_navbar_menu_right.png','default','height:'.($_height-16).'px;')
			.codeChr(1,5).htmlImage(null,'navbar_menu_bottom_left','sk_navbar_menu_bottom_left.png','default')
			.codeChr(1,5).htmlImage(null,'navbar_menu_bottom','sk_navbar_menu_bottom.png','default','width:'.($width-16).'px;')
			.codeChr(1,5).htmlImage(null,'navbar_menu_bottom_right','sk_navbar_menu_bottom_right.png','default');
				
		foreach ($items as $_item)
		{
			if (isset($_item) && is_array($_item))
			{
				$_html_menu .= codeChr(1,5).'<p id="navbar_'.$name.'_menu_item_'.$_item['id'].'" class="navbar_menu_item" style="left:'.(0).'px; top: '.(0+(30*($_id_counter+1))).'px; width: '.($width-50).'px;" >'.$_item['name'].'</p>';
				$_html_menu .= codeChr(1,5).htmlImage(null,'navbar_menu_separator','sk_navbar_menu_separator.png','default','top: '.(30*($_id_counter+1)).'px;width:'.($width).'px;');
			}
			else
			{
				//
			}
    		
    		$_id_counter++;
		}
		
		$_html_menu .= codeChr(1,4).'</div>';

	}
	
	return($_html_menu);
}

/**
* Funzione per estrarre il numero di interfacce Ethernet da un srisposta SOAP
*/
function coreExtractEthernetCount($ethList){
	if (is_array($ethList)){
		$_eth_count = count($ethList);
	}else if (isset($ethList)){
		$_eth_count = 1;
	}else{
		$_eth_count = 0;
	}
	
	return $_eth_count;
}

/**
* Funzione per estrarre le informazioni di una interfaccia Ethernet da una risposta SOAP
*/
function coreExtractEthernetParams($eth){
	if (isset($eth)){
		// Auto
		$_eth_auto = ($eth->DHCPEnabled==true)?"true":"false";
		// IP
		$_eth_ip_list = $eth->IPAddresses->IPAddress;
		if (is_array($_eth_ip_list)){
			$_eth_ip = $_eth_ip_list[0];
			if (isset($_eth_ip_list[1])) $_eth_ip2 = $_eth_ip_list[1];
			else $_eth_ip2 = null;
			if (isset($_eth_ip_list[2])) $_eth_ip3 = $_eth_ip_list[2];
			else $_eth_ip3 = null;
			if (isset($_eth_ip_list[3])) $_eth_ip4 = $_eth_ip_list[3];
			else $_eth_ip4 = null;
		}else{
			$_eth_ip = $_eth_ip_list;
			$_eth_ip2 = null;
			$_eth_ip3 = null;
			$_eth_ip4 = null;
		}
		// Subnet Mask
		$_eth_mask_list = $eth->SubnetMasks->IPAddress;
		if (is_array($_eth_mask_list)){
			$_eth_mask = $_eth_mask_list[0];
			if (isset($_eth_mask_list[1])) $_eth_mask2 = $_eth_mask_list[1];
			else $_eth_mask2 = null;
			if (isset($_eth_mask_list[2])) $_eth_mask3 = $_eth_mask_list[2];
			else $_eth_mask3 = null;
			if (isset($_eth_mask_list[3])) $_eth_mask4 = $_eth_mask_list[3];
			else $_eth_mask4 = null;
		}else{
			$_eth_mask = $_eth_mask_list;
			$_eth_mask2 = null;
			$_eth_mask3 = null;
			$_eth_mask4 = null;
		}
		// Gateway
		$_eth_gw_list = $eth->IPGateways->IPAddress;
		if (is_array($_eth_gw_list)){
			$_eth_gw = $_eth_gw_list[0];
		}else{
			$_eth_gw = $_eth_gw_list;
		}
		// DNS1
		$_eth_dns_list = $eth->DNSServers->IPAddress;
		if (is_array($_eth_dns_list)){
			$_eth_dns1 = $_eth_dns_list[0];
		}else{
			$_eth_dns1 = $_eth_dns_list;
		}
		// DNS2
		if (is_array($_eth_dns_list)){
			$_eth_dns2 = $_eth_dns_list[1];
		}else{
			$_eth_dns2 = null;
		}
	}else{
		$_ath_auto	= null;
		$_eth_ip 	= null;
		$_eth_mask 	= null;
		$_eth_gw 	= null;
		$_eth_dns1 	= null;
		$_eth_dns2 	= null;
	}
	
	$_params = array();
	
	$_params['auto']	= $_eth_auto;
	$_params['ip']		= $_eth_ip;
	$_params['mask']	= $_eth_mask;
	$_params['ip2']		= $_eth_ip2;
	$_params['mask2']	= $_eth_mask2;
	$_params['ip3']		= $_eth_ip3;
	$_params['mask3']	= $_eth_mask3;
	$_params['ip4']		= $_eth_ip4;
	$_params['mask4']	= $_eth_mask4;
	$_params['gw']		= $_eth_gw;
	$_params['dns1']	= $_eth_dns1;
	$_params['dns2']	= $_eth_dns2;
	
	return $_params;
}

/**
* Funzione per memorizzare la configurazione dalla sessione
*/
function coreSetConfigurationToSession($configuration,$openSession=false)
{
	if ($openSession == true) session_start();

	if (isset($configuration))
	{
		coreFixConfig($configuration);
		$_SESSION['configuration'] = serialize($configuration);
	}
	
	session_write_close();
	
	return($configuration);
}

/**
* Funzione per ottenere la configurazione dalla sessione
*/
function coreGetConfigurationFromSession($closeSession=false)
{
	session_start();

	if (isset($_SESSION['configuration']))
	{
		$_configuration = unserialize($_SESSION['configuration']);
	}

	if ($closeSession == true) session_write_close();

	return($_configuration);
}

/**
* Funzione per ottenere il livello di edit dalla sessione
*/
function coreGetEditLevelFromSession($closeSession=false)
{
	session_start();
	global $_conf_console_app_hardware_device_code;
	
	if (isset($_SESSION['edit_level']))
	{
		$_edit_level = unserialize($_SESSION['edit_level']);
		if($_conf_console_app_hardware_device_code	== 'STLC1000' && coreIsChangedNetConfiguration() !== 0){
			$_edit_level = 2;
		}
	}

	if ($closeSession == true) session_write_close();

	return($_edit_level);
}

/**
* Funzione per memorizzare il livello di edit in sessione
*/
function coreSetEditLevelToSession($editLevel,$closeSession=false)
{
	session_start();
	
	if (isset($editLevel))
	{
		$_SESSION['edit_level'] = serialize($editLevel);
	}
	
	session_write_close();
	
	return($editLevel);
}

/**
* Funzione per memorizzare la pagina attiva in sessione
*/
function coreSetPageToSession($page)
{
	session_start();

	if (isset($page))
	{
		$_SESSION['page'] = serialize($page);
	}
	
	session_write_close();
	
	return($page);
}

/**
* Funzione per ottenere la pagina attiva dalla sessione
*/
function coreGetPageFromSession()
{
	session_start();

	if (isset($_SESSION['page']))
	{
		$_page = unserialize($_SESSION['page']);
	}

	session_write_close();

	return($_page);
}

/**
* Funzione per memorizzare il livello corrente per la pagina attiva in sessione
*/
function coreSetLevelToSession($level)
{
	// Recupero la pagina attiva
	$_page = coreGetPageFromSession();
	$_levels = null;
	
	session_start();

	if (isset($level) && isset($_page))
	{
		// Recupero la struttura corrente
		if (isset($_SESSION['levels']))
		{	
			$_levels = unserialize($_SESSION['levels']);
		}
		// ... o eventualmente la creo nuova
		else
		{
			$_levels = array();
		}
		// Setto il livello per la pagina nella struttura
		$_levels[$_page] = $level;
		// Salvo la struttra in sessione
		$_SESSION['levels'] = serialize($_levels);
	}
	
	session_write_close();
	
	return($_levels);
}

/**
* Funzione per ottenere il livello corrente per la pagina attiva dalla sessione
*/
function coreGetLevelFromSession()
{
	// Recupero la pagina attiva
	$_page = coreGetPageFromSession();
	$_level = null;
	
	session_start();

	if (isset($_SESSION['levels']) && isset($_page))
	{
		// Recupero la struttura corrente
		$_levels = unserialize($_SESSION['levels']);
		// Recupero il livello per la pagina attiva
		if (isset($_levels[$_page]))
		{
			$_level = $_levels[$_page];
		}
	}

	session_write_close();

	return($_level);
}

/**
* Funzione per memorizzare un messaggio in sessione
*/
function coreSetMessageToSession($_message_params)
{
	session_start();

	if (isset($_message_params))
	{
		$_SESSION['message'] = serialize($_message_params);
	}
	
	session_write_close();
	
	return($_message_params);
}

/**
* Funzione per ottenere la pagina attiva dalla sessione
*/
function coreGetMessageFromSession()
{
	session_start();

	if (isset($_SESSION['message']))
	{
		$_message_params = unserialize($_SESSION['message']);
	}

	session_write_close();

	return($_message_params);
}

/**
* Funzione per eliminare un messaggio in sessione
*/
function coreDeleteMessageFromSession()
{
	session_start();

	unset($_SESSION['message']);
	
	session_write_close();
	
	return 0;
}

/**
* Funzione per memorizzare il livello corrente per la pagina attiva in sessione
*/
function coreSetIeCFilterToSession($filter)
{
	session_start();

	if (isset($filter))
	{
		$_SESSION['filter_iec'] = $filter;
	}
	
	session_write_close();
	
	return($filter);
}

/**
* Funzione per ottenere il livello corrente per la pagina attiva dalla sessione
*/
function coreGetIeCFilterFromSession()
{
	$_filter = null;
	
	session_start();

	if (isset($_SESSION['filter_iec']))
	{
		$_filter = $_SESSION['filter_iec'];
	}

	session_write_close();

	return($_filter);
}

/**
* Funzione per ottenere il prefisso per worker AJAX in base alla pagina attiva
*/
function coreGetPageAjaxWorkersPrefix()
{
	$_page = coreGetPageFromSession();

	$_page_ajax_workers_prefix = ucfirst(strtolower($_page));
	
	return($_page_ajax_workers_prefix);
}

/**
* Funzione per calcolare il codice di stato associato ad uno stato
*/
function coreGetStatusCode($sevLevel,$offline=false,$active=true)
{
	if ($active == false)
	{
		$_code = -255;
	}
	else
	{
		if ($offline == true)
		{
			$_code = 254;
		}
		else
		{
			$_code = $sevLevel;
		}
	}
	
	return($_code);
}

/**
* Funzione per calcolare il colore associato ad uno stato
*/
function coreGetStatusColor($sevLevel,$offline=false,$active=true,$textColor=false)
{
	global $_conf_console_html_color_code;
	
	if ($active == false)
	{
		$_color = $_conf_console_html_color_code["white"];
	}
	else
	{
		if ($offline == true)
		{
			if ($textColor===false)
				$_color = $_conf_console_html_color_code["black"];
			else
				$_color = $_conf_console_html_color_code["red"];
		}
		else
		{
			if ($sevLevel == -255)
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
			else if ($sevLevel == 0)
			{
				$_color = $_conf_console_html_color_code["green"];
			}
			else if ($sevLevel == 1)
			{
				$_color = $_conf_console_html_color_code["yellow"];
			}
			else if ($sevLevel == 2)
			{
				$_color = $_conf_console_html_color_code["red"];
			}
			else if ($sevLevel == 9)
			{
				$_color = $_conf_console_html_color_code["white"];
			}
			else if ($sevLevel == 255)
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
			else
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
		}
	}
	
	return($_color);
}

/**
* Funzione per calcolare il colore associato ad uno stato
*/
function coreGetStatusSymbol($sevLevel,$offline=false,$active=true)
{
	global $_conf_console_html_color_code;
	
	$_symbol = null;
	
	if ($active == false)
	{
		$_symbol = null;
	}
	else
	{
		if ($offline == true)
		{
			$_symbol = null;
		}
		else
		{
			if ($sevLevel == -255)
			{
				$_symbol = "-255";
			}
			else if ($sevLevel == 0)
			{
				$_symbol = null;
			}
			else if ($sevLevel == 1)
			{
				$_symbol = null;
			}
			else if ($sevLevel == 2)
			{
				$_symbol = null;
			}
			else if ($sevLevel == 9)
			{
				$_symbol = null;
			}
			else if ($sevLevel == 255)
			{
				$_symbol = "255";
			}
			else
			{
				$_symbol = "255";
			}
		}
	}
	
	return($_symbol);
}

/**
* Funzione per ottenere il codice dei pannelli di dettaglio e edit elementi
*/
function corePageBuildDetailsPanels($page,$level)
{
	$_html = codeInit();
	
	if (isset($page) && isset($level))
	{
		switch ($page)
		{
			case "system":
				$_html .= systemPageBuildDetailsPanelHTML( array("level" => "ethernet", "active_level" => $level) );
				$_html .= systemPageBuildDetailsPanelHTML( array("level" => "serial", 	"active_level" => $level) );
			break;
			case "devices":
			default:
				$_html .= devicesPageBuildDetailsPanelHTML( array("level" => "station", "active_level" => $level) );
				$_html .= devicesPageBuildDetailsPanelHTML( array("level" => "building","active_level" => $level) );
				$_html .= devicesPageBuildDetailsPanelHTML( array("level" => "rack", 	"active_level" => $level) );
				$_html .= devicesPageBuildDetailsPanelHTML( array("level" => "device", 	"active_level" => $level) );
			break;
		}
	}
		
	return($_html);
}

/**
* Funzione per ottenere il codice dei pannelli per aggiungere elementi
*/
function corePageBuildAddPanels($page,$level)
{
	$_html = codeInit();
	
	if (isset($page) && isset($level))
	{
		switch ($page)
		{
			case "system":
				/*
				$_html .= systemPageBuildAddPanelHTML( array("level" => "ethernet", "active_level" => $level) );
				$_html .= systemPageBuildAddPanelHTML( array("level" => "serial", 	"active_level" => $level) );
				*/
			break;
			case "devices":
			default:
				$_html .= devicesPageBuildAddPanelHTML( array("level" => "station", "active_level" => $level) );
				$_html .= devicesPageBuildAddPanelHTML( array("level" => "building","active_level" => $level) );
				$_html .= devicesPageBuildAddPanelHTML( array("level" => "rack", 	"active_level" => $level) );
				$_html .= devicesPageBuildAddPanelHTML( array("level" => "device", 	"active_level" => $level) );
			break;
		}
	}
		
	return($_html);
}

// funzione che torna true se la configurazione di rete della sessione corrente � differente da quella contenuta nel system.xml, false altrimenti
// dato che l'operazione deve essere fatta in modo altamente efficiente
// torna 0 se la configurazione non � cambiata
// torna 1 se la configurazione � cambiata
// torna 2 se la configurazione � incompleta 
function coreIsChangedNetConfiguration(){
	global $_conf_apps;
	$_conf_app = $_conf_apps[0];
	
	// configurazione sessione corrente(IN FORMATO SKC OBJ)
	$_ses_configuration = coreGetConfigurationFromSession(true);

	// configurazione attualmente caricata su system.xml(IN FORMATO XML ELEMENT)
	$_current_configuration = simplexml_load_file($_conf_app['config_url']);

	// se la configurazione corrente non esiste allora non riavviare la macchina
	if($_current_configuration !== false){
		/////////////////////// CONTROLLO PARAMETRI SERVER /////////////////
		
		$current_server = null;
		$session_server = null;
		if(isset($_ses_configuration["servers"]) && isset($_ses_configuration["servers"][0])){
			$session_server = $_ses_configuration["servers"][0];
		}
		
		if(isset($_current_configuration->system) && isset($_current_configuration->system[0]->server)){
			$current_server = $_current_configuration->system[0]->server[0];
		}
			
		// se uno dei due non esiste mentra l'altro si allora riavvia
		if(($session_server === null && $current_server !== null ) || ($session_server !== null && $current_server === null )){
			return 1;
		}
			
		// se entrambe le sezioni server sono definite, controllo le differenze sui punti "caldi"
		if($current_server !== null && $session_server !== null ){
			// controllo se � cambiato l'host
			if($session_server->host != $current_server['host']){
				if($session_server->host !== null && $current_server['host'] !== null){
					if(strtolower ($session_server->host) != strtolower ($current_server['host'])){
						return 1;
					}
				} else {
					return 1;
				}
			}
		}
	}
	
	/////////////////////// CONTROLLO PARAMETRI ETHERNET /////////////////
	// raccolgo la sezione network
	$current_network = null;
	$new_network = null;

	if(isset($_ses_configuration["network_active"])){
		$current_network = $_ses_configuration["network_active"];
	}
	
	if(isset($_ses_configuration["network"])){
		$new_network = $_ses_configuration["network"];
	}
	
	// se una delle due configurazioni non esiste allora non riavviare
	if($current_network !== NULL && $new_network !== NULL){
		$new_port_num = count($new_network);
		// confronto ogni porta con l'altra associando port
		for($i = 0; $i < $new_port_num; $i++){
			$new_port = $new_network[$i];
			$id_port = $new_port->port;
			if($id_port >= 1 && $id_port <= 2){
				if( $new_port->ip != $current_network['eth'.$id_port.'_ip'] ){
					return 1;
				}
				if( $new_port->subnet != $current_network['eth'.$id_port.'_mask'] ){
					return 1;
				}
				if( $new_port->ip2 != $current_network['eth'.$id_port.'_ip2'] ){
					return 1;
				}
				if( $new_port->subnet2 != $current_network['eth'.$id_port.'_mask2'] ){
					return 1;
				}
				if( $new_port->ip3 != $current_network['eth'.$id_port.'_ip3'] ){
					return 1;
				}
				if( $new_port->subnet3 != $current_network['eth'.$id_port.'_mask3'] ){
					return 1;
				}
				if( $new_port->ip4 != $current_network['eth'.$id_port.'_ip4'] ){
					return 1;
				}
				if( $new_port->subnet4 != $current_network['eth'.$id_port.'_mask4'] ){
					return 1;
				}
				if( $new_port->gateway != $current_network['eth'.$id_port.'_gw'] ){
					return 1;
				}
				if( $new_port->dns1 != $current_network['eth'.$id_port.'_dns1'] ){
					return 1;
				}
				if( $new_port->dns2 != $current_network['eth'.$id_port.'_dns2'] ){
					return 1;
				}
			}
		}
	} else {
		// i parametri della richiesta soap non sono stati trovati
		logEvent("Parametri relativi alle porte di rete non rilevati non trovati", 1);
		return 2;
	}
	
	return 0;
}

if( !isset($is_cliScript) || $is_cliScript !== true ){
	// Carico la configurazione
	$_configuration = coreGetConfigurationFromSession(true);
	
	if (empty($_configuration) || empty($_configuration["servers"]))
	{
		$_upload = varGetRequest("upload");
		
		$_conf_app0 = $_conf_apps[0];
		
		if (isset($_upload) && $_upload != ""){
			$_configuration_filename = $_conf_console_upload_dir.$_conf_console_upload_filename.".xml";
			$_upload_request = true;
		}
		else{
			$_configuration_filename = $_conf_app0["config_url"];
			$_upload_request = false;
		}
		
		$_station_type_list 	= coreLoadXMLData($_conf_app0["name"],"LoadXMLStationTypeList",$_conf_app0["station_type_list_path"]);
		$_device_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLDeviceTypeList",$_conf_app0["device_type_list_path"]);
		$_port_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLPortTypeList",$_conf_app0["port_type_list_path"]);
		$_building_type_list 	= coreLoadXMLData($_conf_app0["name"],"LoadXMLBuildingTypeList",$_conf_app0["building_type_list_path"]);
		$_rack_type_list 		= coreLoadXMLData($_conf_app0["name"],"LoadXMLRackTypeList",$_conf_app0["rack_type_list_path"]);
		
		$_configuration = coreLoadXMLConfig($_conf_app0["name"],$_configuration_filename);
		
		$_configuration['station_type_list'] 	= $_station_type_list;
		$_configuration['device_type_list'] 	= $_device_type_list;
		$_configuration['port_type_list'] 		= $_port_type_list;
		$_configuration['building_type_list'] 	= $_building_type_list;
		$_configuration['rack_type_list'] 		= $_rack_type_list;
		
		coreSetConfigurationToSession($_configuration,true);
		
		if (isset($_upload) && $_upload != "")
			$_edit_level = 2;
		else
			$_edit_level = $_configuration["edit_level"];
		
		coreSetEditLevelToSession($_edit_level,true);
	}
	else
	{
		$_edit_level = coreGetEditLevelFromSession(true);
	}
}
?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_server.php - Classe per il server STLC1000.
*
* @author Enrico Alborali
* @version 1.0.3.1 06/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}

require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_app_stlcManagerService.php');
/**
* Implementazione della classe dei parametri Server
*/
class server_params{
	public $type;
	public $name;
	public $host;
	
	/**
	* Costruttore classe parametri Server
	*/
	function __construct($type,$name,$host){
		$this->type = $type; // stlc1000
		$this->name = $name;
		$this->note = $host;
	}
}

/**
* Implementazione della classe Server
*/
class server extends server_params{
	public $id;
	
	public $severity;
	public $status;
	
		
	/**
	* Costruttore classe Server
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di un server
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== server:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "server";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		$_item_host			= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('SrvID');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_host 		= $xmlElement->getAttribute('host');
						
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "STLC1000";
				if (!isset($_item_name))
					$_item_name = "Server STLC1000";
				
				logDebug("Extracted server ".$_item_id.":".$_item_type.":".$_item_host);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
				$this->host			= $_item_host;
			}
			else
			{
				logEvent("Impossibile recuperare l'id del server.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Funzione per generare la configurazione di default un server
	*/
	function defaultXMLConfig()
	{
		logDebug("=== server:defaultXMLConfig ===");
		
		$_server_id = "0";
		$_item_host	= "STLC1000";
		
		// Includo la libreria SOAP
		require_once("lib/lib_soap.php");
		
		if(isAliveStlcManagerService() === true){
			$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
		}
		
		if (isset($_client))
		{
			// Preparo i parametri per la richiesta SOAP
			$_request->clientType = 'SKC';
			$_params->request = $_request;
			
			// Chiamo il metodo SOAP con i parametri che ho generato
			$_result = $_client->getSTLCInfo($_params);
			
			if (isset($_result))
			{
				// Recupero i dati del risultato
				$_result_data = $_result->getSTLCInfoResult;
				
				if (isset($_result_data))
				{
					if ($_result_data->status->level == "SUCCESS")
					{
						$_info_data = $_result_data->data;
						
						// Converto SerialNumber STLC1000 e salvo come SrvID
						$_server_id = serialNumberToServerId($_info_data->serialNumber);
						
						$_server_host = $_info_data->hostName;
					}
				}
			}
		}
		
		// Parametri rilevati
		$_item_id 			= $_server_id;
		$_item_host			= $_server_host;
		// Parametri comuni
		$_item_type 		= "STLC1000";
		$_item_name 		= "Server STLC1000";
		
		$this->id			= $_item_id;
		$this->type			= $_item_type;
		$this->name			= $_item_name;
		$this->host			= $_item_host;
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML per il server
	*/
	function buildXMLConfig()
	{
		logDebug("=== server:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		$_xml .= codeChr(1,2).'<server SrvID="'.$this->id.'" host="'.$this->host.'" name="'.$this->name.'" type="'.$this->type.'">';
		$_xml .= '%REGIONS%';
		$_xml .= codeChr(1,2).'</server>';
				
		return($_xml);
	}
}

/**
* Funzione per ottenere il server dalla lista
*/
function getServer($serverList,$serverIndex=0)
{
	$_server = null;
	
	$_list = $serverList;
	$_index = $serverIndex;
	
	if(isset($_list))
	{
		$_item = $_list[$_index];
	
		if (isset($_item))
		{
			$_server = $_item;
		}
	}
	
	return($_server);
}

/**
* Funzione per salvare un server nella lista
*/
function setServer(&$serverList,$server,$serverIndex=0)
{
	$_list = $serverList;
	$_index = $serverIndex;
	$_item = $server;
	
	if(isset($_list))
	{
		$_list[$_index] = $_item;
	}
	
	return($_list);
}

/**
* Funzione per ottenere un id server dalla lista
*/
function getServerId($serverList,$serverIndex=0)
{
	$_id = null;
	
	$_list = $serverList;
	$_index = $serverIndex;
	
	if(isset($_list))
	{
		$_item = $_list[$_index];
	
		if (isset($_item))
		{
			$_id = $_item->id;
		}
	}
	
	return($_id);
}

/**
* Funzione per convertire un serial number STLC1000 (AA.MM.LLL) in SrvID
*/
function serialNumberToServerId($serialNumber)
{
	$_server_id = null;
	
	if (isset($serialNumber))
	{
		$_sn_token = explode(".", $serialNumber);
		
		$_id = $_sn_token[0];
		$_id = bcmul($_id,"256");
		
		$_id = bcadd($_id,$_sn_token[1]);
		$_id = bcmul($_id,"65536");
				
		$_id = bcadd($_id,$_sn_token[2]);
		
		$_server_id = $_id;
	}
	
	return($_server_id);
}

?>
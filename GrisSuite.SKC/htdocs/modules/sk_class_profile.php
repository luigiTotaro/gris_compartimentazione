<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_class_activity.php');

/**
* Telefin STLC1000 Consolle
*
* sk_class_profile.php
*
* @author Davide Ferraretto
* @version 1.0.3.1 16/02/2016
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Implementazione della classe Profile
*/
class Profile{
	public $id;
	public $name;
	
	public $activityList;
		
	/**
	* Costruttore classe Profile
	*/
	function __construct(){
		$this->id = null;
		$this->name = null;
		$this->activityList = null;
	}

	/**
	* Funzione per estrarre la configurazione di un profilo
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== profile:extractXMLConfig ===");
		$_item_id = null;
		$_item_name = null;
		$_item_activities = nulL;
		
		if(isset($xmlElement['id']) === true && trim($xmlElement['id'])!==''){
			$_item_id = $xmlElement['id'];
		}
			
		if(isset($xmlElement['name']) === true && trim($xmlElement['name'])!==''){
			$_item_name = $xmlElement['name'];
		}
		
		$_item_activities = array();
		
		if( isset($xmlElement->activity) === true){
			foreach($xmlElement->activity as $act){
				$act_obj = new Activity();
				$act_obj->extractXMLConfig($act);
				
				if( $act_obj->id !== null ){
					$_item_activities[] = $act_obj;
				} else {
					$_item_activities = null;
					break;
				}
			}
		}
			
		// verifica dati raccolti
		if($_item_id !== null && trim($_item_id)!=='' && $_item_name !== null && trim($_item_name)!=='' && $_item_activities !== null ){
			$this->id = $_item_id;
			$this->name = $_item_name;
			$this->activityList = $_item_activities;
		} else {
			logEvent("Errore nel parsing di un profile da xml");
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML
	*/
	function buildXMLConfig()
	{
		logDebug("=== profile:buildXMLConfig ===");
		
		$_xml = codeChr(1,2).'<profile id="'.$this->id.'" name="'.$this->name.'">';
		if($this->activityList !== null){
			foreach ( $this->activityList as $activity ){
				$_xml .= codeChr(1,3).$activity->buildXMLConfig();
			}
		}
		$_xml .= codeChr(1,2).'</profile>';
				
		return($_xml);
	}
}
?>
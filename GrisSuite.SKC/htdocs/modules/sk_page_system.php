<?php
/**
* Telefin STLC1000 Consolle
*
* sk_page_system.php - Modulo per la gestione pagina Sistema.
*
* @author Enrico Alborali
* @version 1.0.4.2 08/09/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

$_page_system = array();
$_page_system['title'] = 'system';
$_page_system['profile'] = null;

$_page_system_levels = array();

// Livello info generali
$_page_system_level = array();
$_page_system_level['name'] = 'info';
$_page_system_level['profile'] = null;
$_page_system_level['default_mode'] = 'details';
$_page_system_level['info_mode'] = false;
$_page_system_level['details_mode'] = true;
$_page_system_level['edit_mode'] = true;
$_page_system_level['add_mode'] = false;
$_page_system_levels[] = $_page_system_level;

// Livello porte ethernet
$_page_system_level = array();
$_page_system_level['name'] = 'ethernet';
$_page_system_level['profile'] = null;
$_page_system_level['default_mode'] = 'info';
$_page_system_level['info_mode'] = true;
$_page_system_level['details_mode'] = true;
$_page_system_level['edit_mode'] = true;
$_page_system_level['add_mode'] = false;
$_page_system_levels[] = $_page_system_level;

// Livello porte seriali
$_page_system_level = array();
$_page_system_level['name'] = 'serial';
$_page_system_level['profile'] = null;
$_page_system_level['default_mode'] = 'info';
$_page_system_level['info_mode'] = true;
$_page_system_level['details_mode'] = true;
$_page_system_level['edit_mode'] = true;
$_page_system_level['add_mode'] = false;
$_page_system_levels[] = $_page_system_level;

$_page_system['levels'] = $_page_system_levels;
$_page_system['default_level'] = 'info';

$_pages[] = $_page_system;

/**
*
*/
function systemGetPageLevel()
{
	global $_conf_console_app_navbar_ethernet_selector_visible;
	
	$_level = coreGetLevelFromSession();
	
	if ($_level === null)
	{
		if ($_conf_console_app_navbar_ethernet_selector_visible == false)
			$_level = 'info';
		else
			$_level = 'ethernet';
		coreSetLevelToSession($_level);
	}
	return($_level);
}

/**
* Funzione per costruire il menu
*/
function systemPageBuildMenuHTML()
{
	global $_level;
	global $_page_system_levels;
	
	$_html = codeInit();
	
	$_html = htmlNavbarMenu($_page_system_levels);
	
	/*
	$_html .= coreBuildNavbarSelectorButton(0,"info",($_level == "info")?true:false)
		.coreBuildNavbarSelectorButton(1,"ethernet",($_level == "ethernet")?true:false)
		.coreBuildNavbarSelectorButton(2,"serial",($_level == "serial")?true:false)
		
		
	;
	*/
	return($_html);
}

/**
*
*/
function systemPageGetStatusCode($sevLevel,$offline,$active=1)
{
	if ($active != 1)
	{
		$_code = 253;
	}
	else
	{
		if ($offline == true)
		{
			$_code = 255;
		}
		else
		{
			$_code = $sevLevel;
		}
	}
	
	return($_code);
}

/**
*
*/
function systemPageGetStatusColor($sevLevel,$offline,$active=1)
{
	global $_conf_console_html_color_code;
	
	if ($active != 1)
	{
		$_color = $_conf_console_html_color_code["white"];
	}
	else
	{
		if ($offline == true)
		{
			$_color = $_conf_console_html_color_code["red"];
		}
		else
		{
			if ($sevLevel == -255)
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
			else if ($sevLevel == 0)
			{
				$_color = $_conf_console_html_color_code["green"];
			}
			else if ($sevLevel == 1)
			{
				$_color = $_conf_console_html_color_code["yellow"];
			}
			else if ($sevLevel == 2)
			{
				$_color = $_conf_console_html_color_code["red"];
			}
			else if ($sevLevel == 9)
			{
				$_color = $_conf_console_html_color_code["white"];
			}
			/*
			else if ($sevLevel == 253)
			{
				$_color = $_conf_console_html_color_code["white"];
			}
			else if ($sevLevel == 254)
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
			*/
			else if ($sevLevel == 255)
			{
				$_color = $_conf_console_html_color_code["red"];
			}
			else
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
		}
	}
	
	return($_color);
}

/**
*
*/
function systemPageGetStatusText($sevLevel,$offline,$active=1)
{
	if ($active != 1)
	{
		$_text = "Non Attiva";
	}
	else
	{
		if ($offline == true)
		{
			$_text = "Non Raggiungibile";
		}
		else
		{
			if ($sevLevel == 0)
			{
				$_text = "In Servizio";
			}
			else if ($sevLevel == 1)
			{
				$_text = "Anomalia Lieve";
			}
			else if ($sevLevel == 2)
			{
				$_text = "Anomalia Grave";
			}
			else if ($sevLevel == 253)
			{
				$_text = "Non Attiva";
			}
			else if ($sevLevel == 254)
			{
				$_text = "Sconosciuto";
			}
			else if ($sevLevel == 255)
			{
				$_text = "Non Raggiungibile";
			}
			else
			{
				$_text = "Sconosciuto";
			}
		}
	}
	
	return($_text);
}

/**
* Funzione per costruire il codice JavaScript per la creazione della lista dei tipi di porta
*/
function systemPageBuildPortTypeListJavaScript($category="serial")
{
	$_js = codeInit();
	
	global $_configuration;
	
	$_port_type_list = $_configuration['port_type_list'];
	
	$_js .= codeChr(1,4).'var port_type_list = [';
	
	$_first = true;
	foreach ( $_port_type_list as $_port_type )
	{
		if ($_first === true){
			$_js .= codeChr(1,5).'"'.$_port_type->getDisplayName().'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,5).'"'.$_port_type->getDisplayName().'"';
	}
			
	$_js .= codeChr(1,4).'];';
	
	return($_js);
}


/**
* Funzione per costruire il codice JavaScript per il contenuto
*/
function systemPageBuildMenuJavaScript()
{
	$_js = codeInit();

	$_js .= jsBuildScriptOpen()
	
		.codeChr(1,3).'var selected_item = null;'
		.codeChr(1,3).'var details_view_mode = false;'
		.codeChr(1,3).'var details_edit_mode = false;'
		.codeChr(1,3).'var fake_search = false;'
		.codeChr(1,3).'var details_ack_mode = false;'
		
		.codeChr(1,3).'var selected_stream = null;'
		.codeChr(1,3).'var selected_field = null;'
		.codeChr(1,3).'var selected_value = null;'
				
		// FUNZIONI GENERICHE
		
		// Attivazione menu a tendina
		.codeChr(1,3).'function navbar_filter_menuActivate(level){'
			// Nascondo le icone freccina e relativi bottoni di tutti i livelli
			.codeChr(1,4).'$(".navbar_arrow_button").hide();'
			.codeChr(1,4).'$(".navbar_arrow_button").prev().prev().prev().hide();'
			.codeChr(1,4).'$(".navbar_arrow_button").prev().prev().prev().prev().hide();'
			// Mostro il breadcrump di tutti i livelli
			.codeChr(1,4).'$(".navbar_arrow_button").prev().prev().prev().prev().prev().show();'
			// Nascondo il breadcrump del menu interessato
			.codeChr(1,4).'var i = "#navbar_filter_"+level+"_icon_bc";'
			.codeChr(1,4).'$(i).hide();'
			// Attivo le icone freccina e relativo bottone del livello interessato
			.codeChr(1,4).'var i = "#navbar_filter_"+level;'
			.codeChr(1,4).'$(i).show();'
			.codeChr(1,4).'var i = "#navbar_filter_"+level+"_icon_down";'
			.codeChr(1,4).'$(i).show();'
		.codeChr(1,3).'}'
		
		// Gestione Click Testo Menu Filtro Generica
		.codeChr(1,3).'function navbar_filter_textClick(level){'
			.codeChr(1,4).'navbar_active_level = level;'
			// Imposto anche il livello di zoom
			.codeChr(1,4).'navbar_selector_buttonClick(level);'
		.codeChr(1,3).'}'
		
		// FUNZIONI EVENTI SPECIFICI
		
		// Gestione Click Livello Zoom Info
		.codeChr(1,3).'function navbar_infoClick(){'
			.codeChr(1,4).'navbar_selector_buttonClick("info");'
		.codeChr(1,3).'}'
		// Gestione Click Livello Zoom Ethernet
		.codeChr(1,3).'function navbar_ethernetClick(){'
			.codeChr(1,4).'navbar_selector_buttonClick("ethernet");'
		.codeChr(1,3).'}'
		// Gestione Click Livello Zoom Serial
		.codeChr(1,3).'function navbar_serialClick(){'
			.codeChr(1,4).'navbar_selector_buttonClick("serial");'
		.codeChr(1,3).'}'
		
		// Gestione Click Testo Menu Filtro Stazioni
		.codeChr(1,3).'function navbar_filter_station_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("station");'
		.codeChr(1,3).'}'
		// Gestione Click Testo Menu Filtro Edifici
		.codeChr(1,3).'function navbar_filter_building_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("building");'
		.codeChr(1,3).'}'
		// Gestione Click Testo Menu Filtro Armadi
		.codeChr(1,3).'function navbar_filter_rack_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("rack");'
		.codeChr(1,3).'}'
		// Gestione Click Testo Menu Filtro Periferiche
		.codeChr(1,3).'function navbar_filter_device_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("device");'
		.codeChr(1,3).'}'
		
		// Gestione Click Testo Icona Filtro Stazioni
		.codeChr(1,3).'function navbar_filter_stationClick(){'
			//
		.codeChr(1,3).'}'
		// Gestione Click Testo Icona Filtro Edifici
		.codeChr(1,3).'function navbar_filter_buildingClick(){'
			//
		.codeChr(1,3).'}'
		// Gestione Click Testo Icona Filtro Armadi
		.codeChr(1,3).'function navbar_filter_rackClick(){'
			//
		.codeChr(1,3).'}'
		// Gestione Click Testo Icona Filtro Periferiche
		.codeChr(1,3).'function navbar_filter_deviceClick(){'
			//
		.codeChr(1,3).'}'
		
		// Gestione click bottone dettagli Chiudi
		.codeChr(1,3).'function details_button_closeClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
			.codeChr(1,4).'$(".details_panel_icon").hide("");'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info").html("");'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info_btns").html("");'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form").html("");'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form_btns").html("");'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel").hide();'
			.codeChr(1,4).'selected_item = null;'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione click bottone dettagli Chiudi
		.codeChr(1,3).'function add_button_closeClick(){'
			//.codeChr(1,4).'detailsDisableAddMode();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_add_panel").hide();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Funzione per entrare nella modalità di editing
		.codeChr(1,3).'function detailsEnableViewMode(){'
			.codeChr(1,4).'details_view_mode = true;'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel").show();'
		.codeChr(1,3).'}'
		
		// Funzione per uscire dalla modalità di editing
		.codeChr(1,3).'function detailsDisableViewMode(){'
			.codeChr(1,4).'details_view_mode = false;'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel").hide();'
		.codeChr(1,3).'}'
		
		// Funzione per entrare nella modalità di editing
		.codeChr(1,3).'function detailsEnableEditMode(){'
			.codeChr(1,4).'details_edit_mode = true;'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info_btns").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_streams").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_fields").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_values").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_descriptions").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form_btns").fadeTo(\'fast\',1.0);'
		.codeChr(1,3).'}'
		
		// Funzione per uscire dalla modalità di editing
		.codeChr(1,3).'function detailsDisableEditMode(){'
			.codeChr(1,4).'details_edit_mode = false;'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form_btns").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info_btns").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_streams").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_fields").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_values").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_descriptions").fadeTo(\'fast\',1.0);'
		.codeChr(1,3).'}'
		
		
		// FUNZIONI PER INVIO DATI AJAX
		
		// Funzione per inviare i dati per modificare le informazioni stlc1000
		.codeChr(1,3).'function infoSendEditData(){'
			.codeChr(1,4)."info_config_data = 'action=edit'"
				."+'&hostname='+$('#info_details_panel_form_host_input').attr('value')"
				."+'&id='+$('#info_details_panel_form_id_input').attr('value')"
			.codeChr(1,4).'ajaxInfoConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per modificare una porta ethernet
		.codeChr(1,3).'function ethernetSendEditData(){'
			.codeChr(1,4)."ethernet_config_data = 'action=edit"
				."&name='+$('#ethernet_details_panel_form_name_input').attr('value')+'"
				."&port='+$('#ethernet_details_panel_form_port_input').attr('value')+'"
				."&ip='+$('#ethernet_details_panel_form_ip_input').attr('value')+'"
				."&subnet='+$('#ethernet_details_panel_form_subnet_input').attr('value')+'"
				."&ip2='+$('#ethernet_details_panel_form_ip2_input').attr('value')+'"
				."&subnet2='+$('#ethernet_details_panel_form_subnet2_input').attr('value')+'"
				."&ip3='+$('#ethernet_details_panel_form_ip3_input').attr('value')+'"
				."&subnet3='+$('#ethernet_details_panel_form_subnet3_input').attr('value')+'"
				."&ip4='+$('#ethernet_details_panel_form_ip4_input').attr('value')+'"
				."&subnet4='+$('#ethernet_details_panel_form_subnet4_input').attr('value')+'"
				."&gateway='+$('#ethernet_details_panel_form_gateway_input').attr('value')+'"
				."&dns1='+$('#ethernet_details_panel_form_dns1_input').attr('value')+'"
				."&dns2='+$('#ethernet_details_panel_form_dns2_input').attr('value')+'"
				."&id='+selected_item"
			.codeChr(1,4).'ajaxEthernetConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per popolare il form per editare una porta seriale
		.codeChr(1,3).'function serialSendEditFormData(){'
			.codeChr(1,4)."serial_config_data = 'action=edit_form"
				."&type='+$('#serial_details_panel_form_type_select').attr('value')+'"
				."&id='+selected_item"
			.codeChr(1,4).'ajaxSerialConfigSender(true,false,true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per modificare una porta seriale
		.codeChr(1,3).'function serialSendEditData(){'
			.codeChr(1,4)."serial_config_data = 'action=edit"
				."&name='+$('#serial_details_panel_form_name_input').attr('value')+'"
				."&type='+$('#serial_details_panel_form_type_select').attr('value')+'"
				."&com='+$('#serial_details_panel_form_com_input').attr('value')+'"
				."&baud='+$('#serial_details_panel_form_baud_input').attr('value')+'"
				."&echo='+$('#serial_details_panel_form_echo_input').attr('value')+'"
				."&data='+$('#serial_details_panel_form_data_input').attr('value')+'"
				."&stop='+$('#serial_details_panel_form_stop_input').attr('value')+'"
				."&parity='+$('#serial_details_panel_form_parity_input').attr('value')+'"
				."&id='+selected_item"
			.codeChr(1,4).'ajaxSerialConfigSender(true);'
		.codeChr(1,3).'}'
		
		
		// GESTIONE CLICK BOTTONI PANNELLI ADD, DETAILS VIEW E EDIT
		
		// Gestione Click bottone Ethernet Details Edit
		.codeChr(1,3).'function ethernet_details_panel_info_button_editClick(){'
			.codeChr(1,4).'detailsEnableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
			
		// Gestione Click bottone Ethernet Details Cancel
		.codeChr(1,3).'function ethernet_details_panel_form_button_cancelClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Ethernet Details Save
		.codeChr(1,3).'function ethernet_details_panel_form_button_saveClick(){'
			.codeChr(1,4).'ethernetSendEditData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		
		
		// Gestione Click bottone Serial Details Edit
		.codeChr(1,3).'function serial_details_panel_info_button_editClick(){'
			.codeChr(1,4).'detailsEnableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
			
		// Gestione Click bottone Serial Details Cancel
		.codeChr(1,3).'function serial_details_panel_form_button_cancelClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Serial Details Save
		.codeChr(1,3).'function serial_details_panel_form_button_saveClick(){'
			.codeChr(1,4).'serialSendEditData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		
		// Gestione Click bottone STLC1000 Info Details Edit
		.codeChr(1,3).'function info_details_panel_info_button_editClick(){'
			.codeChr(1,4).'detailsEnableEditMode();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone STLC1000 Info Details Save
		.codeChr(1,3).'function info_details_panel_form_button_saveClick(){'
			.codeChr(1,4).'infoSendEditData();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone STLC1000 Info Details Cancel
		.codeChr(1,3).'function info_details_panel_form_button_cancelClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
		.codeChr(1,3).'}'
		
		.jsBuildScriptClose();
		
	return($_js);
}

/**
* Funzione per costruire il codice JavaScript per il menu
*/
function systemPageBuildContentJavaScript()
{
	global $_conf_console_html_color_code;

	$_js = codeInit();
	
	// Codice AJAX Info Config
	$_js_info_send = "info_config_data";
	$_js_info_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
			.codeChr(1,2).'edit_level = $(xml).find("edit_level").attr("value");'
			.codeChr(1,2).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
			.codeChr(1,2).'else $("#bottombar_button_apply_image_light").hide();'
			.codeChr(1,2).'detailsDisableEditMode();'
			.codeChr(1,2).'detailsDisableViewMode();'
			.codeChr(1,2).'ajaxSystemInfoSender(true);'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_info_worker = new ajaxWorker( "InfoConfig", false, 0, $_js_info_receive );
	
	// Codice AJAX Ethernet Config
	$_js_ethernet_send = "ethernet_config_data";
	$_js_ethernet_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
			.codeChr(1,2).'edit_level = $(xml).find("edit_level").attr("value");'
			.codeChr(1,2).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
			.codeChr(1,2).'else $("#bottombar_button_apply_image_light").hide();'
			.codeChr(1,2).'detailsDisableEditMode();'
			.codeChr(1,2).'detailsDisableViewMode();'
			.codeChr(1,2).'ajaxSystemInfoSender(true);'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_ethernet_worker = new ajaxWorker( "EthernetConfig", false, 0, $_js_ethernet_receive );
	
	// Codice AJAX Serial Config
	$_js_serial_send = "serial_config_data";
	$_js_serial_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'var silent = $(xml).find("silent").attr("value");'
		.codeChr(1,1).'var action = $(xml).find("action").attr("value");'
		
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'if (silent == "true"){'
				.codeChr(1,3).'if (action == "edit_form"){'
					// Baud
					.codeChr(1,4).'var baud = $(xml).find("baud").attr("value");'
					.codeChr(1,4).'if (baud != null) $("#serial_details_panel_form_baud_input").val(baud);'
					// Echo
					.codeChr(1,4).'var echo = $(xml).find("echo").attr("value");'
					.codeChr(1,4).'if (echo != null) $("#serial_details_panel_form_echo_input").val(echo);'
					// Data
					.codeChr(1,4).'var data_bit = $(xml).find("data").attr("value");'
					.codeChr(1,4).'if (data_bit != null) $("#serial_details_panel_form_data_input").val(data_bit);'
					// Stop
					.codeChr(1,4).'var stop_bit = $(xml).find("stop").attr("value");'
					.codeChr(1,4).'if (stop_bit != null) $("#serial_details_panel_form_stop_input").val(stop_bit);'
					// Parity
					.codeChr(1,4).'var parity_bit = $(xml).find("parity").attr("value");'
					.codeChr(1,4).'if (parity_bit != null) $("#serial_details_panel_form_parity_input").val(parity_bit);'
				.codeChr(1,3).'}'
			.codeChr(1,2).'}else{'
				.codeChr(1,3).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
				.codeChr(1,3).'edit_level = $(xml).find("edit_level").attr("value");'
				.codeChr(1,3).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
				.codeChr(1,3).'else $("#bottombar_button_apply_image_light").hide();'
				.codeChr(1,3).'detailsDisableEditMode();'
				.codeChr(1,3).'detailsDisableViewMode();'
				.codeChr(1,3).'ajaxSystemInfoSender(true);'
			.codeChr(1,2).'}'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_serial_worker = new ajaxWorker( "SerialConfig", false, 0, $_js_serial_receive );
	
	$_js .= jsBuildScriptOpen()
	
		.$_info_worker->BuildJavaScript("xml",$_js_info_send,true)
		.$_ethernet_worker->BuildJavaScript("xml",$_js_ethernet_send,true)
		.$_serial_worker->BuildJavaScript("xml",$_js_serial_send,true)
		
		.codeChr(1,3).'function pageLinkMouseWheelEvents() {'
				
		
		.codeChr(1,3).'}'
		;
		
	// Link Eventi Pannello Dettagli
	$_js .= 
		 codeFunctionComment("Funzione per impostare gli ascoltatori eventi per il contenuto pagina", 0)
		.codeChr(1,0).'function pageLinkEvents() {'
			
			.codeChr(1,4).'$(".item_icon").mouseenter(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseover(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseleave(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			.codeChr(1,4).'}).mouseout(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			.codeChr(1,4).'});'
			
			.codeChr(1,4).'$(".item_icon_status_up").mouseenter(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseover(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseleave(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			.codeChr(1,4).'}).mouseout(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			// Gestione Click Icona 50x50 Item
			.codeChr(1,4).'}).click(function(event) {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
				//.codeChr(1,5).'$("#details_panel").show();'
				.codeChr(1,5).'if (navbar_active_level == "ethernet"){'
					.codeChr(1,6).'var selected_item_str = $(this).attr("id");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("item_panel_icon_", "");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("_status_up", "");'
					.codeChr(1,6).'selected_item = selected_item_str;'
					.codeChr(1,6).'detailsEnableViewMode();'
					.codeChr(1,6).'ajaxSystemDetailsSender(true,true);'
				.codeChr(1,5).'} else'
				.codeChr(1,5).'if (navbar_active_level == "serial"){'
					.codeChr(1,6).'var selected_item_str = $(this).attr("id");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("item_panel_icon_", "");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("_status_up", "");'
					.codeChr(1,6).'selected_item = selected_item_str;'
					.codeChr(1,6).'detailsEnableViewMode();'
					.codeChr(1,6).'ajaxSystemDetailsSender(true,true);'
				.codeChr(1,5).'}'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
		.codeChr(1,0).'}'
		;
		
	// Link Eventi Pannello Dettagli
	$_js .=
		 codeFunctionComment("Funzione per impostare gli ascoltatori eventi per il pannello dettagli", 0)
		.codeChr(1,0).'function detailsPanelLinkEvents() {'
			// Script jQuery per la gestione degli eventi dei bottoni del pannello dettagli
			.jsPanelButtonEventManager("details_panel")
			// Script jQuery per la gestione degli eventi form edit porta seriale select tipo
			.jsPanelFormSelect("serial_details_panel","type","'json/sk_getporttypes.json.php'",true,"serialSendEditFormData();")
			
			// Gestione Click Input Form Info STLC1000
			.codeChr(1,4).'$("#info_details_panel_form").find(".details_panel_form_input").focus(function() {'
				.codeChr(1,5).'var value = $(this).attr("value");'
				.codeChr(1,5).'var value_active_element = $(this).attr("id");'
				.codeChr(1,5).'value_active_element = value_active_element.replace("info_details_panel_form_", "details_panel_info_");'
				.codeChr(1,5).'value_active_element = value_active_element.replace("_input", "_detected_value");'
				.codeChr(1,5).'value_active_element = "#"+value_active_element;'
				.codeChr(1,5).'var value_active = $(value_active_element).html();'
				.codeChr(1,5).'if (value_active != null && value_active != "" && value_active != value && $(this).data("checked") != true){'
					.codeChr(1,6).'var next=confirm("Il valore configurato e\' diverso da quello rilevato. Vuoi sostituire quello configurato ("+value+") utilizzando quello rilevato ("+value_active+")?");'
					.codeChr(1,6).'if (next== true) $(this).attr("value",value_active);'
				.codeChr(1,5).'}'
				.codeChr(1,5).'$(this).data("checked",true);'
			.codeChr(1,4).'});'
		.codeChr(1,0).'}'
		;
	
	$_js .=
		 jsBarTextEventManager()
		 
		.codeChr(1,3).'$(".topbar_page_icon").mouseenter(function() {'
			.codeChr(1,4).'if (!$(this).prev().prev().is(":visible")) $(this).prev().show();'
		.codeChr(1,3).'}).mouseover(function() {'
			.codeChr(1,4).'if (!$(this).prev().prev().is(":visible")) $(this).prev().show();'
		.codeChr(1,3).'}).mouseleave(function() {'
			.codeChr(1,4).'$(this).prev().hide();'
		.codeChr(1,3).'}).mouseout(function() {'
			.codeChr(1,4).'$(this).prev().hide();'
		.codeChr(1,3).'});'
					
		.codeChr(1,3).'$(".topbar_page_title").mouseenter(function() {'
			.codeChr(1,4).'if (!$(this).prev().prev().prev().is(":visible")) $(this).prev().show();'
		.codeChr(1,3).'}).mouseover(function() {'
			.codeChr(1,4).'if (!$(this).prev().prev().prev().is(":visible")) $(this).prev().show();'
		.codeChr(1,3).'}).mouseleave(function() {'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'}).mouseout(function() {'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'});'
					
		/*
		.codeChr(1,3).'$(".navbar_selector_button").mouseover(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'}).mouseout(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'}).mousedown(function(event) {'
			.codeChr(1,4).'$(this).prev().prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().prev().show();'
			.codeChr(1,4).jsEventPreventDefault()
		.codeChr(1,3).'}).mouseup(function() {'
			.codeChr(1,4).'$(this).prev().prev().prev().show();'
			.codeChr(1,4).'$(this).prev().prev().hide();'
		.codeChr(1,3).'}).click(function(event) {'
			.codeChr(1,4).'eval($(this).attr("id")+"Click();");'
			.codeChr(1,4).jsEventPreventDefault()
		.codeChr(1,3).'});'
		*/
					
		.codeChr(1,3).'$(".navbar_arrow_button").mouseover(function() {'
			.codeChr(1,4).'$(this).prev().prev().show();'
			.codeChr(1,4).'$(this).prev().hide();'
		.codeChr(1,3).'}).mouseout(function() {'
			.codeChr(1,4).'$(this).prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().hide();'
		.codeChr(1,3).'}).mousedown(function(event) {'
			.codeChr(1,4).'$(this).prev().prev().hide();'
			.codeChr(1,4).'$(this).prev().show();'
			.codeChr(1,4).jsEventPreventDefault()
		.codeChr(1,3).'}).mouseup(function() {'
			.codeChr(1,4).'$(this).prev().prev().show();'
			.codeChr(1,4).'$(this).prev().hide();'
		.codeChr(1,3).'}).click(function(event) {'
			.codeChr(1,4).'$(this).prev().prev().prev().prev().toggle();'
			.codeChr(1,4).'$(this).prev().prev().prev().toggle();'
			.codeChr(1,4).'$(this).next().toggle();'
			.codeChr(1,4).'eval($(this).attr("id")+"Click();");'
			.codeChr(1,4).jsEventPreventDefault()
		.codeChr(1,3).'});'
		
		.jsBarSelectorButtonEventManager()
				
		//.codeChr(1,3).'pageLinkEvents();'
													
		.jsBuildScriptClose();
	
	return($_js);
}

/**
* Funzione per costruire il codice per il pannello informazioni di un item generico
*/
function systemPageBuildItemInfoPanel($itemParams)
{
	logDebug("systemPageBuildItemInfoPanel");
	
	// Inizializzo il codice HTML che verra' restituito
	$_html = codeInit();
	
	if (isset($itemParams))
	{
		// Recupero i singoli parametri
		$_item_params 		= $itemParams;
		$_item_category		= $_item_params['Category'];
		$_item_index 		= $_item_params['Index'];
		$_item_id			= $_item_params['PortID'];
		$_item_type			= $_item_params['Type'];
		$_item_name			= $_item_params['Name'];
		$_item_status_code	= $_item_params['StatusCode'];
		
		// Preparo la struttura per il pillolone
		$_item_pill_data = null; // Nessun pillolone
		
		// Genero il codice HTML
		$_html .= htmlPanelBuild( 	$_item_category,
									$_item_index, 
									$_item_id,
									$_item_type,
									$_item_name,
									$_item_status_code,
									$_item_pill_data);
	}
	
	return($_html);
}


/**
* Funzione per costruire il codice HTML per il contenuto della pagina sistema
*/
function systemPageBuildContentHTML($data)
{
	logDebug("systemPageBuildContentHTML()");
	
	global $_configuration;
	global $_conf_console_app_system_edit_enabled;
	global $_conf_console_app_system_info_edit_enabled;
	global $_conf_console_app_hardware_device_code;
	if (!isset($_conf_console_app_hardware_device_code)) $_conf_console_app_hardware_device_code = 'STLC1000';
	$_data = $data;
		
	// Recupero informazioni sul livello di visualizzazione
	$_level = $_data['level'];
	// Recupero informazioni sui filtri attivi
	$_filter = $_data['filter'];
	
	// Inizializzo il codice HTML che verra' restituito
	$_html = codeInit();
	
	if (empty($_level))
	{
		$_level = systemGetPageLevel();
	}
	
	if ($_level == "serial")
	{
		// Recupero parametri di configurazione porte
		$_items = $_configuration["ports"];
		// Recupero informazioni di stato (ottenute dal DB) sulle periferiche
		$_devices_data = $_data['device'];
		
		$_index = 0;
		// Ciclo sugli armadi configurati
		foreach ( $_items as $_item )
		{
			logDebug("Building port HTML panel #".$_index);
		
			$_id = $_item->id;
			$_status_code = -255;
			
			// Preparo i parametri dell'elemento
			$_item_params = array();
			$_item_params['Category'] 	= 'port';
			$_item_params['Index'] 		= $_index;
			$_item_params['PortID'] 	= $_id;
			$_item_params['Type'] 		= $_item->type;
			$_item_params['Name'] 		= $_item->name;
			$_item_params['StatusCode'] = $_status_code;
						
			if ($_item->type != "TCP_Client")
			{
				$_html .= systemPageBuildItemInfoPanel($_item_params);
				$_index++;
			}
		}
		
		$_content_height = 50+(100*ceil(count($_items)/14));
	}
	else if ($_level == "ethernet")
	{
		// Recupero parametri di configurazione porte
		$_items = $_configuration["network"];
		
		$_index = 0;
		// Ciclo sulle porte ethernet configurate
		foreach ( $_items as $_item )
		{
			$_id = $_item->id;
			$_status_code = -255;
			
			// Preparo i parametri della porta
			$_port_params = array();
			$_port_params['Category'] 	= 'port';
			$_port_params['Index'] 		= $_index;
			$_port_params['PortID'] 	= $_id;
			$_port_params['Type'] 		= $_item->type;
			$_port_params['Name'] 		= $_item->name;
			$_port_params['StatusCode'] = $_status_code;
						
			$_html .= systemPageBuildItemInfoPanel($_port_params);
			$_index++;
		}
		
		$_content_height = 50+(100*ceil(count($_items)/14));
	}
	else if ($_level == "info")
	{
		$_info = $_configuration["info"];
		$_servers = $_configuration["servers"];

		$_server = getServer($_servers);
		
		$_sk_sn = $_info["sk_sn"];
		$_sk_id = serialNumberToServerId($_sk_sn);
		$_sk_mode = $_info["sk_mode"];
		$_services_status = $_info["services_status"];
		
		if ($_sk_mode == "NORMAL")
		{
			$_sk_mode_text = "Normale";
		}
		else if ($_sk_mode == "MAINTENANCE")
		{
			$_sk_mode_text = "In manutenzione";
		}
		else
		{
			$_sk_mode_text = "n/d";
		}
		
		if ($_services_status == "1")
		{
			$_services_status_text = "In esecuzione";
		}
		else if ($_services_status == "0")
		{
			$_services_status_text = "Uno o pi&ugrave; servizi non in esecuzione";
		}
		else
		{
			$_services_status_text = "n/d";
		}
		
		
		if (isset($_sk_sn))
			$_sk_sn_html = " (".$_sk_sn.")";
		else
			$_sk_sn_html = "";
			
		// Preparo gli ID
		$_base				= "info";
		$_id_panel			= $_base."_details_panel";
		$_id_panel_info		= $_base."_details_panel_info";
		$_id_button_edit 	= "edit";
		
		$_id_panel_form		= $_base."_details_panel_form";
		$_id_button_save 	= "save";
		$_id_button_cancel 	= "cancel";

		
		// HTML Dettagli STLC1000
		$_panel_info_html = codeInit()
			// Taglio a sinistra
			.htmlPanelVerticalCut('left',270,0,100)
			// Taglio a destra
			.htmlPanelVerticalCut('left',525,0,100)
			// Prima riga
			.(($_conf_console_app_system_edit_enabled!==false && $_conf_console_app_system_info_edit_enabled!==false)?htmlPanelInfo('host'			,0	,5	,135,'Hostname configurato'	,130,$_server->host):'')
			.htmlPanelInfo('host_active'	,275,5	,115,'Hostname rilevato'	,130,$_info["sk_hostname"])
			.htmlPanelInfo('mode'			,530,5	,60	,'Modalit&agrave;'		,140,$_sk_mode_text)
			// Seconda riga
			.(($_conf_console_app_system_edit_enabled!==false && $_conf_console_app_system_info_edit_enabled!==false)?htmlPanelInfo('id'				,0	,35	,135,'ID configurato'		,130,$_server->id):'')
			.htmlPanelInfo('id_active'		,275,35	,115,'ID rilevato'			,130,$_sk_id)
			.htmlPanelInfo('services_status',530,35	,60	,'Servizi'				,140,$_services_status_text)
			// Terza riga
			.htmlPanelInfo('version'		,275,65	,115,'Versione'				,130,$_info["sk_version"])
			.htmlPanelInfo('sn'				,530,65	,60	,'SN'					,140,$_sk_sn)
			;
		
		
		if ($_conf_console_app_system_edit_enabled!==false && $_conf_console_app_system_info_edit_enabled!==false && authCheckProfile("admin")){
			$_panel_info_btns_html = codeInit()
			// Bottone Edit (AUTH)
			.htmlPanelButton($_id_panel_info,$_id_button_edit,"details_panel_button",80,80,'edit',null,false,'admin', "Modifica parametri")
			;
		
			// HTML Form Edit STLC1000
			$_panel_form_html = codeInit()
			// Taglio in alto centrale
			.htmlPanelVerticalCut('left',270,0,100)
			// Prima riga
			.htmlPanelFormInput($_id_panel, 	'host'			,0	,5	,135,'Hostname configurato'	,120,$_server->host)
			.htmlPanelInfo(						'host_detected'	,275,5	,115,'Hostname rilevato'	,120,$_info["sk_hostname"])
			// Seconda riga
			.htmlPanelFormInput($_id_panel,		'id'			,0	,35	,135,'ID configurato'		,120,$_server->id)
			.htmlPanelInfo(						'id_detected'	,275,35	,115,'ID rilevato'			,120,$_sk_id)
			;
		
			// HTML Bottoni Form Edit STLC1000
			$_panel_form_btns_html = codeInit()
			// Bottone Save (AUTH)
			.htmlPanelButton( $_id_panel_form, $_id_button_save,   "details_panel_button",80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Conferma modifche" )
			// Bottone Cancel (AUTH)
			.htmlPanelButton( $_id_panel_form, $_id_button_cancel, "details_panel_button",45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Annulla modifche" )
			;
		}else{
			// HTML Bottoni Informazioni 
			$_panel_info_btns_html = codeInit();
			// HTML Form Edit 
			$_panel_form_html = codeInit();
			// HTML Bottoni Form Edit 
			$_panel_form_btns_html = codeInit();
		}
		$_inner_html = codeInit()
			.codeChr(1,5).htmlDivitis( $_id_panel_info			, 'details_panel_info'		, $_panel_info_html )
			.codeChr(1,5).htmlDivitis( $_id_panel_info."_btns"	, 'details_panel_info_btns'	, $_panel_info_btns_html )
			.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'details_panel_form'		, $_panel_form_html )
			.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'details_panel_form_btns'	, $_panel_form_btns_html )
			;
		
		$_panel_height = 120;
	
		$_html .= htmlFixedPanelBuild('info_details_panel','devices/'.$_conf_console_app_hardware_device_code.'_100.png',$_inner_html,$_panel_height);
		
		$_content_height = $_panel_height+50;
	}
	else
	{
		//
	}
	
	//$_html .= codeChr(1,4).'<div class="item_detail" ></div>';
	
	$_html = htmlDivitis("content_middle", null, $_html, "height: ".($_content_height)."px;");
	
	return($_html);
}

/**
* Funzione per costruire il codice HTML per il dettaglio livello info
*/
function systemPageBuildInfoDetailsHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	
	
	
	
	
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	if (isset($_station->type))
		$r_image 	= $_base.'s/'.$_station->type.'_100.png';
	else
		$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 150;
	$_html 		= $_inner_html;
	
	return($_html);
}

/**
* Funzione per costruire il codice HTML per il dettaglio livello ethernet
*/
function systemPageBuildEthernetDetailsHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	global $_conf_console_app_system_edit_enabled;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_network 				= $_configuration["network"];
	$_port = null;
	
	$_selection 			= $_data['selection'];
	if (isset($_selection["ItemID"]))
		$_selected_port_id 	= $_selection["ItemID"];
	else
		$_selected_port_id	= 1;
	
	// Ciclo sulle porte ethernet configurate
	foreach ( $_network as $_item )
	{
		$_item_id = $_item->id;
		
		// Recupero la configurazione della porta
		if  ($_selected_port_id == $_item_id)
		{
			$_port = $_item;
			break;
		}
	}
	
	// Preparo gli ID
	$_base				= "ethernet";
	$_id_panel			= $_base."_details_panel";
	$_id_panel_info		= $_base."_details_panel_info";
	$_id_button_edit 	= "edit";
	$_id_button_delete 	= "delete";
	
	$_id_panel_form		= $_base."_details_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	if (isset($_port))
	{
		$_offset_y = 5;
		// PARAMETRI CONFIGURATI
		// Preparo i testi
		$_text_info_name		= $_port->name;
		
		if ($_port->ip == "auto")
		{
			$_text_info_dhcp = "attivo";
			$_text_info_view = false;
		}
		else
		{
			$_text_info_dhcp = "non attivo";
			$_text_info_view = true;
		}
		
		$_text_info_port		= $_port->type.$_port->port;
		
		// Indirizzo IP primario
		$_text_info_ip			= $_port->ip;
		$_text_info_subnet		= $_port->subnet;
		// Indirizzi IP secondari
		$_extra_ip				= 0;
		$_extra_2				= false;
		$_extra_3				= false;
		$_extra_4				= false;
		$_text_info_ip2			= $_port->ip2;
		$_text_info_subnet2		= $_port->subnet2;
		if (isset($_text_info_ip2) && $_text_info_ip2 != '') {
			$_extra_2 = true;
			$_extra_ip++;
		}
		$_text_info_ip3			= $_port->ip3;
		$_text_info_subnet3		= $_port->subnet3;
		if (isset($_text_info_ip3) && $_text_info_ip3 != '') {
			$_extra_3 = true;
			$_extra_ip++;
		}
		$_text_info_ip4			= $_port->ip4;
		$_text_info_subnet4		= $_port->subnet4;
		if (isset($_text_info_ip4) && $_text_info_ip4 != '') {
			$_extra_4 = true;
			$_extra_ip++;
		}
				
		$_text_info_gateway		= $_port->gateway;
		$_text_info_dns			= $_port->dns1;
		if (isset($_port->dns2) && $_port->dns2 != "")
			$_text_info_dns		.= ",".$_port->dns2;
			
		$_text_input_port		= "ETH".$_port->port;
		
		// HTML Informazioni Porta Ethernet
		$_panel_info_html = codeInit()
			// Taglio in alto centrale
			.htmlPanelVerticalCut('left',440,0,15+(7)*30)
			;
		
		$_offset_x = 0;
		// Informazioni prima riga SX
		$_row = 0;
		$_panel_info_html .=
			 htmlPanelInfo( 'port',		$_offset_x+0,	$_offset_y+($_row*30),	40,	'Porta',	70, 	$_text_info_port )
			.htmlPanelInfo( 'name',		$_offset_x+140,	$_offset_y+($_row*30),	40,	'Nome',		165, 	$_text_info_name );
		// Informazioni seconda riga SX
		$_row++;
		$_panel_info_html .=
			(($_conf_console_app_system_edit_enabled!==false)?htmlPanelInfo( 'auto',		0,	5+($_row*30),	40,	'DHCP',	70, $_text_info_dhcp ):'');
		
		// SE DHCP NON E' ATTIVO
		if ($_conf_console_app_system_edit_enabled!==false && $_text_info_view)
		{
			// Informazioni terza riga SX
			$_row++;		
			$_panel_info_html .= 
				 htmlPanelInfo( 'ip',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP'.(($_extra_ip>0)?'1':''),	100, $_text_info_ip )
				.htmlPanelInfo( 'subnet', 	$_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask'.(($_extra_ip>0)?'1':''),	100, $_text_info_subnet );
				
			if ($_extra_2){
				$_row++;
				$_panel_info_html .= 
					 htmlPanelInfo( 'ip2',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP2',		100, $_text_info_ip2 )
					 .htmlPanelInfo( 'subnet2', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask2',	100, $_text_info_subnet2 );
			}
			if ($_extra_3){
				$_row++;
				$_panel_info_html .= 
					 htmlPanelInfo( 'ip3',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP3',		100, $_text_info_ip3 )
					 .htmlPanelInfo( 'subnet3', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask3',	100, $_text_info_subnet3 );
			}
			if ($_extra_4){
				$_row++;
				$_panel_info_html .= 
					 htmlPanelInfo( 'ip4',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP4',		100, $_text_info_ip4 )
					 .htmlPanelInfo( 'subnet4', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask4',	100, $_text_info_subnet4 );
			}
		}
		$_row++;
		$_panel_info_html .=	
			 htmlPanelInfo( 'gateway',	$_offset_x+0,	$_offset_y+($_row*30),	25,	'GW',		100, $_text_info_gateway )
			.htmlPanelInfo( 'dns'	, 	$_offset_x+140,	$_offset_y+($_row*30),	30,	'DNS',		185, $_text_info_dns );
		
		$_extra_max = $_extra_ip;
			
		// PARAMETRI RILEVATI
		$_network_active = $_configuration["network_active"];
			
		$_offset_x = 450;
			
		if ($_network_active['eth'.$_port->port.'_auto']=='true')
			$_eth_auto 	= 'attivo';
		else if ($_network_active['eth'.$_port->port.'_auto']=='false')
			$_eth_auto 	= 'non attivo';
		else
			$_eth_auto 	= '';
		$_eth_ip 	= $_network_active['eth'.$_port->port.'_ip'];
		$_eth_mask 	= $_network_active['eth'.$_port->port.'_mask'];
		$_eth_ip2 	= $_network_active['eth'.$_port->port.'_ip2'];
		$_eth_mask2 = $_network_active['eth'.$_port->port.'_mask2'];
		$_eth_ip3 	= $_network_active['eth'.$_port->port.'_ip3'];
		$_eth_mask3 = $_network_active['eth'.$_port->port.'_mask3'];
		$_eth_ip4 	= $_network_active['eth'.$_port->port.'_ip4'];
		$_eth_mask4 = $_network_active['eth'.$_port->port.'_mask4'];
		$_eth_gw 	= $_network_active['eth'.$_port->port.'_gw'];
		$_eth_dns1 	= $_network_active['eth'.$_port->port.'_dns1'];
		$_eth_dns2	= $_network_active['eth'.$_port->port.'_dns2'];
		
		// Indirizzi IP secondari
		$_extra_ip				= 0;
		$_extra_2				= false;
		$_extra_3				= false;
		$_extra_4				= false;
		if (isset($_eth_ip2) && $_eth_ip2 != '') {
			$_extra_2 = true;
			$_extra_ip++;
		}
		if (isset($_eth_ip3) && $_eth_ip3 != '') {
			$_extra_3 = true;
			$_extra_ip++;
		}
		if (isset($_eth_ip4) && $_eth_ip4 != '') {
			$_extra_4 = true;
			$_extra_ip++;
		}
			
			if (isset($_eth_dns1) && isset($_eth_dns2) && $_eth_dns1 != "" && $_eth_dns2 != "")
			{
				$_eth_dns = $_eth_dns1.",".$_eth_dns2;
			}
			else
			{
				$_eth_dns = $_eth_dns1.$_eth_dns2;
			}
				
		// Informazioni prima riga DX
		$_row = 0;
		$_panel_info_html .= 
			htmlPanelInfo( 'active',	$_offset_x+0,	$_offset_y+0,	200,'Parametri rilevati',	160, "" );
		
		// Informazioni seconda riga DX
		$_row++;
		$_panel_info_html .=
			htmlPanelInfo( 'auto_active',	$_offset_x+0,	$_offset_y+($_row*30),	40,	'DHCP',	70, $_eth_auto );
		
		// Informazioni terza riga SX
		$_row++;		
		$_panel_info_html .= 
			 htmlPanelInfo( 'ip',			$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP'.(($_extra_ip>0)?'1':''),	100, $_eth_ip )
			.htmlPanelInfo( 'subnet', 		$_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask'.(($_extra_ip>0)?'1':''),	100, $_eth_mask );
				
		if ($_extra_2){
			$_row++;
			$_panel_info_html .= 
				 htmlPanelInfo( 'ip2',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP2',		100, $_eth_ip2 )
				 .htmlPanelInfo( 'subnet2', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask2',	100, $_eth_mask2 );
		}
		if ($_extra_3){
			$_row++;
			$_panel_info_html .= 
				 htmlPanelInfo( 'ip3',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP3',		100, $_eth_ip3 )
				 .htmlPanelInfo( 'subnet3', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask3',	100, $_eth_mask3 );
		}
		if ($_extra_4){
			$_row++;
			$_panel_info_html .= 
				 htmlPanelInfo( 'ip4',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP4',		100, $_eth_ip4 )
				 .htmlPanelInfo( 'subnet4', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask4',	100, $_eth_mask4 );
		}
		
		$_row++;
		$_panel_info_html .=	
			 htmlPanelInfo( 'gateway',	$_offset_x+0,	$_offset_y+($_row*30),	25,	'GW',		100, $_eth_gw )
			.htmlPanelInfo( 'dns'	, 	$_offset_x+140,	$_offset_y+($_row*30),	30,	'DNS',		185, $_eth_dns );
		
		
		$_extra_max = ($_extra_ip>$_extra_max)?$_extra_ip:$_extra_max;
				
		//if ($_port->port == "1" || authCheckProfile("admin"))
		if ($_conf_console_app_system_edit_enabled!==false)
		{			
			// HTML Bottoni Informazioni Armadio
			$_panel_info_btns_html = codeInit()
				// Bottone Edit (AUTH)
				.htmlPanelButton( $_id_panel_info, $_id_button_edit,	"details_panel_button",80,	200,	'edit',	 null, false, 'admin,IaP_installer,IeC_installer,installer', "Modifica parametri" )
				;
			// HTML Form Edit Porta Ethernet
			$_panel_form_html = codeInit()
				// Taglio in alto centrale
				.htmlPanelVerticalCut('left',440,0,15+(7)*30);
			
			$_offset_x = 0;
			$_offset_y = 5;
				
			// Informazioni in alto a SX
			$_row = 0;
			$_panel_form_html .=
				 htmlPanelInfo( 						'port',		$_offset_x+0,	$_offset_y+($_row*30),	40,	'Porta',	70, $_text_info_port )
				.htmlPanelFormInput( $_id_panel, 	'name',		$_offset_x+140,	$_offset_y+($_row*30),	40,	'Nome',		245, $_text_info_name );
			
			// Informazioni seconda riga DX
			$_row++;
			
			// Informazioni terza riga SX
			$_row++;		
			$_panel_form_html .= 
				 htmlPanelFormInput( $_id_panel,	'ip',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP'.(($_extra_ip>0)?'1':''),	100, $_text_info_ip )
				.htmlPanelFormInput( $_id_panel,	'subnet', 	$_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask'.(($_extra_ip>0)?'1':''),	100, $_text_info_subnet );
				
			$_row++;
			$_panel_form_html .= 
				 htmlPanelFormInput( $_id_panel,	'ip2',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP2',		100, $_text_info_ip2 )
				.htmlPanelFormInput( $_id_panel,	'subnet2',	$_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask2',	100, $_text_info_subnet2 );
			
			$_row++;
			$_panel_form_html .= 
				 htmlPanelFormInput( $_id_panel,	'ip3',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP3',		100, $_text_info_ip3 )
				.htmlPanelFormInput( $_id_panel,	'subnet3',	$_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask3',	100, $_text_info_subnet3 );
			
			$_row++;
			$_panel_form_html .= 
				 htmlPanelFormInput( $_id_panel,	'ip4',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP4',		100, $_text_info_ip4 )
				.htmlPanelFormInput( $_id_panel,	'subnet4',	$_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask4',	100, $_text_info_subnet4 );
			
			$_row++;
			$_panel_form_html .=	
				 htmlPanelFormInput( $_id_panel,	'gateway',	$_offset_x+0,	$_offset_y+($_row*30),	25,	'GW',		100, $_text_info_gateway )
				.htmlPanelFormInput( $_id_panel,	'dns1'	, 	$_offset_x+140,	$_offset_y+($_row*30),	35,	'DNS1',		100, $_port->dns1 )
				.htmlPanelFormInput( $_id_panel,	'dns2'	, 	$_offset_x+290,	$_offset_y+($_row*30),	35,	'DNS2',		100, $_port->dns2 );
			
			$_offset_x = 450;
			
			// Informazioni prima riga DX
		$_row = 0;
		$_panel_form_html .= 
			htmlPanelInfo( 'active',	$_offset_x+0,	$_offset_y+0,	200,'Parametri rilevati',	160, "" );
		
		// Informazioni seconda riga DX
		$_row++;
		$_panel_form_html .=
			htmlPanelInfo( 'auto_active',	$_offset_x+0,	$_offset_y+($_row*30),	40,	'DHCP',	70, $_eth_auto );
		
		// Informazioni terza riga SX
		$_row++;		
		$_panel_form_html .= 
			 htmlPanelInfo( 'ip',			$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP'.(($_extra_ip>0)?'1':''),	100, $_eth_ip )
			.htmlPanelInfo( 'subnet', 		$_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask'.(($_extra_ip>0)?'1':''),	100, $_eth_mask );
				
		if ($_extra_2){
			$_row++;
			$_panel_form_html .= 
				 htmlPanelInfo( 'ip2',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP2',		100, $_eth_ip2 )
				 .htmlPanelInfo( 'subnet2', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask2',	100, $_eth_mask2 );
		}
		if ($_extra_3){
			$_row++;
			$_panel_form_html .= 
				 htmlPanelInfo( 'ip3',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP3',		100, $_eth_ip3 )
				 .htmlPanelInfo( 'subnet3', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask3',	100, $_eth_mask3 );
		}
		if ($_extra_4){
			$_row++;
			$_panel_form_html .= 
				 htmlPanelInfo( 'ip4',		$_offset_x+0,	$_offset_y+($_row*30),	25,	'IP4',		100, $_eth_ip4 )
				 .htmlPanelInfo( 'subnet4', $_offset_x+140,	$_offset_y+($_row*30),	40,	'Mask4',	100, $_eth_mask4 );
		}
		
		$_row++;
		$_panel_form_html .=	
			 htmlPanelInfo( 'gateway',	$_offset_x+0,	$_offset_y+($_row*30),	25,	'GW',		100, $_eth_gw )
			.htmlPanelInfo( 'dns'	, 	$_offset_x+140,	$_offset_y+($_row*30),	30,	'DNS',		185, $_eth_dns );
			
			// HTML Bottoni Form Edit Porta Ethernet
			$_panel_form_btns_html = codeInit()
				// Bottone Save (AUTH)
				.htmlPanelButton( $_id_panel_form, $_id_button_save,   "details_panel_button",80, 200, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Conferma modifche" )
				// Bottone Cancel (AUTH)
				.htmlPanelButton( $_id_panel_form, $_id_button_cancel, "details_panel_button",45, 200, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Annulla modifche" )
				;
		}else{
			// HTML Bottoni Informazioni 
			$_panel_info_btns_html = codeInit();
			// HTML Form Edit 
			$_panel_form_html = codeInit();
			// HTML Bottoni Form Edit 
			$_panel_form_btns_html = codeInit();
		}
	}
	else
	{
		// HTML Informazioni Stazione
		$_panel_info_html = codeInit();
		// HTML Bottoni Informazioni Stazione
		$_panel_info_btns_html = codeInit();
		// HTML Form Edit Stazione
		$_panel_form_html = codeInit();
		// HTML Bottoni Form Edit Stazione
		$_panel_form_btns_html = codeInit();

	}
	
	$_extra_max = 3; // Dimensione pannello fissa
	
	// HTML Pannello Dettaglio Armadio	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis( $_id_panel_info			, 'details_panel_info'		, $_panel_info_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_info."_btns"	, 'details_panel_info_btns'	, $_panel_info_btns_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'details_panel_form'		, $_panel_form_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'details_panel_form_btns'	, $_panel_form_btns_html )
		;
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	if (isset($_port->type))
		$r_image 	= 'ports/'.$_port->type.'_100.png';
	else
		$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 180+($_extra_max*30);
	$_html 		= $_inner_html;
	
	return($_html);
}

/**
* Funzione per costruire il codice HTML per il dettaglio livello serial
*/
function systemPageBuildSerialDetailsHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	global $_conf_console_app_system_edit_enabled;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_ports 				= $_configuration["ports"];
	$_port = null;
	
	$_selection 			= $_data['selection'];
	
	if (isset($_selection["ItemID"]))
		$_selected_port_id 	= $_selection["ItemID"];
	else
		$_selected_port_id	= 1;
	
	// Ciclo sulle porte seriali configurate
	foreach ( $_ports as $_item )
	{
		$_item_id = $_item->id;
		
		// Recupero la configurazione della porta
		if  ($_selected_port_id == $_item_id)
		{
			$_port = $_item;
			break;
		}
	}
	
	// Preparo gli ID
	$_base				= "serial";
	$_id_panel			= $_base."_details_panel";
	$_id_panel_info		= $_base."_details_panel_info";
	$_id_button_edit 	= "edit";
	$_id_button_delete 	= "delete";
	
	$_id_panel_form		= $_base."_details_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	if (isset($_port))
	{
		$_port_type_list		= $_configuration["port_type_list"];
	
		// Preparo i testi
		$_text_info_name		= $_port->name;
		$_text_info_type		= getPortTypeNameFromCode($_port_type_list,$_port->type);
		$_text_info_com			= "COM".$_port->com;
		$_text_info_baud		= $_port->baud;
		$_text_info_echo		= $_port->echo;
		$_text_info_data		= $_port->data;
		$_text_info_stop		= $_port->stop;
		$_text_info_parity		= $_port->parity;
		
		$_text_select_type		= getPortTypeNameFromCode($_port_type_list,$_port->type);
		$_text_input_com		= $_port->com;
		
		// HTML Informazioni Porta
		$_panel_info_html = codeInit()
			// Taglio in alto centrale
			.htmlPanelVerticalCut('left',485,0,100)
			// Informazioni in alto a SX
			.htmlPanelInfo( 'name',		0,	5,	65,	'Nome',		415, $_text_info_name )
			.htmlPanelInfo( 'type',		0,	35,	65,	'Tipo',		170, $_text_info_type )
			.htmlPanelInfo( 'com',		0,	65,	65,	'Hardware',	170, $_text_info_com )
			// Informazioni al centro
			.htmlPanelInfo( 'baud',		245,35,	65,	'Baud',		170, $_text_info_baud )
			.htmlPanelInfo( 'echo',		245,65,	65,	'Echo',		170, $_text_info_echo )
			// Informazioni in alto a DX
			.htmlPanelInfo( 'data', 	490,5,	65,	'Bit dato',				170, $_text_info_data )
			.htmlPanelInfo( 'stop', 	490,35,	65,	'Bit stop',				170, $_text_info_stop )
			.htmlPanelInfo( 'parity', 	490,65,	65,	'Bit parit&agrave;',	170, $_text_info_parity )
			;
		
		if ($_conf_console_app_system_edit_enabled!==false){
			// HTML Bottoni Informazioni Armadio
			$_panel_info_btns_html = codeInit()
				// Bottone Edit (AUTH)
				.htmlPanelButton( $_id_panel_info, $_id_button_edit,	"details_panel_button",80,	80,	'edit',	 null, false, 'admin,IaP_installer,IeC_installer,installer', "Modifica parametri" )
				// Bottone Delete (AUTH)
				//.htmlPanelButton( $_id_panel_info, $_id_button_delete,	45,	80,	'minus', null, false, 'admin,installer' )
				;
			// HTML Form Edit Armadio
			$_panel_form_html = codeInit()
				// Taglio in alto centrale
				.htmlPanelVerticalCut('left',485,0,100)
				// Informazioni in alto a SX
				.htmlPanelFormInput( $_id_panel,	'name',		0,	5,	65,	'Nome',		415, $_text_info_name )
				.htmlPanelFormSelect($_id_panel,  	'type',		0,	35,	65,	'Tipo',		170, $_text_select_type )
				.htmlPanelFormInput( $_id_panel,	'com',		0,	65,	65,	'COM',		170, $_text_input_com )
				// Informazioni al centro
				.htmlPanelFormInput( $_id_panel,	'baud',		245,35,	65,	'Baud',		170, $_text_info_baud )
				.htmlPanelFormInput( $_id_panel,	'echo',		245,65,	65,	'Echo',		170, $_text_info_echo )
				// Informazioni in alto a DX
				.htmlPanelFormInput( $_id_panel,	'data', 	490,5,	65,	'Bit dato',				170, $_text_info_data )
				.htmlPanelFormInput( $_id_panel,	'stop', 	490,35,	65,	'Bit stop',				170, $_text_info_stop )
				.htmlPanelFormInput( $_id_panel,	'parity', 	490,65,	65,	'Bit parit&agrave;', 	170, $_text_info_parity )
				;
			// HTML Bottoni Form Edit Seriale
			$_panel_form_btns_html = codeInit()
				// Bottone Save (AUTH)
				.htmlPanelButton( $_id_panel_form, $_id_button_save,   "details_panel_button",80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Conferma modifche" )
				// Bottone Cancel (AUTH)
				.htmlPanelButton( $_id_panel_form, $_id_button_cancel, "details_panel_button",45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Annulla modifche" )
				;
		}else{
			// HTML Bottoni Informazioni 
			$_panel_info_btns_html = codeInit();
			// HTML Form Edit 
			$_panel_form_html = codeInit();
			// HTML Bottoni Form Edit 
			$_panel_form_btns_html = codeInit();
		}
	}
	else
	{
		// HTML Informazioni 
		$_panel_info_html = codeInit();
		// HTML Bottoni Informazioni 
		$_panel_info_btns_html = codeInit();
		// HTML Form Edit 
		$_panel_form_html = codeInit();
		// HTML Bottoni Form Edit 
		$_panel_form_btns_html = codeInit();

	}
	
	// HTML Pannello Dettaglio Armadio	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis( $_id_panel_info			, 'details_panel_info'		, $_panel_info_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_info."_btns"	, 'details_panel_info_btns'	, $_panel_info_btns_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'details_panel_form'		, $_panel_form_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'details_panel_form_btns'	, $_panel_form_btns_html )
		;
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	if (isset($_port->type))
		$r_image 	= 'ports/'.$_port->type.'_100.png';
	else
		$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 150;
	$_html 		= $_inner_html;
	
	return($_html);
}

/**
* Funzione per costruire il codice HTML per il dettaglio pagina sistema
*/
function systemPageBuildDetailsPanelHTML($data)
{
	logDebug("systemPageBuildDetailsPanelHTML");
	
	global $_configuration;
	
	$_html = codeInit();
	
	$_data = $data;
	
	// Recupero informazioni sul livello di visualizzazione
	$_level = $_data['level'];
	
	switch ($_level)
	{
		default:
		case "info":
			$_inner_html = systemPageBuildInfoDetailsHTML($_data,$_id,$_image,$_height);
		break;
		case "ethernet":
			$_inner_html = systemPageBuildEthernetDetailsHTML($_data,$_id,$_image,$_height);
		break;
		case "serial":
			$_inner_html = systemPageBuildSerialDetailsHTML($_data,$_id,$_image,$_height);
		break;
	}
	
	// Genero il codice HTML del pannello
	$_html .= htmlDetailsPanelBuild($_id,$_image,$_inner_html,$_height,$_height-30);
			
	return($_html);
}

/**
* Funzione per costruire il codice HTML per la pagina periferiche
*/
function systemPageBuildHTML($params)
{
	global $_configuration;

	$_html = codeInit();
	
	// Costruisco il menu
	$_html_menu = systemPageBuildMenuHTML();
	$_js_menu = systemPageBuildMenuJavaScript();
	
	// Costruisco il contenuto
	$_html_content = "";//devicesPageBuildContentHTML($params);
	$_js_content = systemPageBuildContentJavaScript();
	
	// Impacchetto tutto il codice
	$_html .= htmlBodyBuild($_html_menu,$_js_menu,$_html_content,$_js_content);

	return($_html);
}
?>
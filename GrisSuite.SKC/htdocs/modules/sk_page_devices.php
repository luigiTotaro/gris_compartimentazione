<?php
/**
* Telefin STLC1000 Consolle
*
* sk_page_devices.php - Modulo per la gestione pagina Periferiche.
*
* @author Enrico Alborali
* @version 1.0.4.2 08/09/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

$_page_devices = array();
$_page_devices['title'] = 'devices';
$_page_devices['profile'] = null;

$_page_devices_levels = array();

// Livello stazione
$_page_devices_level = array();
$_page_devices_level['name'] = 'station';
$_page_devices_level['profile'] = null;
$_page_devices_level['default_mode'] = 'info';
$_page_devices_level['info_mode'] = true;
$_page_devices_level['details_mode'] = true;
$_page_devices_level['edit_mode'] = true;
$_page_devices_level['add_mode'] = true;
$_page_devices_levels[] = $_page_devices_level;

// Livello edificio
$_page_devices_level = array();
$_page_devices_level['name'] = 'building';
$_page_devices_level['profile'] = null;
$_page_devices_level['default_mode'] = 'info';
$_page_devices_level['info_mode'] = true;
$_page_devices_level['details_mode'] = true;
$_page_devices_level['edit_mode'] = true;
$_page_devices_level['add_mode'] = true;
$_page_devices_levels[] = $_page_devices_level;

// Livello armadio
$_page_devices_level = array();
$_page_devices_level['name'] = 'rack';
$_page_devices_level['profile'] = null;
$_page_devices_level['default_mode'] = 'info';
$_page_devices_level['info_mode'] = true;
$_page_devices_level['details_mode'] = true;
$_page_devices_level['edit_mode'] = true;
$_page_devices_level['add_mode'] = true;
$_page_devices_levels[] = $_page_devices_level;

// Livello periferica
$_page_devices_level = array();
$_page_devices_level['name'] = 'device';
$_page_devices_level['profile'] = null;
$_page_devices_level['default_mode'] = 'info';
$_page_devices_level['info_mode'] = true;
$_page_devices_level['details_mode'] = true;
$_page_devices_level['edit_mode'] = true;
$_page_devices_level['add_mode'] = true;
$_page_devices_levels[] = $_page_devices_level;

$_page_devices['levels'] = $_page_devices_levels;
$_page_devices['default_level'] = 'device';

$_pages[] = $_page_devices;



/**
*
*/
function devicesGetPageLevel()
{
	$_level = coreGetLevelFromSession();
	if ($_level === null)
	{
		$_configuration = coreGetConfigurationFromSession();
		$_stations = $_configuration['stations'];
		$_buildings = $_configuration['buildings'];
		$_locations = $_configuration['locations'];
		if (count($_stations) == 0)
			$_level = 'station';
		else if (count($_buildings) == 0)
			$_level = 'building';
		else if (count($_locations) == 0)
			$_level = 'rack';
		else 
			$_level = 'device';
		coreSetLevelToSession($_level);
	}
	return($_level);
}

/**
* Funzione per costruire il menu
*/
function devicesPageBuildMenuHTML()
{
	global $_level;

	$_html = codeInit();
	
	$items = array();
	// Tipologia Alimentatori
	$item = array();
	$item['id'] = '0';
	$item['name'] = 'Verona P.ta Nuova';
	$items[] = $item;
	// Tipologia Amplificatori
	$item = array();
	$item['id'] = '1';
	$item['name'] = 'Verona P.ta Vescovo';
	$items[] = $item;
	// Tipologia Pannelli Zone
	$item = array();
	$item['id'] = '2';
	$item['name'] = 'Verona Scalo';
	$items[] = $item;
	
	$_stations = $items;
	
	$items = array();
	// Tipologia Alimentatori
	$item = array();
	$item['id'] = '0';
	$item['name'] = 'Fabbricato Viaggiatori';
	$items[] = $item;
	// Tipologia Amplificatori
	$item = array();
	$item['id'] = '1';
	$item['name'] = 'Sottostazione Elettrica';
	$items[] = $item;
	// Tipologia Pannelli Zone
	$item = array();
	$item['id'] = '2';
	$item['name'] = 'Piazzale';
	$items[] = $item;
	
	$_buildings = $items;
	
	$items = array();
	// Tipologia Alimentatori
	$item = array();
	$item['id'] = '0';
	$item['name'] = 'Rack N1';
	$items[] = $item;
	// Tipologia Amplificatori
	$item = array();
	$item['id'] = '1';
	$item['name'] = 'ATPS A';
	$items[] = $item;
	// Tipologia Pannelli Zone
	$item = array();
	$item['id'] = '2';
	$item['name'] = 'ATPS B';
	$items[] = $item;
	//
	$item = array();
	$item['id'] = '3';
	$item['name'] = 'ATPS C';
	$items[] = $item;
	
	$_racks = $items;
	
	$items = array();
	// Tipologia Alimentatori
	$item = array();
	$item['id'] = '0';
	$item['type'] = 'all';
	$item['name'] = 'Tutte le periferiche';
	$item['default'] = true;
	$items[] = $item;
	// Tipologia Amplificatori
	$item = array();
	$item['id'] = '1';
	$item['name'] = 'Amplificatori';
	$items[] = $item;
	// Tipologia Pannelli Zone
	$item = array();
	$item['id'] = '2';
	$item['type'] = 'select';
	$item['name'] = 'Pannelli Zone';
	$items[] = $item;
	// Tipologia Pannelli Zone
	$item = array();
	$item['id'] = '3';
	$item['type'] = 'select';
	$item['name'] = 'Alimentatori';
	$items[] = $item;
	// Tipologia Pannelli Zone
	$item = array();
	$item['id'] = '4';
	$item['type'] = 'none';
	$item['name'] = 'Nessun tipo';
	$items[] = $item;
	
	$_devices = $items;
	
	$_filter_iec_in_session = coreGetIeCFilterFromSession();
	//print('<!-- IeC in session:'.(int)$_filter_iec_in_session.' -->');
	
	if (isset($_filter_iec_in_session)){
		$_iec_selected = ($_filter_iec_in_session===true)?true:false;
	}
	else{
		$_iec_selected = (authCheckProfile('IeC_installer'))?true:false;
		$_SESSION['filter_iec'] = $_iec_selected;
	}
		
	//print('<!-- IeC:'.(int)$_iec_selected.' -->');
		
	$_filters_active = ($_level == 'device')?true:false;
		
	$_html .= 
		 coreBuildNavbarSelectorButton(0,'station',($_level == 'station')?true:false, 'Seleziona livello stazione')
		.coreBuildNavbarSelectorButton(1,'building',($_level == 'building')?true:false, 'Seleziona livello edificio')
		.coreBuildNavbarSelectorButton(2,'rack',($_level == 'rack')?true:false, 'Seleziona livello armadio')
		.coreBuildNavbarSelectorButton(3,'device',($_level == 'device')?true:false, 'Seleziona livello periferica')
		
		/* FILTRI TENDINA
		.coreBuildNavbarMenu(140,170,"filter_station",$_stations,"Tutte le stazioni",130,false,false,"admin,guest")
		.coreBuildNavbarMenu(300,190,"filter_building",$_buildings,"Tutti gli edifici",130,false,false,"admin,guest")
		.coreBuildNavbarMenu(460,170,"filter_rack",$_racks,"Tutti gli armadi",130,false,false,"admin,guest")
		.coreBuildNavbarMenu(620,180,"filter_device",$_devices,"Tutte le periferiche",130,true,false,"admin,guest")
		//*/
		
		//.htmlNavbarButton('topbar','iec',250,'iec',null,false,'default', 'IeC_installer,admin', true, false, false, 'Visualizza solo periferiche collegate al sistema IeC')
				
		// Filtri Stato Periferiche
		.htmlNavbarStatusFilter('device','right',210,'offline','black',true,'Periferiche offline',null,$_filters_active)
		.htmlNavbarStatusFilter('device','right',180,'error','red',true,'Periferiche in errore',null,$_filters_active)
		.htmlNavbarStatusFilter('device','right',150,'warning','yellow',true,'Periferiche in anomalia',null,$_filters_active)
		.htmlNavbarStatusFilter('device','right',120,'ok','green',true,'Periferiche in servizio',null,$_filters_active)
		.htmlNavbarStatusFilter('device','right',90,'unknown','gray',true,'Periferiche in stato sconosciuto',null,$_filters_active)
				
		.htmlNavbarCustomFilter('device','right',250,'iec',$_iec_selected,'Visualizza solo periferiche collegate al sistema IeC','IeC_installer,admin',$_filters_active)
		
		// Bottone Ricerca
		//.htmlNavbarButton('topbar','search',50,'search',null,false,'default',null, false, false, false, 'Cerca')
		
		// Bottone Aggiungi
		.htmlNavbarButton('topbar','add',10,'plus',null,false,'default','IaP_installer,IeC_installer,installer,admin', false, false, false, 'Aggiungi elemento')
		;
	
	return($_html);
}

/**
* Funzione per estrarre le descrizioni di un field
*/
function devicesPageExtractFieldDescriptions($text)
{
	$_descs = array();
	
	$_tokens = explode(";", $text);
	
	foreach ($_tokens as $_key => $_value)
	{
		$_desc = array();
		
		$_sub_tokens = explode("=", $_value);
		
		if (isset($_sub_tokens[0])) $_desc['Description'] = $_sub_tokens[0];
		if (isset($_sub_tokens[1])) $_desc['SevLevel'] = $_sub_tokens[1];
		else $_desc['SevLevel'] = '255';
		
		$_descs[] = $_desc;
	}
	
	return($_descs);
}

function devicesPageGetStatusCode($sevLevel,$offline,$active=1)
{
	if ($active != 1)
	{
		$_code = 253;
	}
	else
	{
		if ($offline == true)
		{
			$_code = 255;
		}
		else
		{
			$_code = $sevLevel;
		}
	}
	
	return($_code);
}

function devicesPageGetStatusColor($sevLevel,$offline,$active=1)
{
	global $_conf_console_html_color_code;
	
	if ($active != 1)
	{
		$_color = $_conf_console_html_color_code["white"];
	}
	else
	{
		if ($offline == true)
		{
			$_color = $_conf_console_html_color_code["red"];
		}
		else
		{
			if ($sevLevel == -255)
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
			else if ($sevLevel == 0)
			{
				$_color = $_conf_console_html_color_code["green"];
			}
			else if ($sevLevel == 1)
			{
				$_color = $_conf_console_html_color_code["yellow"];
			}
			else if ($sevLevel == 2)
			{
				$_color = $_conf_console_html_color_code["red"];
			}
			else if ($sevLevel == 9)
			{
				$_color = $_conf_console_html_color_code["white"];
			}
			else if ($sevLevel == 255)
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
			else
			{
				$_color = $_conf_console_html_color_code["gray"];
			}
		}
	}
	
	return($_color);
}

function devicesPageGetStatusText($sevLevel,$offline,$active=1)
{
	if ($active != 1)
	{
		$_text = "Non Attiva";
	}
	else
	{
		if ($offline == true)
		{
			$_text = "Non Raggiungibile";
		}
		else
		{
			if ($sevLevel == -255)
			{
				$_text = "Sconosciuto";
			}
			else if ($sevLevel == 0)
			{
				$_text = "In Servizio";
			}
			else if ($sevLevel == 1)
			{
				$_text = "Anomalia Lieve";
			}
			else if ($sevLevel == 2)
			{
				$_text = "Anomalia Grave";
			}
			else if ($sevLevel == 9)
			{
				$_text = "Non Applicabile";
			}
			else if ($sevLevel == 255)
			{
				$_text = "Non Raggiungibile";
			}
			else
			{
				$_text = "Sconosciuto";
			}
		}
	}
	
	return($_text);
}

function devicesPageBuildRegionListJavascript()
{
	$_js = codeInit();
	
	global $_conf_apps;
	global $_configuration;
	
	$_conf_app0 = $_conf_apps[0];
	$_configuration['region_list'] = coreLoadXMLData($_conf_app0["name"],"LoadXMLRegionList",$_conf_app0["region_list_path"]);
	
	$_region_list = $_configuration['region_list'];
	
	$_js .= codeChr(1,4).'var region_list = [';
	
	$_first = true;
	if (isset($_region_list) && count($_region_list) > 0)
	{
		foreach ($_region_list as $_region_item)
		{
			if (isset($_region_item))
			{
				$_region	= $_region_item['data'];
				$_zone_list = $_region_item['zone_list'];
			
				if (isset($_zone_list))
				foreach ($_zone_list as $_zone_item)
				{
					if (isset($_zone_item))
					{
						$_zone		= $_zone_item['data'];
						$_node_list	= $_zone_item['node_list'];
						
						if (isset($_node_list))
						foreach ($_node_list as $_node_item)
						{
							if (isset($_node_item))
							{
								$_node	= $_node_item['data'];
								
								$_value = str_replace('"','\"',$_region->name." > ".$_zone->name." > ".$_node->name);
								
								if ($_first === true){
									$_js .= codeChr(1,5).'"'.$_value.'"';
									$_first = false;
								}
								else
									$_js .= ','.codeChr(1,5).'"'.$_value.'"';
							}
						}		
					}
				}
			}
		}
	}
	else
	{
		// Region List Vuota
	}

	$_js .= codeChr(1,4).'];';
	
	return($_js);
}

function devicesPageBuildDeviceTypeListJavascript()
{
	$_js = codeInit();
	
	global $_configuration;
	
	$_device_type_list = $_configuration['device_type_list'];
	
	$_js .= codeChr(1,4).'var device_type_list = [';
	
	$_first = true;
	foreach ( $_device_type_list as $_device_type )
	{
		if ($_first === true){
			$_js .= codeChr(1,5).'"'.$_device_type->getDisplayName().'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,5).'"'.$_device_type->getDisplayName().'"';
	}
			
	$_js .= codeChr(1,4).'];';
	
	return($_js);
}

function devicesPageBuildStationTypeListJavascript()
{
	$_js = codeInit();
	
	global $_configuration;
	
	$_station_type_list = $_configuration['station_type_list'];
	
	$_js .= codeChr(1,4).'var station_type_list = [';
	
	$_first = true;
	
	foreach ( $_station_type_list as $_station_type )
	{
		if ($_first === true){
			$_js .= codeChr(1,5).'"'.$_station_type->getDisplayName().'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,5).'"'.$_station_type->getDisplayName().'"';
	}
			
	$_js .= codeChr(1,4).'];';
	
	return($_js);
}

/**
* Funzione per generare il codice javascript per la lista dei possibili valori del modem size
*/
function devicesPageBuildModemSizeListJavascript()
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'var modem_size_list = [';
	$_first = true;
	for ($s = 1; $s <= 2; $s++)
	{
		if ($_first === true){
			$_js .= codeChr(1,4).'"'.($s*8).'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,4).'"'.($s*8).'"';
	}
	$_js .= codeChr(1,3).'];';
	
	return($_js);
}

/**
* Funzione per generare il codice javascript per la lista dei possibili valori del modem address
*/
function devicesPageBuildModemAddrListJavascript()
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'var modem_addr_8_list = [';
	$_first = true;
	$_js .= codeChr(1,4).'"--"';
	$_first = false;
	for ($s = 0; $s <= 31; $s++)
	{
		if ($_first === true){
			$_js .= codeChr(1,4).'"'.sprintf("%02u",$s).'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,4).'"'.sprintf("%02u",$s).'"';
	}
	$_js .= codeChr(1,3).'];';
	
	$_js .= codeChr(1,3).'var modem_addr_16_list = [';
	$_first = true;
	$_js .= codeChr(1,4).'"--"';
	$_first = false;
	for ($s = 0; $s <= 15; $s++)
	{
		if ($_first === true){
			$_js .= codeChr(1,4).'"'.sprintf("%02u",$s).'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,4).'"'.sprintf("%02u",$s).'"';
	}
	$_js .= codeChr(1,3).'];';
	
	return($_js);
}

/**
* Funzione per generare il codice javascript per la lista dei possibili valori di device address
*/
function devicesPageBuildDeviceAddrListJavascript()
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'var device_addr_8_list = [';
	$_first = true;
	for ($s = 0; $s <= 7; $s++)
	{
		if ($_first === true){
			$_js .= codeChr(1,4).'"'.sprintf("%02u",$s).'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,4).'"'.sprintf("%02u",$s).'"';
	}
	$_js .= codeChr(1,3).'];';
	
	$_js .= codeChr(1,3).'var device_addr_16_list = [';
	$_first = true;
	for ($s = 0; $s <= 15; $s++)
	{
		if ($_first === true){
			$_js .= codeChr(1,4).'"'.sprintf("%02u",$s).'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,4).'"'.sprintf("%02u",$s).'"';
	}
	$_js .= codeChr(1,3).'];';
	
	return($_js);
}

/**
* Funzione per generare il codice javascript per la lista dei possibili valori di CTS address
*/
function devicesPageBuildCTSAddrListJavascript()
{
	$_js = codeInit();
	
	$_js .= codeChr(1,3).'var cts_addr_list = [';
	$_first = true;
	for ($s = 0; $s <= 99; $s++)
	{
		if ($_first === true){
			$_js .= codeChr(1,4).'"'.sprintf("%02u",$s).'"';
			$_first = false;
		}
		else
			$_js .= ','.codeChr(1,4).'"'.sprintf("%02u",$s).'"';
	}
	$_js .= codeChr(1,3).'];';
	
	return($_js);
}

/**
* Funzione per costruire il codice JavaScript per il contenuto
*/
function devicesPageBuildMenuJavaScript()
{
	$_js = codeInit();
	
	$_filter_iec_in_session = coreGetIeCFilterFromSession();

	//print('<!-- IeC in session: '.(int)$_filter_iec_in_session.' -->');
	
	$_filter_iec = (($_filter_iec_in_session===true)?'true':(($_filter_iec_in_session===false)?'false':((authCheckProfile('IeC_installer'))?'true':'false')));

	//print('<!-- IeC selected: '.$_filter_iec.' -->');

	$_js_iec = codeChr(1,3).'device_status_filter_iec_active = '.$_filter_iec.';';

	$_js .= jsBuildScriptOpen()
		
		.codeChr(1,3).'var selected_station = null;'
		.codeChr(1,3).'var selected_building = null;'
		.codeChr(1,3).'var selected_rack = null;'
		.codeChr(1,3).'var selected_device = null;'
		.codeChr(1,3).'var selected_item = null;'
		.codeChr(1,3).'var selected_stream = null;'
		.codeChr(1,3).'var selected_field = null;'
		.codeChr(1,3).'var selected_value = null;'
		.codeChr(1,3).'var details_view_mode = false;'
		.codeChr(1,3).'var details_edit_mode = false;'
		.codeChr(1,3).'var details_ack_mode = false;'
		.codeChr(1,3).'var fake_search = false;'
				
		// FUNZIONI GENERICHE
		
		// Attivazione menu a tendina
		.codeChr(1,3).'function navbar_filter_menuActivate(level){'
			// Nascondo le icone freccina e relativi bottoni di tutti i livelli
			.codeChr(1,4).'$(".navbar_arrow_button").hide();'
			.codeChr(1,4).'$(".navbar_arrow_button").prev().prev().prev().hide();'
			.codeChr(1,4).'$(".navbar_arrow_button").prev().prev().prev().prev().hide();'
			// Mostro il breadcrump di tutti i livelli
			.codeChr(1,4).'$(".navbar_arrow_button").prev().prev().prev().prev().prev().show();'
			// Nascondo il breadcrump del menu interessato
			.codeChr(1,4).'var i = "#navbar_filter_"+level+"_icon_bc";'
			.codeChr(1,4).'$(i).hide();'
			// Attivo le icone freccina e relativo bottone del livello interessato
			.codeChr(1,4).'var i = "#navbar_filter_"+level;'
			.codeChr(1,4).'$(i).show();'
			.codeChr(1,4).'var i = "#navbar_filter_"+level+"_icon_down";'
			.codeChr(1,4).'$(i).show();'
		.codeChr(1,3).'}'
		
		// Gestione Click Testo Menu Filtro Generica
		.codeChr(1,3).'function navbar_filter_textClick(level){'
			.codeChr(1,4).'navbar_active_level = level;'
			// Imposto anche il livello di zoom
			.codeChr(1,4).'navbar_selector_buttonClick(level);'
		.codeChr(1,3).'}'
		
		// FUNZIONI EVENTI SPECIFICI
		
		// Gestione Click Livello Zoom Stazione
		.codeChr(1,3).'function navbar_stationClick(){'
			.codeChr(1,4).'navbar_selector_buttonClick("station");'
		.codeChr(1,3).'}'
		// Gestione Click Livello Zoom Edificio
		.codeChr(1,3).'function navbar_buildingClick(){'
			.codeChr(1,4).'navbar_selector_buttonClick("building");'
		.codeChr(1,3).'}'
		// Gestione Click Livello Zoom Armadio
		.codeChr(1,3).'function navbar_rackClick(){'
			.codeChr(1,4).'navbar_selector_buttonClick("rack");'
		.codeChr(1,3).'}'
		// Gestione Click Livello Zoom Periferica
		.codeChr(1,3).'function navbar_deviceClick(){'
			.codeChr(1,4).'navbar_selector_buttonClick("device");'
		.codeChr(1,3).'}'
		
		// Gestione Click Testo Menu Filtro Stazioni
		.codeChr(1,3).'function navbar_filter_station_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("station");'
		.codeChr(1,3).'}'
		// Gestione Click Testo Menu Filtro Edifici
		.codeChr(1,3).'function navbar_filter_building_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("building");'
		.codeChr(1,3).'}'
		// Gestione Click Testo Menu Filtro Armadi
		.codeChr(1,3).'function navbar_filter_rack_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("rack");'
		.codeChr(1,3).'}'
		// Gestione Click Testo Menu Filtro Periferiche
		.codeChr(1,3).'function navbar_filter_device_textClick(){'
			.codeChr(1,4).'navbar_filter_textClick("device");'
		.codeChr(1,3).'}'
		
		// Gestione Click Testo Icona Filtro Stazioni
		.codeChr(1,3).'function navbar_filter_stationClick(){'
			//
		.codeChr(1,3).'}'
		// Gestione Click Testo Icona Filtro Edifici
		.codeChr(1,3).'function navbar_filter_buildingClick(){'
			//
		.codeChr(1,3).'}'
		// Gestione Click Testo Icona Filtro Armadi
		.codeChr(1,3).'function navbar_filter_rackClick(){'
			//
		.codeChr(1,3).'}'
		// Gestione Click Testo Icona Filtro Periferiche
		.codeChr(1,3).'function navbar_filter_deviceClick(){'
			//
		.codeChr(1,3).'}'
		
		// Gestione click bottone dettagli Chiudi
		.codeChr(1,3).'function details_button_closeClick(){'
			.codeChr(1,4).'if ($(".details_panel_icon")) $(".details_panel_icon").hide();'
			.codeChr(1,4).'detailsDisableEditMode();'
			.codeChr(1,4).'details_view_mode = false;'
			.codeChr(1,4).'details_ack_mode = false;'
			.codeChr(1,4).'selected_item = null;'
			.codeChr(1,4).'selected_station = null;'
			.codeChr(1,4).'selected_building = null;'
			.codeChr(1,4).'selected_rack = null;'
			.codeChr(1,4).'selected_device = null;'
			.codeChr(1,4).'selected_stream = null;'
			.codeChr(1,4).'selected_field = null;'
			.codeChr(1,4).'selected_value = null;'
			
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_details_panel")) $("#"+navbar_active_level+"_details_panel").find(".details_panel_form_select").autocomplete("close");'
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_details_panel")) $("#"+navbar_active_level+"_details_panel").find(".details_panel_form_search").autocomplete("close");'
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_details_panel_info")) $("#"+navbar_active_level+"_details_panel_info").empty();'
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_details_panel_info_btns")) $("#"+navbar_active_level+"_details_panel_info_btns").empty();'
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_details_panel_form")) $("#"+navbar_active_level+"_details_panel_form").empty();'
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_details_panel_form_btns")) $("#"+navbar_active_level+"_details_panel_form_btns").empty();'
			// Cancello i dettali periferica
			.codeChr(1,4).'if (navbar_active_level=="device"){'
				.codeChr(1,5).'if ($("#device_details_panel_streams")) $("#device_details_panel_streams").empty();'
				.codeChr(1,5).'if ($("#device_details_panel_fields")) $("#device_details_panel_fields").empty();'
				.codeChr(1,5).'if ($("#device_details_panel_values")) $("#device_details_panel_values").empty();'
				.codeChr(1,5).'if ($("#device_details_panel_descriptions")) $("#device_details_panel_descriptions").empty();'
			.codeChr(1,4).'}'
			// Cancello la vista armadio
			.codeChr(1,4).'if (navbar_active_level=="rack"){'
				.codeChr(1,5).'if ($("#rack_view")) $("#rack_view").empty();'
			.codeChr(1,4).'}'
			// Chiudo il pannello dettagli
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_details_panel")) $("#"+navbar_active_level+"_details_panel").hide();'
		.codeChr(1,3).'}'
		
		// Funzione per entrare nella modalità di visualizzazzione
		.codeChr(1,3).'function detailsEnableViewMode(){'
			.codeChr(1,4).'details_view_mode = true;'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel").show();'
		.codeChr(1,3).'}'
		
		// Funzione per uscire dalla modalità di visualizzazione
		.codeChr(1,3).'function detailsDisableViewMode(){'
			.codeChr(1,4).'details_button_closeClick();'
		.codeChr(1,3).'}'
		
		// Funzione per entrare nella modalità di ack
		.codeChr(1,3).'function detailsEnableAckMode(item){'
			.codeChr(1,4).'details_ack_mode = true;'
			.codeChr(1,4).'$(".icon_status_ack_panel_sub_layer").show();'
			.codeChr(1,4).'$(".icon_status_ack_panel").hide();'
			.codeChr(1,4).'$(item).next().show();'
		.codeChr(1,3).'}'
		
		// Funzione per uscire dalla modalità di ack
		.codeChr(1,3).'function detailsDisableAckMode(){'
			.codeChr(1,4).'details_ack_mode = false;'
			.codeChr(1,4).'$(".icon_status_ack_panel").hide();'
			.codeChr(1,4).'$(".icon_status_ack_panel_sub_layer").hide();'
		.codeChr(1,3).'}'
		
		// Funzione per entrare nella modalità di posizionamento rack
		.codeChr(1,3).'function detailsEnableRackMode(){'
			//.codeChr(1,4).'$(".rack_sub_layer").show();'
		.codeChr(1,3).'}'
		
		// Funzione per uscire dalla modalità di posizionamento rack
		.codeChr(1,3).'function detailsDisableRackMode(){'
			.codeChr(1,4).'$(".details_panel_form_rack").hide();'
			//.codeChr(1,4).'$(".rack_sub_layer").hide();'
		.codeChr(1,3).'}'
		
		// Funzione per entrare nella modalità di editing
		.codeChr(1,3).'function detailsEnableEditMode(){'
			.codeChr(1,4).'details_edit_mode = true;'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info_btns").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_streams").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_fields").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_values").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_descriptions").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form").fadeTo("fast",1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form_btns").fadeTo("fast",1.0);'
		.codeChr(1,3).'}'
		
		// Funzione per uscire dalla modalità di editing
		.codeChr(1,3).'function detailsDisableEditMode(){'
			.codeChr(1,4).'details_edit_mode = false;'
			.codeChr(1,4).'if (navbar_active_level == "device") $("#device_details_panel_form_rack").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_form_btns").hide();'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_info_btns").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_streams").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_fields").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_values").fadeTo(\'fast\',1.0);'
			.codeChr(1,4).'$("#"+navbar_active_level+"_details_panel_descriptions").fadeTo(\'fast\',1.0);'
		.codeChr(1,3).'}'
		
		// Gestione click bottone dettagli Chiudi
		.codeChr(1,3).'function add_button_closeClick(){'
			.codeChr(1,4).'if (navbar_active_level == "station") stationResetAddData();'
			.codeChr(1,4).'if (navbar_active_level == "building") buildingResetAddData();'
			.codeChr(1,4).'if (navbar_active_level == "rack") rackResetAddData();'
			.codeChr(1,4).'if (navbar_active_level == "device") deviceResetAddData();'
			.codeChr(1,4).'detailsDisableAddMode();'
		.codeChr(1,3).'}'
		
		// Funzione per entrare nella modalità di adding
		.codeChr(1,3).'function detailsEnableAddMode(){'
			.codeChr(1,4).'details_add_mode = true;'
			// Visualizzo il pannello per aggiungere una periferica
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_add_panel")) $("#"+navbar_active_level+"_add_panel").show();'
			.codeChr(1,4).'detailsDisableEditMode();'
			.codeChr(1,4).'detailsDisableViewMode();'
		.codeChr(1,3).'}'
		
		// Funzione per uscire dalla modalità di adding
		.codeChr(1,3).'function detailsDisableAddMode(){'
			.codeChr(1,4).'details_add_mode = false;'
			.codeChr(1,4).'if (navbar_active_level == "device") $("#device_add_panel_form_rack").hide();'
			// Mostro il pannello per aggiungere una periferica
			.codeChr(1,4).'if ($("#"+navbar_active_level+"_add_panel")) $("#"+navbar_active_level+"_add_panel").hide();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Add
		.codeChr(1,3).'function topbar_button_addClick(){'
			// Rendo visibile la sezione del contenuto per aggiungere una periferica
			.codeChr(1,4).'$("#content_add").show();'
			// Abilito la modalita' per aggiungere una periferica
			.codeChr(1,4).'detailsEnableAddMode();'
		.codeChr(1,3).'}'
		
		// Gestione Click Filtri Stato Periferiche
		.codeChr(1,3).'function device_status_filterClick(){'
		.codeChr(1,3).'}'
		
		// FUNZIONI PER INVIO DATI AJAX
		
			
		// Funzione per inviare i dati per modificare una stazione
		.codeChr(1,3).'function stationSendEditData(){'
			.codeChr(1,4)."station_config_data = 'action=edit&name='+$('#station_details_panel_form_name_search').attr('value')+'&type='+$('#station_details_panel_form_type_select').attr('value')+'&id='+selected_item"
			.codeChr(1,4).'ajaxStationConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per eliminare una stazione
		.codeChr(1,3).'function stationSendDeleteData(){'
			.codeChr(1,4)."station_config_data = 'action=delete&id='+selected_item"
			.codeChr(1,4).'ajaxStationConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per aggiungere una stazione
		.codeChr(1,3).'function stationSendAddData(){'
			.codeChr(1,4)."station_config_data = 'action=add&name='+$('#station_add_panel_form_name_search').attr('value')+'&type='+$('#station_add_panel_form_type_select').attr('value')+'&offset='+$('#station_add_panel_form_offset_input').attr('value')"
			.codeChr(1,4).'ajaxStationConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per azzerare il form per aggiungere una stazione
		.codeChr(1,3).'function stationResetAddData(){'
			.codeChr(1,4).'$("#station_add_panel_form_name_search").attr("value","");'
			.codeChr(1,4).'$("#station_add_panel_form_type_select").attr("value","Stazione");'
			.codeChr(1,4).'$("#station_add_panel_form_offset_input").attr("value","0");'
			.codeChr(1,4).'$("#station_add_panel_form_name_search").autocomplete("close");'
			.codeChr(1,4).'$("#station_add_panel_form_type_select").autocomplete("close");'
		.codeChr(1,3).'}'
		
		
		// Funzione per inviare i dati per modificare un edificio
		.codeChr(1,3).'function buildingSendEditData(){'
			.codeChr(1,4)."building_config_data = 'action=edit'"
				."+'&name='+formChrChk($('#building_details_panel_form_name_input').attr('value'))"
				."+'&station='+$('#building_details_panel_form_station_select').attr('value')"
				."+'&note='+formChrChk($('#building_details_panel_form_note_input').attr('value'))"
				."+'&type='+$('#building_details_panel_form_type_select').attr('value')"
				."+'&id='+selected_item"
			.codeChr(1,4).'ajaxBuildingConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per eliminare un edificio
		.codeChr(1,3).'function buildingSendDeleteData(){'
			.codeChr(1,4)."building_config_data = 'action=delete&id='+selected_item"
			.codeChr(1,4).'ajaxBuildingConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per aggiungere un edificio
		.codeChr(1,3).'function buildingSendAddData(){'
			.codeChr(1,4)."building_config_data = 'action=add'"
				."+'&name='+formChrChk($('#building_add_panel_form_name_input').attr('value'))"
				."+'&station='+$('#building_add_panel_form_station_select').attr('value')"
				."+'&note='+formChrChk($('#building_add_panel_form_note_input').attr('value'))"
				."+'&type='+$('#building_add_panel_form_type_select').attr('value')"
			.codeChr(1,4).'ajaxBuildingConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per azzerare il form per aggiungere un edificio
		.codeChr(1,3).'function buildingResetAddData(){'
			.codeChr(1,4).'if ($("#building_add_panel_form_name_input")) $("#building_add_panel_form_name_input").attr("value","Fabbricato Viaggiatori");'
			.codeChr(1,4).'if ($("#building_add_panel_form_station_select")) $("#building_add_panel_form_station_select").attr("value","");'
			.codeChr(1,4).'if ($("#building_add_panel_form_note_input")) $("#building_add_panel_form_note_input").attr("value","Nessuna");'
			.codeChr(1,4).'if ($("#building_add_panel_form_type_select")) $("#building_add_panel_form_type_select").attr("value","Fabbricato Viaggiatori");'
			.codeChr(1,4).'if ($("#building_add_panel_form_station_select")) $("#building_add_panel_form_station_select").autocomplete("close");'
			.codeChr(1,4).'if ($("#building_add_panel_form_type_select")) $("#building_add_panel_form_type_select").autocomplete("close");'
		.codeChr(1,3).'}'
		
		
		// Funzione per inviare i dati per modificare un armadio
		.codeChr(1,3).'function rackSendEditData(){'
			.codeChr(1,4)."rack_config_data = 'action=edit'"
				."+'&name='+formChrChk($('#rack_details_panel_form_name_input').attr('value'))"
				."+'&building='+$('#rack_details_panel_form_building_select').attr('value')"
				."+'&note='+formChrChk($('#rack_details_panel_form_note_input').attr('value'))"
				."+'&type='+$('#rack_details_panel_form_type_select').attr('value')"
				."+'&id='+selected_item"
			.codeChr(1,4).'ajaxRackConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per eliminare un armadio
		.codeChr(1,3).'function rackSendDeleteData(){'
			.codeChr(1,4)."rack_config_data = 'action=delete&id='+selected_item"
			.codeChr(1,4).'ajaxRackConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per aggiungere un armadio
		.codeChr(1,3).'function rackSendAddData(){'
			.codeChr(1,4)."rack_config_data = 'action=add'"
				."+'&name='+formChrChk($('#rack_add_panel_form_name_input').attr('value'))"
				."+'&building='+$('#rack_add_panel_form_building_select').attr('value')"
				."+'&note='+formChrChk($('#rack_add_panel_form_note_input').attr('value'))"
				."+'&type='+$('#rack_add_panel_form_type_select').attr('value')"
			.codeChr(1,4).'ajaxRackConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per azzerare il form per aggiungere un armadio
		.codeChr(1,3).'function rackResetAddData(){'
			.codeChr(1,4).'if ($("#rack_add_panel_form_name_input")) $("#rack_add_panel_form_name_input").attr("value","");'
			.codeChr(1,4).'if ($("#rack_add_panel_form_building_select")) $("#rack_add_panel_form_building_select").attr("value","");'
			.codeChr(1,4).'if ($("#rack_add_panel_form_note_input")) $("#rack_add_panel_form_note_input").attr("value","Nessuna");'
			.codeChr(1,4).'if ($("#rack_add_panel_form_type_select")) $("#rack_add_panel_form_type_select").attr("value","Armadio ATPS 24");'
			.codeChr(1,4).'if ($("#rack_add_panel_form_building_select")) $("#rack_add_panel_form_building_select").autocomplete("close");'
			.codeChr(1,4).'if ($("#rack_add_panel_form_type_select")) $("#rack_add_panel_form_type_select").autocomplete("close");'
		.codeChr(1,3).'}'
		
		
		// Funzione per inviare i dati per costruire la vista di scelta posizione armadio in fase di aggiunta di una periferica
		.codeChr(1,3).'function deviceSendRackPositionAddData(){'
			.codeChr(1,4)."rack_position_data = 'action=add'"
				."+'&type='+$('#device_add_panel_form_type_search').attr('value')"
				."+'&location='+$('#device_add_panel_form_location_select').attr('value')"
				."+'&position='+$('#device_add_panel_form_position_input').attr('value')"
			.codeChr(1,4).'rackpanel="add";'
			.codeChr(1,4)."detailsEnableRackMode();"
			.codeChr(1,4).'ajaxRackPositionSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per costruire la vista di scelta posizione armadio in fase di modifica di una periferica
		.codeChr(1,3).'function deviceSendRackPositionEditData(){'
			.codeChr(1,4)."rack_position_data = 'action=edit'"
				."+'&id='+selected_item"
				."+'&type='+$('#device_details_panel_form_type_search').attr('value')"
				."+'&location='+$('#device_details_panel_form_location_select').attr('value')"
				."+'&position='+$('#device_details_panel_form_position_input').attr('value')"
			.codeChr(1,4).'rackpanel="details";'
			.codeChr(1,4)."detailsEnableRackMode();"
			.codeChr(1,4).'ajaxRackPositionSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per aggiungere un ack
		.codeChr(1,3).'function deviceSendAckAddData(level,id,element){'
			.codeChr(1,4)."ack_config_data = 'action=add'"
				."+'&duration='+$('#'+element).find('#icon_status_ack_add_duration_input').attr('value')"
				."+'&level='+level"
				."+'&device='+selected_item"
				.";"
			.codeChr(1,4)."if (level == 'device'){"
				.codeChr(1,5).'ajaxAckConfigSender(true);'
			.codeChr(1,4)."} else if (level == 'stream'){"
				.codeChr(1,5)."ack_config_data += '&stream='+id"
				.";"
				.codeChr(1,5).'ajaxAckConfigSender(true);'
			.codeChr(1,4)."} else if (level == 'field'){"
				.codeChr(1,5)."ack_config_data += '&stream='+selected_stream"
				."+'&field='+id"
				.";"
				.codeChr(1,5).'ajaxAckConfigSender(true);'
			.codeChr(1,4)."}"
		.codeChr(1,3).'}'
		// Funzione per inviare i dati per modificare un ack
		.codeChr(1,3).'function deviceSendAckEditData(level,id,element){'
			.codeChr(1,4)."ack_config_data = 'action=edit'"
				."+'&id='+$('#'+element).find('#icon_status_ack_edit_id_input').attr('value')"
				."+'&duration='+$('#'+element).find('#icon_status_ack_edit_duration_input').attr('value')"
				."+'&level='+level"
				."+'&device='+selected_item"
				.";"
			.codeChr(1,4)."if (level == 'device'){"
				.codeChr(1,5).'ajaxAckConfigSender(true);'
			.codeChr(1,4)."} else if (level == 'stream'){"
				.codeChr(1,5)."ack_config_data += '&stream='+id"
				.";"
				.codeChr(1,5).'ajaxAckConfigSender(true);'
			.codeChr(1,4)."} else if (level == 'field'){"
				.codeChr(1,5)."ack_config_data += '&stream='+selected_stream"
				."+'&field='+id"
				.";"
				.codeChr(1,5).'ajaxAckConfigSender(true);'
			.codeChr(1,4)."}"
		.codeChr(1,3).'}'
				
		// Funzione per inviare i dati per modificare una periferica
		.codeChr(1,3).'function deviceSendEditData(){'
			.codeChr(1,4)."device_config_data = 'action=edit'"
				."+'&name='+formChrChk($('#device_details_panel_form_name_input').attr('value'))"
				."+'&sn='+formChrChk($('#device_details_panel_form_sn_input').attr('value'))"
				."+'&md_id='+formChrChk($('#device_details_panel_form_md_id_input').attr('value'))"
				."+'&type='+formChrChk($('#device_details_panel_form_type_search').attr('value'))"
				."+'&location='+$('#device_details_panel_form_location_select').attr('value')"
				."+'&port='+$('#device_details_panel_form_port_select').attr('value')"
				."+'&position='+$('#device_details_panel_form_position_input').attr('value')"
				."+'&id='+selected_item"
				."+'&serial_size='+$('#device_details_panel_form_serial_size_select').attr('value')"
				."+'&serial_modem='+$('#device_details_panel_form_serial_modem_select').attr('value')"
				."+'&serial_addr='+$('#device_details_panel_form_serial_addr_select').attr('value')"
				."+'&cts_addr='+$('#device_details_panel_form_cts_addr_select').attr('value')"
				."+'&ip1='+$('#device_details_panel_form_ip1_input').attr('value')"
				."+'&ip2='+$('#device_details_panel_form_ip2_input').attr('value')"
				."+'&ip3='+$('#device_details_panel_form_ip3_input').attr('value')"
				."+'&ip4='+$('#device_details_panel_form_ip4_input').attr('value')"
				."+'&community='+formChrChk($('#device_details_panel_form_community_input').attr('value'))"
			.codeChr(1,4).'ajaxDeviceConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per eliminare una periferica
		.codeChr(1,3).'function deviceSendDeleteData(){'
			.codeChr(1,4)."device_config_data = 'action=delete&id='+selected_item"
			.codeChr(1,4).'ajaxDeviceConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per popolare il form per editare una periferica
		.codeChr(1,3).'function deviceSendEditFormData(){'
			.codeChr(1,4)."device_config_data = 'action=edit_form'"
				."+'&id='+selected_item"
				."+'&type='+formChrChk($('#device_details_panel_form_type_search').attr('value'))"
				."+'&serial_size='+$('#device_details_panel_form_serial_size_select').attr('value')"
			.codeChr(1,4).'ajaxDeviceConfigSender(true,false,true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per popolare il form per aggiungere una periferica
		.codeChr(1,3).'function deviceSendAddFormData(){'
			.codeChr(1,4)."device_config_data = 'action=add_form'"
				."+'&type='+formChrChk($('#device_add_panel_form_type_search').attr('value'))"
				."+'&serial_size='+$('#device_add_panel_form_serial_size_select').attr('value')"
			.codeChr(1,4).'ajaxDeviceConfigSender(true,false,true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per popolare il form per aggiungere una periferica quando si cambia size degli indirizzi seriali
		.codeChr(1,3).'function deviceSendAddFormSerialData(){'
			.codeChr(1,4)."device_config_data = 'action=add_form'"
				."+'&action_option=serial_size_change'"
				."+'&type='+formChrChk($('#device_add_panel_form_type_search').attr('value'))"
				."+'&serial_size='+$('#device_add_panel_form_serial_size_select').attr('value')"
			.codeChr(1,4).'ajaxDeviceConfigSender(true,false,true);'
		.codeChr(1,3).'}'
		
		// Funzione per inviare i dati per aggiungere una periferica
		.codeChr(1,3).'function deviceSendAddData(){'
			.codeChr(1,4)."device_config_data = 'action=add'"
				."+'&name='+formChrChk($('#device_add_panel_form_name_input').attr('value'))"
				."+'&sn='+formChrChk($('#device_add_panel_form_sn_input').attr('value'))"
				."+'&md_id='+formChrChk($('#device_add_panel_form_md_id_input').attr('value'))"
				."+'&type='+formChrChk($('#device_add_panel_form_type_search').attr('value'))"
				."+'&location='+$('#device_add_panel_form_location_select').attr('value')"
				."+'&port='+$('#device_add_panel_form_port_select').attr('value')"
				."+'&position='+$('#device_add_panel_form_position_input').attr('value')"
				."+'&serial_size='+$('#device_add_panel_form_serial_size_select').attr('value')"
				."+'&serial_modem='+$('#device_add_panel_form_serial_modem_select').attr('value')"
				."+'&serial_addr='+$('#device_add_panel_form_serial_addr_select').attr('value')"
				."+'&cts_addr='+$('#device_add_panel_form_cts_addr_select').attr('value')"
				."+'&ip1='+$('#device_add_panel_form_ip1_input').attr('value')"
				."+'&ip2='+$('#device_add_panel_form_ip2_input').attr('value')"
				."+'&ip3='+$('#device_add_panel_form_ip3_input').attr('value')"
				."+'&ip4='+$('#device_add_panel_form_ip4_input').attr('value')"
				."+'&community='+formChrChk($('#device_add_panel_form_community_input').attr('value'))"
			.codeChr(1,4).'ajaxDeviceConfigSender(true);'
		.codeChr(1,3).'}'
		
		// Funzione per azzerare il form per aggiungere una periferica
		.codeChr(1,3).'function deviceResetAddData(){'
			.codeChr(1,4).'$("#device_add_panel_form_type_search").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_type_search").autocomplete("close");'
			// Nascondo campi
			.codeChr(1,4).'$("#device_add_panel_form_name_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_sn_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_md_id_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_location_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_port_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_position_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_serial_size_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_serial_modem_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_serial_addr_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_cts_addr_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_ip1_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_ip2_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_ip3_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_ip4_element").hide();'
			.codeChr(1,4).'$("#device_add_panel_form_community_element").hide();'
			// Reset info comuni
			.codeChr(1,4).'$("#device_add_panel_form_name_input").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_sn_input").attr("value","00.00.000");'
			.codeChr(1,4).'$("#device_add_panel_form_md_id_input").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_location_select").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_location_select").autocomplete("close");'
			.codeChr(1,4).'$("#device_add_panel_form_port_select").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_port_select").autocomplete("close");'
			.codeChr(1,4).'$("#device_add_panel_form_position_input").attr("value","");'
			// Reset indirizzo seriale
			.codeChr(1,4).'$("#device_add_panel_form_serial_size_select").attr("value","16");'
			.codeChr(1,4).'$("#device_add_panel_form_serial_size_select").autocomplete("close");'
			.codeChr(1,4).'$("#device_add_panel_form_serial_modem_select").attr("value","--");'
			.codeChr(1,4).'$("#device_add_panel_form_serial_modem_select").autocomplete("close");'
			.codeChr(1,4).'$("#device_add_panel_form_serial_addr_select").attr("value","00");'
			.codeChr(1,4).'$("#device_add_panel_form_serial_addr_select").autocomplete("close");'
			.codeChr(1,4).'$("#device_add_panel_form_cts_addr_select").attr("value","00");'
			.codeChr(1,4).'$("#device_add_panel_form_cts_addr_select").autocomplete("close");'
			// Reset indirizzo IP
			.codeChr(1,4).'$("#device_add_panel_form_ip1_input").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_ip2_input").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_ip3_input").attr("value","");'
			.codeChr(1,4).'$("#device_add_panel_form_ip4_input").attr("value","");'
			// Chiudo vista armadio
			.codeChr(1,4).'$("#device_add_panel_form_rack").hide();'
		.codeChr(1,3).'}'
		
		
		// GESTIONE CLICK BOTTONI PANNELLI ADD, DETAILS VIEW E EDIT
		
		// Gestione Click bottone Station Details Edit
		.codeChr(1,3).'function station_details_panel_info_button_editClick(){'
			.codeChr(1,4).'detailsEnableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Station Details Delete
		.codeChr(1,3).'function station_details_panel_info_button_deleteClick(){'
			.codeChr(1,4).'var next=confirm(\'Sei sicuro di voler eliminare questa stazione?\');'
			.codeChr(1,4).'if (next== true) stationSendDeleteData();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Station Details Save
		.codeChr(1,3).'function station_details_panel_form_button_saveClick(){'
			.codeChr(1,4).'stationSendEditData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Station Details Cancel
		.codeChr(1,3).'function station_details_panel_form_button_cancelClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Station Add Save
		.codeChr(1,3).'function station_add_panel_form_button_saveClick(){'
			.codeChr(1,4).'stationSendAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Station Cancel Save
		.codeChr(1,3).'function station_add_panel_form_button_cancelClick(){'
			.codeChr(1,4).'stationResetAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		
		// Gestione Click bottone Building Details Edit
		.codeChr(1,3).'function building_details_panel_info_button_editClick(){'
			.codeChr(1,4).'detailsEnableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Building Details Delete
		.codeChr(1,3).'function building_details_panel_info_button_deleteClick(){'
			.codeChr(1,4).'var next=confirm(\'Sei sicuro di voler eliminare questo edificio?\');'
			.codeChr(1,4).'if (next== true) buildingSendDeleteData();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Building Details Save
		.codeChr(1,3).'function building_details_panel_form_button_saveClick(){'
			.codeChr(1,4).'buildingSendEditData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Building Details Cancel
		.codeChr(1,3).'function building_details_panel_form_button_cancelClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Building Add Save
		.codeChr(1,3).'function building_add_panel_form_button_saveClick(){'
			.codeChr(1,4).'buildingSendAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Building Cancel Save
		.codeChr(1,3).'function building_add_panel_form_button_cancelClick(){'
			.codeChr(1,4).'buildingResetAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		
		// Gestione Click bottone Rack Details Edit
		.codeChr(1,3).'function rack_details_panel_info_button_editClick(){'
			.codeChr(1,4).'detailsEnableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Rack Details Delete
		.codeChr(1,3).'function rack_details_panel_info_button_deleteClick(){'
			.codeChr(1,4).'var next=confirm(\'Sei sicuro di voler eliminare questo armadio?\');'
			.codeChr(1,4).'if (next== true) rackSendDeleteData();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Rack Details Save
		.codeChr(1,3).'function rack_details_panel_form_button_saveClick(){'
			.codeChr(1,4).'rackSendEditData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Rack Details Cancel
		.codeChr(1,3).'function rack_details_panel_form_button_cancelClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Rack Add Save
		.codeChr(1,3).'function rack_add_panel_form_button_saveClick(){'
			.codeChr(1,4).'rackSendAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Rack Add Cancel
		.codeChr(1,3).'function rack_add_panel_form_button_cancelClick(){'
			.codeChr(1,4).'rackResetAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		
		// Gestione Click bottone Device Details Edit
		.codeChr(1,3).'function device_details_panel_info_button_editClick(){'
			.codeChr(1,4).'detailsEnableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Device Details Delete
		.codeChr(1,3).'function device_details_panel_info_button_deleteClick(){'
			.codeChr(1,4).'var next=confirm(\'Sei sicuro di voler eliminare questa periferica?\');'
			.codeChr(1,4).'if (next== true) deviceSendDeleteData();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Device Details Save
		.codeChr(1,3).'function device_details_panel_form_button_saveClick(){'
			.codeChr(1,4).'deviceSendEditData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Device Details Cancel 
		.codeChr(1,3).'function device_details_panel_form_button_cancelClick(){'
			.codeChr(1,4).'detailsDisableEditMode();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Device Add Save
		.codeChr(1,3).'function device_add_panel_form_button_saveClick(){'
			.codeChr(1,4).'deviceSendAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		// Gestione Click bottone Device Add Cancel
		.codeChr(1,3).'function device_add_panel_form_button_cancelClick(){'
			.codeChr(1,4).'deviceResetAddData();'
			///.codeChr(1,4).'event.preventDefault();'
		.codeChr(1,3).'}'
		
		
		/* GESTIONE BOTTONI PANNELLI ACK */
		
		// Gestione Click bottone Ack Add
		.codeChr(1,3).'function icon_status_ack_button_addClick(level,id,element){'
			.codeChr(1,4).'deviceSendAckAddData(level,id,element);'
		.codeChr(1,3).'}'
		// Gestione Click bottone Ack Close
		.codeChr(1,3).'function icon_status_ack_button_closeClick(level,id,element){'
			.codeChr(1,4).'detailsDisableAckMode();'
		.codeChr(1,3).'}'
		// Gestione Click bottone Ack Edit
		.codeChr(1,3).'function icon_status_ack_button_editClick(level,id,element){'
			.codeChr(1,4).'$("#"+element).children(".icon_status_ack_edit").show();'
			.codeChr(1,4).'$("#"+element).children(".icon_status_ack_view").hide();'
		.codeChr(1,3).'}'
		// Gestione Click bottone Ack Ok
		.codeChr(1,3).'function icon_status_ack_button_saveClick(level,id,element){'
			.codeChr(1,4).'deviceSendAckEditData(level,id,element);'
		.codeChr(1,3).'}'
		// Gestione Click bottone Ack Cancel
		.codeChr(1,3).'function icon_status_ack_button_cancelClick(level,id,element){'
			.codeChr(1,4).'$("#"+element).children(".icon_status_ack_view").show();'
			.codeChr(1,4).'$("#"+element).children(".icon_status_ack_edit").hide();'
		.codeChr(1,3).'}'
				
		.$_js_iec
		
		.jsBuildScriptClose();
		
	return($_js);
}

/**
* Funzione per costruire il codice JavaScript per il menu
*/
function devicesPageBuildContentJavaScript()
{
	global $_conf_console_html_color_code;

	$_js = codeInit();
	
	// Codice AJAX Station Config
	$_js_station_add_send = "station_config_data";
	$_js_station_add_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'var silent = $(xml).find("silent").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'if (silent == "true"){'
				.codeChr(1,3).'var default_name = $(xml).find("default_name").attr("value");'
				.codeChr(1,3).'if (default_name != null) $("#device_add_panel_form_name_input").val(default_name);'
				.codeChr(1,3).'var enable_name = $(xml).find("enable_name").attr("value");'
				.codeChr(1,3).'if (enable_name == "true") $("#device_add_panel_form_name_element").show();'
			.codeChr(1,2).'}else{'
				.codeChr(1,3).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
				.codeChr(1,3).'edit_level = $(xml).find("edit_level").attr("value");'
				.codeChr(1,3).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
				.codeChr(1,3).'else $("#bottombar_button_apply_image_light").hide();'
				.codeChr(1,3).'stationResetAddData();'
				.codeChr(1,3).'detailsDisableEditMode();'
				.codeChr(1,3).'detailsDisableAddMode();'
				.codeChr(1,3).'detailsDisableViewMode();'
				.codeChr(1,3).'ajaxDevicesInfoSender(true);'
			.codeChr(1,2).'}'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_station_add_worker = new ajaxWorker( "StationConfig", false, 0, $_js_station_add_receive );
	
	// Codice AJAX Building Config
	$_js_building_add_send = "building_config_data";
	$_js_building_add_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'var silent = $(xml).find("silent").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
			.codeChr(1,2).'edit_level = $(xml).find("edit_level").attr("value");'
			.codeChr(1,2).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
			.codeChr(1,2).'else $("#bottombar_button_apply_image_light").hide();'
			.codeChr(1,2).'buildingResetAddData();'
			.codeChr(1,2).'detailsDisableEditMode();'
			.codeChr(1,2).'detailsDisableAddMode();'
			.codeChr(1,2).'detailsDisableViewMode();'
			.codeChr(1,2).'ajaxDevicesInfoSender(true);'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_building_add_worker = new ajaxWorker( "BuildingConfig", false, 0, $_js_building_add_receive );
	
	// Codice AJAX Rack Config
	$_js_rack_add_send = "rack_config_data";
	$_js_rack_add_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'var silent = $(xml).find("silent").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
			.codeChr(1,2).'edit_level = $(xml).find("edit_level").attr("value");'
			.codeChr(1,2).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
			.codeChr(1,2).'else $("#bottombar_button_apply_image_light").hide();'
			.codeChr(1,2).'rackResetAddData();'
			.codeChr(1,2).'detailsDisableEditMode();'
			.codeChr(1,2).'detailsDisableAddMode();'
			.codeChr(1,2).'detailsDisableViewMode();'
			.codeChr(1,2).'ajaxDevicesInfoSender(true);'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_rack_add_worker = new ajaxWorker( "RackConfig", false, 0, $_js_rack_add_receive );	
			
	// Codice AJAX Device Rack Position
	$_js_rack_position_send = "rack_position_data";
	$_js_rack_position_receive = codeInit()
		.codeChr(1,1).'$("#device_"+rackpanel+"_panel_form_rack").html(html);'
		.codeChr(1,1).'$("#device_"+rackpanel+"_panel_form_rack").fadeTo("fast",1.0);'
		.codeChr(1,1).'rackPositionLinkEvents(rackpanel);'
		;
	$_rack_position_worker = new ajaxWorker( "RackPosition", false, 0, $_js_rack_position_receive );
	
	// Codice AJAX Device ACK Config
	$_js_ack_config_send = "ack_config_data";
	$_js_ack_config_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
			.codeChr(1,1).'detailsDisableAckMode();'
			.codeChr(1,2).'ajaxDevicesDetailsSender(true);'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_ack_config_worker = new ajaxWorker( "AckConfig", false, 0, $_js_ack_config_receive );
	
	// Codice AJAX Device Config
	$_js_device_add_send = "device_config_data";
	$_js_device_add_receive = codeInit()
		.codeChr(1,1).'var result = $(xml).find("result").attr("value");'
		.codeChr(1,1).'var description = $(xml).find("description").attr("value");'
		.codeChr(1,1).'var action = $(xml).find("action").attr("value");'
		.codeChr(1,1).'var silent = $(xml).find("silent").attr("value");'
		
		.codeChr(1,1).'if (result == "success"){'
			.codeChr(1,2).'if (silent == "true"){'
				.codeChr(1,3).'if (action == "add_form"){'
					.codeChr(1,4).'var action_option = $(xml).find("action_option").attr("value");'
					
					// 
					.codeChr(1,4).'if (action_option != "serial_size_change"){'
						// Name
						.codeChr(1,5).'var default_name = $(xml).find("default_name").attr("value");'
						.codeChr(1,5).'if (default_name != null) $("#device_add_panel_form_name_input").val(default_name);'
						.codeChr(1,5).'var enable_name = $(xml).find("enable_name").attr("value");'
						.codeChr(1,5).'if (enable_name == "true") $("#device_add_panel_form_name_element").show();'
						// Location
						.codeChr(1,5).'var default_location = $(xml).find("default_location").attr("value");'
						.codeChr(1,5).'if (default_location != null) $("#device_add_panel_form_location_select").val(default_location);'
						.codeChr(1,5).'var enable_location = $(xml).find("enable_location").attr("value");'
						.codeChr(1,5).'if (enable_location == "true") $("#device_add_panel_form_location_element").show();'
						// Position
						.codeChr(1,5).'var default_position = $(xml).find("default_position").attr("value");'
						.codeChr(1,5).'if (default_position != null) $("#device_add_panel_form_position_input").val(default_position);'
						.codeChr(1,5).'var enable_position = $(xml).find("enable_position").attr("value");'
						.codeChr(1,5).'if (enable_position == "true") $("#device_add_panel_form_position_element").show();'
						// SN
						.codeChr(1,5).'var default_sn = $(xml).find("default_sn").attr("value");'
						.codeChr(1,5).'if (default_sn != null) $("#device_add_panel_form_sn_input").val(default_sn);'
						.codeChr(1,5).'var enable_sn = $(xml).find("enable_sn").attr("value");'
						.codeChr(1,5).'if (enable_sn == "true") $("#device_add_panel_form_sn_element").show();'
						// MD ID
						.codeChr(1,5).'var default_md_id = $(xml).find("default_md_id").attr("value");'
						.codeChr(1,5).'if (default_md_id != null) $("#device_add_panel_form_md_id_input").val(default_md_id);'
						.codeChr(1,5).'var enable_md_id = $(xml).find("enable_md_id").attr("value");'
						.codeChr(1,5).'if (enable_md_id == "true") $("#device_add_panel_form_md_id_element").show();'
						// Port
						.codeChr(1,5).'var bind_port = $(xml).find("bind_port").attr("value");'
						.codeChr(1,5).'if (bind_port != null) $( "#device_add_panel_form_port_select" ).autocomplete( "option", "source", bind_port );'
						.codeChr(1,5).'var default_port = $(xml).find("default_port").attr("value");'
						.codeChr(1,5).'if (default_port != null) $("#device_add_panel_form_port_select").val(default_port);'
						.codeChr(1,5).'var enable_port = $(xml).find("enable_port").attr("value");'
						.codeChr(1,5).'if (enable_port == "true") $("#device_add_panel_form_port_element").show();'
								
						// Serial Size
						.codeChr(1,5).'var default_serial_size = $(xml).find("default_serial_size").attr("value");'
						.codeChr(1,5).'if (default_serial_size != null) $("#device_add_panel_form_serial_size_select").val(default_serial_size);'
						.codeChr(1,5).'var enable_serial_size = $(xml).find("enable_serial_size").attr("value");'
						.codeChr(1,5).'if (enable_serial_size == "true") $("#device_add_panel_form_serial_size_element").show();'
						.codeChr(1,5).'else $("#device_add_panel_form_serial_size_element").hide();'
						.codeChr(1,5).'}'
					
						// Serial Modem
						.codeChr(1,5).'var bind_serial_modem = $(xml).find("bind_serial_modem").attr("value");'
						.codeChr(1,5).'if (bind_serial_modem != null) $( "#device_add_panel_form_serial_modem_select" ).autocomplete( "option", "source", eval(bind_serial_modem) );'
						.codeChr(1,5).'var default_serial_modem = $(xml).find("default_serial_modem").attr("value");'
						.codeChr(1,5).'if (default_serial_modem != null) $("#device_add_panel_form_serial_modem_select").val(default_serial_modem);'
						.codeChr(1,5).'var enable_serial_modem = $(xml).find("enable_serial_modem").attr("value");'
						.codeChr(1,5).'if (enable_serial_modem == "true") $("#device_add_panel_form_serial_modem_element").show();'
						.codeChr(1,5).'else $("#device_add_panel_form_serial_modem_element").hide();'
						// Serial Addr
						.codeChr(1,5).'var bind_serial_addr = $(xml).find("bind_serial_addr").attr("value");'
						.codeChr(1,5).'if (bind_serial_addr != null) $( "#device_add_panel_form_serial_addr_select" ).autocomplete( "option", "source", eval(bind_serial_addr) );'
						.codeChr(1,5).'var default_serial_addr = $(xml).find("default_serial_addr").attr("value");'
						.codeChr(1,5).'if (default_serial_addr != null) $("#device_add_panel_form_serial_addr_select").val(default_serial_addr);'
						.codeChr(1,5).'var enable_serial_addr = $(xml).find("enable_serial_addr").attr("value");'
						.codeChr(1,5).'if (enable_serial_addr == "true") $("#device_add_panel_form_serial_addr_element").show();'
						.codeChr(1,5).'else $("#device_add_panel_form_serial_addr_element").hide();'
					
					.codeChr(1,4).'if (action_option != "serial_size_change"){'
					// CTS Addr
					.codeChr(1,4).'var default_cts_addr = $(xml).find("default_cts_addr").attr("value");'
					.codeChr(1,4).'if (default_cts_addr != null) $("#device_add_panel_form_cts_addr_select").val(default_cts_addr);'
					.codeChr(1,4).'var enable_cts_addr = $(xml).find("enable_cts_addr").attr("value");'
					.codeChr(1,4).'if (enable_cts_addr == "true") $("#device_add_panel_form_cts_addr_element").show();'
					.codeChr(1,4).'else $("#device_add_panel_form_cts_addr_element").hide();'
				
					// IP
					.codeChr(1,4).'var default_ip1 = $(xml).find("default_ip1").attr("value");'
					.codeChr(1,4).'if (default_ip1 != null) $("#device_add_panel_form_ip1_input").val(default_ip1);'
					.codeChr(1,4).'var default_ip2 = $(xml).find("default_ip2").attr("value");'
					.codeChr(1,4).'if (default_ip2 != null) $("#device_add_panel_form_ip2_input").val(default_ip2);'
					.codeChr(1,4).'var default_ip3 = $(xml).find("default_ip3").attr("value");'
					.codeChr(1,4).'if (default_ip3 != null) $("#device_add_panel_form_ip3_input").val(default_ip3);'
					.codeChr(1,4).'var default_ip4 = $(xml).find("default_ip4").attr("value");'
					.codeChr(1,4).'if (default_ip4 != null) $("#device_add_panel_form_ip4_input").val(default_ip4);'
					.codeChr(1,4).'var enable_ip = $(xml).find("enable_ip").attr("value");'
					
					.codeChr(1,4).'if (enable_ip == "true"){'
						.codeChr(1,5).'$("#device_add_panel_form_ip1_element").show();'
						.codeChr(1,5).'$("#device_add_panel_form_ip2_element").show();'
						.codeChr(1,5).'$("#device_add_panel_form_ip3_element").show();'
						.codeChr(1,5).'$("#device_add_panel_form_ip4_element").show();'
					.codeChr(1,4).'}else{'
						.codeChr(1,5).'$("#device_add_panel_form_ip1_element").hide();'
						.codeChr(1,5).'$("#device_add_panel_form_ip2_element").hide();'
						.codeChr(1,5).'$("#device_add_panel_form_ip3_element").hide();'
						.codeChr(1,5).'$("#device_add_panel_form_ip4_element").hide();'
					.codeChr(1,4).'}'
				
					// SNMP Community
					.codeChr(1,4).'var default_community = $(xml).find("default_community").attr("value");'
					.codeChr(1,4).'if (default_community != null) $("#device_add_panel_form_community_input").val(default_community);'
					.codeChr(1,4).'var enable_community = $(xml).find("enable_community").attr("value");'
					.codeChr(1,4).'if (enable_community == "true") $("#device_add_panel_form_community_element").show();'
					.codeChr(1,4).'else $("#device_add_panel_form_community_element").hide();'
					
					.codeChr(1,4).'}'
				
				// EDIT FORM
				.codeChr(1,3).'} else if (action == "edit_form"){'
					// Name
					.codeChr(1,4).'var default_name = $(xml).find("default_name").attr("value");'
					.codeChr(1,4).'if (default_name != null) $("#device_details_panel_form_name_input").val(default_name);'
					.codeChr(1,4).'var enable_name = $(xml).find("enable_name").attr("value");'
					.codeChr(1,4).'if (enable_name == "true") $("#device_details_panel_form_name_element").show();'
					// Location
					.codeChr(1,4).'var default_location = $(xml).find("default_location").attr("value");'
					.codeChr(1,4).'if (default_location != null) $("#device_details_panel_form_location_select").val(default_location);'
					.codeChr(1,4).'var enable_location = $(xml).find("enable_location").attr("value");'
					.codeChr(1,4).'if (enable_location == "true") $("#device_details_panel_form_location_element").show();'
					// Position
					.codeChr(1,4).'var default_position = $(xml).find("default_position").attr("value");'
					.codeChr(1,4).'if (default_position != null) $("#device_details_panel_form_position_input").val(default_position);'
					.codeChr(1,4).'var enable_position = $(xml).find("enable_position").attr("value");'
					.codeChr(1,4).'if (enable_position == "true") $("#device_details_panel_form_position_element").show();'
					// SN
					.codeChr(1,4).'var default_sn = $(xml).find("default_sn").attr("value");'
					.codeChr(1,4).'if (default_sn != null) $("#device_details_panel_form_sn_input").val(default_sn);'
					.codeChr(1,4).'var enable_sn = $(xml).find("enable_sn").attr("value");'
					.codeChr(1,4).'if (enable_sn == "true") $("#device_details_panel_form_sn_element").show();'
					// Port
					.codeChr(1,4).'var bind_port = $(xml).find("bind_port").attr("value");'
					.codeChr(1,4).'if (bind_port != null) $( "#device_details_panel_form_port_select" ).autocomplete( "option", "source", bind_port );'
					.codeChr(1,4).'var default_port = $(xml).find("default_port").attr("value");'
					.codeChr(1,4).'if (default_port != null) $("#device_details_panel_form_port_select").val(default_port);'
					.codeChr(1,4).'var enable_port = $(xml).find("enable_port").attr("value");'
					.codeChr(1,4).'if (enable_port == "true") $("#device_details_panel_form_port_element").show();'
								
					// Serial Size
					.codeChr(1,4).'var default_serial_size = $(xml).find("default_serial_size").attr("value");'
					.codeChr(1,4).'if (default_serial_size != null) $("#device_details_panel_form_serial_size_select").val(default_serial_size);'
					.codeChr(1,4).'var enable_serial_size = $(xml).find("enable_serial_size").attr("value");'
					.codeChr(1,4).'if (enable_serial_size == "true") $("#device_details_panel_form_serial_size_element").show();'
					.codeChr(1,4).'else $("#device_details_panel_form_serial_size_element").hide();'
					// Serial Modem
					.codeChr(1,4).'var bind_serial_modem = $(xml).find("bind_serial_modem").attr("value");'
					.codeChr(1,4).'if (bind_serial_modem != null) $( "#device_details_panel_form_serial_modem_select" ).autocomplete( "option", "source", eval(bind_serial_modem) );'
					.codeChr(1,4).'var default_serial_modem = $(xml).find("default_serial_modem").attr("value");'
					.codeChr(1,4).'if (default_serial_modem != null) $("#device_details_panel_form_serial_modem_select").val(default_serial_modem);'
					.codeChr(1,4).'var enable_serial_modem = $(xml).find("enable_serial_modem").attr("value");'
					.codeChr(1,4).'if (enable_serial_modem == "true") $("#device_details_panel_form_serial_modem_element").show();'
					.codeChr(1,4).'else $("#device_details_panel_form_serial_modem_element").hide();'
					// Serial Addr
					.codeChr(1,4).'var bind_serial_addr = $(xml).find("bind_serial_addr").attr("value");'
					.codeChr(1,4).'if (bind_serial_addr != null) $( "#device_details_panel_form_serial_addr_select" ).autocomplete( "option", "source", eval(bind_serial_addr) );'
					.codeChr(1,4).'var default_serial_addr = $(xml).find("default_serial_addr").attr("value");'
					.codeChr(1,4).'if (default_serial_addr != null) $("#device_details_panel_form_serial_addr_select").val(default_serial_addr);'
					.codeChr(1,4).'var enable_serial_addr = $(xml).find("enable_serial_addr").attr("value");'
					.codeChr(1,4).'if (enable_serial_addr == "true") $("#device_details_panel_form_serial_addr_element").show();'
					.codeChr(1,4).'else $("#device_details_panel_form_serial_addr_element").hide();'
				
					// CTS Addr
					.codeChr(1,4).'var default_cts_addr = $(xml).find("default_cts_addr").attr("value");'
					.codeChr(1,4).'if (default_cts_addr != null) $("#device_details_panel_form_cts_addr_select").val(default_cts_addr);'
					.codeChr(1,4).'var enable_cts_addr = $(xml).find("enable_cts_addr").attr("value");'
					.codeChr(1,4).'if (enable_cts_addr == "true") $("#device_details_panel_form_cts_addr_element").show();'
					.codeChr(1,4).'else $("#device_details_panel_form_cts_addr_element").hide();'
				
					// IP
					.codeChr(1,4).'var default_ip1 = $(xml).find("default_ip1").attr("value");'
					.codeChr(1,4).'if (default_ip1 != null) $("#device_details_panel_form_ip1_input").val(default_ip1);'
					.codeChr(1,4).'var default_ip2 = $(xml).find("default_ip2").attr("value");'
					.codeChr(1,4).'if (default_ip2 != null) $("#device_details_panel_form_ip2_input").val(default_ip2);'
					.codeChr(1,4).'var default_ip3 = $(xml).find("default_ip3").attr("value");'
					.codeChr(1,4).'if (default_ip3 != null) $("#device_details_panel_form_ip3_input").val(default_ip3);'
					.codeChr(1,4).'var default_ip4 = $(xml).find("default_ip4").attr("value");'
					.codeChr(1,4).'if (default_ip4 != null) $("#device_details_panel_form_ip4_input").val(default_ip4);'
					.codeChr(1,4).'var enable_ip = $(xml).find("enable_ip").attr("value");'
						
					.codeChr(1,4).'if (enable_ip == "true"){'
						.codeChr(1,5).'$("#device_details_panel_form_ip1_element").show();'
						.codeChr(1,5).'$("#device_details_panel_form_ip2_element").show();'
						.codeChr(1,5).'$("#device_details_panel_form_ip3_element").show();'
						.codeChr(1,5).'$("#device_details_panel_form_ip4_element").show();'
					.codeChr(1,4).'}else{'
						.codeChr(1,5).'$("#device_details_panel_form_ip1_element").hide();'
						.codeChr(1,5).'$("#device_details_panel_form_ip2_element").hide();'
						.codeChr(1,5).'$("#device_details_panel_form_ip3_element").hide();'
						.codeChr(1,5).'$("#device_details_panel_form_ip4_element").hide();'
					.codeChr(1,4).'}'
				
					// SNMP Community
					.codeChr(1,4).'var default_community = $(xml).find("default_community").attr("value");'
					.codeChr(1,4).'if (default_community != null) $("#device_details_panel_form_community_input").val(default_community);'
					.codeChr(1,4).'var enable_community = $(xml).find("enable_community").attr("value");'
					.codeChr(1,4).'if (enable_community == "true") $("#device_details_panel_form_community_element").show();'
					.codeChr(1,4).'else $("#device_details_panel_form_community_element").hide();'
				.codeChr(1,3).'}'
			.codeChr(1,2).'}else{'
				.codeChr(1,3).'edit_level = $(xml).find("edit_level").attr("value");'
				.codeChr(1,3).'if (edit_level == "1" || edit_level == "2") $("#bottombar_button_apply_image_light").show();'
				.codeChr(1,3).'else $("#bottombar_button_apply_image_light").hide();'
				.codeChr(1,3).'showMessage("topbar",description,"'.$_conf_console_html_color_code["green"].'",true);'
				.codeChr(1,3).'deviceResetAddData();'
				.codeChr(1,3).'detailsDisableEditMode();'
				.codeChr(1,3).'detailsDisableAddMode();'
				.codeChr(1,3).'detailsDisableViewMode();'
				.codeChr(1,3).'ajaxDevicesInfoSender(true);'
			.codeChr(1,2).'}'
		.codeChr(1,1).'}else{'
			.codeChr(1,2).'showMessage("topbar",description,"'.$_conf_console_html_color_code["red"].'",true);'
		.codeChr(1,1).'}'
		;
	$_device_add_worker = new ajaxWorker( "DeviceConfig", false, 0, $_js_device_add_receive );
	
	$_js .= jsBuildScriptOpen()
		
		.$_station_add_worker->BuildJavaScript("xml",$_js_station_add_send,true)
		.$_building_add_worker->BuildJavaScript("xml",$_js_building_add_send,true)
		.$_rack_add_worker->BuildJavaScript("xml",$_js_rack_add_send,true)
		.$_rack_position_worker->BuildJavaScript("html",$_js_rack_position_send,false)
		.$_ack_config_worker->BuildJavaScript("xml",$_js_ack_config_send,true)
		.$_device_add_worker->BuildJavaScript("xml",$_js_device_add_send,true)
		
		.codeChr(1,0).'var mmDeltaY = 0;'
		
		// Funzione per gestire gli eventi di mousewheel per il contenuto pagina
		.codeFunctionComment("Funzione per linkare gli ascoltatori degli scroller",0)
		.codeChr(1,0).'function pageContentLinkMouseWheelEvents() {'
		
			// Gestione SCROLL Contenuto INFO Device
			.codeChr(1,1).'$("#content_middle").mousewheel(function(event, delta, deltaX, deltaY) {'
				.codeChr(1,2).'var delta_px = (deltaY!=null)?Math.ceil(deltaY*30):(mmDeltaY*30);'
				
				// Topbar
				.codeChr(1,2).'var topbar_height = $("#topbar").height();'
				// Bottombar
				.codeChr(1,2).'var bottombar_top = $("#bottombar").position().top;'
				.codeChr(1,2).'var bottombar_height = $("#bottombar").height();'
				// Content display				
				.codeChr(1,2).'var content_display_height = bottombar_top-topbar_height;'
				// Posizione e altezza contenuto
				.codeChr(1,2).'var content_top = $(this).position().top-10;'
				.codeChr(1,2).'var content_height = $(this).height();'
				.codeChr(1,2).'var content_bottom = (content_height-content_display_height)+content_top;'
				
				.codeChr(1,2).'if (delta_px < 0 && content_bottom > 0  && details_view_mode == false){'
					.codeChr(1,3).'if ((-delta_px) > content_bottom) delta_px = -content_bottom;'
					.codeChr(1,3).'$(this).animate({'
						.codeChr(1,4).'top: "+=" + delta_px'
					.codeChr(1,3).'}, 0);'
				.codeChr(1,2).'}'
				.codeChr(1,2).'else if (delta_px > 0 && content_top < 0 && details_view_mode == false){'
					.codeChr(1,3).'if (delta_px > (-content_top)) delta_px = -content_top;'
					.codeChr(1,3).'$(this).animate({'
						.codeChr(1,4).'top: "+=" +delta_px'
					.codeChr(1,3).'}, 0);'
				.codeChr(1,2).'}'
			.codeChr(1,1).'});'
			
			.codeChr(1,1).'$("#content_middle").mousemove(function(e){'
				// Topbar
				.codeChr(1,2).'var topbar_height = $("#topbar").height();'
				// Bottombar
				.codeChr(1,2).'var bottombar_top = $("#bottombar").position().top;'
				.codeChr(1,2).'var bottombar_height = $("#bottombar").height();'
				// Content display				
				.codeChr(1,2).'var content_display_height = bottombar_top-topbar_height;'
				// Posizione e altezza contenuto
				.codeChr(1,2).'var content_top = $(this).position().top-10;'
				.codeChr(1,2).'var content_height = $(this).height();'
				.codeChr(1,2).'var content_bottom = (content_height-content_display_height)+content_top;'
			
				.codeChr(1,2).'if (e.pageY < (topbar_height+15) && details_view_mode == false){'
					.codeChr(1,3).'mmDeltaY = 1.2;'
					.codeChr(1,3).'$(this).mousewheel();'
				.codeChr(1,2).'}else if (e.pageY > (bottombar_top-15) && details_view_mode == false){'
					.codeChr(1,3).'mmDeltaY = -1.2;'
					.codeChr(1,3).'$(this).mousewheel();'
				.codeChr(1,2).'}'
      		.codeChr(1,1).'});'
      	.codeChr(1,0).'}'
		
		// Funzione per gestire gli eventi di mousewheel per il pannello aggiungi
		.codeFunctionComment("Funzione per linkare gli ascoltatori degli scroller",0)
		.codeChr(1,0).'function addPanelLinkMouseWheelEvents() {'
			// Gestione SCROLL Contenuto Add
			.codeChr(1,1).'$("#device_add_panel").mousewheel(function(event, delta, deltaX, deltaY) {'
				.codeChr(1,2).'var delta_px = (deltaY!=null)?Math.ceil(deltaY*30):(mmDeltaY*30);'
				// Topbar
				.codeChr(1,2).'var topbar_height = $("#topbar").height();'
				// Bottombar
				.codeChr(1,2).'var bottombar_top = $("#bottombar").position().top;'
				.codeChr(1,2).'var bottombar_height = $("#bottombar").height();'
				// Content display				
				.codeChr(1,2).'var content_display_height = bottombar_top-topbar_height;'
				// Posizione e altezza contenuto
				.codeChr(1,2).'var content_top = $(this).position().top-10;'
				.codeChr(1,2).'var content_height = $(this).height()+30;'
				.codeChr(1,2).'var rack_height = $(this).find("#rack_position").height()+50;'
				.codeChr(1,2).'if (rack_height>content_height) content_height = rack_height+50;'
				.codeChr(1,2).'var content_bottom = (content_height-content_display_height)+content_top;'
				
				.codeChr(1,2).'if (delta_px < 0 && content_bottom > 0  && details_view_mode == false){'
					.codeChr(1,3).'if ((-delta_px) > content_bottom) delta_px = -content_bottom;'
					.codeChr(1,3).'$(this).animate({'
						.codeChr(1,4).'top: "+=" + delta_px'
					.codeChr(1,3).'}, 0);'
				.codeChr(1,2).'}'
				.codeChr(1,2).'else if (delta_px > 0 && content_top < 0 && details_view_mode == false){'
					.codeChr(1,3).'if (delta_px > (-content_top)) delta_px = -content_top;'
					.codeChr(1,3).'$(this).animate({'
						.codeChr(1,4).'top: "+=" +delta_px'
					.codeChr(1,3).'}, 0);'
				.codeChr(1,2).'}'
			.codeChr(1,1).'});'
			.codeChr(1,1).'$("#device_add_panel").mousemove(function(e){'
				// Topbar
				.codeChr(1,2).'var topbar_height = $("#topbar").height();'
				// Bottombar
				.codeChr(1,2).'var bottombar_top = $("#bottombar").position().top;'
				.codeChr(1,2).'var bottombar_height = $("#bottombar").height();'
				// Content display				
				.codeChr(1,2).'var content_display_height = bottombar_top-topbar_height;'
				// Posizione e altezza contenuto
				.codeChr(1,2).'var content_top = $(this).position().top-10;'
				.codeChr(1,2).'var content_height = $(this).height()+30;'
				.codeChr(1,2).'var rack_height = $(this).find("#rack_position").height()+50;'
				.codeChr(1,2).'if (rack_height>content_height) content_height = rack_height+50;'
				.codeChr(1,2).'var content_bottom = (content_height-content_display_height)+content_top;'
			
				.codeChr(1,2).'if (e.pageY < (topbar_height+15) && details_view_mode == false){'
					.codeChr(1,3).'mmDeltaY = 1.2;'
					.codeChr(1,3).'$(this).mousewheel();'
				.codeChr(1,2).'}else if (e.pageY > (bottombar_top-15) && details_view_mode == false){'
					.codeChr(1,3).'mmDeltaY = -1.2;'
					.codeChr(1,3).'$(this).mousewheel();'
				.codeChr(1,2).'}'
      		.codeChr(1,1).'});'
		.codeChr(1,0).'}'
			
		// Funzione per gestire gli eventi di mousewheel per il contenuto pagina
		.codeFunctionComment("Funzione per linkare gli ascoltatori degli scroller",0)
		.codeChr(1,0).'function detailsPanelLinkMouseWheelEvents( page, level ) {'
			// Gestione SCROLL Contenuto Details Device
			.codeChr(1,1).'$("#"+level+"_details_panel").mousewheel(function(event, delta, deltaX, deltaY) {'
				.codeChr(1,2).'var delta_px = (deltaY!=null)?Math.ceil(deltaY*30):(mmDeltaY*30);'
				
				// Topbar
				.codeChr(1,2).'var topbar_height = $("#topbar").height();'
				// Bottombar
				.codeChr(1,2).'var bottombar_top = $("#bottombar").position().top;'
				.codeChr(1,2).'var bottombar_height = $("#bottombar").height();'
				// Content display				
				.codeChr(1,2).'var content_display_height = bottombar_top-topbar_height;'
				// Posizione e altezza contenuto
				.codeChr(1,2).'var content_top = $(this).position().top-5;'
				.codeChr(1,2).'var content_height = $(this).height()+30;'
				.codeChr(1,2).'var rack_height = $(this).find("#rack_position").height()+50;'
				.codeChr(1,2).'if (rack_height>content_height) content_height = rack_height+50;'
				.codeChr(1,2).'var content_bottom = (content_height-content_display_height)+content_top;'
				
				.codeChr(1,2).'if (delta_px < 0 && content_bottom > 0){'
					.codeChr(1,3).'if ((-delta_px) > content_bottom) delta_px = -content_bottom;'
					.codeChr(1,3).'$(this).animate({'
						.codeChr(1,4).'top: "+=" + delta_px'
					.codeChr(1,3).'}, 0);'
				.codeChr(1,2).'}'
				.codeChr(1,2).'else if (delta_px > 0 && content_top < 0){'
					.codeChr(1,3).'if (delta_px > (-content_top)) delta_px = -content_top;'
					.codeChr(1,3).'$(this).animate({'
						.codeChr(1,4).'top: "+=" +delta_px'
					.codeChr(1,3).'}, 0);'
				.codeChr(1,2).'}'
				
			.codeChr(1,1).'});'
			
			.codeChr(1,1).'$("#"+level+"_details_panel").mousemove(function(e){'
				// Topbar
				.codeChr(1,2).'var topbar_height = $("#topbar").height();'
				// Bottombar
				.codeChr(1,2).'var bottombar_top = $("#bottombar").position().top;'
				.codeChr(1,2).'var bottombar_height = $("#bottombar").height();'
				// Content display				
				.codeChr(1,2).'var content_display_height = bottombar_top-topbar_height;'
				// Posizione e altezza contenuto
				.codeChr(1,2).'var content_top = $(this).position().top-5;'
				.codeChr(1,2).'var content_height = $(this).height()+30;'
				.codeChr(1,2).'var rack_height = $(this).find("#rack_position").height()+50;'
				.codeChr(1,2).'if (rack_height>content_height) content_height = rack_height+50;'
				.codeChr(1,2).'var content_bottom = (content_height-content_display_height)+content_top;'
			
				.codeChr(1,2).'if (e.pageY < (topbar_height+15)){'
					.codeChr(1,3).'mmDeltaY = 1.2;'
					.codeChr(1,3).'$(this).mousewheel();'
				.codeChr(1,2).'}else if (e.pageY > (bottombar_top-15)){'
					.codeChr(1,3).'mmDeltaY = -1.2;'
					.codeChr(1,3).'$(this).mousewheel();'
				.codeChr(1,2).'}'
      		.codeChr(1,1).'});'
		.codeChr(1,0).'}'
		
		// Javascript Liste
		.devicesPageBuildStationTypeListJavascript()
		.devicesPageBuildModemSizeListJavascript()
		.devicesPageBuildModemAddrListJavascript()
		.devicesPageBuildDeviceAddrListJavascript()
		.devicesPageBuildCTSAddrListJavascript()
		
		.jsPanelButtonEventManager("add_panel")
				
		.codeChr(1,3).'var rackpanel=null;'
		.codeChr(1,3).'function rackPositionLinkEvents(rackpanel) {'
			.codeChr(1,4).'$(".rack").mousedown(function(event) {'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
			.codeChr(1,4).'$(".rack").click(function(event) {'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
			.codeChr(1,4).'$(".rack_slot_selectable").mousedown(function(event) {'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
			.codeChr(1,4).'$(".rack_slot_selectable").click(function(event) {'
				.codeChr(1,5).'var position = $(this).attr("id");'
				.codeChr(1,5).'position = position.replace("rack_slot_", "");'
				.codeChr(1,5).'position = position.replace("_", ",");'
				.codeChr(1,5).'$("#device_"+rackpanel+"_panel_form_position_input").attr("value",position);'
				.codeChr(1,5).'$(this).parent().parent().parent().hide();'
				.codeChr(1,5).'detailsDisableRackMode();'
				.codeChr(1,5).'var offset = (parseFloat($("#device_details_panel").css("top")))-10;'
				.codeChr(1,5).'$("#device_details_panel").animate({'
					.codeChr(1,4).'top: "-=" +offset'
				.codeChr(1,5).'}, 0);'
				.codeChr(1,5).'var offset = (parseFloat($("#device_add_panel").css("top")))-10;'
				.codeChr(1,5).'$("#device_add_panel").animate({'
					.codeChr(1,4).'top: "-=" +offset'
				.codeChr(1,5).'}, 0);'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
			.codeChr(1,4).'$(".rack_sub_layer")'
				.codeChr(1,5).'.mousedown(function(event) {'
					.codeChr(1,6).jsEventPreventDefault()
				.codeChr(1,5).'})'
				.codeChr(1,5).'.click(function(event) {'
					.codeChr(1,6).'detailsDisableRackMode();'
					.codeChr(1,6).jsEventPreventDefault()
				.codeChr(1,5).'})'
			.codeChr(1,4).';'
		.'}'
		;
		
	// Link Eventi Contenuto Pagina
	$_js .= 
		 codeFunctionComment("Funzione per impostare gli ascoltatori eventi per il contenuto pagina", 0)
		.codeChr(1,0).'function pageLinkEvents() {'
			// Eventi rotellina del mouse
			.codeChr(1,1).'pageContentLinkMouseWheelEvents();'
			
			
			.codeChr(1,4).'$(".item_icon").mouseenter(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseover(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseleave(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			.codeChr(1,4).'}).mouseout(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			.codeChr(1,4).'});'
			
			.codeChr(1,4).'$(".item_icon_status_up").mouseenter(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseover(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","visible");'
				.codeChr(1,5).'$(this).parent().css("z-index","1200");'
				.codeChr(1,5).'$(this).css("z-index","1300");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","hidden");'
			.codeChr(1,4).'}).mouseleave(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			.codeChr(1,4).'}).mouseout(function() {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
			// Gestione Click Icona 50x50 Item
			.codeChr(1,4).'}).click(function(event) {'
				.codeChr(1,5).'$(this).parent().css("visibility","hidden");'
				.codeChr(1,5).'$(this).parent().css("z-index","200");'
				.codeChr(1,5).'$(this).css("z-index","100");'
				.codeChr(1,5).'$(this).parent().find(".item_name").css("visibility","visible");'
				//.codeChr(1,5).'$("#details_panel").show();'
				.codeChr(1,5).'if (navbar_active_level == "station"){'
					.codeChr(1,6).'var selected_item_str = $(this).attr("id");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("item_panel_icon_", "");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("_status_up", "");'
					.codeChr(1,6).'selected_item = selected_item_str;'
					.codeChr(1,6).'detailsEnableViewMode();'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true,true);'
				.codeChr(1,5).'} else'
				.codeChr(1,5).'if (navbar_active_level == "building"){'
					.codeChr(1,6).'var selected_item_str = $(this).attr("id");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("item_panel_icon_", "");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("_status_up", "");'
					.codeChr(1,6).'selected_item = selected_item_str;'
					.codeChr(1,6).'detailsEnableViewMode();'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true,true);'
				.codeChr(1,5).'} else'
				.codeChr(1,5).'if (navbar_active_level == "rack"){'
					.codeChr(1,6).'var selected_item_str = $(this).attr("id");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("item_panel_icon_", "");'
					.codeChr(1,6).'selected_item_str = selected_item_str.replace("_status_up", "");'
					.codeChr(1,6).'selected_item = selected_item_str;'
					.codeChr(1,6).'detailsEnableViewMode();'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true,true);'
				.codeChr(1,5).'} else'
				.codeChr(1,5).'if (navbar_active_level == "device"){'
					.codeChr(1,6).'var selected_device_str = $(this).attr("id");'
					.codeChr(1,6).'selected_device_str = selected_device_str.replace("item_panel_icon_", "");'
					.codeChr(1,6).'selected_device_str = selected_device_str.replace("_status_up", "");'
					.codeChr(1,6).'selected_item = selected_device_str;'
					.codeChr(1,6).'detailsEnableViewMode();'
					.codeChr(1,6).'ajaxDevicesDetailsSender(true,true);'
				.codeChr(1,5).'}'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
			
			// Eventi click posizione armadio form aggiungi periferica
			.codeChr(1,4).'$("#device_add_panel_form_position_input").mousedown(function(event) {'
				.codeChr(1,5).'deviceSendRackPositionAddData();'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
			.codeChr(1,4).'$("#device_add_panel_form_rack").mousedown(function(event) {'
				.codeChr(1,5).jsEventPreventDefault()
			.codeChr(1,4).'});'
		
		.codeChr(1,0).'}'
		;
		
	// Link Eventi Pannello Dettagli
	$_js .=
		 codeFunctionComment("Funzione per impostare gli ascoltatori eventi per il pannello dettagli", 0)
		.codeChr(1,0).'function detailsPanelLinkEvents() {'
			// Script jQuery per la gestione degli eventi dei bottoni del pannello dettagli
			.jsPanelButtonEventManager("details_panel")
			// Script jQuery per la gestione degli eventi di selezione stream, field e value delle periferiche
			.jsDeviceDataSelectionEventManager(null,null)
			// Script jQuery per la gestione degli eventi di ack sui pallini di stato
			.jsIconStatusAckEventManager(null,"IaP_installer,IeC_installer,installer,admin")
			.codeChr(1,1).'$("#device_details_panel_form_position_input").mousedown(function(event) {'
				.codeChr(1,2).'deviceSendRackPositionEditData();'
				.codeChr(1,2).jsEventPreventDefault()
			.codeChr(1,1).'});'
			.codeChr(1,1).'$("#device_details_panel_form_rack").mousedown(function(event) {'
				.codeChr(1,2).jsEventPreventDefault()
			.codeChr(1,1).'});'
			.'$( "#device_details_panel_form_ip1_input" ).keydown(function(event) {
			if ( event.which == 190 || event.which == 110 ) {
				event.preventDefault();
				$("#device_details_panel_form_ip2_input").focus();
			}
			
		});
		$( "#device_details_panel_form_ip2_input" ).keydown(function(event) {
			if ( event.which == 190 || event.which == 110 ) {
				event.preventDefault();
				$("#device_details_panel_form_ip3_input").focus();
			}
			
		});
		$( "#device_details_panel_form_ip3_input" ).keydown(function(event) {
			if ( event.which == 190 || event.which == 110 ) {
				event.preventDefault();
				$("#device_details_panel_form_ip4_input").focus();
			}
			
		});'
			// Javascript gestione form Edit Stazioni
			.jsPanelFormSearch("station_details_panel","name","'json/sk_getregionlist.json.php'")
			.jsPanelFormSelect("station_details_panel","type","station_type_list",true)
				
			// Javascript gestione form Edit Edifici
			.jsPanelFormSelect("building_details_panel","station","'json/sk_getstations.json.php'",true)
			.jsPanelFormSelect("building_details_panel","type","'json/sk_getbuildingtypes.json.php'",true)
				
			// Javascript gestione form Edit Armadi
			.jsPanelFormSelect("rack_details_panel","building","'json/sk_getbuildings.json.php'",true)
			.jsPanelFormSelect("rack_details_panel","type","'json/sk_getracktypes.json.php'",true)
				
			// Javascript gestione form Edit Periferiche
			.jsPanelFormSearch("device_details_panel","type","'json/sk_getdevicetypes.json.php'","deviceSendEditFormData();")
			.jsPanelFormSelect("device_details_panel","location","'json/sk_getracks.json.php'",true)
			.jsPanelFormSelect("device_details_panel","port","'json/sk_getports.json.php'",true)
			.jsPanelFormSelect("device_details_panel","serial_size","modem_size_list",true,"deviceSendEditFormData();")
			.jsPanelFormSelect("device_details_panel","serial_modem","modem_addr_16_list",true)
			.jsPanelFormSelect("device_details_panel","serial_addr","device_addr_16_list",true)
			.jsPanelFormSelect("device_details_panel","cts_addr","cts_addr_list",true)
			
			.codeChr(1,1).'detailsPanelLinkMouseWheelEvents("devices",navbar_active_level);'
			
		.codeChr(1,0).'}'
		;
		
	$_message_params = coreGetMessageFromSession();
	if (isset($_message_params)){
		$_js_message = '
showMessage("'.$_message_params['bar'].'","'.$_message_params['message'].'","'.$_conf_console_html_color_code[$_message_params['color']].'",false);';
		
	}
	else{
		$_js_message = '';
	}
	$_js .=
		 jsBarTextEventManager()
	
		// Javascript Add Stazioni
		.jsPanelFormSearch("station_add_panel","name","'json/sk_getregionlist.json.php'")
		.jsPanelFormSelect("station_add_panel","type","station_type_list",true)
		
		// Javascript Add Edificio
		.jsPanelFormSelect("building_add_panel","station","'json/sk_getstations.json.php'",true)
		.jsPanelFormSelect("building_add_panel","type","'json/sk_getbuildingtypes.json.php'",true)
		
		// Javascript Add Armadio
		.jsPanelFormSelect("rack_add_panel","building","'json/sk_getbuildings.json.php'",true)
		.jsPanelFormSelect("rack_add_panel","type","'json/sk_getracktypes.json.php'",true)
		
		// Javascript gestione form Add Periferiche
		.jsPanelFormSearch("device_add_panel","type","'json/sk_getdevicetypes.json.php'","deviceSendAddFormData();")
		.jsPanelFormSelect("device_add_panel","location","'json/sk_getracks.json.php'",true)
		.jsPanelFormSelect("device_add_panel","port","'json/sk_getports.json.php'",true)
		.jsPanelFormSelect("device_add_panel","serial_size","modem_size_list",true,"deviceSendAddFormSerialData();")
		.jsPanelFormSelect("device_add_panel","serial_modem","modem_addr_16_list",true)
		.jsPanelFormSelect("device_add_panel","serial_addr","device_addr_16_list",true)
		.jsPanelFormSelect("device_add_panel","cts_addr","cts_addr_list",true)
				
		//.jsPageButtonEventManager()
		.jsBarSelectorButtonEventManager()
		
		.codeChr(1,0).'var device_status_filter_offline_active = true;'
		.codeChr(1,0).'var device_status_filter_error_active = true;'
		.codeChr(1,0).'var device_status_filter_warning_active = true;'
		.codeChr(1,0).'var device_status_filter_ok_active = true;'
		.codeChr(1,0).'var device_status_filter_unknown_active = true;'
		.codeChr(1,0).'var device_status_filter_iec_active = true;'
		
		.jsBarStatusFilterEventManager()
		
		.'$( "#device_add_panel_form_ip1_input" ).keydown(function(event) {
			if ( event.which == 190 || event.which == 110 ) {
				event.preventDefault();
				$("#device_add_panel_form_ip2_input").focus();
			}
			
		});
		$( "#device_add_panel_form_ip2_input" ).keydown(function(event) {
			if ( event.which == 190 || event.which == 110 ) {
				event.preventDefault();
				$("#device_add_panel_form_ip3_input").focus();
			}
			
		});
		$( "#device_add_panel_form_ip3_input" ).keydown(function(event) {
			if ( event.which == 190 || event.which == 110 ) {
				event.preventDefault();
				$("#device_add_panel_form_ip4_input").focus();
			}
			
		});'
		//.codeChr(1,3).'pageLinkEvents();'
											
		.$_js_message
													
		.jsBuildScriptClose();
	
	return($_js);
}

/**
* Funzione per generare la lista degli slot di un armadio
*/
function devicesRackSlotsBuild($devices,$rack,$rackType)
{
	$_slots = null;
	
	// Ciclo sulle periferiche
	foreach ($devices as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($rack->stationId == $_device->stationId 
			&& $rack->buildingId == $_device->buildingId
			&& $rack->id == $_device->locationId)
			{
				if (empty($_slots)) $_slots = array();
				if (isset($_device->position)) 
					$_slots[$_device->position] = $_device;
			}
		}	
	}
	
	return($_slots);
}

/**
* Funzione per costruire il codice HTML per la vista armadio
*/
function devicesRackBuild($id,$class,$left,$top,$rackType,$slots,$selectable=false,$selectedPosition=null,$selectedDevice=null,$skin="default")
{
	$_html = codeInit();
	
	$_x = $left;
	$_y = $top;
	
	$_html_content = codeInit();

	if (isset($rackType))
	{
		$_cols = $rackType->maxPositions['x'];
		$_rows = $rackType->maxPositions['y'];
		$_w = 30+($_cols*(4+(30*$rackType->slotWidth)));
		$_h = 30+($_rows*(4+(30*$rackType->slotHeight)));
		$_offset_x = ((30*$rackType->slotWidth)/2)-15;
		$_offset_y = ((30*$rackType->slotHeight)/2)-15;

		// Generazione codice HTML per gli slot degli armadi
		for ($c=1; $c<=$_cols; $c++ )
		{
			for ($r=1; $r<=$_rows; $r++ )
			{
				if (isset($slots)) 
					$_device = $slots[$c.",".$r];
				else
					$_device = null;
				
				if ($selectable === true)
				{
					if (isset($selectedPosition) && $c.",".$r == $selectedPosition)
					{
						$_selected_class = " rack_slot_selected";
						if (isset($selectedDevice))
							$_html_icon = codeChr(1,6).htmlImage("rack_slot_icon".$c."_".$r,'rack_slot_icon',"devices/".$selectedDevice->type."_30.png",$skin,"left:".($_offset_x)."px; top:".($_offset_y)."px;").codeChr(1,5);
						else
							$_html_icon = codeChr(1,6).htmlImage("rack_slot_icon".$c."_".$r,'rack_slot_icon',"devices/unknown_30.png",$skin,"left:".($_offset_x)."px; top:".($_offset_y)."px;").codeChr(1,5);
					}
					else
					{
						$_selected_class = "";
						$_html_icon = "";
					}
					
					if (isset($_device) && $_device->id !== $selectedDevice->id)
					{
						$_html_icon = codeChr(1,6).htmlImage("rack_slot_icon".$c."_".$r,'rack_slot_icon',"devices/".$_device->type."_30.png",$skin,"left:".($_offset_x)."px; top:".($_offset_y)."px;").codeChr(1,5);
						// Vecchio codice con cella non selezionabile
						//$_html_content .= codeChr(1,6).htmlDivitis("rack_slot_".$c."_".$r, "rack_slot".$_selected_class, $_html_icon, "left:".(5+2+($c-1)*(4+(30*$rackType->slotWidth)))."px; top:".(5+2+($r-1)*(4+(30*$rackType->slotHeight)))."px; width: ".(30*$rackType->slotWidth)."px; height: ".(30*$rackType->slotHeight)."px;");
						// Workaround per rendere la cella sempre selezionabile
						$_html_content .= codeChr(1,6).htmlDivitis("rack_slot_".$c."_".$r, "rack_slot_selectable".$_selected_class, $_html_icon, "left:".(5+2+($c-1)*(4+(30*$rackType->slotWidth)))."px; top:".(5+2+($r-1)*(4+(30*$rackType->slotHeight)))."px; width: ".(30*$rackType->slotWidth)."px; height: ".(30*$rackType->slotHeight)."px;");
					}
					else
					{
						$_html_content .= codeChr(1,6).htmlDivitis("rack_slot_".$c."_".$r, "rack_slot_selectable".$_selected_class, $_html_icon, "left:".(5+2+($c-1)*(4+(30*$rackType->slotWidth)))."px; top:".(5+2+($r-1)*(4+(30*$rackType->slotHeight)))."px; width: ".(30*$rackType->slotWidth)."px; height: ".(30*$rackType->slotHeight)."px;");
					}
				}
				else
				{
					if (isset($_device))
					{
						$_html_icon = codeChr(1,6).htmlImage("rack_slot_icon".$c."_".$r,'rack_slot_icon',"devices/".$_device->type."_30.png",$skin,"left:".($_offset_x)."px; top:".($_offset_y)."px;").codeChr(1,5);
						$_html_content .= codeChr(1,6).htmlDivitis("rack_slot_".$c."_".$r, "rack_slot", $_html_icon, "left:".(5+2+($c-1)*(4+(30*$rackType->slotWidth)))."px; top:".(5+2+($r-1)*(4+(30*$rackType->slotHeight)))."px; width: ".(30*$rackType->slotWidth)."px; height: ".(30*$rackType->slotHeight)."px;");
					}
					else
					{
						$_html_icon = "";
						$_html_content .= codeChr(1,6).htmlDivitis("rack_slot_".$c."_".$r, "rack_slot", $_html_icon, "left:".(5+2+($c-1)*(4+(30*$rackType->slotWidth)))."px; top:".(5+2+($r-1)*(4+(30*$rackType->slotHeight)))."px; width: ".(30*$rackType->slotWidth)."px; height: ".(30*$rackType->slotHeight)."px;");
					}
				}
				
				// Etichetta colonne
				if ($c==1) $_html_content .= codeChr(1,6).htmlParagraph(null, "rack_slot_number", $r, "left: -18px; top: ".(5+2+($r-1)*(4+(30*$rackType->slotHeight))+$_offset_y+10)."px;");
			}
			// Etichetta colonne
			$_html_content .= codeChr(1,6).htmlParagraph(null, "rack_slot_number", $c, "left:".(5+2+($c-1)*(4+(30*$rackType->slotWidth))+$_offset_x)."px; top: -9px;");
		}
		$_html_content .= codeChr(1,5);
	
	// Generazione codice HTML per la cornice dell'armadio
	$_html_rack = 
		 codeChr(1,5).htmlImage('rack_top_left','rack_border','gui/sk_rack_top_left.png',$skin)
		.codeChr(1,5).htmlImage('rack_top','rack_border','gui/sk_rack_top.png',$skin,"width: ".($_w-20)."px;")
		.codeChr(1,5).htmlImage('rack_top_right','rack_border','gui/sk_rack_top_right.png',$skin)
		.codeChr(1,5).htmlImage('rack_left','rack_border','gui/sk_rack_left.png',$skin,"height: ".($_h-20)."px;")
		.codeChr(1,5).htmlDivitis("rack_content_".$id, "rack_content", $_html_content,"width: ".($_w-20)."px; height: ".($_h-20)."px;")
		.codeChr(1,5).htmlImage('rack_right','rack_border','gui/sk_rack_right.png',$skin,"height: ".($_h-20)."px;")
		.codeChr(1,5).htmlImage('rack_bottom_left','rack_border','gui/sk_rack_bottom_left.png',$skin)
		.codeChr(1,5).htmlImage('rack_bottom','rack_border','gui/sk_rack_bottom.png',$skin,"width: ".($_w-20)."px;")
		.codeChr(1,5).htmlImage('rack_bottom_right','rack_border','gui/sk_rack_bottom_right.png',$skin)
		.codeChr(1,4)
	;
		if ($selectable === true)		
			$_html .= codeChr(1,4).htmlDivitis($id."_rack_sub_layer","rack_sub_layer","",null,null);
		$_html .= codeChr(1,4).htmlDivitis("rack_".$id, "rack", $_html_rack, "left:".$_x."px; top:".$_y."px; width: ".$_w."px; height: ".$_h."px;");
	}
	
	return($_html);
}

/**
* Funzione per costruire il codice per il pannello informazioni di un item generico
*/
function devicesPageBuildItemInfoPanel($itemParams)
{
	logDebug("devicesPageBuildItemInfoPanel");
	
	// Inizializzo il codice HTML che verra' restituito
	$_html = codeInit();
	
	if (isset($itemParams))
	{
		// Recupero i singoli parametri
		$_item_params 		= $itemParams;
		$_item_category		= $_item_params['Category'];
		$_item_index 		= $_item_params['Index'];
		$_item_id			= $_item_params['DevID'];
		$_item_type			= $_item_params['Type'];
		$_item_name			= $_item_params['Name'];
		$_item_status_code	= $_item_params['StatusCode'];
		
		// Preparo la struttura per il pillolone
		$_item_pill_data = array();
		$_item_pill_data['255'] = $_item_params['f255'];
		$_item_pill_data['2'] 	= $_item_params['f2'];
		$_item_pill_data['1'] 	= $_item_params['f1'];
		$_item_pill_data['0'] 	= $_item_params['f0'];
		$_item_pill_data['-255'] = $_item_params['fm255'];
		
		// Genero il codice HTML
		$_html .= htmlPanelBuild( 	$_item_category,
									$_item_index, 
									$_item_id,
									$_item_type,
									$_item_name,
									$_item_status_code,
									$_item_pill_data);
	}
	
	return($_html);
}

/**
*
*/
function devicesStatusFilterPass($_device_status_code,$_filter_mask=0x00){
	$_pass = true;
	
	/*DEBUG
	print('status_code='.$_device_status_code.';filter_mask='.$_filter_mask);
	//*/
	
	$_test_mask = 0x00;
	
	if ($_device_status_code==254){
		$_test_mask=0x01;
	}
	else if ($_device_status_code==2){
		$_test_mask=0x02;
	}
	else if ($_device_status_code==1){
		$_test_mask=0x04;
	}
	else if ($_device_status_code==0){
		$_test_mask=0x08;
	}
	else if ($_device_status_code==255){
		$_test_mask=0x10;
	}
	
	
	$_test_result=$_filter_mask&$_test_mask;
	if ($_test_result!=$_test_mask) $_pass = false;
	
	/*DEBUG
	print(';test_mask='.$_test_mask.';test_result='.$_test_result);
	//*/
	
	return $_pass;
}

/**
* Funzione per costruire il codice HTML per il contenuto della pagina periferiche
*/
function devicesPageBuildContentHTML($data, $errorMessage=null)
{
	logDebug("devicesPageBuildContentHTML");
	
	global $_configuration;
	
	// Inizializzo il codice HTML che verra' restituito
	$_html = codeInit();
	
	if (isset($data) && is_array($data)){
		$_data = $data;
			
		// Recupero informazioni sul livello di visualizzazione
		$_level = $_data['level'];
		// Recupero informazioni sui filtri attivi
		$_filter = $_data['filter'];
		// Recupero la maschera dei filtri di stati
		$_filter_mask = $_filter['filter_mask'];
		$_filter_iec = $_filter['iec'];
			
		if (empty($_level))
		{
			$_level = devicesGetPageLevel();
		}
		
		if ($_level == "device")
		{
			// Recupero parametri di configurazione
			$_devices = $_configuration["devices"];
			// Recupero informazioni di stato (ottenute dal DB) sulle periferiche
			$_devices_data = $_data['device'];
		
			$i = 0;
			// Ciclo sulle periferiche
			foreach ( $_devices as $_device )
			{
				logDebug("Building device HTML panel #".$i);
			
				$_devid = $_device->getDevId();
					
				$_device_status_code = 255;
			
				$_device_f255 	= null;
				$_device_f2 	= null;
				$_device_f1 	= null;
				$_device_f0 	= null;
				$_device_fm255 	= null;
				
				// Ciclo sui dati di stato delle periferiche
				foreach ( $_devices_data as $_device_data )
				{
					// Recupero stato periferica
					if ($_device_data['DevID'] == $_devid)
					{
						$_device_sevlevel = $_device_data['SevLevel'];
						$_device_offline = $_device_data['Offline'];
						$_device_active = $_device_data['Active'];
						
						/* DEBUG
						print("<!-- DEVICE DATA ".$i.": ".serialize($_device_data)." -->");
						//*/
						
						$_device_status_code = coreGetStatusCode($_device_sevlevel, $_device_offline, $_device_active);
																
						$_device_f255 	= $_device_data['f255'];
						$_device_f2 	= $_device_data['f2'];
						$_device_f1 	= $_device_data['f1'];
						$_device_f0 	= $_device_data['f0'];
						$_device_fm255 	= $_device_data['fm255'];
						
						break;
					}
				}
			
				// Preparo i parametri della periferica
				$_device_params = array();
				$_device_params['Category'] = 'device';
				$_device_params['Index'] = $i;
				$_device_params['DevID'] = $_devid;
				$_device_params['Type'] = $_device->type;
				$_device_params['Name'] = $_device->name;
				$_device_params['StatusCode'] = $_device_status_code;
				$_device_params['f255'] = $_device_f255;
				$_device_params['f2'] = $_device_f2;
				$_device_params['f1'] = $_device_f1;
				$_device_params['f0'] = $_device_f0;
				$_device_params['fm255'] = $_device_fm255;
				
				/* DEBUG
				print("<!-- DEVICE PARAMS ".$i.": ".serialize($_device_params)." -->");
				//*/
				
				if (devicesStatusFilterPass($_device_status_code,$_filter_mask)==true){
					if ($_filter_iec == true){
						if ($_device->monitoringDeviceId != null && $_device->monitoringDeviceId != ''){
							$_html .= devicesPageBuildItemInfoPanel($_device_params);
							$i++;
						}
						else{
							
						}
					}
					else{
						$_html .= devicesPageBuildItemInfoPanel($_device_params);
						//$_html .= htmlPanelBuild($i,$_devid,$_device->type,$_device->name,$_device_status_code);
						$i++;
					}
				}
			}
			
			//$_content_height = 50+(80*ceil(count($_devices)/14));
			$_content_height = 50+(80*ceil($i/14));
		}
		else if ($_level == "rack")
		{
			// Recupero parametri di configurazione armadi
			$_items = $_configuration["locations"];
			// Recupero informazioni di stato (ottenute dal DB) sulle periferiche
			$_devices_data = $_data['device'];
			
			$_index = 0;
			// Ciclo sugli armadi configurati
			foreach ( $_items as $_item )
			{
				logDebug("Building rack HTML panel #".$_index);
			
				$_id = $_item->getRackId();
				$_status_code = -255;
			
				$_f255 	= 1;
				$_f2 	= 1;
				$_f1 	= 1;
				$_f0 	= 1;
				$_fm255 = 1;
			
				// Preparo i parametri dell'elemento
				$_item_params = array();
				$_item_params['Category'] 	= 'rack';
				$_item_params['Index'] 		= $_index;
				$_item_params['DevID'] 		= $_id;
				$_item_params['Type'] 		= $_item->type;
				$_item_params['Name'] 		= $_item->name;
				$_item_params['StatusCode'] = $_status_code;
				$_item_params['f255'] 		= $_f255;
				$_item_params['f2'] 		= $_f2;
				$_item_params['f1'] 		= $_f1;
				$_item_params['f0'] 		= $_f0;
				$_item_params['fm255'] 		= $_fm255;
				
				$_html .= devicesPageBuildItemInfoPanel($_item_params);
			
				$_index++;
			}
			
			$_content_height = 50+(80*ceil(count($_items)/14));
		}
		else if ($_level == "building")
		{
			// Recupero parametri di configurazione edifici
			$_items = $_configuration["buildings"];
			// Recupero informazioni di stato (ottenute dal DB) sulle periferiche
			$_devices_data = $_data['device'];
			
			$_index = 0;
			// Ciclo sugli armadi configurati
			foreach ( $_items as $_item )
			{
				logDebug("Building building HTML panel #".$_index);
			
				$_id = $_item->getBuildingId();
				$_status_code = -255;
			
				// Ciclo sui dati di stato delle periferiche
				foreach ( $_devices_data as $_device_data )
				{
					// Recupero stato periferica
					if ($_device_data['DevID'] == $_devid)
					{
						$_device_sevlevel = $_device_data['SevLevel'];
						$_device_offline = $_device_data['Offline'];
						$_device_active = $_device_data['Active'];
						
						$_device_status_code = coreGetStatusCode($_device_sevlevel, $_device_offline, $_device_active);
											
						$_f255 	= $_device_data['f255'];
						$_f2 	= $_device_data['f2'];
						$_f1 	= $_device_data['f1'];
						$_f0 	= $_device_data['f0'];
						$_fm255 = $_device_data['fm255'];
						
						break;
					}
				}
			
				// Preparo i parametri dell'elemento
				$_item_params = array();
				$_item_params['Category'] 	= 'building';
				$_item_params['Index'] 		= $_index;
				$_item_params['DevID'] 		= $_id;
				$_item_params['Type'] 		= $_item->type;
				$_item_params['Name'] 		= $_item->name;
				$_item_params['StatusCode'] = $_status_code;
				$_item_params['f255'] 		= $_f255;
				$_item_params['f2'] 		= $_f2;
				$_item_params['f1'] 		= $_f1;
				$_item_params['f0'] 		= $_f0;
				$_item_params['fm255'] 		= $_fm255;
				
				$_html .= devicesPageBuildItemInfoPanel($_item_params);
			
				$_index++;
			}
			
			$_content_height = 50+(80*ceil(count($_items)/14));
		}
		else if ($_level == "station")
		{
			// Recupero parametri di configurazione stazioni
			$_items = $_configuration["stations"];
			// Recupero informazioni di stato (ottenute dal DB) sulle periferiche
			$_devices_data = $_data['device'];
			
			$_index = 0;
			// Ciclo sugli armadi configurati
			foreach ( $_items as $_item )
			{
				logDebug("Building station HTML panel #".$_index);
			
				$_id = $_item->getStationId();
				$_status_code = -255;
			
				// Ciclo sui dati di stato delle periferiche
				foreach ( $_devices_data as $_device_data )
				{
					// Recupero stato periferica
					if ($_device_data['DevID'] == $_devid)
					{
						$_device_sevlevel = $_device_data['SevLevel'];
						$_device_offline = $_device_data['Offline'];
						$_device_active = $_device_data['Active'];
						
						$_device_status_code = coreGetStatusCode($_device_sevlevel, $_device_offline, $_device_active);
											
						$_f255 	= $_device_data['f255'];
						$_f2 	= $_device_data['f2'];
						$_f1 	= $_device_data['f1'];
						$_f0 	= $_device_data['f0'];
						$_fm255 = $_device_data['fm255'];
						
						break;
					}
				}
			
				// Preparo i parametri dell'elemento
				$_item_params = array();
				$_item_params['Category'] 	= 'station';
				$_item_params['Index'] 		= $_index;
				$_item_params['DevID'] 		= $_id;
				$_item_params['Type'] 		= $_item->type;
				$_item_params['Name'] 		= $_item->name;
				$_item_params['StatusCode'] = $_status_code;
				$_item_params['f255'] 		= $_f255;
				$_item_params['f2'] 		= $_f2;
				$_item_params['f1'] 		= $_f1;
				$_item_params['f0'] 		= $_f0;
				$_item_params['fm255'] 		= $_fm255;
				
				$_html .= devicesPageBuildItemInfoPanel($_item_params);
			
				$_index++;
			}
			
			$_content_height = 50+(80*ceil(count($_items)/14));
		}
		else
		{
			//
		}
	}
	else{
		if (isset($errorMessage) && $errorMessage != ''){
			$_html .= htmlParagraph($id, null, $errorMessage);
		}
		else{
			$_html .= htmlParagraph($id, null, 'Impossibile ottenere dati.');
		}
	}
	//$_html .= codeChr(1,4).'<div class="item_detail" ></div>';
	
	$_html = htmlDivitis("content_middle", null, $_html, "height: ".$_content_height."px;");
	
	return($_html);
}

/**
* Funzione per generare il codice HTML dei dettagli stazione
*/
function devicesPageBuildStationDetailsHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_conf_stations = $_configuration["stations"];
	$_regions = $_configuration["regions"];
	$_zones = $_configuration["zones"];
	$_nodes = $_configuration["nodes"];
	$_station_type_list = $_configuration["station_type_list"];
	$_conf_station = null;
	
	$_selection = $_data['selection'];
	$_selected_station_id = $_selection["ItemID"];
	
	// Ciclo sulle stazioni configurate
	foreach ( $_conf_stations as $_i_station )
	{
		$_station_id = $_i_station->getStationId();
		
		// Recupero la configurazione della stazione
		if  ($_selected_station_id == $_station_id)
		{
			$_station = $_i_station;
			break;
		}
	}
	
	// Preparo gli ID
	$_base				= "station";
	$_id_panel			= $_base."_details_panel";
	$_id_panel_info		= $_base."_details_panel_info";
	$_id_button_edit 	= "edit";
	$_id_button_delete 	= "delete";
	
	$_id_panel_form		= $_base."_details_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	if (isset($_station))
	{
		// Recupero informazioni Region Zone e Node
		$_node_id = $_station->nodeId;
		$_node = getNodeFromId($_nodes,$_node_id);
		$_zone_id = $_node->zoneId;
		$_region_id = $_node->regionId;
		$_zone = getZoneFromId($_zones,$_zone_id);
		$_region = getRegionFromId($_regions,$_region_id);
		
		// Preparo i testi
		$_text_info_region	= $_region->name;
		$_text_info_zone	= $_zone->name;
		$_text_info_node	= $_node->name;
		$_text_info_type	= getStationTypeNameFromCode($_station_type_list,$_station->type);
		$_text_info_id		= $_station->nodeId;
		$_text_info_status	= "n/d";
		$_text_select_name	= $_region->name." > ".$_zone->name." > ".$_node->name;
		$_text_select_type	= getStationTypeNameFromCode($_station_type_list,$_station->type);
		$_text_info_offset 	= $_node->devIdOffset;
	
		// HTML Informazioni Stazione
		$_panel_info_html = codeInit()
		// Taglio in alto centrale
		.htmlPanelVerticalCut('left',485,0,100)
		// Informazioni in alto a SX
		.htmlPanelInfo('region',	0,	5,	95,	'Compartimento', 375, $_text_info_region)
		.htmlPanelInfo('zone',		0,	35,	95,	'Linea',		 375, $_text_info_zone)
		.htmlPanelInfo('node',		0,	65,	95,	'Stazione',		 375, $_text_info_node)
		// Informazioni in alto a DX
		.htmlPanelInfo('type',		490,5,	65,	'Tipo',			 170, $_text_info_type)
		.htmlPanelInfo('id',		490,35,	65,	'ID',		 	 170, $_text_info_id)
		.htmlPanelInfo('offset',	490,65,	65,	'Offset ID', 	 170, $_text_info_offset)
		;
		// HTML Bottoni Informazioni Stazione
		$_panel_info_btns_html = codeInit()
		// Bottone Edit (AUTH)
		.htmlPanelButton( $_id_panel_info, $_id_button_edit,"details_panel_button",	80,	80,	'edit',	 null, false, 'admin,IaP_installer,IeC_installer,installer', "Modifica stazione" )
		// Bottone Delete (AUTH)
		.htmlPanelButton( $_id_panel_info, $_id_button_delete,"details_panel_button",	45,	80,	'minus', null, false, 'admin,IaP_installer,IeC_installer,installer', "Elimina stazione" )
		;
		// HTML Form Edit Stazione
		$_panel_form_html = codeInit()
		// Taglio in alto centrale
		.htmlPanelVerticalCut('left',485,0,100)
		// Informazioni in alto a SX
		.htmlPanelFormSearch( $_id_panel, 'name',	0,	5,	65,	'Stazione', 405, $_text_select_name)
		// Informazioni in alto a DX
		.htmlPanelFormSelect( $_id_panel, 'type',	490,5,	65,	'Tipo',		170, $_text_select_type, null, null, true)
		;
		// HTML Bottoni Form Edit Stazione
		$_panel_form_btns_html = codeInit()
		// Bottone Save (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_save,"details_panel_button",   80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Conferma modifiche" )
		// Bottone Cancel (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_cancel,"details_panel_button", 45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Annulla modifiche" )
		;
	}
	else
	{
		// HTML Informazioni Stazione
		$_panel_info_html = codeInit();
		// HTML Bottoni Informazioni Stazione
		$_panel_info_btns_html = codeInit();
		// HTML Form Edit Stazione
		$_panel_form_html = codeInit();
		// HTML Bottoni Form Edit Stazione
		$_panel_form_btns_html = codeInit();
	}
		
	// HTML Pannello Dettaglio Stazione	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis( $_id_panel_info			, 'details_panel_info'		, $_panel_info_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_info."_btns"	, 'details_panel_info_btns'	, $_panel_info_btns_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'details_panel_form'		, $_panel_form_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'details_panel_form_btns'	, $_panel_form_btns_html )
		;
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	if (isset($_station->type))
		$r_image 	= $_base.'s/'.$_station->type.'_100.png';
	else
		$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 150;
	$_html 		= $_inner_html;
	
	return($_html);
}

/**
* Funzione
*/
function devicesPageBuildBuildingDetailsHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_buildings = $_configuration["buildings"];
	$_building = null;
	
	$_selection = $_data['selection'];
	$_selected_building_id = $_selection["ItemID"];
	
	// Ciclo sulle stazioni configurate
	foreach ( $_buildings as $_item )
	{
		$_building_id = $_item->getBuildingId();
		
		// Recupero la configurazione dell'edificio
		if  ($_selected_building_id == $_building_id)
		{
			$_building = $_item;
			break;
		}
	}
	
	// Preparo gli ID
	$_base				= "building";
	$_id_panel			= $_base."_details_panel";
	$_id_panel_info		= $_base."_details_panel_info";
	$_id_button_edit 	= "edit";
	$_id_button_delete 	= "delete";
	
	$_id_panel_form		= $_base."_details_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	if (isset($_building))
	{
		$_building_type_list 	= $_configuration["building_type_list"];
		$_stations 				= $_configuration["stations"];
		
		$_station = getStationFromId($_stations,$_building->stationId);
			
		// Preparo i testi
		$_text_info_name		= $_building->name;
		$_text_info_station		= $_station->name;
		$_text_info_note		= $_building->note;
		$_text_info_type		= getBuildingTypeNameFromCode($_building_type_list,$_building->type);
		$_text_info_status		= "n/d";
		$_text_select_station	= (isset($_station))?$_station->getDisplayName():"";
		$_text_select_type		= getBuildingTypeNameFromCode($_building_type_list,$_building->type);
	
	// HTML Informazioni Edificio
	$_panel_info_html = codeInit()
		// Taglio in alto centrale
		.htmlPanelVerticalCut('left',485,0,100)
		// Informazioni in alto a SX
		.htmlPanelInfo('name',		0,	5,	65,	'Nome', 	405, $_text_info_name)
		.htmlPanelInfo('station',	0,	35,	65,	'Stazione',	405, $_text_info_station)
		.htmlPanelInfo('note',		0,	65,	65,	'Note',		405, $_text_info_note)
		// Informazioni in alto a DX
		.htmlPanelInfo('type',		490,5,	65,	'Tipo',		170, $_text_info_type)
		//.htmlPanelInfo('status',	490,65,	65,	'Stato',	170, $_text_info_status)
		;
	// HTML Bottoni Informazioni Edificio
	$_panel_info_btns_html = codeInit()
		// Bottone Edit (AUTH)
		.htmlPanelButton( $_id_panel_info, $_id_button_edit,"details_panel_button",	80,	80,	'edit',	 null, false, 'admin,IaP_installer,IeC_installer,installer', "Modifica edificio" )
		// Bottone Delete (AUTH)
		.htmlPanelButton( $_id_panel_info, $_id_button_delete,"details_panel_button",	45,	80,	'minus', null, false, 'admin,IaP_installer,IeC_installer,installer', "Elimina edificio" )
		;
	// HTML Form Edit Edificio
	$_panel_form_html = codeInit()
		// Taglio in alto centrale
		.htmlPanelVerticalCut('left',485,0,100)
		// Informazioni in alto a SX
		.htmlPanelFormInput( $_id_panel,	'name',		0,	5,	65,	'Nome', 	405, $_text_info_name)
		.htmlPanelFormSelect( $_id_panel,	'station',	0,	35,	65,	'Stazione',	405, $_text_select_station)
		.htmlPanelFormInput( $_id_panel,	'note',		0,	65,	65,	'Note',		405, $_text_info_note)
		// Informazioni in alto a DX
		.htmlPanelFormSelect( $_id_panel, 	'type',		490,5,	65,	'Tipo',		170, $_text_select_type)
		;
	// HTML Bottoni Form Edit Edificio
	$_panel_form_btns_html = codeInit()
		// Bottone Save (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_save,"details_panel_button",   80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Conferma modifiche" )
		// Bottone Cancel (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_cancel,"details_panel_button", 45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Annulla modifiche" )
		;
	}
	else
	{
		// HTML Informazioni Stazione
		$_panel_info_html = codeInit();
		// HTML Bottoni Informazioni Stazione
		$_panel_info_btns_html = codeInit();
		// HTML Form Edit Stazione
		$_panel_form_html = codeInit();
		// HTML Bottoni Form Edit Stazione
		$_panel_form_btns_html = codeInit();
	}
	
	// HTML Pannello Dettaglio Edificio	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis( $_id_panel_info			, 'details_panel_info'		, $_panel_info_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_info."_btns"	, 'details_panel_info_btns'	, $_panel_info_btns_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'details_panel_form'		, $_panel_form_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'details_panel_form_btns'	, $_panel_form_btns_html )
		;
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	if (isset($_building->type))
		$r_image 	= $_base.'s/'.$_building->type.'_100.png';
	else
		$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 150;
	$_html 		= $_inner_html;
	
	return($_html);
}

/**
* Funzione per costruire il codice HTML dei dettagli Armadio
*/
function devicesPageBuildRackDetailsHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_racks = $_configuration["locations"];
	$_rack = null;
	
	$_selection = $_data['selection'];
	$_selected_rack_id = $_selection["ItemID"];
	
	// Ciclo sulle stazioni configurate
	foreach ( $_racks as $_item )
	{
		$_rack_id = $_item->getRackId();
		
		// Recupero la configurazione dell'edificio
		if  ($_selected_rack_id == $_rack_id)
		{
			$_rack = $_item;
			break;
		}
	}
	
	// Preparo gli ID
	$_base				= "rack";
	$_id_panel			= $_base."_details_panel";
	$_id_panel_info		= $_base."_details_panel_info";
	$_id_button_edit 	= "edit";
	$_id_button_delete 	= "delete";
	
	$_id_panel_form		= $_base."_details_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	if (isset($_rack))
	{
		$_rack_type_list 	= $_configuration["rack_type_list"];
		$_buildings 		= $_configuration["buildings"];
		
		$_building = getBuildingFromId($_buildings,$_rack->stationId,$_rack->buildingId);
		
		$_type		= getRackTypeFromCode($_rack_type_list,$_rack->type);
		if (isset($_type))
		{
			$_type_name = $_type->name;
		}
		//$_type_name = getRackTypeNameFromCode($_rack_type_list,$_rack->type);
		
		// Preparo i testi
		$_text_info_name		= $_rack->name;
		$_text_info_building	= (isset($_building))?$_building->getDisplayName():"";
		$_text_info_note		= $_rack->note;
		$_text_info_type		= $_type_name;
		$_text_info_status		= "n/d";
		$_text_select_building	= (isset($_building))?$_building->getDisplayName():"";
		$_text_select_type		= $_type_name;
	
		// HTML Informazioni Armadio
		$_panel_info_html = codeInit()
			// Taglio in alto centrale
			.htmlPanelVerticalCut('left',485,0,100)
			// Informazioni in alto a SX
			.htmlPanelInfo('name',		0,	5,	65,	'Nome', 	405, $_text_info_name)
			.htmlPanelInfo('building',	0,	35,	65,	'Edificio',	405, $_text_info_building)
			.htmlPanelInfo('note',		0,	65,	65,	'Note',		405, $_text_info_note)
			// Informazioni in alto a DX
			.htmlPanelInfo('type',		490,5,	65,	'Tipo',		170, $_text_info_type)
			//.htmlPanelInfo('status',	490,65,	65,	'Stato',	170, $_text_info_status)
			;
		// HTML Bottoni Informazioni Armadio
		$_panel_info_btns_html = codeInit()
			// Bottone Edit (AUTH)
			.htmlPanelButton( $_id_panel_info, $_id_button_edit,"details_panel_button",	80,	80,	'edit',	 null, false, 'admin,IaP_installer,IeC_installer,installer', "Modifica armadio" )
			// Bottone Delete (AUTH)
			.htmlPanelButton( $_id_panel_info, $_id_button_delete,"details_panel_button",	45,	80,	'minus', null, false, 'admin,IaP_installer,IeC_installer,installer', "Elimina armadio" )
			;
		// HTML Form Edit Armadio
		$_panel_form_html = codeInit()
			// Taglio in alto centrale
			.htmlPanelVerticalCut('left',485,0,100)
			// Informazioni in alto a SX
			.htmlPanelFormInput( $_id_panel,	'name',		0,	5,	65,	'Nome', 	405, $_text_info_name)
			.htmlPanelFormSelect( $_id_panel,	'building',	0,	35,	65,	'Edificio',	405, $_text_select_building)
			.htmlPanelFormInput( $_id_panel,	'note',		0,	65,	65,	'Note',		405, $_text_info_note)
			// Informazioni in alto a DX
			.htmlPanelFormSelect( $_id_panel, 	'type',		490,5,	65,	'Tipo',		170, $_text_select_type)
			;
		// HTML Bottoni Form Edit Armadio
		$_panel_form_btns_html = codeInit()
			// Bottone Save (AUTH)
			.htmlPanelButton( $_id_panel_form, $_id_button_save,"details_panel_button",   80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Conferma modifiche" )
			// Bottone Cancel (AUTH)
			.htmlPanelButton( $_id_panel_form, $_id_button_cancel,"details_panel_button", 45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Annulla modifiche" )
			;
	}
	else
	{
		// HTML Informazioni Stazione
		$_panel_info_html = codeInit();
		// HTML Bottoni Informazioni Stazione
		$_panel_info_btns_html = codeInit();
		// HTML Form Edit Stazione
		$_panel_form_html = codeInit();
		// HTML Bottoni Form Edit Stazione
		$_panel_form_btns_html = codeInit();
	}
	
	// HTML Pannello Dettaglio Armadio	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis( $_id_panel_info			, 'details_panel_info'		, $_panel_info_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_info."_btns"	, 'details_panel_info_btns'	, $_panel_info_btns_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'details_panel_form'		, $_panel_form_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'details_panel_form_btns'	, $_panel_form_btns_html )
		;
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	if (isset($_rack->type))
		$r_image 	= $_base.'s/'.$_rack->type.'_100.png';
	else
		$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 150;
	$_html 		= $_inner_html;
	
	$_devices 		= $_configuration["devices"];
	
	$_slots = devicesRackSlotsBuild($_devices,$_rack,$_type);
	
	$_rack_height = 30+($_type->maxPositions['y']*(4+30*$_type->slotHeight));
	
	$r_height += $_rack_height;
	
	$_html		.= devicesRackBuild("view","rack",30,130,$_type,$_slots);
	
	return($_html);
}

/**
*
*/
function devicesPageGetAck($ackList,$devId,$strId=null,$fieId=null)
{
	$_ack = null;
	
	$_list = $ackList;
	
	// Ciclo sulla lista degli ACK
	foreach ( $_list as $_item )
	{
		if ($_item["DevID"] == $devId)
		{
			if ((!isset($_item["StrID"]) && $strId === null) || (isset($_item["StrID"]) && $_item["StrID"] === $strId))
			{
				if ((!isset($_item["FieldID"]) && $fieId === null) || (isset($_item["FieldID"]) && $_item["FieldID"] === $fieId))
				{
					$_ack = $_item;
					break;
				}
			}
		}
	}
	
	return($_ack);
}

/**
*
*/
function devicesPageIsAck($ackList,$devId,$strId=null,$fieId=null)
{
	$_is_ack = false;
	
	$_list = $ackList;
	
	// Ciclo sulla lista degli ACK
	foreach ( $_list as $_item )
	{
		if ($_item["DevID"] == $devId)
		{
			if ((!isset($_item["StrID"]) && $strId === null) || (isset($_item["StrID"]) && $_item["StrID"] === $strId))
			{
				if ((!isset($_item["FieldID"]) && $fieId === null) || (isset($_item["FieldID"]) && $_item["FieldID"] === $fieId))
				{
					$_is_ack = true;
					break;
				}
			}
		}
	}
	
	return($_is_ack);
}

/**
*
*/
function devicesGetHigestSevLevel($fieldList,$strId,$fieldId)
{
	$_sev_level = -255;
	
	foreach ($fieldList as $_index => $_field)
	{
		if ($_field['StrID'] == $strId && $_field['FieldID'] == $fieldId)
		{
			$_sev_level = ($_sev_level>$_field['SevLevel'])?$_sev_level:$_field['SevLevel'];
		}
	}
	
	return($_sev_level);
}

/**
* Funzione per generare il codice HTML per il dettaglio periferica
*/
function devicesPageBuildDeviceDetailsHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_devices = $_configuration["devices"];
	$_device = null;
	
	$_selection 			= $_data['selection'];
	$_selected_device_id 	= $_selection["ItemID"];
	$_devices_data 			= $_data['device'];
	$_device_data 			= $_devices_data[0];
	$_fields_data			= $_data['field'];
	$_ack_data				= $_data['ack'];
		
	// Ciclo sulle periferiche configurate
	foreach ( $_devices as $_item )
	{
		$_devid = $_item->getDevId();
		
		// Recupero stato periferica
		if ($_selected_device_id == $_devid)
		{
			$_device = $_item;
			break;
		}
	}
	
	// $_device = informazioni di cofnigrazione
	// $_device_data = dati diagnostici da DB 
	
	// Preparo gli ID
	$_base				= "device";
	$_id_panel			= $_base."_details_panel";
	$_id_panel_info		= $_base."_details_panel_info";
	$_id_button_edit 	= "edit";
	$_id_button_delete 	= "delete";
	$_id_panel_form		= $_base."_details_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	if (isset($_device))
	{
		$_location	= getRack($_configuration["locations"],$_device->stationId,$_device->buildingId,$_device->locationId);
		
		if (count($_fields_data) > 0)
			$_device_sevlevel	= (isset($_device_data))?$_device_data['SevLevel']:"-255";
		else
			$_device_sevlevel	= "-255";
		$_device_offline		= (isset($_device_data))?$_device_data['Offline']:false;
		$_device_active			= ($_device->active == "true")?true:false;
		
		// Preparo i testi
		$_text_info_name		= $_device->name;
		$_text_info_sn			= $_device->sn;
		$_color_info_status		= coreGetStatusColor($_device_sevlevel,$_device_offline,$_device_active);
		$_symbol_status			= coreGetStatusSymbol($_device_sevlevel,$_device_offline,$_device_active);
		$_color_info_text		= coreGetStatusColor($_device_sevlevel,$_device_offline,$_device_active,true);
		$_community_html		= '';
		
		if (isset($_device_data['Description']) && $_device_data['Description'] != "" && $_device_data['Offline'] != "1")
			$_text_info_status	= $_device_data['Description'];
		else
			$_text_info_status	= devicesPageGetStatusText($_device_sevlevel,$_device_offline,$_device_active);
		$_text_info_type		= getDeviceTypeNameFromCode($_configuration["device_type_list"],$_device->type);
		$_text_info_location	= (isset($_location))?$_location->getDisplayName():""; // Se viene cancellata la location l'oggetto non e' piu' disponibile
		$_text_info_position	= $_device->position;
		//$_text_info_port		= getPortDisplayNameFromId($_configuration["ports"],$_device->port);
		//$_text_info_address	= $_device->addr;
		$_text_monitoring_device_id = $_device->monitoringDeviceId;
		$_is_IeC_device			= (isset($_device->monitoringDeviceId) && $_device->monitoringDeviceId != '')?true:false;
		
		$_device_port = getPortFromId($_configuration["ports"],$_device->port);
		if (isset($_device_port))
		{
			$_text_info_port = $_device_port->getDisplayName();
			if (portTypeIsSerial($_device_port->type))
			{
				$_text_info_address			= portDecodeSerialAddress($_device->addr,$_device->modemSize,$_device->modemAddr,true);
				$_text_select_serial_size	= $_device->modemSize;
				$_text_select_serial_modem	= portHexToIntAddress($_device->modemAddr);
				$_text_select_serial_addr	= portDecodeSerialAddress($_device->addr,$_device->modemSize,$_device->modemAddr,false);
				if (deviceTypeIsSTSI($_device->type))
				{
					$_text_info_cts_addr		= "CTS:".portHexToIntAddress($_device->ctsAddr);
					$_text_select_cts_addr		= portHexToIntAddress($_device->ctsAddr);
					$_cts_visible				= null;
				}
				else
				{
					$_text_info_cts_addr		= "";
					$_text_select_cts_addr		= "00";
					$_cts_visible				= "display: none";
				}
				$_text_input_ip				= array("","","","");
				$_serial_visible			= null;
				$_ip_visible				= "display: none";
				$_community_visible			= "display: none";
				$_text_input_community		= "public";
			}
			else
			{
				$_text_select_serial_size	= "16";
				$_text_select_serial_modem	= "--";
				$_text_select_serial_addr	= "00";
				$_text_info_address			= $_device->addr;
				$_text_input_ip 			= portDecodeIPAddress($_device->addr);
				$_serial_visible			= "display: none";
				$_cts_visible				= "display: none";
				$_text_info_cts_addr		= "";
				$_text_select_cts_addr		= "00";
				$_ip_visible				= null;
				if ($_device->type == "XXIP000")
				{
					$_community_visible 	= "display: none";
					$_text_input_community	= "public";
				}
				else
				{
					$_community_visible		= null;
					$_text_input_community	= $_device->snmpCommunity;
					$_community_html = htmlPanelInfo( 'community',		0,		95,	88,	'Community',	200, $_text_input_community );					
				}
			}
		}
		else
		{
			$_text_info_port = "";
		}
		
		$_text_select_type		= getDeviceTypeNameFromCode($_configuration["device_type_list"],$_device->type);
		$_text_select_location	= (isset($_location))?$_location->getDisplayName():"";//$_device_data['StationName'].' > '.$_device_data['BuildingName'].' > '.$_device_data['RackName'];
		$_text_select_port		= getPortDisplayNameFromId($_configuration["ports"],$_device->port);
		
		if (isset($_device_data))
		{
			$_device_ack = devicesPageGetAck($_ack_data,$_device->getDevId());
			$_device_ack_profile = "admin,IaP_installer,IeC_installer,installer";
		}
		else
		{
			$_device_ack = null;
			$_device_ack_profile = "none";
		}
		
		// HTML Informazioni Periferica
		$_panel_info_html = codeInit()
			// Taglio in alto centrale
			.htmlPanelVerticalCut('left',488,0,130)
			// Informazioni in alto a SX
			.htmlPanelInfo( 'type',		0,		5,	88,	'Tipo',			394, $_text_info_type )
			.htmlPanelInfo( 'location',	0,		35,	88,	'Ubicazione',	280, $_text_info_location )
			.htmlPanelInfo( 'position',	378,	35,	62,	'Posizione',	40,  $_text_info_position )
			.htmlPanelInfo( 'port',		0,		65,	88,	'Collegamento',	200, $_text_info_port )
			.htmlPanelInfo( 'address',	298,	65,	62,	'Indirizzo',	120, $_text_info_address." ".$_text_info_cts_addr )
			// Informazioni in alto a DX
			.htmlPanelInfo( 'name',		493,	5,	48,	'Nome',			190, $_text_info_name )
			
			.htmlPanelInfo( 'dev_sn',	493,	35,	48,	'N.Serie',		190, $_text_info_sn )
			.$_community_html
			.htmlPanelInfo( 'status',	493,	65,	78,	'Stato',		160, $_text_info_status,	false,	$_color_info_text )
			.htmlIconStatus( $_id_panel, null, 	540, 68, $_color_info_status, $_symbol_status, false, null, true, null, $_device_ack_profile, $_device_ack)
			;
			
		if ($_is_IeC_device){
			$_panel_info_html .= htmlPanelInfo( 'md_id',	493,	95,	48,	'ID IeC',	190, $_text_monitoring_device_id , false, null, 'admin,IeC_installer');
			$_panel_info_html .= htmlPanelInfo( 'md_id',	493,	95,	48,	'',			190, 'Collegata al sistema IeC' , false, null, 'IaP_installer,installer,guest');
			
			// HTML Bottoni Informazioni Periferica
			$_panel_info_btns_html = codeInit()
				// Bottone Edit (AUTH)
				.htmlPanelButton($_id_panel_info,$_id_button_edit,"details_panel_button",80,110,'edit',null,false,'admin,IeC_installer', "Modifica periferica" )
				// Bottone Delete (AUTH)
				.htmlPanelButton($_id_panel_info,$_id_button_delete,"details_panel_button",45,110,'minus',null,false,'admin,IeC_installer', "Elimina periferica" )
			;
		}
		else{
			// HTML Bottoni Informazioni Periferica
			$_panel_info_btns_html = codeInit()
				// Bottone Edit (AUTH)
				.htmlPanelButton($_id_panel_info,$_id_button_edit,"details_panel_button",80,110,'edit',null,false,'admin,IaP_installer,IeC_installer,installer', "Modifica periferica" )
				// Bottone Delete (AUTH)
				.htmlPanelButton($_id_panel_info,$_id_button_delete,"details_panel_button",45,110,'minus',null,false,'admin,IaP_installer,IeC_installer,installer', "Elimina periferica" )
			;
		}
			
		// HTML Form Edit Periferica
		$_panel_form_html = codeInit()
			// Campi in alto a SX
			.htmlPanelFormSearch( $_id_panel,	'type',			0,	5,	88,	'Tipo',				394, $_text_select_type )
			.htmlPanelFormSelect( $_id_panel,	'location',		0,	35,	88,	'Ubicazione',		280, $_text_select_location,	null,	null, true ) 
			.htmlPanelFormInput( $_id_panel,	'position',		378,35,	62,	'Posizione',		40,  $_text_info_position,		null,	null, true )
			.htmlPanelFormSelect( $_id_panel,	'port',			0,	65,	88,	'Collegamento',		200, $_text_select_port,		null,	null, true )
		
			// Campi in alto a DX
			.htmlPanelFormInput( $_id_panel,	'name',			493,5,	48,	'Nome',				190, $_text_info_name,				null )
			.htmlPanelFormInput( $_id_panel,	'sn',			493,35,	48,	'N.Serie',			190, $_text_info_sn,				null )
			.htmlPanelFormInput( $_id_panel,	'md_id',		493,95,	48,	'ID IeC',			190, $_text_monitoring_device_id,	null , null, false, 'admin,IeC_installer')
			
			// Indirizzo Seriale
			.htmlPanelFormSelect( $_id_panel,	'serial_size',	298,65,	87,	'Indirizzo: Size',	50, $_text_select_serial_size,	null,	$_serial_visible, true )
			.htmlPanelFormSelect( $_id_panel,	'serial_modem',	446,65,	43,	'Modem',			50, $_text_select_serial_modem,	null,	$_serial_visible, true )
			.htmlPanelFormSelect( $_id_panel,	'serial_addr',	550,65,	40,	'Addr',				50, $_text_select_serial_addr,	null,	$_serial_visible, true )
		
			// Indirizzo CTS
			.htmlPanelFormSelect( $_id_panel,	'cts_addr',		651,65,	30,	'CTS',				50, $_text_select_cts_addr,		null,	$_cts_visible, true ) // Da 00 a 99
		
			// Indirizzo IP
			.htmlPanelFormInput( $_id_panel,	'ip1',			307,65,	17,	'IP',				30, $_text_input_ip[0],			null,	$_ip_visible )
			.htmlPanelFormInput( $_id_panel,	'ip2',			365,65,	1,	'.',				30, $_text_input_ip[1],			null,	$_ip_visible )
			.htmlPanelFormInput( $_id_panel,	'ip3',			407,65,	1,	'.',				30, $_text_input_ip[2],			null,	$_ip_visible )
			.htmlPanelFormInput( $_id_panel,	'ip4',			449,65,	1,	'.',				30, $_text_input_ip[3],			null,	$_ip_visible )
		
			// Community
			.htmlPanelFormInput( $_id_panel,	'community',	0,95,	88,	'Community',		200, $_text_input_community,	null,	$_community_visible )
			.codeChr(1,6).htmlDivitis('device_details_panel_form_rack'	,'details_panel_form_rack'	,"", "display: none;")
			;
			
		// HTML Bottoni Form Edit Periferica
		$_panel_form_btns_html = codeInit()
			// Bottone Save (AUTH)
			.htmlPanelButton($_id_panel_form,$_id_button_save,"details_panel_button",80,110,'ok',null,false,'admin,IaP_installer,IeC_installer,installer', "Conferma modifiche" )
			// Bottone Cancel (AUTH)
			.htmlPanelButton($_id_panel_form,$_id_button_cancel,"details_panel_button",45,110,'cancel',null,false,'admin,IaP_installer,IeC_installer,installer', "Annulla modifiche")
			;
	}
	else
	{
		// HTML Informazioni Stazione
		$_panel_info_html = codeInit();
		// HTML Bottoni Informazioni Stazione
		$_panel_info_btns_html = codeInit();
		// HTML Form Edit Stazione
		$_panel_form_html = codeInit();
		// HTML Bottoni Form Edit Stazione
		$_panel_form_btns_html = codeInit();
	}
	
	// HTML Pannello Dettaglio Periferica	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis($_id_panel_info			,'details_panel_info'		,$_panel_info_html)
		.codeChr(1,5).htmlDivitis($_id_panel_info."_btns"	,'details_panel_info_btns'	,$_panel_info_btns_html)
		.codeChr(1,5).htmlDivitis($_id_panel_form			,'details_panel_form'		,$_panel_form_html)
		.codeChr(1,5).htmlDivitis($_id_panel_form."_btns"	,'details_panel_form_btns'	,$_panel_form_btns_html)
		;
	
	//*
	// Estraggo le informazioni degli STREAM			
	$_html_streams = codeInit();
	if (isset($_data) && is_array($_data) && (count($_data)>0) && isset($_data['stream']))
	{
		// Ciclo sugli STREAM
		$_id_counter_s = 0;
		foreach ($_data['stream'] as $key => $value)
		{
			$_stream_ack = devicesPageGetAck($_ack_data,$_device->getDevId(),$value['StrID']);
			
			// Visualizzo uno STREAM
			$_datetime = $value['DateTime'];
			if ($value['StrID'] == $_selection['StrID'])
			{
				$_html_streams .= htmlPanelTextValue($value['StrID'],10,(155+30+($_id_counter_s*30)),$value['SevLevel'],230,$value['Name'],133,$_datetime->format( 'd/m/Y H:i:s' ),true,true,null,null,false,"default","stream",$_stream_ack);
			}
			else
			{
				$_html_streams .= htmlPanelTextValue($value['StrID'],10,(155+30+($_id_counter_s*30)),$value['SevLevel'],230,$value['Name'],133,$_datetime->format( 'd/m/Y H:i:s' ),true,false,null,null,false,"default","stream",$_stream_ack);
			}
			$_id_counter_s++;
		}
	}
	
	// Estraggo le informazioni dei field e relativi value e description
	$_html_fields = codeInit();
	$_html_values = codeInit();
	$_html_description = codeInit();
	//*
	if (isset($_data) && is_array($_data) && (count($_data)>0) && isset($_data['field']))
	{
		$_id_counter_f = 0;
		$_id_counter_v = 0;
		$_id_counter_d = 0;
		
		foreach ($_data['field'] as $key => $value)
		{
			$_is_table = null;
			if( isset($value['IsTable']) && $value['IsTable'] === 1){
				$_is_table = true;
			}

			$_field_array_count = 0;
			foreach ($_data['field'] as $_field_id => $_field)
			{
				if ($_field['FieldID'] == $value['FieldID']) $_field_array_count++;
			}
			if ($_field_array_count>1) $_field_array = true;
			else $_field_array = false;
		
			if ($value['StrID'] == $_selection['StrID'])
			{
				//$_field_ack = null;
				$_field_ack = devicesPageGetAck($_ack_data,$_device->getDevId(),$value['StrID'],$value['FieldID']);
				
				//*
				if ($value['FieldID'] == $_selection['FieldID'])
				{
					//*
					// Visualizzo un FIELD (il primo di un eventuale array)
					if ($value['ArrayID'] == 0) 
					{
						$_field_sev_level = devicesGetHigestSevLevel($_data['field'],$value['StrID'],$value['FieldID']);
						$_html_fields .= htmlPanelTextName($value['FieldID'],418 ,(155+30+($_id_counter_f*30)) ,$_field_sev_level, 225, $value['Name'],true,true,$_field_array,null,"default","field",$_field_ack,$_is_table);
						$_id_counter_f++;
					}
					//*/
					//*
					if ($value['ArrayID'] == $_selection['ValueID'])
					{
						if ($_field_array)
						{
							$_html_values .= htmlPanelTextValue($value['ArrayID'], 683, (155+30+($_id_counter_v*30)), $value['SevLevel'], 15, ($value['ArrayID']+1), 225, $value['Value'], true, true, null, null, true, $_is_table );
						}
						else
						{
							$_html_values .= htmlPanelTextValue($value['ArrayID'], 683, (155+30+($_id_counter_v*30)), $value['SevLevel'], 0, '', 240, $value['Value'], true, true, null, null, true, $_is_table );
						}

						$_id_counter_d_offset = $_id_counter_v+1;
						
						$_descriptions = devicesPageExtractFieldDescriptions($value['Description']);
						
						foreach ($_descriptions as $_desc_id => $_desc)
						{
							// Visualizzo una DESCRIZIONE
							$_html_description .= htmlPanelTextName( null, 713, (155+30+(($_id_counter_d_offset+$_id_counter_d)*30)), $_desc['SevLevel'], 225, $_desc['Description'], false, false, false, $_is_table );
							$_id_counter_d++;
						}
						$_id_counter_v += $_id_counter_d;
					}
					else
					{
						// Visualizzo un VALORE
						if ($_field_array)
						{
							$_html_values .= htmlPanelTextValue($value['ArrayID'], 683, (155+30+($_id_counter_v*30)), $value['SevLevel'], 15, ($value['ArrayID']+1), 225, $value['Value'], true, false, null, null, true, $_is_table );
						}
						else
						{
							$_html_values .= htmlPanelTextValue($value['ArrayID'], 683, (155+30+($_id_counter_v*30)), $value['SevLevel'], 0, '', 240, $value['Value'], true, false, null, null, true, $_is_table );
						}
					}
					//*/
					$_id_counter_v++;
				}
				else
				{
					//*
					// Visualizzo un FIELD (il primo di un eventuale array)
					if ($value['ArrayID'] == 0){
						//*
						$_field_sev_level = devicesGetHigestSevLevel($_data['field'],$value['StrID'],$value['FieldID']);
						
						$_html_fields .= htmlPanelTextName($value['FieldID'], 418, (155+30+($_id_counter_f*30)), $_field_sev_level, 225, $value['Name'], true, false, $_field_array ,null,"default","field",$_field_ack, $_is_table);
						//*/
						$_id_counter_f++;
					}
					//*/
				}
				//*/
			}
			else
			{
			
			}
		}
	}
	//*/
	$_rows = $_id_counter_s;
	//*
	if ($_id_counter_f > $_rows) $_rows = $_id_counter_f;
	if ($_id_counter_v > $_rows) $_rows = $_id_counter_v;
	if (($_id_counter_d_offset+$_id_counter_d) > $_rows) $_rows = ($_id_counter_d_offset+$_id_counter_d);
	//*/
	if ($_rows > 0)
	{
		if ($_rows < 3) $_rows = 3;
		$r_height = 150+30+50+(30*$_rows);
	}
	else
	{
		$r_height = 150+30;
	}
	
	//$_inner_html .= "<!-- TYPE: ".$_device_data["Type"]."=".$_device->type."-->";
	if (isset($_device_data) && ($_device->type == "INFSTAZ1" || $_device->type == "SYSNET000" || $_device_data["Type"] == $_device->type))
	{			
		// === STREAM ===
		// Apertura
		$_inner_html .= codeChr(1,5).'<div id="device_details_panel_streams">';
		if (count($_data['stream']) > 0)
		{
			// Visualizzo stream
			$_inner_html .= $_html_streams;
			// Taglio stream|field
			$_inner_html .= htmlPanelVerticalCut('left' ,418 ,125+30,60+(30*$_rows));
		}
		// Chiusura
		$_inner_html .= codeChr(1,5).'</div>';
		//*
		// === FIELD ===
		// Apertura
		$_inner_html .= codeChr(1,5).'<div id="device_details_panel_fields">';
		if (count($_data['field']) > 0)
		{
			// Visualizzo field
			$_inner_html .= $_html_fields;
			// Taglio field|value
			$_inner_html .= htmlPanelVerticalCut('left',683 ,125+30,60+(30*$_rows));
		}
		$_inner_html .= codeChr(1,5).'</div>';
		//*		
		// === VALUE ===
		// Apertura
		$_inner_html .= codeChr(1,5).'<div id="device_details_panel_values">';
		if (count($_data['field']) > 0)
		{
			$_inner_html .= $_html_values;
		}
		// Chiusura
		$_inner_html .= codeChr(1,5).'</div>';
		//*		
		// === DESCRIPTION ===
		// Apertura
		$_inner_html .= codeChr(1,5).'<div id="device_details_panel_descriptions">';
		if (count($_data['field']) > 0)
		{	
			// Freccina in giu'
			$_inner_html .= codeChr(1,5).htmlImage(null,'details_panel_selection_bottom','gui/sk_details_selection_bottom.png','default','left: 713px; top: '.(125+30+(30*$_id_counter_d_offset)).'px; display: inherit;');
			// Taglio value|descrizioni
			$_inner_html .= htmlPanelLCut('left' ,713 ,125+30+30+(30*$_id_counter_d_offset),(30*$_id_counter_d),235 );
			// Visualizzo le description	
			$_inner_html .= $_html_description;
		}
		
		// Chiusura
		$_inner_html .= codeChr(1,5).'</div>';
		//*/
	}
	else
	{
		$r_height = 150+30;
	}
	//*/	
		
	$r_id = $_id_panel;
	if (isset($_device->type))
		$r_image = $_base.'s/'.$_device->type.'_100.png'; 
	else
		$r_image = 'gui/sk_transparent.png';
	$_html .= $_inner_html;
	//*/
		
	return($_html);
}

/**
* Funzione per costruire il codice HTML per il dettaglio pagina periferiche
*/
function devicesPageBuildDetailsPanelHTML($data)
{
	logDebug("devicesPageBuildDetailsPanelHTML");
	
	global $_configuration;
	
	$_html = codeInit();
	
	$_data = $data;
	
	// Recupero informazioni sul livello di visualizzazione
	$_level = $_data['level'];
	
	switch ($_level)
	{
		case "device":
			$_inner_html = devicesPageBuildDeviceDetailsHTML($_data,$_id,$_image,$_height);
			$_info_panel_height = 150;
		break;
		case "rack":
			$_inner_html = devicesPageBuildRackDetailsHTML($_data,$_id,$_image,$_height);
			$_info_panel_height = 120;
		break;
		case "building":
			$_inner_html = devicesPageBuildBuildingDetailsHTML($_data,$_id,$_image,$_height);
			$_info_panel_height = 120;
		break;
		case "station":
			$_inner_html = devicesPageBuildStationDetailsHTML($_data,$_id,$_image,$_height);
			$_info_panel_height = 120;
		break;
		default:
			
		break;
	}
	
	// Genero il codice HTML del pannello
	$_html .= htmlDetailsPanelBuild($_id,$_image,$_inner_html,$_height,$_info_panel_height);
		
	return($_html);
}

/**
* Funzione per costruire il codice HTML per il form per aggiungere periferiche
*/
function devicesPageBuildDeviceAddHTML($data,&$r_id,&$r_image,&$r_height)
{
	logDebug("devicesPageBuildDeviceAddHTML()");
	
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_conf_devices 	= $_configuration["devices"];
	$_locations = $_configuration["locations"];
	
	// Calcolo il valori di default
	$_default_sn = "00.00.000";
	
	// Preparo gli ID
	$_base				= "device";
	$_id_panel			= $_base."_add_panel";
	$_id_panel_form		= $_base."_add_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	// Preparo i testi
	$_text_select_name		= "";
	$_text_info_sn			= $_default_sn;
	$_text_info_md_id		= '';
	$_text_select_type		= "";
	$_text_select_location	= $_default_location_name;
	$_text_info_position	= "";
	$_text_select_port		= "";
	$_text_select_address	= "";
	
	$_text_select_serial_size	= "16";
	$_text_select_serial_modem	= "--";
	$_text_select_serial_addr	= "00";
	$_text_select_serial_cts	= "00";
	
	$_text_input_ip1			= "";
	$_text_input_ip2			= "";
	$_text_input_ip3			= "";
	$_text_input_ip4			= "";
	
	$_text_input_community		= "public";
	
	// HTML Form AGGIUNGI Periferica
	$_panel_form_html = codeInit()
		// Campi in alto a SX
		.htmlPanelFormSearch( $_id_panel,	'type',			0,	5,	88,	'Tipo',				394, $_text_select_type )
		.htmlPanelFormSelect( $_id_panel,	'location',		0,	35,	88,	'Ubicazione',		280, $_text_select_location,	null,	"display: none", true ) 
		.htmlPanelFormInput( $_id_panel,	'position',		378,35,	62,	'Posizione',		40,  $_text_info_position,		null,	"display: none", true )
		.htmlPanelFormSelect( $_id_panel,	'port',			0,	65,	88,	'Collegamento',		200, $_text_select_port,		null,	"display: none", true )
		
		// Campi in alto a DX
		.htmlPanelFormInput( $_id_panel,	'name',			493,5,	48,	'Nome',				190, $_text_info_name,			null,	"display: none" )
		.htmlPanelFormInput( $_id_panel,	'sn',			493,35,	48,	'N.Serie',			190, $_text_info_sn,			null,	"display: none" )
		.htmlPanelFormInput( $_id_panel,	'md_id',		493,95,	48,	'ID IeC',			190, $_text_info_md_id,			null,	"display: none" , false, 'admin,IeC_installer' )
		
		// Indirizzo Seriale
		.htmlPanelFormSelect( $_id_panel,	'serial_size',	298,65,	87,	'Indirizzo: Size',	50, $_text_select_serial_size,	null,	"display: none", true )
		.htmlPanelFormSelect( $_id_panel,	'serial_modem',	446,65,	43,	'Modem',			50, $_text_select_serial_modem,	null,	"display: none", true )
		.htmlPanelFormSelect( $_id_panel,	'serial_addr',	550,65,	40,	'Addr',				50, $_text_select_serial_addr,	null,	"display: none", true )
		
		// Indirizzo CTS
		.htmlPanelFormSelect( $_id_panel,	'cts_addr',		651,65,	30,	'CTS',				50, $_text_select_serial_cts,	null,	"display: none", true ) // Da 00 a 99
		
		// Indirizzo IP
		.htmlPanelFormInput( $_id_panel,	'ip1',			307,65,	17,	'IP',				30, $_text_input_ip1,			null,	"display: none" )
		.htmlPanelFormInput( $_id_panel,	'ip2',			365,65,	1,	'.',				30, $_text_input_ip2,			null,	"display: none" )
		.htmlPanelFormInput( $_id_panel,	'ip3',			407,65,	1,	'.',				30, $_text_input_ip3,			null,	"display: none" )
		.htmlPanelFormInput( $_id_panel,	'ip4',			449,65,	1,	'.',				30, $_text_input_ip4,			null,	"display: none" )
		
		// Community
		.htmlPanelFormInput( $_id_panel,	'community',	493,65,	68,	'Community',		170, $_text_input_community,	null,	"display: none" )
		.codeChr(1,6).htmlDivitis('device_add_panel_form_rack'	,'details_panel_form_rack'	,"", "display: none;")
		;
	
	// HTML Bottoni Form Edit Periferica
	$_panel_form_btns_html = codeInit()
		// Bottone Save (AUTH)
		.htmlPanelButton($_id_panel_form,$_id_button_save,"add_panel_button",80,110,'ok',null,false,'admin,IaP_installer,IeC_installer,installer', "Aggiungi periferica" )
		// Bottone Cancel (AUTH)
		.htmlPanelButton($_id_panel_form,$_id_button_cancel,"add_panel_button",45,110,'cancel',null,false,'admin,IaP_installer,IeC_installer,installer', "Ripristina campi")
		;
	// HTML Pannello Dettaglio Periferica	
	$_html = codeInit()
		.codeChr(1,5).htmlDivitis($_id_panel_form			,'add_panel_form'		,$_panel_form_html)
		.codeChr(1,5).htmlDivitis($_id_panel_form."_btns"	,'add_panel_form_btns'	,$_panel_form_btns_html)
		;
	
	$r_id = $_id_panel;
	$r_height = 150;
	if (isset($_device['Type']))
		$r_image = $_base.'s/'.$_device['Type'].'_100.png'; 
	else
		$r_image = 'gui/sk_transparent.png';
	
	return($_html);
}

/**
*
*/
function devicesPageBuildRackAddHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_conf_devices 	= $_configuration["devices"];
	
	// Calcolo i valori di default
	
	// Preparo gli ID
	$_base				= "rack";
	$_id_panel			= $_base."_add_panel";
	$_id_panel_form		= $_base."_add_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	// Preparo i testi
	$_text_info_name		= "";
	$_text_info_note		= "Nessuna";
	$_text_select_building	= "";
	$_text_select_type		= "Armadio ATPS 24";
	
	// HTML Form Add Armadio
	$_panel_form_html = codeInit()
		// Taglio in alto centrale
		.htmlPanelVerticalCut('left',485,0,100)
		// Informazioni in alto a SX
		.htmlPanelFormInput( $_id_panel,	'name',		0,	5,	65,	'Nome', 	405, $_text_info_name)
		.htmlPanelFormSelect( $_id_panel,	'building',	0,	35,	65,	'Edificio',	405, $_text_select_building)
		.htmlPanelFormInput( $_id_panel,	'note',		0,	65,	65,	'Note',		405, $_text_info_note)
		// Informazioni in alto a DX
		.htmlPanelFormSelect( $_id_panel, 	'type',		490,5,	65,	'Tipo',		170, $_text_select_type)
		;
	// HTML Bottoni Form Add Armadio
	$_panel_form_btns_html = codeInit()
		// Bottone Save (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_save,   "add_panel_button",80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Aggiungi armadio" )
		// Bottone Cancel (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_cancel, "add_panel_button",45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Ripristina campi" )
		;
	// HTML Pannello Dettaglio Armadio	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'add_panel_form'		, $_panel_form_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'add_panel_form_btns'	, $_panel_form_btns_html )
		;
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	//$r_image 	= $_base.'s/ATPS24_100.png';
	$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 120;
	$_html 		= $_inner_html;
	
	return($_html);
}

/**
*
*/
function devicesPageBuildBuildingAddHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_conf_devices 	= $_configuration["devices"];
	
	// Calcolo i valori di default
	
	// Preparo gli ID
	$_base				= "building";
	$_id_panel			= $_base."_add_panel";
	$_id_panel_form		= $_base."_add_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	// Preparo i testi
	$_text_info_name		= "Fabbricato Viaggiatori";
	$_text_select_station	= "";
	$_text_info_note		= "Nessuna";
	$_text_select_type		= "Fabbricato Viaggiatori";
	
	// HTML Form Add Edificio
	$_panel_form_html = codeInit()
		// Taglio in alto centrale
		.htmlPanelVerticalCut('left',485,0,100)
		// Informazioni in alto a SX
		.htmlPanelFormInput( $_id_panel,	'name',		0,	5,	65,	'Nome', 	405, $_text_info_name)
		.htmlPanelFormSelect( $_id_panel,	'station',	0,	35,	65,	'Stazione',	405, $_text_select_station)
		.htmlPanelFormInput( $_id_panel,	'note',		0,	65,	65,	'Note',		405, $_text_info_note)
		// Informazioni in alto a DX
		.htmlPanelFormSelect( $_id_panel, 	'type',		490,5,	65,	'Tipo',		170, $_text_select_type)
		;
	// HTML Bottoni Form Add Edificio
	$_panel_form_btns_html = codeInit()
		// Bottone Save (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_save,   "add_panel_button",80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Aggiungi edificio" )
		// Bottone Cancel (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_cancel, "add_panel_button",45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Ripristina campi" )
		;
	// HTML Pannello Dettaglio Edificio	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis( $_id_panel_form			, 'add_panel_form'		, $_panel_form_html )
		.codeChr(1,5).htmlDivitis( $_id_panel_form."_btns"	, 'add_panel_form_btns'	, $_panel_form_btns_html )
		;
	
	// Preparo il risultato finale
	$r_id 		= $_id_panel;
	//$r_image 	= $_base.'s/building_100.png';
	$r_image 	= 'gui/sk_transparent.png';
	$r_height 	= 120;
	$_html 		= $_inner_html;
	
	return($_html);
}

/**
*
*/
function devicesPageBuildStationAddHTML($data,&$r_id,&$r_image,&$r_height)
{
	$_html = codeInit();
	
	global $_configuration;
	
	$_data = $data;
	
	// Recupero parametri di configurazione
	$_conf_devices 			= $_configuration["devices"];
	
	$_station_type_list 	= $_configuration['station_type_list'];
	
	// Calcolo il valori di default
	
	// Preparo gli ID
	$_base				= "station";
	$_id_panel			= $_base."_add_panel";
	$_id_panel_form		= $_base."_add_panel_form";
	$_id_button_save 	= "save";
	$_id_button_cancel 	= "cancel";
	
	// Preparo i testi
	$_text_info_region	= "";
	$_text_info_zone	= "";
	$_text_info_node	= "";
	$_text_info_type	= "";
	$_text_info_status	= "";
	$_text_select_name	= "";
	$_text_select_type	= "Stazione";
	$_text_info_offset	= '0';
	
	// HTML Form Edit Stazione
	$_panel_form_html = codeInit()
		// Taglio in alto centrale
		.htmlPanelVerticalCut('left',485,0,100)
		// Informazioni in alto a SX
		.htmlPanelFormSearch( $_id_panel, 'name',	0,	5,	65,	'Stazione', 405, $_text_select_name)
		// Informazioni in alto a DX
		.htmlPanelFormSelect( $_id_panel, 'type',	490,5,	65,	'Tipo',		170, $_text_select_type, null, null, true )
		// ID Offset
		.htmlPanelFormInput( $_id_panel,  'offset',	490,35,	65,	'Offset ID',170, $_text_info_offset )
		;
	// HTML Bottoni Form Edit Stazione
	$_panel_form_btns_html = codeInit()
		// Bottone Save (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_save,   "add_panel_button",80, 80, 'ok',     null, false, 'admin,IaP_installer,IeC_installer,installer', "Aggiungi stazione" )
		// Bottone Cancel (AUTH)
		.htmlPanelButton( $_id_panel_form, $_id_button_cancel, "add_panel_button",45, 80, 'cancel', null, false, 'admin,IaP_installer,IeC_installer,installer', "Ripristina campi" )
		;
	// HTML Pannello Dettaglio Periferica	
	$_inner_html = codeInit()
		.codeChr(1,5).htmlDivitis($_id_panel_form			,'add_panel_form'		,$_panel_form_html)
		.codeChr(1,5).htmlDivitis($_id_panel_form."_btns"	,'add_panel_form_btns'	,$_panel_form_btns_html)
		;
	
	$r_id 		= $_id_panel;
	$r_height 	= 120;
	//$r_image 	= $_base.'s/station_100.png';
	$r_image 	= 'gui/sk_transparent.png';
	$_html		= $_inner_html;
	
	return($_html);
}

/**
* Funzione per costruire il codice HTML per il pannello per aggiungere elementi
*/
function devicesPageBuildAddPanelHTML($data=null)
{
	logDebug("devicesPageBuildAddPanelHTML()");
	
	$_html = codeInit();
	
	$_data = $data;
	
	// Recupero informazioni sul livello di visualizzazione
	$_level = $_data['level'];
	
	switch ($_level)
	{
		case "rack":
			$_inner_html = devicesPageBuildRackAddHTML($_data,$_id,$_image,$_height);
		break;
		case "building":
			$_inner_html = devicesPageBuildBuildingAddHTML($_data,$_id,$_image,$_height);
		break;
		case "station":
			$_inner_html = devicesPageBuildStationAddHTML($_data,$_id,$_image,$_height);
		break;
		case "device":
		default:
			$_inner_html = devicesPageBuildDeviceAddHTML($_data,$_id,$_image,$_height);
		break;
	}
	
	// Genero il codice HTML del pannello
	$_html .= htmlAddPanelBuild($_id,$_image,$_inner_html,$_height);
		
	return($_html);
}

/**
* Funzione per costruire il codice HTML per la pagina periferiche
*/
function devicesPageBuildHTML($params)
{
	global $_configuration;

	$_html = codeInit();
	
	$_html_menu = devicesPageBuildMenuHTML();
	$_html_content = "";//devicesPageBuildContentHTML($params);
	
	$_js_menu = devicesPageBuildMenuJavaScript();
	$_js_content = devicesPageBuildContentJavaScript();
	
	$_html .= htmlBodyBuild($_html_menu,$_js_menu,$_html_content,$_js_content);

	return($_html);
}
?>
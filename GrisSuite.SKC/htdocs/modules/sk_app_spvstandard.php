<?php
/**
* Telefin STLC1000 Consolle
*
* sk_app_spvstandard.php - Modulo per l'applicazione Supervisore Standard.
*
* @author Enrico Alborali
* @version 1.0.4.1 25/03/2014
* @copyright 2011-2014 Telefin S.p.A.
*/

if(!isset($_SERVER['DOCUMENT_ROOT']) || $_SERVER['DOCUMENT_ROOT'] == ''){
	$_SERVER['DOCUMENT_ROOT'] = '.';
}
require_once($_SERVER['DOCUMENT_ROOT'].'/conf/sk_config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_class_profile.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_class_activity.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/lib/lib_profile.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/conf/sk_profiles.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/sk_app_stlcManagerService.php');

/**
* Funzione per estrarre la configurazione di una porta di comunicazione
*/
function spvstandardExtractXMLPortConfig($_config_xml)
{
	///global $_tt_asterisk_manager_server;
	
	$_depth_to_match = 2;
	$_name_to_match = "item";
	
	/*
	<item id="1" name="Porta COM4 (RS485)" type="STLC1000_RS485" com="4" baud="9600" echo="true" data="8" stop="0" parity="N" timeout="500" persistent="true"/>
	<item id="2" name="SNMP Manager Locale" type="TCP_Client" timeout="450" persistent="false" ip="127.0.0.1" port="5220"/>
	*/
	
	$_item 				= null;
	$_item_id 			= null;
	$_item_type 		= null;
	$_item_name 		= null;
	
	$_item_com			= null;
	$_item_baud			= null;
	$_item_echo			= null;
	$_item_data			= null;
	$_item_stop			= null;
	$_item_parity		= null;
	
	$_item_ip			= null;
	
	$_item_timeout		= null;
	$_item_persistent	= null;
	
	
	if (isset($ttXMLElement) 
		&& $ttXMLElement->depth == $_depth_to_match
		&& $ttXMLElement->name == $_name_to_match)
	{
		$_item_id 		= $ttXMLElement->getAttribute('id');
		$_item_type 	= $ttXMLElement->getAttribute('type');
		$_item_name 	= $ttXMLElement->getAttribute('name');
		
		$_item_com 		= $ttXMLElement->getAttribute('com');
		$_item_baud 	= $ttXMLElement->getAttribute('baud');
		$_item_echo		= $ttXMLElement->getAttribute('echo');
		$_item_data		= $ttXMLElement->getAttribute('data');
		$_item_stop 	= $ttXMLElement->getAttribute('stop');
		$_item_parity 	= $ttXMLElement->getAttribute('parity');
		
		$_item_ip		= $ttXMLElement->getAttribute('ip');
		
		$_item_timeout 		= $ttXMLElement->getAttribute('timeout');
		$_item_persistent	= $ttXMLElement->getAttribute('persistent');
		
		if (isset($_item_id))
		{
			if (!isset($_item_type))
				$_item_type = "TEL";
			if (!isset($_item_mode))
				$_item_mode = "panel";
			if (!isset($_item_name))
				$_item_name = "Tel.";
			if (!isset($_item_tag))
				$_item_tag = "";
			if (isset($_item_visible) && ($_item_visible == "true" || $_item_visible == "TRUE"))
				$_item_visible = true;
			else
				$_item_visible = false;
			if (!isset($_item_x))
				$_item_x = 50;
			if (!isset($_item_y))
				$_item_y = 50;
			if (!isset($_item_size))
				$_item_size = 1;
			if (!isset($_item_layout))
				$_item_layout = "top_left";
			if (!isset($_item_width))
				$_item_width = 200;
			if (!isset($_item_height))
				$_item_height = 200;
			if (!isset($_item_pbx))
				$_item_pbx = $_tt_asterisk_manager_server; // Se non lo specifico utilizzo il manager Asterisk di default
								
			//DEBUG: print($_item_id.":".$_item_type.",".$_item_name.",".$_item_tag.",".$_item_visible.",".$_item_x.",".$_item_y.",".$_item_layout.",".$_item_width.",".$_item_height);
								
			$_item = new ttItem($_item_id,$_item_type,$_item_mode,$_item_name,$_item_tag,$_item_visible,$_item_x,$_item_y,$_item_size,$_item_layout,$_item_width,$_item_height,$_item_pbx,$_item_exten);
		}
	}
	
	return($_item);
}

function urlExists($url=NULL){  
    if($url == NULL) return false;  
    $ch = curl_init($url);  
    curl_setopt($ch, CURLOPT_TIMEOUT, 2);  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $data = curl_exec($ch);  
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
    curl_close($ch);  
    if($httpcode>=200 && $httpcode<300){  
        return true;  
    } else {  
        return false;  
    }  
}

/**
 * Funzione per auto configurare le porte seriali in base al profilo hardware e ai parametri di default
 */
function spvstandardAutoConfigureSerialPorts($_port_type_list,$_ports=null,$_port_profile_string='{"ports": [{"id": 1, "name": "Porta COM1", "com": 1, "type": "RS232"},{"id": 2, "name": "Porta COM2", "com": 2, "type": "RS232"},{"id": 3, "name": "Porta COM3", "com": 3, "type": "RS485_STLC1000"},{"id": 4, "name": "Porta COM4", "com": 4, "type": "RS485_STLC1000"}]}'){
	if ($_ports==null)
		$_ports = array();
	
	$_default_profile = json_decode($_port_profile_string, true);
	$_default_ports = $_default_profile['ports'];
	
	if (count($_default_ports)>0){
		foreach ($_default_ports as $_default_port){
			if (isset($_default_port)){
				$_port = new port();
			
				$_port->id		= $_default_port['id'];
				$_port->name	= $_default_port['name'];
				$_port->com 	= $_default_port['com'];
				$_port->type	= $_default_port['type'];
							
				// Recupero i parametri di default per il tipo di porta
				$_port_type	= getPortTypeFromCode($_port_type_list,$_default_port['type']);
				
				if (isset($_port_type)){
				
					$_port->baud 		= $_port_type->defaultBaud;
					$_port->echo 		= $_port_type->defaultEcho;
					$_port->data 		= $_port_type->defaultData;
					$_port->stop 		= $_port_type->defaultStop;
					$_port->parity 		= $_port_type->defaultParity;
					$_port->timeout 	= $_port_type->defaultTimeout;
					$_port->persistent 	= $_port_type->defaultPersistent;
							
					$_ports[] 			= $_port;
				
					$_edit_level 		= 1;
					
					logEvent('Aggiunta automaticamente COM'.$_port->id.': '.serialize($_port),1);
				}
				else{
					logEvent('Impossibile aggiungere automaticamente COM'.$_port->id.': parametri tipo '.$_default_port['type'].' non trovati.',2);
				}
			}
		}
	}
	
	return $_ports;
}

/**
 * Funzione per auto assegnare le porte seriali (da usare durante l'importazione della configurazione del sistema)
 */
function spvstandardAutoMatchSerialPorts($_devices,$_new_ports,$_udploaded_ports,$_serial_ports_import_rule='{"ports_import_rule": [{"from_type": "TCP_Client", "to_type": "TCP_Client"},{"from_type": "RS232", "to_type": "RS232"},{"from_type": "RS485", "to_type": "RS485", "failover_type": "RS422"},{"from_type": "RS422", "to_type": "RS422", "failover_type": "RS485"},{"from_type": "STLC1000_RS485", "to_type": "STLC1000_RS485", "failover_type": "STLC1000_RS422"},{"from_type": "STLC1000_RS422", "to_type": "STLC1000_RS422", "failover_type": "STLC1000_RS485"}]}'){
	$_import_rules = json_decode($_serial_ports_import_rule, true);
	$_ports_import_rules = $_import_rules['ports_import_rule'];
	
	$_matches = array(); //index = from, value = to
	$_messages = '';
	
	logDebug("=== spvstandardAutoMatchSerialPorts ===");
	logDebug('Importo '.count($_udploaded_ports).' porte assegnandole a '.count($_new_ports).' porte usando '.count($_ports_import_rules).' regole');
	// Scorro le porte importate
	foreach ($_udploaded_ports as $_uploaded_port){
		$_port_matched = false;
		$_uploaded_port_type = $_uploaded_port->type;
		$_uploaded_port_id = $_uploaded_port->id;
		logDebug('Importo la porta '.$_uploaded_port->name);
		// Cerco un match (verificando il tipo) nelle regole
		foreach ($_ports_import_rules as $_rule){
			$_from_type = $_rule['from_type'];
			$_to_type 	= $_rule['to_type'];
			logDebug('Provo con la regola di match '.$_from_type.' => '.$_to_type.'.');
			// Se il tipo di provenienza corrisponde
			if ($_from_type == $_uploaded_port_type){
				// Cerco una corrispondenza giusta nelle porte nuove
				foreach ($_new_ports as $_new_port){
					// Se il tipo nuovo corrisponde
					$_new_port_type = $_new_port->type;
					$_new_port_id = $_new_port->id;
					$_new_port_name = $_new_port->name;
					logDebug('Cerco di assegnare la porta ID:'.$_uploaded_port_id.' alla porta ID:'.$_new_port_id.'.');
					if ($_to_type == $_new_port_type){
						$_port_used = false;
						// Controllo se la porta nuova è stata già assegnata
						foreach ($_matches as $_match_from => $_match_to){
							if ($_match_to == $_new_port_id){
								$_port_used = true;
							}
						}
						if (!$_port_used){
							$_matches[$_uploaded_port_id] = $_new_port_id;
							$_port_matched = true;
							logEvent('Assegnata la porta importata ID:'.$_uploaded_port_id.' alla porta '.$_new_port_name.' ID:'.$_new_port_id.'.',0);
						}
					}
					if ($_port_matched) break;
				}
				// Se non ho trovato una corrispondenza scalo sul tipo alternativo
				if (!$_port_matched){
					$_failover_type 	= $_rule['failover_type'];
					$_to_type 			= $_rule['to_type'];
					// Cerco una corrispondenza giusta nelle porte nuove
					foreach ($_new_ports as $_new_port){
						// Se il tipo nuovo corrisponde
						$_new_port_type = $_new_port->type;
						$_new_port_id = $_new_port->id;
						$_new_port_name = $_new_port->name;
						if ($_failover_type == $_new_port_type){
							$_port_used = false;
							// Controllo se la porta nuova è stata già assegnata
							foreach ($_matches as $_match_from => $_match_to){
								if ($_match_to == $_new_port_id){
									$_port_used = true;
								}
							}
							if (!$_port_used){
								$port_used_by_device = false;
								// controllo se esiste almeno una periferica che utilizza la porta
								foreach ($_devices as $_device_index => $_device){
									if( isset($_device->port) === true ){
										if($_device->port === $_uploaded_port_id){
											$port_used_by_device = true;
											break;
										}
									}
								}
								
								$_matches[$_uploaded_port_id] = $_new_port_id;
								$_port_matched = true;
								
								if( $port_used_by_device === true ){
									/* FAILOVER MATCH */
									$_messages .= 'La porta '.$_new_port_name.' &egrave; utilizzata ma deve essere configurata come '.$_to_type.' per poter funzionare correttamente. ';
									logEvent('Assegnata la porta importata ID:'.$_uploaded_port_id.' alla porta '.$_new_port_name.' ID:'.$_new_port_id.'.',0);
									logEvent('La porta '.$_new_port_name.' è utilizzata ma deve essere configurata come '.$_to_type.' per poter funzionare correttamente.',1);
								}
							}
						}
						if ($_port_matched) break;
					}
				}
			}
			if ($_port_matched) break;
		}
		if (!$_port_matched){
			/* NO MATCH */
			$_messages .= 'Impossibile trovare una porta corrispondente alla '.$_uploaded_port->name.'. ('.$_uploaded_port_type.') importata. ';
			logEvent('Impossibile trovare una porta corrispondente alla '.$_uploaded_port->name.'. ('.$_uploaded_port_type.') importata.',2);
		}
	}
	
	// Scorro le periferiche importate
	foreach ($_devices as $_device_index => $_device){
		$_from_id = $_device->port;
		// Cerco una corrispondenza nelle assegnaizoni di porte
		foreach ($_matches as $_match_from => $_match_to){
			$_device_matched = false;
			if ($_match_from == $_from_id){
				$_device->port = $_match_to;
				$_device_matched = true;
				logEvent('Assegnata la porta (ID:'.$_match_to.') alla periferica '.$_device->name.'.',0);
			}
			if ($_device_matched) break;
		}
		if (!$_device_matched){
			/* NO MATCH */
			$_messages .= 'Impossibile assegnare una porta alla periferica '.$_device->name.'. ';
			logEvent('Impossibile assegnare una porta alla periferica '.$_device->name.'.',2);
		}
	}
	
	if ($_messages != ''){
		$_message_params['bar'] = 'topbar';
		$_message_params['message'] = 'ATTENZIONE! '.$_messages;
		$_message_params['color'] = 'red';
		coreSetMessageToSession($_message_params);
	}
	
	return $_devices;
}

/**
* Funzione per caricare la configurazione dell'applicazione
*/       
function spvstandardLoadXMLConfig($_config_xml)
{
	global $_conf_apps;
	global $_conf_console_app_auto_add_tcpip,$_conf_console_app_auto_add_ethernet;
	global $_conf_console_app_auto_add_serial,$_conf_console_app_hardware_serial_ports,$_conf_console_app_hardware_serial_ports_import_rule;
	global $_conf_console_app_network_import_enabled;
	global $_port_type_list;
	global $_upload_request;
	global $is_cliScript;

	$_conf_app0 = $_conf_apps[0];
	
	logDebug("=== spvstandardLoadXMLConfig ===");
		
	$_configuration	= array();
	
	$_node_depth	= 0;
	$_node_type		= null;
	$_node_name		= "";
	$_node_value	= "";
	
	$_section_telefin 		= false;
	$_section_port			= false;
	$_section_port_item 	= false;
	$_section_network		= false;
	$_section_network_item 	= false;
	$_section_topography	= false;
	$_section_station		= false;
	$_section_building		= false;
	$_section_location		= false;
	$_section_server		= false;
	$_section_region		= false;
	$_section_zone			= false;
	$_section_node			= false;
	$_section_device		= false;	
	
	$_port_count 		= 0;
	$_network_count		= 0;
	$_station_count 	= 0;
	$_building_count	= 0;
	$_location_count	= 0;
	$_server_count 		= 0;
	$_region_count 		= 0;
	$_zone_count 		= 0;
	$_node_count 		= 0;
	$_device_count 		= 0;
	
	$_node_depth 	= 1;
	
	$_ports 	= array();
	$_network 	= array();
	$_stations 	= array();
	$_buildings = array();
	$_locations = array();
	$_servers 	= array();
	$_regions 	= array();
	$_zones 	= array();
	$_nodes 	= array();
	$_devices 	= array();
	
	// Download automatico Region List
	$_remote_region_list_url = $_conf_app0["remote_region_list_url"];
	$_remote_region_list_path = $_conf_app0["remote_region_list_path"];
	$_local_region_list_path = $_conf_app0["region_list_path"];
	
	if(!isset($is_cliScript) || $is_cliScript === false){
		session_start();
	}
	
	//logEvent("Variabile _remote_region_list_url (".$_remote_region_list_url.")",1);
	
	if (isset($_remote_region_list_url) && $_remote_region_list_url != ''){
	$_test = urlExists($_remote_region_list_url);
	
	//logEvent("Variabile _test region_list (".$_test.")",1);
	//$_test = true;
	
	if ($_test) $_remote_region_list_xml = file_get_contents($_remote_region_list_url,FILE_TEXT);
	else $_remote_region_list_xml = false;
	
	if ($_remote_region_list_xml !== false)
	{
		$_result = file_put_contents($_remote_region_list_path,$_remote_region_list_xml);
		if ($_result !== false)
		{
			$_local_region_list_md5 = md5_file($_local_region_list_path);
			$_remote_region_list_md5 = md5_file($_remote_region_list_path);
			if ($_local_region_list_md5 != $_remote_region_list_md5)
			{
				$_result = file_put_contents($_local_region_list_path,$_remote_region_list_xml);
				logEvent("Aggiornato file Region List da server remoto.",0);
			}
			$_SESSION['local_region_list'] = false;
		}
		else{
			$_SESSION['local_region_list'] = true;
			logEvent("Impossibile salvare in locale (".$_remote_region_list_path.") il file Region List remoto.",1);
		}
	}
	else
	{
		$_SESSION['local_region_list'] = true;
		logEvent("Impossibile scaricare file Region List da server remoto (".$_remote_region_list_url.").",1);
	}
	}

	// Download automatico Device Type List
	$_remote_device_type_list_url = $_conf_app0["remote_device_type_list_url"];
	$_remote_device_type_list_path = $_conf_app0["remote_device_type_list_path"];
	$_local_device_type_list_path = $_conf_app0["device_type_list_path"];
	
	if (isset($_remote_device_type_list_url) && $_remote_device_type_list_url != ''){
	$_test = urlExists($_remote_device_type_list_url);
	
	//logEvent("Variabile _test device_type (".$_test.")",1);
	//$_test = true;
	
	if ($_test) $_remote_device_type_list_xml = file_get_contents($_remote_device_type_list_url,FILE_TEXT);
	else $_remote_device_type_list_xml = false;
	
	if ($_remote_device_type_list_xml !== false)
	{
		$_result = file_put_contents($_remote_device_type_list_path,$_remote_device_type_list_xml);
		if ($_result !== false)
		{
			$_local_device_type_list_md5 = md5_file($_local_device_type_list_path);
			$_remote_device_type_list_md5 = md5_file($_remote_device_type_list_path);
			if ($_local_device_type_list_md5 != $_remote_device_type_list_md5)
			{
				$_result = file_put_contents($_local_device_type_list_path,$_remote_device_type_list_xml);
				logEvent("Aggiornato file Device Type List da server remoto.",0);
			}
			$_SESSION['local_device_type_list'] = false;
		}
		else{
			$_SESSION['local_device_type_list'] = true;
			logEvent("Impossibile salvare in locale (".$_remote_device_type_list_path.") il file Device Type List remoto.",1);
		}
	}
	else
	{
		$_SESSION['local_device_type_list'] = true;
		logEvent("Impossibile scaricare file Device Type List da server remoto (".$_remote_device_type_list_url.").",1);
	}
	}

	if(!isset($is_cliScript) || $is_cliScript === false){
		session_write_close();
	}

	while ($_config_xml->read() && $_node_depth != 0)
	{
		if ($_config_xml->nodeType == XMLReader::ELEMENT)
		{
			// <telefin>
			if ($_config_xml->depth == 0
				&& $_config_xml->name == "telefin")
			{
				logDebug("<telefin> found");
				$_section_telefin = true;
			}
			
			// === PORTE DI COMUNICAZIONE ===
			// <port>
			else if ($_section_telefin == true
					&& $_config_xml->depth == 1
					&& $_config_xml->name == "port")
			{
				logDebug("<port> found");
				$_section_port = true;
								
				
			}
			// <item>
			else if ($_section_port == true
					&& $_config_xml->depth == 2
					&& $_config_xml->name == "item")
			{
				logDebug("<item> found");
				$_section_port_item = true;
					
				$_port = new port();
								
				if (isset($_port) && $_port != null)
				{
					$_port->extractXMLConfig($_config_xml);
					$_ports[$_port_count] = $_port;
					$_port_count++;
					logDebug("New port object: ".serialize($_port));
				}
			}
			
			// === NETWORK ===
			// <network>
			else if ($_section_telefin == true
					&& $_config_xml->depth == 1
					&& $_config_xml->name == "network")
			{
				logDebug("<network> found");
				$_section_network = true;
								
				
			}
			// <item>
			else if ($_section_network == true
					&& $_config_xml->depth == 2
					&& $_config_xml->name == "item")
			{
				logDebug("<item> found");
				$_section_network_item = true;
					
				$_port = new port();
								
				if (isset($_port) && $_port != null)
				{
					$_port->extractXMLConfig($_config_xml);
					$_network[$_network_count] = $_port;
					$_network_count++;
					logDebug("New network object: ".serialize($_port));
				}
			}
			
			// === TOPOGRAFIA ===
			// <topography>
			else if ($_section_telefin == true
					&& $_config_xml->depth == 1
					&& $_config_xml->name == "topography")
			{
				logDebug("<topography> found");
				$_section_topography = true;
				
			
			}
			// <station>
			else if ($_section_topography == true
					&& $_config_xml->depth == 2
					&& $_config_xml->name == "station")
			{
				logDebug("<station> found");
				$_section_station = true;
				
				$_station = new station();
				
				if (isset($_station) && $_station != null)
				{
					$_station->extractXMLConfig($_config_xml);
					$_stations[$_station_count] = $_station;
					$_station_count++;
					logDebug("New station object: ".serialize($_station));
				}
			}
			// <building>
			else if ($_section_station == true
					&& $_config_xml->depth == 3
					&& $_config_xml->name == "building")
			{
				logDebug("<building> found");
				$_section_building = true;
				
				$_building = new building();
				
				if (isset($_building) && $_building != null)
				{
					$_building->extractXMLConfig($_config_xml);
					$_building->stationId = $_station->id;
					$_buildings[$_building_count] = $_building;
					$_building_count++;
					logDebug("New building object: ".serialize($_building));
				}
			}
			// <location>
			else if ($_section_building == true
					&& $_config_xml->depth == 4
					&& $_config_xml->name == "location")
			{
				logDebug("<location> found");
				$_section_location = true;
				
				$_location = new rack();
				
				if (isset($_location) && $_location != null)
				{
					$_location->extractXMLConfig($_config_xml);
					$_location->stationId = $_station->id;
					$_location->buildingId = $_building->id;
					$_locations[$_location_count] = $_location;
					$_location_count++;
					logDebug("New location (rack) object: ".serialize($_location));
				}
			}
			
			// === SISTEMA DIAGNOSTICATO ===
			// <system>
			else if ($_section_telefin == true
					&& $_config_xml->depth == 1
					&& $_config_xml->name == "system")
			{
				logDebug("<system> found");
				$_section_system = true;
				
				
			}
			// <server>
			else if ($_section_system == true
					&& $_config_xml->depth == 2
					&& $_config_xml->name == "server")
			{
				logDebug("<server> found");
				$_section_server = true;
				
				$_server = new server();
				
				if (isset($_server) && $_server != null)
				{
					$_server->extractXMLConfig($_config_xml);
					$_servers[$_server_count] = $_server;
					$_server_count++;
					logDebug("New server object: ".serialize($_server));
				}
			}
			// <region>
			else if ($_section_server == true
					&& $_config_xml->depth == 3
					&& $_config_xml->name == "region")
			{
				logDebug("<region> found");
				$_section_region = true;
				
				$_region = new region();
				
				if (isset($_region) && $_region != null)
				{
					$_region->extractXMLConfig($_config_xml);
					$_region->serverId = $_server->id;
					$_regions[$_region_count] = $_region;
					$_region_count++;
					logDebug("New region object: ".serialize($_region));
				}
			}
			// <zone>
			else if ($_section_region == true
					&& $_config_xml->depth == 4
					&& $_config_xml->name == "zone")
			{
				logDebug("<zone> found");
				$_section_zone = true;
				
				$_zone = new zone();
				
				if (isset($_zone) && $_zone != null)
				{
					$_zone->extractXMLConfig($_config_xml);
					$_zone->serverId = $_server->id;
					$_zone->regionId = $_region->id;
					$_zones[$_zone_count] = $_zone;
					$_zone_count++;
					logDebug("New zone object: ".serialize($_zone));
				}
			}
			// <node>
			else if ($_section_zone == true
					&& $_config_xml->depth == 5
					&& $_config_xml->name == "node")
			{
				logDebug("<node> found");
				$_section_node = true;
				
				$_node = new node();
				
				if (isset($_node) && $_node != null)
				{
					$_node->extractXMLConfig($_config_xml);
					$_node->serverId = $_server->id;
					$_node->regionId = $_region->id;
					$_node->zoneId = $_zone->id;
					$_nodes[$_node_count] = $_node;
					$_node_count++;
					logDebug("New node object: ".serialize($_node));
				}
			}
			// <device>
			else if ($_section_node == true
					&& $_config_xml->depth == 6
					&& $_config_xml->name == "device")
			{
				logDebug("<device> found");
				$_section_device = true;
				
				$_device = new device();
				
				if (isset($_device) && $_device != null)
				{
					$_device->extractXMLConfig($_config_xml);
					$_device->serverId = $_server->id;
					$_device->regionId = $_region->id;
					$_device->zoneId = $_zone->id;
					$_device->nodeId = $_node->id;
					$_devices[$_device_count] = $_device;
					$_device_count++;
					logDebug("New device object: ".serialize($_device));
				}
			}
			// <scheduler>
			else if ($_section_telefin == true
					&& $_config_xml->depth == 1
					&& $_config_xml->name == "scheduler")
			{
				logDebug("<scheduler> found");
				
				$_scheduler = $_config_xml->readOuterXML();
					
				logDebug("Extracted scheduler XML: ".serialize($_scheduler));
			}
			// <database>
			else if ($_section_telefin == true
					&& $_config_xml->depth == 1
					&& $_config_xml->name == "database")
			{
				logDebug("<database> found");
				
				$_database = $_config_xml->readOuterXML();
					
				logDebug("Extracted database XML: ".serialize($_database));
			}
			
			$_node_depth++;
		}
		if ($_config_xml->nodeType == XMLReader::END_ELEMENT)
		{
			// </telefin>
			if ($_section_telefin == true
				&& $_config_xml->depth == 0
				&& $_config_xml->name == "telefin")
			{
				logDebug("</telefin> found");
				$_section_telefin = false;
			}
			
			// === PORTE DI COMUNICAZIONE ===
			// </port>
			else if ($_section_port == true
					 && $_config_xml->depth == 1
					 && $_config_xml->name == "port")
			{
				logDebug("</port> found");
				$_section_port = false;
				$_section_port_item = false;
			}
			// </item>
			else if ($_section_port_item == true
					&& $_config_xml->depth == 2
					 && $_config_xml->name == "item")
			{
				logDebug("</item> found");
				$_section_port_item = false;
			}
			// === NETWORK ===
			// </network>
			else if ($_section_network == true
					 && $_config_xml->depth == 1
					 && $_config_xml->name == "network")
			{
				logDebug("</network> found");
				$_section_network = false;
				$_section_network_item = false;
			}
			// </item>
			else if ($_section_network_item == true
					&& $_config_xml->depth == 2
					 && $_config_xml->name == "item")
			{
				logDebug("</item> found");
				$_section_network_item = false;
			}
			
			// === TOPOGRAFIA ===
			// </topography>
			else if ($_section_topography == true
					&& $_config_xml->depth == 1
					 && $_config_xml->name == "topography")
			{
				logDebug("</topography> found");
				$_section_topography = false;
			}
			// </station>
			else if ($_section_station == true
					&& $_config_xml->depth == 2
					 && $_config_xml->name == "station")
			{
				logDebug("</station> found");
				$_section_station = false;
			}
			// </building>
			else if ($_section_building == true
					&& $_config_xml->depth == 3
					 && $_config_xml->name == "building")
			{
				logDebug("</building> found");
				$_section_building = false;
			}
			// </location>
			else if ($_section_location == true
					&& $_config_xml->depth == 4
					 && $_config_xml->name == "location")
			{
				logDebug("</location> found");
				$_section_location = false;
			}
			
			// === SISTEMA DIAGNOSTICATO ===
			// </system>
			else if ($_section_system == true
					&& $_config_xml->depth == 1
					 && $_config_xml->name == "system")
			{
				logDebug("</system> found");
				$_section_system = false;
			}
			// </server>
			else if ($_section_server == true
					&& $_config_xml->depth == 2
					 && $_config_xml->name == "server")
			{
				logDebug("</server> found");
				$_section_server = false;
			}
			// </region>
			else if ($_section_region == true
					&& $_config_xml->depth == 3
					 && $_config_xml->name == "region")
			{
				logDebug("</region> found");
				$_section_region = false;
			}
			// </zone>
			else if ($_section_zone == true
					&& $_config_xml->depth == 4
					 && $_config_xml->name == "zone")
			{
				logDebug("</zone> found");
				$_section_zone = false;
			}
			// </node>
			else if ($_section_node == true
					&& $_config_xml->depth == 5
					 && $_config_xml->name == "node")
			{
				logDebug("</node> found");
				$_section_node = false;
			}
			// </device>
			else if ($_section_device == true
					&& $_config_xml->depth == 6
					 && $_config_xml->name == "device")
			{
				logDebug("</device> found");
				$_section_device = false;
			}
			
			$_node_depth--;
		}
	}

	// Link automatico station->node (in caso di system.xml vecchio)
	stationLinkNodes($_stations,$_nodes,$_devices);
	
	$_info = array();
	$_edit_level = 0;
	
	// Autogenerazione configurazione porte ethernet (solo se non sono configurate porte Ethernet)
	if ($_conf_console_app_auto_add_ethernet !==false && count($_network) == 0){
		logEvent("Configurazione porte Ethernet non trovata. Avvio rilevamento automatico.",1);
		if(isAliveStlcManagerService() === true){
			$_client = soapConnect("STLCManager.Service.ClientComm/ClientCommandService");
		}

		if (isset($_client)){
			// Preparo i parametri per la richiesta SOAP
			$_request->clientType = 'SKC';
			$_params->request = $_request;

			// Chiamo il metodo SOAP con i parametri che ho generato
			$_result = $_client->getSTLCNetwork($_params);
				
			if (isset($_result)){
				// Recupero i dati del risultato
				$_result_data = $_result->getSTLCNetworkResult;
	
				if (isset($_result_data)){
					if ($_result_data->status->level == "SUCCESS"){
						$_network_data = $_result_data->data;
				
						$_network_eth_list = $_network_data->Network->STLCNetwork;
				
						// === Recupero numero ETH ===
						$_eth_count = coreExtractEthernetCount($_network_eth_list);
					
						logEvent("Rilevate ".$_eth_count." porte Ethernet.",0);
								
						for ($_e = 1; $_e <= $_eth_count; $_e++) {
							if ($_eth_count == 1) $_eth = $_network_eth_list;
							else $_eth = $_network_eth_list[$_e-1];
					
							logEvent("Rilevamento automatico porta ".$_e.".",0);
					
							$_eth_params = coreExtractEthernetParams($_eth);
							$_eth_port_num = str_replace( 'ETH', '', $_eth->Name );
					
							$_port = new port(); // Istanzio una nuova porta
							$_port->id			= ($_e-1);
							$_port->type		= "ETH";
							$_port->name		= "Interfaccia Ethernet ".$_eth->Name;
			
							$_port->port		= $_eth_port_num;
						
							$_port->auto		= $_eth_params['auto'];
							
							if ($_port->auto == 'true'){
								$_port->ip 		= 'auto';
								$_port->subnet	= '';
								$_port->ip2		= '';
								$_port->subnet2	= '';
								$_port->ip3		= '';
								$_port->subnet3	= '';
								$_port->ip4		= '';
								$_port->subnet4	= '';
								$_port->gateway	= '';
							}
							else{
								if (isset($_eth_params['ip']))
									$_port->ip			= codeMAddressToIp($_eth_params['ip']->m_Address);
								else
									$_port->ip			= $_SERVER["SERVER_ADDR"];
								if (isset($_eth_params['mask']))
									$_port->subnet		= codeMAddressToIp($_eth_params['mask']->m_Address);
								else
									$_port->subnet		= "255.255.255.0";
									
								if (isset($_eth_params['ip2'])){
									$_port->ip2			= codeMAddressToIp($_eth_params['ip2']->m_Address);
									if (isset($_eth_params['mask']))
										$_port->subnet2		= codeMAddressToIp($_eth_params['mask2']->m_Address);
									else
										$_port->subnet2		= "255.255.255.0";
								}
								if (isset($_eth_params['ip3'])){
									$_port->ip3			= codeMAddressToIp($_eth_params['ip3']->m_Address);
									if (isset($_eth_params['mask']))
										$_port->subnet3		= codeMAddressToIp($_eth_params['mask3']->m_Address);
									else
										$_port->subnet3		= "255.255.255.0";
								}
								if (isset($_eth_params['ip4'])){
									$_port->ip4			= codeMAddressToIp($_eth_params['ip4']->m_Address);
									if (isset($_eth_params['mask']))
										$_port->subnet4		= codeMAddressToIp($_eth_params['mask4']->m_Address);
									else
										$_port->subnet4		= "255.255.255.0";
								}
								if (isset($_eth_params['gw']))
									$_port->gateway		= codeMAddressToIp($_eth_params['gw']->m_Address);
								else
									$_port->gateway		= "";
							}
														
							if (isset($_eth_params['dns1']))
								$_port->dns1 		= codeMAddressToIp($_eth_params['dns1']->m_Address);
							else
								$_port->dns1		= "";
							if (isset($_eth_params['dns2']))
								$_port->dns2		= codeMAddressToIp($_eth_params['dns2']->m_Address);
							else
								$_port->dns2		= "";
			
							$_network[] = $_port; // Aggiungo la nuova porta in network
							$_edit_level = 2;
			
							logEvent("Aggiunta automaticamente ".$_port->name.": ".serialize($_port),1);
						}
					}else{
						logEvent("Estrazione risultati SOAP getSTLCNetworkResult fallita.",2);
					}
				}else{
					logEvent("Impossibile estrarre risultati SOAP con getSTLCNetworkResult.",2);
				}
			}else{
				logEvent("Nessun risultato ottenuto dalla chiamata SOAP getSTLCNetwork.",2);
			}
		}else{
			logEvent("Impossibile connettersi al Manager in SOAP.",2);
		}
	}

	// Verifico se il numero di porte seriali configurate non sia inferiore a quello del profilo	
	if ($_conf_console_app_auto_add_serial !== false){
		$_default_profile = json_decode($_conf_console_app_hardware_serial_ports, true);
		$_default_ports = $_default_profile['ports'];
		if (count($_ports) >= (count($_default_ports)+1))
			$_serial_found = true;
		else
			$_serial_found = false;
		// Se ho fatto un upload di un system.xml ricreo la configurazione delle seriali	
		if ($_upload_request==true){
			$_serial_found = false;
			$_udploaded_ports = $_ports; // Mi copio le porte caricate per un successivo match 
		}
	}
	
	if ($_serial_found == false){
		unset($_ports);
		$_ports = array();
	}
	
	// Autogenerazione configurazione porta Rete TCP/IP
	if ($_conf_console_app_auto_add_tcpip !== false){
		$_snmp_found = false;
		if (count($_ports) > 0){
			if (isset($_ports)){
				foreach ($_ports as $_index => $_item){
					if (isset($_item)){
						if ($_item->type == "TCP_Client" && $_item->port == "5220"){
							$_snmp_found = true;
						}
					}
				}
			}
		}
		
		if (!$_snmp_found){
			$_port = new port();
			
			$_port->id			= getFirstFreePortId($_ports);
			$_port->type		= "TCP_Client";
			$_port->name		= "Rete TCP/IP";
			// Parametri TCP
			$_port->ip 			= "127.0.0.1";
			$_port->port 		= "5220";
			$_port->timeout 	= "450";
			$_port->persistent 	= "false";
			
			$_ports[] 			= $_port;
			$_edit_level 		= 1;
			
			logEvent("Aggiunta automaticamente Rete TCP/IP: ".serialize($_port),1);
		}
	}
	
	// Autogenerazione configurazione porte COM
	if ($_serial_found == false){
		$_ports = spvstandardAutoConfigureSerialPorts($_port_type_list,$_ports,$_conf_console_app_hardware_serial_ports);
		
		if ($_upload_request == true)
			$_devices = spvstandardAutoMatchSerialPorts($_devices,$_ports,$_udploaded_ports,$_conf_console_app_hardware_serial_ports_import_rule);
	}
			
	// Autogenerazione configurazione server
	if (count($_servers) == 0){
		$_server = new server();
		
		$_server->defaultXMLConfig();
		
		$_servers[] 	= $_server;
		$_edit_level 	= 2;
		
		logEvent("Aggiunta automaticamente configurazione server: ".serialize($_server),1);
	}
	
	if ($_upload_request==true && $_conf_console_app_network_import_enabled==false){
		unset($_network);
		$_network = array();
	}
	
	$_configuration["ports"]		= $_ports;
	$_configuration["network"]		= $_network;
	$_configuration["stations"]		= $_stations;
	$_configuration["buildings"]	= $_buildings;
	$_configuration["locations"]	= $_locations;
	$_configuration["servers"]		= $_servers;
	$_configuration["regions"]		= $_regions;
	$_configuration["zones"]		= $_zones;
	$_configuration["nodes"]		= $_nodes;
	$_configuration["devices"]		= $_devices;
	$_configuration["scheduler"]	= $_scheduler;
	$_configuration["database"]		= $_database;
	
	$_configuration["edit_level"]	= $_edit_level;
	
	return($_configuration);
}

/**
* Funzione per generare la sezione port della configurazione XML per il supervisore standard
*/
function spvstandardBuildXMLPort($configuration)
{
	$_xml = codeInit();
	
	$_ports = $configuration["ports"];
	
	$_xml_port 	= codeInit();
		
	foreach ($_ports as $_port_index => $_port)
	{
		$_xml_port .= $_port->buildXMLConfig();
	}
	
	$_xml .= codeChr(1,1).'<port>';
	$_xml .= $_xml_port;
	$_xml .= codeChr(1,1).'</port>';
	
	return($_xml);
}

/**
* Funzione per generare la sezione network della configurazione XML per il supervisore standard
*/
function spvstandardBuildXMLNetwork($configuration)
{
	$_xml = codeInit();
	
	$_ports = $configuration["network"];
	
	$_xml_port 	= codeInit();
		
	foreach ($_ports as $_port_index => $_port)
	{
		$_xml_port .= $_port->buildXMLConfig();
	}
	
	$_xml .= codeChr(1,1).'<network>';
	$_xml .= $_xml_port;
	$_xml .= codeChr(1,1).'</network>';
	
	return($_xml);
}

/**
* Funzione per generare la sezione topography della configurazione XML per il supervisore standard
*/
function spvstandardBuildXMLTopography($configuration)
{
	logDebug("=== spvstandardBuildXMLTopography ===");
	
	$_xml = codeInit();
	
	$_stations	= $configuration["stations"];
	$_buildings = $configuration["buildings"];
	$_locations = $configuration["locations"];
	$_devices	= $configuration["devices"];
	
	$_xml_station 	= codeInit();
	$_xml_building 	= codeInit();
	$_xml_location 	= codeInit();
	
	foreach ($_stations as $_station_index => $_station)
	{
		$_xml_station .= $_station->buildXMLConfig($_devices);
		
		foreach ($_buildings as $_building_index => $_building)
		{
	    	if ($_building->stationId == $_station->id)
    		{
    			$_xml_building .= $_building->buildXMLConfig();
    			
    			foreach ($_locations as $_location_index => $_location)
    			{
					if ($_location->stationId == $_station->id && $_location->buildingId == $_building->id)
	    				$_xml_location .= $_location->buildXMLConfig();
	    				$_location_index++;
    			}
    			$_xml_building = str_replace("%LOCATIONS%", $_xml_location, $_xml_building);
    			$_xml_location 	= codeInit();
    		}
	    }
	    $_xml_station = str_replace("%BUILDINGS%", $_xml_building, $_xml_station);
	    $_xml_building 	= codeInit();
	}

	$_xml .= codeChr(1,1).'<topography>';
	$_xml .= $_xml_station;
	$_xml .= codeChr(1,1).'</topography>';
	
	return($_xml);
}

/**
* Funzione per generare la sezione system della configurazione XML per il supervisore standard
*/
function spvstandardBuildXMLSystem($configuration)
{
	$_xml = codeInit();
	
	$_servers	= $configuration["servers"];
	$_regions	= $configuration["regions"];
	$_zones 	= $configuration["zones"];
	$_nodes 	= $configuration["nodes"];
	$_devices 	= $configuration["devices"];
	
	$_xml_server 	= codeInit();
	$_xml_region 	= codeInit();
	$_xml_zone 		= codeInit();
	$_xml_node		= codeInit();
	$_xml_device	= codeInit();
	
	foreach ($_servers as $_server_index => $_server)
	{
		$_xml_server .= $_server->buildXMLConfig();
		
		foreach ($_regions as $_region_index => $_region)
		{
	    	//if ($_region->serverId == $_server->id)
    		{
    			$_xml_region .= $_region->buildXMLConfig();
    			
    			foreach ($_zones as $_zone_index => $_zone)
    			{
    				//if ($_zone->serverId == $_server->id && $_zone->regionId == $_region->id)
					if ($_zone->regionId == $_region->id)
					{
	    				$_xml_zone .= $_zone->buildXMLConfig();
	    				
	    				foreach ($_nodes as $_node_index => $_node)
	    				{
	    					//if ($_node->serverId == $_server->id && $_node->regionId == $_region->id && $_node->zoneId == $_zone->id)
	    					if ($_node->regionId == $_region->id && $_node->zoneId == $_zone->id)
	    					{
	    						$_xml_node .= $_node->buildXMLConfig();
	    						
	    						foreach ($_devices as $_device_index => $_device)
	    						{
	    							//if ($_device->serverId == $_server->id && $_device->regionId == $_region->id && $_device->zoneId == $_zone->id && $_device->nodeId == $_node->id)
	    							if ($_device->regionId == $_region->id && $_device->zoneId == $_zone->id && $_device->nodeId == $_node->id)
	    							{
	    								$_xml_device .= $_device->buildXMLConfig();
	    							}
	    						}
	    						$_xml_node = str_replace("%DEVICES%", $_xml_device, $_xml_node);
	    						$_xml_device	= codeInit();
	    					}
	    				}
	    				$_xml_zone = str_replace("%NODES%", $_xml_node, $_xml_zone);
	    				$_xml_node		= codeInit();
	    			}
    			}
    			$_xml_region = str_replace("%ZONES%", $_xml_zone, $_xml_region);
    			$_xml_zone 		= codeInit();
    		}
	    }
	    $_xml_server = str_replace("%REGIONS%", $_xml_region, $_xml_server);
	    $_xml_region 	= codeInit();
	}

	$_xml .= codeChr(1,1).'<system code="SPV_STLC1000" name="Impianto di Supervisione">';
	$_xml .= $_xml_server;
	$_xml .= codeChr(1,1).'</system>';
	
	return($_xml);
}

/**
* Funzione per generare la sezione scheduler della configurazione XML per il supervisore standard
*/
function spvstandardBuildXMLScheduler($configuration)
{
	global $_profiles;
		
	$_xml = codeChr(1,1).'<scheduler>';
	foreach($_profiles as $profile){
		$_xml .= $profile->buildXMLConfig();
	}
	$_xml .= codeChr(1,1).'</scheduler>';
		
	return($_xml);
}

/**
* Funzione per generare la sezione database della configurazione XML per il supervisore standard
*/
function spvstandardBuildXMLDatabase($configuration)
{
	$_xml = codeInit();
	
	$_database	= $configuration["database"];
	
	$_template = codeChr(1,1).'<database>'
					.codeChr(1,2).'<connection name="Locale" type="MSSQL" version="Express 2005" host="localhost\SQLEXPRESS" user="WindowsAuthentication" password="" persistent="false">'
						.codeChr(1,3).'<schema name="telefin"/>'
					.codeChr(1,2).'</connection>'
				.codeChr(1,1).'</database>';
	
	// if (isset($_database) &&  $_database != "") $_xml = codeChr(1,1).$_database;
	// Se nn e' presente in memoria la configurazione del database utilizzo il template
	//else
	
	// Sovrascrivo sempre la sezione database
	$_xml = $_template;
	
	return($_xml);
}

/**
* Funzione per generare il file di configurazione XML per il supervisore standard
*/
function spvstandardBuildXMLConfig($configuration,$sw="spvstandard")
{
	global $_conf_console_app_name;
	global $_conf_console_app_vendor;
	global $is_cliScript;
	$_user = null;
	$_datetime = date('d/m/Y H:i:s',$_SERVER['REQUEST_TIME']);
	
	if( !isset($is_cliScript) || $is_cliScript === false ){
		$_user = authGetUser();
		$_ip = $_user['ip'];
	} else {
		$_ip = 'localhost';
	}
	
	$_xml = codeInit();
	
	$_xml .= xmlBuildHeader();
	
	$_xml .= codeChr(1,0).'<telefin sw="'.$sw.'" configurator="'.$_conf_console_app_name.'" version="'.getVersion().'" date_time="'.$_datetime.'" vendor="'.$_conf_console_app_vendor.'" gris_username="'.$_user['gris_username'].'" operator_username="'.$_user['username'].'" operator_ip="'.$_ip.'">';
	
	$_xml .= spvstandardBuildXMLPort($configuration);
	$_xml .= spvstandardBuildXMLNetwork($configuration);
	$_xml .= spvstandardBuildXMLTopography($configuration);
	$_xml .= spvstandardBuildXMLSystem($configuration);
	$_xml .= spvstandardBuildXMLScheduler($configuration);
	$_xml .= spvstandardBuildXMLDatabase($configuration);
	
	$_xml .= codeChr(1,0).'</telefin>';
	
	return($_xml);
}

/**
* Funzione per salvare la configurazione dell'applicazione
*/
function spvstandardSaveXMLConfig($xmlConfigUrl,$configuration)
{
	$_xml = codeInit();
	$_result = null;
	
	if (isset($configuration))
	{
		$_xml = spvstandardBuildXMLConfig($configuration);
		
		$_result = file_put_contents($xmlConfigUrl,$_xml);
	}

	return($_result);
}

/**
* Funzione per caricare la region list
*/       
function spvstandardLoadXMLRegionList($_list_xml)
{
	logDebug("=== spvstandardLoadXMLRegionList ===");
		
	$_regions = null;
	
	$_node_depth	= 0;
	$_node_type		= null;
	$_node_name		= "";
	$_node_value	= "";
	
	$_section_telefin 		= false;
	$_section_system		= false;
	$_section_region		= false;
	$_section_zone			= false;
	$_section_node			= false;
	
	$_region_id	= null;
	$_zone_id	= null;
	$_node_id	= null;
		
	$_node_depth 	= 1;
	
	while ($_list_xml->read() && $_node_depth != 0)
	{
		logDebug("XML Read (".$_list_xml->depth."):".$_list_xml->nodeType."=".$_list_xml->name);
		if ($_list_xml->nodeType == XMLReader::ELEMENT)
		{
			// <telefin>
			if ($_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("<telefin> found");
				$_section_telefin = true;
			}
					
			// === SISTEMA DIAGNOSTICATO ===
			// <system>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "system")
			{
				logDebug("<system> found");
				$_section_system = true;
				
				$_regions = array();
			}
			// <region>
			else if ($_section_system == true
					&& $_list_xml->depth == 2
					&& $_list_xml->name == "region")
			{
				logDebug("<region> found");
				$_section_region = true;
				
				$_region_o = new region();
				
				if (isset($_region_o) && $_region_o != null)
				{
					$_region_o->extractXMLConfig($_list_xml,$_list_xml->depth);
					$_region = array();
					$_region['data'] = $_region_o;
					$_zones = array();
					logDebug("New region object: ".serialize($_region));
				}
			}
			// <zone>
			else if ($_section_region == true
					&& $_list_xml->depth == 3
					&& $_list_xml->name == "zone")
			{
				logDebug("<zone> found");
				$_section_zone = true;
				
				$_zone_o = new zone();
				
				if (isset($_zone_o) && $_zone_o != null)
				{
					$_zone_o->extractXMLConfig($_list_xml,$_list_xml->depth);
					$_zone_o->regionId = $_region_o->id;
					$_zone = array();
					$_zone['data'] = $_zone_o;
					$_nodes = array();
					logDebug("New zone object: ".serialize($_zone));
				}
			}
			// <node>
			else if ($_section_zone == true
					&& $_list_xml->depth == 4
					&& $_list_xml->name == "node")
			{
				logDebug("<node/> found");
				$_section_node = true;
				
				$_node_o = new node();
				
				if (isset($_node_o) && $_node_o != null)
				{
					$_node_o->extractXMLConfig($_list_xml,$_list_xml->depth);
					$_node_o->regionId = $_region_o->id;
					$_node_o->zoneId = $_zone_o->id;
					$_node = array();
					$_node['data'] = $_node_o;
					logDebug("New node object: ".serialize($_node));
					$_nodes[] = $_node;
					$_section_node = false;
				}
			}
						
			$_node_depth++;
		}
		if ($_list_xml->nodeType == XMLReader::END_ELEMENT)
		{
			// </telefin>
			if ($_section_telefin == true
				&& $_list_xml->depth == 0
				&& $_list_xml->name == "telefin")
			{
				logDebug("</telefin> found");
				$_section_telefin = false;
			}
			
			// === SISTEMA DIAGNOSTICATO ===
			// </system>
			else if ($_section_system == true
					&& $_list_xml->depth == 1
					 && $_list_xml->name == "system")
			{
				logDebug("</system> found");
				$_section_system = false;
			}
			// </region>
			else if ($_section_region == true
					&& $_list_xml->depth == 2
					 && $_list_xml->name == "region")
			{
				logDebug("</region> found");
				$_section_region = false;
				
				$_region['zone_list'] = $_zones;
				$_regions[] = $_region;
			}
			// </zone>
			else if ($_list_xml->depth == 3
					 && $_list_xml->name == "zone")
			{
				logDebug("</zone> found");
				$_section_zone = false;
				
				$_zone['node_list'] = $_nodes;
				$_zones[] = $_zone;
			}
									
			$_node_depth--;
		}
	}
	
	return($_regions);
}

/**
* Funzione per caricare la station type list
*/       
function spvstandardLoadXMLStationTypeList($_list_xml)
{
	logDebug("=== spvstandardLoadXMLStationTypeList ===");
		
	$_type_list = array();
	
	$_node_depth	= 0;
	$_node_type		= null;
	$_node_name		= "";
	$_node_value	= "";
	
	$_section_telefin 		= false;
	$_section_station_type	= false;
	$_section_type			= false;
	
	$_type_count	= 0;
	
	$_node_depth 	= 1;
	
	while ($_list_xml->read() && $_node_depth != 0)
	{
		if ($_list_xml->nodeType == XMLReader::ELEMENT)
		{
			// <telefin>
			if ($_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("<telefin> found");
				$_section_telefin = true;
			}
			
			// === LISTA ===
			// <station_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "station_type")
			{
				logDebug("<station_type> found");
				$_section_station_type = true;
			}
			// <type>
			else if ($_section_station_type == true
					&& $_list_xml->depth == 2
					&& $_list_xml->name == "type")
			{
				logDebug("<type> found");
				$_section_type = true;
					
				$_type = new station_type($_type_count);
								
				if (isset($_type) && $_type != null)
				{
					$_type->extractXMLConfig($_list_xml);
					$_type_list[$_type_count] = $_type;
					$_type_count++;
					logDebug("New station type object: ".serialize($_type));
				}
			}
									
			$_node_depth++;
		}
		if ($_list_xml->nodeType == XMLReader::END_ELEMENT)
		{
			// </telefin>
			if ($_section_telefin == true
				&& $_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("</telefin> found");
				$_section_telefin = false;
			}
			
			// === LISTA ===
			// </station_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "station_type")
			{
				logDebug("</station_type> found");
				$_section_station_type = false;
				$_section_type = false;
			}
						
			$_node_depth--;
		}
	}
	
	return($_type_list);
}

/**
* Funzione per caricare la building type list
*/       
function spvstandardLoadXMLBuildingTypeList($_list_xml)
{
	logDebug("=== spvstandardLoadXMLBuildingTypeList ===");
		
	$_type_list = array();
	
	$_node_depth	= 0;
	$_node_type		= null;
	$_node_name		= "";
	$_node_value	= "";
	
	$_section_telefin 		= false;
	$_section_building_type	= false;
	$_section_type			= false;
	
	$_type_count	= 0;
	
	$_node_depth 	= 1;
	
	while ($_list_xml->read() && $_node_depth != 0)
	{
		if ($_list_xml->nodeType == XMLReader::ELEMENT)
		{
			// <telefin>
			if ($_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("<telefin> found");
				$_section_telefin = true;
			}
			
			// === LISTA ===
			// <building_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "building_type")
			{
				logDebug("<building_type> found");
				$_section_building_type = true;
			}
			// <type>
			else if ($_section_building_type == true
					&& $_list_xml->depth == 2
					&& $_list_xml->name == "type")
			{
				logDebug("<type> found");
				$_section_type = true;
					
				$_type = new building_type($_type_count);
								
				if (isset($_type) && $_type != null)
				{
					$_type->extractXMLConfig($_list_xml);
					$_type_list[$_type_count] = $_type;
					$_type_count++;
					logDebug("New building type object: ".serialize($_type));
				}
			}
									
			$_node_depth++;
		}
		if ($_list_xml->nodeType == XMLReader::END_ELEMENT)
		{
			// </telefin>
			if ($_section_telefin == true
				&& $_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("</telefin> found");
				$_section_telefin = false;
			}
			
			// === LISTA ===
			// </building_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "building_type")
			{
				logDebug("</building_type> found");
				$_section_building_type = false;
				$_section_type = false;
			}
						
			$_node_depth--;
		}
	}
	
	return($_type_list);
}

/**
* Funzione per caricare la rack type list
*/       
function spvstandardLoadXMLRackTypeList($_list_xml)
{
	logDebug("=== spvstandardLoadXMLRackTypeList ===");
		
	$_type_list = array();
	
	$_node_depth	= 0;
	$_node_type		= null;
	$_node_name		= "";
	$_node_value	= "";
	
	$_section_telefin 		= false;
	$_section_list			= false;
	$_section_type			= false;
	
	$_type_count	= 0;
	
	$_node_depth 	= 1;
	
	while ($_list_xml->read() && $_node_depth != 0)
	{
		if ($_list_xml->nodeType == XMLReader::ELEMENT)
		{
			// <telefin>
			if ($_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("<telefin> found");
				$_section_telefin = true;
			}
			
			// === LISTA ===
			// <rack_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "rack_type")
			{
				logDebug("<rack_type> found");
				$_section_list = true;
			}
			// <type>
			else if ($_section_list == true
					&& $_list_xml->depth == 2
					&& $_list_xml->name == "type")
			{
				logDebug("<type> found");
				$_section_type = true;
					
				$_type = new rack_type($_type_count);
								
				if (isset($_type) && $_type != null)
				{
					$_type->extractXMLConfig($_list_xml);
					$_type_list[$_type_count] = $_type;
					$_type_count++;
					logDebug("New rack type object: ".serialize($_type));
				}
			}
									
			$_node_depth++;
		}
		if ($_list_xml->nodeType == XMLReader::END_ELEMENT)
		{
			// </telefin>
			if ($_section_telefin == true
				&& $_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("</telefin> found");
				$_section_telefin = false;
			}
			
			// === LISTA ===
			// </rack_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "rack_type")
			{
				logDebug("</rack_type> found");
				$_section_list = false;
				$_section_type = false;
			}
						
			$_node_depth--;
		}
	}
	
	return($_type_list);
}

/**
* Funzione per caricare la device type list
*/       
function spvstandardLoadXMLDeviceTypeList($_list_xml)
{
	logDebug("=== spvstandardLoadXMLDeviceTypeList ===");
		
	$_type_list = array();
	
	$_node_depth	= 0;
	$_node_type		= null;
	$_node_name		= "";
	$_node_value	= "";
	
	$_section_telefin 		= false;
	$_section_device_type	= false;
	$_section_type			= false;
	
	$_type_count	= 0;
	
	$_node_depth 	= 1;
	
	while ($_list_xml->read() && $_node_depth != 0)
	{
		if ($_list_xml->nodeType == XMLReader::ELEMENT)
		{
			// <telefin>
			if ($_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("<telefin> found");
				$_section_telefin = true;
			}
			
			// === LISTA ===
			// <device_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "device_type")
			{
				logDebug("<device_type> found");
				$_section_device_type = true;
			}
			// <type>
			else if ($_section_device_type == true
					&& $_list_xml->depth == 2
					&& $_list_xml->name == "type")
			{
				logDebug("<type> found");
				$_section_type = true;
					
				$_type = new device_type($_type_count);
								
				if (isset($_type) && $_type != null)
				{
					$_type->extractXMLConfig($_list_xml);
					$_type_list[$_type_count] = $_type;
					$_type_count++;
					logDebug("New device type object: ".serialize($_type));
				}
			}
									
			$_node_depth++;
		}
		if ($_list_xml->nodeType == XMLReader::END_ELEMENT)
		{
			// </telefin>
			if ($_section_telefin == true
				&& $_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("</telefin> found");
				$_section_telefin = false;
			}
			
			// === LISTA ===
			// </device_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "device_type")
			{
				logDebug("</device_type> found");
				$_section_device_type = false;
				$_section_type = false;
			}
						
			$_node_depth--;
		}
	}
	
	return($_type_list);
}

/**
* Funzione per caricare la port type list
*/       
function spvstandardLoadXMLPortTypeList($_list_xml)
{
	logDebug("=== spvstandardLoadXMLPortTypeList ===");
		
	$_type_list = array();
	
	$_node_depth	= 0;
	$_node_type		= null;
	$_node_name		= "";
	$_node_value	= "";
	
	$_section_telefin 	= false;
	$_section_port_type	= false;
	$_section_type		= false;
	
	$_type_count	= 0;
	
	$_node_depth 	= 1;
	
	while ($_list_xml->read() && $_node_depth != 0)
	{
		if ($_list_xml->nodeType == XMLReader::ELEMENT)
		{
			// <telefin>
			if ($_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("<telefin> found");
				$_section_telefin = true;
			}
			
			// === LISTA ===
			// <port_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "port_type")
			{
				logDebug("<port_type> found");
				$_section_port_type = true;
			}
			// <type>
			else if ($_section_port_type == true
					&& $_list_xml->depth == 2
					&& $_list_xml->name == "type")
			{
				logDebug("<type> found");
				$_section_type = true;
					
				$_type = new port_type($_type_count);
								
				if (isset($_type) && $_type != null)
				{
					$_type->extractXMLConfig($_list_xml);
					$_type_list[$_type_count] = $_type;
					$_type_count++;
					logDebug("New port type object: ".serialize($_type));
				}
			}
									
			$_node_depth++;
		}
		if ($_list_xml->nodeType == XMLReader::END_ELEMENT)
		{
			// </telefin>
			if ($_section_telefin == true
				&& $_list_xml->depth == 0
				&& ($_list_xml->name == "telefin" || $_list_xml->name == "Telefin"))
			{
				logDebug("</telefin> found");
				$_section_telefin = false;
			}
			
			// === LISTA ===
			// </port_type>
			else if ($_section_telefin == true
					&& $_list_xml->depth == 1
					&& $_list_xml->name == "port_type")
			{
				logDebug("</port_type> found");
				$_section_port_type = false;
				$_section_type = false;
			}
						
			$_node_depth--;
		}
	}
	
	return($_type_list);
}

// esegue il parsing del file xml passato in input e recupera i parametri che andranno a fare l'ovveride di quelli nel system.xml
// ritorna una struttura contenente i vari parametri da sostituire
function parsingSTLCManagetServiceConfing($xml_file){
	$res = array();
	$element_ar = array(
			0 => 'GlobalSettings',
			1 => 'LocalSettings'
	);
	
	try {
		$xml = simplexml_load_file($xml_file);
		
		if($xml === false){
			error_log("Il file " . $xml_file . " non � valido.");
		} else {
			if(isset($xml->STLCManagerSettings) !== false){
				$xml = $xml->STLCManagerSettings;
				// scorro tutti gli elementi dell'array
				for($i = 0; $i < count($element_ar); $i++){
					$str_elem = $element_ar[$i];
					if( isset($xml->{$str_elem}) === true ){
						$elem = $xml->{$str_elem};
						foreach ($elem->Setting as $setting){
							if( isset($setting['name']) === true && isset($setting['value']) === true && trim($setting['value'])!==''){
								switch ((string) $setting['name']){
									case 'ForcedServerID':
										$res['SrvID'] = (string)$setting['value'];
										break;
									case 'VirtualHostName':
										$res['host'] = (string)$setting['value'];
										break;
									case 'ForcedServerName':
										$res['name'] = (string)$setting['value'];
										break;
									case 'ForcedServerType':
										$res['type'] = (string)$setting['value'];
										break;
									default:
										break;
								}
							}
						}
					}
				}
			}
		}
	} catch( Exception $e ) {
		error_log("Errore durante il parsing del file ". $xml_file . ": " . e);
		$res = null;
	}
	
	return $res;
}

function stringToSerialNumber($str){
	$res = null;
	
	try {
		$sn = explode('.', str_replace(',', '.', $str));
		
		$byAnno = intval ($sn[0]);
		$byWeek = intval ($sn[1]);
		$wProg = intval ($sn[2]);
		
		$res = 0;
		$res |= ($byAnno << 24);
		$res |= ($byWeek << 16);
		$res |= ($wProg);
	} catch ( Exception $e ) {
		$res = null;
	}
	
	return $res;
}

?>
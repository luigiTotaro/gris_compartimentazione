<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_rack.php - Classe edificio di un armadio (o posizione).
*
* @author Enrico Alborali
* @version 1.0.3.3 13/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Implementazione della classe dei parametri Rack
*/
class rack_params{
	public $type;
	public $name;
	public $note;
		
	/**
	* Costruttore classe parametri Rack
	*/
	function __construct($type,$name,$note){
		$this->type = $type; // building, platform (binario), none
		$this->name = $name;
		$this->note = $note;
	}
}

/**
* Implementazione della classe Rack
*/
class rack extends rack_params{
	public $id;
	public $stationId;
	public $buildingId;
	
	public $severity;
	public $status;
	
	/**
	* Costruttore classe Rack
	*/
	function __construct(){
	}

	/**
	* Funzione per estrarre la configurazione di un edificio
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== rack:extractXMLConfig ===");
		
		$_depth_to_match = 4;
		$_name_to_match = "location";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		$_item_note			= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('id');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_note 		= $xmlElement->getAttribute('note');
						
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "none";
				if (!isset($_item_name))
					$_item_name = "Nessuno";
					
				logDebug("Extracted rack ".$_item_id.":".$_item_type.":".$_item_note);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
				$this->note			= $_item_note;
			}
			else
			{
				logEvent("Impossibile recuperare l'id dell'armadio.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML per l'armadio
	*/
	function buildXMLConfig()
	{
		logDebug("=== rack:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		$_xml .= codeChr(1,4).'<location id="'.$this->id.'" name="'.utf8_decode($this->name).'" type="'.$this->type.'" note="'.utf8_decode($this->note).'" />';
				
		return($_xml);
	}
	
	/**
	* Metodo per ottenere il DevId a 64 bit in formato stringa per un oggetto device
	*/
	function getRackId()
	{
		$_id = $this->id;
		$_id = bcmul($_id,"65536");
		
		$_id = bcadd($_id,$this->buildingId);
		$_id = bcmul($_id,"65536");
		
		$_id = bcadd($_id,$this->stationId);
		$_id = bcmul($_id,"65536");
		
		//$_id = bcadd($_id,"0");
		
		return($_id);
	}
	
	/**
	* Metodo per ottenere il DisplayName dell'armadio
	*/
	function getDisplayName()
	{
		global $_configuration;
		
		$_stations 	= $_configuration["stations"];
		$_buildings = $_configuration["buildings"];
		
		// Recupero informazioni
		$_station_id = $this->stationId;
		$_building_id = $this->buildingId;
		$_station = getStationFromId($_stations,$_station_id);
		$_building = getBuildingFromId($_buildings,$_station_id,$_building_id);
		
		$_name = $_station->name." - ".$_building->name." - ".$this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere un oggetto rack da una lista di rack
*/
function getRack($locations,$stationId,$buildingId,$locationId)
{
	$_rack = null;

	foreach ($locations as $_location_index => $_location)
	{
		if ($_location->stationId == $stationId
			&& $_location->buildingId == $buildingId
			&& $_location->id == $locationId)
		{
			$_rack = $_location;
			break;
		}
	}
	
	return($_rack);
}

/**
* Funzione per ottenere l'indice di un armadio in base all'id
*/
function getRackIndexFromRackId($rackList,$rackId)
{
	$_index = null;
	
	$_list = $rackList;
	$_id = $rackId;
	
	foreach ($_list as $_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->getRackId() == $_id)
			{
				$_index = $_index;
			 	break;
			}
		}
	}
	
	return($_index);
}

/**
* Funzione per ottenere un armadio dalla lista
*/
function getRackFromRackId($rackList,$rackId)
{
	$_rack = null;
	
	$_list = $rackList;
	$_id = $rackId;
	
	foreach ($_list as $_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->getRackId() == $_id)
			{
				$_rack = $_item;
			 	break;
			}
		}
	}
	
	return($_rack);
}

/**
* Funzione per ottenere id stazione dal suo display name
*/
function getRackIdFromRackLongName($rackList,$rackLongName)
{
	$_id = null;

	$_list 		= $rackList;
	$_long_name = $rackLongName;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_value = $_item->getDisplayName();
				
				if ($_value == $_long_name)
				{
					$_id = $_item->id;
					break;
				}
			}
		}
	}
	
	return($_id);
}

/**
* Funzione per ottenere un edificio dal suo display name
*/
function getRackFromRackLongName($rackList,$rackLongName)
{
	$_rack = null;

	$_list 		= $rackList;
	$_long_name = $rackLongName;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_value = $_item->getDisplayName();
				
				if ($_value == $_long_name)
				{
					$_rack = $_item;
					break;
				}
			}
		}
	}
	
	return($_rack);
}

/**
* Funzione per verificare se il nome dell'armadio non e' stato gia' utilizzato
*/
function isFreeRackName($list,$stationId,$buildingId,$value,$exclude=null)
{
	$_list = $list;
	$_value = $value;
	
	$_free = true;
	$_id = 0;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->name == $_value && $_item->stationId == $stationId && $_item->buildingId == $buildingId && $_item->name != $exclude)
			{
				$_free = false;
		 		break;
			}
		}
	}
	
	return($_free);
}

/**
* Funzione per ottenere il primo id disponibile per un armadio
*/
function getFirstFreeRackId($list,$stationId,$buildingId)
{
	$_list = $list;
	
	$_found = false;
	$_id = 0;
	
	while (!$_found && $_id < 65535)
	{
		$_found = true;
	
		foreach ($_list as $_item_index => $_item)
		{
			if (isset($_item))
			{
				if ($_item->stationId == $stationId && $_item->buildingId == $buildingId && $_item->id == $_id)
				{
					$_found = false;
			 		$_id++;
			 		break;
				}
			}
		}
	}
	
	if (!$_found) $_id = null;
	
	return($_id);
}

/**
* Funzione per verificare se una posizione in un armadio/rack/location e' valida
*/
function positionIsValid($locations,$stationId,$buildingId,$locationId,$position)
{
	$_found = false;
	$_valid = false;
	
	foreach ($locations as $_location_index => $_location)
	{
		$_rack = getRack($locations,$stationId,$buildingId,$locationId);
		if (isset($_rack))
		{
			$_found = true;
			break;
		}
	}
	
	if ($_found === true)
	{
		$_positions = explode(",", $position);
	
		if (count($_positions) == count($_rack->maxPositions))
		{
			$_valid = true;
			foreach ($_positions as $_position_index => $_position)
			{
				if ($_position >= 1 && $_position <= $_rack->maxPositions[$_position_index])
				{
					$_valid = false;
					break;	
				}
			}
		}
	}
	
	return($_valid);
}

/**
* Funzione per verificare se una posizione di un armadio/rack/location e' libera
*/
function positionIsFree($locations,$devices,$stationId,$buildingId,$locationId,$position,$serverId,$regionId,$zoneId,$nodeId,$deviceId)
{
	$_is_free = true;
	
	if (positionIsValid($locations,$stationId,$buildingId,$locationId,$position))
	{
		foreach ($devices as $_device_index => $_device)
		{
			if (isset($_device))
			{
				if ($_device->stationId == $stationId
				&& $_device->buildingId == $buildingId
				&& $_device->locationId == $locationId
				&& $_device->position == $position
				&& !($_device->serverId == $serverId
				&& $_device->regionId == $regionId
				&& $_device->zoneId == $zoneId
				&& $_device->nodeId == $nodeId
				&& $_device->id == $deviceId))
				{
					$_is_free = false;
					break;
				}
			}
		}
	}
	else
	{
		$_is_free = false;	
	}
	
	return($_is_free);
}

/**
* Funzione per verificare se l'armadio e' associato a una o piu' periferiche
*/
function rackIsUsedByDevice($list,$stationId,$buildingId,$rackId)
{
	$_list = $list;
	
	$_used = false;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->stationId == $stationId && $_item->buildingId == $buildingId && $_item->locationId == $rackId)
			{
				$_used = true;
		 		break;
			}
		}
	}
	
	return($_used);
}

/**
* Funzione per eseguire lo shift degli id degli armadi con id successivo a quello dato
*/
function rackShiftNextId(&$rackList,&$deviceList,$stationId,$buildingId,$rackId)
{
	$_shifted = false;
	
	// Eseguo lo shift agli armadi
	foreach ($rackList as $_rack_index => $_rack)
	{
		if (isset($_rack))
		{
			if ($_rack->stationId == $stationId && $_rack->buildingId == $buildingId && $_rack->id > $rackId)
			{
				// Decremento di 1 l'id
				$_rack->id--;
				// Salvo l'armadio
				$rackList[$_rack_index] = $_rack;
				$_shifted = true;
			}
		}						
	}
				
	// Propago lo shift alle periferiche
	foreach ($deviceList as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($_device->stationId == $stationId && $_device->buildingId == $buildingId && $_device->locationId > $rackId)
			{
				// Decremento di 1 l'id
				$_device->locationId--;
				// Salvo la periferica
				$deviceList[$_device_index] = $_device;
				$_shifted = true;
			}
		}	
	}	
	
	return($_shifted);
}

/**
* Funzione per aggiornare gli id della topografia degli armadi
*/
function rackUpdateTopographyId(&$rackList,$oldStationId,$oldBuildingId,$stationId,$buildingId)
{
	$_updated = false;
	
	// Cerco armadi nell'edificio
	foreach ($rackList as $_rack_index => $_rack)
	{
		if (isset($_rack))
		{
			if ($_rack->stationId == $oldStationId && 
			($_rack->buildingId == $oldBuildingId || $oldBuildingId == null))
			{
				// Aggiorno gli id dell'armadio
				$_rack->stationId 	= $stationId;
				if ($buildingId != null)
					$_rack->buildingId 	= $buildingId;
				// Salvo l'armadio
				$rackList[$_rack_index] = $_rack;
				$_updated = true;
			}
		}	
	}
	
	return($_updated);
}

/**
* Implementazione della classe rack type
*/
class rack_type{
	// Parametri comuni
	public $id;
	public $code;
	public $name;
	public $defaultName;
	public $visible;
	public $maxPositions; // Array (un elemento per ogni dimensione)
	public $slotWidth;
	public $slotHeight;
	
	/**
	* Costruttore classe rack type
	*/
	function __construct($id){
		$this->id = $id; 
	}

	/**
	* Funzione per estrarre la configurazione di un rack type
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== rack_type:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "type";
	
		// Parametri comuni
		$_item_code 		= null;
		$_item_name 		= null;
		$_item_default_name	= null;
		$_item_visible		= null;
		$_item_max_pos_x	= null;
		$_item_max_pos_y	= null;
		$_item_max_pos_z	= null;
		$_item_slot_width	= null;
		$_item_slot_height	= null;
						
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_code 		= $xmlElement->getAttribute('code');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_default_name = $xmlElement->getAttribute('default_name');
			$_item_visible		= $xmlElement->getAttribute("visible");
			$_item_max_pos_x	= $xmlElement->getAttribute('max_pos_x');
			$_item_max_pos_y	= $xmlElement->getAttribute('max_pos_y');
			$_item_max_pos_z	= $xmlElement->getAttribute('max_pos_z');
			$_item_slot_width	= $xmlElement->getAttribute('slot_width');
			$_item_slot_height	= $xmlElement->getAttribute('slot_height');
			
			// Verifico parametri recuperati
			if (isset($_item_code))
			{
				if (empty($_item_name))
				{
					$_item_name = $_item_code;
					logEvent("Impossibile recuperare il nome del tipo armadio.",1);
				}
				if (empty($_item_default_name))
					$_item_default_name = $_item_name;
				if ($_item_visible == "false")
					$_item_visible = false;
				else
					$_item_visible = true;
				if (empty($_item_max_pos_x))
					$_item_max_pos_x = 0;
				if (empty($_item_max_pos_y))
					$_item_max_pos_y = 0;
				if (empty($_item_max_pos_z))
					$_item_max_pos_z = 0;
				if (empty($_item_slot_width))
					$_item_slot_width = 1;
				if (empty($_item_slot_height))
					$_item_slot_height = 1;
				
				$_item_max_pos = array();
				if ($_item_max_pos_x != 0)
					$_item_max_pos['x'] = $_item_max_pos_x;
				if ($_item_max_pos_y != 0)
					$_item_max_pos['y'] = $_item_max_pos_y;
				if ($_item_max_pos_z != 0)
					$_item_max_pos['z'] = $_item_max_pos_z;
				
				logDebug("Extracted rack type ".$_item_code.":".$_item_name);
				
				// Salvo parametri comuni
				$this->code			= $_item_code;
				$this->name			= $_item_name;
				$this->defaultName	= $_item_default_name;
				$this->visible		= $_item_visible;
				$this->maxPositions	= $_item_max_pos;
				$this->slotWidth	= $_item_slot_width;
				$this->slotHeight	= $_item_slot_height;
			}
			else
			{
				logEvent("Impossibile recuperare il codice del tipo armadio.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per ottenere il display name per un port type
	*/
	function getDisplayName()
	{
		$_name = $this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere l'oggetto Tipo Armadio dal codice
*/
function getRackTypeFromCode($typeList,$typeCode)
{
	$_type = null;
	
	foreach ($typeList as $_type_index => $_type_item)
	{
		if (isset($_type_item))
		{
			if ($_type_item->code == $typeCode)
			{
				$_type = $_type_item;
			 	break;
			}
		}
	}
	
	return($_type);
}

/**
* Funzione per ottenere il codice di un tipo armadio dal nome
*/
function getRackTypeCodeFromName($typeList,$typeName)
{
	$_code = null;
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->name == $typeName)
			{
				$_code = $_type->code;
			 	break;
			}
		}
	}
	
	return($_code);
}

/**
* Funzione per ottenere il nome di un tipo armadio dal codice
*/
function getRackTypeNameFromCode($typeList,$typeCode)
{
	$_name = "Tipo Sconosciuto";
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->code == $typeCode)
			{
				$_name = $_type->name;
			 	break;
			}
		}
	}
	
	return($_name);
}

?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_station.php - Classe stazione ferroviaria.
*
* @author Enrico Alborali
* @version 1.0.3.2 10/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Implementazione della classe dei parametri Station
*/
class station_params{
	public $type;
	public $name;
	public $nodeId;
	
	/**
	* Costruttore classe parametri Station
	*/
	function __construct($type,$name){
		$this->type = $type; // station, stop, niche
		$this->name = $name;
	}
}

/**
* Implementazione della classe Station
*/
class station extends station_params{
	public $id;
	
	public $severity;
	public $status;
	
		
	/**
	* Costruttore classe Station
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di una stazione
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== station:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "station";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		$_item_node_id		= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('id');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_node_id		= $xmlElement->getAttribute('NodID');
						
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "station";
				if (!isset($_item_name))
					$_item_name = "Stazione";
					
				logDebug("Extracted station ".$_item_id.":".$_item_type);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
				$this->nodeId		= $_item_node_id;
			}
			else
			{
				logEvent("Impossibile recuperare l'id della stazione.",2);
			}
		}
	
		return($this);
	}

	/**
	* Metodo per generare la configurazione XML per la stazione
	*/
	function buildXMLConfig($deviceList=null)
	{
		logDebug("=== station:buildXMLConfig ===");
		
		if (isset($this->nodeId) && $this->nodeId!="")
		{
			$_item_node_id = $this->nodeId;
		}
		else
		{
			$_item_node_id = getStationNodeIdFromDevices($deviceList,$this->id);
		}
		
		//print("<!--".$this->nodeId."=".$_item_node_id." ".serialize($deviceList)." ".$this->id." -->");
		
		$_xml = codeInit();
		
		$_xml .= codeChr(1,2).'<station id="'.$this->id.'" NodID="'.$_item_node_id.'" type="'.$this->type.'" name="'.$this->name.'">';
		$_xml .= "%BUILDINGS%";
		$_xml .= codeChr(1,2).'</station>';
				
		return($_xml);
	}
	
	/**
	* Metodo per ottenere il BuildingId a 64 bit in formato stringa per un oggetto station
	*/
	function getStationId()
	{
		$_id = 0;
		$_id = bcmul($_id,"65536");
		
		//$_id = bcadd($_id,"0");
		$_id = bcmul($_id,"65536");
		
		$_id = bcadd($_id,$this->id);
		$_id = bcmul($_id,"65536");
		
		//$_id = bcadd($_id,"0");
		
		return($_id);
	}
	
	/**
	* Metodo per ottenere il DisplayName della stazione
	*/
	function getDisplayName()
	{
		global $_configuration;
		
		$_regions = $_configuration["regions"];
		$_zones = $_configuration["zones"];
		$_nodes = $_configuration["nodes"];
		
		// Recupero informazioni Region Zone e Node
		$_node_id = $this->nodeId;
		$_node = getNodeFromId($_nodes,$_node_id);
		$_zone_id = $_node->zoneId;
		$_region_id = $_node->regionId;
		$_zone = getZoneFromId($_zones,$_zone_id);
		$_region = getRegionFromId($_regions,$_region_id);
	
		$_name = $_region->name." > ".$_zone->name." > ".$_node->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere l'indice di una stazione in base all'id
*/
function getStationIndexFromStationId($stationList,$stationId)
{
	$_index = null;
	
	foreach ($stationList as $_station_index => $_station_item)
	{
		if (isset($_station_item))
		{
			if ($_station_item->getStationId() == $stationId)
			{
				$_index = $_station_index;
			 	break;
			}
		}
	}
	
	return($_index);
}

/**
* Funzione per ottenere l'indice di una stazione in base all'id
*/
function getStationIndexFromId($stationList,$stationId)
{
	$_index = null;
	
	foreach ($stationList as $_station_index => $_station_item)
	{
		if (isset($_station_item))
		{
			if ($_station_item->id == $stationId)
			{
				$_index = $_station_index;
			 	break;
			}
		}
	}
	
	return($_index);
}

/**
* Funzione per ottenere una stazione dalla lista
*/
function getStationFromStationId($stationList,$stationId)
{
	$_station = null;
	
	foreach ($stationList as $_station_index => $_station_item)
	{
		if (isset($_station_item))
		{
			if ($_station_item->getStationId() == $stationId)
			{
				$_station = $_station_item;
			 	break;
			}
		}
	}
	
	return($_station);
}

/**
* Funzione per ottenere una stazione dalla lista
*/
function getStationFromId($stationList,$stationId)
{
	$_station = null;
	
	foreach ($stationList as $_station_index => $_station_item)
	{
		if (isset($_station_item))
		{
			if ($_station_item->id == $stationId)
			{
				$_station = $_station_item;
			 	break;
			}
		}
	}
	
	return($_station);
}

/**
* Funzione per ottenere il node id di una stazione verificando le periferiche configurate
*/
function getStationNodeIdFromDevices($devices,$stationId)
{
	$_id = null;

	$_list = $devices;
	$_item_id = $stationId;
	
	//print("<!-- COUNT ".count($_list)." ITEM_ID ".$_item_id." -->");
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			//print("<!--".$_item->stationId."=".$_item_id." -->");
			if ($_item->stationId == $_item_id)
			{
				$_id = $_item->nodeId;
			 	break;
			}
		}
	}
	
	return($_id);
}

/**
* Funzione per ottenere id stazione dal suo display name
*/
function getStationIdFromStationLongName($stationList,$stationlongName)
{
	$_id = null;

	$_list 		= $stationList;
	$_long_name = $stationlongName;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_value = $_item->getDisplayName();
				
				if ($_value == $_long_name)
				{
					$_id = $_item->id;
					break;
				}
			}
		}
	}
	
	return($_id);
}

/**
* Funzione per ottenere il primo id disponibile per una stazione
*/
function getFirstFreeStationId($list)
{
	$_list = $list;
	
	$_found = false;
	$_id = 0;
	
	while (!$_found && $_id < 65535)
	{
		$_found = true;
	
		foreach ($_list as $_item_index => $_item)
		{
			if (isset($_item))
			{
				if ($_item->id == $_id)
				{
					$_found = false;
			 		$_id++;
			 		break;
				}
			}
		}
	}
	
	if (!$_found) $_id = null;
	
	return($_id);
}

/**
* Funzione per verificare se il nome della stazione non e' stato gia' utilizzato
*/
function isFreeStationName($list,$value,$exclude=null)
{
	$_list = $list;
	$_value = $value;
	
	$_free = true;
	$_id = 0;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->name == $_value && $_item->name != $exclude)
			{
				$_free = false;
		 		break;
			}
		}
	}
	
	return($_free);
}

/**
* Funzione per verificare se la stazione e' associata a una o piu' edifici
*/
function stationIsUsedByBuilding($list,$stationId)
{
	$_list = $list;
	
	$_used = false;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->stationId == $stationId)
			{
				$_used = true;
		 		break;
			}
		}
	}
	
	return($_used);
}

/**
* Funzione per associare automaticamente gli elementi station ai rispettivi node
*/
function stationLinkNodes(&$stationList,$nodeList,$deviceList)
{
	logDebug("=== stationLinkNodes() ===");
	
	$_list = $stationList;
	
	$_linked = false;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if (empty($_item->nodeId) || $_item->nodeId=="")
			{
				// Recupero e salvo il NodId
				$_item_node_id = getStationNodeIdFromDevices($deviceList,$_item->id);
				$_item->nodeId = $_item_node_id;
				// Recupero e salvo il nome del Node
				$_node = getNodeFromId($nodeList,$_item_node_id);
				if (isset($_node)) $_item->name = $_node->name;
				// Salvo la station linkata nella lista				
				$_list[$_item_index] = $_item;
				$_linked = true;
			}
		}
	}
	
	return($_linked);
}

/**
* Funzione per eseguire lo shift degli id delle stazioni con id successivo a quello dato
*/
function stationShiftNextId(&$stationList,&$buildingList,&$rackList,&$nodeList,&$deviceList,$stationId)
{
	$_shifted = false;
	
	// Eseguo lo shift alle stazioni
	foreach ($stationList as $_station_index => $_station)
	{
		if (isset($_station))
		{
			if ($_station->id > $stationId)
			{
				// Decremento di 1 l'id della stazione
				$_station->id--;
				// Salvo la stazione
				$stationList[$_station_index] = $_station;
				$_shifted = true;
			}
		}
	}
	
	// Propago lo shift agli edifici
	foreach ($buildingList as $_building_index => $_building)
	{
		if (isset($_building))
		{
			if ($_building->stationId > $stationId)
			{
				// Decremento di 1 l'id
				$_building->stationId--;
				// Salvo l'edificio
				$buildingList[$_building_index] = $_building;
				$_shifted = true;
			}
		}
	}
						
	// Propago lo shift agli armadi
	foreach ($rackList as $_rack_index => $_rack)
	{
		if (isset($_rack))
		{
			if ($_rack->stationId > $stationId)
			{
				// Decremento di 1 l'id
				$_rack->stationId--;
				// Salvo l'armadio
				$rackList[$_rack_index] = $_rack;
				$_shifted = true;
			}
		}						
	}
				
	// Propago lo shift ai nodi
	foreach ($nodeList as $_node_index => $_node)
	{
		if (isset($_node))
		{
			if ($_node->stationId > $stationId)
			{
				// Decremento di 1 l'id
				$_node->stationId--;
				// Salvo la periferica
				$nodeList[$_node_index] = $_node;
				$_shifted = true;
			}
		}	
	}	
	
	// Propago lo shift alle periferiche
	foreach ($deviceList as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($_device->stationId > $stationId)
			{
				// Decremento di 1 l'id
				$_device->stationId--;
				// Salvo la periferica
				$deviceList[$_device_index] = $_device;
				$_shifted = true;
			}
		}	
	}
	
	return($_shifted);
}

/**
* Implementazione della classe station type
*/
class station_type{
	public $id;
	public $code;
	public $name;
	
	/**
	* Costruttore classe device type
	*/
	function __construct($id){
		$this->id = $id; 
	}

	/**
	* Funzione per estrarre la configurazione di un device type
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== station_type:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "type";
	
		// Parametri comuni
		$_item_code 		= null;
		$_item_name 		= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			//<type name="Telefin DT00000 - Pannello DTS Standard" default_name="Pannello DTS" code="DT00000" port_type="STLC1000_RS485"/>
		
			// Recupero parametri comuni
			$_item_code 		= $xmlElement->getAttribute('code');
			$_item_name 		= $xmlElement->getAttribute('name');
						
			// Verifico parametri recuperati
			if (isset($_item_code))
			{
				if (!isset($_item_name))
				{
					$_item_name = $_item_code;
					logEvent("Impossibile recuperare il nome del tipo periferica.",1);
				}
				
				logDebug("Extracted station type ".$_item_code.":".$_item_name);
				
				// Salvo parametri comuni					
				$this->code			= $_item_code;
				$this->name			= $_item_name;
			}
			else
			{
				logEvent("Impossibile recuperare il codice del tipo periferica.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per ottenere il display name per un device type
	*/
	function getDisplayName()
	{
		//$_name = $this->code." - ".$this->name;
		$_name = $this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere il codice di un tipo stazione dal nome
*/
function getStationTypeCodeFromName($typeList,$typeName)
{
	$_code = null;
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->name == $typeName)
			{
				$_code = $_type->code;
			 	break;
			}
		}
	}
	
	return($_code);
}

/**
* Funzione per ottenere il nome di un tipo stazione dal codice
*/
function getStationTypeNameFromCode($typeList,$typeCode)
{
	$_name = "Tipo Sconosciuto";
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->code == $typeCode)
			{
				$_name = $_type->name;
			 	break;
			}
		}
	}
	
	return($_name);
}



?>
<?php
/**
* Telefin STLC1000 Consolle
*
* sk_class_building.php - Classe edificio di una stazione ferroviaria.
*
* @author Enrico Alborali
* @version 1.0.3.0 02/02/2012
* @copyright 2011-2012 Telefin S.p.A.
*/

/**
* Implementazione della classe dei parametri Building
*/
class building_params{
	public $type;
	public $name;
	public $note;
	
	/**
	* Costruttore classe parametri Building
	*/
	function __construct($type,$name,$note){
		$this->type = $type; // building, platform (binario), none
		$this->name = $name;
		$this->note = $note;
	}
}

/**
* Implementazione della classe Building
*/
class building extends building_params{
	public $id;
	public $stationId;
	
	public $severity;
	public $status;
	
	/**
	* Costruttore classe Building
	*/
	function __construct(){
	
	}

	/**
	* Funzione per estrarre la configurazione di un edificio
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== building:extractXMLConfig ===");
		
		$_depth_to_match = 3;
		$_name_to_match = "building";
	
		// Parametri comuni
		$_item_id 			= null;
		$_item_type 		= null;
		$_item_name 		= null;
		$_item_note			= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_id 			= $xmlElement->getAttribute('id');
			$_item_type 		= $xmlElement->getAttribute('type');
			$_item_name 		= $xmlElement->getAttribute('name');
			$_item_note 		= $xmlElement->getAttribute('note');
						
			// Verifico parametri recuperati
			if (isset($_item_id))
			{
				if (!isset($_item_type))
					$_item_type = "building";
				if (!isset($_item_name))
					$_item_name = "Fabbricato Viaggiatori";
					
				logDebug("Extracted building ".$_item_id.":".$_item_type.":".$_item_note);
				
				// Salvo parametri comuni					
				$this->id			= $_item_id;
				$this->type			= $_item_type;
				$this->name			= $_item_name;
				$this->note			= $_item_note;
			}
			else
			{
				logEvent("Impossibile recuperare l'id dell'edificio.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per generare la configurazione XML per l'edificio
	*/
	function buildXMLConfig()
	{
		logDebug("=== building:buildXMLConfig ===");
		
		$_xml = codeInit();
		
		$_xml .= codeChr(1,3).'<building id="'.$this->id.'" name="'.utf8_decode($this->name).'" type="'.$this->type.'" note="'.utf8_decode($this->note).'" >';
		$_xml .= "%LOCATIONS%";
		$_xml .= codeChr(1,3).'</building>';
				
		return($_xml);
	}
	
	/**
	* Metodo per ottenere il BuildingId a 64 bit in formato stringa per un oggetto building
	*/
	function getBuildingId()
	{
		$_id = 0;
		$_id = bcmul($_id,"65536");
		
		$_id = bcadd($_id,$this->id);
		$_id = bcmul($_id,"65536");
		
		$_id = bcadd($_id,$this->stationId);
		$_id = bcmul($_id,"65536");
		
		//$_id = bcadd($_id,"0");
		
		return($_id);
	}
	
	/**
	* Metodo per ottenere il DisplayName della stazione
	*/
	function getDisplayName()
	{
		global $_configuration;
		
		$_stations = $_configuration["stations"];
		
		// Recupero informazioni
		$_station_id = $this->stationId;
		$_station = getStationFromId($_stations,$_station_id);
		
		$_name = $_station->name." > ".$this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere l'indice di una stazione in base all'id
*/
function getBuildingIndexFromBuildingId($buildingList,$buildingId)
{
	$_index = null;
	
	$_list = $buildingList;
	$_id = $buildingId;
	
	foreach ($_list as $_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->getBuildingId() == $_id)
			{
				$_index = $_index;
			 	break;
			}
		}
	}
	
	return($_index);
}

/**
* Funzione per ottenere una stazione dalla lista
*/
function getBuildingFromId($buildingList,$stationId,$buildingId)
{
	$_building = null;
	
	foreach ($buildingList as $_building_index => $_building_item)
	{
		if (isset($_building_item))
		{
			if ($_building_item->id == $buildingId && $_building_item->stationId == $stationId)
			{
				$_building = $_building_item;
			 	break;
			}
		}
	}
	
	return($_building);
}

/**
* Funzione per ottenere una stazione dalla lista
*/
function getBuildingFromBuildingId($buildingList,$buildingId)
{
	$_building = null;
	
	foreach ($buildingList as $_building_index => $_building_item)
	{
		if (isset($_building_item))
		{
			if ($_building_item->getBuildingId() == $buildingId)
			{
				$_building = $_building_item;
			 	break;
			}
		}
	}
	
	return($_building);
}

/**
* Funzione per ottenere id stazione dal suo display name
*/
function getBuildingIdFromBuildingLongName($buildingList,$buildinglongName)
{
	$_id = null;

	$_list 		= $buildingList;
	$_long_name = $buildinglongName;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_value = $_item->getDisplayName();
				
				if ($_value == $_long_name)
				{
					$_id = $_item->id;
					break;
				}
			}
		}
	}
	
	return($_id);
}

/**
* Funzione per ottenere un edificio dal suo display name
*/
function getBuildingFromBuildingLongName($buildingList,$buildinglongName)
{
	$_building = null;

	$_list 		= $buildingList;
	$_long_name = $buildinglongName;
	
	if (isset($_list) && count($_list) > 0)
	{
		foreach ($_list as $_item)
		{
			if (isset($_item))
			{
				$_value = $_item->getDisplayName();
				
				if ($_value == $_long_name)
				{
					$_building = $_item;
					break;
				}
			}
		}
	}
	
	return($_building);
}

/**
* Funzione per ottenere il primo id disponibile per un edificio
*/
function getFirstFreeBuildingId($list,$stationId)
{
	$_list = $list;
	
	$_found = false;
	$_id = 0;
	
	while (!$_found && $_id < 65535)
	{
		$_found = true;
	
		foreach ($_list as $_item_index => $_item)
		{
			if (isset($_item))
			{
				if ($_item->stationId == $stationId && $_item->id == $_id) // Cerco solo all'interno della stessa stazione
				{
					$_found = false;
			 		$_id++;
			 		break;
				}
			}
		}
	}
	
	if (!$_found) $_id = null;
	
	return($_id);
}

/**
* Funzione per verificare se il nome dell'edificio non e' stato gia' utilizzato
*/
function isFreeBuildingName($list,$stationId,$value,$exclude=null)
{
	$_list = $list;
	$_value = $value;
	
	$_free = true;
	$_id = 0;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->name == $_value && $_item->stationId == $stationId && $_item->name != $exclude)
			{
				$_free = false;
		 		break;
			}
		}
	}
	
	return($_free);
}

/**
* Funzione per verificare se l'edificio e' associato a una o piu' armadi
*/
function buildingIsUsedByRack($list,$stationId,$buildingId)
{
	$_list = $list;
	
	$_used = false;
	
	foreach ($_list as $_item_index => $_item)
	{
		if (isset($_item))
		{
			if ($_item->stationId == $stationId && $_item->buildingId == $buildingId)
			{
				$_used = true;
		 		break;
			}
		}
	}
	
	return($_used);
}

/**
* Funzione per eseguire lo shift degli id degli armadi con id successivo a quello dato
*/
function buildingShiftNextId(&$buildingList,&$rackList,&$deviceList,$stationId,$buildingId)
{
	$_shifted = false;
	
	// Eseguo lo shift agli edifici
	foreach ($buildingList as $_building_index => $_building)
	{
		if (isset($_building))
		{
			if ($_building->stationId == $stationId && $_building->id > $buildingId)
			{
				// Decremento di 1 l'id
				$_building->id--;
				// Salvo l'edificio
				$buildingList[$_building_index] = $_building;
				$_shifted = true;
			}
		}
	}
						
	// Propago lo shift agli armadi
	foreach ($rackList as $_rack_index => $_rack)
	{
		if (isset($_rack))
		{
			if ($_rack->buildingId > $buildingId)
			{
				// Decremento di 1 l'id
				$_rack->buildingId--;
				// Salvo l'armadio
				$rackList[$_rack_index] = $_rack;
				$_shifted = true;
			}
		}						
	}
				
	// Propago lo shift alle periferiche
	foreach ($deviceList as $_device_index => $_device)
	{
		if (isset($_device))
		{
			if ($_device->buildingId > $buildingId)
			{
				// Decremento di 1 l'id
				$_device->buildingId--;
				// Salvo la periferica
				$deviceList[$_device_index] = $_device;
				$_shifted = true;
			}
		}	
	}	
	
	return($_shifted);
}

/**
* Implementazione della classe building type
*/
class building_type{
	public $id;
	public $code;
	public $name;
	
	/**
	* Costruttore classe building type
	*/
	function __construct($id){
		$this->id = $id; 
	}

	/**
	* Funzione per estrarre la configurazione di un building type
	*/
	function extractXMLConfig($xmlElement)
	{
		logDebug("=== building_type:extractXMLConfig ===");
		
		$_depth_to_match = 2;
		$_name_to_match = "type";
	
		// Parametri comuni
		$_item_code 		= null;
		$_item_name 		= null;
		
		if (isset($xmlElement) 
			&& $xmlElement->depth == $_depth_to_match
			&& $xmlElement->name == $_name_to_match)
		{
			// Recupero parametri comuni
			$_item_code 		= $xmlElement->getAttribute('code');
			$_item_name 		= $xmlElement->getAttribute('name');
						
			// Verifico parametri recuperati
			if (isset($_item_code))
			{
				if (!isset($_item_name))
				{
					$_item_name = $_item_code;
					logEvent("Impossibile recuperare il nome del tipo edificio.",1);
				}
				
				logDebug("Extracted building type ".$_item_code.":".$_item_name);
				
				// Salvo parametri comuni					
				$this->code			= $_item_code;
				$this->name			= $_item_name;
			}
			else
			{
				logEvent("Impossibile recuperare il codice del tipo edificio.",2);
			}
		}
	
		return($this);
	}
	
	/**
	* Metodo per ottenere il display name per un device type
	*/
	function getDisplayName()
	{
		$_name = $this->name;
				
		return($_name);
	}
}

/**
* Funzione per ottenere il codice di un tipo edificio dal nome
*/
function getBuildingTypeCodeFromName($typeList,$typeName)
{
	$_code = null;
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->name == $typeName)
			{
				$_code = $_type->code;
			 	break;
			}
		}
	}
	
	return($_code);
}

/**
* Funzione per ottenere il nome di un tipo edificio dal codice
*/
function getBuildingTypeNameFromCode($typeList,$typeCode)
{
	$_name = "Tipo Sconosciuto";
	
	foreach ($typeList as $_type_index => $_type)
	{
		if (isset($_type))
		{
			if ($_type->code == $typeCode)
			{
				$_name = $_type->name;
			 	break;
			}
		}
	}
	
	return($_name);
}

?>
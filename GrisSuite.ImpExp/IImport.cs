using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace GrisSuite
{
    public interface IImport
    {
        DataSet ImportFileToDataSet(string fileName);
    }
}

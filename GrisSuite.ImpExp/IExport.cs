using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Text;

namespace GrisSuite
{
    public interface IExport
    {
        void ExportToFile(string fileName);
        void ExportToStream(Stream stream);
    }
}

﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using GrisSuite.Data;
using GrisSuite.Data.dsDeviceTypesTableAdapters;

namespace GrisSuite {
    public class ImpExpDeviceTypeList : IImport, IExport {
        #region IExport Members

        public void ExportToFile(string fileName) {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            this.ExportToStream(fs);
            fs.Close();
        }

        public void ExportToStream(Stream stream) {
            dsDeviceTypes.DeviceTypeDataTable dt = new dsDeviceTypes.DeviceTypeDataTable();
            DeviceTypesTableAdapter da = new DeviceTypesTableAdapter();
            da.Fill(dt);

            XmlWriterSettings settings = new XmlWriterSettings {
                                                                   Indent = true,
                                                                   Encoding = Encoding.GetEncoding("iso-8859-1"),
                                                                   OmitXmlDeclaration = true
                                                               };

            using (XmlWriter writer = XmlWriter.Create(stream, settings)) {
                if (writer != null) {
                    writer.WriteStartDocument(true);
                    writer.WriteStartElement("Telefin");
                    writer.WriteStartElement("device_type");
                    writer.WriteAttributeString("version", GetDeviceTypeListVersion());
                    writer.WriteAttributeString("date", GetDeviceTypeListRevisionDate().ToString("dd.MM.yyyy"));

                    foreach (dsDeviceTypes.DeviceTypeRow deviceType in dt.Rows) {
                        if (!deviceType.IsDeviceTypeDescriptionNull() &&
                            !string.IsNullOrEmpty(deviceType.DeviceTypeDescription)) {
                            writer.WriteStartElement("type");
                            writer.WriteAttributeString("name", deviceType.DeviceTypeDescription);
                            writer.WriteAttributeString("default_name", deviceType.DefaultName);
                            writer.WriteAttributeString("code", deviceType.DeviceTypeID);
                            writer.WriteAttributeString("port_type", deviceType.PortType);
                            if (!deviceType.IsSnmpCommunityNull()) {
                                writer.WriteAttributeString("snmp_community", deviceType.SnmpCommunity);
                            }
                            writer.WriteEndElement();
                        }
                    }

                    writer.WriteEndElement(); // device_type
                    writer.WriteEndElement(); // Telefin
                    writer.WriteEndDocument();
                    writer.Flush();
                }
            }
        }

        #endregion

        #region IImport Members

        public System.Data.DataSet ImportFileToDataSet(string fileName) {
            throw new NotImplementedException();
        }

        #endregion

        private static string GetDeviceTypeListVersion() {
            Data.dsParametersTableAdapters.ParametersTableAdapter pa =
                new Data.dsParametersTableAdapters.ParametersTableAdapter();
            dsParameters ds = new dsParameters();
            int rc = pa.FillByName(ds.Parameters, "DeviceTypeListVersion");
            if (rc == 0) {
                // versione iniziale della DeviceTypeList, come restituita da WS
                return "2.0.6.1";
            }

            return ds.Parameters[0].ParameterValue;
        }

        private static DateTime GetDeviceTypeListRevisionDate() {
            Data.dsParametersTableAdapters.ParametersTableAdapter pa =
                new Data.dsParametersTableAdapters.ParametersTableAdapter();
            dsParameters ds = new dsParameters();
            int rc = pa.FillByName(ds.Parameters, "DeviceTypeListRevisionDate");
            DateTime defaultDate = new DateTime(2009, 12, 01, 10, 30, 0, 0);
            if (rc == 0) {
                // versione iniziale della DeviceTypeList, come restituita da WS
                return defaultDate;
            }

            DateTime paramDate;

            if (DateTime.TryParse(ds.Parameters[0].ParameterValue, out paramDate)) {
                return paramDate;
            }

            return defaultDate;
        }
    }
}
using System;
using System.IO;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using GrisSuite.Data;

namespace GrisSuite
{
    public class ImpExpRegionList : IImport, IExport
    {
        #region IImport Members

        public DataSet ImportFileToDataSet(string fileName)
        {
            dsConfig ds = new dsConfig();
            XmlDocument xDoc = new XmlDocument();
            StreamReader sr = null;
            try
            {
                if (File.Exists(fileName))
                {
                    sr = new StreamReader(fileName, System.Text.ASCIIEncoding.ASCII);
                    xDoc.Load(sr);
                    LoadRegions(ds, xDoc);
                    dsConfig.Update(ds, null, true);
                }
                else
                {
                    throw new Exception(string.Format("File not found {0}",fileName));
                }
            }
            catch 
            {
                throw;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
            return ds;

        }

        #endregion

        #region IExport Members

        public void ExportToFile(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            this.ExportToStream(fs);
            fs.Close();
        }

        public void ExportToStream(Stream stream)
        {
            dsConfig ds = new dsConfig();
            dsConfig.Fill(ds, new string[] { "Regions","Zones","Nodes" });

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
			settings.Encoding = Encoding.GetEncoding("iso-8859-1");
			settings.OmitXmlDeclaration = true;

            using (XmlWriter writer = XmlWriter.Create(stream, settings)) {
               // Write XML data.
				writer.WriteStartDocument(true);
				writer.WriteStartElement("telefin");
				writer.WriteStartElement("system");
				writer.WriteAttributeString("version", GetRegionListVersion());
				writer.WriteAttributeString("date", DateTime.Now.ToString("dd.MM.yyyy"));

				foreach (dsConfig.RegionsRow region in ds.Regions.Rows)
				{
				   writer.WriteStartElement("region");
				   writer.WriteAttributeString("id", region.GetDecodedRegId().ToString());
				   writer.WriteAttributeString("name", region.Name);

				   foreach (dsConfig.ZonesRow zone in region.GetChildRows("Regions_Zones"))
				   {
					   UInt16 dZonId = zone.GetDecodedZonId();
					   writer.WriteStartElement("zone");
					   writer.WriteAttributeString("id", dZonId.ToString());
					   if (dZonId != 0)
							writer.WriteAttributeString("old_id", FindOldId(zone.ZonID,dZonId).ToString());

					   writer.WriteAttributeString("name", zone.Name);

					   foreach (dsConfig.NodesRow node in zone.GetChildRows("Zones_Nodes"))
					   {
						   writer.WriteStartElement("node");
						   writer.WriteAttributeString("id", node.GetDecodedNodId().ToString());
						   writer.WriteAttributeString("name", node.Name);
						   if (!node.IsMetersNull() && node.Meters != 0)
								writer.WriteAttributeString("km", ((int)(node.Meters/1000)).ToString() );
							writer.WriteEndElement();
					   }
					   writer.WriteEndElement();
				   }
				   writer.WriteEndElement();
				}
               
				writer.WriteEndElement(); 
				writer.WriteEndElement();
				writer.WriteEndDocument();
				writer.Flush();
            }
        }

        #endregion

        #region Private Members

        //private Int64 EncodeRegId(UInt16 regId)
        //{
        //    UInt64 longValue = regId;
        //    longValue |= (UInt64)0xFFFF << 16;
        //    longValue |= (UInt64)0xFFFF << 32;

        //    return (Int64)longValue;
        //}

        //private UInt16 DecodeRegId(Int64 idComplete)
        //{
        //    Int64 longValue = idComplete & 0xFFFF;
        //    return (UInt16)longValue;
        //}

        //private Int64 EncodeZonId(UInt16 regId, UInt16 zonId)
        //{
        //    UInt64 longValue = regId;
        //    longValue |= (UInt64)zonId << 16;

        //    return (Int64)longValue;
        //}

        //private UInt16 DecodeZonId(Int64 idComplete)
        //{
        //    Int64 longValue = idComplete & 0xFFFF0000;
        //    return (UInt16)(longValue >> 16);
        //}


        //private Int64 EncodeNodId(UInt16 regId, UInt16 zonId, UInt16 nodId)
        //{
        //    UInt64 longValue = regId;
        //    longValue |= (UInt64)zonId << 16;
        //    longValue |= (UInt64)nodId << 32;

        //    return (Int64)longValue;
        //}

        //private UInt16 DecodeNodId(Int64 idComplete)
        //{
        //    Int64 longValue = idComplete & 0xFFFF00000000;
        //    return (UInt16)(longValue >> 32);
        //}

        private void LoadRegions(dsConfig ds, XmlDocument xDoc)
        {
            UInt16 regId = 0;
            UInt16 zonId = 0;
            UInt16 nodId = 0;

            try
            {
                DataTable xdtRegion = LocalUtil.GetTableFormXML(xDoc, "/telefin/system/region", "id", "name");

                foreach (DataRow xrRegion in xdtRegion.Rows)
                {
                    regId = LocalUtil.IsNullUInt16(xrRegion["id"]);
                    zonId = 0;
                    nodId = 0;

                    dsConfig.RegionsRow regionRow = ds.Regions.NewRegionsRow();
                    regionRow.SetDecodedRegId(regId);
                    regionRow.Name = LocalUtil.IsNullString(xrRegion["name"]);
                    regionRow.Removed = 0;
                    ds.Regions.AddRegionsRow(regionRow);

                    string getZones = string.Format("/telefin/system/region[@id=\"{0}\"]/zone", regId);

                    DataTable xdtZones = LocalUtil.GetTableFormXML(xDoc, getZones, "id", "name");

                    foreach (DataRow xrZones in xdtZones.Rows)
                    {
                        zonId = LocalUtil.IsNullUInt16(xrZones["id"]);
                        nodId = 0;

                        dsConfig.ZonesRow zonesRow = ds.Zones.NewZonesRow();
                        zonesRow.SetDecodedZonId(regId, zonId);
                        zonesRow.RegionsRow = regionRow;
                        zonesRow.Name = LocalUtil.IsNullString(xrZones["name"]);
                        zonesRow.Removed = 0;
                        ds.Zones.AddZonesRow(zonesRow);

                        string getNodes = string.Format("/telefin/system/region[@id=\"{0}\"]/zone[@id=\"{1}\"]/node", regId, zonId);

                        DataTable xdtNodes = LocalUtil.GetTableFormXML(xDoc, getNodes, "id", "name", "km");

                        foreach (DataRow xrNodes in xdtNodes.Rows)
                        {
                            nodId = LocalUtil.IsNullUInt16(xrNodes["id"]);
                            dsConfig.NodesRow nodesRow = ds.Nodes.NewNodesRow();
                            nodesRow.SetDecodedNodId(regId, zonId, nodId);
                            nodesRow.ZonesRow = zonesRow;
                            nodesRow.Name = LocalUtil.IsNullString(xrNodes["name"]);
                            nodesRow.Meters = LocalUtil.IsNullInt(xrNodes["km"]) * 1000;
                            ds.Nodes.AddNodesRow(nodesRow);
                        }
                    }
                }
                ds.AcceptChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error on processing RegId={0}, ZonId={1}, NodId={2}", regId, zonId, nodId), ex);
            }

        }

        private UInt16 FindOldId(long completeId, UInt16 id)
        {
            switch (completeId) 
            {
                case 73072641	:return 217;	// Compartimento di Torino	Linea Alessandria - S. Giuseppe di Cairo
                case 78839810	:return 212;	// Compartimento di Milano	Linea Arona - Novara - Alessandria
                case 72679425	:return 212;	// Compartimento di Torino	Linea Arona - Novara - Mortara - Alessandria
                case 73138177	:return 441;	// Compartimento di Torino	Linea Asti - Genova
                case 104988678	:return 441;	// Compartimento di Genova	Linea Asti - Genova
                case 118161416	:return 453;	// Compartimento di Firenze	Linea Borgo S. Lorenzo - Firenze
                case 137756683	:return 1102;   // Compartimento di Napoli	Linea Cassino - Caserta
                case 150863885	:return 1302;   // Compartimento di Reggio Calabria	Linea Catanzaro Lido - Marina S. Lorenzo
                case 150994957 	:return 628;	// Compartimento di Reggio Calabria	Linea Cosenza - Sibari - Metaponto
                case 78774274	:return 256;	// Compartimento di Milano	Linea Laveno - Oleggio
                case 118226952	:return 466;	// Compartimento di Firenze	Linea Lucca - Pisa S. Rossore
                case 118292488	:return 467;	// Compartimento di Firenze	Linea Lucca - Pistoia
                case 78708738	:return 240;	// Compartimento di Milano	Linea Milano - Torino
                case 137822219	:return 626;	// Compartimento di Napoli	Linea Napoli - Battipaglia
                case 137691147	:return 1101;   // Compartimento di Napoli	Linea Napoli - Formia
                case 157351950	:return 656;	// Compartimento di Palermo	Linea Palermo - Trapani
                case 150929421	:return 626;	// Compartimento di Reggio Calabria	Linea Reggio Calabria - Melito
                case 131334154	:return 546;	// Compartimento di Roma	Linea Roma - Formia
                case 72810497 	:return 204;	// Compartimento di Torino	Linea Santhia` - Biella - Novara
                case 150798349	:return 1301;   // Compartimento di Reggio Calabria	Linea Sibari - Catanzaro Lido
                case 72744961	:return 119;	// Compartimento di Torino	Linea Torino - Alessandria
                case 72941569	:return 201;	// Compartimento di Torino	Linea Torino - Modane
                case 72876033	:return 240;	// Compartimento di Torino	Linea Torino - Novara
                case 73007105	:return 223;	// Compartimento di Torino	Linea Torino - S. Giuseppe di Cairo
                case 104923142	:return 223;	// Compartimento di Genova	Linea Torino - S. Giuseppe di Cairo
                case 85327875	:return 118;	// Compartimento di Verona	Linea Verona - Brescia
                default : return id;
            }
        }

		private string GetRegionListVersion ()
		{
			GrisSuite.Data.dsParametersTableAdapters.ParametersTableAdapter pa = new GrisSuite.Data.dsParametersTableAdapters.ParametersTableAdapter();
			dsParameters ds = new dsParameters();
			int rc = pa.FillByName(ds.Parameters, "RegionListVersion");
			if ( rc == 0 )
			{
				return "1.0.3.0";
			}
			else
			{
				return ds.Parameters[0].ParameterValue;
			}
		}

        #endregion
    }
}

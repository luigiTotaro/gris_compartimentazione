using System;
using System.Xml;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace GrisSuite
{
    internal class LocalUtil
    {


        internal static DataTable GetTableFormXML(XmlDocument xDoc, string sPath, params string[] cols)
        { 
            DataTable dt = new DataTable();

            if (cols != null)
            {
                for (int i = 0; i < cols.Length; i++)
                {
                    dt.Columns.Add(cols[i]);
                }
            }

            XmlNodeList xnl = xDoc.SelectNodes(sPath);

            foreach (XmlNode xn in xnl)
            {
                XmlNodeList xnl2 = xn.ChildNodes;
                XmlAttributeCollection xal2 = xn.Attributes;

                // adegua la struttura della tabella
                foreach (XmlNode xn2 in xnl2)
                {
                    if ((xn2 is XmlAttribute) || (xn2 is XmlElement && !xn2.HasChildNodes && xn2.Value != string.Empty))
                    {
                        if (!dt.Columns.Contains(xn2.Name))
                            dt.Columns.Add(xn2.Name);
                    }
                }
                // adegua la struttura della tabella Attributi
                foreach (XmlAttribute xa in xal2)
                {
                    if (!dt.Columns.Contains(xa.Name))
                        dt.Columns.Add(xa.Name);
                }

                // crea la riga
                DataRow r = dt.NewRow();
                // elements simple
                foreach (XmlNode xn2 in xnl2)
                {
                    if ((xn2 is XmlElement && !xn2.HasChildNodes && xn2.Value != string.Empty))
                    {
                        r[xn2.Name] = xn2.Value;
                    }
                }
                // attributes simple
                foreach (XmlAttribute xa in xal2)
                {
                    r[xa.Name] = xa.Value;
                }

                dt.Rows.Add(r);
            }
            return dt;
        }


        static internal string IsNullString(object o)
        {
            if (o == null || o == DBNull.Value)
                return string.Empty;
            else
                return o.ToString();
        }

        static internal int IsNullInt(object o)
        {
            if (o == null || o == DBNull.Value)
                return 0;
            else if (!(o is int))
                return int.Parse(o.ToString());
            else
                return (int)o;
        }

        static internal UInt16 IsNullUInt16(object o)
        {
            if (o == null || o == DBNull.Value)
                return 0;
            else if (!(o is UInt16))
                return UInt16.Parse(o.ToString());
            else
                return (UInt16)o;
        }


        static internal bool IsNullBool(object o)
        {
            if (o == null || o == DBNull.Value)
                return false;
            else if (o is int)
                return ((int)o != 0);
            else if (o is uint)
                return ((uint)o != 0);
            else if (o is long)
                return ((long)o != 0);
            else if (o is string)
                if (o.ToString() == string.Empty)
                    return false;
                else
                    return bool.Parse(o.ToString());
            else
                return (bool)o;

        }

        static internal decimal IsNullDecimal(object o)
        {
            if (o == null || o == DBNull.Value)
                return 0;
            else
                return (decimal)o;
        }

        static internal Double IsNullDouble(object o)
        {
            if (o == null || o == DBNull.Value)
                return 0;
            else if (!(o is Double))
                return Double.Parse(o.ToString());
            else
                return (Double)o;
        }


        static internal DateTime? IsNullDatetime(object o)
        {
            if (o == null || o == DBNull.Value)
                return null;
            else if (o is String)
                if (o.ToString().IndexOf(':') >= 0)
                    return DateTime.Parse("1900-01-01T" + o.ToString());
                else
                    return DateTime.Parse(o.ToString());
            else if (o is TimeSpan)
                return DateTime.Parse("1900-01-01").Add((TimeSpan)o);
            else
                return (DateTime)o;
        }



    }
}
